graph [
  node [
    id 0
    label "uj&#281;cie"
    origin "text"
  ]
  node [
    id 1
    label "monitoring"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "gdzie"
    origin "text"
  ]
  node [
    id 4
    label "nieznany"
    origin "text"
  ]
  node [
    id 5
    label "sprawca"
    origin "text"
  ]
  node [
    id 6
    label "rysowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "klucz"
    origin "text"
  ]
  node [
    id 8
    label "nowa"
    origin "text"
  ]
  node [
    id 9
    label "lamborghini"
    origin "text"
  ]
  node [
    id 10
    label "aventador"
    origin "text"
  ]
  node [
    id 11
    label "sekunda"
    origin "text"
  ]
  node [
    id 12
    label "zaparkowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pod"
    origin "text"
  ]
  node [
    id 14
    label "dom"
    origin "text"
  ]
  node [
    id 15
    label "pochwytanie"
  ]
  node [
    id 16
    label "wording"
  ]
  node [
    id 17
    label "wzbudzenie"
  ]
  node [
    id 18
    label "withdrawal"
  ]
  node [
    id 19
    label "capture"
  ]
  node [
    id 20
    label "podniesienie"
  ]
  node [
    id 21
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 22
    label "wytw&#243;r"
  ]
  node [
    id 23
    label "film"
  ]
  node [
    id 24
    label "scena"
  ]
  node [
    id 25
    label "zapisanie"
  ]
  node [
    id 26
    label "prezentacja"
  ]
  node [
    id 27
    label "rzucenie"
  ]
  node [
    id 28
    label "zamkni&#281;cie"
  ]
  node [
    id 29
    label "zabranie"
  ]
  node [
    id 30
    label "poinformowanie"
  ]
  node [
    id 31
    label "zaaresztowanie"
  ]
  node [
    id 32
    label "strona"
  ]
  node [
    id 33
    label "wzi&#281;cie"
  ]
  node [
    id 34
    label "pokaz&#243;wka"
  ]
  node [
    id 35
    label "prezenter"
  ]
  node [
    id 36
    label "wypowied&#378;"
  ]
  node [
    id 37
    label "impreza"
  ]
  node [
    id 38
    label "grafika_u&#380;ytkowa"
  ]
  node [
    id 39
    label "show"
  ]
  node [
    id 40
    label "szkolenie"
  ]
  node [
    id 41
    label "komunikat"
  ]
  node [
    id 42
    label "informacja"
  ]
  node [
    id 43
    label "zaj&#281;cie"
  ]
  node [
    id 44
    label "wyniesienie"
  ]
  node [
    id 45
    label "pickings"
  ]
  node [
    id 46
    label "pozabieranie"
  ]
  node [
    id 47
    label "pousuwanie"
  ]
  node [
    id 48
    label "przesuni&#281;cie"
  ]
  node [
    id 49
    label "doprowadzenie"
  ]
  node [
    id 50
    label "przeniesienie"
  ]
  node [
    id 51
    label "spowodowanie"
  ]
  node [
    id 52
    label "zniewolenie"
  ]
  node [
    id 53
    label "zniesienie"
  ]
  node [
    id 54
    label "z&#322;apanie"
  ]
  node [
    id 55
    label "coitus_interruptus"
  ]
  node [
    id 56
    label "zrobienie"
  ]
  node [
    id 57
    label "udanie_si&#281;"
  ]
  node [
    id 58
    label "wywiezienie"
  ]
  node [
    id 59
    label "removal"
  ]
  node [
    id 60
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 61
    label "telling"
  ]
  node [
    id 62
    label "arousal"
  ]
  node [
    id 63
    label "wywo&#322;anie"
  ]
  node [
    id 64
    label "rozbudzenie"
  ]
  node [
    id 65
    label "podwy&#380;szenie"
  ]
  node [
    id 66
    label "kurtyna"
  ]
  node [
    id 67
    label "akt"
  ]
  node [
    id 68
    label "widzownia"
  ]
  node [
    id 69
    label "sznurownia"
  ]
  node [
    id 70
    label "dramaturgy"
  ]
  node [
    id 71
    label "sphere"
  ]
  node [
    id 72
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 73
    label "sztuka"
  ]
  node [
    id 74
    label "budka_suflera"
  ]
  node [
    id 75
    label "epizod"
  ]
  node [
    id 76
    label "wydarzenie"
  ]
  node [
    id 77
    label "fragment"
  ]
  node [
    id 78
    label "k&#322;&#243;tnia"
  ]
  node [
    id 79
    label "kiesze&#324;"
  ]
  node [
    id 80
    label "stadium"
  ]
  node [
    id 81
    label "podest"
  ]
  node [
    id 82
    label "horyzont"
  ]
  node [
    id 83
    label "teren"
  ]
  node [
    id 84
    label "instytucja"
  ]
  node [
    id 85
    label "przedstawienie"
  ]
  node [
    id 86
    label "proscenium"
  ]
  node [
    id 87
    label "przedstawianie"
  ]
  node [
    id 88
    label "nadscenie"
  ]
  node [
    id 89
    label "antyteatr"
  ]
  node [
    id 90
    label "przedstawia&#263;"
  ]
  node [
    id 91
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 92
    label "dmuchni&#281;cie"
  ]
  node [
    id 93
    label "niesienie"
  ]
  node [
    id 94
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 95
    label "nakazanie"
  ]
  node [
    id 96
    label "ruszenie"
  ]
  node [
    id 97
    label "pokonanie"
  ]
  node [
    id 98
    label "take"
  ]
  node [
    id 99
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 100
    label "wymienienie_si&#281;"
  ]
  node [
    id 101
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 102
    label "uciekni&#281;cie"
  ]
  node [
    id 103
    label "pobranie"
  ]
  node [
    id 104
    label "poczytanie"
  ]
  node [
    id 105
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 106
    label "u&#380;ycie"
  ]
  node [
    id 107
    label "powodzenie"
  ]
  node [
    id 108
    label "wej&#347;cie"
  ]
  node [
    id 109
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 110
    label "przyj&#281;cie"
  ]
  node [
    id 111
    label "kupienie"
  ]
  node [
    id 112
    label "bite"
  ]
  node [
    id 113
    label "dostanie"
  ]
  node [
    id 114
    label "wyruchanie"
  ]
  node [
    id 115
    label "odziedziczenie"
  ]
  node [
    id 116
    label "otrzymanie"
  ]
  node [
    id 117
    label "branie"
  ]
  node [
    id 118
    label "wygranie"
  ]
  node [
    id 119
    label "wzi&#261;&#263;"
  ]
  node [
    id 120
    label "obj&#281;cie"
  ]
  node [
    id 121
    label "w&#322;o&#380;enie"
  ]
  node [
    id 122
    label "zacz&#281;cie"
  ]
  node [
    id 123
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 124
    label "commitment"
  ]
  node [
    id 125
    label "zgarni&#281;cie"
  ]
  node [
    id 126
    label "czynno&#347;&#263;"
  ]
  node [
    id 127
    label "przedmiot"
  ]
  node [
    id 128
    label "p&#322;&#243;d"
  ]
  node [
    id 129
    label "work"
  ]
  node [
    id 130
    label "rezultat"
  ]
  node [
    id 131
    label "konwulsja"
  ]
  node [
    id 132
    label "pierdolni&#281;cie"
  ]
  node [
    id 133
    label "poruszenie"
  ]
  node [
    id 134
    label "opuszczenie"
  ]
  node [
    id 135
    label "most"
  ]
  node [
    id 136
    label "odej&#347;cie"
  ]
  node [
    id 137
    label "przewr&#243;cenie"
  ]
  node [
    id 138
    label "wyzwanie"
  ]
  node [
    id 139
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 140
    label "skonstruowanie"
  ]
  node [
    id 141
    label "grzmotni&#281;cie"
  ]
  node [
    id 142
    label "zdecydowanie"
  ]
  node [
    id 143
    label "przeznaczenie"
  ]
  node [
    id 144
    label "&#347;wiat&#322;o"
  ]
  node [
    id 145
    label "przemieszczenie"
  ]
  node [
    id 146
    label "wyposa&#380;enie"
  ]
  node [
    id 147
    label "podejrzenie"
  ]
  node [
    id 148
    label "czar"
  ]
  node [
    id 149
    label "shy"
  ]
  node [
    id 150
    label "oddzia&#322;anie"
  ]
  node [
    id 151
    label "cie&#324;"
  ]
  node [
    id 152
    label "zrezygnowanie"
  ]
  node [
    id 153
    label "porzucenie"
  ]
  node [
    id 154
    label "atak"
  ]
  node [
    id 155
    label "powiedzenie"
  ]
  node [
    id 156
    label "towar"
  ]
  node [
    id 157
    label "rzucanie"
  ]
  node [
    id 158
    label "przekazanie"
  ]
  node [
    id 159
    label "arrangement"
  ]
  node [
    id 160
    label "rozpisanie"
  ]
  node [
    id 161
    label "preservation"
  ]
  node [
    id 162
    label "wype&#322;nienie"
  ]
  node [
    id 163
    label "utrwalenie"
  ]
  node [
    id 164
    label "lekarstwo"
  ]
  node [
    id 165
    label "record"
  ]
  node [
    id 166
    label "w&#322;&#261;czenie"
  ]
  node [
    id 167
    label "zalecenie"
  ]
  node [
    id 168
    label "powi&#281;kszenie"
  ]
  node [
    id 169
    label "movement"
  ]
  node [
    id 170
    label "raise"
  ]
  node [
    id 171
    label "obrz&#281;d"
  ]
  node [
    id 172
    label "pomo&#380;enie"
  ]
  node [
    id 173
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 174
    label "erection"
  ]
  node [
    id 175
    label "za&#322;apanie"
  ]
  node [
    id 176
    label "ulepszenie"
  ]
  node [
    id 177
    label "policzenie"
  ]
  node [
    id 178
    label "przybli&#380;enie"
  ]
  node [
    id 179
    label "erecting"
  ]
  node [
    id 180
    label "msza"
  ]
  node [
    id 181
    label "pochwalenie"
  ]
  node [
    id 182
    label "wywy&#380;szenie"
  ]
  node [
    id 183
    label "wy&#380;szy"
  ]
  node [
    id 184
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 185
    label "zmienienie"
  ]
  node [
    id 186
    label "odbudowanie"
  ]
  node [
    id 187
    label "kartka"
  ]
  node [
    id 188
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 189
    label "logowanie"
  ]
  node [
    id 190
    label "plik"
  ]
  node [
    id 191
    label "s&#261;d"
  ]
  node [
    id 192
    label "adres_internetowy"
  ]
  node [
    id 193
    label "linia"
  ]
  node [
    id 194
    label "serwis_internetowy"
  ]
  node [
    id 195
    label "posta&#263;"
  ]
  node [
    id 196
    label "bok"
  ]
  node [
    id 197
    label "skr&#281;canie"
  ]
  node [
    id 198
    label "skr&#281;ca&#263;"
  ]
  node [
    id 199
    label "orientowanie"
  ]
  node [
    id 200
    label "skr&#281;ci&#263;"
  ]
  node [
    id 201
    label "zorientowanie"
  ]
  node [
    id 202
    label "ty&#322;"
  ]
  node [
    id 203
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 204
    label "layout"
  ]
  node [
    id 205
    label "obiekt"
  ]
  node [
    id 206
    label "zorientowa&#263;"
  ]
  node [
    id 207
    label "pagina"
  ]
  node [
    id 208
    label "podmiot"
  ]
  node [
    id 209
    label "g&#243;ra"
  ]
  node [
    id 210
    label "orientowa&#263;"
  ]
  node [
    id 211
    label "voice"
  ]
  node [
    id 212
    label "orientacja"
  ]
  node [
    id 213
    label "prz&#243;d"
  ]
  node [
    id 214
    label "internet"
  ]
  node [
    id 215
    label "powierzchnia"
  ]
  node [
    id 216
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 217
    label "forma"
  ]
  node [
    id 218
    label "skr&#281;cenie"
  ]
  node [
    id 219
    label "animatronika"
  ]
  node [
    id 220
    label "odczulenie"
  ]
  node [
    id 221
    label "odczula&#263;"
  ]
  node [
    id 222
    label "blik"
  ]
  node [
    id 223
    label "odczuli&#263;"
  ]
  node [
    id 224
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 225
    label "muza"
  ]
  node [
    id 226
    label "postprodukcja"
  ]
  node [
    id 227
    label "block"
  ]
  node [
    id 228
    label "trawiarnia"
  ]
  node [
    id 229
    label "sklejarka"
  ]
  node [
    id 230
    label "filmoteka"
  ]
  node [
    id 231
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 232
    label "klatka"
  ]
  node [
    id 233
    label "rozbieg&#243;wka"
  ]
  node [
    id 234
    label "napisy"
  ]
  node [
    id 235
    label "ta&#347;ma"
  ]
  node [
    id 236
    label "odczulanie"
  ]
  node [
    id 237
    label "anamorfoza"
  ]
  node [
    id 238
    label "dorobek"
  ]
  node [
    id 239
    label "ty&#322;&#243;wka"
  ]
  node [
    id 240
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 241
    label "b&#322;ona"
  ]
  node [
    id 242
    label "emulsja_fotograficzna"
  ]
  node [
    id 243
    label "photograph"
  ]
  node [
    id 244
    label "czo&#322;&#243;wka"
  ]
  node [
    id 245
    label "rola"
  ]
  node [
    id 246
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 247
    label "przyskrzynienie"
  ]
  node [
    id 248
    label "przygaszenie"
  ]
  node [
    id 249
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 250
    label "profligacy"
  ]
  node [
    id 251
    label "dissolution"
  ]
  node [
    id 252
    label "ukrycie"
  ]
  node [
    id 253
    label "miejsce"
  ]
  node [
    id 254
    label "completion"
  ]
  node [
    id 255
    label "end"
  ]
  node [
    id 256
    label "mechanizm"
  ]
  node [
    id 257
    label "exit"
  ]
  node [
    id 258
    label "zatrzymanie"
  ]
  node [
    id 259
    label "rozwi&#261;zanie"
  ]
  node [
    id 260
    label "zawarcie"
  ]
  node [
    id 261
    label "closing"
  ]
  node [
    id 262
    label "z&#322;o&#380;enie"
  ]
  node [
    id 263
    label "release"
  ]
  node [
    id 264
    label "umieszczenie"
  ]
  node [
    id 265
    label "zablokowanie"
  ]
  node [
    id 266
    label "zako&#324;czenie"
  ]
  node [
    id 267
    label "pozamykanie"
  ]
  node [
    id 268
    label "obserwacja"
  ]
  node [
    id 269
    label "kamera"
  ]
  node [
    id 270
    label "ochrona"
  ]
  node [
    id 271
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 272
    label "formacja"
  ]
  node [
    id 273
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 274
    label "obstawianie"
  ]
  node [
    id 275
    label "obstawienie"
  ]
  node [
    id 276
    label "tarcza"
  ]
  node [
    id 277
    label "ubezpieczenie"
  ]
  node [
    id 278
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 279
    label "transportacja"
  ]
  node [
    id 280
    label "obstawia&#263;"
  ]
  node [
    id 281
    label "borowiec"
  ]
  node [
    id 282
    label "chemical_bond"
  ]
  node [
    id 283
    label "badanie"
  ]
  node [
    id 284
    label "proces_my&#347;lowy"
  ]
  node [
    id 285
    label "remark"
  ]
  node [
    id 286
    label "metoda"
  ]
  node [
    id 287
    label "zasada"
  ]
  node [
    id 288
    label "stwierdzenie"
  ]
  node [
    id 289
    label "observation"
  ]
  node [
    id 290
    label "krosownica"
  ]
  node [
    id 291
    label "wideotelefon"
  ]
  node [
    id 292
    label "przyrz&#261;d"
  ]
  node [
    id 293
    label "ranek"
  ]
  node [
    id 294
    label "doba"
  ]
  node [
    id 295
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 296
    label "noc"
  ]
  node [
    id 297
    label "podwiecz&#243;r"
  ]
  node [
    id 298
    label "po&#322;udnie"
  ]
  node [
    id 299
    label "godzina"
  ]
  node [
    id 300
    label "przedpo&#322;udnie"
  ]
  node [
    id 301
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 302
    label "long_time"
  ]
  node [
    id 303
    label "wiecz&#243;r"
  ]
  node [
    id 304
    label "t&#322;usty_czwartek"
  ]
  node [
    id 305
    label "popo&#322;udnie"
  ]
  node [
    id 306
    label "walentynki"
  ]
  node [
    id 307
    label "czynienie_si&#281;"
  ]
  node [
    id 308
    label "s&#322;o&#324;ce"
  ]
  node [
    id 309
    label "rano"
  ]
  node [
    id 310
    label "tydzie&#324;"
  ]
  node [
    id 311
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 312
    label "wzej&#347;cie"
  ]
  node [
    id 313
    label "czas"
  ]
  node [
    id 314
    label "wsta&#263;"
  ]
  node [
    id 315
    label "day"
  ]
  node [
    id 316
    label "termin"
  ]
  node [
    id 317
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 318
    label "wstanie"
  ]
  node [
    id 319
    label "przedwiecz&#243;r"
  ]
  node [
    id 320
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 321
    label "Sylwester"
  ]
  node [
    id 322
    label "poprzedzanie"
  ]
  node [
    id 323
    label "czasoprzestrze&#324;"
  ]
  node [
    id 324
    label "laba"
  ]
  node [
    id 325
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 326
    label "chronometria"
  ]
  node [
    id 327
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 328
    label "rachuba_czasu"
  ]
  node [
    id 329
    label "przep&#322;ywanie"
  ]
  node [
    id 330
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 331
    label "czasokres"
  ]
  node [
    id 332
    label "odczyt"
  ]
  node [
    id 333
    label "chwila"
  ]
  node [
    id 334
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 335
    label "dzieje"
  ]
  node [
    id 336
    label "kategoria_gramatyczna"
  ]
  node [
    id 337
    label "poprzedzenie"
  ]
  node [
    id 338
    label "trawienie"
  ]
  node [
    id 339
    label "pochodzi&#263;"
  ]
  node [
    id 340
    label "period"
  ]
  node [
    id 341
    label "okres_czasu"
  ]
  node [
    id 342
    label "poprzedza&#263;"
  ]
  node [
    id 343
    label "schy&#322;ek"
  ]
  node [
    id 344
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 345
    label "odwlekanie_si&#281;"
  ]
  node [
    id 346
    label "zegar"
  ]
  node [
    id 347
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 348
    label "czwarty_wymiar"
  ]
  node [
    id 349
    label "pochodzenie"
  ]
  node [
    id 350
    label "koniugacja"
  ]
  node [
    id 351
    label "Zeitgeist"
  ]
  node [
    id 352
    label "trawi&#263;"
  ]
  node [
    id 353
    label "pogoda"
  ]
  node [
    id 354
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 355
    label "poprzedzi&#263;"
  ]
  node [
    id 356
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 357
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 358
    label "time_period"
  ]
  node [
    id 359
    label "nazewnictwo"
  ]
  node [
    id 360
    label "term"
  ]
  node [
    id 361
    label "przypadni&#281;cie"
  ]
  node [
    id 362
    label "ekspiracja"
  ]
  node [
    id 363
    label "przypa&#347;&#263;"
  ]
  node [
    id 364
    label "chronogram"
  ]
  node [
    id 365
    label "praktyka"
  ]
  node [
    id 366
    label "nazwa"
  ]
  node [
    id 367
    label "spotkanie"
  ]
  node [
    id 368
    label "night"
  ]
  node [
    id 369
    label "zach&#243;d"
  ]
  node [
    id 370
    label "vesper"
  ]
  node [
    id 371
    label "pora"
  ]
  node [
    id 372
    label "odwieczerz"
  ]
  node [
    id 373
    label "blady_&#347;wit"
  ]
  node [
    id 374
    label "podkurek"
  ]
  node [
    id 375
    label "aurora"
  ]
  node [
    id 376
    label "wsch&#243;d"
  ]
  node [
    id 377
    label "zjawisko"
  ]
  node [
    id 378
    label "&#347;rodek"
  ]
  node [
    id 379
    label "obszar"
  ]
  node [
    id 380
    label "Ziemia"
  ]
  node [
    id 381
    label "dwunasta"
  ]
  node [
    id 382
    label "strona_&#347;wiata"
  ]
  node [
    id 383
    label "dopo&#322;udnie"
  ]
  node [
    id 384
    label "p&#243;&#322;noc"
  ]
  node [
    id 385
    label "nokturn"
  ]
  node [
    id 386
    label "time"
  ]
  node [
    id 387
    label "p&#243;&#322;godzina"
  ]
  node [
    id 388
    label "jednostka_czasu"
  ]
  node [
    id 389
    label "minuta"
  ]
  node [
    id 390
    label "kwadrans"
  ]
  node [
    id 391
    label "jednostka_geologiczna"
  ]
  node [
    id 392
    label "weekend"
  ]
  node [
    id 393
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 394
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 395
    label "miesi&#261;c"
  ]
  node [
    id 396
    label "S&#322;o&#324;ce"
  ]
  node [
    id 397
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 398
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 399
    label "kochanie"
  ]
  node [
    id 400
    label "sunlight"
  ]
  node [
    id 401
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 402
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 403
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 404
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 405
    label "mount"
  ]
  node [
    id 406
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 407
    label "wzej&#347;&#263;"
  ]
  node [
    id 408
    label "ascend"
  ]
  node [
    id 409
    label "kuca&#263;"
  ]
  node [
    id 410
    label "wyzdrowie&#263;"
  ]
  node [
    id 411
    label "opu&#347;ci&#263;"
  ]
  node [
    id 412
    label "rise"
  ]
  node [
    id 413
    label "arise"
  ]
  node [
    id 414
    label "stan&#261;&#263;"
  ]
  node [
    id 415
    label "przesta&#263;"
  ]
  node [
    id 416
    label "wyzdrowienie"
  ]
  node [
    id 417
    label "le&#380;enie"
  ]
  node [
    id 418
    label "kl&#281;czenie"
  ]
  node [
    id 419
    label "uniesienie_si&#281;"
  ]
  node [
    id 420
    label "siedzenie"
  ]
  node [
    id 421
    label "beginning"
  ]
  node [
    id 422
    label "przestanie"
  ]
  node [
    id 423
    label "grudzie&#324;"
  ]
  node [
    id 424
    label "luty"
  ]
  node [
    id 425
    label "sprawiciel"
  ]
  node [
    id 426
    label "cz&#322;owiek"
  ]
  node [
    id 427
    label "ludzko&#347;&#263;"
  ]
  node [
    id 428
    label "asymilowanie"
  ]
  node [
    id 429
    label "wapniak"
  ]
  node [
    id 430
    label "asymilowa&#263;"
  ]
  node [
    id 431
    label "os&#322;abia&#263;"
  ]
  node [
    id 432
    label "hominid"
  ]
  node [
    id 433
    label "podw&#322;adny"
  ]
  node [
    id 434
    label "os&#322;abianie"
  ]
  node [
    id 435
    label "g&#322;owa"
  ]
  node [
    id 436
    label "figura"
  ]
  node [
    id 437
    label "portrecista"
  ]
  node [
    id 438
    label "dwun&#243;g"
  ]
  node [
    id 439
    label "profanum"
  ]
  node [
    id 440
    label "mikrokosmos"
  ]
  node [
    id 441
    label "nasada"
  ]
  node [
    id 442
    label "duch"
  ]
  node [
    id 443
    label "antropochoria"
  ]
  node [
    id 444
    label "osoba"
  ]
  node [
    id 445
    label "wz&#243;r"
  ]
  node [
    id 446
    label "senior"
  ]
  node [
    id 447
    label "oddzia&#322;ywanie"
  ]
  node [
    id 448
    label "Adam"
  ]
  node [
    id 449
    label "homo_sapiens"
  ]
  node [
    id 450
    label "polifag"
  ]
  node [
    id 451
    label "kre&#347;li&#263;"
  ]
  node [
    id 452
    label "draw"
  ]
  node [
    id 453
    label "kancerowa&#263;"
  ]
  node [
    id 454
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 455
    label "describe"
  ]
  node [
    id 456
    label "opowiada&#263;"
  ]
  node [
    id 457
    label "robi&#263;"
  ]
  node [
    id 458
    label "clear"
  ]
  node [
    id 459
    label "usuwa&#263;"
  ]
  node [
    id 460
    label "report"
  ]
  node [
    id 461
    label "tworzy&#263;"
  ]
  node [
    id 462
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 463
    label "delineate"
  ]
  node [
    id 464
    label "prawi&#263;"
  ]
  node [
    id 465
    label "relate"
  ]
  node [
    id 466
    label "uszkadza&#263;"
  ]
  node [
    id 467
    label "kod"
  ]
  node [
    id 468
    label "spis"
  ]
  node [
    id 469
    label "narz&#281;dzie"
  ]
  node [
    id 470
    label "zbi&#243;r"
  ]
  node [
    id 471
    label "szyfr"
  ]
  node [
    id 472
    label "kliniec"
  ]
  node [
    id 473
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 474
    label "nakr&#281;ca&#263;"
  ]
  node [
    id 475
    label "za&#322;&#261;cznik"
  ]
  node [
    id 476
    label "instrument_strunowy"
  ]
  node [
    id 477
    label "szyk"
  ]
  node [
    id 478
    label "obja&#347;nienie"
  ]
  node [
    id 479
    label "z&#322;&#261;czenie"
  ]
  node [
    id 480
    label "kompleks"
  ]
  node [
    id 481
    label "radical"
  ]
  node [
    id 482
    label "spos&#243;b"
  ]
  node [
    id 483
    label "znak_muzyczny"
  ]
  node [
    id 484
    label "code"
  ]
  node [
    id 485
    label "przycisk"
  ]
  node [
    id 486
    label "podstawa"
  ]
  node [
    id 487
    label "kryptografia"
  ]
  node [
    id 488
    label "cipher"
  ]
  node [
    id 489
    label "whole"
  ]
  node [
    id 490
    label "wypowiedzenie"
  ]
  node [
    id 491
    label "sznyt"
  ]
  node [
    id 492
    label "skrzyd&#322;o"
  ]
  node [
    id 493
    label "kolejno&#347;&#263;"
  ]
  node [
    id 494
    label "explanation"
  ]
  node [
    id 495
    label "zrozumia&#322;y"
  ]
  node [
    id 496
    label "catalog"
  ]
  node [
    id 497
    label "pozycja"
  ]
  node [
    id 498
    label "tekst"
  ]
  node [
    id 499
    label "sumariusz"
  ]
  node [
    id 500
    label "book"
  ]
  node [
    id 501
    label "stock"
  ]
  node [
    id 502
    label "figurowa&#263;"
  ]
  node [
    id 503
    label "wyliczanka"
  ]
  node [
    id 504
    label "struktura"
  ]
  node [
    id 505
    label "psychika"
  ]
  node [
    id 506
    label "group"
  ]
  node [
    id 507
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 508
    label "ligand"
  ]
  node [
    id 509
    label "sum"
  ]
  node [
    id 510
    label "band"
  ]
  node [
    id 511
    label "wi&#261;zanie_koordynacyjne"
  ]
  node [
    id 512
    label "niezb&#281;dnik"
  ]
  node [
    id 513
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 514
    label "tylec"
  ]
  node [
    id 515
    label "urz&#261;dzenie"
  ]
  node [
    id 516
    label "dodatek"
  ]
  node [
    id 517
    label "language"
  ]
  node [
    id 518
    label "szyfrowanie"
  ]
  node [
    id 519
    label "ci&#261;g"
  ]
  node [
    id 520
    label "szablon"
  ]
  node [
    id 521
    label "composing"
  ]
  node [
    id 522
    label "zespolenie"
  ]
  node [
    id 523
    label "zjednoczenie"
  ]
  node [
    id 524
    label "kompozycja"
  ]
  node [
    id 525
    label "element"
  ]
  node [
    id 526
    label "junction"
  ]
  node [
    id 527
    label "zgrzeina"
  ]
  node [
    id 528
    label "akt_p&#322;ciowy"
  ]
  node [
    id 529
    label "joining"
  ]
  node [
    id 530
    label "kruszywo"
  ]
  node [
    id 531
    label "sklepienie"
  ]
  node [
    id 532
    label "element_konstrukcyjny"
  ]
  node [
    id 533
    label "egzemplarz"
  ]
  node [
    id 534
    label "series"
  ]
  node [
    id 535
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 536
    label "uprawianie"
  ]
  node [
    id 537
    label "praca_rolnicza"
  ]
  node [
    id 538
    label "collection"
  ]
  node [
    id 539
    label "dane"
  ]
  node [
    id 540
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 541
    label "pakiet_klimatyczny"
  ]
  node [
    id 542
    label "poj&#281;cie"
  ]
  node [
    id 543
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 544
    label "gathering"
  ]
  node [
    id 545
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 546
    label "album"
  ]
  node [
    id 547
    label "model"
  ]
  node [
    id 548
    label "tryb"
  ]
  node [
    id 549
    label "nature"
  ]
  node [
    id 550
    label "pot&#281;ga"
  ]
  node [
    id 551
    label "documentation"
  ]
  node [
    id 552
    label "column"
  ]
  node [
    id 553
    label "zasadzi&#263;"
  ]
  node [
    id 554
    label "za&#322;o&#380;enie"
  ]
  node [
    id 555
    label "punkt_odniesienia"
  ]
  node [
    id 556
    label "zasadzenie"
  ]
  node [
    id 557
    label "d&#243;&#322;"
  ]
  node [
    id 558
    label "dzieci&#281;ctwo"
  ]
  node [
    id 559
    label "background"
  ]
  node [
    id 560
    label "podstawowy"
  ]
  node [
    id 561
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 562
    label "strategia"
  ]
  node [
    id 563
    label "pomys&#322;"
  ]
  node [
    id 564
    label "&#347;ciana"
  ]
  node [
    id 565
    label "interfejs"
  ]
  node [
    id 566
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 567
    label "pole"
  ]
  node [
    id 568
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 569
    label "nacisk"
  ]
  node [
    id 570
    label "wymowa"
  ]
  node [
    id 571
    label "d&#378;wi&#281;k"
  ]
  node [
    id 572
    label "mata&#263;"
  ]
  node [
    id 573
    label "zakr&#281;ca&#263;"
  ]
  node [
    id 574
    label "wrench"
  ]
  node [
    id 575
    label "okr&#281;ca&#263;"
  ]
  node [
    id 576
    label "nak&#322;ada&#263;"
  ]
  node [
    id 577
    label "finish_up"
  ]
  node [
    id 578
    label "nagrywa&#263;"
  ]
  node [
    id 579
    label "spin"
  ]
  node [
    id 580
    label "cheat"
  ]
  node [
    id 581
    label "wzbudza&#263;"
  ]
  node [
    id 582
    label "rusza&#263;"
  ]
  node [
    id 583
    label "wzmaga&#263;"
  ]
  node [
    id 584
    label "gwiazda"
  ]
  node [
    id 585
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 586
    label "Arktur"
  ]
  node [
    id 587
    label "kszta&#322;t"
  ]
  node [
    id 588
    label "Gwiazda_Polarna"
  ]
  node [
    id 589
    label "agregatka"
  ]
  node [
    id 590
    label "gromada"
  ]
  node [
    id 591
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 592
    label "Nibiru"
  ]
  node [
    id 593
    label "konstelacja"
  ]
  node [
    id 594
    label "ornament"
  ]
  node [
    id 595
    label "delta_Scuti"
  ]
  node [
    id 596
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 597
    label "s&#322;awa"
  ]
  node [
    id 598
    label "promie&#324;"
  ]
  node [
    id 599
    label "star"
  ]
  node [
    id 600
    label "gwiazdosz"
  ]
  node [
    id 601
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 602
    label "asocjacja_gwiazd"
  ]
  node [
    id 603
    label "supergrupa"
  ]
  node [
    id 604
    label "supersamoch&#243;d"
  ]
  node [
    id 605
    label "samoch&#243;d"
  ]
  node [
    id 606
    label "Lamborghini"
  ]
  node [
    id 607
    label "pojazd_drogowy"
  ]
  node [
    id 608
    label "spryskiwacz"
  ]
  node [
    id 609
    label "baga&#380;nik"
  ]
  node [
    id 610
    label "silnik"
  ]
  node [
    id 611
    label "dachowanie"
  ]
  node [
    id 612
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 613
    label "pompa_wodna"
  ]
  node [
    id 614
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 615
    label "poduszka_powietrzna"
  ]
  node [
    id 616
    label "tempomat"
  ]
  node [
    id 617
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 618
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 619
    label "deska_rozdzielcza"
  ]
  node [
    id 620
    label "immobilizer"
  ]
  node [
    id 621
    label "t&#322;umik"
  ]
  node [
    id 622
    label "ABS"
  ]
  node [
    id 623
    label "kierownica"
  ]
  node [
    id 624
    label "bak"
  ]
  node [
    id 625
    label "dwu&#347;lad"
  ]
  node [
    id 626
    label "poci&#261;g_drogowy"
  ]
  node [
    id 627
    label "wycieraczka"
  ]
  node [
    id 628
    label "samoch&#243;d_sportowy"
  ]
  node [
    id 629
    label "mikrosekunda"
  ]
  node [
    id 630
    label "milisekunda"
  ]
  node [
    id 631
    label "jednostka"
  ]
  node [
    id 632
    label "tercja"
  ]
  node [
    id 633
    label "nanosekunda"
  ]
  node [
    id 634
    label "uk&#322;ad_SI"
  ]
  node [
    id 635
    label "przyswoi&#263;"
  ]
  node [
    id 636
    label "one"
  ]
  node [
    id 637
    label "ewoluowanie"
  ]
  node [
    id 638
    label "supremum"
  ]
  node [
    id 639
    label "skala"
  ]
  node [
    id 640
    label "przyswajanie"
  ]
  node [
    id 641
    label "wyewoluowanie"
  ]
  node [
    id 642
    label "reakcja"
  ]
  node [
    id 643
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 644
    label "przeliczy&#263;"
  ]
  node [
    id 645
    label "wyewoluowa&#263;"
  ]
  node [
    id 646
    label "ewoluowa&#263;"
  ]
  node [
    id 647
    label "matematyka"
  ]
  node [
    id 648
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 649
    label "rzut"
  ]
  node [
    id 650
    label "liczba_naturalna"
  ]
  node [
    id 651
    label "czynnik_biotyczny"
  ]
  node [
    id 652
    label "individual"
  ]
  node [
    id 653
    label "przyswaja&#263;"
  ]
  node [
    id 654
    label "przyswojenie"
  ]
  node [
    id 655
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 656
    label "starzenie_si&#281;"
  ]
  node [
    id 657
    label "przeliczanie"
  ]
  node [
    id 658
    label "funkcja"
  ]
  node [
    id 659
    label "przelicza&#263;"
  ]
  node [
    id 660
    label "infimum"
  ]
  node [
    id 661
    label "przeliczenie"
  ]
  node [
    id 662
    label "stopie&#324;_pisma"
  ]
  node [
    id 663
    label "godzina_kanoniczna"
  ]
  node [
    id 664
    label "hokej"
  ]
  node [
    id 665
    label "hokej_na_lodzie"
  ]
  node [
    id 666
    label "zamek"
  ]
  node [
    id 667
    label "interwa&#322;"
  ]
  node [
    id 668
    label "mecz"
  ]
  node [
    id 669
    label "zapis"
  ]
  node [
    id 670
    label "stopie&#324;"
  ]
  node [
    id 671
    label "design"
  ]
  node [
    id 672
    label "park"
  ]
  node [
    id 673
    label "umie&#347;ci&#263;"
  ]
  node [
    id 674
    label "zatrzyma&#263;"
  ]
  node [
    id 675
    label "komornik"
  ]
  node [
    id 676
    label "suspend"
  ]
  node [
    id 677
    label "zaczepi&#263;"
  ]
  node [
    id 678
    label "bury"
  ]
  node [
    id 679
    label "bankrupt"
  ]
  node [
    id 680
    label "zabra&#263;"
  ]
  node [
    id 681
    label "continue"
  ]
  node [
    id 682
    label "give"
  ]
  node [
    id 683
    label "spowodowa&#263;"
  ]
  node [
    id 684
    label "zamkn&#261;&#263;"
  ]
  node [
    id 685
    label "przechowa&#263;"
  ]
  node [
    id 686
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 687
    label "zaaresztowa&#263;"
  ]
  node [
    id 688
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 689
    label "przerwa&#263;"
  ]
  node [
    id 690
    label "unieruchomi&#263;"
  ]
  node [
    id 691
    label "anticipate"
  ]
  node [
    id 692
    label "set"
  ]
  node [
    id 693
    label "put"
  ]
  node [
    id 694
    label "uplasowa&#263;"
  ]
  node [
    id 695
    label "wpierniczy&#263;"
  ]
  node [
    id 696
    label "okre&#347;li&#263;"
  ]
  node [
    id 697
    label "zrobi&#263;"
  ]
  node [
    id 698
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 699
    label "zmieni&#263;"
  ]
  node [
    id 700
    label "umieszcza&#263;"
  ]
  node [
    id 701
    label "teren_zielony"
  ]
  node [
    id 702
    label "teren_przemys&#322;owy"
  ]
  node [
    id 703
    label "sprz&#281;t"
  ]
  node [
    id 704
    label "tabor"
  ]
  node [
    id 705
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 706
    label "ballpark"
  ]
  node [
    id 707
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 708
    label "rodzina"
  ]
  node [
    id 709
    label "substancja_mieszkaniowa"
  ]
  node [
    id 710
    label "siedziba"
  ]
  node [
    id 711
    label "dom_rodzinny"
  ]
  node [
    id 712
    label "budynek"
  ]
  node [
    id 713
    label "grupa"
  ]
  node [
    id 714
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 715
    label "stead"
  ]
  node [
    id 716
    label "garderoba"
  ]
  node [
    id 717
    label "wiecha"
  ]
  node [
    id 718
    label "fratria"
  ]
  node [
    id 719
    label "plemi&#281;"
  ]
  node [
    id 720
    label "family"
  ]
  node [
    id 721
    label "moiety"
  ]
  node [
    id 722
    label "str&#243;j"
  ]
  node [
    id 723
    label "odzie&#380;"
  ]
  node [
    id 724
    label "szatnia"
  ]
  node [
    id 725
    label "szafa_ubraniowa"
  ]
  node [
    id 726
    label "pomieszczenie"
  ]
  node [
    id 727
    label "powinowaci"
  ]
  node [
    id 728
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 729
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 730
    label "rodze&#324;stwo"
  ]
  node [
    id 731
    label "jednostka_systematyczna"
  ]
  node [
    id 732
    label "krewni"
  ]
  node [
    id 733
    label "Ossoli&#324;scy"
  ]
  node [
    id 734
    label "potomstwo"
  ]
  node [
    id 735
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 736
    label "theater"
  ]
  node [
    id 737
    label "Soplicowie"
  ]
  node [
    id 738
    label "kin"
  ]
  node [
    id 739
    label "rodzice"
  ]
  node [
    id 740
    label "ordynacja"
  ]
  node [
    id 741
    label "Ostrogscy"
  ]
  node [
    id 742
    label "bliscy"
  ]
  node [
    id 743
    label "przyjaciel_domu"
  ]
  node [
    id 744
    label "rz&#261;d"
  ]
  node [
    id 745
    label "Firlejowie"
  ]
  node [
    id 746
    label "Kossakowie"
  ]
  node [
    id 747
    label "Czartoryscy"
  ]
  node [
    id 748
    label "Sapiehowie"
  ]
  node [
    id 749
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 750
    label "mienie"
  ]
  node [
    id 751
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 752
    label "stan"
  ]
  node [
    id 753
    label "rzecz"
  ]
  node [
    id 754
    label "immoblizacja"
  ]
  node [
    id 755
    label "balkon"
  ]
  node [
    id 756
    label "budowla"
  ]
  node [
    id 757
    label "pod&#322;oga"
  ]
  node [
    id 758
    label "kondygnacja"
  ]
  node [
    id 759
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 760
    label "dach"
  ]
  node [
    id 761
    label "strop"
  ]
  node [
    id 762
    label "klatka_schodowa"
  ]
  node [
    id 763
    label "przedpro&#380;e"
  ]
  node [
    id 764
    label "Pentagon"
  ]
  node [
    id 765
    label "alkierz"
  ]
  node [
    id 766
    label "front"
  ]
  node [
    id 767
    label "&#321;ubianka"
  ]
  node [
    id 768
    label "miejsce_pracy"
  ]
  node [
    id 769
    label "dzia&#322;_personalny"
  ]
  node [
    id 770
    label "Kreml"
  ]
  node [
    id 771
    label "Bia&#322;y_Dom"
  ]
  node [
    id 772
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 773
    label "sadowisko"
  ]
  node [
    id 774
    label "odm&#322;adzanie"
  ]
  node [
    id 775
    label "liga"
  ]
  node [
    id 776
    label "Entuzjastki"
  ]
  node [
    id 777
    label "Terranie"
  ]
  node [
    id 778
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 779
    label "category"
  ]
  node [
    id 780
    label "oddzia&#322;"
  ]
  node [
    id 781
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 782
    label "cz&#261;steczka"
  ]
  node [
    id 783
    label "stage_set"
  ]
  node [
    id 784
    label "type"
  ]
  node [
    id 785
    label "specgrupa"
  ]
  node [
    id 786
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 787
    label "&#346;wietliki"
  ]
  node [
    id 788
    label "odm&#322;odzenie"
  ]
  node [
    id 789
    label "Eurogrupa"
  ]
  node [
    id 790
    label "odm&#322;adza&#263;"
  ]
  node [
    id 791
    label "formacja_geologiczna"
  ]
  node [
    id 792
    label "harcerze_starsi"
  ]
  node [
    id 793
    label "osoba_prawna"
  ]
  node [
    id 794
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 795
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 796
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 797
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 798
    label "biuro"
  ]
  node [
    id 799
    label "organizacja"
  ]
  node [
    id 800
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 801
    label "Fundusze_Unijne"
  ]
  node [
    id 802
    label "zamyka&#263;"
  ]
  node [
    id 803
    label "establishment"
  ]
  node [
    id 804
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 805
    label "urz&#261;d"
  ]
  node [
    id 806
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 807
    label "afiliowa&#263;"
  ]
  node [
    id 808
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 809
    label "standard"
  ]
  node [
    id 810
    label "zamykanie"
  ]
  node [
    id 811
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 812
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 813
    label "pos&#322;uchanie"
  ]
  node [
    id 814
    label "skumanie"
  ]
  node [
    id 815
    label "teoria"
  ]
  node [
    id 816
    label "clasp"
  ]
  node [
    id 817
    label "przem&#243;wienie"
  ]
  node [
    id 818
    label "perch"
  ]
  node [
    id 819
    label "kita"
  ]
  node [
    id 820
    label "wieniec"
  ]
  node [
    id 821
    label "wilk"
  ]
  node [
    id 822
    label "kwiatostan"
  ]
  node [
    id 823
    label "p&#281;k"
  ]
  node [
    id 824
    label "ogon"
  ]
  node [
    id 825
    label "wi&#261;zka"
  ]
  node [
    id 826
    label "Aventador"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 253
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 787
  ]
  edge [
    source 14
    target 788
  ]
  edge [
    source 14
    target 789
  ]
  edge [
    source 14
    target 790
  ]
  edge [
    source 14
    target 791
  ]
  edge [
    source 14
    target 792
  ]
  edge [
    source 14
    target 793
  ]
  edge [
    source 14
    target 794
  ]
  edge [
    source 14
    target 795
  ]
  edge [
    source 14
    target 796
  ]
  edge [
    source 14
    target 797
  ]
  edge [
    source 14
    target 798
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 800
  ]
  edge [
    source 14
    target 801
  ]
  edge [
    source 14
    target 802
  ]
  edge [
    source 14
    target 803
  ]
  edge [
    source 14
    target 804
  ]
  edge [
    source 14
    target 805
  ]
  edge [
    source 14
    target 806
  ]
  edge [
    source 14
    target 807
  ]
  edge [
    source 14
    target 808
  ]
  edge [
    source 14
    target 809
  ]
  edge [
    source 14
    target 810
  ]
  edge [
    source 14
    target 811
  ]
  edge [
    source 14
    target 812
  ]
  edge [
    source 14
    target 813
  ]
  edge [
    source 14
    target 814
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 815
  ]
  edge [
    source 14
    target 402
  ]
  edge [
    source 14
    target 816
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 14
    target 817
  ]
  edge [
    source 14
    target 818
  ]
  edge [
    source 14
    target 819
  ]
  edge [
    source 14
    target 820
  ]
  edge [
    source 14
    target 821
  ]
  edge [
    source 14
    target 822
  ]
  edge [
    source 14
    target 823
  ]
  edge [
    source 14
    target 824
  ]
  edge [
    source 14
    target 825
  ]
  edge [
    source 14
    target 826
  ]
]
