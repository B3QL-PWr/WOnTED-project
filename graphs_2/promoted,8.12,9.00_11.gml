graph [
  node [
    id 0
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "nazwa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "fruktanami"
    origin "text"
  ]
  node [
    id 3
    label "wywo&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pacjent"
    origin "text"
  ]
  node [
    id 5
    label "uskar&#380;a&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "nietolerancja"
    origin "text"
  ]
  node [
    id 8
    label "odwadnia&#263;"
  ]
  node [
    id 9
    label "wi&#261;zanie"
  ]
  node [
    id 10
    label "odwodni&#263;"
  ]
  node [
    id 11
    label "bratnia_dusza"
  ]
  node [
    id 12
    label "powi&#261;zanie"
  ]
  node [
    id 13
    label "zwi&#261;zanie"
  ]
  node [
    id 14
    label "konstytucja"
  ]
  node [
    id 15
    label "organizacja"
  ]
  node [
    id 16
    label "marriage"
  ]
  node [
    id 17
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 18
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 19
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 20
    label "zwi&#261;za&#263;"
  ]
  node [
    id 21
    label "odwadnianie"
  ]
  node [
    id 22
    label "odwodnienie"
  ]
  node [
    id 23
    label "marketing_afiliacyjny"
  ]
  node [
    id 24
    label "substancja_chemiczna"
  ]
  node [
    id 25
    label "koligacja"
  ]
  node [
    id 26
    label "bearing"
  ]
  node [
    id 27
    label "lokant"
  ]
  node [
    id 28
    label "azeotrop"
  ]
  node [
    id 29
    label "odprowadza&#263;"
  ]
  node [
    id 30
    label "drain"
  ]
  node [
    id 31
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 32
    label "cia&#322;o"
  ]
  node [
    id 33
    label "powodowa&#263;"
  ]
  node [
    id 34
    label "osusza&#263;"
  ]
  node [
    id 35
    label "odci&#261;ga&#263;"
  ]
  node [
    id 36
    label "odsuwa&#263;"
  ]
  node [
    id 37
    label "struktura"
  ]
  node [
    id 38
    label "zbi&#243;r"
  ]
  node [
    id 39
    label "akt"
  ]
  node [
    id 40
    label "cezar"
  ]
  node [
    id 41
    label "dokument"
  ]
  node [
    id 42
    label "budowa"
  ]
  node [
    id 43
    label "uchwa&#322;a"
  ]
  node [
    id 44
    label "numeracja"
  ]
  node [
    id 45
    label "odprowadzanie"
  ]
  node [
    id 46
    label "powodowanie"
  ]
  node [
    id 47
    label "odci&#261;ganie"
  ]
  node [
    id 48
    label "dehydratacja"
  ]
  node [
    id 49
    label "osuszanie"
  ]
  node [
    id 50
    label "proces_chemiczny"
  ]
  node [
    id 51
    label "odsuwanie"
  ]
  node [
    id 52
    label "odsun&#261;&#263;"
  ]
  node [
    id 53
    label "spowodowa&#263;"
  ]
  node [
    id 54
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 55
    label "odprowadzi&#263;"
  ]
  node [
    id 56
    label "osuszy&#263;"
  ]
  node [
    id 57
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 58
    label "dehydration"
  ]
  node [
    id 59
    label "oznaka"
  ]
  node [
    id 60
    label "osuszenie"
  ]
  node [
    id 61
    label "spowodowanie"
  ]
  node [
    id 62
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 63
    label "odprowadzenie"
  ]
  node [
    id 64
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 65
    label "odsuni&#281;cie"
  ]
  node [
    id 66
    label "narta"
  ]
  node [
    id 67
    label "przedmiot"
  ]
  node [
    id 68
    label "podwi&#261;zywanie"
  ]
  node [
    id 69
    label "dressing"
  ]
  node [
    id 70
    label "socket"
  ]
  node [
    id 71
    label "szermierka"
  ]
  node [
    id 72
    label "przywi&#261;zywanie"
  ]
  node [
    id 73
    label "pakowanie"
  ]
  node [
    id 74
    label "my&#347;lenie"
  ]
  node [
    id 75
    label "do&#322;&#261;czanie"
  ]
  node [
    id 76
    label "communication"
  ]
  node [
    id 77
    label "wytwarzanie"
  ]
  node [
    id 78
    label "cement"
  ]
  node [
    id 79
    label "ceg&#322;a"
  ]
  node [
    id 80
    label "combination"
  ]
  node [
    id 81
    label "zobowi&#261;zywanie"
  ]
  node [
    id 82
    label "szcz&#281;ka"
  ]
  node [
    id 83
    label "anga&#380;owanie"
  ]
  node [
    id 84
    label "wi&#261;za&#263;"
  ]
  node [
    id 85
    label "twardnienie"
  ]
  node [
    id 86
    label "tobo&#322;ek"
  ]
  node [
    id 87
    label "podwi&#261;zanie"
  ]
  node [
    id 88
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 89
    label "przywi&#261;zanie"
  ]
  node [
    id 90
    label "przymocowywanie"
  ]
  node [
    id 91
    label "scalanie"
  ]
  node [
    id 92
    label "mezomeria"
  ]
  node [
    id 93
    label "wi&#281;&#378;"
  ]
  node [
    id 94
    label "fusion"
  ]
  node [
    id 95
    label "kojarzenie_si&#281;"
  ]
  node [
    id 96
    label "&#322;&#261;czenie"
  ]
  node [
    id 97
    label "uchwyt"
  ]
  node [
    id 98
    label "rozmieszczenie"
  ]
  node [
    id 99
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 100
    label "zmiana"
  ]
  node [
    id 101
    label "element_konstrukcyjny"
  ]
  node [
    id 102
    label "obezw&#322;adnianie"
  ]
  node [
    id 103
    label "manewr"
  ]
  node [
    id 104
    label "miecz"
  ]
  node [
    id 105
    label "oddzia&#322;ywanie"
  ]
  node [
    id 106
    label "obwi&#261;zanie"
  ]
  node [
    id 107
    label "zawi&#261;zek"
  ]
  node [
    id 108
    label "obwi&#261;zywanie"
  ]
  node [
    id 109
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 110
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 111
    label "w&#281;ze&#322;"
  ]
  node [
    id 112
    label "consort"
  ]
  node [
    id 113
    label "opakowa&#263;"
  ]
  node [
    id 114
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 115
    label "relate"
  ]
  node [
    id 116
    label "form"
  ]
  node [
    id 117
    label "unify"
  ]
  node [
    id 118
    label "incorporate"
  ]
  node [
    id 119
    label "bind"
  ]
  node [
    id 120
    label "zawi&#261;za&#263;"
  ]
  node [
    id 121
    label "zaprawa"
  ]
  node [
    id 122
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 123
    label "powi&#261;za&#263;"
  ]
  node [
    id 124
    label "scali&#263;"
  ]
  node [
    id 125
    label "zatrzyma&#263;"
  ]
  node [
    id 126
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 127
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 128
    label "ograniczenie"
  ]
  node [
    id 129
    label "po&#322;&#261;czenie"
  ]
  node [
    id 130
    label "do&#322;&#261;czenie"
  ]
  node [
    id 131
    label "opakowanie"
  ]
  node [
    id 132
    label "attachment"
  ]
  node [
    id 133
    label "obezw&#322;adnienie"
  ]
  node [
    id 134
    label "zawi&#261;zanie"
  ]
  node [
    id 135
    label "tying"
  ]
  node [
    id 136
    label "st&#281;&#380;enie"
  ]
  node [
    id 137
    label "affiliation"
  ]
  node [
    id 138
    label "fastening"
  ]
  node [
    id 139
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 140
    label "z&#322;&#261;czenie"
  ]
  node [
    id 141
    label "zobowi&#261;zanie"
  ]
  node [
    id 142
    label "roztw&#243;r"
  ]
  node [
    id 143
    label "podmiot"
  ]
  node [
    id 144
    label "jednostka_organizacyjna"
  ]
  node [
    id 145
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 146
    label "TOPR"
  ]
  node [
    id 147
    label "endecki"
  ]
  node [
    id 148
    label "zesp&#243;&#322;"
  ]
  node [
    id 149
    label "od&#322;am"
  ]
  node [
    id 150
    label "przedstawicielstwo"
  ]
  node [
    id 151
    label "Cepelia"
  ]
  node [
    id 152
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 153
    label "ZBoWiD"
  ]
  node [
    id 154
    label "organization"
  ]
  node [
    id 155
    label "centrala"
  ]
  node [
    id 156
    label "GOPR"
  ]
  node [
    id 157
    label "ZOMO"
  ]
  node [
    id 158
    label "ZMP"
  ]
  node [
    id 159
    label "komitet_koordynacyjny"
  ]
  node [
    id 160
    label "przybud&#243;wka"
  ]
  node [
    id 161
    label "boj&#243;wka"
  ]
  node [
    id 162
    label "zrelatywizowa&#263;"
  ]
  node [
    id 163
    label "zrelatywizowanie"
  ]
  node [
    id 164
    label "mention"
  ]
  node [
    id 165
    label "pomy&#347;lenie"
  ]
  node [
    id 166
    label "relatywizowa&#263;"
  ]
  node [
    id 167
    label "relatywizowanie"
  ]
  node [
    id 168
    label "kontakt"
  ]
  node [
    id 169
    label "broadcast"
  ]
  node [
    id 170
    label "nada&#263;"
  ]
  node [
    id 171
    label "okre&#347;li&#263;"
  ]
  node [
    id 172
    label "zdecydowa&#263;"
  ]
  node [
    id 173
    label "zrobi&#263;"
  ]
  node [
    id 174
    label "situate"
  ]
  node [
    id 175
    label "nominate"
  ]
  node [
    id 176
    label "da&#263;"
  ]
  node [
    id 177
    label "za&#322;atwi&#263;"
  ]
  node [
    id 178
    label "give"
  ]
  node [
    id 179
    label "zarekomendowa&#263;"
  ]
  node [
    id 180
    label "przes&#322;a&#263;"
  ]
  node [
    id 181
    label "donie&#347;&#263;"
  ]
  node [
    id 182
    label "call"
  ]
  node [
    id 183
    label "dispose"
  ]
  node [
    id 184
    label "poleca&#263;"
  ]
  node [
    id 185
    label "oznajmia&#263;"
  ]
  node [
    id 186
    label "create"
  ]
  node [
    id 187
    label "wydala&#263;"
  ]
  node [
    id 188
    label "wzywa&#263;"
  ]
  node [
    id 189
    label "przetwarza&#263;"
  ]
  node [
    id 190
    label "invite"
  ]
  node [
    id 191
    label "cry"
  ]
  node [
    id 192
    label "address"
  ]
  node [
    id 193
    label "nakazywa&#263;"
  ]
  node [
    id 194
    label "donosi&#263;"
  ]
  node [
    id 195
    label "pobudza&#263;"
  ]
  node [
    id 196
    label "prosi&#263;"
  ]
  node [
    id 197
    label "order"
  ]
  node [
    id 198
    label "blurt_out"
  ]
  node [
    id 199
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 200
    label "usuwa&#263;"
  ]
  node [
    id 201
    label "convert"
  ]
  node [
    id 202
    label "wyzyskiwa&#263;"
  ]
  node [
    id 203
    label "opracowywa&#263;"
  ]
  node [
    id 204
    label "analizowa&#263;"
  ]
  node [
    id 205
    label "tworzy&#263;"
  ]
  node [
    id 206
    label "ordynowa&#263;"
  ]
  node [
    id 207
    label "doradza&#263;"
  ]
  node [
    id 208
    label "wydawa&#263;"
  ]
  node [
    id 209
    label "m&#243;wi&#263;"
  ]
  node [
    id 210
    label "control"
  ]
  node [
    id 211
    label "charge"
  ]
  node [
    id 212
    label "placard"
  ]
  node [
    id 213
    label "powierza&#263;"
  ]
  node [
    id 214
    label "zadawa&#263;"
  ]
  node [
    id 215
    label "inform"
  ]
  node [
    id 216
    label "informowa&#263;"
  ]
  node [
    id 217
    label "mie&#263;_miejsce"
  ]
  node [
    id 218
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 219
    label "motywowa&#263;"
  ]
  node [
    id 220
    label "act"
  ]
  node [
    id 221
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 222
    label "cz&#322;owiek"
  ]
  node [
    id 223
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 224
    label "klient"
  ]
  node [
    id 225
    label "przypadek"
  ]
  node [
    id 226
    label "piel&#281;gniarz"
  ]
  node [
    id 227
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 228
    label "od&#322;&#261;czanie"
  ]
  node [
    id 229
    label "chory"
  ]
  node [
    id 230
    label "od&#322;&#261;czenie"
  ]
  node [
    id 231
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 232
    label "szpitalnik"
  ]
  node [
    id 233
    label "ludzko&#347;&#263;"
  ]
  node [
    id 234
    label "asymilowanie"
  ]
  node [
    id 235
    label "wapniak"
  ]
  node [
    id 236
    label "asymilowa&#263;"
  ]
  node [
    id 237
    label "os&#322;abia&#263;"
  ]
  node [
    id 238
    label "posta&#263;"
  ]
  node [
    id 239
    label "hominid"
  ]
  node [
    id 240
    label "podw&#322;adny"
  ]
  node [
    id 241
    label "os&#322;abianie"
  ]
  node [
    id 242
    label "g&#322;owa"
  ]
  node [
    id 243
    label "figura"
  ]
  node [
    id 244
    label "portrecista"
  ]
  node [
    id 245
    label "dwun&#243;g"
  ]
  node [
    id 246
    label "profanum"
  ]
  node [
    id 247
    label "mikrokosmos"
  ]
  node [
    id 248
    label "nasada"
  ]
  node [
    id 249
    label "duch"
  ]
  node [
    id 250
    label "antropochoria"
  ]
  node [
    id 251
    label "osoba"
  ]
  node [
    id 252
    label "wz&#243;r"
  ]
  node [
    id 253
    label "senior"
  ]
  node [
    id 254
    label "Adam"
  ]
  node [
    id 255
    label "homo_sapiens"
  ]
  node [
    id 256
    label "polifag"
  ]
  node [
    id 257
    label "agent_rozliczeniowy"
  ]
  node [
    id 258
    label "komputer_cyfrowy"
  ]
  node [
    id 259
    label "us&#322;ugobiorca"
  ]
  node [
    id 260
    label "Rzymianin"
  ]
  node [
    id 261
    label "szlachcic"
  ]
  node [
    id 262
    label "obywatel"
  ]
  node [
    id 263
    label "klientela"
  ]
  node [
    id 264
    label "bratek"
  ]
  node [
    id 265
    label "program"
  ]
  node [
    id 266
    label "krzy&#380;owiec"
  ]
  node [
    id 267
    label "tytu&#322;"
  ]
  node [
    id 268
    label "szpitalnicy"
  ]
  node [
    id 269
    label "kawaler"
  ]
  node [
    id 270
    label "zakonnik"
  ]
  node [
    id 271
    label "wydarzenie"
  ]
  node [
    id 272
    label "happening"
  ]
  node [
    id 273
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 274
    label "schorzenie"
  ]
  node [
    id 275
    label "przyk&#322;ad"
  ]
  node [
    id 276
    label "kategoria_gramatyczna"
  ]
  node [
    id 277
    label "przeznaczenie"
  ]
  node [
    id 278
    label "cutoff"
  ]
  node [
    id 279
    label "od&#322;&#261;czony"
  ]
  node [
    id 280
    label "odbicie"
  ]
  node [
    id 281
    label "przerwanie"
  ]
  node [
    id 282
    label "odci&#281;cie"
  ]
  node [
    id 283
    label "release"
  ]
  node [
    id 284
    label "ablation"
  ]
  node [
    id 285
    label "oddzielenie"
  ]
  node [
    id 286
    label "severance"
  ]
  node [
    id 287
    label "odcinanie"
  ]
  node [
    id 288
    label "oddzielanie"
  ]
  node [
    id 289
    label "odbijanie"
  ]
  node [
    id 290
    label "przerywanie"
  ]
  node [
    id 291
    label "rupture"
  ]
  node [
    id 292
    label "dissociation"
  ]
  node [
    id 293
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 294
    label "take"
  ]
  node [
    id 295
    label "przerywa&#263;"
  ]
  node [
    id 296
    label "odcina&#263;"
  ]
  node [
    id 297
    label "abstract"
  ]
  node [
    id 298
    label "oddziela&#263;"
  ]
  node [
    id 299
    label "challenge"
  ]
  node [
    id 300
    label "publish"
  ]
  node [
    id 301
    label "separate"
  ]
  node [
    id 302
    label "oddzieli&#263;"
  ]
  node [
    id 303
    label "odci&#261;&#263;"
  ]
  node [
    id 304
    label "amputate"
  ]
  node [
    id 305
    label "przerwa&#263;"
  ]
  node [
    id 306
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 307
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 308
    label "choro"
  ]
  node [
    id 309
    label "nieprzytomny"
  ]
  node [
    id 310
    label "skandaliczny"
  ]
  node [
    id 311
    label "chor&#243;bka"
  ]
  node [
    id 312
    label "nienormalny"
  ]
  node [
    id 313
    label "niezdrowy"
  ]
  node [
    id 314
    label "w&#347;ciek&#322;y"
  ]
  node [
    id 315
    label "niezrozumia&#322;y"
  ]
  node [
    id 316
    label "chorowanie"
  ]
  node [
    id 317
    label "le&#380;alnia"
  ]
  node [
    id 318
    label "psychiczny"
  ]
  node [
    id 319
    label "zachorowanie"
  ]
  node [
    id 320
    label "rozchorowywanie_si&#281;"
  ]
  node [
    id 321
    label "uczulanie"
  ]
  node [
    id 322
    label "alergen"
  ]
  node [
    id 323
    label "odczulenie"
  ]
  node [
    id 324
    label "rumie&#324;_wielopostaciowy"
  ]
  node [
    id 325
    label "odczula&#263;"
  ]
  node [
    id 326
    label "odczuli&#263;"
  ]
  node [
    id 327
    label "uczulenie"
  ]
  node [
    id 328
    label "choroba_somatyczna"
  ]
  node [
    id 329
    label "uczuli&#263;"
  ]
  node [
    id 330
    label "sensybilizacja"
  ]
  node [
    id 331
    label "intolerance"
  ]
  node [
    id 332
    label "krzywda"
  ]
  node [
    id 333
    label "uczula&#263;"
  ]
  node [
    id 334
    label "odczulanie"
  ]
  node [
    id 335
    label "kaszel"
  ]
  node [
    id 336
    label "reakcja"
  ]
  node [
    id 337
    label "react"
  ]
  node [
    id 338
    label "zachowanie"
  ]
  node [
    id 339
    label "reaction"
  ]
  node [
    id 340
    label "organizm"
  ]
  node [
    id 341
    label "rozmowa"
  ]
  node [
    id 342
    label "response"
  ]
  node [
    id 343
    label "rezultat"
  ]
  node [
    id 344
    label "respondent"
  ]
  node [
    id 345
    label "ognisko"
  ]
  node [
    id 346
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 347
    label "powalenie"
  ]
  node [
    id 348
    label "odezwanie_si&#281;"
  ]
  node [
    id 349
    label "atakowanie"
  ]
  node [
    id 350
    label "grupa_ryzyka"
  ]
  node [
    id 351
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 352
    label "nabawienie_si&#281;"
  ]
  node [
    id 353
    label "inkubacja"
  ]
  node [
    id 354
    label "kryzys"
  ]
  node [
    id 355
    label "powali&#263;"
  ]
  node [
    id 356
    label "remisja"
  ]
  node [
    id 357
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 358
    label "zajmowa&#263;"
  ]
  node [
    id 359
    label "zaburzenie"
  ]
  node [
    id 360
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 361
    label "badanie_histopatologiczne"
  ]
  node [
    id 362
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 363
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 364
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 365
    label "odzywanie_si&#281;"
  ]
  node [
    id 366
    label "diagnoza"
  ]
  node [
    id 367
    label "atakowa&#263;"
  ]
  node [
    id 368
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 369
    label "nabawianie_si&#281;"
  ]
  node [
    id 370
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 371
    label "zajmowanie"
  ]
  node [
    id 372
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 373
    label "obelga"
  ]
  node [
    id 374
    label "bias"
  ]
  node [
    id 375
    label "strata"
  ]
  node [
    id 376
    label "alergia"
  ]
  node [
    id 377
    label "wzbudza&#263;"
  ]
  node [
    id 378
    label "szkodzi&#263;"
  ]
  node [
    id 379
    label "uwra&#380;liwia&#263;"
  ]
  node [
    id 380
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 381
    label "usun&#261;&#263;"
  ]
  node [
    id 382
    label "wyleczy&#263;"
  ]
  node [
    id 383
    label "zmniejszy&#263;"
  ]
  node [
    id 384
    label "uwra&#380;liwi&#263;"
  ]
  node [
    id 385
    label "wywo&#322;a&#263;"
  ]
  node [
    id 386
    label "wzbudzi&#263;"
  ]
  node [
    id 387
    label "zmniejszanie"
  ]
  node [
    id 388
    label "usuwanie"
  ]
  node [
    id 389
    label "desensitization"
  ]
  node [
    id 390
    label "terapia"
  ]
  node [
    id 391
    label "wzbudzenie"
  ]
  node [
    id 392
    label "sensitizing"
  ]
  node [
    id 393
    label "zwi&#281;kszenie"
  ]
  node [
    id 394
    label "czu&#322;y"
  ]
  node [
    id 395
    label "wywo&#322;anie"
  ]
  node [
    id 396
    label "leczy&#263;"
  ]
  node [
    id 397
    label "zmniejsza&#263;"
  ]
  node [
    id 398
    label "wzbudzanie"
  ]
  node [
    id 399
    label "szkodzenie"
  ]
  node [
    id 400
    label "substancja"
  ]
  node [
    id 401
    label "allergen"
  ]
  node [
    id 402
    label "usuni&#281;cie"
  ]
  node [
    id 403
    label "zmniejszenie"
  ]
  node [
    id 404
    label "wyleczenie"
  ]
  node [
    id 405
    label "ekspulsja"
  ]
  node [
    id 406
    label "atak"
  ]
  node [
    id 407
    label "grypa"
  ]
  node [
    id 408
    label "czynno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
]
