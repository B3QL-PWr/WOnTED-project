graph [
  node [
    id 0
    label "unteregg"
    origin "text"
  ]
  node [
    id 1
    label "Marlene"
  ]
  node [
    id 2
    label "Preisinger"
  ]
  node [
    id 3
    label "Johann"
  ]
  node [
    id 4
    label "Nepomuk"
  ]
  node [
    id 5
    label "Holzhey"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
]
