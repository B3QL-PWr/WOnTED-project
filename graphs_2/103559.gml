graph [
  node [
    id 0
    label "anna"
    origin "text"
  ]
  node [
    id 1
    label "mat"
    origin "text"
  ]
  node [
    id 2
    label "nawet"
    origin "text"
  ]
  node [
    id 3
    label "czas"
    origin "text"
  ]
  node [
    id 4
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 7
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 8
    label "podoficer_marynarki"
  ]
  node [
    id 9
    label "ruch"
  ]
  node [
    id 10
    label "szachy"
  ]
  node [
    id 11
    label "szach"
  ]
  node [
    id 12
    label "move"
  ]
  node [
    id 13
    label "zmiana"
  ]
  node [
    id 14
    label "model"
  ]
  node [
    id 15
    label "aktywno&#347;&#263;"
  ]
  node [
    id 16
    label "utrzymywanie"
  ]
  node [
    id 17
    label "utrzymywa&#263;"
  ]
  node [
    id 18
    label "taktyka"
  ]
  node [
    id 19
    label "d&#322;ugi"
  ]
  node [
    id 20
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 21
    label "natural_process"
  ]
  node [
    id 22
    label "kanciasty"
  ]
  node [
    id 23
    label "utrzyma&#263;"
  ]
  node [
    id 24
    label "myk"
  ]
  node [
    id 25
    label "manewr"
  ]
  node [
    id 26
    label "utrzymanie"
  ]
  node [
    id 27
    label "wydarzenie"
  ]
  node [
    id 28
    label "tumult"
  ]
  node [
    id 29
    label "stopek"
  ]
  node [
    id 30
    label "movement"
  ]
  node [
    id 31
    label "strumie&#324;"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "komunikacja"
  ]
  node [
    id 34
    label "lokomocja"
  ]
  node [
    id 35
    label "drift"
  ]
  node [
    id 36
    label "commercial_enterprise"
  ]
  node [
    id 37
    label "zjawisko"
  ]
  node [
    id 38
    label "apraksja"
  ]
  node [
    id 39
    label "proces"
  ]
  node [
    id 40
    label "poruszenie"
  ]
  node [
    id 41
    label "mechanika"
  ]
  node [
    id 42
    label "travel"
  ]
  node [
    id 43
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 44
    label "dyssypacja_energii"
  ]
  node [
    id 45
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 46
    label "kr&#243;tki"
  ]
  node [
    id 47
    label "Persja"
  ]
  node [
    id 48
    label "pozycja"
  ]
  node [
    id 49
    label "monarcha"
  ]
  node [
    id 50
    label "Afganistan"
  ]
  node [
    id 51
    label "arrest"
  ]
  node [
    id 52
    label "linia_przemiany"
  ]
  node [
    id 53
    label "przes&#322;ona"
  ]
  node [
    id 54
    label "niedoczas"
  ]
  node [
    id 55
    label "zegar_szachowy"
  ]
  node [
    id 56
    label "szachownica"
  ]
  node [
    id 57
    label "dual"
  ]
  node [
    id 58
    label "bicie_w_przelocie"
  ]
  node [
    id 59
    label "p&#243;&#322;ruch"
  ]
  node [
    id 60
    label "tempo"
  ]
  node [
    id 61
    label "sport_umys&#322;owy"
  ]
  node [
    id 62
    label "promocja"
  ]
  node [
    id 63
    label "gra_planszowa"
  ]
  node [
    id 64
    label "roszada"
  ]
  node [
    id 65
    label "chronometria"
  ]
  node [
    id 66
    label "odczyt"
  ]
  node [
    id 67
    label "laba"
  ]
  node [
    id 68
    label "czasoprzestrze&#324;"
  ]
  node [
    id 69
    label "time_period"
  ]
  node [
    id 70
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 71
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 72
    label "Zeitgeist"
  ]
  node [
    id 73
    label "pochodzenie"
  ]
  node [
    id 74
    label "przep&#322;ywanie"
  ]
  node [
    id 75
    label "schy&#322;ek"
  ]
  node [
    id 76
    label "czwarty_wymiar"
  ]
  node [
    id 77
    label "kategoria_gramatyczna"
  ]
  node [
    id 78
    label "poprzedzi&#263;"
  ]
  node [
    id 79
    label "pogoda"
  ]
  node [
    id 80
    label "czasokres"
  ]
  node [
    id 81
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 82
    label "poprzedzenie"
  ]
  node [
    id 83
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 84
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 85
    label "dzieje"
  ]
  node [
    id 86
    label "zegar"
  ]
  node [
    id 87
    label "koniugacja"
  ]
  node [
    id 88
    label "trawi&#263;"
  ]
  node [
    id 89
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 90
    label "poprzedza&#263;"
  ]
  node [
    id 91
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 92
    label "trawienie"
  ]
  node [
    id 93
    label "chwila"
  ]
  node [
    id 94
    label "rachuba_czasu"
  ]
  node [
    id 95
    label "poprzedzanie"
  ]
  node [
    id 96
    label "okres_czasu"
  ]
  node [
    id 97
    label "period"
  ]
  node [
    id 98
    label "odwlekanie_si&#281;"
  ]
  node [
    id 99
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 100
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 101
    label "pochodzi&#263;"
  ]
  node [
    id 102
    label "time"
  ]
  node [
    id 103
    label "blok"
  ]
  node [
    id 104
    label "reading"
  ]
  node [
    id 105
    label "handout"
  ]
  node [
    id 106
    label "podawanie"
  ]
  node [
    id 107
    label "wyk&#322;ad"
  ]
  node [
    id 108
    label "lecture"
  ]
  node [
    id 109
    label "pomiar"
  ]
  node [
    id 110
    label "meteorology"
  ]
  node [
    id 111
    label "warunki"
  ]
  node [
    id 112
    label "weather"
  ]
  node [
    id 113
    label "pok&#243;j"
  ]
  node [
    id 114
    label "atak"
  ]
  node [
    id 115
    label "prognoza_meteorologiczna"
  ]
  node [
    id 116
    label "potrzyma&#263;"
  ]
  node [
    id 117
    label "program"
  ]
  node [
    id 118
    label "czas_wolny"
  ]
  node [
    id 119
    label "metrologia"
  ]
  node [
    id 120
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 121
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 122
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 123
    label "czasomierz"
  ]
  node [
    id 124
    label "tyka&#263;"
  ]
  node [
    id 125
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 126
    label "tykn&#261;&#263;"
  ]
  node [
    id 127
    label "nabicie"
  ]
  node [
    id 128
    label "bicie"
  ]
  node [
    id 129
    label "kotwica"
  ]
  node [
    id 130
    label "godzinnik"
  ]
  node [
    id 131
    label "werk"
  ]
  node [
    id 132
    label "urz&#261;dzenie"
  ]
  node [
    id 133
    label "wahad&#322;o"
  ]
  node [
    id 134
    label "kurant"
  ]
  node [
    id 135
    label "cyferblat"
  ]
  node [
    id 136
    label "liczba"
  ]
  node [
    id 137
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 138
    label "czasownik"
  ]
  node [
    id 139
    label "osoba"
  ]
  node [
    id 140
    label "tryb"
  ]
  node [
    id 141
    label "coupling"
  ]
  node [
    id 142
    label "fleksja"
  ]
  node [
    id 143
    label "orz&#281;sek"
  ]
  node [
    id 144
    label "lutowa&#263;"
  ]
  node [
    id 145
    label "metal"
  ]
  node [
    id 146
    label "przetrawia&#263;"
  ]
  node [
    id 147
    label "poch&#322;ania&#263;"
  ]
  node [
    id 148
    label "marnowa&#263;"
  ]
  node [
    id 149
    label "digest"
  ]
  node [
    id 150
    label "usuwa&#263;"
  ]
  node [
    id 151
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 152
    label "sp&#281;dza&#263;"
  ]
  node [
    id 153
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 154
    label "marnowanie"
  ]
  node [
    id 155
    label "unicestwianie"
  ]
  node [
    id 156
    label "sp&#281;dzanie"
  ]
  node [
    id 157
    label "digestion"
  ]
  node [
    id 158
    label "perystaltyka"
  ]
  node [
    id 159
    label "proces_fizjologiczny"
  ]
  node [
    id 160
    label "rozk&#322;adanie"
  ]
  node [
    id 161
    label "przetrawianie"
  ]
  node [
    id 162
    label "contemplation"
  ]
  node [
    id 163
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 164
    label "background"
  ]
  node [
    id 165
    label "str&#243;j"
  ]
  node [
    id 166
    label "wynikanie"
  ]
  node [
    id 167
    label "origin"
  ]
  node [
    id 168
    label "zaczynanie_si&#281;"
  ]
  node [
    id 169
    label "beginning"
  ]
  node [
    id 170
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 171
    label "geneza"
  ]
  node [
    id 172
    label "cross"
  ]
  node [
    id 173
    label "swimming"
  ]
  node [
    id 174
    label "min&#261;&#263;"
  ]
  node [
    id 175
    label "przeby&#263;"
  ]
  node [
    id 176
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 177
    label "zago&#347;ci&#263;"
  ]
  node [
    id 178
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 179
    label "proceed"
  ]
  node [
    id 180
    label "pour"
  ]
  node [
    id 181
    label "mija&#263;"
  ]
  node [
    id 182
    label "sail"
  ]
  node [
    id 183
    label "przebywa&#263;"
  ]
  node [
    id 184
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 185
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 186
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 187
    label "carry"
  ]
  node [
    id 188
    label "go&#347;ci&#263;"
  ]
  node [
    id 189
    label "przebycie"
  ]
  node [
    id 190
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 191
    label "mini&#281;cie"
  ]
  node [
    id 192
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 193
    label "zaistnienie"
  ]
  node [
    id 194
    label "doznanie"
  ]
  node [
    id 195
    label "cruise"
  ]
  node [
    id 196
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 197
    label "zjawianie_si&#281;"
  ]
  node [
    id 198
    label "mijanie"
  ]
  node [
    id 199
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 200
    label "przebywanie"
  ]
  node [
    id 201
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 202
    label "flux"
  ]
  node [
    id 203
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 204
    label "zaznawanie"
  ]
  node [
    id 205
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 206
    label "overwhelm"
  ]
  node [
    id 207
    label "zrobi&#263;"
  ]
  node [
    id 208
    label "opatrzy&#263;"
  ]
  node [
    id 209
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 210
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 211
    label "opatrywanie"
  ]
  node [
    id 212
    label "zanikni&#281;cie"
  ]
  node [
    id 213
    label "departure"
  ]
  node [
    id 214
    label "odej&#347;cie"
  ]
  node [
    id 215
    label "opuszczenie"
  ]
  node [
    id 216
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 217
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 218
    label "ciecz"
  ]
  node [
    id 219
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 220
    label "oddalenie_si&#281;"
  ]
  node [
    id 221
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 222
    label "poby&#263;"
  ]
  node [
    id 223
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 224
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 225
    label "bolt"
  ]
  node [
    id 226
    label "uda&#263;_si&#281;"
  ]
  node [
    id 227
    label "date"
  ]
  node [
    id 228
    label "fall"
  ]
  node [
    id 229
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 230
    label "spowodowa&#263;"
  ]
  node [
    id 231
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 232
    label "wynika&#263;"
  ]
  node [
    id 233
    label "zdarzenie_si&#281;"
  ]
  node [
    id 234
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 235
    label "progress"
  ]
  node [
    id 236
    label "opatrzenie"
  ]
  node [
    id 237
    label "opatrywa&#263;"
  ]
  node [
    id 238
    label "charakter"
  ]
  node [
    id 239
    label "epoka"
  ]
  node [
    id 240
    label "ciota"
  ]
  node [
    id 241
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 242
    label "flow"
  ]
  node [
    id 243
    label "choroba_przyrodzona"
  ]
  node [
    id 244
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 245
    label "kres"
  ]
  node [
    id 246
    label "przestrze&#324;"
  ]
  node [
    id 247
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 248
    label "go_steady"
  ]
  node [
    id 249
    label "visualize"
  ]
  node [
    id 250
    label "pozna&#263;"
  ]
  node [
    id 251
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 252
    label "insert"
  ]
  node [
    id 253
    label "znale&#378;&#263;"
  ]
  node [
    id 254
    label "befall"
  ]
  node [
    id 255
    label "act"
  ]
  node [
    id 256
    label "wymy&#347;li&#263;"
  ]
  node [
    id 257
    label "znaj&#347;&#263;"
  ]
  node [
    id 258
    label "pozyska&#263;"
  ]
  node [
    id 259
    label "odzyska&#263;"
  ]
  node [
    id 260
    label "oceni&#263;"
  ]
  node [
    id 261
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 262
    label "dozna&#263;"
  ]
  node [
    id 263
    label "wykry&#263;"
  ]
  node [
    id 264
    label "devise"
  ]
  node [
    id 265
    label "invent"
  ]
  node [
    id 266
    label "przyswoi&#263;"
  ]
  node [
    id 267
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 268
    label "teach"
  ]
  node [
    id 269
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 270
    label "zrozumie&#263;"
  ]
  node [
    id 271
    label "experience"
  ]
  node [
    id 272
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 273
    label "topographic_point"
  ]
  node [
    id 274
    label "feel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
]
