graph [
  node [
    id 0
    label "roewe"
    origin "text"
  ]
  node [
    id 1
    label "MG"
  ]
  node [
    id 2
    label "rover"
  ]
  node [
    id 3
    label "Shanghai"
  ]
  node [
    id 4
    label "Automotive"
  ]
  node [
    id 5
    label "Industry"
  ]
  node [
    id 6
    label "Corporation"
  ]
  node [
    id 7
    label "Beijing"
  ]
  node [
    id 8
    label "auto"
  ]
  node [
    id 9
    label "show"
  ]
  node [
    id 10
    label "2006"
  ]
  node [
    id 11
    label "Roewe"
  ]
  node [
    id 12
    label "750"
  ]
  node [
    id 13
    label "75"
  ]
  node [
    id 14
    label "lando"
  ]
  node [
    id 15
    label "tata"
  ]
  node [
    id 16
    label "Motors"
  ]
  node [
    id 17
    label "550"
  ]
  node [
    id 18
    label "motor"
  ]
  node [
    id 19
    label "Lexusa"
  ]
  node [
    id 20
    label "IS"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 19
    target 20
  ]
]
