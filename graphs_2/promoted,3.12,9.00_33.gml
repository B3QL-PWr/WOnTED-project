graph [
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "badanie"
    origin "text"
  ]
  node [
    id 2
    label "wskazywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "starsza"
    origin "text"
  ]
  node [
    id 4
    label "osoba"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "k&#322;ama&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 8
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 9
    label "minuta"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 11
    label "przyj&#261;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "k&#322;amstwo"
    origin "text"
  ]
  node [
    id 13
    label "prawda"
    origin "text"
  ]
  node [
    id 14
    label "kolejny"
  ]
  node [
    id 15
    label "nowo"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "bie&#380;&#261;cy"
  ]
  node [
    id 18
    label "drugi"
  ]
  node [
    id 19
    label "narybek"
  ]
  node [
    id 20
    label "obcy"
  ]
  node [
    id 21
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 22
    label "nowotny"
  ]
  node [
    id 23
    label "nadprzyrodzony"
  ]
  node [
    id 24
    label "nieznany"
  ]
  node [
    id 25
    label "pozaludzki"
  ]
  node [
    id 26
    label "obco"
  ]
  node [
    id 27
    label "tameczny"
  ]
  node [
    id 28
    label "nieznajomo"
  ]
  node [
    id 29
    label "inny"
  ]
  node [
    id 30
    label "cudzy"
  ]
  node [
    id 31
    label "istota_&#380;ywa"
  ]
  node [
    id 32
    label "zaziemsko"
  ]
  node [
    id 33
    label "jednoczesny"
  ]
  node [
    id 34
    label "unowocze&#347;nianie"
  ]
  node [
    id 35
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 36
    label "tera&#378;niejszy"
  ]
  node [
    id 37
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 38
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 39
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 40
    label "nast&#281;pnie"
  ]
  node [
    id 41
    label "nastopny"
  ]
  node [
    id 42
    label "kolejno"
  ]
  node [
    id 43
    label "kt&#243;ry&#347;"
  ]
  node [
    id 44
    label "sw&#243;j"
  ]
  node [
    id 45
    label "przeciwny"
  ]
  node [
    id 46
    label "wt&#243;ry"
  ]
  node [
    id 47
    label "dzie&#324;"
  ]
  node [
    id 48
    label "odwrotnie"
  ]
  node [
    id 49
    label "podobny"
  ]
  node [
    id 50
    label "bie&#380;&#261;co"
  ]
  node [
    id 51
    label "ci&#261;g&#322;y"
  ]
  node [
    id 52
    label "aktualny"
  ]
  node [
    id 53
    label "ludzko&#347;&#263;"
  ]
  node [
    id 54
    label "asymilowanie"
  ]
  node [
    id 55
    label "wapniak"
  ]
  node [
    id 56
    label "asymilowa&#263;"
  ]
  node [
    id 57
    label "os&#322;abia&#263;"
  ]
  node [
    id 58
    label "posta&#263;"
  ]
  node [
    id 59
    label "hominid"
  ]
  node [
    id 60
    label "podw&#322;adny"
  ]
  node [
    id 61
    label "os&#322;abianie"
  ]
  node [
    id 62
    label "g&#322;owa"
  ]
  node [
    id 63
    label "figura"
  ]
  node [
    id 64
    label "portrecista"
  ]
  node [
    id 65
    label "dwun&#243;g"
  ]
  node [
    id 66
    label "profanum"
  ]
  node [
    id 67
    label "mikrokosmos"
  ]
  node [
    id 68
    label "nasada"
  ]
  node [
    id 69
    label "duch"
  ]
  node [
    id 70
    label "antropochoria"
  ]
  node [
    id 71
    label "wz&#243;r"
  ]
  node [
    id 72
    label "senior"
  ]
  node [
    id 73
    label "oddzia&#322;ywanie"
  ]
  node [
    id 74
    label "Adam"
  ]
  node [
    id 75
    label "homo_sapiens"
  ]
  node [
    id 76
    label "polifag"
  ]
  node [
    id 77
    label "dopiero_co"
  ]
  node [
    id 78
    label "formacja"
  ]
  node [
    id 79
    label "potomstwo"
  ]
  node [
    id 80
    label "obserwowanie"
  ]
  node [
    id 81
    label "zrecenzowanie"
  ]
  node [
    id 82
    label "kontrola"
  ]
  node [
    id 83
    label "analysis"
  ]
  node [
    id 84
    label "rektalny"
  ]
  node [
    id 85
    label "ustalenie"
  ]
  node [
    id 86
    label "macanie"
  ]
  node [
    id 87
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 88
    label "usi&#322;owanie"
  ]
  node [
    id 89
    label "udowadnianie"
  ]
  node [
    id 90
    label "praca"
  ]
  node [
    id 91
    label "bia&#322;a_niedziela"
  ]
  node [
    id 92
    label "diagnostyka"
  ]
  node [
    id 93
    label "dociekanie"
  ]
  node [
    id 94
    label "rezultat"
  ]
  node [
    id 95
    label "sprawdzanie"
  ]
  node [
    id 96
    label "penetrowanie"
  ]
  node [
    id 97
    label "czynno&#347;&#263;"
  ]
  node [
    id 98
    label "krytykowanie"
  ]
  node [
    id 99
    label "omawianie"
  ]
  node [
    id 100
    label "ustalanie"
  ]
  node [
    id 101
    label "rozpatrywanie"
  ]
  node [
    id 102
    label "investigation"
  ]
  node [
    id 103
    label "wziernikowanie"
  ]
  node [
    id 104
    label "examination"
  ]
  node [
    id 105
    label "czepianie_si&#281;"
  ]
  node [
    id 106
    label "opiniowanie"
  ]
  node [
    id 107
    label "ocenianie"
  ]
  node [
    id 108
    label "discussion"
  ]
  node [
    id 109
    label "dyskutowanie"
  ]
  node [
    id 110
    label "temat"
  ]
  node [
    id 111
    label "zaopiniowanie"
  ]
  node [
    id 112
    label "colony"
  ]
  node [
    id 113
    label "powodowanie"
  ]
  node [
    id 114
    label "robienie"
  ]
  node [
    id 115
    label "colonization"
  ]
  node [
    id 116
    label "decydowanie"
  ]
  node [
    id 117
    label "umacnianie"
  ]
  node [
    id 118
    label "liquidation"
  ]
  node [
    id 119
    label "decyzja"
  ]
  node [
    id 120
    label "umocnienie"
  ]
  node [
    id 121
    label "appointment"
  ]
  node [
    id 122
    label "spowodowanie"
  ]
  node [
    id 123
    label "localization"
  ]
  node [
    id 124
    label "informacja"
  ]
  node [
    id 125
    label "zdecydowanie"
  ]
  node [
    id 126
    label "zrobienie"
  ]
  node [
    id 127
    label "przeszukiwanie"
  ]
  node [
    id 128
    label "docieranie"
  ]
  node [
    id 129
    label "penetration"
  ]
  node [
    id 130
    label "przemy&#347;liwanie"
  ]
  node [
    id 131
    label "dzia&#322;anie"
  ]
  node [
    id 132
    label "typ"
  ]
  node [
    id 133
    label "event"
  ]
  node [
    id 134
    label "przyczyna"
  ]
  node [
    id 135
    label "activity"
  ]
  node [
    id 136
    label "bezproblemowy"
  ]
  node [
    id 137
    label "wydarzenie"
  ]
  node [
    id 138
    label "legalizacja_ponowna"
  ]
  node [
    id 139
    label "instytucja"
  ]
  node [
    id 140
    label "w&#322;adza"
  ]
  node [
    id 141
    label "perlustracja"
  ]
  node [
    id 142
    label "legalizacja_pierwotna"
  ]
  node [
    id 143
    label "podejmowanie"
  ]
  node [
    id 144
    label "effort"
  ]
  node [
    id 145
    label "staranie_si&#281;"
  ]
  node [
    id 146
    label "essay"
  ]
  node [
    id 147
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 148
    label "redagowanie"
  ]
  node [
    id 149
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 150
    label "przymierzanie"
  ]
  node [
    id 151
    label "przymierzenie"
  ]
  node [
    id 152
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 153
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 154
    label "najem"
  ]
  node [
    id 155
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 156
    label "zak&#322;ad"
  ]
  node [
    id 157
    label "stosunek_pracy"
  ]
  node [
    id 158
    label "benedykty&#324;ski"
  ]
  node [
    id 159
    label "poda&#380;_pracy"
  ]
  node [
    id 160
    label "pracowanie"
  ]
  node [
    id 161
    label "tyrka"
  ]
  node [
    id 162
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 163
    label "wytw&#243;r"
  ]
  node [
    id 164
    label "miejsce"
  ]
  node [
    id 165
    label "zaw&#243;d"
  ]
  node [
    id 166
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 167
    label "tynkarski"
  ]
  node [
    id 168
    label "pracowa&#263;"
  ]
  node [
    id 169
    label "zmiana"
  ]
  node [
    id 170
    label "czynnik_produkcji"
  ]
  node [
    id 171
    label "zobowi&#261;zanie"
  ]
  node [
    id 172
    label "kierownictwo"
  ]
  node [
    id 173
    label "siedziba"
  ]
  node [
    id 174
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 175
    label "analizowanie"
  ]
  node [
    id 176
    label "uzasadnianie"
  ]
  node [
    id 177
    label "presentation"
  ]
  node [
    id 178
    label "pokazywanie"
  ]
  node [
    id 179
    label "endoscopy"
  ]
  node [
    id 180
    label "rozmy&#347;lanie"
  ]
  node [
    id 181
    label "quest"
  ]
  node [
    id 182
    label "dop&#322;ywanie"
  ]
  node [
    id 183
    label "examen"
  ]
  node [
    id 184
    label "diagnosis"
  ]
  node [
    id 185
    label "medycyna"
  ]
  node [
    id 186
    label "anamneza"
  ]
  node [
    id 187
    label "dotykanie"
  ]
  node [
    id 188
    label "dr&#243;b"
  ]
  node [
    id 189
    label "pomacanie"
  ]
  node [
    id 190
    label "feel"
  ]
  node [
    id 191
    label "palpation"
  ]
  node [
    id 192
    label "namacanie"
  ]
  node [
    id 193
    label "hodowanie"
  ]
  node [
    id 194
    label "patrzenie"
  ]
  node [
    id 195
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 196
    label "doszukanie_si&#281;"
  ]
  node [
    id 197
    label "dostrzeganie"
  ]
  node [
    id 198
    label "poobserwowanie"
  ]
  node [
    id 199
    label "observation"
  ]
  node [
    id 200
    label "bocianie_gniazdo"
  ]
  node [
    id 201
    label "warto&#347;&#263;"
  ]
  node [
    id 202
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 203
    label "set"
  ]
  node [
    id 204
    label "podkre&#347;la&#263;"
  ]
  node [
    id 205
    label "by&#263;"
  ]
  node [
    id 206
    label "podawa&#263;"
  ]
  node [
    id 207
    label "wyraz"
  ]
  node [
    id 208
    label "pokazywa&#263;"
  ]
  node [
    id 209
    label "wybiera&#263;"
  ]
  node [
    id 210
    label "signify"
  ]
  node [
    id 211
    label "represent"
  ]
  node [
    id 212
    label "indicate"
  ]
  node [
    id 213
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 214
    label "exhibit"
  ]
  node [
    id 215
    label "wyra&#380;a&#263;"
  ]
  node [
    id 216
    label "przedstawia&#263;"
  ]
  node [
    id 217
    label "przeszkala&#263;"
  ]
  node [
    id 218
    label "powodowa&#263;"
  ]
  node [
    id 219
    label "introduce"
  ]
  node [
    id 220
    label "exsert"
  ]
  node [
    id 221
    label "bespeak"
  ]
  node [
    id 222
    label "informowa&#263;"
  ]
  node [
    id 223
    label "wyjmowa&#263;"
  ]
  node [
    id 224
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 225
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 226
    label "sie&#263;_rybacka"
  ]
  node [
    id 227
    label "take"
  ]
  node [
    id 228
    label "ustala&#263;"
  ]
  node [
    id 229
    label "kotwica"
  ]
  node [
    id 230
    label "poja&#347;nia&#263;"
  ]
  node [
    id 231
    label "robi&#263;"
  ]
  node [
    id 232
    label "u&#322;atwia&#263;"
  ]
  node [
    id 233
    label "elaborate"
  ]
  node [
    id 234
    label "give"
  ]
  node [
    id 235
    label "suplikowa&#263;"
  ]
  node [
    id 236
    label "przek&#322;ada&#263;"
  ]
  node [
    id 237
    label "przekonywa&#263;"
  ]
  node [
    id 238
    label "interpretowa&#263;"
  ]
  node [
    id 239
    label "broni&#263;"
  ]
  node [
    id 240
    label "j&#281;zyk"
  ]
  node [
    id 241
    label "explain"
  ]
  node [
    id 242
    label "sprawowa&#263;"
  ]
  node [
    id 243
    label "uzasadnia&#263;"
  ]
  node [
    id 244
    label "kreska"
  ]
  node [
    id 245
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 246
    label "oznacza&#263;"
  ]
  node [
    id 247
    label "rysowa&#263;"
  ]
  node [
    id 248
    label "underscore"
  ]
  node [
    id 249
    label "signalize"
  ]
  node [
    id 250
    label "tenis"
  ]
  node [
    id 251
    label "deal"
  ]
  node [
    id 252
    label "dawa&#263;"
  ]
  node [
    id 253
    label "stawia&#263;"
  ]
  node [
    id 254
    label "rozgrywa&#263;"
  ]
  node [
    id 255
    label "kelner"
  ]
  node [
    id 256
    label "siatk&#243;wka"
  ]
  node [
    id 257
    label "cover"
  ]
  node [
    id 258
    label "tender"
  ]
  node [
    id 259
    label "jedzenie"
  ]
  node [
    id 260
    label "faszerowa&#263;"
  ]
  node [
    id 261
    label "serwowa&#263;"
  ]
  node [
    id 262
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 263
    label "mie&#263;_miejsce"
  ]
  node [
    id 264
    label "equal"
  ]
  node [
    id 265
    label "trwa&#263;"
  ]
  node [
    id 266
    label "chodzi&#263;"
  ]
  node [
    id 267
    label "si&#281;ga&#263;"
  ]
  node [
    id 268
    label "stan"
  ]
  node [
    id 269
    label "obecno&#347;&#263;"
  ]
  node [
    id 270
    label "stand"
  ]
  node [
    id 271
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 272
    label "uczestniczy&#263;"
  ]
  node [
    id 273
    label "gem"
  ]
  node [
    id 274
    label "kompozycja"
  ]
  node [
    id 275
    label "runda"
  ]
  node [
    id 276
    label "muzyka"
  ]
  node [
    id 277
    label "zestaw"
  ]
  node [
    id 278
    label "rozmiar"
  ]
  node [
    id 279
    label "zrewaluowa&#263;"
  ]
  node [
    id 280
    label "zmienna"
  ]
  node [
    id 281
    label "wskazywanie"
  ]
  node [
    id 282
    label "rewaluowanie"
  ]
  node [
    id 283
    label "cel"
  ]
  node [
    id 284
    label "korzy&#347;&#263;"
  ]
  node [
    id 285
    label "poj&#281;cie"
  ]
  node [
    id 286
    label "worth"
  ]
  node [
    id 287
    label "cecha"
  ]
  node [
    id 288
    label "zrewaluowanie"
  ]
  node [
    id 289
    label "rewaluowa&#263;"
  ]
  node [
    id 290
    label "wabik"
  ]
  node [
    id 291
    label "strona"
  ]
  node [
    id 292
    label "term"
  ]
  node [
    id 293
    label "oznaka"
  ]
  node [
    id 294
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 295
    label "leksem"
  ]
  node [
    id 296
    label "element"
  ]
  node [
    id 297
    label "&#347;wiadczenie"
  ]
  node [
    id 298
    label "starzy"
  ]
  node [
    id 299
    label "matka"
  ]
  node [
    id 300
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 301
    label "dwa_ognie"
  ]
  node [
    id 302
    label "gracz"
  ]
  node [
    id 303
    label "rozsadnik"
  ]
  node [
    id 304
    label "rodzice"
  ]
  node [
    id 305
    label "staruszka"
  ]
  node [
    id 306
    label "ro&#347;lina"
  ]
  node [
    id 307
    label "macocha"
  ]
  node [
    id 308
    label "matka_zast&#281;pcza"
  ]
  node [
    id 309
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 310
    label "samica"
  ]
  node [
    id 311
    label "zawodnik"
  ]
  node [
    id 312
    label "matczysko"
  ]
  node [
    id 313
    label "macierz"
  ]
  node [
    id 314
    label "Matka_Boska"
  ]
  node [
    id 315
    label "obiekt"
  ]
  node [
    id 316
    label "przodkini"
  ]
  node [
    id 317
    label "zakonnica"
  ]
  node [
    id 318
    label "stara"
  ]
  node [
    id 319
    label "rodzic"
  ]
  node [
    id 320
    label "owad"
  ]
  node [
    id 321
    label "stary"
  ]
  node [
    id 322
    label "Chocho&#322;"
  ]
  node [
    id 323
    label "Herkules_Poirot"
  ]
  node [
    id 324
    label "Edyp"
  ]
  node [
    id 325
    label "parali&#380;owa&#263;"
  ]
  node [
    id 326
    label "Harry_Potter"
  ]
  node [
    id 327
    label "Casanova"
  ]
  node [
    id 328
    label "Gargantua"
  ]
  node [
    id 329
    label "Zgredek"
  ]
  node [
    id 330
    label "Winnetou"
  ]
  node [
    id 331
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 332
    label "Dulcynea"
  ]
  node [
    id 333
    label "kategoria_gramatyczna"
  ]
  node [
    id 334
    label "person"
  ]
  node [
    id 335
    label "Sherlock_Holmes"
  ]
  node [
    id 336
    label "Quasimodo"
  ]
  node [
    id 337
    label "Plastu&#347;"
  ]
  node [
    id 338
    label "Faust"
  ]
  node [
    id 339
    label "Wallenrod"
  ]
  node [
    id 340
    label "Dwukwiat"
  ]
  node [
    id 341
    label "koniugacja"
  ]
  node [
    id 342
    label "Don_Juan"
  ]
  node [
    id 343
    label "Don_Kiszot"
  ]
  node [
    id 344
    label "Hamlet"
  ]
  node [
    id 345
    label "Werter"
  ]
  node [
    id 346
    label "istota"
  ]
  node [
    id 347
    label "Szwejk"
  ]
  node [
    id 348
    label "mentalno&#347;&#263;"
  ]
  node [
    id 349
    label "superego"
  ]
  node [
    id 350
    label "psychika"
  ]
  node [
    id 351
    label "znaczenie"
  ]
  node [
    id 352
    label "wn&#281;trze"
  ]
  node [
    id 353
    label "charakter"
  ]
  node [
    id 354
    label "charakterystyka"
  ]
  node [
    id 355
    label "zaistnie&#263;"
  ]
  node [
    id 356
    label "Osjan"
  ]
  node [
    id 357
    label "kto&#347;"
  ]
  node [
    id 358
    label "wygl&#261;d"
  ]
  node [
    id 359
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 360
    label "osobowo&#347;&#263;"
  ]
  node [
    id 361
    label "trim"
  ]
  node [
    id 362
    label "poby&#263;"
  ]
  node [
    id 363
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 364
    label "Aspazja"
  ]
  node [
    id 365
    label "punkt_widzenia"
  ]
  node [
    id 366
    label "kompleksja"
  ]
  node [
    id 367
    label "wytrzyma&#263;"
  ]
  node [
    id 368
    label "budowa"
  ]
  node [
    id 369
    label "pozosta&#263;"
  ]
  node [
    id 370
    label "point"
  ]
  node [
    id 371
    label "przedstawienie"
  ]
  node [
    id 372
    label "go&#347;&#263;"
  ]
  node [
    id 373
    label "hamper"
  ]
  node [
    id 374
    label "spasm"
  ]
  node [
    id 375
    label "mrozi&#263;"
  ]
  node [
    id 376
    label "pora&#380;a&#263;"
  ]
  node [
    id 377
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 378
    label "fleksja"
  ]
  node [
    id 379
    label "liczba"
  ]
  node [
    id 380
    label "coupling"
  ]
  node [
    id 381
    label "tryb"
  ]
  node [
    id 382
    label "czas"
  ]
  node [
    id 383
    label "czasownik"
  ]
  node [
    id 384
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 385
    label "orz&#281;sek"
  ]
  node [
    id 386
    label "pryncypa&#322;"
  ]
  node [
    id 387
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 388
    label "kszta&#322;t"
  ]
  node [
    id 389
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 390
    label "wiedza"
  ]
  node [
    id 391
    label "kierowa&#263;"
  ]
  node [
    id 392
    label "alkohol"
  ]
  node [
    id 393
    label "zdolno&#347;&#263;"
  ]
  node [
    id 394
    label "&#380;ycie"
  ]
  node [
    id 395
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 396
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 397
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 398
    label "sztuka"
  ]
  node [
    id 399
    label "dekiel"
  ]
  node [
    id 400
    label "&#347;ci&#281;cie"
  ]
  node [
    id 401
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 402
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 403
    label "&#347;ci&#281;gno"
  ]
  node [
    id 404
    label "noosfera"
  ]
  node [
    id 405
    label "byd&#322;o"
  ]
  node [
    id 406
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 407
    label "makrocefalia"
  ]
  node [
    id 408
    label "ucho"
  ]
  node [
    id 409
    label "g&#243;ra"
  ]
  node [
    id 410
    label "m&#243;zg"
  ]
  node [
    id 411
    label "fryzura"
  ]
  node [
    id 412
    label "umys&#322;"
  ]
  node [
    id 413
    label "cia&#322;o"
  ]
  node [
    id 414
    label "cz&#322;onek"
  ]
  node [
    id 415
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 416
    label "czaszka"
  ]
  node [
    id 417
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 418
    label "dziedzina"
  ]
  node [
    id 419
    label "hipnotyzowanie"
  ]
  node [
    id 420
    label "&#347;lad"
  ]
  node [
    id 421
    label "natural_process"
  ]
  node [
    id 422
    label "reakcja_chemiczna"
  ]
  node [
    id 423
    label "wdzieranie_si&#281;"
  ]
  node [
    id 424
    label "zjawisko"
  ]
  node [
    id 425
    label "act"
  ]
  node [
    id 426
    label "lobbysta"
  ]
  node [
    id 427
    label "allochoria"
  ]
  node [
    id 428
    label "fotograf"
  ]
  node [
    id 429
    label "malarz"
  ]
  node [
    id 430
    label "artysta"
  ]
  node [
    id 431
    label "p&#322;aszczyzna"
  ]
  node [
    id 432
    label "przedmiot"
  ]
  node [
    id 433
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 434
    label "bierka_szachowa"
  ]
  node [
    id 435
    label "obiekt_matematyczny"
  ]
  node [
    id 436
    label "gestaltyzm"
  ]
  node [
    id 437
    label "styl"
  ]
  node [
    id 438
    label "obraz"
  ]
  node [
    id 439
    label "rzecz"
  ]
  node [
    id 440
    label "d&#378;wi&#281;k"
  ]
  node [
    id 441
    label "character"
  ]
  node [
    id 442
    label "rze&#378;ba"
  ]
  node [
    id 443
    label "stylistyka"
  ]
  node [
    id 444
    label "figure"
  ]
  node [
    id 445
    label "antycypacja"
  ]
  node [
    id 446
    label "ornamentyka"
  ]
  node [
    id 447
    label "facet"
  ]
  node [
    id 448
    label "popis"
  ]
  node [
    id 449
    label "wiersz"
  ]
  node [
    id 450
    label "symetria"
  ]
  node [
    id 451
    label "lingwistyka_kognitywna"
  ]
  node [
    id 452
    label "karta"
  ]
  node [
    id 453
    label "shape"
  ]
  node [
    id 454
    label "podzbi&#243;r"
  ]
  node [
    id 455
    label "perspektywa"
  ]
  node [
    id 456
    label "Szekspir"
  ]
  node [
    id 457
    label "Mickiewicz"
  ]
  node [
    id 458
    label "cierpienie"
  ]
  node [
    id 459
    label "piek&#322;o"
  ]
  node [
    id 460
    label "human_body"
  ]
  node [
    id 461
    label "ofiarowywanie"
  ]
  node [
    id 462
    label "sfera_afektywna"
  ]
  node [
    id 463
    label "nekromancja"
  ]
  node [
    id 464
    label "Po&#347;wist"
  ]
  node [
    id 465
    label "podekscytowanie"
  ]
  node [
    id 466
    label "deformowanie"
  ]
  node [
    id 467
    label "sumienie"
  ]
  node [
    id 468
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 469
    label "deformowa&#263;"
  ]
  node [
    id 470
    label "zjawa"
  ]
  node [
    id 471
    label "zmar&#322;y"
  ]
  node [
    id 472
    label "istota_nadprzyrodzona"
  ]
  node [
    id 473
    label "power"
  ]
  node [
    id 474
    label "entity"
  ]
  node [
    id 475
    label "ofiarowywa&#263;"
  ]
  node [
    id 476
    label "oddech"
  ]
  node [
    id 477
    label "seksualno&#347;&#263;"
  ]
  node [
    id 478
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 479
    label "byt"
  ]
  node [
    id 480
    label "si&#322;a"
  ]
  node [
    id 481
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 482
    label "ego"
  ]
  node [
    id 483
    label "ofiarowanie"
  ]
  node [
    id 484
    label "fizjonomia"
  ]
  node [
    id 485
    label "kompleks"
  ]
  node [
    id 486
    label "zapalno&#347;&#263;"
  ]
  node [
    id 487
    label "T&#281;sknica"
  ]
  node [
    id 488
    label "ofiarowa&#263;"
  ]
  node [
    id 489
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 490
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 491
    label "passion"
  ]
  node [
    id 492
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 493
    label "atom"
  ]
  node [
    id 494
    label "odbicie"
  ]
  node [
    id 495
    label "przyroda"
  ]
  node [
    id 496
    label "Ziemia"
  ]
  node [
    id 497
    label "kosmos"
  ]
  node [
    id 498
    label "miniatura"
  ]
  node [
    id 499
    label "oszukiwa&#263;"
  ]
  node [
    id 500
    label "lie"
  ]
  node [
    id 501
    label "m&#243;wi&#263;"
  ]
  node [
    id 502
    label "mitomania"
  ]
  node [
    id 503
    label "pitoli&#263;"
  ]
  node [
    id 504
    label "gaworzy&#263;"
  ]
  node [
    id 505
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 506
    label "remark"
  ]
  node [
    id 507
    label "rozmawia&#263;"
  ]
  node [
    id 508
    label "umie&#263;"
  ]
  node [
    id 509
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 510
    label "dziama&#263;"
  ]
  node [
    id 511
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 512
    label "formu&#322;owa&#263;"
  ]
  node [
    id 513
    label "dysfonia"
  ]
  node [
    id 514
    label "express"
  ]
  node [
    id 515
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 516
    label "talk"
  ]
  node [
    id 517
    label "u&#380;ywa&#263;"
  ]
  node [
    id 518
    label "prawi&#263;"
  ]
  node [
    id 519
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 520
    label "powiada&#263;"
  ]
  node [
    id 521
    label "tell"
  ]
  node [
    id 522
    label "chew_the_fat"
  ]
  node [
    id 523
    label "say"
  ]
  node [
    id 524
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 525
    label "wydobywa&#263;"
  ]
  node [
    id 526
    label "okre&#347;la&#263;"
  ]
  node [
    id 527
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 528
    label "orzyna&#263;"
  ]
  node [
    id 529
    label "oszwabia&#263;"
  ]
  node [
    id 530
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 531
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 532
    label "cheat"
  ]
  node [
    id 533
    label "talk_through_one's_hat"
  ]
  node [
    id 534
    label "ple&#347;&#263;"
  ]
  node [
    id 535
    label "choroba_psychiczna"
  ]
  node [
    id 536
    label "k&#322;amanie"
  ]
  node [
    id 537
    label "time"
  ]
  node [
    id 538
    label "zapis"
  ]
  node [
    id 539
    label "sekunda"
  ]
  node [
    id 540
    label "jednostka"
  ]
  node [
    id 541
    label "stopie&#324;"
  ]
  node [
    id 542
    label "godzina"
  ]
  node [
    id 543
    label "design"
  ]
  node [
    id 544
    label "kwadrans"
  ]
  node [
    id 545
    label "przyswoi&#263;"
  ]
  node [
    id 546
    label "one"
  ]
  node [
    id 547
    label "ewoluowanie"
  ]
  node [
    id 548
    label "supremum"
  ]
  node [
    id 549
    label "skala"
  ]
  node [
    id 550
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 551
    label "przyswajanie"
  ]
  node [
    id 552
    label "wyewoluowanie"
  ]
  node [
    id 553
    label "reakcja"
  ]
  node [
    id 554
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 555
    label "przeliczy&#263;"
  ]
  node [
    id 556
    label "wyewoluowa&#263;"
  ]
  node [
    id 557
    label "ewoluowa&#263;"
  ]
  node [
    id 558
    label "matematyka"
  ]
  node [
    id 559
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 560
    label "rzut"
  ]
  node [
    id 561
    label "liczba_naturalna"
  ]
  node [
    id 562
    label "czynnik_biotyczny"
  ]
  node [
    id 563
    label "individual"
  ]
  node [
    id 564
    label "przyswaja&#263;"
  ]
  node [
    id 565
    label "przyswojenie"
  ]
  node [
    id 566
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 567
    label "starzenie_si&#281;"
  ]
  node [
    id 568
    label "przeliczanie"
  ]
  node [
    id 569
    label "funkcja"
  ]
  node [
    id 570
    label "przelicza&#263;"
  ]
  node [
    id 571
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 572
    label "infimum"
  ]
  node [
    id 573
    label "przeliczenie"
  ]
  node [
    id 574
    label "spos&#243;b"
  ]
  node [
    id 575
    label "entrance"
  ]
  node [
    id 576
    label "wpis"
  ]
  node [
    id 577
    label "normalizacja"
  ]
  node [
    id 578
    label "mikrosekunda"
  ]
  node [
    id 579
    label "milisekunda"
  ]
  node [
    id 580
    label "tercja"
  ]
  node [
    id 581
    label "nanosekunda"
  ]
  node [
    id 582
    label "uk&#322;ad_SI"
  ]
  node [
    id 583
    label "jednostka_czasu"
  ]
  node [
    id 584
    label "doba"
  ]
  node [
    id 585
    label "p&#243;&#322;godzina"
  ]
  node [
    id 586
    label "podstopie&#324;"
  ]
  node [
    id 587
    label "wielko&#347;&#263;"
  ]
  node [
    id 588
    label "rank"
  ]
  node [
    id 589
    label "wschodek"
  ]
  node [
    id 590
    label "przymiotnik"
  ]
  node [
    id 591
    label "gama"
  ]
  node [
    id 592
    label "podzia&#322;"
  ]
  node [
    id 593
    label "schody"
  ]
  node [
    id 594
    label "poziom"
  ]
  node [
    id 595
    label "przys&#322;&#243;wek"
  ]
  node [
    id 596
    label "ocena"
  ]
  node [
    id 597
    label "degree"
  ]
  node [
    id 598
    label "szczebel"
  ]
  node [
    id 599
    label "podn&#243;&#380;ek"
  ]
  node [
    id 600
    label "forma"
  ]
  node [
    id 601
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 602
    label "gotowy"
  ]
  node [
    id 603
    label "might"
  ]
  node [
    id 604
    label "uprawi&#263;"
  ]
  node [
    id 605
    label "public_treasury"
  ]
  node [
    id 606
    label "pole"
  ]
  node [
    id 607
    label "obrobi&#263;"
  ]
  node [
    id 608
    label "nietrze&#378;wy"
  ]
  node [
    id 609
    label "czekanie"
  ]
  node [
    id 610
    label "martwy"
  ]
  node [
    id 611
    label "bliski"
  ]
  node [
    id 612
    label "gotowo"
  ]
  node [
    id 613
    label "przygotowanie"
  ]
  node [
    id 614
    label "przygotowywanie"
  ]
  node [
    id 615
    label "dyspozycyjny"
  ]
  node [
    id 616
    label "zalany"
  ]
  node [
    id 617
    label "nieuchronny"
  ]
  node [
    id 618
    label "doj&#347;cie"
  ]
  node [
    id 619
    label "przybra&#263;"
  ]
  node [
    id 620
    label "strike"
  ]
  node [
    id 621
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 622
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 623
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 624
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 625
    label "receive"
  ]
  node [
    id 626
    label "obra&#263;"
  ]
  node [
    id 627
    label "zrobi&#263;"
  ]
  node [
    id 628
    label "uzna&#263;"
  ]
  node [
    id 629
    label "draw"
  ]
  node [
    id 630
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 631
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 632
    label "przyj&#281;cie"
  ]
  node [
    id 633
    label "fall"
  ]
  node [
    id 634
    label "swallow"
  ]
  node [
    id 635
    label "odebra&#263;"
  ]
  node [
    id 636
    label "dostarczy&#263;"
  ]
  node [
    id 637
    label "umie&#347;ci&#263;"
  ]
  node [
    id 638
    label "wzi&#261;&#263;"
  ]
  node [
    id 639
    label "absorb"
  ]
  node [
    id 640
    label "undertake"
  ]
  node [
    id 641
    label "nastawi&#263;"
  ]
  node [
    id 642
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 643
    label "incorporate"
  ]
  node [
    id 644
    label "obejrze&#263;"
  ]
  node [
    id 645
    label "impersonate"
  ]
  node [
    id 646
    label "dokoptowa&#263;"
  ]
  node [
    id 647
    label "prosecute"
  ]
  node [
    id 648
    label "uruchomi&#263;"
  ]
  node [
    id 649
    label "zacz&#261;&#263;"
  ]
  node [
    id 650
    label "post&#261;pi&#263;"
  ]
  node [
    id 651
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 652
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 653
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 654
    label "zorganizowa&#263;"
  ]
  node [
    id 655
    label "appoint"
  ]
  node [
    id 656
    label "wystylizowa&#263;"
  ]
  node [
    id 657
    label "cause"
  ]
  node [
    id 658
    label "przerobi&#263;"
  ]
  node [
    id 659
    label "nabra&#263;"
  ]
  node [
    id 660
    label "make"
  ]
  node [
    id 661
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 662
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 663
    label "wydali&#263;"
  ]
  node [
    id 664
    label "zwi&#281;kszy&#263;_si&#281;"
  ]
  node [
    id 665
    label "turn"
  ]
  node [
    id 666
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 667
    label "sta&#263;_si&#281;"
  ]
  node [
    id 668
    label "nazwa&#263;"
  ]
  node [
    id 669
    label "upi&#281;kszy&#263;"
  ]
  node [
    id 670
    label "assume"
  ]
  node [
    id 671
    label "increase"
  ]
  node [
    id 672
    label "rise"
  ]
  node [
    id 673
    label "pozwoli&#263;"
  ]
  node [
    id 674
    label "digest"
  ]
  node [
    id 675
    label "pu&#347;ci&#263;"
  ]
  node [
    id 676
    label "zlecenie"
  ]
  node [
    id 677
    label "pozbawi&#263;"
  ]
  node [
    id 678
    label "zabra&#263;"
  ]
  node [
    id 679
    label "sketch"
  ]
  node [
    id 680
    label "chwyci&#263;"
  ]
  node [
    id 681
    label "dozna&#263;"
  ]
  node [
    id 682
    label "spowodowa&#263;"
  ]
  node [
    id 683
    label "odzyska&#263;"
  ]
  node [
    id 684
    label "deliver"
  ]
  node [
    id 685
    label "deprive"
  ]
  node [
    id 686
    label "give_birth"
  ]
  node [
    id 687
    label "powo&#322;a&#263;"
  ]
  node [
    id 688
    label "okroi&#263;"
  ]
  node [
    id 689
    label "usun&#261;&#263;"
  ]
  node [
    id 690
    label "shell"
  ]
  node [
    id 691
    label "distill"
  ]
  node [
    id 692
    label "wybra&#263;"
  ]
  node [
    id 693
    label "ostruga&#263;"
  ]
  node [
    id 694
    label "zainteresowa&#263;"
  ]
  node [
    id 695
    label "spo&#380;y&#263;"
  ]
  node [
    id 696
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 697
    label "pozna&#263;"
  ]
  node [
    id 698
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 699
    label "consume"
  ]
  node [
    id 700
    label "odziedziczy&#263;"
  ]
  node [
    id 701
    label "ruszy&#263;"
  ]
  node [
    id 702
    label "zaatakowa&#263;"
  ]
  node [
    id 703
    label "skorzysta&#263;"
  ]
  node [
    id 704
    label "uciec"
  ]
  node [
    id 705
    label "nakaza&#263;"
  ]
  node [
    id 706
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 707
    label "obskoczy&#263;"
  ]
  node [
    id 708
    label "bra&#263;"
  ]
  node [
    id 709
    label "u&#380;y&#263;"
  ]
  node [
    id 710
    label "get"
  ]
  node [
    id 711
    label "wyrucha&#263;"
  ]
  node [
    id 712
    label "World_Health_Organization"
  ]
  node [
    id 713
    label "wyciupcia&#263;"
  ]
  node [
    id 714
    label "wygra&#263;"
  ]
  node [
    id 715
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 716
    label "withdraw"
  ]
  node [
    id 717
    label "wzi&#281;cie"
  ]
  node [
    id 718
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 719
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 720
    label "poczyta&#263;"
  ]
  node [
    id 721
    label "obj&#261;&#263;"
  ]
  node [
    id 722
    label "seize"
  ]
  node [
    id 723
    label "aim"
  ]
  node [
    id 724
    label "pokona&#263;"
  ]
  node [
    id 725
    label "arise"
  ]
  node [
    id 726
    label "uda&#263;_si&#281;"
  ]
  node [
    id 727
    label "otrzyma&#263;"
  ]
  node [
    id 728
    label "wej&#347;&#263;"
  ]
  node [
    id 729
    label "poruszy&#263;"
  ]
  node [
    id 730
    label "dosta&#263;"
  ]
  node [
    id 731
    label "oceni&#263;"
  ]
  node [
    id 732
    label "przyzna&#263;"
  ]
  node [
    id 733
    label "stwierdzi&#263;"
  ]
  node [
    id 734
    label "assent"
  ]
  node [
    id 735
    label "rede"
  ]
  node [
    id 736
    label "see"
  ]
  node [
    id 737
    label "admit"
  ]
  node [
    id 738
    label "wprowadzi&#263;"
  ]
  node [
    id 739
    label "put"
  ]
  node [
    id 740
    label "uplasowa&#263;"
  ]
  node [
    id 741
    label "wpierniczy&#263;"
  ]
  node [
    id 742
    label "okre&#347;li&#263;"
  ]
  node [
    id 743
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 744
    label "zmieni&#263;"
  ]
  node [
    id 745
    label "umieszcza&#263;"
  ]
  node [
    id 746
    label "wytworzy&#263;"
  ]
  node [
    id 747
    label "picture"
  ]
  node [
    id 748
    label "impreza"
  ]
  node [
    id 749
    label "spotkanie"
  ]
  node [
    id 750
    label "wpuszczenie"
  ]
  node [
    id 751
    label "credence"
  ]
  node [
    id 752
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 753
    label "dopuszczenie"
  ]
  node [
    id 754
    label "zareagowanie"
  ]
  node [
    id 755
    label "uznanie"
  ]
  node [
    id 756
    label "presumption"
  ]
  node [
    id 757
    label "entertainment"
  ]
  node [
    id 758
    label "reception"
  ]
  node [
    id 759
    label "umieszczenie"
  ]
  node [
    id 760
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 761
    label "zgodzenie_si&#281;"
  ]
  node [
    id 762
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 763
    label "party"
  ]
  node [
    id 764
    label "stanie_si&#281;"
  ]
  node [
    id 765
    label "w&#322;&#261;czenie"
  ]
  node [
    id 766
    label "nieprawda"
  ]
  node [
    id 767
    label "wymys&#322;"
  ]
  node [
    id 768
    label "blef"
  ]
  node [
    id 769
    label "oszustwo"
  ]
  node [
    id 770
    label "lipa"
  ]
  node [
    id 771
    label "inicjatywa"
  ]
  node [
    id 772
    label "pomys&#322;"
  ]
  node [
    id 773
    label "concoction"
  ]
  node [
    id 774
    label "s&#261;d"
  ]
  node [
    id 775
    label "u&#347;mierci&#263;"
  ]
  node [
    id 776
    label "fib"
  ]
  node [
    id 777
    label "u&#347;miercenie"
  ]
  node [
    id 778
    label "fa&#322;szywo&#347;&#263;"
  ]
  node [
    id 779
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 780
    label "ba&#322;amutnia"
  ]
  node [
    id 781
    label "trickery"
  ]
  node [
    id 782
    label "s&#322;up"
  ]
  node [
    id 783
    label "&#347;ciema"
  ]
  node [
    id 784
    label "czyn"
  ]
  node [
    id 785
    label "kuna"
  ]
  node [
    id 786
    label "siaja"
  ]
  node [
    id 787
    label "&#322;ub"
  ]
  node [
    id 788
    label "&#347;lazowate"
  ]
  node [
    id 789
    label "linden"
  ]
  node [
    id 790
    label "orzech"
  ]
  node [
    id 791
    label "nieudany"
  ]
  node [
    id 792
    label "drzewo"
  ]
  node [
    id 793
    label "ro&#347;lina_miododajna"
  ]
  node [
    id 794
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 795
    label "drewno"
  ]
  node [
    id 796
    label "lipowate"
  ]
  node [
    id 797
    label "baloney"
  ]
  node [
    id 798
    label "sytuacja"
  ]
  node [
    id 799
    label "zmy&#322;ka"
  ]
  node [
    id 800
    label "gra"
  ]
  node [
    id 801
    label "fraud"
  ]
  node [
    id 802
    label "zagranie"
  ]
  node [
    id 803
    label "podst&#281;p"
  ]
  node [
    id 804
    label "antic"
  ]
  node [
    id 805
    label "za&#322;o&#380;enie"
  ]
  node [
    id 806
    label "nieprawdziwy"
  ]
  node [
    id 807
    label "prawdziwy"
  ]
  node [
    id 808
    label "truth"
  ]
  node [
    id 809
    label "realia"
  ]
  node [
    id 810
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 811
    label "message"
  ]
  node [
    id 812
    label "kontekst"
  ]
  node [
    id 813
    label "podwini&#281;cie"
  ]
  node [
    id 814
    label "zap&#322;acenie"
  ]
  node [
    id 815
    label "przyodzianie"
  ]
  node [
    id 816
    label "budowla"
  ]
  node [
    id 817
    label "pokrycie"
  ]
  node [
    id 818
    label "rozebranie"
  ]
  node [
    id 819
    label "zak&#322;adka"
  ]
  node [
    id 820
    label "struktura"
  ]
  node [
    id 821
    label "poubieranie"
  ]
  node [
    id 822
    label "infliction"
  ]
  node [
    id 823
    label "pozak&#322;adanie"
  ]
  node [
    id 824
    label "program"
  ]
  node [
    id 825
    label "przebranie"
  ]
  node [
    id 826
    label "przywdzianie"
  ]
  node [
    id 827
    label "obleczenie_si&#281;"
  ]
  node [
    id 828
    label "utworzenie"
  ]
  node [
    id 829
    label "str&#243;j"
  ]
  node [
    id 830
    label "twierdzenie"
  ]
  node [
    id 831
    label "obleczenie"
  ]
  node [
    id 832
    label "wyko&#324;czenie"
  ]
  node [
    id 833
    label "proposition"
  ]
  node [
    id 834
    label "przewidzenie"
  ]
  node [
    id 835
    label "zesp&#243;&#322;"
  ]
  node [
    id 836
    label "podejrzany"
  ]
  node [
    id 837
    label "s&#261;downictwo"
  ]
  node [
    id 838
    label "system"
  ]
  node [
    id 839
    label "biuro"
  ]
  node [
    id 840
    label "court"
  ]
  node [
    id 841
    label "forum"
  ]
  node [
    id 842
    label "bronienie"
  ]
  node [
    id 843
    label "urz&#261;d"
  ]
  node [
    id 844
    label "oskar&#380;yciel"
  ]
  node [
    id 845
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 846
    label "skazany"
  ]
  node [
    id 847
    label "post&#281;powanie"
  ]
  node [
    id 848
    label "my&#347;l"
  ]
  node [
    id 849
    label "pods&#261;dny"
  ]
  node [
    id 850
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 851
    label "obrona"
  ]
  node [
    id 852
    label "wypowied&#378;"
  ]
  node [
    id 853
    label "antylogizm"
  ]
  node [
    id 854
    label "konektyw"
  ]
  node [
    id 855
    label "&#347;wiadek"
  ]
  node [
    id 856
    label "procesowicz"
  ]
  node [
    id 857
    label "&#380;ywny"
  ]
  node [
    id 858
    label "szczery"
  ]
  node [
    id 859
    label "naturalny"
  ]
  node [
    id 860
    label "naprawd&#281;"
  ]
  node [
    id 861
    label "realnie"
  ]
  node [
    id 862
    label "zgodny"
  ]
  node [
    id 863
    label "m&#261;dry"
  ]
  node [
    id 864
    label "prawdziwie"
  ]
  node [
    id 865
    label "nieprawdziwie"
  ]
  node [
    id 866
    label "niezgodny"
  ]
  node [
    id 867
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 868
    label "udawany"
  ]
  node [
    id 869
    label "nieszczery"
  ]
  node [
    id 870
    label "niehistoryczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 239
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
]
