graph [
  node [
    id 0
    label "taka"
    origin "text"
  ]
  node [
    id 1
    label "mama"
    origin "text"
  ]
  node [
    id 2
    label "refleksja"
    origin "text"
  ]
  node [
    id 3
    label "wpis"
    origin "text"
  ]
  node [
    id 4
    label "Bangladesz"
  ]
  node [
    id 5
    label "jednostka_monetarna"
  ]
  node [
    id 6
    label "Bengal"
  ]
  node [
    id 7
    label "przodkini"
  ]
  node [
    id 8
    label "matka_zast&#281;pcza"
  ]
  node [
    id 9
    label "matczysko"
  ]
  node [
    id 10
    label "rodzice"
  ]
  node [
    id 11
    label "stara"
  ]
  node [
    id 12
    label "macierz"
  ]
  node [
    id 13
    label "rodzic"
  ]
  node [
    id 14
    label "Matka_Boska"
  ]
  node [
    id 15
    label "macocha"
  ]
  node [
    id 16
    label "starzy"
  ]
  node [
    id 17
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 18
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 19
    label "pokolenie"
  ]
  node [
    id 20
    label "wapniaki"
  ]
  node [
    id 21
    label "krewna"
  ]
  node [
    id 22
    label "opiekun"
  ]
  node [
    id 23
    label "wapniak"
  ]
  node [
    id 24
    label "rodzic_chrzestny"
  ]
  node [
    id 25
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 26
    label "matka"
  ]
  node [
    id 27
    label "&#380;ona"
  ]
  node [
    id 28
    label "kobieta"
  ]
  node [
    id 29
    label "partnerka"
  ]
  node [
    id 30
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 31
    label "matuszka"
  ]
  node [
    id 32
    label "parametryzacja"
  ]
  node [
    id 33
    label "pa&#324;stwo"
  ]
  node [
    id 34
    label "poj&#281;cie"
  ]
  node [
    id 35
    label "mod"
  ]
  node [
    id 36
    label "patriota"
  ]
  node [
    id 37
    label "m&#281;&#380;atka"
  ]
  node [
    id 38
    label "my&#347;l"
  ]
  node [
    id 39
    label "meditation"
  ]
  node [
    id 40
    label "namys&#322;"
  ]
  node [
    id 41
    label "trud"
  ]
  node [
    id 42
    label "s&#261;d"
  ]
  node [
    id 43
    label "szko&#322;a"
  ]
  node [
    id 44
    label "wytw&#243;r"
  ]
  node [
    id 45
    label "p&#322;&#243;d"
  ]
  node [
    id 46
    label "thinking"
  ]
  node [
    id 47
    label "umys&#322;"
  ]
  node [
    id 48
    label "political_orientation"
  ]
  node [
    id 49
    label "istota"
  ]
  node [
    id 50
    label "pomys&#322;"
  ]
  node [
    id 51
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 52
    label "idea"
  ]
  node [
    id 53
    label "system"
  ]
  node [
    id 54
    label "fantomatyka"
  ]
  node [
    id 55
    label "inscription"
  ]
  node [
    id 56
    label "op&#322;ata"
  ]
  node [
    id 57
    label "akt"
  ]
  node [
    id 58
    label "tekst"
  ]
  node [
    id 59
    label "czynno&#347;&#263;"
  ]
  node [
    id 60
    label "entrance"
  ]
  node [
    id 61
    label "ekscerpcja"
  ]
  node [
    id 62
    label "j&#281;zykowo"
  ]
  node [
    id 63
    label "wypowied&#378;"
  ]
  node [
    id 64
    label "redakcja"
  ]
  node [
    id 65
    label "pomini&#281;cie"
  ]
  node [
    id 66
    label "dzie&#322;o"
  ]
  node [
    id 67
    label "preparacja"
  ]
  node [
    id 68
    label "odmianka"
  ]
  node [
    id 69
    label "opu&#347;ci&#263;"
  ]
  node [
    id 70
    label "koniektura"
  ]
  node [
    id 71
    label "pisa&#263;"
  ]
  node [
    id 72
    label "obelga"
  ]
  node [
    id 73
    label "kwota"
  ]
  node [
    id 74
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 75
    label "activity"
  ]
  node [
    id 76
    label "bezproblemowy"
  ]
  node [
    id 77
    label "wydarzenie"
  ]
  node [
    id 78
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 79
    label "podnieci&#263;"
  ]
  node [
    id 80
    label "scena"
  ]
  node [
    id 81
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 82
    label "numer"
  ]
  node [
    id 83
    label "po&#380;ycie"
  ]
  node [
    id 84
    label "podniecenie"
  ]
  node [
    id 85
    label "nago&#347;&#263;"
  ]
  node [
    id 86
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 87
    label "fascyku&#322;"
  ]
  node [
    id 88
    label "seks"
  ]
  node [
    id 89
    label "podniecanie"
  ]
  node [
    id 90
    label "imisja"
  ]
  node [
    id 91
    label "zwyczaj"
  ]
  node [
    id 92
    label "rozmna&#380;anie"
  ]
  node [
    id 93
    label "ruch_frykcyjny"
  ]
  node [
    id 94
    label "ontologia"
  ]
  node [
    id 95
    label "na_pieska"
  ]
  node [
    id 96
    label "pozycja_misjonarska"
  ]
  node [
    id 97
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 98
    label "fragment"
  ]
  node [
    id 99
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 100
    label "z&#322;&#261;czenie"
  ]
  node [
    id 101
    label "gra_wst&#281;pna"
  ]
  node [
    id 102
    label "erotyka"
  ]
  node [
    id 103
    label "urzeczywistnienie"
  ]
  node [
    id 104
    label "baraszki"
  ]
  node [
    id 105
    label "certificate"
  ]
  node [
    id 106
    label "po&#380;&#261;danie"
  ]
  node [
    id 107
    label "wzw&#243;d"
  ]
  node [
    id 108
    label "funkcja"
  ]
  node [
    id 109
    label "act"
  ]
  node [
    id 110
    label "dokument"
  ]
  node [
    id 111
    label "arystotelizm"
  ]
  node [
    id 112
    label "podnieca&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
]
