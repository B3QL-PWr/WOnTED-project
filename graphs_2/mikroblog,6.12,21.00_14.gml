graph [
  node [
    id 0
    label "&#322;adny"
    origin "text"
  ]
  node [
    id 1
    label "p&#322;ot"
    origin "text"
  ]
  node [
    id 2
    label "wycialem"
    origin "text"
  ]
  node [
    id 3
    label "przyzwoity"
  ]
  node [
    id 4
    label "g&#322;adki"
  ]
  node [
    id 5
    label "ch&#281;dogi"
  ]
  node [
    id 6
    label "obyczajny"
  ]
  node [
    id 7
    label "dobrze"
  ]
  node [
    id 8
    label "niez&#322;y"
  ]
  node [
    id 9
    label "ca&#322;y"
  ]
  node [
    id 10
    label "&#347;warny"
  ]
  node [
    id 11
    label "harny"
  ]
  node [
    id 12
    label "przyjemny"
  ]
  node [
    id 13
    label "po&#380;&#261;dany"
  ]
  node [
    id 14
    label "&#322;adnie"
  ]
  node [
    id 15
    label "dobry"
  ]
  node [
    id 16
    label "z&#322;y"
  ]
  node [
    id 17
    label "intensywny"
  ]
  node [
    id 18
    label "udolny"
  ]
  node [
    id 19
    label "skuteczny"
  ]
  node [
    id 20
    label "&#347;mieszny"
  ]
  node [
    id 21
    label "niczegowaty"
  ]
  node [
    id 22
    label "nieszpetny"
  ]
  node [
    id 23
    label "spory"
  ]
  node [
    id 24
    label "pozytywny"
  ]
  node [
    id 25
    label "korzystny"
  ]
  node [
    id 26
    label "nie&#378;le"
  ]
  node [
    id 27
    label "dobroczynny"
  ]
  node [
    id 28
    label "czw&#243;rka"
  ]
  node [
    id 29
    label "spokojny"
  ]
  node [
    id 30
    label "mi&#322;y"
  ]
  node [
    id 31
    label "grzeczny"
  ]
  node [
    id 32
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 33
    label "powitanie"
  ]
  node [
    id 34
    label "zwrot"
  ]
  node [
    id 35
    label "pomy&#347;lny"
  ]
  node [
    id 36
    label "moralny"
  ]
  node [
    id 37
    label "drogi"
  ]
  node [
    id 38
    label "odpowiedni"
  ]
  node [
    id 39
    label "pos&#322;uszny"
  ]
  node [
    id 40
    label "jedyny"
  ]
  node [
    id 41
    label "du&#380;y"
  ]
  node [
    id 42
    label "zdr&#243;w"
  ]
  node [
    id 43
    label "calu&#347;ko"
  ]
  node [
    id 44
    label "kompletny"
  ]
  node [
    id 45
    label "&#380;ywy"
  ]
  node [
    id 46
    label "pe&#322;ny"
  ]
  node [
    id 47
    label "podobny"
  ]
  node [
    id 48
    label "ca&#322;o"
  ]
  node [
    id 49
    label "skromny"
  ]
  node [
    id 50
    label "kulturalny"
  ]
  node [
    id 51
    label "stosowny"
  ]
  node [
    id 52
    label "przystojny"
  ]
  node [
    id 53
    label "nale&#380;yty"
  ]
  node [
    id 54
    label "przyzwoicie"
  ]
  node [
    id 55
    label "wystarczaj&#261;cy"
  ]
  node [
    id 56
    label "przyjemnie"
  ]
  node [
    id 57
    label "pieski"
  ]
  node [
    id 58
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 59
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 60
    label "niekorzystny"
  ]
  node [
    id 61
    label "z&#322;oszczenie"
  ]
  node [
    id 62
    label "sierdzisty"
  ]
  node [
    id 63
    label "niegrzeczny"
  ]
  node [
    id 64
    label "zez&#322;oszczenie"
  ]
  node [
    id 65
    label "zdenerwowany"
  ]
  node [
    id 66
    label "negatywny"
  ]
  node [
    id 67
    label "rozgniewanie"
  ]
  node [
    id 68
    label "gniewanie"
  ]
  node [
    id 69
    label "niemoralny"
  ]
  node [
    id 70
    label "&#378;le"
  ]
  node [
    id 71
    label "niepomy&#347;lny"
  ]
  node [
    id 72
    label "syf"
  ]
  node [
    id 73
    label "ch&#281;dogo"
  ]
  node [
    id 74
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 75
    label "odpowiednio"
  ]
  node [
    id 76
    label "dobroczynnie"
  ]
  node [
    id 77
    label "moralnie"
  ]
  node [
    id 78
    label "korzystnie"
  ]
  node [
    id 79
    label "pozytywnie"
  ]
  node [
    id 80
    label "lepiej"
  ]
  node [
    id 81
    label "wiele"
  ]
  node [
    id 82
    label "skutecznie"
  ]
  node [
    id 83
    label "pomy&#347;lnie"
  ]
  node [
    id 84
    label "bezproblemowy"
  ]
  node [
    id 85
    label "elegancki"
  ]
  node [
    id 86
    label "og&#243;lnikowy"
  ]
  node [
    id 87
    label "atrakcyjny"
  ]
  node [
    id 88
    label "g&#322;adzenie"
  ]
  node [
    id 89
    label "nieruchomy"
  ]
  node [
    id 90
    label "&#322;atwy"
  ]
  node [
    id 91
    label "r&#243;wny"
  ]
  node [
    id 92
    label "wyg&#322;adzanie_si&#281;"
  ]
  node [
    id 93
    label "jednobarwny"
  ]
  node [
    id 94
    label "przyg&#322;adzenie"
  ]
  node [
    id 95
    label "obtaczanie"
  ]
  node [
    id 96
    label "g&#322;adko"
  ]
  node [
    id 97
    label "prosty"
  ]
  node [
    id 98
    label "przyg&#322;adzanie"
  ]
  node [
    id 99
    label "cisza"
  ]
  node [
    id 100
    label "okr&#261;g&#322;y"
  ]
  node [
    id 101
    label "wyg&#322;adzenie_si&#281;"
  ]
  node [
    id 102
    label "wyg&#322;adzenie"
  ]
  node [
    id 103
    label "wyr&#243;wnanie"
  ]
  node [
    id 104
    label "zuchwa&#322;y"
  ]
  node [
    id 105
    label "smaczny"
  ]
  node [
    id 106
    label "porz&#261;dny"
  ]
  node [
    id 107
    label "ogrodzenie"
  ]
  node [
    id 108
    label "bramka"
  ]
  node [
    id 109
    label "grodza"
  ]
  node [
    id 110
    label "okalanie"
  ]
  node [
    id 111
    label "railing"
  ]
  node [
    id 112
    label "przeszkoda"
  ]
  node [
    id 113
    label "przestrze&#324;"
  ]
  node [
    id 114
    label "reservation"
  ]
  node [
    id 115
    label "otoczenie"
  ]
  node [
    id 116
    label "wjazd"
  ]
  node [
    id 117
    label "wskok"
  ]
  node [
    id 118
    label "kraal"
  ]
  node [
    id 119
    label "limitation"
  ]
  node [
    id 120
    label "rzecz"
  ]
  node [
    id 121
    label "obstawianie"
  ]
  node [
    id 122
    label "trafienie"
  ]
  node [
    id 123
    label "przedmiot"
  ]
  node [
    id 124
    label "obstawienie"
  ]
  node [
    id 125
    label "zawiasy"
  ]
  node [
    id 126
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 127
    label "s&#322;upek"
  ]
  node [
    id 128
    label "boisko"
  ]
  node [
    id 129
    label "siatka"
  ]
  node [
    id 130
    label "obstawia&#263;"
  ]
  node [
    id 131
    label "zamek"
  ]
  node [
    id 132
    label "goal"
  ]
  node [
    id 133
    label "poprzeczka"
  ]
  node [
    id 134
    label "obstawi&#263;"
  ]
  node [
    id 135
    label "wej&#347;cie"
  ]
  node [
    id 136
    label "brama"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
]
