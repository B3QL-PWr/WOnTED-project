graph [
  node [
    id 0
    label "licytacja"
    origin "text"
  ]
  node [
    id 1
    label "z&#322;ot&#243;wka"
    origin "text"
  ]
  node [
    id 2
    label "gimbynieznajo"
    origin "text"
  ]
  node [
    id 3
    label "przetarg"
  ]
  node [
    id 4
    label "rozdanie"
  ]
  node [
    id 5
    label "faza"
  ]
  node [
    id 6
    label "pas"
  ]
  node [
    id 7
    label "sprzeda&#380;"
  ]
  node [
    id 8
    label "bryd&#380;"
  ]
  node [
    id 9
    label "tysi&#261;c"
  ]
  node [
    id 10
    label "skat"
  ]
  node [
    id 11
    label "gra_w_karty"
  ]
  node [
    id 12
    label "przeniesienie_praw"
  ]
  node [
    id 13
    label "przeda&#380;"
  ]
  node [
    id 14
    label "transakcja"
  ]
  node [
    id 15
    label "sprzedaj&#261;cy"
  ]
  node [
    id 16
    label "rabat"
  ]
  node [
    id 17
    label "cykl_astronomiczny"
  ]
  node [
    id 18
    label "coil"
  ]
  node [
    id 19
    label "zjawisko"
  ]
  node [
    id 20
    label "fotoelement"
  ]
  node [
    id 21
    label "komutowanie"
  ]
  node [
    id 22
    label "stan_skupienia"
  ]
  node [
    id 23
    label "nastr&#243;j"
  ]
  node [
    id 24
    label "przerywacz"
  ]
  node [
    id 25
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 26
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 27
    label "kraw&#281;d&#378;"
  ]
  node [
    id 28
    label "obsesja"
  ]
  node [
    id 29
    label "dw&#243;jnik"
  ]
  node [
    id 30
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 31
    label "okres"
  ]
  node [
    id 32
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 33
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 34
    label "przew&#243;d"
  ]
  node [
    id 35
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 36
    label "czas"
  ]
  node [
    id 37
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 38
    label "obw&#243;d"
  ]
  node [
    id 39
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 40
    label "degree"
  ]
  node [
    id 41
    label "komutowa&#263;"
  ]
  node [
    id 42
    label "danie"
  ]
  node [
    id 43
    label "porozdawanie"
  ]
  node [
    id 44
    label "distribute"
  ]
  node [
    id 45
    label "runda"
  ]
  node [
    id 46
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 47
    label "inwit"
  ]
  node [
    id 48
    label "odzywanie_si&#281;"
  ]
  node [
    id 49
    label "rekontra"
  ]
  node [
    id 50
    label "odezwanie_si&#281;"
  ]
  node [
    id 51
    label "korona"
  ]
  node [
    id 52
    label "odwrotka"
  ]
  node [
    id 53
    label "sport"
  ]
  node [
    id 54
    label "rober"
  ]
  node [
    id 55
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 56
    label "longer"
  ]
  node [
    id 57
    label "kontrakt"
  ]
  node [
    id 58
    label "kwota"
  ]
  node [
    id 59
    label "liczba"
  ]
  node [
    id 60
    label "molarity"
  ]
  node [
    id 61
    label "tauzen"
  ]
  node [
    id 62
    label "patyk"
  ]
  node [
    id 63
    label "musik"
  ]
  node [
    id 64
    label "&#347;piew"
  ]
  node [
    id 65
    label "sale"
  ]
  node [
    id 66
    label "konkurs"
  ]
  node [
    id 67
    label "przybitka"
  ]
  node [
    id 68
    label "sp&#243;r"
  ]
  node [
    id 69
    label "auction"
  ]
  node [
    id 70
    label "dodatek"
  ]
  node [
    id 71
    label "linia"
  ]
  node [
    id 72
    label "kawa&#322;ek"
  ]
  node [
    id 73
    label "figura_heraldyczna"
  ]
  node [
    id 74
    label "wci&#281;cie"
  ]
  node [
    id 75
    label "obszar"
  ]
  node [
    id 76
    label "bielizna"
  ]
  node [
    id 77
    label "miejsce"
  ]
  node [
    id 78
    label "sk&#322;ad"
  ]
  node [
    id 79
    label "obiekt"
  ]
  node [
    id 80
    label "zagranie"
  ]
  node [
    id 81
    label "heraldyka"
  ]
  node [
    id 82
    label "odznaka"
  ]
  node [
    id 83
    label "tarcza_herbowa"
  ]
  node [
    id 84
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 85
    label "nap&#281;d"
  ]
  node [
    id 86
    label "grosz"
  ]
  node [
    id 87
    label "pieni&#261;dz"
  ]
  node [
    id 88
    label "jednostka_monetarna"
  ]
  node [
    id 89
    label "Polska"
  ]
  node [
    id 90
    label "rozmienia&#263;"
  ]
  node [
    id 91
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 92
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 93
    label "moniak"
  ]
  node [
    id 94
    label "nomina&#322;"
  ]
  node [
    id 95
    label "zdewaluowa&#263;"
  ]
  node [
    id 96
    label "dewaluowanie"
  ]
  node [
    id 97
    label "pieni&#261;dze"
  ]
  node [
    id 98
    label "wytw&#243;r"
  ]
  node [
    id 99
    label "numizmat"
  ]
  node [
    id 100
    label "rozmienianie"
  ]
  node [
    id 101
    label "rozmieni&#263;"
  ]
  node [
    id 102
    label "dewaluowa&#263;"
  ]
  node [
    id 103
    label "rozmienienie"
  ]
  node [
    id 104
    label "zdewaluowanie"
  ]
  node [
    id 105
    label "coin"
  ]
  node [
    id 106
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 107
    label "z&#322;oty"
  ]
  node [
    id 108
    label "groszak"
  ]
  node [
    id 109
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 110
    label "szyling_austryjacki"
  ]
  node [
    id 111
    label "moneta"
  ]
  node [
    id 112
    label "Mazowsze"
  ]
  node [
    id 113
    label "Pa&#322;uki"
  ]
  node [
    id 114
    label "Pomorze_Zachodnie"
  ]
  node [
    id 115
    label "Powi&#347;le"
  ]
  node [
    id 116
    label "Wolin"
  ]
  node [
    id 117
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 118
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 119
    label "So&#322;a"
  ]
  node [
    id 120
    label "Unia_Europejska"
  ]
  node [
    id 121
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 122
    label "Opolskie"
  ]
  node [
    id 123
    label "Suwalszczyzna"
  ]
  node [
    id 124
    label "Krajna"
  ]
  node [
    id 125
    label "barwy_polskie"
  ]
  node [
    id 126
    label "Nadbu&#380;e"
  ]
  node [
    id 127
    label "Podlasie"
  ]
  node [
    id 128
    label "Izera"
  ]
  node [
    id 129
    label "Ma&#322;opolska"
  ]
  node [
    id 130
    label "Warmia"
  ]
  node [
    id 131
    label "Mazury"
  ]
  node [
    id 132
    label "NATO"
  ]
  node [
    id 133
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 134
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 135
    label "Lubelszczyzna"
  ]
  node [
    id 136
    label "Kaczawa"
  ]
  node [
    id 137
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 138
    label "Kielecczyzna"
  ]
  node [
    id 139
    label "Lubuskie"
  ]
  node [
    id 140
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 141
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 142
    label "&#321;&#243;dzkie"
  ]
  node [
    id 143
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 144
    label "Kujawy"
  ]
  node [
    id 145
    label "Podkarpacie"
  ]
  node [
    id 146
    label "Wielkopolska"
  ]
  node [
    id 147
    label "Wis&#322;a"
  ]
  node [
    id 148
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 149
    label "Bory_Tucholskie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
]
