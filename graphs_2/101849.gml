graph [
  node [
    id 0
    label "creative"
    origin "text"
  ]
  node [
    id 1
    label "commons"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "ko&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "naukowy"
    origin "text"
  ]
  node [
    id 5
    label "w&#322;asno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "intelektualny"
    origin "text"
  ]
  node [
    id 7
    label "wydzia&#322;"
    origin "text"
  ]
  node [
    id 8
    label "prawa"
    origin "text"
  ]
  node [
    id 9
    label "administracja"
    origin "text"
  ]
  node [
    id 10
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 11
    label "warszawskie"
    origin "text"
  ]
  node [
    id 12
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "konferencja"
    origin "text"
  ]
  node [
    id 14
    label "zatytu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "kultura"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "legalny"
    origin "text"
  ]
  node [
    id 18
    label "polski"
    origin "text"
  ]
  node [
    id 19
    label "prawie"
    origin "text"
  ]
  node [
    id 20
    label "autorski"
    origin "text"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "gang"
  ]
  node [
    id 23
    label "&#322;ama&#263;"
  ]
  node [
    id 24
    label "zabawa"
  ]
  node [
    id 25
    label "&#322;amanie"
  ]
  node [
    id 26
    label "obr&#281;cz"
  ]
  node [
    id 27
    label "piasta"
  ]
  node [
    id 28
    label "lap"
  ]
  node [
    id 29
    label "figura_geometryczna"
  ]
  node [
    id 30
    label "sphere"
  ]
  node [
    id 31
    label "grupa"
  ]
  node [
    id 32
    label "o&#347;"
  ]
  node [
    id 33
    label "kolokwium"
  ]
  node [
    id 34
    label "pi"
  ]
  node [
    id 35
    label "zwolnica"
  ]
  node [
    id 36
    label "p&#243;&#322;kole"
  ]
  node [
    id 37
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 38
    label "sejmik"
  ]
  node [
    id 39
    label "pojazd"
  ]
  node [
    id 40
    label "figura_ograniczona"
  ]
  node [
    id 41
    label "whip"
  ]
  node [
    id 42
    label "okr&#261;g"
  ]
  node [
    id 43
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 44
    label "odcinek_ko&#322;a"
  ]
  node [
    id 45
    label "stowarzyszenie"
  ]
  node [
    id 46
    label "podwozie"
  ]
  node [
    id 47
    label "odm&#322;adzanie"
  ]
  node [
    id 48
    label "liga"
  ]
  node [
    id 49
    label "jednostka_systematyczna"
  ]
  node [
    id 50
    label "asymilowanie"
  ]
  node [
    id 51
    label "gromada"
  ]
  node [
    id 52
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 53
    label "asymilowa&#263;"
  ]
  node [
    id 54
    label "egzemplarz"
  ]
  node [
    id 55
    label "Entuzjastki"
  ]
  node [
    id 56
    label "zbi&#243;r"
  ]
  node [
    id 57
    label "kompozycja"
  ]
  node [
    id 58
    label "Terranie"
  ]
  node [
    id 59
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 60
    label "category"
  ]
  node [
    id 61
    label "pakiet_klimatyczny"
  ]
  node [
    id 62
    label "oddzia&#322;"
  ]
  node [
    id 63
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 64
    label "cz&#261;steczka"
  ]
  node [
    id 65
    label "stage_set"
  ]
  node [
    id 66
    label "type"
  ]
  node [
    id 67
    label "specgrupa"
  ]
  node [
    id 68
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 69
    label "&#346;wietliki"
  ]
  node [
    id 70
    label "odm&#322;odzenie"
  ]
  node [
    id 71
    label "Eurogrupa"
  ]
  node [
    id 72
    label "odm&#322;adza&#263;"
  ]
  node [
    id 73
    label "formacja_geologiczna"
  ]
  node [
    id 74
    label "harcerze_starsi"
  ]
  node [
    id 75
    label "zboczenie"
  ]
  node [
    id 76
    label "om&#243;wienie"
  ]
  node [
    id 77
    label "sponiewieranie"
  ]
  node [
    id 78
    label "discipline"
  ]
  node [
    id 79
    label "rzecz"
  ]
  node [
    id 80
    label "omawia&#263;"
  ]
  node [
    id 81
    label "kr&#261;&#380;enie"
  ]
  node [
    id 82
    label "tre&#347;&#263;"
  ]
  node [
    id 83
    label "robienie"
  ]
  node [
    id 84
    label "sponiewiera&#263;"
  ]
  node [
    id 85
    label "element"
  ]
  node [
    id 86
    label "entity"
  ]
  node [
    id 87
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 88
    label "tematyka"
  ]
  node [
    id 89
    label "w&#261;tek"
  ]
  node [
    id 90
    label "charakter"
  ]
  node [
    id 91
    label "zbaczanie"
  ]
  node [
    id 92
    label "program_nauczania"
  ]
  node [
    id 93
    label "om&#243;wi&#263;"
  ]
  node [
    id 94
    label "omawianie"
  ]
  node [
    id 95
    label "thing"
  ]
  node [
    id 96
    label "istota"
  ]
  node [
    id 97
    label "zbacza&#263;"
  ]
  node [
    id 98
    label "zboczy&#263;"
  ]
  node [
    id 99
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 100
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 101
    label "Chewra_Kadisza"
  ]
  node [
    id 102
    label "organizacja"
  ]
  node [
    id 103
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 104
    label "fabianie"
  ]
  node [
    id 105
    label "Rotary_International"
  ]
  node [
    id 106
    label "Eleusis"
  ]
  node [
    id 107
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 108
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 109
    label "Monar"
  ]
  node [
    id 110
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 111
    label "organ"
  ]
  node [
    id 112
    label "zgromadzenie"
  ]
  node [
    id 113
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 114
    label "rozrywka"
  ]
  node [
    id 115
    label "impreza"
  ]
  node [
    id 116
    label "igraszka"
  ]
  node [
    id 117
    label "taniec"
  ]
  node [
    id 118
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 119
    label "gambling"
  ]
  node [
    id 120
    label "chwyt"
  ]
  node [
    id 121
    label "game"
  ]
  node [
    id 122
    label "igra"
  ]
  node [
    id 123
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 124
    label "cecha"
  ]
  node [
    id 125
    label "nabawienie_si&#281;"
  ]
  node [
    id 126
    label "ubaw"
  ]
  node [
    id 127
    label "wodzirej"
  ]
  node [
    id 128
    label "Rzym_Zachodni"
  ]
  node [
    id 129
    label "whole"
  ]
  node [
    id 130
    label "ilo&#347;&#263;"
  ]
  node [
    id 131
    label "Rzym_Wschodni"
  ]
  node [
    id 132
    label "urz&#261;dzenie"
  ]
  node [
    id 133
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 134
    label "gangster"
  ]
  node [
    id 135
    label "banda"
  ]
  node [
    id 136
    label "wy&#322;amywanie"
  ]
  node [
    id 137
    label "dzielenie"
  ]
  node [
    id 138
    label "powodowanie"
  ]
  node [
    id 139
    label "kszta&#322;towanie"
  ]
  node [
    id 140
    label "b&#243;l"
  ]
  node [
    id 141
    label "spread"
  ]
  node [
    id 142
    label "breakage"
  ]
  node [
    id 143
    label "pokonywanie"
  ]
  node [
    id 144
    label "za&#322;amywanie_si&#281;"
  ]
  node [
    id 145
    label "sk&#322;adanie"
  ]
  node [
    id 146
    label "przygn&#281;bianie"
  ]
  node [
    id 147
    label "misdemeanor"
  ]
  node [
    id 148
    label "czynno&#347;&#263;"
  ]
  node [
    id 149
    label "pokonywa&#263;"
  ]
  node [
    id 150
    label "przygn&#281;bia&#263;"
  ]
  node [
    id 151
    label "robi&#263;"
  ]
  node [
    id 152
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 153
    label "sk&#322;ada&#263;"
  ]
  node [
    id 154
    label "&#322;omi&#263;"
  ]
  node [
    id 155
    label "transgress"
  ]
  node [
    id 156
    label "dzieli&#263;"
  ]
  node [
    id 157
    label "p&#322;atowiec"
  ]
  node [
    id 158
    label "bombowiec"
  ]
  node [
    id 159
    label "zawieszenie"
  ]
  node [
    id 160
    label "liczba"
  ]
  node [
    id 161
    label "sta&#322;a"
  ]
  node [
    id 162
    label "colloquium"
  ]
  node [
    id 163
    label "sympozjum"
  ]
  node [
    id 164
    label "kolos"
  ]
  node [
    id 165
    label "praca_pisemna"
  ]
  node [
    id 166
    label "sprawdzian"
  ]
  node [
    id 167
    label "struktura"
  ]
  node [
    id 168
    label "granica"
  ]
  node [
    id 169
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 170
    label "suport"
  ]
  node [
    id 171
    label "prosta"
  ]
  node [
    id 172
    label "o&#347;rodek"
  ]
  node [
    id 173
    label "z&#261;b"
  ]
  node [
    id 174
    label "okucie"
  ]
  node [
    id 175
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 176
    label "przek&#322;adnia"
  ]
  node [
    id 177
    label "dekielek"
  ]
  node [
    id 178
    label "figura_p&#322;aska"
  ]
  node [
    id 179
    label "circumference"
  ]
  node [
    id 180
    label "&#322;uk"
  ]
  node [
    id 181
    label "circle"
  ]
  node [
    id 182
    label "odholowa&#263;"
  ]
  node [
    id 183
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 184
    label "tabor"
  ]
  node [
    id 185
    label "przyholowywanie"
  ]
  node [
    id 186
    label "przyholowa&#263;"
  ]
  node [
    id 187
    label "przyholowanie"
  ]
  node [
    id 188
    label "fukni&#281;cie"
  ]
  node [
    id 189
    label "l&#261;d"
  ]
  node [
    id 190
    label "zielona_karta"
  ]
  node [
    id 191
    label "fukanie"
  ]
  node [
    id 192
    label "przyholowywa&#263;"
  ]
  node [
    id 193
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 194
    label "woda"
  ]
  node [
    id 195
    label "przeszklenie"
  ]
  node [
    id 196
    label "test_zderzeniowy"
  ]
  node [
    id 197
    label "powietrze"
  ]
  node [
    id 198
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 199
    label "odzywka"
  ]
  node [
    id 200
    label "nadwozie"
  ]
  node [
    id 201
    label "odholowanie"
  ]
  node [
    id 202
    label "prowadzenie_si&#281;"
  ]
  node [
    id 203
    label "odholowywa&#263;"
  ]
  node [
    id 204
    label "pod&#322;oga"
  ]
  node [
    id 205
    label "odholowywanie"
  ]
  node [
    id 206
    label "hamulec"
  ]
  node [
    id 207
    label "naukowo"
  ]
  node [
    id 208
    label "teoretyczny"
  ]
  node [
    id 209
    label "edukacyjnie"
  ]
  node [
    id 210
    label "scjentyficzny"
  ]
  node [
    id 211
    label "skomplikowany"
  ]
  node [
    id 212
    label "specjalistyczny"
  ]
  node [
    id 213
    label "zgodny"
  ]
  node [
    id 214
    label "specjalny"
  ]
  node [
    id 215
    label "nierealny"
  ]
  node [
    id 216
    label "teoretycznie"
  ]
  node [
    id 217
    label "zgodnie"
  ]
  node [
    id 218
    label "zbie&#380;ny"
  ]
  node [
    id 219
    label "spokojny"
  ]
  node [
    id 220
    label "dobry"
  ]
  node [
    id 221
    label "specjalistycznie"
  ]
  node [
    id 222
    label "fachowo"
  ]
  node [
    id 223
    label "fachowy"
  ]
  node [
    id 224
    label "intencjonalny"
  ]
  node [
    id 225
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 226
    label "niedorozw&#243;j"
  ]
  node [
    id 227
    label "szczeg&#243;lny"
  ]
  node [
    id 228
    label "specjalnie"
  ]
  node [
    id 229
    label "nieetatowy"
  ]
  node [
    id 230
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 231
    label "nienormalny"
  ]
  node [
    id 232
    label "umy&#347;lnie"
  ]
  node [
    id 233
    label "odpowiedni"
  ]
  node [
    id 234
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 235
    label "trudny"
  ]
  node [
    id 236
    label "skomplikowanie"
  ]
  node [
    id 237
    label "intelektualnie"
  ]
  node [
    id 238
    label "my&#347;l&#261;cy"
  ]
  node [
    id 239
    label "wznios&#322;y"
  ]
  node [
    id 240
    label "g&#322;&#281;boki"
  ]
  node [
    id 241
    label "umys&#322;owy"
  ]
  node [
    id 242
    label "inteligentny"
  ]
  node [
    id 243
    label "prawo_rzeczowe"
  ]
  node [
    id 244
    label "przej&#347;cie"
  ]
  node [
    id 245
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 246
    label "rodowo&#347;&#263;"
  ]
  node [
    id 247
    label "charakterystyka"
  ]
  node [
    id 248
    label "patent"
  ]
  node [
    id 249
    label "mienie"
  ]
  node [
    id 250
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 251
    label "dobra"
  ]
  node [
    id 252
    label "stan"
  ]
  node [
    id 253
    label "przej&#347;&#263;"
  ]
  node [
    id 254
    label "possession"
  ]
  node [
    id 255
    label "attribute"
  ]
  node [
    id 256
    label "Ohio"
  ]
  node [
    id 257
    label "wci&#281;cie"
  ]
  node [
    id 258
    label "Nowy_York"
  ]
  node [
    id 259
    label "warstwa"
  ]
  node [
    id 260
    label "samopoczucie"
  ]
  node [
    id 261
    label "Illinois"
  ]
  node [
    id 262
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 263
    label "state"
  ]
  node [
    id 264
    label "Jukatan"
  ]
  node [
    id 265
    label "Kalifornia"
  ]
  node [
    id 266
    label "Wirginia"
  ]
  node [
    id 267
    label "wektor"
  ]
  node [
    id 268
    label "Goa"
  ]
  node [
    id 269
    label "Teksas"
  ]
  node [
    id 270
    label "Waszyngton"
  ]
  node [
    id 271
    label "miejsce"
  ]
  node [
    id 272
    label "Massachusetts"
  ]
  node [
    id 273
    label "Alaska"
  ]
  node [
    id 274
    label "Arakan"
  ]
  node [
    id 275
    label "Hawaje"
  ]
  node [
    id 276
    label "Maryland"
  ]
  node [
    id 277
    label "punkt"
  ]
  node [
    id 278
    label "Michigan"
  ]
  node [
    id 279
    label "Arizona"
  ]
  node [
    id 280
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 281
    label "Georgia"
  ]
  node [
    id 282
    label "poziom"
  ]
  node [
    id 283
    label "Pensylwania"
  ]
  node [
    id 284
    label "shape"
  ]
  node [
    id 285
    label "Luizjana"
  ]
  node [
    id 286
    label "Nowy_Meksyk"
  ]
  node [
    id 287
    label "Alabama"
  ]
  node [
    id 288
    label "Kansas"
  ]
  node [
    id 289
    label "Oregon"
  ]
  node [
    id 290
    label "Oklahoma"
  ]
  node [
    id 291
    label "Floryda"
  ]
  node [
    id 292
    label "jednostka_administracyjna"
  ]
  node [
    id 293
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 294
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 295
    label "jednostka_monetarna"
  ]
  node [
    id 296
    label "centym"
  ]
  node [
    id 297
    label "Wilko"
  ]
  node [
    id 298
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 299
    label "frymark"
  ]
  node [
    id 300
    label "commodity"
  ]
  node [
    id 301
    label "ustawa"
  ]
  node [
    id 302
    label "podlec"
  ]
  node [
    id 303
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 304
    label "min&#261;&#263;"
  ]
  node [
    id 305
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 306
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 307
    label "zaliczy&#263;"
  ]
  node [
    id 308
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 309
    label "zmieni&#263;"
  ]
  node [
    id 310
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 311
    label "przeby&#263;"
  ]
  node [
    id 312
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 313
    label "die"
  ]
  node [
    id 314
    label "dozna&#263;"
  ]
  node [
    id 315
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 316
    label "zacz&#261;&#263;"
  ]
  node [
    id 317
    label "happen"
  ]
  node [
    id 318
    label "pass"
  ]
  node [
    id 319
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 320
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 321
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 322
    label "beat"
  ]
  node [
    id 323
    label "absorb"
  ]
  node [
    id 324
    label "przerobi&#263;"
  ]
  node [
    id 325
    label "pique"
  ]
  node [
    id 326
    label "przesta&#263;"
  ]
  node [
    id 327
    label "fideikomisarz"
  ]
  node [
    id 328
    label "rodow&#243;d"
  ]
  node [
    id 329
    label "obrysowa&#263;"
  ]
  node [
    id 330
    label "p&#281;d"
  ]
  node [
    id 331
    label "zarobi&#263;"
  ]
  node [
    id 332
    label "przypomnie&#263;"
  ]
  node [
    id 333
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 334
    label "perpetrate"
  ]
  node [
    id 335
    label "za&#347;piewa&#263;"
  ]
  node [
    id 336
    label "drag"
  ]
  node [
    id 337
    label "string"
  ]
  node [
    id 338
    label "wy&#322;udzi&#263;"
  ]
  node [
    id 339
    label "describe"
  ]
  node [
    id 340
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 341
    label "draw"
  ]
  node [
    id 342
    label "dane"
  ]
  node [
    id 343
    label "wypomnie&#263;"
  ]
  node [
    id 344
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 345
    label "nabra&#263;"
  ]
  node [
    id 346
    label "nak&#322;oni&#263;"
  ]
  node [
    id 347
    label "wydosta&#263;"
  ]
  node [
    id 348
    label "rozci&#261;gn&#261;&#263;"
  ]
  node [
    id 349
    label "remove"
  ]
  node [
    id 350
    label "zmusi&#263;"
  ]
  node [
    id 351
    label "pozyska&#263;"
  ]
  node [
    id 352
    label "zabra&#263;"
  ]
  node [
    id 353
    label "ocali&#263;"
  ]
  node [
    id 354
    label "rozprostowa&#263;"
  ]
  node [
    id 355
    label "wynalazek"
  ]
  node [
    id 356
    label "zezwolenie"
  ]
  node [
    id 357
    label "za&#347;wiadczenie"
  ]
  node [
    id 358
    label "spos&#243;b"
  ]
  node [
    id 359
    label "&#347;wiadectwo"
  ]
  node [
    id 360
    label "ochrona_patentowa"
  ]
  node [
    id 361
    label "pozwolenie"
  ]
  node [
    id 362
    label "wy&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 363
    label "akt"
  ]
  node [
    id 364
    label "pomys&#322;"
  ]
  node [
    id 365
    label "koncesja"
  ]
  node [
    id 366
    label "&#380;egluga"
  ]
  node [
    id 367
    label "autorstwo"
  ]
  node [
    id 368
    label "mini&#281;cie"
  ]
  node [
    id 369
    label "wymienienie"
  ]
  node [
    id 370
    label "zaliczenie"
  ]
  node [
    id 371
    label "traversal"
  ]
  node [
    id 372
    label "zdarzenie_si&#281;"
  ]
  node [
    id 373
    label "przewy&#380;szenie"
  ]
  node [
    id 374
    label "experience"
  ]
  node [
    id 375
    label "przepuszczenie"
  ]
  node [
    id 376
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 377
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 378
    label "strain"
  ]
  node [
    id 379
    label "faza"
  ]
  node [
    id 380
    label "przerobienie"
  ]
  node [
    id 381
    label "wydeptywanie"
  ]
  node [
    id 382
    label "crack"
  ]
  node [
    id 383
    label "wydeptanie"
  ]
  node [
    id 384
    label "wstawka"
  ]
  node [
    id 385
    label "prze&#380;ycie"
  ]
  node [
    id 386
    label "uznanie"
  ]
  node [
    id 387
    label "doznanie"
  ]
  node [
    id 388
    label "dostanie_si&#281;"
  ]
  node [
    id 389
    label "trwanie"
  ]
  node [
    id 390
    label "przebycie"
  ]
  node [
    id 391
    label "wytyczenie"
  ]
  node [
    id 392
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 393
    label "przepojenie"
  ]
  node [
    id 394
    label "nas&#261;czenie"
  ]
  node [
    id 395
    label "nale&#380;enie"
  ]
  node [
    id 396
    label "odmienienie"
  ]
  node [
    id 397
    label "przedostanie_si&#281;"
  ]
  node [
    id 398
    label "przemokni&#281;cie"
  ]
  node [
    id 399
    label "nasycenie_si&#281;"
  ]
  node [
    id 400
    label "zacz&#281;cie"
  ]
  node [
    id 401
    label "stanie_si&#281;"
  ]
  node [
    id 402
    label "offense"
  ]
  node [
    id 403
    label "przestanie"
  ]
  node [
    id 404
    label "wy&#322;udzenie"
  ]
  node [
    id 405
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 406
    label "rozprostowanie"
  ]
  node [
    id 407
    label "nabranie"
  ]
  node [
    id 408
    label "rozci&#261;gni&#281;cie"
  ]
  node [
    id 409
    label "zmuszenie"
  ]
  node [
    id 410
    label "powyci&#261;ganie"
  ]
  node [
    id 411
    label "obrysowanie"
  ]
  node [
    id 412
    label "pozyskanie"
  ]
  node [
    id 413
    label "nak&#322;onienie"
  ]
  node [
    id 414
    label "wypomnienie"
  ]
  node [
    id 415
    label "wyratowanie"
  ]
  node [
    id 416
    label "przemieszczenie"
  ]
  node [
    id 417
    label "za&#347;piewanie"
  ]
  node [
    id 418
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 419
    label "zarobienie"
  ]
  node [
    id 420
    label "powyjmowanie"
  ]
  node [
    id 421
    label "wydostanie"
  ]
  node [
    id 422
    label "dobycie"
  ]
  node [
    id 423
    label "zabranie"
  ]
  node [
    id 424
    label "przypomnienie"
  ]
  node [
    id 425
    label "opis"
  ]
  node [
    id 426
    label "parametr"
  ]
  node [
    id 427
    label "analiza"
  ]
  node [
    id 428
    label "specyfikacja"
  ]
  node [
    id 429
    label "wykres"
  ]
  node [
    id 430
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 431
    label "posta&#263;"
  ]
  node [
    id 432
    label "interpretacja"
  ]
  node [
    id 433
    label "inteligentnie"
  ]
  node [
    id 434
    label "g&#322;&#281;boko"
  ]
  node [
    id 435
    label "umys&#322;owo"
  ]
  node [
    id 436
    label "intensywny"
  ]
  node [
    id 437
    label "gruntowny"
  ]
  node [
    id 438
    label "mocny"
  ]
  node [
    id 439
    label "szczery"
  ]
  node [
    id 440
    label "ukryty"
  ]
  node [
    id 441
    label "silny"
  ]
  node [
    id 442
    label "wyrazisty"
  ]
  node [
    id 443
    label "daleki"
  ]
  node [
    id 444
    label "dog&#322;&#281;bny"
  ]
  node [
    id 445
    label "niezrozumia&#322;y"
  ]
  node [
    id 446
    label "niski"
  ]
  node [
    id 447
    label "m&#261;dry"
  ]
  node [
    id 448
    label "zmy&#347;lny"
  ]
  node [
    id 449
    label "wysokich_lot&#243;w"
  ]
  node [
    id 450
    label "szlachetny"
  ]
  node [
    id 451
    label "powa&#380;ny"
  ]
  node [
    id 452
    label "podnios&#322;y"
  ]
  node [
    id 453
    label "wznio&#347;le"
  ]
  node [
    id 454
    label "oderwany"
  ]
  node [
    id 455
    label "pi&#281;kny"
  ]
  node [
    id 456
    label "rozumnie"
  ]
  node [
    id 457
    label "przytomny"
  ]
  node [
    id 458
    label "jednostka_organizacyjna"
  ]
  node [
    id 459
    label "relation"
  ]
  node [
    id 460
    label "urz&#261;d"
  ]
  node [
    id 461
    label "miejsce_pracy"
  ]
  node [
    id 462
    label "podsekcja"
  ]
  node [
    id 463
    label "insourcing"
  ]
  node [
    id 464
    label "politechnika"
  ]
  node [
    id 465
    label "katedra"
  ]
  node [
    id 466
    label "ministerstwo"
  ]
  node [
    id 467
    label "dzia&#322;"
  ]
  node [
    id 468
    label "departament"
  ]
  node [
    id 469
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 470
    label "sfera"
  ]
  node [
    id 471
    label "zakres"
  ]
  node [
    id 472
    label "zesp&#243;&#322;"
  ]
  node [
    id 473
    label "wytw&#243;r"
  ]
  node [
    id 474
    label "column"
  ]
  node [
    id 475
    label "distribution"
  ]
  node [
    id 476
    label "stopie&#324;"
  ]
  node [
    id 477
    label "competence"
  ]
  node [
    id 478
    label "bezdro&#380;e"
  ]
  node [
    id 479
    label "poddzia&#322;"
  ]
  node [
    id 480
    label "tum"
  ]
  node [
    id 481
    label "pulpit"
  ]
  node [
    id 482
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 483
    label "tron"
  ]
  node [
    id 484
    label "NKWD"
  ]
  node [
    id 485
    label "ministerium"
  ]
  node [
    id 486
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 487
    label "MSW"
  ]
  node [
    id 488
    label "resort"
  ]
  node [
    id 489
    label "stanowisko"
  ]
  node [
    id 490
    label "position"
  ]
  node [
    id 491
    label "instytucja"
  ]
  node [
    id 492
    label "siedziba"
  ]
  node [
    id 493
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 494
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 495
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 496
    label "mianowaniec"
  ]
  node [
    id 497
    label "okienko"
  ]
  node [
    id 498
    label "w&#322;adza"
  ]
  node [
    id 499
    label "Stanford"
  ]
  node [
    id 500
    label "academy"
  ]
  node [
    id 501
    label "Harvard"
  ]
  node [
    id 502
    label "uczelnia"
  ]
  node [
    id 503
    label "Sorbona"
  ]
  node [
    id 504
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 505
    label "Princeton"
  ]
  node [
    id 506
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 507
    label "ku&#378;nia"
  ]
  node [
    id 508
    label "us&#322;uga"
  ]
  node [
    id 509
    label "biuro"
  ]
  node [
    id 510
    label "zarz&#261;d"
  ]
  node [
    id 511
    label "petent"
  ]
  node [
    id 512
    label "dziekanat"
  ]
  node [
    id 513
    label "gospodarka"
  ]
  node [
    id 514
    label "mechanika"
  ]
  node [
    id 515
    label "usenet"
  ]
  node [
    id 516
    label "rozprz&#261;c"
  ]
  node [
    id 517
    label "zachowanie"
  ]
  node [
    id 518
    label "cybernetyk"
  ]
  node [
    id 519
    label "podsystem"
  ]
  node [
    id 520
    label "system"
  ]
  node [
    id 521
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 522
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 523
    label "sk&#322;ad"
  ]
  node [
    id 524
    label "systemat"
  ]
  node [
    id 525
    label "konstrukcja"
  ]
  node [
    id 526
    label "konstelacja"
  ]
  node [
    id 527
    label "&#321;ubianka"
  ]
  node [
    id 528
    label "dzia&#322;_personalny"
  ]
  node [
    id 529
    label "Kreml"
  ]
  node [
    id 530
    label "Bia&#322;y_Dom"
  ]
  node [
    id 531
    label "budynek"
  ]
  node [
    id 532
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 533
    label "sadowisko"
  ]
  node [
    id 534
    label "s&#261;d"
  ]
  node [
    id 535
    label "boks"
  ]
  node [
    id 536
    label "biurko"
  ]
  node [
    id 537
    label "palestra"
  ]
  node [
    id 538
    label "Biuro_Lustracyjne"
  ]
  node [
    id 539
    label "agency"
  ]
  node [
    id 540
    label "board"
  ]
  node [
    id 541
    label "pomieszczenie"
  ]
  node [
    id 542
    label "organ_sp&#243;&#322;ki"
  ]
  node [
    id 543
    label "kierownictwo"
  ]
  node [
    id 544
    label "Bruksela"
  ]
  node [
    id 545
    label "administration"
  ]
  node [
    id 546
    label "centrala"
  ]
  node [
    id 547
    label "inwentarz"
  ]
  node [
    id 548
    label "rynek"
  ]
  node [
    id 549
    label "mieszkalnictwo"
  ]
  node [
    id 550
    label "agregat_ekonomiczny"
  ]
  node [
    id 551
    label "farmaceutyka"
  ]
  node [
    id 552
    label "produkowanie"
  ]
  node [
    id 553
    label "rolnictwo"
  ]
  node [
    id 554
    label "transport"
  ]
  node [
    id 555
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 556
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 557
    label "obronno&#347;&#263;"
  ]
  node [
    id 558
    label "sektor_prywatny"
  ]
  node [
    id 559
    label "sch&#322;adza&#263;"
  ]
  node [
    id 560
    label "czerwona_strefa"
  ]
  node [
    id 561
    label "pole"
  ]
  node [
    id 562
    label "sektor_publiczny"
  ]
  node [
    id 563
    label "bankowo&#347;&#263;"
  ]
  node [
    id 564
    label "gospodarowanie"
  ]
  node [
    id 565
    label "obora"
  ]
  node [
    id 566
    label "gospodarka_wodna"
  ]
  node [
    id 567
    label "gospodarka_le&#347;na"
  ]
  node [
    id 568
    label "gospodarowa&#263;"
  ]
  node [
    id 569
    label "fabryka"
  ]
  node [
    id 570
    label "wytw&#243;rnia"
  ]
  node [
    id 571
    label "stodo&#322;a"
  ]
  node [
    id 572
    label "przemys&#322;"
  ]
  node [
    id 573
    label "spichlerz"
  ]
  node [
    id 574
    label "sch&#322;adzanie"
  ]
  node [
    id 575
    label "sch&#322;odzenie"
  ]
  node [
    id 576
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 577
    label "zasada"
  ]
  node [
    id 578
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 579
    label "regulacja_cen"
  ]
  node [
    id 580
    label "szkolnictwo"
  ]
  node [
    id 581
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 582
    label "diecezja"
  ]
  node [
    id 583
    label "klient"
  ]
  node [
    id 584
    label "warsztat"
  ]
  node [
    id 585
    label "kanclerz"
  ]
  node [
    id 586
    label "szko&#322;a"
  ]
  node [
    id 587
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 588
    label "podkanclerz"
  ]
  node [
    id 589
    label "miasteczko_studenckie"
  ]
  node [
    id 590
    label "kwestura"
  ]
  node [
    id 591
    label "wyk&#322;adanie"
  ]
  node [
    id 592
    label "rektorat"
  ]
  node [
    id 593
    label "school"
  ]
  node [
    id 594
    label "senat"
  ]
  node [
    id 595
    label "promotorstwo"
  ]
  node [
    id 596
    label "paryski"
  ]
  node [
    id 597
    label "ameryka&#324;ski"
  ]
  node [
    id 598
    label "planowa&#263;"
  ]
  node [
    id 599
    label "dostosowywa&#263;"
  ]
  node [
    id 600
    label "treat"
  ]
  node [
    id 601
    label "pozyskiwa&#263;"
  ]
  node [
    id 602
    label "ensnare"
  ]
  node [
    id 603
    label "skupia&#263;"
  ]
  node [
    id 604
    label "create"
  ]
  node [
    id 605
    label "przygotowywa&#263;"
  ]
  node [
    id 606
    label "tworzy&#263;"
  ]
  node [
    id 607
    label "standard"
  ]
  node [
    id 608
    label "wprowadza&#263;"
  ]
  node [
    id 609
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 610
    label "wprawia&#263;"
  ]
  node [
    id 611
    label "zaczyna&#263;"
  ]
  node [
    id 612
    label "wpisywa&#263;"
  ]
  node [
    id 613
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 614
    label "wchodzi&#263;"
  ]
  node [
    id 615
    label "take"
  ]
  node [
    id 616
    label "zapoznawa&#263;"
  ]
  node [
    id 617
    label "powodowa&#263;"
  ]
  node [
    id 618
    label "inflict"
  ]
  node [
    id 619
    label "umieszcza&#263;"
  ]
  node [
    id 620
    label "schodzi&#263;"
  ]
  node [
    id 621
    label "induct"
  ]
  node [
    id 622
    label "begin"
  ]
  node [
    id 623
    label "doprowadza&#263;"
  ]
  node [
    id 624
    label "ognisko"
  ]
  node [
    id 625
    label "huddle"
  ]
  node [
    id 626
    label "zbiera&#263;"
  ]
  node [
    id 627
    label "masowa&#263;"
  ]
  node [
    id 628
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 629
    label "uzyskiwa&#263;"
  ]
  node [
    id 630
    label "wytwarza&#263;"
  ]
  node [
    id 631
    label "tease"
  ]
  node [
    id 632
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 633
    label "pope&#322;nia&#263;"
  ]
  node [
    id 634
    label "get"
  ]
  node [
    id 635
    label "consist"
  ]
  node [
    id 636
    label "stanowi&#263;"
  ]
  node [
    id 637
    label "raise"
  ]
  node [
    id 638
    label "sposobi&#263;"
  ]
  node [
    id 639
    label "usposabia&#263;"
  ]
  node [
    id 640
    label "train"
  ]
  node [
    id 641
    label "arrange"
  ]
  node [
    id 642
    label "szkoli&#263;"
  ]
  node [
    id 643
    label "wykonywa&#263;"
  ]
  node [
    id 644
    label "pryczy&#263;"
  ]
  node [
    id 645
    label "mean"
  ]
  node [
    id 646
    label "lot_&#347;lizgowy"
  ]
  node [
    id 647
    label "organize"
  ]
  node [
    id 648
    label "project"
  ]
  node [
    id 649
    label "my&#347;le&#263;"
  ]
  node [
    id 650
    label "volunteer"
  ]
  node [
    id 651
    label "opracowywa&#263;"
  ]
  node [
    id 652
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 653
    label "zmienia&#263;"
  ]
  node [
    id 654
    label "equal"
  ]
  node [
    id 655
    label "model"
  ]
  node [
    id 656
    label "ordinariness"
  ]
  node [
    id 657
    label "zorganizowa&#263;"
  ]
  node [
    id 658
    label "taniec_towarzyski"
  ]
  node [
    id 659
    label "organizowanie"
  ]
  node [
    id 660
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 661
    label "criterion"
  ]
  node [
    id 662
    label "zorganizowanie"
  ]
  node [
    id 663
    label "cover"
  ]
  node [
    id 664
    label "Ja&#322;ta"
  ]
  node [
    id 665
    label "spotkanie"
  ]
  node [
    id 666
    label "konferencyjka"
  ]
  node [
    id 667
    label "conference"
  ]
  node [
    id 668
    label "grusza_pospolita"
  ]
  node [
    id 669
    label "Poczdam"
  ]
  node [
    id 670
    label "gathering"
  ]
  node [
    id 671
    label "zawarcie"
  ]
  node [
    id 672
    label "wydarzenie"
  ]
  node [
    id 673
    label "znajomy"
  ]
  node [
    id 674
    label "powitanie"
  ]
  node [
    id 675
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 676
    label "spowodowanie"
  ]
  node [
    id 677
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 678
    label "znalezienie"
  ]
  node [
    id 679
    label "match"
  ]
  node [
    id 680
    label "employment"
  ]
  node [
    id 681
    label "po&#380;egnanie"
  ]
  node [
    id 682
    label "gather"
  ]
  node [
    id 683
    label "spotykanie"
  ]
  node [
    id 684
    label "spotkanie_si&#281;"
  ]
  node [
    id 685
    label "title"
  ]
  node [
    id 686
    label "nazwa&#263;"
  ]
  node [
    id 687
    label "broadcast"
  ]
  node [
    id 688
    label "nada&#263;"
  ]
  node [
    id 689
    label "okre&#347;li&#263;"
  ]
  node [
    id 690
    label "asymilowanie_si&#281;"
  ]
  node [
    id 691
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 692
    label "Wsch&#243;d"
  ]
  node [
    id 693
    label "praca_rolnicza"
  ]
  node [
    id 694
    label "przejmowanie"
  ]
  node [
    id 695
    label "zjawisko"
  ]
  node [
    id 696
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 697
    label "makrokosmos"
  ]
  node [
    id 698
    label "konwencja"
  ]
  node [
    id 699
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 700
    label "propriety"
  ]
  node [
    id 701
    label "przejmowa&#263;"
  ]
  node [
    id 702
    label "brzoskwiniarnia"
  ]
  node [
    id 703
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 704
    label "sztuka"
  ]
  node [
    id 705
    label "zwyczaj"
  ]
  node [
    id 706
    label "jako&#347;&#263;"
  ]
  node [
    id 707
    label "kuchnia"
  ]
  node [
    id 708
    label "tradycja"
  ]
  node [
    id 709
    label "populace"
  ]
  node [
    id 710
    label "hodowla"
  ]
  node [
    id 711
    label "religia"
  ]
  node [
    id 712
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 713
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 714
    label "przej&#281;cie"
  ]
  node [
    id 715
    label "przej&#261;&#263;"
  ]
  node [
    id 716
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 717
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 718
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 719
    label "warto&#347;&#263;"
  ]
  node [
    id 720
    label "quality"
  ]
  node [
    id 721
    label "co&#347;"
  ]
  node [
    id 722
    label "syf"
  ]
  node [
    id 723
    label "absolutorium"
  ]
  node [
    id 724
    label "dzia&#322;anie"
  ]
  node [
    id 725
    label "activity"
  ]
  node [
    id 726
    label "proces"
  ]
  node [
    id 727
    label "boski"
  ]
  node [
    id 728
    label "krajobraz"
  ]
  node [
    id 729
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 730
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 731
    label "przywidzenie"
  ]
  node [
    id 732
    label "presence"
  ]
  node [
    id 733
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 734
    label "potrzymanie"
  ]
  node [
    id 735
    label "pod&#243;j"
  ]
  node [
    id 736
    label "filiacja"
  ]
  node [
    id 737
    label "licencjonowanie"
  ]
  node [
    id 738
    label "opasa&#263;"
  ]
  node [
    id 739
    label "ch&#243;w"
  ]
  node [
    id 740
    label "licencja"
  ]
  node [
    id 741
    label "sokolarnia"
  ]
  node [
    id 742
    label "potrzyma&#263;"
  ]
  node [
    id 743
    label "rozp&#322;&#243;d"
  ]
  node [
    id 744
    label "grupa_organizm&#243;w"
  ]
  node [
    id 745
    label "wypas"
  ]
  node [
    id 746
    label "wychowalnia"
  ]
  node [
    id 747
    label "pstr&#261;garnia"
  ]
  node [
    id 748
    label "krzy&#380;owanie"
  ]
  node [
    id 749
    label "licencjonowa&#263;"
  ]
  node [
    id 750
    label "odch&#243;w"
  ]
  node [
    id 751
    label "tucz"
  ]
  node [
    id 752
    label "ud&#243;j"
  ]
  node [
    id 753
    label "klatka"
  ]
  node [
    id 754
    label "opasienie"
  ]
  node [
    id 755
    label "wych&#243;w"
  ]
  node [
    id 756
    label "obrz&#261;dek"
  ]
  node [
    id 757
    label "opasanie"
  ]
  node [
    id 758
    label "polish"
  ]
  node [
    id 759
    label "akwarium"
  ]
  node [
    id 760
    label "biotechnika"
  ]
  node [
    id 761
    label "m&#322;ot"
  ]
  node [
    id 762
    label "znak"
  ]
  node [
    id 763
    label "drzewo"
  ]
  node [
    id 764
    label "pr&#243;ba"
  ]
  node [
    id 765
    label "marka"
  ]
  node [
    id 766
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 767
    label "uk&#322;ad"
  ]
  node [
    id 768
    label "styl"
  ]
  node [
    id 769
    label "line"
  ]
  node [
    id 770
    label "kanon"
  ]
  node [
    id 771
    label "zjazd"
  ]
  node [
    id 772
    label "biom"
  ]
  node [
    id 773
    label "szata_ro&#347;linna"
  ]
  node [
    id 774
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 775
    label "formacja_ro&#347;linna"
  ]
  node [
    id 776
    label "przyroda"
  ]
  node [
    id 777
    label "zielono&#347;&#263;"
  ]
  node [
    id 778
    label "pi&#281;tro"
  ]
  node [
    id 779
    label "plant"
  ]
  node [
    id 780
    label "ro&#347;lina"
  ]
  node [
    id 781
    label "geosystem"
  ]
  node [
    id 782
    label "pr&#243;bowanie"
  ]
  node [
    id 783
    label "rola"
  ]
  node [
    id 784
    label "cz&#322;owiek"
  ]
  node [
    id 785
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 786
    label "realizacja"
  ]
  node [
    id 787
    label "scena"
  ]
  node [
    id 788
    label "didaskalia"
  ]
  node [
    id 789
    label "czyn"
  ]
  node [
    id 790
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 791
    label "environment"
  ]
  node [
    id 792
    label "head"
  ]
  node [
    id 793
    label "scenariusz"
  ]
  node [
    id 794
    label "jednostka"
  ]
  node [
    id 795
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 796
    label "utw&#243;r"
  ]
  node [
    id 797
    label "kultura_duchowa"
  ]
  node [
    id 798
    label "fortel"
  ]
  node [
    id 799
    label "theatrical_performance"
  ]
  node [
    id 800
    label "ambala&#380;"
  ]
  node [
    id 801
    label "sprawno&#347;&#263;"
  ]
  node [
    id 802
    label "kobieta"
  ]
  node [
    id 803
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 804
    label "Faust"
  ]
  node [
    id 805
    label "scenografia"
  ]
  node [
    id 806
    label "ods&#322;ona"
  ]
  node [
    id 807
    label "turn"
  ]
  node [
    id 808
    label "pokaz"
  ]
  node [
    id 809
    label "przedstawienie"
  ]
  node [
    id 810
    label "przedstawi&#263;"
  ]
  node [
    id 811
    label "Apollo"
  ]
  node [
    id 812
    label "przedstawianie"
  ]
  node [
    id 813
    label "przedstawia&#263;"
  ]
  node [
    id 814
    label "towar"
  ]
  node [
    id 815
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 816
    label "ceremony"
  ]
  node [
    id 817
    label "kult"
  ]
  node [
    id 818
    label "mitologia"
  ]
  node [
    id 819
    label "wyznanie"
  ]
  node [
    id 820
    label "ideologia"
  ]
  node [
    id 821
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 822
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 823
    label "nawracanie_si&#281;"
  ]
  node [
    id 824
    label "duchowny"
  ]
  node [
    id 825
    label "rela"
  ]
  node [
    id 826
    label "kosmologia"
  ]
  node [
    id 827
    label "kosmogonia"
  ]
  node [
    id 828
    label "nawraca&#263;"
  ]
  node [
    id 829
    label "mistyka"
  ]
  node [
    id 830
    label "staro&#347;cina_weselna"
  ]
  node [
    id 831
    label "folklor"
  ]
  node [
    id 832
    label "objawienie"
  ]
  node [
    id 833
    label "dorobek"
  ]
  node [
    id 834
    label "tworzenie"
  ]
  node [
    id 835
    label "kreacja"
  ]
  node [
    id 836
    label "creation"
  ]
  node [
    id 837
    label "zaj&#281;cie"
  ]
  node [
    id 838
    label "tajniki"
  ]
  node [
    id 839
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 840
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 841
    label "jedzenie"
  ]
  node [
    id 842
    label "zaplecze"
  ]
  node [
    id 843
    label "zlewozmywak"
  ]
  node [
    id 844
    label "gotowa&#263;"
  ]
  node [
    id 845
    label "ciemna_materia"
  ]
  node [
    id 846
    label "planeta"
  ]
  node [
    id 847
    label "mikrokosmos"
  ]
  node [
    id 848
    label "ekosfera"
  ]
  node [
    id 849
    label "przestrze&#324;"
  ]
  node [
    id 850
    label "czarna_dziura"
  ]
  node [
    id 851
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 852
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 853
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 854
    label "kosmos"
  ]
  node [
    id 855
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 856
    label "poprawno&#347;&#263;"
  ]
  node [
    id 857
    label "og&#322;ada"
  ]
  node [
    id 858
    label "service"
  ]
  node [
    id 859
    label "stosowno&#347;&#263;"
  ]
  node [
    id 860
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 861
    label "Ukraina"
  ]
  node [
    id 862
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 863
    label "blok_wschodni"
  ]
  node [
    id 864
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 865
    label "wsch&#243;d"
  ]
  node [
    id 866
    label "Europa_Wschodnia"
  ]
  node [
    id 867
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 868
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 869
    label "czerpa&#263;"
  ]
  node [
    id 870
    label "bra&#263;"
  ]
  node [
    id 871
    label "go"
  ]
  node [
    id 872
    label "handle"
  ]
  node [
    id 873
    label "wzbudza&#263;"
  ]
  node [
    id 874
    label "ogarnia&#263;"
  ]
  node [
    id 875
    label "bang"
  ]
  node [
    id 876
    label "wzi&#261;&#263;"
  ]
  node [
    id 877
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 878
    label "stimulate"
  ]
  node [
    id 879
    label "ogarn&#261;&#263;"
  ]
  node [
    id 880
    label "wzbudzi&#263;"
  ]
  node [
    id 881
    label "thrill"
  ]
  node [
    id 882
    label "czerpanie"
  ]
  node [
    id 883
    label "acquisition"
  ]
  node [
    id 884
    label "branie"
  ]
  node [
    id 885
    label "caparison"
  ]
  node [
    id 886
    label "movement"
  ]
  node [
    id 887
    label "wzbudzanie"
  ]
  node [
    id 888
    label "ogarnianie"
  ]
  node [
    id 889
    label "wra&#380;enie"
  ]
  node [
    id 890
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 891
    label "interception"
  ]
  node [
    id 892
    label "wzbudzenie"
  ]
  node [
    id 893
    label "emotion"
  ]
  node [
    id 894
    label "zaczerpni&#281;cie"
  ]
  node [
    id 895
    label "wzi&#281;cie"
  ]
  node [
    id 896
    label "object"
  ]
  node [
    id 897
    label "temat"
  ]
  node [
    id 898
    label "wpadni&#281;cie"
  ]
  node [
    id 899
    label "obiekt"
  ]
  node [
    id 900
    label "wpa&#347;&#263;"
  ]
  node [
    id 901
    label "wpadanie"
  ]
  node [
    id 902
    label "wpada&#263;"
  ]
  node [
    id 903
    label "uprawa"
  ]
  node [
    id 904
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 905
    label "mie&#263;_miejsce"
  ]
  node [
    id 906
    label "trwa&#263;"
  ]
  node [
    id 907
    label "chodzi&#263;"
  ]
  node [
    id 908
    label "si&#281;ga&#263;"
  ]
  node [
    id 909
    label "obecno&#347;&#263;"
  ]
  node [
    id 910
    label "stand"
  ]
  node [
    id 911
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 912
    label "uczestniczy&#263;"
  ]
  node [
    id 913
    label "participate"
  ]
  node [
    id 914
    label "istnie&#263;"
  ]
  node [
    id 915
    label "pozostawa&#263;"
  ]
  node [
    id 916
    label "zostawa&#263;"
  ]
  node [
    id 917
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 918
    label "adhere"
  ]
  node [
    id 919
    label "compass"
  ]
  node [
    id 920
    label "korzysta&#263;"
  ]
  node [
    id 921
    label "appreciation"
  ]
  node [
    id 922
    label "osi&#261;ga&#263;"
  ]
  node [
    id 923
    label "dociera&#263;"
  ]
  node [
    id 924
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 925
    label "mierzy&#263;"
  ]
  node [
    id 926
    label "u&#380;ywa&#263;"
  ]
  node [
    id 927
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 928
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 929
    label "exsert"
  ]
  node [
    id 930
    label "being"
  ]
  node [
    id 931
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 932
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 933
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 934
    label "p&#322;ywa&#263;"
  ]
  node [
    id 935
    label "run"
  ]
  node [
    id 936
    label "bangla&#263;"
  ]
  node [
    id 937
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 938
    label "przebiega&#263;"
  ]
  node [
    id 939
    label "wk&#322;ada&#263;"
  ]
  node [
    id 940
    label "proceed"
  ]
  node [
    id 941
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 942
    label "carry"
  ]
  node [
    id 943
    label "bywa&#263;"
  ]
  node [
    id 944
    label "dziama&#263;"
  ]
  node [
    id 945
    label "stara&#263;_si&#281;"
  ]
  node [
    id 946
    label "para"
  ]
  node [
    id 947
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 948
    label "str&#243;j"
  ]
  node [
    id 949
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 950
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 951
    label "krok"
  ]
  node [
    id 952
    label "tryb"
  ]
  node [
    id 953
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 954
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 955
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 956
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 957
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 958
    label "continue"
  ]
  node [
    id 959
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 960
    label "gajny"
  ]
  node [
    id 961
    label "legalnie"
  ]
  node [
    id 962
    label "gajowy"
  ]
  node [
    id 963
    label "legally"
  ]
  node [
    id 964
    label "Polish"
  ]
  node [
    id 965
    label "goniony"
  ]
  node [
    id 966
    label "oberek"
  ]
  node [
    id 967
    label "ryba_po_grecku"
  ]
  node [
    id 968
    label "sztajer"
  ]
  node [
    id 969
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 970
    label "krakowiak"
  ]
  node [
    id 971
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 972
    label "pierogi_ruskie"
  ]
  node [
    id 973
    label "lacki"
  ]
  node [
    id 974
    label "polak"
  ]
  node [
    id 975
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 976
    label "chodzony"
  ]
  node [
    id 977
    label "po_polsku"
  ]
  node [
    id 978
    label "mazur"
  ]
  node [
    id 979
    label "polsko"
  ]
  node [
    id 980
    label "skoczny"
  ]
  node [
    id 981
    label "drabant"
  ]
  node [
    id 982
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 983
    label "j&#281;zyk"
  ]
  node [
    id 984
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 985
    label "artykulator"
  ]
  node [
    id 986
    label "kod"
  ]
  node [
    id 987
    label "kawa&#322;ek"
  ]
  node [
    id 988
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 989
    label "gramatyka"
  ]
  node [
    id 990
    label "stylik"
  ]
  node [
    id 991
    label "przet&#322;umaczenie"
  ]
  node [
    id 992
    label "formalizowanie"
  ]
  node [
    id 993
    label "ssanie"
  ]
  node [
    id 994
    label "ssa&#263;"
  ]
  node [
    id 995
    label "language"
  ]
  node [
    id 996
    label "liza&#263;"
  ]
  node [
    id 997
    label "napisa&#263;"
  ]
  node [
    id 998
    label "konsonantyzm"
  ]
  node [
    id 999
    label "wokalizm"
  ]
  node [
    id 1000
    label "pisa&#263;"
  ]
  node [
    id 1001
    label "fonetyka"
  ]
  node [
    id 1002
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1003
    label "jeniec"
  ]
  node [
    id 1004
    label "but"
  ]
  node [
    id 1005
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1006
    label "po_koroniarsku"
  ]
  node [
    id 1007
    label "t&#322;umaczenie"
  ]
  node [
    id 1008
    label "m&#243;wienie"
  ]
  node [
    id 1009
    label "pype&#263;"
  ]
  node [
    id 1010
    label "lizanie"
  ]
  node [
    id 1011
    label "pismo"
  ]
  node [
    id 1012
    label "formalizowa&#263;"
  ]
  node [
    id 1013
    label "rozumie&#263;"
  ]
  node [
    id 1014
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1015
    label "rozumienie"
  ]
  node [
    id 1016
    label "makroglosja"
  ]
  node [
    id 1017
    label "m&#243;wi&#263;"
  ]
  node [
    id 1018
    label "jama_ustna"
  ]
  node [
    id 1019
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1020
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1021
    label "natural_language"
  ]
  node [
    id 1022
    label "s&#322;ownictwo"
  ]
  node [
    id 1023
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 1024
    label "wschodnioeuropejski"
  ]
  node [
    id 1025
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 1026
    label "poga&#324;ski"
  ]
  node [
    id 1027
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 1028
    label "topielec"
  ]
  node [
    id 1029
    label "europejski"
  ]
  node [
    id 1030
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 1031
    label "langosz"
  ]
  node [
    id 1032
    label "gwardzista"
  ]
  node [
    id 1033
    label "melodia"
  ]
  node [
    id 1034
    label "taniec_ludowy"
  ]
  node [
    id 1035
    label "&#347;redniowieczny"
  ]
  node [
    id 1036
    label "europejsko"
  ]
  node [
    id 1037
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 1038
    label "weso&#322;y"
  ]
  node [
    id 1039
    label "sprawny"
  ]
  node [
    id 1040
    label "rytmiczny"
  ]
  node [
    id 1041
    label "skocznie"
  ]
  node [
    id 1042
    label "energiczny"
  ]
  node [
    id 1043
    label "przytup"
  ]
  node [
    id 1044
    label "ho&#322;ubiec"
  ]
  node [
    id 1045
    label "wodzi&#263;"
  ]
  node [
    id 1046
    label "lendler"
  ]
  node [
    id 1047
    label "austriacki"
  ]
  node [
    id 1048
    label "polka"
  ]
  node [
    id 1049
    label "ludowy"
  ]
  node [
    id 1050
    label "pie&#347;&#324;"
  ]
  node [
    id 1051
    label "mieszkaniec"
  ]
  node [
    id 1052
    label "centu&#347;"
  ]
  node [
    id 1053
    label "lalka"
  ]
  node [
    id 1054
    label "Ma&#322;opolanin"
  ]
  node [
    id 1055
    label "krakauer"
  ]
  node [
    id 1056
    label "w&#322;asny"
  ]
  node [
    id 1057
    label "oryginalny"
  ]
  node [
    id 1058
    label "autorsko"
  ]
  node [
    id 1059
    label "niespotykany"
  ]
  node [
    id 1060
    label "o&#380;ywczy"
  ]
  node [
    id 1061
    label "ekscentryczny"
  ]
  node [
    id 1062
    label "nowy"
  ]
  node [
    id 1063
    label "oryginalnie"
  ]
  node [
    id 1064
    label "inny"
  ]
  node [
    id 1065
    label "pierwotny"
  ]
  node [
    id 1066
    label "prawdziwy"
  ]
  node [
    id 1067
    label "warto&#347;ciowy"
  ]
  node [
    id 1068
    label "samodzielny"
  ]
  node [
    id 1069
    label "zwi&#261;zany"
  ]
  node [
    id 1070
    label "czyj&#347;"
  ]
  node [
    id 1071
    label "swoisty"
  ]
  node [
    id 1072
    label "osobny"
  ]
  node [
    id 1073
    label "prawnie"
  ]
  node [
    id 1074
    label "indywidualnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 263
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 469
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 247
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 255
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 56
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 54
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 517
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 491
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 541
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 15
    target 848
  ]
  edge [
    source 15
    target 849
  ]
  edge [
    source 15
    target 850
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 852
  ]
  edge [
    source 15
    target 853
  ]
  edge [
    source 15
    target 854
  ]
  edge [
    source 15
    target 855
  ]
  edge [
    source 15
    target 856
  ]
  edge [
    source 15
    target 857
  ]
  edge [
    source 15
    target 858
  ]
  edge [
    source 15
    target 859
  ]
  edge [
    source 15
    target 860
  ]
  edge [
    source 15
    target 861
  ]
  edge [
    source 15
    target 862
  ]
  edge [
    source 15
    target 863
  ]
  edge [
    source 15
    target 864
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 249
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 271
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 252
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 634
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 256
  ]
  edge [
    source 16
    target 257
  ]
  edge [
    source 16
    target 258
  ]
  edge [
    source 16
    target 259
  ]
  edge [
    source 16
    target 260
  ]
  edge [
    source 16
    target 261
  ]
  edge [
    source 16
    target 262
  ]
  edge [
    source 16
    target 263
  ]
  edge [
    source 16
    target 264
  ]
  edge [
    source 16
    target 265
  ]
  edge [
    source 16
    target 266
  ]
  edge [
    source 16
    target 267
  ]
  edge [
    source 16
    target 268
  ]
  edge [
    source 16
    target 269
  ]
  edge [
    source 16
    target 270
  ]
  edge [
    source 16
    target 271
  ]
  edge [
    source 16
    target 272
  ]
  edge [
    source 16
    target 273
  ]
  edge [
    source 16
    target 274
  ]
  edge [
    source 16
    target 275
  ]
  edge [
    source 16
    target 276
  ]
  edge [
    source 16
    target 277
  ]
  edge [
    source 16
    target 278
  ]
  edge [
    source 16
    target 279
  ]
  edge [
    source 16
    target 280
  ]
  edge [
    source 16
    target 281
  ]
  edge [
    source 16
    target 282
  ]
  edge [
    source 16
    target 283
  ]
  edge [
    source 16
    target 284
  ]
  edge [
    source 16
    target 285
  ]
  edge [
    source 16
    target 286
  ]
  edge [
    source 16
    target 287
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 288
  ]
  edge [
    source 16
    target 289
  ]
  edge [
    source 16
    target 290
  ]
  edge [
    source 16
    target 291
  ]
  edge [
    source 16
    target 292
  ]
  edge [
    source 16
    target 293
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 358
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 73
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 75
  ]
  edge [
    source 18
    target 76
  ]
  edge [
    source 18
    target 77
  ]
  edge [
    source 18
    target 78
  ]
  edge [
    source 18
    target 79
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 18
    target 81
  ]
  edge [
    source 18
    target 82
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 18
    target 84
  ]
  edge [
    source 18
    target 85
  ]
  edge [
    source 18
    target 86
  ]
  edge [
    source 18
    target 87
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 18
    target 96
  ]
  edge [
    source 18
    target 97
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 18
    target 1039
  ]
  edge [
    source 18
    target 1040
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 20
    target 1058
  ]
  edge [
    source 20
    target 1059
  ]
  edge [
    source 20
    target 1060
  ]
  edge [
    source 20
    target 1061
  ]
  edge [
    source 20
    target 1062
  ]
  edge [
    source 20
    target 1063
  ]
  edge [
    source 20
    target 1064
  ]
  edge [
    source 20
    target 1065
  ]
  edge [
    source 20
    target 1066
  ]
  edge [
    source 20
    target 1067
  ]
  edge [
    source 20
    target 1068
  ]
  edge [
    source 20
    target 1069
  ]
  edge [
    source 20
    target 1070
  ]
  edge [
    source 20
    target 1071
  ]
  edge [
    source 20
    target 1072
  ]
  edge [
    source 20
    target 1073
  ]
  edge [
    source 20
    target 1074
  ]
]
