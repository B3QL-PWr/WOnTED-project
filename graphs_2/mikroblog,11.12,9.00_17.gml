graph [
  node [
    id 0
    label "joanna"
    origin "text"
  ]
  node [
    id 1
    label "j&#281;drzejczyk"
    origin "text"
  ]
  node [
    id 2
    label "rockowy"
    origin "text"
  ]
  node [
    id 3
    label "stylizacja"
    origin "text"
  ]
  node [
    id 4
    label "charakterystyczny"
  ]
  node [
    id 5
    label "nieklasyczny"
  ]
  node [
    id 6
    label "muzyczny"
  ]
  node [
    id 7
    label "rockowo"
  ]
  node [
    id 8
    label "charakterystycznie"
  ]
  node [
    id 9
    label "szczeg&#243;lny"
  ]
  node [
    id 10
    label "wyj&#261;tkowy"
  ]
  node [
    id 11
    label "typowy"
  ]
  node [
    id 12
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 13
    label "podobny"
  ]
  node [
    id 14
    label "klasyczny"
  ]
  node [
    id 15
    label "niestandardowo"
  ]
  node [
    id 16
    label "nietypowy"
  ]
  node [
    id 17
    label "nietradycyjny"
  ]
  node [
    id 18
    label "nieklasycznie"
  ]
  node [
    id 19
    label "uporz&#261;dkowany"
  ]
  node [
    id 20
    label "muzycznie"
  ]
  node [
    id 21
    label "artystyczny"
  ]
  node [
    id 22
    label "melodyjny"
  ]
  node [
    id 23
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 24
    label "wygl&#261;d"
  ]
  node [
    id 25
    label "na&#347;ladownictwo"
  ]
  node [
    id 26
    label "wypowied&#378;"
  ]
  node [
    id 27
    label "styl"
  ]
  node [
    id 28
    label "stylization"
  ]
  node [
    id 29
    label "otoczka"
  ]
  node [
    id 30
    label "modyfikacja"
  ]
  node [
    id 31
    label "imitacja"
  ]
  node [
    id 32
    label "zestawienie"
  ]
  node [
    id 33
    label "szafiarka"
  ]
  node [
    id 34
    label "trzonek"
  ]
  node [
    id 35
    label "reakcja"
  ]
  node [
    id 36
    label "narz&#281;dzie"
  ]
  node [
    id 37
    label "spos&#243;b"
  ]
  node [
    id 38
    label "zbi&#243;r"
  ]
  node [
    id 39
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 40
    label "zachowanie"
  ]
  node [
    id 41
    label "stylik"
  ]
  node [
    id 42
    label "dyscyplina_sportowa"
  ]
  node [
    id 43
    label "handle"
  ]
  node [
    id 44
    label "stroke"
  ]
  node [
    id 45
    label "line"
  ]
  node [
    id 46
    label "napisa&#263;"
  ]
  node [
    id 47
    label "charakter"
  ]
  node [
    id 48
    label "natural_language"
  ]
  node [
    id 49
    label "pisa&#263;"
  ]
  node [
    id 50
    label "kanon"
  ]
  node [
    id 51
    label "behawior"
  ]
  node [
    id 52
    label "model"
  ]
  node [
    id 53
    label "kopia"
  ]
  node [
    id 54
    label "okrywa"
  ]
  node [
    id 55
    label "kontekst"
  ]
  node [
    id 56
    label "postarzenie"
  ]
  node [
    id 57
    label "kszta&#322;t"
  ]
  node [
    id 58
    label "postarzanie"
  ]
  node [
    id 59
    label "brzydota"
  ]
  node [
    id 60
    label "portrecista"
  ]
  node [
    id 61
    label "postarza&#263;"
  ]
  node [
    id 62
    label "nadawanie"
  ]
  node [
    id 63
    label "postarzy&#263;"
  ]
  node [
    id 64
    label "cecha"
  ]
  node [
    id 65
    label "widok"
  ]
  node [
    id 66
    label "prostota"
  ]
  node [
    id 67
    label "ubarwienie"
  ]
  node [
    id 68
    label "shape"
  ]
  node [
    id 69
    label "sumariusz"
  ]
  node [
    id 70
    label "ustawienie"
  ]
  node [
    id 71
    label "z&#322;amanie"
  ]
  node [
    id 72
    label "kompozycja"
  ]
  node [
    id 73
    label "strata"
  ]
  node [
    id 74
    label "composition"
  ]
  node [
    id 75
    label "book"
  ]
  node [
    id 76
    label "informacja"
  ]
  node [
    id 77
    label "stock"
  ]
  node [
    id 78
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 79
    label "catalog"
  ]
  node [
    id 80
    label "z&#322;o&#380;enie"
  ]
  node [
    id 81
    label "sprawozdanie_finansowe"
  ]
  node [
    id 82
    label "figurowa&#263;"
  ]
  node [
    id 83
    label "z&#322;&#261;czenie"
  ]
  node [
    id 84
    label "count"
  ]
  node [
    id 85
    label "wyra&#380;enie"
  ]
  node [
    id 86
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 87
    label "wyliczanka"
  ]
  node [
    id 88
    label "set"
  ]
  node [
    id 89
    label "analiza"
  ]
  node [
    id 90
    label "deficyt"
  ]
  node [
    id 91
    label "obrot&#243;wka"
  ]
  node [
    id 92
    label "przedstawienie"
  ]
  node [
    id 93
    label "pozycja"
  ]
  node [
    id 94
    label "tekst"
  ]
  node [
    id 95
    label "comparison"
  ]
  node [
    id 96
    label "zanalizowanie"
  ]
  node [
    id 97
    label "technika"
  ]
  node [
    id 98
    label "praktyka"
  ]
  node [
    id 99
    label "dorobek"
  ]
  node [
    id 100
    label "tworzenie"
  ]
  node [
    id 101
    label "kreacja"
  ]
  node [
    id 102
    label "creation"
  ]
  node [
    id 103
    label "kultura"
  ]
  node [
    id 104
    label "modification"
  ]
  node [
    id 105
    label "wyraz_pochodny"
  ]
  node [
    id 106
    label "przer&#243;bka"
  ]
  node [
    id 107
    label "przystosowanie"
  ]
  node [
    id 108
    label "zmiana"
  ]
  node [
    id 109
    label "pos&#322;uchanie"
  ]
  node [
    id 110
    label "s&#261;d"
  ]
  node [
    id 111
    label "sparafrazowanie"
  ]
  node [
    id 112
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 113
    label "strawestowa&#263;"
  ]
  node [
    id 114
    label "sparafrazowa&#263;"
  ]
  node [
    id 115
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 116
    label "trawestowa&#263;"
  ]
  node [
    id 117
    label "sformu&#322;owanie"
  ]
  node [
    id 118
    label "parafrazowanie"
  ]
  node [
    id 119
    label "ozdobnik"
  ]
  node [
    id 120
    label "delimitacja"
  ]
  node [
    id 121
    label "parafrazowa&#263;"
  ]
  node [
    id 122
    label "komunikat"
  ]
  node [
    id 123
    label "trawestowanie"
  ]
  node [
    id 124
    label "strawestowanie"
  ]
  node [
    id 125
    label "rezultat"
  ]
  node [
    id 126
    label "blogerka"
  ]
  node [
    id 127
    label "fashionistka"
  ]
  node [
    id 128
    label "Joanna"
  ]
  node [
    id 129
    label "J&#281;drzejczyk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 128
    target 129
  ]
]
