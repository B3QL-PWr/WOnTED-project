graph [
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "sonda"
    origin "text"
  ]
  node [
    id 2
    label "voyager"
    origin "text"
  ]
  node [
    id 3
    label "opu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 4
    label "heliosfera"
    origin "text"
  ]
  node [
    id 5
    label "dotrze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przestrze&#324;"
    origin "text"
  ]
  node [
    id 7
    label "mi&#281;dzygwiezdny"
    origin "text"
  ]
  node [
    id 8
    label "tak"
    origin "text"
  ]
  node [
    id 9
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pora_roku"
  ]
  node [
    id 11
    label "badanie"
  ]
  node [
    id 12
    label "statek_kosmiczny"
  ]
  node [
    id 13
    label "narz&#281;dzie"
  ]
  node [
    id 14
    label "lead"
  ]
  node [
    id 15
    label "leash"
  ]
  node [
    id 16
    label "przyrz&#261;d"
  ]
  node [
    id 17
    label "sonda&#380;"
  ]
  node [
    id 18
    label "utensylia"
  ]
  node [
    id 19
    label "&#347;rodek"
  ]
  node [
    id 20
    label "niezb&#281;dnik"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "spos&#243;b"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 25
    label "tylec"
  ]
  node [
    id 26
    label "urz&#261;dzenie"
  ]
  node [
    id 27
    label "obserwowanie"
  ]
  node [
    id 28
    label "zrecenzowanie"
  ]
  node [
    id 29
    label "kontrola"
  ]
  node [
    id 30
    label "analysis"
  ]
  node [
    id 31
    label "rektalny"
  ]
  node [
    id 32
    label "ustalenie"
  ]
  node [
    id 33
    label "macanie"
  ]
  node [
    id 34
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 35
    label "usi&#322;owanie"
  ]
  node [
    id 36
    label "udowadnianie"
  ]
  node [
    id 37
    label "praca"
  ]
  node [
    id 38
    label "bia&#322;a_niedziela"
  ]
  node [
    id 39
    label "diagnostyka"
  ]
  node [
    id 40
    label "dociekanie"
  ]
  node [
    id 41
    label "rezultat"
  ]
  node [
    id 42
    label "sprawdzanie"
  ]
  node [
    id 43
    label "penetrowanie"
  ]
  node [
    id 44
    label "czynno&#347;&#263;"
  ]
  node [
    id 45
    label "krytykowanie"
  ]
  node [
    id 46
    label "omawianie"
  ]
  node [
    id 47
    label "ustalanie"
  ]
  node [
    id 48
    label "rozpatrywanie"
  ]
  node [
    id 49
    label "investigation"
  ]
  node [
    id 50
    label "wziernikowanie"
  ]
  node [
    id 51
    label "examination"
  ]
  node [
    id 52
    label "d&#243;&#322;"
  ]
  node [
    id 53
    label "artyku&#322;"
  ]
  node [
    id 54
    label "akapit"
  ]
  node [
    id 55
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 56
    label "pozostawi&#263;"
  ]
  node [
    id 57
    label "obni&#380;y&#263;"
  ]
  node [
    id 58
    label "zostawi&#263;"
  ]
  node [
    id 59
    label "przesta&#263;"
  ]
  node [
    id 60
    label "potani&#263;"
  ]
  node [
    id 61
    label "drop"
  ]
  node [
    id 62
    label "evacuate"
  ]
  node [
    id 63
    label "humiliate"
  ]
  node [
    id 64
    label "tekst"
  ]
  node [
    id 65
    label "leave"
  ]
  node [
    id 66
    label "straci&#263;"
  ]
  node [
    id 67
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 68
    label "authorize"
  ]
  node [
    id 69
    label "omin&#261;&#263;"
  ]
  node [
    id 70
    label "doprowadzi&#263;"
  ]
  node [
    id 71
    label "skrzywdzi&#263;"
  ]
  node [
    id 72
    label "shove"
  ]
  node [
    id 73
    label "wyda&#263;"
  ]
  node [
    id 74
    label "zaplanowa&#263;"
  ]
  node [
    id 75
    label "shelve"
  ]
  node [
    id 76
    label "zachowa&#263;"
  ]
  node [
    id 77
    label "impart"
  ]
  node [
    id 78
    label "da&#263;"
  ]
  node [
    id 79
    label "wyznaczy&#263;"
  ]
  node [
    id 80
    label "liszy&#263;"
  ]
  node [
    id 81
    label "zerwa&#263;"
  ]
  node [
    id 82
    label "spowodowa&#263;"
  ]
  node [
    id 83
    label "release"
  ]
  node [
    id 84
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 85
    label "przekaza&#263;"
  ]
  node [
    id 86
    label "stworzy&#263;"
  ]
  node [
    id 87
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 88
    label "zabra&#263;"
  ]
  node [
    id 89
    label "zrezygnowa&#263;"
  ]
  node [
    id 90
    label "permit"
  ]
  node [
    id 91
    label "stracenie"
  ]
  node [
    id 92
    label "leave_office"
  ]
  node [
    id 93
    label "zabi&#263;"
  ]
  node [
    id 94
    label "forfeit"
  ]
  node [
    id 95
    label "wytraci&#263;"
  ]
  node [
    id 96
    label "waste"
  ]
  node [
    id 97
    label "przegra&#263;"
  ]
  node [
    id 98
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 99
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 100
    label "execute"
  ]
  node [
    id 101
    label "pomin&#261;&#263;"
  ]
  node [
    id 102
    label "wymin&#261;&#263;"
  ]
  node [
    id 103
    label "sidestep"
  ]
  node [
    id 104
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 105
    label "unikn&#261;&#263;"
  ]
  node [
    id 106
    label "przej&#347;&#263;"
  ]
  node [
    id 107
    label "obej&#347;&#263;"
  ]
  node [
    id 108
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 109
    label "shed"
  ]
  node [
    id 110
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 111
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 112
    label "overhaul"
  ]
  node [
    id 113
    label "sink"
  ]
  node [
    id 114
    label "fall"
  ]
  node [
    id 115
    label "zmniejszy&#263;"
  ]
  node [
    id 116
    label "zabrzmie&#263;"
  ]
  node [
    id 117
    label "zmieni&#263;"
  ]
  node [
    id 118
    label "refuse"
  ]
  node [
    id 119
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 120
    label "coating"
  ]
  node [
    id 121
    label "sko&#324;czy&#263;"
  ]
  node [
    id 122
    label "fail"
  ]
  node [
    id 123
    label "dropiowate"
  ]
  node [
    id 124
    label "kania"
  ]
  node [
    id 125
    label "bustard"
  ]
  node [
    id 126
    label "ptak"
  ]
  node [
    id 127
    label "ekscerpcja"
  ]
  node [
    id 128
    label "j&#281;zykowo"
  ]
  node [
    id 129
    label "wypowied&#378;"
  ]
  node [
    id 130
    label "redakcja"
  ]
  node [
    id 131
    label "wytw&#243;r"
  ]
  node [
    id 132
    label "pomini&#281;cie"
  ]
  node [
    id 133
    label "dzie&#322;o"
  ]
  node [
    id 134
    label "preparacja"
  ]
  node [
    id 135
    label "odmianka"
  ]
  node [
    id 136
    label "koniektura"
  ]
  node [
    id 137
    label "pisa&#263;"
  ]
  node [
    id 138
    label "obelga"
  ]
  node [
    id 139
    label "strefa"
  ]
  node [
    id 140
    label "S&#322;o&#324;ce"
  ]
  node [
    id 141
    label "heliopauza"
  ]
  node [
    id 142
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 143
    label "obrona_strefowa"
  ]
  node [
    id 144
    label "obszar"
  ]
  node [
    id 145
    label "granica"
  ]
  node [
    id 146
    label "pochylanie_si&#281;"
  ]
  node [
    id 147
    label "pochyli&#263;_si&#281;"
  ]
  node [
    id 148
    label "pochyla&#263;_si&#281;"
  ]
  node [
    id 149
    label "apeks"
  ]
  node [
    id 150
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 151
    label "s&#322;o&#324;ce"
  ]
  node [
    id 152
    label "pochylenie_si&#281;"
  ]
  node [
    id 153
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 154
    label "aspekt"
  ]
  node [
    id 155
    label "czas_s&#322;oneczny"
  ]
  node [
    id 156
    label "utrze&#263;"
  ]
  node [
    id 157
    label "znale&#378;&#263;"
  ]
  node [
    id 158
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 159
    label "silnik"
  ]
  node [
    id 160
    label "catch"
  ]
  node [
    id 161
    label "dopasowa&#263;"
  ]
  node [
    id 162
    label "advance"
  ]
  node [
    id 163
    label "get"
  ]
  node [
    id 164
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 165
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 166
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 167
    label "dorobi&#263;"
  ]
  node [
    id 168
    label "become"
  ]
  node [
    id 169
    label "zarobi&#263;"
  ]
  node [
    id 170
    label "doda&#263;"
  ]
  node [
    id 171
    label "do&#322;o&#380;y&#263;"
  ]
  node [
    id 172
    label "wytworzy&#263;"
  ]
  node [
    id 173
    label "wymy&#347;li&#263;"
  ]
  node [
    id 174
    label "grate"
  ]
  node [
    id 175
    label "rozdrobni&#263;"
  ]
  node [
    id 176
    label "dostosowa&#263;"
  ]
  node [
    id 177
    label "przeszy&#263;"
  ]
  node [
    id 178
    label "scali&#263;"
  ]
  node [
    id 179
    label "zw&#281;zi&#263;"
  ]
  node [
    id 180
    label "accommodate"
  ]
  node [
    id 181
    label "match"
  ]
  node [
    id 182
    label "adjust"
  ]
  node [
    id 183
    label "act"
  ]
  node [
    id 184
    label "pozyska&#263;"
  ]
  node [
    id 185
    label "oceni&#263;"
  ]
  node [
    id 186
    label "devise"
  ]
  node [
    id 187
    label "dozna&#263;"
  ]
  node [
    id 188
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 189
    label "wykry&#263;"
  ]
  node [
    id 190
    label "odzyska&#263;"
  ]
  node [
    id 191
    label "znaj&#347;&#263;"
  ]
  node [
    id 192
    label "invent"
  ]
  node [
    id 193
    label "wyr&#243;wna&#263;"
  ]
  node [
    id 194
    label "udoskonali&#263;"
  ]
  node [
    id 195
    label "evening"
  ]
  node [
    id 196
    label "biblioteka"
  ]
  node [
    id 197
    label "radiator"
  ]
  node [
    id 198
    label "wyci&#261;garka"
  ]
  node [
    id 199
    label "gondola_silnikowa"
  ]
  node [
    id 200
    label "aerosanie"
  ]
  node [
    id 201
    label "podgrzewacz"
  ]
  node [
    id 202
    label "motogodzina"
  ]
  node [
    id 203
    label "motoszybowiec"
  ]
  node [
    id 204
    label "program"
  ]
  node [
    id 205
    label "gniazdo_zaworowe"
  ]
  node [
    id 206
    label "mechanizm"
  ]
  node [
    id 207
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 208
    label "dociera&#263;"
  ]
  node [
    id 209
    label "samoch&#243;d"
  ]
  node [
    id 210
    label "dotarcie"
  ]
  node [
    id 211
    label "nap&#281;d"
  ]
  node [
    id 212
    label "motor&#243;wka"
  ]
  node [
    id 213
    label "rz&#281;zi&#263;"
  ]
  node [
    id 214
    label "perpetuum_mobile"
  ]
  node [
    id 215
    label "docieranie"
  ]
  node [
    id 216
    label "bombowiec"
  ]
  node [
    id 217
    label "rz&#281;&#380;enie"
  ]
  node [
    id 218
    label "rozdzielanie"
  ]
  node [
    id 219
    label "bezbrze&#380;e"
  ]
  node [
    id 220
    label "punkt"
  ]
  node [
    id 221
    label "czasoprzestrze&#324;"
  ]
  node [
    id 222
    label "zbi&#243;r"
  ]
  node [
    id 223
    label "niezmierzony"
  ]
  node [
    id 224
    label "przedzielenie"
  ]
  node [
    id 225
    label "nielito&#347;ciwy"
  ]
  node [
    id 226
    label "rozdziela&#263;"
  ]
  node [
    id 227
    label "oktant"
  ]
  node [
    id 228
    label "miejsce"
  ]
  node [
    id 229
    label "przedzieli&#263;"
  ]
  node [
    id 230
    label "przestw&#243;r"
  ]
  node [
    id 231
    label "warunek_lokalowy"
  ]
  node [
    id 232
    label "plac"
  ]
  node [
    id 233
    label "location"
  ]
  node [
    id 234
    label "uwaga"
  ]
  node [
    id 235
    label "status"
  ]
  node [
    id 236
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 237
    label "chwila"
  ]
  node [
    id 238
    label "cia&#322;o"
  ]
  node [
    id 239
    label "cecha"
  ]
  node [
    id 240
    label "rz&#261;d"
  ]
  node [
    id 241
    label "po&#322;o&#380;enie"
  ]
  node [
    id 242
    label "sprawa"
  ]
  node [
    id 243
    label "ust&#281;p"
  ]
  node [
    id 244
    label "plan"
  ]
  node [
    id 245
    label "obiekt_matematyczny"
  ]
  node [
    id 246
    label "problemat"
  ]
  node [
    id 247
    label "plamka"
  ]
  node [
    id 248
    label "stopie&#324;_pisma"
  ]
  node [
    id 249
    label "jednostka"
  ]
  node [
    id 250
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 251
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 252
    label "mark"
  ]
  node [
    id 253
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 254
    label "prosta"
  ]
  node [
    id 255
    label "problematyka"
  ]
  node [
    id 256
    label "obiekt"
  ]
  node [
    id 257
    label "zapunktowa&#263;"
  ]
  node [
    id 258
    label "podpunkt"
  ]
  node [
    id 259
    label "wojsko"
  ]
  node [
    id 260
    label "kres"
  ]
  node [
    id 261
    label "point"
  ]
  node [
    id 262
    label "pozycja"
  ]
  node [
    id 263
    label "&#243;semka"
  ]
  node [
    id 264
    label "steradian"
  ]
  node [
    id 265
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 266
    label "octant"
  ]
  node [
    id 267
    label "przyrz&#261;d_nawigacyjny"
  ]
  node [
    id 268
    label "czas"
  ]
  node [
    id 269
    label "dzielenie"
  ]
  node [
    id 270
    label "rozprowadzanie"
  ]
  node [
    id 271
    label "oddzielanie"
  ]
  node [
    id 272
    label "odr&#243;&#380;nianie"
  ]
  node [
    id 273
    label "przek&#322;adanie"
  ]
  node [
    id 274
    label "rozdawanie"
  ]
  node [
    id 275
    label "division"
  ]
  node [
    id 276
    label "sk&#322;&#243;canie"
  ]
  node [
    id 277
    label "separation"
  ]
  node [
    id 278
    label "dissociation"
  ]
  node [
    id 279
    label "ogrom"
  ]
  node [
    id 280
    label "emocja"
  ]
  node [
    id 281
    label "wpa&#347;&#263;"
  ]
  node [
    id 282
    label "intensywno&#347;&#263;"
  ]
  node [
    id 283
    label "wpada&#263;"
  ]
  node [
    id 284
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 285
    label "disunion"
  ]
  node [
    id 286
    label "porozdzielanie"
  ]
  node [
    id 287
    label "oddzielenie"
  ]
  node [
    id 288
    label "podzielenie"
  ]
  node [
    id 289
    label "deal"
  ]
  node [
    id 290
    label "share"
  ]
  node [
    id 291
    label "cover"
  ]
  node [
    id 292
    label "sk&#322;&#243;ca&#263;"
  ]
  node [
    id 293
    label "odr&#243;&#380;nia&#263;"
  ]
  node [
    id 294
    label "dzieli&#263;"
  ]
  node [
    id 295
    label "rozdawa&#263;"
  ]
  node [
    id 296
    label "oddziela&#263;"
  ]
  node [
    id 297
    label "oddzieli&#263;"
  ]
  node [
    id 298
    label "part"
  ]
  node [
    id 299
    label "break"
  ]
  node [
    id 300
    label "podzieli&#263;"
  ]
  node [
    id 301
    label "nieograniczenie"
  ]
  node [
    id 302
    label "rozleg&#322;y"
  ]
  node [
    id 303
    label "otwarty"
  ]
  node [
    id 304
    label "straszny"
  ]
  node [
    id 305
    label "bezwzgl&#281;dny"
  ]
  node [
    id 306
    label "twardy"
  ]
  node [
    id 307
    label "bezlito&#347;ny"
  ]
  node [
    id 308
    label "okrutny"
  ]
  node [
    id 309
    label "niepob&#322;a&#380;liwy"
  ]
  node [
    id 310
    label "niemi&#322;osiernie"
  ]
  node [
    id 311
    label "nie&#322;askawy"
  ]
  node [
    id 312
    label "egzemplarz"
  ]
  node [
    id 313
    label "series"
  ]
  node [
    id 314
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 315
    label "uprawianie"
  ]
  node [
    id 316
    label "praca_rolnicza"
  ]
  node [
    id 317
    label "collection"
  ]
  node [
    id 318
    label "dane"
  ]
  node [
    id 319
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 320
    label "pakiet_klimatyczny"
  ]
  node [
    id 321
    label "poj&#281;cie"
  ]
  node [
    id 322
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 323
    label "sum"
  ]
  node [
    id 324
    label "gathering"
  ]
  node [
    id 325
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 326
    label "album"
  ]
  node [
    id 327
    label "post&#261;pi&#263;"
  ]
  node [
    id 328
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 329
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 330
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 331
    label "zorganizowa&#263;"
  ]
  node [
    id 332
    label "appoint"
  ]
  node [
    id 333
    label "wystylizowa&#263;"
  ]
  node [
    id 334
    label "cause"
  ]
  node [
    id 335
    label "przerobi&#263;"
  ]
  node [
    id 336
    label "nabra&#263;"
  ]
  node [
    id 337
    label "make"
  ]
  node [
    id 338
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 339
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 340
    label "wydali&#263;"
  ]
  node [
    id 341
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 342
    label "see"
  ]
  node [
    id 343
    label "usun&#261;&#263;"
  ]
  node [
    id 344
    label "sack"
  ]
  node [
    id 345
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 346
    label "restore"
  ]
  node [
    id 347
    label "stage"
  ]
  node [
    id 348
    label "urobi&#263;"
  ]
  node [
    id 349
    label "ensnare"
  ]
  node [
    id 350
    label "wprowadzi&#263;"
  ]
  node [
    id 351
    label "przygotowa&#263;"
  ]
  node [
    id 352
    label "standard"
  ]
  node [
    id 353
    label "skupi&#263;"
  ]
  node [
    id 354
    label "podbi&#263;"
  ]
  node [
    id 355
    label "umocni&#263;"
  ]
  node [
    id 356
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 357
    label "zadowoli&#263;"
  ]
  node [
    id 358
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 359
    label "zabezpieczy&#263;"
  ]
  node [
    id 360
    label "pomy&#347;le&#263;"
  ]
  node [
    id 361
    label "woda"
  ]
  node [
    id 362
    label "hoax"
  ]
  node [
    id 363
    label "deceive"
  ]
  node [
    id 364
    label "oszwabi&#263;"
  ]
  node [
    id 365
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 366
    label "objecha&#263;"
  ]
  node [
    id 367
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 368
    label "gull"
  ]
  node [
    id 369
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 370
    label "wzi&#261;&#263;"
  ]
  node [
    id 371
    label "naby&#263;"
  ]
  node [
    id 372
    label "fraud"
  ]
  node [
    id 373
    label "kupi&#263;"
  ]
  node [
    id 374
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 375
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 376
    label "stylize"
  ]
  node [
    id 377
    label "nada&#263;"
  ]
  node [
    id 378
    label "upodobni&#263;"
  ]
  node [
    id 379
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 380
    label "zaliczy&#263;"
  ]
  node [
    id 381
    label "overwork"
  ]
  node [
    id 382
    label "zamieni&#263;"
  ]
  node [
    id 383
    label "zmodyfikowa&#263;"
  ]
  node [
    id 384
    label "change"
  ]
  node [
    id 385
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 386
    label "convert"
  ]
  node [
    id 387
    label "prze&#380;y&#263;"
  ]
  node [
    id 388
    label "przetworzy&#263;"
  ]
  node [
    id 389
    label "upora&#263;_si&#281;"
  ]
  node [
    id 390
    label "sprawi&#263;"
  ]
  node [
    id 391
    label "Voyager"
  ]
  node [
    id 392
    label "2"
  ]
  node [
    id 393
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 391
    target 392
  ]
  edge [
    source 391
    target 393
  ]
]
