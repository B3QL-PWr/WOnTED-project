graph [
  node [
    id 0
    label "plan"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dobry"
    origin "text"
  ]
  node [
    id 3
    label "model"
  ]
  node [
    id 4
    label "intencja"
  ]
  node [
    id 5
    label "punkt"
  ]
  node [
    id 6
    label "rysunek"
  ]
  node [
    id 7
    label "miejsce_pracy"
  ]
  node [
    id 8
    label "przestrze&#324;"
  ]
  node [
    id 9
    label "wytw&#243;r"
  ]
  node [
    id 10
    label "device"
  ]
  node [
    id 11
    label "pomys&#322;"
  ]
  node [
    id 12
    label "obraz"
  ]
  node [
    id 13
    label "reprezentacja"
  ]
  node [
    id 14
    label "agreement"
  ]
  node [
    id 15
    label "dekoracja"
  ]
  node [
    id 16
    label "perspektywa"
  ]
  node [
    id 17
    label "zesp&#243;&#322;"
  ]
  node [
    id 18
    label "dru&#380;yna"
  ]
  node [
    id 19
    label "emblemat"
  ]
  node [
    id 20
    label "deputation"
  ]
  node [
    id 21
    label "kreska"
  ]
  node [
    id 22
    label "kszta&#322;t"
  ]
  node [
    id 23
    label "picture"
  ]
  node [
    id 24
    label "teka"
  ]
  node [
    id 25
    label "photograph"
  ]
  node [
    id 26
    label "ilustracja"
  ]
  node [
    id 27
    label "grafika"
  ]
  node [
    id 28
    label "plastyka"
  ]
  node [
    id 29
    label "shape"
  ]
  node [
    id 30
    label "spos&#243;b"
  ]
  node [
    id 31
    label "cz&#322;owiek"
  ]
  node [
    id 32
    label "prezenter"
  ]
  node [
    id 33
    label "typ"
  ]
  node [
    id 34
    label "mildew"
  ]
  node [
    id 35
    label "zi&#243;&#322;ko"
  ]
  node [
    id 36
    label "motif"
  ]
  node [
    id 37
    label "pozowanie"
  ]
  node [
    id 38
    label "ideal"
  ]
  node [
    id 39
    label "wz&#243;r"
  ]
  node [
    id 40
    label "matryca"
  ]
  node [
    id 41
    label "adaptation"
  ]
  node [
    id 42
    label "ruch"
  ]
  node [
    id 43
    label "pozowa&#263;"
  ]
  node [
    id 44
    label "imitacja"
  ]
  node [
    id 45
    label "orygina&#322;"
  ]
  node [
    id 46
    label "facet"
  ]
  node [
    id 47
    label "miniatura"
  ]
  node [
    id 48
    label "rozdzielanie"
  ]
  node [
    id 49
    label "bezbrze&#380;e"
  ]
  node [
    id 50
    label "czasoprzestrze&#324;"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "niezmierzony"
  ]
  node [
    id 53
    label "przedzielenie"
  ]
  node [
    id 54
    label "nielito&#347;ciwy"
  ]
  node [
    id 55
    label "rozdziela&#263;"
  ]
  node [
    id 56
    label "oktant"
  ]
  node [
    id 57
    label "miejsce"
  ]
  node [
    id 58
    label "przedzieli&#263;"
  ]
  node [
    id 59
    label "przestw&#243;r"
  ]
  node [
    id 60
    label "representation"
  ]
  node [
    id 61
    label "effigy"
  ]
  node [
    id 62
    label "podobrazie"
  ]
  node [
    id 63
    label "scena"
  ]
  node [
    id 64
    label "human_body"
  ]
  node [
    id 65
    label "projekcja"
  ]
  node [
    id 66
    label "oprawia&#263;"
  ]
  node [
    id 67
    label "zjawisko"
  ]
  node [
    id 68
    label "postprodukcja"
  ]
  node [
    id 69
    label "t&#322;o"
  ]
  node [
    id 70
    label "inning"
  ]
  node [
    id 71
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 72
    label "pulment"
  ]
  node [
    id 73
    label "pogl&#261;d"
  ]
  node [
    id 74
    label "plama_barwna"
  ]
  node [
    id 75
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 76
    label "oprawianie"
  ]
  node [
    id 77
    label "sztafa&#380;"
  ]
  node [
    id 78
    label "parkiet"
  ]
  node [
    id 79
    label "opinion"
  ]
  node [
    id 80
    label "uj&#281;cie"
  ]
  node [
    id 81
    label "zaj&#347;cie"
  ]
  node [
    id 82
    label "persona"
  ]
  node [
    id 83
    label "filmoteka"
  ]
  node [
    id 84
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 85
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 86
    label "ziarno"
  ]
  node [
    id 87
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 88
    label "wypunktowa&#263;"
  ]
  node [
    id 89
    label "ostro&#347;&#263;"
  ]
  node [
    id 90
    label "malarz"
  ]
  node [
    id 91
    label "napisy"
  ]
  node [
    id 92
    label "przeplot"
  ]
  node [
    id 93
    label "punktowa&#263;"
  ]
  node [
    id 94
    label "anamorfoza"
  ]
  node [
    id 95
    label "przedstawienie"
  ]
  node [
    id 96
    label "ty&#322;&#243;wka"
  ]
  node [
    id 97
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 98
    label "widok"
  ]
  node [
    id 99
    label "czo&#322;&#243;wka"
  ]
  node [
    id 100
    label "rola"
  ]
  node [
    id 101
    label "thinking"
  ]
  node [
    id 102
    label "pocz&#261;tki"
  ]
  node [
    id 103
    label "ukra&#347;&#263;"
  ]
  node [
    id 104
    label "ukradzenie"
  ]
  node [
    id 105
    label "idea"
  ]
  node [
    id 106
    label "system"
  ]
  node [
    id 107
    label "przedmiot"
  ]
  node [
    id 108
    label "p&#322;&#243;d"
  ]
  node [
    id 109
    label "work"
  ]
  node [
    id 110
    label "rezultat"
  ]
  node [
    id 111
    label "patrzenie"
  ]
  node [
    id 112
    label "figura_geometryczna"
  ]
  node [
    id 113
    label "dystans"
  ]
  node [
    id 114
    label "patrze&#263;"
  ]
  node [
    id 115
    label "decentracja"
  ]
  node [
    id 116
    label "anticipation"
  ]
  node [
    id 117
    label "krajobraz"
  ]
  node [
    id 118
    label "metoda"
  ]
  node [
    id 119
    label "expectation"
  ]
  node [
    id 120
    label "scene"
  ]
  node [
    id 121
    label "pojmowanie"
  ]
  node [
    id 122
    label "posta&#263;"
  ]
  node [
    id 123
    label "widzie&#263;"
  ]
  node [
    id 124
    label "prognoza"
  ]
  node [
    id 125
    label "tryb"
  ]
  node [
    id 126
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 127
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 128
    label "ferm"
  ]
  node [
    id 129
    label "upi&#281;kszanie"
  ]
  node [
    id 130
    label "adornment"
  ]
  node [
    id 131
    label "pi&#281;kniejszy"
  ]
  node [
    id 132
    label "sznurownia"
  ]
  node [
    id 133
    label "plan_zdj&#281;ciowy"
  ]
  node [
    id 134
    label "scenografia"
  ]
  node [
    id 135
    label "wystr&#243;j"
  ]
  node [
    id 136
    label "ozdoba"
  ]
  node [
    id 137
    label "po&#322;o&#380;enie"
  ]
  node [
    id 138
    label "sprawa"
  ]
  node [
    id 139
    label "ust&#281;p"
  ]
  node [
    id 140
    label "obiekt_matematyczny"
  ]
  node [
    id 141
    label "problemat"
  ]
  node [
    id 142
    label "plamka"
  ]
  node [
    id 143
    label "stopie&#324;_pisma"
  ]
  node [
    id 144
    label "jednostka"
  ]
  node [
    id 145
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 146
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 147
    label "mark"
  ]
  node [
    id 148
    label "chwila"
  ]
  node [
    id 149
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 150
    label "prosta"
  ]
  node [
    id 151
    label "problematyka"
  ]
  node [
    id 152
    label "obiekt"
  ]
  node [
    id 153
    label "zapunktowa&#263;"
  ]
  node [
    id 154
    label "podpunkt"
  ]
  node [
    id 155
    label "wojsko"
  ]
  node [
    id 156
    label "kres"
  ]
  node [
    id 157
    label "point"
  ]
  node [
    id 158
    label "pozycja"
  ]
  node [
    id 159
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 160
    label "mie&#263;_miejsce"
  ]
  node [
    id 161
    label "equal"
  ]
  node [
    id 162
    label "trwa&#263;"
  ]
  node [
    id 163
    label "chodzi&#263;"
  ]
  node [
    id 164
    label "si&#281;ga&#263;"
  ]
  node [
    id 165
    label "stan"
  ]
  node [
    id 166
    label "obecno&#347;&#263;"
  ]
  node [
    id 167
    label "stand"
  ]
  node [
    id 168
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 169
    label "uczestniczy&#263;"
  ]
  node [
    id 170
    label "participate"
  ]
  node [
    id 171
    label "robi&#263;"
  ]
  node [
    id 172
    label "istnie&#263;"
  ]
  node [
    id 173
    label "pozostawa&#263;"
  ]
  node [
    id 174
    label "zostawa&#263;"
  ]
  node [
    id 175
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 176
    label "adhere"
  ]
  node [
    id 177
    label "compass"
  ]
  node [
    id 178
    label "korzysta&#263;"
  ]
  node [
    id 179
    label "appreciation"
  ]
  node [
    id 180
    label "osi&#261;ga&#263;"
  ]
  node [
    id 181
    label "dociera&#263;"
  ]
  node [
    id 182
    label "get"
  ]
  node [
    id 183
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 184
    label "mierzy&#263;"
  ]
  node [
    id 185
    label "u&#380;ywa&#263;"
  ]
  node [
    id 186
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 187
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 188
    label "exsert"
  ]
  node [
    id 189
    label "being"
  ]
  node [
    id 190
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 191
    label "cecha"
  ]
  node [
    id 192
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 193
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 194
    label "p&#322;ywa&#263;"
  ]
  node [
    id 195
    label "run"
  ]
  node [
    id 196
    label "bangla&#263;"
  ]
  node [
    id 197
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 198
    label "przebiega&#263;"
  ]
  node [
    id 199
    label "wk&#322;ada&#263;"
  ]
  node [
    id 200
    label "proceed"
  ]
  node [
    id 201
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 202
    label "carry"
  ]
  node [
    id 203
    label "bywa&#263;"
  ]
  node [
    id 204
    label "dziama&#263;"
  ]
  node [
    id 205
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 206
    label "stara&#263;_si&#281;"
  ]
  node [
    id 207
    label "para"
  ]
  node [
    id 208
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 209
    label "str&#243;j"
  ]
  node [
    id 210
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 211
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 212
    label "krok"
  ]
  node [
    id 213
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 214
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 215
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 216
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 217
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 218
    label "continue"
  ]
  node [
    id 219
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 220
    label "Ohio"
  ]
  node [
    id 221
    label "wci&#281;cie"
  ]
  node [
    id 222
    label "Nowy_York"
  ]
  node [
    id 223
    label "warstwa"
  ]
  node [
    id 224
    label "samopoczucie"
  ]
  node [
    id 225
    label "Illinois"
  ]
  node [
    id 226
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 227
    label "state"
  ]
  node [
    id 228
    label "Jukatan"
  ]
  node [
    id 229
    label "Kalifornia"
  ]
  node [
    id 230
    label "Wirginia"
  ]
  node [
    id 231
    label "wektor"
  ]
  node [
    id 232
    label "Teksas"
  ]
  node [
    id 233
    label "Goa"
  ]
  node [
    id 234
    label "Waszyngton"
  ]
  node [
    id 235
    label "Massachusetts"
  ]
  node [
    id 236
    label "Alaska"
  ]
  node [
    id 237
    label "Arakan"
  ]
  node [
    id 238
    label "Hawaje"
  ]
  node [
    id 239
    label "Maryland"
  ]
  node [
    id 240
    label "Michigan"
  ]
  node [
    id 241
    label "Arizona"
  ]
  node [
    id 242
    label "Georgia"
  ]
  node [
    id 243
    label "poziom"
  ]
  node [
    id 244
    label "Pensylwania"
  ]
  node [
    id 245
    label "Luizjana"
  ]
  node [
    id 246
    label "Nowy_Meksyk"
  ]
  node [
    id 247
    label "Alabama"
  ]
  node [
    id 248
    label "ilo&#347;&#263;"
  ]
  node [
    id 249
    label "Kansas"
  ]
  node [
    id 250
    label "Oregon"
  ]
  node [
    id 251
    label "Floryda"
  ]
  node [
    id 252
    label "Oklahoma"
  ]
  node [
    id 253
    label "jednostka_administracyjna"
  ]
  node [
    id 254
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 255
    label "dobroczynny"
  ]
  node [
    id 256
    label "czw&#243;rka"
  ]
  node [
    id 257
    label "spokojny"
  ]
  node [
    id 258
    label "skuteczny"
  ]
  node [
    id 259
    label "&#347;mieszny"
  ]
  node [
    id 260
    label "mi&#322;y"
  ]
  node [
    id 261
    label "grzeczny"
  ]
  node [
    id 262
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 263
    label "powitanie"
  ]
  node [
    id 264
    label "dobrze"
  ]
  node [
    id 265
    label "ca&#322;y"
  ]
  node [
    id 266
    label "zwrot"
  ]
  node [
    id 267
    label "pomy&#347;lny"
  ]
  node [
    id 268
    label "moralny"
  ]
  node [
    id 269
    label "drogi"
  ]
  node [
    id 270
    label "pozytywny"
  ]
  node [
    id 271
    label "odpowiedni"
  ]
  node [
    id 272
    label "korzystny"
  ]
  node [
    id 273
    label "pos&#322;uszny"
  ]
  node [
    id 274
    label "moralnie"
  ]
  node [
    id 275
    label "warto&#347;ciowy"
  ]
  node [
    id 276
    label "etycznie"
  ]
  node [
    id 277
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 278
    label "nale&#380;ny"
  ]
  node [
    id 279
    label "nale&#380;yty"
  ]
  node [
    id 280
    label "typowy"
  ]
  node [
    id 281
    label "uprawniony"
  ]
  node [
    id 282
    label "zasadniczy"
  ]
  node [
    id 283
    label "stosownie"
  ]
  node [
    id 284
    label "taki"
  ]
  node [
    id 285
    label "charakterystyczny"
  ]
  node [
    id 286
    label "prawdziwy"
  ]
  node [
    id 287
    label "ten"
  ]
  node [
    id 288
    label "pozytywnie"
  ]
  node [
    id 289
    label "fajny"
  ]
  node [
    id 290
    label "dodatnio"
  ]
  node [
    id 291
    label "przyjemny"
  ]
  node [
    id 292
    label "po&#380;&#261;dany"
  ]
  node [
    id 293
    label "niepowa&#380;ny"
  ]
  node [
    id 294
    label "o&#347;mieszanie"
  ]
  node [
    id 295
    label "&#347;miesznie"
  ]
  node [
    id 296
    label "bawny"
  ]
  node [
    id 297
    label "o&#347;mieszenie"
  ]
  node [
    id 298
    label "dziwny"
  ]
  node [
    id 299
    label "nieadekwatny"
  ]
  node [
    id 300
    label "wolny"
  ]
  node [
    id 301
    label "uspokajanie_si&#281;"
  ]
  node [
    id 302
    label "bezproblemowy"
  ]
  node [
    id 303
    label "spokojnie"
  ]
  node [
    id 304
    label "uspokojenie_si&#281;"
  ]
  node [
    id 305
    label "cicho"
  ]
  node [
    id 306
    label "uspokojenie"
  ]
  node [
    id 307
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 308
    label "nietrudny"
  ]
  node [
    id 309
    label "uspokajanie"
  ]
  node [
    id 310
    label "zale&#380;ny"
  ]
  node [
    id 311
    label "uleg&#322;y"
  ]
  node [
    id 312
    label "pos&#322;usznie"
  ]
  node [
    id 313
    label "grzecznie"
  ]
  node [
    id 314
    label "stosowny"
  ]
  node [
    id 315
    label "niewinny"
  ]
  node [
    id 316
    label "konserwatywny"
  ]
  node [
    id 317
    label "nijaki"
  ]
  node [
    id 318
    label "korzystnie"
  ]
  node [
    id 319
    label "drogo"
  ]
  node [
    id 320
    label "bliski"
  ]
  node [
    id 321
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 322
    label "przyjaciel"
  ]
  node [
    id 323
    label "jedyny"
  ]
  node [
    id 324
    label "du&#380;y"
  ]
  node [
    id 325
    label "zdr&#243;w"
  ]
  node [
    id 326
    label "calu&#347;ko"
  ]
  node [
    id 327
    label "kompletny"
  ]
  node [
    id 328
    label "&#380;ywy"
  ]
  node [
    id 329
    label "pe&#322;ny"
  ]
  node [
    id 330
    label "podobny"
  ]
  node [
    id 331
    label "ca&#322;o"
  ]
  node [
    id 332
    label "poskutkowanie"
  ]
  node [
    id 333
    label "sprawny"
  ]
  node [
    id 334
    label "skutecznie"
  ]
  node [
    id 335
    label "skutkowanie"
  ]
  node [
    id 336
    label "pomy&#347;lnie"
  ]
  node [
    id 337
    label "toto-lotek"
  ]
  node [
    id 338
    label "trafienie"
  ]
  node [
    id 339
    label "arkusz_drukarski"
  ]
  node [
    id 340
    label "&#322;&#243;dka"
  ]
  node [
    id 341
    label "four"
  ]
  node [
    id 342
    label "&#263;wiartka"
  ]
  node [
    id 343
    label "hotel"
  ]
  node [
    id 344
    label "cyfra"
  ]
  node [
    id 345
    label "pok&#243;j"
  ]
  node [
    id 346
    label "stopie&#324;"
  ]
  node [
    id 347
    label "minialbum"
  ]
  node [
    id 348
    label "osada"
  ]
  node [
    id 349
    label "p&#322;yta_winylowa"
  ]
  node [
    id 350
    label "blotka"
  ]
  node [
    id 351
    label "zaprz&#281;g"
  ]
  node [
    id 352
    label "przedtrzonowiec"
  ]
  node [
    id 353
    label "turn"
  ]
  node [
    id 354
    label "turning"
  ]
  node [
    id 355
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 356
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 357
    label "skr&#281;t"
  ]
  node [
    id 358
    label "obr&#243;t"
  ]
  node [
    id 359
    label "fraza_czasownikowa"
  ]
  node [
    id 360
    label "jednostka_leksykalna"
  ]
  node [
    id 361
    label "zmiana"
  ]
  node [
    id 362
    label "wyra&#380;enie"
  ]
  node [
    id 363
    label "welcome"
  ]
  node [
    id 364
    label "spotkanie"
  ]
  node [
    id 365
    label "pozdrowienie"
  ]
  node [
    id 366
    label "zwyczaj"
  ]
  node [
    id 367
    label "greeting"
  ]
  node [
    id 368
    label "zdarzony"
  ]
  node [
    id 369
    label "odpowiednio"
  ]
  node [
    id 370
    label "odpowiadanie"
  ]
  node [
    id 371
    label "specjalny"
  ]
  node [
    id 372
    label "kochanek"
  ]
  node [
    id 373
    label "sk&#322;onny"
  ]
  node [
    id 374
    label "wybranek"
  ]
  node [
    id 375
    label "umi&#322;owany"
  ]
  node [
    id 376
    label "przyjemnie"
  ]
  node [
    id 377
    label "mi&#322;o"
  ]
  node [
    id 378
    label "kochanie"
  ]
  node [
    id 379
    label "dyplomata"
  ]
  node [
    id 380
    label "dobroczynnie"
  ]
  node [
    id 381
    label "lepiej"
  ]
  node [
    id 382
    label "wiele"
  ]
  node [
    id 383
    label "spo&#322;eczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
]
