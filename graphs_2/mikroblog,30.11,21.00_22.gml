graph [
  node [
    id 0
    label "wczoraj"
    origin "text"
  ]
  node [
    id 1
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "rozmowa"
    origin "text"
  ]
  node [
    id 3
    label "szef"
    origin "text"
  ]
  node [
    id 4
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 5
    label "wypowiedzenie"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "z&#322;o&#380;y&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dobra"
    origin "text"
  ]
  node [
    id 9
    label "ile"
    origin "text"
  ]
  node [
    id 10
    label "dawno"
  ]
  node [
    id 11
    label "doba"
  ]
  node [
    id 12
    label "niedawno"
  ]
  node [
    id 13
    label "aktualnie"
  ]
  node [
    id 14
    label "ostatni"
  ]
  node [
    id 15
    label "dawny"
  ]
  node [
    id 16
    label "d&#322;ugotrwale"
  ]
  node [
    id 17
    label "wcze&#347;niej"
  ]
  node [
    id 18
    label "ongi&#347;"
  ]
  node [
    id 19
    label "dawnie"
  ]
  node [
    id 20
    label "tydzie&#324;"
  ]
  node [
    id 21
    label "noc"
  ]
  node [
    id 22
    label "dzie&#324;"
  ]
  node [
    id 23
    label "czas"
  ]
  node [
    id 24
    label "godzina"
  ]
  node [
    id 25
    label "long_time"
  ]
  node [
    id 26
    label "jednostka_geologiczna"
  ]
  node [
    id 27
    label "proszek"
  ]
  node [
    id 28
    label "tablet"
  ]
  node [
    id 29
    label "dawka"
  ]
  node [
    id 30
    label "blister"
  ]
  node [
    id 31
    label "lekarstwo"
  ]
  node [
    id 32
    label "cecha"
  ]
  node [
    id 33
    label "cisza"
  ]
  node [
    id 34
    label "odpowied&#378;"
  ]
  node [
    id 35
    label "rozhowor"
  ]
  node [
    id 36
    label "discussion"
  ]
  node [
    id 37
    label "czynno&#347;&#263;"
  ]
  node [
    id 38
    label "activity"
  ]
  node [
    id 39
    label "bezproblemowy"
  ]
  node [
    id 40
    label "wydarzenie"
  ]
  node [
    id 41
    label "g&#322;adki"
  ]
  node [
    id 42
    label "cicha_praca"
  ]
  node [
    id 43
    label "przerwa"
  ]
  node [
    id 44
    label "cicha_msza"
  ]
  node [
    id 45
    label "pok&#243;j"
  ]
  node [
    id 46
    label "motionlessness"
  ]
  node [
    id 47
    label "spok&#243;j"
  ]
  node [
    id 48
    label "zjawisko"
  ]
  node [
    id 49
    label "ci&#261;g"
  ]
  node [
    id 50
    label "tajemno&#347;&#263;"
  ]
  node [
    id 51
    label "peace"
  ]
  node [
    id 52
    label "cicha_modlitwa"
  ]
  node [
    id 53
    label "react"
  ]
  node [
    id 54
    label "replica"
  ]
  node [
    id 55
    label "wyj&#347;cie"
  ]
  node [
    id 56
    label "respondent"
  ]
  node [
    id 57
    label "dokument"
  ]
  node [
    id 58
    label "reakcja"
  ]
  node [
    id 59
    label "pryncypa&#322;"
  ]
  node [
    id 60
    label "kierownictwo"
  ]
  node [
    id 61
    label "kierowa&#263;"
  ]
  node [
    id 62
    label "cz&#322;owiek"
  ]
  node [
    id 63
    label "zwrot"
  ]
  node [
    id 64
    label "punkt"
  ]
  node [
    id 65
    label "turn"
  ]
  node [
    id 66
    label "turning"
  ]
  node [
    id 67
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 68
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 69
    label "skr&#281;t"
  ]
  node [
    id 70
    label "obr&#243;t"
  ]
  node [
    id 71
    label "fraza_czasownikowa"
  ]
  node [
    id 72
    label "jednostka_leksykalna"
  ]
  node [
    id 73
    label "zmiana"
  ]
  node [
    id 74
    label "wyra&#380;enie"
  ]
  node [
    id 75
    label "ludzko&#347;&#263;"
  ]
  node [
    id 76
    label "asymilowanie"
  ]
  node [
    id 77
    label "wapniak"
  ]
  node [
    id 78
    label "asymilowa&#263;"
  ]
  node [
    id 79
    label "os&#322;abia&#263;"
  ]
  node [
    id 80
    label "posta&#263;"
  ]
  node [
    id 81
    label "hominid"
  ]
  node [
    id 82
    label "podw&#322;adny"
  ]
  node [
    id 83
    label "os&#322;abianie"
  ]
  node [
    id 84
    label "g&#322;owa"
  ]
  node [
    id 85
    label "figura"
  ]
  node [
    id 86
    label "portrecista"
  ]
  node [
    id 87
    label "dwun&#243;g"
  ]
  node [
    id 88
    label "profanum"
  ]
  node [
    id 89
    label "mikrokosmos"
  ]
  node [
    id 90
    label "nasada"
  ]
  node [
    id 91
    label "duch"
  ]
  node [
    id 92
    label "antropochoria"
  ]
  node [
    id 93
    label "osoba"
  ]
  node [
    id 94
    label "wz&#243;r"
  ]
  node [
    id 95
    label "senior"
  ]
  node [
    id 96
    label "oddzia&#322;ywanie"
  ]
  node [
    id 97
    label "Adam"
  ]
  node [
    id 98
    label "homo_sapiens"
  ]
  node [
    id 99
    label "polifag"
  ]
  node [
    id 100
    label "biuro"
  ]
  node [
    id 101
    label "lead"
  ]
  node [
    id 102
    label "zesp&#243;&#322;"
  ]
  node [
    id 103
    label "siedziba"
  ]
  node [
    id 104
    label "praca"
  ]
  node [
    id 105
    label "w&#322;adza"
  ]
  node [
    id 106
    label "g&#322;os"
  ]
  node [
    id 107
    label "zwierzchnik"
  ]
  node [
    id 108
    label "sterowa&#263;"
  ]
  node [
    id 109
    label "wysy&#322;a&#263;"
  ]
  node [
    id 110
    label "manipulate"
  ]
  node [
    id 111
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 112
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 113
    label "ustawia&#263;"
  ]
  node [
    id 114
    label "give"
  ]
  node [
    id 115
    label "przeznacza&#263;"
  ]
  node [
    id 116
    label "control"
  ]
  node [
    id 117
    label "match"
  ]
  node [
    id 118
    label "motywowa&#263;"
  ]
  node [
    id 119
    label "administrowa&#263;"
  ]
  node [
    id 120
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 121
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 122
    label "order"
  ]
  node [
    id 123
    label "indicate"
  ]
  node [
    id 124
    label "odwadnia&#263;"
  ]
  node [
    id 125
    label "wi&#261;zanie"
  ]
  node [
    id 126
    label "odwodni&#263;"
  ]
  node [
    id 127
    label "bratnia_dusza"
  ]
  node [
    id 128
    label "powi&#261;zanie"
  ]
  node [
    id 129
    label "zwi&#261;zanie"
  ]
  node [
    id 130
    label "konstytucja"
  ]
  node [
    id 131
    label "organizacja"
  ]
  node [
    id 132
    label "marriage"
  ]
  node [
    id 133
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 134
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 135
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 136
    label "zwi&#261;za&#263;"
  ]
  node [
    id 137
    label "odwadnianie"
  ]
  node [
    id 138
    label "odwodnienie"
  ]
  node [
    id 139
    label "marketing_afiliacyjny"
  ]
  node [
    id 140
    label "substancja_chemiczna"
  ]
  node [
    id 141
    label "koligacja"
  ]
  node [
    id 142
    label "bearing"
  ]
  node [
    id 143
    label "lokant"
  ]
  node [
    id 144
    label "azeotrop"
  ]
  node [
    id 145
    label "odprowadza&#263;"
  ]
  node [
    id 146
    label "drain"
  ]
  node [
    id 147
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 148
    label "cia&#322;o"
  ]
  node [
    id 149
    label "powodowa&#263;"
  ]
  node [
    id 150
    label "osusza&#263;"
  ]
  node [
    id 151
    label "odci&#261;ga&#263;"
  ]
  node [
    id 152
    label "odsuwa&#263;"
  ]
  node [
    id 153
    label "struktura"
  ]
  node [
    id 154
    label "zbi&#243;r"
  ]
  node [
    id 155
    label "akt"
  ]
  node [
    id 156
    label "cezar"
  ]
  node [
    id 157
    label "budowa"
  ]
  node [
    id 158
    label "uchwa&#322;a"
  ]
  node [
    id 159
    label "numeracja"
  ]
  node [
    id 160
    label "odprowadzanie"
  ]
  node [
    id 161
    label "powodowanie"
  ]
  node [
    id 162
    label "odci&#261;ganie"
  ]
  node [
    id 163
    label "dehydratacja"
  ]
  node [
    id 164
    label "osuszanie"
  ]
  node [
    id 165
    label "proces_chemiczny"
  ]
  node [
    id 166
    label "odsuwanie"
  ]
  node [
    id 167
    label "odsun&#261;&#263;"
  ]
  node [
    id 168
    label "spowodowa&#263;"
  ]
  node [
    id 169
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 170
    label "odprowadzi&#263;"
  ]
  node [
    id 171
    label "osuszy&#263;"
  ]
  node [
    id 172
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 173
    label "dehydration"
  ]
  node [
    id 174
    label "oznaka"
  ]
  node [
    id 175
    label "osuszenie"
  ]
  node [
    id 176
    label "spowodowanie"
  ]
  node [
    id 177
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 178
    label "odprowadzenie"
  ]
  node [
    id 179
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 180
    label "odsuni&#281;cie"
  ]
  node [
    id 181
    label "narta"
  ]
  node [
    id 182
    label "przedmiot"
  ]
  node [
    id 183
    label "podwi&#261;zywanie"
  ]
  node [
    id 184
    label "dressing"
  ]
  node [
    id 185
    label "socket"
  ]
  node [
    id 186
    label "szermierka"
  ]
  node [
    id 187
    label "przywi&#261;zywanie"
  ]
  node [
    id 188
    label "pakowanie"
  ]
  node [
    id 189
    label "my&#347;lenie"
  ]
  node [
    id 190
    label "do&#322;&#261;czanie"
  ]
  node [
    id 191
    label "communication"
  ]
  node [
    id 192
    label "wytwarzanie"
  ]
  node [
    id 193
    label "cement"
  ]
  node [
    id 194
    label "ceg&#322;a"
  ]
  node [
    id 195
    label "combination"
  ]
  node [
    id 196
    label "zobowi&#261;zywanie"
  ]
  node [
    id 197
    label "szcz&#281;ka"
  ]
  node [
    id 198
    label "anga&#380;owanie"
  ]
  node [
    id 199
    label "wi&#261;za&#263;"
  ]
  node [
    id 200
    label "twardnienie"
  ]
  node [
    id 201
    label "tobo&#322;ek"
  ]
  node [
    id 202
    label "podwi&#261;zanie"
  ]
  node [
    id 203
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 204
    label "przywi&#261;zanie"
  ]
  node [
    id 205
    label "przymocowywanie"
  ]
  node [
    id 206
    label "scalanie"
  ]
  node [
    id 207
    label "mezomeria"
  ]
  node [
    id 208
    label "wi&#281;&#378;"
  ]
  node [
    id 209
    label "fusion"
  ]
  node [
    id 210
    label "kojarzenie_si&#281;"
  ]
  node [
    id 211
    label "&#322;&#261;czenie"
  ]
  node [
    id 212
    label "uchwyt"
  ]
  node [
    id 213
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 214
    label "rozmieszczenie"
  ]
  node [
    id 215
    label "element_konstrukcyjny"
  ]
  node [
    id 216
    label "obezw&#322;adnianie"
  ]
  node [
    id 217
    label "manewr"
  ]
  node [
    id 218
    label "miecz"
  ]
  node [
    id 219
    label "obwi&#261;zanie"
  ]
  node [
    id 220
    label "zawi&#261;zek"
  ]
  node [
    id 221
    label "obwi&#261;zywanie"
  ]
  node [
    id 222
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 223
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 224
    label "w&#281;ze&#322;"
  ]
  node [
    id 225
    label "consort"
  ]
  node [
    id 226
    label "opakowa&#263;"
  ]
  node [
    id 227
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 228
    label "relate"
  ]
  node [
    id 229
    label "form"
  ]
  node [
    id 230
    label "unify"
  ]
  node [
    id 231
    label "incorporate"
  ]
  node [
    id 232
    label "bind"
  ]
  node [
    id 233
    label "zawi&#261;za&#263;"
  ]
  node [
    id 234
    label "zaprawa"
  ]
  node [
    id 235
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 236
    label "powi&#261;za&#263;"
  ]
  node [
    id 237
    label "scali&#263;"
  ]
  node [
    id 238
    label "zatrzyma&#263;"
  ]
  node [
    id 239
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 240
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 241
    label "ograniczenie"
  ]
  node [
    id 242
    label "po&#322;&#261;czenie"
  ]
  node [
    id 243
    label "do&#322;&#261;czenie"
  ]
  node [
    id 244
    label "opakowanie"
  ]
  node [
    id 245
    label "attachment"
  ]
  node [
    id 246
    label "obezw&#322;adnienie"
  ]
  node [
    id 247
    label "zawi&#261;zanie"
  ]
  node [
    id 248
    label "tying"
  ]
  node [
    id 249
    label "st&#281;&#380;enie"
  ]
  node [
    id 250
    label "affiliation"
  ]
  node [
    id 251
    label "fastening"
  ]
  node [
    id 252
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 253
    label "z&#322;&#261;czenie"
  ]
  node [
    id 254
    label "zobowi&#261;zanie"
  ]
  node [
    id 255
    label "roztw&#243;r"
  ]
  node [
    id 256
    label "podmiot"
  ]
  node [
    id 257
    label "jednostka_organizacyjna"
  ]
  node [
    id 258
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 259
    label "TOPR"
  ]
  node [
    id 260
    label "endecki"
  ]
  node [
    id 261
    label "od&#322;am"
  ]
  node [
    id 262
    label "przedstawicielstwo"
  ]
  node [
    id 263
    label "Cepelia"
  ]
  node [
    id 264
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 265
    label "ZBoWiD"
  ]
  node [
    id 266
    label "organization"
  ]
  node [
    id 267
    label "centrala"
  ]
  node [
    id 268
    label "GOPR"
  ]
  node [
    id 269
    label "ZOMO"
  ]
  node [
    id 270
    label "ZMP"
  ]
  node [
    id 271
    label "komitet_koordynacyjny"
  ]
  node [
    id 272
    label "przybud&#243;wka"
  ]
  node [
    id 273
    label "boj&#243;wka"
  ]
  node [
    id 274
    label "zrelatywizowa&#263;"
  ]
  node [
    id 275
    label "zrelatywizowanie"
  ]
  node [
    id 276
    label "mention"
  ]
  node [
    id 277
    label "pomy&#347;lenie"
  ]
  node [
    id 278
    label "relatywizowa&#263;"
  ]
  node [
    id 279
    label "relatywizowanie"
  ]
  node [
    id 280
    label "kontakt"
  ]
  node [
    id 281
    label "konwersja"
  ]
  node [
    id 282
    label "notice"
  ]
  node [
    id 283
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 284
    label "przepowiedzenie"
  ]
  node [
    id 285
    label "rozwi&#261;zanie"
  ]
  node [
    id 286
    label "generowa&#263;"
  ]
  node [
    id 287
    label "wydanie"
  ]
  node [
    id 288
    label "message"
  ]
  node [
    id 289
    label "generowanie"
  ]
  node [
    id 290
    label "wydobycie"
  ]
  node [
    id 291
    label "zwerbalizowanie"
  ]
  node [
    id 292
    label "szyk"
  ]
  node [
    id 293
    label "notification"
  ]
  node [
    id 294
    label "powiedzenie"
  ]
  node [
    id 295
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 296
    label "denunciation"
  ]
  node [
    id 297
    label "leksem"
  ]
  node [
    id 298
    label "sformu&#322;owanie"
  ]
  node [
    id 299
    label "zdarzenie_si&#281;"
  ]
  node [
    id 300
    label "poj&#281;cie"
  ]
  node [
    id 301
    label "poinformowanie"
  ]
  node [
    id 302
    label "wording"
  ]
  node [
    id 303
    label "kompozycja"
  ]
  node [
    id 304
    label "oznaczenie"
  ]
  node [
    id 305
    label "znak_j&#281;zykowy"
  ]
  node [
    id 306
    label "ozdobnik"
  ]
  node [
    id 307
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 308
    label "grupa_imienna"
  ]
  node [
    id 309
    label "term"
  ]
  node [
    id 310
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 311
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 312
    label "ujawnienie"
  ]
  node [
    id 313
    label "affirmation"
  ]
  node [
    id 314
    label "zapisanie"
  ]
  node [
    id 315
    label "rzucenie"
  ]
  node [
    id 316
    label "wyeksploatowanie"
  ]
  node [
    id 317
    label "draw"
  ]
  node [
    id 318
    label "uwydatnienie"
  ]
  node [
    id 319
    label "uzyskanie"
  ]
  node [
    id 320
    label "fusillade"
  ]
  node [
    id 321
    label "wyratowanie"
  ]
  node [
    id 322
    label "wyj&#281;cie"
  ]
  node [
    id 323
    label "powyci&#261;ganie"
  ]
  node [
    id 324
    label "wydostanie"
  ]
  node [
    id 325
    label "dobycie"
  ]
  node [
    id 326
    label "explosion"
  ]
  node [
    id 327
    label "g&#243;rnictwo"
  ]
  node [
    id 328
    label "produkcja"
  ]
  node [
    id 329
    label "zrobienie"
  ]
  node [
    id 330
    label "delivery"
  ]
  node [
    id 331
    label "rendition"
  ]
  node [
    id 332
    label "d&#378;wi&#281;k"
  ]
  node [
    id 333
    label "egzemplarz"
  ]
  node [
    id 334
    label "impression"
  ]
  node [
    id 335
    label "publikacja"
  ]
  node [
    id 336
    label "zadenuncjowanie"
  ]
  node [
    id 337
    label "zapach"
  ]
  node [
    id 338
    label "reszta"
  ]
  node [
    id 339
    label "wytworzenie"
  ]
  node [
    id 340
    label "issue"
  ]
  node [
    id 341
    label "danie"
  ]
  node [
    id 342
    label "czasopismo"
  ]
  node [
    id 343
    label "podanie"
  ]
  node [
    id 344
    label "wprowadzenie"
  ]
  node [
    id 345
    label "odmiana"
  ]
  node [
    id 346
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 347
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 348
    label "urz&#261;dzenie"
  ]
  node [
    id 349
    label "po&#322;&#243;g"
  ]
  node [
    id 350
    label "spe&#322;nienie"
  ]
  node [
    id 351
    label "dula"
  ]
  node [
    id 352
    label "spos&#243;b"
  ]
  node [
    id 353
    label "usuni&#281;cie"
  ]
  node [
    id 354
    label "wymy&#347;lenie"
  ]
  node [
    id 355
    label "po&#322;o&#380;na"
  ]
  node [
    id 356
    label "uniewa&#380;nienie"
  ]
  node [
    id 357
    label "proces_fizjologiczny"
  ]
  node [
    id 358
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 359
    label "pomys&#322;"
  ]
  node [
    id 360
    label "szok_poporodowy"
  ]
  node [
    id 361
    label "event"
  ]
  node [
    id 362
    label "marc&#243;wka"
  ]
  node [
    id 363
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 364
    label "birth"
  ]
  node [
    id 365
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 366
    label "wynik"
  ]
  node [
    id 367
    label "przestanie"
  ]
  node [
    id 368
    label "whole"
  ]
  node [
    id 369
    label "sznyt"
  ]
  node [
    id 370
    label "skrzyd&#322;o"
  ]
  node [
    id 371
    label "kolejno&#347;&#263;"
  ]
  node [
    id 372
    label "tworzenie"
  ]
  node [
    id 373
    label "restructure"
  ]
  node [
    id 374
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 375
    label "exchange"
  ]
  node [
    id 376
    label "reorganizacja"
  ]
  node [
    id 377
    label "dane"
  ]
  node [
    id 378
    label "choroba_psychiczna"
  ]
  node [
    id 379
    label "mechanizm_obronny"
  ]
  node [
    id 380
    label "zaburzenie_somatoformiczne"
  ]
  node [
    id 381
    label "reakcja_chemiczna"
  ]
  node [
    id 382
    label "zamiana"
  ]
  node [
    id 383
    label "operacja_logiczna"
  ]
  node [
    id 384
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 385
    label "proces_fizyczny"
  ]
  node [
    id 386
    label "metamorphosis"
  ]
  node [
    id 387
    label "transformation"
  ]
  node [
    id 388
    label "d&#322;ug"
  ]
  node [
    id 389
    label "tworzy&#263;"
  ]
  node [
    id 390
    label "wytwarza&#263;"
  ]
  node [
    id 391
    label "przewidzenie"
  ]
  node [
    id 392
    label "powt&#243;rzenie"
  ]
  node [
    id 393
    label "condensation"
  ]
  node [
    id 394
    label "zw&#281;&#380;enie"
  ]
  node [
    id 395
    label "samog&#322;oska_&#347;cie&#347;niona"
  ]
  node [
    id 396
    label "zmienienie"
  ]
  node [
    id 397
    label "zwarty"
  ]
  node [
    id 398
    label "rozwleczenie"
  ]
  node [
    id 399
    label "wyznanie"
  ]
  node [
    id 400
    label "zapeszenie"
  ]
  node [
    id 401
    label "wypowied&#378;"
  ]
  node [
    id 402
    label "dodanie"
  ]
  node [
    id 403
    label "proverb"
  ]
  node [
    id 404
    label "ozwanie_si&#281;"
  ]
  node [
    id 405
    label "nazwanie"
  ]
  node [
    id 406
    label "statement"
  ]
  node [
    id 407
    label "doprowadzenie"
  ]
  node [
    id 408
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 409
    label "jednostka_monetarna"
  ]
  node [
    id 410
    label "centym"
  ]
  node [
    id 411
    label "Wilko"
  ]
  node [
    id 412
    label "mienie"
  ]
  node [
    id 413
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 414
    label "frymark"
  ]
  node [
    id 415
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 416
    label "commodity"
  ]
  node [
    id 417
    label "integer"
  ]
  node [
    id 418
    label "liczba"
  ]
  node [
    id 419
    label "zlewanie_si&#281;"
  ]
  node [
    id 420
    label "ilo&#347;&#263;"
  ]
  node [
    id 421
    label "uk&#322;ad"
  ]
  node [
    id 422
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 423
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 424
    label "pe&#322;ny"
  ]
  node [
    id 425
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 426
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 427
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 428
    label "stan"
  ]
  node [
    id 429
    label "rzecz"
  ]
  node [
    id 430
    label "immoblizacja"
  ]
  node [
    id 431
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 432
    label "przej&#347;cie"
  ]
  node [
    id 433
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 434
    label "rodowo&#347;&#263;"
  ]
  node [
    id 435
    label "patent"
  ]
  node [
    id 436
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 437
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 438
    label "przej&#347;&#263;"
  ]
  node [
    id 439
    label "possession"
  ]
  node [
    id 440
    label "maj&#261;tek"
  ]
  node [
    id 441
    label "Iwaszkiewicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
]
