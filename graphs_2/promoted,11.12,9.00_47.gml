graph [
  node [
    id 0
    label "cze&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 2
    label "honorowa&#263;"
  ]
  node [
    id 3
    label "uhonorowanie"
  ]
  node [
    id 4
    label "zaimponowanie"
  ]
  node [
    id 5
    label "honorowanie"
  ]
  node [
    id 6
    label "uszanowa&#263;"
  ]
  node [
    id 7
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 8
    label "uszanowanie"
  ]
  node [
    id 9
    label "szacuneczek"
  ]
  node [
    id 10
    label "rewerencja"
  ]
  node [
    id 11
    label "uhonorowa&#263;"
  ]
  node [
    id 12
    label "dobro"
  ]
  node [
    id 13
    label "szanowa&#263;"
  ]
  node [
    id 14
    label "respect"
  ]
  node [
    id 15
    label "postawa"
  ]
  node [
    id 16
    label "imponowanie"
  ]
  node [
    id 17
    label "ekstraspekcja"
  ]
  node [
    id 18
    label "feeling"
  ]
  node [
    id 19
    label "wiedza"
  ]
  node [
    id 20
    label "zemdle&#263;"
  ]
  node [
    id 21
    label "psychika"
  ]
  node [
    id 22
    label "stan"
  ]
  node [
    id 23
    label "Freud"
  ]
  node [
    id 24
    label "psychoanaliza"
  ]
  node [
    id 25
    label "conscience"
  ]
  node [
    id 26
    label "warto&#347;&#263;"
  ]
  node [
    id 27
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 28
    label "dobro&#263;"
  ]
  node [
    id 29
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 30
    label "krzywa_Engla"
  ]
  node [
    id 31
    label "cel"
  ]
  node [
    id 32
    label "dobra"
  ]
  node [
    id 33
    label "go&#322;&#261;bek"
  ]
  node [
    id 34
    label "despond"
  ]
  node [
    id 35
    label "litera"
  ]
  node [
    id 36
    label "kalokagatia"
  ]
  node [
    id 37
    label "rzecz"
  ]
  node [
    id 38
    label "g&#322;agolica"
  ]
  node [
    id 39
    label "nastawienie"
  ]
  node [
    id 40
    label "pozycja"
  ]
  node [
    id 41
    label "attitude"
  ]
  node [
    id 42
    label "powa&#380;anie"
  ]
  node [
    id 43
    label "wypowied&#378;"
  ]
  node [
    id 44
    label "uznawanie"
  ]
  node [
    id 45
    label "p&#322;acenie"
  ]
  node [
    id 46
    label "honor"
  ]
  node [
    id 47
    label "okazywanie"
  ]
  node [
    id 48
    label "czci&#263;"
  ]
  node [
    id 49
    label "acknowledge"
  ]
  node [
    id 50
    label "wyp&#322;aca&#263;"
  ]
  node [
    id 51
    label "notice"
  ]
  node [
    id 52
    label "fit"
  ]
  node [
    id 53
    label "uznawa&#263;"
  ]
  node [
    id 54
    label "treasure"
  ]
  node [
    id 55
    label "czu&#263;"
  ]
  node [
    id 56
    label "respektowa&#263;"
  ]
  node [
    id 57
    label "wyra&#380;a&#263;"
  ]
  node [
    id 58
    label "chowa&#263;"
  ]
  node [
    id 59
    label "wyrazi&#263;"
  ]
  node [
    id 60
    label "spare_part"
  ]
  node [
    id 61
    label "nagrodzi&#263;"
  ]
  node [
    id 62
    label "uczci&#263;"
  ]
  node [
    id 63
    label "wyp&#322;aci&#263;"
  ]
  node [
    id 64
    label "wzbudzanie"
  ]
  node [
    id 65
    label "szanowanie"
  ]
  node [
    id 66
    label "wzbudzenie"
  ]
  node [
    id 67
    label "nagrodzenie"
  ]
  node [
    id 68
    label "zap&#322;acenie"
  ]
  node [
    id 69
    label "wyra&#380;enie"
  ]
  node [
    id 70
    label "greet"
  ]
  node [
    id 71
    label "welcome"
  ]
  node [
    id 72
    label "pozdrawia&#263;"
  ]
  node [
    id 73
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 74
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 75
    label "robi&#263;"
  ]
  node [
    id 76
    label "obchodzi&#263;"
  ]
  node [
    id 77
    label "bless"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
]
