graph [
  node [
    id 0
    label "lawrence"
    origin "text"
  ]
  node [
    id 1
    label "lessig"
    origin "text"
  ]
  node [
    id 2
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "podczas"
    origin "text"
  ]
  node [
    id 4
    label "zjazd"
    origin "text"
  ]
  node [
    id 5
    label "isummit"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 8
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "dubrovniku"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 13
    label "tak"
    origin "text"
  ]
  node [
    id 14
    label "intensywnie"
    origin "text"
  ]
  node [
    id 15
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 16
    label "rzecz"
    origin "text"
  ]
  node [
    id 17
    label "wolny"
    origin "text"
  ]
  node [
    id 18
    label "kultura"
    origin "text"
  ]
  node [
    id 19
    label "swoje"
    origin "text"
  ]
  node [
    id 20
    label "blog"
    origin "text"
  ]
  node [
    id 21
    label "t&#322;umacz"
    origin "text"
  ]
  node [
    id 22
    label "chwila"
    origin "text"
  ]
  node [
    id 23
    label "obecna"
    origin "text"
  ]
  node [
    id 24
    label "istotny"
    origin "text"
  ]
  node [
    id 25
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "walek"
    origin "text"
  ]
  node [
    id 27
    label "og&#243;lny"
    origin "text"
  ]
  node [
    id 28
    label "korupcja"
    origin "text"
  ]
  node [
    id 29
    label "system"
    origin "text"
  ]
  node [
    id 30
    label "polityczny"
    origin "text"
  ]
  node [
    id 31
    label "publish"
  ]
  node [
    id 32
    label "poda&#263;"
  ]
  node [
    id 33
    label "opublikowa&#263;"
  ]
  node [
    id 34
    label "obwo&#322;a&#263;"
  ]
  node [
    id 35
    label "declare"
  ]
  node [
    id 36
    label "communicate"
  ]
  node [
    id 37
    label "upubliczni&#263;"
  ]
  node [
    id 38
    label "picture"
  ]
  node [
    id 39
    label "wydawnictwo"
  ]
  node [
    id 40
    label "wprowadzi&#263;"
  ]
  node [
    id 41
    label "tenis"
  ]
  node [
    id 42
    label "supply"
  ]
  node [
    id 43
    label "da&#263;"
  ]
  node [
    id 44
    label "ustawi&#263;"
  ]
  node [
    id 45
    label "siatk&#243;wka"
  ]
  node [
    id 46
    label "give"
  ]
  node [
    id 47
    label "zagra&#263;"
  ]
  node [
    id 48
    label "jedzenie"
  ]
  node [
    id 49
    label "poinformowa&#263;"
  ]
  node [
    id 50
    label "introduce"
  ]
  node [
    id 51
    label "nafaszerowa&#263;"
  ]
  node [
    id 52
    label "zaserwowa&#263;"
  ]
  node [
    id 53
    label "zakomunikowa&#263;"
  ]
  node [
    id 54
    label "okre&#347;li&#263;"
  ]
  node [
    id 55
    label "kombinacja_alpejska"
  ]
  node [
    id 56
    label "rally"
  ]
  node [
    id 57
    label "pochy&#322;o&#347;&#263;"
  ]
  node [
    id 58
    label "manewr"
  ]
  node [
    id 59
    label "przyjazd"
  ]
  node [
    id 60
    label "spotkanie"
  ]
  node [
    id 61
    label "dojazd"
  ]
  node [
    id 62
    label "jazda"
  ]
  node [
    id 63
    label "wy&#347;cig"
  ]
  node [
    id 64
    label "odjazd"
  ]
  node [
    id 65
    label "meeting"
  ]
  node [
    id 66
    label "doznanie"
  ]
  node [
    id 67
    label "gathering"
  ]
  node [
    id 68
    label "zawarcie"
  ]
  node [
    id 69
    label "wydarzenie"
  ]
  node [
    id 70
    label "znajomy"
  ]
  node [
    id 71
    label "powitanie"
  ]
  node [
    id 72
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 73
    label "spowodowanie"
  ]
  node [
    id 74
    label "zdarzenie_si&#281;"
  ]
  node [
    id 75
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 76
    label "znalezienie"
  ]
  node [
    id 77
    label "match"
  ]
  node [
    id 78
    label "employment"
  ]
  node [
    id 79
    label "po&#380;egnanie"
  ]
  node [
    id 80
    label "gather"
  ]
  node [
    id 81
    label "spotykanie"
  ]
  node [
    id 82
    label "spotkanie_si&#281;"
  ]
  node [
    id 83
    label "finisz"
  ]
  node [
    id 84
    label "bieg"
  ]
  node [
    id 85
    label "Formu&#322;a_1"
  ]
  node [
    id 86
    label "zmagania"
  ]
  node [
    id 87
    label "contest"
  ]
  node [
    id 88
    label "celownik"
  ]
  node [
    id 89
    label "lista_startowa"
  ]
  node [
    id 90
    label "torowiec"
  ]
  node [
    id 91
    label "start"
  ]
  node [
    id 92
    label "rywalizacja"
  ]
  node [
    id 93
    label "start_lotny"
  ]
  node [
    id 94
    label "racing"
  ]
  node [
    id 95
    label "prolog"
  ]
  node [
    id 96
    label "lotny_finisz"
  ]
  node [
    id 97
    label "zawody"
  ]
  node [
    id 98
    label "premia_g&#243;rska"
  ]
  node [
    id 99
    label "formacja"
  ]
  node [
    id 100
    label "szwadron"
  ]
  node [
    id 101
    label "wykrzyknik"
  ]
  node [
    id 102
    label "awantura"
  ]
  node [
    id 103
    label "journey"
  ]
  node [
    id 104
    label "sport"
  ]
  node [
    id 105
    label "heca"
  ]
  node [
    id 106
    label "ruch"
  ]
  node [
    id 107
    label "cavalry"
  ]
  node [
    id 108
    label "szale&#324;stwo"
  ]
  node [
    id 109
    label "chor&#261;giew"
  ]
  node [
    id 110
    label "p&#322;aszczyzna"
  ]
  node [
    id 111
    label "droga"
  ]
  node [
    id 112
    label "utrzymywanie"
  ]
  node [
    id 113
    label "move"
  ]
  node [
    id 114
    label "movement"
  ]
  node [
    id 115
    label "posuni&#281;cie"
  ]
  node [
    id 116
    label "myk"
  ]
  node [
    id 117
    label "taktyka"
  ]
  node [
    id 118
    label "utrzyma&#263;"
  ]
  node [
    id 119
    label "maneuver"
  ]
  node [
    id 120
    label "utrzymanie"
  ]
  node [
    id 121
    label "utrzymywa&#263;"
  ]
  node [
    id 122
    label "przybycie"
  ]
  node [
    id 123
    label "arrival"
  ]
  node [
    id 124
    label "grogginess"
  ]
  node [
    id 125
    label "odurzenie"
  ]
  node [
    id 126
    label "departure"
  ]
  node [
    id 127
    label "zamroczenie"
  ]
  node [
    id 128
    label "grupa"
  ]
  node [
    id 129
    label "odkrycie"
  ]
  node [
    id 130
    label "zgromadzenie"
  ]
  node [
    id 131
    label "miting"
  ]
  node [
    id 132
    label "dok&#322;adnie"
  ]
  node [
    id 133
    label "punctiliously"
  ]
  node [
    id 134
    label "meticulously"
  ]
  node [
    id 135
    label "precyzyjnie"
  ]
  node [
    id 136
    label "dok&#322;adny"
  ]
  node [
    id 137
    label "rzetelnie"
  ]
  node [
    id 138
    label "dispose"
  ]
  node [
    id 139
    label "zrezygnowa&#263;"
  ]
  node [
    id 140
    label "zrobi&#263;"
  ]
  node [
    id 141
    label "cause"
  ]
  node [
    id 142
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 143
    label "wytworzy&#263;"
  ]
  node [
    id 144
    label "przesta&#263;"
  ]
  node [
    id 145
    label "drop"
  ]
  node [
    id 146
    label "post&#261;pi&#263;"
  ]
  node [
    id 147
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 148
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 149
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 150
    label "zorganizowa&#263;"
  ]
  node [
    id 151
    label "appoint"
  ]
  node [
    id 152
    label "wystylizowa&#263;"
  ]
  node [
    id 153
    label "przerobi&#263;"
  ]
  node [
    id 154
    label "nabra&#263;"
  ]
  node [
    id 155
    label "make"
  ]
  node [
    id 156
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 157
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 158
    label "wydali&#263;"
  ]
  node [
    id 159
    label "manufacture"
  ]
  node [
    id 160
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 161
    label "model"
  ]
  node [
    id 162
    label "nada&#263;"
  ]
  node [
    id 163
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 164
    label "coating"
  ]
  node [
    id 165
    label "sko&#324;czy&#263;"
  ]
  node [
    id 166
    label "leave_office"
  ]
  node [
    id 167
    label "fail"
  ]
  node [
    id 168
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 169
    label "mie&#263;_miejsce"
  ]
  node [
    id 170
    label "equal"
  ]
  node [
    id 171
    label "trwa&#263;"
  ]
  node [
    id 172
    label "chodzi&#263;"
  ]
  node [
    id 173
    label "si&#281;ga&#263;"
  ]
  node [
    id 174
    label "stan"
  ]
  node [
    id 175
    label "obecno&#347;&#263;"
  ]
  node [
    id 176
    label "stand"
  ]
  node [
    id 177
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 178
    label "uczestniczy&#263;"
  ]
  node [
    id 179
    label "participate"
  ]
  node [
    id 180
    label "robi&#263;"
  ]
  node [
    id 181
    label "istnie&#263;"
  ]
  node [
    id 182
    label "pozostawa&#263;"
  ]
  node [
    id 183
    label "zostawa&#263;"
  ]
  node [
    id 184
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 185
    label "adhere"
  ]
  node [
    id 186
    label "compass"
  ]
  node [
    id 187
    label "korzysta&#263;"
  ]
  node [
    id 188
    label "appreciation"
  ]
  node [
    id 189
    label "osi&#261;ga&#263;"
  ]
  node [
    id 190
    label "dociera&#263;"
  ]
  node [
    id 191
    label "get"
  ]
  node [
    id 192
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 193
    label "mierzy&#263;"
  ]
  node [
    id 194
    label "u&#380;ywa&#263;"
  ]
  node [
    id 195
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 196
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 197
    label "exsert"
  ]
  node [
    id 198
    label "being"
  ]
  node [
    id 199
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 200
    label "cecha"
  ]
  node [
    id 201
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 202
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 203
    label "p&#322;ywa&#263;"
  ]
  node [
    id 204
    label "run"
  ]
  node [
    id 205
    label "bangla&#263;"
  ]
  node [
    id 206
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 207
    label "przebiega&#263;"
  ]
  node [
    id 208
    label "wk&#322;ada&#263;"
  ]
  node [
    id 209
    label "proceed"
  ]
  node [
    id 210
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 211
    label "carry"
  ]
  node [
    id 212
    label "bywa&#263;"
  ]
  node [
    id 213
    label "dziama&#263;"
  ]
  node [
    id 214
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 215
    label "stara&#263;_si&#281;"
  ]
  node [
    id 216
    label "para"
  ]
  node [
    id 217
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 218
    label "str&#243;j"
  ]
  node [
    id 219
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 220
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 221
    label "krok"
  ]
  node [
    id 222
    label "tryb"
  ]
  node [
    id 223
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 224
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 225
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 226
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 227
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 228
    label "continue"
  ]
  node [
    id 229
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 230
    label "Ohio"
  ]
  node [
    id 231
    label "wci&#281;cie"
  ]
  node [
    id 232
    label "Nowy_York"
  ]
  node [
    id 233
    label "warstwa"
  ]
  node [
    id 234
    label "samopoczucie"
  ]
  node [
    id 235
    label "Illinois"
  ]
  node [
    id 236
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 237
    label "state"
  ]
  node [
    id 238
    label "Jukatan"
  ]
  node [
    id 239
    label "Kalifornia"
  ]
  node [
    id 240
    label "Wirginia"
  ]
  node [
    id 241
    label "wektor"
  ]
  node [
    id 242
    label "Goa"
  ]
  node [
    id 243
    label "Teksas"
  ]
  node [
    id 244
    label "Waszyngton"
  ]
  node [
    id 245
    label "miejsce"
  ]
  node [
    id 246
    label "Massachusetts"
  ]
  node [
    id 247
    label "Alaska"
  ]
  node [
    id 248
    label "Arakan"
  ]
  node [
    id 249
    label "Hawaje"
  ]
  node [
    id 250
    label "Maryland"
  ]
  node [
    id 251
    label "punkt"
  ]
  node [
    id 252
    label "Michigan"
  ]
  node [
    id 253
    label "Arizona"
  ]
  node [
    id 254
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 255
    label "Georgia"
  ]
  node [
    id 256
    label "poziom"
  ]
  node [
    id 257
    label "Pensylwania"
  ]
  node [
    id 258
    label "shape"
  ]
  node [
    id 259
    label "Luizjana"
  ]
  node [
    id 260
    label "Nowy_Meksyk"
  ]
  node [
    id 261
    label "Alabama"
  ]
  node [
    id 262
    label "ilo&#347;&#263;"
  ]
  node [
    id 263
    label "Kansas"
  ]
  node [
    id 264
    label "Oregon"
  ]
  node [
    id 265
    label "Oklahoma"
  ]
  node [
    id 266
    label "Floryda"
  ]
  node [
    id 267
    label "jednostka_administracyjna"
  ]
  node [
    id 268
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 269
    label "intensywny"
  ]
  node [
    id 270
    label "g&#281;sto"
  ]
  node [
    id 271
    label "dynamicznie"
  ]
  node [
    id 272
    label "densely"
  ]
  node [
    id 273
    label "g&#281;sty"
  ]
  node [
    id 274
    label "gor&#261;czkowo"
  ]
  node [
    id 275
    label "ci&#281;&#380;ki"
  ]
  node [
    id 276
    label "obficie"
  ]
  node [
    id 277
    label "ci&#281;&#380;ko"
  ]
  node [
    id 278
    label "mocno"
  ]
  node [
    id 279
    label "dynamically"
  ]
  node [
    id 280
    label "dynamiczny"
  ]
  node [
    id 281
    label "zmiennie"
  ]
  node [
    id 282
    label "ostro"
  ]
  node [
    id 283
    label "energiczny"
  ]
  node [
    id 284
    label "szybki"
  ]
  node [
    id 285
    label "znacz&#261;cy"
  ]
  node [
    id 286
    label "zwarty"
  ]
  node [
    id 287
    label "efektywny"
  ]
  node [
    id 288
    label "ogrodnictwo"
  ]
  node [
    id 289
    label "pe&#322;ny"
  ]
  node [
    id 290
    label "nieproporcjonalny"
  ]
  node [
    id 291
    label "specjalny"
  ]
  node [
    id 292
    label "function"
  ]
  node [
    id 293
    label "determine"
  ]
  node [
    id 294
    label "work"
  ]
  node [
    id 295
    label "powodowa&#263;"
  ]
  node [
    id 296
    label "reakcja_chemiczna"
  ]
  node [
    id 297
    label "commit"
  ]
  node [
    id 298
    label "organizowa&#263;"
  ]
  node [
    id 299
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 300
    label "czyni&#263;"
  ]
  node [
    id 301
    label "stylizowa&#263;"
  ]
  node [
    id 302
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 303
    label "falowa&#263;"
  ]
  node [
    id 304
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 305
    label "peddle"
  ]
  node [
    id 306
    label "praca"
  ]
  node [
    id 307
    label "wydala&#263;"
  ]
  node [
    id 308
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 309
    label "tentegowa&#263;"
  ]
  node [
    id 310
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 311
    label "urz&#261;dza&#263;"
  ]
  node [
    id 312
    label "oszukiwa&#263;"
  ]
  node [
    id 313
    label "ukazywa&#263;"
  ]
  node [
    id 314
    label "przerabia&#263;"
  ]
  node [
    id 315
    label "act"
  ]
  node [
    id 316
    label "post&#281;powa&#263;"
  ]
  node [
    id 317
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 318
    label "motywowa&#263;"
  ]
  node [
    id 319
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 320
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 321
    label "rozumie&#263;"
  ]
  node [
    id 322
    label "szczeka&#263;"
  ]
  node [
    id 323
    label "rozmawia&#263;"
  ]
  node [
    id 324
    label "m&#243;wi&#263;"
  ]
  node [
    id 325
    label "funkcjonowa&#263;"
  ]
  node [
    id 326
    label "ko&#322;o"
  ]
  node [
    id 327
    label "spos&#243;b"
  ]
  node [
    id 328
    label "modalno&#347;&#263;"
  ]
  node [
    id 329
    label "z&#261;b"
  ]
  node [
    id 330
    label "kategoria_gramatyczna"
  ]
  node [
    id 331
    label "skala"
  ]
  node [
    id 332
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 333
    label "koniugacja"
  ]
  node [
    id 334
    label "object"
  ]
  node [
    id 335
    label "przedmiot"
  ]
  node [
    id 336
    label "temat"
  ]
  node [
    id 337
    label "wpadni&#281;cie"
  ]
  node [
    id 338
    label "mienie"
  ]
  node [
    id 339
    label "przyroda"
  ]
  node [
    id 340
    label "istota"
  ]
  node [
    id 341
    label "obiekt"
  ]
  node [
    id 342
    label "wpa&#347;&#263;"
  ]
  node [
    id 343
    label "wpadanie"
  ]
  node [
    id 344
    label "wpada&#263;"
  ]
  node [
    id 345
    label "co&#347;"
  ]
  node [
    id 346
    label "budynek"
  ]
  node [
    id 347
    label "thing"
  ]
  node [
    id 348
    label "poj&#281;cie"
  ]
  node [
    id 349
    label "program"
  ]
  node [
    id 350
    label "strona"
  ]
  node [
    id 351
    label "zboczenie"
  ]
  node [
    id 352
    label "om&#243;wienie"
  ]
  node [
    id 353
    label "sponiewieranie"
  ]
  node [
    id 354
    label "discipline"
  ]
  node [
    id 355
    label "omawia&#263;"
  ]
  node [
    id 356
    label "kr&#261;&#380;enie"
  ]
  node [
    id 357
    label "tre&#347;&#263;"
  ]
  node [
    id 358
    label "robienie"
  ]
  node [
    id 359
    label "sponiewiera&#263;"
  ]
  node [
    id 360
    label "element"
  ]
  node [
    id 361
    label "entity"
  ]
  node [
    id 362
    label "tematyka"
  ]
  node [
    id 363
    label "w&#261;tek"
  ]
  node [
    id 364
    label "charakter"
  ]
  node [
    id 365
    label "zbaczanie"
  ]
  node [
    id 366
    label "program_nauczania"
  ]
  node [
    id 367
    label "om&#243;wi&#263;"
  ]
  node [
    id 368
    label "omawianie"
  ]
  node [
    id 369
    label "zbacza&#263;"
  ]
  node [
    id 370
    label "zboczy&#263;"
  ]
  node [
    id 371
    label "mentalno&#347;&#263;"
  ]
  node [
    id 372
    label "superego"
  ]
  node [
    id 373
    label "psychika"
  ]
  node [
    id 374
    label "znaczenie"
  ]
  node [
    id 375
    label "wn&#281;trze"
  ]
  node [
    id 376
    label "asymilowanie_si&#281;"
  ]
  node [
    id 377
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 378
    label "Wsch&#243;d"
  ]
  node [
    id 379
    label "praca_rolnicza"
  ]
  node [
    id 380
    label "przejmowanie"
  ]
  node [
    id 381
    label "zjawisko"
  ]
  node [
    id 382
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 383
    label "makrokosmos"
  ]
  node [
    id 384
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 385
    label "konwencja"
  ]
  node [
    id 386
    label "propriety"
  ]
  node [
    id 387
    label "przejmowa&#263;"
  ]
  node [
    id 388
    label "brzoskwiniarnia"
  ]
  node [
    id 389
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 390
    label "sztuka"
  ]
  node [
    id 391
    label "zwyczaj"
  ]
  node [
    id 392
    label "jako&#347;&#263;"
  ]
  node [
    id 393
    label "kuchnia"
  ]
  node [
    id 394
    label "tradycja"
  ]
  node [
    id 395
    label "populace"
  ]
  node [
    id 396
    label "hodowla"
  ]
  node [
    id 397
    label "religia"
  ]
  node [
    id 398
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 399
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 400
    label "przej&#281;cie"
  ]
  node [
    id 401
    label "przej&#261;&#263;"
  ]
  node [
    id 402
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 403
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 404
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 405
    label "woda"
  ]
  node [
    id 406
    label "teren"
  ]
  node [
    id 407
    label "mikrokosmos"
  ]
  node [
    id 408
    label "ekosystem"
  ]
  node [
    id 409
    label "stw&#243;r"
  ]
  node [
    id 410
    label "obiekt_naturalny"
  ]
  node [
    id 411
    label "environment"
  ]
  node [
    id 412
    label "Ziemia"
  ]
  node [
    id 413
    label "przyra"
  ]
  node [
    id 414
    label "wszechstworzenie"
  ]
  node [
    id 415
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 416
    label "fauna"
  ]
  node [
    id 417
    label "biota"
  ]
  node [
    id 418
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 419
    label "strike"
  ]
  node [
    id 420
    label "zaziera&#263;"
  ]
  node [
    id 421
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 422
    label "czu&#263;"
  ]
  node [
    id 423
    label "spotyka&#263;"
  ]
  node [
    id 424
    label "pogo"
  ]
  node [
    id 425
    label "d&#378;wi&#281;k"
  ]
  node [
    id 426
    label "ogrom"
  ]
  node [
    id 427
    label "zapach"
  ]
  node [
    id 428
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 429
    label "popada&#263;"
  ]
  node [
    id 430
    label "odwiedza&#263;"
  ]
  node [
    id 431
    label "wymy&#347;la&#263;"
  ]
  node [
    id 432
    label "przypomina&#263;"
  ]
  node [
    id 433
    label "ujmowa&#263;"
  ]
  node [
    id 434
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 435
    label "&#347;wiat&#322;o"
  ]
  node [
    id 436
    label "fall"
  ]
  node [
    id 437
    label "chowa&#263;"
  ]
  node [
    id 438
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 439
    label "demaskowa&#263;"
  ]
  node [
    id 440
    label "ulega&#263;"
  ]
  node [
    id 441
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 442
    label "emocja"
  ]
  node [
    id 443
    label "flatten"
  ]
  node [
    id 444
    label "ulec"
  ]
  node [
    id 445
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 446
    label "collapse"
  ]
  node [
    id 447
    label "fall_upon"
  ]
  node [
    id 448
    label "ponie&#347;&#263;"
  ]
  node [
    id 449
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 450
    label "uderzy&#263;"
  ]
  node [
    id 451
    label "wymy&#347;li&#263;"
  ]
  node [
    id 452
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 453
    label "decline"
  ]
  node [
    id 454
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 455
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 456
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 457
    label "spotka&#263;"
  ]
  node [
    id 458
    label "odwiedzi&#263;"
  ]
  node [
    id 459
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 460
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 461
    label "uleganie"
  ]
  node [
    id 462
    label "dostawanie_si&#281;"
  ]
  node [
    id 463
    label "odwiedzanie"
  ]
  node [
    id 464
    label "ciecz"
  ]
  node [
    id 465
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 466
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 467
    label "postrzeganie"
  ]
  node [
    id 468
    label "rzeka"
  ]
  node [
    id 469
    label "wymy&#347;lanie"
  ]
  node [
    id 470
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 471
    label "ingress"
  ]
  node [
    id 472
    label "dzianie_si&#281;"
  ]
  node [
    id 473
    label "wp&#322;ywanie"
  ]
  node [
    id 474
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 475
    label "overlap"
  ]
  node [
    id 476
    label "wkl&#281;sanie"
  ]
  node [
    id 477
    label "wymy&#347;lenie"
  ]
  node [
    id 478
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 479
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 480
    label "ulegni&#281;cie"
  ]
  node [
    id 481
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 482
    label "poniesienie"
  ]
  node [
    id 483
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 484
    label "odwiedzenie"
  ]
  node [
    id 485
    label "uderzenie"
  ]
  node [
    id 486
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 487
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 488
    label "dostanie_si&#281;"
  ]
  node [
    id 489
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 490
    label "release"
  ]
  node [
    id 491
    label "rozbicie_si&#281;"
  ]
  node [
    id 492
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 493
    label "przej&#347;cie"
  ]
  node [
    id 494
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 495
    label "rodowo&#347;&#263;"
  ]
  node [
    id 496
    label "patent"
  ]
  node [
    id 497
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 498
    label "dobra"
  ]
  node [
    id 499
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 500
    label "przej&#347;&#263;"
  ]
  node [
    id 501
    label "possession"
  ]
  node [
    id 502
    label "sprawa"
  ]
  node [
    id 503
    label "wyraz_pochodny"
  ]
  node [
    id 504
    label "fraza"
  ]
  node [
    id 505
    label "forum"
  ]
  node [
    id 506
    label "topik"
  ]
  node [
    id 507
    label "forma"
  ]
  node [
    id 508
    label "melodia"
  ]
  node [
    id 509
    label "otoczka"
  ]
  node [
    id 510
    label "rozrzedzenie"
  ]
  node [
    id 511
    label "rzedni&#281;cie"
  ]
  node [
    id 512
    label "niespieszny"
  ]
  node [
    id 513
    label "zwalnianie_si&#281;"
  ]
  node [
    id 514
    label "wakowa&#263;"
  ]
  node [
    id 515
    label "rozwadnianie"
  ]
  node [
    id 516
    label "niezale&#380;ny"
  ]
  node [
    id 517
    label "zrzedni&#281;cie"
  ]
  node [
    id 518
    label "swobodnie"
  ]
  node [
    id 519
    label "rozrzedzanie"
  ]
  node [
    id 520
    label "rozwodnienie"
  ]
  node [
    id 521
    label "strza&#322;"
  ]
  node [
    id 522
    label "wolnie"
  ]
  node [
    id 523
    label "zwolnienie_si&#281;"
  ]
  node [
    id 524
    label "wolno"
  ]
  node [
    id 525
    label "lu&#378;no"
  ]
  node [
    id 526
    label "niespiesznie"
  ]
  node [
    id 527
    label "spokojny"
  ]
  node [
    id 528
    label "trafny"
  ]
  node [
    id 529
    label "shot"
  ]
  node [
    id 530
    label "przykro&#347;&#263;"
  ]
  node [
    id 531
    label "huk"
  ]
  node [
    id 532
    label "bum-bum"
  ]
  node [
    id 533
    label "pi&#322;ka"
  ]
  node [
    id 534
    label "eksplozja"
  ]
  node [
    id 535
    label "wyrzut"
  ]
  node [
    id 536
    label "usi&#322;owanie"
  ]
  node [
    id 537
    label "przypadek"
  ]
  node [
    id 538
    label "shooting"
  ]
  node [
    id 539
    label "odgadywanie"
  ]
  node [
    id 540
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 541
    label "usamodzielnienie"
  ]
  node [
    id 542
    label "usamodzielnianie"
  ]
  node [
    id 543
    label "niezale&#380;nie"
  ]
  node [
    id 544
    label "thinly"
  ]
  node [
    id 545
    label "wolniej"
  ]
  node [
    id 546
    label "swobodny"
  ]
  node [
    id 547
    label "free"
  ]
  node [
    id 548
    label "lu&#378;ny"
  ]
  node [
    id 549
    label "dowolnie"
  ]
  node [
    id 550
    label "naturalnie"
  ]
  node [
    id 551
    label "dilution"
  ]
  node [
    id 552
    label "powodowanie"
  ]
  node [
    id 553
    label "rozcie&#324;czanie"
  ]
  node [
    id 554
    label "chrzczenie"
  ]
  node [
    id 555
    label "rzadki"
  ]
  node [
    id 556
    label "stanie_si&#281;"
  ]
  node [
    id 557
    label "ochrzczenie"
  ]
  node [
    id 558
    label "rozcie&#324;czenie"
  ]
  node [
    id 559
    label "rarefaction"
  ]
  node [
    id 560
    label "czynno&#347;&#263;"
  ]
  node [
    id 561
    label "lekko"
  ]
  node [
    id 562
    label "&#322;atwo"
  ]
  node [
    id 563
    label "odlegle"
  ]
  node [
    id 564
    label "przyjemnie"
  ]
  node [
    id 565
    label "nieformalnie"
  ]
  node [
    id 566
    label "stawanie_si&#281;"
  ]
  node [
    id 567
    label "stanowisko"
  ]
  node [
    id 568
    label "warto&#347;&#263;"
  ]
  node [
    id 569
    label "quality"
  ]
  node [
    id 570
    label "syf"
  ]
  node [
    id 571
    label "absolutorium"
  ]
  node [
    id 572
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 573
    label "dzia&#322;anie"
  ]
  node [
    id 574
    label "activity"
  ]
  node [
    id 575
    label "proces"
  ]
  node [
    id 576
    label "boski"
  ]
  node [
    id 577
    label "krajobraz"
  ]
  node [
    id 578
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 579
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 580
    label "przywidzenie"
  ]
  node [
    id 581
    label "presence"
  ]
  node [
    id 582
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 583
    label "potrzymanie"
  ]
  node [
    id 584
    label "rolnictwo"
  ]
  node [
    id 585
    label "pod&#243;j"
  ]
  node [
    id 586
    label "filiacja"
  ]
  node [
    id 587
    label "licencjonowanie"
  ]
  node [
    id 588
    label "opasa&#263;"
  ]
  node [
    id 589
    label "ch&#243;w"
  ]
  node [
    id 590
    label "licencja"
  ]
  node [
    id 591
    label "sokolarnia"
  ]
  node [
    id 592
    label "potrzyma&#263;"
  ]
  node [
    id 593
    label "rozp&#322;&#243;d"
  ]
  node [
    id 594
    label "grupa_organizm&#243;w"
  ]
  node [
    id 595
    label "wypas"
  ]
  node [
    id 596
    label "wychowalnia"
  ]
  node [
    id 597
    label "pstr&#261;garnia"
  ]
  node [
    id 598
    label "krzy&#380;owanie"
  ]
  node [
    id 599
    label "licencjonowa&#263;"
  ]
  node [
    id 600
    label "odch&#243;w"
  ]
  node [
    id 601
    label "tucz"
  ]
  node [
    id 602
    label "ud&#243;j"
  ]
  node [
    id 603
    label "klatka"
  ]
  node [
    id 604
    label "opasienie"
  ]
  node [
    id 605
    label "wych&#243;w"
  ]
  node [
    id 606
    label "obrz&#261;dek"
  ]
  node [
    id 607
    label "opasanie"
  ]
  node [
    id 608
    label "polish"
  ]
  node [
    id 609
    label "akwarium"
  ]
  node [
    id 610
    label "biotechnika"
  ]
  node [
    id 611
    label "charakterystyka"
  ]
  node [
    id 612
    label "m&#322;ot"
  ]
  node [
    id 613
    label "znak"
  ]
  node [
    id 614
    label "drzewo"
  ]
  node [
    id 615
    label "pr&#243;ba"
  ]
  node [
    id 616
    label "attribute"
  ]
  node [
    id 617
    label "marka"
  ]
  node [
    id 618
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 619
    label "zbi&#243;r"
  ]
  node [
    id 620
    label "uk&#322;ad"
  ]
  node [
    id 621
    label "styl"
  ]
  node [
    id 622
    label "line"
  ]
  node [
    id 623
    label "kanon"
  ]
  node [
    id 624
    label "biom"
  ]
  node [
    id 625
    label "szata_ro&#347;linna"
  ]
  node [
    id 626
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 627
    label "formacja_ro&#347;linna"
  ]
  node [
    id 628
    label "zielono&#347;&#263;"
  ]
  node [
    id 629
    label "pi&#281;tro"
  ]
  node [
    id 630
    label "plant"
  ]
  node [
    id 631
    label "ro&#347;lina"
  ]
  node [
    id 632
    label "geosystem"
  ]
  node [
    id 633
    label "pr&#243;bowanie"
  ]
  node [
    id 634
    label "rola"
  ]
  node [
    id 635
    label "cz&#322;owiek"
  ]
  node [
    id 636
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 637
    label "realizacja"
  ]
  node [
    id 638
    label "scena"
  ]
  node [
    id 639
    label "didaskalia"
  ]
  node [
    id 640
    label "czyn"
  ]
  node [
    id 641
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 642
    label "head"
  ]
  node [
    id 643
    label "scenariusz"
  ]
  node [
    id 644
    label "egzemplarz"
  ]
  node [
    id 645
    label "jednostka"
  ]
  node [
    id 646
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 647
    label "utw&#243;r"
  ]
  node [
    id 648
    label "kultura_duchowa"
  ]
  node [
    id 649
    label "fortel"
  ]
  node [
    id 650
    label "theatrical_performance"
  ]
  node [
    id 651
    label "ambala&#380;"
  ]
  node [
    id 652
    label "sprawno&#347;&#263;"
  ]
  node [
    id 653
    label "kobieta"
  ]
  node [
    id 654
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 655
    label "Faust"
  ]
  node [
    id 656
    label "scenografia"
  ]
  node [
    id 657
    label "ods&#322;ona"
  ]
  node [
    id 658
    label "turn"
  ]
  node [
    id 659
    label "pokaz"
  ]
  node [
    id 660
    label "przedstawienie"
  ]
  node [
    id 661
    label "przedstawi&#263;"
  ]
  node [
    id 662
    label "Apollo"
  ]
  node [
    id 663
    label "przedstawianie"
  ]
  node [
    id 664
    label "przedstawia&#263;"
  ]
  node [
    id 665
    label "towar"
  ]
  node [
    id 666
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 667
    label "zachowanie"
  ]
  node [
    id 668
    label "ceremony"
  ]
  node [
    id 669
    label "kult"
  ]
  node [
    id 670
    label "mitologia"
  ]
  node [
    id 671
    label "wyznanie"
  ]
  node [
    id 672
    label "ideologia"
  ]
  node [
    id 673
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 674
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 675
    label "nawracanie_si&#281;"
  ]
  node [
    id 676
    label "duchowny"
  ]
  node [
    id 677
    label "rela"
  ]
  node [
    id 678
    label "kosmologia"
  ]
  node [
    id 679
    label "kosmogonia"
  ]
  node [
    id 680
    label "nawraca&#263;"
  ]
  node [
    id 681
    label "mistyka"
  ]
  node [
    id 682
    label "staro&#347;cina_weselna"
  ]
  node [
    id 683
    label "folklor"
  ]
  node [
    id 684
    label "objawienie"
  ]
  node [
    id 685
    label "dorobek"
  ]
  node [
    id 686
    label "tworzenie"
  ]
  node [
    id 687
    label "kreacja"
  ]
  node [
    id 688
    label "creation"
  ]
  node [
    id 689
    label "zaj&#281;cie"
  ]
  node [
    id 690
    label "instytucja"
  ]
  node [
    id 691
    label "tajniki"
  ]
  node [
    id 692
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 693
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 694
    label "zaplecze"
  ]
  node [
    id 695
    label "pomieszczenie"
  ]
  node [
    id 696
    label "zlewozmywak"
  ]
  node [
    id 697
    label "gotowa&#263;"
  ]
  node [
    id 698
    label "ciemna_materia"
  ]
  node [
    id 699
    label "planeta"
  ]
  node [
    id 700
    label "ekosfera"
  ]
  node [
    id 701
    label "przestrze&#324;"
  ]
  node [
    id 702
    label "czarna_dziura"
  ]
  node [
    id 703
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 704
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 705
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 706
    label "kosmos"
  ]
  node [
    id 707
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 708
    label "poprawno&#347;&#263;"
  ]
  node [
    id 709
    label "og&#322;ada"
  ]
  node [
    id 710
    label "service"
  ]
  node [
    id 711
    label "stosowno&#347;&#263;"
  ]
  node [
    id 712
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 713
    label "Ukraina"
  ]
  node [
    id 714
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 715
    label "blok_wschodni"
  ]
  node [
    id 716
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 717
    label "wsch&#243;d"
  ]
  node [
    id 718
    label "Europa_Wschodnia"
  ]
  node [
    id 719
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 720
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 721
    label "treat"
  ]
  node [
    id 722
    label "czerpa&#263;"
  ]
  node [
    id 723
    label "bra&#263;"
  ]
  node [
    id 724
    label "go"
  ]
  node [
    id 725
    label "handle"
  ]
  node [
    id 726
    label "wzbudza&#263;"
  ]
  node [
    id 727
    label "ogarnia&#263;"
  ]
  node [
    id 728
    label "bang"
  ]
  node [
    id 729
    label "wzi&#261;&#263;"
  ]
  node [
    id 730
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 731
    label "stimulate"
  ]
  node [
    id 732
    label "ogarn&#261;&#263;"
  ]
  node [
    id 733
    label "wzbudzi&#263;"
  ]
  node [
    id 734
    label "thrill"
  ]
  node [
    id 735
    label "czerpanie"
  ]
  node [
    id 736
    label "acquisition"
  ]
  node [
    id 737
    label "branie"
  ]
  node [
    id 738
    label "caparison"
  ]
  node [
    id 739
    label "wzbudzanie"
  ]
  node [
    id 740
    label "ogarnianie"
  ]
  node [
    id 741
    label "wra&#380;enie"
  ]
  node [
    id 742
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 743
    label "interception"
  ]
  node [
    id 744
    label "wzbudzenie"
  ]
  node [
    id 745
    label "emotion"
  ]
  node [
    id 746
    label "zaczerpni&#281;cie"
  ]
  node [
    id 747
    label "wzi&#281;cie"
  ]
  node [
    id 748
    label "uprawa"
  ]
  node [
    id 749
    label "komcio"
  ]
  node [
    id 750
    label "blogosfera"
  ]
  node [
    id 751
    label "pami&#281;tnik"
  ]
  node [
    id 752
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 753
    label "pami&#261;tka"
  ]
  node [
    id 754
    label "notes"
  ]
  node [
    id 755
    label "zapiski"
  ]
  node [
    id 756
    label "raptularz"
  ]
  node [
    id 757
    label "album"
  ]
  node [
    id 758
    label "utw&#243;r_epicki"
  ]
  node [
    id 759
    label "kartka"
  ]
  node [
    id 760
    label "logowanie"
  ]
  node [
    id 761
    label "plik"
  ]
  node [
    id 762
    label "s&#261;d"
  ]
  node [
    id 763
    label "adres_internetowy"
  ]
  node [
    id 764
    label "linia"
  ]
  node [
    id 765
    label "serwis_internetowy"
  ]
  node [
    id 766
    label "posta&#263;"
  ]
  node [
    id 767
    label "bok"
  ]
  node [
    id 768
    label "skr&#281;canie"
  ]
  node [
    id 769
    label "skr&#281;ca&#263;"
  ]
  node [
    id 770
    label "orientowanie"
  ]
  node [
    id 771
    label "skr&#281;ci&#263;"
  ]
  node [
    id 772
    label "uj&#281;cie"
  ]
  node [
    id 773
    label "zorientowanie"
  ]
  node [
    id 774
    label "ty&#322;"
  ]
  node [
    id 775
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 776
    label "fragment"
  ]
  node [
    id 777
    label "layout"
  ]
  node [
    id 778
    label "zorientowa&#263;"
  ]
  node [
    id 779
    label "pagina"
  ]
  node [
    id 780
    label "podmiot"
  ]
  node [
    id 781
    label "g&#243;ra"
  ]
  node [
    id 782
    label "orientowa&#263;"
  ]
  node [
    id 783
    label "voice"
  ]
  node [
    id 784
    label "orientacja"
  ]
  node [
    id 785
    label "prz&#243;d"
  ]
  node [
    id 786
    label "internet"
  ]
  node [
    id 787
    label "powierzchnia"
  ]
  node [
    id 788
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 789
    label "skr&#281;cenie"
  ]
  node [
    id 790
    label "komentarz"
  ]
  node [
    id 791
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 792
    label "Jakub_Wujek"
  ]
  node [
    id 793
    label "Jan_Czeczot"
  ]
  node [
    id 794
    label "przek&#322;adacz"
  ]
  node [
    id 795
    label "przek&#322;adowca"
  ]
  node [
    id 796
    label "aplikacja"
  ]
  node [
    id 797
    label "interpretator"
  ]
  node [
    id 798
    label "interpreter"
  ]
  node [
    id 799
    label "uczony"
  ]
  node [
    id 800
    label "autor"
  ]
  node [
    id 801
    label "aktor"
  ]
  node [
    id 802
    label "artysta"
  ]
  node [
    id 803
    label "pismo"
  ]
  node [
    id 804
    label "application"
  ]
  node [
    id 805
    label "naszycie"
  ]
  node [
    id 806
    label "wz&#243;r"
  ]
  node [
    id 807
    label "applique"
  ]
  node [
    id 808
    label "praktyka"
  ]
  node [
    id 809
    label "ozdoba"
  ]
  node [
    id 810
    label "urz&#261;dzenie"
  ]
  node [
    id 811
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 812
    label "time"
  ]
  node [
    id 813
    label "czas"
  ]
  node [
    id 814
    label "poprzedzanie"
  ]
  node [
    id 815
    label "czasoprzestrze&#324;"
  ]
  node [
    id 816
    label "laba"
  ]
  node [
    id 817
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 818
    label "chronometria"
  ]
  node [
    id 819
    label "rachuba_czasu"
  ]
  node [
    id 820
    label "przep&#322;ywanie"
  ]
  node [
    id 821
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 822
    label "czasokres"
  ]
  node [
    id 823
    label "odczyt"
  ]
  node [
    id 824
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 825
    label "dzieje"
  ]
  node [
    id 826
    label "poprzedzenie"
  ]
  node [
    id 827
    label "trawienie"
  ]
  node [
    id 828
    label "pochodzi&#263;"
  ]
  node [
    id 829
    label "period"
  ]
  node [
    id 830
    label "okres_czasu"
  ]
  node [
    id 831
    label "poprzedza&#263;"
  ]
  node [
    id 832
    label "schy&#322;ek"
  ]
  node [
    id 833
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 834
    label "odwlekanie_si&#281;"
  ]
  node [
    id 835
    label "zegar"
  ]
  node [
    id 836
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 837
    label "czwarty_wymiar"
  ]
  node [
    id 838
    label "pochodzenie"
  ]
  node [
    id 839
    label "Zeitgeist"
  ]
  node [
    id 840
    label "trawi&#263;"
  ]
  node [
    id 841
    label "pogoda"
  ]
  node [
    id 842
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 843
    label "poprzedzi&#263;"
  ]
  node [
    id 844
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 845
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 846
    label "time_period"
  ]
  node [
    id 847
    label "realny"
  ]
  node [
    id 848
    label "du&#380;y"
  ]
  node [
    id 849
    label "dono&#347;ny"
  ]
  node [
    id 850
    label "silny"
  ]
  node [
    id 851
    label "istotnie"
  ]
  node [
    id 852
    label "znaczny"
  ]
  node [
    id 853
    label "eksponowany"
  ]
  node [
    id 854
    label "doros&#322;y"
  ]
  node [
    id 855
    label "niema&#322;o"
  ]
  node [
    id 856
    label "wiele"
  ]
  node [
    id 857
    label "rozwini&#281;ty"
  ]
  node [
    id 858
    label "dorodny"
  ]
  node [
    id 859
    label "wa&#380;ny"
  ]
  node [
    id 860
    label "prawdziwy"
  ]
  node [
    id 861
    label "du&#380;o"
  ]
  node [
    id 862
    label "podobny"
  ]
  node [
    id 863
    label "mo&#380;liwy"
  ]
  node [
    id 864
    label "realnie"
  ]
  node [
    id 865
    label "krzepienie"
  ]
  node [
    id 866
    label "&#380;ywotny"
  ]
  node [
    id 867
    label "mocny"
  ]
  node [
    id 868
    label "pokrzepienie"
  ]
  node [
    id 869
    label "zdecydowany"
  ]
  node [
    id 870
    label "niepodwa&#380;alny"
  ]
  node [
    id 871
    label "przekonuj&#261;cy"
  ]
  node [
    id 872
    label "wytrzyma&#322;y"
  ]
  node [
    id 873
    label "konkretny"
  ]
  node [
    id 874
    label "zdrowy"
  ]
  node [
    id 875
    label "silnie"
  ]
  node [
    id 876
    label "meflochina"
  ]
  node [
    id 877
    label "zajebisty"
  ]
  node [
    id 878
    label "znacznie"
  ]
  node [
    id 879
    label "zauwa&#380;alny"
  ]
  node [
    id 880
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 881
    label "gromowy"
  ]
  node [
    id 882
    label "dono&#347;nie"
  ]
  node [
    id 883
    label "g&#322;o&#347;ny"
  ]
  node [
    id 884
    label "importantly"
  ]
  node [
    id 885
    label "plon"
  ]
  node [
    id 886
    label "surrender"
  ]
  node [
    id 887
    label "kojarzy&#263;"
  ]
  node [
    id 888
    label "impart"
  ]
  node [
    id 889
    label "dawa&#263;"
  ]
  node [
    id 890
    label "reszta"
  ]
  node [
    id 891
    label "wiano"
  ]
  node [
    id 892
    label "produkcja"
  ]
  node [
    id 893
    label "wprowadza&#263;"
  ]
  node [
    id 894
    label "podawa&#263;"
  ]
  node [
    id 895
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 896
    label "ujawnia&#263;"
  ]
  node [
    id 897
    label "placard"
  ]
  node [
    id 898
    label "powierza&#263;"
  ]
  node [
    id 899
    label "denuncjowa&#263;"
  ]
  node [
    id 900
    label "tajemnica"
  ]
  node [
    id 901
    label "panna_na_wydaniu"
  ]
  node [
    id 902
    label "wytwarza&#263;"
  ]
  node [
    id 903
    label "train"
  ]
  node [
    id 904
    label "przekazywa&#263;"
  ]
  node [
    id 905
    label "dostarcza&#263;"
  ]
  node [
    id 906
    label "&#322;adowa&#263;"
  ]
  node [
    id 907
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 908
    label "przeznacza&#263;"
  ]
  node [
    id 909
    label "traktowa&#263;"
  ]
  node [
    id 910
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 911
    label "obiecywa&#263;"
  ]
  node [
    id 912
    label "odst&#281;powa&#263;"
  ]
  node [
    id 913
    label "tender"
  ]
  node [
    id 914
    label "rap"
  ]
  node [
    id 915
    label "umieszcza&#263;"
  ]
  node [
    id 916
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 917
    label "t&#322;uc"
  ]
  node [
    id 918
    label "render"
  ]
  node [
    id 919
    label "wpiernicza&#263;"
  ]
  node [
    id 920
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 921
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 922
    label "p&#322;aci&#263;"
  ]
  node [
    id 923
    label "hold_out"
  ]
  node [
    id 924
    label "nalewa&#263;"
  ]
  node [
    id 925
    label "zezwala&#263;"
  ]
  node [
    id 926
    label "hold"
  ]
  node [
    id 927
    label "rynek"
  ]
  node [
    id 928
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 929
    label "wprawia&#263;"
  ]
  node [
    id 930
    label "zaczyna&#263;"
  ]
  node [
    id 931
    label "wpisywa&#263;"
  ]
  node [
    id 932
    label "wchodzi&#263;"
  ]
  node [
    id 933
    label "take"
  ]
  node [
    id 934
    label "zapoznawa&#263;"
  ]
  node [
    id 935
    label "inflict"
  ]
  node [
    id 936
    label "schodzi&#263;"
  ]
  node [
    id 937
    label "induct"
  ]
  node [
    id 938
    label "begin"
  ]
  node [
    id 939
    label "doprowadza&#263;"
  ]
  node [
    id 940
    label "create"
  ]
  node [
    id 941
    label "donosi&#263;"
  ]
  node [
    id 942
    label "inform"
  ]
  node [
    id 943
    label "demaskator"
  ]
  node [
    id 944
    label "dostrzega&#263;"
  ]
  node [
    id 945
    label "objawia&#263;"
  ]
  node [
    id 946
    label "unwrap"
  ]
  node [
    id 947
    label "informowa&#263;"
  ]
  node [
    id 948
    label "indicate"
  ]
  node [
    id 949
    label "zaskakiwa&#263;"
  ]
  node [
    id 950
    label "cover"
  ]
  node [
    id 951
    label "swat"
  ]
  node [
    id 952
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 953
    label "relate"
  ]
  node [
    id 954
    label "wyznawa&#263;"
  ]
  node [
    id 955
    label "oddawa&#263;"
  ]
  node [
    id 956
    label "confide"
  ]
  node [
    id 957
    label "zleca&#263;"
  ]
  node [
    id 958
    label "ufa&#263;"
  ]
  node [
    id 959
    label "command"
  ]
  node [
    id 960
    label "grant"
  ]
  node [
    id 961
    label "deal"
  ]
  node [
    id 962
    label "stawia&#263;"
  ]
  node [
    id 963
    label "rozgrywa&#263;"
  ]
  node [
    id 964
    label "kelner"
  ]
  node [
    id 965
    label "faszerowa&#263;"
  ]
  node [
    id 966
    label "serwowa&#263;"
  ]
  node [
    id 967
    label "kwota"
  ]
  node [
    id 968
    label "wydanie"
  ]
  node [
    id 969
    label "remainder"
  ]
  node [
    id 970
    label "pozosta&#322;y"
  ]
  node [
    id 971
    label "wyda&#263;"
  ]
  node [
    id 972
    label "impreza"
  ]
  node [
    id 973
    label "tingel-tangel"
  ]
  node [
    id 974
    label "numer"
  ]
  node [
    id 975
    label "monta&#380;"
  ]
  node [
    id 976
    label "postprodukcja"
  ]
  node [
    id 977
    label "performance"
  ]
  node [
    id 978
    label "fabrication"
  ]
  node [
    id 979
    label "product"
  ]
  node [
    id 980
    label "uzysk"
  ]
  node [
    id 981
    label "rozw&#243;j"
  ]
  node [
    id 982
    label "odtworzenie"
  ]
  node [
    id 983
    label "trema"
  ]
  node [
    id 984
    label "kooperowa&#263;"
  ]
  node [
    id 985
    label "return"
  ]
  node [
    id 986
    label "metr"
  ]
  node [
    id 987
    label "rezultat"
  ]
  node [
    id 988
    label "naturalia"
  ]
  node [
    id 989
    label "wypaplanie"
  ]
  node [
    id 990
    label "enigmat"
  ]
  node [
    id 991
    label "wiedza"
  ]
  node [
    id 992
    label "zachowywanie"
  ]
  node [
    id 993
    label "secret"
  ]
  node [
    id 994
    label "obowi&#261;zek"
  ]
  node [
    id 995
    label "dyskrecja"
  ]
  node [
    id 996
    label "informacja"
  ]
  node [
    id 997
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 998
    label "taj&#324;"
  ]
  node [
    id 999
    label "zachowa&#263;"
  ]
  node [
    id 1000
    label "zachowywa&#263;"
  ]
  node [
    id 1001
    label "posa&#380;ek"
  ]
  node [
    id 1002
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 1003
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 1004
    label "debit"
  ]
  node [
    id 1005
    label "redaktor"
  ]
  node [
    id 1006
    label "druk"
  ]
  node [
    id 1007
    label "publikacja"
  ]
  node [
    id 1008
    label "redakcja"
  ]
  node [
    id 1009
    label "szata_graficzna"
  ]
  node [
    id 1010
    label "firma"
  ]
  node [
    id 1011
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 1012
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 1013
    label "poster"
  ]
  node [
    id 1014
    label "phone"
  ]
  node [
    id 1015
    label "intonacja"
  ]
  node [
    id 1016
    label "note"
  ]
  node [
    id 1017
    label "onomatopeja"
  ]
  node [
    id 1018
    label "modalizm"
  ]
  node [
    id 1019
    label "nadlecenie"
  ]
  node [
    id 1020
    label "sound"
  ]
  node [
    id 1021
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1022
    label "solmizacja"
  ]
  node [
    id 1023
    label "seria"
  ]
  node [
    id 1024
    label "dobiec"
  ]
  node [
    id 1025
    label "transmiter"
  ]
  node [
    id 1026
    label "heksachord"
  ]
  node [
    id 1027
    label "akcent"
  ]
  node [
    id 1028
    label "repetycja"
  ]
  node [
    id 1029
    label "brzmienie"
  ]
  node [
    id 1030
    label "liczba_kwantowa"
  ]
  node [
    id 1031
    label "kosmetyk"
  ]
  node [
    id 1032
    label "ciasto"
  ]
  node [
    id 1033
    label "aromat"
  ]
  node [
    id 1034
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 1035
    label "puff"
  ]
  node [
    id 1036
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 1037
    label "przyprawa"
  ]
  node [
    id 1038
    label "upojno&#347;&#263;"
  ]
  node [
    id 1039
    label "owiewanie"
  ]
  node [
    id 1040
    label "smak"
  ]
  node [
    id 1041
    label "og&#243;lnie"
  ]
  node [
    id 1042
    label "zbiorowy"
  ]
  node [
    id 1043
    label "og&#243;&#322;owy"
  ]
  node [
    id 1044
    label "nadrz&#281;dny"
  ]
  node [
    id 1045
    label "ca&#322;y"
  ]
  node [
    id 1046
    label "kompletny"
  ]
  node [
    id 1047
    label "&#322;&#261;czny"
  ]
  node [
    id 1048
    label "powszechnie"
  ]
  node [
    id 1049
    label "jedyny"
  ]
  node [
    id 1050
    label "zdr&#243;w"
  ]
  node [
    id 1051
    label "calu&#347;ko"
  ]
  node [
    id 1052
    label "&#380;ywy"
  ]
  node [
    id 1053
    label "ca&#322;o"
  ]
  node [
    id 1054
    label "&#322;&#261;cznie"
  ]
  node [
    id 1055
    label "zbiorczy"
  ]
  node [
    id 1056
    label "pierwszorz&#281;dny"
  ]
  node [
    id 1057
    label "nadrz&#281;dnie"
  ]
  node [
    id 1058
    label "wsp&#243;lny"
  ]
  node [
    id 1059
    label "zbiorowo"
  ]
  node [
    id 1060
    label "kompletnie"
  ]
  node [
    id 1061
    label "zupe&#322;ny"
  ]
  node [
    id 1062
    label "w_pizdu"
  ]
  node [
    id 1063
    label "posp&#243;lnie"
  ]
  node [
    id 1064
    label "generalny"
  ]
  node [
    id 1065
    label "powszechny"
  ]
  node [
    id 1066
    label "cz&#281;sto"
  ]
  node [
    id 1067
    label "patologia"
  ]
  node [
    id 1068
    label "bribery"
  ]
  node [
    id 1069
    label "przest&#281;pstwo"
  ]
  node [
    id 1070
    label "brudny"
  ]
  node [
    id 1071
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 1072
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 1073
    label "crime"
  ]
  node [
    id 1074
    label "sprawstwo"
  ]
  node [
    id 1075
    label "ognisko"
  ]
  node [
    id 1076
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1077
    label "powalenie"
  ]
  node [
    id 1078
    label "odezwanie_si&#281;"
  ]
  node [
    id 1079
    label "atakowanie"
  ]
  node [
    id 1080
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1081
    label "patomorfologia"
  ]
  node [
    id 1082
    label "grupa_ryzyka"
  ]
  node [
    id 1083
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1084
    label "patolnia"
  ]
  node [
    id 1085
    label "nabawienie_si&#281;"
  ]
  node [
    id 1086
    label "przemoc"
  ]
  node [
    id 1087
    label "&#347;rodowisko"
  ]
  node [
    id 1088
    label "inkubacja"
  ]
  node [
    id 1089
    label "medycyna"
  ]
  node [
    id 1090
    label "szambo"
  ]
  node [
    id 1091
    label "gangsterski"
  ]
  node [
    id 1092
    label "fizjologia_patologiczna"
  ]
  node [
    id 1093
    label "kryzys"
  ]
  node [
    id 1094
    label "powali&#263;"
  ]
  node [
    id 1095
    label "remisja"
  ]
  node [
    id 1096
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1097
    label "zajmowa&#263;"
  ]
  node [
    id 1098
    label "zaburzenie"
  ]
  node [
    id 1099
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1100
    label "neuropatologia"
  ]
  node [
    id 1101
    label "aspo&#322;eczny"
  ]
  node [
    id 1102
    label "badanie_histopatologiczne"
  ]
  node [
    id 1103
    label "abnormality"
  ]
  node [
    id 1104
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1105
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1106
    label "patogeneza"
  ]
  node [
    id 1107
    label "psychopatologia"
  ]
  node [
    id 1108
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1109
    label "paleopatologia"
  ]
  node [
    id 1110
    label "logopatologia"
  ]
  node [
    id 1111
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1112
    label "immunopatologia"
  ]
  node [
    id 1113
    label "osteopatologia"
  ]
  node [
    id 1114
    label "odzywanie_si&#281;"
  ]
  node [
    id 1115
    label "diagnoza"
  ]
  node [
    id 1116
    label "atakowa&#263;"
  ]
  node [
    id 1117
    label "histopatologia"
  ]
  node [
    id 1118
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1119
    label "nabawianie_si&#281;"
  ]
  node [
    id 1120
    label "underworld"
  ]
  node [
    id 1121
    label "meteoropatologia"
  ]
  node [
    id 1122
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1123
    label "zajmowanie"
  ]
  node [
    id 1124
    label "j&#261;dro"
  ]
  node [
    id 1125
    label "systemik"
  ]
  node [
    id 1126
    label "rozprz&#261;c"
  ]
  node [
    id 1127
    label "oprogramowanie"
  ]
  node [
    id 1128
    label "systemat"
  ]
  node [
    id 1129
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1130
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1131
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1132
    label "struktura"
  ]
  node [
    id 1133
    label "usenet"
  ]
  node [
    id 1134
    label "porz&#261;dek"
  ]
  node [
    id 1135
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1136
    label "przyn&#281;ta"
  ]
  node [
    id 1137
    label "p&#322;&#243;d"
  ]
  node [
    id 1138
    label "net"
  ]
  node [
    id 1139
    label "w&#281;dkarstwo"
  ]
  node [
    id 1140
    label "eratem"
  ]
  node [
    id 1141
    label "oddzia&#322;"
  ]
  node [
    id 1142
    label "doktryna"
  ]
  node [
    id 1143
    label "pulpit"
  ]
  node [
    id 1144
    label "konstelacja"
  ]
  node [
    id 1145
    label "jednostka_geologiczna"
  ]
  node [
    id 1146
    label "o&#347;"
  ]
  node [
    id 1147
    label "podsystem"
  ]
  node [
    id 1148
    label "metoda"
  ]
  node [
    id 1149
    label "ryba"
  ]
  node [
    id 1150
    label "Leopard"
  ]
  node [
    id 1151
    label "Android"
  ]
  node [
    id 1152
    label "cybernetyk"
  ]
  node [
    id 1153
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1154
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1155
    label "method"
  ]
  node [
    id 1156
    label "sk&#322;ad"
  ]
  node [
    id 1157
    label "podstawa"
  ]
  node [
    id 1158
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1159
    label "pot&#281;ga"
  ]
  node [
    id 1160
    label "documentation"
  ]
  node [
    id 1161
    label "column"
  ]
  node [
    id 1162
    label "zasadzi&#263;"
  ]
  node [
    id 1163
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1164
    label "punkt_odniesienia"
  ]
  node [
    id 1165
    label "zasadzenie"
  ]
  node [
    id 1166
    label "d&#243;&#322;"
  ]
  node [
    id 1167
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1168
    label "background"
  ]
  node [
    id 1169
    label "podstawowy"
  ]
  node [
    id 1170
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1171
    label "strategia"
  ]
  node [
    id 1172
    label "pomys&#322;"
  ]
  node [
    id 1173
    label "&#347;ciana"
  ]
  node [
    id 1174
    label "narz&#281;dzie"
  ]
  node [
    id 1175
    label "nature"
  ]
  node [
    id 1176
    label "relacja"
  ]
  node [
    id 1177
    label "zasada"
  ]
  node [
    id 1178
    label "styl_architektoniczny"
  ]
  node [
    id 1179
    label "normalizacja"
  ]
  node [
    id 1180
    label "series"
  ]
  node [
    id 1181
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1182
    label "uprawianie"
  ]
  node [
    id 1183
    label "collection"
  ]
  node [
    id 1184
    label "dane"
  ]
  node [
    id 1185
    label "pakiet_klimatyczny"
  ]
  node [
    id 1186
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1187
    label "sum"
  ]
  node [
    id 1188
    label "pos&#322;uchanie"
  ]
  node [
    id 1189
    label "skumanie"
  ]
  node [
    id 1190
    label "wytw&#243;r"
  ]
  node [
    id 1191
    label "teoria"
  ]
  node [
    id 1192
    label "clasp"
  ]
  node [
    id 1193
    label "przem&#243;wienie"
  ]
  node [
    id 1194
    label "mechanika"
  ]
  node [
    id 1195
    label "konstrukcja"
  ]
  node [
    id 1196
    label "system_komputerowy"
  ]
  node [
    id 1197
    label "sprz&#281;t"
  ]
  node [
    id 1198
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1199
    label "moczownik"
  ]
  node [
    id 1200
    label "embryo"
  ]
  node [
    id 1201
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1202
    label "zarodek"
  ]
  node [
    id 1203
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1204
    label "latawiec"
  ]
  node [
    id 1205
    label "reengineering"
  ]
  node [
    id 1206
    label "integer"
  ]
  node [
    id 1207
    label "liczba"
  ]
  node [
    id 1208
    label "zlewanie_si&#281;"
  ]
  node [
    id 1209
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1210
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1211
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1212
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 1213
    label "grupa_dyskusyjna"
  ]
  node [
    id 1214
    label "doctrine"
  ]
  node [
    id 1215
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 1216
    label "kr&#281;gowiec"
  ]
  node [
    id 1217
    label "doniczkowiec"
  ]
  node [
    id 1218
    label "mi&#281;so"
  ]
  node [
    id 1219
    label "patroszy&#263;"
  ]
  node [
    id 1220
    label "rakowato&#347;&#263;"
  ]
  node [
    id 1221
    label "ryby"
  ]
  node [
    id 1222
    label "fish"
  ]
  node [
    id 1223
    label "linia_boczna"
  ]
  node [
    id 1224
    label "tar&#322;o"
  ]
  node [
    id 1225
    label "wyrostek_filtracyjny"
  ]
  node [
    id 1226
    label "m&#281;tnooki"
  ]
  node [
    id 1227
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 1228
    label "pokrywa_skrzelowa"
  ]
  node [
    id 1229
    label "ikra"
  ]
  node [
    id 1230
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 1231
    label "szczelina_skrzelowa"
  ]
  node [
    id 1232
    label "urozmaicenie"
  ]
  node [
    id 1233
    label "pu&#322;apka"
  ]
  node [
    id 1234
    label "pon&#281;ta"
  ]
  node [
    id 1235
    label "wabik"
  ]
  node [
    id 1236
    label "blat"
  ]
  node [
    id 1237
    label "interfejs"
  ]
  node [
    id 1238
    label "okno"
  ]
  node [
    id 1239
    label "obszar"
  ]
  node [
    id 1240
    label "ikona"
  ]
  node [
    id 1241
    label "system_operacyjny"
  ]
  node [
    id 1242
    label "mebel"
  ]
  node [
    id 1243
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1244
    label "oswobodzi&#263;"
  ]
  node [
    id 1245
    label "os&#322;abi&#263;"
  ]
  node [
    id 1246
    label "disengage"
  ]
  node [
    id 1247
    label "zdezorganizowa&#263;"
  ]
  node [
    id 1248
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1249
    label "reakcja"
  ]
  node [
    id 1250
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1251
    label "pochowanie"
  ]
  node [
    id 1252
    label "zdyscyplinowanie"
  ]
  node [
    id 1253
    label "post&#261;pienie"
  ]
  node [
    id 1254
    label "post"
  ]
  node [
    id 1255
    label "bearing"
  ]
  node [
    id 1256
    label "zwierz&#281;"
  ]
  node [
    id 1257
    label "behawior"
  ]
  node [
    id 1258
    label "observation"
  ]
  node [
    id 1259
    label "dieta"
  ]
  node [
    id 1260
    label "podtrzymanie"
  ]
  node [
    id 1261
    label "etolog"
  ]
  node [
    id 1262
    label "przechowanie"
  ]
  node [
    id 1263
    label "zrobienie"
  ]
  node [
    id 1264
    label "relaxation"
  ]
  node [
    id 1265
    label "os&#322;abienie"
  ]
  node [
    id 1266
    label "oswobodzenie"
  ]
  node [
    id 1267
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1268
    label "zdezorganizowanie"
  ]
  node [
    id 1269
    label "naukowiec"
  ]
  node [
    id 1270
    label "provider"
  ]
  node [
    id 1271
    label "b&#322;&#261;d"
  ]
  node [
    id 1272
    label "hipertekst"
  ]
  node [
    id 1273
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1274
    label "mem"
  ]
  node [
    id 1275
    label "grooming"
  ]
  node [
    id 1276
    label "gra_sieciowa"
  ]
  node [
    id 1277
    label "media"
  ]
  node [
    id 1278
    label "biznes_elektroniczny"
  ]
  node [
    id 1279
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1280
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1281
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1282
    label "netbook"
  ]
  node [
    id 1283
    label "e-hazard"
  ]
  node [
    id 1284
    label "podcast"
  ]
  node [
    id 1285
    label "prezenter"
  ]
  node [
    id 1286
    label "typ"
  ]
  node [
    id 1287
    label "mildew"
  ]
  node [
    id 1288
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1289
    label "motif"
  ]
  node [
    id 1290
    label "pozowanie"
  ]
  node [
    id 1291
    label "ideal"
  ]
  node [
    id 1292
    label "matryca"
  ]
  node [
    id 1293
    label "adaptation"
  ]
  node [
    id 1294
    label "pozowa&#263;"
  ]
  node [
    id 1295
    label "imitacja"
  ]
  node [
    id 1296
    label "orygina&#322;"
  ]
  node [
    id 1297
    label "facet"
  ]
  node [
    id 1298
    label "miniatura"
  ]
  node [
    id 1299
    label "zesp&#243;&#322;"
  ]
  node [
    id 1300
    label "podejrzany"
  ]
  node [
    id 1301
    label "s&#261;downictwo"
  ]
  node [
    id 1302
    label "biuro"
  ]
  node [
    id 1303
    label "court"
  ]
  node [
    id 1304
    label "bronienie"
  ]
  node [
    id 1305
    label "urz&#261;d"
  ]
  node [
    id 1306
    label "oskar&#380;yciel"
  ]
  node [
    id 1307
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1308
    label "skazany"
  ]
  node [
    id 1309
    label "post&#281;powanie"
  ]
  node [
    id 1310
    label "broni&#263;"
  ]
  node [
    id 1311
    label "my&#347;l"
  ]
  node [
    id 1312
    label "pods&#261;dny"
  ]
  node [
    id 1313
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1314
    label "obrona"
  ]
  node [
    id 1315
    label "wypowied&#378;"
  ]
  node [
    id 1316
    label "antylogizm"
  ]
  node [
    id 1317
    label "konektyw"
  ]
  node [
    id 1318
    label "&#347;wiadek"
  ]
  node [
    id 1319
    label "procesowicz"
  ]
  node [
    id 1320
    label "lias"
  ]
  node [
    id 1321
    label "dzia&#322;"
  ]
  node [
    id 1322
    label "klasa"
  ]
  node [
    id 1323
    label "filia"
  ]
  node [
    id 1324
    label "malm"
  ]
  node [
    id 1325
    label "whole"
  ]
  node [
    id 1326
    label "dogger"
  ]
  node [
    id 1327
    label "promocja"
  ]
  node [
    id 1328
    label "kurs"
  ]
  node [
    id 1329
    label "bank"
  ]
  node [
    id 1330
    label "ajencja"
  ]
  node [
    id 1331
    label "wojsko"
  ]
  node [
    id 1332
    label "siedziba"
  ]
  node [
    id 1333
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 1334
    label "agencja"
  ]
  node [
    id 1335
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 1336
    label "szpital"
  ]
  node [
    id 1337
    label "algebra_liniowa"
  ]
  node [
    id 1338
    label "macierz_j&#261;drowa"
  ]
  node [
    id 1339
    label "atom"
  ]
  node [
    id 1340
    label "nukleon"
  ]
  node [
    id 1341
    label "kariokineza"
  ]
  node [
    id 1342
    label "core"
  ]
  node [
    id 1343
    label "chemia_j&#261;drowa"
  ]
  node [
    id 1344
    label "anorchizm"
  ]
  node [
    id 1345
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 1346
    label "nasieniak"
  ]
  node [
    id 1347
    label "wn&#281;trostwo"
  ]
  node [
    id 1348
    label "ziarno"
  ]
  node [
    id 1349
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 1350
    label "j&#261;derko"
  ]
  node [
    id 1351
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 1352
    label "jajo"
  ]
  node [
    id 1353
    label "chromosom"
  ]
  node [
    id 1354
    label "organellum"
  ]
  node [
    id 1355
    label "moszna"
  ]
  node [
    id 1356
    label "przeciwobraz"
  ]
  node [
    id 1357
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 1358
    label "&#347;rodek"
  ]
  node [
    id 1359
    label "protoplazma"
  ]
  node [
    id 1360
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 1361
    label "nukleosynteza"
  ]
  node [
    id 1362
    label "subsystem"
  ]
  node [
    id 1363
    label "granica"
  ]
  node [
    id 1364
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 1365
    label "suport"
  ]
  node [
    id 1366
    label "prosta"
  ]
  node [
    id 1367
    label "o&#347;rodek"
  ]
  node [
    id 1368
    label "eonotem"
  ]
  node [
    id 1369
    label "constellation"
  ]
  node [
    id 1370
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 1371
    label "Ptak_Rajski"
  ]
  node [
    id 1372
    label "W&#281;&#380;ownik"
  ]
  node [
    id 1373
    label "Panna"
  ]
  node [
    id 1374
    label "W&#261;&#380;"
  ]
  node [
    id 1375
    label "blokada"
  ]
  node [
    id 1376
    label "hurtownia"
  ]
  node [
    id 1377
    label "pole"
  ]
  node [
    id 1378
    label "pas"
  ]
  node [
    id 1379
    label "basic"
  ]
  node [
    id 1380
    label "sk&#322;adnik"
  ]
  node [
    id 1381
    label "sklep"
  ]
  node [
    id 1382
    label "obr&#243;bka"
  ]
  node [
    id 1383
    label "constitution"
  ]
  node [
    id 1384
    label "fabryka"
  ]
  node [
    id 1385
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1386
    label "rank_and_file"
  ]
  node [
    id 1387
    label "set"
  ]
  node [
    id 1388
    label "tabulacja"
  ]
  node [
    id 1389
    label "tekst"
  ]
  node [
    id 1390
    label "internowanie"
  ]
  node [
    id 1391
    label "prorz&#261;dowy"
  ]
  node [
    id 1392
    label "internowa&#263;"
  ]
  node [
    id 1393
    label "politycznie"
  ]
  node [
    id 1394
    label "wi&#281;zie&#324;"
  ]
  node [
    id 1395
    label "ideologiczny"
  ]
  node [
    id 1396
    label "pierdel"
  ]
  node [
    id 1397
    label "&#321;ubianka"
  ]
  node [
    id 1398
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 1399
    label "kiciarz"
  ]
  node [
    id 1400
    label "ciupa"
  ]
  node [
    id 1401
    label "reedukator"
  ]
  node [
    id 1402
    label "pasiak"
  ]
  node [
    id 1403
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 1404
    label "Butyrki"
  ]
  node [
    id 1405
    label "miejsce_odosobnienia"
  ]
  node [
    id 1406
    label "ideologicznie"
  ]
  node [
    id 1407
    label "powa&#380;ny"
  ]
  node [
    id 1408
    label "internat"
  ]
  node [
    id 1409
    label "jeniec"
  ]
  node [
    id 1410
    label "zamkn&#261;&#263;"
  ]
  node [
    id 1411
    label "wi&#281;zie&#324;_polityczny"
  ]
  node [
    id 1412
    label "zamyka&#263;"
  ]
  node [
    id 1413
    label "zamykanie"
  ]
  node [
    id 1414
    label "imprisonment"
  ]
  node [
    id 1415
    label "zamkni&#281;cie"
  ]
  node [
    id 1416
    label "oportunistyczny"
  ]
  node [
    id 1417
    label "przychylny"
  ]
  node [
    id 1418
    label "prorz&#261;dowo"
  ]
  node [
    id 1419
    label "Lawrence"
  ]
  node [
    id 1420
    label "Lessig"
  ]
  node [
    id 1421
    label "zjednoczy&#263;"
  ]
  node [
    id 1422
    label "CC"
  ]
  node [
    id 1423
    label "polski"
  ]
  node [
    id 1424
    label "Creative"
  ]
  node [
    id 1425
    label "Commons"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 213
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 222
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 269
  ]
  edge [
    source 14
    target 270
  ]
  edge [
    source 14
    target 271
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 273
  ]
  edge [
    source 14
    target 274
  ]
  edge [
    source 14
    target 275
  ]
  edge [
    source 14
    target 276
  ]
  edge [
    source 14
    target 277
  ]
  edge [
    source 14
    target 278
  ]
  edge [
    source 14
    target 279
  ]
  edge [
    source 14
    target 280
  ]
  edge [
    source 14
    target 281
  ]
  edge [
    source 14
    target 282
  ]
  edge [
    source 14
    target 283
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 14
    target 286
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 288
  ]
  edge [
    source 14
    target 289
  ]
  edge [
    source 14
    target 290
  ]
  edge [
    source 14
    target 291
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 292
  ]
  edge [
    source 15
    target 293
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 294
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 295
  ]
  edge [
    source 15
    target 296
  ]
  edge [
    source 15
    target 297
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 298
  ]
  edge [
    source 15
    target 299
  ]
  edge [
    source 15
    target 300
  ]
  edge [
    source 15
    target 46
  ]
  edge [
    source 15
    target 301
  ]
  edge [
    source 15
    target 302
  ]
  edge [
    source 15
    target 303
  ]
  edge [
    source 15
    target 304
  ]
  edge [
    source 15
    target 305
  ]
  edge [
    source 15
    target 306
  ]
  edge [
    source 15
    target 307
  ]
  edge [
    source 15
    target 308
  ]
  edge [
    source 15
    target 309
  ]
  edge [
    source 15
    target 310
  ]
  edge [
    source 15
    target 311
  ]
  edge [
    source 15
    target 312
  ]
  edge [
    source 15
    target 313
  ]
  edge [
    source 15
    target 314
  ]
  edge [
    source 15
    target 315
  ]
  edge [
    source 15
    target 316
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 317
  ]
  edge [
    source 15
    target 318
  ]
  edge [
    source 15
    target 319
  ]
  edge [
    source 15
    target 320
  ]
  edge [
    source 15
    target 321
  ]
  edge [
    source 15
    target 322
  ]
  edge [
    source 15
    target 323
  ]
  edge [
    source 15
    target 324
  ]
  edge [
    source 15
    target 325
  ]
  edge [
    source 15
    target 326
  ]
  edge [
    source 15
    target 327
  ]
  edge [
    source 15
    target 328
  ]
  edge [
    source 15
    target 329
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 330
  ]
  edge [
    source 15
    target 331
  ]
  edge [
    source 15
    target 332
  ]
  edge [
    source 15
    target 333
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 334
  ]
  edge [
    source 16
    target 335
  ]
  edge [
    source 16
    target 336
  ]
  edge [
    source 16
    target 337
  ]
  edge [
    source 16
    target 338
  ]
  edge [
    source 16
    target 339
  ]
  edge [
    source 16
    target 340
  ]
  edge [
    source 16
    target 341
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 342
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 345
  ]
  edge [
    source 16
    target 346
  ]
  edge [
    source 16
    target 347
  ]
  edge [
    source 16
    target 348
  ]
  edge [
    source 16
    target 349
  ]
  edge [
    source 16
    target 350
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 352
  ]
  edge [
    source 16
    target 353
  ]
  edge [
    source 16
    target 354
  ]
  edge [
    source 16
    target 355
  ]
  edge [
    source 16
    target 356
  ]
  edge [
    source 16
    target 357
  ]
  edge [
    source 16
    target 358
  ]
  edge [
    source 16
    target 359
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 361
  ]
  edge [
    source 16
    target 214
  ]
  edge [
    source 16
    target 362
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 397
  ]
  edge [
    source 16
    target 398
  ]
  edge [
    source 16
    target 399
  ]
  edge [
    source 16
    target 400
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 402
  ]
  edge [
    source 16
    target 403
  ]
  edge [
    source 16
    target 404
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 406
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 418
  ]
  edge [
    source 16
    target 419
  ]
  edge [
    source 16
    target 420
  ]
  edge [
    source 16
    target 421
  ]
  edge [
    source 16
    target 422
  ]
  edge [
    source 16
    target 423
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 424
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 425
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 427
  ]
  edge [
    source 16
    target 428
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 430
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 445
  ]
  edge [
    source 16
    target 446
  ]
  edge [
    source 16
    target 447
  ]
  edge [
    source 16
    target 448
  ]
  edge [
    source 16
    target 449
  ]
  edge [
    source 16
    target 450
  ]
  edge [
    source 16
    target 451
  ]
  edge [
    source 16
    target 452
  ]
  edge [
    source 16
    target 453
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 455
  ]
  edge [
    source 16
    target 456
  ]
  edge [
    source 16
    target 457
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 459
  ]
  edge [
    source 16
    target 460
  ]
  edge [
    source 16
    target 461
  ]
  edge [
    source 16
    target 462
  ]
  edge [
    source 16
    target 463
  ]
  edge [
    source 16
    target 464
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 465
  ]
  edge [
    source 16
    target 466
  ]
  edge [
    source 16
    target 467
  ]
  edge [
    source 16
    target 468
  ]
  edge [
    source 16
    target 469
  ]
  edge [
    source 16
    target 470
  ]
  edge [
    source 16
    target 471
  ]
  edge [
    source 16
    target 472
  ]
  edge [
    source 16
    target 473
  ]
  edge [
    source 16
    target 474
  ]
  edge [
    source 16
    target 475
  ]
  edge [
    source 16
    target 476
  ]
  edge [
    source 16
    target 477
  ]
  edge [
    source 16
    target 60
  ]
  edge [
    source 16
    target 478
  ]
  edge [
    source 16
    target 479
  ]
  edge [
    source 16
    target 480
  ]
  edge [
    source 16
    target 481
  ]
  edge [
    source 16
    target 482
  ]
  edge [
    source 16
    target 483
  ]
  edge [
    source 16
    target 484
  ]
  edge [
    source 16
    target 485
  ]
  edge [
    source 16
    target 486
  ]
  edge [
    source 16
    target 487
  ]
  edge [
    source 16
    target 488
  ]
  edge [
    source 16
    target 489
  ]
  edge [
    source 16
    target 490
  ]
  edge [
    source 16
    target 491
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 493
  ]
  edge [
    source 16
    target 494
  ]
  edge [
    source 16
    target 495
  ]
  edge [
    source 16
    target 496
  ]
  edge [
    source 16
    target 497
  ]
  edge [
    source 16
    target 498
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 499
  ]
  edge [
    source 16
    target 500
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 502
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 504
  ]
  edge [
    source 16
    target 505
  ]
  edge [
    source 16
    target 506
  ]
  edge [
    source 16
    target 507
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 509
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 510
  ]
  edge [
    source 17
    target 511
  ]
  edge [
    source 17
    target 512
  ]
  edge [
    source 17
    target 513
  ]
  edge [
    source 17
    target 514
  ]
  edge [
    source 17
    target 515
  ]
  edge [
    source 17
    target 516
  ]
  edge [
    source 17
    target 517
  ]
  edge [
    source 17
    target 518
  ]
  edge [
    source 17
    target 519
  ]
  edge [
    source 17
    target 520
  ]
  edge [
    source 17
    target 521
  ]
  edge [
    source 17
    target 522
  ]
  edge [
    source 17
    target 523
  ]
  edge [
    source 17
    target 524
  ]
  edge [
    source 17
    target 525
  ]
  edge [
    source 17
    target 526
  ]
  edge [
    source 17
    target 527
  ]
  edge [
    source 17
    target 528
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 530
  ]
  edge [
    source 17
    target 531
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 485
  ]
  edge [
    source 17
    target 534
  ]
  edge [
    source 17
    target 535
  ]
  edge [
    source 17
    target 536
  ]
  edge [
    source 17
    target 537
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 539
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 541
  ]
  edge [
    source 17
    target 542
  ]
  edge [
    source 17
    target 543
  ]
  edge [
    source 17
    target 544
  ]
  edge [
    source 17
    target 545
  ]
  edge [
    source 17
    target 546
  ]
  edge [
    source 17
    target 547
  ]
  edge [
    source 17
    target 548
  ]
  edge [
    source 17
    target 549
  ]
  edge [
    source 17
    target 550
  ]
  edge [
    source 17
    target 551
  ]
  edge [
    source 17
    target 552
  ]
  edge [
    source 17
    target 553
  ]
  edge [
    source 17
    target 554
  ]
  edge [
    source 17
    target 555
  ]
  edge [
    source 17
    target 556
  ]
  edge [
    source 17
    target 557
  ]
  edge [
    source 17
    target 558
  ]
  edge [
    source 17
    target 559
  ]
  edge [
    source 17
    target 560
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 561
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 17
    target 563
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 565
  ]
  edge [
    source 17
    target 566
  ]
  edge [
    source 17
    target 567
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 376
  ]
  edge [
    source 18
    target 377
  ]
  edge [
    source 18
    target 378
  ]
  edge [
    source 18
    target 335
  ]
  edge [
    source 18
    target 379
  ]
  edge [
    source 18
    target 380
  ]
  edge [
    source 18
    target 381
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 382
  ]
  edge [
    source 18
    target 383
  ]
  edge [
    source 18
    target 385
  ]
  edge [
    source 18
    target 384
  ]
  edge [
    source 18
    target 386
  ]
  edge [
    source 18
    target 387
  ]
  edge [
    source 18
    target 388
  ]
  edge [
    source 18
    target 389
  ]
  edge [
    source 18
    target 390
  ]
  edge [
    source 18
    target 391
  ]
  edge [
    source 18
    target 392
  ]
  edge [
    source 18
    target 393
  ]
  edge [
    source 18
    target 394
  ]
  edge [
    source 18
    target 395
  ]
  edge [
    source 18
    target 396
  ]
  edge [
    source 18
    target 397
  ]
  edge [
    source 18
    target 398
  ]
  edge [
    source 18
    target 399
  ]
  edge [
    source 18
    target 400
  ]
  edge [
    source 18
    target 401
  ]
  edge [
    source 18
    target 402
  ]
  edge [
    source 18
    target 403
  ]
  edge [
    source 18
    target 404
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 345
  ]
  edge [
    source 18
    target 237
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 364
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 617
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 339
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 638
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 640
  ]
  edge [
    source 18
    target 641
  ]
  edge [
    source 18
    target 411
  ]
  edge [
    source 18
    target 642
  ]
  edge [
    source 18
    target 643
  ]
  edge [
    source 18
    target 644
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 646
  ]
  edge [
    source 18
    target 647
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 48
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 407
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 351
  ]
  edge [
    source 18
    target 352
  ]
  edge [
    source 18
    target 353
  ]
  edge [
    source 18
    target 354
  ]
  edge [
    source 18
    target 355
  ]
  edge [
    source 18
    target 356
  ]
  edge [
    source 18
    target 357
  ]
  edge [
    source 18
    target 358
  ]
  edge [
    source 18
    target 359
  ]
  edge [
    source 18
    target 360
  ]
  edge [
    source 18
    target 361
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 362
  ]
  edge [
    source 18
    target 363
  ]
  edge [
    source 18
    target 365
  ]
  edge [
    source 18
    target 366
  ]
  edge [
    source 18
    target 367
  ]
  edge [
    source 18
    target 368
  ]
  edge [
    source 18
    target 347
  ]
  edge [
    source 18
    target 340
  ]
  edge [
    source 18
    target 369
  ]
  edge [
    source 18
    target 370
  ]
  edge [
    source 18
    target 334
  ]
  edge [
    source 18
    target 336
  ]
  edge [
    source 18
    target 337
  ]
  edge [
    source 18
    target 338
  ]
  edge [
    source 18
    target 341
  ]
  edge [
    source 18
    target 342
  ]
  edge [
    source 18
    target 343
  ]
  edge [
    source 18
    target 344
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 761
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 20
    target 763
  ]
  edge [
    source 20
    target 764
  ]
  edge [
    source 20
    target 765
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 772
  ]
  edge [
    source 20
    target 773
  ]
  edge [
    source 20
    target 774
  ]
  edge [
    source 20
    target 775
  ]
  edge [
    source 20
    target 776
  ]
  edge [
    source 20
    target 777
  ]
  edge [
    source 20
    target 341
  ]
  edge [
    source 20
    target 778
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 780
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 783
  ]
  edge [
    source 20
    target 784
  ]
  edge [
    source 20
    target 785
  ]
  edge [
    source 20
    target 786
  ]
  edge [
    source 20
    target 787
  ]
  edge [
    source 20
    target 788
  ]
  edge [
    source 20
    target 507
  ]
  edge [
    source 20
    target 789
  ]
  edge [
    source 20
    target 619
  ]
  edge [
    source 20
    target 790
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 794
  ]
  edge [
    source 21
    target 795
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 797
  ]
  edge [
    source 21
    target 798
  ]
  edge [
    source 21
    target 799
  ]
  edge [
    source 21
    target 349
  ]
  edge [
    source 21
    target 800
  ]
  edge [
    source 21
    target 801
  ]
  edge [
    source 21
    target 802
  ]
  edge [
    source 21
    target 803
  ]
  edge [
    source 21
    target 804
  ]
  edge [
    source 21
    target 805
  ]
  edge [
    source 21
    target 806
  ]
  edge [
    source 21
    target 807
  ]
  edge [
    source 21
    target 808
  ]
  edge [
    source 21
    target 809
  ]
  edge [
    source 21
    target 810
  ]
  edge [
    source 21
    target 635
  ]
  edge [
    source 21
    target 811
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 812
  ]
  edge [
    source 22
    target 813
  ]
  edge [
    source 22
    target 814
  ]
  edge [
    source 22
    target 815
  ]
  edge [
    source 22
    target 816
  ]
  edge [
    source 22
    target 817
  ]
  edge [
    source 22
    target 818
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 819
  ]
  edge [
    source 22
    target 820
  ]
  edge [
    source 22
    target 821
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 22
    target 823
  ]
  edge [
    source 22
    target 824
  ]
  edge [
    source 22
    target 825
  ]
  edge [
    source 22
    target 330
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 22
    target 827
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 829
  ]
  edge [
    source 22
    target 830
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 22
    target 833
  ]
  edge [
    source 22
    target 834
  ]
  edge [
    source 22
    target 835
  ]
  edge [
    source 22
    target 836
  ]
  edge [
    source 22
    target 837
  ]
  edge [
    source 22
    target 838
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 839
  ]
  edge [
    source 22
    target 840
  ]
  edge [
    source 22
    target 841
  ]
  edge [
    source 22
    target 842
  ]
  edge [
    source 22
    target 843
  ]
  edge [
    source 22
    target 844
  ]
  edge [
    source 22
    target 845
  ]
  edge [
    source 22
    target 846
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 847
  ]
  edge [
    source 24
    target 848
  ]
  edge [
    source 24
    target 849
  ]
  edge [
    source 24
    target 850
  ]
  edge [
    source 24
    target 851
  ]
  edge [
    source 24
    target 852
  ]
  edge [
    source 24
    target 853
  ]
  edge [
    source 24
    target 854
  ]
  edge [
    source 24
    target 855
  ]
  edge [
    source 24
    target 856
  ]
  edge [
    source 24
    target 857
  ]
  edge [
    source 24
    target 858
  ]
  edge [
    source 24
    target 859
  ]
  edge [
    source 24
    target 860
  ]
  edge [
    source 24
    target 861
  ]
  edge [
    source 24
    target 862
  ]
  edge [
    source 24
    target 863
  ]
  edge [
    source 24
    target 864
  ]
  edge [
    source 24
    target 269
  ]
  edge [
    source 24
    target 865
  ]
  edge [
    source 24
    target 866
  ]
  edge [
    source 24
    target 867
  ]
  edge [
    source 24
    target 868
  ]
  edge [
    source 24
    target 869
  ]
  edge [
    source 24
    target 870
  ]
  edge [
    source 24
    target 278
  ]
  edge [
    source 24
    target 871
  ]
  edge [
    source 24
    target 872
  ]
  edge [
    source 24
    target 873
  ]
  edge [
    source 24
    target 874
  ]
  edge [
    source 24
    target 875
  ]
  edge [
    source 24
    target 876
  ]
  edge [
    source 24
    target 877
  ]
  edge [
    source 24
    target 878
  ]
  edge [
    source 24
    target 879
  ]
  edge [
    source 24
    target 880
  ]
  edge [
    source 24
    target 881
  ]
  edge [
    source 24
    target 882
  ]
  edge [
    source 24
    target 883
  ]
  edge [
    source 24
    target 884
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 885
  ]
  edge [
    source 25
    target 46
  ]
  edge [
    source 25
    target 886
  ]
  edge [
    source 25
    target 887
  ]
  edge [
    source 25
    target 425
  ]
  edge [
    source 25
    target 888
  ]
  edge [
    source 25
    target 889
  ]
  edge [
    source 25
    target 890
  ]
  edge [
    source 25
    target 427
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 25
    target 891
  ]
  edge [
    source 25
    target 892
  ]
  edge [
    source 25
    target 893
  ]
  edge [
    source 25
    target 894
  ]
  edge [
    source 25
    target 895
  ]
  edge [
    source 25
    target 896
  ]
  edge [
    source 25
    target 897
  ]
  edge [
    source 25
    target 898
  ]
  edge [
    source 25
    target 899
  ]
  edge [
    source 25
    target 900
  ]
  edge [
    source 25
    target 901
  ]
  edge [
    source 25
    target 902
  ]
  edge [
    source 25
    target 903
  ]
  edge [
    source 25
    target 904
  ]
  edge [
    source 25
    target 905
  ]
  edge [
    source 25
    target 906
  ]
  edge [
    source 25
    target 907
  ]
  edge [
    source 25
    target 908
  ]
  edge [
    source 25
    target 909
  ]
  edge [
    source 25
    target 910
  ]
  edge [
    source 25
    target 911
  ]
  edge [
    source 25
    target 912
  ]
  edge [
    source 25
    target 913
  ]
  edge [
    source 25
    target 914
  ]
  edge [
    source 25
    target 915
  ]
  edge [
    source 25
    target 916
  ]
  edge [
    source 25
    target 917
  ]
  edge [
    source 25
    target 918
  ]
  edge [
    source 25
    target 919
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 25
    target 920
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 921
  ]
  edge [
    source 25
    target 922
  ]
  edge [
    source 25
    target 923
  ]
  edge [
    source 25
    target 924
  ]
  edge [
    source 25
    target 925
  ]
  edge [
    source 25
    target 926
  ]
  edge [
    source 25
    target 298
  ]
  edge [
    source 25
    target 299
  ]
  edge [
    source 25
    target 300
  ]
  edge [
    source 25
    target 301
  ]
  edge [
    source 25
    target 302
  ]
  edge [
    source 25
    target 303
  ]
  edge [
    source 25
    target 304
  ]
  edge [
    source 25
    target 305
  ]
  edge [
    source 25
    target 306
  ]
  edge [
    source 25
    target 307
  ]
  edge [
    source 25
    target 308
  ]
  edge [
    source 25
    target 309
  ]
  edge [
    source 25
    target 310
  ]
  edge [
    source 25
    target 311
  ]
  edge [
    source 25
    target 312
  ]
  edge [
    source 25
    target 294
  ]
  edge [
    source 25
    target 313
  ]
  edge [
    source 25
    target 314
  ]
  edge [
    source 25
    target 315
  ]
  edge [
    source 25
    target 316
  ]
  edge [
    source 25
    target 927
  ]
  edge [
    source 25
    target 928
  ]
  edge [
    source 25
    target 929
  ]
  edge [
    source 25
    target 930
  ]
  edge [
    source 25
    target 931
  ]
  edge [
    source 25
    target 932
  ]
  edge [
    source 25
    target 933
  ]
  edge [
    source 25
    target 934
  ]
  edge [
    source 25
    target 295
  ]
  edge [
    source 25
    target 935
  ]
  edge [
    source 25
    target 936
  ]
  edge [
    source 25
    target 937
  ]
  edge [
    source 25
    target 938
  ]
  edge [
    source 25
    target 939
  ]
  edge [
    source 25
    target 940
  ]
  edge [
    source 25
    target 941
  ]
  edge [
    source 25
    target 942
  ]
  edge [
    source 25
    target 943
  ]
  edge [
    source 25
    target 944
  ]
  edge [
    source 25
    target 945
  ]
  edge [
    source 25
    target 946
  ]
  edge [
    source 25
    target 947
  ]
  edge [
    source 25
    target 948
  ]
  edge [
    source 25
    target 949
  ]
  edge [
    source 25
    target 950
  ]
  edge [
    source 25
    target 321
  ]
  edge [
    source 25
    target 951
  ]
  edge [
    source 25
    target 952
  ]
  edge [
    source 25
    target 953
  ]
  edge [
    source 25
    target 954
  ]
  edge [
    source 25
    target 955
  ]
  edge [
    source 25
    target 956
  ]
  edge [
    source 25
    target 957
  ]
  edge [
    source 25
    target 958
  ]
  edge [
    source 25
    target 959
  ]
  edge [
    source 25
    target 960
  ]
  edge [
    source 25
    target 41
  ]
  edge [
    source 25
    target 961
  ]
  edge [
    source 25
    target 962
  ]
  edge [
    source 25
    target 963
  ]
  edge [
    source 25
    target 964
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 25
    target 965
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 25
    target 966
  ]
  edge [
    source 25
    target 967
  ]
  edge [
    source 25
    target 968
  ]
  edge [
    source 25
    target 969
  ]
  edge [
    source 25
    target 970
  ]
  edge [
    source 25
    target 971
  ]
  edge [
    source 25
    target 788
  ]
  edge [
    source 25
    target 972
  ]
  edge [
    source 25
    target 637
  ]
  edge [
    source 25
    target 973
  ]
  edge [
    source 25
    target 974
  ]
  edge [
    source 25
    target 975
  ]
  edge [
    source 25
    target 976
  ]
  edge [
    source 25
    target 977
  ]
  edge [
    source 25
    target 978
  ]
  edge [
    source 25
    target 619
  ]
  edge [
    source 25
    target 979
  ]
  edge [
    source 25
    target 389
  ]
  edge [
    source 25
    target 980
  ]
  edge [
    source 25
    target 981
  ]
  edge [
    source 25
    target 982
  ]
  edge [
    source 25
    target 685
  ]
  edge [
    source 25
    target 687
  ]
  edge [
    source 25
    target 983
  ]
  edge [
    source 25
    target 688
  ]
  edge [
    source 25
    target 984
  ]
  edge [
    source 25
    target 985
  ]
  edge [
    source 25
    target 986
  ]
  edge [
    source 25
    target 987
  ]
  edge [
    source 25
    target 988
  ]
  edge [
    source 25
    target 989
  ]
  edge [
    source 25
    target 990
  ]
  edge [
    source 25
    target 991
  ]
  edge [
    source 25
    target 327
  ]
  edge [
    source 25
    target 667
  ]
  edge [
    source 25
    target 992
  ]
  edge [
    source 25
    target 993
  ]
  edge [
    source 25
    target 994
  ]
  edge [
    source 25
    target 995
  ]
  edge [
    source 25
    target 996
  ]
  edge [
    source 25
    target 997
  ]
  edge [
    source 25
    target 998
  ]
  edge [
    source 25
    target 999
  ]
  edge [
    source 25
    target 1000
  ]
  edge [
    source 25
    target 1001
  ]
  edge [
    source 25
    target 338
  ]
  edge [
    source 25
    target 1002
  ]
  edge [
    source 25
    target 1003
  ]
  edge [
    source 25
    target 1004
  ]
  edge [
    source 25
    target 1005
  ]
  edge [
    source 25
    target 1006
  ]
  edge [
    source 25
    target 1007
  ]
  edge [
    source 25
    target 1008
  ]
  edge [
    source 25
    target 1009
  ]
  edge [
    source 25
    target 1010
  ]
  edge [
    source 25
    target 1011
  ]
  edge [
    source 25
    target 1012
  ]
  edge [
    source 25
    target 1013
  ]
  edge [
    source 25
    target 1014
  ]
  edge [
    source 25
    target 337
  ]
  edge [
    source 25
    target 381
  ]
  edge [
    source 25
    target 1015
  ]
  edge [
    source 25
    target 342
  ]
  edge [
    source 25
    target 1016
  ]
  edge [
    source 25
    target 1017
  ]
  edge [
    source 25
    target 1018
  ]
  edge [
    source 25
    target 1019
  ]
  edge [
    source 25
    target 1020
  ]
  edge [
    source 25
    target 1021
  ]
  edge [
    source 25
    target 344
  ]
  edge [
    source 25
    target 1022
  ]
  edge [
    source 25
    target 1023
  ]
  edge [
    source 25
    target 1024
  ]
  edge [
    source 25
    target 1025
  ]
  edge [
    source 25
    target 1026
  ]
  edge [
    source 25
    target 1027
  ]
  edge [
    source 25
    target 1028
  ]
  edge [
    source 25
    target 1029
  ]
  edge [
    source 25
    target 343
  ]
  edge [
    source 25
    target 1030
  ]
  edge [
    source 25
    target 1031
  ]
  edge [
    source 25
    target 1032
  ]
  edge [
    source 25
    target 1033
  ]
  edge [
    source 25
    target 1034
  ]
  edge [
    source 25
    target 1035
  ]
  edge [
    source 25
    target 1036
  ]
  edge [
    source 25
    target 1037
  ]
  edge [
    source 25
    target 1038
  ]
  edge [
    source 25
    target 1039
  ]
  edge [
    source 25
    target 1040
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1041
  ]
  edge [
    source 27
    target 1042
  ]
  edge [
    source 27
    target 1043
  ]
  edge [
    source 27
    target 1044
  ]
  edge [
    source 27
    target 1045
  ]
  edge [
    source 27
    target 1046
  ]
  edge [
    source 27
    target 1047
  ]
  edge [
    source 27
    target 1048
  ]
  edge [
    source 27
    target 1049
  ]
  edge [
    source 27
    target 848
  ]
  edge [
    source 27
    target 1050
  ]
  edge [
    source 27
    target 1051
  ]
  edge [
    source 27
    target 1052
  ]
  edge [
    source 27
    target 289
  ]
  edge [
    source 27
    target 862
  ]
  edge [
    source 27
    target 1053
  ]
  edge [
    source 27
    target 1054
  ]
  edge [
    source 27
    target 1055
  ]
  edge [
    source 27
    target 1056
  ]
  edge [
    source 27
    target 1057
  ]
  edge [
    source 27
    target 1058
  ]
  edge [
    source 27
    target 1059
  ]
  edge [
    source 27
    target 1060
  ]
  edge [
    source 27
    target 1061
  ]
  edge [
    source 27
    target 1062
  ]
  edge [
    source 27
    target 1063
  ]
  edge [
    source 27
    target 1064
  ]
  edge [
    source 27
    target 1065
  ]
  edge [
    source 27
    target 1066
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 537
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 1091
  ]
  edge [
    source 28
    target 1092
  ]
  edge [
    source 28
    target 1093
  ]
  edge [
    source 28
    target 1094
  ]
  edge [
    source 28
    target 1095
  ]
  edge [
    source 28
    target 1096
  ]
  edge [
    source 28
    target 1097
  ]
  edge [
    source 28
    target 1098
  ]
  edge [
    source 28
    target 1099
  ]
  edge [
    source 28
    target 1100
  ]
  edge [
    source 28
    target 1101
  ]
  edge [
    source 28
    target 1102
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 1104
  ]
  edge [
    source 28
    target 1105
  ]
  edge [
    source 28
    target 1106
  ]
  edge [
    source 28
    target 1107
  ]
  edge [
    source 28
    target 1108
  ]
  edge [
    source 28
    target 1109
  ]
  edge [
    source 28
    target 1110
  ]
  edge [
    source 28
    target 1111
  ]
  edge [
    source 28
    target 1112
  ]
  edge [
    source 28
    target 1113
  ]
  edge [
    source 28
    target 1114
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 1116
  ]
  edge [
    source 28
    target 1117
  ]
  edge [
    source 28
    target 1118
  ]
  edge [
    source 28
    target 1119
  ]
  edge [
    source 28
    target 1120
  ]
  edge [
    source 28
    target 1121
  ]
  edge [
    source 28
    target 1122
  ]
  edge [
    source 28
    target 1123
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1124
  ]
  edge [
    source 29
    target 1125
  ]
  edge [
    source 29
    target 1126
  ]
  edge [
    source 29
    target 1127
  ]
  edge [
    source 29
    target 348
  ]
  edge [
    source 29
    target 1128
  ]
  edge [
    source 29
    target 1129
  ]
  edge [
    source 29
    target 1130
  ]
  edge [
    source 29
    target 1131
  ]
  edge [
    source 29
    target 161
  ]
  edge [
    source 29
    target 1132
  ]
  edge [
    source 29
    target 1133
  ]
  edge [
    source 29
    target 762
  ]
  edge [
    source 29
    target 619
  ]
  edge [
    source 29
    target 1134
  ]
  edge [
    source 29
    target 1135
  ]
  edge [
    source 29
    target 1136
  ]
  edge [
    source 29
    target 1137
  ]
  edge [
    source 29
    target 1138
  ]
  edge [
    source 29
    target 1139
  ]
  edge [
    source 29
    target 1140
  ]
  edge [
    source 29
    target 1141
  ]
  edge [
    source 29
    target 1142
  ]
  edge [
    source 29
    target 1143
  ]
  edge [
    source 29
    target 1144
  ]
  edge [
    source 29
    target 1145
  ]
  edge [
    source 29
    target 1146
  ]
  edge [
    source 29
    target 1147
  ]
  edge [
    source 29
    target 1148
  ]
  edge [
    source 29
    target 1149
  ]
  edge [
    source 29
    target 1150
  ]
  edge [
    source 29
    target 327
  ]
  edge [
    source 29
    target 1151
  ]
  edge [
    source 29
    target 667
  ]
  edge [
    source 29
    target 1152
  ]
  edge [
    source 29
    target 1153
  ]
  edge [
    source 29
    target 1154
  ]
  edge [
    source 29
    target 1155
  ]
  edge [
    source 29
    target 1156
  ]
  edge [
    source 29
    target 1157
  ]
  edge [
    source 29
    target 1158
  ]
  edge [
    source 29
    target 1159
  ]
  edge [
    source 29
    target 1160
  ]
  edge [
    source 29
    target 335
  ]
  edge [
    source 29
    target 1161
  ]
  edge [
    source 29
    target 1162
  ]
  edge [
    source 29
    target 1163
  ]
  edge [
    source 29
    target 1164
  ]
  edge [
    source 29
    target 1165
  ]
  edge [
    source 29
    target 767
  ]
  edge [
    source 29
    target 1166
  ]
  edge [
    source 29
    target 1167
  ]
  edge [
    source 29
    target 1168
  ]
  edge [
    source 29
    target 1169
  ]
  edge [
    source 29
    target 1170
  ]
  edge [
    source 29
    target 1171
  ]
  edge [
    source 29
    target 1172
  ]
  edge [
    source 29
    target 1173
  ]
  edge [
    source 29
    target 1174
  ]
  edge [
    source 29
    target 222
  ]
  edge [
    source 29
    target 1175
  ]
  edge [
    source 29
    target 1176
  ]
  edge [
    source 29
    target 620
  ]
  edge [
    source 29
    target 174
  ]
  edge [
    source 29
    target 1177
  ]
  edge [
    source 29
    target 200
  ]
  edge [
    source 29
    target 1178
  ]
  edge [
    source 29
    target 1179
  ]
  edge [
    source 29
    target 644
  ]
  edge [
    source 29
    target 1180
  ]
  edge [
    source 29
    target 1181
  ]
  edge [
    source 29
    target 1182
  ]
  edge [
    source 29
    target 379
  ]
  edge [
    source 29
    target 1183
  ]
  edge [
    source 29
    target 1184
  ]
  edge [
    source 29
    target 811
  ]
  edge [
    source 29
    target 1185
  ]
  edge [
    source 29
    target 1186
  ]
  edge [
    source 29
    target 1187
  ]
  edge [
    source 29
    target 67
  ]
  edge [
    source 29
    target 757
  ]
  edge [
    source 29
    target 1188
  ]
  edge [
    source 29
    target 1189
  ]
  edge [
    source 29
    target 784
  ]
  edge [
    source 29
    target 1190
  ]
  edge [
    source 29
    target 773
  ]
  edge [
    source 29
    target 1191
  ]
  edge [
    source 29
    target 481
  ]
  edge [
    source 29
    target 1192
  ]
  edge [
    source 29
    target 507
  ]
  edge [
    source 29
    target 1193
  ]
  edge [
    source 29
    target 1194
  ]
  edge [
    source 29
    target 1195
  ]
  edge [
    source 29
    target 1196
  ]
  edge [
    source 29
    target 1197
  ]
  edge [
    source 29
    target 1198
  ]
  edge [
    source 29
    target 1199
  ]
  edge [
    source 29
    target 1200
  ]
  edge [
    source 29
    target 1201
  ]
  edge [
    source 29
    target 1202
  ]
  edge [
    source 29
    target 1203
  ]
  edge [
    source 29
    target 1204
  ]
  edge [
    source 29
    target 1205
  ]
  edge [
    source 29
    target 349
  ]
  edge [
    source 29
    target 1206
  ]
  edge [
    source 29
    target 1207
  ]
  edge [
    source 29
    target 1208
  ]
  edge [
    source 29
    target 262
  ]
  edge [
    source 29
    target 1209
  ]
  edge [
    source 29
    target 1210
  ]
  edge [
    source 29
    target 289
  ]
  edge [
    source 29
    target 1211
  ]
  edge [
    source 29
    target 1212
  ]
  edge [
    source 29
    target 1213
  ]
  edge [
    source 29
    target 1214
  ]
  edge [
    source 29
    target 1215
  ]
  edge [
    source 29
    target 1216
  ]
  edge [
    source 29
    target 635
  ]
  edge [
    source 29
    target 1217
  ]
  edge [
    source 29
    target 1218
  ]
  edge [
    source 29
    target 1219
  ]
  edge [
    source 29
    target 1220
  ]
  edge [
    source 29
    target 1221
  ]
  edge [
    source 29
    target 1222
  ]
  edge [
    source 29
    target 1223
  ]
  edge [
    source 29
    target 1224
  ]
  edge [
    source 29
    target 1225
  ]
  edge [
    source 29
    target 1226
  ]
  edge [
    source 29
    target 1227
  ]
  edge [
    source 29
    target 1228
  ]
  edge [
    source 29
    target 1229
  ]
  edge [
    source 29
    target 1230
  ]
  edge [
    source 29
    target 1231
  ]
  edge [
    source 29
    target 104
  ]
  edge [
    source 29
    target 1232
  ]
  edge [
    source 29
    target 1233
  ]
  edge [
    source 29
    target 1234
  ]
  edge [
    source 29
    target 1235
  ]
  edge [
    source 29
    target 1236
  ]
  edge [
    source 29
    target 1237
  ]
  edge [
    source 29
    target 1238
  ]
  edge [
    source 29
    target 1239
  ]
  edge [
    source 29
    target 1240
  ]
  edge [
    source 29
    target 1241
  ]
  edge [
    source 29
    target 1242
  ]
  edge [
    source 29
    target 1243
  ]
  edge [
    source 29
    target 1244
  ]
  edge [
    source 29
    target 1245
  ]
  edge [
    source 29
    target 1246
  ]
  edge [
    source 29
    target 1247
  ]
  edge [
    source 29
    target 1248
  ]
  edge [
    source 29
    target 1249
  ]
  edge [
    source 29
    target 1250
  ]
  edge [
    source 29
    target 900
  ]
  edge [
    source 29
    target 69
  ]
  edge [
    source 29
    target 1251
  ]
  edge [
    source 29
    target 1252
  ]
  edge [
    source 29
    target 1253
  ]
  edge [
    source 29
    target 1254
  ]
  edge [
    source 29
    target 1255
  ]
  edge [
    source 29
    target 1256
  ]
  edge [
    source 29
    target 1257
  ]
  edge [
    source 29
    target 1258
  ]
  edge [
    source 29
    target 1259
  ]
  edge [
    source 29
    target 1260
  ]
  edge [
    source 29
    target 1261
  ]
  edge [
    source 29
    target 1262
  ]
  edge [
    source 29
    target 1263
  ]
  edge [
    source 29
    target 1264
  ]
  edge [
    source 29
    target 1265
  ]
  edge [
    source 29
    target 1266
  ]
  edge [
    source 29
    target 1267
  ]
  edge [
    source 29
    target 1268
  ]
  edge [
    source 29
    target 1269
  ]
  edge [
    source 29
    target 1270
  ]
  edge [
    source 29
    target 1271
  ]
  edge [
    source 29
    target 1272
  ]
  edge [
    source 29
    target 1273
  ]
  edge [
    source 29
    target 1274
  ]
  edge [
    source 29
    target 1275
  ]
  edge [
    source 29
    target 1276
  ]
  edge [
    source 29
    target 1277
  ]
  edge [
    source 29
    target 1278
  ]
  edge [
    source 29
    target 1279
  ]
  edge [
    source 29
    target 1280
  ]
  edge [
    source 29
    target 1281
  ]
  edge [
    source 29
    target 1282
  ]
  edge [
    source 29
    target 1283
  ]
  edge [
    source 29
    target 1284
  ]
  edge [
    source 29
    target 350
  ]
  edge [
    source 29
    target 1285
  ]
  edge [
    source 29
    target 1286
  ]
  edge [
    source 29
    target 1287
  ]
  edge [
    source 29
    target 1288
  ]
  edge [
    source 29
    target 1289
  ]
  edge [
    source 29
    target 1290
  ]
  edge [
    source 29
    target 1291
  ]
  edge [
    source 29
    target 806
  ]
  edge [
    source 29
    target 1292
  ]
  edge [
    source 29
    target 1293
  ]
  edge [
    source 29
    target 106
  ]
  edge [
    source 29
    target 1294
  ]
  edge [
    source 29
    target 1295
  ]
  edge [
    source 29
    target 1296
  ]
  edge [
    source 29
    target 1297
  ]
  edge [
    source 29
    target 1298
  ]
  edge [
    source 29
    target 1299
  ]
  edge [
    source 29
    target 1300
  ]
  edge [
    source 29
    target 1301
  ]
  edge [
    source 29
    target 1302
  ]
  edge [
    source 29
    target 1303
  ]
  edge [
    source 29
    target 505
  ]
  edge [
    source 29
    target 1304
  ]
  edge [
    source 29
    target 1305
  ]
  edge [
    source 29
    target 1306
  ]
  edge [
    source 29
    target 1307
  ]
  edge [
    source 29
    target 1308
  ]
  edge [
    source 29
    target 1309
  ]
  edge [
    source 29
    target 1310
  ]
  edge [
    source 29
    target 1311
  ]
  edge [
    source 29
    target 1312
  ]
  edge [
    source 29
    target 1313
  ]
  edge [
    source 29
    target 1314
  ]
  edge [
    source 29
    target 1315
  ]
  edge [
    source 29
    target 690
  ]
  edge [
    source 29
    target 1316
  ]
  edge [
    source 29
    target 1317
  ]
  edge [
    source 29
    target 1318
  ]
  edge [
    source 29
    target 1319
  ]
  edge [
    source 29
    target 1320
  ]
  edge [
    source 29
    target 1321
  ]
  edge [
    source 29
    target 645
  ]
  edge [
    source 29
    target 629
  ]
  edge [
    source 29
    target 1322
  ]
  edge [
    source 29
    target 1323
  ]
  edge [
    source 29
    target 1324
  ]
  edge [
    source 29
    target 1325
  ]
  edge [
    source 29
    target 1326
  ]
  edge [
    source 29
    target 256
  ]
  edge [
    source 29
    target 1327
  ]
  edge [
    source 29
    target 1328
  ]
  edge [
    source 29
    target 1329
  ]
  edge [
    source 29
    target 99
  ]
  edge [
    source 29
    target 1330
  ]
  edge [
    source 29
    target 1331
  ]
  edge [
    source 29
    target 1332
  ]
  edge [
    source 29
    target 1333
  ]
  edge [
    source 29
    target 1334
  ]
  edge [
    source 29
    target 1335
  ]
  edge [
    source 29
    target 1336
  ]
  edge [
    source 29
    target 1337
  ]
  edge [
    source 29
    target 1338
  ]
  edge [
    source 29
    target 1339
  ]
  edge [
    source 29
    target 1340
  ]
  edge [
    source 29
    target 1341
  ]
  edge [
    source 29
    target 1342
  ]
  edge [
    source 29
    target 1343
  ]
  edge [
    source 29
    target 1344
  ]
  edge [
    source 29
    target 1345
  ]
  edge [
    source 29
    target 1346
  ]
  edge [
    source 29
    target 1347
  ]
  edge [
    source 29
    target 1348
  ]
  edge [
    source 29
    target 1349
  ]
  edge [
    source 29
    target 1350
  ]
  edge [
    source 29
    target 1351
  ]
  edge [
    source 29
    target 1352
  ]
  edge [
    source 29
    target 703
  ]
  edge [
    source 29
    target 1353
  ]
  edge [
    source 29
    target 1354
  ]
  edge [
    source 29
    target 1355
  ]
  edge [
    source 29
    target 1356
  ]
  edge [
    source 29
    target 1357
  ]
  edge [
    source 29
    target 1358
  ]
  edge [
    source 29
    target 1359
  ]
  edge [
    source 29
    target 374
  ]
  edge [
    source 29
    target 1360
  ]
  edge [
    source 29
    target 788
  ]
  edge [
    source 29
    target 1361
  ]
  edge [
    source 29
    target 1362
  ]
  edge [
    source 29
    target 326
  ]
  edge [
    source 29
    target 1363
  ]
  edge [
    source 29
    target 1364
  ]
  edge [
    source 29
    target 1365
  ]
  edge [
    source 29
    target 1366
  ]
  edge [
    source 29
    target 1367
  ]
  edge [
    source 29
    target 1368
  ]
  edge [
    source 29
    target 1369
  ]
  edge [
    source 29
    target 1370
  ]
  edge [
    source 29
    target 1371
  ]
  edge [
    source 29
    target 1372
  ]
  edge [
    source 29
    target 1373
  ]
  edge [
    source 29
    target 1374
  ]
  edge [
    source 29
    target 1375
  ]
  edge [
    source 29
    target 1376
  ]
  edge [
    source 29
    target 695
  ]
  edge [
    source 29
    target 1377
  ]
  edge [
    source 29
    target 1378
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 1379
  ]
  edge [
    source 29
    target 1380
  ]
  edge [
    source 29
    target 1381
  ]
  edge [
    source 29
    target 1382
  ]
  edge [
    source 29
    target 1383
  ]
  edge [
    source 29
    target 1384
  ]
  edge [
    source 29
    target 435
  ]
  edge [
    source 29
    target 1385
  ]
  edge [
    source 29
    target 570
  ]
  edge [
    source 29
    target 1386
  ]
  edge [
    source 29
    target 1387
  ]
  edge [
    source 29
    target 1388
  ]
  edge [
    source 29
    target 1389
  ]
  edge [
    source 30
    target 1390
  ]
  edge [
    source 30
    target 1391
  ]
  edge [
    source 30
    target 1392
  ]
  edge [
    source 30
    target 1393
  ]
  edge [
    source 30
    target 1394
  ]
  edge [
    source 30
    target 1395
  ]
  edge [
    source 30
    target 1396
  ]
  edge [
    source 30
    target 1397
  ]
  edge [
    source 30
    target 635
  ]
  edge [
    source 30
    target 1398
  ]
  edge [
    source 30
    target 1399
  ]
  edge [
    source 30
    target 1400
  ]
  edge [
    source 30
    target 1401
  ]
  edge [
    source 30
    target 1402
  ]
  edge [
    source 30
    target 1403
  ]
  edge [
    source 30
    target 1404
  ]
  edge [
    source 30
    target 1405
  ]
  edge [
    source 30
    target 1406
  ]
  edge [
    source 30
    target 1407
  ]
  edge [
    source 30
    target 1408
  ]
  edge [
    source 30
    target 1409
  ]
  edge [
    source 30
    target 1410
  ]
  edge [
    source 30
    target 1411
  ]
  edge [
    source 30
    target 1412
  ]
  edge [
    source 30
    target 1413
  ]
  edge [
    source 30
    target 1414
  ]
  edge [
    source 30
    target 1415
  ]
  edge [
    source 30
    target 1416
  ]
  edge [
    source 30
    target 1417
  ]
  edge [
    source 30
    target 1418
  ]
  edge [
    source 174
    target 1421
  ]
  edge [
    source 1419
    target 1420
  ]
  edge [
    source 1422
    target 1423
  ]
  edge [
    source 1424
    target 1425
  ]
]
