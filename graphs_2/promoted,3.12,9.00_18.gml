graph [
  node [
    id 0
    label "przestroga"
    origin "text"
  ]
  node [
    id 1
    label "uwaga"
  ]
  node [
    id 2
    label "admonition"
  ]
  node [
    id 3
    label "wypowied&#378;"
  ]
  node [
    id 4
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 5
    label "stan"
  ]
  node [
    id 6
    label "nagana"
  ]
  node [
    id 7
    label "tekst"
  ]
  node [
    id 8
    label "upomnienie"
  ]
  node [
    id 9
    label "dzienniczek"
  ]
  node [
    id 10
    label "wzgl&#261;d"
  ]
  node [
    id 11
    label "gossip"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
]
