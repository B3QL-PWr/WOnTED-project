graph [
  node [
    id 0
    label "ludwik"
    origin "text"
  ]
  node [
    id 1
    label "gmina"
    origin "text"
  ]
  node [
    id 2
    label "teresin"
    origin "text"
  ]
  node [
    id 3
    label "moneta"
  ]
  node [
    id 4
    label "antyk"
  ]
  node [
    id 5
    label "staro&#380;ytno&#347;&#263;"
  ]
  node [
    id 6
    label "styl_dorycki"
  ]
  node [
    id 7
    label "styl_jo&#324;ski"
  ]
  node [
    id 8
    label "styl_koryncki"
  ]
  node [
    id 9
    label "aretalogia"
  ]
  node [
    id 10
    label "okres_klasyczny"
  ]
  node [
    id 11
    label "epoka"
  ]
  node [
    id 12
    label "styl_kompozytowy"
  ]
  node [
    id 13
    label "klasycyzm"
  ]
  node [
    id 14
    label "staro&#263;"
  ]
  node [
    id 15
    label "awers"
  ]
  node [
    id 16
    label "legenda"
  ]
  node [
    id 17
    label "liga"
  ]
  node [
    id 18
    label "rewers"
  ]
  node [
    id 19
    label "egzerga"
  ]
  node [
    id 20
    label "pieni&#261;dz"
  ]
  node [
    id 21
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 22
    label "otok"
  ]
  node [
    id 23
    label "balansjerka"
  ]
  node [
    id 24
    label "Biskupice"
  ]
  node [
    id 25
    label "radny"
  ]
  node [
    id 26
    label "urz&#261;d"
  ]
  node [
    id 27
    label "powiat"
  ]
  node [
    id 28
    label "rada_gminy"
  ]
  node [
    id 29
    label "Dobro&#324;"
  ]
  node [
    id 30
    label "organizacja_religijna"
  ]
  node [
    id 31
    label "Karlsbad"
  ]
  node [
    id 32
    label "Wielka_Wie&#347;"
  ]
  node [
    id 33
    label "jednostka_administracyjna"
  ]
  node [
    id 34
    label "stanowisko"
  ]
  node [
    id 35
    label "position"
  ]
  node [
    id 36
    label "instytucja"
  ]
  node [
    id 37
    label "siedziba"
  ]
  node [
    id 38
    label "organ"
  ]
  node [
    id 39
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 40
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 41
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 42
    label "mianowaniec"
  ]
  node [
    id 43
    label "dzia&#322;"
  ]
  node [
    id 44
    label "okienko"
  ]
  node [
    id 45
    label "w&#322;adza"
  ]
  node [
    id 46
    label "Zabrze"
  ]
  node [
    id 47
    label "Ma&#322;opolska"
  ]
  node [
    id 48
    label "Niemcy"
  ]
  node [
    id 49
    label "wojew&#243;dztwo"
  ]
  node [
    id 50
    label "rada"
  ]
  node [
    id 51
    label "samorz&#261;dowiec"
  ]
  node [
    id 52
    label "przedstawiciel"
  ]
  node [
    id 53
    label "rajca"
  ]
  node [
    id 54
    label "pierwszy"
  ]
  node [
    id 55
    label "wojna"
  ]
  node [
    id 56
    label "&#347;wiatowy"
  ]
  node [
    id 57
    label "Tadeusz"
  ]
  node [
    id 58
    label "Sroczy&#324;ski"
  ]
  node [
    id 59
    label "Seroki"
  ]
  node [
    id 60
    label "parcela"
  ]
  node [
    id 61
    label "kolonia"
  ]
  node [
    id 62
    label "podlesie"
  ]
  node [
    id 63
    label "drugi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 63
  ]
  edge [
    source 56
    target 63
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
]
