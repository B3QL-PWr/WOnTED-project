graph [
  node [
    id 0
    label "pies"
    origin "text"
  ]
  node [
    id 1
    label "zwierzeta"
    origin "text"
  ]
  node [
    id 2
    label "cytowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "informacja"
    origin "text"
  ]
  node [
    id 4
    label "oficjalny"
    origin "text"
  ]
  node [
    id 5
    label "doniesienie"
    origin "text"
  ]
  node [
    id 6
    label "bie&#380;&#261;co"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "niezmiernie"
    origin "text"
  ]
  node [
    id 9
    label "przykro"
    origin "text"
  ]
  node [
    id 10
    label "skutek"
    origin "text"
  ]
  node [
    id 11
    label "szarpanina"
    origin "text"
  ]
  node [
    id 12
    label "dziecko"
    origin "text"
  ]
  node [
    id 13
    label "ojciec"
    origin "text"
  ]
  node [
    id 14
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 16
    label "szok"
    origin "text"
  ]
  node [
    id 17
    label "ratowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 19
    label "syn"
    origin "text"
  ]
  node [
    id 20
    label "uderzy&#263;"
    origin "text"
  ]
  node [
    id 21
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 22
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 23
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 24
    label "butelka"
    origin "text"
  ]
  node [
    id 25
    label "piwo"
    origin "text"
  ]
  node [
    id 26
    label "piese&#322;"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "Cerber"
  ]
  node [
    id 29
    label "szczeka&#263;"
  ]
  node [
    id 30
    label "&#322;ajdak"
  ]
  node [
    id 31
    label "kabanos"
  ]
  node [
    id 32
    label "wyzwisko"
  ]
  node [
    id 33
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 34
    label "samiec"
  ]
  node [
    id 35
    label "spragniony"
  ]
  node [
    id 36
    label "policjant"
  ]
  node [
    id 37
    label "rakarz"
  ]
  node [
    id 38
    label "szczu&#263;"
  ]
  node [
    id 39
    label "wycie"
  ]
  node [
    id 40
    label "istota_&#380;ywa"
  ]
  node [
    id 41
    label "trufla"
  ]
  node [
    id 42
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 43
    label "zawy&#263;"
  ]
  node [
    id 44
    label "sobaka"
  ]
  node [
    id 45
    label "dogoterapia"
  ]
  node [
    id 46
    label "s&#322;u&#380;enie"
  ]
  node [
    id 47
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 48
    label "psowate"
  ]
  node [
    id 49
    label "wy&#263;"
  ]
  node [
    id 50
    label "szczucie"
  ]
  node [
    id 51
    label "czworon&#243;g"
  ]
  node [
    id 52
    label "sympatyk"
  ]
  node [
    id 53
    label "entuzjasta"
  ]
  node [
    id 54
    label "critter"
  ]
  node [
    id 55
    label "zwierz&#281;_domowe"
  ]
  node [
    id 56
    label "kr&#281;gowiec"
  ]
  node [
    id 57
    label "tetrapody"
  ]
  node [
    id 58
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 59
    label "zwierz&#281;"
  ]
  node [
    id 60
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 61
    label "ludzko&#347;&#263;"
  ]
  node [
    id 62
    label "asymilowanie"
  ]
  node [
    id 63
    label "wapniak"
  ]
  node [
    id 64
    label "asymilowa&#263;"
  ]
  node [
    id 65
    label "os&#322;abia&#263;"
  ]
  node [
    id 66
    label "posta&#263;"
  ]
  node [
    id 67
    label "hominid"
  ]
  node [
    id 68
    label "podw&#322;adny"
  ]
  node [
    id 69
    label "os&#322;abianie"
  ]
  node [
    id 70
    label "figura"
  ]
  node [
    id 71
    label "portrecista"
  ]
  node [
    id 72
    label "dwun&#243;g"
  ]
  node [
    id 73
    label "profanum"
  ]
  node [
    id 74
    label "mikrokosmos"
  ]
  node [
    id 75
    label "nasada"
  ]
  node [
    id 76
    label "duch"
  ]
  node [
    id 77
    label "antropochoria"
  ]
  node [
    id 78
    label "osoba"
  ]
  node [
    id 79
    label "wz&#243;r"
  ]
  node [
    id 80
    label "senior"
  ]
  node [
    id 81
    label "oddzia&#322;ywanie"
  ]
  node [
    id 82
    label "Adam"
  ]
  node [
    id 83
    label "homo_sapiens"
  ]
  node [
    id 84
    label "polifag"
  ]
  node [
    id 85
    label "palconogie"
  ]
  node [
    id 86
    label "stra&#380;nik"
  ]
  node [
    id 87
    label "wielog&#322;owy"
  ]
  node [
    id 88
    label "przek&#261;ska"
  ]
  node [
    id 89
    label "w&#281;dzi&#263;"
  ]
  node [
    id 90
    label "przysmak"
  ]
  node [
    id 91
    label "kie&#322;basa"
  ]
  node [
    id 92
    label "cygaro"
  ]
  node [
    id 93
    label "kot"
  ]
  node [
    id 94
    label "zooterapia"
  ]
  node [
    id 95
    label "&#380;o&#322;nierz"
  ]
  node [
    id 96
    label "robi&#263;"
  ]
  node [
    id 97
    label "trwa&#263;"
  ]
  node [
    id 98
    label "use"
  ]
  node [
    id 99
    label "suffice"
  ]
  node [
    id 100
    label "cel"
  ]
  node [
    id 101
    label "pracowa&#263;"
  ]
  node [
    id 102
    label "match"
  ]
  node [
    id 103
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 104
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 105
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 106
    label "wait"
  ]
  node [
    id 107
    label "pomaga&#263;"
  ]
  node [
    id 108
    label "czekoladka"
  ]
  node [
    id 109
    label "afrodyzjak"
  ]
  node [
    id 110
    label "workowiec"
  ]
  node [
    id 111
    label "nos"
  ]
  node [
    id 112
    label "grzyb_owocnikowy"
  ]
  node [
    id 113
    label "truflowate"
  ]
  node [
    id 114
    label "grzyb_mikoryzowy"
  ]
  node [
    id 115
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 116
    label "powodowanie"
  ]
  node [
    id 117
    label "pod&#380;eganie"
  ]
  node [
    id 118
    label "atakowanie"
  ]
  node [
    id 119
    label "fomentation"
  ]
  node [
    id 120
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 121
    label "bark"
  ]
  node [
    id 122
    label "m&#243;wi&#263;"
  ]
  node [
    id 123
    label "hum"
  ]
  node [
    id 124
    label "obgadywa&#263;"
  ]
  node [
    id 125
    label "kozio&#322;"
  ]
  node [
    id 126
    label "szcz&#281;ka&#263;"
  ]
  node [
    id 127
    label "karabin"
  ]
  node [
    id 128
    label "wymy&#347;la&#263;"
  ]
  node [
    id 129
    label "wilk"
  ]
  node [
    id 130
    label "ha&#322;asowa&#263;"
  ]
  node [
    id 131
    label "fa&#322;szowa&#263;"
  ]
  node [
    id 132
    label "p&#322;aka&#263;"
  ]
  node [
    id 133
    label "snivel"
  ]
  node [
    id 134
    label "yip"
  ]
  node [
    id 135
    label "pracownik_komunalny"
  ]
  node [
    id 136
    label "uczynny"
  ]
  node [
    id 137
    label "s&#322;ugiwanie"
  ]
  node [
    id 138
    label "pomaganie"
  ]
  node [
    id 139
    label "bycie"
  ]
  node [
    id 140
    label "wys&#322;u&#380;enie_si&#281;"
  ]
  node [
    id 141
    label "request"
  ]
  node [
    id 142
    label "trwanie"
  ]
  node [
    id 143
    label "robienie"
  ]
  node [
    id 144
    label "service"
  ]
  node [
    id 145
    label "przydawanie_si&#281;"
  ]
  node [
    id 146
    label "czynno&#347;&#263;"
  ]
  node [
    id 147
    label "pracowanie"
  ]
  node [
    id 148
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 149
    label "wydoby&#263;"
  ]
  node [
    id 150
    label "rant"
  ]
  node [
    id 151
    label "rave"
  ]
  node [
    id 152
    label "zabrzmie&#263;"
  ]
  node [
    id 153
    label "tease"
  ]
  node [
    id 154
    label "pod&#380;ega&#263;"
  ]
  node [
    id 155
    label "podjudza&#263;"
  ]
  node [
    id 156
    label "wo&#322;anie"
  ]
  node [
    id 157
    label "wydobywanie"
  ]
  node [
    id 158
    label "brzmienie"
  ]
  node [
    id 159
    label "wydawanie"
  ]
  node [
    id 160
    label "d&#378;wi&#281;k"
  ]
  node [
    id 161
    label "whimper"
  ]
  node [
    id 162
    label "cholera"
  ]
  node [
    id 163
    label "wypowied&#378;"
  ]
  node [
    id 164
    label "chuj"
  ]
  node [
    id 165
    label "bluzg"
  ]
  node [
    id 166
    label "chujowy"
  ]
  node [
    id 167
    label "obelga"
  ]
  node [
    id 168
    label "szmata"
  ]
  node [
    id 169
    label "ch&#281;tny"
  ]
  node [
    id 170
    label "z&#322;akniony"
  ]
  node [
    id 171
    label "upodlenie_si&#281;"
  ]
  node [
    id 172
    label "skurwysyn"
  ]
  node [
    id 173
    label "upadlanie_si&#281;"
  ]
  node [
    id 174
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 175
    label "psubrat"
  ]
  node [
    id 176
    label "policja"
  ]
  node [
    id 177
    label "blacharz"
  ]
  node [
    id 178
    label "str&#243;&#380;"
  ]
  node [
    id 179
    label "pa&#322;a"
  ]
  node [
    id 180
    label "mundurowy"
  ]
  node [
    id 181
    label "glina"
  ]
  node [
    id 182
    label "quote"
  ]
  node [
    id 183
    label "wymienia&#263;"
  ]
  node [
    id 184
    label "przytacza&#263;"
  ]
  node [
    id 185
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 186
    label "adduce"
  ]
  node [
    id 187
    label "doprowadza&#263;"
  ]
  node [
    id 188
    label "umieszcza&#263;"
  ]
  node [
    id 189
    label "report"
  ]
  node [
    id 190
    label "podawa&#263;"
  ]
  node [
    id 191
    label "mienia&#263;"
  ]
  node [
    id 192
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 193
    label "zmienia&#263;"
  ]
  node [
    id 194
    label "zakomunikowa&#263;"
  ]
  node [
    id 195
    label "mention"
  ]
  node [
    id 196
    label "punkt"
  ]
  node [
    id 197
    label "publikacja"
  ]
  node [
    id 198
    label "wiedza"
  ]
  node [
    id 199
    label "doj&#347;cie"
  ]
  node [
    id 200
    label "obiega&#263;"
  ]
  node [
    id 201
    label "powzi&#281;cie"
  ]
  node [
    id 202
    label "dane"
  ]
  node [
    id 203
    label "obiegni&#281;cie"
  ]
  node [
    id 204
    label "sygna&#322;"
  ]
  node [
    id 205
    label "obieganie"
  ]
  node [
    id 206
    label "powzi&#261;&#263;"
  ]
  node [
    id 207
    label "obiec"
  ]
  node [
    id 208
    label "doj&#347;&#263;"
  ]
  node [
    id 209
    label "po&#322;o&#380;enie"
  ]
  node [
    id 210
    label "sprawa"
  ]
  node [
    id 211
    label "ust&#281;p"
  ]
  node [
    id 212
    label "plan"
  ]
  node [
    id 213
    label "obiekt_matematyczny"
  ]
  node [
    id 214
    label "problemat"
  ]
  node [
    id 215
    label "plamka"
  ]
  node [
    id 216
    label "stopie&#324;_pisma"
  ]
  node [
    id 217
    label "jednostka"
  ]
  node [
    id 218
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 219
    label "miejsce"
  ]
  node [
    id 220
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 221
    label "mark"
  ]
  node [
    id 222
    label "chwila"
  ]
  node [
    id 223
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 224
    label "prosta"
  ]
  node [
    id 225
    label "problematyka"
  ]
  node [
    id 226
    label "obiekt"
  ]
  node [
    id 227
    label "zapunktowa&#263;"
  ]
  node [
    id 228
    label "podpunkt"
  ]
  node [
    id 229
    label "wojsko"
  ]
  node [
    id 230
    label "kres"
  ]
  node [
    id 231
    label "przestrze&#324;"
  ]
  node [
    id 232
    label "point"
  ]
  node [
    id 233
    label "pozycja"
  ]
  node [
    id 234
    label "cognition"
  ]
  node [
    id 235
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 236
    label "intelekt"
  ]
  node [
    id 237
    label "pozwolenie"
  ]
  node [
    id 238
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 239
    label "zaawansowanie"
  ]
  node [
    id 240
    label "wykszta&#322;cenie"
  ]
  node [
    id 241
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 242
    label "przekazywa&#263;"
  ]
  node [
    id 243
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 244
    label "pulsation"
  ]
  node [
    id 245
    label "przekazywanie"
  ]
  node [
    id 246
    label "przewodzenie"
  ]
  node [
    id 247
    label "po&#322;&#261;czenie"
  ]
  node [
    id 248
    label "fala"
  ]
  node [
    id 249
    label "przekazanie"
  ]
  node [
    id 250
    label "przewodzi&#263;"
  ]
  node [
    id 251
    label "znak"
  ]
  node [
    id 252
    label "zapowied&#378;"
  ]
  node [
    id 253
    label "medium_transmisyjne"
  ]
  node [
    id 254
    label "demodulacja"
  ]
  node [
    id 255
    label "przekaza&#263;"
  ]
  node [
    id 256
    label "czynnik"
  ]
  node [
    id 257
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 258
    label "aliasing"
  ]
  node [
    id 259
    label "wizja"
  ]
  node [
    id 260
    label "modulacja"
  ]
  node [
    id 261
    label "drift"
  ]
  node [
    id 262
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 263
    label "tekst"
  ]
  node [
    id 264
    label "druk"
  ]
  node [
    id 265
    label "produkcja"
  ]
  node [
    id 266
    label "notification"
  ]
  node [
    id 267
    label "edytowa&#263;"
  ]
  node [
    id 268
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 269
    label "spakowanie"
  ]
  node [
    id 270
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 271
    label "pakowa&#263;"
  ]
  node [
    id 272
    label "rekord"
  ]
  node [
    id 273
    label "korelator"
  ]
  node [
    id 274
    label "wyci&#261;ganie"
  ]
  node [
    id 275
    label "pakowanie"
  ]
  node [
    id 276
    label "sekwencjonowa&#263;"
  ]
  node [
    id 277
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 278
    label "jednostka_informacji"
  ]
  node [
    id 279
    label "zbi&#243;r"
  ]
  node [
    id 280
    label "evidence"
  ]
  node [
    id 281
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 282
    label "rozpakowywanie"
  ]
  node [
    id 283
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 284
    label "rozpakowanie"
  ]
  node [
    id 285
    label "rozpakowywa&#263;"
  ]
  node [
    id 286
    label "konwersja"
  ]
  node [
    id 287
    label "nap&#322;ywanie"
  ]
  node [
    id 288
    label "rozpakowa&#263;"
  ]
  node [
    id 289
    label "spakowa&#263;"
  ]
  node [
    id 290
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 291
    label "edytowanie"
  ]
  node [
    id 292
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 293
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 294
    label "sekwencjonowanie"
  ]
  node [
    id 295
    label "flow"
  ]
  node [
    id 296
    label "odwiedza&#263;"
  ]
  node [
    id 297
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 298
    label "rotate"
  ]
  node [
    id 299
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 300
    label "authorize"
  ]
  node [
    id 301
    label "podj&#261;&#263;"
  ]
  node [
    id 302
    label "zacz&#261;&#263;"
  ]
  node [
    id 303
    label "otrzyma&#263;"
  ]
  node [
    id 304
    label "sta&#263;_si&#281;"
  ]
  node [
    id 305
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 306
    label "supervene"
  ]
  node [
    id 307
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 308
    label "zaj&#347;&#263;"
  ]
  node [
    id 309
    label "catch"
  ]
  node [
    id 310
    label "get"
  ]
  node [
    id 311
    label "bodziec"
  ]
  node [
    id 312
    label "przesy&#322;ka"
  ]
  node [
    id 313
    label "dodatek"
  ]
  node [
    id 314
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 315
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 316
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 317
    label "heed"
  ]
  node [
    id 318
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 319
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 320
    label "spowodowa&#263;"
  ]
  node [
    id 321
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 322
    label "dozna&#263;"
  ]
  node [
    id 323
    label "dokoptowa&#263;"
  ]
  node [
    id 324
    label "postrzega&#263;"
  ]
  node [
    id 325
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 326
    label "orgazm"
  ]
  node [
    id 327
    label "dolecie&#263;"
  ]
  node [
    id 328
    label "drive"
  ]
  node [
    id 329
    label "dotrze&#263;"
  ]
  node [
    id 330
    label "uzyska&#263;"
  ]
  node [
    id 331
    label "dop&#322;ata"
  ]
  node [
    id 332
    label "become"
  ]
  node [
    id 333
    label "odwiedzi&#263;"
  ]
  node [
    id 334
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 335
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 336
    label "orb"
  ]
  node [
    id 337
    label "podj&#281;cie"
  ]
  node [
    id 338
    label "otrzymanie"
  ]
  node [
    id 339
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 340
    label "dochodzenie"
  ]
  node [
    id 341
    label "uzyskanie"
  ]
  node [
    id 342
    label "skill"
  ]
  node [
    id 343
    label "dochrapanie_si&#281;"
  ]
  node [
    id 344
    label "znajomo&#347;ci"
  ]
  node [
    id 345
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 346
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 347
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 348
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 349
    label "powi&#261;zanie"
  ]
  node [
    id 350
    label "entrance"
  ]
  node [
    id 351
    label "affiliation"
  ]
  node [
    id 352
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 353
    label "dor&#281;czenie"
  ]
  node [
    id 354
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 355
    label "spowodowanie"
  ]
  node [
    id 356
    label "dost&#281;p"
  ]
  node [
    id 357
    label "gotowy"
  ]
  node [
    id 358
    label "avenue"
  ]
  node [
    id 359
    label "postrzeganie"
  ]
  node [
    id 360
    label "doznanie"
  ]
  node [
    id 361
    label "dojrza&#322;y"
  ]
  node [
    id 362
    label "dojechanie"
  ]
  node [
    id 363
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 364
    label "ingress"
  ]
  node [
    id 365
    label "strzelenie"
  ]
  node [
    id 366
    label "orzekni&#281;cie"
  ]
  node [
    id 367
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 368
    label "dolecenie"
  ]
  node [
    id 369
    label "rozpowszechnienie"
  ]
  node [
    id 370
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 371
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 372
    label "stanie_si&#281;"
  ]
  node [
    id 373
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 374
    label "zrobienie"
  ]
  node [
    id 375
    label "odwiedzanie"
  ]
  node [
    id 376
    label "biegni&#281;cie"
  ]
  node [
    id 377
    label "zakre&#347;lanie"
  ]
  node [
    id 378
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 379
    label "okr&#261;&#380;anie"
  ]
  node [
    id 380
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 381
    label "zakre&#347;lenie"
  ]
  node [
    id 382
    label "odwiedzenie"
  ]
  node [
    id 383
    label "okr&#261;&#380;enie"
  ]
  node [
    id 384
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 385
    label "formalizowanie"
  ]
  node [
    id 386
    label "formalnie"
  ]
  node [
    id 387
    label "oficjalnie"
  ]
  node [
    id 388
    label "jawny"
  ]
  node [
    id 389
    label "legalny"
  ]
  node [
    id 390
    label "sformalizowanie"
  ]
  node [
    id 391
    label "pow&#347;ci&#261;gliwie"
  ]
  node [
    id 392
    label "ujawnienie_si&#281;"
  ]
  node [
    id 393
    label "ujawnianie_si&#281;"
  ]
  node [
    id 394
    label "zdecydowany"
  ]
  node [
    id 395
    label "znajomy"
  ]
  node [
    id 396
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 397
    label "jawnie"
  ]
  node [
    id 398
    label "ujawnienie"
  ]
  node [
    id 399
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 400
    label "ujawnianie"
  ]
  node [
    id 401
    label "ewidentny"
  ]
  node [
    id 402
    label "gajny"
  ]
  node [
    id 403
    label "legalnie"
  ]
  node [
    id 404
    label "formalny"
  ]
  node [
    id 405
    label "regularly"
  ]
  node [
    id 406
    label "pozornie"
  ]
  node [
    id 407
    label "kompletnie"
  ]
  node [
    id 408
    label "nadanie"
  ]
  node [
    id 409
    label "sprecyzowanie"
  ]
  node [
    id 410
    label "j&#281;zyk"
  ]
  node [
    id 411
    label "nadawanie"
  ]
  node [
    id 412
    label "precyzowanie"
  ]
  node [
    id 413
    label "do&#322;&#261;czenie"
  ]
  node [
    id 414
    label "message"
  ]
  node [
    id 415
    label "naznoszenie"
  ]
  node [
    id 416
    label "zawiadomienie"
  ]
  node [
    id 417
    label "zniesienie"
  ]
  node [
    id 418
    label "zaniesienie"
  ]
  node [
    id 419
    label "announcement"
  ]
  node [
    id 420
    label "fetch"
  ]
  node [
    id 421
    label "poinformowanie"
  ]
  node [
    id 422
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 423
    label "touch"
  ]
  node [
    id 424
    label "spotkanie"
  ]
  node [
    id 425
    label "dotarcie"
  ]
  node [
    id 426
    label "telling"
  ]
  node [
    id 427
    label "komunikat"
  ]
  node [
    id 428
    label "odniesienie"
  ]
  node [
    id 429
    label "dostarczenie"
  ]
  node [
    id 430
    label "przeniesienie"
  ]
  node [
    id 431
    label "zasnucie_si&#281;"
  ]
  node [
    id 432
    label "gaze"
  ]
  node [
    id 433
    label "zakrycie"
  ]
  node [
    id 434
    label "znoszenie"
  ]
  node [
    id 435
    label "communication"
  ]
  node [
    id 436
    label "signal"
  ]
  node [
    id 437
    label "znie&#347;&#263;"
  ]
  node [
    id 438
    label "znosi&#263;"
  ]
  node [
    id 439
    label "zarys"
  ]
  node [
    id 440
    label "depesza_emska"
  ]
  node [
    id 441
    label "summation"
  ]
  node [
    id 442
    label "ranny"
  ]
  node [
    id 443
    label "jajko"
  ]
  node [
    id 444
    label "zgromadzenie"
  ]
  node [
    id 445
    label "urodzenie"
  ]
  node [
    id 446
    label "suspension"
  ]
  node [
    id 447
    label "poddanie_si&#281;"
  ]
  node [
    id 448
    label "extinction"
  ]
  node [
    id 449
    label "coitus_interruptus"
  ]
  node [
    id 450
    label "przetrwanie"
  ]
  node [
    id 451
    label "&#347;cierpienie"
  ]
  node [
    id 452
    label "abolicjonista"
  ]
  node [
    id 453
    label "zniszczenie"
  ]
  node [
    id 454
    label "posk&#322;adanie"
  ]
  node [
    id 455
    label "zebranie"
  ]
  node [
    id 456
    label "removal"
  ]
  node [
    id 457
    label "withdrawal"
  ]
  node [
    id 458
    label "revocation"
  ]
  node [
    id 459
    label "usuni&#281;cie"
  ]
  node [
    id 460
    label "wygranie"
  ]
  node [
    id 461
    label "porwanie"
  ]
  node [
    id 462
    label "uniewa&#380;nienie"
  ]
  node [
    id 463
    label "aktualnie"
  ]
  node [
    id 464
    label "ci&#261;gle"
  ]
  node [
    id 465
    label "bie&#380;&#261;cy"
  ]
  node [
    id 466
    label "ninie"
  ]
  node [
    id 467
    label "aktualny"
  ]
  node [
    id 468
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 469
    label "stale"
  ]
  node [
    id 470
    label "ci&#261;g&#322;y"
  ]
  node [
    id 471
    label "nieprzerwanie"
  ]
  node [
    id 472
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 473
    label "mie&#263;_miejsce"
  ]
  node [
    id 474
    label "equal"
  ]
  node [
    id 475
    label "chodzi&#263;"
  ]
  node [
    id 476
    label "si&#281;ga&#263;"
  ]
  node [
    id 477
    label "stan"
  ]
  node [
    id 478
    label "obecno&#347;&#263;"
  ]
  node [
    id 479
    label "stand"
  ]
  node [
    id 480
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 481
    label "uczestniczy&#263;"
  ]
  node [
    id 482
    label "participate"
  ]
  node [
    id 483
    label "istnie&#263;"
  ]
  node [
    id 484
    label "pozostawa&#263;"
  ]
  node [
    id 485
    label "zostawa&#263;"
  ]
  node [
    id 486
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 487
    label "adhere"
  ]
  node [
    id 488
    label "compass"
  ]
  node [
    id 489
    label "korzysta&#263;"
  ]
  node [
    id 490
    label "appreciation"
  ]
  node [
    id 491
    label "osi&#261;ga&#263;"
  ]
  node [
    id 492
    label "dociera&#263;"
  ]
  node [
    id 493
    label "mierzy&#263;"
  ]
  node [
    id 494
    label "u&#380;ywa&#263;"
  ]
  node [
    id 495
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 496
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 497
    label "exsert"
  ]
  node [
    id 498
    label "being"
  ]
  node [
    id 499
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 500
    label "cecha"
  ]
  node [
    id 501
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 502
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 503
    label "p&#322;ywa&#263;"
  ]
  node [
    id 504
    label "run"
  ]
  node [
    id 505
    label "bangla&#263;"
  ]
  node [
    id 506
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 507
    label "przebiega&#263;"
  ]
  node [
    id 508
    label "wk&#322;ada&#263;"
  ]
  node [
    id 509
    label "proceed"
  ]
  node [
    id 510
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 511
    label "carry"
  ]
  node [
    id 512
    label "bywa&#263;"
  ]
  node [
    id 513
    label "dziama&#263;"
  ]
  node [
    id 514
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 515
    label "stara&#263;_si&#281;"
  ]
  node [
    id 516
    label "para"
  ]
  node [
    id 517
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 518
    label "str&#243;j"
  ]
  node [
    id 519
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 520
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 521
    label "krok"
  ]
  node [
    id 522
    label "tryb"
  ]
  node [
    id 523
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 524
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 525
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 526
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 527
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 528
    label "continue"
  ]
  node [
    id 529
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 530
    label "Ohio"
  ]
  node [
    id 531
    label "wci&#281;cie"
  ]
  node [
    id 532
    label "Nowy_York"
  ]
  node [
    id 533
    label "warstwa"
  ]
  node [
    id 534
    label "samopoczucie"
  ]
  node [
    id 535
    label "Illinois"
  ]
  node [
    id 536
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 537
    label "state"
  ]
  node [
    id 538
    label "Jukatan"
  ]
  node [
    id 539
    label "Kalifornia"
  ]
  node [
    id 540
    label "Wirginia"
  ]
  node [
    id 541
    label "wektor"
  ]
  node [
    id 542
    label "Teksas"
  ]
  node [
    id 543
    label "Goa"
  ]
  node [
    id 544
    label "Waszyngton"
  ]
  node [
    id 545
    label "Massachusetts"
  ]
  node [
    id 546
    label "Alaska"
  ]
  node [
    id 547
    label "Arakan"
  ]
  node [
    id 548
    label "Hawaje"
  ]
  node [
    id 549
    label "Maryland"
  ]
  node [
    id 550
    label "Michigan"
  ]
  node [
    id 551
    label "Arizona"
  ]
  node [
    id 552
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 553
    label "Georgia"
  ]
  node [
    id 554
    label "poziom"
  ]
  node [
    id 555
    label "Pensylwania"
  ]
  node [
    id 556
    label "shape"
  ]
  node [
    id 557
    label "Luizjana"
  ]
  node [
    id 558
    label "Nowy_Meksyk"
  ]
  node [
    id 559
    label "Alabama"
  ]
  node [
    id 560
    label "ilo&#347;&#263;"
  ]
  node [
    id 561
    label "Kansas"
  ]
  node [
    id 562
    label "Oregon"
  ]
  node [
    id 563
    label "Floryda"
  ]
  node [
    id 564
    label "Oklahoma"
  ]
  node [
    id 565
    label "jednostka_administracyjna"
  ]
  node [
    id 566
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 567
    label "niezmierny"
  ]
  node [
    id 568
    label "szczytnie"
  ]
  node [
    id 569
    label "ogromnie"
  ]
  node [
    id 570
    label "ogromny"
  ]
  node [
    id 571
    label "olbrzymi"
  ]
  node [
    id 572
    label "dono&#347;nie"
  ]
  node [
    id 573
    label "bardzo"
  ]
  node [
    id 574
    label "intensywnie"
  ]
  node [
    id 575
    label "wznio&#347;le"
  ]
  node [
    id 576
    label "chwalebnie"
  ]
  node [
    id 577
    label "szczytny"
  ]
  node [
    id 578
    label "otwarty"
  ]
  node [
    id 579
    label "rozleg&#322;y"
  ]
  node [
    id 580
    label "nieograniczenie"
  ]
  node [
    id 581
    label "nieprzyjemnie"
  ]
  node [
    id 582
    label "niepo&#380;&#261;danie"
  ]
  node [
    id 583
    label "przykry"
  ]
  node [
    id 584
    label "&#378;le"
  ]
  node [
    id 585
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 586
    label "niemi&#322;y"
  ]
  node [
    id 587
    label "unpleasantly"
  ]
  node [
    id 588
    label "nieprzyjemny"
  ]
  node [
    id 589
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 590
    label "przeciwnie"
  ]
  node [
    id 591
    label "niegrzeczny"
  ]
  node [
    id 592
    label "dokuczliwy"
  ]
  node [
    id 593
    label "rezultat"
  ]
  node [
    id 594
    label "dzia&#322;anie"
  ]
  node [
    id 595
    label "typ"
  ]
  node [
    id 596
    label "event"
  ]
  node [
    id 597
    label "przyczyna"
  ]
  node [
    id 598
    label "bust"
  ]
  node [
    id 599
    label "scramble"
  ]
  node [
    id 600
    label "targanina"
  ]
  node [
    id 601
    label "wyszarpywanie"
  ]
  node [
    id 602
    label "zmagania"
  ]
  node [
    id 603
    label "poszarpanie"
  ]
  node [
    id 604
    label "wyszarpanie"
  ]
  node [
    id 605
    label "problem"
  ]
  node [
    id 606
    label "struggle"
  ]
  node [
    id 607
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 608
    label "sytuacja"
  ]
  node [
    id 609
    label "zaj&#347;cie"
  ]
  node [
    id 610
    label "wydostawanie"
  ]
  node [
    id 611
    label "pozyskiwanie"
  ]
  node [
    id 612
    label "poranienie"
  ]
  node [
    id 613
    label "rozerwanie"
  ]
  node [
    id 614
    label "laceration"
  ]
  node [
    id 615
    label "ukszta&#322;towanie"
  ]
  node [
    id 616
    label "zmienienie"
  ]
  node [
    id 617
    label "pozyskanie"
  ]
  node [
    id 618
    label "wydostanie"
  ]
  node [
    id 619
    label "adhesion"
  ]
  node [
    id 620
    label "gravity"
  ]
  node [
    id 621
    label "wyjmowanie"
  ]
  node [
    id 622
    label "doci&#261;ganie"
  ]
  node [
    id 623
    label "compression"
  ]
  node [
    id 624
    label "wyt&#322;aczanie"
  ]
  node [
    id 625
    label "pobranie"
  ]
  node [
    id 626
    label "zabieranie"
  ]
  node [
    id 627
    label "tension"
  ]
  node [
    id 628
    label "wianie"
  ]
  node [
    id 629
    label "radzenie_sobie"
  ]
  node [
    id 630
    label "doci&#261;gni&#281;cie"
  ]
  node [
    id 631
    label "grip"
  ]
  node [
    id 632
    label "ci&#261;ganie"
  ]
  node [
    id 633
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 634
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 635
    label "powracanie"
  ]
  node [
    id 636
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 637
    label "podci&#261;ganie"
  ]
  node [
    id 638
    label "przyci&#261;ganie"
  ]
  node [
    id 639
    label "przemieszczanie"
  ]
  node [
    id 640
    label "wa&#380;enie"
  ]
  node [
    id 641
    label "przewlekanie"
  ]
  node [
    id 642
    label "poci&#261;ganie"
  ]
  node [
    id 643
    label "draw"
  ]
  node [
    id 644
    label "podci&#261;gni&#281;cie"
  ]
  node [
    id 645
    label "zaci&#261;ganie"
  ]
  node [
    id 646
    label "zmierzanie"
  ]
  node [
    id 647
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 648
    label "oddzieranie"
  ]
  node [
    id 649
    label "wyd&#322;u&#380;anie"
  ]
  node [
    id 650
    label "obrabianie"
  ]
  node [
    id 651
    label "continuance"
  ]
  node [
    id 652
    label "urwanie"
  ]
  node [
    id 653
    label "oddarcie"
  ]
  node [
    id 654
    label "przewo&#380;enie"
  ]
  node [
    id 655
    label "przesuwanie"
  ]
  node [
    id 656
    label "zerwanie"
  ]
  node [
    id 657
    label "ruszanie"
  ]
  node [
    id 658
    label "chcenie"
  ]
  node [
    id 659
    label "wch&#322;anianie"
  ]
  node [
    id 660
    label "traction"
  ]
  node [
    id 661
    label "urywanie"
  ]
  node [
    id 662
    label "przeci&#261;ganie"
  ]
  node [
    id 663
    label "pow&#322;&#243;czenie"
  ]
  node [
    id 664
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 665
    label "przyci&#261;gni&#281;cie"
  ]
  node [
    id 666
    label "set"
  ]
  node [
    id 667
    label "ploy"
  ]
  node [
    id 668
    label "wydarzenie"
  ]
  node [
    id 669
    label "skrycie_si&#281;"
  ]
  node [
    id 670
    label "happening"
  ]
  node [
    id 671
    label "porobienie_si&#281;"
  ]
  node [
    id 672
    label "krajobraz"
  ]
  node [
    id 673
    label "przyobleczenie_si&#281;"
  ]
  node [
    id 674
    label "podej&#347;cie"
  ]
  node [
    id 675
    label "przestanie"
  ]
  node [
    id 676
    label "subiekcja"
  ]
  node [
    id 677
    label "jajko_Kolumba"
  ]
  node [
    id 678
    label "obstruction"
  ]
  node [
    id 679
    label "trudno&#347;&#263;"
  ]
  node [
    id 680
    label "pierepa&#322;ka"
  ]
  node [
    id 681
    label "ambaras"
  ]
  node [
    id 682
    label "warunki"
  ]
  node [
    id 683
    label "szczeg&#243;&#322;"
  ]
  node [
    id 684
    label "motyw"
  ]
  node [
    id 685
    label "realia"
  ]
  node [
    id 686
    label "obrona"
  ]
  node [
    id 687
    label "konfrontacyjny"
  ]
  node [
    id 688
    label "zaatakowanie"
  ]
  node [
    id 689
    label "contest"
  ]
  node [
    id 690
    label "sambo"
  ]
  node [
    id 691
    label "wrestle"
  ]
  node [
    id 692
    label "military_action"
  ]
  node [
    id 693
    label "szamotanina"
  ]
  node [
    id 694
    label "utulenie"
  ]
  node [
    id 695
    label "pediatra"
  ]
  node [
    id 696
    label "dzieciak"
  ]
  node [
    id 697
    label "utulanie"
  ]
  node [
    id 698
    label "dzieciarnia"
  ]
  node [
    id 699
    label "niepe&#322;noletni"
  ]
  node [
    id 700
    label "organizm"
  ]
  node [
    id 701
    label "utula&#263;"
  ]
  node [
    id 702
    label "cz&#322;owieczek"
  ]
  node [
    id 703
    label "fledgling"
  ]
  node [
    id 704
    label "utuli&#263;"
  ]
  node [
    id 705
    label "m&#322;odzik"
  ]
  node [
    id 706
    label "pedofil"
  ]
  node [
    id 707
    label "m&#322;odziak"
  ]
  node [
    id 708
    label "potomek"
  ]
  node [
    id 709
    label "entliczek-pentliczek"
  ]
  node [
    id 710
    label "potomstwo"
  ]
  node [
    id 711
    label "sraluch"
  ]
  node [
    id 712
    label "czeladka"
  ]
  node [
    id 713
    label "dzietno&#347;&#263;"
  ]
  node [
    id 714
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 715
    label "bawienie_si&#281;"
  ]
  node [
    id 716
    label "pomiot"
  ]
  node [
    id 717
    label "grupa"
  ]
  node [
    id 718
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 719
    label "kinderbal"
  ]
  node [
    id 720
    label "krewny"
  ]
  node [
    id 721
    label "ma&#322;oletny"
  ]
  node [
    id 722
    label "m&#322;ody"
  ]
  node [
    id 723
    label "p&#322;aszczyzna"
  ]
  node [
    id 724
    label "odwadnia&#263;"
  ]
  node [
    id 725
    label "przyswoi&#263;"
  ]
  node [
    id 726
    label "sk&#243;ra"
  ]
  node [
    id 727
    label "odwodni&#263;"
  ]
  node [
    id 728
    label "ewoluowanie"
  ]
  node [
    id 729
    label "staw"
  ]
  node [
    id 730
    label "ow&#322;osienie"
  ]
  node [
    id 731
    label "unerwienie"
  ]
  node [
    id 732
    label "reakcja"
  ]
  node [
    id 733
    label "wyewoluowanie"
  ]
  node [
    id 734
    label "przyswajanie"
  ]
  node [
    id 735
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 736
    label "wyewoluowa&#263;"
  ]
  node [
    id 737
    label "biorytm"
  ]
  node [
    id 738
    label "ewoluowa&#263;"
  ]
  node [
    id 739
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 740
    label "otworzy&#263;"
  ]
  node [
    id 741
    label "otwiera&#263;"
  ]
  node [
    id 742
    label "czynnik_biotyczny"
  ]
  node [
    id 743
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 744
    label "otworzenie"
  ]
  node [
    id 745
    label "otwieranie"
  ]
  node [
    id 746
    label "individual"
  ]
  node [
    id 747
    label "ty&#322;"
  ]
  node [
    id 748
    label "szkielet"
  ]
  node [
    id 749
    label "przyswaja&#263;"
  ]
  node [
    id 750
    label "przyswojenie"
  ]
  node [
    id 751
    label "odwadnianie"
  ]
  node [
    id 752
    label "odwodnienie"
  ]
  node [
    id 753
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 754
    label "starzenie_si&#281;"
  ]
  node [
    id 755
    label "uk&#322;ad"
  ]
  node [
    id 756
    label "prz&#243;d"
  ]
  node [
    id 757
    label "temperatura"
  ]
  node [
    id 758
    label "l&#281;d&#378;wie"
  ]
  node [
    id 759
    label "cia&#322;o"
  ]
  node [
    id 760
    label "cz&#322;onek"
  ]
  node [
    id 761
    label "degenerat"
  ]
  node [
    id 762
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 763
    label "zwyrol"
  ]
  node [
    id 764
    label "czerniak"
  ]
  node [
    id 765
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 766
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 767
    label "paszcza"
  ]
  node [
    id 768
    label "popapraniec"
  ]
  node [
    id 769
    label "skuba&#263;"
  ]
  node [
    id 770
    label "skubanie"
  ]
  node [
    id 771
    label "skubni&#281;cie"
  ]
  node [
    id 772
    label "agresja"
  ]
  node [
    id 773
    label "zwierz&#281;ta"
  ]
  node [
    id 774
    label "fukni&#281;cie"
  ]
  node [
    id 775
    label "farba"
  ]
  node [
    id 776
    label "fukanie"
  ]
  node [
    id 777
    label "gad"
  ]
  node [
    id 778
    label "siedzie&#263;"
  ]
  node [
    id 779
    label "oswaja&#263;"
  ]
  node [
    id 780
    label "tresowa&#263;"
  ]
  node [
    id 781
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 782
    label "poligamia"
  ]
  node [
    id 783
    label "oz&#243;r"
  ]
  node [
    id 784
    label "skubn&#261;&#263;"
  ]
  node [
    id 785
    label "wios&#322;owa&#263;"
  ]
  node [
    id 786
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 787
    label "le&#380;enie"
  ]
  node [
    id 788
    label "niecz&#322;owiek"
  ]
  node [
    id 789
    label "wios&#322;owanie"
  ]
  node [
    id 790
    label "napasienie_si&#281;"
  ]
  node [
    id 791
    label "wiwarium"
  ]
  node [
    id 792
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 793
    label "animalista"
  ]
  node [
    id 794
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 795
    label "budowa"
  ]
  node [
    id 796
    label "hodowla"
  ]
  node [
    id 797
    label "pasienie_si&#281;"
  ]
  node [
    id 798
    label "sodomita"
  ]
  node [
    id 799
    label "monogamia"
  ]
  node [
    id 800
    label "przyssawka"
  ]
  node [
    id 801
    label "zachowanie"
  ]
  node [
    id 802
    label "budowa_cia&#322;a"
  ]
  node [
    id 803
    label "okrutnik"
  ]
  node [
    id 804
    label "grzbiet"
  ]
  node [
    id 805
    label "weterynarz"
  ]
  node [
    id 806
    label "&#322;eb"
  ]
  node [
    id 807
    label "wylinka"
  ]
  node [
    id 808
    label "bestia"
  ]
  node [
    id 809
    label "poskramia&#263;"
  ]
  node [
    id 810
    label "fauna"
  ]
  node [
    id 811
    label "treser"
  ]
  node [
    id 812
    label "siedzenie"
  ]
  node [
    id 813
    label "le&#380;e&#263;"
  ]
  node [
    id 814
    label "uspokojenie"
  ]
  node [
    id 815
    label "utulenie_si&#281;"
  ]
  node [
    id 816
    label "u&#347;pienie"
  ]
  node [
    id 817
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 818
    label "uspokoi&#263;"
  ]
  node [
    id 819
    label "utulanie_si&#281;"
  ]
  node [
    id 820
    label "usypianie"
  ]
  node [
    id 821
    label "pocieszanie"
  ]
  node [
    id 822
    label "uspokajanie"
  ]
  node [
    id 823
    label "usypia&#263;"
  ]
  node [
    id 824
    label "uspokaja&#263;"
  ]
  node [
    id 825
    label "wyliczanka"
  ]
  node [
    id 826
    label "specjalista"
  ]
  node [
    id 827
    label "harcerz"
  ]
  node [
    id 828
    label "ch&#322;opta&#347;"
  ]
  node [
    id 829
    label "zawodnik"
  ]
  node [
    id 830
    label "go&#322;ow&#261;s"
  ]
  node [
    id 831
    label "m&#322;ode"
  ]
  node [
    id 832
    label "stopie&#324;_harcerski"
  ]
  node [
    id 833
    label "g&#243;wniarz"
  ]
  node [
    id 834
    label "beniaminek"
  ]
  node [
    id 835
    label "dewiant"
  ]
  node [
    id 836
    label "istotka"
  ]
  node [
    id 837
    label "bech"
  ]
  node [
    id 838
    label "dziecinny"
  ]
  node [
    id 839
    label "naiwniak"
  ]
  node [
    id 840
    label "kszta&#322;ciciel"
  ]
  node [
    id 841
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 842
    label "kuwada"
  ]
  node [
    id 843
    label "tworzyciel"
  ]
  node [
    id 844
    label "rodzice"
  ]
  node [
    id 845
    label "&#347;w"
  ]
  node [
    id 846
    label "pomys&#322;odawca"
  ]
  node [
    id 847
    label "rodzic"
  ]
  node [
    id 848
    label "wykonawca"
  ]
  node [
    id 849
    label "ojczym"
  ]
  node [
    id 850
    label "przodek"
  ]
  node [
    id 851
    label "papa"
  ]
  node [
    id 852
    label "zakonnik"
  ]
  node [
    id 853
    label "stary"
  ]
  node [
    id 854
    label "br"
  ]
  node [
    id 855
    label "mnich"
  ]
  node [
    id 856
    label "zakon"
  ]
  node [
    id 857
    label "wyznawca"
  ]
  node [
    id 858
    label "opiekun"
  ]
  node [
    id 859
    label "rodzic_chrzestny"
  ]
  node [
    id 860
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 861
    label "ojcowie"
  ]
  node [
    id 862
    label "linea&#380;"
  ]
  node [
    id 863
    label "chodnik"
  ]
  node [
    id 864
    label "w&#243;z"
  ]
  node [
    id 865
    label "p&#322;ug"
  ]
  node [
    id 866
    label "wyrobisko"
  ]
  node [
    id 867
    label "dziad"
  ]
  node [
    id 868
    label "antecesor"
  ]
  node [
    id 869
    label "post&#281;p"
  ]
  node [
    id 870
    label "inicjator"
  ]
  node [
    id 871
    label "podmiot_gospodarczy"
  ]
  node [
    id 872
    label "artysta"
  ]
  node [
    id 873
    label "muzyk"
  ]
  node [
    id 874
    label "materia&#322;_budowlany"
  ]
  node [
    id 875
    label "twarz"
  ]
  node [
    id 876
    label "gun_muzzle"
  ]
  node [
    id 877
    label "izolacja"
  ]
  node [
    id 878
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 879
    label "nienowoczesny"
  ]
  node [
    id 880
    label "gruba_ryba"
  ]
  node [
    id 881
    label "zestarzenie_si&#281;"
  ]
  node [
    id 882
    label "poprzedni"
  ]
  node [
    id 883
    label "dawno"
  ]
  node [
    id 884
    label "staro"
  ]
  node [
    id 885
    label "m&#261;&#380;"
  ]
  node [
    id 886
    label "starzy"
  ]
  node [
    id 887
    label "dotychczasowy"
  ]
  node [
    id 888
    label "p&#243;&#378;ny"
  ]
  node [
    id 889
    label "d&#322;ugoletni"
  ]
  node [
    id 890
    label "charakterystyczny"
  ]
  node [
    id 891
    label "brat"
  ]
  node [
    id 892
    label "po_staro&#347;wiecku"
  ]
  node [
    id 893
    label "zwierzchnik"
  ]
  node [
    id 894
    label "odleg&#322;y"
  ]
  node [
    id 895
    label "starczo"
  ]
  node [
    id 896
    label "dawniej"
  ]
  node [
    id 897
    label "niegdysiejszy"
  ]
  node [
    id 898
    label "nauczyciel"
  ]
  node [
    id 899
    label "autor"
  ]
  node [
    id 900
    label "doros&#322;y"
  ]
  node [
    id 901
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 902
    label "jegomo&#347;&#263;"
  ]
  node [
    id 903
    label "andropauza"
  ]
  node [
    id 904
    label "pa&#324;stwo"
  ]
  node [
    id 905
    label "bratek"
  ]
  node [
    id 906
    label "ch&#322;opina"
  ]
  node [
    id 907
    label "twardziel"
  ]
  node [
    id 908
    label "androlog"
  ]
  node [
    id 909
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 910
    label "pokolenie"
  ]
  node [
    id 911
    label "wapniaki"
  ]
  node [
    id 912
    label "syndrom_kuwady"
  ]
  node [
    id 913
    label "na&#347;ladownictwo"
  ]
  node [
    id 914
    label "zwyczaj"
  ]
  node [
    id 915
    label "ci&#261;&#380;a"
  ]
  node [
    id 916
    label "&#380;onaty"
  ]
  node [
    id 917
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 918
    label "sprawdza&#263;"
  ]
  node [
    id 919
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 920
    label "feel"
  ]
  node [
    id 921
    label "try"
  ]
  node [
    id 922
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 923
    label "przedstawienie"
  ]
  node [
    id 924
    label "kosztowa&#263;"
  ]
  node [
    id 925
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 926
    label "examine"
  ]
  node [
    id 927
    label "szpiegowa&#263;"
  ]
  node [
    id 928
    label "konsumowa&#263;"
  ]
  node [
    id 929
    label "savor"
  ]
  node [
    id 930
    label "cena"
  ]
  node [
    id 931
    label "doznawa&#263;"
  ]
  node [
    id 932
    label "essay"
  ]
  node [
    id 933
    label "pr&#243;bowanie"
  ]
  node [
    id 934
    label "zademonstrowanie"
  ]
  node [
    id 935
    label "obgadanie"
  ]
  node [
    id 936
    label "realizacja"
  ]
  node [
    id 937
    label "scena"
  ]
  node [
    id 938
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 939
    label "narration"
  ]
  node [
    id 940
    label "cyrk"
  ]
  node [
    id 941
    label "wytw&#243;r"
  ]
  node [
    id 942
    label "theatrical_performance"
  ]
  node [
    id 943
    label "opisanie"
  ]
  node [
    id 944
    label "malarstwo"
  ]
  node [
    id 945
    label "scenografia"
  ]
  node [
    id 946
    label "teatr"
  ]
  node [
    id 947
    label "ukazanie"
  ]
  node [
    id 948
    label "zapoznanie"
  ]
  node [
    id 949
    label "pokaz"
  ]
  node [
    id 950
    label "podanie"
  ]
  node [
    id 951
    label "spos&#243;b"
  ]
  node [
    id 952
    label "ods&#322;ona"
  ]
  node [
    id 953
    label "exhibit"
  ]
  node [
    id 954
    label "pokazanie"
  ]
  node [
    id 955
    label "wyst&#261;pienie"
  ]
  node [
    id 956
    label "przedstawi&#263;"
  ]
  node [
    id 957
    label "przedstawianie"
  ]
  node [
    id 958
    label "przedstawia&#263;"
  ]
  node [
    id 959
    label "rola"
  ]
  node [
    id 960
    label "prze&#380;ycie"
  ]
  node [
    id 961
    label "wstrz&#261;s"
  ]
  node [
    id 962
    label "shock_absorber"
  ]
  node [
    id 963
    label "zaskoczenie"
  ]
  node [
    id 964
    label "zrozumienie"
  ]
  node [
    id 965
    label "siurpryza"
  ]
  node [
    id 966
    label "wzbudzenie"
  ]
  node [
    id 967
    label "zaskok"
  ]
  node [
    id 968
    label "surprise"
  ]
  node [
    id 969
    label "zdziwienie"
  ]
  node [
    id 970
    label "wra&#380;enie"
  ]
  node [
    id 971
    label "przej&#347;cie"
  ]
  node [
    id 972
    label "poradzenie_sobie"
  ]
  node [
    id 973
    label "survival"
  ]
  node [
    id 974
    label "zaburzenie"
  ]
  node [
    id 975
    label "oznaka"
  ]
  node [
    id 976
    label "impact"
  ]
  node [
    id 977
    label "ruch"
  ]
  node [
    id 978
    label "zmiana"
  ]
  node [
    id 979
    label "wybawia&#263;"
  ]
  node [
    id 980
    label "deliver"
  ]
  node [
    id 981
    label "zapobiega&#263;"
  ]
  node [
    id 982
    label "stanowi&#263;"
  ]
  node [
    id 983
    label "oddala&#263;"
  ]
  node [
    id 984
    label "anticipate"
  ]
  node [
    id 985
    label "oddalenie"
  ]
  node [
    id 986
    label "remove"
  ]
  node [
    id 987
    label "pokazywa&#263;"
  ]
  node [
    id 988
    label "nakazywa&#263;"
  ]
  node [
    id 989
    label "przemieszcza&#263;"
  ]
  node [
    id 990
    label "oddali&#263;"
  ]
  node [
    id 991
    label "dissolve"
  ]
  node [
    id 992
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 993
    label "oddalanie"
  ]
  node [
    id 994
    label "retard"
  ]
  node [
    id 995
    label "sprawia&#263;"
  ]
  node [
    id 996
    label "odrzuca&#263;"
  ]
  node [
    id 997
    label "zwalnia&#263;"
  ]
  node [
    id 998
    label "odk&#322;ada&#263;"
  ]
  node [
    id 999
    label "determine"
  ]
  node [
    id 1000
    label "work"
  ]
  node [
    id 1001
    label "powodowa&#263;"
  ]
  node [
    id 1002
    label "reakcja_chemiczna"
  ]
  node [
    id 1003
    label "uwalnia&#263;"
  ]
  node [
    id 1004
    label "decide"
  ]
  node [
    id 1005
    label "pies_my&#347;liwski"
  ]
  node [
    id 1006
    label "decydowa&#263;"
  ]
  node [
    id 1007
    label "represent"
  ]
  node [
    id 1008
    label "zatrzymywa&#263;"
  ]
  node [
    id 1009
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1010
    label "typify"
  ]
  node [
    id 1011
    label "dziewka"
  ]
  node [
    id 1012
    label "sikorka"
  ]
  node [
    id 1013
    label "kora"
  ]
  node [
    id 1014
    label "dziewcz&#281;"
  ]
  node [
    id 1015
    label "dziewoja"
  ]
  node [
    id 1016
    label "dziecina"
  ]
  node [
    id 1017
    label "m&#322;&#243;dka"
  ]
  node [
    id 1018
    label "dziunia"
  ]
  node [
    id 1019
    label "dziewczynina"
  ]
  node [
    id 1020
    label "siksa"
  ]
  node [
    id 1021
    label "potomkini"
  ]
  node [
    id 1022
    label "krewna"
  ]
  node [
    id 1023
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 1024
    label "dziewczyna"
  ]
  node [
    id 1025
    label "prostytutka"
  ]
  node [
    id 1026
    label "ma&#322;olata"
  ]
  node [
    id 1027
    label "sikora"
  ]
  node [
    id 1028
    label "panna"
  ]
  node [
    id 1029
    label "laska"
  ]
  node [
    id 1030
    label "zwrot"
  ]
  node [
    id 1031
    label "crust"
  ]
  node [
    id 1032
    label "ciasto"
  ]
  node [
    id 1033
    label "szabla"
  ]
  node [
    id 1034
    label "drzewko"
  ]
  node [
    id 1035
    label "drzewo"
  ]
  node [
    id 1036
    label "po&#347;ciel&#243;wka"
  ]
  node [
    id 1037
    label "harfa"
  ]
  node [
    id 1038
    label "bawe&#322;na"
  ]
  node [
    id 1039
    label "tkanka_sta&#322;a"
  ]
  node [
    id 1040
    label "piskl&#281;"
  ]
  node [
    id 1041
    label "samica"
  ]
  node [
    id 1042
    label "ptak"
  ]
  node [
    id 1043
    label "upierzenie"
  ]
  node [
    id 1044
    label "kobieta"
  ]
  node [
    id 1045
    label "m&#322;odzie&#380;"
  ]
  node [
    id 1046
    label "mo&#322;odyca"
  ]
  node [
    id 1047
    label "usynowienie"
  ]
  node [
    id 1048
    label "usynawianie"
  ]
  node [
    id 1049
    label "przysposabianie"
  ]
  node [
    id 1050
    label "przysposobienie"
  ]
  node [
    id 1051
    label "adoption"
  ]
  node [
    id 1052
    label "urazi&#263;"
  ]
  node [
    id 1053
    label "strike"
  ]
  node [
    id 1054
    label "wystartowa&#263;"
  ]
  node [
    id 1055
    label "przywali&#263;"
  ]
  node [
    id 1056
    label "dupn&#261;&#263;"
  ]
  node [
    id 1057
    label "skrytykowa&#263;"
  ]
  node [
    id 1058
    label "nast&#261;pi&#263;"
  ]
  node [
    id 1059
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 1060
    label "sztachn&#261;&#263;"
  ]
  node [
    id 1061
    label "rap"
  ]
  node [
    id 1062
    label "zrobi&#263;"
  ]
  node [
    id 1063
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1064
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 1065
    label "crush"
  ]
  node [
    id 1066
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 1067
    label "postara&#263;_si&#281;"
  ]
  node [
    id 1068
    label "fall"
  ]
  node [
    id 1069
    label "hopn&#261;&#263;"
  ]
  node [
    id 1070
    label "zada&#263;"
  ]
  node [
    id 1071
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1072
    label "dotkn&#261;&#263;"
  ]
  node [
    id 1073
    label "anoint"
  ]
  node [
    id 1074
    label "transgress"
  ]
  node [
    id 1075
    label "chop"
  ]
  node [
    id 1076
    label "jebn&#261;&#263;"
  ]
  node [
    id 1077
    label "lumber"
  ]
  node [
    id 1078
    label "act"
  ]
  node [
    id 1079
    label "zaopiniowa&#263;"
  ]
  node [
    id 1080
    label "review"
  ]
  node [
    id 1081
    label "oceni&#263;"
  ]
  node [
    id 1082
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 1083
    label "spotka&#263;"
  ]
  node [
    id 1084
    label "dotkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1085
    label "allude"
  ]
  node [
    id 1086
    label "range"
  ]
  node [
    id 1087
    label "diss"
  ]
  node [
    id 1088
    label "pique"
  ]
  node [
    id 1089
    label "wzbudzi&#263;"
  ]
  node [
    id 1090
    label "post&#261;pi&#263;"
  ]
  node [
    id 1091
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1092
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1093
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1094
    label "zorganizowa&#263;"
  ]
  node [
    id 1095
    label "appoint"
  ]
  node [
    id 1096
    label "wystylizowa&#263;"
  ]
  node [
    id 1097
    label "cause"
  ]
  node [
    id 1098
    label "przerobi&#263;"
  ]
  node [
    id 1099
    label "nabra&#263;"
  ]
  node [
    id 1100
    label "make"
  ]
  node [
    id 1101
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1102
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1103
    label "wydali&#263;"
  ]
  node [
    id 1104
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 1105
    label "fall_upon"
  ]
  node [
    id 1106
    label "zrani&#263;"
  ]
  node [
    id 1107
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 1108
    label "zaszkodzi&#263;"
  ]
  node [
    id 1109
    label "put"
  ]
  node [
    id 1110
    label "deal"
  ]
  node [
    id 1111
    label "zaj&#261;&#263;"
  ]
  node [
    id 1112
    label "distribute"
  ]
  node [
    id 1113
    label "nakarmi&#263;"
  ]
  node [
    id 1114
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 1115
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 1116
    label "pieprzn&#261;&#263;"
  ]
  node [
    id 1117
    label "podrzuci&#263;"
  ]
  node [
    id 1118
    label "dokuczy&#263;"
  ]
  node [
    id 1119
    label "overwhelm"
  ]
  node [
    id 1120
    label "przyrzn&#261;&#263;"
  ]
  node [
    id 1121
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 1122
    label "przygnie&#347;&#263;"
  ]
  node [
    id 1123
    label "muzyka_rozrywkowa"
  ]
  node [
    id 1124
    label "karpiowate"
  ]
  node [
    id 1125
    label "ryba"
  ]
  node [
    id 1126
    label "czarna_muzyka"
  ]
  node [
    id 1127
    label "drapie&#380;nik"
  ]
  node [
    id 1128
    label "asp"
  ]
  node [
    id 1129
    label "waln&#261;&#263;"
  ]
  node [
    id 1130
    label "originate"
  ]
  node [
    id 1131
    label "zakrzykn&#261;&#263;"
  ]
  node [
    id 1132
    label "skoczy&#263;"
  ]
  node [
    id 1133
    label "poskoczy&#263;"
  ]
  node [
    id 1134
    label "zaliczy&#263;"
  ]
  node [
    id 1135
    label "wybuchn&#261;&#263;"
  ]
  node [
    id 1136
    label "spa&#347;&#263;"
  ]
  node [
    id 1137
    label "z&#322;apa&#263;"
  ]
  node [
    id 1138
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 1139
    label "zaatakowa&#263;"
  ]
  node [
    id 1140
    label "nacisn&#261;&#263;"
  ]
  node [
    id 1141
    label "gamble"
  ]
  node [
    id 1142
    label "odej&#347;&#263;"
  ]
  node [
    id 1143
    label "zaleci&#263;_si&#281;"
  ]
  node [
    id 1144
    label "samolot"
  ]
  node [
    id 1145
    label "jedyny"
  ]
  node [
    id 1146
    label "du&#380;y"
  ]
  node [
    id 1147
    label "zdr&#243;w"
  ]
  node [
    id 1148
    label "calu&#347;ko"
  ]
  node [
    id 1149
    label "kompletny"
  ]
  node [
    id 1150
    label "&#380;ywy"
  ]
  node [
    id 1151
    label "pe&#322;ny"
  ]
  node [
    id 1152
    label "podobny"
  ]
  node [
    id 1153
    label "ca&#322;o"
  ]
  node [
    id 1154
    label "zupe&#322;ny"
  ]
  node [
    id 1155
    label "w_pizdu"
  ]
  node [
    id 1156
    label "przypominanie"
  ]
  node [
    id 1157
    label "podobnie"
  ]
  node [
    id 1158
    label "upodabnianie_si&#281;"
  ]
  node [
    id 1159
    label "upodobnienie"
  ]
  node [
    id 1160
    label "drugi"
  ]
  node [
    id 1161
    label "taki"
  ]
  node [
    id 1162
    label "upodobnienie_si&#281;"
  ]
  node [
    id 1163
    label "zasymilowanie"
  ]
  node [
    id 1164
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1165
    label "ukochany"
  ]
  node [
    id 1166
    label "najlepszy"
  ]
  node [
    id 1167
    label "optymalnie"
  ]
  node [
    id 1168
    label "znaczny"
  ]
  node [
    id 1169
    label "niema&#322;o"
  ]
  node [
    id 1170
    label "wiele"
  ]
  node [
    id 1171
    label "rozwini&#281;ty"
  ]
  node [
    id 1172
    label "dorodny"
  ]
  node [
    id 1173
    label "wa&#380;ny"
  ]
  node [
    id 1174
    label "prawdziwy"
  ]
  node [
    id 1175
    label "du&#380;o"
  ]
  node [
    id 1176
    label "zdrowy"
  ]
  node [
    id 1177
    label "ciekawy"
  ]
  node [
    id 1178
    label "szybki"
  ]
  node [
    id 1179
    label "&#380;ywotny"
  ]
  node [
    id 1180
    label "naturalny"
  ]
  node [
    id 1181
    label "&#380;ywo"
  ]
  node [
    id 1182
    label "o&#380;ywianie"
  ]
  node [
    id 1183
    label "&#380;ycie"
  ]
  node [
    id 1184
    label "silny"
  ]
  node [
    id 1185
    label "g&#322;&#281;boki"
  ]
  node [
    id 1186
    label "wyra&#378;ny"
  ]
  node [
    id 1187
    label "czynny"
  ]
  node [
    id 1188
    label "zgrabny"
  ]
  node [
    id 1189
    label "realistyczny"
  ]
  node [
    id 1190
    label "energiczny"
  ]
  node [
    id 1191
    label "nieograniczony"
  ]
  node [
    id 1192
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1193
    label "satysfakcja"
  ]
  node [
    id 1194
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1195
    label "wype&#322;nienie"
  ]
  node [
    id 1196
    label "pe&#322;no"
  ]
  node [
    id 1197
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1198
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1199
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1200
    label "r&#243;wny"
  ]
  node [
    id 1201
    label "nieuszkodzony"
  ]
  node [
    id 1202
    label "odpowiednio"
  ]
  node [
    id 1203
    label "energia"
  ]
  node [
    id 1204
    label "parametr"
  ]
  node [
    id 1205
    label "rozwi&#261;zanie"
  ]
  node [
    id 1206
    label "wuchta"
  ]
  node [
    id 1207
    label "zaleta"
  ]
  node [
    id 1208
    label "moment_si&#322;y"
  ]
  node [
    id 1209
    label "mn&#243;stwo"
  ]
  node [
    id 1210
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1211
    label "zjawisko"
  ]
  node [
    id 1212
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1213
    label "capacity"
  ]
  node [
    id 1214
    label "magnitude"
  ]
  node [
    id 1215
    label "potencja"
  ]
  node [
    id 1216
    label "przemoc"
  ]
  node [
    id 1217
    label "proces"
  ]
  node [
    id 1218
    label "boski"
  ]
  node [
    id 1219
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1220
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1221
    label "przywidzenie"
  ]
  node [
    id 1222
    label "presence"
  ]
  node [
    id 1223
    label "charakter"
  ]
  node [
    id 1224
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1225
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1226
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1227
    label "emitowa&#263;"
  ]
  node [
    id 1228
    label "egzergia"
  ]
  node [
    id 1229
    label "kwant_energii"
  ]
  node [
    id 1230
    label "szwung"
  ]
  node [
    id 1231
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1232
    label "power"
  ]
  node [
    id 1233
    label "emitowanie"
  ]
  node [
    id 1234
    label "energy"
  ]
  node [
    id 1235
    label "wymiar"
  ]
  node [
    id 1236
    label "zmienna"
  ]
  node [
    id 1237
    label "charakterystyka"
  ]
  node [
    id 1238
    label "wielko&#347;&#263;"
  ]
  node [
    id 1239
    label "patologia"
  ]
  node [
    id 1240
    label "przewaga"
  ]
  node [
    id 1241
    label "drastyczny"
  ]
  node [
    id 1242
    label "po&#322;&#243;g"
  ]
  node [
    id 1243
    label "spe&#322;nienie"
  ]
  node [
    id 1244
    label "dula"
  ]
  node [
    id 1245
    label "wymy&#347;lenie"
  ]
  node [
    id 1246
    label "po&#322;o&#380;na"
  ]
  node [
    id 1247
    label "wyj&#347;cie"
  ]
  node [
    id 1248
    label "proces_fizjologiczny"
  ]
  node [
    id 1249
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1250
    label "pomys&#322;"
  ]
  node [
    id 1251
    label "szok_poporodowy"
  ]
  node [
    id 1252
    label "marc&#243;wka"
  ]
  node [
    id 1253
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 1254
    label "birth"
  ]
  node [
    id 1255
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1256
    label "wynik"
  ]
  node [
    id 1257
    label "enormousness"
  ]
  node [
    id 1258
    label "posiada&#263;"
  ]
  node [
    id 1259
    label "potencja&#322;"
  ]
  node [
    id 1260
    label "zapomnienie"
  ]
  node [
    id 1261
    label "zapomina&#263;"
  ]
  node [
    id 1262
    label "zapominanie"
  ]
  node [
    id 1263
    label "ability"
  ]
  node [
    id 1264
    label "obliczeniowo"
  ]
  node [
    id 1265
    label "zapomnie&#263;"
  ]
  node [
    id 1266
    label "facylitacja"
  ]
  node [
    id 1267
    label "warto&#347;&#263;"
  ]
  node [
    id 1268
    label "zrewaluowa&#263;"
  ]
  node [
    id 1269
    label "rewaluowanie"
  ]
  node [
    id 1270
    label "korzy&#347;&#263;"
  ]
  node [
    id 1271
    label "zrewaluowanie"
  ]
  node [
    id 1272
    label "rewaluowa&#263;"
  ]
  node [
    id 1273
    label "wabik"
  ]
  node [
    id 1274
    label "strona"
  ]
  node [
    id 1275
    label "m&#322;ot"
  ]
  node [
    id 1276
    label "pr&#243;ba"
  ]
  node [
    id 1277
    label "attribute"
  ]
  node [
    id 1278
    label "marka"
  ]
  node [
    id 1279
    label "moc"
  ]
  node [
    id 1280
    label "potency"
  ]
  node [
    id 1281
    label "byt"
  ]
  node [
    id 1282
    label "tomizm"
  ]
  node [
    id 1283
    label "wydolno&#347;&#263;"
  ]
  node [
    id 1284
    label "poj&#281;cie"
  ]
  node [
    id 1285
    label "arystotelizm"
  ]
  node [
    id 1286
    label "gotowo&#347;&#263;"
  ]
  node [
    id 1287
    label "zrejterowanie"
  ]
  node [
    id 1288
    label "zmobilizowa&#263;"
  ]
  node [
    id 1289
    label "przedmiot"
  ]
  node [
    id 1290
    label "dezerter"
  ]
  node [
    id 1291
    label "oddzia&#322;_karny"
  ]
  node [
    id 1292
    label "rezerwa"
  ]
  node [
    id 1293
    label "tabor"
  ]
  node [
    id 1294
    label "wermacht"
  ]
  node [
    id 1295
    label "cofni&#281;cie"
  ]
  node [
    id 1296
    label "struktura"
  ]
  node [
    id 1297
    label "szko&#322;a"
  ]
  node [
    id 1298
    label "korpus"
  ]
  node [
    id 1299
    label "soldateska"
  ]
  node [
    id 1300
    label "ods&#322;ugiwanie"
  ]
  node [
    id 1301
    label "werbowanie_si&#281;"
  ]
  node [
    id 1302
    label "zdemobilizowanie"
  ]
  node [
    id 1303
    label "oddzia&#322;"
  ]
  node [
    id 1304
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 1305
    label "s&#322;u&#380;ba"
  ]
  node [
    id 1306
    label "or&#281;&#380;"
  ]
  node [
    id 1307
    label "Legia_Cudzoziemska"
  ]
  node [
    id 1308
    label "Armia_Czerwona"
  ]
  node [
    id 1309
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 1310
    label "rejterowanie"
  ]
  node [
    id 1311
    label "Czerwona_Gwardia"
  ]
  node [
    id 1312
    label "zrejterowa&#263;"
  ]
  node [
    id 1313
    label "sztabslekarz"
  ]
  node [
    id 1314
    label "zmobilizowanie"
  ]
  node [
    id 1315
    label "wojo"
  ]
  node [
    id 1316
    label "pospolite_ruszenie"
  ]
  node [
    id 1317
    label "Eurokorpus"
  ]
  node [
    id 1318
    label "mobilizowanie"
  ]
  node [
    id 1319
    label "rejterowa&#263;"
  ]
  node [
    id 1320
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 1321
    label "mobilizowa&#263;"
  ]
  node [
    id 1322
    label "Armia_Krajowa"
  ]
  node [
    id 1323
    label "dryl"
  ]
  node [
    id 1324
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 1325
    label "petarda"
  ]
  node [
    id 1326
    label "zdemobilizowa&#263;"
  ]
  node [
    id 1327
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 1328
    label "pryncypa&#322;"
  ]
  node [
    id 1329
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1330
    label "kszta&#322;t"
  ]
  node [
    id 1331
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1332
    label "kierowa&#263;"
  ]
  node [
    id 1333
    label "alkohol"
  ]
  node [
    id 1334
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1335
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1336
    label "sztuka"
  ]
  node [
    id 1337
    label "dekiel"
  ]
  node [
    id 1338
    label "ro&#347;lina"
  ]
  node [
    id 1339
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1340
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1341
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1342
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1343
    label "noosfera"
  ]
  node [
    id 1344
    label "byd&#322;o"
  ]
  node [
    id 1345
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1346
    label "makrocefalia"
  ]
  node [
    id 1347
    label "ucho"
  ]
  node [
    id 1348
    label "g&#243;ra"
  ]
  node [
    id 1349
    label "m&#243;zg"
  ]
  node [
    id 1350
    label "kierownictwo"
  ]
  node [
    id 1351
    label "fryzura"
  ]
  node [
    id 1352
    label "umys&#322;"
  ]
  node [
    id 1353
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1354
    label "czaszka"
  ]
  node [
    id 1355
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1356
    label "podmiot"
  ]
  node [
    id 1357
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 1358
    label "organ"
  ]
  node [
    id 1359
    label "ptaszek"
  ]
  node [
    id 1360
    label "organizacja"
  ]
  node [
    id 1361
    label "element_anatomiczny"
  ]
  node [
    id 1362
    label "przyrodzenie"
  ]
  node [
    id 1363
    label "fiut"
  ]
  node [
    id 1364
    label "shaft"
  ]
  node [
    id 1365
    label "wchodzenie"
  ]
  node [
    id 1366
    label "przedstawiciel"
  ]
  node [
    id 1367
    label "wej&#347;cie"
  ]
  node [
    id 1368
    label "co&#347;"
  ]
  node [
    id 1369
    label "budynek"
  ]
  node [
    id 1370
    label "thing"
  ]
  node [
    id 1371
    label "program"
  ]
  node [
    id 1372
    label "rzecz"
  ]
  node [
    id 1373
    label "przelezienie"
  ]
  node [
    id 1374
    label "&#347;piew"
  ]
  node [
    id 1375
    label "Synaj"
  ]
  node [
    id 1376
    label "Kreml"
  ]
  node [
    id 1377
    label "kierunek"
  ]
  node [
    id 1378
    label "wysoki"
  ]
  node [
    id 1379
    label "element"
  ]
  node [
    id 1380
    label "wzniesienie"
  ]
  node [
    id 1381
    label "pi&#281;tro"
  ]
  node [
    id 1382
    label "Ropa"
  ]
  node [
    id 1383
    label "kupa"
  ]
  node [
    id 1384
    label "przele&#378;&#263;"
  ]
  node [
    id 1385
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 1386
    label "karczek"
  ]
  node [
    id 1387
    label "rami&#261;czko"
  ]
  node [
    id 1388
    label "Jaworze"
  ]
  node [
    id 1389
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1390
    label "przedzia&#322;ek"
  ]
  node [
    id 1391
    label "pasemko"
  ]
  node [
    id 1392
    label "fryz"
  ]
  node [
    id 1393
    label "w&#322;osy"
  ]
  node [
    id 1394
    label "grzywka"
  ]
  node [
    id 1395
    label "egreta"
  ]
  node [
    id 1396
    label "falownica"
  ]
  node [
    id 1397
    label "fonta&#378;"
  ]
  node [
    id 1398
    label "fryzura_intymna"
  ]
  node [
    id 1399
    label "ozdoba"
  ]
  node [
    id 1400
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1401
    label "didaskalia"
  ]
  node [
    id 1402
    label "czyn"
  ]
  node [
    id 1403
    label "environment"
  ]
  node [
    id 1404
    label "head"
  ]
  node [
    id 1405
    label "scenariusz"
  ]
  node [
    id 1406
    label "egzemplarz"
  ]
  node [
    id 1407
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1408
    label "utw&#243;r"
  ]
  node [
    id 1409
    label "kultura_duchowa"
  ]
  node [
    id 1410
    label "fortel"
  ]
  node [
    id 1411
    label "ambala&#380;"
  ]
  node [
    id 1412
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1413
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1414
    label "Faust"
  ]
  node [
    id 1415
    label "turn"
  ]
  node [
    id 1416
    label "Apollo"
  ]
  node [
    id 1417
    label "kultura"
  ]
  node [
    id 1418
    label "towar"
  ]
  node [
    id 1419
    label "raj_utracony"
  ]
  node [
    id 1420
    label "umieranie"
  ]
  node [
    id 1421
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1422
    label "prze&#380;ywanie"
  ]
  node [
    id 1423
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1424
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1425
    label "umarcie"
  ]
  node [
    id 1426
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1427
    label "subsistence"
  ]
  node [
    id 1428
    label "okres_noworodkowy"
  ]
  node [
    id 1429
    label "wiek_matuzalemowy"
  ]
  node [
    id 1430
    label "entity"
  ]
  node [
    id 1431
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1432
    label "do&#380;ywanie"
  ]
  node [
    id 1433
    label "dzieci&#324;stwo"
  ]
  node [
    id 1434
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1435
    label "rozw&#243;j"
  ]
  node [
    id 1436
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1437
    label "czas"
  ]
  node [
    id 1438
    label "menopauza"
  ]
  node [
    id 1439
    label "&#347;mier&#263;"
  ]
  node [
    id 1440
    label "koleje_losu"
  ]
  node [
    id 1441
    label "zegar_biologiczny"
  ]
  node [
    id 1442
    label "przebywanie"
  ]
  node [
    id 1443
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1444
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1445
    label "life"
  ]
  node [
    id 1446
    label "staro&#347;&#263;"
  ]
  node [
    id 1447
    label "mi&#281;sie&#324;"
  ]
  node [
    id 1448
    label "szew_kostny"
  ]
  node [
    id 1449
    label "trzewioczaszka"
  ]
  node [
    id 1450
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 1451
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 1452
    label "m&#243;zgoczaszka"
  ]
  node [
    id 1453
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 1454
    label "dynia"
  ]
  node [
    id 1455
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 1456
    label "rozszczep_czaszki"
  ]
  node [
    id 1457
    label "szew_strza&#322;kowy"
  ]
  node [
    id 1458
    label "puszka_m&#243;zgowa"
  ]
  node [
    id 1459
    label "mak&#243;wka"
  ]
  node [
    id 1460
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 1461
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 1462
    label "zatoka"
  ]
  node [
    id 1463
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 1464
    label "oczod&#243;&#322;"
  ]
  node [
    id 1465
    label "potylica"
  ]
  node [
    id 1466
    label "lemiesz"
  ]
  node [
    id 1467
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 1468
    label "&#380;uchwa"
  ]
  node [
    id 1469
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 1470
    label "diafanoskopia"
  ]
  node [
    id 1471
    label "ciemi&#281;"
  ]
  node [
    id 1472
    label "substancja_szara"
  ]
  node [
    id 1473
    label "encefalografia"
  ]
  node [
    id 1474
    label "przedmurze"
  ]
  node [
    id 1475
    label "bruzda"
  ]
  node [
    id 1476
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 1477
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 1478
    label "most"
  ]
  node [
    id 1479
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 1480
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 1481
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 1482
    label "podwzg&#243;rze"
  ]
  node [
    id 1483
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 1484
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 1485
    label "wzg&#243;rze"
  ]
  node [
    id 1486
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 1487
    label "elektroencefalogram"
  ]
  node [
    id 1488
    label "przodom&#243;zgowie"
  ]
  node [
    id 1489
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 1490
    label "projektodawca"
  ]
  node [
    id 1491
    label "przysadka"
  ]
  node [
    id 1492
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 1493
    label "zw&#243;j"
  ]
  node [
    id 1494
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 1495
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 1496
    label "kora_m&#243;zgowa"
  ]
  node [
    id 1497
    label "kresom&#243;zgowie"
  ]
  node [
    id 1498
    label "poduszka"
  ]
  node [
    id 1499
    label "napinacz"
  ]
  node [
    id 1500
    label "czapka"
  ]
  node [
    id 1501
    label "ucho_zewn&#281;trzne"
  ]
  node [
    id 1502
    label "elektronystagmografia"
  ]
  node [
    id 1503
    label "handle"
  ]
  node [
    id 1504
    label "ochraniacz"
  ]
  node [
    id 1505
    label "ma&#322;&#380;owina"
  ]
  node [
    id 1506
    label "ucho_wewn&#281;trzne"
  ]
  node [
    id 1507
    label "uchwyt"
  ]
  node [
    id 1508
    label "ucho_&#347;rodkowe"
  ]
  node [
    id 1509
    label "przew&#243;d_s&#322;uchowy"
  ]
  node [
    id 1510
    label "otw&#243;r"
  ]
  node [
    id 1511
    label "lid"
  ]
  node [
    id 1512
    label "ko&#322;o"
  ]
  node [
    id 1513
    label "pokrywa"
  ]
  node [
    id 1514
    label "dekielek"
  ]
  node [
    id 1515
    label "os&#322;ona"
  ]
  node [
    id 1516
    label "g&#322;upek"
  ]
  node [
    id 1517
    label "g&#322;os"
  ]
  node [
    id 1518
    label "ekshumowanie"
  ]
  node [
    id 1519
    label "jednostka_organizacyjna"
  ]
  node [
    id 1520
    label "zabalsamowanie"
  ]
  node [
    id 1521
    label "zesp&#243;&#322;"
  ]
  node [
    id 1522
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1523
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1524
    label "mi&#281;so"
  ]
  node [
    id 1525
    label "zabalsamowa&#263;"
  ]
  node [
    id 1526
    label "Izba_Konsyliarska"
  ]
  node [
    id 1527
    label "kremacja"
  ]
  node [
    id 1528
    label "sekcja"
  ]
  node [
    id 1529
    label "materia"
  ]
  node [
    id 1530
    label "pochowanie"
  ]
  node [
    id 1531
    label "tanatoplastyk"
  ]
  node [
    id 1532
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1533
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1534
    label "pochowa&#263;"
  ]
  node [
    id 1535
    label "tanatoplastyka"
  ]
  node [
    id 1536
    label "balsamowa&#263;"
  ]
  node [
    id 1537
    label "nieumar&#322;y"
  ]
  node [
    id 1538
    label "balsamowanie"
  ]
  node [
    id 1539
    label "ekshumowa&#263;"
  ]
  node [
    id 1540
    label "pogrzeb"
  ]
  node [
    id 1541
    label "zbiorowisko"
  ]
  node [
    id 1542
    label "ro&#347;liny"
  ]
  node [
    id 1543
    label "p&#281;d"
  ]
  node [
    id 1544
    label "wegetowanie"
  ]
  node [
    id 1545
    label "zadziorek"
  ]
  node [
    id 1546
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1547
    label "do&#322;owa&#263;"
  ]
  node [
    id 1548
    label "wegetacja"
  ]
  node [
    id 1549
    label "owoc"
  ]
  node [
    id 1550
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1551
    label "strzyc"
  ]
  node [
    id 1552
    label "w&#322;&#243;kno"
  ]
  node [
    id 1553
    label "g&#322;uszenie"
  ]
  node [
    id 1554
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1555
    label "fitotron"
  ]
  node [
    id 1556
    label "bulwka"
  ]
  node [
    id 1557
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1558
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1559
    label "epiderma"
  ]
  node [
    id 1560
    label "gumoza"
  ]
  node [
    id 1561
    label "strzy&#380;enie"
  ]
  node [
    id 1562
    label "wypotnik"
  ]
  node [
    id 1563
    label "flawonoid"
  ]
  node [
    id 1564
    label "wyro&#347;le"
  ]
  node [
    id 1565
    label "do&#322;owanie"
  ]
  node [
    id 1566
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1567
    label "pora&#380;a&#263;"
  ]
  node [
    id 1568
    label "fitocenoza"
  ]
  node [
    id 1569
    label "fotoautotrof"
  ]
  node [
    id 1570
    label "nieuleczalnie_chory"
  ]
  node [
    id 1571
    label "wegetowa&#263;"
  ]
  node [
    id 1572
    label "pochewka"
  ]
  node [
    id 1573
    label "sok"
  ]
  node [
    id 1574
    label "system_korzeniowy"
  ]
  node [
    id 1575
    label "zawi&#261;zek"
  ]
  node [
    id 1576
    label "pami&#281;&#263;"
  ]
  node [
    id 1577
    label "pomieszanie_si&#281;"
  ]
  node [
    id 1578
    label "wn&#281;trze"
  ]
  node [
    id 1579
    label "wyobra&#378;nia"
  ]
  node [
    id 1580
    label "obci&#281;cie"
  ]
  node [
    id 1581
    label "decapitation"
  ]
  node [
    id 1582
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1583
    label "opitolenie"
  ]
  node [
    id 1584
    label "poobcinanie"
  ]
  node [
    id 1585
    label "zmro&#380;enie"
  ]
  node [
    id 1586
    label "snub"
  ]
  node [
    id 1587
    label "kr&#243;j"
  ]
  node [
    id 1588
    label "oblanie"
  ]
  node [
    id 1589
    label "przeegzaminowanie"
  ]
  node [
    id 1590
    label "odbicie"
  ]
  node [
    id 1591
    label "uderzenie"
  ]
  node [
    id 1592
    label "ping-pong"
  ]
  node [
    id 1593
    label "cut"
  ]
  node [
    id 1594
    label "gilotyna"
  ]
  node [
    id 1595
    label "szafot"
  ]
  node [
    id 1596
    label "skr&#243;cenie"
  ]
  node [
    id 1597
    label "kara_&#347;mierci"
  ]
  node [
    id 1598
    label "siatk&#243;wka"
  ]
  node [
    id 1599
    label "k&#322;&#243;tnia"
  ]
  node [
    id 1600
    label "splay"
  ]
  node [
    id 1601
    label "zabicie"
  ]
  node [
    id 1602
    label "tenis"
  ]
  node [
    id 1603
    label "odci&#281;cie"
  ]
  node [
    id 1604
    label "st&#281;&#380;enie"
  ]
  node [
    id 1605
    label "decapitate"
  ]
  node [
    id 1606
    label "usun&#261;&#263;"
  ]
  node [
    id 1607
    label "obci&#261;&#263;"
  ]
  node [
    id 1608
    label "naruszy&#263;"
  ]
  node [
    id 1609
    label "obni&#380;y&#263;"
  ]
  node [
    id 1610
    label "okroi&#263;"
  ]
  node [
    id 1611
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1612
    label "zaci&#261;&#263;"
  ]
  node [
    id 1613
    label "obla&#263;"
  ]
  node [
    id 1614
    label "odbi&#263;"
  ]
  node [
    id 1615
    label "skr&#243;ci&#263;"
  ]
  node [
    id 1616
    label "pozbawi&#263;"
  ]
  node [
    id 1617
    label "opitoli&#263;"
  ]
  node [
    id 1618
    label "zabi&#263;"
  ]
  node [
    id 1619
    label "wywo&#322;a&#263;"
  ]
  node [
    id 1620
    label "unieruchomi&#263;"
  ]
  node [
    id 1621
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 1622
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1623
    label "odci&#261;&#263;"
  ]
  node [
    id 1624
    label "write_out"
  ]
  node [
    id 1625
    label "wada_wrodzona"
  ]
  node [
    id 1626
    label "formacja"
  ]
  node [
    id 1627
    label "punkt_widzenia"
  ]
  node [
    id 1628
    label "wygl&#261;d"
  ]
  node [
    id 1629
    label "spirala"
  ]
  node [
    id 1630
    label "p&#322;at"
  ]
  node [
    id 1631
    label "comeliness"
  ]
  node [
    id 1632
    label "kielich"
  ]
  node [
    id 1633
    label "face"
  ]
  node [
    id 1634
    label "blaszka"
  ]
  node [
    id 1635
    label "p&#281;tla"
  ]
  node [
    id 1636
    label "pasmo"
  ]
  node [
    id 1637
    label "linearno&#347;&#263;"
  ]
  node [
    id 1638
    label "gwiazda"
  ]
  node [
    id 1639
    label "miniatura"
  ]
  node [
    id 1640
    label "kr&#281;torogie"
  ]
  node [
    id 1641
    label "czochrad&#322;o"
  ]
  node [
    id 1642
    label "posp&#243;lstwo"
  ]
  node [
    id 1643
    label "kraal"
  ]
  node [
    id 1644
    label "livestock"
  ]
  node [
    id 1645
    label "u&#380;ywka"
  ]
  node [
    id 1646
    label "najebka"
  ]
  node [
    id 1647
    label "upajanie"
  ]
  node [
    id 1648
    label "szk&#322;o"
  ]
  node [
    id 1649
    label "wypicie"
  ]
  node [
    id 1650
    label "rozgrzewacz"
  ]
  node [
    id 1651
    label "nap&#243;j"
  ]
  node [
    id 1652
    label "alko"
  ]
  node [
    id 1653
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1654
    label "picie"
  ]
  node [
    id 1655
    label "upojenie"
  ]
  node [
    id 1656
    label "upija&#263;"
  ]
  node [
    id 1657
    label "likwor"
  ]
  node [
    id 1658
    label "poniewierca"
  ]
  node [
    id 1659
    label "grupa_hydroksylowa"
  ]
  node [
    id 1660
    label "spirytualia"
  ]
  node [
    id 1661
    label "le&#380;akownia"
  ]
  node [
    id 1662
    label "upi&#263;"
  ]
  node [
    id 1663
    label "piwniczka"
  ]
  node [
    id 1664
    label "gorzelnia_rolnicza"
  ]
  node [
    id 1665
    label "sterowa&#263;"
  ]
  node [
    id 1666
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1667
    label "manipulate"
  ]
  node [
    id 1668
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1669
    label "ustawia&#263;"
  ]
  node [
    id 1670
    label "give"
  ]
  node [
    id 1671
    label "przeznacza&#263;"
  ]
  node [
    id 1672
    label "control"
  ]
  node [
    id 1673
    label "motywowa&#263;"
  ]
  node [
    id 1674
    label "administrowa&#263;"
  ]
  node [
    id 1675
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1676
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1677
    label "order"
  ]
  node [
    id 1678
    label "indicate"
  ]
  node [
    id 1679
    label "biuro"
  ]
  node [
    id 1680
    label "lead"
  ]
  node [
    id 1681
    label "siedziba"
  ]
  node [
    id 1682
    label "praca"
  ]
  node [
    id 1683
    label "w&#322;adza"
  ]
  node [
    id 1684
    label "naczynie"
  ]
  node [
    id 1685
    label "korkownica"
  ]
  node [
    id 1686
    label "zabawa"
  ]
  node [
    id 1687
    label "szyjka"
  ]
  node [
    id 1688
    label "niemowl&#281;"
  ]
  node [
    id 1689
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1690
    label "glass"
  ]
  node [
    id 1691
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 1692
    label "vessel"
  ]
  node [
    id 1693
    label "sprz&#281;t"
  ]
  node [
    id 1694
    label "statki"
  ]
  node [
    id 1695
    label "rewaskularyzacja"
  ]
  node [
    id 1696
    label "ceramika"
  ]
  node [
    id 1697
    label "drewno"
  ]
  node [
    id 1698
    label "przew&#243;d"
  ]
  node [
    id 1699
    label "unaczyni&#263;"
  ]
  node [
    id 1700
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 1701
    label "receptacle"
  ]
  node [
    id 1702
    label "temat"
  ]
  node [
    id 1703
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1704
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1705
    label "rozrywka"
  ]
  node [
    id 1706
    label "impreza"
  ]
  node [
    id 1707
    label "igraszka"
  ]
  node [
    id 1708
    label "taniec"
  ]
  node [
    id 1709
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 1710
    label "gambling"
  ]
  node [
    id 1711
    label "chwyt"
  ]
  node [
    id 1712
    label "game"
  ]
  node [
    id 1713
    label "igra"
  ]
  node [
    id 1714
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 1715
    label "nabawienie_si&#281;"
  ]
  node [
    id 1716
    label "ubaw"
  ]
  node [
    id 1717
    label "wodzirej"
  ]
  node [
    id 1718
    label "dr&#243;b"
  ]
  node [
    id 1719
    label "pr&#243;g"
  ]
  node [
    id 1720
    label "tuszka"
  ]
  node [
    id 1721
    label "dr&#243;bka"
  ]
  node [
    id 1722
    label "instrument_strunowy"
  ]
  node [
    id 1723
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 1724
    label "&#347;piochy"
  ]
  node [
    id 1725
    label "wyprawka"
  ]
  node [
    id 1726
    label "gaworzy&#263;"
  ]
  node [
    id 1727
    label "koszulka"
  ]
  node [
    id 1728
    label "dzidziu&#347;"
  ]
  node [
    id 1729
    label "gaworzenie"
  ]
  node [
    id 1730
    label "niunia"
  ]
  node [
    id 1731
    label "smoczek"
  ]
  node [
    id 1732
    label "uwarzenie"
  ]
  node [
    id 1733
    label "warzenie"
  ]
  node [
    id 1734
    label "bacik"
  ]
  node [
    id 1735
    label "uwarzy&#263;"
  ]
  node [
    id 1736
    label "birofilia"
  ]
  node [
    id 1737
    label "warzy&#263;"
  ]
  node [
    id 1738
    label "nawarzy&#263;"
  ]
  node [
    id 1739
    label "browarnia"
  ]
  node [
    id 1740
    label "nawarzenie"
  ]
  node [
    id 1741
    label "anta&#322;"
  ]
  node [
    id 1742
    label "porcja"
  ]
  node [
    id 1743
    label "ciecz"
  ]
  node [
    id 1744
    label "substancja"
  ]
  node [
    id 1745
    label "wypitek"
  ]
  node [
    id 1746
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 1747
    label "okazanie_si&#281;"
  ]
  node [
    id 1748
    label "ograniczenie"
  ]
  node [
    id 1749
    label "ruszenie"
  ]
  node [
    id 1750
    label "podzianie_si&#281;"
  ]
  node [
    id 1751
    label "powychodzenie"
  ]
  node [
    id 1752
    label "opuszczenie"
  ]
  node [
    id 1753
    label "postrze&#380;enie"
  ]
  node [
    id 1754
    label "transgression"
  ]
  node [
    id 1755
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 1756
    label "wychodzenie"
  ]
  node [
    id 1757
    label "uko&#324;czenie"
  ]
  node [
    id 1758
    label "powiedzenie_si&#281;"
  ]
  node [
    id 1759
    label "policzenie"
  ]
  node [
    id 1760
    label "podziewanie_si&#281;"
  ]
  node [
    id 1761
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1762
    label "exit"
  ]
  node [
    id 1763
    label "vent"
  ]
  node [
    id 1764
    label "uwolnienie_si&#281;"
  ]
  node [
    id 1765
    label "deviation"
  ]
  node [
    id 1766
    label "release"
  ]
  node [
    id 1767
    label "wych&#243;d"
  ]
  node [
    id 1768
    label "wypadni&#281;cie"
  ]
  node [
    id 1769
    label "odch&#243;d"
  ]
  node [
    id 1770
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 1771
    label "zagranie"
  ]
  node [
    id 1772
    label "zako&#324;czenie"
  ]
  node [
    id 1773
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 1774
    label "emergence"
  ]
  node [
    id 1775
    label "wykonanie"
  ]
  node [
    id 1776
    label "nagotowanie"
  ]
  node [
    id 1777
    label "wino"
  ]
  node [
    id 1778
    label "beczka"
  ]
  node [
    id 1779
    label "wygotowywanie"
  ]
  node [
    id 1780
    label "wy&#322;&#261;czanie"
  ]
  node [
    id 1781
    label "po_kucharsku"
  ]
  node [
    id 1782
    label "produkowanie"
  ]
  node [
    id 1783
    label "nagotowanie_si&#281;"
  ]
  node [
    id 1784
    label "boiling"
  ]
  node [
    id 1785
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 1786
    label "rozgotowywanie"
  ]
  node [
    id 1787
    label "przyrz&#261;dzanie"
  ]
  node [
    id 1788
    label "rozgotowanie"
  ]
  node [
    id 1789
    label "gotowanie"
  ]
  node [
    id 1790
    label "dekokcja"
  ]
  node [
    id 1791
    label "nagotowa&#263;"
  ]
  node [
    id 1792
    label "wyprodukowa&#263;"
  ]
  node [
    id 1793
    label "brew"
  ]
  node [
    id 1794
    label "roast"
  ]
  node [
    id 1795
    label "antena"
  ]
  node [
    id 1796
    label "skr&#281;t"
  ]
  node [
    id 1797
    label "&#322;&#243;dka"
  ]
  node [
    id 1798
    label "narkotyk_mi&#281;kki"
  ]
  node [
    id 1799
    label "nalewak"
  ]
  node [
    id 1800
    label "gibon"
  ]
  node [
    id 1801
    label "klucz"
  ]
  node [
    id 1802
    label "zami&#322;owanie"
  ]
  node [
    id 1803
    label "wytw&#243;rnia"
  ]
  node [
    id 1804
    label "s&#322;odownia"
  ]
  node [
    id 1805
    label "kuchnia"
  ]
  node [
    id 1806
    label "fudge"
  ]
  node [
    id 1807
    label "train"
  ]
  node [
    id 1808
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 1809
    label "produkowa&#263;"
  ]
  node [
    id 1810
    label "kucharz"
  ]
  node [
    id 1811
    label "XD"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 59
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 339
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 450
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 18
    target 62
  ]
  edge [
    source 18
    target 63
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 65
  ]
  edge [
    source 18
    target 66
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 68
  ]
  edge [
    source 18
    target 69
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 71
  ]
  edge [
    source 18
    target 72
  ]
  edge [
    source 18
    target 73
  ]
  edge [
    source 18
    target 74
  ]
  edge [
    source 18
    target 75
  ]
  edge [
    source 18
    target 76
  ]
  edge [
    source 18
    target 77
  ]
  edge [
    source 18
    target 78
  ]
  edge [
    source 18
    target 79
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 18
    target 81
  ]
  edge [
    source 18
    target 82
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 18
    target 84
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 18
    target 1039
  ]
  edge [
    source 18
    target 1040
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 694
  ]
  edge [
    source 19
    target 695
  ]
  edge [
    source 19
    target 696
  ]
  edge [
    source 19
    target 697
  ]
  edge [
    source 19
    target 698
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 699
  ]
  edge [
    source 19
    target 700
  ]
  edge [
    source 19
    target 701
  ]
  edge [
    source 19
    target 702
  ]
  edge [
    source 19
    target 703
  ]
  edge [
    source 19
    target 59
  ]
  edge [
    source 19
    target 704
  ]
  edge [
    source 19
    target 705
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 708
  ]
  edge [
    source 19
    target 709
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 1058
  ]
  edge [
    source 20
    target 1059
  ]
  edge [
    source 20
    target 1060
  ]
  edge [
    source 20
    target 1061
  ]
  edge [
    source 20
    target 1062
  ]
  edge [
    source 20
    target 1063
  ]
  edge [
    source 20
    target 1064
  ]
  edge [
    source 20
    target 1065
  ]
  edge [
    source 20
    target 1066
  ]
  edge [
    source 20
    target 1067
  ]
  edge [
    source 20
    target 1068
  ]
  edge [
    source 20
    target 1069
  ]
  edge [
    source 20
    target 320
  ]
  edge [
    source 20
    target 1070
  ]
  edge [
    source 20
    target 1071
  ]
  edge [
    source 20
    target 1072
  ]
  edge [
    source 20
    target 325
  ]
  edge [
    source 20
    target 1073
  ]
  edge [
    source 20
    target 1074
  ]
  edge [
    source 20
    target 1075
  ]
  edge [
    source 20
    target 1076
  ]
  edge [
    source 20
    target 1077
  ]
  edge [
    source 20
    target 1078
  ]
  edge [
    source 20
    target 1079
  ]
  edge [
    source 20
    target 1080
  ]
  edge [
    source 20
    target 1081
  ]
  edge [
    source 20
    target 1082
  ]
  edge [
    source 20
    target 1083
  ]
  edge [
    source 20
    target 1084
  ]
  edge [
    source 20
    target 1085
  ]
  edge [
    source 20
    target 1086
  ]
  edge [
    source 20
    target 1087
  ]
  edge [
    source 20
    target 1088
  ]
  edge [
    source 20
    target 1089
  ]
  edge [
    source 20
    target 1090
  ]
  edge [
    source 20
    target 1091
  ]
  edge [
    source 20
    target 1092
  ]
  edge [
    source 20
    target 1093
  ]
  edge [
    source 20
    target 1094
  ]
  edge [
    source 20
    target 1095
  ]
  edge [
    source 20
    target 1096
  ]
  edge [
    source 20
    target 1097
  ]
  edge [
    source 20
    target 1098
  ]
  edge [
    source 20
    target 1099
  ]
  edge [
    source 20
    target 1100
  ]
  edge [
    source 20
    target 1101
  ]
  edge [
    source 20
    target 1102
  ]
  edge [
    source 20
    target 1103
  ]
  edge [
    source 20
    target 1104
  ]
  edge [
    source 20
    target 318
  ]
  edge [
    source 20
    target 329
  ]
  edge [
    source 20
    target 1105
  ]
  edge [
    source 20
    target 1106
  ]
  edge [
    source 20
    target 1107
  ]
  edge [
    source 20
    target 1108
  ]
  edge [
    source 20
    target 1109
  ]
  edge [
    source 20
    target 1110
  ]
  edge [
    source 20
    target 666
  ]
  edge [
    source 20
    target 1111
  ]
  edge [
    source 20
    target 1112
  ]
  edge [
    source 20
    target 1113
  ]
  edge [
    source 20
    target 1114
  ]
  edge [
    source 20
    target 1115
  ]
  edge [
    source 20
    target 1116
  ]
  edge [
    source 20
    target 1117
  ]
  edge [
    source 20
    target 976
  ]
  edge [
    source 20
    target 1118
  ]
  edge [
    source 20
    target 1119
  ]
  edge [
    source 20
    target 1120
  ]
  edge [
    source 20
    target 1121
  ]
  edge [
    source 20
    target 1122
  ]
  edge [
    source 20
    target 1123
  ]
  edge [
    source 20
    target 1124
  ]
  edge [
    source 20
    target 1125
  ]
  edge [
    source 20
    target 1126
  ]
  edge [
    source 20
    target 1127
  ]
  edge [
    source 20
    target 1128
  ]
  edge [
    source 20
    target 1129
  ]
  edge [
    source 20
    target 1130
  ]
  edge [
    source 20
    target 1131
  ]
  edge [
    source 20
    target 1132
  ]
  edge [
    source 20
    target 1133
  ]
  edge [
    source 20
    target 1134
  ]
  edge [
    source 20
    target 1135
  ]
  edge [
    source 20
    target 1136
  ]
  edge [
    source 20
    target 1137
  ]
  edge [
    source 20
    target 1138
  ]
  edge [
    source 20
    target 1139
  ]
  edge [
    source 20
    target 306
  ]
  edge [
    source 20
    target 1140
  ]
  edge [
    source 20
    target 1141
  ]
  edge [
    source 20
    target 1142
  ]
  edge [
    source 20
    target 1143
  ]
  edge [
    source 20
    target 1144
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1145
  ]
  edge [
    source 21
    target 1146
  ]
  edge [
    source 21
    target 1147
  ]
  edge [
    source 21
    target 1148
  ]
  edge [
    source 21
    target 1149
  ]
  edge [
    source 21
    target 1150
  ]
  edge [
    source 21
    target 1151
  ]
  edge [
    source 21
    target 1152
  ]
  edge [
    source 21
    target 1153
  ]
  edge [
    source 21
    target 407
  ]
  edge [
    source 21
    target 1154
  ]
  edge [
    source 21
    target 1155
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 21
    target 62
  ]
  edge [
    source 21
    target 1159
  ]
  edge [
    source 21
    target 1160
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 58
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 1168
  ]
  edge [
    source 21
    target 1169
  ]
  edge [
    source 21
    target 1170
  ]
  edge [
    source 21
    target 1171
  ]
  edge [
    source 21
    target 1172
  ]
  edge [
    source 21
    target 1173
  ]
  edge [
    source 21
    target 1174
  ]
  edge [
    source 21
    target 1175
  ]
  edge [
    source 21
    target 1176
  ]
  edge [
    source 21
    target 1177
  ]
  edge [
    source 21
    target 1178
  ]
  edge [
    source 21
    target 1179
  ]
  edge [
    source 21
    target 1180
  ]
  edge [
    source 21
    target 1181
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 1182
  ]
  edge [
    source 21
    target 1183
  ]
  edge [
    source 21
    target 1184
  ]
  edge [
    source 21
    target 1185
  ]
  edge [
    source 21
    target 1186
  ]
  edge [
    source 21
    target 1187
  ]
  edge [
    source 21
    target 467
  ]
  edge [
    source 21
    target 1188
  ]
  edge [
    source 21
    target 1189
  ]
  edge [
    source 21
    target 1190
  ]
  edge [
    source 21
    target 1191
  ]
  edge [
    source 21
    target 1192
  ]
  edge [
    source 21
    target 1193
  ]
  edge [
    source 21
    target 1194
  ]
  edge [
    source 21
    target 578
  ]
  edge [
    source 21
    target 1195
  ]
  edge [
    source 21
    target 241
  ]
  edge [
    source 21
    target 1196
  ]
  edge [
    source 21
    target 1197
  ]
  edge [
    source 21
    target 1198
  ]
  edge [
    source 21
    target 1199
  ]
  edge [
    source 21
    target 1200
  ]
  edge [
    source 21
    target 1201
  ]
  edge [
    source 21
    target 1202
  ]
  edge [
    source 22
    target 1203
  ]
  edge [
    source 22
    target 1204
  ]
  edge [
    source 22
    target 1205
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 500
  ]
  edge [
    source 22
    target 1206
  ]
  edge [
    source 22
    target 1207
  ]
  edge [
    source 22
    target 718
  ]
  edge [
    source 22
    target 1208
  ]
  edge [
    source 22
    target 1209
  ]
  edge [
    source 22
    target 1210
  ]
  edge [
    source 22
    target 1211
  ]
  edge [
    source 22
    target 1212
  ]
  edge [
    source 22
    target 1213
  ]
  edge [
    source 22
    target 1214
  ]
  edge [
    source 22
    target 1215
  ]
  edge [
    source 22
    target 1216
  ]
  edge [
    source 22
    target 1217
  ]
  edge [
    source 22
    target 1218
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 1219
  ]
  edge [
    source 22
    target 1220
  ]
  edge [
    source 22
    target 1221
  ]
  edge [
    source 22
    target 1222
  ]
  edge [
    source 22
    target 1223
  ]
  edge [
    source 22
    target 1224
  ]
  edge [
    source 22
    target 1225
  ]
  edge [
    source 22
    target 1226
  ]
  edge [
    source 22
    target 1227
  ]
  edge [
    source 22
    target 1228
  ]
  edge [
    source 22
    target 1229
  ]
  edge [
    source 22
    target 1230
  ]
  edge [
    source 22
    target 1231
  ]
  edge [
    source 22
    target 1232
  ]
  edge [
    source 22
    target 1233
  ]
  edge [
    source 22
    target 1234
  ]
  edge [
    source 22
    target 1235
  ]
  edge [
    source 22
    target 1236
  ]
  edge [
    source 22
    target 1237
  ]
  edge [
    source 22
    target 1238
  ]
  edge [
    source 22
    target 1239
  ]
  edge [
    source 22
    target 772
  ]
  edge [
    source 22
    target 1240
  ]
  edge [
    source 22
    target 1241
  ]
  edge [
    source 22
    target 1242
  ]
  edge [
    source 22
    target 1243
  ]
  edge [
    source 22
    target 1244
  ]
  edge [
    source 22
    target 951
  ]
  edge [
    source 22
    target 459
  ]
  edge [
    source 22
    target 1245
  ]
  edge [
    source 22
    target 1246
  ]
  edge [
    source 22
    target 1247
  ]
  edge [
    source 22
    target 462
  ]
  edge [
    source 22
    target 1248
  ]
  edge [
    source 22
    target 1249
  ]
  edge [
    source 22
    target 1250
  ]
  edge [
    source 22
    target 1251
  ]
  edge [
    source 22
    target 596
  ]
  edge [
    source 22
    target 1252
  ]
  edge [
    source 22
    target 1253
  ]
  edge [
    source 22
    target 1254
  ]
  edge [
    source 22
    target 1255
  ]
  edge [
    source 22
    target 1256
  ]
  edge [
    source 22
    target 675
  ]
  edge [
    source 22
    target 560
  ]
  edge [
    source 22
    target 1257
  ]
  edge [
    source 22
    target 1258
  ]
  edge [
    source 22
    target 1259
  ]
  edge [
    source 22
    target 1260
  ]
  edge [
    source 22
    target 1261
  ]
  edge [
    source 22
    target 1262
  ]
  edge [
    source 22
    target 1263
  ]
  edge [
    source 22
    target 1264
  ]
  edge [
    source 22
    target 1265
  ]
  edge [
    source 22
    target 1266
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 1267
  ]
  edge [
    source 22
    target 1268
  ]
  edge [
    source 22
    target 1269
  ]
  edge [
    source 22
    target 1270
  ]
  edge [
    source 22
    target 1271
  ]
  edge [
    source 22
    target 1272
  ]
  edge [
    source 22
    target 1273
  ]
  edge [
    source 22
    target 1274
  ]
  edge [
    source 22
    target 1275
  ]
  edge [
    source 22
    target 251
  ]
  edge [
    source 22
    target 1035
  ]
  edge [
    source 22
    target 1276
  ]
  edge [
    source 22
    target 1277
  ]
  edge [
    source 22
    target 1278
  ]
  edge [
    source 22
    target 1279
  ]
  edge [
    source 22
    target 1280
  ]
  edge [
    source 22
    target 1281
  ]
  edge [
    source 22
    target 1282
  ]
  edge [
    source 22
    target 1283
  ]
  edge [
    source 22
    target 1284
  ]
  edge [
    source 22
    target 1285
  ]
  edge [
    source 22
    target 1286
  ]
  edge [
    source 22
    target 1287
  ]
  edge [
    source 22
    target 1288
  ]
  edge [
    source 22
    target 1289
  ]
  edge [
    source 22
    target 1290
  ]
  edge [
    source 22
    target 1291
  ]
  edge [
    source 22
    target 1292
  ]
  edge [
    source 22
    target 1293
  ]
  edge [
    source 22
    target 1294
  ]
  edge [
    source 22
    target 1295
  ]
  edge [
    source 22
    target 248
  ]
  edge [
    source 22
    target 1296
  ]
  edge [
    source 22
    target 1297
  ]
  edge [
    source 22
    target 1298
  ]
  edge [
    source 22
    target 1299
  ]
  edge [
    source 22
    target 1300
  ]
  edge [
    source 22
    target 1301
  ]
  edge [
    source 22
    target 1302
  ]
  edge [
    source 22
    target 1303
  ]
  edge [
    source 22
    target 1304
  ]
  edge [
    source 22
    target 1305
  ]
  edge [
    source 22
    target 1306
  ]
  edge [
    source 22
    target 1307
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 1310
  ]
  edge [
    source 22
    target 1311
  ]
  edge [
    source 22
    target 1312
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 22
    target 1317
  ]
  edge [
    source 22
    target 1318
  ]
  edge [
    source 22
    target 1319
  ]
  edge [
    source 22
    target 1320
  ]
  edge [
    source 22
    target 1321
  ]
  edge [
    source 22
    target 1322
  ]
  edge [
    source 22
    target 686
  ]
  edge [
    source 22
    target 1323
  ]
  edge [
    source 22
    target 1324
  ]
  edge [
    source 22
    target 1325
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 1326
  ]
  edge [
    source 22
    target 1327
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1328
  ]
  edge [
    source 23
    target 1329
  ]
  edge [
    source 23
    target 1330
  ]
  edge [
    source 23
    target 1331
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 1332
  ]
  edge [
    source 23
    target 1333
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 500
  ]
  edge [
    source 23
    target 1183
  ]
  edge [
    source 23
    target 1334
  ]
  edge [
    source 23
    target 1335
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 1336
  ]
  edge [
    source 23
    target 1337
  ]
  edge [
    source 23
    target 1338
  ]
  edge [
    source 23
    target 1339
  ]
  edge [
    source 23
    target 1340
  ]
  edge [
    source 23
    target 1341
  ]
  edge [
    source 23
    target 1342
  ]
  edge [
    source 23
    target 1343
  ]
  edge [
    source 23
    target 1344
  ]
  edge [
    source 23
    target 1345
  ]
  edge [
    source 23
    target 1346
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 1347
  ]
  edge [
    source 23
    target 1348
  ]
  edge [
    source 23
    target 1349
  ]
  edge [
    source 23
    target 1350
  ]
  edge [
    source 23
    target 1351
  ]
  edge [
    source 23
    target 1352
  ]
  edge [
    source 23
    target 759
  ]
  edge [
    source 23
    target 760
  ]
  edge [
    source 23
    target 1353
  ]
  edge [
    source 23
    target 1354
  ]
  edge [
    source 23
    target 1355
  ]
  edge [
    source 23
    target 1356
  ]
  edge [
    source 23
    target 1357
  ]
  edge [
    source 23
    target 1358
  ]
  edge [
    source 23
    target 1359
  ]
  edge [
    source 23
    target 1360
  ]
  edge [
    source 23
    target 1361
  ]
  edge [
    source 23
    target 1362
  ]
  edge [
    source 23
    target 1363
  ]
  edge [
    source 23
    target 1364
  ]
  edge [
    source 23
    target 1365
  ]
  edge [
    source 23
    target 717
  ]
  edge [
    source 23
    target 1366
  ]
  edge [
    source 23
    target 1367
  ]
  edge [
    source 23
    target 61
  ]
  edge [
    source 23
    target 62
  ]
  edge [
    source 23
    target 63
  ]
  edge [
    source 23
    target 64
  ]
  edge [
    source 23
    target 65
  ]
  edge [
    source 23
    target 66
  ]
  edge [
    source 23
    target 67
  ]
  edge [
    source 23
    target 68
  ]
  edge [
    source 23
    target 69
  ]
  edge [
    source 23
    target 70
  ]
  edge [
    source 23
    target 71
  ]
  edge [
    source 23
    target 72
  ]
  edge [
    source 23
    target 73
  ]
  edge [
    source 23
    target 74
  ]
  edge [
    source 23
    target 75
  ]
  edge [
    source 23
    target 76
  ]
  edge [
    source 23
    target 77
  ]
  edge [
    source 23
    target 78
  ]
  edge [
    source 23
    target 79
  ]
  edge [
    source 23
    target 80
  ]
  edge [
    source 23
    target 81
  ]
  edge [
    source 23
    target 82
  ]
  edge [
    source 23
    target 83
  ]
  edge [
    source 23
    target 84
  ]
  edge [
    source 23
    target 1368
  ]
  edge [
    source 23
    target 1369
  ]
  edge [
    source 23
    target 1370
  ]
  edge [
    source 23
    target 1284
  ]
  edge [
    source 23
    target 1371
  ]
  edge [
    source 23
    target 1372
  ]
  edge [
    source 23
    target 1274
  ]
  edge [
    source 23
    target 1289
  ]
  edge [
    source 23
    target 1373
  ]
  edge [
    source 23
    target 1374
  ]
  edge [
    source 23
    target 1375
  ]
  edge [
    source 23
    target 1376
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 1377
  ]
  edge [
    source 23
    target 1378
  ]
  edge [
    source 23
    target 1379
  ]
  edge [
    source 23
    target 1380
  ]
  edge [
    source 23
    target 1381
  ]
  edge [
    source 23
    target 1382
  ]
  edge [
    source 23
    target 1383
  ]
  edge [
    source 23
    target 1384
  ]
  edge [
    source 23
    target 1385
  ]
  edge [
    source 23
    target 1386
  ]
  edge [
    source 23
    target 1387
  ]
  edge [
    source 23
    target 1388
  ]
  edge [
    source 23
    target 1389
  ]
  edge [
    source 23
    target 1390
  ]
  edge [
    source 23
    target 951
  ]
  edge [
    source 23
    target 1391
  ]
  edge [
    source 23
    target 1392
  ]
  edge [
    source 23
    target 1393
  ]
  edge [
    source 23
    target 1394
  ]
  edge [
    source 23
    target 1395
  ]
  edge [
    source 23
    target 1396
  ]
  edge [
    source 23
    target 1397
  ]
  edge [
    source 23
    target 1398
  ]
  edge [
    source 23
    target 1399
  ]
  edge [
    source 23
    target 933
  ]
  edge [
    source 23
    target 959
  ]
  edge [
    source 23
    target 1400
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 937
  ]
  edge [
    source 23
    target 1401
  ]
  edge [
    source 23
    target 1402
  ]
  edge [
    source 23
    target 938
  ]
  edge [
    source 23
    target 1403
  ]
  edge [
    source 23
    target 1404
  ]
  edge [
    source 23
    target 1405
  ]
  edge [
    source 23
    target 1406
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 1407
  ]
  edge [
    source 23
    target 1408
  ]
  edge [
    source 23
    target 1409
  ]
  edge [
    source 23
    target 1410
  ]
  edge [
    source 23
    target 942
  ]
  edge [
    source 23
    target 1411
  ]
  edge [
    source 23
    target 1412
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1413
  ]
  edge [
    source 23
    target 1414
  ]
  edge [
    source 23
    target 945
  ]
  edge [
    source 23
    target 952
  ]
  edge [
    source 23
    target 1415
  ]
  edge [
    source 23
    target 949
  ]
  edge [
    source 23
    target 560
  ]
  edge [
    source 23
    target 923
  ]
  edge [
    source 23
    target 956
  ]
  edge [
    source 23
    target 1416
  ]
  edge [
    source 23
    target 1417
  ]
  edge [
    source 23
    target 957
  ]
  edge [
    source 23
    target 958
  ]
  edge [
    source 23
    target 1418
  ]
  edge [
    source 23
    target 1258
  ]
  edge [
    source 23
    target 1259
  ]
  edge [
    source 23
    target 1261
  ]
  edge [
    source 23
    target 1260
  ]
  edge [
    source 23
    target 1262
  ]
  edge [
    source 23
    target 1263
  ]
  edge [
    source 23
    target 1264
  ]
  edge [
    source 23
    target 1265
  ]
  edge [
    source 23
    target 1419
  ]
  edge [
    source 23
    target 1420
  ]
  edge [
    source 23
    target 1421
  ]
  edge [
    source 23
    target 1422
  ]
  edge [
    source 23
    target 1423
  ]
  edge [
    source 23
    target 1424
  ]
  edge [
    source 23
    target 1242
  ]
  edge [
    source 23
    target 1425
  ]
  edge [
    source 23
    target 1426
  ]
  edge [
    source 23
    target 1427
  ]
  edge [
    source 23
    target 1232
  ]
  edge [
    source 23
    target 1428
  ]
  edge [
    source 23
    target 960
  ]
  edge [
    source 23
    target 1429
  ]
  edge [
    source 23
    target 1255
  ]
  edge [
    source 23
    target 1430
  ]
  edge [
    source 23
    target 1225
  ]
  edge [
    source 23
    target 1431
  ]
  edge [
    source 23
    target 1432
  ]
  edge [
    source 23
    target 1281
  ]
  edge [
    source 23
    target 903
  ]
  edge [
    source 23
    target 1433
  ]
  edge [
    source 23
    target 1434
  ]
  edge [
    source 23
    target 1435
  ]
  edge [
    source 23
    target 1436
  ]
  edge [
    source 23
    target 1437
  ]
  edge [
    source 23
    target 1438
  ]
  edge [
    source 23
    target 1439
  ]
  edge [
    source 23
    target 1440
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 1441
  ]
  edge [
    source 23
    target 1230
  ]
  edge [
    source 23
    target 1442
  ]
  edge [
    source 23
    target 682
  ]
  edge [
    source 23
    target 1443
  ]
  edge [
    source 23
    target 1444
  ]
  edge [
    source 23
    target 1150
  ]
  edge [
    source 23
    target 1445
  ]
  edge [
    source 23
    target 1446
  ]
  edge [
    source 23
    target 1234
  ]
  edge [
    source 23
    target 1447
  ]
  edge [
    source 23
    target 1237
  ]
  edge [
    source 23
    target 1275
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1276
  ]
  edge [
    source 23
    target 1277
  ]
  edge [
    source 23
    target 1278
  ]
  edge [
    source 23
    target 1448
  ]
  edge [
    source 23
    target 1449
  ]
  edge [
    source 23
    target 1450
  ]
  edge [
    source 23
    target 1451
  ]
  edge [
    source 23
    target 1452
  ]
  edge [
    source 23
    target 1453
  ]
  edge [
    source 23
    target 1454
  ]
  edge [
    source 23
    target 1455
  ]
  edge [
    source 23
    target 1456
  ]
  edge [
    source 23
    target 1457
  ]
  edge [
    source 23
    target 1458
  ]
  edge [
    source 23
    target 1459
  ]
  edge [
    source 23
    target 1460
  ]
  edge [
    source 23
    target 1461
  ]
  edge [
    source 23
    target 748
  ]
  edge [
    source 23
    target 1462
  ]
  edge [
    source 23
    target 1463
  ]
  edge [
    source 23
    target 1464
  ]
  edge [
    source 23
    target 1465
  ]
  edge [
    source 23
    target 1466
  ]
  edge [
    source 23
    target 1467
  ]
  edge [
    source 23
    target 1468
  ]
  edge [
    source 23
    target 1469
  ]
  edge [
    source 23
    target 1470
  ]
  edge [
    source 23
    target 806
  ]
  edge [
    source 23
    target 1471
  ]
  edge [
    source 23
    target 1472
  ]
  edge [
    source 23
    target 1473
  ]
  edge [
    source 23
    target 1474
  ]
  edge [
    source 23
    target 1475
  ]
  edge [
    source 23
    target 1476
  ]
  edge [
    source 23
    target 1477
  ]
  edge [
    source 23
    target 1478
  ]
  edge [
    source 23
    target 1479
  ]
  edge [
    source 23
    target 1480
  ]
  edge [
    source 23
    target 1481
  ]
  edge [
    source 23
    target 1482
  ]
  edge [
    source 23
    target 1483
  ]
  edge [
    source 23
    target 1484
  ]
  edge [
    source 23
    target 1485
  ]
  edge [
    source 23
    target 1486
  ]
  edge [
    source 23
    target 1487
  ]
  edge [
    source 23
    target 1488
  ]
  edge [
    source 23
    target 1489
  ]
  edge [
    source 23
    target 1490
  ]
  edge [
    source 23
    target 1491
  ]
  edge [
    source 23
    target 1492
  ]
  edge [
    source 23
    target 1493
  ]
  edge [
    source 23
    target 1494
  ]
  edge [
    source 23
    target 1495
  ]
  edge [
    source 23
    target 1496
  ]
  edge [
    source 23
    target 1497
  ]
  edge [
    source 23
    target 1498
  ]
  edge [
    source 23
    target 1499
  ]
  edge [
    source 23
    target 1500
  ]
  edge [
    source 23
    target 1501
  ]
  edge [
    source 23
    target 1502
  ]
  edge [
    source 23
    target 1503
  ]
  edge [
    source 23
    target 1504
  ]
  edge [
    source 23
    target 1505
  ]
  edge [
    source 23
    target 875
  ]
  edge [
    source 23
    target 1506
  ]
  edge [
    source 23
    target 1507
  ]
  edge [
    source 23
    target 1508
  ]
  edge [
    source 23
    target 1509
  ]
  edge [
    source 23
    target 1510
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 1511
  ]
  edge [
    source 23
    target 1512
  ]
  edge [
    source 23
    target 1513
  ]
  edge [
    source 23
    target 1514
  ]
  edge [
    source 23
    target 1515
  ]
  edge [
    source 23
    target 1516
  ]
  edge [
    source 23
    target 1517
  ]
  edge [
    source 23
    target 893
  ]
  edge [
    source 23
    target 1518
  ]
  edge [
    source 23
    target 755
  ]
  edge [
    source 23
    target 1519
  ]
  edge [
    source 23
    target 723
  ]
  edge [
    source 23
    target 724
  ]
  edge [
    source 23
    target 1520
  ]
  edge [
    source 23
    target 1521
  ]
  edge [
    source 23
    target 1522
  ]
  edge [
    source 23
    target 727
  ]
  edge [
    source 23
    target 726
  ]
  edge [
    source 23
    target 1523
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 1524
  ]
  edge [
    source 23
    target 1525
  ]
  edge [
    source 23
    target 1526
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 1527
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 737
  ]
  edge [
    source 23
    target 1528
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 23
    target 740
  ]
  edge [
    source 23
    target 741
  ]
  edge [
    source 23
    target 743
  ]
  edge [
    source 23
    target 744
  ]
  edge [
    source 23
    target 1529
  ]
  edge [
    source 23
    target 1530
  ]
  edge [
    source 23
    target 745
  ]
  edge [
    source 23
    target 747
  ]
  edge [
    source 23
    target 1531
  ]
  edge [
    source 23
    target 751
  ]
  edge [
    source 23
    target 1532
  ]
  edge [
    source 23
    target 752
  ]
  edge [
    source 23
    target 1533
  ]
  edge [
    source 23
    target 753
  ]
  edge [
    source 23
    target 1534
  ]
  edge [
    source 23
    target 1535
  ]
  edge [
    source 23
    target 1536
  ]
  edge [
    source 23
    target 1537
  ]
  edge [
    source 23
    target 757
  ]
  edge [
    source 23
    target 1538
  ]
  edge [
    source 23
    target 1539
  ]
  edge [
    source 23
    target 758
  ]
  edge [
    source 23
    target 756
  ]
  edge [
    source 23
    target 1540
  ]
  edge [
    source 23
    target 1541
  ]
  edge [
    source 23
    target 1542
  ]
  edge [
    source 23
    target 1543
  ]
  edge [
    source 23
    target 1544
  ]
  edge [
    source 23
    target 1545
  ]
  edge [
    source 23
    target 765
  ]
  edge [
    source 23
    target 1546
  ]
  edge [
    source 23
    target 1547
  ]
  edge [
    source 23
    target 1548
  ]
  edge [
    source 23
    target 1549
  ]
  edge [
    source 23
    target 1550
  ]
  edge [
    source 23
    target 1551
  ]
  edge [
    source 23
    target 1552
  ]
  edge [
    source 23
    target 1553
  ]
  edge [
    source 23
    target 1554
  ]
  edge [
    source 23
    target 1555
  ]
  edge [
    source 23
    target 1556
  ]
  edge [
    source 23
    target 1557
  ]
  edge [
    source 23
    target 1558
  ]
  edge [
    source 23
    target 1559
  ]
  edge [
    source 23
    target 1560
  ]
  edge [
    source 23
    target 1561
  ]
  edge [
    source 23
    target 1562
  ]
  edge [
    source 23
    target 1563
  ]
  edge [
    source 23
    target 1564
  ]
  edge [
    source 23
    target 1565
  ]
  edge [
    source 23
    target 1566
  ]
  edge [
    source 23
    target 1567
  ]
  edge [
    source 23
    target 1568
  ]
  edge [
    source 23
    target 796
  ]
  edge [
    source 23
    target 1569
  ]
  edge [
    source 23
    target 1570
  ]
  edge [
    source 23
    target 1571
  ]
  edge [
    source 23
    target 1572
  ]
  edge [
    source 23
    target 1573
  ]
  edge [
    source 23
    target 1574
  ]
  edge [
    source 23
    target 1575
  ]
  edge [
    source 23
    target 1576
  ]
  edge [
    source 23
    target 1577
  ]
  edge [
    source 23
    target 1578
  ]
  edge [
    source 23
    target 1579
  ]
  edge [
    source 23
    target 1580
  ]
  edge [
    source 23
    target 1581
  ]
  edge [
    source 23
    target 1582
  ]
  edge [
    source 23
    target 1583
  ]
  edge [
    source 23
    target 1584
  ]
  edge [
    source 23
    target 1585
  ]
  edge [
    source 23
    target 1586
  ]
  edge [
    source 23
    target 1587
  ]
  edge [
    source 23
    target 1588
  ]
  edge [
    source 23
    target 1589
  ]
  edge [
    source 23
    target 1590
  ]
  edge [
    source 23
    target 355
  ]
  edge [
    source 23
    target 1591
  ]
  edge [
    source 23
    target 1592
  ]
  edge [
    source 23
    target 1593
  ]
  edge [
    source 23
    target 1594
  ]
  edge [
    source 23
    target 1595
  ]
  edge [
    source 23
    target 1596
  ]
  edge [
    source 23
    target 453
  ]
  edge [
    source 23
    target 1597
  ]
  edge [
    source 23
    target 1598
  ]
  edge [
    source 23
    target 1599
  ]
  edge [
    source 23
    target 615
  ]
  edge [
    source 23
    target 1600
  ]
  edge [
    source 23
    target 1601
  ]
  edge [
    source 23
    target 1602
  ]
  edge [
    source 23
    target 459
  ]
  edge [
    source 23
    target 1603
  ]
  edge [
    source 23
    target 1604
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 1605
  ]
  edge [
    source 23
    target 1606
  ]
  edge [
    source 23
    target 1607
  ]
  edge [
    source 23
    target 1608
  ]
  edge [
    source 23
    target 1609
  ]
  edge [
    source 23
    target 1610
  ]
  edge [
    source 23
    target 1611
  ]
  edge [
    source 23
    target 1612
  ]
  edge [
    source 23
    target 1613
  ]
  edge [
    source 23
    target 1614
  ]
  edge [
    source 23
    target 1615
  ]
  edge [
    source 23
    target 1616
  ]
  edge [
    source 23
    target 1617
  ]
  edge [
    source 23
    target 1618
  ]
  edge [
    source 23
    target 320
  ]
  edge [
    source 23
    target 1619
  ]
  edge [
    source 23
    target 1620
  ]
  edge [
    source 23
    target 1621
  ]
  edge [
    source 23
    target 1622
  ]
  edge [
    source 23
    target 1623
  ]
  edge [
    source 23
    target 1624
  ]
  edge [
    source 23
    target 1625
  ]
  edge [
    source 23
    target 1626
  ]
  edge [
    source 23
    target 1627
  ]
  edge [
    source 23
    target 1628
  ]
  edge [
    source 23
    target 1629
  ]
  edge [
    source 23
    target 1630
  ]
  edge [
    source 23
    target 1631
  ]
  edge [
    source 23
    target 1632
  ]
  edge [
    source 23
    target 1633
  ]
  edge [
    source 23
    target 1634
  ]
  edge [
    source 23
    target 1223
  ]
  edge [
    source 23
    target 1635
  ]
  edge [
    source 23
    target 1636
  ]
  edge [
    source 23
    target 1637
  ]
  edge [
    source 23
    target 1638
  ]
  edge [
    source 23
    target 1639
  ]
  edge [
    source 23
    target 1640
  ]
  edge [
    source 23
    target 1641
  ]
  edge [
    source 23
    target 1642
  ]
  edge [
    source 23
    target 1643
  ]
  edge [
    source 23
    target 1644
  ]
  edge [
    source 23
    target 1645
  ]
  edge [
    source 23
    target 1646
  ]
  edge [
    source 23
    target 1647
  ]
  edge [
    source 23
    target 1648
  ]
  edge [
    source 23
    target 1649
  ]
  edge [
    source 23
    target 1650
  ]
  edge [
    source 23
    target 1651
  ]
  edge [
    source 23
    target 1652
  ]
  edge [
    source 23
    target 1653
  ]
  edge [
    source 23
    target 1654
  ]
  edge [
    source 23
    target 1655
  ]
  edge [
    source 23
    target 1656
  ]
  edge [
    source 23
    target 1657
  ]
  edge [
    source 23
    target 1658
  ]
  edge [
    source 23
    target 1659
  ]
  edge [
    source 23
    target 1660
  ]
  edge [
    source 23
    target 1661
  ]
  edge [
    source 23
    target 1662
  ]
  edge [
    source 23
    target 1663
  ]
  edge [
    source 23
    target 1664
  ]
  edge [
    source 23
    target 1665
  ]
  edge [
    source 23
    target 1666
  ]
  edge [
    source 23
    target 1667
  ]
  edge [
    source 23
    target 105
  ]
  edge [
    source 23
    target 1668
  ]
  edge [
    source 23
    target 1669
  ]
  edge [
    source 23
    target 1670
  ]
  edge [
    source 23
    target 1671
  ]
  edge [
    source 23
    target 1672
  ]
  edge [
    source 23
    target 102
  ]
  edge [
    source 23
    target 1673
  ]
  edge [
    source 23
    target 1674
  ]
  edge [
    source 23
    target 1675
  ]
  edge [
    source 23
    target 1676
  ]
  edge [
    source 23
    target 1677
  ]
  edge [
    source 23
    target 1678
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 1679
  ]
  edge [
    source 23
    target 1680
  ]
  edge [
    source 23
    target 1681
  ]
  edge [
    source 23
    target 1682
  ]
  edge [
    source 23
    target 1683
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1684
  ]
  edge [
    source 24
    target 1685
  ]
  edge [
    source 24
    target 1686
  ]
  edge [
    source 24
    target 1687
  ]
  edge [
    source 24
    target 1688
  ]
  edge [
    source 24
    target 1689
  ]
  edge [
    source 24
    target 1690
  ]
  edge [
    source 24
    target 1691
  ]
  edge [
    source 24
    target 1692
  ]
  edge [
    source 24
    target 1693
  ]
  edge [
    source 24
    target 1694
  ]
  edge [
    source 24
    target 1695
  ]
  edge [
    source 24
    target 1696
  ]
  edge [
    source 24
    target 1697
  ]
  edge [
    source 24
    target 1698
  ]
  edge [
    source 24
    target 1699
  ]
  edge [
    source 24
    target 1700
  ]
  edge [
    source 24
    target 1701
  ]
  edge [
    source 24
    target 1702
  ]
  edge [
    source 24
    target 560
  ]
  edge [
    source 24
    target 1703
  ]
  edge [
    source 24
    target 1578
  ]
  edge [
    source 24
    target 1704
  ]
  edge [
    source 24
    target 1705
  ]
  edge [
    source 24
    target 1706
  ]
  edge [
    source 24
    target 1707
  ]
  edge [
    source 24
    target 1708
  ]
  edge [
    source 24
    target 1709
  ]
  edge [
    source 24
    target 1710
  ]
  edge [
    source 24
    target 1711
  ]
  edge [
    source 24
    target 1712
  ]
  edge [
    source 24
    target 1713
  ]
  edge [
    source 24
    target 1714
  ]
  edge [
    source 24
    target 500
  ]
  edge [
    source 24
    target 1715
  ]
  edge [
    source 24
    target 1716
  ]
  edge [
    source 24
    target 1717
  ]
  edge [
    source 24
    target 1718
  ]
  edge [
    source 24
    target 1719
  ]
  edge [
    source 24
    target 1720
  ]
  edge [
    source 24
    target 1721
  ]
  edge [
    source 24
    target 1524
  ]
  edge [
    source 24
    target 1389
  ]
  edge [
    source 24
    target 1722
  ]
  edge [
    source 24
    target 1723
  ]
  edge [
    source 24
    target 1724
  ]
  edge [
    source 24
    target 1725
  ]
  edge [
    source 24
    target 1726
  ]
  edge [
    source 24
    target 1727
  ]
  edge [
    source 24
    target 1728
  ]
  edge [
    source 24
    target 1729
  ]
  edge [
    source 24
    target 1730
  ]
  edge [
    source 24
    target 1731
  ]
  edge [
    source 25
    target 1732
  ]
  edge [
    source 25
    target 1733
  ]
  edge [
    source 25
    target 1333
  ]
  edge [
    source 25
    target 1651
  ]
  edge [
    source 25
    target 1734
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1735
  ]
  edge [
    source 25
    target 1736
  ]
  edge [
    source 25
    target 1737
  ]
  edge [
    source 25
    target 1738
  ]
  edge [
    source 25
    target 1739
  ]
  edge [
    source 25
    target 1740
  ]
  edge [
    source 25
    target 1741
  ]
  edge [
    source 25
    target 1645
  ]
  edge [
    source 25
    target 1646
  ]
  edge [
    source 25
    target 1647
  ]
  edge [
    source 25
    target 1648
  ]
  edge [
    source 25
    target 1649
  ]
  edge [
    source 25
    target 1650
  ]
  edge [
    source 25
    target 1652
  ]
  edge [
    source 25
    target 1653
  ]
  edge [
    source 25
    target 1654
  ]
  edge [
    source 25
    target 1655
  ]
  edge [
    source 25
    target 1656
  ]
  edge [
    source 25
    target 1657
  ]
  edge [
    source 25
    target 1658
  ]
  edge [
    source 25
    target 1659
  ]
  edge [
    source 25
    target 1660
  ]
  edge [
    source 25
    target 1661
  ]
  edge [
    source 25
    target 1662
  ]
  edge [
    source 25
    target 1663
  ]
  edge [
    source 25
    target 1664
  ]
  edge [
    source 25
    target 1742
  ]
  edge [
    source 25
    target 1743
  ]
  edge [
    source 25
    target 1744
  ]
  edge [
    source 25
    target 1745
  ]
  edge [
    source 25
    target 1746
  ]
  edge [
    source 25
    target 1747
  ]
  edge [
    source 25
    target 1748
  ]
  edge [
    source 25
    target 341
  ]
  edge [
    source 25
    target 1749
  ]
  edge [
    source 25
    target 1750
  ]
  edge [
    source 25
    target 424
  ]
  edge [
    source 25
    target 1751
  ]
  edge [
    source 25
    target 1752
  ]
  edge [
    source 25
    target 1753
  ]
  edge [
    source 25
    target 1754
  ]
  edge [
    source 25
    target 1755
  ]
  edge [
    source 25
    target 1756
  ]
  edge [
    source 25
    target 1757
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 1758
  ]
  edge [
    source 25
    target 1759
  ]
  edge [
    source 25
    target 1760
  ]
  edge [
    source 25
    target 1761
  ]
  edge [
    source 25
    target 1762
  ]
  edge [
    source 25
    target 1763
  ]
  edge [
    source 25
    target 1764
  ]
  edge [
    source 25
    target 1765
  ]
  edge [
    source 25
    target 1766
  ]
  edge [
    source 25
    target 1767
  ]
  edge [
    source 25
    target 457
  ]
  edge [
    source 25
    target 1768
  ]
  edge [
    source 25
    target 367
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 25
    target 1769
  ]
  edge [
    source 25
    target 1442
  ]
  edge [
    source 25
    target 923
  ]
  edge [
    source 25
    target 1770
  ]
  edge [
    source 25
    target 1771
  ]
  edge [
    source 25
    target 1772
  ]
  edge [
    source 25
    target 1773
  ]
  edge [
    source 25
    target 1774
  ]
  edge [
    source 25
    target 1775
  ]
  edge [
    source 25
    target 1776
  ]
  edge [
    source 25
    target 1777
  ]
  edge [
    source 25
    target 1778
  ]
  edge [
    source 25
    target 1779
  ]
  edge [
    source 25
    target 1780
  ]
  edge [
    source 25
    target 1781
  ]
  edge [
    source 25
    target 1782
  ]
  edge [
    source 25
    target 1783
  ]
  edge [
    source 25
    target 1784
  ]
  edge [
    source 25
    target 1785
  ]
  edge [
    source 25
    target 1786
  ]
  edge [
    source 25
    target 1787
  ]
  edge [
    source 25
    target 1788
  ]
  edge [
    source 25
    target 1789
  ]
  edge [
    source 25
    target 1790
  ]
  edge [
    source 25
    target 1791
  ]
  edge [
    source 25
    target 1792
  ]
  edge [
    source 25
    target 1793
  ]
  edge [
    source 25
    target 1794
  ]
  edge [
    source 25
    target 1795
  ]
  edge [
    source 25
    target 1796
  ]
  edge [
    source 25
    target 1797
  ]
  edge [
    source 25
    target 1798
  ]
  edge [
    source 25
    target 1799
  ]
  edge [
    source 25
    target 1800
  ]
  edge [
    source 25
    target 1801
  ]
  edge [
    source 25
    target 1802
  ]
  edge [
    source 25
    target 1803
  ]
  edge [
    source 25
    target 1804
  ]
  edge [
    source 25
    target 1805
  ]
  edge [
    source 25
    target 1806
  ]
  edge [
    source 25
    target 1807
  ]
  edge [
    source 25
    target 1808
  ]
  edge [
    source 25
    target 1809
  ]
  edge [
    source 25
    target 1810
  ]
  edge [
    source 584
    target 1811
  ]
]
