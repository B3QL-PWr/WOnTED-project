graph [
  node [
    id 0
    label "statek"
    origin "text"
  ]
  node [
    id 1
    label "zestaw"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "mowa"
    origin "text"
  ]
  node [
    id 4
    label "rozdzia&#322;"
    origin "text"
  ]
  node [
    id 5
    label "poddawa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 8
    label "woda"
    origin "text"
  ]
  node [
    id 9
    label "p&#322;yn&#261;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "lub"
    origin "text"
  ]
  node [
    id 11
    label "stoj&#261;ca"
    origin "text"
  ]
  node [
    id 12
    label "rejon"
    origin "text"
  ]
  node [
    id 13
    label "proba"
    origin "text"
  ]
  node [
    id 14
    label "aby"
    origin "text"
  ]
  node [
    id 15
    label "wykaza&#263;"
    origin "text"
  ]
  node [
    id 16
    label "zdolny"
    origin "text"
  ]
  node [
    id 17
    label "zatrzymanie"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "podczas"
    origin "text"
  ]
  node [
    id 20
    label "ruch"
    origin "text"
  ]
  node [
    id 21
    label "pr&#261;d"
    origin "text"
  ]
  node [
    id 22
    label "u&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "cela"
    origin "text"
  ]
  node [
    id 24
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 25
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 26
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 27
    label "nap&#281;dowy"
    origin "text"
  ]
  node [
    id 28
    label "bez"
    origin "text"
  ]
  node [
    id 29
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 30
    label "kotwica"
    origin "text"
  ]
  node [
    id 31
    label "zasadniczo"
    origin "text"
  ]
  node [
    id 32
    label "manewr"
    origin "text"
  ]
  node [
    id 33
    label "przeprowadza&#263;"
    origin "text"
  ]
  node [
    id 34
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 35
    label "schemat"
    origin "text"
  ]
  node [
    id 36
    label "przedstawi&#263;"
    origin "text"
  ]
  node [
    id 37
    label "rys"
    origin "text"
  ]
  node [
    id 38
    label "rozpoczyna&#263;"
    origin "text"
  ]
  node [
    id 39
    label "gdy"
    origin "text"
  ]
  node [
    id 40
    label "p&#322;yn"
    origin "text"
  ]
  node [
    id 41
    label "sta&#322;a"
    origin "text"
  ]
  node [
    id 42
    label "pr&#281;dko&#347;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "mo&#380;liwie"
    origin "text"
  ]
  node [
    id 44
    label "jak"
    origin "text"
  ]
  node [
    id 45
    label "bardzo"
    origin "text"
  ]
  node [
    id 46
    label "zbli&#380;ony"
    origin "text"
  ]
  node [
    id 47
    label "warto&#347;&#263;"
    origin "text"
  ]
  node [
    id 48
    label "hora"
    origin "text"
  ]
  node [
    id 49
    label "wzgl&#281;dem"
    origin "text"
  ]
  node [
    id 50
    label "poprzez"
    origin "text"
  ]
  node [
    id 51
    label "przesterowa&#263;"
    origin "text"
  ]
  node [
    id 52
    label "praca"
    origin "text"
  ]
  node [
    id 53
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 54
    label "komenda"
    origin "text"
  ]
  node [
    id 55
    label "stop"
    origin "text"
  ]
  node [
    id 56
    label "przestawa&#263;"
    origin "text"
  ]
  node [
    id 57
    label "przemieszcza&#263;"
    origin "text"
  ]
  node [
    id 58
    label "brzeg"
    origin "text"
  ]
  node [
    id 59
    label "punkt"
    origin "text"
  ]
  node [
    id 60
    label "dawny"
    origin "text"
  ]
  node [
    id 61
    label "dobija&#263;"
  ]
  node [
    id 62
    label "zakotwiczenie"
  ]
  node [
    id 63
    label "odcumowywa&#263;"
  ]
  node [
    id 64
    label "p&#322;ywa&#263;"
  ]
  node [
    id 65
    label "odkotwicza&#263;"
  ]
  node [
    id 66
    label "zwodowanie"
  ]
  node [
    id 67
    label "odkotwiczy&#263;"
  ]
  node [
    id 68
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 69
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 70
    label "odcumowanie"
  ]
  node [
    id 71
    label "odcumowa&#263;"
  ]
  node [
    id 72
    label "zacumowanie"
  ]
  node [
    id 73
    label "kotwiczenie"
  ]
  node [
    id 74
    label "kad&#322;ub"
  ]
  node [
    id 75
    label "reling"
  ]
  node [
    id 76
    label "kabina"
  ]
  node [
    id 77
    label "kotwiczy&#263;"
  ]
  node [
    id 78
    label "szkutnictwo"
  ]
  node [
    id 79
    label "korab"
  ]
  node [
    id 80
    label "odbijacz"
  ]
  node [
    id 81
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 82
    label "dobijanie"
  ]
  node [
    id 83
    label "dobi&#263;"
  ]
  node [
    id 84
    label "proporczyk"
  ]
  node [
    id 85
    label "pok&#322;ad"
  ]
  node [
    id 86
    label "odkotwiczenie"
  ]
  node [
    id 87
    label "kabestan"
  ]
  node [
    id 88
    label "cumowanie"
  ]
  node [
    id 89
    label "zaw&#243;r_denny"
  ]
  node [
    id 90
    label "zadokowa&#263;"
  ]
  node [
    id 91
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 92
    label "flota"
  ]
  node [
    id 93
    label "rostra"
  ]
  node [
    id 94
    label "zr&#281;bnica"
  ]
  node [
    id 95
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 96
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 97
    label "bumsztak"
  ]
  node [
    id 98
    label "sterownik_automatyczny"
  ]
  node [
    id 99
    label "nadbud&#243;wka"
  ]
  node [
    id 100
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 101
    label "cumowa&#263;"
  ]
  node [
    id 102
    label "armator"
  ]
  node [
    id 103
    label "odcumowywanie"
  ]
  node [
    id 104
    label "ster"
  ]
  node [
    id 105
    label "zakotwiczy&#263;"
  ]
  node [
    id 106
    label "zacumowa&#263;"
  ]
  node [
    id 107
    label "wodowanie"
  ]
  node [
    id 108
    label "dobicie"
  ]
  node [
    id 109
    label "zadokowanie"
  ]
  node [
    id 110
    label "dokowa&#263;"
  ]
  node [
    id 111
    label "trap"
  ]
  node [
    id 112
    label "odkotwiczanie"
  ]
  node [
    id 113
    label "luk"
  ]
  node [
    id 114
    label "dzi&#243;b"
  ]
  node [
    id 115
    label "armada"
  ]
  node [
    id 116
    label "&#380;yroskop"
  ]
  node [
    id 117
    label "futr&#243;wka"
  ]
  node [
    id 118
    label "pojazd"
  ]
  node [
    id 119
    label "sztormtrap"
  ]
  node [
    id 120
    label "skrajnik"
  ]
  node [
    id 121
    label "dokowanie"
  ]
  node [
    id 122
    label "zwodowa&#263;"
  ]
  node [
    id 123
    label "grobla"
  ]
  node [
    id 124
    label "Z&#322;ota_Flota"
  ]
  node [
    id 125
    label "zbi&#243;r"
  ]
  node [
    id 126
    label "formacja"
  ]
  node [
    id 127
    label "pieni&#261;dze"
  ]
  node [
    id 128
    label "flotylla"
  ]
  node [
    id 129
    label "eskadra"
  ]
  node [
    id 130
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 131
    label "marynarka_wojenna"
  ]
  node [
    id 132
    label "odholowa&#263;"
  ]
  node [
    id 133
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 134
    label "tabor"
  ]
  node [
    id 135
    label "przyholowywanie"
  ]
  node [
    id 136
    label "przyholowa&#263;"
  ]
  node [
    id 137
    label "przyholowanie"
  ]
  node [
    id 138
    label "fukni&#281;cie"
  ]
  node [
    id 139
    label "l&#261;d"
  ]
  node [
    id 140
    label "zielona_karta"
  ]
  node [
    id 141
    label "fukanie"
  ]
  node [
    id 142
    label "przyholowywa&#263;"
  ]
  node [
    id 143
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 144
    label "przeszklenie"
  ]
  node [
    id 145
    label "test_zderzeniowy"
  ]
  node [
    id 146
    label "powietrze"
  ]
  node [
    id 147
    label "odzywka"
  ]
  node [
    id 148
    label "nadwozie"
  ]
  node [
    id 149
    label "odholowanie"
  ]
  node [
    id 150
    label "prowadzenie_si&#281;"
  ]
  node [
    id 151
    label "odholowywa&#263;"
  ]
  node [
    id 152
    label "pod&#322;oga"
  ]
  node [
    id 153
    label "odholowywanie"
  ]
  node [
    id 154
    label "hamulec"
  ]
  node [
    id 155
    label "podwozie"
  ]
  node [
    id 156
    label "but"
  ]
  node [
    id 157
    label "mur"
  ]
  node [
    id 158
    label "ok&#322;adzina"
  ]
  node [
    id 159
    label "wy&#347;ci&#243;&#322;ka"
  ]
  node [
    id 160
    label "obicie"
  ]
  node [
    id 161
    label "listwa"
  ]
  node [
    id 162
    label "por&#281;cz"
  ]
  node [
    id 163
    label "szafka"
  ]
  node [
    id 164
    label "railing"
  ]
  node [
    id 165
    label "p&#243;&#322;ka"
  ]
  node [
    id 166
    label "barierka"
  ]
  node [
    id 167
    label "oznaka"
  ]
  node [
    id 168
    label "flaga"
  ]
  node [
    id 169
    label "flag"
  ]
  node [
    id 170
    label "proporzec"
  ]
  node [
    id 171
    label "tyczka"
  ]
  node [
    id 172
    label "metryczna_jednostka_obj&#281;to&#347;ci"
  ]
  node [
    id 173
    label "sterownica"
  ]
  node [
    id 174
    label "kilometr_sze&#347;cienny"
  ]
  node [
    id 175
    label "wolant"
  ]
  node [
    id 176
    label "powierzchnia_sterowa"
  ]
  node [
    id 177
    label "sterolotka"
  ]
  node [
    id 178
    label "&#380;agl&#243;wka"
  ]
  node [
    id 179
    label "statek_powietrzny"
  ]
  node [
    id 180
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 181
    label "rumpel"
  ]
  node [
    id 182
    label "mechanizm"
  ]
  node [
    id 183
    label "przyw&#243;dztwo"
  ]
  node [
    id 184
    label "jacht"
  ]
  node [
    id 185
    label "schodki"
  ]
  node [
    id 186
    label "accommodation_ladder"
  ]
  node [
    id 187
    label "gyroscope"
  ]
  node [
    id 188
    label "samolot"
  ]
  node [
    id 189
    label "przyrz&#261;d_nawigacyjny"
  ]
  node [
    id 190
    label "ci&#281;gnik"
  ]
  node [
    id 191
    label "wci&#261;garka"
  ]
  node [
    id 192
    label "zr&#261;b"
  ]
  node [
    id 193
    label "otw&#243;r"
  ]
  node [
    id 194
    label "czo&#322;g"
  ]
  node [
    id 195
    label "pomieszczenie"
  ]
  node [
    id 196
    label "winda"
  ]
  node [
    id 197
    label "bombowiec"
  ]
  node [
    id 198
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 199
    label "wagonik"
  ]
  node [
    id 200
    label "narz&#281;dzie"
  ]
  node [
    id 201
    label "emocja"
  ]
  node [
    id 202
    label "wybieranie"
  ]
  node [
    id 203
    label "wybiera&#263;"
  ]
  node [
    id 204
    label "zegar"
  ]
  node [
    id 205
    label "wybra&#263;"
  ]
  node [
    id 206
    label "wybranie"
  ]
  node [
    id 207
    label "ochrona"
  ]
  node [
    id 208
    label "dobud&#243;wka"
  ]
  node [
    id 209
    label "ptak"
  ]
  node [
    id 210
    label "grzebie&#324;"
  ]
  node [
    id 211
    label "organ"
  ]
  node [
    id 212
    label "struktura_anatomiczna"
  ]
  node [
    id 213
    label "bow"
  ]
  node [
    id 214
    label "ustnik"
  ]
  node [
    id 215
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 216
    label "zako&#324;czenie"
  ]
  node [
    id 217
    label "ostry"
  ]
  node [
    id 218
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 219
    label "blizna"
  ]
  node [
    id 220
    label "dziob&#243;wka"
  ]
  node [
    id 221
    label "drabinka_linowa"
  ]
  node [
    id 222
    label "wa&#322;"
  ]
  node [
    id 223
    label "przegroda"
  ]
  node [
    id 224
    label "trawers"
  ]
  node [
    id 225
    label "kil"
  ]
  node [
    id 226
    label "nadst&#281;pka"
  ]
  node [
    id 227
    label "pachwina"
  ]
  node [
    id 228
    label "brzuch"
  ]
  node [
    id 229
    label "konstrukcyjna_linia_wodna"
  ]
  node [
    id 230
    label "dekolt"
  ]
  node [
    id 231
    label "zad"
  ]
  node [
    id 232
    label "z&#322;ad"
  ]
  node [
    id 233
    label "z&#281;za"
  ]
  node [
    id 234
    label "korpus"
  ]
  node [
    id 235
    label "bok"
  ]
  node [
    id 236
    label "pupa"
  ]
  node [
    id 237
    label "krocze"
  ]
  node [
    id 238
    label "pier&#347;"
  ]
  node [
    id 239
    label "p&#322;atowiec"
  ]
  node [
    id 240
    label "poszycie"
  ]
  node [
    id 241
    label "gr&#243;d&#378;"
  ]
  node [
    id 242
    label "wr&#281;ga"
  ]
  node [
    id 243
    label "maszyna"
  ]
  node [
    id 244
    label "blokownia"
  ]
  node [
    id 245
    label "plecy"
  ]
  node [
    id 246
    label "stojak"
  ]
  node [
    id 247
    label "falszkil"
  ]
  node [
    id 248
    label "klatka_piersiowa"
  ]
  node [
    id 249
    label "biodro"
  ]
  node [
    id 250
    label "pacha"
  ]
  node [
    id 251
    label "podwodzie"
  ]
  node [
    id 252
    label "stewa"
  ]
  node [
    id 253
    label "p&#322;aszczyzna"
  ]
  node [
    id 254
    label "sp&#261;g"
  ]
  node [
    id 255
    label "przestrze&#324;"
  ]
  node [
    id 256
    label "pok&#322;adnik"
  ]
  node [
    id 257
    label "warstwa"
  ]
  node [
    id 258
    label "powierzchnia"
  ]
  node [
    id 259
    label "strop"
  ]
  node [
    id 260
    label "poszycie_pok&#322;adowe"
  ]
  node [
    id 261
    label "kipa"
  ]
  node [
    id 262
    label "p&#243;&#322;pok&#322;ad"
  ]
  node [
    id 263
    label "jut"
  ]
  node [
    id 264
    label "z&#322;o&#380;e"
  ]
  node [
    id 265
    label "zatrzyma&#263;_si&#281;"
  ]
  node [
    id 266
    label "zadomowi&#263;_si&#281;"
  ]
  node [
    id 267
    label "anchor"
  ]
  node [
    id 268
    label "osadzi&#263;"
  ]
  node [
    id 269
    label "przymocowa&#263;"
  ]
  node [
    id 270
    label "przymocowywa&#263;"
  ]
  node [
    id 271
    label "sta&#263;"
  ]
  node [
    id 272
    label "anchorage"
  ]
  node [
    id 273
    label "przymocowywanie"
  ]
  node [
    id 274
    label "stanie"
  ]
  node [
    id 275
    label "zadomowienie_si&#281;"
  ]
  node [
    id 276
    label "przymocowanie"
  ]
  node [
    id 277
    label "osadzenie"
  ]
  node [
    id 278
    label "wyprowadzi&#263;"
  ]
  node [
    id 279
    label "przywi&#261;za&#263;"
  ]
  node [
    id 280
    label "moor"
  ]
  node [
    id 281
    label "zrobi&#263;"
  ]
  node [
    id 282
    label "aerostat"
  ]
  node [
    id 283
    label "cuma"
  ]
  node [
    id 284
    label "wyprowadzenie"
  ]
  node [
    id 285
    label "launching"
  ]
  node [
    id 286
    label "l&#261;dowanie"
  ]
  node [
    id 287
    label "wywodzenie"
  ]
  node [
    id 288
    label "odczepia&#263;"
  ]
  node [
    id 289
    label "rzemios&#322;o"
  ]
  node [
    id 290
    label "impression"
  ]
  node [
    id 291
    label "dokuczenie"
  ]
  node [
    id 292
    label "dorobienie"
  ]
  node [
    id 293
    label "og&#322;oszenie_drukiem"
  ]
  node [
    id 294
    label "nail"
  ]
  node [
    id 295
    label "zawini&#281;cie"
  ]
  node [
    id 296
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 297
    label "doci&#347;ni&#281;cie"
  ]
  node [
    id 298
    label "dotarcie"
  ]
  node [
    id 299
    label "przygn&#281;bienie"
  ]
  node [
    id 300
    label "adjudication"
  ]
  node [
    id 301
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 302
    label "zabicie"
  ]
  node [
    id 303
    label "stacja_kosmiczna"
  ]
  node [
    id 304
    label "wprowadzanie"
  ]
  node [
    id 305
    label "modu&#322;_dokuj&#261;cy"
  ]
  node [
    id 306
    label "przywi&#261;zywanie"
  ]
  node [
    id 307
    label "mooring"
  ]
  node [
    id 308
    label "odcumowanie_si&#281;"
  ]
  node [
    id 309
    label "odczepienie"
  ]
  node [
    id 310
    label "wprowadzenie"
  ]
  node [
    id 311
    label "implantation"
  ]
  node [
    id 312
    label "obsadzenie"
  ]
  node [
    id 313
    label "przywi&#261;zywa&#263;"
  ]
  node [
    id 314
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 315
    label "zawijanie"
  ]
  node [
    id 316
    label "dociskanie"
  ]
  node [
    id 317
    label "zabijanie"
  ]
  node [
    id 318
    label "dop&#322;ywanie"
  ]
  node [
    id 319
    label "docieranie"
  ]
  node [
    id 320
    label "dokuczanie"
  ]
  node [
    id 321
    label "przygn&#281;bianie"
  ]
  node [
    id 322
    label "podmiot_gospodarczy"
  ]
  node [
    id 323
    label "przedsi&#281;biorca"
  ]
  node [
    id 324
    label "&#380;eglugowiec"
  ]
  node [
    id 325
    label "przywi&#261;zanie"
  ]
  node [
    id 326
    label "zrobienie"
  ]
  node [
    id 327
    label "doprowadzi&#263;"
  ]
  node [
    id 328
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 329
    label "dopisa&#263;"
  ]
  node [
    id 330
    label "nabi&#263;"
  ]
  node [
    id 331
    label "get_through"
  ]
  node [
    id 332
    label "przybi&#263;"
  ]
  node [
    id 333
    label "popsu&#263;"
  ]
  node [
    id 334
    label "wybi&#263;"
  ]
  node [
    id 335
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 336
    label "pogorszy&#263;"
  ]
  node [
    id 337
    label "zabi&#263;"
  ]
  node [
    id 338
    label "wbi&#263;"
  ]
  node [
    id 339
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 340
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 341
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 342
    label "dorobi&#263;"
  ]
  node [
    id 343
    label "doj&#347;&#263;"
  ]
  node [
    id 344
    label "za&#322;ama&#263;"
  ]
  node [
    id 345
    label "dopi&#261;&#263;"
  ]
  node [
    id 346
    label "dotrze&#263;"
  ]
  node [
    id 347
    label "pogr&#261;&#380;y&#263;"
  ]
  node [
    id 348
    label "dock"
  ]
  node [
    id 349
    label "wprowadza&#263;"
  ]
  node [
    id 350
    label "drukarz"
  ]
  node [
    id 351
    label "odczepianie"
  ]
  node [
    id 352
    label "odcumowywanie_si&#281;"
  ]
  node [
    id 353
    label "odczepi&#263;"
  ]
  node [
    id 354
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 355
    label "sterowa&#263;"
  ]
  node [
    id 356
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 357
    label "ciecz"
  ]
  node [
    id 358
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 359
    label "mie&#263;"
  ]
  node [
    id 360
    label "m&#243;wi&#263;"
  ]
  node [
    id 361
    label "lata&#263;"
  ]
  node [
    id 362
    label "swimming"
  ]
  node [
    id 363
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 364
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 365
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 366
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 367
    label "pracowa&#263;"
  ]
  node [
    id 368
    label "sink"
  ]
  node [
    id 369
    label "zanika&#263;"
  ]
  node [
    id 370
    label "falowa&#263;"
  ]
  node [
    id 371
    label "dr&#261;g"
  ]
  node [
    id 372
    label "element"
  ]
  node [
    id 373
    label "dais"
  ]
  node [
    id 374
    label "zdobienie"
  ]
  node [
    id 375
    label "dopisywa&#263;"
  ]
  node [
    id 376
    label "psu&#263;"
  ]
  node [
    id 377
    label "wyko&#324;cza&#263;"
  ]
  node [
    id 378
    label "osi&#261;ga&#263;"
  ]
  node [
    id 379
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 380
    label "wbija&#263;"
  ]
  node [
    id 381
    label "wybija&#263;"
  ]
  node [
    id 382
    label "doprowadza&#263;"
  ]
  node [
    id 383
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 384
    label "za&#322;amywa&#263;"
  ]
  node [
    id 385
    label "dorabia&#263;"
  ]
  node [
    id 386
    label "pogr&#261;&#380;a&#263;"
  ]
  node [
    id 387
    label "dociera&#263;"
  ]
  node [
    id 388
    label "nabija&#263;"
  ]
  node [
    id 389
    label "dochodzi&#263;"
  ]
  node [
    id 390
    label "dopina&#263;"
  ]
  node [
    id 391
    label "zabija&#263;"
  ]
  node [
    id 392
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 393
    label "przybija&#263;"
  ]
  node [
    id 394
    label "pogarsza&#263;"
  ]
  node [
    id 395
    label "obsadzi&#263;"
  ]
  node [
    id 396
    label "cuddle"
  ]
  node [
    id 397
    label "wprowadzi&#263;"
  ]
  node [
    id 398
    label "struktura"
  ]
  node [
    id 399
    label "stage_set"
  ]
  node [
    id 400
    label "sk&#322;ada&#263;"
  ]
  node [
    id 401
    label "sygna&#322;"
  ]
  node [
    id 402
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 403
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 404
    label "egzemplarz"
  ]
  node [
    id 405
    label "series"
  ]
  node [
    id 406
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 407
    label "uprawianie"
  ]
  node [
    id 408
    label "praca_rolnicza"
  ]
  node [
    id 409
    label "collection"
  ]
  node [
    id 410
    label "dane"
  ]
  node [
    id 411
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 412
    label "pakiet_klimatyczny"
  ]
  node [
    id 413
    label "poj&#281;cie"
  ]
  node [
    id 414
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 415
    label "sum"
  ]
  node [
    id 416
    label "gathering"
  ]
  node [
    id 417
    label "album"
  ]
  node [
    id 418
    label "przekazywa&#263;"
  ]
  node [
    id 419
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 420
    label "pulsation"
  ]
  node [
    id 421
    label "przekazywanie"
  ]
  node [
    id 422
    label "przewodzenie"
  ]
  node [
    id 423
    label "d&#378;wi&#281;k"
  ]
  node [
    id 424
    label "po&#322;&#261;czenie"
  ]
  node [
    id 425
    label "fala"
  ]
  node [
    id 426
    label "doj&#347;cie"
  ]
  node [
    id 427
    label "przekazanie"
  ]
  node [
    id 428
    label "przewodzi&#263;"
  ]
  node [
    id 429
    label "znak"
  ]
  node [
    id 430
    label "zapowied&#378;"
  ]
  node [
    id 431
    label "medium_transmisyjne"
  ]
  node [
    id 432
    label "demodulacja"
  ]
  node [
    id 433
    label "przekaza&#263;"
  ]
  node [
    id 434
    label "czynnik"
  ]
  node [
    id 435
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 436
    label "aliasing"
  ]
  node [
    id 437
    label "wizja"
  ]
  node [
    id 438
    label "modulacja"
  ]
  node [
    id 439
    label "point"
  ]
  node [
    id 440
    label "drift"
  ]
  node [
    id 441
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 442
    label "mechanika"
  ]
  node [
    id 443
    label "o&#347;"
  ]
  node [
    id 444
    label "usenet"
  ]
  node [
    id 445
    label "rozprz&#261;c"
  ]
  node [
    id 446
    label "zachowanie"
  ]
  node [
    id 447
    label "cybernetyk"
  ]
  node [
    id 448
    label "podsystem"
  ]
  node [
    id 449
    label "system"
  ]
  node [
    id 450
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 451
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 452
    label "sk&#322;ad"
  ]
  node [
    id 453
    label "systemat"
  ]
  node [
    id 454
    label "cecha"
  ]
  node [
    id 455
    label "konstrukcja"
  ]
  node [
    id 456
    label "konstelacja"
  ]
  node [
    id 457
    label "opracowa&#263;"
  ]
  node [
    id 458
    label "give"
  ]
  node [
    id 459
    label "note"
  ]
  node [
    id 460
    label "da&#263;"
  ]
  node [
    id 461
    label "marshal"
  ]
  node [
    id 462
    label "zmieni&#263;"
  ]
  node [
    id 463
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 464
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 465
    label "fold"
  ]
  node [
    id 466
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 467
    label "zebra&#263;"
  ]
  node [
    id 468
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 469
    label "spowodowa&#263;"
  ]
  node [
    id 470
    label "jell"
  ]
  node [
    id 471
    label "frame"
  ]
  node [
    id 472
    label "set"
  ]
  node [
    id 473
    label "scali&#263;"
  ]
  node [
    id 474
    label "odda&#263;"
  ]
  node [
    id 475
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 476
    label "pay"
  ]
  node [
    id 477
    label "zbiera&#263;"
  ]
  node [
    id 478
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 479
    label "przywraca&#263;"
  ]
  node [
    id 480
    label "dawa&#263;"
  ]
  node [
    id 481
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 482
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 483
    label "convey"
  ]
  node [
    id 484
    label "publicize"
  ]
  node [
    id 485
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 486
    label "render"
  ]
  node [
    id 487
    label "uk&#322;ada&#263;"
  ]
  node [
    id 488
    label "opracowywa&#263;"
  ]
  node [
    id 489
    label "oddawa&#263;"
  ]
  node [
    id 490
    label "train"
  ]
  node [
    id 491
    label "zmienia&#263;"
  ]
  node [
    id 492
    label "dzieli&#263;"
  ]
  node [
    id 493
    label "scala&#263;"
  ]
  node [
    id 494
    label "gramatyka"
  ]
  node [
    id 495
    label "kod"
  ]
  node [
    id 496
    label "przet&#322;umaczenie"
  ]
  node [
    id 497
    label "konsonantyzm"
  ]
  node [
    id 498
    label "zdolno&#347;&#263;"
  ]
  node [
    id 499
    label "wokalizm"
  ]
  node [
    id 500
    label "fonetyka"
  ]
  node [
    id 501
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 502
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 503
    label "po_koroniarsku"
  ]
  node [
    id 504
    label "t&#322;umaczenie"
  ]
  node [
    id 505
    label "m&#243;wienie"
  ]
  node [
    id 506
    label "pismo"
  ]
  node [
    id 507
    label "rozumie&#263;"
  ]
  node [
    id 508
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 509
    label "czynno&#347;&#263;"
  ]
  node [
    id 510
    label "rozumienie"
  ]
  node [
    id 511
    label "wypowied&#378;"
  ]
  node [
    id 512
    label "address"
  ]
  node [
    id 513
    label "komunikacja"
  ]
  node [
    id 514
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 515
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 516
    label "s&#322;ownictwo"
  ]
  node [
    id 517
    label "tongue"
  ]
  node [
    id 518
    label "pos&#322;uchanie"
  ]
  node [
    id 519
    label "s&#261;d"
  ]
  node [
    id 520
    label "sparafrazowanie"
  ]
  node [
    id 521
    label "strawestowa&#263;"
  ]
  node [
    id 522
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 523
    label "trawestowa&#263;"
  ]
  node [
    id 524
    label "sparafrazowa&#263;"
  ]
  node [
    id 525
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 526
    label "sformu&#322;owanie"
  ]
  node [
    id 527
    label "parafrazowanie"
  ]
  node [
    id 528
    label "ozdobnik"
  ]
  node [
    id 529
    label "delimitacja"
  ]
  node [
    id 530
    label "parafrazowa&#263;"
  ]
  node [
    id 531
    label "stylizacja"
  ]
  node [
    id 532
    label "komunikat"
  ]
  node [
    id 533
    label "trawestowanie"
  ]
  node [
    id 534
    label "strawestowanie"
  ]
  node [
    id 535
    label "rezultat"
  ]
  node [
    id 536
    label "posiada&#263;"
  ]
  node [
    id 537
    label "potencja&#322;"
  ]
  node [
    id 538
    label "zapomnienie"
  ]
  node [
    id 539
    label "zapomina&#263;"
  ]
  node [
    id 540
    label "zapominanie"
  ]
  node [
    id 541
    label "ability"
  ]
  node [
    id 542
    label "obliczeniowo"
  ]
  node [
    id 543
    label "zapomnie&#263;"
  ]
  node [
    id 544
    label "transportation_system"
  ]
  node [
    id 545
    label "explicite"
  ]
  node [
    id 546
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 547
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 548
    label "wydeptywanie"
  ]
  node [
    id 549
    label "miejsce"
  ]
  node [
    id 550
    label "wydeptanie"
  ]
  node [
    id 551
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 552
    label "implicite"
  ]
  node [
    id 553
    label "ekspedytor"
  ]
  node [
    id 554
    label "activity"
  ]
  node [
    id 555
    label "bezproblemowy"
  ]
  node [
    id 556
    label "wydarzenie"
  ]
  node [
    id 557
    label "language"
  ]
  node [
    id 558
    label "code"
  ]
  node [
    id 559
    label "szyfrowanie"
  ]
  node [
    id 560
    label "ci&#261;g"
  ]
  node [
    id 561
    label "szablon"
  ]
  node [
    id 562
    label "cover"
  ]
  node [
    id 563
    label "j&#281;zyk"
  ]
  node [
    id 564
    label "public_speaking"
  ]
  node [
    id 565
    label "powiadanie"
  ]
  node [
    id 566
    label "przepowiadanie"
  ]
  node [
    id 567
    label "wyznawanie"
  ]
  node [
    id 568
    label "wypowiadanie"
  ]
  node [
    id 569
    label "wydobywanie"
  ]
  node [
    id 570
    label "gaworzenie"
  ]
  node [
    id 571
    label "stosowanie"
  ]
  node [
    id 572
    label "wyra&#380;anie"
  ]
  node [
    id 573
    label "formu&#322;owanie"
  ]
  node [
    id 574
    label "dowalenie"
  ]
  node [
    id 575
    label "przerywanie"
  ]
  node [
    id 576
    label "wydawanie"
  ]
  node [
    id 577
    label "dogadywanie_si&#281;"
  ]
  node [
    id 578
    label "dodawanie"
  ]
  node [
    id 579
    label "prawienie"
  ]
  node [
    id 580
    label "opowiadanie"
  ]
  node [
    id 581
    label "ozywanie_si&#281;"
  ]
  node [
    id 582
    label "zapeszanie"
  ]
  node [
    id 583
    label "zwracanie_si&#281;"
  ]
  node [
    id 584
    label "dysfonia"
  ]
  node [
    id 585
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 586
    label "speaking"
  ]
  node [
    id 587
    label "zauwa&#380;enie"
  ]
  node [
    id 588
    label "mawianie"
  ]
  node [
    id 589
    label "opowiedzenie"
  ]
  node [
    id 590
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 591
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 592
    label "informowanie"
  ]
  node [
    id 593
    label "dogadanie_si&#281;"
  ]
  node [
    id 594
    label "wygadanie"
  ]
  node [
    id 595
    label "psychotest"
  ]
  node [
    id 596
    label "wk&#322;ad"
  ]
  node [
    id 597
    label "handwriting"
  ]
  node [
    id 598
    label "przekaz"
  ]
  node [
    id 599
    label "dzie&#322;o"
  ]
  node [
    id 600
    label "paleograf"
  ]
  node [
    id 601
    label "interpunkcja"
  ]
  node [
    id 602
    label "dzia&#322;"
  ]
  node [
    id 603
    label "grafia"
  ]
  node [
    id 604
    label "communication"
  ]
  node [
    id 605
    label "script"
  ]
  node [
    id 606
    label "zajawka"
  ]
  node [
    id 607
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 608
    label "list"
  ]
  node [
    id 609
    label "adres"
  ]
  node [
    id 610
    label "Zwrotnica"
  ]
  node [
    id 611
    label "czasopismo"
  ]
  node [
    id 612
    label "ok&#322;adka"
  ]
  node [
    id 613
    label "ortografia"
  ]
  node [
    id 614
    label "letter"
  ]
  node [
    id 615
    label "paleografia"
  ]
  node [
    id 616
    label "dokument"
  ]
  node [
    id 617
    label "prasa"
  ]
  node [
    id 618
    label "terminology"
  ]
  node [
    id 619
    label "termin"
  ]
  node [
    id 620
    label "fleksja"
  ]
  node [
    id 621
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 622
    label "sk&#322;adnia"
  ]
  node [
    id 623
    label "kategoria_gramatyczna"
  ]
  node [
    id 624
    label "morfologia"
  ]
  node [
    id 625
    label "asymilowa&#263;"
  ]
  node [
    id 626
    label "g&#322;osownia"
  ]
  node [
    id 627
    label "zasymilowa&#263;"
  ]
  node [
    id 628
    label "phonetics"
  ]
  node [
    id 629
    label "asymilowanie"
  ]
  node [
    id 630
    label "palatogram"
  ]
  node [
    id 631
    label "transkrypcja"
  ]
  node [
    id 632
    label "zasymilowanie"
  ]
  node [
    id 633
    label "explanation"
  ]
  node [
    id 634
    label "bronienie"
  ]
  node [
    id 635
    label "remark"
  ]
  node [
    id 636
    label "przek&#322;adanie"
  ]
  node [
    id 637
    label "zrozumia&#322;y"
  ]
  node [
    id 638
    label "robienie"
  ]
  node [
    id 639
    label "przekonywanie"
  ]
  node [
    id 640
    label "uzasadnianie"
  ]
  node [
    id 641
    label "rozwianie"
  ]
  node [
    id 642
    label "rozwiewanie"
  ]
  node [
    id 643
    label "tekst"
  ]
  node [
    id 644
    label "gossip"
  ]
  node [
    id 645
    label "przedstawianie"
  ]
  node [
    id 646
    label "rendition"
  ]
  node [
    id 647
    label "kr&#281;ty"
  ]
  node [
    id 648
    label "zinterpretowa&#263;"
  ]
  node [
    id 649
    label "put"
  ]
  node [
    id 650
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 651
    label "przekona&#263;"
  ]
  node [
    id 652
    label "poja&#347;nia&#263;"
  ]
  node [
    id 653
    label "robi&#263;"
  ]
  node [
    id 654
    label "u&#322;atwia&#263;"
  ]
  node [
    id 655
    label "elaborate"
  ]
  node [
    id 656
    label "suplikowa&#263;"
  ]
  node [
    id 657
    label "przek&#322;ada&#263;"
  ]
  node [
    id 658
    label "przekonywa&#263;"
  ]
  node [
    id 659
    label "interpretowa&#263;"
  ]
  node [
    id 660
    label "broni&#263;"
  ]
  node [
    id 661
    label "explain"
  ]
  node [
    id 662
    label "przedstawia&#263;"
  ]
  node [
    id 663
    label "sprawowa&#263;"
  ]
  node [
    id 664
    label "uzasadnia&#263;"
  ]
  node [
    id 665
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 666
    label "gaworzy&#263;"
  ]
  node [
    id 667
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 668
    label "rozmawia&#263;"
  ]
  node [
    id 669
    label "wyra&#380;a&#263;"
  ]
  node [
    id 670
    label "umie&#263;"
  ]
  node [
    id 671
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 672
    label "dziama&#263;"
  ]
  node [
    id 673
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 674
    label "formu&#322;owa&#263;"
  ]
  node [
    id 675
    label "express"
  ]
  node [
    id 676
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 677
    label "talk"
  ]
  node [
    id 678
    label "prawi&#263;"
  ]
  node [
    id 679
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 680
    label "powiada&#263;"
  ]
  node [
    id 681
    label "tell"
  ]
  node [
    id 682
    label "chew_the_fat"
  ]
  node [
    id 683
    label "say"
  ]
  node [
    id 684
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 685
    label "informowa&#263;"
  ]
  node [
    id 686
    label "wydobywa&#263;"
  ]
  node [
    id 687
    label "okre&#347;la&#263;"
  ]
  node [
    id 688
    label "hermeneutyka"
  ]
  node [
    id 689
    label "bycie"
  ]
  node [
    id 690
    label "kontekst"
  ]
  node [
    id 691
    label "apprehension"
  ]
  node [
    id 692
    label "wytw&#243;r"
  ]
  node [
    id 693
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 694
    label "interpretation"
  ]
  node [
    id 695
    label "obja&#347;nienie"
  ]
  node [
    id 696
    label "czucie"
  ]
  node [
    id 697
    label "realization"
  ]
  node [
    id 698
    label "kumanie"
  ]
  node [
    id 699
    label "wnioskowanie"
  ]
  node [
    id 700
    label "wiedzie&#263;"
  ]
  node [
    id 701
    label "kuma&#263;"
  ]
  node [
    id 702
    label "czu&#263;"
  ]
  node [
    id 703
    label "match"
  ]
  node [
    id 704
    label "empatia"
  ]
  node [
    id 705
    label "odbiera&#263;"
  ]
  node [
    id 706
    label "see"
  ]
  node [
    id 707
    label "zna&#263;"
  ]
  node [
    id 708
    label "faza"
  ]
  node [
    id 709
    label "interruption"
  ]
  node [
    id 710
    label "podzia&#322;"
  ]
  node [
    id 711
    label "podrozdzia&#322;"
  ]
  node [
    id 712
    label "fragment"
  ]
  node [
    id 713
    label "przebiec"
  ]
  node [
    id 714
    label "charakter"
  ]
  node [
    id 715
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 716
    label "motyw"
  ]
  node [
    id 717
    label "przebiegni&#281;cie"
  ]
  node [
    id 718
    label "fabu&#322;a"
  ]
  node [
    id 719
    label "cykl_astronomiczny"
  ]
  node [
    id 720
    label "coil"
  ]
  node [
    id 721
    label "zjawisko"
  ]
  node [
    id 722
    label "fotoelement"
  ]
  node [
    id 723
    label "komutowanie"
  ]
  node [
    id 724
    label "stan_skupienia"
  ]
  node [
    id 725
    label "nastr&#243;j"
  ]
  node [
    id 726
    label "przerywacz"
  ]
  node [
    id 727
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 728
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 729
    label "kraw&#281;d&#378;"
  ]
  node [
    id 730
    label "obsesja"
  ]
  node [
    id 731
    label "dw&#243;jnik"
  ]
  node [
    id 732
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 733
    label "okres"
  ]
  node [
    id 734
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 735
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 736
    label "przew&#243;d"
  ]
  node [
    id 737
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 738
    label "czas"
  ]
  node [
    id 739
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 740
    label "obw&#243;d"
  ]
  node [
    id 741
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 742
    label "degree"
  ]
  node [
    id 743
    label "komutowa&#263;"
  ]
  node [
    id 744
    label "eksdywizja"
  ]
  node [
    id 745
    label "blastogeneza"
  ]
  node [
    id 746
    label "stopie&#324;"
  ]
  node [
    id 747
    label "competence"
  ]
  node [
    id 748
    label "fission"
  ]
  node [
    id 749
    label "distribution"
  ]
  node [
    id 750
    label "utw&#243;r"
  ]
  node [
    id 751
    label "tytu&#322;"
  ]
  node [
    id 752
    label "zak&#322;adka"
  ]
  node [
    id 753
    label "nomina&#322;"
  ]
  node [
    id 754
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 755
    label "wydawnictwo"
  ]
  node [
    id 756
    label "ekslibris"
  ]
  node [
    id 757
    label "przek&#322;adacz"
  ]
  node [
    id 758
    label "bibliofilstwo"
  ]
  node [
    id 759
    label "falc"
  ]
  node [
    id 760
    label "pagina"
  ]
  node [
    id 761
    label "zw&#243;j"
  ]
  node [
    id 762
    label "podpowiada&#263;"
  ]
  node [
    id 763
    label "decydowa&#263;"
  ]
  node [
    id 764
    label "rezygnowa&#263;"
  ]
  node [
    id 765
    label "use"
  ]
  node [
    id 766
    label "determine"
  ]
  node [
    id 767
    label "work"
  ]
  node [
    id 768
    label "powodowa&#263;"
  ]
  node [
    id 769
    label "reakcja_chemiczna"
  ]
  node [
    id 770
    label "prompt"
  ]
  node [
    id 771
    label "doradza&#263;"
  ]
  node [
    id 772
    label "pomaga&#263;"
  ]
  node [
    id 773
    label "decide"
  ]
  node [
    id 774
    label "klasyfikator"
  ]
  node [
    id 775
    label "mean"
  ]
  node [
    id 776
    label "charge"
  ]
  node [
    id 777
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 778
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 779
    label "mie&#263;_miejsce"
  ]
  node [
    id 780
    label "equal"
  ]
  node [
    id 781
    label "trwa&#263;"
  ]
  node [
    id 782
    label "chodzi&#263;"
  ]
  node [
    id 783
    label "si&#281;ga&#263;"
  ]
  node [
    id 784
    label "stan"
  ]
  node [
    id 785
    label "obecno&#347;&#263;"
  ]
  node [
    id 786
    label "stand"
  ]
  node [
    id 787
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 788
    label "uczestniczy&#263;"
  ]
  node [
    id 789
    label "participate"
  ]
  node [
    id 790
    label "istnie&#263;"
  ]
  node [
    id 791
    label "pozostawa&#263;"
  ]
  node [
    id 792
    label "zostawa&#263;"
  ]
  node [
    id 793
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 794
    label "adhere"
  ]
  node [
    id 795
    label "compass"
  ]
  node [
    id 796
    label "appreciation"
  ]
  node [
    id 797
    label "get"
  ]
  node [
    id 798
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 799
    label "mierzy&#263;"
  ]
  node [
    id 800
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 801
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 802
    label "exsert"
  ]
  node [
    id 803
    label "being"
  ]
  node [
    id 804
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 805
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 806
    label "run"
  ]
  node [
    id 807
    label "bangla&#263;"
  ]
  node [
    id 808
    label "przebiega&#263;"
  ]
  node [
    id 809
    label "wk&#322;ada&#263;"
  ]
  node [
    id 810
    label "proceed"
  ]
  node [
    id 811
    label "carry"
  ]
  node [
    id 812
    label "bywa&#263;"
  ]
  node [
    id 813
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 814
    label "stara&#263;_si&#281;"
  ]
  node [
    id 815
    label "para"
  ]
  node [
    id 816
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 817
    label "str&#243;j"
  ]
  node [
    id 818
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 819
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 820
    label "krok"
  ]
  node [
    id 821
    label "tryb"
  ]
  node [
    id 822
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 823
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 824
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 825
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 826
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 827
    label "continue"
  ]
  node [
    id 828
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 829
    label "Ohio"
  ]
  node [
    id 830
    label "wci&#281;cie"
  ]
  node [
    id 831
    label "Nowy_York"
  ]
  node [
    id 832
    label "samopoczucie"
  ]
  node [
    id 833
    label "Illinois"
  ]
  node [
    id 834
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 835
    label "state"
  ]
  node [
    id 836
    label "Jukatan"
  ]
  node [
    id 837
    label "Kalifornia"
  ]
  node [
    id 838
    label "Wirginia"
  ]
  node [
    id 839
    label "wektor"
  ]
  node [
    id 840
    label "Teksas"
  ]
  node [
    id 841
    label "Goa"
  ]
  node [
    id 842
    label "Waszyngton"
  ]
  node [
    id 843
    label "Massachusetts"
  ]
  node [
    id 844
    label "Alaska"
  ]
  node [
    id 845
    label "Arakan"
  ]
  node [
    id 846
    label "Hawaje"
  ]
  node [
    id 847
    label "Maryland"
  ]
  node [
    id 848
    label "Michigan"
  ]
  node [
    id 849
    label "Arizona"
  ]
  node [
    id 850
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 851
    label "Georgia"
  ]
  node [
    id 852
    label "poziom"
  ]
  node [
    id 853
    label "Pensylwania"
  ]
  node [
    id 854
    label "shape"
  ]
  node [
    id 855
    label "Luizjana"
  ]
  node [
    id 856
    label "Nowy_Meksyk"
  ]
  node [
    id 857
    label "Alabama"
  ]
  node [
    id 858
    label "ilo&#347;&#263;"
  ]
  node [
    id 859
    label "Kansas"
  ]
  node [
    id 860
    label "Oregon"
  ]
  node [
    id 861
    label "Floryda"
  ]
  node [
    id 862
    label "Oklahoma"
  ]
  node [
    id 863
    label "jednostka_administracyjna"
  ]
  node [
    id 864
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 865
    label "do&#347;wiadczenie"
  ]
  node [
    id 866
    label "spotkanie"
  ]
  node [
    id 867
    label "pobiera&#263;"
  ]
  node [
    id 868
    label "metal_szlachetny"
  ]
  node [
    id 869
    label "pobranie"
  ]
  node [
    id 870
    label "usi&#322;owanie"
  ]
  node [
    id 871
    label "pobra&#263;"
  ]
  node [
    id 872
    label "pobieranie"
  ]
  node [
    id 873
    label "effort"
  ]
  node [
    id 874
    label "analiza_chemiczna"
  ]
  node [
    id 875
    label "item"
  ]
  node [
    id 876
    label "sytuacja"
  ]
  node [
    id 877
    label "probiernictwo"
  ]
  node [
    id 878
    label "test"
  ]
  node [
    id 879
    label "dow&#243;d"
  ]
  node [
    id 880
    label "oznakowanie"
  ]
  node [
    id 881
    label "fakt"
  ]
  node [
    id 882
    label "stawia&#263;"
  ]
  node [
    id 883
    label "kodzik"
  ]
  node [
    id 884
    label "postawi&#263;"
  ]
  node [
    id 885
    label "mark"
  ]
  node [
    id 886
    label "herb"
  ]
  node [
    id 887
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 888
    label "attribute"
  ]
  node [
    id 889
    label "implikowa&#263;"
  ]
  node [
    id 890
    label "badanie"
  ]
  node [
    id 891
    label "przechodzenie"
  ]
  node [
    id 892
    label "quiz"
  ]
  node [
    id 893
    label "sprawdzian"
  ]
  node [
    id 894
    label "arkusz"
  ]
  node [
    id 895
    label "przechodzi&#263;"
  ]
  node [
    id 896
    label "rozmiar"
  ]
  node [
    id 897
    label "part"
  ]
  node [
    id 898
    label "Rzym_Zachodni"
  ]
  node [
    id 899
    label "whole"
  ]
  node [
    id 900
    label "Rzym_Wschodni"
  ]
  node [
    id 901
    label "urz&#261;dzenie"
  ]
  node [
    id 902
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 903
    label "szko&#322;a"
  ]
  node [
    id 904
    label "obserwowanie"
  ]
  node [
    id 905
    label "wiedza"
  ]
  node [
    id 906
    label "wy&#347;wiadczenie"
  ]
  node [
    id 907
    label "assay"
  ]
  node [
    id 908
    label "znawstwo"
  ]
  node [
    id 909
    label "skill"
  ]
  node [
    id 910
    label "checkup"
  ]
  node [
    id 911
    label "do&#347;wiadczanie"
  ]
  node [
    id 912
    label "zbadanie"
  ]
  node [
    id 913
    label "potraktowanie"
  ]
  node [
    id 914
    label "eksperiencja"
  ]
  node [
    id 915
    label "poczucie"
  ]
  node [
    id 916
    label "warunki"
  ]
  node [
    id 917
    label "szczeg&#243;&#322;"
  ]
  node [
    id 918
    label "realia"
  ]
  node [
    id 919
    label "doznanie"
  ]
  node [
    id 920
    label "zawarcie"
  ]
  node [
    id 921
    label "znajomy"
  ]
  node [
    id 922
    label "powitanie"
  ]
  node [
    id 923
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 924
    label "spowodowanie"
  ]
  node [
    id 925
    label "zdarzenie_si&#281;"
  ]
  node [
    id 926
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 927
    label "znalezienie"
  ]
  node [
    id 928
    label "employment"
  ]
  node [
    id 929
    label "po&#380;egnanie"
  ]
  node [
    id 930
    label "gather"
  ]
  node [
    id 931
    label "spotykanie"
  ]
  node [
    id 932
    label "spotkanie_si&#281;"
  ]
  node [
    id 933
    label "dzia&#322;anie"
  ]
  node [
    id 934
    label "typ"
  ]
  node [
    id 935
    label "event"
  ]
  node [
    id 936
    label "przyczyna"
  ]
  node [
    id 937
    label "podejmowanie"
  ]
  node [
    id 938
    label "staranie_si&#281;"
  ]
  node [
    id 939
    label "essay"
  ]
  node [
    id 940
    label "kontrola"
  ]
  node [
    id 941
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 942
    label "uzyskanie"
  ]
  node [
    id 943
    label "otrzymanie"
  ]
  node [
    id 944
    label "capture"
  ]
  node [
    id 945
    label "pr&#243;bka"
  ]
  node [
    id 946
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 947
    label "wyci&#281;cie"
  ]
  node [
    id 948
    label "przeszczepienie"
  ]
  node [
    id 949
    label "wymienienie_si&#281;"
  ]
  node [
    id 950
    label "wzi&#281;cie"
  ]
  node [
    id 951
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 952
    label "wyci&#261;&#263;"
  ]
  node [
    id 953
    label "wzi&#261;&#263;"
  ]
  node [
    id 954
    label "catch"
  ]
  node [
    id 955
    label "otrzyma&#263;"
  ]
  node [
    id 956
    label "skopiowa&#263;"
  ]
  node [
    id 957
    label "uzyska&#263;"
  ]
  node [
    id 958
    label "arise"
  ]
  node [
    id 959
    label "branie"
  ]
  node [
    id 960
    label "wycinanie"
  ]
  node [
    id 961
    label "bite"
  ]
  node [
    id 962
    label "otrzymywanie"
  ]
  node [
    id 963
    label "wch&#322;anianie"
  ]
  node [
    id 964
    label "wymienianie_si&#281;"
  ]
  node [
    id 965
    label "przeszczepianie"
  ]
  node [
    id 966
    label "levy"
  ]
  node [
    id 967
    label "wycina&#263;"
  ]
  node [
    id 968
    label "kopiowa&#263;"
  ]
  node [
    id 969
    label "bra&#263;"
  ]
  node [
    id 970
    label "open"
  ]
  node [
    id 971
    label "wch&#322;ania&#263;"
  ]
  node [
    id 972
    label "otrzymywa&#263;"
  ]
  node [
    id 973
    label "raise"
  ]
  node [
    id 974
    label "dotleni&#263;"
  ]
  node [
    id 975
    label "spi&#281;trza&#263;"
  ]
  node [
    id 976
    label "spi&#281;trzenie"
  ]
  node [
    id 977
    label "utylizator"
  ]
  node [
    id 978
    label "obiekt_naturalny"
  ]
  node [
    id 979
    label "p&#322;ycizna"
  ]
  node [
    id 980
    label "nabranie"
  ]
  node [
    id 981
    label "Waruna"
  ]
  node [
    id 982
    label "przyroda"
  ]
  node [
    id 983
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 984
    label "przybieranie"
  ]
  node [
    id 985
    label "uci&#261;g"
  ]
  node [
    id 986
    label "bombast"
  ]
  node [
    id 987
    label "kryptodepresja"
  ]
  node [
    id 988
    label "water"
  ]
  node [
    id 989
    label "wysi&#281;k"
  ]
  node [
    id 990
    label "pustka"
  ]
  node [
    id 991
    label "przybrze&#380;e"
  ]
  node [
    id 992
    label "nap&#243;j"
  ]
  node [
    id 993
    label "spi&#281;trzanie"
  ]
  node [
    id 994
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 995
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 996
    label "bicie"
  ]
  node [
    id 997
    label "klarownik"
  ]
  node [
    id 998
    label "chlastanie"
  ]
  node [
    id 999
    label "woda_s&#322;odka"
  ]
  node [
    id 1000
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1001
    label "nabra&#263;"
  ]
  node [
    id 1002
    label "chlasta&#263;"
  ]
  node [
    id 1003
    label "uj&#281;cie_wody"
  ]
  node [
    id 1004
    label "zrzut"
  ]
  node [
    id 1005
    label "wodnik"
  ]
  node [
    id 1006
    label "l&#243;d"
  ]
  node [
    id 1007
    label "wybrze&#380;e"
  ]
  node [
    id 1008
    label "deklamacja"
  ]
  node [
    id 1009
    label "tlenek"
  ]
  node [
    id 1010
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 1011
    label "wpadni&#281;cie"
  ]
  node [
    id 1012
    label "ciek&#322;y"
  ]
  node [
    id 1013
    label "chlupa&#263;"
  ]
  node [
    id 1014
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 1015
    label "wytoczenie"
  ]
  node [
    id 1016
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 1017
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 1018
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 1019
    label "nieprzejrzysty"
  ]
  node [
    id 1020
    label "podbiega&#263;"
  ]
  node [
    id 1021
    label "baniak"
  ]
  node [
    id 1022
    label "zachlupa&#263;"
  ]
  node [
    id 1023
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 1024
    label "odp&#322;ywanie"
  ]
  node [
    id 1025
    label "cia&#322;o"
  ]
  node [
    id 1026
    label "podbiec"
  ]
  node [
    id 1027
    label "wpadanie"
  ]
  node [
    id 1028
    label "substancja"
  ]
  node [
    id 1029
    label "zwi&#261;zek_nieorganiczny"
  ]
  node [
    id 1030
    label "porcja"
  ]
  node [
    id 1031
    label "wypitek"
  ]
  node [
    id 1032
    label "futility"
  ]
  node [
    id 1033
    label "nico&#347;&#263;"
  ]
  node [
    id 1034
    label "pusta&#263;"
  ]
  node [
    id 1035
    label "uroczysko"
  ]
  node [
    id 1036
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1037
    label "wydzielina"
  ]
  node [
    id 1038
    label "pas"
  ]
  node [
    id 1039
    label "teren"
  ]
  node [
    id 1040
    label "linia"
  ]
  node [
    id 1041
    label "ekoton"
  ]
  node [
    id 1042
    label "str&#261;d"
  ]
  node [
    id 1043
    label "energia"
  ]
  node [
    id 1044
    label "si&#322;a"
  ]
  node [
    id 1045
    label "&#347;ruba_okr&#281;towa"
  ]
  node [
    id 1046
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 1047
    label "gleba"
  ]
  node [
    id 1048
    label "nasyci&#263;"
  ]
  node [
    id 1049
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 1050
    label "dostarczy&#263;"
  ]
  node [
    id 1051
    label "oszwabienie"
  ]
  node [
    id 1052
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 1053
    label "ponacinanie"
  ]
  node [
    id 1054
    label "pozostanie"
  ]
  node [
    id 1055
    label "przyw&#322;aszczenie"
  ]
  node [
    id 1056
    label "pope&#322;nienie"
  ]
  node [
    id 1057
    label "porobienie_si&#281;"
  ]
  node [
    id 1058
    label "wkr&#281;cenie"
  ]
  node [
    id 1059
    label "zdarcie"
  ]
  node [
    id 1060
    label "fraud"
  ]
  node [
    id 1061
    label "podstawienie"
  ]
  node [
    id 1062
    label "kupienie"
  ]
  node [
    id 1063
    label "nabranie_si&#281;"
  ]
  node [
    id 1064
    label "procurement"
  ]
  node [
    id 1065
    label "ogolenie"
  ]
  node [
    id 1066
    label "zamydlenie_"
  ]
  node [
    id 1067
    label "hoax"
  ]
  node [
    id 1068
    label "deceive"
  ]
  node [
    id 1069
    label "oszwabi&#263;"
  ]
  node [
    id 1070
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 1071
    label "objecha&#263;"
  ]
  node [
    id 1072
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 1073
    label "gull"
  ]
  node [
    id 1074
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 1075
    label "naby&#263;"
  ]
  node [
    id 1076
    label "kupi&#263;"
  ]
  node [
    id 1077
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1078
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 1079
    label "zlodowacenie"
  ]
  node [
    id 1080
    label "lody"
  ]
  node [
    id 1081
    label "cia&#322;o_sta&#322;e"
  ]
  node [
    id 1082
    label "lodowacenie"
  ]
  node [
    id 1083
    label "g&#322;ad&#378;"
  ]
  node [
    id 1084
    label "kostkarka"
  ]
  node [
    id 1085
    label "urz&#261;dzenie_wodne"
  ]
  node [
    id 1086
    label "rozcinanie"
  ]
  node [
    id 1087
    label "uderzanie"
  ]
  node [
    id 1088
    label "chlustanie"
  ]
  node [
    id 1089
    label "blockage"
  ]
  node [
    id 1090
    label "pomno&#380;enie"
  ]
  node [
    id 1091
    label "przeszkoda"
  ]
  node [
    id 1092
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1093
    label "spi&#281;trzenie_si&#281;"
  ]
  node [
    id 1094
    label "sterta"
  ]
  node [
    id 1095
    label "formacja_geologiczna"
  ]
  node [
    id 1096
    label "accumulation"
  ]
  node [
    id 1097
    label "accretion"
  ]
  node [
    id 1098
    label "cz&#322;owiek"
  ]
  node [
    id 1099
    label "ptak_wodny"
  ]
  node [
    id 1100
    label "duch"
  ]
  node [
    id 1101
    label "chru&#347;ciele"
  ]
  node [
    id 1102
    label "tama"
  ]
  node [
    id 1103
    label "upi&#281;kszanie"
  ]
  node [
    id 1104
    label "podnoszenie_si&#281;"
  ]
  node [
    id 1105
    label "t&#281;&#380;enie"
  ]
  node [
    id 1106
    label "pi&#281;kniejszy"
  ]
  node [
    id 1107
    label "adornment"
  ]
  node [
    id 1108
    label "stawanie_si&#281;"
  ]
  node [
    id 1109
    label "kszta&#322;t"
  ]
  node [
    id 1110
    label "pasemko"
  ]
  node [
    id 1111
    label "znak_diakrytyczny"
  ]
  node [
    id 1112
    label "zafalowanie"
  ]
  node [
    id 1113
    label "kot"
  ]
  node [
    id 1114
    label "przemoc"
  ]
  node [
    id 1115
    label "reakcja"
  ]
  node [
    id 1116
    label "strumie&#324;"
  ]
  node [
    id 1117
    label "karb"
  ]
  node [
    id 1118
    label "mn&#243;stwo"
  ]
  node [
    id 1119
    label "fit"
  ]
  node [
    id 1120
    label "grzywa_fali"
  ]
  node [
    id 1121
    label "efekt_Dopplera"
  ]
  node [
    id 1122
    label "obcinka"
  ]
  node [
    id 1123
    label "t&#322;um"
  ]
  node [
    id 1124
    label "stream"
  ]
  node [
    id 1125
    label "zafalowa&#263;"
  ]
  node [
    id 1126
    label "rozbicie_si&#281;"
  ]
  node [
    id 1127
    label "wojsko"
  ]
  node [
    id 1128
    label "clutter"
  ]
  node [
    id 1129
    label "rozbijanie_si&#281;"
  ]
  node [
    id 1130
    label "czo&#322;o_fali"
  ]
  node [
    id 1131
    label "uk&#322;adanie"
  ]
  node [
    id 1132
    label "powodowanie"
  ]
  node [
    id 1133
    label "pi&#281;trzenie_si&#281;"
  ]
  node [
    id 1134
    label "rozcina&#263;"
  ]
  node [
    id 1135
    label "splash"
  ]
  node [
    id 1136
    label "chlusta&#263;"
  ]
  node [
    id 1137
    label "uderza&#263;"
  ]
  node [
    id 1138
    label "hinduizm"
  ]
  node [
    id 1139
    label "niebo"
  ]
  node [
    id 1140
    label "accumulate"
  ]
  node [
    id 1141
    label "pomno&#380;y&#263;"
  ]
  node [
    id 1142
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 1143
    label "strike"
  ]
  node [
    id 1144
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 1145
    label "usuwanie"
  ]
  node [
    id 1146
    label "t&#322;oczenie"
  ]
  node [
    id 1147
    label "klinowanie"
  ]
  node [
    id 1148
    label "depopulation"
  ]
  node [
    id 1149
    label "zestrzeliwanie"
  ]
  node [
    id 1150
    label "tryskanie"
  ]
  node [
    id 1151
    label "wybijanie"
  ]
  node [
    id 1152
    label "zestrzelenie"
  ]
  node [
    id 1153
    label "zag&#322;&#281;bianie"
  ]
  node [
    id 1154
    label "wygrywanie"
  ]
  node [
    id 1155
    label "pracowanie"
  ]
  node [
    id 1156
    label "odstrzeliwanie"
  ]
  node [
    id 1157
    label "ripple"
  ]
  node [
    id 1158
    label "bita_&#347;mietana"
  ]
  node [
    id 1159
    label "wystrzelanie"
  ]
  node [
    id 1160
    label "&#322;adowanie"
  ]
  node [
    id 1161
    label "nalewanie"
  ]
  node [
    id 1162
    label "zaklinowanie"
  ]
  node [
    id 1163
    label "wylatywanie"
  ]
  node [
    id 1164
    label "przybijanie"
  ]
  node [
    id 1165
    label "chybianie"
  ]
  node [
    id 1166
    label "plucie"
  ]
  node [
    id 1167
    label "piana"
  ]
  node [
    id 1168
    label "rap"
  ]
  node [
    id 1169
    label "przestrzeliwanie"
  ]
  node [
    id 1170
    label "ruszanie_si&#281;"
  ]
  node [
    id 1171
    label "walczenie"
  ]
  node [
    id 1172
    label "dorzynanie"
  ]
  node [
    id 1173
    label "ostrzelanie"
  ]
  node [
    id 1174
    label "wbijanie_si&#281;"
  ]
  node [
    id 1175
    label "licznik"
  ]
  node [
    id 1176
    label "hit"
  ]
  node [
    id 1177
    label "kopalnia"
  ]
  node [
    id 1178
    label "ostrzeliwanie"
  ]
  node [
    id 1179
    label "trafianie"
  ]
  node [
    id 1180
    label "serce"
  ]
  node [
    id 1181
    label "pra&#380;enie"
  ]
  node [
    id 1182
    label "odpalanie"
  ]
  node [
    id 1183
    label "przyrz&#261;dzanie"
  ]
  node [
    id 1184
    label "odstrzelenie"
  ]
  node [
    id 1185
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 1186
    label "&#380;&#322;obienie"
  ]
  node [
    id 1187
    label "postrzelanie"
  ]
  node [
    id 1188
    label "mi&#281;so"
  ]
  node [
    id 1189
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 1190
    label "rejestrowanie"
  ]
  node [
    id 1191
    label "fire"
  ]
  node [
    id 1192
    label "chybienie"
  ]
  node [
    id 1193
    label "grzanie"
  ]
  node [
    id 1194
    label "brzmienie"
  ]
  node [
    id 1195
    label "collision"
  ]
  node [
    id 1196
    label "palenie"
  ]
  node [
    id 1197
    label "kropni&#281;cie"
  ]
  node [
    id 1198
    label "prze&#322;adowywanie"
  ]
  node [
    id 1199
    label "granie"
  ]
  node [
    id 1200
    label "&#322;adunek"
  ]
  node [
    id 1201
    label "kopia"
  ]
  node [
    id 1202
    label "shit"
  ]
  node [
    id 1203
    label "zbiornik_retencyjny"
  ]
  node [
    id 1204
    label "grandilokwencja"
  ]
  node [
    id 1205
    label "tkanina"
  ]
  node [
    id 1206
    label "pretensjonalno&#347;&#263;"
  ]
  node [
    id 1207
    label "patos"
  ]
  node [
    id 1208
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 1209
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1210
    label "przedmiot"
  ]
  node [
    id 1211
    label "mikrokosmos"
  ]
  node [
    id 1212
    label "ekosystem"
  ]
  node [
    id 1213
    label "rzecz"
  ]
  node [
    id 1214
    label "stw&#243;r"
  ]
  node [
    id 1215
    label "environment"
  ]
  node [
    id 1216
    label "Ziemia"
  ]
  node [
    id 1217
    label "przyra"
  ]
  node [
    id 1218
    label "wszechstworzenie"
  ]
  node [
    id 1219
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1220
    label "fauna"
  ]
  node [
    id 1221
    label "biota"
  ]
  node [
    id 1222
    label "recytatyw"
  ]
  node [
    id 1223
    label "pustos&#322;owie"
  ]
  node [
    id 1224
    label "wyst&#261;pienie"
  ]
  node [
    id 1225
    label "lecie&#263;"
  ]
  node [
    id 1226
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 1227
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 1228
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 1229
    label "bie&#380;e&#263;"
  ]
  node [
    id 1230
    label "zwierz&#281;"
  ]
  node [
    id 1231
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 1232
    label "biega&#263;"
  ]
  node [
    id 1233
    label "tent-fly"
  ]
  node [
    id 1234
    label "rise"
  ]
  node [
    id 1235
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 1236
    label "po&#380;&#261;da&#263;"
  ]
  node [
    id 1237
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 1238
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 1239
    label "omdlewa&#263;"
  ]
  node [
    id 1240
    label "spada&#263;"
  ]
  node [
    id 1241
    label "rush"
  ]
  node [
    id 1242
    label "odchodzi&#263;"
  ]
  node [
    id 1243
    label "fly"
  ]
  node [
    id 1244
    label "i&#347;&#263;"
  ]
  node [
    id 1245
    label "mija&#263;"
  ]
  node [
    id 1246
    label "popyt"
  ]
  node [
    id 1247
    label "biec"
  ]
  node [
    id 1248
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 1249
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1250
    label "hula&#263;"
  ]
  node [
    id 1251
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1252
    label "rozwolnienie"
  ]
  node [
    id 1253
    label "uprawia&#263;"
  ]
  node [
    id 1254
    label "&#347;ciga&#263;_si&#281;"
  ]
  node [
    id 1255
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 1256
    label "dash"
  ]
  node [
    id 1257
    label "cieka&#263;"
  ]
  node [
    id 1258
    label "ucieka&#263;"
  ]
  node [
    id 1259
    label "chorowa&#263;"
  ]
  node [
    id 1260
    label "degenerat"
  ]
  node [
    id 1261
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 1262
    label "zwyrol"
  ]
  node [
    id 1263
    label "czerniak"
  ]
  node [
    id 1264
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1265
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 1266
    label "paszcza"
  ]
  node [
    id 1267
    label "popapraniec"
  ]
  node [
    id 1268
    label "skuba&#263;"
  ]
  node [
    id 1269
    label "skubanie"
  ]
  node [
    id 1270
    label "skubni&#281;cie"
  ]
  node [
    id 1271
    label "agresja"
  ]
  node [
    id 1272
    label "zwierz&#281;ta"
  ]
  node [
    id 1273
    label "farba"
  ]
  node [
    id 1274
    label "istota_&#380;ywa"
  ]
  node [
    id 1275
    label "gad"
  ]
  node [
    id 1276
    label "siedzie&#263;"
  ]
  node [
    id 1277
    label "oswaja&#263;"
  ]
  node [
    id 1278
    label "tresowa&#263;"
  ]
  node [
    id 1279
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 1280
    label "poligamia"
  ]
  node [
    id 1281
    label "oz&#243;r"
  ]
  node [
    id 1282
    label "skubn&#261;&#263;"
  ]
  node [
    id 1283
    label "wios&#322;owa&#263;"
  ]
  node [
    id 1284
    label "le&#380;enie"
  ]
  node [
    id 1285
    label "niecz&#322;owiek"
  ]
  node [
    id 1286
    label "wios&#322;owanie"
  ]
  node [
    id 1287
    label "napasienie_si&#281;"
  ]
  node [
    id 1288
    label "wiwarium"
  ]
  node [
    id 1289
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 1290
    label "animalista"
  ]
  node [
    id 1291
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 1292
    label "budowa"
  ]
  node [
    id 1293
    label "hodowla"
  ]
  node [
    id 1294
    label "pasienie_si&#281;"
  ]
  node [
    id 1295
    label "sodomita"
  ]
  node [
    id 1296
    label "monogamia"
  ]
  node [
    id 1297
    label "przyssawka"
  ]
  node [
    id 1298
    label "budowa_cia&#322;a"
  ]
  node [
    id 1299
    label "okrutnik"
  ]
  node [
    id 1300
    label "grzbiet"
  ]
  node [
    id 1301
    label "weterynarz"
  ]
  node [
    id 1302
    label "&#322;eb"
  ]
  node [
    id 1303
    label "wylinka"
  ]
  node [
    id 1304
    label "bestia"
  ]
  node [
    id 1305
    label "poskramia&#263;"
  ]
  node [
    id 1306
    label "treser"
  ]
  node [
    id 1307
    label "siedzenie"
  ]
  node [
    id 1308
    label "le&#380;e&#263;"
  ]
  node [
    id 1309
    label "bind"
  ]
  node [
    id 1310
    label "get_in_touch"
  ]
  node [
    id 1311
    label "dokoptowywa&#263;"
  ]
  node [
    id 1312
    label "wi&#261;za&#263;"
  ]
  node [
    id 1313
    label "umieszcza&#263;"
  ]
  node [
    id 1314
    label "impersonate"
  ]
  node [
    id 1315
    label "incorporate"
  ]
  node [
    id 1316
    label "submit"
  ]
  node [
    id 1317
    label "dokoptowa&#263;"
  ]
  node [
    id 1318
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1319
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1320
    label "Mazowsze"
  ]
  node [
    id 1321
    label "Anglia"
  ]
  node [
    id 1322
    label "Amazonia"
  ]
  node [
    id 1323
    label "Bordeaux"
  ]
  node [
    id 1324
    label "Naddniestrze"
  ]
  node [
    id 1325
    label "Europa_Zachodnia"
  ]
  node [
    id 1326
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1327
    label "Armagnac"
  ]
  node [
    id 1328
    label "Olszanica"
  ]
  node [
    id 1329
    label "holarktyka"
  ]
  node [
    id 1330
    label "Zamojszczyzna"
  ]
  node [
    id 1331
    label "Amhara"
  ]
  node [
    id 1332
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1333
    label "Arktyka"
  ]
  node [
    id 1334
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1335
    label "Ma&#322;opolska"
  ]
  node [
    id 1336
    label "Turkiestan"
  ]
  node [
    id 1337
    label "Noworosja"
  ]
  node [
    id 1338
    label "Mezoameryka"
  ]
  node [
    id 1339
    label "Lubelszczyzna"
  ]
  node [
    id 1340
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1341
    label "Ba&#322;kany"
  ]
  node [
    id 1342
    label "Kurdystan"
  ]
  node [
    id 1343
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1344
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1345
    label "Baszkiria"
  ]
  node [
    id 1346
    label "Szkocja"
  ]
  node [
    id 1347
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1348
    label "Rakowice"
  ]
  node [
    id 1349
    label "Tonkin"
  ]
  node [
    id 1350
    label "akrecja"
  ]
  node [
    id 1351
    label "Maghreb"
  ]
  node [
    id 1352
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1353
    label "&#321;&#281;g"
  ]
  node [
    id 1354
    label "Kresy_Zachodnie"
  ]
  node [
    id 1355
    label "Nadrenia"
  ]
  node [
    id 1356
    label "wsch&#243;d"
  ]
  node [
    id 1357
    label "Wielkopolska"
  ]
  node [
    id 1358
    label "Zabajkale"
  ]
  node [
    id 1359
    label "Apulia"
  ]
  node [
    id 1360
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1361
    label "po&#322;udnie"
  ]
  node [
    id 1362
    label "Bojkowszczyzna"
  ]
  node [
    id 1363
    label "Piotrowo"
  ]
  node [
    id 1364
    label "Liguria"
  ]
  node [
    id 1365
    label "Pamir"
  ]
  node [
    id 1366
    label "Ruda_Pabianicka"
  ]
  node [
    id 1367
    label "Ludwin&#243;w"
  ]
  node [
    id 1368
    label "Indochiny"
  ]
  node [
    id 1369
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1370
    label "Polinezja"
  ]
  node [
    id 1371
    label "Kurpie"
  ]
  node [
    id 1372
    label "Podlasie"
  ]
  node [
    id 1373
    label "antroposfera"
  ]
  node [
    id 1374
    label "S&#261;decczyzna"
  ]
  node [
    id 1375
    label "Umbria"
  ]
  node [
    id 1376
    label "Karaiby"
  ]
  node [
    id 1377
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1378
    label "Kielecczyzna"
  ]
  node [
    id 1379
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1380
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1381
    label "Skandynawia"
  ]
  node [
    id 1382
    label "Kujawy"
  ]
  node [
    id 1383
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1384
    label "Tyrol"
  ]
  node [
    id 1385
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1386
    label "Huculszczyzna"
  ]
  node [
    id 1387
    label "Turyngia"
  ]
  node [
    id 1388
    label "Toskania"
  ]
  node [
    id 1389
    label "Podhale"
  ]
  node [
    id 1390
    label "Bory_Tucholskie"
  ]
  node [
    id 1391
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1392
    label "p&#243;&#322;noc"
  ]
  node [
    id 1393
    label "Kosowo"
  ]
  node [
    id 1394
    label "Kalabria"
  ]
  node [
    id 1395
    label "Hercegowina"
  ]
  node [
    id 1396
    label "Lotaryngia"
  ]
  node [
    id 1397
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1398
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1399
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1400
    label "Walia"
  ]
  node [
    id 1401
    label "Pow&#261;zki"
  ]
  node [
    id 1402
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1403
    label "Opolskie"
  ]
  node [
    id 1404
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1405
    label "Kampania"
  ]
  node [
    id 1406
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1407
    label "Sand&#380;ak"
  ]
  node [
    id 1408
    label "Zabu&#380;e"
  ]
  node [
    id 1409
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1410
    label "Syjon"
  ]
  node [
    id 1411
    label "Kabylia"
  ]
  node [
    id 1412
    label "Lombardia"
  ]
  node [
    id 1413
    label "Warmia"
  ]
  node [
    id 1414
    label "Neogea"
  ]
  node [
    id 1415
    label "Kaszmir"
  ]
  node [
    id 1416
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1417
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1418
    label "Kaukaz"
  ]
  node [
    id 1419
    label "Europa_Wschodnia"
  ]
  node [
    id 1420
    label "Antarktyka"
  ]
  node [
    id 1421
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1422
    label "Biskupizna"
  ]
  node [
    id 1423
    label "Afryka_Wschodnia"
  ]
  node [
    id 1424
    label "Podkarpacie"
  ]
  node [
    id 1425
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1426
    label "obszar"
  ]
  node [
    id 1427
    label "Afryka_Zachodnia"
  ]
  node [
    id 1428
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1429
    label "Bo&#347;nia"
  ]
  node [
    id 1430
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1431
    label "Oceania"
  ]
  node [
    id 1432
    label "Zab&#322;ocie"
  ]
  node [
    id 1433
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1434
    label "Powi&#347;le"
  ]
  node [
    id 1435
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1436
    label "zach&#243;d"
  ]
  node [
    id 1437
    label "Opolszczyzna"
  ]
  node [
    id 1438
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1439
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1440
    label "Podbeskidzie"
  ]
  node [
    id 1441
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1442
    label "Kaszuby"
  ]
  node [
    id 1443
    label "Ko&#322;yma"
  ]
  node [
    id 1444
    label "Szlezwik"
  ]
  node [
    id 1445
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1446
    label "Mikronezja"
  ]
  node [
    id 1447
    label "Polesie"
  ]
  node [
    id 1448
    label "Kerala"
  ]
  node [
    id 1449
    label "Mazury"
  ]
  node [
    id 1450
    label "Palestyna"
  ]
  node [
    id 1451
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1452
    label "Lauda"
  ]
  node [
    id 1453
    label "Azja_Wschodnia"
  ]
  node [
    id 1454
    label "Syberia_Zachodnia"
  ]
  node [
    id 1455
    label "Zakarpacie"
  ]
  node [
    id 1456
    label "Galicja"
  ]
  node [
    id 1457
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1458
    label "Lubuskie"
  ]
  node [
    id 1459
    label "Laponia"
  ]
  node [
    id 1460
    label "pas_planetoid"
  ]
  node [
    id 1461
    label "Syberia_Wschodnia"
  ]
  node [
    id 1462
    label "Yorkshire"
  ]
  node [
    id 1463
    label "Bawaria"
  ]
  node [
    id 1464
    label "Zag&#243;rze"
  ]
  node [
    id 1465
    label "Andaluzja"
  ]
  node [
    id 1466
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1467
    label "Oksytania"
  ]
  node [
    id 1468
    label "Kociewie"
  ]
  node [
    id 1469
    label "Lasko"
  ]
  node [
    id 1470
    label "Notogea"
  ]
  node [
    id 1471
    label "rozdzielanie"
  ]
  node [
    id 1472
    label "bezbrze&#380;e"
  ]
  node [
    id 1473
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1474
    label "niezmierzony"
  ]
  node [
    id 1475
    label "przedzielenie"
  ]
  node [
    id 1476
    label "nielito&#347;ciwy"
  ]
  node [
    id 1477
    label "rozdziela&#263;"
  ]
  node [
    id 1478
    label "oktant"
  ]
  node [
    id 1479
    label "przedzieli&#263;"
  ]
  node [
    id 1480
    label "przestw&#243;r"
  ]
  node [
    id 1481
    label "terytorium"
  ]
  node [
    id 1482
    label "zakres"
  ]
  node [
    id 1483
    label "wymiar"
  ]
  node [
    id 1484
    label "wiecz&#243;r"
  ]
  node [
    id 1485
    label "sunset"
  ]
  node [
    id 1486
    label "szar&#243;wka"
  ]
  node [
    id 1487
    label "strona_&#347;wiata"
  ]
  node [
    id 1488
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1489
    label "pora"
  ]
  node [
    id 1490
    label "trud"
  ]
  node [
    id 1491
    label "s&#322;o&#324;ce"
  ]
  node [
    id 1492
    label "brzask"
  ]
  node [
    id 1493
    label "pocz&#261;tek"
  ]
  node [
    id 1494
    label "szabas"
  ]
  node [
    id 1495
    label "rano"
  ]
  node [
    id 1496
    label "&#347;rodek"
  ]
  node [
    id 1497
    label "dzie&#324;"
  ]
  node [
    id 1498
    label "dwunasta"
  ]
  node [
    id 1499
    label "godzina"
  ]
  node [
    id 1500
    label "Boreasz"
  ]
  node [
    id 1501
    label "noc"
  ]
  node [
    id 1502
    label "&#347;wiat"
  ]
  node [
    id 1503
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1504
    label "Bie&#380;an&#243;w-Prokocim"
  ]
  node [
    id 1505
    label "Pr&#261;dnik_Czerwony"
  ]
  node [
    id 1506
    label "Czy&#380;yny"
  ]
  node [
    id 1507
    label "Zwierzyniec"
  ]
  node [
    id 1508
    label "Serbia"
  ]
  node [
    id 1509
    label "euro"
  ]
  node [
    id 1510
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1511
    label "lodowiec_kontynentalny"
  ]
  node [
    id 1512
    label "Antarktyda"
  ]
  node [
    id 1513
    label "Rataje"
  ]
  node [
    id 1514
    label "G&#322;uszyna"
  ]
  node [
    id 1515
    label "Warszawa"
  ]
  node [
    id 1516
    label "Kaw&#281;czyn"
  ]
  node [
    id 1517
    label "Podg&#243;rze"
  ]
  node [
    id 1518
    label "D&#281;bniki"
  ]
  node [
    id 1519
    label "Kresy"
  ]
  node [
    id 1520
    label "palearktyka"
  ]
  node [
    id 1521
    label "nearktyka"
  ]
  node [
    id 1522
    label "biosfera"
  ]
  node [
    id 1523
    label "Judea"
  ]
  node [
    id 1524
    label "moszaw"
  ]
  node [
    id 1525
    label "Kanaan"
  ]
  node [
    id 1526
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1527
    label "Anglosas"
  ]
  node [
    id 1528
    label "Jerozolima"
  ]
  node [
    id 1529
    label "Etiopia"
  ]
  node [
    id 1530
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1531
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1532
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1533
    label "Wiktoria"
  ]
  node [
    id 1534
    label "Wielka_Brytania"
  ]
  node [
    id 1535
    label "Guernsey"
  ]
  node [
    id 1536
    label "Conrad"
  ]
  node [
    id 1537
    label "funt_szterling"
  ]
  node [
    id 1538
    label "Unia_Europejska"
  ]
  node [
    id 1539
    label "Portland"
  ]
  node [
    id 1540
    label "NATO"
  ]
  node [
    id 1541
    label "El&#380;bieta_I"
  ]
  node [
    id 1542
    label "Kornwalia"
  ]
  node [
    id 1543
    label "Dolna_Frankonia"
  ]
  node [
    id 1544
    label "Niemcy"
  ]
  node [
    id 1545
    label "W&#322;ochy"
  ]
  node [
    id 1546
    label "Ukraina"
  ]
  node [
    id 1547
    label "Wyspy_Marshalla"
  ]
  node [
    id 1548
    label "Nauru"
  ]
  node [
    id 1549
    label "Mariany"
  ]
  node [
    id 1550
    label "dolar"
  ]
  node [
    id 1551
    label "Karpaty"
  ]
  node [
    id 1552
    label "Beskid_Niski"
  ]
  node [
    id 1553
    label "Polska"
  ]
  node [
    id 1554
    label "Mariensztat"
  ]
  node [
    id 1555
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1556
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1557
    label "Paj&#281;czno"
  ]
  node [
    id 1558
    label "Mogielnica"
  ]
  node [
    id 1559
    label "Gop&#322;o"
  ]
  node [
    id 1560
    label "Francja"
  ]
  node [
    id 1561
    label "Moza"
  ]
  node [
    id 1562
    label "Poprad"
  ]
  node [
    id 1563
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1564
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1565
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1566
    label "Bojanowo"
  ]
  node [
    id 1567
    label "Obra"
  ]
  node [
    id 1568
    label "Wilkowo_Polskie"
  ]
  node [
    id 1569
    label "Dobra"
  ]
  node [
    id 1570
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1571
    label "Samoa"
  ]
  node [
    id 1572
    label "Tonga"
  ]
  node [
    id 1573
    label "Tuwalu"
  ]
  node [
    id 1574
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1575
    label "Rosja"
  ]
  node [
    id 1576
    label "Etruria"
  ]
  node [
    id 1577
    label "Rumelia"
  ]
  node [
    id 1578
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1579
    label "Nowa_Zelandia"
  ]
  node [
    id 1580
    label "Ocean_Spokojny"
  ]
  node [
    id 1581
    label "Palau"
  ]
  node [
    id 1582
    label "Melanezja"
  ]
  node [
    id 1583
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1584
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1585
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1586
    label "Czeczenia"
  ]
  node [
    id 1587
    label "Inguszetia"
  ]
  node [
    id 1588
    label "Abchazja"
  ]
  node [
    id 1589
    label "Sarmata"
  ]
  node [
    id 1590
    label "Dagestan"
  ]
  node [
    id 1591
    label "Eurazja"
  ]
  node [
    id 1592
    label "Indie"
  ]
  node [
    id 1593
    label "Pakistan"
  ]
  node [
    id 1594
    label "Czarnog&#243;ra"
  ]
  node [
    id 1595
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1596
    label "Tatry"
  ]
  node [
    id 1597
    label "Podtatrze"
  ]
  node [
    id 1598
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1599
    label "jezioro"
  ]
  node [
    id 1600
    label "&#346;l&#261;sk"
  ]
  node [
    id 1601
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1602
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1603
    label "Mo&#322;dawia"
  ]
  node [
    id 1604
    label "Podole"
  ]
  node [
    id 1605
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1606
    label "Hiszpania"
  ]
  node [
    id 1607
    label "Austro-W&#281;gry"
  ]
  node [
    id 1608
    label "Algieria"
  ]
  node [
    id 1609
    label "funt_szkocki"
  ]
  node [
    id 1610
    label "Kaledonia"
  ]
  node [
    id 1611
    label "Libia"
  ]
  node [
    id 1612
    label "Maroko"
  ]
  node [
    id 1613
    label "Tunezja"
  ]
  node [
    id 1614
    label "Mauretania"
  ]
  node [
    id 1615
    label "Sahara_Zachodnia"
  ]
  node [
    id 1616
    label "Biskupice"
  ]
  node [
    id 1617
    label "Iwanowice"
  ]
  node [
    id 1618
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1619
    label "Rogo&#378;nik"
  ]
  node [
    id 1620
    label "Ropa"
  ]
  node [
    id 1621
    label "Buriacja"
  ]
  node [
    id 1622
    label "Rozewie"
  ]
  node [
    id 1623
    label "Norwegia"
  ]
  node [
    id 1624
    label "Szwecja"
  ]
  node [
    id 1625
    label "Finlandia"
  ]
  node [
    id 1626
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1627
    label "Kuba"
  ]
  node [
    id 1628
    label "Jamajka"
  ]
  node [
    id 1629
    label "Aruba"
  ]
  node [
    id 1630
    label "Haiti"
  ]
  node [
    id 1631
    label "Kajmany"
  ]
  node [
    id 1632
    label "Portoryko"
  ]
  node [
    id 1633
    label "Anguilla"
  ]
  node [
    id 1634
    label "Bahamy"
  ]
  node [
    id 1635
    label "Antyle"
  ]
  node [
    id 1636
    label "Czechy"
  ]
  node [
    id 1637
    label "Amazonka"
  ]
  node [
    id 1638
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1639
    label "Wietnam"
  ]
  node [
    id 1640
    label "Austria"
  ]
  node [
    id 1641
    label "Alpy"
  ]
  node [
    id 1642
    label "rozrost"
  ]
  node [
    id 1643
    label "wzrost"
  ]
  node [
    id 1644
    label "dysk_akrecyjny"
  ]
  node [
    id 1645
    label "proces_biologiczny"
  ]
  node [
    id 1646
    label "troch&#281;"
  ]
  node [
    id 1647
    label "uzasadni&#263;"
  ]
  node [
    id 1648
    label "testify"
  ]
  node [
    id 1649
    label "realize"
  ]
  node [
    id 1650
    label "stwierdzi&#263;"
  ]
  node [
    id 1651
    label "wyrazi&#263;"
  ]
  node [
    id 1652
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1653
    label "zakomunikowa&#263;"
  ]
  node [
    id 1654
    label "oznaczy&#263;"
  ]
  node [
    id 1655
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 1656
    label "vent"
  ]
  node [
    id 1657
    label "powiedzie&#263;"
  ]
  node [
    id 1658
    label "uzna&#263;"
  ]
  node [
    id 1659
    label "oznajmi&#263;"
  ]
  node [
    id 1660
    label "declare"
  ]
  node [
    id 1661
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 1662
    label "sk&#322;onny"
  ]
  node [
    id 1663
    label "dobry"
  ]
  node [
    id 1664
    label "zdolnie"
  ]
  node [
    id 1665
    label "podatnie"
  ]
  node [
    id 1666
    label "gotowy"
  ]
  node [
    id 1667
    label "podda&#263;_si&#281;"
  ]
  node [
    id 1668
    label "dobroczynny"
  ]
  node [
    id 1669
    label "czw&#243;rka"
  ]
  node [
    id 1670
    label "spokojny"
  ]
  node [
    id 1671
    label "skuteczny"
  ]
  node [
    id 1672
    label "&#347;mieszny"
  ]
  node [
    id 1673
    label "mi&#322;y"
  ]
  node [
    id 1674
    label "grzeczny"
  ]
  node [
    id 1675
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1676
    label "dobrze"
  ]
  node [
    id 1677
    label "ca&#322;y"
  ]
  node [
    id 1678
    label "zwrot"
  ]
  node [
    id 1679
    label "pomy&#347;lny"
  ]
  node [
    id 1680
    label "moralny"
  ]
  node [
    id 1681
    label "drogi"
  ]
  node [
    id 1682
    label "pozytywny"
  ]
  node [
    id 1683
    label "odpowiedni"
  ]
  node [
    id 1684
    label "korzystny"
  ]
  node [
    id 1685
    label "pos&#322;uszny"
  ]
  node [
    id 1686
    label "utalentowany"
  ]
  node [
    id 1687
    label "przefiltrowanie"
  ]
  node [
    id 1688
    label "zamkni&#281;cie"
  ]
  node [
    id 1689
    label "career"
  ]
  node [
    id 1690
    label "zaaresztowanie"
  ]
  node [
    id 1691
    label "przechowanie"
  ]
  node [
    id 1692
    label "closure"
  ]
  node [
    id 1693
    label "observation"
  ]
  node [
    id 1694
    label "funkcjonowanie"
  ]
  node [
    id 1695
    label "uniemo&#380;liwienie"
  ]
  node [
    id 1696
    label "pochowanie"
  ]
  node [
    id 1697
    label "discontinuance"
  ]
  node [
    id 1698
    label "przerwanie"
  ]
  node [
    id 1699
    label "zaczepienie"
  ]
  node [
    id 1700
    label "pozajmowanie"
  ]
  node [
    id 1701
    label "hipostaza"
  ]
  node [
    id 1702
    label "przetrzymanie"
  ]
  node [
    id 1703
    label "oddzia&#322;anie"
  ]
  node [
    id 1704
    label "&#322;apanie"
  ]
  node [
    id 1705
    label "z&#322;apanie"
  ]
  node [
    id 1706
    label "check"
  ]
  node [
    id 1707
    label "unieruchomienie"
  ]
  node [
    id 1708
    label "zabranie"
  ]
  node [
    id 1709
    label "przestanie"
  ]
  node [
    id 1710
    label "reply"
  ]
  node [
    id 1711
    label "zahipnotyzowanie"
  ]
  node [
    id 1712
    label "chemia"
  ]
  node [
    id 1713
    label "wdarcie_si&#281;"
  ]
  node [
    id 1714
    label "campaign"
  ]
  node [
    id 1715
    label "causing"
  ]
  node [
    id 1716
    label "zaj&#281;cie"
  ]
  node [
    id 1717
    label "wyniesienie"
  ]
  node [
    id 1718
    label "pickings"
  ]
  node [
    id 1719
    label "pozabieranie"
  ]
  node [
    id 1720
    label "pousuwanie"
  ]
  node [
    id 1721
    label "przesuni&#281;cie"
  ]
  node [
    id 1722
    label "przeniesienie"
  ]
  node [
    id 1723
    label "zniewolenie"
  ]
  node [
    id 1724
    label "zniesienie"
  ]
  node [
    id 1725
    label "coitus_interruptus"
  ]
  node [
    id 1726
    label "udanie_si&#281;"
  ]
  node [
    id 1727
    label "wywiezienie"
  ]
  node [
    id 1728
    label "removal"
  ]
  node [
    id 1729
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1730
    label "doprowadzenie"
  ]
  node [
    id 1731
    label "przeszkodzenie"
  ]
  node [
    id 1732
    label "ukr&#281;cenie_"
  ]
  node [
    id 1733
    label "frustration"
  ]
  node [
    id 1734
    label "immobilization"
  ]
  node [
    id 1735
    label "wstrzymanie"
  ]
  node [
    id 1736
    label "nieruchomy"
  ]
  node [
    id 1737
    label "porozrywanie"
  ]
  node [
    id 1738
    label "przerywa&#263;"
  ]
  node [
    id 1739
    label "rozerwanie"
  ]
  node [
    id 1740
    label "poprzerywanie"
  ]
  node [
    id 1741
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 1742
    label "severance"
  ]
  node [
    id 1743
    label "przerzedzenie"
  ]
  node [
    id 1744
    label "przerwa&#263;"
  ]
  node [
    id 1745
    label "odpoczywanie"
  ]
  node [
    id 1746
    label "przedziurawienie"
  ]
  node [
    id 1747
    label "wada_wrodzona"
  ]
  node [
    id 1748
    label "urwanie"
  ]
  node [
    id 1749
    label "cutoff"
  ]
  node [
    id 1750
    label "kultywar"
  ]
  node [
    id 1751
    label "clang"
  ]
  node [
    id 1752
    label "zabieranie"
  ]
  node [
    id 1753
    label "d&#322;o&#324;"
  ]
  node [
    id 1754
    label "wy&#322;awianie"
  ]
  node [
    id 1755
    label "na&#322;apanie_si&#281;"
  ]
  node [
    id 1756
    label "zara&#380;anie_si&#281;"
  ]
  node [
    id 1757
    label "porywanie"
  ]
  node [
    id 1758
    label "uniemo&#380;liwianie"
  ]
  node [
    id 1759
    label "ogarnianie"
  ]
  node [
    id 1760
    label "cause"
  ]
  node [
    id 1761
    label "causal_agent"
  ]
  node [
    id 1762
    label "zaskoczenie"
  ]
  node [
    id 1763
    label "zara&#380;enie_si&#281;"
  ]
  node [
    id 1764
    label "porwanie"
  ]
  node [
    id 1765
    label "chwycenie"
  ]
  node [
    id 1766
    label "na&#322;apanie"
  ]
  node [
    id 1767
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 1768
    label "oduczenie"
  ]
  node [
    id 1769
    label "disavowal"
  ]
  node [
    id 1770
    label "cessation"
  ]
  node [
    id 1771
    label "przeczekanie"
  ]
  node [
    id 1772
    label "commitment"
  ]
  node [
    id 1773
    label "zgarni&#281;cie"
  ]
  node [
    id 1774
    label "ukrycie"
  ]
  node [
    id 1775
    label "retention"
  ]
  node [
    id 1776
    label "preserve"
  ]
  node [
    id 1777
    label "zmagazynowanie"
  ]
  node [
    id 1778
    label "podtrzymanie"
  ]
  node [
    id 1779
    label "uchronienie"
  ]
  node [
    id 1780
    label "potrzymanie"
  ]
  node [
    id 1781
    label "pokonanie"
  ]
  node [
    id 1782
    label "utrzymanie"
  ]
  node [
    id 1783
    label "experience"
  ]
  node [
    id 1784
    label "przepuszczenie"
  ]
  node [
    id 1785
    label "nerka"
  ]
  node [
    id 1786
    label "oczyszczenie"
  ]
  node [
    id 1787
    label "usuni&#281;cie"
  ]
  node [
    id 1788
    label "poumieszczanie"
  ]
  node [
    id 1789
    label "burying"
  ]
  node [
    id 1790
    label "powk&#322;adanie"
  ]
  node [
    id 1791
    label "zw&#322;oki"
  ]
  node [
    id 1792
    label "burial"
  ]
  node [
    id 1793
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1794
    label "gr&#243;b"
  ]
  node [
    id 1795
    label "spocz&#281;cie"
  ]
  node [
    id 1796
    label "zagadni&#281;cie"
  ]
  node [
    id 1797
    label "napadni&#281;cie"
  ]
  node [
    id 1798
    label "attack"
  ]
  node [
    id 1799
    label "zaatakowanie"
  ]
  node [
    id 1800
    label "odwiedzenie"
  ]
  node [
    id 1801
    label "clasp"
  ]
  node [
    id 1802
    label "zaczepianie"
  ]
  node [
    id 1803
    label "podtrzymywanie"
  ]
  node [
    id 1804
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1805
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1806
    label "nakr&#281;cenie"
  ]
  node [
    id 1807
    label "uruchomienie"
  ]
  node [
    id 1808
    label "funkcja"
  ]
  node [
    id 1809
    label "nakr&#281;canie"
  ]
  node [
    id 1810
    label "impact"
  ]
  node [
    id 1811
    label "tr&#243;jstronny"
  ]
  node [
    id 1812
    label "dzianie_si&#281;"
  ]
  node [
    id 1813
    label "uruchamianie"
  ]
  node [
    id 1814
    label "pogl&#261;d"
  ]
  node [
    id 1815
    label "Jezus_Chrystus"
  ]
  node [
    id 1816
    label "osoba"
  ]
  node [
    id 1817
    label "B&#243;g_Ojciec"
  ]
  node [
    id 1818
    label "Plotyn"
  ]
  node [
    id 1819
    label "Tr&#243;jca_&#346;wi&#281;ta"
  ]
  node [
    id 1820
    label "figura_my&#347;li"
  ]
  node [
    id 1821
    label "hypostasis"
  ]
  node [
    id 1822
    label "Duch_&#346;wi&#281;ty"
  ]
  node [
    id 1823
    label "wcielenie"
  ]
  node [
    id 1824
    label "stani&#281;cie"
  ]
  node [
    id 1825
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1826
    label "przyskrzynienie"
  ]
  node [
    id 1827
    label "przygaszenie"
  ]
  node [
    id 1828
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 1829
    label "profligacy"
  ]
  node [
    id 1830
    label "dissolution"
  ]
  node [
    id 1831
    label "completion"
  ]
  node [
    id 1832
    label "end"
  ]
  node [
    id 1833
    label "uj&#281;cie"
  ]
  node [
    id 1834
    label "exit"
  ]
  node [
    id 1835
    label "rozwi&#261;zanie"
  ]
  node [
    id 1836
    label "closing"
  ]
  node [
    id 1837
    label "z&#322;o&#380;enie"
  ]
  node [
    id 1838
    label "release"
  ]
  node [
    id 1839
    label "umieszczenie"
  ]
  node [
    id 1840
    label "zablokowanie"
  ]
  node [
    id 1841
    label "pozamykanie"
  ]
  node [
    id 1842
    label "utrzymywanie"
  ]
  node [
    id 1843
    label "move"
  ]
  node [
    id 1844
    label "poruszenie"
  ]
  node [
    id 1845
    label "movement"
  ]
  node [
    id 1846
    label "myk"
  ]
  node [
    id 1847
    label "utrzyma&#263;"
  ]
  node [
    id 1848
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1849
    label "travel"
  ]
  node [
    id 1850
    label "kanciasty"
  ]
  node [
    id 1851
    label "commercial_enterprise"
  ]
  node [
    id 1852
    label "model"
  ]
  node [
    id 1853
    label "proces"
  ]
  node [
    id 1854
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1855
    label "kr&#243;tki"
  ]
  node [
    id 1856
    label "taktyka"
  ]
  node [
    id 1857
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1858
    label "apraksja"
  ]
  node [
    id 1859
    label "natural_process"
  ]
  node [
    id 1860
    label "utrzymywa&#263;"
  ]
  node [
    id 1861
    label "d&#322;ugi"
  ]
  node [
    id 1862
    label "dyssypacja_energii"
  ]
  node [
    id 1863
    label "tumult"
  ]
  node [
    id 1864
    label "stopek"
  ]
  node [
    id 1865
    label "zmiana"
  ]
  node [
    id 1866
    label "lokomocja"
  ]
  node [
    id 1867
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1868
    label "kognicja"
  ]
  node [
    id 1869
    label "przebieg"
  ]
  node [
    id 1870
    label "rozprawa"
  ]
  node [
    id 1871
    label "legislacyjnie"
  ]
  node [
    id 1872
    label "przes&#322;anka"
  ]
  node [
    id 1873
    label "nast&#281;pstwo"
  ]
  node [
    id 1874
    label "action"
  ]
  node [
    id 1875
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1876
    label "postawa"
  ]
  node [
    id 1877
    label "posuni&#281;cie"
  ]
  node [
    id 1878
    label "maneuver"
  ]
  node [
    id 1879
    label "absolutorium"
  ]
  node [
    id 1880
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1881
    label "boski"
  ]
  node [
    id 1882
    label "krajobraz"
  ]
  node [
    id 1883
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1884
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1885
    label "przywidzenie"
  ]
  node [
    id 1886
    label "presence"
  ]
  node [
    id 1887
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1888
    label "rewizja"
  ]
  node [
    id 1889
    label "passage"
  ]
  node [
    id 1890
    label "change"
  ]
  node [
    id 1891
    label "ferment"
  ]
  node [
    id 1892
    label "komplet"
  ]
  node [
    id 1893
    label "anatomopatolog"
  ]
  node [
    id 1894
    label "zmianka"
  ]
  node [
    id 1895
    label "amendment"
  ]
  node [
    id 1896
    label "odmienianie"
  ]
  node [
    id 1897
    label "tura"
  ]
  node [
    id 1898
    label "daleki"
  ]
  node [
    id 1899
    label "d&#322;ugo"
  ]
  node [
    id 1900
    label "mechanika_teoretyczna"
  ]
  node [
    id 1901
    label "mechanika_gruntu"
  ]
  node [
    id 1902
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 1903
    label "mechanika_klasyczna"
  ]
  node [
    id 1904
    label "elektromechanika"
  ]
  node [
    id 1905
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 1906
    label "nauka"
  ]
  node [
    id 1907
    label "fizyka"
  ]
  node [
    id 1908
    label "aeromechanika"
  ]
  node [
    id 1909
    label "telemechanika"
  ]
  node [
    id 1910
    label "hydromechanika"
  ]
  node [
    id 1911
    label "disquiet"
  ]
  node [
    id 1912
    label "ha&#322;as"
  ]
  node [
    id 1913
    label "woda_powierzchniowa"
  ]
  node [
    id 1914
    label "ciek_wodny"
  ]
  node [
    id 1915
    label "Ajgospotamoj"
  ]
  node [
    id 1916
    label "szybki"
  ]
  node [
    id 1917
    label "jednowyrazowy"
  ]
  node [
    id 1918
    label "bliski"
  ]
  node [
    id 1919
    label "s&#322;aby"
  ]
  node [
    id 1920
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1921
    label "kr&#243;tko"
  ]
  node [
    id 1922
    label "drobny"
  ]
  node [
    id 1923
    label "brak"
  ]
  node [
    id 1924
    label "z&#322;y"
  ]
  node [
    id 1925
    label "obronienie"
  ]
  node [
    id 1926
    label "zap&#322;acenie"
  ]
  node [
    id 1927
    label "preservation"
  ]
  node [
    id 1928
    label "byt"
  ]
  node [
    id 1929
    label "bearing"
  ]
  node [
    id 1930
    label "zdo&#322;anie"
  ]
  node [
    id 1931
    label "subsystencja"
  ]
  node [
    id 1932
    label "uniesienie"
  ]
  node [
    id 1933
    label "wy&#380;ywienie"
  ]
  node [
    id 1934
    label "zapewnienie"
  ]
  node [
    id 1935
    label "wychowanie"
  ]
  node [
    id 1936
    label "obroni&#263;"
  ]
  node [
    id 1937
    label "potrzyma&#263;"
  ]
  node [
    id 1938
    label "op&#322;aci&#263;"
  ]
  node [
    id 1939
    label "zdo&#322;a&#263;"
  ]
  node [
    id 1940
    label "podtrzyma&#263;"
  ]
  node [
    id 1941
    label "feed"
  ]
  node [
    id 1942
    label "przetrzyma&#263;"
  ]
  node [
    id 1943
    label "foster"
  ]
  node [
    id 1944
    label "zapewni&#263;"
  ]
  node [
    id 1945
    label "zachowa&#263;"
  ]
  node [
    id 1946
    label "unie&#347;&#263;"
  ]
  node [
    id 1947
    label "argue"
  ]
  node [
    id 1948
    label "podtrzymywa&#263;"
  ]
  node [
    id 1949
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1950
    label "twierdzi&#263;"
  ]
  node [
    id 1951
    label "zapewnia&#263;"
  ]
  node [
    id 1952
    label "corroborate"
  ]
  node [
    id 1953
    label "trzyma&#263;"
  ]
  node [
    id 1954
    label "panowa&#263;"
  ]
  node [
    id 1955
    label "defy"
  ]
  node [
    id 1956
    label "cope"
  ]
  node [
    id 1957
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 1958
    label "zachowywa&#263;"
  ]
  node [
    id 1959
    label "trzymanie"
  ]
  node [
    id 1960
    label "wychowywanie"
  ]
  node [
    id 1961
    label "panowanie"
  ]
  node [
    id 1962
    label "zachowywanie"
  ]
  node [
    id 1963
    label "twierdzenie"
  ]
  node [
    id 1964
    label "chowanie"
  ]
  node [
    id 1965
    label "op&#322;acanie"
  ]
  node [
    id 1966
    label "s&#261;dzenie"
  ]
  node [
    id 1967
    label "zapewnianie"
  ]
  node [
    id 1968
    label "wzbudzenie"
  ]
  node [
    id 1969
    label "gesture"
  ]
  node [
    id 1970
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1971
    label "poruszanie_si&#281;"
  ]
  node [
    id 1972
    label "nietaktowny"
  ]
  node [
    id 1973
    label "kanciasto"
  ]
  node [
    id 1974
    label "niezgrabny"
  ]
  node [
    id 1975
    label "kanciaty"
  ]
  node [
    id 1976
    label "szorstki"
  ]
  node [
    id 1977
    label "niesk&#322;adny"
  ]
  node [
    id 1978
    label "stra&#380;nik"
  ]
  node [
    id 1979
    label "przedszkole"
  ]
  node [
    id 1980
    label "opiekun"
  ]
  node [
    id 1981
    label "spos&#243;b"
  ]
  node [
    id 1982
    label "prezenter"
  ]
  node [
    id 1983
    label "mildew"
  ]
  node [
    id 1984
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1985
    label "motif"
  ]
  node [
    id 1986
    label "pozowanie"
  ]
  node [
    id 1987
    label "ideal"
  ]
  node [
    id 1988
    label "wz&#243;r"
  ]
  node [
    id 1989
    label "matryca"
  ]
  node [
    id 1990
    label "adaptation"
  ]
  node [
    id 1991
    label "pozowa&#263;"
  ]
  node [
    id 1992
    label "imitacja"
  ]
  node [
    id 1993
    label "orygina&#322;"
  ]
  node [
    id 1994
    label "facet"
  ]
  node [
    id 1995
    label "miniatura"
  ]
  node [
    id 1996
    label "apraxia"
  ]
  node [
    id 1997
    label "zaburzenie"
  ]
  node [
    id 1998
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1999
    label "sport_motorowy"
  ]
  node [
    id 2000
    label "jazda"
  ]
  node [
    id 2001
    label "zwiad"
  ]
  node [
    id 2002
    label "metoda"
  ]
  node [
    id 2003
    label "pocz&#261;tki"
  ]
  node [
    id 2004
    label "wrinkle"
  ]
  node [
    id 2005
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 2006
    label "Sierpie&#324;"
  ]
  node [
    id 2007
    label "Michnik"
  ]
  node [
    id 2008
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 2009
    label "solidarno&#347;ciowiec"
  ]
  node [
    id 2010
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 2011
    label "przep&#322;yw"
  ]
  node [
    id 2012
    label "ideologia"
  ]
  node [
    id 2013
    label "apparent_motion"
  ]
  node [
    id 2014
    label "przyp&#322;yw"
  ]
  node [
    id 2015
    label "electricity"
  ]
  node [
    id 2016
    label "dreszcz"
  ]
  node [
    id 2017
    label "praktyka"
  ]
  node [
    id 2018
    label "flow"
  ]
  node [
    id 2019
    label "p&#322;yw"
  ]
  node [
    id 2020
    label "obieg"
  ]
  node [
    id 2021
    label "flux"
  ]
  node [
    id 2022
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 2023
    label "emitowa&#263;"
  ]
  node [
    id 2024
    label "egzergia"
  ]
  node [
    id 2025
    label "kwant_energii"
  ]
  node [
    id 2026
    label "szwung"
  ]
  node [
    id 2027
    label "power"
  ]
  node [
    id 2028
    label "emitowanie"
  ]
  node [
    id 2029
    label "energy"
  ]
  node [
    id 2030
    label "j&#261;dro"
  ]
  node [
    id 2031
    label "systemik"
  ]
  node [
    id 2032
    label "oprogramowanie"
  ]
  node [
    id 2033
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 2034
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 2035
    label "porz&#261;dek"
  ]
  node [
    id 2036
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 2037
    label "przyn&#281;ta"
  ]
  node [
    id 2038
    label "p&#322;&#243;d"
  ]
  node [
    id 2039
    label "net"
  ]
  node [
    id 2040
    label "w&#281;dkarstwo"
  ]
  node [
    id 2041
    label "eratem"
  ]
  node [
    id 2042
    label "oddzia&#322;"
  ]
  node [
    id 2043
    label "doktryna"
  ]
  node [
    id 2044
    label "pulpit"
  ]
  node [
    id 2045
    label "jednostka_geologiczna"
  ]
  node [
    id 2046
    label "ryba"
  ]
  node [
    id 2047
    label "Leopard"
  ]
  node [
    id 2048
    label "Android"
  ]
  node [
    id 2049
    label "method"
  ]
  node [
    id 2050
    label "podstawa"
  ]
  node [
    id 2051
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 2052
    label "practice"
  ]
  node [
    id 2053
    label "czyn"
  ]
  node [
    id 2054
    label "zwyczaj"
  ]
  node [
    id 2055
    label "throb"
  ]
  node [
    id 2056
    label "ci&#261;goty"
  ]
  node [
    id 2057
    label "political_orientation"
  ]
  node [
    id 2058
    label "idea"
  ]
  node [
    id 2059
    label "distribute"
  ]
  node [
    id 2060
    label "bash"
  ]
  node [
    id 2061
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 2062
    label "doznawa&#263;"
  ]
  node [
    id 2063
    label "uzyskiwa&#263;"
  ]
  node [
    id 2064
    label "mutant"
  ]
  node [
    id 2065
    label "dobrostan"
  ]
  node [
    id 2066
    label "u&#380;ycie"
  ]
  node [
    id 2067
    label "u&#380;y&#263;"
  ]
  node [
    id 2068
    label "bawienie"
  ]
  node [
    id 2069
    label "lubo&#347;&#263;"
  ]
  node [
    id 2070
    label "prze&#380;ycie"
  ]
  node [
    id 2071
    label "u&#380;ywanie"
  ]
  node [
    id 2072
    label "hurt"
  ]
  node [
    id 2073
    label "klasztor"
  ]
  node [
    id 2074
    label "amfilada"
  ]
  node [
    id 2075
    label "front"
  ]
  node [
    id 2076
    label "apartment"
  ]
  node [
    id 2077
    label "udost&#281;pnienie"
  ]
  node [
    id 2078
    label "sklepienie"
  ]
  node [
    id 2079
    label "sufit"
  ]
  node [
    id 2080
    label "zakamarek"
  ]
  node [
    id 2081
    label "siedziba"
  ]
  node [
    id 2082
    label "wirydarz"
  ]
  node [
    id 2083
    label "kustodia"
  ]
  node [
    id 2084
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 2085
    label "zakon"
  ]
  node [
    id 2086
    label "refektarz"
  ]
  node [
    id 2087
    label "kapitularz"
  ]
  node [
    id 2088
    label "oratorium"
  ]
  node [
    id 2089
    label "&#321;agiewniki"
  ]
  node [
    id 2090
    label "wy&#322;&#261;czny"
  ]
  node [
    id 2091
    label "w&#322;asny"
  ]
  node [
    id 2092
    label "unikatowy"
  ]
  node [
    id 2093
    label "jedyny"
  ]
  node [
    id 2094
    label "treaty"
  ]
  node [
    id 2095
    label "umowa"
  ]
  node [
    id 2096
    label "przestawi&#263;"
  ]
  node [
    id 2097
    label "alliance"
  ]
  node [
    id 2098
    label "ONZ"
  ]
  node [
    id 2099
    label "zawrze&#263;"
  ]
  node [
    id 2100
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 2101
    label "wi&#281;&#378;"
  ]
  node [
    id 2102
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 2103
    label "traktat_wersalski"
  ]
  node [
    id 2104
    label "warunek"
  ]
  node [
    id 2105
    label "gestia_transportowa"
  ]
  node [
    id 2106
    label "contract"
  ]
  node [
    id 2107
    label "porozumienie"
  ]
  node [
    id 2108
    label "klauzula"
  ]
  node [
    id 2109
    label "zrelatywizowa&#263;"
  ]
  node [
    id 2110
    label "zrelatywizowanie"
  ]
  node [
    id 2111
    label "podporz&#261;dkowanie"
  ]
  node [
    id 2112
    label "niesamodzielno&#347;&#263;"
  ]
  node [
    id 2113
    label "status"
  ]
  node [
    id 2114
    label "relatywizowa&#263;"
  ]
  node [
    id 2115
    label "zwi&#261;zek"
  ]
  node [
    id 2116
    label "relatywizowanie"
  ]
  node [
    id 2117
    label "zwi&#261;zanie"
  ]
  node [
    id 2118
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 2119
    label "wi&#261;zanie"
  ]
  node [
    id 2120
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 2121
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 2122
    label "bratnia_dusza"
  ]
  node [
    id 2123
    label "marriage"
  ]
  node [
    id 2124
    label "marketing_afiliacyjny"
  ]
  node [
    id 2125
    label "integer"
  ]
  node [
    id 2126
    label "liczba"
  ]
  node [
    id 2127
    label "zlewanie_si&#281;"
  ]
  node [
    id 2128
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 2129
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 2130
    label "pe&#322;ny"
  ]
  node [
    id 2131
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 2132
    label "grupa_dyskusyjna"
  ]
  node [
    id 2133
    label "zmieszczenie"
  ]
  node [
    id 2134
    label "umawianie_si&#281;"
  ]
  node [
    id 2135
    label "zapoznanie"
  ]
  node [
    id 2136
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 2137
    label "zapoznanie_si&#281;"
  ]
  node [
    id 2138
    label "zawieranie"
  ]
  node [
    id 2139
    label "ustalenie"
  ]
  node [
    id 2140
    label "inclusion"
  ]
  node [
    id 2141
    label "uchwalenie"
  ]
  node [
    id 2142
    label "North_Atlantic_Treaty_Organization"
  ]
  node [
    id 2143
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 2144
    label "sta&#263;_si&#281;"
  ]
  node [
    id 2145
    label "raptowny"
  ]
  node [
    id 2146
    label "insert"
  ]
  node [
    id 2147
    label "pozna&#263;"
  ]
  node [
    id 2148
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 2149
    label "boil"
  ]
  node [
    id 2150
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 2151
    label "zamkn&#261;&#263;"
  ]
  node [
    id 2152
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 2153
    label "ustali&#263;"
  ]
  node [
    id 2154
    label "admit"
  ]
  node [
    id 2155
    label "wezbra&#263;"
  ]
  node [
    id 2156
    label "embrace"
  ]
  node [
    id 2157
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 2158
    label "b&#322;&#281;kitne_he&#322;my"
  ]
  node [
    id 2159
    label "misja_weryfikacyjna"
  ]
  node [
    id 2160
    label "WIPO"
  ]
  node [
    id 2161
    label "United_Nations"
  ]
  node [
    id 2162
    label "nastawi&#263;"
  ]
  node [
    id 2163
    label "sprawi&#263;"
  ]
  node [
    id 2164
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 2165
    label "transfer"
  ]
  node [
    id 2166
    label "shift"
  ]
  node [
    id 2167
    label "counterchange"
  ]
  node [
    id 2168
    label "przebudowa&#263;"
  ]
  node [
    id 2169
    label "relaxation"
  ]
  node [
    id 2170
    label "os&#322;abienie"
  ]
  node [
    id 2171
    label "oswobodzenie"
  ]
  node [
    id 2172
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 2173
    label "zdezorganizowanie"
  ]
  node [
    id 2174
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 2175
    label "tajemnica"
  ]
  node [
    id 2176
    label "zdyscyplinowanie"
  ]
  node [
    id 2177
    label "post&#261;pienie"
  ]
  node [
    id 2178
    label "post"
  ]
  node [
    id 2179
    label "behawior"
  ]
  node [
    id 2180
    label "dieta"
  ]
  node [
    id 2181
    label "etolog"
  ]
  node [
    id 2182
    label "oswobodzi&#263;"
  ]
  node [
    id 2183
    label "os&#322;abi&#263;"
  ]
  node [
    id 2184
    label "disengage"
  ]
  node [
    id 2185
    label "zdezorganizowa&#263;"
  ]
  node [
    id 2186
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 2187
    label "naukowiec"
  ]
  node [
    id 2188
    label "tkanka"
  ]
  node [
    id 2189
    label "jednostka_organizacyjna"
  ]
  node [
    id 2190
    label "tw&#243;r"
  ]
  node [
    id 2191
    label "organogeneza"
  ]
  node [
    id 2192
    label "zesp&#243;&#322;"
  ]
  node [
    id 2193
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 2194
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 2195
    label "dekortykacja"
  ]
  node [
    id 2196
    label "Izba_Konsyliarska"
  ]
  node [
    id 2197
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 2198
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 2199
    label "stomia"
  ]
  node [
    id 2200
    label "okolica"
  ]
  node [
    id 2201
    label "Komitet_Region&#243;w"
  ]
  node [
    id 2202
    label "subsystem"
  ]
  node [
    id 2203
    label "ko&#322;o"
  ]
  node [
    id 2204
    label "granica"
  ]
  node [
    id 2205
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 2206
    label "suport"
  ]
  node [
    id 2207
    label "prosta"
  ]
  node [
    id 2208
    label "o&#347;rodek"
  ]
  node [
    id 2209
    label "ekshumowanie"
  ]
  node [
    id 2210
    label "odwadnia&#263;"
  ]
  node [
    id 2211
    label "zabalsamowanie"
  ]
  node [
    id 2212
    label "odwodni&#263;"
  ]
  node [
    id 2213
    label "sk&#243;ra"
  ]
  node [
    id 2214
    label "staw"
  ]
  node [
    id 2215
    label "ow&#322;osienie"
  ]
  node [
    id 2216
    label "zabalsamowa&#263;"
  ]
  node [
    id 2217
    label "unerwienie"
  ]
  node [
    id 2218
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 2219
    label "kremacja"
  ]
  node [
    id 2220
    label "biorytm"
  ]
  node [
    id 2221
    label "sekcja"
  ]
  node [
    id 2222
    label "otworzy&#263;"
  ]
  node [
    id 2223
    label "otwiera&#263;"
  ]
  node [
    id 2224
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 2225
    label "otworzenie"
  ]
  node [
    id 2226
    label "materia"
  ]
  node [
    id 2227
    label "otwieranie"
  ]
  node [
    id 2228
    label "ty&#322;"
  ]
  node [
    id 2229
    label "szkielet"
  ]
  node [
    id 2230
    label "tanatoplastyk"
  ]
  node [
    id 2231
    label "odwadnianie"
  ]
  node [
    id 2232
    label "odwodnienie"
  ]
  node [
    id 2233
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 2234
    label "nieumar&#322;y"
  ]
  node [
    id 2235
    label "pochowa&#263;"
  ]
  node [
    id 2236
    label "balsamowa&#263;"
  ]
  node [
    id 2237
    label "tanatoplastyka"
  ]
  node [
    id 2238
    label "temperatura"
  ]
  node [
    id 2239
    label "ekshumowa&#263;"
  ]
  node [
    id 2240
    label "balsamowanie"
  ]
  node [
    id 2241
    label "prz&#243;d"
  ]
  node [
    id 2242
    label "l&#281;d&#378;wie"
  ]
  node [
    id 2243
    label "cz&#322;onek"
  ]
  node [
    id 2244
    label "pogrzeb"
  ]
  node [
    id 2245
    label "constellation"
  ]
  node [
    id 2246
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 2247
    label "Ptak_Rajski"
  ]
  node [
    id 2248
    label "W&#281;&#380;ownik"
  ]
  node [
    id 2249
    label "Panna"
  ]
  node [
    id 2250
    label "W&#261;&#380;"
  ]
  node [
    id 2251
    label "blokada"
  ]
  node [
    id 2252
    label "hurtownia"
  ]
  node [
    id 2253
    label "pole"
  ]
  node [
    id 2254
    label "basic"
  ]
  node [
    id 2255
    label "sk&#322;adnik"
  ]
  node [
    id 2256
    label "sklep"
  ]
  node [
    id 2257
    label "obr&#243;bka"
  ]
  node [
    id 2258
    label "constitution"
  ]
  node [
    id 2259
    label "fabryka"
  ]
  node [
    id 2260
    label "&#347;wiat&#322;o"
  ]
  node [
    id 2261
    label "syf"
  ]
  node [
    id 2262
    label "rank_and_file"
  ]
  node [
    id 2263
    label "tabulacja"
  ]
  node [
    id 2264
    label "nap&#281;dny"
  ]
  node [
    id 2265
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 2266
    label "krzew"
  ]
  node [
    id 2267
    label "delfinidyna"
  ]
  node [
    id 2268
    label "pi&#380;maczkowate"
  ]
  node [
    id 2269
    label "ki&#347;&#263;"
  ]
  node [
    id 2270
    label "hy&#263;ka"
  ]
  node [
    id 2271
    label "pestkowiec"
  ]
  node [
    id 2272
    label "kwiat"
  ]
  node [
    id 2273
    label "ro&#347;lina"
  ]
  node [
    id 2274
    label "owoc"
  ]
  node [
    id 2275
    label "oliwkowate"
  ]
  node [
    id 2276
    label "lilac"
  ]
  node [
    id 2277
    label "kostka"
  ]
  node [
    id 2278
    label "kita"
  ]
  node [
    id 2279
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 2280
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 2281
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 2282
    label "powerball"
  ]
  node [
    id 2283
    label "&#380;ubr"
  ]
  node [
    id 2284
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 2285
    label "p&#281;k"
  ]
  node [
    id 2286
    label "r&#281;ka"
  ]
  node [
    id 2287
    label "ogon"
  ]
  node [
    id 2288
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 2289
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 2290
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 2291
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 2292
    label "flakon"
  ]
  node [
    id 2293
    label "przykoronek"
  ]
  node [
    id 2294
    label "kielich"
  ]
  node [
    id 2295
    label "dno_kwiatowe"
  ]
  node [
    id 2296
    label "organ_ro&#347;linny"
  ]
  node [
    id 2297
    label "warga"
  ]
  node [
    id 2298
    label "korona"
  ]
  node [
    id 2299
    label "rurka"
  ]
  node [
    id 2300
    label "ozdoba"
  ]
  node [
    id 2301
    label "&#322;yko"
  ]
  node [
    id 2302
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 2303
    label "karczowa&#263;"
  ]
  node [
    id 2304
    label "wykarczowanie"
  ]
  node [
    id 2305
    label "skupina"
  ]
  node [
    id 2306
    label "wykarczowa&#263;"
  ]
  node [
    id 2307
    label "karczowanie"
  ]
  node [
    id 2308
    label "fanerofit"
  ]
  node [
    id 2309
    label "zbiorowisko"
  ]
  node [
    id 2310
    label "ro&#347;liny"
  ]
  node [
    id 2311
    label "p&#281;d"
  ]
  node [
    id 2312
    label "wegetowanie"
  ]
  node [
    id 2313
    label "zadziorek"
  ]
  node [
    id 2314
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 2315
    label "do&#322;owa&#263;"
  ]
  node [
    id 2316
    label "wegetacja"
  ]
  node [
    id 2317
    label "strzyc"
  ]
  node [
    id 2318
    label "w&#322;&#243;kno"
  ]
  node [
    id 2319
    label "g&#322;uszenie"
  ]
  node [
    id 2320
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 2321
    label "fitotron"
  ]
  node [
    id 2322
    label "bulwka"
  ]
  node [
    id 2323
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 2324
    label "odn&#243;&#380;ka"
  ]
  node [
    id 2325
    label "epiderma"
  ]
  node [
    id 2326
    label "gumoza"
  ]
  node [
    id 2327
    label "strzy&#380;enie"
  ]
  node [
    id 2328
    label "wypotnik"
  ]
  node [
    id 2329
    label "flawonoid"
  ]
  node [
    id 2330
    label "wyro&#347;le"
  ]
  node [
    id 2331
    label "do&#322;owanie"
  ]
  node [
    id 2332
    label "g&#322;uszy&#263;"
  ]
  node [
    id 2333
    label "pora&#380;a&#263;"
  ]
  node [
    id 2334
    label "fitocenoza"
  ]
  node [
    id 2335
    label "fotoautotrof"
  ]
  node [
    id 2336
    label "nieuleczalnie_chory"
  ]
  node [
    id 2337
    label "wegetowa&#263;"
  ]
  node [
    id 2338
    label "pochewka"
  ]
  node [
    id 2339
    label "sok"
  ]
  node [
    id 2340
    label "system_korzeniowy"
  ]
  node [
    id 2341
    label "zawi&#261;zek"
  ]
  node [
    id 2342
    label "pestka"
  ]
  node [
    id 2343
    label "mi&#261;&#380;sz"
  ]
  node [
    id 2344
    label "frukt"
  ]
  node [
    id 2345
    label "drylowanie"
  ]
  node [
    id 2346
    label "produkt"
  ]
  node [
    id 2347
    label "owocnia"
  ]
  node [
    id 2348
    label "fruktoza"
  ]
  node [
    id 2349
    label "obiekt"
  ]
  node [
    id 2350
    label "gniazdo_nasienne"
  ]
  node [
    id 2351
    label "glukoza"
  ]
  node [
    id 2352
    label "antocyjanidyn"
  ]
  node [
    id 2353
    label "szczeciowce"
  ]
  node [
    id 2354
    label "jasnotowce"
  ]
  node [
    id 2355
    label "Oleaceae"
  ]
  node [
    id 2356
    label "wielkopolski"
  ]
  node [
    id 2357
    label "bez_czarny"
  ]
  node [
    id 2358
    label "wytwarza&#263;"
  ]
  node [
    id 2359
    label "take"
  ]
  node [
    id 2360
    label "godzinnik"
  ]
  node [
    id 2361
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 2362
    label "wahad&#322;o"
  ]
  node [
    id 2363
    label "kurant"
  ]
  node [
    id 2364
    label "cyferblat"
  ]
  node [
    id 2365
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 2366
    label "nabicie"
  ]
  node [
    id 2367
    label "werk"
  ]
  node [
    id 2368
    label "czasomierz"
  ]
  node [
    id 2369
    label "tyka&#263;"
  ]
  node [
    id 2370
    label "tykn&#261;&#263;"
  ]
  node [
    id 2371
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 2372
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 2373
    label "ogrom"
  ]
  node [
    id 2374
    label "iskrzy&#263;"
  ]
  node [
    id 2375
    label "d&#322;awi&#263;"
  ]
  node [
    id 2376
    label "ostygn&#261;&#263;"
  ]
  node [
    id 2377
    label "stygn&#261;&#263;"
  ]
  node [
    id 2378
    label "wpa&#347;&#263;"
  ]
  node [
    id 2379
    label "afekt"
  ]
  node [
    id 2380
    label "wpada&#263;"
  ]
  node [
    id 2381
    label "niezb&#281;dnik"
  ]
  node [
    id 2382
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 2383
    label "tylec"
  ]
  node [
    id 2384
    label "zu&#380;ycie"
  ]
  node [
    id 2385
    label "powo&#322;anie"
  ]
  node [
    id 2386
    label "powybieranie"
  ]
  node [
    id 2387
    label "sie&#263;_rybacka"
  ]
  node [
    id 2388
    label "wyj&#281;cie"
  ]
  node [
    id 2389
    label "optowanie"
  ]
  node [
    id 2390
    label "wyjmowa&#263;"
  ]
  node [
    id 2391
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 2392
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 2393
    label "ustala&#263;"
  ]
  node [
    id 2394
    label "powo&#322;a&#263;"
  ]
  node [
    id 2395
    label "zu&#380;y&#263;"
  ]
  node [
    id 2396
    label "wyj&#261;&#263;"
  ]
  node [
    id 2397
    label "distill"
  ]
  node [
    id 2398
    label "pick"
  ]
  node [
    id 2399
    label "powo&#322;ywanie"
  ]
  node [
    id 2400
    label "election"
  ]
  node [
    id 2401
    label "wyiskanie"
  ]
  node [
    id 2402
    label "wyjmowanie"
  ]
  node [
    id 2403
    label "iskanie"
  ]
  node [
    id 2404
    label "chosen"
  ]
  node [
    id 2405
    label "zu&#380;ywanie"
  ]
  node [
    id 2406
    label "okre&#347;lanie"
  ]
  node [
    id 2407
    label "og&#243;lnie"
  ]
  node [
    id 2408
    label "pryncypalnie"
  ]
  node [
    id 2409
    label "g&#322;&#243;wnie"
  ]
  node [
    id 2410
    label "zasadniczy"
  ]
  node [
    id 2411
    label "surowo"
  ]
  node [
    id 2412
    label "powa&#380;nie"
  ]
  node [
    id 2413
    label "surowy"
  ]
  node [
    id 2414
    label "srodze"
  ]
  node [
    id 2415
    label "sternly"
  ]
  node [
    id 2416
    label "gro&#378;ny"
  ]
  node [
    id 2417
    label "oszcz&#281;dnie"
  ]
  node [
    id 2418
    label "twardo"
  ]
  node [
    id 2419
    label "surowie"
  ]
  node [
    id 2420
    label "&#322;&#261;cznie"
  ]
  node [
    id 2421
    label "nadrz&#281;dnie"
  ]
  node [
    id 2422
    label "og&#243;lny"
  ]
  node [
    id 2423
    label "posp&#243;lnie"
  ]
  node [
    id 2424
    label "zbiorowo"
  ]
  node [
    id 2425
    label "generalny"
  ]
  node [
    id 2426
    label "g&#322;&#243;wny"
  ]
  node [
    id 2427
    label "przyspieszenie"
  ]
  node [
    id 2428
    label "percussion"
  ]
  node [
    id 2429
    label "measurement"
  ]
  node [
    id 2430
    label "przemieszczenie"
  ]
  node [
    id 2431
    label "wykonywa&#263;"
  ]
  node [
    id 2432
    label "transact"
  ]
  node [
    id 2433
    label "string"
  ]
  node [
    id 2434
    label "tworzy&#263;"
  ]
  node [
    id 2435
    label "pope&#322;nia&#263;"
  ]
  node [
    id 2436
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 2437
    label "consist"
  ]
  node [
    id 2438
    label "stanowi&#263;"
  ]
  node [
    id 2439
    label "muzyka"
  ]
  node [
    id 2440
    label "create"
  ]
  node [
    id 2441
    label "rola"
  ]
  node [
    id 2442
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 2443
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 2444
    label "motywowa&#263;"
  ]
  node [
    id 2445
    label "act"
  ]
  node [
    id 2446
    label "organizowa&#263;"
  ]
  node [
    id 2447
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 2448
    label "czyni&#263;"
  ]
  node [
    id 2449
    label "stylizowa&#263;"
  ]
  node [
    id 2450
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 2451
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 2452
    label "peddle"
  ]
  node [
    id 2453
    label "wydala&#263;"
  ]
  node [
    id 2454
    label "tentegowa&#263;"
  ]
  node [
    id 2455
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 2456
    label "urz&#261;dza&#263;"
  ]
  node [
    id 2457
    label "oszukiwa&#263;"
  ]
  node [
    id 2458
    label "ukazywa&#263;"
  ]
  node [
    id 2459
    label "przerabia&#263;"
  ]
  node [
    id 2460
    label "post&#281;powa&#263;"
  ]
  node [
    id 2461
    label "aid"
  ]
  node [
    id 2462
    label "concur"
  ]
  node [
    id 2463
    label "sprzyja&#263;"
  ]
  node [
    id 2464
    label "skutkowa&#263;"
  ]
  node [
    id 2465
    label "digest"
  ]
  node [
    id 2466
    label "back"
  ]
  node [
    id 2467
    label "rysunek"
  ]
  node [
    id 2468
    label "drabina_analgetyczna"
  ]
  node [
    id 2469
    label "radiation_pattern"
  ]
  node [
    id 2470
    label "pomys&#322;"
  ]
  node [
    id 2471
    label "exemplar"
  ]
  node [
    id 2472
    label "kreska"
  ]
  node [
    id 2473
    label "picture"
  ]
  node [
    id 2474
    label "teka"
  ]
  node [
    id 2475
    label "photograph"
  ]
  node [
    id 2476
    label "ilustracja"
  ]
  node [
    id 2477
    label "grafika"
  ]
  node [
    id 2478
    label "plastyka"
  ]
  node [
    id 2479
    label "ukra&#347;&#263;"
  ]
  node [
    id 2480
    label "ukradzenie"
  ]
  node [
    id 2481
    label "ukaza&#263;"
  ]
  node [
    id 2482
    label "przedstawienie"
  ]
  node [
    id 2483
    label "pokaza&#263;"
  ]
  node [
    id 2484
    label "poda&#263;"
  ]
  node [
    id 2485
    label "zapozna&#263;"
  ]
  node [
    id 2486
    label "represent"
  ]
  node [
    id 2487
    label "zaproponowa&#263;"
  ]
  node [
    id 2488
    label "zademonstrowa&#263;"
  ]
  node [
    id 2489
    label "typify"
  ]
  node [
    id 2490
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 2491
    label "opisa&#263;"
  ]
  node [
    id 2492
    label "tenis"
  ]
  node [
    id 2493
    label "supply"
  ]
  node [
    id 2494
    label "ustawi&#263;"
  ]
  node [
    id 2495
    label "siatk&#243;wka"
  ]
  node [
    id 2496
    label "zagra&#263;"
  ]
  node [
    id 2497
    label "jedzenie"
  ]
  node [
    id 2498
    label "poinformowa&#263;"
  ]
  node [
    id 2499
    label "introduce"
  ]
  node [
    id 2500
    label "nafaszerowa&#263;"
  ]
  node [
    id 2501
    label "zaserwowa&#263;"
  ]
  node [
    id 2502
    label "udowodni&#263;"
  ]
  node [
    id 2503
    label "przeszkoli&#263;"
  ]
  node [
    id 2504
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 2505
    label "indicate"
  ]
  node [
    id 2506
    label "zach&#281;ci&#263;"
  ]
  node [
    id 2507
    label "volunteer"
  ]
  node [
    id 2508
    label "kandydatura"
  ]
  node [
    id 2509
    label "announce"
  ]
  node [
    id 2510
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 2511
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 2512
    label "perform"
  ]
  node [
    id 2513
    label "wyj&#347;&#263;"
  ]
  node [
    id 2514
    label "zrezygnowa&#263;"
  ]
  node [
    id 2515
    label "odst&#261;pi&#263;"
  ]
  node [
    id 2516
    label "nak&#322;oni&#263;"
  ]
  node [
    id 2517
    label "appear"
  ]
  node [
    id 2518
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 2519
    label "zacz&#261;&#263;"
  ]
  node [
    id 2520
    label "happen"
  ]
  node [
    id 2521
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 2522
    label "unwrap"
  ]
  node [
    id 2523
    label "relate"
  ]
  node [
    id 2524
    label "delineate"
  ]
  node [
    id 2525
    label "attest"
  ]
  node [
    id 2526
    label "obznajomi&#263;"
  ]
  node [
    id 2527
    label "teach"
  ]
  node [
    id 2528
    label "pr&#243;bowanie"
  ]
  node [
    id 2529
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 2530
    label "zademonstrowanie"
  ]
  node [
    id 2531
    label "report"
  ]
  node [
    id 2532
    label "obgadanie"
  ]
  node [
    id 2533
    label "realizacja"
  ]
  node [
    id 2534
    label "scena"
  ]
  node [
    id 2535
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 2536
    label "narration"
  ]
  node [
    id 2537
    label "cyrk"
  ]
  node [
    id 2538
    label "posta&#263;"
  ]
  node [
    id 2539
    label "theatrical_performance"
  ]
  node [
    id 2540
    label "opisanie"
  ]
  node [
    id 2541
    label "malarstwo"
  ]
  node [
    id 2542
    label "scenografia"
  ]
  node [
    id 2543
    label "teatr"
  ]
  node [
    id 2544
    label "ukazanie"
  ]
  node [
    id 2545
    label "pokaz"
  ]
  node [
    id 2546
    label "podanie"
  ]
  node [
    id 2547
    label "ods&#322;ona"
  ]
  node [
    id 2548
    label "exhibit"
  ]
  node [
    id 2549
    label "pokazanie"
  ]
  node [
    id 2550
    label "podstawy"
  ]
  node [
    id 2551
    label "opracowanie"
  ]
  node [
    id 2552
    label "twarz"
  ]
  node [
    id 2553
    label "charakterystyka"
  ]
  node [
    id 2554
    label "m&#322;ot"
  ]
  node [
    id 2555
    label "drzewo"
  ]
  node [
    id 2556
    label "marka"
  ]
  node [
    id 2557
    label "detail"
  ]
  node [
    id 2558
    label "przygotowanie"
  ]
  node [
    id 2559
    label "paper"
  ]
  node [
    id 2560
    label "cera"
  ]
  node [
    id 2561
    label "wielko&#347;&#263;"
  ]
  node [
    id 2562
    label "przedstawiciel"
  ]
  node [
    id 2563
    label "profil"
  ]
  node [
    id 2564
    label "p&#322;e&#263;"
  ]
  node [
    id 2565
    label "zas&#322;ona"
  ]
  node [
    id 2566
    label "p&#243;&#322;profil"
  ]
  node [
    id 2567
    label "policzek"
  ]
  node [
    id 2568
    label "brew"
  ]
  node [
    id 2569
    label "micha"
  ]
  node [
    id 2570
    label "reputacja"
  ]
  node [
    id 2571
    label "wyraz_twarzy"
  ]
  node [
    id 2572
    label "powieka"
  ]
  node [
    id 2573
    label "czo&#322;o"
  ]
  node [
    id 2574
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 2575
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 2576
    label "twarzyczka"
  ]
  node [
    id 2577
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 2578
    label "ucho"
  ]
  node [
    id 2579
    label "usta"
  ]
  node [
    id 2580
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 2581
    label "oko"
  ]
  node [
    id 2582
    label "nos"
  ]
  node [
    id 2583
    label "podbr&#243;dek"
  ]
  node [
    id 2584
    label "liczko"
  ]
  node [
    id 2585
    label "pysk"
  ]
  node [
    id 2586
    label "maskowato&#347;&#263;"
  ]
  node [
    id 2587
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 2588
    label "start"
  ]
  node [
    id 2589
    label "begin"
  ]
  node [
    id 2590
    label "prosecute"
  ]
  node [
    id 2591
    label "lot"
  ]
  node [
    id 2592
    label "rozpocz&#281;cie"
  ]
  node [
    id 2593
    label "uczestnictwo"
  ]
  node [
    id 2594
    label "okno_startowe"
  ]
  node [
    id 2595
    label "blok_startowy"
  ]
  node [
    id 2596
    label "wy&#347;cig"
  ]
  node [
    id 2597
    label "przenikanie"
  ]
  node [
    id 2598
    label "cz&#261;steczka"
  ]
  node [
    id 2599
    label "temperatura_krytyczna"
  ]
  node [
    id 2600
    label "przenika&#263;"
  ]
  node [
    id 2601
    label "smolisty"
  ]
  node [
    id 2602
    label "cytozol"
  ]
  node [
    id 2603
    label "up&#322;ynnianie"
  ]
  node [
    id 2604
    label "roztopienie_si&#281;"
  ]
  node [
    id 2605
    label "roztapianie_si&#281;"
  ]
  node [
    id 2606
    label "up&#322;ynnienie"
  ]
  node [
    id 2607
    label "proces_fizyczny"
  ]
  node [
    id 2608
    label "wymy&#347;lenie"
  ]
  node [
    id 2609
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 2610
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 2611
    label "ulegni&#281;cie"
  ]
  node [
    id 2612
    label "collapse"
  ]
  node [
    id 2613
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 2614
    label "poniesienie"
  ]
  node [
    id 2615
    label "zapach"
  ]
  node [
    id 2616
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 2617
    label "uderzenie"
  ]
  node [
    id 2618
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 2619
    label "rzeka"
  ]
  node [
    id 2620
    label "postrzeganie"
  ]
  node [
    id 2621
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 2622
    label "dostanie_si&#281;"
  ]
  node [
    id 2623
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 2624
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 2625
    label "departure"
  ]
  node [
    id 2626
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 2627
    label "sp&#322;yni&#281;cie"
  ]
  node [
    id 2628
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 2629
    label "approach"
  ]
  node [
    id 2630
    label "nabiega&#263;"
  ]
  node [
    id 2631
    label "carboy"
  ]
  node [
    id 2632
    label "mak&#243;wka"
  ]
  node [
    id 2633
    label "zbiornik"
  ]
  node [
    id 2634
    label "zawarto&#347;&#263;"
  ]
  node [
    id 2635
    label "gar"
  ]
  node [
    id 2636
    label "czaszka"
  ]
  node [
    id 2637
    label "dynia"
  ]
  node [
    id 2638
    label "znoszenie"
  ]
  node [
    id 2639
    label "odprowadzanie"
  ]
  node [
    id 2640
    label "cribbage"
  ]
  node [
    id 2641
    label "zdejmowanie"
  ]
  node [
    id 2642
    label "zmuszanie"
  ]
  node [
    id 2643
    label "przepisywanie"
  ]
  node [
    id 2644
    label "constriction"
  ]
  node [
    id 2645
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 2646
    label "gromadzenie_si&#281;"
  ]
  node [
    id 2647
    label "gromadzenie"
  ]
  node [
    id 2648
    label "kradzenie"
  ]
  node [
    id 2649
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 2650
    label "przebranie"
  ]
  node [
    id 2651
    label "dostawanie_si&#281;"
  ]
  node [
    id 2652
    label "kurczenie"
  ]
  node [
    id 2653
    label "przewi&#261;zywanie"
  ]
  node [
    id 2654
    label "wylewa&#263;"
  ]
  node [
    id 2655
    label "brzmie&#263;"
  ]
  node [
    id 2656
    label "boast"
  ]
  node [
    id 2657
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 2658
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 2659
    label "zabrzmie&#263;"
  ]
  node [
    id 2660
    label "wytoczenie_si&#281;"
  ]
  node [
    id 2661
    label "obrobienie"
  ]
  node [
    id 2662
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 2663
    label "ukszta&#322;towanie"
  ]
  node [
    id 2664
    label "wydostanie"
  ]
  node [
    id 2665
    label "faza_termodynamiczna"
  ]
  node [
    id 2666
    label "roztw&#243;r"
  ]
  node [
    id 2667
    label "niepewny"
  ]
  node [
    id 2668
    label "m&#261;cenie"
  ]
  node [
    id 2669
    label "trudny"
  ]
  node [
    id 2670
    label "niejawny"
  ]
  node [
    id 2671
    label "ci&#281;&#380;ki"
  ]
  node [
    id 2672
    label "ciemny"
  ]
  node [
    id 2673
    label "nieklarowny"
  ]
  node [
    id 2674
    label "niezrozumia&#322;y"
  ]
  node [
    id 2675
    label "zanieczyszczanie"
  ]
  node [
    id 2676
    label "zanieczyszczenie"
  ]
  node [
    id 2677
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 2678
    label "uleganie"
  ]
  node [
    id 2679
    label "odwiedzanie"
  ]
  node [
    id 2680
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 2681
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 2682
    label "wymy&#347;lanie"
  ]
  node [
    id 2683
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 2684
    label "ingress"
  ]
  node [
    id 2685
    label "wp&#322;ywanie"
  ]
  node [
    id 2686
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 2687
    label "overlap"
  ]
  node [
    id 2688
    label "wkl&#281;sanie"
  ]
  node [
    id 2689
    label "zanikn&#261;&#263;"
  ]
  node [
    id 2690
    label "drip"
  ]
  node [
    id 2691
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 2692
    label "odej&#347;&#263;"
  ]
  node [
    id 2693
    label "wy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 2694
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 2695
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 2696
    label "cruise"
  ]
  node [
    id 2697
    label "opu&#347;ci&#263;"
  ]
  node [
    id 2698
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 2699
    label "remove"
  ]
  node [
    id 2700
    label "wyp&#322;ywanie"
  ]
  node [
    id 2701
    label "przenoszenie_si&#281;"
  ]
  node [
    id 2702
    label "oddalanie_si&#281;"
  ]
  node [
    id 2703
    label "wy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 2704
    label "odwlekanie"
  ]
  node [
    id 2705
    label "odchodzenie"
  ]
  node [
    id 2706
    label "przesuwanie_si&#281;"
  ]
  node [
    id 2707
    label "zanikanie"
  ]
  node [
    id 2708
    label "emergence"
  ]
  node [
    id 2709
    label "opuszczanie"
  ]
  node [
    id 2710
    label "s&#322;abni&#281;cie"
  ]
  node [
    id 2711
    label "odprowadza&#263;"
  ]
  node [
    id 2712
    label "przewi&#261;zywa&#263;"
  ]
  node [
    id 2713
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 2714
    label "stiffen"
  ]
  node [
    id 2715
    label "pozyskiwa&#263;"
  ]
  node [
    id 2716
    label "znosi&#263;"
  ]
  node [
    id 2717
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 2718
    label "zmusza&#263;"
  ]
  node [
    id 2719
    label "zdejmowa&#263;"
  ]
  node [
    id 2720
    label "kra&#347;&#263;"
  ]
  node [
    id 2721
    label "przepisywa&#263;"
  ]
  node [
    id 2722
    label "kurczy&#263;"
  ]
  node [
    id 2723
    label "clamp"
  ]
  node [
    id 2724
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 2725
    label "constant"
  ]
  node [
    id 2726
    label "r&#243;&#380;niczka"
  ]
  node [
    id 2727
    label "&#347;rodowisko"
  ]
  node [
    id 2728
    label "szambo"
  ]
  node [
    id 2729
    label "aspo&#322;eczny"
  ]
  node [
    id 2730
    label "component"
  ]
  node [
    id 2731
    label "szkodnik"
  ]
  node [
    id 2732
    label "gangsterski"
  ]
  node [
    id 2733
    label "underworld"
  ]
  node [
    id 2734
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 2735
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 2736
    label "warunek_lokalowy"
  ]
  node [
    id 2737
    label "rzadko&#347;&#263;"
  ]
  node [
    id 2738
    label "zaleta"
  ]
  node [
    id 2739
    label "measure"
  ]
  node [
    id 2740
    label "znaczenie"
  ]
  node [
    id 2741
    label "opinia"
  ]
  node [
    id 2742
    label "dymensja"
  ]
  node [
    id 2743
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 2744
    label "potencja"
  ]
  node [
    id 2745
    label "property"
  ]
  node [
    id 2746
    label "celerity"
  ]
  node [
    id 2747
    label "tempo"
  ]
  node [
    id 2748
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 2749
    label "szachy"
  ]
  node [
    id 2750
    label "rytm"
  ]
  node [
    id 2751
    label "widen"
  ]
  node [
    id 2752
    label "develop"
  ]
  node [
    id 2753
    label "perpetrate"
  ]
  node [
    id 2754
    label "expand"
  ]
  node [
    id 2755
    label "zdobywa&#263;_podst&#281;pem"
  ]
  node [
    id 2756
    label "prostowa&#263;"
  ]
  node [
    id 2757
    label "ocala&#263;"
  ]
  node [
    id 2758
    label "wy&#322;udza&#263;"
  ]
  node [
    id 2759
    label "przypomina&#263;"
  ]
  node [
    id 2760
    label "&#347;piewa&#263;"
  ]
  node [
    id 2761
    label "zabiera&#263;"
  ]
  node [
    id 2762
    label "wydostawa&#263;"
  ]
  node [
    id 2763
    label "rozci&#261;ga&#263;"
  ]
  node [
    id 2764
    label "obrysowywa&#263;"
  ]
  node [
    id 2765
    label "zarabia&#263;"
  ]
  node [
    id 2766
    label "nak&#322;ania&#263;"
  ]
  node [
    id 2767
    label "mo&#380;liwy"
  ]
  node [
    id 2768
    label "zno&#347;nie"
  ]
  node [
    id 2769
    label "urealnianie"
  ]
  node [
    id 2770
    label "mo&#380;ebny"
  ]
  node [
    id 2771
    label "umo&#380;liwianie"
  ]
  node [
    id 2772
    label "zno&#347;ny"
  ]
  node [
    id 2773
    label "umo&#380;liwienie"
  ]
  node [
    id 2774
    label "urealnienie"
  ]
  node [
    id 2775
    label "dost&#281;pny"
  ]
  node [
    id 2776
    label "wzgl&#281;dnie"
  ]
  node [
    id 2777
    label "nie&#378;le"
  ]
  node [
    id 2778
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 2779
    label "zobo"
  ]
  node [
    id 2780
    label "yakalo"
  ]
  node [
    id 2781
    label "byd&#322;o"
  ]
  node [
    id 2782
    label "dzo"
  ]
  node [
    id 2783
    label "kr&#281;torogie"
  ]
  node [
    id 2784
    label "g&#322;owa"
  ]
  node [
    id 2785
    label "czochrad&#322;o"
  ]
  node [
    id 2786
    label "posp&#243;lstwo"
  ]
  node [
    id 2787
    label "kraal"
  ]
  node [
    id 2788
    label "livestock"
  ]
  node [
    id 2789
    label "prze&#380;uwacz"
  ]
  node [
    id 2790
    label "bizon"
  ]
  node [
    id 2791
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 2792
    label "zebu"
  ]
  node [
    id 2793
    label "byd&#322;o_domowe"
  ]
  node [
    id 2794
    label "w_chuj"
  ]
  node [
    id 2795
    label "niedaleki"
  ]
  node [
    id 2796
    label "blisko"
  ]
  node [
    id 2797
    label "zwi&#261;zany"
  ]
  node [
    id 2798
    label "przesz&#322;y"
  ]
  node [
    id 2799
    label "silny"
  ]
  node [
    id 2800
    label "zbli&#380;enie"
  ]
  node [
    id 2801
    label "oddalony"
  ]
  node [
    id 2802
    label "dok&#322;adny"
  ]
  node [
    id 2803
    label "nieodleg&#322;y"
  ]
  node [
    id 2804
    label "przysz&#322;y"
  ]
  node [
    id 2805
    label "ma&#322;y"
  ]
  node [
    id 2806
    label "zrewaluowa&#263;"
  ]
  node [
    id 2807
    label "zmienna"
  ]
  node [
    id 2808
    label "wskazywanie"
  ]
  node [
    id 2809
    label "rewaluowanie"
  ]
  node [
    id 2810
    label "cel"
  ]
  node [
    id 2811
    label "wskazywa&#263;"
  ]
  node [
    id 2812
    label "korzy&#347;&#263;"
  ]
  node [
    id 2813
    label "worth"
  ]
  node [
    id 2814
    label "zrewaluowanie"
  ]
  node [
    id 2815
    label "rewaluowa&#263;"
  ]
  node [
    id 2816
    label "wabik"
  ]
  node [
    id 2817
    label "strona"
  ]
  node [
    id 2818
    label "skumanie"
  ]
  node [
    id 2819
    label "orientacja"
  ]
  node [
    id 2820
    label "zorientowanie"
  ]
  node [
    id 2821
    label "teoria"
  ]
  node [
    id 2822
    label "forma"
  ]
  node [
    id 2823
    label "przem&#243;wienie"
  ]
  node [
    id 2824
    label "kartka"
  ]
  node [
    id 2825
    label "logowanie"
  ]
  node [
    id 2826
    label "plik"
  ]
  node [
    id 2827
    label "adres_internetowy"
  ]
  node [
    id 2828
    label "serwis_internetowy"
  ]
  node [
    id 2829
    label "skr&#281;canie"
  ]
  node [
    id 2830
    label "skr&#281;ca&#263;"
  ]
  node [
    id 2831
    label "orientowanie"
  ]
  node [
    id 2832
    label "skr&#281;ci&#263;"
  ]
  node [
    id 2833
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 2834
    label "layout"
  ]
  node [
    id 2835
    label "zorientowa&#263;"
  ]
  node [
    id 2836
    label "podmiot"
  ]
  node [
    id 2837
    label "g&#243;ra"
  ]
  node [
    id 2838
    label "orientowa&#263;"
  ]
  node [
    id 2839
    label "voice"
  ]
  node [
    id 2840
    label "internet"
  ]
  node [
    id 2841
    label "skr&#281;cenie"
  ]
  node [
    id 2842
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 2843
    label "thing"
  ]
  node [
    id 2844
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 2845
    label "circumference"
  ]
  node [
    id 2846
    label "odzie&#380;"
  ]
  node [
    id 2847
    label "variable"
  ]
  node [
    id 2848
    label "dobro"
  ]
  node [
    id 2849
    label "podniesienie"
  ]
  node [
    id 2850
    label "przewarto&#347;ciowywanie"
  ]
  node [
    id 2851
    label "podnoszenie"
  ]
  node [
    id 2852
    label "warto&#347;ciowy"
  ]
  node [
    id 2853
    label "appreciate"
  ]
  node [
    id 2854
    label "podnosi&#263;"
  ]
  node [
    id 2855
    label "podnie&#347;&#263;"
  ]
  node [
    id 2856
    label "magnes"
  ]
  node [
    id 2857
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 2858
    label "podkre&#347;la&#263;"
  ]
  node [
    id 2859
    label "podawa&#263;"
  ]
  node [
    id 2860
    label "wyraz"
  ]
  node [
    id 2861
    label "pokazywa&#263;"
  ]
  node [
    id 2862
    label "signify"
  ]
  node [
    id 2863
    label "command"
  ]
  node [
    id 2864
    label "pokierowanie"
  ]
  node [
    id 2865
    label "wywiedzenie"
  ]
  node [
    id 2866
    label "podkre&#347;lanie"
  ]
  node [
    id 2867
    label "pokazywanie"
  ]
  node [
    id 2868
    label "show"
  ]
  node [
    id 2869
    label "assignment"
  ]
  node [
    id 2870
    label "indication"
  ]
  node [
    id 2871
    label "podawanie"
  ]
  node [
    id 2872
    label "taniec_ludowy"
  ]
  node [
    id 2873
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 2874
    label "melodia"
  ]
  node [
    id 2875
    label "taniec"
  ]
  node [
    id 2876
    label "devotion"
  ]
  node [
    id 2877
    label "powa&#380;anie"
  ]
  node [
    id 2878
    label "dba&#322;o&#347;&#263;"
  ]
  node [
    id 2879
    label "obrz&#281;d"
  ]
  node [
    id 2880
    label "karnet"
  ]
  node [
    id 2881
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 2882
    label "parkiet"
  ]
  node [
    id 2883
    label "choreologia"
  ]
  node [
    id 2884
    label "krok_taneczny"
  ]
  node [
    id 2885
    label "zanucenie"
  ]
  node [
    id 2886
    label "nuta"
  ]
  node [
    id 2887
    label "zakosztowa&#263;"
  ]
  node [
    id 2888
    label "zanuci&#263;"
  ]
  node [
    id 2889
    label "oskoma"
  ]
  node [
    id 2890
    label "melika"
  ]
  node [
    id 2891
    label "nucenie"
  ]
  node [
    id 2892
    label "nuci&#263;"
  ]
  node [
    id 2893
    label "istota"
  ]
  node [
    id 2894
    label "taste"
  ]
  node [
    id 2895
    label "inclination"
  ]
  node [
    id 2896
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 2897
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 2898
    label "najem"
  ]
  node [
    id 2899
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 2900
    label "zak&#322;ad"
  ]
  node [
    id 2901
    label "stosunek_pracy"
  ]
  node [
    id 2902
    label "benedykty&#324;ski"
  ]
  node [
    id 2903
    label "poda&#380;_pracy"
  ]
  node [
    id 2904
    label "tyrka"
  ]
  node [
    id 2905
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 2906
    label "zaw&#243;d"
  ]
  node [
    id 2907
    label "tynkarski"
  ]
  node [
    id 2908
    label "czynnik_produkcji"
  ]
  node [
    id 2909
    label "zobowi&#261;zanie"
  ]
  node [
    id 2910
    label "kierownictwo"
  ]
  node [
    id 2911
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 2912
    label "plac"
  ]
  node [
    id 2913
    label "location"
  ]
  node [
    id 2914
    label "uwaga"
  ]
  node [
    id 2915
    label "chwila"
  ]
  node [
    id 2916
    label "rz&#261;d"
  ]
  node [
    id 2917
    label "stosunek_prawny"
  ]
  node [
    id 2918
    label "oblig"
  ]
  node [
    id 2919
    label "uregulowa&#263;"
  ]
  node [
    id 2920
    label "occupation"
  ]
  node [
    id 2921
    label "duty"
  ]
  node [
    id 2922
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 2923
    label "obowi&#261;zek"
  ]
  node [
    id 2924
    label "statement"
  ]
  node [
    id 2925
    label "miejsce_pracy"
  ]
  node [
    id 2926
    label "&#321;ubianka"
  ]
  node [
    id 2927
    label "dzia&#322;_personalny"
  ]
  node [
    id 2928
    label "Kreml"
  ]
  node [
    id 2929
    label "Bia&#322;y_Dom"
  ]
  node [
    id 2930
    label "budynek"
  ]
  node [
    id 2931
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 2932
    label "sadowisko"
  ]
  node [
    id 2933
    label "instytucja"
  ]
  node [
    id 2934
    label "wyko&#324;czenie"
  ]
  node [
    id 2935
    label "firma"
  ]
  node [
    id 2936
    label "company"
  ]
  node [
    id 2937
    label "instytut"
  ]
  node [
    id 2938
    label "cierpliwy"
  ]
  node [
    id 2939
    label "mozolny"
  ]
  node [
    id 2940
    label "wytrwa&#322;y"
  ]
  node [
    id 2941
    label "benedykty&#324;sko"
  ]
  node [
    id 2942
    label "typowy"
  ]
  node [
    id 2943
    label "po_benedykty&#324;sku"
  ]
  node [
    id 2944
    label "przepracowanie_si&#281;"
  ]
  node [
    id 2945
    label "zarz&#261;dzanie"
  ]
  node [
    id 2946
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 2947
    label "podlizanie_si&#281;"
  ]
  node [
    id 2948
    label "dopracowanie"
  ]
  node [
    id 2949
    label "podlizywanie_si&#281;"
  ]
  node [
    id 2950
    label "d&#261;&#380;enie"
  ]
  node [
    id 2951
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 2952
    label "postaranie_si&#281;"
  ]
  node [
    id 2953
    label "odpocz&#281;cie"
  ]
  node [
    id 2954
    label "spracowanie_si&#281;"
  ]
  node [
    id 2955
    label "skakanie"
  ]
  node [
    id 2956
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 2957
    label "zaprz&#281;ganie"
  ]
  node [
    id 2958
    label "wyrabianie"
  ]
  node [
    id 2959
    label "przepracowanie"
  ]
  node [
    id 2960
    label "przepracowywanie"
  ]
  node [
    id 2961
    label "awansowanie"
  ]
  node [
    id 2962
    label "courtship"
  ]
  node [
    id 2963
    label "zapracowanie"
  ]
  node [
    id 2964
    label "wyrobienie"
  ]
  node [
    id 2965
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 2966
    label "zawodoznawstwo"
  ]
  node [
    id 2967
    label "office"
  ]
  node [
    id 2968
    label "kwalifikacje"
  ]
  node [
    id 2969
    label "craft"
  ]
  node [
    id 2970
    label "transakcja"
  ]
  node [
    id 2971
    label "endeavor"
  ]
  node [
    id 2972
    label "podejmowa&#263;"
  ]
  node [
    id 2973
    label "do"
  ]
  node [
    id 2974
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 2975
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 2976
    label "dzia&#322;a&#263;"
  ]
  node [
    id 2977
    label "funkcjonowa&#263;"
  ]
  node [
    id 2978
    label "biuro"
  ]
  node [
    id 2979
    label "lead"
  ]
  node [
    id 2980
    label "w&#322;adza"
  ]
  node [
    id 2981
    label "react"
  ]
  node [
    id 2982
    label "ponosi&#263;"
  ]
  node [
    id 2983
    label "pytanie"
  ]
  node [
    id 2984
    label "equate"
  ]
  node [
    id 2985
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 2986
    label "answer"
  ]
  node [
    id 2987
    label "tone"
  ]
  node [
    id 2988
    label "contend"
  ]
  node [
    id 2989
    label "reagowa&#263;"
  ]
  node [
    id 2990
    label "impart"
  ]
  node [
    id 2991
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 2992
    label "dostarcza&#263;"
  ]
  node [
    id 2993
    label "&#322;adowa&#263;"
  ]
  node [
    id 2994
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 2995
    label "przeznacza&#263;"
  ]
  node [
    id 2996
    label "surrender"
  ]
  node [
    id 2997
    label "traktowa&#263;"
  ]
  node [
    id 2998
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 2999
    label "obiecywa&#263;"
  ]
  node [
    id 3000
    label "odst&#281;powa&#263;"
  ]
  node [
    id 3001
    label "tender"
  ]
  node [
    id 3002
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 3003
    label "t&#322;uc"
  ]
  node [
    id 3004
    label "powierza&#263;"
  ]
  node [
    id 3005
    label "wpiernicza&#263;"
  ]
  node [
    id 3006
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 3007
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 3008
    label "p&#322;aci&#263;"
  ]
  node [
    id 3009
    label "hold_out"
  ]
  node [
    id 3010
    label "nalewa&#263;"
  ]
  node [
    id 3011
    label "zezwala&#263;"
  ]
  node [
    id 3012
    label "hold"
  ]
  node [
    id 3013
    label "wst&#281;powa&#263;"
  ]
  node [
    id 3014
    label "make"
  ]
  node [
    id 3015
    label "bolt"
  ]
  node [
    id 3016
    label "odci&#261;ga&#263;"
  ]
  node [
    id 3017
    label "sprawa"
  ]
  node [
    id 3018
    label "wypytanie"
  ]
  node [
    id 3019
    label "egzaminowanie"
  ]
  node [
    id 3020
    label "wywo&#322;ywanie"
  ]
  node [
    id 3021
    label "rozpytywanie"
  ]
  node [
    id 3022
    label "wypowiedzenie"
  ]
  node [
    id 3023
    label "problemat"
  ]
  node [
    id 3024
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 3025
    label "problematyka"
  ]
  node [
    id 3026
    label "zadanie"
  ]
  node [
    id 3027
    label "przes&#322;uchiwanie"
  ]
  node [
    id 3028
    label "question"
  ]
  node [
    id 3029
    label "sprawdzanie"
  ]
  node [
    id 3030
    label "odpowiadanie"
  ]
  node [
    id 3031
    label "survey"
  ]
  node [
    id 3032
    label "komender&#243;wka"
  ]
  node [
    id 3033
    label "polecenie"
  ]
  node [
    id 3034
    label "formu&#322;a"
  ]
  node [
    id 3035
    label "posterunek"
  ]
  node [
    id 3036
    label "psiarnia"
  ]
  node [
    id 3037
    label "direction"
  ]
  node [
    id 3038
    label "awansowa&#263;"
  ]
  node [
    id 3039
    label "wakowa&#263;"
  ]
  node [
    id 3040
    label "powierzanie"
  ]
  node [
    id 3041
    label "pozycja"
  ]
  node [
    id 3042
    label "agencja"
  ]
  node [
    id 3043
    label "warta"
  ]
  node [
    id 3044
    label "ukaz"
  ]
  node [
    id 3045
    label "pognanie"
  ]
  node [
    id 3046
    label "rekomendacja"
  ]
  node [
    id 3047
    label "pobiegni&#281;cie"
  ]
  node [
    id 3048
    label "education"
  ]
  node [
    id 3049
    label "doradzenie"
  ]
  node [
    id 3050
    label "recommendation"
  ]
  node [
    id 3051
    label "zaordynowanie"
  ]
  node [
    id 3052
    label "powierzenie"
  ]
  node [
    id 3053
    label "przesadzenie"
  ]
  node [
    id 3054
    label "consign"
  ]
  node [
    id 3055
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 3056
    label "zapis"
  ]
  node [
    id 3057
    label "formularz"
  ]
  node [
    id 3058
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 3059
    label "kultura_duchowa"
  ]
  node [
    id 3060
    label "rule"
  ]
  node [
    id 3061
    label "kultura"
  ]
  node [
    id 3062
    label "ceremony"
  ]
  node [
    id 3063
    label "komisariat"
  ]
  node [
    id 3064
    label "stalag"
  ]
  node [
    id 3065
    label "rozkaz"
  ]
  node [
    id 3066
    label "przesyca&#263;"
  ]
  node [
    id 3067
    label "przesycenie"
  ]
  node [
    id 3068
    label "przesycanie"
  ]
  node [
    id 3069
    label "struktura_metalu"
  ]
  node [
    id 3070
    label "mieszanina"
  ]
  node [
    id 3071
    label "znak_nakazu"
  ]
  node [
    id 3072
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 3073
    label "reflektor"
  ]
  node [
    id 3074
    label "alia&#380;"
  ]
  node [
    id 3075
    label "przesyci&#263;"
  ]
  node [
    id 3076
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 3077
    label "reflector"
  ]
  node [
    id 3078
    label "lampa"
  ]
  node [
    id 3079
    label "teleskop"
  ]
  node [
    id 3080
    label "pr&#281;t"
  ]
  node [
    id 3081
    label "dipol"
  ]
  node [
    id 3082
    label "zwierciad&#322;o"
  ]
  node [
    id 3083
    label "frakcja"
  ]
  node [
    id 3084
    label "synteza"
  ]
  node [
    id 3085
    label "obr&#243;bka_termiczna"
  ]
  node [
    id 3086
    label "glut"
  ]
  node [
    id 3087
    label "saturation"
  ]
  node [
    id 3088
    label "nasycenie"
  ]
  node [
    id 3089
    label "impregnation"
  ]
  node [
    id 3090
    label "opanowanie"
  ]
  node [
    id 3091
    label "nadanie"
  ]
  node [
    id 3092
    label "obrabia&#263;"
  ]
  node [
    id 3093
    label "sludge"
  ]
  node [
    id 3094
    label "nadawa&#263;"
  ]
  node [
    id 3095
    label "przesadza&#263;"
  ]
  node [
    id 3096
    label "przesadzanie"
  ]
  node [
    id 3097
    label "nadawanie"
  ]
  node [
    id 3098
    label "nada&#263;"
  ]
  node [
    id 3099
    label "obrobi&#263;"
  ]
  node [
    id 3100
    label "overcharge"
  ]
  node [
    id 3101
    label "ogarn&#261;&#263;"
  ]
  node [
    id 3102
    label "przesadzi&#263;"
  ]
  node [
    id 3103
    label "pull"
  ]
  node [
    id 3104
    label "unfold"
  ]
  node [
    id 3105
    label "gallop"
  ]
  node [
    id 3106
    label "wyd&#322;u&#380;y&#263;"
  ]
  node [
    id 3107
    label "wym&#243;wi&#263;"
  ]
  node [
    id 3108
    label "metal"
  ]
  node [
    id 3109
    label "rozci&#261;gn&#261;&#263;"
  ]
  node [
    id 3110
    label "przesun&#261;&#263;"
  ]
  node [
    id 3111
    label "stall"
  ]
  node [
    id 3112
    label "wyd&#322;u&#380;enie"
  ]
  node [
    id 3113
    label "wym&#243;wienie"
  ]
  node [
    id 3114
    label "przetkanie"
  ]
  node [
    id 3115
    label "rozci&#261;gni&#281;cie"
  ]
  node [
    id 3116
    label "&#380;y&#263;"
  ]
  node [
    id 3117
    label "coating"
  ]
  node [
    id 3118
    label "przebywa&#263;"
  ]
  node [
    id 3119
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 3120
    label "ko&#324;czy&#263;"
  ]
  node [
    id 3121
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 3122
    label "finish_up"
  ]
  node [
    id 3123
    label "pause"
  ]
  node [
    id 3124
    label "stay"
  ]
  node [
    id 3125
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 3126
    label "satisfy"
  ]
  node [
    id 3127
    label "close"
  ]
  node [
    id 3128
    label "zako&#324;cza&#263;"
  ]
  node [
    id 3129
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 3130
    label "tkwi&#263;"
  ]
  node [
    id 3131
    label "hesitate"
  ]
  node [
    id 3132
    label "translokowa&#263;"
  ]
  node [
    id 3133
    label "go"
  ]
  node [
    id 3134
    label "goban"
  ]
  node [
    id 3135
    label "gra_planszowa"
  ]
  node [
    id 3136
    label "sport_umys&#322;owy"
  ]
  node [
    id 3137
    label "chi&#324;ski"
  ]
  node [
    id 3138
    label "kraj"
  ]
  node [
    id 3139
    label "koniec"
  ]
  node [
    id 3140
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 3141
    label "armia"
  ]
  node [
    id 3142
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 3143
    label "poprowadzi&#263;"
  ]
  node [
    id 3144
    label "cord"
  ]
  node [
    id 3145
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 3146
    label "trasa"
  ]
  node [
    id 3147
    label "tract"
  ]
  node [
    id 3148
    label "materia&#322;_zecerski"
  ]
  node [
    id 3149
    label "przeorientowywanie"
  ]
  node [
    id 3150
    label "curve"
  ]
  node [
    id 3151
    label "figura_geometryczna"
  ]
  node [
    id 3152
    label "wygl&#261;d"
  ]
  node [
    id 3153
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 3154
    label "jard"
  ]
  node [
    id 3155
    label "szczep"
  ]
  node [
    id 3156
    label "phreaker"
  ]
  node [
    id 3157
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 3158
    label "grupa_organizm&#243;w"
  ]
  node [
    id 3159
    label "prowadzi&#263;"
  ]
  node [
    id 3160
    label "przeorientowywa&#263;"
  ]
  node [
    id 3161
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 3162
    label "access"
  ]
  node [
    id 3163
    label "przeorientowanie"
  ]
  node [
    id 3164
    label "przeorientowa&#263;"
  ]
  node [
    id 3165
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 3166
    label "billing"
  ]
  node [
    id 3167
    label "szpaler"
  ]
  node [
    id 3168
    label "sztrych"
  ]
  node [
    id 3169
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 3170
    label "drzewo_genealogiczne"
  ]
  node [
    id 3171
    label "transporter"
  ]
  node [
    id 3172
    label "line"
  ]
  node [
    id 3173
    label "kompleksja"
  ]
  node [
    id 3174
    label "granice"
  ]
  node [
    id 3175
    label "kontakt"
  ]
  node [
    id 3176
    label "przewo&#378;nik"
  ]
  node [
    id 3177
    label "przystanek"
  ]
  node [
    id 3178
    label "linijka"
  ]
  node [
    id 3179
    label "coalescence"
  ]
  node [
    id 3180
    label "Ural"
  ]
  node [
    id 3181
    label "prowadzenie"
  ]
  node [
    id 3182
    label "ostatnie_podrygi"
  ]
  node [
    id 3183
    label "visitation"
  ]
  node [
    id 3184
    label "agonia"
  ]
  node [
    id 3185
    label "defenestracja"
  ]
  node [
    id 3186
    label "kres"
  ]
  node [
    id 3187
    label "mogi&#322;a"
  ]
  node [
    id 3188
    label "kres_&#380;ycia"
  ]
  node [
    id 3189
    label "szereg"
  ]
  node [
    id 3190
    label "szeol"
  ]
  node [
    id 3191
    label "pogrzebanie"
  ]
  node [
    id 3192
    label "&#380;a&#322;oba"
  ]
  node [
    id 3193
    label "Katar"
  ]
  node [
    id 3194
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 3195
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 3196
    label "Gwatemala"
  ]
  node [
    id 3197
    label "Afganistan"
  ]
  node [
    id 3198
    label "Ekwador"
  ]
  node [
    id 3199
    label "Tad&#380;ykistan"
  ]
  node [
    id 3200
    label "Bhutan"
  ]
  node [
    id 3201
    label "Argentyna"
  ]
  node [
    id 3202
    label "D&#380;ibuti"
  ]
  node [
    id 3203
    label "Wenezuela"
  ]
  node [
    id 3204
    label "Gabon"
  ]
  node [
    id 3205
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 3206
    label "Rwanda"
  ]
  node [
    id 3207
    label "Liechtenstein"
  ]
  node [
    id 3208
    label "organizacja"
  ]
  node [
    id 3209
    label "Sri_Lanka"
  ]
  node [
    id 3210
    label "Madagaskar"
  ]
  node [
    id 3211
    label "Kongo"
  ]
  node [
    id 3212
    label "Bangladesz"
  ]
  node [
    id 3213
    label "Kanada"
  ]
  node [
    id 3214
    label "Wehrlen"
  ]
  node [
    id 3215
    label "Surinam"
  ]
  node [
    id 3216
    label "Chile"
  ]
  node [
    id 3217
    label "Uganda"
  ]
  node [
    id 3218
    label "W&#281;gry"
  ]
  node [
    id 3219
    label "Birma"
  ]
  node [
    id 3220
    label "Kazachstan"
  ]
  node [
    id 3221
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 3222
    label "Armenia"
  ]
  node [
    id 3223
    label "Timor_Wschodni"
  ]
  node [
    id 3224
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 3225
    label "Izrael"
  ]
  node [
    id 3226
    label "Estonia"
  ]
  node [
    id 3227
    label "Komory"
  ]
  node [
    id 3228
    label "Kamerun"
  ]
  node [
    id 3229
    label "Belize"
  ]
  node [
    id 3230
    label "Sierra_Leone"
  ]
  node [
    id 3231
    label "Luksemburg"
  ]
  node [
    id 3232
    label "USA"
  ]
  node [
    id 3233
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 3234
    label "Barbados"
  ]
  node [
    id 3235
    label "San_Marino"
  ]
  node [
    id 3236
    label "Bu&#322;garia"
  ]
  node [
    id 3237
    label "Indonezja"
  ]
  node [
    id 3238
    label "Malawi"
  ]
  node [
    id 3239
    label "Zambia"
  ]
  node [
    id 3240
    label "Angola"
  ]
  node [
    id 3241
    label "Grenada"
  ]
  node [
    id 3242
    label "Nepal"
  ]
  node [
    id 3243
    label "Panama"
  ]
  node [
    id 3244
    label "Rumunia"
  ]
  node [
    id 3245
    label "Malediwy"
  ]
  node [
    id 3246
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 3247
    label "S&#322;owacja"
  ]
  node [
    id 3248
    label "Egipt"
  ]
  node [
    id 3249
    label "Kolumbia"
  ]
  node [
    id 3250
    label "Mozambik"
  ]
  node [
    id 3251
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 3252
    label "Laos"
  ]
  node [
    id 3253
    label "Burundi"
  ]
  node [
    id 3254
    label "Suazi"
  ]
  node [
    id 3255
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 3256
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 3257
    label "Trynidad_i_Tobago"
  ]
  node [
    id 3258
    label "Dominika"
  ]
  node [
    id 3259
    label "Syria"
  ]
  node [
    id 3260
    label "Gwinea_Bissau"
  ]
  node [
    id 3261
    label "Liberia"
  ]
  node [
    id 3262
    label "Zimbabwe"
  ]
  node [
    id 3263
    label "Dominikana"
  ]
  node [
    id 3264
    label "Senegal"
  ]
  node [
    id 3265
    label "Gruzja"
  ]
  node [
    id 3266
    label "Chorwacja"
  ]
  node [
    id 3267
    label "Togo"
  ]
  node [
    id 3268
    label "Meksyk"
  ]
  node [
    id 3269
    label "Macedonia"
  ]
  node [
    id 3270
    label "Gujana"
  ]
  node [
    id 3271
    label "Zair"
  ]
  node [
    id 3272
    label "Kambod&#380;a"
  ]
  node [
    id 3273
    label "Albania"
  ]
  node [
    id 3274
    label "Mauritius"
  ]
  node [
    id 3275
    label "Monako"
  ]
  node [
    id 3276
    label "Gwinea"
  ]
  node [
    id 3277
    label "Mali"
  ]
  node [
    id 3278
    label "Nigeria"
  ]
  node [
    id 3279
    label "Kostaryka"
  ]
  node [
    id 3280
    label "Hanower"
  ]
  node [
    id 3281
    label "Paragwaj"
  ]
  node [
    id 3282
    label "Wyspy_Salomona"
  ]
  node [
    id 3283
    label "Seszele"
  ]
  node [
    id 3284
    label "Boliwia"
  ]
  node [
    id 3285
    label "Kirgistan"
  ]
  node [
    id 3286
    label "Irlandia"
  ]
  node [
    id 3287
    label "Czad"
  ]
  node [
    id 3288
    label "Irak"
  ]
  node [
    id 3289
    label "Lesoto"
  ]
  node [
    id 3290
    label "Malta"
  ]
  node [
    id 3291
    label "Andora"
  ]
  node [
    id 3292
    label "Chiny"
  ]
  node [
    id 3293
    label "Filipiny"
  ]
  node [
    id 3294
    label "Brazylia"
  ]
  node [
    id 3295
    label "Nikaragua"
  ]
  node [
    id 3296
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 3297
    label "Kenia"
  ]
  node [
    id 3298
    label "Niger"
  ]
  node [
    id 3299
    label "Portugalia"
  ]
  node [
    id 3300
    label "Fid&#380;i"
  ]
  node [
    id 3301
    label "Botswana"
  ]
  node [
    id 3302
    label "Tajlandia"
  ]
  node [
    id 3303
    label "Australia"
  ]
  node [
    id 3304
    label "Burkina_Faso"
  ]
  node [
    id 3305
    label "Benin"
  ]
  node [
    id 3306
    label "Tanzania"
  ]
  node [
    id 3307
    label "interior"
  ]
  node [
    id 3308
    label "&#321;otwa"
  ]
  node [
    id 3309
    label "Kiribati"
  ]
  node [
    id 3310
    label "Rodezja"
  ]
  node [
    id 3311
    label "Cypr"
  ]
  node [
    id 3312
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 3313
    label "Peru"
  ]
  node [
    id 3314
    label "Urugwaj"
  ]
  node [
    id 3315
    label "Jordania"
  ]
  node [
    id 3316
    label "Grecja"
  ]
  node [
    id 3317
    label "Azerbejd&#380;an"
  ]
  node [
    id 3318
    label "Turcja"
  ]
  node [
    id 3319
    label "ziemia"
  ]
  node [
    id 3320
    label "Oman"
  ]
  node [
    id 3321
    label "Sudan"
  ]
  node [
    id 3322
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 3323
    label "Uzbekistan"
  ]
  node [
    id 3324
    label "Honduras"
  ]
  node [
    id 3325
    label "Mongolia"
  ]
  node [
    id 3326
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 3327
    label "Tajwan"
  ]
  node [
    id 3328
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 3329
    label "Liban"
  ]
  node [
    id 3330
    label "Japonia"
  ]
  node [
    id 3331
    label "Ghana"
  ]
  node [
    id 3332
    label "Bahrajn"
  ]
  node [
    id 3333
    label "Belgia"
  ]
  node [
    id 3334
    label "Kuwejt"
  ]
  node [
    id 3335
    label "Litwa"
  ]
  node [
    id 3336
    label "S&#322;owenia"
  ]
  node [
    id 3337
    label "Szwajcaria"
  ]
  node [
    id 3338
    label "Erytrea"
  ]
  node [
    id 3339
    label "Arabia_Saudyjska"
  ]
  node [
    id 3340
    label "granica_pa&#324;stwa"
  ]
  node [
    id 3341
    label "Malezja"
  ]
  node [
    id 3342
    label "Korea"
  ]
  node [
    id 3343
    label "Jemen"
  ]
  node [
    id 3344
    label "Namibia"
  ]
  node [
    id 3345
    label "Brunei"
  ]
  node [
    id 3346
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 3347
    label "Khitai"
  ]
  node [
    id 3348
    label "Iran"
  ]
  node [
    id 3349
    label "Gambia"
  ]
  node [
    id 3350
    label "Somalia"
  ]
  node [
    id 3351
    label "Holandia"
  ]
  node [
    id 3352
    label "Turkmenistan"
  ]
  node [
    id 3353
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 3354
    label "Salwador"
  ]
  node [
    id 3355
    label "po&#322;o&#380;enie"
  ]
  node [
    id 3356
    label "ust&#281;p"
  ]
  node [
    id 3357
    label "plan"
  ]
  node [
    id 3358
    label "obiekt_matematyczny"
  ]
  node [
    id 3359
    label "plamka"
  ]
  node [
    id 3360
    label "stopie&#324;_pisma"
  ]
  node [
    id 3361
    label "jednostka"
  ]
  node [
    id 3362
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 3363
    label "zapunktowa&#263;"
  ]
  node [
    id 3364
    label "podpunkt"
  ]
  node [
    id 3365
    label "debit"
  ]
  node [
    id 3366
    label "druk"
  ]
  node [
    id 3367
    label "szata_graficzna"
  ]
  node [
    id 3368
    label "wydawa&#263;"
  ]
  node [
    id 3369
    label "szermierka"
  ]
  node [
    id 3370
    label "spis"
  ]
  node [
    id 3371
    label "wyda&#263;"
  ]
  node [
    id 3372
    label "ustawienie"
  ]
  node [
    id 3373
    label "publikacja"
  ]
  node [
    id 3374
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 3375
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 3376
    label "rozmieszczenie"
  ]
  node [
    id 3377
    label "redaktor"
  ]
  node [
    id 3378
    label "awans"
  ]
  node [
    id 3379
    label "poster"
  ]
  node [
    id 3380
    label "przyswoi&#263;"
  ]
  node [
    id 3381
    label "ludzko&#347;&#263;"
  ]
  node [
    id 3382
    label "one"
  ]
  node [
    id 3383
    label "ewoluowanie"
  ]
  node [
    id 3384
    label "supremum"
  ]
  node [
    id 3385
    label "skala"
  ]
  node [
    id 3386
    label "przyswajanie"
  ]
  node [
    id 3387
    label "wyewoluowanie"
  ]
  node [
    id 3388
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 3389
    label "przeliczy&#263;"
  ]
  node [
    id 3390
    label "wyewoluowa&#263;"
  ]
  node [
    id 3391
    label "ewoluowa&#263;"
  ]
  node [
    id 3392
    label "matematyka"
  ]
  node [
    id 3393
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 3394
    label "rzut"
  ]
  node [
    id 3395
    label "liczba_naturalna"
  ]
  node [
    id 3396
    label "czynnik_biotyczny"
  ]
  node [
    id 3397
    label "figura"
  ]
  node [
    id 3398
    label "individual"
  ]
  node [
    id 3399
    label "portrecista"
  ]
  node [
    id 3400
    label "przyswaja&#263;"
  ]
  node [
    id 3401
    label "przyswojenie"
  ]
  node [
    id 3402
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 3403
    label "profanum"
  ]
  node [
    id 3404
    label "starzenie_si&#281;"
  ]
  node [
    id 3405
    label "przeliczanie"
  ]
  node [
    id 3406
    label "oddzia&#322;ywanie"
  ]
  node [
    id 3407
    label "antropochoria"
  ]
  node [
    id 3408
    label "homo_sapiens"
  ]
  node [
    id 3409
    label "przelicza&#263;"
  ]
  node [
    id 3410
    label "infimum"
  ]
  node [
    id 3411
    label "przeliczenie"
  ]
  node [
    id 3412
    label "time"
  ]
  node [
    id 3413
    label "przenocowanie"
  ]
  node [
    id 3414
    label "pora&#380;ka"
  ]
  node [
    id 3415
    label "nak&#322;adzenie"
  ]
  node [
    id 3416
    label "pouk&#322;adanie"
  ]
  node [
    id 3417
    label "pokrycie"
  ]
  node [
    id 3418
    label "zepsucie"
  ]
  node [
    id 3419
    label "trim"
  ]
  node [
    id 3420
    label "ugoszczenie"
  ]
  node [
    id 3421
    label "zbudowanie"
  ]
  node [
    id 3422
    label "reading"
  ]
  node [
    id 3423
    label "wygranie"
  ]
  node [
    id 3424
    label "presentation"
  ]
  node [
    id 3425
    label "toaleta"
  ]
  node [
    id 3426
    label "artyku&#322;"
  ]
  node [
    id 3427
    label "urywek"
  ]
  node [
    id 3428
    label "co&#347;"
  ]
  node [
    id 3429
    label "program"
  ]
  node [
    id 3430
    label "object"
  ]
  node [
    id 3431
    label "temat"
  ]
  node [
    id 3432
    label "proposition"
  ]
  node [
    id 3433
    label "intencja"
  ]
  node [
    id 3434
    label "device"
  ]
  node [
    id 3435
    label "obraz"
  ]
  node [
    id 3436
    label "reprezentacja"
  ]
  node [
    id 3437
    label "agreement"
  ]
  node [
    id 3438
    label "dekoracja"
  ]
  node [
    id 3439
    label "perspektywa"
  ]
  node [
    id 3440
    label "krzywa"
  ]
  node [
    id 3441
    label "odcinek"
  ]
  node [
    id 3442
    label "straight_line"
  ]
  node [
    id 3443
    label "proste_sko&#347;ne"
  ]
  node [
    id 3444
    label "problem"
  ]
  node [
    id 3445
    label "pokry&#263;"
  ]
  node [
    id 3446
    label "zdoby&#263;"
  ]
  node [
    id 3447
    label "zyska&#263;"
  ]
  node [
    id 3448
    label "zaskutkowa&#263;"
  ]
  node [
    id 3449
    label "unieruchomi&#263;"
  ]
  node [
    id 3450
    label "op&#322;aci&#263;_si&#281;"
  ]
  node [
    id 3451
    label "zrejterowanie"
  ]
  node [
    id 3452
    label "zmobilizowa&#263;"
  ]
  node [
    id 3453
    label "dezerter"
  ]
  node [
    id 3454
    label "oddzia&#322;_karny"
  ]
  node [
    id 3455
    label "rezerwa"
  ]
  node [
    id 3456
    label "wermacht"
  ]
  node [
    id 3457
    label "cofni&#281;cie"
  ]
  node [
    id 3458
    label "soldateska"
  ]
  node [
    id 3459
    label "ods&#322;ugiwanie"
  ]
  node [
    id 3460
    label "werbowanie_si&#281;"
  ]
  node [
    id 3461
    label "zdemobilizowanie"
  ]
  node [
    id 3462
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 3463
    label "s&#322;u&#380;ba"
  ]
  node [
    id 3464
    label "or&#281;&#380;"
  ]
  node [
    id 3465
    label "Legia_Cudzoziemska"
  ]
  node [
    id 3466
    label "Armia_Czerwona"
  ]
  node [
    id 3467
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 3468
    label "rejterowanie"
  ]
  node [
    id 3469
    label "Czerwona_Gwardia"
  ]
  node [
    id 3470
    label "zrejterowa&#263;"
  ]
  node [
    id 3471
    label "sztabslekarz"
  ]
  node [
    id 3472
    label "zmobilizowanie"
  ]
  node [
    id 3473
    label "wojo"
  ]
  node [
    id 3474
    label "pospolite_ruszenie"
  ]
  node [
    id 3475
    label "Eurokorpus"
  ]
  node [
    id 3476
    label "mobilizowanie"
  ]
  node [
    id 3477
    label "rejterowa&#263;"
  ]
  node [
    id 3478
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 3479
    label "mobilizowa&#263;"
  ]
  node [
    id 3480
    label "Armia_Krajowa"
  ]
  node [
    id 3481
    label "obrona"
  ]
  node [
    id 3482
    label "dryl"
  ]
  node [
    id 3483
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 3484
    label "petarda"
  ]
  node [
    id 3485
    label "zdemobilizowa&#263;"
  ]
  node [
    id 3486
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 3487
    label "przestarza&#322;y"
  ]
  node [
    id 3488
    label "odleg&#322;y"
  ]
  node [
    id 3489
    label "od_dawna"
  ]
  node [
    id 3490
    label "poprzedni"
  ]
  node [
    id 3491
    label "dawno"
  ]
  node [
    id 3492
    label "d&#322;ugoletni"
  ]
  node [
    id 3493
    label "anachroniczny"
  ]
  node [
    id 3494
    label "dawniej"
  ]
  node [
    id 3495
    label "niegdysiejszy"
  ]
  node [
    id 3496
    label "wcze&#347;niejszy"
  ]
  node [
    id 3497
    label "kombatant"
  ]
  node [
    id 3498
    label "stary"
  ]
  node [
    id 3499
    label "poprzednio"
  ]
  node [
    id 3500
    label "zestarzenie_si&#281;"
  ]
  node [
    id 3501
    label "archaicznie"
  ]
  node [
    id 3502
    label "zgrzybienie"
  ]
  node [
    id 3503
    label "niedzisiejszy"
  ]
  node [
    id 3504
    label "staro&#347;wiecki"
  ]
  node [
    id 3505
    label "przestarzale"
  ]
  node [
    id 3506
    label "anachronicznie"
  ]
  node [
    id 3507
    label "niezgodny"
  ]
  node [
    id 3508
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 3509
    label "ongi&#347;"
  ]
  node [
    id 3510
    label "odlegle"
  ]
  node [
    id 3511
    label "delikatny"
  ]
  node [
    id 3512
    label "r&#243;&#380;ny"
  ]
  node [
    id 3513
    label "daleko"
  ]
  node [
    id 3514
    label "obcy"
  ]
  node [
    id 3515
    label "nieobecny"
  ]
  node [
    id 3516
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 3517
    label "ojciec"
  ]
  node [
    id 3518
    label "nienowoczesny"
  ]
  node [
    id 3519
    label "gruba_ryba"
  ]
  node [
    id 3520
    label "staro"
  ]
  node [
    id 3521
    label "m&#261;&#380;"
  ]
  node [
    id 3522
    label "starzy"
  ]
  node [
    id 3523
    label "dotychczasowy"
  ]
  node [
    id 3524
    label "p&#243;&#378;ny"
  ]
  node [
    id 3525
    label "charakterystyczny"
  ]
  node [
    id 3526
    label "brat"
  ]
  node [
    id 3527
    label "po_staro&#347;wiecku"
  ]
  node [
    id 3528
    label "zwierzchnik"
  ]
  node [
    id 3529
    label "starczo"
  ]
  node [
    id 3530
    label "dojrza&#322;y"
  ]
  node [
    id 3531
    label "wcze&#347;niej"
  ]
  node [
    id 3532
    label "miniony"
  ]
  node [
    id 3533
    label "ostatni"
  ]
  node [
    id 3534
    label "wieloletni"
  ]
  node [
    id 3535
    label "kiedy&#347;"
  ]
  node [
    id 3536
    label "d&#322;ugotrwale"
  ]
  node [
    id 3537
    label "dawnie"
  ]
  node [
    id 3538
    label "wyjadacz"
  ]
  node [
    id 3539
    label "weteran"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 463
  ]
  edge [
    source 1
    target 464
  ]
  edge [
    source 1
    target 465
  ]
  edge [
    source 1
    target 466
  ]
  edge [
    source 1
    target 467
  ]
  edge [
    source 1
    target 468
  ]
  edge [
    source 1
    target 469
  ]
  edge [
    source 1
    target 470
  ]
  edge [
    source 1
    target 471
  ]
  edge [
    source 1
    target 472
  ]
  edge [
    source 1
    target 473
  ]
  edge [
    source 1
    target 474
  ]
  edge [
    source 1
    target 475
  ]
  edge [
    source 1
    target 476
  ]
  edge [
    source 1
    target 477
  ]
  edge [
    source 1
    target 478
  ]
  edge [
    source 1
    target 479
  ]
  edge [
    source 1
    target 480
  ]
  edge [
    source 1
    target 481
  ]
  edge [
    source 1
    target 482
  ]
  edge [
    source 1
    target 483
  ]
  edge [
    source 1
    target 484
  ]
  edge [
    source 1
    target 485
  ]
  edge [
    source 1
    target 486
  ]
  edge [
    source 1
    target 487
  ]
  edge [
    source 1
    target 488
  ]
  edge [
    source 1
    target 489
  ]
  edge [
    source 1
    target 490
  ]
  edge [
    source 1
    target 491
  ]
  edge [
    source 1
    target 492
  ]
  edge [
    source 1
    target 493
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 3
    target 625
  ]
  edge [
    source 3
    target 626
  ]
  edge [
    source 3
    target 627
  ]
  edge [
    source 3
    target 628
  ]
  edge [
    source 3
    target 629
  ]
  edge [
    source 3
    target 630
  ]
  edge [
    source 3
    target 631
  ]
  edge [
    source 3
    target 632
  ]
  edge [
    source 3
    target 633
  ]
  edge [
    source 3
    target 634
  ]
  edge [
    source 3
    target 635
  ]
  edge [
    source 3
    target 636
  ]
  edge [
    source 3
    target 637
  ]
  edge [
    source 3
    target 638
  ]
  edge [
    source 3
    target 639
  ]
  edge [
    source 3
    target 640
  ]
  edge [
    source 3
    target 641
  ]
  edge [
    source 3
    target 642
  ]
  edge [
    source 3
    target 643
  ]
  edge [
    source 3
    target 644
  ]
  edge [
    source 3
    target 645
  ]
  edge [
    source 3
    target 646
  ]
  edge [
    source 3
    target 647
  ]
  edge [
    source 3
    target 648
  ]
  edge [
    source 3
    target 649
  ]
  edge [
    source 3
    target 650
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 651
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 652
  ]
  edge [
    source 3
    target 653
  ]
  edge [
    source 3
    target 654
  ]
  edge [
    source 3
    target 655
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 656
  ]
  edge [
    source 3
    target 657
  ]
  edge [
    source 3
    target 658
  ]
  edge [
    source 3
    target 659
  ]
  edge [
    source 3
    target 660
  ]
  edge [
    source 3
    target 661
  ]
  edge [
    source 3
    target 662
  ]
  edge [
    source 3
    target 663
  ]
  edge [
    source 3
    target 664
  ]
  edge [
    source 3
    target 665
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 666
  ]
  edge [
    source 3
    target 667
  ]
  edge [
    source 3
    target 668
  ]
  edge [
    source 3
    target 669
  ]
  edge [
    source 3
    target 670
  ]
  edge [
    source 3
    target 671
  ]
  edge [
    source 3
    target 672
  ]
  edge [
    source 3
    target 673
  ]
  edge [
    source 3
    target 674
  ]
  edge [
    source 3
    target 675
  ]
  edge [
    source 3
    target 676
  ]
  edge [
    source 3
    target 677
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 678
  ]
  edge [
    source 3
    target 679
  ]
  edge [
    source 3
    target 680
  ]
  edge [
    source 3
    target 681
  ]
  edge [
    source 3
    target 682
  ]
  edge [
    source 3
    target 683
  ]
  edge [
    source 3
    target 684
  ]
  edge [
    source 3
    target 685
  ]
  edge [
    source 3
    target 686
  ]
  edge [
    source 3
    target 687
  ]
  edge [
    source 3
    target 688
  ]
  edge [
    source 3
    target 689
  ]
  edge [
    source 3
    target 690
  ]
  edge [
    source 3
    target 691
  ]
  edge [
    source 3
    target 692
  ]
  edge [
    source 3
    target 693
  ]
  edge [
    source 3
    target 694
  ]
  edge [
    source 3
    target 695
  ]
  edge [
    source 3
    target 696
  ]
  edge [
    source 3
    target 697
  ]
  edge [
    source 3
    target 698
  ]
  edge [
    source 3
    target 699
  ]
  edge [
    source 3
    target 700
  ]
  edge [
    source 3
    target 701
  ]
  edge [
    source 3
    target 702
  ]
  edge [
    source 3
    target 703
  ]
  edge [
    source 3
    target 704
  ]
  edge [
    source 3
    target 705
  ]
  edge [
    source 3
    target 706
  ]
  edge [
    source 3
    target 707
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 762
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 763
  ]
  edge [
    source 5
    target 679
  ]
  edge [
    source 5
    target 764
  ]
  edge [
    source 5
    target 765
  ]
  edge [
    source 5
    target 653
  ]
  edge [
    source 5
    target 766
  ]
  edge [
    source 5
    target 767
  ]
  edge [
    source 5
    target 768
  ]
  edge [
    source 5
    target 769
  ]
  edge [
    source 5
    target 770
  ]
  edge [
    source 5
    target 771
  ]
  edge [
    source 5
    target 772
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 773
  ]
  edge [
    source 5
    target 774
  ]
  edge [
    source 5
    target 775
  ]
  edge [
    source 5
    target 776
  ]
  edge [
    source 5
    target 777
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 786
  ]
  edge [
    source 6
    target 787
  ]
  edge [
    source 6
    target 788
  ]
  edge [
    source 6
    target 789
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 790
  ]
  edge [
    source 6
    target 791
  ]
  edge [
    source 6
    target 792
  ]
  edge [
    source 6
    target 793
  ]
  edge [
    source 6
    target 794
  ]
  edge [
    source 6
    target 795
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 796
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 797
  ]
  edge [
    source 6
    target 798
  ]
  edge [
    source 6
    target 799
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 800
  ]
  edge [
    source 6
    target 801
  ]
  edge [
    source 6
    target 802
  ]
  edge [
    source 6
    target 803
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 804
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 806
  ]
  edge [
    source 6
    target 807
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 809
  ]
  edge [
    source 6
    target 810
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 811
  ]
  edge [
    source 6
    target 812
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 813
  ]
  edge [
    source 6
    target 814
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 816
  ]
  edge [
    source 6
    target 817
  ]
  edge [
    source 6
    target 818
  ]
  edge [
    source 6
    target 819
  ]
  edge [
    source 6
    target 820
  ]
  edge [
    source 6
    target 821
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 823
  ]
  edge [
    source 6
    target 824
  ]
  edge [
    source 6
    target 825
  ]
  edge [
    source 6
    target 826
  ]
  edge [
    source 6
    target 827
  ]
  edge [
    source 6
    target 828
  ]
  edge [
    source 6
    target 829
  ]
  edge [
    source 6
    target 830
  ]
  edge [
    source 6
    target 831
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 832
  ]
  edge [
    source 6
    target 833
  ]
  edge [
    source 6
    target 834
  ]
  edge [
    source 6
    target 835
  ]
  edge [
    source 6
    target 836
  ]
  edge [
    source 6
    target 837
  ]
  edge [
    source 6
    target 838
  ]
  edge [
    source 6
    target 839
  ]
  edge [
    source 6
    target 840
  ]
  edge [
    source 6
    target 841
  ]
  edge [
    source 6
    target 842
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 843
  ]
  edge [
    source 6
    target 844
  ]
  edge [
    source 6
    target 845
  ]
  edge [
    source 6
    target 846
  ]
  edge [
    source 6
    target 847
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 848
  ]
  edge [
    source 6
    target 849
  ]
  edge [
    source 6
    target 850
  ]
  edge [
    source 6
    target 851
  ]
  edge [
    source 6
    target 852
  ]
  edge [
    source 6
    target 853
  ]
  edge [
    source 6
    target 854
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 860
  ]
  edge [
    source 6
    target 861
  ]
  edge [
    source 6
    target 862
  ]
  edge [
    source 6
    target 863
  ]
  edge [
    source 6
    target 864
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 865
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 867
  ]
  edge [
    source 7
    target 868
  ]
  edge [
    source 7
    target 869
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 870
  ]
  edge [
    source 7
    target 871
  ]
  edge [
    source 7
    target 872
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 873
  ]
  edge [
    source 7
    target 874
  ]
  edge [
    source 7
    target 875
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 876
  ]
  edge [
    source 7
    target 877
  ]
  edge [
    source 7
    target 858
  ]
  edge [
    source 7
    target 878
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 879
  ]
  edge [
    source 7
    target 880
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 882
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 883
  ]
  edge [
    source 7
    target 884
  ]
  edge [
    source 7
    target 885
  ]
  edge [
    source 7
    target 886
  ]
  edge [
    source 7
    target 887
  ]
  edge [
    source 7
    target 888
  ]
  edge [
    source 7
    target 889
  ]
  edge [
    source 7
    target 890
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 891
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 893
  ]
  edge [
    source 7
    target 894
  ]
  edge [
    source 7
    target 895
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 896
  ]
  edge [
    source 7
    target 897
  ]
  edge [
    source 7
    target 898
  ]
  edge [
    source 7
    target 899
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 900
  ]
  edge [
    source 7
    target 901
  ]
  edge [
    source 7
    target 902
  ]
  edge [
    source 7
    target 903
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 7
    target 907
  ]
  edge [
    source 7
    target 908
  ]
  edge [
    source 7
    target 909
  ]
  edge [
    source 7
    target 910
  ]
  edge [
    source 7
    target 911
  ]
  edge [
    source 7
    target 912
  ]
  edge [
    source 7
    target 913
  ]
  edge [
    source 7
    target 914
  ]
  edge [
    source 7
    target 915
  ]
  edge [
    source 7
    target 850
  ]
  edge [
    source 7
    target 916
  ]
  edge [
    source 7
    target 917
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 918
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 921
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 923
  ]
  edge [
    source 7
    target 924
  ]
  edge [
    source 7
    target 925
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 928
  ]
  edge [
    source 7
    target 929
  ]
  edge [
    source 7
    target 930
  ]
  edge [
    source 7
    target 931
  ]
  edge [
    source 7
    target 932
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 933
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 7
    target 937
  ]
  edge [
    source 7
    target 938
  ]
  edge [
    source 7
    target 939
  ]
  edge [
    source 7
    target 940
  ]
  edge [
    source 7
    target 941
  ]
  edge [
    source 7
    target 942
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 944
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 974
  ]
  edge [
    source 8
    target 975
  ]
  edge [
    source 8
    target 976
  ]
  edge [
    source 8
    target 977
  ]
  edge [
    source 8
    target 978
  ]
  edge [
    source 8
    target 979
  ]
  edge [
    source 8
    target 980
  ]
  edge [
    source 8
    target 981
  ]
  edge [
    source 8
    target 982
  ]
  edge [
    source 8
    target 983
  ]
  edge [
    source 8
    target 984
  ]
  edge [
    source 8
    target 985
  ]
  edge [
    source 8
    target 986
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 987
  ]
  edge [
    source 8
    target 988
  ]
  edge [
    source 8
    target 989
  ]
  edge [
    source 8
    target 990
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 991
  ]
  edge [
    source 8
    target 992
  ]
  edge [
    source 8
    target 993
  ]
  edge [
    source 8
    target 994
  ]
  edge [
    source 8
    target 995
  ]
  edge [
    source 8
    target 996
  ]
  edge [
    source 8
    target 997
  ]
  edge [
    source 8
    target 998
  ]
  edge [
    source 8
    target 999
  ]
  edge [
    source 8
    target 1000
  ]
  edge [
    source 8
    target 1001
  ]
  edge [
    source 8
    target 1002
  ]
  edge [
    source 8
    target 1003
  ]
  edge [
    source 8
    target 1004
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 1005
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 1006
  ]
  edge [
    source 8
    target 1007
  ]
  edge [
    source 8
    target 1008
  ]
  edge [
    source 8
    target 1009
  ]
  edge [
    source 8
    target 1010
  ]
  edge [
    source 8
    target 1011
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 1012
  ]
  edge [
    source 8
    target 1013
  ]
  edge [
    source 8
    target 1014
  ]
  edge [
    source 8
    target 1015
  ]
  edge [
    source 8
    target 1016
  ]
  edge [
    source 8
    target 1017
  ]
  edge [
    source 8
    target 1018
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 1019
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 1020
  ]
  edge [
    source 8
    target 1021
  ]
  edge [
    source 8
    target 1022
  ]
  edge [
    source 8
    target 1023
  ]
  edge [
    source 8
    target 1024
  ]
  edge [
    source 8
    target 1025
  ]
  edge [
    source 8
    target 1026
  ]
  edge [
    source 8
    target 1027
  ]
  edge [
    source 8
    target 1028
  ]
  edge [
    source 8
    target 1029
  ]
  edge [
    source 8
    target 1030
  ]
  edge [
    source 8
    target 1031
  ]
  edge [
    source 8
    target 1032
  ]
  edge [
    source 8
    target 1033
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 1034
  ]
  edge [
    source 8
    target 1035
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 1036
  ]
  edge [
    source 8
    target 1037
  ]
  edge [
    source 8
    target 1038
  ]
  edge [
    source 8
    target 1039
  ]
  edge [
    source 8
    target 1040
  ]
  edge [
    source 8
    target 1041
  ]
  edge [
    source 8
    target 1042
  ]
  edge [
    source 8
    target 1043
  ]
  edge [
    source 8
    target 1044
  ]
  edge [
    source 8
    target 1045
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 1046
  ]
  edge [
    source 8
    target 1047
  ]
  edge [
    source 8
    target 1048
  ]
  edge [
    source 8
    target 1049
  ]
  edge [
    source 8
    target 1050
  ]
  edge [
    source 8
    target 1051
  ]
  edge [
    source 8
    target 1052
  ]
  edge [
    source 8
    target 1053
  ]
  edge [
    source 8
    target 1054
  ]
  edge [
    source 8
    target 1055
  ]
  edge [
    source 8
    target 1056
  ]
  edge [
    source 8
    target 1057
  ]
  edge [
    source 8
    target 1058
  ]
  edge [
    source 8
    target 1059
  ]
  edge [
    source 8
    target 1060
  ]
  edge [
    source 8
    target 1061
  ]
  edge [
    source 8
    target 1062
  ]
  edge [
    source 8
    target 1063
  ]
  edge [
    source 8
    target 1064
  ]
  edge [
    source 8
    target 1065
  ]
  edge [
    source 8
    target 1066
  ]
  edge [
    source 8
    target 950
  ]
  edge [
    source 8
    target 1067
  ]
  edge [
    source 8
    target 1068
  ]
  edge [
    source 8
    target 1069
  ]
  edge [
    source 8
    target 1070
  ]
  edge [
    source 8
    target 1071
  ]
  edge [
    source 8
    target 1072
  ]
  edge [
    source 8
    target 1073
  ]
  edge [
    source 8
    target 1074
  ]
  edge [
    source 8
    target 953
  ]
  edge [
    source 8
    target 1075
  ]
  edge [
    source 8
    target 1076
  ]
  edge [
    source 8
    target 1077
  ]
  edge [
    source 8
    target 1078
  ]
  edge [
    source 8
    target 1079
  ]
  edge [
    source 8
    target 1080
  ]
  edge [
    source 8
    target 1081
  ]
  edge [
    source 8
    target 1082
  ]
  edge [
    source 8
    target 1083
  ]
  edge [
    source 8
    target 1084
  ]
  edge [
    source 8
    target 1085
  ]
  edge [
    source 8
    target 1086
  ]
  edge [
    source 8
    target 1087
  ]
  edge [
    source 8
    target 1088
  ]
  edge [
    source 8
    target 1089
  ]
  edge [
    source 8
    target 1090
  ]
  edge [
    source 8
    target 1091
  ]
  edge [
    source 8
    target 1092
  ]
  edge [
    source 8
    target 924
  ]
  edge [
    source 8
    target 1093
  ]
  edge [
    source 8
    target 1094
  ]
  edge [
    source 8
    target 1095
  ]
  edge [
    source 8
    target 1096
  ]
  edge [
    source 8
    target 1097
  ]
  edge [
    source 8
    target 1098
  ]
  edge [
    source 8
    target 1099
  ]
  edge [
    source 8
    target 1100
  ]
  edge [
    source 8
    target 1101
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 1102
  ]
  edge [
    source 8
    target 1103
  ]
  edge [
    source 8
    target 1104
  ]
  edge [
    source 8
    target 1105
  ]
  edge [
    source 8
    target 1106
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 1107
  ]
  edge [
    source 8
    target 1108
  ]
  edge [
    source 8
    target 1109
  ]
  edge [
    source 8
    target 1110
  ]
  edge [
    source 8
    target 1111
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 1112
  ]
  edge [
    source 8
    target 1113
  ]
  edge [
    source 8
    target 1114
  ]
  edge [
    source 8
    target 1115
  ]
  edge [
    source 8
    target 1116
  ]
  edge [
    source 8
    target 1117
  ]
  edge [
    source 8
    target 1118
  ]
  edge [
    source 8
    target 1119
  ]
  edge [
    source 8
    target 1120
  ]
  edge [
    source 8
    target 1121
  ]
  edge [
    source 8
    target 1122
  ]
  edge [
    source 8
    target 1123
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 1124
  ]
  edge [
    source 8
    target 1125
  ]
  edge [
    source 8
    target 1126
  ]
  edge [
    source 8
    target 1127
  ]
  edge [
    source 8
    target 1128
  ]
  edge [
    source 8
    target 1129
  ]
  edge [
    source 8
    target 1130
  ]
  edge [
    source 8
    target 1131
  ]
  edge [
    source 8
    target 1132
  ]
  edge [
    source 8
    target 1133
  ]
  edge [
    source 8
    target 1134
  ]
  edge [
    source 8
    target 1135
  ]
  edge [
    source 8
    target 1136
  ]
  edge [
    source 8
    target 1137
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 1138
  ]
  edge [
    source 8
    target 1139
  ]
  edge [
    source 8
    target 1140
  ]
  edge [
    source 8
    target 1141
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 1142
  ]
  edge [
    source 8
    target 1143
  ]
  edge [
    source 8
    target 1144
  ]
  edge [
    source 8
    target 1145
  ]
  edge [
    source 8
    target 1146
  ]
  edge [
    source 8
    target 1147
  ]
  edge [
    source 8
    target 1148
  ]
  edge [
    source 8
    target 1149
  ]
  edge [
    source 8
    target 1150
  ]
  edge [
    source 8
    target 1151
  ]
  edge [
    source 8
    target 1152
  ]
  edge [
    source 8
    target 1153
  ]
  edge [
    source 8
    target 1154
  ]
  edge [
    source 8
    target 1155
  ]
  edge [
    source 8
    target 1156
  ]
  edge [
    source 8
    target 1157
  ]
  edge [
    source 8
    target 1158
  ]
  edge [
    source 8
    target 1159
  ]
  edge [
    source 8
    target 1160
  ]
  edge [
    source 8
    target 1161
  ]
  edge [
    source 8
    target 1162
  ]
  edge [
    source 8
    target 1163
  ]
  edge [
    source 8
    target 1164
  ]
  edge [
    source 8
    target 1165
  ]
  edge [
    source 8
    target 1166
  ]
  edge [
    source 8
    target 1167
  ]
  edge [
    source 8
    target 1168
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 1169
  ]
  edge [
    source 8
    target 1170
  ]
  edge [
    source 8
    target 1171
  ]
  edge [
    source 8
    target 1172
  ]
  edge [
    source 8
    target 1173
  ]
  edge [
    source 8
    target 1174
  ]
  edge [
    source 8
    target 1175
  ]
  edge [
    source 8
    target 1176
  ]
  edge [
    source 8
    target 1177
  ]
  edge [
    source 8
    target 1178
  ]
  edge [
    source 8
    target 1179
  ]
  edge [
    source 8
    target 1180
  ]
  edge [
    source 8
    target 1181
  ]
  edge [
    source 8
    target 1182
  ]
  edge [
    source 8
    target 1183
  ]
  edge [
    source 8
    target 1184
  ]
  edge [
    source 8
    target 1185
  ]
  edge [
    source 8
    target 1186
  ]
  edge [
    source 8
    target 1187
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 1188
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 1189
  ]
  edge [
    source 8
    target 1190
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 1191
  ]
  edge [
    source 8
    target 1192
  ]
  edge [
    source 8
    target 1193
  ]
  edge [
    source 8
    target 1194
  ]
  edge [
    source 8
    target 1195
  ]
  edge [
    source 8
    target 1196
  ]
  edge [
    source 8
    target 1197
  ]
  edge [
    source 8
    target 1198
  ]
  edge [
    source 8
    target 1199
  ]
  edge [
    source 8
    target 1200
  ]
  edge [
    source 8
    target 1201
  ]
  edge [
    source 8
    target 1202
  ]
  edge [
    source 8
    target 1203
  ]
  edge [
    source 8
    target 1204
  ]
  edge [
    source 8
    target 1205
  ]
  edge [
    source 8
    target 1206
  ]
  edge [
    source 8
    target 1207
  ]
  edge [
    source 8
    target 1208
  ]
  edge [
    source 8
    target 1209
  ]
  edge [
    source 8
    target 1210
  ]
  edge [
    source 8
    target 1211
  ]
  edge [
    source 8
    target 1212
  ]
  edge [
    source 8
    target 1213
  ]
  edge [
    source 8
    target 1214
  ]
  edge [
    source 8
    target 1215
  ]
  edge [
    source 8
    target 1216
  ]
  edge [
    source 8
    target 1217
  ]
  edge [
    source 8
    target 1218
  ]
  edge [
    source 8
    target 1219
  ]
  edge [
    source 8
    target 1220
  ]
  edge [
    source 8
    target 1221
  ]
  edge [
    source 8
    target 1222
  ]
  edge [
    source 8
    target 1223
  ]
  edge [
    source 8
    target 1224
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1225
  ]
  edge [
    source 9
    target 1226
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 1227
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 1228
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 1229
  ]
  edge [
    source 9
    target 1230
  ]
  edge [
    source 9
    target 1231
  ]
  edge [
    source 9
    target 1232
  ]
  edge [
    source 9
    target 1233
  ]
  edge [
    source 9
    target 1234
  ]
  edge [
    source 9
    target 1235
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 1236
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 1237
  ]
  edge [
    source 9
    target 1238
  ]
  edge [
    source 9
    target 1239
  ]
  edge [
    source 9
    target 1240
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 1241
  ]
  edge [
    source 9
    target 1242
  ]
  edge [
    source 9
    target 1243
  ]
  edge [
    source 9
    target 1244
  ]
  edge [
    source 9
    target 1245
  ]
  edge [
    source 9
    target 1246
  ]
  edge [
    source 9
    target 1247
  ]
  edge [
    source 9
    target 1248
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 1249
  ]
  edge [
    source 9
    target 1250
  ]
  edge [
    source 9
    target 1251
  ]
  edge [
    source 9
    target 1252
  ]
  edge [
    source 9
    target 1253
  ]
  edge [
    source 9
    target 1254
  ]
  edge [
    source 9
    target 1255
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 1256
  ]
  edge [
    source 9
    target 1257
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 1258
  ]
  edge [
    source 9
    target 1259
  ]
  edge [
    source 9
    target 1260
  ]
  edge [
    source 9
    target 1261
  ]
  edge [
    source 9
    target 1098
  ]
  edge [
    source 9
    target 1262
  ]
  edge [
    source 9
    target 1263
  ]
  edge [
    source 9
    target 1264
  ]
  edge [
    source 9
    target 1265
  ]
  edge [
    source 9
    target 1266
  ]
  edge [
    source 9
    target 1267
  ]
  edge [
    source 9
    target 1268
  ]
  edge [
    source 9
    target 1269
  ]
  edge [
    source 9
    target 1270
  ]
  edge [
    source 9
    target 1271
  ]
  edge [
    source 9
    target 1272
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 1273
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 1274
  ]
  edge [
    source 9
    target 1275
  ]
  edge [
    source 9
    target 1276
  ]
  edge [
    source 9
    target 1277
  ]
  edge [
    source 9
    target 1278
  ]
  edge [
    source 9
    target 1279
  ]
  edge [
    source 9
    target 1280
  ]
  edge [
    source 9
    target 1281
  ]
  edge [
    source 9
    target 1282
  ]
  edge [
    source 9
    target 1283
  ]
  edge [
    source 9
    target 1284
  ]
  edge [
    source 9
    target 1285
  ]
  edge [
    source 9
    target 1286
  ]
  edge [
    source 9
    target 1287
  ]
  edge [
    source 9
    target 1288
  ]
  edge [
    source 9
    target 1289
  ]
  edge [
    source 9
    target 1290
  ]
  edge [
    source 9
    target 1291
  ]
  edge [
    source 9
    target 1292
  ]
  edge [
    source 9
    target 1293
  ]
  edge [
    source 9
    target 1294
  ]
  edge [
    source 9
    target 1295
  ]
  edge [
    source 9
    target 1296
  ]
  edge [
    source 9
    target 1297
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 1298
  ]
  edge [
    source 9
    target 1299
  ]
  edge [
    source 9
    target 1300
  ]
  edge [
    source 9
    target 1301
  ]
  edge [
    source 9
    target 1302
  ]
  edge [
    source 9
    target 1303
  ]
  edge [
    source 9
    target 1304
  ]
  edge [
    source 9
    target 1305
  ]
  edge [
    source 9
    target 1220
  ]
  edge [
    source 9
    target 1306
  ]
  edge [
    source 9
    target 1307
  ]
  edge [
    source 9
    target 1308
  ]
  edge [
    source 9
    target 1309
  ]
  edge [
    source 9
    target 1310
  ]
  edge [
    source 9
    target 1311
  ]
  edge [
    source 9
    target 1312
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 1313
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 1314
  ]
  edge [
    source 9
    target 1315
  ]
  edge [
    source 9
    target 1316
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 1317
  ]
  edge [
    source 9
    target 1318
  ]
  edge [
    source 9
    target 1319
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1320
  ]
  edge [
    source 12
    target 1321
  ]
  edge [
    source 12
    target 1322
  ]
  edge [
    source 12
    target 1323
  ]
  edge [
    source 12
    target 1324
  ]
  edge [
    source 12
    target 1325
  ]
  edge [
    source 12
    target 1326
  ]
  edge [
    source 12
    target 1327
  ]
  edge [
    source 12
    target 1328
  ]
  edge [
    source 12
    target 1329
  ]
  edge [
    source 12
    target 1330
  ]
  edge [
    source 12
    target 1331
  ]
  edge [
    source 12
    target 1332
  ]
  edge [
    source 12
    target 1333
  ]
  edge [
    source 12
    target 1334
  ]
  edge [
    source 12
    target 1335
  ]
  edge [
    source 12
    target 1336
  ]
  edge [
    source 12
    target 1337
  ]
  edge [
    source 12
    target 1338
  ]
  edge [
    source 12
    target 1339
  ]
  edge [
    source 12
    target 1340
  ]
  edge [
    source 12
    target 1341
  ]
  edge [
    source 12
    target 1342
  ]
  edge [
    source 12
    target 1343
  ]
  edge [
    source 12
    target 1344
  ]
  edge [
    source 12
    target 1345
  ]
  edge [
    source 12
    target 1346
  ]
  edge [
    source 12
    target 1347
  ]
  edge [
    source 12
    target 1348
  ]
  edge [
    source 12
    target 1349
  ]
  edge [
    source 12
    target 1350
  ]
  edge [
    source 12
    target 1351
  ]
  edge [
    source 12
    target 1352
  ]
  edge [
    source 12
    target 1353
  ]
  edge [
    source 12
    target 1354
  ]
  edge [
    source 12
    target 1355
  ]
  edge [
    source 12
    target 1356
  ]
  edge [
    source 12
    target 1357
  ]
  edge [
    source 12
    target 1358
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 1359
  ]
  edge [
    source 12
    target 1360
  ]
  edge [
    source 12
    target 1361
  ]
  edge [
    source 12
    target 1362
  ]
  edge [
    source 12
    target 1363
  ]
  edge [
    source 12
    target 1364
  ]
  edge [
    source 12
    target 1365
  ]
  edge [
    source 12
    target 1366
  ]
  edge [
    source 12
    target 1367
  ]
  edge [
    source 12
    target 1368
  ]
  edge [
    source 12
    target 1369
  ]
  edge [
    source 12
    target 1370
  ]
  edge [
    source 12
    target 1371
  ]
  edge [
    source 12
    target 1372
  ]
  edge [
    source 12
    target 1373
  ]
  edge [
    source 12
    target 1374
  ]
  edge [
    source 12
    target 1375
  ]
  edge [
    source 12
    target 1376
  ]
  edge [
    source 12
    target 1377
  ]
  edge [
    source 12
    target 1378
  ]
  edge [
    source 12
    target 1379
  ]
  edge [
    source 12
    target 1380
  ]
  edge [
    source 12
    target 1381
  ]
  edge [
    source 12
    target 1382
  ]
  edge [
    source 12
    target 1383
  ]
  edge [
    source 12
    target 1384
  ]
  edge [
    source 12
    target 1385
  ]
  edge [
    source 12
    target 1386
  ]
  edge [
    source 12
    target 1387
  ]
  edge [
    source 12
    target 1388
  ]
  edge [
    source 12
    target 1389
  ]
  edge [
    source 12
    target 1390
  ]
  edge [
    source 12
    target 1391
  ]
  edge [
    source 12
    target 1392
  ]
  edge [
    source 12
    target 1393
  ]
  edge [
    source 12
    target 1394
  ]
  edge [
    source 12
    target 1395
  ]
  edge [
    source 12
    target 1396
  ]
  edge [
    source 12
    target 1397
  ]
  edge [
    source 12
    target 1398
  ]
  edge [
    source 12
    target 1399
  ]
  edge [
    source 12
    target 1400
  ]
  edge [
    source 12
    target 1401
  ]
  edge [
    source 12
    target 1402
  ]
  edge [
    source 12
    target 1403
  ]
  edge [
    source 12
    target 1404
  ]
  edge [
    source 12
    target 1405
  ]
  edge [
    source 12
    target 1406
  ]
  edge [
    source 12
    target 1407
  ]
  edge [
    source 12
    target 1408
  ]
  edge [
    source 12
    target 1409
  ]
  edge [
    source 12
    target 1410
  ]
  edge [
    source 12
    target 1411
  ]
  edge [
    source 12
    target 1412
  ]
  edge [
    source 12
    target 1413
  ]
  edge [
    source 12
    target 1414
  ]
  edge [
    source 12
    target 1415
  ]
  edge [
    source 12
    target 1416
  ]
  edge [
    source 12
    target 1417
  ]
  edge [
    source 12
    target 1418
  ]
  edge [
    source 12
    target 1419
  ]
  edge [
    source 12
    target 1420
  ]
  edge [
    source 12
    target 1421
  ]
  edge [
    source 12
    target 1422
  ]
  edge [
    source 12
    target 1423
  ]
  edge [
    source 12
    target 1424
  ]
  edge [
    source 12
    target 1425
  ]
  edge [
    source 12
    target 1426
  ]
  edge [
    source 12
    target 1427
  ]
  edge [
    source 12
    target 1428
  ]
  edge [
    source 12
    target 1429
  ]
  edge [
    source 12
    target 1430
  ]
  edge [
    source 12
    target 1431
  ]
  edge [
    source 12
    target 1432
  ]
  edge [
    source 12
    target 1433
  ]
  edge [
    source 12
    target 1434
  ]
  edge [
    source 12
    target 1435
  ]
  edge [
    source 12
    target 1436
  ]
  edge [
    source 12
    target 1437
  ]
  edge [
    source 12
    target 1438
  ]
  edge [
    source 12
    target 1439
  ]
  edge [
    source 12
    target 1440
  ]
  edge [
    source 12
    target 1441
  ]
  edge [
    source 12
    target 1442
  ]
  edge [
    source 12
    target 1443
  ]
  edge [
    source 12
    target 1444
  ]
  edge [
    source 12
    target 1445
  ]
  edge [
    source 12
    target 1446
  ]
  edge [
    source 12
    target 1447
  ]
  edge [
    source 12
    target 1448
  ]
  edge [
    source 12
    target 1449
  ]
  edge [
    source 12
    target 1450
  ]
  edge [
    source 12
    target 1451
  ]
  edge [
    source 12
    target 1452
  ]
  edge [
    source 12
    target 1453
  ]
  edge [
    source 12
    target 1454
  ]
  edge [
    source 12
    target 1455
  ]
  edge [
    source 12
    target 1456
  ]
  edge [
    source 12
    target 1457
  ]
  edge [
    source 12
    target 1458
  ]
  edge [
    source 12
    target 1459
  ]
  edge [
    source 12
    target 1460
  ]
  edge [
    source 12
    target 1461
  ]
  edge [
    source 12
    target 1462
  ]
  edge [
    source 12
    target 1463
  ]
  edge [
    source 12
    target 1464
  ]
  edge [
    source 12
    target 1465
  ]
  edge [
    source 12
    target 1466
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 1467
  ]
  edge [
    source 12
    target 1468
  ]
  edge [
    source 12
    target 1469
  ]
  edge [
    source 12
    target 1470
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 1471
  ]
  edge [
    source 12
    target 1472
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 1473
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 1474
  ]
  edge [
    source 12
    target 1475
  ]
  edge [
    source 12
    target 1476
  ]
  edge [
    source 12
    target 1477
  ]
  edge [
    source 12
    target 1478
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 1479
  ]
  edge [
    source 12
    target 1480
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 1481
  ]
  edge [
    source 12
    target 1482
  ]
  edge [
    source 12
    target 1483
  ]
  edge [
    source 12
    target 1484
  ]
  edge [
    source 12
    target 1485
  ]
  edge [
    source 12
    target 1486
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 1487
  ]
  edge [
    source 12
    target 1488
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 1489
  ]
  edge [
    source 12
    target 1490
  ]
  edge [
    source 12
    target 1491
  ]
  edge [
    source 12
    target 1492
  ]
  edge [
    source 12
    target 1493
  ]
  edge [
    source 12
    target 1494
  ]
  edge [
    source 12
    target 1495
  ]
  edge [
    source 12
    target 1496
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1497
  ]
  edge [
    source 12
    target 1498
  ]
  edge [
    source 12
    target 1499
  ]
  edge [
    source 12
    target 1500
  ]
  edge [
    source 12
    target 1501
  ]
  edge [
    source 12
    target 1502
  ]
  edge [
    source 12
    target 1503
  ]
  edge [
    source 12
    target 1504
  ]
  edge [
    source 12
    target 1505
  ]
  edge [
    source 12
    target 1506
  ]
  edge [
    source 12
    target 1507
  ]
  edge [
    source 12
    target 1508
  ]
  edge [
    source 12
    target 1509
  ]
  edge [
    source 12
    target 1510
  ]
  edge [
    source 12
    target 1511
  ]
  edge [
    source 12
    target 1512
  ]
  edge [
    source 12
    target 1513
  ]
  edge [
    source 12
    target 1514
  ]
  edge [
    source 12
    target 1515
  ]
  edge [
    source 12
    target 1516
  ]
  edge [
    source 12
    target 1517
  ]
  edge [
    source 12
    target 1518
  ]
  edge [
    source 12
    target 1519
  ]
  edge [
    source 12
    target 1520
  ]
  edge [
    source 12
    target 1521
  ]
  edge [
    source 12
    target 1522
  ]
  edge [
    source 12
    target 1523
  ]
  edge [
    source 12
    target 1524
  ]
  edge [
    source 12
    target 1525
  ]
  edge [
    source 12
    target 1526
  ]
  edge [
    source 12
    target 1527
  ]
  edge [
    source 12
    target 1528
  ]
  edge [
    source 12
    target 1529
  ]
  edge [
    source 12
    target 1530
  ]
  edge [
    source 12
    target 1531
  ]
  edge [
    source 12
    target 1532
  ]
  edge [
    source 12
    target 1533
  ]
  edge [
    source 12
    target 1534
  ]
  edge [
    source 12
    target 1535
  ]
  edge [
    source 12
    target 1536
  ]
  edge [
    source 12
    target 1537
  ]
  edge [
    source 12
    target 1538
  ]
  edge [
    source 12
    target 1539
  ]
  edge [
    source 12
    target 1540
  ]
  edge [
    source 12
    target 1541
  ]
  edge [
    source 12
    target 1542
  ]
  edge [
    source 12
    target 1543
  ]
  edge [
    source 12
    target 1544
  ]
  edge [
    source 12
    target 1545
  ]
  edge [
    source 12
    target 1546
  ]
  edge [
    source 12
    target 1547
  ]
  edge [
    source 12
    target 1548
  ]
  edge [
    source 12
    target 1549
  ]
  edge [
    source 12
    target 1550
  ]
  edge [
    source 12
    target 1551
  ]
  edge [
    source 12
    target 1552
  ]
  edge [
    source 12
    target 1553
  ]
  edge [
    source 12
    target 1554
  ]
  edge [
    source 12
    target 1555
  ]
  edge [
    source 12
    target 1556
  ]
  edge [
    source 12
    target 1557
  ]
  edge [
    source 12
    target 1558
  ]
  edge [
    source 12
    target 1559
  ]
  edge [
    source 12
    target 1560
  ]
  edge [
    source 12
    target 1561
  ]
  edge [
    source 12
    target 1562
  ]
  edge [
    source 12
    target 1563
  ]
  edge [
    source 12
    target 1564
  ]
  edge [
    source 12
    target 1565
  ]
  edge [
    source 12
    target 1566
  ]
  edge [
    source 12
    target 1567
  ]
  edge [
    source 12
    target 1568
  ]
  edge [
    source 12
    target 1569
  ]
  edge [
    source 12
    target 1570
  ]
  edge [
    source 12
    target 1571
  ]
  edge [
    source 12
    target 1572
  ]
  edge [
    source 12
    target 1573
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 1574
  ]
  edge [
    source 12
    target 1575
  ]
  edge [
    source 12
    target 1576
  ]
  edge [
    source 12
    target 1577
  ]
  edge [
    source 12
    target 1578
  ]
  edge [
    source 12
    target 1579
  ]
  edge [
    source 12
    target 1580
  ]
  edge [
    source 12
    target 1581
  ]
  edge [
    source 12
    target 1582
  ]
  edge [
    source 12
    target 1583
  ]
  edge [
    source 12
    target 1584
  ]
  edge [
    source 12
    target 1585
  ]
  edge [
    source 12
    target 1586
  ]
  edge [
    source 12
    target 1587
  ]
  edge [
    source 12
    target 1588
  ]
  edge [
    source 12
    target 1589
  ]
  edge [
    source 12
    target 1590
  ]
  edge [
    source 12
    target 1591
  ]
  edge [
    source 12
    target 1592
  ]
  edge [
    source 12
    target 1593
  ]
  edge [
    source 12
    target 1594
  ]
  edge [
    source 12
    target 1595
  ]
  edge [
    source 12
    target 1596
  ]
  edge [
    source 12
    target 1597
  ]
  edge [
    source 12
    target 1598
  ]
  edge [
    source 12
    target 1599
  ]
  edge [
    source 12
    target 1600
  ]
  edge [
    source 12
    target 1601
  ]
  edge [
    source 12
    target 1602
  ]
  edge [
    source 12
    target 1603
  ]
  edge [
    source 12
    target 1604
  ]
  edge [
    source 12
    target 1605
  ]
  edge [
    source 12
    target 1606
  ]
  edge [
    source 12
    target 1607
  ]
  edge [
    source 12
    target 1608
  ]
  edge [
    source 12
    target 1609
  ]
  edge [
    source 12
    target 1610
  ]
  edge [
    source 12
    target 1611
  ]
  edge [
    source 12
    target 1612
  ]
  edge [
    source 12
    target 1613
  ]
  edge [
    source 12
    target 1614
  ]
  edge [
    source 12
    target 1615
  ]
  edge [
    source 12
    target 1616
  ]
  edge [
    source 12
    target 1617
  ]
  edge [
    source 12
    target 1618
  ]
  edge [
    source 12
    target 1619
  ]
  edge [
    source 12
    target 1620
  ]
  edge [
    source 12
    target 1621
  ]
  edge [
    source 12
    target 1622
  ]
  edge [
    source 12
    target 1623
  ]
  edge [
    source 12
    target 1624
  ]
  edge [
    source 12
    target 1625
  ]
  edge [
    source 12
    target 1626
  ]
  edge [
    source 12
    target 1627
  ]
  edge [
    source 12
    target 1628
  ]
  edge [
    source 12
    target 1629
  ]
  edge [
    source 12
    target 1630
  ]
  edge [
    source 12
    target 1631
  ]
  edge [
    source 12
    target 1632
  ]
  edge [
    source 12
    target 1633
  ]
  edge [
    source 12
    target 1634
  ]
  edge [
    source 12
    target 1635
  ]
  edge [
    source 12
    target 1636
  ]
  edge [
    source 12
    target 1637
  ]
  edge [
    source 12
    target 1638
  ]
  edge [
    source 12
    target 1639
  ]
  edge [
    source 12
    target 1640
  ]
  edge [
    source 12
    target 1641
  ]
  edge [
    source 12
    target 1642
  ]
  edge [
    source 12
    target 1643
  ]
  edge [
    source 12
    target 1644
  ]
  edge [
    source 12
    target 1645
  ]
  edge [
    source 12
    target 1097
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1646
  ]
  edge [
    source 15
    target 1647
  ]
  edge [
    source 15
    target 1648
  ]
  edge [
    source 15
    target 1649
  ]
  edge [
    source 15
    target 1650
  ]
  edge [
    source 15
    target 1651
  ]
  edge [
    source 15
    target 1652
  ]
  edge [
    source 15
    target 1653
  ]
  edge [
    source 15
    target 1654
  ]
  edge [
    source 15
    target 1655
  ]
  edge [
    source 15
    target 1656
  ]
  edge [
    source 15
    target 1657
  ]
  edge [
    source 15
    target 1658
  ]
  edge [
    source 15
    target 1659
  ]
  edge [
    source 15
    target 1660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 1661
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1662
  ]
  edge [
    source 16
    target 1663
  ]
  edge [
    source 16
    target 1664
  ]
  edge [
    source 16
    target 1665
  ]
  edge [
    source 16
    target 1666
  ]
  edge [
    source 16
    target 1667
  ]
  edge [
    source 16
    target 1668
  ]
  edge [
    source 16
    target 1669
  ]
  edge [
    source 16
    target 1670
  ]
  edge [
    source 16
    target 1671
  ]
  edge [
    source 16
    target 1672
  ]
  edge [
    source 16
    target 1673
  ]
  edge [
    source 16
    target 1674
  ]
  edge [
    source 16
    target 1675
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 1676
  ]
  edge [
    source 16
    target 1677
  ]
  edge [
    source 16
    target 1678
  ]
  edge [
    source 16
    target 1679
  ]
  edge [
    source 16
    target 1680
  ]
  edge [
    source 16
    target 1681
  ]
  edge [
    source 16
    target 1682
  ]
  edge [
    source 16
    target 1683
  ]
  edge [
    source 16
    target 1684
  ]
  edge [
    source 16
    target 1685
  ]
  edge [
    source 16
    target 1686
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 32
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 1132
  ]
  edge [
    source 17
    target 1687
  ]
  edge [
    source 17
    target 1688
  ]
  edge [
    source 17
    target 1689
  ]
  edge [
    source 17
    target 1690
  ]
  edge [
    source 17
    target 1691
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 1692
  ]
  edge [
    source 17
    target 1693
  ]
  edge [
    source 17
    target 1694
  ]
  edge [
    source 17
    target 1695
  ]
  edge [
    source 17
    target 1696
  ]
  edge [
    source 17
    target 1697
  ]
  edge [
    source 17
    target 1698
  ]
  edge [
    source 17
    target 1699
  ]
  edge [
    source 17
    target 509
  ]
  edge [
    source 17
    target 1700
  ]
  edge [
    source 17
    target 1701
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 1702
  ]
  edge [
    source 17
    target 1703
  ]
  edge [
    source 17
    target 1704
  ]
  edge [
    source 17
    target 1705
  ]
  edge [
    source 17
    target 1706
  ]
  edge [
    source 17
    target 1707
  ]
  edge [
    source 17
    target 1708
  ]
  edge [
    source 17
    target 1709
  ]
  edge [
    source 17
    target 1710
  ]
  edge [
    source 17
    target 1711
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 1712
  ]
  edge [
    source 17
    target 1713
  ]
  edge [
    source 17
    target 298
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 1714
  ]
  edge [
    source 17
    target 1715
  ]
  edge [
    source 17
    target 554
  ]
  edge [
    source 17
    target 555
  ]
  edge [
    source 17
    target 556
  ]
  edge [
    source 17
    target 1716
  ]
  edge [
    source 17
    target 1717
  ]
  edge [
    source 17
    target 1718
  ]
  edge [
    source 17
    target 1719
  ]
  edge [
    source 17
    target 1720
  ]
  edge [
    source 17
    target 1721
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 1722
  ]
  edge [
    source 17
    target 1723
  ]
  edge [
    source 17
    target 1724
  ]
  edge [
    source 17
    target 1725
  ]
  edge [
    source 17
    target 326
  ]
  edge [
    source 17
    target 1726
  ]
  edge [
    source 17
    target 1727
  ]
  edge [
    source 17
    target 1728
  ]
  edge [
    source 17
    target 1729
  ]
  edge [
    source 17
    target 1730
  ]
  edge [
    source 17
    target 1731
  ]
  edge [
    source 17
    target 1732
  ]
  edge [
    source 17
    target 1733
  ]
  edge [
    source 17
    target 1734
  ]
  edge [
    source 17
    target 1735
  ]
  edge [
    source 17
    target 1736
  ]
  edge [
    source 17
    target 1737
  ]
  edge [
    source 17
    target 1738
  ]
  edge [
    source 17
    target 1739
  ]
  edge [
    source 17
    target 1740
  ]
  edge [
    source 17
    target 1741
  ]
  edge [
    source 17
    target 1742
  ]
  edge [
    source 17
    target 575
  ]
  edge [
    source 17
    target 1743
  ]
  edge [
    source 17
    target 1744
  ]
  edge [
    source 17
    target 1745
  ]
  edge [
    source 17
    target 1746
  ]
  edge [
    source 17
    target 1747
  ]
  edge [
    source 17
    target 1748
  ]
  edge [
    source 17
    target 1749
  ]
  edge [
    source 17
    target 1750
  ]
  edge [
    source 17
    target 1751
  ]
  edge [
    source 17
    target 1752
  ]
  edge [
    source 17
    target 510
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 1753
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 1754
  ]
  edge [
    source 17
    target 1755
  ]
  edge [
    source 17
    target 1756
  ]
  edge [
    source 17
    target 1757
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 1758
  ]
  edge [
    source 17
    target 1759
  ]
  edge [
    source 17
    target 1760
  ]
  edge [
    source 17
    target 1761
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 1762
  ]
  edge [
    source 17
    target 1763
  ]
  edge [
    source 17
    target 1764
  ]
  edge [
    source 17
    target 1765
  ]
  edge [
    source 17
    target 1766
  ]
  edge [
    source 17
    target 1767
  ]
  edge [
    source 17
    target 1768
  ]
  edge [
    source 17
    target 1769
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 1770
  ]
  edge [
    source 17
    target 1771
  ]
  edge [
    source 17
    target 1772
  ]
  edge [
    source 17
    target 1773
  ]
  edge [
    source 17
    target 1774
  ]
  edge [
    source 17
    target 1775
  ]
  edge [
    source 17
    target 1776
  ]
  edge [
    source 17
    target 1777
  ]
  edge [
    source 17
    target 1778
  ]
  edge [
    source 17
    target 1779
  ]
  edge [
    source 17
    target 1780
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1781
  ]
  edge [
    source 17
    target 1782
  ]
  edge [
    source 17
    target 1783
  ]
  edge [
    source 17
    target 1784
  ]
  edge [
    source 17
    target 1785
  ]
  edge [
    source 17
    target 1786
  ]
  edge [
    source 17
    target 1787
  ]
  edge [
    source 17
    target 1788
  ]
  edge [
    source 17
    target 1789
  ]
  edge [
    source 17
    target 1790
  ]
  edge [
    source 17
    target 1791
  ]
  edge [
    source 17
    target 1792
  ]
  edge [
    source 17
    target 1793
  ]
  edge [
    source 17
    target 1794
  ]
  edge [
    source 17
    target 1795
  ]
  edge [
    source 17
    target 1796
  ]
  edge [
    source 17
    target 1797
  ]
  edge [
    source 17
    target 1798
  ]
  edge [
    source 17
    target 1799
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1800
  ]
  edge [
    source 17
    target 276
  ]
  edge [
    source 17
    target 1801
  ]
  edge [
    source 17
    target 1802
  ]
  edge [
    source 17
    target 1803
  ]
  edge [
    source 17
    target 1804
  ]
  edge [
    source 17
    target 1805
  ]
  edge [
    source 17
    target 1806
  ]
  edge [
    source 17
    target 1807
  ]
  edge [
    source 17
    target 1808
  ]
  edge [
    source 17
    target 1809
  ]
  edge [
    source 17
    target 1810
  ]
  edge [
    source 17
    target 1811
  ]
  edge [
    source 17
    target 1812
  ]
  edge [
    source 17
    target 1813
  ]
  edge [
    source 17
    target 1814
  ]
  edge [
    source 17
    target 1815
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 1816
  ]
  edge [
    source 17
    target 1817
  ]
  edge [
    source 17
    target 1818
  ]
  edge [
    source 17
    target 1819
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 1820
  ]
  edge [
    source 17
    target 1821
  ]
  edge [
    source 17
    target 1822
  ]
  edge [
    source 17
    target 1823
  ]
  edge [
    source 17
    target 1824
  ]
  edge [
    source 17
    target 1210
  ]
  edge [
    source 17
    target 1825
  ]
  edge [
    source 17
    target 1826
  ]
  edge [
    source 17
    target 1827
  ]
  edge [
    source 17
    target 1828
  ]
  edge [
    source 17
    target 1829
  ]
  edge [
    source 17
    target 1830
  ]
  edge [
    source 17
    target 549
  ]
  edge [
    source 17
    target 1831
  ]
  edge [
    source 17
    target 1832
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 1833
  ]
  edge [
    source 17
    target 1834
  ]
  edge [
    source 17
    target 1835
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 1836
  ]
  edge [
    source 17
    target 1837
  ]
  edge [
    source 17
    target 1838
  ]
  edge [
    source 17
    target 1839
  ]
  edge [
    source 17
    target 1840
  ]
  edge [
    source 17
    target 1841
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 33
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 442
  ]
  edge [
    source 20
    target 1842
  ]
  edge [
    source 20
    target 1843
  ]
  edge [
    source 20
    target 1844
  ]
  edge [
    source 20
    target 1845
  ]
  edge [
    source 20
    target 1846
  ]
  edge [
    source 20
    target 1847
  ]
  edge [
    source 20
    target 1848
  ]
  edge [
    source 20
    target 721
  ]
  edge [
    source 20
    target 1782
  ]
  edge [
    source 20
    target 1849
  ]
  edge [
    source 20
    target 1850
  ]
  edge [
    source 20
    target 1851
  ]
  edge [
    source 20
    target 1852
  ]
  edge [
    source 20
    target 1116
  ]
  edge [
    source 20
    target 1853
  ]
  edge [
    source 20
    target 1854
  ]
  edge [
    source 20
    target 1855
  ]
  edge [
    source 20
    target 1856
  ]
  edge [
    source 20
    target 1857
  ]
  edge [
    source 20
    target 1858
  ]
  edge [
    source 20
    target 1859
  ]
  edge [
    source 20
    target 1860
  ]
  edge [
    source 20
    target 1861
  ]
  edge [
    source 20
    target 556
  ]
  edge [
    source 20
    target 1862
  ]
  edge [
    source 20
    target 1863
  ]
  edge [
    source 20
    target 1864
  ]
  edge [
    source 20
    target 509
  ]
  edge [
    source 20
    target 1865
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 1866
  ]
  edge [
    source 20
    target 1867
  ]
  edge [
    source 20
    target 513
  ]
  edge [
    source 20
    target 440
  ]
  edge [
    source 20
    target 1868
  ]
  edge [
    source 20
    target 1869
  ]
  edge [
    source 20
    target 1870
  ]
  edge [
    source 20
    target 1871
  ]
  edge [
    source 20
    target 1872
  ]
  edge [
    source 20
    target 1873
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 713
  ]
  edge [
    source 20
    target 714
  ]
  edge [
    source 20
    target 715
  ]
  edge [
    source 20
    target 716
  ]
  edge [
    source 20
    target 717
  ]
  edge [
    source 20
    target 718
  ]
  edge [
    source 20
    target 1874
  ]
  edge [
    source 20
    target 784
  ]
  edge [
    source 20
    target 1875
  ]
  edge [
    source 20
    target 1876
  ]
  edge [
    source 20
    target 1877
  ]
  edge [
    source 20
    target 1878
  ]
  edge [
    source 20
    target 1879
  ]
  edge [
    source 20
    target 1880
  ]
  edge [
    source 20
    target 933
  ]
  edge [
    source 20
    target 554
  ]
  edge [
    source 20
    target 1881
  ]
  edge [
    source 20
    target 1882
  ]
  edge [
    source 20
    target 1883
  ]
  edge [
    source 20
    target 1884
  ]
  edge [
    source 20
    target 1885
  ]
  edge [
    source 20
    target 1886
  ]
  edge [
    source 20
    target 1887
  ]
  edge [
    source 20
    target 544
  ]
  edge [
    source 20
    target 545
  ]
  edge [
    source 20
    target 546
  ]
  edge [
    source 20
    target 547
  ]
  edge [
    source 20
    target 548
  ]
  edge [
    source 20
    target 549
  ]
  edge [
    source 20
    target 550
  ]
  edge [
    source 20
    target 551
  ]
  edge [
    source 20
    target 552
  ]
  edge [
    source 20
    target 553
  ]
  edge [
    source 20
    target 555
  ]
  edge [
    source 20
    target 1888
  ]
  edge [
    source 20
    target 1889
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 1890
  ]
  edge [
    source 20
    target 1891
  ]
  edge [
    source 20
    target 1892
  ]
  edge [
    source 20
    target 1893
  ]
  edge [
    source 20
    target 1894
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 1895
  ]
  edge [
    source 20
    target 52
  ]
  edge [
    source 20
    target 1896
  ]
  edge [
    source 20
    target 1897
  ]
  edge [
    source 20
    target 1898
  ]
  edge [
    source 20
    target 1899
  ]
  edge [
    source 20
    target 398
  ]
  edge [
    source 20
    target 1900
  ]
  edge [
    source 20
    target 1901
  ]
  edge [
    source 20
    target 1902
  ]
  edge [
    source 20
    target 1903
  ]
  edge [
    source 20
    target 1904
  ]
  edge [
    source 20
    target 1905
  ]
  edge [
    source 20
    target 1906
  ]
  edge [
    source 20
    target 454
  ]
  edge [
    source 20
    target 1907
  ]
  edge [
    source 20
    target 1908
  ]
  edge [
    source 20
    target 1909
  ]
  edge [
    source 20
    target 1910
  ]
  edge [
    source 20
    target 1911
  ]
  edge [
    source 20
    target 1912
  ]
  edge [
    source 20
    target 1913
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 1914
  ]
  edge [
    source 20
    target 1118
  ]
  edge [
    source 20
    target 1915
  ]
  edge [
    source 20
    target 425
  ]
  edge [
    source 20
    target 1916
  ]
  edge [
    source 20
    target 1917
  ]
  edge [
    source 20
    target 1918
  ]
  edge [
    source 20
    target 1919
  ]
  edge [
    source 20
    target 1920
  ]
  edge [
    source 20
    target 1921
  ]
  edge [
    source 20
    target 1922
  ]
  edge [
    source 20
    target 1923
  ]
  edge [
    source 20
    target 1924
  ]
  edge [
    source 20
    target 1925
  ]
  edge [
    source 20
    target 1926
  ]
  edge [
    source 20
    target 446
  ]
  edge [
    source 20
    target 1780
  ]
  edge [
    source 20
    target 1702
  ]
  edge [
    source 20
    target 1927
  ]
  edge [
    source 20
    target 1928
  ]
  edge [
    source 20
    target 1929
  ]
  edge [
    source 20
    target 1930
  ]
  edge [
    source 20
    target 1931
  ]
  edge [
    source 20
    target 1932
  ]
  edge [
    source 20
    target 1933
  ]
  edge [
    source 20
    target 1934
  ]
  edge [
    source 20
    target 1778
  ]
  edge [
    source 20
    target 1935
  ]
  edge [
    source 20
    target 326
  ]
  edge [
    source 20
    target 1936
  ]
  edge [
    source 20
    target 1937
  ]
  edge [
    source 20
    target 1938
  ]
  edge [
    source 20
    target 1939
  ]
  edge [
    source 20
    target 1940
  ]
  edge [
    source 20
    target 1941
  ]
  edge [
    source 20
    target 281
  ]
  edge [
    source 20
    target 1942
  ]
  edge [
    source 20
    target 1943
  ]
  edge [
    source 20
    target 1776
  ]
  edge [
    source 20
    target 1944
  ]
  edge [
    source 20
    target 1945
  ]
  edge [
    source 20
    target 1946
  ]
  edge [
    source 20
    target 1947
  ]
  edge [
    source 20
    target 1948
  ]
  edge [
    source 20
    target 1949
  ]
  edge [
    source 20
    target 1950
  ]
  edge [
    source 20
    target 1951
  ]
  edge [
    source 20
    target 1952
  ]
  edge [
    source 20
    target 1953
  ]
  edge [
    source 20
    target 1954
  ]
  edge [
    source 20
    target 1955
  ]
  edge [
    source 20
    target 1956
  ]
  edge [
    source 20
    target 660
  ]
  edge [
    source 20
    target 663
  ]
  edge [
    source 20
    target 1957
  ]
  edge [
    source 20
    target 1958
  ]
  edge [
    source 20
    target 634
  ]
  edge [
    source 20
    target 1959
  ]
  edge [
    source 20
    target 1803
  ]
  edge [
    source 20
    target 689
  ]
  edge [
    source 20
    target 1960
  ]
  edge [
    source 20
    target 1961
  ]
  edge [
    source 20
    target 1962
  ]
  edge [
    source 20
    target 1963
  ]
  edge [
    source 20
    target 1964
  ]
  edge [
    source 20
    target 1775
  ]
  edge [
    source 20
    target 1965
  ]
  edge [
    source 20
    target 1966
  ]
  edge [
    source 20
    target 1967
  ]
  edge [
    source 20
    target 1968
  ]
  edge [
    source 20
    target 1969
  ]
  edge [
    source 20
    target 924
  ]
  edge [
    source 20
    target 1970
  ]
  edge [
    source 20
    target 1971
  ]
  edge [
    source 20
    target 925
  ]
  edge [
    source 20
    target 1972
  ]
  edge [
    source 20
    target 1973
  ]
  edge [
    source 20
    target 1974
  ]
  edge [
    source 20
    target 1975
  ]
  edge [
    source 20
    target 1976
  ]
  edge [
    source 20
    target 1977
  ]
  edge [
    source 20
    target 1978
  ]
  edge [
    source 20
    target 903
  ]
  edge [
    source 20
    target 1979
  ]
  edge [
    source 20
    target 1980
  ]
  edge [
    source 20
    target 1981
  ]
  edge [
    source 20
    target 1098
  ]
  edge [
    source 20
    target 1982
  ]
  edge [
    source 20
    target 934
  ]
  edge [
    source 20
    target 1983
  ]
  edge [
    source 20
    target 1984
  ]
  edge [
    source 20
    target 1985
  ]
  edge [
    source 20
    target 1986
  ]
  edge [
    source 20
    target 1987
  ]
  edge [
    source 20
    target 1988
  ]
  edge [
    source 20
    target 1989
  ]
  edge [
    source 20
    target 1990
  ]
  edge [
    source 20
    target 1991
  ]
  edge [
    source 20
    target 1992
  ]
  edge [
    source 20
    target 1993
  ]
  edge [
    source 20
    target 1994
  ]
  edge [
    source 20
    target 1995
  ]
  edge [
    source 20
    target 1996
  ]
  edge [
    source 20
    target 1997
  ]
  edge [
    source 20
    target 1998
  ]
  edge [
    source 20
    target 1999
  ]
  edge [
    source 20
    target 2000
  ]
  edge [
    source 20
    target 2001
  ]
  edge [
    source 20
    target 2002
  ]
  edge [
    source 20
    target 2003
  ]
  edge [
    source 20
    target 2004
  ]
  edge [
    source 20
    target 2005
  ]
  edge [
    source 20
    target 2006
  ]
  edge [
    source 20
    target 2007
  ]
  edge [
    source 20
    target 2008
  ]
  edge [
    source 20
    target 2009
  ]
  edge [
    source 20
    target 35
  ]
  edge [
    source 20
    target 42
  ]
  edge [
    source 20
    target 48
  ]
  edge [
    source 21
    target 2010
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 2011
  ]
  edge [
    source 21
    target 2012
  ]
  edge [
    source 21
    target 2013
  ]
  edge [
    source 21
    target 2014
  ]
  edge [
    source 21
    target 2002
  ]
  edge [
    source 21
    target 2015
  ]
  edge [
    source 21
    target 2016
  ]
  edge [
    source 21
    target 721
  ]
  edge [
    source 21
    target 2017
  ]
  edge [
    source 21
    target 449
  ]
  edge [
    source 21
    target 2018
  ]
  edge [
    source 21
    target 2019
  ]
  edge [
    source 21
    target 1643
  ]
  edge [
    source 21
    target 547
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 2020
  ]
  edge [
    source 21
    target 2021
  ]
  edge [
    source 21
    target 2022
  ]
  edge [
    source 21
    target 2023
  ]
  edge [
    source 21
    target 2024
  ]
  edge [
    source 21
    target 2025
  ]
  edge [
    source 21
    target 2026
  ]
  edge [
    source 21
    target 735
  ]
  edge [
    source 21
    target 2027
  ]
  edge [
    source 21
    target 454
  ]
  edge [
    source 21
    target 2028
  ]
  edge [
    source 21
    target 2029
  ]
  edge [
    source 21
    target 1853
  ]
  edge [
    source 21
    target 1881
  ]
  edge [
    source 21
    target 1882
  ]
  edge [
    source 21
    target 1883
  ]
  edge [
    source 21
    target 1884
  ]
  edge [
    source 21
    target 1885
  ]
  edge [
    source 21
    target 1886
  ]
  edge [
    source 21
    target 714
  ]
  edge [
    source 21
    target 1887
  ]
  edge [
    source 21
    target 2030
  ]
  edge [
    source 21
    target 2031
  ]
  edge [
    source 21
    target 445
  ]
  edge [
    source 21
    target 2032
  ]
  edge [
    source 21
    target 413
  ]
  edge [
    source 21
    target 453
  ]
  edge [
    source 21
    target 2033
  ]
  edge [
    source 21
    target 130
  ]
  edge [
    source 21
    target 2034
  ]
  edge [
    source 21
    target 1852
  ]
  edge [
    source 21
    target 398
  ]
  edge [
    source 21
    target 444
  ]
  edge [
    source 21
    target 519
  ]
  edge [
    source 21
    target 125
  ]
  edge [
    source 21
    target 2035
  ]
  edge [
    source 21
    target 2036
  ]
  edge [
    source 21
    target 2037
  ]
  edge [
    source 21
    target 2038
  ]
  edge [
    source 21
    target 2039
  ]
  edge [
    source 21
    target 2040
  ]
  edge [
    source 21
    target 2041
  ]
  edge [
    source 21
    target 2042
  ]
  edge [
    source 21
    target 2043
  ]
  edge [
    source 21
    target 2044
  ]
  edge [
    source 21
    target 456
  ]
  edge [
    source 21
    target 2045
  ]
  edge [
    source 21
    target 443
  ]
  edge [
    source 21
    target 448
  ]
  edge [
    source 21
    target 2046
  ]
  edge [
    source 21
    target 2047
  ]
  edge [
    source 21
    target 1981
  ]
  edge [
    source 21
    target 2048
  ]
  edge [
    source 21
    target 446
  ]
  edge [
    source 21
    target 447
  ]
  edge [
    source 21
    target 450
  ]
  edge [
    source 21
    target 451
  ]
  edge [
    source 21
    target 2049
  ]
  edge [
    source 21
    target 452
  ]
  edge [
    source 21
    target 2050
  ]
  edge [
    source 21
    target 2051
  ]
  edge [
    source 21
    target 2052
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 2053
  ]
  edge [
    source 21
    target 1906
  ]
  edge [
    source 21
    target 2054
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 52
  ]
  edge [
    source 21
    target 442
  ]
  edge [
    source 21
    target 1842
  ]
  edge [
    source 21
    target 1843
  ]
  edge [
    source 21
    target 1844
  ]
  edge [
    source 21
    target 1845
  ]
  edge [
    source 21
    target 1846
  ]
  edge [
    source 21
    target 1847
  ]
  edge [
    source 21
    target 1848
  ]
  edge [
    source 21
    target 1782
  ]
  edge [
    source 21
    target 1849
  ]
  edge [
    source 21
    target 1850
  ]
  edge [
    source 21
    target 1851
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1854
  ]
  edge [
    source 21
    target 1855
  ]
  edge [
    source 21
    target 1856
  ]
  edge [
    source 21
    target 1857
  ]
  edge [
    source 21
    target 1858
  ]
  edge [
    source 21
    target 1859
  ]
  edge [
    source 21
    target 1860
  ]
  edge [
    source 21
    target 1861
  ]
  edge [
    source 21
    target 556
  ]
  edge [
    source 21
    target 1862
  ]
  edge [
    source 21
    target 1863
  ]
  edge [
    source 21
    target 1864
  ]
  edge [
    source 21
    target 509
  ]
  edge [
    source 21
    target 1865
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 1866
  ]
  edge [
    source 21
    target 1867
  ]
  edge [
    source 21
    target 513
  ]
  edge [
    source 21
    target 440
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 2055
  ]
  edge [
    source 21
    target 2056
  ]
  edge [
    source 21
    target 2057
  ]
  edge [
    source 21
    target 2058
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 2059
  ]
  edge [
    source 22
    target 458
  ]
  edge [
    source 22
    target 2060
  ]
  edge [
    source 22
    target 2061
  ]
  edge [
    source 22
    target 2062
  ]
  edge [
    source 22
    target 765
  ]
  edge [
    source 22
    target 2063
  ]
  edge [
    source 22
    target 2064
  ]
  edge [
    source 22
    target 919
  ]
  edge [
    source 22
    target 2065
  ]
  edge [
    source 22
    target 2066
  ]
  edge [
    source 22
    target 2067
  ]
  edge [
    source 22
    target 2068
  ]
  edge [
    source 22
    target 2069
  ]
  edge [
    source 22
    target 2070
  ]
  edge [
    source 22
    target 2071
  ]
  edge [
    source 22
    target 2072
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 2073
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 2074
  ]
  edge [
    source 23
    target 2075
  ]
  edge [
    source 23
    target 2076
  ]
  edge [
    source 23
    target 2077
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 549
  ]
  edge [
    source 23
    target 2078
  ]
  edge [
    source 23
    target 2079
  ]
  edge [
    source 23
    target 1839
  ]
  edge [
    source 23
    target 2080
  ]
  edge [
    source 23
    target 2081
  ]
  edge [
    source 23
    target 2082
  ]
  edge [
    source 23
    target 2083
  ]
  edge [
    source 23
    target 2084
  ]
  edge [
    source 23
    target 2085
  ]
  edge [
    source 23
    target 2086
  ]
  edge [
    source 23
    target 2087
  ]
  edge [
    source 23
    target 2088
  ]
  edge [
    source 23
    target 2089
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 2090
  ]
  edge [
    source 24
    target 2091
  ]
  edge [
    source 24
    target 2092
  ]
  edge [
    source 24
    target 2093
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 51
  ]
  edge [
    source 26
    target 445
  ]
  edge [
    source 26
    target 2094
  ]
  edge [
    source 26
    target 453
  ]
  edge [
    source 26
    target 130
  ]
  edge [
    source 26
    target 449
  ]
  edge [
    source 26
    target 2095
  ]
  edge [
    source 26
    target 403
  ]
  edge [
    source 26
    target 398
  ]
  edge [
    source 26
    target 444
  ]
  edge [
    source 26
    target 2096
  ]
  edge [
    source 26
    target 125
  ]
  edge [
    source 26
    target 2097
  ]
  edge [
    source 26
    target 2098
  ]
  edge [
    source 26
    target 1540
  ]
  edge [
    source 26
    target 456
  ]
  edge [
    source 26
    target 443
  ]
  edge [
    source 26
    target 448
  ]
  edge [
    source 26
    target 920
  ]
  edge [
    source 26
    target 2099
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 2100
  ]
  edge [
    source 26
    target 2101
  ]
  edge [
    source 26
    target 2102
  ]
  edge [
    source 26
    target 446
  ]
  edge [
    source 26
    target 447
  ]
  edge [
    source 26
    target 450
  ]
  edge [
    source 26
    target 451
  ]
  edge [
    source 26
    target 452
  ]
  edge [
    source 26
    target 2103
  ]
  edge [
    source 26
    target 1025
  ]
  edge [
    source 26
    target 2053
  ]
  edge [
    source 26
    target 2104
  ]
  edge [
    source 26
    target 2105
  ]
  edge [
    source 26
    target 2106
  ]
  edge [
    source 26
    target 2107
  ]
  edge [
    source 26
    target 2108
  ]
  edge [
    source 26
    target 2109
  ]
  edge [
    source 26
    target 2110
  ]
  edge [
    source 26
    target 2111
  ]
  edge [
    source 26
    target 2112
  ]
  edge [
    source 26
    target 2113
  ]
  edge [
    source 26
    target 2114
  ]
  edge [
    source 26
    target 2115
  ]
  edge [
    source 26
    target 2116
  ]
  edge [
    source 26
    target 2117
  ]
  edge [
    source 26
    target 2118
  ]
  edge [
    source 26
    target 2119
  ]
  edge [
    source 26
    target 2120
  ]
  edge [
    source 26
    target 2121
  ]
  edge [
    source 26
    target 2122
  ]
  edge [
    source 26
    target 2123
  ]
  edge [
    source 26
    target 1319
  ]
  edge [
    source 26
    target 2124
  ]
  edge [
    source 26
    target 404
  ]
  edge [
    source 26
    target 405
  ]
  edge [
    source 26
    target 406
  ]
  edge [
    source 26
    target 407
  ]
  edge [
    source 26
    target 408
  ]
  edge [
    source 26
    target 409
  ]
  edge [
    source 26
    target 410
  ]
  edge [
    source 26
    target 411
  ]
  edge [
    source 26
    target 412
  ]
  edge [
    source 26
    target 413
  ]
  edge [
    source 26
    target 414
  ]
  edge [
    source 26
    target 415
  ]
  edge [
    source 26
    target 416
  ]
  edge [
    source 26
    target 417
  ]
  edge [
    source 26
    target 442
  ]
  edge [
    source 26
    target 454
  ]
  edge [
    source 26
    target 455
  ]
  edge [
    source 26
    target 2125
  ]
  edge [
    source 26
    target 2126
  ]
  edge [
    source 26
    target 2127
  ]
  edge [
    source 26
    target 858
  ]
  edge [
    source 26
    target 2128
  ]
  edge [
    source 26
    target 2129
  ]
  edge [
    source 26
    target 2130
  ]
  edge [
    source 26
    target 2131
  ]
  edge [
    source 26
    target 2132
  ]
  edge [
    source 26
    target 2133
  ]
  edge [
    source 26
    target 2134
  ]
  edge [
    source 26
    target 2135
  ]
  edge [
    source 26
    target 2136
  ]
  edge [
    source 26
    target 2137
  ]
  edge [
    source 26
    target 2138
  ]
  edge [
    source 26
    target 921
  ]
  edge [
    source 26
    target 2139
  ]
  edge [
    source 26
    target 1830
  ]
  edge [
    source 26
    target 1826
  ]
  edge [
    source 26
    target 924
  ]
  edge [
    source 26
    target 1841
  ]
  edge [
    source 26
    target 2140
  ]
  edge [
    source 26
    target 1828
  ]
  edge [
    source 26
    target 2141
  ]
  edge [
    source 26
    target 326
  ]
  edge [
    source 26
    target 2142
  ]
  edge [
    source 26
    target 1652
  ]
  edge [
    source 26
    target 2143
  ]
  edge [
    source 26
    target 2144
  ]
  edge [
    source 26
    target 2145
  ]
  edge [
    source 26
    target 2146
  ]
  edge [
    source 26
    target 1315
  ]
  edge [
    source 26
    target 2147
  ]
  edge [
    source 26
    target 2148
  ]
  edge [
    source 26
    target 2149
  ]
  edge [
    source 26
    target 2150
  ]
  edge [
    source 26
    target 2151
  ]
  edge [
    source 26
    target 2152
  ]
  edge [
    source 26
    target 2153
  ]
  edge [
    source 26
    target 2154
  ]
  edge [
    source 26
    target 2155
  ]
  edge [
    source 26
    target 2156
  ]
  edge [
    source 26
    target 2157
  ]
  edge [
    source 26
    target 2158
  ]
  edge [
    source 26
    target 2159
  ]
  edge [
    source 26
    target 2160
  ]
  edge [
    source 26
    target 2161
  ]
  edge [
    source 26
    target 2162
  ]
  edge [
    source 26
    target 2163
  ]
  edge [
    source 26
    target 2164
  ]
  edge [
    source 26
    target 2165
  ]
  edge [
    source 26
    target 1890
  ]
  edge [
    source 26
    target 2166
  ]
  edge [
    source 26
    target 884
  ]
  edge [
    source 26
    target 2167
  ]
  edge [
    source 26
    target 462
  ]
  edge [
    source 26
    target 2168
  ]
  edge [
    source 26
    target 2169
  ]
  edge [
    source 26
    target 2170
  ]
  edge [
    source 26
    target 2171
  ]
  edge [
    source 26
    target 2172
  ]
  edge [
    source 26
    target 2173
  ]
  edge [
    source 26
    target 1115
  ]
  edge [
    source 26
    target 2174
  ]
  edge [
    source 26
    target 2175
  ]
  edge [
    source 26
    target 1981
  ]
  edge [
    source 26
    target 556
  ]
  edge [
    source 26
    target 1696
  ]
  edge [
    source 26
    target 2176
  ]
  edge [
    source 26
    target 2177
  ]
  edge [
    source 26
    target 2178
  ]
  edge [
    source 26
    target 1929
  ]
  edge [
    source 26
    target 1230
  ]
  edge [
    source 26
    target 2179
  ]
  edge [
    source 26
    target 1693
  ]
  edge [
    source 26
    target 2180
  ]
  edge [
    source 26
    target 1778
  ]
  edge [
    source 26
    target 2181
  ]
  edge [
    source 26
    target 1691
  ]
  edge [
    source 26
    target 2182
  ]
  edge [
    source 26
    target 2183
  ]
  edge [
    source 26
    target 2184
  ]
  edge [
    source 26
    target 2185
  ]
  edge [
    source 26
    target 2186
  ]
  edge [
    source 26
    target 2187
  ]
  edge [
    source 26
    target 2030
  ]
  edge [
    source 26
    target 2031
  ]
  edge [
    source 26
    target 2032
  ]
  edge [
    source 26
    target 2033
  ]
  edge [
    source 26
    target 2034
  ]
  edge [
    source 26
    target 1852
  ]
  edge [
    source 26
    target 519
  ]
  edge [
    source 26
    target 2035
  ]
  edge [
    source 26
    target 2036
  ]
  edge [
    source 26
    target 2037
  ]
  edge [
    source 26
    target 2038
  ]
  edge [
    source 26
    target 2039
  ]
  edge [
    source 26
    target 2040
  ]
  edge [
    source 26
    target 2041
  ]
  edge [
    source 26
    target 2042
  ]
  edge [
    source 26
    target 2043
  ]
  edge [
    source 26
    target 2044
  ]
  edge [
    source 26
    target 2045
  ]
  edge [
    source 26
    target 2002
  ]
  edge [
    source 26
    target 2046
  ]
  edge [
    source 26
    target 2047
  ]
  edge [
    source 26
    target 2048
  ]
  edge [
    source 26
    target 2049
  ]
  edge [
    source 26
    target 2050
  ]
  edge [
    source 26
    target 2051
  ]
  edge [
    source 26
    target 2188
  ]
  edge [
    source 26
    target 2189
  ]
  edge [
    source 26
    target 607
  ]
  edge [
    source 26
    target 2190
  ]
  edge [
    source 26
    target 2191
  ]
  edge [
    source 26
    target 2192
  ]
  edge [
    source 26
    target 2193
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 2194
  ]
  edge [
    source 26
    target 2195
  ]
  edge [
    source 26
    target 2196
  ]
  edge [
    source 26
    target 2197
  ]
  edge [
    source 26
    target 2198
  ]
  edge [
    source 26
    target 2199
  ]
  edge [
    source 26
    target 1292
  ]
  edge [
    source 26
    target 2200
  ]
  edge [
    source 26
    target 218
  ]
  edge [
    source 26
    target 2201
  ]
  edge [
    source 26
    target 2202
  ]
  edge [
    source 26
    target 2203
  ]
  edge [
    source 26
    target 2204
  ]
  edge [
    source 26
    target 2205
  ]
  edge [
    source 26
    target 2206
  ]
  edge [
    source 26
    target 2207
  ]
  edge [
    source 26
    target 2208
  ]
  edge [
    source 26
    target 2209
  ]
  edge [
    source 26
    target 253
  ]
  edge [
    source 26
    target 2210
  ]
  edge [
    source 26
    target 2211
  ]
  edge [
    source 26
    target 2212
  ]
  edge [
    source 26
    target 2213
  ]
  edge [
    source 26
    target 2214
  ]
  edge [
    source 26
    target 2215
  ]
  edge [
    source 26
    target 1188
  ]
  edge [
    source 26
    target 2216
  ]
  edge [
    source 26
    target 2217
  ]
  edge [
    source 26
    target 2218
  ]
  edge [
    source 26
    target 2219
  ]
  edge [
    source 26
    target 549
  ]
  edge [
    source 26
    target 2220
  ]
  edge [
    source 26
    target 2221
  ]
  edge [
    source 26
    target 1274
  ]
  edge [
    source 26
    target 2222
  ]
  edge [
    source 26
    target 2223
  ]
  edge [
    source 26
    target 2224
  ]
  edge [
    source 26
    target 2225
  ]
  edge [
    source 26
    target 2226
  ]
  edge [
    source 26
    target 2227
  ]
  edge [
    source 26
    target 2228
  ]
  edge [
    source 26
    target 2229
  ]
  edge [
    source 26
    target 2230
  ]
  edge [
    source 26
    target 2231
  ]
  edge [
    source 26
    target 2232
  ]
  edge [
    source 26
    target 2233
  ]
  edge [
    source 26
    target 2234
  ]
  edge [
    source 26
    target 2235
  ]
  edge [
    source 26
    target 2236
  ]
  edge [
    source 26
    target 2237
  ]
  edge [
    source 26
    target 2238
  ]
  edge [
    source 26
    target 2239
  ]
  edge [
    source 26
    target 2240
  ]
  edge [
    source 26
    target 2241
  ]
  edge [
    source 26
    target 2242
  ]
  edge [
    source 26
    target 2243
  ]
  edge [
    source 26
    target 2244
  ]
  edge [
    source 26
    target 2245
  ]
  edge [
    source 26
    target 2246
  ]
  edge [
    source 26
    target 2247
  ]
  edge [
    source 26
    target 2248
  ]
  edge [
    source 26
    target 2249
  ]
  edge [
    source 26
    target 2250
  ]
  edge [
    source 26
    target 2251
  ]
  edge [
    source 26
    target 2252
  ]
  edge [
    source 26
    target 195
  ]
  edge [
    source 26
    target 2253
  ]
  edge [
    source 26
    target 1038
  ]
  edge [
    source 26
    target 2254
  ]
  edge [
    source 26
    target 2255
  ]
  edge [
    source 26
    target 2256
  ]
  edge [
    source 26
    target 2257
  ]
  edge [
    source 26
    target 2258
  ]
  edge [
    source 26
    target 2259
  ]
  edge [
    source 26
    target 2260
  ]
  edge [
    source 26
    target 1875
  ]
  edge [
    source 26
    target 2261
  ]
  edge [
    source 26
    target 2262
  ]
  edge [
    source 26
    target 472
  ]
  edge [
    source 26
    target 2263
  ]
  edge [
    source 26
    target 643
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 52
  ]
  edge [
    source 27
    target 2264
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 2265
  ]
  edge [
    source 28
    target 2266
  ]
  edge [
    source 28
    target 2267
  ]
  edge [
    source 28
    target 2268
  ]
  edge [
    source 28
    target 2269
  ]
  edge [
    source 28
    target 2270
  ]
  edge [
    source 28
    target 2271
  ]
  edge [
    source 28
    target 2272
  ]
  edge [
    source 28
    target 2273
  ]
  edge [
    source 28
    target 2274
  ]
  edge [
    source 28
    target 2275
  ]
  edge [
    source 28
    target 2276
  ]
  edge [
    source 28
    target 2277
  ]
  edge [
    source 28
    target 2278
  ]
  edge [
    source 28
    target 2279
  ]
  edge [
    source 28
    target 2280
  ]
  edge [
    source 28
    target 1753
  ]
  edge [
    source 28
    target 2281
  ]
  edge [
    source 28
    target 2282
  ]
  edge [
    source 28
    target 2283
  ]
  edge [
    source 28
    target 2284
  ]
  edge [
    source 28
    target 2285
  ]
  edge [
    source 28
    target 2286
  ]
  edge [
    source 28
    target 2287
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 2288
  ]
  edge [
    source 28
    target 2289
  ]
  edge [
    source 28
    target 2290
  ]
  edge [
    source 28
    target 2291
  ]
  edge [
    source 28
    target 2292
  ]
  edge [
    source 28
    target 2293
  ]
  edge [
    source 28
    target 2294
  ]
  edge [
    source 28
    target 2295
  ]
  edge [
    source 28
    target 2296
  ]
  edge [
    source 28
    target 2297
  ]
  edge [
    source 28
    target 2298
  ]
  edge [
    source 28
    target 2299
  ]
  edge [
    source 28
    target 2300
  ]
  edge [
    source 28
    target 2301
  ]
  edge [
    source 28
    target 2302
  ]
  edge [
    source 28
    target 2303
  ]
  edge [
    source 28
    target 2304
  ]
  edge [
    source 28
    target 2305
  ]
  edge [
    source 28
    target 2306
  ]
  edge [
    source 28
    target 2307
  ]
  edge [
    source 28
    target 2308
  ]
  edge [
    source 28
    target 2309
  ]
  edge [
    source 28
    target 2310
  ]
  edge [
    source 28
    target 2311
  ]
  edge [
    source 28
    target 2312
  ]
  edge [
    source 28
    target 2313
  ]
  edge [
    source 28
    target 1264
  ]
  edge [
    source 28
    target 2314
  ]
  edge [
    source 28
    target 2315
  ]
  edge [
    source 28
    target 2316
  ]
  edge [
    source 28
    target 1209
  ]
  edge [
    source 28
    target 2317
  ]
  edge [
    source 28
    target 2318
  ]
  edge [
    source 28
    target 2319
  ]
  edge [
    source 28
    target 2320
  ]
  edge [
    source 28
    target 2321
  ]
  edge [
    source 28
    target 2322
  ]
  edge [
    source 28
    target 2323
  ]
  edge [
    source 28
    target 2324
  ]
  edge [
    source 28
    target 2325
  ]
  edge [
    source 28
    target 2326
  ]
  edge [
    source 28
    target 2327
  ]
  edge [
    source 28
    target 2328
  ]
  edge [
    source 28
    target 2329
  ]
  edge [
    source 28
    target 2330
  ]
  edge [
    source 28
    target 2331
  ]
  edge [
    source 28
    target 2332
  ]
  edge [
    source 28
    target 2333
  ]
  edge [
    source 28
    target 2334
  ]
  edge [
    source 28
    target 1293
  ]
  edge [
    source 28
    target 2335
  ]
  edge [
    source 28
    target 2336
  ]
  edge [
    source 28
    target 2337
  ]
  edge [
    source 28
    target 2338
  ]
  edge [
    source 28
    target 2339
  ]
  edge [
    source 28
    target 2340
  ]
  edge [
    source 28
    target 2341
  ]
  edge [
    source 28
    target 2342
  ]
  edge [
    source 28
    target 2343
  ]
  edge [
    source 28
    target 2344
  ]
  edge [
    source 28
    target 2345
  ]
  edge [
    source 28
    target 2346
  ]
  edge [
    source 28
    target 2347
  ]
  edge [
    source 28
    target 2348
  ]
  edge [
    source 28
    target 2349
  ]
  edge [
    source 28
    target 2350
  ]
  edge [
    source 28
    target 535
  ]
  edge [
    source 28
    target 2351
  ]
  edge [
    source 28
    target 2352
  ]
  edge [
    source 28
    target 2353
  ]
  edge [
    source 28
    target 2354
  ]
  edge [
    source 28
    target 2355
  ]
  edge [
    source 28
    target 2356
  ]
  edge [
    source 28
    target 2357
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 765
  ]
  edge [
    source 29
    target 2063
  ]
  edge [
    source 29
    target 2358
  ]
  edge [
    source 29
    target 2359
  ]
  edge [
    source 29
    target 797
  ]
  edge [
    source 29
    target 885
  ]
  edge [
    source 29
    target 768
  ]
  edge [
    source 29
    target 2059
  ]
  edge [
    source 29
    target 458
  ]
  edge [
    source 29
    target 2060
  ]
  edge [
    source 29
    target 2061
  ]
  edge [
    source 29
    target 2062
  ]
  edge [
    source 29
    target 49
  ]
  edge [
    source 29
    target 54
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 77
  ]
  edge [
    source 30
    target 62
  ]
  edge [
    source 30
    target 201
  ]
  edge [
    source 30
    target 202
  ]
  edge [
    source 30
    target 203
  ]
  edge [
    source 30
    target 204
  ]
  edge [
    source 30
    target 105
  ]
  edge [
    source 30
    target 205
  ]
  edge [
    source 30
    target 206
  ]
  edge [
    source 30
    target 73
  ]
  edge [
    source 30
    target 2360
  ]
  edge [
    source 30
    target 996
  ]
  edge [
    source 30
    target 2361
  ]
  edge [
    source 30
    target 2362
  ]
  edge [
    source 30
    target 2363
  ]
  edge [
    source 30
    target 2364
  ]
  edge [
    source 30
    target 2365
  ]
  edge [
    source 30
    target 2366
  ]
  edge [
    source 30
    target 2367
  ]
  edge [
    source 30
    target 2368
  ]
  edge [
    source 30
    target 2369
  ]
  edge [
    source 30
    target 2370
  ]
  edge [
    source 30
    target 738
  ]
  edge [
    source 30
    target 2371
  ]
  edge [
    source 30
    target 901
  ]
  edge [
    source 30
    target 61
  ]
  edge [
    source 30
    target 63
  ]
  edge [
    source 30
    target 64
  ]
  edge [
    source 30
    target 65
  ]
  edge [
    source 30
    target 66
  ]
  edge [
    source 30
    target 67
  ]
  edge [
    source 30
    target 68
  ]
  edge [
    source 30
    target 69
  ]
  edge [
    source 30
    target 70
  ]
  edge [
    source 30
    target 71
  ]
  edge [
    source 30
    target 72
  ]
  edge [
    source 30
    target 74
  ]
  edge [
    source 30
    target 75
  ]
  edge [
    source 30
    target 76
  ]
  edge [
    source 30
    target 78
  ]
  edge [
    source 30
    target 79
  ]
  edge [
    source 30
    target 80
  ]
  edge [
    source 30
    target 81
  ]
  edge [
    source 30
    target 82
  ]
  edge [
    source 30
    target 83
  ]
  edge [
    source 30
    target 84
  ]
  edge [
    source 30
    target 85
  ]
  edge [
    source 30
    target 86
  ]
  edge [
    source 30
    target 87
  ]
  edge [
    source 30
    target 88
  ]
  edge [
    source 30
    target 89
  ]
  edge [
    source 30
    target 90
  ]
  edge [
    source 30
    target 91
  ]
  edge [
    source 30
    target 92
  ]
  edge [
    source 30
    target 93
  ]
  edge [
    source 30
    target 94
  ]
  edge [
    source 30
    target 95
  ]
  edge [
    source 30
    target 96
  ]
  edge [
    source 30
    target 97
  ]
  edge [
    source 30
    target 98
  ]
  edge [
    source 30
    target 99
  ]
  edge [
    source 30
    target 100
  ]
  edge [
    source 30
    target 101
  ]
  edge [
    source 30
    target 102
  ]
  edge [
    source 30
    target 103
  ]
  edge [
    source 30
    target 104
  ]
  edge [
    source 30
    target 106
  ]
  edge [
    source 30
    target 107
  ]
  edge [
    source 30
    target 108
  ]
  edge [
    source 30
    target 109
  ]
  edge [
    source 30
    target 110
  ]
  edge [
    source 30
    target 111
  ]
  edge [
    source 30
    target 112
  ]
  edge [
    source 30
    target 113
  ]
  edge [
    source 30
    target 114
  ]
  edge [
    source 30
    target 115
  ]
  edge [
    source 30
    target 116
  ]
  edge [
    source 30
    target 117
  ]
  edge [
    source 30
    target 118
  ]
  edge [
    source 30
    target 119
  ]
  edge [
    source 30
    target 120
  ]
  edge [
    source 30
    target 121
  ]
  edge [
    source 30
    target 122
  ]
  edge [
    source 30
    target 123
  ]
  edge [
    source 30
    target 2372
  ]
  edge [
    source 30
    target 2373
  ]
  edge [
    source 30
    target 2374
  ]
  edge [
    source 30
    target 2375
  ]
  edge [
    source 30
    target 2376
  ]
  edge [
    source 30
    target 2377
  ]
  edge [
    source 30
    target 784
  ]
  edge [
    source 30
    target 2238
  ]
  edge [
    source 30
    target 2378
  ]
  edge [
    source 30
    target 2379
  ]
  edge [
    source 30
    target 2380
  ]
  edge [
    source 30
    target 1496
  ]
  edge [
    source 30
    target 2381
  ]
  edge [
    source 30
    target 1210
  ]
  edge [
    source 30
    target 1981
  ]
  edge [
    source 30
    target 1098
  ]
  edge [
    source 30
    target 2382
  ]
  edge [
    source 30
    target 2383
  ]
  edge [
    source 30
    target 2384
  ]
  edge [
    source 30
    target 2385
  ]
  edge [
    source 30
    target 2386
  ]
  edge [
    source 30
    target 2139
  ]
  edge [
    source 30
    target 2387
  ]
  edge [
    source 30
    target 2388
  ]
  edge [
    source 30
    target 2389
  ]
  edge [
    source 30
    target 2390
  ]
  edge [
    source 30
    target 2391
  ]
  edge [
    source 30
    target 2392
  ]
  edge [
    source 30
    target 2359
  ]
  edge [
    source 30
    target 2393
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 2394
  ]
  edge [
    source 30
    target 2395
  ]
  edge [
    source 30
    target 2396
  ]
  edge [
    source 30
    target 2153
  ]
  edge [
    source 30
    target 2397
  ]
  edge [
    source 30
    target 2398
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 270
  ]
  edge [
    source 30
    target 271
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 268
  ]
  edge [
    source 30
    target 269
  ]
  edge [
    source 30
    target 2399
  ]
  edge [
    source 30
    target 2400
  ]
  edge [
    source 30
    target 2401
  ]
  edge [
    source 30
    target 2402
  ]
  edge [
    source 30
    target 2403
  ]
  edge [
    source 30
    target 2404
  ]
  edge [
    source 30
    target 2405
  ]
  edge [
    source 30
    target 2406
  ]
  edge [
    source 31
    target 2407
  ]
  edge [
    source 31
    target 2408
  ]
  edge [
    source 31
    target 2409
  ]
  edge [
    source 31
    target 2410
  ]
  edge [
    source 31
    target 2411
  ]
  edge [
    source 31
    target 2412
  ]
  edge [
    source 31
    target 2413
  ]
  edge [
    source 31
    target 2414
  ]
  edge [
    source 31
    target 2415
  ]
  edge [
    source 31
    target 2416
  ]
  edge [
    source 31
    target 2417
  ]
  edge [
    source 31
    target 2418
  ]
  edge [
    source 31
    target 2419
  ]
  edge [
    source 31
    target 2420
  ]
  edge [
    source 31
    target 2421
  ]
  edge [
    source 31
    target 2422
  ]
  edge [
    source 31
    target 2423
  ]
  edge [
    source 31
    target 2424
  ]
  edge [
    source 31
    target 2425
  ]
  edge [
    source 31
    target 2426
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 58
  ]
  edge [
    source 32
    target 1842
  ]
  edge [
    source 32
    target 1843
  ]
  edge [
    source 32
    target 556
  ]
  edge [
    source 32
    target 1845
  ]
  edge [
    source 32
    target 1877
  ]
  edge [
    source 32
    target 1846
  ]
  edge [
    source 32
    target 1856
  ]
  edge [
    source 32
    target 1847
  ]
  edge [
    source 32
    target 1878
  ]
  edge [
    source 32
    target 1782
  ]
  edge [
    source 32
    target 1860
  ]
  edge [
    source 32
    target 442
  ]
  edge [
    source 32
    target 1844
  ]
  edge [
    source 32
    target 1848
  ]
  edge [
    source 32
    target 721
  ]
  edge [
    source 32
    target 1849
  ]
  edge [
    source 32
    target 1850
  ]
  edge [
    source 32
    target 1851
  ]
  edge [
    source 32
    target 1852
  ]
  edge [
    source 32
    target 1116
  ]
  edge [
    source 32
    target 1853
  ]
  edge [
    source 32
    target 1854
  ]
  edge [
    source 32
    target 1855
  ]
  edge [
    source 32
    target 1857
  ]
  edge [
    source 32
    target 1858
  ]
  edge [
    source 32
    target 1859
  ]
  edge [
    source 32
    target 1861
  ]
  edge [
    source 32
    target 1862
  ]
  edge [
    source 32
    target 1863
  ]
  edge [
    source 32
    target 1864
  ]
  edge [
    source 32
    target 509
  ]
  edge [
    source 32
    target 1865
  ]
  edge [
    source 32
    target 1866
  ]
  edge [
    source 32
    target 1867
  ]
  edge [
    source 32
    target 513
  ]
  edge [
    source 32
    target 440
  ]
  edge [
    source 32
    target 713
  ]
  edge [
    source 32
    target 714
  ]
  edge [
    source 32
    target 715
  ]
  edge [
    source 32
    target 716
  ]
  edge [
    source 32
    target 717
  ]
  edge [
    source 32
    target 718
  ]
  edge [
    source 32
    target 2427
  ]
  edge [
    source 32
    target 2428
  ]
  edge [
    source 32
    target 2429
  ]
  edge [
    source 32
    target 2053
  ]
  edge [
    source 32
    target 2430
  ]
  edge [
    source 32
    target 950
  ]
  edge [
    source 32
    target 2001
  ]
  edge [
    source 32
    target 2002
  ]
  edge [
    source 32
    target 2003
  ]
  edge [
    source 32
    target 2004
  ]
  edge [
    source 32
    target 2005
  ]
  edge [
    source 32
    target 1925
  ]
  edge [
    source 32
    target 1926
  ]
  edge [
    source 32
    target 446
  ]
  edge [
    source 32
    target 1780
  ]
  edge [
    source 32
    target 1702
  ]
  edge [
    source 32
    target 1927
  ]
  edge [
    source 32
    target 1928
  ]
  edge [
    source 32
    target 1929
  ]
  edge [
    source 32
    target 1930
  ]
  edge [
    source 32
    target 1931
  ]
  edge [
    source 32
    target 1932
  ]
  edge [
    source 32
    target 1933
  ]
  edge [
    source 32
    target 1934
  ]
  edge [
    source 32
    target 1778
  ]
  edge [
    source 32
    target 1935
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 1936
  ]
  edge [
    source 32
    target 1937
  ]
  edge [
    source 32
    target 1938
  ]
  edge [
    source 32
    target 1939
  ]
  edge [
    source 32
    target 1940
  ]
  edge [
    source 32
    target 1941
  ]
  edge [
    source 32
    target 281
  ]
  edge [
    source 32
    target 1942
  ]
  edge [
    source 32
    target 1943
  ]
  edge [
    source 32
    target 1776
  ]
  edge [
    source 32
    target 1944
  ]
  edge [
    source 32
    target 1945
  ]
  edge [
    source 32
    target 1946
  ]
  edge [
    source 32
    target 1947
  ]
  edge [
    source 32
    target 1948
  ]
  edge [
    source 32
    target 1949
  ]
  edge [
    source 32
    target 1950
  ]
  edge [
    source 32
    target 1951
  ]
  edge [
    source 32
    target 1952
  ]
  edge [
    source 32
    target 1953
  ]
  edge [
    source 32
    target 1954
  ]
  edge [
    source 32
    target 1955
  ]
  edge [
    source 32
    target 1956
  ]
  edge [
    source 32
    target 660
  ]
  edge [
    source 32
    target 663
  ]
  edge [
    source 32
    target 1957
  ]
  edge [
    source 32
    target 1958
  ]
  edge [
    source 32
    target 634
  ]
  edge [
    source 32
    target 1959
  ]
  edge [
    source 32
    target 1803
  ]
  edge [
    source 32
    target 689
  ]
  edge [
    source 32
    target 1960
  ]
  edge [
    source 32
    target 1961
  ]
  edge [
    source 32
    target 1962
  ]
  edge [
    source 32
    target 1963
  ]
  edge [
    source 32
    target 1964
  ]
  edge [
    source 32
    target 1775
  ]
  edge [
    source 32
    target 1965
  ]
  edge [
    source 32
    target 1966
  ]
  edge [
    source 32
    target 1967
  ]
  edge [
    source 32
    target 1998
  ]
  edge [
    source 32
    target 32
  ]
  edge [
    source 33
    target 653
  ]
  edge [
    source 33
    target 2431
  ]
  edge [
    source 33
    target 768
  ]
  edge [
    source 33
    target 2432
  ]
  edge [
    source 33
    target 378
  ]
  edge [
    source 33
    target 2433
  ]
  edge [
    source 33
    target 2434
  ]
  edge [
    source 33
    target 772
  ]
  edge [
    source 33
    target 2435
  ]
  edge [
    source 33
    target 2436
  ]
  edge [
    source 33
    target 2358
  ]
  edge [
    source 33
    target 797
  ]
  edge [
    source 33
    target 2437
  ]
  edge [
    source 33
    target 2438
  ]
  edge [
    source 33
    target 973
  ]
  edge [
    source 33
    target 1249
  ]
  edge [
    source 33
    target 2439
  ]
  edge [
    source 33
    target 767
  ]
  edge [
    source 33
    target 2440
  ]
  edge [
    source 33
    target 52
  ]
  edge [
    source 33
    target 2441
  ]
  edge [
    source 33
    target 2442
  ]
  edge [
    source 33
    target 2063
  ]
  edge [
    source 33
    target 387
  ]
  edge [
    source 33
    target 885
  ]
  edge [
    source 33
    target 779
  ]
  edge [
    source 33
    target 2443
  ]
  edge [
    source 33
    target 2444
  ]
  edge [
    source 33
    target 2445
  ]
  edge [
    source 33
    target 679
  ]
  edge [
    source 33
    target 2446
  ]
  edge [
    source 33
    target 2447
  ]
  edge [
    source 33
    target 2448
  ]
  edge [
    source 33
    target 458
  ]
  edge [
    source 33
    target 2449
  ]
  edge [
    source 33
    target 2450
  ]
  edge [
    source 33
    target 370
  ]
  edge [
    source 33
    target 2451
  ]
  edge [
    source 33
    target 2452
  ]
  edge [
    source 33
    target 2453
  ]
  edge [
    source 33
    target 2454
  ]
  edge [
    source 33
    target 2455
  ]
  edge [
    source 33
    target 2456
  ]
  edge [
    source 33
    target 2457
  ]
  edge [
    source 33
    target 2458
  ]
  edge [
    source 33
    target 2459
  ]
  edge [
    source 33
    target 2460
  ]
  edge [
    source 33
    target 2461
  ]
  edge [
    source 33
    target 654
  ]
  edge [
    source 33
    target 2462
  ]
  edge [
    source 33
    target 2463
  ]
  edge [
    source 33
    target 2464
  ]
  edge [
    source 33
    target 2465
  ]
  edge [
    source 33
    target 1515
  ]
  edge [
    source 33
    target 2466
  ]
  edge [
    source 33
    target 410
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1852
  ]
  edge [
    source 35
    target 2467
  ]
  edge [
    source 35
    target 1983
  ]
  edge [
    source 35
    target 2468
  ]
  edge [
    source 35
    target 2469
  ]
  edge [
    source 35
    target 2470
  ]
  edge [
    source 35
    target 2471
  ]
  edge [
    source 35
    target 2472
  ]
  edge [
    source 35
    target 1109
  ]
  edge [
    source 35
    target 2473
  ]
  edge [
    source 35
    target 2474
  ]
  edge [
    source 35
    target 2475
  ]
  edge [
    source 35
    target 2476
  ]
  edge [
    source 35
    target 2477
  ]
  edge [
    source 35
    target 2478
  ]
  edge [
    source 35
    target 854
  ]
  edge [
    source 35
    target 692
  ]
  edge [
    source 35
    target 2003
  ]
  edge [
    source 35
    target 2479
  ]
  edge [
    source 35
    target 2480
  ]
  edge [
    source 35
    target 2058
  ]
  edge [
    source 35
    target 449
  ]
  edge [
    source 35
    target 1981
  ]
  edge [
    source 35
    target 1098
  ]
  edge [
    source 35
    target 1982
  ]
  edge [
    source 35
    target 934
  ]
  edge [
    source 35
    target 1984
  ]
  edge [
    source 35
    target 1985
  ]
  edge [
    source 35
    target 1986
  ]
  edge [
    source 35
    target 1987
  ]
  edge [
    source 35
    target 1988
  ]
  edge [
    source 35
    target 1989
  ]
  edge [
    source 35
    target 1990
  ]
  edge [
    source 35
    target 1991
  ]
  edge [
    source 35
    target 1992
  ]
  edge [
    source 35
    target 1993
  ]
  edge [
    source 35
    target 1994
  ]
  edge [
    source 35
    target 1995
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 2481
  ]
  edge [
    source 36
    target 2482
  ]
  edge [
    source 36
    target 2483
  ]
  edge [
    source 36
    target 2484
  ]
  edge [
    source 36
    target 2485
  ]
  edge [
    source 36
    target 675
  ]
  edge [
    source 36
    target 2486
  ]
  edge [
    source 36
    target 2487
  ]
  edge [
    source 36
    target 2488
  ]
  edge [
    source 36
    target 2489
  ]
  edge [
    source 36
    target 2490
  ]
  edge [
    source 36
    target 2491
  ]
  edge [
    source 36
    target 2492
  ]
  edge [
    source 36
    target 2493
  ]
  edge [
    source 36
    target 460
  ]
  edge [
    source 36
    target 2494
  ]
  edge [
    source 36
    target 2495
  ]
  edge [
    source 36
    target 458
  ]
  edge [
    source 36
    target 2496
  ]
  edge [
    source 36
    target 2497
  ]
  edge [
    source 36
    target 2498
  ]
  edge [
    source 36
    target 2499
  ]
  edge [
    source 36
    target 2500
  ]
  edge [
    source 36
    target 2501
  ]
  edge [
    source 36
    target 1648
  ]
  edge [
    source 36
    target 439
  ]
  edge [
    source 36
    target 2502
  ]
  edge [
    source 36
    target 469
  ]
  edge [
    source 36
    target 1651
  ]
  edge [
    source 36
    target 2503
  ]
  edge [
    source 36
    target 2504
  ]
  edge [
    source 36
    target 2505
  ]
  edge [
    source 36
    target 2506
  ]
  edge [
    source 36
    target 2507
  ]
  edge [
    source 36
    target 2508
  ]
  edge [
    source 36
    target 2509
  ]
  edge [
    source 36
    target 2510
  ]
  edge [
    source 36
    target 2511
  ]
  edge [
    source 36
    target 2512
  ]
  edge [
    source 36
    target 2513
  ]
  edge [
    source 36
    target 2514
  ]
  edge [
    source 36
    target 2515
  ]
  edge [
    source 36
    target 2516
  ]
  edge [
    source 36
    target 2517
  ]
  edge [
    source 36
    target 2518
  ]
  edge [
    source 36
    target 2519
  ]
  edge [
    source 36
    target 2520
  ]
  edge [
    source 36
    target 2521
  ]
  edge [
    source 36
    target 2522
  ]
  edge [
    source 36
    target 2523
  ]
  edge [
    source 36
    target 648
  ]
  edge [
    source 36
    target 2524
  ]
  edge [
    source 36
    target 2525
  ]
  edge [
    source 36
    target 2146
  ]
  edge [
    source 36
    target 2526
  ]
  edge [
    source 36
    target 2099
  ]
  edge [
    source 36
    target 2147
  ]
  edge [
    source 36
    target 2527
  ]
  edge [
    source 36
    target 2528
  ]
  edge [
    source 36
    target 2529
  ]
  edge [
    source 36
    target 2530
  ]
  edge [
    source 36
    target 2531
  ]
  edge [
    source 36
    target 2532
  ]
  edge [
    source 36
    target 2533
  ]
  edge [
    source 36
    target 2534
  ]
  edge [
    source 36
    target 2535
  ]
  edge [
    source 36
    target 2536
  ]
  edge [
    source 36
    target 2537
  ]
  edge [
    source 36
    target 692
  ]
  edge [
    source 36
    target 2538
  ]
  edge [
    source 36
    target 2539
  ]
  edge [
    source 36
    target 2540
  ]
  edge [
    source 36
    target 2541
  ]
  edge [
    source 36
    target 2542
  ]
  edge [
    source 36
    target 2543
  ]
  edge [
    source 36
    target 2544
  ]
  edge [
    source 36
    target 2135
  ]
  edge [
    source 36
    target 2545
  ]
  edge [
    source 36
    target 2546
  ]
  edge [
    source 36
    target 1981
  ]
  edge [
    source 36
    target 2547
  ]
  edge [
    source 36
    target 2548
  ]
  edge [
    source 36
    target 2549
  ]
  edge [
    source 36
    target 1224
  ]
  edge [
    source 36
    target 645
  ]
  edge [
    source 36
    target 662
  ]
  edge [
    source 36
    target 2441
  ]
  edge [
    source 37
    target 2550
  ]
  edge [
    source 37
    target 2551
  ]
  edge [
    source 37
    target 2552
  ]
  edge [
    source 37
    target 454
  ]
  edge [
    source 37
    target 2553
  ]
  edge [
    source 37
    target 2554
  ]
  edge [
    source 37
    target 429
  ]
  edge [
    source 37
    target 2555
  ]
  edge [
    source 37
    target 888
  ]
  edge [
    source 37
    target 2556
  ]
  edge [
    source 37
    target 905
  ]
  edge [
    source 37
    target 2557
  ]
  edge [
    source 37
    target 2558
  ]
  edge [
    source 37
    target 643
  ]
  edge [
    source 37
    target 1870
  ]
  edge [
    source 37
    target 2559
  ]
  edge [
    source 37
    target 1098
  ]
  edge [
    source 37
    target 2560
  ]
  edge [
    source 37
    target 2561
  ]
  edge [
    source 37
    target 2562
  ]
  edge [
    source 37
    target 2563
  ]
  edge [
    source 37
    target 2564
  ]
  edge [
    source 37
    target 2538
  ]
  edge [
    source 37
    target 2565
  ]
  edge [
    source 37
    target 2566
  ]
  edge [
    source 37
    target 2567
  ]
  edge [
    source 37
    target 2568
  ]
  edge [
    source 37
    target 143
  ]
  edge [
    source 37
    target 1833
  ]
  edge [
    source 37
    target 2569
  ]
  edge [
    source 37
    target 2570
  ]
  edge [
    source 37
    target 2571
  ]
  edge [
    source 37
    target 2572
  ]
  edge [
    source 37
    target 2573
  ]
  edge [
    source 37
    target 2574
  ]
  edge [
    source 37
    target 2575
  ]
  edge [
    source 37
    target 2576
  ]
  edge [
    source 37
    target 2577
  ]
  edge [
    source 37
    target 2578
  ]
  edge [
    source 37
    target 2579
  ]
  edge [
    source 37
    target 2580
  ]
  edge [
    source 37
    target 114
  ]
  edge [
    source 37
    target 2241
  ]
  edge [
    source 37
    target 2581
  ]
  edge [
    source 37
    target 2582
  ]
  edge [
    source 37
    target 2583
  ]
  edge [
    source 37
    target 2584
  ]
  edge [
    source 37
    target 2585
  ]
  edge [
    source 37
    target 2586
  ]
  edge [
    source 38
    target 2587
  ]
  edge [
    source 38
    target 2588
  ]
  edge [
    source 38
    target 365
  ]
  edge [
    source 38
    target 2589
  ]
  edge [
    source 38
    target 663
  ]
  edge [
    source 38
    target 2590
  ]
  edge [
    source 38
    target 2591
  ]
  edge [
    source 38
    target 2592
  ]
  edge [
    source 38
    target 2593
  ]
  edge [
    source 38
    target 2594
  ]
  edge [
    source 38
    target 1493
  ]
  edge [
    source 38
    target 2595
  ]
  edge [
    source 38
    target 2596
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1010
  ]
  edge [
    source 40
    target 1011
  ]
  edge [
    source 40
    target 64
  ]
  edge [
    source 40
    target 1012
  ]
  edge [
    source 40
    target 1013
  ]
  edge [
    source 40
    target 1014
  ]
  edge [
    source 40
    target 1015
  ]
  edge [
    source 40
    target 1016
  ]
  edge [
    source 40
    target 1017
  ]
  edge [
    source 40
    target 1018
  ]
  edge [
    source 40
    target 724
  ]
  edge [
    source 40
    target 1019
  ]
  edge [
    source 40
    target 732
  ]
  edge [
    source 40
    target 1020
  ]
  edge [
    source 40
    target 1021
  ]
  edge [
    source 40
    target 1022
  ]
  edge [
    source 40
    target 1023
  ]
  edge [
    source 40
    target 1024
  ]
  edge [
    source 40
    target 1025
  ]
  edge [
    source 40
    target 1026
  ]
  edge [
    source 40
    target 1027
  ]
  edge [
    source 40
    target 1028
  ]
  edge [
    source 40
    target 2597
  ]
  edge [
    source 40
    target 1928
  ]
  edge [
    source 40
    target 2226
  ]
  edge [
    source 40
    target 2598
  ]
  edge [
    source 40
    target 2599
  ]
  edge [
    source 40
    target 2600
  ]
  edge [
    source 40
    target 2601
  ]
  edge [
    source 40
    target 2209
  ]
  edge [
    source 40
    target 2189
  ]
  edge [
    source 40
    target 253
  ]
  edge [
    source 40
    target 2210
  ]
  edge [
    source 40
    target 2211
  ]
  edge [
    source 40
    target 2192
  ]
  edge [
    source 40
    target 2197
  ]
  edge [
    source 40
    target 2212
  ]
  edge [
    source 40
    target 2213
  ]
  edge [
    source 40
    target 2198
  ]
  edge [
    source 40
    target 2214
  ]
  edge [
    source 40
    target 2215
  ]
  edge [
    source 40
    target 1188
  ]
  edge [
    source 40
    target 2216
  ]
  edge [
    source 40
    target 2196
  ]
  edge [
    source 40
    target 2217
  ]
  edge [
    source 40
    target 2218
  ]
  edge [
    source 40
    target 125
  ]
  edge [
    source 40
    target 2219
  ]
  edge [
    source 40
    target 549
  ]
  edge [
    source 40
    target 2220
  ]
  edge [
    source 40
    target 2221
  ]
  edge [
    source 40
    target 1274
  ]
  edge [
    source 40
    target 2222
  ]
  edge [
    source 40
    target 2223
  ]
  edge [
    source 40
    target 2224
  ]
  edge [
    source 40
    target 2225
  ]
  edge [
    source 40
    target 1696
  ]
  edge [
    source 40
    target 2227
  ]
  edge [
    source 40
    target 2229
  ]
  edge [
    source 40
    target 2228
  ]
  edge [
    source 40
    target 2230
  ]
  edge [
    source 40
    target 2231
  ]
  edge [
    source 40
    target 2201
  ]
  edge [
    source 40
    target 2232
  ]
  edge [
    source 40
    target 2157
  ]
  edge [
    source 40
    target 2233
  ]
  edge [
    source 40
    target 2235
  ]
  edge [
    source 40
    target 2237
  ]
  edge [
    source 40
    target 2236
  ]
  edge [
    source 40
    target 2234
  ]
  edge [
    source 40
    target 2238
  ]
  edge [
    source 40
    target 2240
  ]
  edge [
    source 40
    target 2239
  ]
  edge [
    source 40
    target 2242
  ]
  edge [
    source 40
    target 2241
  ]
  edge [
    source 40
    target 2243
  ]
  edge [
    source 40
    target 2244
  ]
  edge [
    source 40
    target 2602
  ]
  edge [
    source 40
    target 2603
  ]
  edge [
    source 40
    target 2604
  ]
  edge [
    source 40
    target 2605
  ]
  edge [
    source 40
    target 2606
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 40
    target 2607
  ]
  edge [
    source 40
    target 2608
  ]
  edge [
    source 40
    target 866
  ]
  edge [
    source 40
    target 2609
  ]
  edge [
    source 40
    target 2610
  ]
  edge [
    source 40
    target 2611
  ]
  edge [
    source 40
    target 2612
  ]
  edge [
    source 40
    target 2613
  ]
  edge [
    source 40
    target 1213
  ]
  edge [
    source 40
    target 423
  ]
  edge [
    source 40
    target 2614
  ]
  edge [
    source 40
    target 2615
  ]
  edge [
    source 40
    target 2616
  ]
  edge [
    source 40
    target 1800
  ]
  edge [
    source 40
    target 2617
  ]
  edge [
    source 40
    target 2618
  ]
  edge [
    source 40
    target 2619
  ]
  edge [
    source 40
    target 2620
  ]
  edge [
    source 40
    target 2621
  ]
  edge [
    source 40
    target 2260
  ]
  edge [
    source 40
    target 2622
  ]
  edge [
    source 40
    target 2623
  ]
  edge [
    source 40
    target 1838
  ]
  edge [
    source 40
    target 1126
  ]
  edge [
    source 40
    target 2624
  ]
  edge [
    source 40
    target 2625
  ]
  edge [
    source 40
    target 2626
  ]
  edge [
    source 40
    target 2627
  ]
  edge [
    source 40
    target 2628
  ]
  edge [
    source 40
    target 1247
  ]
  edge [
    source 40
    target 2629
  ]
  edge [
    source 40
    target 800
  ]
  edge [
    source 40
    target 2630
  ]
  edge [
    source 40
    target 2631
  ]
  edge [
    source 40
    target 2632
  ]
  edge [
    source 40
    target 2633
  ]
  edge [
    source 40
    target 1302
  ]
  edge [
    source 40
    target 2634
  ]
  edge [
    source 40
    target 2635
  ]
  edge [
    source 40
    target 2636
  ]
  edge [
    source 40
    target 2637
  ]
  edge [
    source 40
    target 2638
  ]
  edge [
    source 40
    target 2639
  ]
  edge [
    source 40
    target 1132
  ]
  edge [
    source 40
    target 2640
  ]
  edge [
    source 40
    target 2641
  ]
  edge [
    source 40
    target 2642
  ]
  edge [
    source 40
    target 2643
  ]
  edge [
    source 40
    target 2644
  ]
  edge [
    source 40
    target 2645
  ]
  edge [
    source 40
    target 2646
  ]
  edge [
    source 40
    target 966
  ]
  edge [
    source 40
    target 2647
  ]
  edge [
    source 40
    target 2648
  ]
  edge [
    source 40
    target 2649
  ]
  edge [
    source 40
    target 2650
  ]
  edge [
    source 40
    target 1157
  ]
  edge [
    source 40
    target 2651
  ]
  edge [
    source 40
    target 2652
  ]
  edge [
    source 40
    target 2653
  ]
  edge [
    source 40
    target 1135
  ]
  edge [
    source 40
    target 2654
  ]
  edge [
    source 40
    target 2655
  ]
  edge [
    source 40
    target 2656
  ]
  edge [
    source 40
    target 2657
  ]
  edge [
    source 40
    target 2658
  ]
  edge [
    source 40
    target 2659
  ]
  edge [
    source 40
    target 2660
  ]
  edge [
    source 40
    target 1052
  ]
  edge [
    source 40
    target 2661
  ]
  edge [
    source 40
    target 284
  ]
  edge [
    source 40
    target 2662
  ]
  edge [
    source 40
    target 2663
  ]
  edge [
    source 40
    target 2664
  ]
  edge [
    source 40
    target 2665
  ]
  edge [
    source 40
    target 2666
  ]
  edge [
    source 40
    target 454
  ]
  edge [
    source 40
    target 2667
  ]
  edge [
    source 40
    target 2668
  ]
  edge [
    source 40
    target 2669
  ]
  edge [
    source 40
    target 2670
  ]
  edge [
    source 40
    target 2671
  ]
  edge [
    source 40
    target 2672
  ]
  edge [
    source 40
    target 2673
  ]
  edge [
    source 40
    target 2674
  ]
  edge [
    source 40
    target 2675
  ]
  edge [
    source 40
    target 2676
  ]
  edge [
    source 40
    target 2677
  ]
  edge [
    source 40
    target 1924
  ]
  edge [
    source 40
    target 2678
  ]
  edge [
    source 40
    target 2679
  ]
  edge [
    source 40
    target 931
  ]
  edge [
    source 40
    target 2680
  ]
  edge [
    source 40
    target 2681
  ]
  edge [
    source 40
    target 2682
  ]
  edge [
    source 40
    target 2683
  ]
  edge [
    source 40
    target 2684
  ]
  edge [
    source 40
    target 1812
  ]
  edge [
    source 40
    target 2685
  ]
  edge [
    source 40
    target 2686
  ]
  edge [
    source 40
    target 2687
  ]
  edge [
    source 40
    target 2688
  ]
  edge [
    source 40
    target 354
  ]
  edge [
    source 40
    target 355
  ]
  edge [
    source 40
    target 356
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 40
    target 359
  ]
  edge [
    source 40
    target 360
  ]
  edge [
    source 40
    target 361
  ]
  edge [
    source 40
    target 362
  ]
  edge [
    source 40
    target 363
  ]
  edge [
    source 40
    target 364
  ]
  edge [
    source 40
    target 365
  ]
  edge [
    source 40
    target 366
  ]
  edge [
    source 40
    target 367
  ]
  edge [
    source 40
    target 368
  ]
  edge [
    source 40
    target 369
  ]
  edge [
    source 40
    target 370
  ]
  edge [
    source 40
    target 2689
  ]
  edge [
    source 40
    target 2690
  ]
  edge [
    source 40
    target 2691
  ]
  edge [
    source 40
    target 2692
  ]
  edge [
    source 40
    target 2693
  ]
  edge [
    source 40
    target 2694
  ]
  edge [
    source 40
    target 2695
  ]
  edge [
    source 40
    target 2696
  ]
  edge [
    source 40
    target 2697
  ]
  edge [
    source 40
    target 2698
  ]
  edge [
    source 40
    target 2699
  ]
  edge [
    source 40
    target 2700
  ]
  edge [
    source 40
    target 2701
  ]
  edge [
    source 40
    target 2702
  ]
  edge [
    source 40
    target 2703
  ]
  edge [
    source 40
    target 2704
  ]
  edge [
    source 40
    target 2705
  ]
  edge [
    source 40
    target 2706
  ]
  edge [
    source 40
    target 2707
  ]
  edge [
    source 40
    target 738
  ]
  edge [
    source 40
    target 2708
  ]
  edge [
    source 40
    target 2709
  ]
  edge [
    source 40
    target 2710
  ]
  edge [
    source 40
    target 2711
  ]
  edge [
    source 40
    target 2712
  ]
  edge [
    source 40
    target 968
  ]
  edge [
    source 40
    target 2713
  ]
  edge [
    source 40
    target 2714
  ]
  edge [
    source 40
    target 653
  ]
  edge [
    source 40
    target 2715
  ]
  edge [
    source 40
    target 2716
  ]
  edge [
    source 40
    target 2717
  ]
  edge [
    source 40
    target 2718
  ]
  edge [
    source 40
    target 1309
  ]
  edge [
    source 40
    target 893
  ]
  edge [
    source 40
    target 2719
  ]
  edge [
    source 40
    target 768
  ]
  edge [
    source 40
    target 2720
  ]
  edge [
    source 40
    target 2721
  ]
  edge [
    source 40
    target 2722
  ]
  edge [
    source 40
    target 2723
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 2724
  ]
  edge [
    source 41
    target 372
  ]
  edge [
    source 41
    target 2725
  ]
  edge [
    source 41
    target 2561
  ]
  edge [
    source 41
    target 2726
  ]
  edge [
    source 41
    target 2727
  ]
  edge [
    source 41
    target 1210
  ]
  edge [
    source 41
    target 2226
  ]
  edge [
    source 41
    target 2728
  ]
  edge [
    source 41
    target 2729
  ]
  edge [
    source 41
    target 2730
  ]
  edge [
    source 41
    target 2731
  ]
  edge [
    source 41
    target 2732
  ]
  edge [
    source 41
    target 413
  ]
  edge [
    source 41
    target 2733
  ]
  edge [
    source 41
    target 2734
  ]
  edge [
    source 41
    target 2735
  ]
  edge [
    source 41
    target 218
  ]
  edge [
    source 41
    target 130
  ]
  edge [
    source 41
    target 2736
  ]
  edge [
    source 41
    target 896
  ]
  edge [
    source 41
    target 2126
  ]
  edge [
    source 41
    target 454
  ]
  edge [
    source 41
    target 2737
  ]
  edge [
    source 41
    target 2738
  ]
  edge [
    source 41
    target 858
  ]
  edge [
    source 41
    target 2739
  ]
  edge [
    source 41
    target 2740
  ]
  edge [
    source 41
    target 2741
  ]
  edge [
    source 41
    target 2742
  ]
  edge [
    source 41
    target 2743
  ]
  edge [
    source 41
    target 498
  ]
  edge [
    source 41
    target 2744
  ]
  edge [
    source 41
    target 2745
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1043
  ]
  edge [
    source 42
    target 2746
  ]
  edge [
    source 42
    target 2747
  ]
  edge [
    source 42
    target 798
  ]
  edge [
    source 42
    target 2748
  ]
  edge [
    source 42
    target 454
  ]
  edge [
    source 42
    target 125
  ]
  edge [
    source 42
    target 2749
  ]
  edge [
    source 42
    target 2750
  ]
  edge [
    source 42
    target 2022
  ]
  edge [
    source 42
    target 2010
  ]
  edge [
    source 42
    target 2023
  ]
  edge [
    source 42
    target 2024
  ]
  edge [
    source 42
    target 2025
  ]
  edge [
    source 42
    target 2026
  ]
  edge [
    source 42
    target 735
  ]
  edge [
    source 42
    target 2027
  ]
  edge [
    source 42
    target 721
  ]
  edge [
    source 42
    target 2028
  ]
  edge [
    source 42
    target 2029
  ]
  edge [
    source 42
    target 2553
  ]
  edge [
    source 42
    target 2554
  ]
  edge [
    source 42
    target 429
  ]
  edge [
    source 42
    target 2555
  ]
  edge [
    source 42
    target 888
  ]
  edge [
    source 42
    target 2556
  ]
  edge [
    source 42
    target 2751
  ]
  edge [
    source 42
    target 2752
  ]
  edge [
    source 42
    target 378
  ]
  edge [
    source 42
    target 2753
  ]
  edge [
    source 42
    target 2754
  ]
  edge [
    source 42
    target 2755
  ]
  edge [
    source 42
    target 2718
  ]
  edge [
    source 42
    target 2756
  ]
  edge [
    source 42
    target 2757
  ]
  edge [
    source 42
    target 2758
  ]
  edge [
    source 42
    target 2759
  ]
  edge [
    source 42
    target 2760
  ]
  edge [
    source 42
    target 2761
  ]
  edge [
    source 42
    target 2762
  ]
  edge [
    source 42
    target 410
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 42
    target 971
  ]
  edge [
    source 42
    target 2763
  ]
  edge [
    source 42
    target 2764
  ]
  edge [
    source 42
    target 490
  ]
  edge [
    source 42
    target 2765
  ]
  edge [
    source 42
    target 2766
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2767
  ]
  edge [
    source 43
    target 2768
  ]
  edge [
    source 43
    target 2769
  ]
  edge [
    source 43
    target 2770
  ]
  edge [
    source 43
    target 2771
  ]
  edge [
    source 43
    target 2772
  ]
  edge [
    source 43
    target 2773
  ]
  edge [
    source 43
    target 2774
  ]
  edge [
    source 43
    target 2775
  ]
  edge [
    source 43
    target 2776
  ]
  edge [
    source 43
    target 2777
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 2778
  ]
  edge [
    source 44
    target 2779
  ]
  edge [
    source 44
    target 2780
  ]
  edge [
    source 44
    target 2781
  ]
  edge [
    source 44
    target 2782
  ]
  edge [
    source 44
    target 2783
  ]
  edge [
    source 44
    target 125
  ]
  edge [
    source 44
    target 2784
  ]
  edge [
    source 44
    target 2785
  ]
  edge [
    source 44
    target 2786
  ]
  edge [
    source 44
    target 2787
  ]
  edge [
    source 44
    target 2788
  ]
  edge [
    source 44
    target 2789
  ]
  edge [
    source 44
    target 2790
  ]
  edge [
    source 44
    target 2791
  ]
  edge [
    source 44
    target 2792
  ]
  edge [
    source 44
    target 2793
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 2794
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1918
  ]
  edge [
    source 46
    target 2795
  ]
  edge [
    source 46
    target 2796
  ]
  edge [
    source 46
    target 1098
  ]
  edge [
    source 46
    target 921
  ]
  edge [
    source 46
    target 2797
  ]
  edge [
    source 46
    target 2798
  ]
  edge [
    source 46
    target 2799
  ]
  edge [
    source 46
    target 2800
  ]
  edge [
    source 46
    target 1855
  ]
  edge [
    source 46
    target 2801
  ]
  edge [
    source 46
    target 2802
  ]
  edge [
    source 46
    target 2803
  ]
  edge [
    source 46
    target 2804
  ]
  edge [
    source 46
    target 1666
  ]
  edge [
    source 46
    target 2805
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 896
  ]
  edge [
    source 47
    target 2806
  ]
  edge [
    source 47
    target 2807
  ]
  edge [
    source 47
    target 2808
  ]
  edge [
    source 47
    target 2809
  ]
  edge [
    source 47
    target 2810
  ]
  edge [
    source 47
    target 2811
  ]
  edge [
    source 47
    target 2812
  ]
  edge [
    source 47
    target 413
  ]
  edge [
    source 47
    target 2813
  ]
  edge [
    source 47
    target 454
  ]
  edge [
    source 47
    target 2814
  ]
  edge [
    source 47
    target 2815
  ]
  edge [
    source 47
    target 2816
  ]
  edge [
    source 47
    target 2817
  ]
  edge [
    source 47
    target 518
  ]
  edge [
    source 47
    target 2818
  ]
  edge [
    source 47
    target 2819
  ]
  edge [
    source 47
    target 692
  ]
  edge [
    source 47
    target 2820
  ]
  edge [
    source 47
    target 2821
  ]
  edge [
    source 47
    target 2613
  ]
  edge [
    source 47
    target 1801
  ]
  edge [
    source 47
    target 2822
  ]
  edge [
    source 47
    target 2823
  ]
  edge [
    source 47
    target 2824
  ]
  edge [
    source 47
    target 804
  ]
  edge [
    source 47
    target 2825
  ]
  edge [
    source 47
    target 2826
  ]
  edge [
    source 47
    target 519
  ]
  edge [
    source 47
    target 2827
  ]
  edge [
    source 47
    target 1040
  ]
  edge [
    source 47
    target 2828
  ]
  edge [
    source 47
    target 2538
  ]
  edge [
    source 47
    target 235
  ]
  edge [
    source 47
    target 2829
  ]
  edge [
    source 47
    target 2830
  ]
  edge [
    source 47
    target 2831
  ]
  edge [
    source 47
    target 2832
  ]
  edge [
    source 47
    target 1833
  ]
  edge [
    source 47
    target 2228
  ]
  edge [
    source 47
    target 2833
  ]
  edge [
    source 47
    target 712
  ]
  edge [
    source 47
    target 2834
  ]
  edge [
    source 47
    target 2349
  ]
  edge [
    source 47
    target 2835
  ]
  edge [
    source 47
    target 760
  ]
  edge [
    source 47
    target 2836
  ]
  edge [
    source 47
    target 2837
  ]
  edge [
    source 47
    target 2838
  ]
  edge [
    source 47
    target 2839
  ]
  edge [
    source 47
    target 2241
  ]
  edge [
    source 47
    target 2840
  ]
  edge [
    source 47
    target 258
  ]
  edge [
    source 47
    target 218
  ]
  edge [
    source 47
    target 2841
  ]
  edge [
    source 47
    target 2553
  ]
  edge [
    source 47
    target 2554
  ]
  edge [
    source 47
    target 429
  ]
  edge [
    source 47
    target 2555
  ]
  edge [
    source 47
    target 888
  ]
  edge [
    source 47
    target 2556
  ]
  edge [
    source 47
    target 59
  ]
  edge [
    source 47
    target 2842
  ]
  edge [
    source 47
    target 549
  ]
  edge [
    source 47
    target 535
  ]
  edge [
    source 47
    target 2843
  ]
  edge [
    source 47
    target 2844
  ]
  edge [
    source 47
    target 1213
  ]
  edge [
    source 47
    target 2736
  ]
  edge [
    source 47
    target 2126
  ]
  edge [
    source 47
    target 2845
  ]
  edge [
    source 47
    target 2846
  ]
  edge [
    source 47
    target 858
  ]
  edge [
    source 47
    target 2740
  ]
  edge [
    source 47
    target 2742
  ]
  edge [
    source 47
    target 2724
  ]
  edge [
    source 47
    target 2847
  ]
  edge [
    source 47
    target 2561
  ]
  edge [
    source 47
    target 2738
  ]
  edge [
    source 47
    target 2848
  ]
  edge [
    source 47
    target 2849
  ]
  edge [
    source 47
    target 2850
  ]
  edge [
    source 47
    target 2851
  ]
  edge [
    source 47
    target 2852
  ]
  edge [
    source 47
    target 2853
  ]
  edge [
    source 47
    target 2854
  ]
  edge [
    source 47
    target 2855
  ]
  edge [
    source 47
    target 434
  ]
  edge [
    source 47
    target 1210
  ]
  edge [
    source 47
    target 2856
  ]
  edge [
    source 47
    target 2857
  ]
  edge [
    source 47
    target 501
  ]
  edge [
    source 47
    target 472
  ]
  edge [
    source 47
    target 2858
  ]
  edge [
    source 47
    target 2859
  ]
  edge [
    source 47
    target 2860
  ]
  edge [
    source 47
    target 2861
  ]
  edge [
    source 47
    target 203
  ]
  edge [
    source 47
    target 2862
  ]
  edge [
    source 47
    target 2486
  ]
  edge [
    source 47
    target 2505
  ]
  edge [
    source 47
    target 2863
  ]
  edge [
    source 47
    target 287
  ]
  edge [
    source 47
    target 2864
  ]
  edge [
    source 47
    target 2865
  ]
  edge [
    source 47
    target 202
  ]
  edge [
    source 47
    target 2866
  ]
  edge [
    source 47
    target 2867
  ]
  edge [
    source 47
    target 2868
  ]
  edge [
    source 47
    target 2869
  ]
  edge [
    source 47
    target 504
  ]
  edge [
    source 47
    target 2870
  ]
  edge [
    source 47
    target 2871
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 2872
  ]
  edge [
    source 48
    target 2873
  ]
  edge [
    source 48
    target 2874
  ]
  edge [
    source 48
    target 2875
  ]
  edge [
    source 48
    target 2876
  ]
  edge [
    source 48
    target 2877
  ]
  edge [
    source 48
    target 2878
  ]
  edge [
    source 48
    target 2879
  ]
  edge [
    source 48
    target 125
  ]
  edge [
    source 48
    target 2880
  ]
  edge [
    source 48
    target 2881
  ]
  edge [
    source 48
    target 750
  ]
  edge [
    source 48
    target 2882
  ]
  edge [
    source 48
    target 2883
  ]
  edge [
    source 48
    target 509
  ]
  edge [
    source 48
    target 2884
  ]
  edge [
    source 48
    target 2885
  ]
  edge [
    source 48
    target 2886
  ]
  edge [
    source 48
    target 2887
  ]
  edge [
    source 48
    target 606
  ]
  edge [
    source 48
    target 2888
  ]
  edge [
    source 48
    target 201
  ]
  edge [
    source 48
    target 2889
  ]
  edge [
    source 48
    target 2890
  ]
  edge [
    source 48
    target 2891
  ]
  edge [
    source 48
    target 2892
  ]
  edge [
    source 48
    target 2893
  ]
  edge [
    source 48
    target 1194
  ]
  edge [
    source 48
    target 721
  ]
  edge [
    source 48
    target 2894
  ]
  edge [
    source 48
    target 2439
  ]
  edge [
    source 48
    target 2895
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 58
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 2096
  ]
  edge [
    source 51
    target 2162
  ]
  edge [
    source 51
    target 2163
  ]
  edge [
    source 51
    target 2164
  ]
  edge [
    source 51
    target 2165
  ]
  edge [
    source 51
    target 1890
  ]
  edge [
    source 51
    target 2166
  ]
  edge [
    source 51
    target 884
  ]
  edge [
    source 51
    target 2167
  ]
  edge [
    source 51
    target 462
  ]
  edge [
    source 51
    target 2168
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 2896
  ]
  edge [
    source 52
    target 2897
  ]
  edge [
    source 52
    target 2898
  ]
  edge [
    source 52
    target 2899
  ]
  edge [
    source 52
    target 2900
  ]
  edge [
    source 52
    target 2901
  ]
  edge [
    source 52
    target 2902
  ]
  edge [
    source 52
    target 2903
  ]
  edge [
    source 52
    target 1155
  ]
  edge [
    source 52
    target 2904
  ]
  edge [
    source 52
    target 2905
  ]
  edge [
    source 52
    target 692
  ]
  edge [
    source 52
    target 549
  ]
  edge [
    source 52
    target 2906
  ]
  edge [
    source 52
    target 735
  ]
  edge [
    source 52
    target 2907
  ]
  edge [
    source 52
    target 367
  ]
  edge [
    source 52
    target 509
  ]
  edge [
    source 52
    target 1865
  ]
  edge [
    source 52
    target 2908
  ]
  edge [
    source 52
    target 2909
  ]
  edge [
    source 52
    target 2910
  ]
  edge [
    source 52
    target 2081
  ]
  edge [
    source 52
    target 2911
  ]
  edge [
    source 52
    target 1210
  ]
  edge [
    source 52
    target 2038
  ]
  edge [
    source 52
    target 767
  ]
  edge [
    source 52
    target 535
  ]
  edge [
    source 52
    target 554
  ]
  edge [
    source 52
    target 555
  ]
  edge [
    source 52
    target 556
  ]
  edge [
    source 52
    target 2736
  ]
  edge [
    source 52
    target 2912
  ]
  edge [
    source 52
    target 2913
  ]
  edge [
    source 52
    target 2914
  ]
  edge [
    source 52
    target 255
  ]
  edge [
    source 52
    target 2113
  ]
  edge [
    source 52
    target 2833
  ]
  edge [
    source 52
    target 2915
  ]
  edge [
    source 52
    target 1025
  ]
  edge [
    source 52
    target 454
  ]
  edge [
    source 52
    target 218
  ]
  edge [
    source 52
    target 2916
  ]
  edge [
    source 52
    target 2917
  ]
  edge [
    source 52
    target 2918
  ]
  edge [
    source 52
    target 2919
  ]
  edge [
    source 52
    target 1703
  ]
  edge [
    source 52
    target 2920
  ]
  edge [
    source 52
    target 2921
  ]
  edge [
    source 52
    target 2922
  ]
  edge [
    source 52
    target 430
  ]
  edge [
    source 52
    target 2923
  ]
  edge [
    source 52
    target 2924
  ]
  edge [
    source 52
    target 1934
  ]
  edge [
    source 52
    target 2925
  ]
  edge [
    source 52
    target 2926
  ]
  edge [
    source 52
    target 2927
  ]
  edge [
    source 52
    target 2928
  ]
  edge [
    source 52
    target 2929
  ]
  edge [
    source 52
    target 2930
  ]
  edge [
    source 52
    target 2931
  ]
  edge [
    source 52
    target 2932
  ]
  edge [
    source 52
    target 752
  ]
  edge [
    source 52
    target 2189
  ]
  edge [
    source 52
    target 2933
  ]
  edge [
    source 52
    target 2934
  ]
  edge [
    source 52
    target 2935
  ]
  edge [
    source 52
    target 2053
  ]
  edge [
    source 52
    target 2936
  ]
  edge [
    source 52
    target 2937
  ]
  edge [
    source 52
    target 2095
  ]
  edge [
    source 52
    target 2938
  ]
  edge [
    source 52
    target 2939
  ]
  edge [
    source 52
    target 2940
  ]
  edge [
    source 52
    target 1675
  ]
  edge [
    source 52
    target 2941
  ]
  edge [
    source 52
    target 2942
  ]
  edge [
    source 52
    target 2943
  ]
  edge [
    source 52
    target 1888
  ]
  edge [
    source 52
    target 1889
  ]
  edge [
    source 52
    target 167
  ]
  edge [
    source 52
    target 1890
  ]
  edge [
    source 52
    target 1891
  ]
  edge [
    source 52
    target 1892
  ]
  edge [
    source 52
    target 1893
  ]
  edge [
    source 52
    target 1894
  ]
  edge [
    source 52
    target 738
  ]
  edge [
    source 52
    target 721
  ]
  edge [
    source 52
    target 1895
  ]
  edge [
    source 52
    target 1896
  ]
  edge [
    source 52
    target 1897
  ]
  edge [
    source 52
    target 2944
  ]
  edge [
    source 52
    target 2945
  ]
  edge [
    source 52
    target 2946
  ]
  edge [
    source 52
    target 2947
  ]
  edge [
    source 52
    target 2948
  ]
  edge [
    source 52
    target 2949
  ]
  edge [
    source 52
    target 1813
  ]
  edge [
    source 52
    target 933
  ]
  edge [
    source 52
    target 2950
  ]
  edge [
    source 52
    target 2951
  ]
  edge [
    source 52
    target 1807
  ]
  edge [
    source 52
    target 1809
  ]
  edge [
    source 52
    target 1694
  ]
  edge [
    source 52
    target 1811
  ]
  edge [
    source 52
    target 2952
  ]
  edge [
    source 52
    target 2953
  ]
  edge [
    source 52
    target 1806
  ]
  edge [
    source 52
    target 2954
  ]
  edge [
    source 52
    target 2955
  ]
  edge [
    source 52
    target 2956
  ]
  edge [
    source 52
    target 1803
  ]
  edge [
    source 52
    target 1804
  ]
  edge [
    source 52
    target 2957
  ]
  edge [
    source 52
    target 937
  ]
  edge [
    source 52
    target 243
  ]
  edge [
    source 52
    target 2958
  ]
  edge [
    source 52
    target 1812
  ]
  edge [
    source 52
    target 765
  ]
  edge [
    source 52
    target 2959
  ]
  edge [
    source 52
    target 1971
  ]
  edge [
    source 52
    target 1808
  ]
  edge [
    source 52
    target 1810
  ]
  edge [
    source 52
    target 2960
  ]
  edge [
    source 52
    target 2961
  ]
  edge [
    source 52
    target 2962
  ]
  edge [
    source 52
    target 2963
  ]
  edge [
    source 52
    target 2964
  ]
  edge [
    source 52
    target 2965
  ]
  edge [
    source 52
    target 1805
  ]
  edge [
    source 52
    target 2966
  ]
  edge [
    source 52
    target 201
  ]
  edge [
    source 52
    target 2967
  ]
  edge [
    source 52
    target 2968
  ]
  edge [
    source 52
    target 2969
  ]
  edge [
    source 52
    target 2970
  ]
  edge [
    source 52
    target 2971
  ]
  edge [
    source 52
    target 1249
  ]
  edge [
    source 52
    target 779
  ]
  edge [
    source 52
    target 2972
  ]
  edge [
    source 52
    target 672
  ]
  edge [
    source 52
    target 2973
  ]
  edge [
    source 52
    target 2974
  ]
  edge [
    source 52
    target 807
  ]
  edge [
    source 52
    target 2975
  ]
  edge [
    source 52
    target 2976
  ]
  edge [
    source 52
    target 366
  ]
  edge [
    source 52
    target 821
  ]
  edge [
    source 52
    target 2977
  ]
  edge [
    source 52
    target 2978
  ]
  edge [
    source 52
    target 2979
  ]
  edge [
    source 52
    target 2192
  ]
  edge [
    source 52
    target 2980
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 52
    target 59
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 2981
  ]
  edge [
    source 53
    target 480
  ]
  edge [
    source 53
    target 2982
  ]
  edge [
    source 53
    target 2531
  ]
  edge [
    source 53
    target 2983
  ]
  edge [
    source 53
    target 2984
  ]
  edge [
    source 53
    target 2985
  ]
  edge [
    source 53
    target 2986
  ]
  edge [
    source 53
    target 768
  ]
  edge [
    source 53
    target 2987
  ]
  edge [
    source 53
    target 2988
  ]
  edge [
    source 53
    target 2989
  ]
  edge [
    source 53
    target 2990
  ]
  edge [
    source 53
    target 2991
  ]
  edge [
    source 53
    target 788
  ]
  edge [
    source 53
    target 779
  ]
  edge [
    source 53
    target 2443
  ]
  edge [
    source 53
    target 2444
  ]
  edge [
    source 53
    target 2445
  ]
  edge [
    source 53
    target 679
  ]
  edge [
    source 53
    target 778
  ]
  edge [
    source 53
    target 780
  ]
  edge [
    source 53
    target 781
  ]
  edge [
    source 53
    target 782
  ]
  edge [
    source 53
    target 783
  ]
  edge [
    source 53
    target 784
  ]
  edge [
    source 53
    target 785
  ]
  edge [
    source 53
    target 786
  ]
  edge [
    source 53
    target 787
  ]
  edge [
    source 53
    target 418
  ]
  edge [
    source 53
    target 2992
  ]
  edge [
    source 53
    target 653
  ]
  edge [
    source 53
    target 2993
  ]
  edge [
    source 53
    target 2994
  ]
  edge [
    source 53
    target 458
  ]
  edge [
    source 53
    target 2995
  ]
  edge [
    source 53
    target 2996
  ]
  edge [
    source 53
    target 2997
  ]
  edge [
    source 53
    target 2998
  ]
  edge [
    source 53
    target 2999
  ]
  edge [
    source 53
    target 3000
  ]
  edge [
    source 53
    target 3001
  ]
  edge [
    source 53
    target 1168
  ]
  edge [
    source 53
    target 1313
  ]
  edge [
    source 53
    target 3002
  ]
  edge [
    source 53
    target 3003
  ]
  edge [
    source 53
    target 3004
  ]
  edge [
    source 53
    target 486
  ]
  edge [
    source 53
    target 3005
  ]
  edge [
    source 53
    target 802
  ]
  edge [
    source 53
    target 3006
  ]
  edge [
    source 53
    target 490
  ]
  edge [
    source 53
    target 825
  ]
  edge [
    source 53
    target 3007
  ]
  edge [
    source 53
    target 3008
  ]
  edge [
    source 53
    target 3009
  ]
  edge [
    source 53
    target 3010
  ]
  edge [
    source 53
    target 3011
  ]
  edge [
    source 53
    target 3012
  ]
  edge [
    source 53
    target 3013
  ]
  edge [
    source 53
    target 817
  ]
  edge [
    source 53
    target 2072
  ]
  edge [
    source 53
    target 2465
  ]
  edge [
    source 53
    target 3014
  ]
  edge [
    source 53
    target 3015
  ]
  edge [
    source 53
    target 3016
  ]
  edge [
    source 53
    target 2062
  ]
  edge [
    source 53
    target 2095
  ]
  edge [
    source 53
    target 562
  ]
  edge [
    source 53
    target 3017
  ]
  edge [
    source 53
    target 3018
  ]
  edge [
    source 53
    target 3019
  ]
  edge [
    source 53
    target 583
  ]
  edge [
    source 53
    target 3020
  ]
  edge [
    source 53
    target 3021
  ]
  edge [
    source 53
    target 3022
  ]
  edge [
    source 53
    target 511
  ]
  edge [
    source 53
    target 3023
  ]
  edge [
    source 53
    target 3024
  ]
  edge [
    source 53
    target 3025
  ]
  edge [
    source 53
    target 893
  ]
  edge [
    source 53
    target 3026
  ]
  edge [
    source 53
    target 3027
  ]
  edge [
    source 53
    target 3028
  ]
  edge [
    source 53
    target 3029
  ]
  edge [
    source 53
    target 3030
  ]
  edge [
    source 53
    target 3031
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 3032
  ]
  edge [
    source 54
    target 3033
  ]
  edge [
    source 54
    target 3034
  ]
  edge [
    source 54
    target 401
  ]
  edge [
    source 54
    target 3035
  ]
  edge [
    source 54
    target 3036
  ]
  edge [
    source 54
    target 3037
  ]
  edge [
    source 54
    target 3038
  ]
  edge [
    source 54
    target 882
  ]
  edge [
    source 54
    target 3039
  ]
  edge [
    source 54
    target 3040
  ]
  edge [
    source 54
    target 884
  ]
  edge [
    source 54
    target 3041
  ]
  edge [
    source 54
    target 3042
  ]
  edge [
    source 54
    target 2961
  ]
  edge [
    source 54
    target 3043
  ]
  edge [
    source 54
    target 3044
  ]
  edge [
    source 54
    target 3045
  ]
  edge [
    source 54
    target 3046
  ]
  edge [
    source 54
    target 511
  ]
  edge [
    source 54
    target 3047
  ]
  edge [
    source 54
    target 3048
  ]
  edge [
    source 54
    target 3049
  ]
  edge [
    source 54
    target 2924
  ]
  edge [
    source 54
    target 3050
  ]
  edge [
    source 54
    target 3026
  ]
  edge [
    source 54
    target 3051
  ]
  edge [
    source 54
    target 3052
  ]
  edge [
    source 54
    target 3053
  ]
  edge [
    source 54
    target 3054
  ]
  edge [
    source 54
    target 418
  ]
  edge [
    source 54
    target 419
  ]
  edge [
    source 54
    target 420
  ]
  edge [
    source 54
    target 421
  ]
  edge [
    source 54
    target 422
  ]
  edge [
    source 54
    target 423
  ]
  edge [
    source 54
    target 424
  ]
  edge [
    source 54
    target 425
  ]
  edge [
    source 54
    target 426
  ]
  edge [
    source 54
    target 427
  ]
  edge [
    source 54
    target 428
  ]
  edge [
    source 54
    target 429
  ]
  edge [
    source 54
    target 430
  ]
  edge [
    source 54
    target 431
  ]
  edge [
    source 54
    target 432
  ]
  edge [
    source 54
    target 343
  ]
  edge [
    source 54
    target 433
  ]
  edge [
    source 54
    target 434
  ]
  edge [
    source 54
    target 435
  ]
  edge [
    source 54
    target 436
  ]
  edge [
    source 54
    target 437
  ]
  edge [
    source 54
    target 438
  ]
  edge [
    source 54
    target 439
  ]
  edge [
    source 54
    target 440
  ]
  edge [
    source 54
    target 441
  ]
  edge [
    source 54
    target 3055
  ]
  edge [
    source 54
    target 3056
  ]
  edge [
    source 54
    target 3057
  ]
  edge [
    source 54
    target 3058
  ]
  edge [
    source 54
    target 526
  ]
  edge [
    source 54
    target 3059
  ]
  edge [
    source 54
    target 3060
  ]
  edge [
    source 54
    target 3061
  ]
  edge [
    source 54
    target 3062
  ]
  edge [
    source 54
    target 3063
  ]
  edge [
    source 54
    target 2192
  ]
  edge [
    source 54
    target 3064
  ]
  edge [
    source 54
    target 3065
  ]
  edge [
    source 55
    target 3066
  ]
  edge [
    source 55
    target 3067
  ]
  edge [
    source 55
    target 3068
  ]
  edge [
    source 55
    target 3069
  ]
  edge [
    source 55
    target 3070
  ]
  edge [
    source 55
    target 3071
  ]
  edge [
    source 55
    target 3072
  ]
  edge [
    source 55
    target 3073
  ]
  edge [
    source 55
    target 3074
  ]
  edge [
    source 55
    target 3075
  ]
  edge [
    source 55
    target 3076
  ]
  edge [
    source 55
    target 3077
  ]
  edge [
    source 55
    target 3078
  ]
  edge [
    source 55
    target 3079
  ]
  edge [
    source 55
    target 3080
  ]
  edge [
    source 55
    target 372
  ]
  edge [
    source 55
    target 3081
  ]
  edge [
    source 55
    target 3082
  ]
  edge [
    source 55
    target 727
  ]
  edge [
    source 55
    target 1028
  ]
  edge [
    source 55
    target 3083
  ]
  edge [
    source 55
    target 3084
  ]
  edge [
    source 55
    target 125
  ]
  edge [
    source 55
    target 3085
  ]
  edge [
    source 55
    target 3086
  ]
  edge [
    source 55
    target 2661
  ]
  edge [
    source 55
    target 3087
  ]
  edge [
    source 55
    target 3088
  ]
  edge [
    source 55
    target 3089
  ]
  edge [
    source 55
    target 3090
  ]
  edge [
    source 55
    target 3053
  ]
  edge [
    source 55
    target 3091
  ]
  edge [
    source 55
    target 3092
  ]
  edge [
    source 55
    target 2600
  ]
  edge [
    source 55
    target 3093
  ]
  edge [
    source 55
    target 3094
  ]
  edge [
    source 55
    target 3095
  ]
  edge [
    source 55
    target 2597
  ]
  edge [
    source 55
    target 3096
  ]
  edge [
    source 55
    target 3097
  ]
  edge [
    source 55
    target 2681
  ]
  edge [
    source 55
    target 1048
  ]
  edge [
    source 55
    target 3098
  ]
  edge [
    source 55
    target 3099
  ]
  edge [
    source 55
    target 3100
  ]
  edge [
    source 55
    target 3101
  ]
  edge [
    source 55
    target 3102
  ]
  edge [
    source 55
    target 3103
  ]
  edge [
    source 55
    target 3104
  ]
  edge [
    source 55
    target 2164
  ]
  edge [
    source 55
    target 1998
  ]
  edge [
    source 55
    target 3105
  ]
  edge [
    source 55
    target 3106
  ]
  edge [
    source 55
    target 3107
  ]
  edge [
    source 55
    target 3108
  ]
  edge [
    source 55
    target 269
  ]
  edge [
    source 55
    target 3109
  ]
  edge [
    source 55
    target 3110
  ]
  edge [
    source 55
    target 3111
  ]
  edge [
    source 55
    target 3112
  ]
  edge [
    source 55
    target 944
  ]
  edge [
    source 55
    target 1721
  ]
  edge [
    source 55
    target 3113
  ]
  edge [
    source 55
    target 3114
  ]
  edge [
    source 55
    target 276
  ]
  edge [
    source 55
    target 3115
  ]
  edge [
    source 55
    target 2430
  ]
  edge [
    source 55
    target 2623
  ]
  edge [
    source 55
    target 721
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 3116
  ]
  edge [
    source 56
    target 3117
  ]
  edge [
    source 56
    target 3118
  ]
  edge [
    source 56
    target 766
  ]
  edge [
    source 56
    target 3119
  ]
  edge [
    source 56
    target 3120
  ]
  edge [
    source 56
    target 3121
  ]
  edge [
    source 56
    target 3122
  ]
  edge [
    source 56
    target 790
  ]
  edge [
    source 56
    target 3123
  ]
  edge [
    source 56
    target 3124
  ]
  edge [
    source 56
    target 2437
  ]
  edge [
    source 56
    target 3125
  ]
  edge [
    source 56
    target 2998
  ]
  edge [
    source 56
    target 3126
  ]
  edge [
    source 56
    target 653
  ]
  edge [
    source 56
    target 3127
  ]
  edge [
    source 56
    target 3128
  ]
  edge [
    source 56
    target 3129
  ]
  edge [
    source 56
    target 2438
  ]
  edge [
    source 56
    target 3130
  ]
  edge [
    source 56
    target 356
  ]
  edge [
    source 56
    target 365
  ]
  edge [
    source 56
    target 3131
  ]
  edge [
    source 57
    target 3132
  ]
  edge [
    source 57
    target 3133
  ]
  edge [
    source 57
    target 768
  ]
  edge [
    source 57
    target 653
  ]
  edge [
    source 57
    target 779
  ]
  edge [
    source 57
    target 2443
  ]
  edge [
    source 57
    target 2444
  ]
  edge [
    source 57
    target 2445
  ]
  edge [
    source 57
    target 679
  ]
  edge [
    source 57
    target 2446
  ]
  edge [
    source 57
    target 2447
  ]
  edge [
    source 57
    target 2448
  ]
  edge [
    source 57
    target 458
  ]
  edge [
    source 57
    target 2449
  ]
  edge [
    source 57
    target 2450
  ]
  edge [
    source 57
    target 370
  ]
  edge [
    source 57
    target 2451
  ]
  edge [
    source 57
    target 2452
  ]
  edge [
    source 57
    target 2453
  ]
  edge [
    source 57
    target 1249
  ]
  edge [
    source 57
    target 2454
  ]
  edge [
    source 57
    target 2455
  ]
  edge [
    source 57
    target 2456
  ]
  edge [
    source 57
    target 2457
  ]
  edge [
    source 57
    target 767
  ]
  edge [
    source 57
    target 2458
  ]
  edge [
    source 57
    target 2459
  ]
  edge [
    source 57
    target 2460
  ]
  edge [
    source 57
    target 3134
  ]
  edge [
    source 57
    target 3135
  ]
  edge [
    source 57
    target 3136
  ]
  edge [
    source 57
    target 3137
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 1040
  ]
  edge [
    source 58
    target 3138
  ]
  edge [
    source 58
    target 125
  ]
  edge [
    source 58
    target 1041
  ]
  edge [
    source 58
    target 1042
  ]
  edge [
    source 58
    target 3139
  ]
  edge [
    source 58
    target 404
  ]
  edge [
    source 58
    target 405
  ]
  edge [
    source 58
    target 406
  ]
  edge [
    source 58
    target 407
  ]
  edge [
    source 58
    target 408
  ]
  edge [
    source 58
    target 409
  ]
  edge [
    source 58
    target 410
  ]
  edge [
    source 58
    target 411
  ]
  edge [
    source 58
    target 412
  ]
  edge [
    source 58
    target 413
  ]
  edge [
    source 58
    target 414
  ]
  edge [
    source 58
    target 415
  ]
  edge [
    source 58
    target 416
  ]
  edge [
    source 58
    target 130
  ]
  edge [
    source 58
    target 417
  ]
  edge [
    source 58
    target 1212
  ]
  edge [
    source 58
    target 1109
  ]
  edge [
    source 58
    target 3140
  ]
  edge [
    source 58
    target 3141
  ]
  edge [
    source 58
    target 1098
  ]
  edge [
    source 58
    target 3142
  ]
  edge [
    source 58
    target 3143
  ]
  edge [
    source 58
    target 3144
  ]
  edge [
    source 58
    target 3145
  ]
  edge [
    source 58
    target 454
  ]
  edge [
    source 58
    target 3146
  ]
  edge [
    source 58
    target 424
  ]
  edge [
    source 58
    target 3147
  ]
  edge [
    source 58
    target 3148
  ]
  edge [
    source 58
    target 3149
  ]
  edge [
    source 58
    target 403
  ]
  edge [
    source 58
    target 3150
  ]
  edge [
    source 58
    target 3151
  ]
  edge [
    source 58
    target 3152
  ]
  edge [
    source 58
    target 3153
  ]
  edge [
    source 58
    target 3154
  ]
  edge [
    source 58
    target 3155
  ]
  edge [
    source 58
    target 3156
  ]
  edge [
    source 58
    target 3157
  ]
  edge [
    source 58
    target 3158
  ]
  edge [
    source 58
    target 3159
  ]
  edge [
    source 58
    target 3160
  ]
  edge [
    source 58
    target 3161
  ]
  edge [
    source 58
    target 3162
  ]
  edge [
    source 58
    target 3163
  ]
  edge [
    source 58
    target 3164
  ]
  edge [
    source 58
    target 3165
  ]
  edge [
    source 58
    target 3166
  ]
  edge [
    source 58
    target 2204
  ]
  edge [
    source 58
    target 3167
  ]
  edge [
    source 58
    target 3168
  ]
  edge [
    source 58
    target 2172
  ]
  edge [
    source 58
    target 3169
  ]
  edge [
    source 58
    target 3170
  ]
  edge [
    source 58
    target 3171
  ]
  edge [
    source 58
    target 3172
  ]
  edge [
    source 58
    target 712
  ]
  edge [
    source 58
    target 3173
  ]
  edge [
    source 58
    target 736
  ]
  edge [
    source 58
    target 1292
  ]
  edge [
    source 58
    target 3174
  ]
  edge [
    source 58
    target 3175
  ]
  edge [
    source 58
    target 2916
  ]
  edge [
    source 58
    target 3176
  ]
  edge [
    source 58
    target 3177
  ]
  edge [
    source 58
    target 3178
  ]
  edge [
    source 58
    target 1981
  ]
  edge [
    source 58
    target 1092
  ]
  edge [
    source 58
    target 3179
  ]
  edge [
    source 58
    target 3180
  ]
  edge [
    source 58
    target 439
  ]
  edge [
    source 58
    target 1929
  ]
  edge [
    source 58
    target 3181
  ]
  edge [
    source 58
    target 643
  ]
  edge [
    source 58
    target 2186
  ]
  edge [
    source 58
    target 475
  ]
  edge [
    source 58
    target 218
  ]
  edge [
    source 58
    target 3182
  ]
  edge [
    source 58
    target 3183
  ]
  edge [
    source 58
    target 3184
  ]
  edge [
    source 58
    target 3185
  ]
  edge [
    source 58
    target 933
  ]
  edge [
    source 58
    target 3186
  ]
  edge [
    source 58
    target 556
  ]
  edge [
    source 58
    target 3187
  ]
  edge [
    source 58
    target 3188
  ]
  edge [
    source 58
    target 3189
  ]
  edge [
    source 58
    target 3190
  ]
  edge [
    source 58
    target 3191
  ]
  edge [
    source 58
    target 549
  ]
  edge [
    source 58
    target 2915
  ]
  edge [
    source 58
    target 3192
  ]
  edge [
    source 58
    target 302
  ]
  edge [
    source 58
    target 1007
  ]
  edge [
    source 58
    target 3193
  ]
  edge [
    source 58
    target 3194
  ]
  edge [
    source 58
    target 1320
  ]
  edge [
    source 58
    target 1611
  ]
  edge [
    source 58
    target 3195
  ]
  edge [
    source 58
    target 3196
  ]
  edge [
    source 58
    target 1321
  ]
  edge [
    source 58
    target 1322
  ]
  edge [
    source 58
    target 3197
  ]
  edge [
    source 58
    target 3198
  ]
  edge [
    source 58
    target 1323
  ]
  edge [
    source 58
    target 3199
  ]
  edge [
    source 58
    target 3200
  ]
  edge [
    source 58
    target 3201
  ]
  edge [
    source 58
    target 3202
  ]
  edge [
    source 58
    target 3203
  ]
  edge [
    source 58
    target 1546
  ]
  edge [
    source 58
    target 3204
  ]
  edge [
    source 58
    target 1324
  ]
  edge [
    source 58
    target 1326
  ]
  edge [
    source 58
    target 1325
  ]
  edge [
    source 58
    target 1327
  ]
  edge [
    source 58
    target 3205
  ]
  edge [
    source 58
    target 3206
  ]
  edge [
    source 58
    target 3207
  ]
  edge [
    source 58
    target 1331
  ]
  edge [
    source 58
    target 3208
  ]
  edge [
    source 58
    target 3209
  ]
  edge [
    source 58
    target 1332
  ]
  edge [
    source 58
    target 1330
  ]
  edge [
    source 58
    target 3210
  ]
  edge [
    source 58
    target 1572
  ]
  edge [
    source 58
    target 3211
  ]
  edge [
    source 58
    target 3212
  ]
  edge [
    source 58
    target 3213
  ]
  edge [
    source 58
    target 1335
  ]
  edge [
    source 58
    target 3214
  ]
  edge [
    source 58
    target 1336
  ]
  edge [
    source 58
    target 1608
  ]
  edge [
    source 58
    target 1337
  ]
  edge [
    source 58
    target 3215
  ]
  edge [
    source 58
    target 3216
  ]
  edge [
    source 58
    target 1615
  ]
  edge [
    source 58
    target 3217
  ]
  edge [
    source 58
    target 1339
  ]
  edge [
    source 58
    target 3218
  ]
  edge [
    source 58
    target 1338
  ]
  edge [
    source 58
    target 3219
  ]
  edge [
    source 58
    target 1341
  ]
  edge [
    source 58
    target 1342
  ]
  edge [
    source 58
    target 3220
  ]
  edge [
    source 58
    target 3221
  ]
  edge [
    source 58
    target 1344
  ]
  edge [
    source 58
    target 1343
  ]
  edge [
    source 58
    target 3222
  ]
  edge [
    source 58
    target 1573
  ]
  edge [
    source 58
    target 3223
  ]
  edge [
    source 58
    target 1347
  ]
  edge [
    source 58
    target 1346
  ]
  edge [
    source 58
    target 1345
  ]
  edge [
    source 58
    target 1349
  ]
  edge [
    source 58
    target 1351
  ]
  edge [
    source 58
    target 3224
  ]
  edge [
    source 58
    target 3225
  ]
  edge [
    source 58
    target 1352
  ]
  edge [
    source 58
    target 1355
  ]
  edge [
    source 58
    target 3226
  ]
  edge [
    source 58
    target 3227
  ]
  edge [
    source 58
    target 1389
  ]
  edge [
    source 58
    target 1357
  ]
  edge [
    source 58
    target 1358
  ]
  edge [
    source 58
    target 3228
  ]
  edge [
    source 58
    target 1630
  ]
  edge [
    source 58
    target 3229
  ]
  edge [
    source 58
    target 3230
  ]
  edge [
    source 58
    target 1359
  ]
  edge [
    source 58
    target 3231
  ]
  edge [
    source 58
    target 3232
  ]
  edge [
    source 58
    target 3233
  ]
  edge [
    source 58
    target 3234
  ]
  edge [
    source 58
    target 3235
  ]
  edge [
    source 58
    target 3236
  ]
  edge [
    source 58
    target 1639
  ]
  edge [
    source 58
    target 3237
  ]
  edge [
    source 58
    target 1362
  ]
  edge [
    source 58
    target 3238
  ]
  edge [
    source 58
    target 1560
  ]
  edge [
    source 58
    target 3239
  ]
  edge [
    source 58
    target 1382
  ]
  edge [
    source 58
    target 3240
  ]
  edge [
    source 58
    target 1364
  ]
  edge [
    source 58
    target 3241
  ]
  edge [
    source 58
    target 1365
  ]
  edge [
    source 58
    target 3242
  ]
  edge [
    source 58
    target 3243
  ]
  edge [
    source 58
    target 3244
  ]
  edge [
    source 58
    target 1368
  ]
  edge [
    source 58
    target 1372
  ]
  edge [
    source 58
    target 1370
  ]
  edge [
    source 58
    target 1371
  ]
  edge [
    source 58
    target 1369
  ]
  edge [
    source 58
    target 1374
  ]
  edge [
    source 58
    target 1375
  ]
  edge [
    source 58
    target 1594
  ]
  edge [
    source 58
    target 3245
  ]
  edge [
    source 58
    target 3246
  ]
  edge [
    source 58
    target 3247
  ]
  edge [
    source 58
    target 1376
  ]
  edge [
    source 58
    target 1377
  ]
  edge [
    source 58
    target 1378
  ]
  edge [
    source 58
    target 1379
  ]
  edge [
    source 58
    target 1380
  ]
  edge [
    source 58
    target 3248
  ]
  edge [
    source 58
    target 3249
  ]
  edge [
    source 58
    target 3250
  ]
  edge [
    source 58
    target 3251
  ]
  edge [
    source 58
    target 3252
  ]
  edge [
    source 58
    target 3253
  ]
  edge [
    source 58
    target 3254
  ]
  edge [
    source 58
    target 3255
  ]
  edge [
    source 58
    target 1636
  ]
  edge [
    source 58
    target 3256
  ]
  edge [
    source 58
    target 1547
  ]
  edge [
    source 58
    target 3257
  ]
  edge [
    source 58
    target 3258
  ]
  edge [
    source 58
    target 1581
  ]
  edge [
    source 58
    target 3259
  ]
  edge [
    source 58
    target 1381
  ]
  edge [
    source 58
    target 3260
  ]
  edge [
    source 58
    target 3261
  ]
  edge [
    source 58
    target 3262
  ]
  edge [
    source 58
    target 1553
  ]
  edge [
    source 58
    target 1628
  ]
  edge [
    source 58
    target 1384
  ]
  edge [
    source 58
    target 1386
  ]
  edge [
    source 58
    target 1390
  ]
  edge [
    source 58
    target 1387
  ]
  edge [
    source 58
    target 1385
  ]
  edge [
    source 58
    target 3263
  ]
  edge [
    source 58
    target 3264
  ]
  edge [
    source 58
    target 3265
  ]
  edge [
    source 58
    target 3266
  ]
  edge [
    source 58
    target 3267
  ]
  edge [
    source 58
    target 3268
  ]
  edge [
    source 58
    target 863
  ]
  edge [
    source 58
    target 3269
  ]
  edge [
    source 58
    target 3270
  ]
  edge [
    source 58
    target 3271
  ]
  edge [
    source 58
    target 3272
  ]
  edge [
    source 58
    target 3273
  ]
  edge [
    source 58
    target 3274
  ]
  edge [
    source 58
    target 3275
  ]
  edge [
    source 58
    target 3276
  ]
  edge [
    source 58
    target 3277
  ]
  edge [
    source 58
    target 3278
  ]
  edge [
    source 58
    target 1394
  ]
  edge [
    source 58
    target 1395
  ]
  edge [
    source 58
    target 3279
  ]
  edge [
    source 58
    target 1397
  ]
  edge [
    source 58
    target 1398
  ]
  edge [
    source 58
    target 1396
  ]
  edge [
    source 58
    target 3280
  ]
  edge [
    source 58
    target 3281
  ]
  edge [
    source 58
    target 1545
  ]
  edge [
    source 58
    target 3282
  ]
  edge [
    source 58
    target 3283
  ]
  edge [
    source 58
    target 1606
  ]
  edge [
    source 58
    target 1391
  ]
  edge [
    source 58
    target 1399
  ]
  edge [
    source 58
    target 1400
  ]
  edge [
    source 58
    target 3284
  ]
  edge [
    source 58
    target 1402
  ]
  edge [
    source 58
    target 1403
  ]
  edge [
    source 58
    target 3285
  ]
  edge [
    source 58
    target 1404
  ]
  edge [
    source 58
    target 3286
  ]
  edge [
    source 58
    target 1405
  ]
  edge [
    source 58
    target 3287
  ]
  edge [
    source 58
    target 3288
  ]
  edge [
    source 58
    target 3289
  ]
  edge [
    source 58
    target 3290
  ]
  edge [
    source 58
    target 3291
  ]
  edge [
    source 58
    target 1407
  ]
  edge [
    source 58
    target 3292
  ]
  edge [
    source 58
    target 3293
  ]
  edge [
    source 58
    target 1409
  ]
  edge [
    source 58
    target 1410
  ]
  edge [
    source 58
    target 1544
  ]
  edge [
    source 58
    target 1411
  ]
  edge [
    source 58
    target 1412
  ]
  edge [
    source 58
    target 1413
  ]
  edge [
    source 58
    target 3294
  ]
  edge [
    source 58
    target 3295
  ]
  edge [
    source 58
    target 1593
  ]
  edge [
    source 58
    target 1438
  ]
  edge [
    source 58
    target 3296
  ]
  edge [
    source 58
    target 1415
  ]
  edge [
    source 58
    target 3297
  ]
  edge [
    source 58
    target 3298
  ]
  edge [
    source 58
    target 1613
  ]
  edge [
    source 58
    target 3299
  ]
  edge [
    source 58
    target 3300
  ]
  edge [
    source 58
    target 1612
  ]
  edge [
    source 58
    target 3301
  ]
  edge [
    source 58
    target 3302
  ]
  edge [
    source 58
    target 3303
  ]
  edge [
    source 58
    target 1417
  ]
  edge [
    source 58
    target 1419
  ]
  edge [
    source 58
    target 1416
  ]
  edge [
    source 58
    target 3304
  ]
  edge [
    source 58
    target 3305
  ]
  edge [
    source 58
    target 3306
  ]
  edge [
    source 58
    target 3307
  ]
  edge [
    source 58
    target 1592
  ]
  edge [
    source 58
    target 3308
  ]
  edge [
    source 58
    target 1422
  ]
  edge [
    source 58
    target 1421
  ]
  edge [
    source 58
    target 3309
  ]
  edge [
    source 58
    target 1418
  ]
  edge [
    source 58
    target 1626
  ]
  edge [
    source 58
    target 3310
  ]
  edge [
    source 58
    target 1423
  ]
  edge [
    source 58
    target 3311
  ]
  edge [
    source 58
    target 3312
  ]
  edge [
    source 58
    target 1425
  ]
  edge [
    source 58
    target 1424
  ]
  edge [
    source 58
    target 1426
  ]
  edge [
    source 58
    target 3313
  ]
  edge [
    source 58
    target 1388
  ]
  edge [
    source 58
    target 1427
  ]
  edge [
    source 58
    target 1640
  ]
  edge [
    source 58
    target 1440
  ]
  edge [
    source 58
    target 3314
  ]
  edge [
    source 58
    target 1428
  ]
  edge [
    source 58
    target 3315
  ]
  edge [
    source 58
    target 1429
  ]
  edge [
    source 58
    target 1430
  ]
  edge [
    source 58
    target 3316
  ]
  edge [
    source 58
    target 3317
  ]
  edge [
    source 58
    target 1431
  ]
  edge [
    source 58
    target 3318
  ]
  edge [
    source 58
    target 1433
  ]
  edge [
    source 58
    target 1571
  ]
  edge [
    source 58
    target 1434
  ]
  edge [
    source 58
    target 1435
  ]
  edge [
    source 58
    target 3319
  ]
  edge [
    source 58
    target 3320
  ]
  edge [
    source 58
    target 3321
  ]
  edge [
    source 58
    target 1439
  ]
  edge [
    source 58
    target 1441
  ]
  edge [
    source 58
    target 3322
  ]
  edge [
    source 58
    target 3323
  ]
  edge [
    source 58
    target 1595
  ]
  edge [
    source 58
    target 3324
  ]
  edge [
    source 58
    target 3325
  ]
  edge [
    source 58
    target 1632
  ]
  edge [
    source 58
    target 1442
  ]
  edge [
    source 58
    target 1443
  ]
  edge [
    source 58
    target 1444
  ]
  edge [
    source 58
    target 3326
  ]
  edge [
    source 58
    target 1508
  ]
  edge [
    source 58
    target 1445
  ]
  edge [
    source 58
    target 3327
  ]
  edge [
    source 58
    target 1534
  ]
  edge [
    source 58
    target 3328
  ]
  edge [
    source 58
    target 3329
  ]
  edge [
    source 58
    target 3330
  ]
  edge [
    source 58
    target 3331
  ]
  edge [
    source 58
    target 3332
  ]
  edge [
    source 58
    target 3333
  ]
  edge [
    source 58
    target 1529
  ]
  edge [
    source 58
    target 1446
  ]
  edge [
    source 58
    target 1447
  ]
  edge [
    source 58
    target 3334
  ]
  edge [
    source 58
    target 1448
  ]
  edge [
    source 58
    target 1449
  ]
  edge [
    source 58
    target 1634
  ]
  edge [
    source 58
    target 1575
  ]
  edge [
    source 58
    target 1603
  ]
  edge [
    source 58
    target 1450
  ]
  edge [
    source 58
    target 1451
  ]
  edge [
    source 58
    target 1452
  ]
  edge [
    source 58
    target 1453
  ]
  edge [
    source 58
    target 3335
  ]
  edge [
    source 58
    target 3336
  ]
  edge [
    source 58
    target 3337
  ]
  edge [
    source 58
    target 3338
  ]
  edge [
    source 58
    target 1458
  ]
  edge [
    source 58
    target 1627
  ]
  edge [
    source 58
    target 3339
  ]
  edge [
    source 58
    target 1456
  ]
  edge [
    source 58
    target 1455
  ]
  edge [
    source 58
    target 1457
  ]
  edge [
    source 58
    target 1459
  ]
  edge [
    source 58
    target 3340
  ]
  edge [
    source 58
    target 3341
  ]
  edge [
    source 58
    target 3342
  ]
  edge [
    source 58
    target 1462
  ]
  edge [
    source 58
    target 1463
  ]
  edge [
    source 58
    target 1464
  ]
  edge [
    source 58
    target 3343
  ]
  edge [
    source 58
    target 1579
  ]
  edge [
    source 58
    target 1465
  ]
  edge [
    source 58
    target 3344
  ]
  edge [
    source 58
    target 1548
  ]
  edge [
    source 58
    target 1466
  ]
  edge [
    source 58
    target 3345
  ]
  edge [
    source 58
    target 1467
  ]
  edge [
    source 58
    target 1437
  ]
  edge [
    source 58
    target 3346
  ]
  edge [
    source 58
    target 1468
  ]
  edge [
    source 58
    target 3347
  ]
  edge [
    source 58
    target 1614
  ]
  edge [
    source 58
    target 3348
  ]
  edge [
    source 58
    target 3349
  ]
  edge [
    source 58
    target 3350
  ]
  edge [
    source 58
    target 3351
  ]
  edge [
    source 58
    target 1469
  ]
  edge [
    source 58
    target 3352
  ]
  edge [
    source 58
    target 3353
  ]
  edge [
    source 58
    target 3354
  ]
  edge [
    source 58
    target 974
  ]
  edge [
    source 58
    target 975
  ]
  edge [
    source 58
    target 976
  ]
  edge [
    source 58
    target 977
  ]
  edge [
    source 58
    target 978
  ]
  edge [
    source 58
    target 979
  ]
  edge [
    source 58
    target 980
  ]
  edge [
    source 58
    target 981
  ]
  edge [
    source 58
    target 982
  ]
  edge [
    source 58
    target 983
  ]
  edge [
    source 58
    target 984
  ]
  edge [
    source 58
    target 985
  ]
  edge [
    source 58
    target 986
  ]
  edge [
    source 58
    target 425
  ]
  edge [
    source 58
    target 987
  ]
  edge [
    source 58
    target 988
  ]
  edge [
    source 58
    target 989
  ]
  edge [
    source 58
    target 990
  ]
  edge [
    source 58
    target 357
  ]
  edge [
    source 58
    target 991
  ]
  edge [
    source 58
    target 992
  ]
  edge [
    source 58
    target 993
  ]
  edge [
    source 58
    target 994
  ]
  edge [
    source 58
    target 995
  ]
  edge [
    source 58
    target 996
  ]
  edge [
    source 58
    target 997
  ]
  edge [
    source 58
    target 998
  ]
  edge [
    source 58
    target 999
  ]
  edge [
    source 58
    target 1000
  ]
  edge [
    source 58
    target 1001
  ]
  edge [
    source 58
    target 1002
  ]
  edge [
    source 58
    target 1003
  ]
  edge [
    source 58
    target 1004
  ]
  edge [
    source 58
    target 511
  ]
  edge [
    source 58
    target 1005
  ]
  edge [
    source 58
    target 118
  ]
  edge [
    source 58
    target 1006
  ]
  edge [
    source 58
    target 1008
  ]
  edge [
    source 58
    target 1009
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 3355
  ]
  edge [
    source 59
    target 3017
  ]
  edge [
    source 59
    target 3356
  ]
  edge [
    source 59
    target 3357
  ]
  edge [
    source 59
    target 3358
  ]
  edge [
    source 59
    target 3023
  ]
  edge [
    source 59
    target 3359
  ]
  edge [
    source 59
    target 3360
  ]
  edge [
    source 59
    target 3361
  ]
  edge [
    source 59
    target 3024
  ]
  edge [
    source 59
    target 549
  ]
  edge [
    source 59
    target 3362
  ]
  edge [
    source 59
    target 885
  ]
  edge [
    source 59
    target 2915
  ]
  edge [
    source 59
    target 3161
  ]
  edge [
    source 59
    target 2207
  ]
  edge [
    source 59
    target 3025
  ]
  edge [
    source 59
    target 2349
  ]
  edge [
    source 59
    target 3363
  ]
  edge [
    source 59
    target 3364
  ]
  edge [
    source 59
    target 1127
  ]
  edge [
    source 59
    target 3186
  ]
  edge [
    source 59
    target 255
  ]
  edge [
    source 59
    target 439
  ]
  edge [
    source 59
    target 3041
  ]
  edge [
    source 59
    target 2736
  ]
  edge [
    source 59
    target 2912
  ]
  edge [
    source 59
    target 2913
  ]
  edge [
    source 59
    target 2914
  ]
  edge [
    source 59
    target 2113
  ]
  edge [
    source 59
    target 2833
  ]
  edge [
    source 59
    target 1025
  ]
  edge [
    source 59
    target 454
  ]
  edge [
    source 59
    target 218
  ]
  edge [
    source 59
    target 2916
  ]
  edge [
    source 59
    target 3365
  ]
  edge [
    source 59
    target 3366
  ]
  edge [
    source 59
    target 804
  ]
  edge [
    source 59
    target 3367
  ]
  edge [
    source 59
    target 3368
  ]
  edge [
    source 59
    target 3369
  ]
  edge [
    source 59
    target 3370
  ]
  edge [
    source 59
    target 3371
  ]
  edge [
    source 59
    target 3372
  ]
  edge [
    source 59
    target 3373
  ]
  edge [
    source 59
    target 3374
  ]
  edge [
    source 59
    target 609
  ]
  edge [
    source 59
    target 3375
  ]
  edge [
    source 59
    target 3376
  ]
  edge [
    source 59
    target 876
  ]
  edge [
    source 59
    target 3377
  ]
  edge [
    source 59
    target 3038
  ]
  edge [
    source 59
    target 1929
  ]
  edge [
    source 59
    target 2740
  ]
  edge [
    source 59
    target 3378
  ]
  edge [
    source 59
    target 2961
  ]
  edge [
    source 59
    target 3379
  ]
  edge [
    source 59
    target 1308
  ]
  edge [
    source 59
    target 3380
  ]
  edge [
    source 59
    target 3381
  ]
  edge [
    source 59
    target 3382
  ]
  edge [
    source 59
    target 413
  ]
  edge [
    source 59
    target 3383
  ]
  edge [
    source 59
    target 3384
  ]
  edge [
    source 59
    target 3385
  ]
  edge [
    source 59
    target 130
  ]
  edge [
    source 59
    target 3386
  ]
  edge [
    source 59
    target 3387
  ]
  edge [
    source 59
    target 1115
  ]
  edge [
    source 59
    target 3388
  ]
  edge [
    source 59
    target 3389
  ]
  edge [
    source 59
    target 3390
  ]
  edge [
    source 59
    target 3391
  ]
  edge [
    source 59
    target 3392
  ]
  edge [
    source 59
    target 3393
  ]
  edge [
    source 59
    target 3394
  ]
  edge [
    source 59
    target 3395
  ]
  edge [
    source 59
    target 3396
  ]
  edge [
    source 59
    target 2784
  ]
  edge [
    source 59
    target 3397
  ]
  edge [
    source 59
    target 3398
  ]
  edge [
    source 59
    target 3399
  ]
  edge [
    source 59
    target 3400
  ]
  edge [
    source 59
    target 3401
  ]
  edge [
    source 59
    target 3402
  ]
  edge [
    source 59
    target 3403
  ]
  edge [
    source 59
    target 1211
  ]
  edge [
    source 59
    target 3404
  ]
  edge [
    source 59
    target 1100
  ]
  edge [
    source 59
    target 3405
  ]
  edge [
    source 59
    target 1816
  ]
  edge [
    source 59
    target 3406
  ]
  edge [
    source 59
    target 3407
  ]
  edge [
    source 59
    target 1808
  ]
  edge [
    source 59
    target 3408
  ]
  edge [
    source 59
    target 3409
  ]
  edge [
    source 59
    target 3410
  ]
  edge [
    source 59
    target 3411
  ]
  edge [
    source 59
    target 3412
  ]
  edge [
    source 59
    target 738
  ]
  edge [
    source 59
    target 3413
  ]
  edge [
    source 59
    target 3414
  ]
  edge [
    source 59
    target 3415
  ]
  edge [
    source 59
    target 3416
  ]
  edge [
    source 59
    target 3417
  ]
  edge [
    source 59
    target 3418
  ]
  edge [
    source 59
    target 924
  ]
  edge [
    source 59
    target 3419
  ]
  edge [
    source 59
    target 3420
  ]
  edge [
    source 59
    target 1284
  ]
  edge [
    source 59
    target 3421
  ]
  edge [
    source 59
    target 1839
  ]
  edge [
    source 59
    target 3422
  ]
  edge [
    source 59
    target 509
  ]
  edge [
    source 59
    target 302
  ]
  edge [
    source 59
    target 3423
  ]
  edge [
    source 59
    target 3424
  ]
  edge [
    source 59
    target 1889
  ]
  edge [
    source 59
    target 3425
  ]
  edge [
    source 59
    target 712
  ]
  edge [
    source 59
    target 643
  ]
  edge [
    source 59
    target 3426
  ]
  edge [
    source 59
    target 3427
  ]
  edge [
    source 59
    target 3428
  ]
  edge [
    source 59
    target 2930
  ]
  edge [
    source 59
    target 2843
  ]
  edge [
    source 59
    target 3429
  ]
  edge [
    source 59
    target 1213
  ]
  edge [
    source 59
    target 2817
  ]
  edge [
    source 59
    target 1868
  ]
  edge [
    source 59
    target 3430
  ]
  edge [
    source 59
    target 1870
  ]
  edge [
    source 59
    target 3431
  ]
  edge [
    source 59
    target 556
  ]
  edge [
    source 59
    target 917
  ]
  edge [
    source 59
    target 3432
  ]
  edge [
    source 59
    target 1872
  ]
  edge [
    source 59
    target 2058
  ]
  edge [
    source 59
    target 1471
  ]
  edge [
    source 59
    target 1472
  ]
  edge [
    source 59
    target 1473
  ]
  edge [
    source 59
    target 125
  ]
  edge [
    source 59
    target 1474
  ]
  edge [
    source 59
    target 1475
  ]
  edge [
    source 59
    target 1476
  ]
  edge [
    source 59
    target 1477
  ]
  edge [
    source 59
    target 1478
  ]
  edge [
    source 59
    target 1479
  ]
  edge [
    source 59
    target 1480
  ]
  edge [
    source 59
    target 1852
  ]
  edge [
    source 59
    target 3433
  ]
  edge [
    source 59
    target 2467
  ]
  edge [
    source 59
    target 2925
  ]
  edge [
    source 59
    target 692
  ]
  edge [
    source 59
    target 3434
  ]
  edge [
    source 59
    target 2470
  ]
  edge [
    source 59
    target 3435
  ]
  edge [
    source 59
    target 3436
  ]
  edge [
    source 59
    target 3437
  ]
  edge [
    source 59
    target 3438
  ]
  edge [
    source 59
    target 3439
  ]
  edge [
    source 59
    target 3440
  ]
  edge [
    source 59
    target 3441
  ]
  edge [
    source 59
    target 3442
  ]
  edge [
    source 59
    target 3146
  ]
  edge [
    source 59
    target 3443
  ]
  edge [
    source 59
    target 3444
  ]
  edge [
    source 59
    target 3182
  ]
  edge [
    source 59
    target 933
  ]
  edge [
    source 59
    target 3139
  ]
  edge [
    source 59
    target 3445
  ]
  edge [
    source 59
    target 1273
  ]
  edge [
    source 59
    target 3446
  ]
  edge [
    source 59
    target 341
  ]
  edge [
    source 59
    target 3447
  ]
  edge [
    source 59
    target 269
  ]
  edge [
    source 59
    target 3448
  ]
  edge [
    source 59
    target 3449
  ]
  edge [
    source 59
    target 3450
  ]
  edge [
    source 59
    target 3451
  ]
  edge [
    source 59
    target 3452
  ]
  edge [
    source 59
    target 1210
  ]
  edge [
    source 59
    target 3453
  ]
  edge [
    source 59
    target 3454
  ]
  edge [
    source 59
    target 3455
  ]
  edge [
    source 59
    target 134
  ]
  edge [
    source 59
    target 3456
  ]
  edge [
    source 59
    target 3457
  ]
  edge [
    source 59
    target 2744
  ]
  edge [
    source 59
    target 425
  ]
  edge [
    source 59
    target 398
  ]
  edge [
    source 59
    target 903
  ]
  edge [
    source 59
    target 234
  ]
  edge [
    source 59
    target 3458
  ]
  edge [
    source 59
    target 3459
  ]
  edge [
    source 59
    target 3460
  ]
  edge [
    source 59
    target 3461
  ]
  edge [
    source 59
    target 2042
  ]
  edge [
    source 59
    target 3462
  ]
  edge [
    source 59
    target 3463
  ]
  edge [
    source 59
    target 3464
  ]
  edge [
    source 59
    target 3465
  ]
  edge [
    source 59
    target 3466
  ]
  edge [
    source 59
    target 3467
  ]
  edge [
    source 59
    target 3468
  ]
  edge [
    source 59
    target 3469
  ]
  edge [
    source 59
    target 1044
  ]
  edge [
    source 59
    target 3470
  ]
  edge [
    source 59
    target 3471
  ]
  edge [
    source 59
    target 3472
  ]
  edge [
    source 59
    target 3473
  ]
  edge [
    source 59
    target 3474
  ]
  edge [
    source 59
    target 3475
  ]
  edge [
    source 59
    target 3476
  ]
  edge [
    source 59
    target 3477
  ]
  edge [
    source 59
    target 3478
  ]
  edge [
    source 59
    target 3479
  ]
  edge [
    source 59
    target 3480
  ]
  edge [
    source 59
    target 3481
  ]
  edge [
    source 59
    target 3482
  ]
  edge [
    source 59
    target 3483
  ]
  edge [
    source 59
    target 3484
  ]
  edge [
    source 59
    target 3485
  ]
  edge [
    source 59
    target 3486
  ]
  edge [
    source 60
    target 3487
  ]
  edge [
    source 60
    target 3488
  ]
  edge [
    source 60
    target 2798
  ]
  edge [
    source 60
    target 3489
  ]
  edge [
    source 60
    target 3490
  ]
  edge [
    source 60
    target 3491
  ]
  edge [
    source 60
    target 3492
  ]
  edge [
    source 60
    target 3493
  ]
  edge [
    source 60
    target 3494
  ]
  edge [
    source 60
    target 3495
  ]
  edge [
    source 60
    target 3496
  ]
  edge [
    source 60
    target 3497
  ]
  edge [
    source 60
    target 3498
  ]
  edge [
    source 60
    target 3499
  ]
  edge [
    source 60
    target 3500
  ]
  edge [
    source 60
    target 3404
  ]
  edge [
    source 60
    target 3501
  ]
  edge [
    source 60
    target 3502
  ]
  edge [
    source 60
    target 3503
  ]
  edge [
    source 60
    target 3504
  ]
  edge [
    source 60
    target 3505
  ]
  edge [
    source 60
    target 3506
  ]
  edge [
    source 60
    target 3507
  ]
  edge [
    source 60
    target 3508
  ]
  edge [
    source 60
    target 3509
  ]
  edge [
    source 60
    target 3510
  ]
  edge [
    source 60
    target 3511
  ]
  edge [
    source 60
    target 3512
  ]
  edge [
    source 60
    target 3513
  ]
  edge [
    source 60
    target 1919
  ]
  edge [
    source 60
    target 1898
  ]
  edge [
    source 60
    target 2801
  ]
  edge [
    source 60
    target 3514
  ]
  edge [
    source 60
    target 3515
  ]
  edge [
    source 60
    target 3516
  ]
  edge [
    source 60
    target 3517
  ]
  edge [
    source 60
    target 3518
  ]
  edge [
    source 60
    target 3519
  ]
  edge [
    source 60
    target 3520
  ]
  edge [
    source 60
    target 3521
  ]
  edge [
    source 60
    target 3522
  ]
  edge [
    source 60
    target 3523
  ]
  edge [
    source 60
    target 3524
  ]
  edge [
    source 60
    target 3525
  ]
  edge [
    source 60
    target 3526
  ]
  edge [
    source 60
    target 3527
  ]
  edge [
    source 60
    target 3528
  ]
  edge [
    source 60
    target 921
  ]
  edge [
    source 60
    target 3529
  ]
  edge [
    source 60
    target 3530
  ]
  edge [
    source 60
    target 3531
  ]
  edge [
    source 60
    target 3532
  ]
  edge [
    source 60
    target 3533
  ]
  edge [
    source 60
    target 1861
  ]
  edge [
    source 60
    target 3534
  ]
  edge [
    source 60
    target 3535
  ]
  edge [
    source 60
    target 3536
  ]
  edge [
    source 60
    target 3537
  ]
  edge [
    source 60
    target 3538
  ]
  edge [
    source 60
    target 3539
  ]
]
