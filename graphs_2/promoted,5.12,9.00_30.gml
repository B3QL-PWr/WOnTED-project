graph [
  node [
    id 0
    label "rodzic"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "sprzeciwia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "obowi&#261;zkowy"
    origin "text"
  ]
  node [
    id 5
    label "szczepienie"
    origin "text"
  ]
  node [
    id 6
    label "ochronny"
    origin "text"
  ]
  node [
    id 7
    label "dziecko"
    origin "text"
  ]
  node [
    id 8
    label "powinny"
    origin "text"
  ]
  node [
    id 9
    label "p&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wysoki"
    origin "text"
  ]
  node [
    id 11
    label "sk&#322;adka"
    origin "text"
  ]
  node [
    id 12
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 13
    label "postulowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "lekarz"
    origin "text"
  ]
  node [
    id 15
    label "prawnik"
    origin "text"
  ]
  node [
    id 16
    label "zrzeszony"
    origin "text"
  ]
  node [
    id 17
    label "polski"
    origin "text"
  ]
  node [
    id 18
    label "towarzystwo"
    origin "text"
  ]
  node [
    id 19
    label "prawa"
    origin "text"
  ]
  node [
    id 20
    label "medyczny"
    origin "text"
  ]
  node [
    id 21
    label "opiekun"
  ]
  node [
    id 22
    label "rodzice"
  ]
  node [
    id 23
    label "wapniak"
  ]
  node [
    id 24
    label "rodzic_chrzestny"
  ]
  node [
    id 25
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 26
    label "starzy"
  ]
  node [
    id 27
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 28
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 29
    label "pokolenie"
  ]
  node [
    id 30
    label "wapniaki"
  ]
  node [
    id 31
    label "nadzorca"
  ]
  node [
    id 32
    label "funkcjonariusz"
  ]
  node [
    id 33
    label "cz&#322;owiek"
  ]
  node [
    id 34
    label "doros&#322;y"
  ]
  node [
    id 35
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 36
    label "jajko"
  ]
  node [
    id 37
    label "konieczny"
  ]
  node [
    id 38
    label "porz&#261;dny"
  ]
  node [
    id 39
    label "kursowy"
  ]
  node [
    id 40
    label "obbligato"
  ]
  node [
    id 41
    label "obligatoryjnie"
  ]
  node [
    id 42
    label "niezb&#281;dnie"
  ]
  node [
    id 43
    label "intensywny"
  ]
  node [
    id 44
    label "przyzwoity"
  ]
  node [
    id 45
    label "ch&#281;dogi"
  ]
  node [
    id 46
    label "schludny"
  ]
  node [
    id 47
    label "porz&#261;dnie"
  ]
  node [
    id 48
    label "prawdziwy"
  ]
  node [
    id 49
    label "dobry"
  ]
  node [
    id 50
    label "obligatoryjny"
  ]
  node [
    id 51
    label "element"
  ]
  node [
    id 52
    label "uprawianie"
  ]
  node [
    id 53
    label "uodpornianie"
  ]
  node [
    id 54
    label "metoda"
  ]
  node [
    id 55
    label "zabieg"
  ]
  node [
    id 56
    label "wariolacja"
  ]
  node [
    id 57
    label "immunizacja"
  ]
  node [
    id 58
    label "uszlachetnianie"
  ]
  node [
    id 59
    label "inoculation"
  ]
  node [
    id 60
    label "immunizacja_czynna"
  ]
  node [
    id 61
    label "czyn"
  ]
  node [
    id 62
    label "leczenie"
  ]
  node [
    id 63
    label "operation"
  ]
  node [
    id 64
    label "uodparnianie_si&#281;"
  ]
  node [
    id 65
    label "powodowanie"
  ]
  node [
    id 66
    label "wzmacnianie"
  ]
  node [
    id 67
    label "odporny"
  ]
  node [
    id 68
    label "wytrzyma&#322;y"
  ]
  node [
    id 69
    label "immunization"
  ]
  node [
    id 70
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 71
    label "method"
  ]
  node [
    id 72
    label "spos&#243;b"
  ]
  node [
    id 73
    label "doktryna"
  ]
  node [
    id 74
    label "uwznio&#347;lanie"
  ]
  node [
    id 75
    label "ulepszanie"
  ]
  node [
    id 76
    label "polish"
  ]
  node [
    id 77
    label "surowica"
  ]
  node [
    id 78
    label "odporno&#347;&#263;"
  ]
  node [
    id 79
    label "pielenie"
  ]
  node [
    id 80
    label "culture"
  ]
  node [
    id 81
    label "sianie"
  ]
  node [
    id 82
    label "zbi&#243;r"
  ]
  node [
    id 83
    label "stanowisko"
  ]
  node [
    id 84
    label "sadzenie"
  ]
  node [
    id 85
    label "oprysk"
  ]
  node [
    id 86
    label "orka"
  ]
  node [
    id 87
    label "rolnictwo"
  ]
  node [
    id 88
    label "siew"
  ]
  node [
    id 89
    label "exercise"
  ]
  node [
    id 90
    label "koszenie"
  ]
  node [
    id 91
    label "obrabianie"
  ]
  node [
    id 92
    label "czynno&#347;&#263;"
  ]
  node [
    id 93
    label "zajmowanie_si&#281;"
  ]
  node [
    id 94
    label "use"
  ]
  node [
    id 95
    label "biotechnika"
  ]
  node [
    id 96
    label "hodowanie"
  ]
  node [
    id 97
    label "ochronnie"
  ]
  node [
    id 98
    label "ulgowo"
  ]
  node [
    id 99
    label "utulenie"
  ]
  node [
    id 100
    label "pediatra"
  ]
  node [
    id 101
    label "dzieciak"
  ]
  node [
    id 102
    label "utulanie"
  ]
  node [
    id 103
    label "dzieciarnia"
  ]
  node [
    id 104
    label "niepe&#322;noletni"
  ]
  node [
    id 105
    label "organizm"
  ]
  node [
    id 106
    label "utula&#263;"
  ]
  node [
    id 107
    label "cz&#322;owieczek"
  ]
  node [
    id 108
    label "fledgling"
  ]
  node [
    id 109
    label "zwierz&#281;"
  ]
  node [
    id 110
    label "utuli&#263;"
  ]
  node [
    id 111
    label "m&#322;odzik"
  ]
  node [
    id 112
    label "pedofil"
  ]
  node [
    id 113
    label "m&#322;odziak"
  ]
  node [
    id 114
    label "potomek"
  ]
  node [
    id 115
    label "entliczek-pentliczek"
  ]
  node [
    id 116
    label "potomstwo"
  ]
  node [
    id 117
    label "sraluch"
  ]
  node [
    id 118
    label "czeladka"
  ]
  node [
    id 119
    label "dzietno&#347;&#263;"
  ]
  node [
    id 120
    label "bawienie_si&#281;"
  ]
  node [
    id 121
    label "pomiot"
  ]
  node [
    id 122
    label "grupa"
  ]
  node [
    id 123
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 124
    label "kinderbal"
  ]
  node [
    id 125
    label "krewny"
  ]
  node [
    id 126
    label "ludzko&#347;&#263;"
  ]
  node [
    id 127
    label "asymilowanie"
  ]
  node [
    id 128
    label "asymilowa&#263;"
  ]
  node [
    id 129
    label "os&#322;abia&#263;"
  ]
  node [
    id 130
    label "posta&#263;"
  ]
  node [
    id 131
    label "hominid"
  ]
  node [
    id 132
    label "podw&#322;adny"
  ]
  node [
    id 133
    label "os&#322;abianie"
  ]
  node [
    id 134
    label "g&#322;owa"
  ]
  node [
    id 135
    label "figura"
  ]
  node [
    id 136
    label "portrecista"
  ]
  node [
    id 137
    label "dwun&#243;g"
  ]
  node [
    id 138
    label "profanum"
  ]
  node [
    id 139
    label "mikrokosmos"
  ]
  node [
    id 140
    label "nasada"
  ]
  node [
    id 141
    label "duch"
  ]
  node [
    id 142
    label "antropochoria"
  ]
  node [
    id 143
    label "osoba"
  ]
  node [
    id 144
    label "wz&#243;r"
  ]
  node [
    id 145
    label "senior"
  ]
  node [
    id 146
    label "oddzia&#322;ywanie"
  ]
  node [
    id 147
    label "Adam"
  ]
  node [
    id 148
    label "homo_sapiens"
  ]
  node [
    id 149
    label "polifag"
  ]
  node [
    id 150
    label "ma&#322;oletny"
  ]
  node [
    id 151
    label "m&#322;ody"
  ]
  node [
    id 152
    label "degenerat"
  ]
  node [
    id 153
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 154
    label "zwyrol"
  ]
  node [
    id 155
    label "czerniak"
  ]
  node [
    id 156
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 157
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 158
    label "paszcza"
  ]
  node [
    id 159
    label "popapraniec"
  ]
  node [
    id 160
    label "skuba&#263;"
  ]
  node [
    id 161
    label "skubanie"
  ]
  node [
    id 162
    label "skubni&#281;cie"
  ]
  node [
    id 163
    label "agresja"
  ]
  node [
    id 164
    label "zwierz&#281;ta"
  ]
  node [
    id 165
    label "fukni&#281;cie"
  ]
  node [
    id 166
    label "farba"
  ]
  node [
    id 167
    label "fukanie"
  ]
  node [
    id 168
    label "istota_&#380;ywa"
  ]
  node [
    id 169
    label "gad"
  ]
  node [
    id 170
    label "siedzie&#263;"
  ]
  node [
    id 171
    label "oswaja&#263;"
  ]
  node [
    id 172
    label "tresowa&#263;"
  ]
  node [
    id 173
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 174
    label "poligamia"
  ]
  node [
    id 175
    label "oz&#243;r"
  ]
  node [
    id 176
    label "skubn&#261;&#263;"
  ]
  node [
    id 177
    label "wios&#322;owa&#263;"
  ]
  node [
    id 178
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 179
    label "le&#380;enie"
  ]
  node [
    id 180
    label "niecz&#322;owiek"
  ]
  node [
    id 181
    label "wios&#322;owanie"
  ]
  node [
    id 182
    label "napasienie_si&#281;"
  ]
  node [
    id 183
    label "wiwarium"
  ]
  node [
    id 184
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 185
    label "animalista"
  ]
  node [
    id 186
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 187
    label "budowa"
  ]
  node [
    id 188
    label "hodowla"
  ]
  node [
    id 189
    label "pasienie_si&#281;"
  ]
  node [
    id 190
    label "sodomita"
  ]
  node [
    id 191
    label "monogamia"
  ]
  node [
    id 192
    label "przyssawka"
  ]
  node [
    id 193
    label "zachowanie"
  ]
  node [
    id 194
    label "budowa_cia&#322;a"
  ]
  node [
    id 195
    label "okrutnik"
  ]
  node [
    id 196
    label "grzbiet"
  ]
  node [
    id 197
    label "weterynarz"
  ]
  node [
    id 198
    label "&#322;eb"
  ]
  node [
    id 199
    label "wylinka"
  ]
  node [
    id 200
    label "bestia"
  ]
  node [
    id 201
    label "poskramia&#263;"
  ]
  node [
    id 202
    label "fauna"
  ]
  node [
    id 203
    label "treser"
  ]
  node [
    id 204
    label "siedzenie"
  ]
  node [
    id 205
    label "le&#380;e&#263;"
  ]
  node [
    id 206
    label "p&#322;aszczyzna"
  ]
  node [
    id 207
    label "odwadnia&#263;"
  ]
  node [
    id 208
    label "przyswoi&#263;"
  ]
  node [
    id 209
    label "sk&#243;ra"
  ]
  node [
    id 210
    label "odwodni&#263;"
  ]
  node [
    id 211
    label "ewoluowanie"
  ]
  node [
    id 212
    label "staw"
  ]
  node [
    id 213
    label "ow&#322;osienie"
  ]
  node [
    id 214
    label "unerwienie"
  ]
  node [
    id 215
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 216
    label "reakcja"
  ]
  node [
    id 217
    label "wyewoluowanie"
  ]
  node [
    id 218
    label "przyswajanie"
  ]
  node [
    id 219
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 220
    label "wyewoluowa&#263;"
  ]
  node [
    id 221
    label "miejsce"
  ]
  node [
    id 222
    label "biorytm"
  ]
  node [
    id 223
    label "ewoluowa&#263;"
  ]
  node [
    id 224
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 225
    label "otworzy&#263;"
  ]
  node [
    id 226
    label "otwiera&#263;"
  ]
  node [
    id 227
    label "czynnik_biotyczny"
  ]
  node [
    id 228
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 229
    label "otworzenie"
  ]
  node [
    id 230
    label "otwieranie"
  ]
  node [
    id 231
    label "individual"
  ]
  node [
    id 232
    label "szkielet"
  ]
  node [
    id 233
    label "ty&#322;"
  ]
  node [
    id 234
    label "obiekt"
  ]
  node [
    id 235
    label "przyswaja&#263;"
  ]
  node [
    id 236
    label "przyswojenie"
  ]
  node [
    id 237
    label "odwadnianie"
  ]
  node [
    id 238
    label "odwodnienie"
  ]
  node [
    id 239
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 240
    label "starzenie_si&#281;"
  ]
  node [
    id 241
    label "prz&#243;d"
  ]
  node [
    id 242
    label "uk&#322;ad"
  ]
  node [
    id 243
    label "temperatura"
  ]
  node [
    id 244
    label "l&#281;d&#378;wie"
  ]
  node [
    id 245
    label "cia&#322;o"
  ]
  node [
    id 246
    label "cz&#322;onek"
  ]
  node [
    id 247
    label "utulanie_si&#281;"
  ]
  node [
    id 248
    label "usypianie"
  ]
  node [
    id 249
    label "pocieszanie"
  ]
  node [
    id 250
    label "uspokajanie"
  ]
  node [
    id 251
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 252
    label "uspokoi&#263;"
  ]
  node [
    id 253
    label "uspokojenie"
  ]
  node [
    id 254
    label "utulenie_si&#281;"
  ]
  node [
    id 255
    label "u&#347;pienie"
  ]
  node [
    id 256
    label "usypia&#263;"
  ]
  node [
    id 257
    label "uspokaja&#263;"
  ]
  node [
    id 258
    label "dewiant"
  ]
  node [
    id 259
    label "specjalista"
  ]
  node [
    id 260
    label "wyliczanka"
  ]
  node [
    id 261
    label "harcerz"
  ]
  node [
    id 262
    label "ch&#322;opta&#347;"
  ]
  node [
    id 263
    label "zawodnik"
  ]
  node [
    id 264
    label "go&#322;ow&#261;s"
  ]
  node [
    id 265
    label "m&#322;ode"
  ]
  node [
    id 266
    label "stopie&#324;_harcerski"
  ]
  node [
    id 267
    label "g&#243;wniarz"
  ]
  node [
    id 268
    label "beniaminek"
  ]
  node [
    id 269
    label "istotka"
  ]
  node [
    id 270
    label "bech"
  ]
  node [
    id 271
    label "dziecinny"
  ]
  node [
    id 272
    label "naiwniak"
  ]
  node [
    id 273
    label "nale&#380;ny"
  ]
  node [
    id 274
    label "nale&#380;nie"
  ]
  node [
    id 275
    label "nale&#380;yty"
  ]
  node [
    id 276
    label "godny"
  ]
  node [
    id 277
    label "przynale&#380;ny"
  ]
  node [
    id 278
    label "give"
  ]
  node [
    id 279
    label "wydawa&#263;"
  ]
  node [
    id 280
    label "pay"
  ]
  node [
    id 281
    label "osi&#261;ga&#263;"
  ]
  node [
    id 282
    label "buli&#263;"
  ]
  node [
    id 283
    label "robi&#263;"
  ]
  node [
    id 284
    label "mie&#263;_miejsce"
  ]
  node [
    id 285
    label "plon"
  ]
  node [
    id 286
    label "surrender"
  ]
  node [
    id 287
    label "kojarzy&#263;"
  ]
  node [
    id 288
    label "d&#378;wi&#281;k"
  ]
  node [
    id 289
    label "impart"
  ]
  node [
    id 290
    label "dawa&#263;"
  ]
  node [
    id 291
    label "reszta"
  ]
  node [
    id 292
    label "zapach"
  ]
  node [
    id 293
    label "wydawnictwo"
  ]
  node [
    id 294
    label "wiano"
  ]
  node [
    id 295
    label "produkcja"
  ]
  node [
    id 296
    label "wprowadza&#263;"
  ]
  node [
    id 297
    label "podawa&#263;"
  ]
  node [
    id 298
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 299
    label "ujawnia&#263;"
  ]
  node [
    id 300
    label "placard"
  ]
  node [
    id 301
    label "powierza&#263;"
  ]
  node [
    id 302
    label "denuncjowa&#263;"
  ]
  node [
    id 303
    label "tajemnica"
  ]
  node [
    id 304
    label "panna_na_wydaniu"
  ]
  node [
    id 305
    label "wytwarza&#263;"
  ]
  node [
    id 306
    label "train"
  ]
  node [
    id 307
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 308
    label "uzyskiwa&#263;"
  ]
  node [
    id 309
    label "dociera&#263;"
  ]
  node [
    id 310
    label "mark"
  ]
  node [
    id 311
    label "get"
  ]
  node [
    id 312
    label "wyrafinowany"
  ]
  node [
    id 313
    label "niepo&#347;ledni"
  ]
  node [
    id 314
    label "du&#380;y"
  ]
  node [
    id 315
    label "chwalebny"
  ]
  node [
    id 316
    label "z_wysoka"
  ]
  node [
    id 317
    label "wznios&#322;y"
  ]
  node [
    id 318
    label "daleki"
  ]
  node [
    id 319
    label "wysoce"
  ]
  node [
    id 320
    label "szczytnie"
  ]
  node [
    id 321
    label "znaczny"
  ]
  node [
    id 322
    label "warto&#347;ciowy"
  ]
  node [
    id 323
    label "wysoko"
  ]
  node [
    id 324
    label "uprzywilejowany"
  ]
  node [
    id 325
    label "niema&#322;o"
  ]
  node [
    id 326
    label "wiele"
  ]
  node [
    id 327
    label "rozwini&#281;ty"
  ]
  node [
    id 328
    label "dorodny"
  ]
  node [
    id 329
    label "wa&#380;ny"
  ]
  node [
    id 330
    label "du&#380;o"
  ]
  node [
    id 331
    label "szczeg&#243;lny"
  ]
  node [
    id 332
    label "lekki"
  ]
  node [
    id 333
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 334
    label "znacznie"
  ]
  node [
    id 335
    label "zauwa&#380;alny"
  ]
  node [
    id 336
    label "niez&#322;y"
  ]
  node [
    id 337
    label "niepo&#347;lednio"
  ]
  node [
    id 338
    label "wyj&#261;tkowy"
  ]
  node [
    id 339
    label "pochwalny"
  ]
  node [
    id 340
    label "wspania&#322;y"
  ]
  node [
    id 341
    label "szlachetny"
  ]
  node [
    id 342
    label "powa&#380;ny"
  ]
  node [
    id 343
    label "chwalebnie"
  ]
  node [
    id 344
    label "podnios&#322;y"
  ]
  node [
    id 345
    label "wznio&#347;le"
  ]
  node [
    id 346
    label "oderwany"
  ]
  node [
    id 347
    label "pi&#281;kny"
  ]
  node [
    id 348
    label "rewaluowanie"
  ]
  node [
    id 349
    label "warto&#347;ciowo"
  ]
  node [
    id 350
    label "drogi"
  ]
  node [
    id 351
    label "u&#380;yteczny"
  ]
  node [
    id 352
    label "zrewaluowanie"
  ]
  node [
    id 353
    label "obyty"
  ]
  node [
    id 354
    label "wykwintny"
  ]
  node [
    id 355
    label "wyrafinowanie"
  ]
  node [
    id 356
    label "wymy&#347;lny"
  ]
  node [
    id 357
    label "dawny"
  ]
  node [
    id 358
    label "ogl&#281;dny"
  ]
  node [
    id 359
    label "d&#322;ugi"
  ]
  node [
    id 360
    label "daleko"
  ]
  node [
    id 361
    label "odleg&#322;y"
  ]
  node [
    id 362
    label "zwi&#261;zany"
  ]
  node [
    id 363
    label "r&#243;&#380;ny"
  ]
  node [
    id 364
    label "s&#322;aby"
  ]
  node [
    id 365
    label "odlegle"
  ]
  node [
    id 366
    label "oddalony"
  ]
  node [
    id 367
    label "g&#322;&#281;boki"
  ]
  node [
    id 368
    label "obcy"
  ]
  node [
    id 369
    label "nieobecny"
  ]
  node [
    id 370
    label "przysz&#322;y"
  ]
  node [
    id 371
    label "g&#243;rno"
  ]
  node [
    id 372
    label "szczytny"
  ]
  node [
    id 373
    label "intensywnie"
  ]
  node [
    id 374
    label "wielki"
  ]
  node [
    id 375
    label "niezmiernie"
  ]
  node [
    id 376
    label "&#243;semka"
  ]
  node [
    id 377
    label "czw&#243;rka"
  ]
  node [
    id 378
    label "sk&#322;ada&#263;_si&#281;"
  ]
  node [
    id 379
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 380
    label "zbi&#243;rka"
  ]
  node [
    id 381
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 382
    label "arkusz"
  ]
  node [
    id 383
    label "fee"
  ]
  node [
    id 384
    label "kwota"
  ]
  node [
    id 385
    label "uregulowa&#263;"
  ]
  node [
    id 386
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 387
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 388
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 389
    label "p&#322;at"
  ]
  node [
    id 390
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 391
    label "kwestarz"
  ]
  node [
    id 392
    label "kwestowanie"
  ]
  node [
    id 393
    label "apel"
  ]
  node [
    id 394
    label "recoil"
  ]
  node [
    id 395
    label "collection"
  ]
  node [
    id 396
    label "spotkanie"
  ]
  node [
    id 397
    label "koszyk&#243;wka"
  ]
  node [
    id 398
    label "chwyt"
  ]
  node [
    id 399
    label "toto-lotek"
  ]
  node [
    id 400
    label "trafienie"
  ]
  node [
    id 401
    label "arkusz_drukarski"
  ]
  node [
    id 402
    label "&#322;&#243;dka"
  ]
  node [
    id 403
    label "four"
  ]
  node [
    id 404
    label "&#263;wiartka"
  ]
  node [
    id 405
    label "hotel"
  ]
  node [
    id 406
    label "cyfra"
  ]
  node [
    id 407
    label "pok&#243;j"
  ]
  node [
    id 408
    label "stopie&#324;"
  ]
  node [
    id 409
    label "minialbum"
  ]
  node [
    id 410
    label "osada"
  ]
  node [
    id 411
    label "p&#322;yta_winylowa"
  ]
  node [
    id 412
    label "blotka"
  ]
  node [
    id 413
    label "zaprz&#281;g"
  ]
  node [
    id 414
    label "przedtrzonowiec"
  ]
  node [
    id 415
    label "przyrz&#261;d_asekuracyjny"
  ]
  node [
    id 416
    label "&#322;&#243;d&#378;_regatowa"
  ]
  node [
    id 417
    label "nuta"
  ]
  node [
    id 418
    label "kszta&#322;t"
  ]
  node [
    id 419
    label "trzonowiec"
  ]
  node [
    id 420
    label "przegub"
  ]
  node [
    id 421
    label "w&#281;ze&#322;"
  ]
  node [
    id 422
    label "&#243;sma_cz&#281;&#347;&#263;"
  ]
  node [
    id 423
    label "eight"
  ]
  node [
    id 424
    label "eighth"
  ]
  node [
    id 425
    label "cepy"
  ]
  node [
    id 426
    label "bilard"
  ]
  node [
    id 427
    label "kapica"
  ]
  node [
    id 428
    label "format_arkusza"
  ]
  node [
    id 429
    label "wisdom_tooth"
  ]
  node [
    id 430
    label "&#322;&#243;d&#378;_wios&#322;owa"
  ]
  node [
    id 431
    label "zdrowy"
  ]
  node [
    id 432
    label "zdrowotnie"
  ]
  node [
    id 433
    label "prozdrowotny"
  ]
  node [
    id 434
    label "dobrze"
  ]
  node [
    id 435
    label "zdrowo"
  ]
  node [
    id 436
    label "wyzdrowienie"
  ]
  node [
    id 437
    label "wyleczenie_si&#281;"
  ]
  node [
    id 438
    label "uzdrowienie"
  ]
  node [
    id 439
    label "silny"
  ]
  node [
    id 440
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 441
    label "normalny"
  ]
  node [
    id 442
    label "rozs&#261;dny"
  ]
  node [
    id 443
    label "korzystny"
  ]
  node [
    id 444
    label "zdrowienie"
  ]
  node [
    id 445
    label "solidny"
  ]
  node [
    id 446
    label "uzdrawianie"
  ]
  node [
    id 447
    label "dobroczynny"
  ]
  node [
    id 448
    label "spokojny"
  ]
  node [
    id 449
    label "skuteczny"
  ]
  node [
    id 450
    label "&#347;mieszny"
  ]
  node [
    id 451
    label "mi&#322;y"
  ]
  node [
    id 452
    label "grzeczny"
  ]
  node [
    id 453
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 454
    label "powitanie"
  ]
  node [
    id 455
    label "ca&#322;y"
  ]
  node [
    id 456
    label "zwrot"
  ]
  node [
    id 457
    label "pomy&#347;lny"
  ]
  node [
    id 458
    label "moralny"
  ]
  node [
    id 459
    label "pozytywny"
  ]
  node [
    id 460
    label "odpowiedni"
  ]
  node [
    id 461
    label "pos&#322;uszny"
  ]
  node [
    id 462
    label "postulate"
  ]
  node [
    id 463
    label "prosi&#263;"
  ]
  node [
    id 464
    label "invite"
  ]
  node [
    id 465
    label "poleca&#263;"
  ]
  node [
    id 466
    label "trwa&#263;"
  ]
  node [
    id 467
    label "zaprasza&#263;"
  ]
  node [
    id 468
    label "zach&#281;ca&#263;"
  ]
  node [
    id 469
    label "suffice"
  ]
  node [
    id 470
    label "preach"
  ]
  node [
    id 471
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 472
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 473
    label "pies"
  ]
  node [
    id 474
    label "zezwala&#263;"
  ]
  node [
    id 475
    label "ask"
  ]
  node [
    id 476
    label "Mesmer"
  ]
  node [
    id 477
    label "pracownik"
  ]
  node [
    id 478
    label "Galen"
  ]
  node [
    id 479
    label "zbada&#263;"
  ]
  node [
    id 480
    label "medyk"
  ]
  node [
    id 481
    label "eskulap"
  ]
  node [
    id 482
    label "lekarze"
  ]
  node [
    id 483
    label "Hipokrates"
  ]
  node [
    id 484
    label "dokt&#243;r"
  ]
  node [
    id 485
    label "&#347;rodowisko"
  ]
  node [
    id 486
    label "student"
  ]
  node [
    id 487
    label "praktyk"
  ]
  node [
    id 488
    label "salariat"
  ]
  node [
    id 489
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 490
    label "delegowanie"
  ]
  node [
    id 491
    label "pracu&#347;"
  ]
  node [
    id 492
    label "r&#281;ka"
  ]
  node [
    id 493
    label "delegowa&#263;"
  ]
  node [
    id 494
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 495
    label "Aesculapius"
  ]
  node [
    id 496
    label "sprawdzi&#263;"
  ]
  node [
    id 497
    label "pozna&#263;"
  ]
  node [
    id 498
    label "zdecydowa&#263;"
  ]
  node [
    id 499
    label "zrobi&#263;"
  ]
  node [
    id 500
    label "wybada&#263;"
  ]
  node [
    id 501
    label "examine"
  ]
  node [
    id 502
    label "prawnicy"
  ]
  node [
    id 503
    label "Machiavelli"
  ]
  node [
    id 504
    label "jurysta"
  ]
  node [
    id 505
    label "aplikant"
  ]
  node [
    id 506
    label "indeks"
  ]
  node [
    id 507
    label "s&#322;uchacz"
  ]
  node [
    id 508
    label "immatrykulowanie"
  ]
  node [
    id 509
    label "absolwent"
  ]
  node [
    id 510
    label "immatrykulowa&#263;"
  ]
  node [
    id 511
    label "akademik"
  ]
  node [
    id 512
    label "tutor"
  ]
  node [
    id 513
    label "znawca"
  ]
  node [
    id 514
    label "wyspecjalizowanie_si&#281;"
  ]
  node [
    id 515
    label "spec"
  ]
  node [
    id 516
    label "renesans"
  ]
  node [
    id 517
    label "sta&#380;ysta"
  ]
  node [
    id 518
    label "stowarzyszenie"
  ]
  node [
    id 519
    label "podmiot"
  ]
  node [
    id 520
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 521
    label "organ"
  ]
  node [
    id 522
    label "ptaszek"
  ]
  node [
    id 523
    label "organizacja"
  ]
  node [
    id 524
    label "element_anatomiczny"
  ]
  node [
    id 525
    label "przyrodzenie"
  ]
  node [
    id 526
    label "fiut"
  ]
  node [
    id 527
    label "shaft"
  ]
  node [
    id 528
    label "wchodzenie"
  ]
  node [
    id 529
    label "przedstawiciel"
  ]
  node [
    id 530
    label "wej&#347;cie"
  ]
  node [
    id 531
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 532
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 533
    label "Chewra_Kadisza"
  ]
  node [
    id 534
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 535
    label "fabianie"
  ]
  node [
    id 536
    label "Rotary_International"
  ]
  node [
    id 537
    label "Eleusis"
  ]
  node [
    id 538
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 539
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 540
    label "Monar"
  ]
  node [
    id 541
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 542
    label "przedmiot"
  ]
  node [
    id 543
    label "Polish"
  ]
  node [
    id 544
    label "goniony"
  ]
  node [
    id 545
    label "oberek"
  ]
  node [
    id 546
    label "ryba_po_grecku"
  ]
  node [
    id 547
    label "sztajer"
  ]
  node [
    id 548
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 549
    label "krakowiak"
  ]
  node [
    id 550
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 551
    label "pierogi_ruskie"
  ]
  node [
    id 552
    label "lacki"
  ]
  node [
    id 553
    label "polak"
  ]
  node [
    id 554
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 555
    label "chodzony"
  ]
  node [
    id 556
    label "po_polsku"
  ]
  node [
    id 557
    label "mazur"
  ]
  node [
    id 558
    label "polsko"
  ]
  node [
    id 559
    label "skoczny"
  ]
  node [
    id 560
    label "drabant"
  ]
  node [
    id 561
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 562
    label "j&#281;zyk"
  ]
  node [
    id 563
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 564
    label "artykulator"
  ]
  node [
    id 565
    label "kod"
  ]
  node [
    id 566
    label "kawa&#322;ek"
  ]
  node [
    id 567
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 568
    label "gramatyka"
  ]
  node [
    id 569
    label "stylik"
  ]
  node [
    id 570
    label "przet&#322;umaczenie"
  ]
  node [
    id 571
    label "formalizowanie"
  ]
  node [
    id 572
    label "ssa&#263;"
  ]
  node [
    id 573
    label "ssanie"
  ]
  node [
    id 574
    label "language"
  ]
  node [
    id 575
    label "liza&#263;"
  ]
  node [
    id 576
    label "napisa&#263;"
  ]
  node [
    id 577
    label "konsonantyzm"
  ]
  node [
    id 578
    label "wokalizm"
  ]
  node [
    id 579
    label "pisa&#263;"
  ]
  node [
    id 580
    label "fonetyka"
  ]
  node [
    id 581
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 582
    label "jeniec"
  ]
  node [
    id 583
    label "but"
  ]
  node [
    id 584
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 585
    label "po_koroniarsku"
  ]
  node [
    id 586
    label "kultura_duchowa"
  ]
  node [
    id 587
    label "t&#322;umaczenie"
  ]
  node [
    id 588
    label "m&#243;wienie"
  ]
  node [
    id 589
    label "pype&#263;"
  ]
  node [
    id 590
    label "lizanie"
  ]
  node [
    id 591
    label "pismo"
  ]
  node [
    id 592
    label "formalizowa&#263;"
  ]
  node [
    id 593
    label "rozumie&#263;"
  ]
  node [
    id 594
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 595
    label "rozumienie"
  ]
  node [
    id 596
    label "makroglosja"
  ]
  node [
    id 597
    label "m&#243;wi&#263;"
  ]
  node [
    id 598
    label "jama_ustna"
  ]
  node [
    id 599
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 600
    label "formacja_geologiczna"
  ]
  node [
    id 601
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 602
    label "natural_language"
  ]
  node [
    id 603
    label "s&#322;ownictwo"
  ]
  node [
    id 604
    label "urz&#261;dzenie"
  ]
  node [
    id 605
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 606
    label "wschodnioeuropejski"
  ]
  node [
    id 607
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 608
    label "poga&#324;ski"
  ]
  node [
    id 609
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 610
    label "topielec"
  ]
  node [
    id 611
    label "europejski"
  ]
  node [
    id 612
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 613
    label "langosz"
  ]
  node [
    id 614
    label "zboczenie"
  ]
  node [
    id 615
    label "om&#243;wienie"
  ]
  node [
    id 616
    label "sponiewieranie"
  ]
  node [
    id 617
    label "discipline"
  ]
  node [
    id 618
    label "rzecz"
  ]
  node [
    id 619
    label "omawia&#263;"
  ]
  node [
    id 620
    label "kr&#261;&#380;enie"
  ]
  node [
    id 621
    label "tre&#347;&#263;"
  ]
  node [
    id 622
    label "robienie"
  ]
  node [
    id 623
    label "sponiewiera&#263;"
  ]
  node [
    id 624
    label "entity"
  ]
  node [
    id 625
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 626
    label "tematyka"
  ]
  node [
    id 627
    label "w&#261;tek"
  ]
  node [
    id 628
    label "charakter"
  ]
  node [
    id 629
    label "zbaczanie"
  ]
  node [
    id 630
    label "program_nauczania"
  ]
  node [
    id 631
    label "om&#243;wi&#263;"
  ]
  node [
    id 632
    label "omawianie"
  ]
  node [
    id 633
    label "thing"
  ]
  node [
    id 634
    label "kultura"
  ]
  node [
    id 635
    label "istota"
  ]
  node [
    id 636
    label "zbacza&#263;"
  ]
  node [
    id 637
    label "zboczy&#263;"
  ]
  node [
    id 638
    label "gwardzista"
  ]
  node [
    id 639
    label "melodia"
  ]
  node [
    id 640
    label "taniec"
  ]
  node [
    id 641
    label "taniec_ludowy"
  ]
  node [
    id 642
    label "&#347;redniowieczny"
  ]
  node [
    id 643
    label "europejsko"
  ]
  node [
    id 644
    label "specjalny"
  ]
  node [
    id 645
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 646
    label "weso&#322;y"
  ]
  node [
    id 647
    label "sprawny"
  ]
  node [
    id 648
    label "rytmiczny"
  ]
  node [
    id 649
    label "skocznie"
  ]
  node [
    id 650
    label "energiczny"
  ]
  node [
    id 651
    label "przytup"
  ]
  node [
    id 652
    label "ho&#322;ubiec"
  ]
  node [
    id 653
    label "wodzi&#263;"
  ]
  node [
    id 654
    label "lendler"
  ]
  node [
    id 655
    label "austriacki"
  ]
  node [
    id 656
    label "polka"
  ]
  node [
    id 657
    label "ludowy"
  ]
  node [
    id 658
    label "pie&#347;&#324;"
  ]
  node [
    id 659
    label "mieszkaniec"
  ]
  node [
    id 660
    label "centu&#347;"
  ]
  node [
    id 661
    label "lalka"
  ]
  node [
    id 662
    label "Ma&#322;opolanin"
  ]
  node [
    id 663
    label "krakauer"
  ]
  node [
    id 664
    label "partnership"
  ]
  node [
    id 665
    label "asystencja"
  ]
  node [
    id 666
    label "wi&#281;&#378;"
  ]
  node [
    id 667
    label "obecno&#347;&#263;"
  ]
  node [
    id 668
    label "grono"
  ]
  node [
    id 669
    label "zwi&#261;zanie"
  ]
  node [
    id 670
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 671
    label "wi&#261;zanie"
  ]
  node [
    id 672
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 673
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 674
    label "bratnia_dusza"
  ]
  node [
    id 675
    label "marriage"
  ]
  node [
    id 676
    label "zwi&#261;zek"
  ]
  node [
    id 677
    label "zwi&#261;za&#263;"
  ]
  node [
    id 678
    label "marketing_afiliacyjny"
  ]
  node [
    id 679
    label "ki&#347;&#263;"
  ]
  node [
    id 680
    label "mirycetyna"
  ]
  node [
    id 681
    label "owoc"
  ]
  node [
    id 682
    label "jagoda"
  ]
  node [
    id 683
    label "stan"
  ]
  node [
    id 684
    label "being"
  ]
  node [
    id 685
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 686
    label "cecha"
  ]
  node [
    id 687
    label "odm&#322;adzanie"
  ]
  node [
    id 688
    label "liga"
  ]
  node [
    id 689
    label "jednostka_systematyczna"
  ]
  node [
    id 690
    label "gromada"
  ]
  node [
    id 691
    label "egzemplarz"
  ]
  node [
    id 692
    label "Entuzjastki"
  ]
  node [
    id 693
    label "kompozycja"
  ]
  node [
    id 694
    label "Terranie"
  ]
  node [
    id 695
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 696
    label "category"
  ]
  node [
    id 697
    label "pakiet_klimatyczny"
  ]
  node [
    id 698
    label "oddzia&#322;"
  ]
  node [
    id 699
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 700
    label "cz&#261;steczka"
  ]
  node [
    id 701
    label "stage_set"
  ]
  node [
    id 702
    label "type"
  ]
  node [
    id 703
    label "specgrupa"
  ]
  node [
    id 704
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 705
    label "&#346;wietliki"
  ]
  node [
    id 706
    label "odm&#322;odzenie"
  ]
  node [
    id 707
    label "Eurogrupa"
  ]
  node [
    id 708
    label "odm&#322;adza&#263;"
  ]
  node [
    id 709
    label "harcerze_starsi"
  ]
  node [
    id 710
    label "jednostka_organizacyjna"
  ]
  node [
    id 711
    label "struktura"
  ]
  node [
    id 712
    label "TOPR"
  ]
  node [
    id 713
    label "endecki"
  ]
  node [
    id 714
    label "zesp&#243;&#322;"
  ]
  node [
    id 715
    label "przedstawicielstwo"
  ]
  node [
    id 716
    label "od&#322;am"
  ]
  node [
    id 717
    label "Cepelia"
  ]
  node [
    id 718
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 719
    label "ZBoWiD"
  ]
  node [
    id 720
    label "organization"
  ]
  node [
    id 721
    label "centrala"
  ]
  node [
    id 722
    label "GOPR"
  ]
  node [
    id 723
    label "ZOMO"
  ]
  node [
    id 724
    label "ZMP"
  ]
  node [
    id 725
    label "komitet_koordynacyjny"
  ]
  node [
    id 726
    label "przybud&#243;wka"
  ]
  node [
    id 727
    label "boj&#243;wka"
  ]
  node [
    id 728
    label "wym&#243;g"
  ]
  node [
    id 729
    label "asysta"
  ]
  node [
    id 730
    label "harcerstwo"
  ]
  node [
    id 731
    label "G&#322;osk&#243;w"
  ]
  node [
    id 732
    label "reedukator"
  ]
  node [
    id 733
    label "leczniczy"
  ]
  node [
    id 734
    label "lekarsko"
  ]
  node [
    id 735
    label "medycznie"
  ]
  node [
    id 736
    label "paramedyczny"
  ]
  node [
    id 737
    label "profilowy"
  ]
  node [
    id 738
    label "bia&#322;y"
  ]
  node [
    id 739
    label "praktyczny"
  ]
  node [
    id 740
    label "specjalistyczny"
  ]
  node [
    id 741
    label "zgodny"
  ]
  node [
    id 742
    label "intencjonalny"
  ]
  node [
    id 743
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 744
    label "niedorozw&#243;j"
  ]
  node [
    id 745
    label "specjalnie"
  ]
  node [
    id 746
    label "nieetatowy"
  ]
  node [
    id 747
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 748
    label "nienormalny"
  ]
  node [
    id 749
    label "umy&#347;lnie"
  ]
  node [
    id 750
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 751
    label "leczniczo"
  ]
  node [
    id 752
    label "specjalistycznie"
  ]
  node [
    id 753
    label "fachowo"
  ]
  node [
    id 754
    label "fachowy"
  ]
  node [
    id 755
    label "zgodnie"
  ]
  node [
    id 756
    label "zbie&#380;ny"
  ]
  node [
    id 757
    label "racjonalny"
  ]
  node [
    id 758
    label "praktycznie"
  ]
  node [
    id 759
    label "oficjalnie"
  ]
  node [
    id 760
    label "lekarski"
  ]
  node [
    id 761
    label "carat"
  ]
  node [
    id 762
    label "bia&#322;y_murzyn"
  ]
  node [
    id 763
    label "Rosjanin"
  ]
  node [
    id 764
    label "bia&#322;e"
  ]
  node [
    id 765
    label "jasnosk&#243;ry"
  ]
  node [
    id 766
    label "bierka_szachowa"
  ]
  node [
    id 767
    label "bia&#322;y_taniec"
  ]
  node [
    id 768
    label "dzia&#322;acz"
  ]
  node [
    id 769
    label "bezbarwny"
  ]
  node [
    id 770
    label "siwy"
  ]
  node [
    id 771
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 772
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 773
    label "Polak"
  ]
  node [
    id 774
    label "bia&#322;o"
  ]
  node [
    id 775
    label "typ_orientalny"
  ]
  node [
    id 776
    label "libera&#322;"
  ]
  node [
    id 777
    label "czysty"
  ]
  node [
    id 778
    label "&#347;nie&#380;nie"
  ]
  node [
    id 779
    label "konserwatysta"
  ]
  node [
    id 780
    label "&#347;nie&#380;no"
  ]
  node [
    id 781
    label "bia&#322;as"
  ]
  node [
    id 782
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 783
    label "blady"
  ]
  node [
    id 784
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 785
    label "nacjonalista"
  ]
  node [
    id 786
    label "jasny"
  ]
  node [
    id 787
    label "podobny"
  ]
  node [
    id 788
    label "paramedycznie"
  ]
  node [
    id 789
    label "og&#243;lnopolski"
  ]
  node [
    id 790
    label "kongres"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 33
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 502
  ]
  edge [
    source 15
    target 503
  ]
  edge [
    source 15
    target 504
  ]
  edge [
    source 15
    target 259
  ]
  edge [
    source 15
    target 505
  ]
  edge [
    source 15
    target 486
  ]
  edge [
    source 15
    target 506
  ]
  edge [
    source 15
    target 507
  ]
  edge [
    source 15
    target 508
  ]
  edge [
    source 15
    target 509
  ]
  edge [
    source 15
    target 510
  ]
  edge [
    source 15
    target 511
  ]
  edge [
    source 15
    target 512
  ]
  edge [
    source 15
    target 33
  ]
  edge [
    source 15
    target 513
  ]
  edge [
    source 15
    target 514
  ]
  edge [
    source 15
    target 515
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 485
  ]
  edge [
    source 15
    target 516
  ]
  edge [
    source 15
    target 517
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 520
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 245
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 533
  ]
  edge [
    source 16
    target 534
  ]
  edge [
    source 16
    target 535
  ]
  edge [
    source 16
    target 536
  ]
  edge [
    source 16
    target 537
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 539
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 542
  ]
  edge [
    source 17
    target 543
  ]
  edge [
    source 17
    target 544
  ]
  edge [
    source 17
    target 545
  ]
  edge [
    source 17
    target 546
  ]
  edge [
    source 17
    target 547
  ]
  edge [
    source 17
    target 548
  ]
  edge [
    source 17
    target 549
  ]
  edge [
    source 17
    target 550
  ]
  edge [
    source 17
    target 551
  ]
  edge [
    source 17
    target 552
  ]
  edge [
    source 17
    target 553
  ]
  edge [
    source 17
    target 554
  ]
  edge [
    source 17
    target 555
  ]
  edge [
    source 17
    target 556
  ]
  edge [
    source 17
    target 557
  ]
  edge [
    source 17
    target 558
  ]
  edge [
    source 17
    target 559
  ]
  edge [
    source 17
    target 560
  ]
  edge [
    source 17
    target 561
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 17
    target 563
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 565
  ]
  edge [
    source 17
    target 566
  ]
  edge [
    source 17
    target 567
  ]
  edge [
    source 17
    target 568
  ]
  edge [
    source 17
    target 569
  ]
  edge [
    source 17
    target 570
  ]
  edge [
    source 17
    target 571
  ]
  edge [
    source 17
    target 572
  ]
  edge [
    source 17
    target 573
  ]
  edge [
    source 17
    target 574
  ]
  edge [
    source 17
    target 575
  ]
  edge [
    source 17
    target 576
  ]
  edge [
    source 17
    target 577
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 579
  ]
  edge [
    source 17
    target 580
  ]
  edge [
    source 17
    target 581
  ]
  edge [
    source 17
    target 582
  ]
  edge [
    source 17
    target 583
  ]
  edge [
    source 17
    target 584
  ]
  edge [
    source 17
    target 585
  ]
  edge [
    source 17
    target 586
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 588
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 591
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 593
  ]
  edge [
    source 17
    target 521
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 595
  ]
  edge [
    source 17
    target 72
  ]
  edge [
    source 17
    target 596
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 598
  ]
  edge [
    source 17
    target 599
  ]
  edge [
    source 17
    target 600
  ]
  edge [
    source 17
    target 601
  ]
  edge [
    source 17
    target 602
  ]
  edge [
    source 17
    target 603
  ]
  edge [
    source 17
    target 604
  ]
  edge [
    source 17
    target 605
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 610
  ]
  edge [
    source 17
    target 611
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 614
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 622
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 51
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 625
  ]
  edge [
    source 17
    target 626
  ]
  edge [
    source 17
    target 627
  ]
  edge [
    source 17
    target 628
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 632
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 635
  ]
  edge [
    source 17
    target 636
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 638
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 531
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 523
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 82
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 519
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 734
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 736
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 644
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 331
  ]
  edge [
    source 20
    target 745
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 747
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 460
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 448
  ]
  edge [
    source 20
    target 49
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 351
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 761
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 20
    target 763
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 764
  ]
  edge [
    source 20
    target 765
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 772
  ]
  edge [
    source 20
    target 773
  ]
  edge [
    source 20
    target 774
  ]
  edge [
    source 20
    target 775
  ]
  edge [
    source 20
    target 776
  ]
  edge [
    source 20
    target 777
  ]
  edge [
    source 20
    target 778
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 780
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 783
  ]
  edge [
    source 20
    target 784
  ]
  edge [
    source 20
    target 785
  ]
  edge [
    source 20
    target 786
  ]
  edge [
    source 20
    target 787
  ]
  edge [
    source 20
    target 788
  ]
  edge [
    source 20
    target 789
  ]
  edge [
    source 20
    target 790
  ]
  edge [
    source 789
    target 790
  ]
]
