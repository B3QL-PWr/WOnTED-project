graph [
  node [
    id 0
    label "gdy"
    origin "text"
  ]
  node [
    id 1
    label "przychodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "obrabiarka"
    origin "text"
  ]
  node [
    id 4
    label "korea"
    origin "text"
  ]
  node [
    id 5
    label "dokumentacja"
    origin "text"
  ]
  node [
    id 6
    label "spakowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pudlo"
    origin "text"
  ]
  node [
    id 8
    label "papier"
    origin "text"
  ]
  node [
    id 9
    label "drukarka"
    origin "text"
  ]
  node [
    id 10
    label "przybywa&#263;"
  ]
  node [
    id 11
    label "dochodzi&#263;"
  ]
  node [
    id 12
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 13
    label "robi&#263;"
  ]
  node [
    id 14
    label "uzyskiwa&#263;"
  ]
  node [
    id 15
    label "claim"
  ]
  node [
    id 16
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 17
    label "osi&#261;ga&#263;"
  ]
  node [
    id 18
    label "ripen"
  ]
  node [
    id 19
    label "supervene"
  ]
  node [
    id 20
    label "doczeka&#263;"
  ]
  node [
    id 21
    label "przesy&#322;ka"
  ]
  node [
    id 22
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 23
    label "doznawa&#263;"
  ]
  node [
    id 24
    label "reach"
  ]
  node [
    id 25
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 26
    label "dociera&#263;"
  ]
  node [
    id 27
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 28
    label "zachodzi&#263;"
  ]
  node [
    id 29
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 30
    label "postrzega&#263;"
  ]
  node [
    id 31
    label "orgazm"
  ]
  node [
    id 32
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 33
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 34
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 35
    label "dokoptowywa&#263;"
  ]
  node [
    id 36
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 37
    label "dolatywa&#263;"
  ]
  node [
    id 38
    label "powodowa&#263;"
  ]
  node [
    id 39
    label "submit"
  ]
  node [
    id 40
    label "get"
  ]
  node [
    id 41
    label "zyskiwa&#263;"
  ]
  node [
    id 42
    label "gwiazda"
  ]
  node [
    id 43
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 44
    label "Arktur"
  ]
  node [
    id 45
    label "kszta&#322;t"
  ]
  node [
    id 46
    label "Gwiazda_Polarna"
  ]
  node [
    id 47
    label "agregatka"
  ]
  node [
    id 48
    label "gromada"
  ]
  node [
    id 49
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 50
    label "S&#322;o&#324;ce"
  ]
  node [
    id 51
    label "Nibiru"
  ]
  node [
    id 52
    label "konstelacja"
  ]
  node [
    id 53
    label "ornament"
  ]
  node [
    id 54
    label "delta_Scuti"
  ]
  node [
    id 55
    label "&#347;wiat&#322;o"
  ]
  node [
    id 56
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 57
    label "obiekt"
  ]
  node [
    id 58
    label "s&#322;awa"
  ]
  node [
    id 59
    label "promie&#324;"
  ]
  node [
    id 60
    label "star"
  ]
  node [
    id 61
    label "gwiazdosz"
  ]
  node [
    id 62
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 63
    label "asocjacja_gwiazd"
  ]
  node [
    id 64
    label "supergrupa"
  ]
  node [
    id 65
    label "wrzeciono"
  ]
  node [
    id 66
    label "wrzeciennik"
  ]
  node [
    id 67
    label "imak"
  ]
  node [
    id 68
    label "suport"
  ]
  node [
    id 69
    label "maszyna"
  ]
  node [
    id 70
    label "sto&#380;ek_Morse'a"
  ]
  node [
    id 71
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 72
    label "cz&#322;owiek"
  ]
  node [
    id 73
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 74
    label "tuleja"
  ]
  node [
    id 75
    label "pracowanie"
  ]
  node [
    id 76
    label "kad&#322;ub"
  ]
  node [
    id 77
    label "n&#243;&#380;"
  ]
  node [
    id 78
    label "b&#281;benek"
  ]
  node [
    id 79
    label "wa&#322;"
  ]
  node [
    id 80
    label "maszyneria"
  ]
  node [
    id 81
    label "prototypownia"
  ]
  node [
    id 82
    label "trawers"
  ]
  node [
    id 83
    label "deflektor"
  ]
  node [
    id 84
    label "mechanizm"
  ]
  node [
    id 85
    label "kolumna"
  ]
  node [
    id 86
    label "wa&#322;ek"
  ]
  node [
    id 87
    label "pracowa&#263;"
  ]
  node [
    id 88
    label "b&#281;ben"
  ]
  node [
    id 89
    label "rz&#281;zi&#263;"
  ]
  node [
    id 90
    label "przyk&#322;adka"
  ]
  node [
    id 91
    label "t&#322;ok"
  ]
  node [
    id 92
    label "dehumanizacja"
  ]
  node [
    id 93
    label "rami&#281;"
  ]
  node [
    id 94
    label "rz&#281;&#380;enie"
  ]
  node [
    id 95
    label "urz&#261;dzenie"
  ]
  node [
    id 96
    label "spindle"
  ]
  node [
    id 97
    label "narz&#281;dzie"
  ]
  node [
    id 98
    label "prz&#281;&#347;lik"
  ]
  node [
    id 99
    label "prz&#281;&#347;lica"
  ]
  node [
    id 100
    label "zgrubienie"
  ]
  node [
    id 101
    label "wios&#322;o"
  ]
  node [
    id 102
    label "spike"
  ]
  node [
    id 103
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 104
    label "support"
  ]
  node [
    id 105
    label "rower"
  ]
  node [
    id 106
    label "uchwyt"
  ]
  node [
    id 107
    label "ekscerpcja"
  ]
  node [
    id 108
    label "materia&#322;"
  ]
  node [
    id 109
    label "operat"
  ]
  node [
    id 110
    label "kosztorys"
  ]
  node [
    id 111
    label "materia"
  ]
  node [
    id 112
    label "nawil&#380;arka"
  ]
  node [
    id 113
    label "bielarnia"
  ]
  node [
    id 114
    label "dyspozycja"
  ]
  node [
    id 115
    label "dane"
  ]
  node [
    id 116
    label "tworzywo"
  ]
  node [
    id 117
    label "substancja"
  ]
  node [
    id 118
    label "kandydat"
  ]
  node [
    id 119
    label "archiwum"
  ]
  node [
    id 120
    label "krajka"
  ]
  node [
    id 121
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 122
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 123
    label "krajalno&#347;&#263;"
  ]
  node [
    id 124
    label "opracowanie"
  ]
  node [
    id 125
    label "plan"
  ]
  node [
    id 126
    label "obmiar"
  ]
  node [
    id 127
    label "wyb&#243;r"
  ]
  node [
    id 128
    label "tekst"
  ]
  node [
    id 129
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 130
    label "throng"
  ]
  node [
    id 131
    label "owin&#261;&#263;"
  ]
  node [
    id 132
    label "skonwertowa&#263;"
  ]
  node [
    id 133
    label "pack"
  ]
  node [
    id 134
    label "zreorganizowa&#263;"
  ]
  node [
    id 135
    label "convert"
  ]
  node [
    id 136
    label "przekszta&#322;ci&#263;"
  ]
  node [
    id 137
    label "otoczy&#263;"
  ]
  node [
    id 138
    label "twine"
  ]
  node [
    id 139
    label "ubra&#263;"
  ]
  node [
    id 140
    label "oblec_si&#281;"
  ]
  node [
    id 141
    label "przekaza&#263;"
  ]
  node [
    id 142
    label "str&#243;j"
  ]
  node [
    id 143
    label "insert"
  ]
  node [
    id 144
    label "wpoi&#263;"
  ]
  node [
    id 145
    label "pour"
  ]
  node [
    id 146
    label "przyodzia&#263;"
  ]
  node [
    id 147
    label "natchn&#261;&#263;"
  ]
  node [
    id 148
    label "load"
  ]
  node [
    id 149
    label "wzbudzi&#263;"
  ]
  node [
    id 150
    label "deposit"
  ]
  node [
    id 151
    label "umie&#347;ci&#263;"
  ]
  node [
    id 152
    label "oblec"
  ]
  node [
    id 153
    label "edytowa&#263;"
  ]
  node [
    id 154
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 155
    label "spakowanie"
  ]
  node [
    id 156
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 157
    label "pakowa&#263;"
  ]
  node [
    id 158
    label "rekord"
  ]
  node [
    id 159
    label "korelator"
  ]
  node [
    id 160
    label "wyci&#261;ganie"
  ]
  node [
    id 161
    label "pakowanie"
  ]
  node [
    id 162
    label "sekwencjonowa&#263;"
  ]
  node [
    id 163
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 164
    label "jednostka_informacji"
  ]
  node [
    id 165
    label "zbi&#243;r"
  ]
  node [
    id 166
    label "evidence"
  ]
  node [
    id 167
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 168
    label "rozpakowywanie"
  ]
  node [
    id 169
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 170
    label "rozpakowanie"
  ]
  node [
    id 171
    label "informacja"
  ]
  node [
    id 172
    label "rozpakowywa&#263;"
  ]
  node [
    id 173
    label "konwersja"
  ]
  node [
    id 174
    label "nap&#322;ywanie"
  ]
  node [
    id 175
    label "rozpakowa&#263;"
  ]
  node [
    id 176
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 177
    label "edytowanie"
  ]
  node [
    id 178
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 179
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 180
    label "sekwencjonowanie"
  ]
  node [
    id 181
    label "wytw&#243;r"
  ]
  node [
    id 182
    label "nak&#322;uwacz"
  ]
  node [
    id 183
    label "parafa"
  ]
  node [
    id 184
    label "raport&#243;wka"
  ]
  node [
    id 185
    label "papeteria"
  ]
  node [
    id 186
    label "fascyku&#322;"
  ]
  node [
    id 187
    label "registratura"
  ]
  node [
    id 188
    label "libra"
  ]
  node [
    id 189
    label "format_arkusza"
  ]
  node [
    id 190
    label "artyku&#322;"
  ]
  node [
    id 191
    label "writing"
  ]
  node [
    id 192
    label "sygnatariusz"
  ]
  node [
    id 193
    label "blok"
  ]
  node [
    id 194
    label "prawda"
  ]
  node [
    id 195
    label "znak_j&#281;zykowy"
  ]
  node [
    id 196
    label "nag&#322;&#243;wek"
  ]
  node [
    id 197
    label "szkic"
  ]
  node [
    id 198
    label "line"
  ]
  node [
    id 199
    label "fragment"
  ]
  node [
    id 200
    label "wyr&#243;b"
  ]
  node [
    id 201
    label "rodzajnik"
  ]
  node [
    id 202
    label "dokument"
  ]
  node [
    id 203
    label "towar"
  ]
  node [
    id 204
    label "paragraf"
  ]
  node [
    id 205
    label "przedmiot"
  ]
  node [
    id 206
    label "p&#322;&#243;d"
  ]
  node [
    id 207
    label "work"
  ]
  node [
    id 208
    label "rezultat"
  ]
  node [
    id 209
    label "stationery"
  ]
  node [
    id 210
    label "komplet"
  ]
  node [
    id 211
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 212
    label "szpikulec"
  ]
  node [
    id 213
    label "jednostka"
  ]
  node [
    id 214
    label "quire"
  ]
  node [
    id 215
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 216
    label "waga"
  ]
  node [
    id 217
    label "jednostka_masy"
  ]
  node [
    id 218
    label "przedstawiciel"
  ]
  node [
    id 219
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 220
    label "wydanie"
  ]
  node [
    id 221
    label "plik"
  ]
  node [
    id 222
    label "torba"
  ]
  node [
    id 223
    label "biuro"
  ]
  node [
    id 224
    label "wpis"
  ]
  node [
    id 225
    label "register"
  ]
  node [
    id 226
    label "paraph"
  ]
  node [
    id 227
    label "podpis"
  ]
  node [
    id 228
    label "dupleks"
  ]
  node [
    id 229
    label "kom&#243;rka"
  ]
  node [
    id 230
    label "furnishing"
  ]
  node [
    id 231
    label "zabezpieczenie"
  ]
  node [
    id 232
    label "zrobienie"
  ]
  node [
    id 233
    label "wyrz&#261;dzenie"
  ]
  node [
    id 234
    label "zagospodarowanie"
  ]
  node [
    id 235
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 236
    label "ig&#322;a"
  ]
  node [
    id 237
    label "wirnik"
  ]
  node [
    id 238
    label "aparatura"
  ]
  node [
    id 239
    label "system_energetyczny"
  ]
  node [
    id 240
    label "impulsator"
  ]
  node [
    id 241
    label "sprz&#281;t"
  ]
  node [
    id 242
    label "czynno&#347;&#263;"
  ]
  node [
    id 243
    label "blokowanie"
  ]
  node [
    id 244
    label "set"
  ]
  node [
    id 245
    label "zablokowanie"
  ]
  node [
    id 246
    label "przygotowanie"
  ]
  node [
    id 247
    label "komora"
  ]
  node [
    id 248
    label "j&#281;zyk"
  ]
  node [
    id 249
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 250
    label "kserokopiarka"
  ]
  node [
    id 251
    label "karton"
  ]
  node [
    id 252
    label "system_&#322;&#261;czno&#347;ci"
  ]
  node [
    id 253
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 254
    label "odbitka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
]
