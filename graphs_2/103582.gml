graph [
  node [
    id 0
    label "czu&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "s&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 3
    label "cover"
    origin "text"
  ]
  node [
    id 4
    label "swoje"
    origin "text"
  ]
  node [
    id 5
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 6
    label "postrzega&#263;"
  ]
  node [
    id 7
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 8
    label "listen"
  ]
  node [
    id 9
    label "s&#322;ycha&#263;"
  ]
  node [
    id 10
    label "read"
  ]
  node [
    id 11
    label "perceive"
  ]
  node [
    id 12
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 13
    label "punkt_widzenia"
  ]
  node [
    id 14
    label "obacza&#263;"
  ]
  node [
    id 15
    label "widzie&#263;"
  ]
  node [
    id 16
    label "dochodzi&#263;"
  ]
  node [
    id 17
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 18
    label "notice"
  ]
  node [
    id 19
    label "doj&#347;&#263;"
  ]
  node [
    id 20
    label "os&#261;dza&#263;"
  ]
  node [
    id 21
    label "doba"
  ]
  node [
    id 22
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 23
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 24
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 25
    label "dzi&#347;"
  ]
  node [
    id 26
    label "teraz"
  ]
  node [
    id 27
    label "czas"
  ]
  node [
    id 28
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 29
    label "jednocze&#347;nie"
  ]
  node [
    id 30
    label "tydzie&#324;"
  ]
  node [
    id 31
    label "noc"
  ]
  node [
    id 32
    label "dzie&#324;"
  ]
  node [
    id 33
    label "godzina"
  ]
  node [
    id 34
    label "long_time"
  ]
  node [
    id 35
    label "jednostka_geologiczna"
  ]
  node [
    id 36
    label "kawa&#322;ek"
  ]
  node [
    id 37
    label "aran&#380;acja"
  ]
  node [
    id 38
    label "kawa&#322;"
  ]
  node [
    id 39
    label "plot"
  ]
  node [
    id 40
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 41
    label "piece"
  ]
  node [
    id 42
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 43
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 44
    label "podp&#322;ywanie"
  ]
  node [
    id 45
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 46
    label "jazz"
  ]
  node [
    id 47
    label "przer&#243;bka"
  ]
  node [
    id 48
    label "opracowanie"
  ]
  node [
    id 49
    label "obrazowanie"
  ]
  node [
    id 50
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 51
    label "organ"
  ]
  node [
    id 52
    label "tre&#347;&#263;"
  ]
  node [
    id 53
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 54
    label "part"
  ]
  node [
    id 55
    label "element_anatomiczny"
  ]
  node [
    id 56
    label "tekst"
  ]
  node [
    id 57
    label "komunikat"
  ]
  node [
    id 58
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 59
    label "zbi&#243;r"
  ]
  node [
    id 60
    label "dorobek"
  ]
  node [
    id 61
    label "tworzenie"
  ]
  node [
    id 62
    label "kreacja"
  ]
  node [
    id 63
    label "creation"
  ]
  node [
    id 64
    label "kultura"
  ]
  node [
    id 65
    label "ekscerpcja"
  ]
  node [
    id 66
    label "j&#281;zykowo"
  ]
  node [
    id 67
    label "wypowied&#378;"
  ]
  node [
    id 68
    label "redakcja"
  ]
  node [
    id 69
    label "wytw&#243;r"
  ]
  node [
    id 70
    label "pomini&#281;cie"
  ]
  node [
    id 71
    label "dzie&#322;o"
  ]
  node [
    id 72
    label "preparacja"
  ]
  node [
    id 73
    label "odmianka"
  ]
  node [
    id 74
    label "opu&#347;ci&#263;"
  ]
  node [
    id 75
    label "koniektura"
  ]
  node [
    id 76
    label "pisa&#263;"
  ]
  node [
    id 77
    label "obelga"
  ]
  node [
    id 78
    label "communication"
  ]
  node [
    id 79
    label "kreacjonista"
  ]
  node [
    id 80
    label "roi&#263;_si&#281;"
  ]
  node [
    id 81
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 82
    label "imaging"
  ]
  node [
    id 83
    label "przedstawianie"
  ]
  node [
    id 84
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 85
    label "temat"
  ]
  node [
    id 86
    label "istota"
  ]
  node [
    id 87
    label "informacja"
  ]
  node [
    id 88
    label "zawarto&#347;&#263;"
  ]
  node [
    id 89
    label "tkanka"
  ]
  node [
    id 90
    label "jednostka_organizacyjna"
  ]
  node [
    id 91
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 92
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 93
    label "tw&#243;r"
  ]
  node [
    id 94
    label "organogeneza"
  ]
  node [
    id 95
    label "zesp&#243;&#322;"
  ]
  node [
    id 96
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 97
    label "struktura_anatomiczna"
  ]
  node [
    id 98
    label "uk&#322;ad"
  ]
  node [
    id 99
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 100
    label "dekortykacja"
  ]
  node [
    id 101
    label "Izba_Konsyliarska"
  ]
  node [
    id 102
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 103
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 104
    label "stomia"
  ]
  node [
    id 105
    label "budowa"
  ]
  node [
    id 106
    label "okolica"
  ]
  node [
    id 107
    label "Komitet_Region&#243;w"
  ]
  node [
    id 108
    label "ta&#347;ma"
  ]
  node [
    id 109
    label "plecionka"
  ]
  node [
    id 110
    label "parciak"
  ]
  node [
    id 111
    label "p&#322;&#243;tno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
]
