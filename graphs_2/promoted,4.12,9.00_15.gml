graph [
  node [
    id 0
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "undertaking"
  ]
  node [
    id 3
    label "wystarcza&#263;"
  ]
  node [
    id 4
    label "kosztowa&#263;"
  ]
  node [
    id 5
    label "pozostawa&#263;"
  ]
  node [
    id 6
    label "czeka&#263;"
  ]
  node [
    id 7
    label "sprawowa&#263;"
  ]
  node [
    id 8
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 9
    label "trwa&#263;"
  ]
  node [
    id 10
    label "przebywa&#263;"
  ]
  node [
    id 11
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 12
    label "wystarczy&#263;"
  ]
  node [
    id 13
    label "wystawa&#263;"
  ]
  node [
    id 14
    label "by&#263;"
  ]
  node [
    id 15
    label "base"
  ]
  node [
    id 16
    label "digest"
  ]
  node [
    id 17
    label "mieszka&#263;"
  ]
  node [
    id 18
    label "stand"
  ]
  node [
    id 19
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "dobywa&#263;_si&#281;"
  ]
  node [
    id 21
    label "adhere"
  ]
  node [
    id 22
    label "zostawa&#263;"
  ]
  node [
    id 23
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 24
    label "istnie&#263;"
  ]
  node [
    id 25
    label "panowa&#263;"
  ]
  node [
    id 26
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 27
    label "function"
  ]
  node [
    id 28
    label "bind"
  ]
  node [
    id 29
    label "zjednywa&#263;"
  ]
  node [
    id 30
    label "hesitate"
  ]
  node [
    id 31
    label "pause"
  ]
  node [
    id 32
    label "przestawa&#263;"
  ]
  node [
    id 33
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 34
    label "tkwi&#263;"
  ]
  node [
    id 35
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 36
    label "stan"
  ]
  node [
    id 37
    label "equal"
  ]
  node [
    id 38
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 39
    label "chodzi&#263;"
  ]
  node [
    id 40
    label "uczestniczy&#263;"
  ]
  node [
    id 41
    label "obecno&#347;&#263;"
  ]
  node [
    id 42
    label "si&#281;ga&#263;"
  ]
  node [
    id 43
    label "mie&#263;_miejsce"
  ]
  node [
    id 44
    label "savor"
  ]
  node [
    id 45
    label "try"
  ]
  node [
    id 46
    label "essay"
  ]
  node [
    id 47
    label "doznawa&#263;"
  ]
  node [
    id 48
    label "cena"
  ]
  node [
    id 49
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 50
    label "suffice"
  ]
  node [
    id 51
    label "dostawa&#263;"
  ]
  node [
    id 52
    label "zaspokaja&#263;"
  ]
  node [
    id 53
    label "stawa&#263;"
  ]
  node [
    id 54
    label "spowodowa&#263;"
  ]
  node [
    id 55
    label "stan&#261;&#263;"
  ]
  node [
    id 56
    label "zaspokoi&#263;"
  ]
  node [
    id 57
    label "dosta&#263;"
  ]
  node [
    id 58
    label "hold"
  ]
  node [
    id 59
    label "look"
  ]
  node [
    id 60
    label "decydowa&#263;"
  ]
  node [
    id 61
    label "anticipate"
  ]
  node [
    id 62
    label "pauzowa&#263;"
  ]
  node [
    id 63
    label "oczekiwa&#263;"
  ]
  node [
    id 64
    label "sp&#281;dza&#263;"
  ]
  node [
    id 65
    label "blend"
  ]
  node [
    id 66
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 67
    label "support"
  ]
  node [
    id 68
    label "stop"
  ]
  node [
    id 69
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 70
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 71
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 72
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 73
    label "prosecute"
  ]
  node [
    id 74
    label "fall"
  ]
  node [
    id 75
    label "zajmowa&#263;"
  ]
  node [
    id 76
    label "room"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
]
