graph [
  node [
    id 0
    label "gdy"
    origin "text"
  ]
  node [
    id 1
    label "recenzowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dron"
    origin "text"
  ]
  node [
    id 3
    label "tello"
    origin "text"
  ]
  node [
    id 4
    label "szczeg&#243;lny"
    origin "text"
  ]
  node [
    id 5
    label "uwaga"
    origin "text"
  ]
  node [
    id 6
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 7
    label "zaprogramowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "samolot"
  ]
  node [
    id 9
    label "spalin&#243;wka"
  ]
  node [
    id 10
    label "katapulta"
  ]
  node [
    id 11
    label "pilot_automatyczny"
  ]
  node [
    id 12
    label "kad&#322;ub"
  ]
  node [
    id 13
    label "kabina"
  ]
  node [
    id 14
    label "wiatrochron"
  ]
  node [
    id 15
    label "wylatywanie"
  ]
  node [
    id 16
    label "kapotowanie"
  ]
  node [
    id 17
    label "kapotowa&#263;"
  ]
  node [
    id 18
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 19
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 20
    label "skrzyd&#322;o"
  ]
  node [
    id 21
    label "pok&#322;ad"
  ]
  node [
    id 22
    label "kapota&#380;"
  ]
  node [
    id 23
    label "sta&#322;op&#322;at"
  ]
  node [
    id 24
    label "sterownica"
  ]
  node [
    id 25
    label "p&#322;atowiec"
  ]
  node [
    id 26
    label "wylecenie"
  ]
  node [
    id 27
    label "wylecie&#263;"
  ]
  node [
    id 28
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 29
    label "wylatywa&#263;"
  ]
  node [
    id 30
    label "gondola"
  ]
  node [
    id 31
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 32
    label "dzi&#243;b"
  ]
  node [
    id 33
    label "inhalator_tlenowy"
  ]
  node [
    id 34
    label "kapot"
  ]
  node [
    id 35
    label "kabinka"
  ]
  node [
    id 36
    label "&#380;yroskop"
  ]
  node [
    id 37
    label "czarna_skrzynka"
  ]
  node [
    id 38
    label "lecenie"
  ]
  node [
    id 39
    label "fotel_lotniczy"
  ]
  node [
    id 40
    label "wy&#347;lizg"
  ]
  node [
    id 41
    label "szczeg&#243;lnie"
  ]
  node [
    id 42
    label "wyj&#261;tkowy"
  ]
  node [
    id 43
    label "wyj&#261;tkowo"
  ]
  node [
    id 44
    label "inny"
  ]
  node [
    id 45
    label "specially"
  ]
  node [
    id 46
    label "osobnie"
  ]
  node [
    id 47
    label "wypowied&#378;"
  ]
  node [
    id 48
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 49
    label "stan"
  ]
  node [
    id 50
    label "nagana"
  ]
  node [
    id 51
    label "tekst"
  ]
  node [
    id 52
    label "upomnienie"
  ]
  node [
    id 53
    label "dzienniczek"
  ]
  node [
    id 54
    label "wzgl&#261;d"
  ]
  node [
    id 55
    label "gossip"
  ]
  node [
    id 56
    label "Ohio"
  ]
  node [
    id 57
    label "wci&#281;cie"
  ]
  node [
    id 58
    label "Nowy_York"
  ]
  node [
    id 59
    label "warstwa"
  ]
  node [
    id 60
    label "samopoczucie"
  ]
  node [
    id 61
    label "Illinois"
  ]
  node [
    id 62
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 63
    label "state"
  ]
  node [
    id 64
    label "Jukatan"
  ]
  node [
    id 65
    label "Kalifornia"
  ]
  node [
    id 66
    label "Wirginia"
  ]
  node [
    id 67
    label "wektor"
  ]
  node [
    id 68
    label "by&#263;"
  ]
  node [
    id 69
    label "Teksas"
  ]
  node [
    id 70
    label "Goa"
  ]
  node [
    id 71
    label "Waszyngton"
  ]
  node [
    id 72
    label "miejsce"
  ]
  node [
    id 73
    label "Massachusetts"
  ]
  node [
    id 74
    label "Alaska"
  ]
  node [
    id 75
    label "Arakan"
  ]
  node [
    id 76
    label "Hawaje"
  ]
  node [
    id 77
    label "Maryland"
  ]
  node [
    id 78
    label "punkt"
  ]
  node [
    id 79
    label "Michigan"
  ]
  node [
    id 80
    label "Arizona"
  ]
  node [
    id 81
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 82
    label "Georgia"
  ]
  node [
    id 83
    label "poziom"
  ]
  node [
    id 84
    label "Pensylwania"
  ]
  node [
    id 85
    label "shape"
  ]
  node [
    id 86
    label "Luizjana"
  ]
  node [
    id 87
    label "Nowy_Meksyk"
  ]
  node [
    id 88
    label "Alabama"
  ]
  node [
    id 89
    label "ilo&#347;&#263;"
  ]
  node [
    id 90
    label "Kansas"
  ]
  node [
    id 91
    label "Oregon"
  ]
  node [
    id 92
    label "Floryda"
  ]
  node [
    id 93
    label "Oklahoma"
  ]
  node [
    id 94
    label "jednostka_administracyjna"
  ]
  node [
    id 95
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 96
    label "ekscerpcja"
  ]
  node [
    id 97
    label "j&#281;zykowo"
  ]
  node [
    id 98
    label "redakcja"
  ]
  node [
    id 99
    label "wytw&#243;r"
  ]
  node [
    id 100
    label "pomini&#281;cie"
  ]
  node [
    id 101
    label "dzie&#322;o"
  ]
  node [
    id 102
    label "preparacja"
  ]
  node [
    id 103
    label "odmianka"
  ]
  node [
    id 104
    label "opu&#347;ci&#263;"
  ]
  node [
    id 105
    label "koniektura"
  ]
  node [
    id 106
    label "pisa&#263;"
  ]
  node [
    id 107
    label "obelga"
  ]
  node [
    id 108
    label "pos&#322;uchanie"
  ]
  node [
    id 109
    label "s&#261;d"
  ]
  node [
    id 110
    label "sparafrazowanie"
  ]
  node [
    id 111
    label "strawestowa&#263;"
  ]
  node [
    id 112
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 113
    label "trawestowa&#263;"
  ]
  node [
    id 114
    label "sparafrazowa&#263;"
  ]
  node [
    id 115
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 116
    label "sformu&#322;owanie"
  ]
  node [
    id 117
    label "parafrazowanie"
  ]
  node [
    id 118
    label "ozdobnik"
  ]
  node [
    id 119
    label "delimitacja"
  ]
  node [
    id 120
    label "parafrazowa&#263;"
  ]
  node [
    id 121
    label "stylizacja"
  ]
  node [
    id 122
    label "komunikat"
  ]
  node [
    id 123
    label "trawestowanie"
  ]
  node [
    id 124
    label "strawestowanie"
  ]
  node [
    id 125
    label "rezultat"
  ]
  node [
    id 126
    label "admonicja"
  ]
  node [
    id 127
    label "ukaranie"
  ]
  node [
    id 128
    label "krytyka"
  ]
  node [
    id 129
    label "censure"
  ]
  node [
    id 130
    label "wezwanie"
  ]
  node [
    id 131
    label "pouczenie"
  ]
  node [
    id 132
    label "monitorium"
  ]
  node [
    id 133
    label "napomnienie"
  ]
  node [
    id 134
    label "steering"
  ]
  node [
    id 135
    label "kara"
  ]
  node [
    id 136
    label "punkt_widzenia"
  ]
  node [
    id 137
    label "przyczyna"
  ]
  node [
    id 138
    label "zeszyt"
  ]
  node [
    id 139
    label "trypanosomoza"
  ]
  node [
    id 140
    label "criticism"
  ]
  node [
    id 141
    label "schorzenie"
  ]
  node [
    id 142
    label "&#347;widrowiec_nagany"
  ]
  node [
    id 143
    label "free"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 143
  ]
]
