graph [
  node [
    id 0
    label "szanowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "plusujesz"
    origin "text"
  ]
  node [
    id 2
    label "powa&#380;anie"
  ]
  node [
    id 3
    label "treasure"
  ]
  node [
    id 4
    label "czu&#263;"
  ]
  node [
    id 5
    label "respektowa&#263;"
  ]
  node [
    id 6
    label "wyra&#380;a&#263;"
  ]
  node [
    id 7
    label "chowa&#263;"
  ]
  node [
    id 8
    label "postrzega&#263;"
  ]
  node [
    id 9
    label "przewidywa&#263;"
  ]
  node [
    id 10
    label "by&#263;"
  ]
  node [
    id 11
    label "smell"
  ]
  node [
    id 12
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 13
    label "uczuwa&#263;"
  ]
  node [
    id 14
    label "spirit"
  ]
  node [
    id 15
    label "doznawa&#263;"
  ]
  node [
    id 16
    label "anticipate"
  ]
  node [
    id 17
    label "znaczy&#263;"
  ]
  node [
    id 18
    label "give_voice"
  ]
  node [
    id 19
    label "oznacza&#263;"
  ]
  node [
    id 20
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 21
    label "represent"
  ]
  node [
    id 22
    label "komunikowa&#263;"
  ]
  node [
    id 23
    label "convey"
  ]
  node [
    id 24
    label "arouse"
  ]
  node [
    id 25
    label "report"
  ]
  node [
    id 26
    label "hide"
  ]
  node [
    id 27
    label "znosi&#263;"
  ]
  node [
    id 28
    label "train"
  ]
  node [
    id 29
    label "przetrzymywa&#263;"
  ]
  node [
    id 30
    label "hodowa&#263;"
  ]
  node [
    id 31
    label "meliniarz"
  ]
  node [
    id 32
    label "umieszcza&#263;"
  ]
  node [
    id 33
    label "ukrywa&#263;"
  ]
  node [
    id 34
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "continue"
  ]
  node [
    id 36
    label "wk&#322;ada&#263;"
  ]
  node [
    id 37
    label "appreciate"
  ]
  node [
    id 38
    label "zachowywa&#263;"
  ]
  node [
    id 39
    label "tennis"
  ]
  node [
    id 40
    label "honorowa&#263;"
  ]
  node [
    id 41
    label "uhonorowanie"
  ]
  node [
    id 42
    label "zaimponowanie"
  ]
  node [
    id 43
    label "honorowanie"
  ]
  node [
    id 44
    label "uszanowa&#263;"
  ]
  node [
    id 45
    label "chowanie"
  ]
  node [
    id 46
    label "respektowanie"
  ]
  node [
    id 47
    label "uszanowanie"
  ]
  node [
    id 48
    label "szacuneczek"
  ]
  node [
    id 49
    label "rewerencja"
  ]
  node [
    id 50
    label "uhonorowa&#263;"
  ]
  node [
    id 51
    label "czucie"
  ]
  node [
    id 52
    label "fame"
  ]
  node [
    id 53
    label "respect"
  ]
  node [
    id 54
    label "postawa"
  ]
  node [
    id 55
    label "imponowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
]
