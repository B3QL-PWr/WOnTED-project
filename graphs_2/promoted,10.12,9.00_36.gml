graph [
  node [
    id 0
    label "karetka"
    origin "text"
  ]
  node [
    id 1
    label "wie&#378;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "krak&#243;w"
    origin "text"
  ]
  node [
    id 3
    label "serce"
    origin "text"
  ]
  node [
    id 4
    label "przeszczep"
    origin "text"
  ]
  node [
    id 5
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "robil"
    origin "text"
  ]
  node [
    id 7
    label "czas"
    origin "text"
  ]
  node [
    id 8
    label "kierowca"
    origin "text"
  ]
  node [
    id 9
    label "bus"
    origin "text"
  ]
  node [
    id 10
    label "samoch&#243;d"
  ]
  node [
    id 11
    label "poduszka_powietrzna"
  ]
  node [
    id 12
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 13
    label "pompa_wodna"
  ]
  node [
    id 14
    label "bak"
  ]
  node [
    id 15
    label "deska_rozdzielcza"
  ]
  node [
    id 16
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 17
    label "spryskiwacz"
  ]
  node [
    id 18
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 19
    label "baga&#380;nik"
  ]
  node [
    id 20
    label "poci&#261;g_drogowy"
  ]
  node [
    id 21
    label "immobilizer"
  ]
  node [
    id 22
    label "kierownica"
  ]
  node [
    id 23
    label "ABS"
  ]
  node [
    id 24
    label "dwu&#347;lad"
  ]
  node [
    id 25
    label "tempomat"
  ]
  node [
    id 26
    label "pojazd_drogowy"
  ]
  node [
    id 27
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 28
    label "wycieraczka"
  ]
  node [
    id 29
    label "most"
  ]
  node [
    id 30
    label "silnik"
  ]
  node [
    id 31
    label "t&#322;umik"
  ]
  node [
    id 32
    label "dachowanie"
  ]
  node [
    id 33
    label "carry"
  ]
  node [
    id 34
    label "transportowa&#263;"
  ]
  node [
    id 35
    label "przemieszcza&#263;"
  ]
  node [
    id 36
    label "komora"
  ]
  node [
    id 37
    label "wsierdzie"
  ]
  node [
    id 38
    label "deformowa&#263;"
  ]
  node [
    id 39
    label "strunowiec"
  ]
  node [
    id 40
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 41
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 42
    label "deformowanie"
  ]
  node [
    id 43
    label "pikawa"
  ]
  node [
    id 44
    label "sfera_afektywna"
  ]
  node [
    id 45
    label "sumienie"
  ]
  node [
    id 46
    label "entity"
  ]
  node [
    id 47
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 48
    label "dusza"
  ]
  node [
    id 49
    label "psychika"
  ]
  node [
    id 50
    label "organ"
  ]
  node [
    id 51
    label "dobro&#263;"
  ]
  node [
    id 52
    label "wola"
  ]
  node [
    id 53
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 54
    label "charakter"
  ]
  node [
    id 55
    label "fizjonomia"
  ]
  node [
    id 56
    label "podroby"
  ]
  node [
    id 57
    label "power"
  ]
  node [
    id 58
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 59
    label "kier"
  ]
  node [
    id 60
    label "kardiografia"
  ]
  node [
    id 61
    label "favor"
  ]
  node [
    id 62
    label "zastawka"
  ]
  node [
    id 63
    label "podekscytowanie"
  ]
  node [
    id 64
    label "mi&#281;sie&#324;"
  ]
  node [
    id 65
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 66
    label "kompleks"
  ]
  node [
    id 67
    label "heart"
  ]
  node [
    id 68
    label "punkt"
  ]
  node [
    id 69
    label "cz&#322;owiek"
  ]
  node [
    id 70
    label "systol"
  ]
  node [
    id 71
    label "pulsowanie"
  ]
  node [
    id 72
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 73
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 74
    label "cecha"
  ]
  node [
    id 75
    label "courage"
  ]
  node [
    id 76
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 77
    label "seksualno&#347;&#263;"
  ]
  node [
    id 78
    label "koniuszek_serca"
  ]
  node [
    id 79
    label "zapalno&#347;&#263;"
  ]
  node [
    id 80
    label "ego"
  ]
  node [
    id 81
    label "osobowo&#347;&#263;"
  ]
  node [
    id 82
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 83
    label "pulsowa&#263;"
  ]
  node [
    id 84
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 85
    label "passion"
  ]
  node [
    id 86
    label "mikrokosmos"
  ]
  node [
    id 87
    label "kompleksja"
  ]
  node [
    id 88
    label "nastawienie"
  ]
  node [
    id 89
    label "elektrokardiografia"
  ]
  node [
    id 90
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 91
    label "dzwon"
  ]
  node [
    id 92
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 93
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 94
    label "przedsionek"
  ]
  node [
    id 95
    label "kszta&#322;t"
  ]
  node [
    id 96
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 97
    label "karta"
  ]
  node [
    id 98
    label "zajawka"
  ]
  node [
    id 99
    label "inclination"
  ]
  node [
    id 100
    label "emocja"
  ]
  node [
    id 101
    label "mniemanie"
  ]
  node [
    id 102
    label "oskoma"
  ]
  node [
    id 103
    label "wish"
  ]
  node [
    id 104
    label "bearing"
  ]
  node [
    id 105
    label "powaga"
  ]
  node [
    id 106
    label "set"
  ]
  node [
    id 107
    label "gotowanie_si&#281;"
  ]
  node [
    id 108
    label "w&#322;&#261;czenie"
  ]
  node [
    id 109
    label "ponastawianie"
  ]
  node [
    id 110
    label "umieszczenie"
  ]
  node [
    id 111
    label "oddzia&#322;anie"
  ]
  node [
    id 112
    label "ustawienie"
  ]
  node [
    id 113
    label "ukierunkowanie"
  ]
  node [
    id 114
    label "podej&#347;cie"
  ]
  node [
    id 115
    label "z&#322;amanie"
  ]
  node [
    id 116
    label "z&#322;o&#380;enie"
  ]
  node [
    id 117
    label "dobro"
  ]
  node [
    id 118
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 119
    label "go&#322;&#261;bek"
  ]
  node [
    id 120
    label "spirala"
  ]
  node [
    id 121
    label "miniatura"
  ]
  node [
    id 122
    label "blaszka"
  ]
  node [
    id 123
    label "g&#322;owa"
  ]
  node [
    id 124
    label "kielich"
  ]
  node [
    id 125
    label "p&#322;at"
  ]
  node [
    id 126
    label "wygl&#261;d"
  ]
  node [
    id 127
    label "pasmo"
  ]
  node [
    id 128
    label "obiekt"
  ]
  node [
    id 129
    label "comeliness"
  ]
  node [
    id 130
    label "face"
  ]
  node [
    id 131
    label "formacja"
  ]
  node [
    id 132
    label "gwiazda"
  ]
  node [
    id 133
    label "punkt_widzenia"
  ]
  node [
    id 134
    label "p&#281;tla"
  ]
  node [
    id 135
    label "linearno&#347;&#263;"
  ]
  node [
    id 136
    label "przyroda"
  ]
  node [
    id 137
    label "odbicie"
  ]
  node [
    id 138
    label "atom"
  ]
  node [
    id 139
    label "kosmos"
  ]
  node [
    id 140
    label "Ziemia"
  ]
  node [
    id 141
    label "dogrzanie"
  ]
  node [
    id 142
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 143
    label "dogrzewa&#263;"
  ]
  node [
    id 144
    label "masa_mi&#281;&#347;niowa"
  ]
  node [
    id 145
    label "przyczep"
  ]
  node [
    id 146
    label "niedow&#322;ad_po&#322;owiczy"
  ]
  node [
    id 147
    label "brzusiec"
  ]
  node [
    id 148
    label "&#347;ci&#281;gno"
  ]
  node [
    id 149
    label "dogrza&#263;"
  ]
  node [
    id 150
    label "hemiplegia"
  ]
  node [
    id 151
    label "dogrzewanie"
  ]
  node [
    id 152
    label "elektromiografia"
  ]
  node [
    id 153
    label "fosfagen"
  ]
  node [
    id 154
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 155
    label "uk&#322;ad"
  ]
  node [
    id 156
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 157
    label "Komitet_Region&#243;w"
  ]
  node [
    id 158
    label "struktura_anatomiczna"
  ]
  node [
    id 159
    label "organogeneza"
  ]
  node [
    id 160
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 161
    label "tw&#243;r"
  ]
  node [
    id 162
    label "tkanka"
  ]
  node [
    id 163
    label "stomia"
  ]
  node [
    id 164
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 165
    label "budowa"
  ]
  node [
    id 166
    label "dekortykacja"
  ]
  node [
    id 167
    label "okolica"
  ]
  node [
    id 168
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 169
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 170
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 171
    label "Izba_Konsyliarska"
  ]
  node [
    id 172
    label "zesp&#243;&#322;"
  ]
  node [
    id 173
    label "jednostka_organizacyjna"
  ]
  node [
    id 174
    label "charakterystyka"
  ]
  node [
    id 175
    label "m&#322;ot"
  ]
  node [
    id 176
    label "marka"
  ]
  node [
    id 177
    label "pr&#243;ba"
  ]
  node [
    id 178
    label "attribute"
  ]
  node [
    id 179
    label "drzewo"
  ]
  node [
    id 180
    label "znak"
  ]
  node [
    id 181
    label "ticket"
  ]
  node [
    id 182
    label "formularz"
  ]
  node [
    id 183
    label "p&#322;ytka"
  ]
  node [
    id 184
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 185
    label "danie"
  ]
  node [
    id 186
    label "komputer"
  ]
  node [
    id 187
    label "circuit_board"
  ]
  node [
    id 188
    label "charter"
  ]
  node [
    id 189
    label "kartonik"
  ]
  node [
    id 190
    label "oferta"
  ]
  node [
    id 191
    label "cennik"
  ]
  node [
    id 192
    label "zezwolenie"
  ]
  node [
    id 193
    label "chart"
  ]
  node [
    id 194
    label "restauracja"
  ]
  node [
    id 195
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 196
    label "urz&#261;dzenie"
  ]
  node [
    id 197
    label "menu"
  ]
  node [
    id 198
    label "kartka"
  ]
  node [
    id 199
    label "agitation"
  ]
  node [
    id 200
    label "poruszenie"
  ]
  node [
    id 201
    label "excitation"
  ]
  node [
    id 202
    label "podniecenie_si&#281;"
  ]
  node [
    id 203
    label "nastr&#243;j"
  ]
  node [
    id 204
    label "asymilowa&#263;"
  ]
  node [
    id 205
    label "nasada"
  ]
  node [
    id 206
    label "profanum"
  ]
  node [
    id 207
    label "wz&#243;r"
  ]
  node [
    id 208
    label "senior"
  ]
  node [
    id 209
    label "asymilowanie"
  ]
  node [
    id 210
    label "os&#322;abia&#263;"
  ]
  node [
    id 211
    label "homo_sapiens"
  ]
  node [
    id 212
    label "osoba"
  ]
  node [
    id 213
    label "ludzko&#347;&#263;"
  ]
  node [
    id 214
    label "Adam"
  ]
  node [
    id 215
    label "hominid"
  ]
  node [
    id 216
    label "posta&#263;"
  ]
  node [
    id 217
    label "portrecista"
  ]
  node [
    id 218
    label "polifag"
  ]
  node [
    id 219
    label "podw&#322;adny"
  ]
  node [
    id 220
    label "dwun&#243;g"
  ]
  node [
    id 221
    label "wapniak"
  ]
  node [
    id 222
    label "duch"
  ]
  node [
    id 223
    label "os&#322;abianie"
  ]
  node [
    id 224
    label "antropochoria"
  ]
  node [
    id 225
    label "figura"
  ]
  node [
    id 226
    label "oddzia&#322;ywanie"
  ]
  node [
    id 227
    label "obiekt_matematyczny"
  ]
  node [
    id 228
    label "stopie&#324;_pisma"
  ]
  node [
    id 229
    label "pozycja"
  ]
  node [
    id 230
    label "problemat"
  ]
  node [
    id 231
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 232
    label "point"
  ]
  node [
    id 233
    label "plamka"
  ]
  node [
    id 234
    label "przestrze&#324;"
  ]
  node [
    id 235
    label "mark"
  ]
  node [
    id 236
    label "ust&#281;p"
  ]
  node [
    id 237
    label "po&#322;o&#380;enie"
  ]
  node [
    id 238
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 239
    label "miejsce"
  ]
  node [
    id 240
    label "kres"
  ]
  node [
    id 241
    label "plan"
  ]
  node [
    id 242
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 243
    label "chwila"
  ]
  node [
    id 244
    label "podpunkt"
  ]
  node [
    id 245
    label "jednostka"
  ]
  node [
    id 246
    label "sprawa"
  ]
  node [
    id 247
    label "problematyka"
  ]
  node [
    id 248
    label "prosta"
  ]
  node [
    id 249
    label "wojsko"
  ]
  node [
    id 250
    label "zapunktowa&#263;"
  ]
  node [
    id 251
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 252
    label "nieoboj&#281;tno&#347;&#263;"
  ]
  node [
    id 253
    label "jako&#347;&#263;"
  ]
  node [
    id 254
    label "warto&#347;&#263;"
  ]
  node [
    id 255
    label "distortion"
  ]
  node [
    id 256
    label "contortion"
  ]
  node [
    id 257
    label "zmienianie"
  ]
  node [
    id 258
    label "zmienia&#263;"
  ]
  node [
    id 259
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 260
    label "corrupt"
  ]
  node [
    id 261
    label "przedmiot"
  ]
  node [
    id 262
    label "zbi&#243;r"
  ]
  node [
    id 263
    label "wydarzenie"
  ]
  node [
    id 264
    label "zjawisko"
  ]
  node [
    id 265
    label "group"
  ]
  node [
    id 266
    label "struktura"
  ]
  node [
    id 267
    label "band"
  ]
  node [
    id 268
    label "wi&#261;zanie_koordynacyjne"
  ]
  node [
    id 269
    label "ligand"
  ]
  node [
    id 270
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 271
    label "sum"
  ]
  node [
    id 272
    label "faza"
  ]
  node [
    id 273
    label "bycie"
  ]
  node [
    id 274
    label "ripple"
  ]
  node [
    id 275
    label "zabicie"
  ]
  node [
    id 276
    label "pracowanie"
  ]
  node [
    id 277
    label "throb"
  ]
  node [
    id 278
    label "proceed"
  ]
  node [
    id 279
    label "pracowa&#263;"
  ]
  node [
    id 280
    label "wzbiera&#263;"
  ]
  node [
    id 281
    label "k&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 282
    label "riot"
  ]
  node [
    id 283
    label "badanie"
  ]
  node [
    id 284
    label "spoczynkowy"
  ]
  node [
    id 285
    label "cardiography"
  ]
  node [
    id 286
    label "core"
  ]
  node [
    id 287
    label "kolor"
  ]
  node [
    id 288
    label "erotyka"
  ]
  node [
    id 289
    label "love"
  ]
  node [
    id 290
    label "podniecanie"
  ]
  node [
    id 291
    label "po&#380;ycie"
  ]
  node [
    id 292
    label "ukochanie"
  ]
  node [
    id 293
    label "baraszki"
  ]
  node [
    id 294
    label "numer"
  ]
  node [
    id 295
    label "ruch_frykcyjny"
  ]
  node [
    id 296
    label "tendency"
  ]
  node [
    id 297
    label "wzw&#243;d"
  ]
  node [
    id 298
    label "wi&#281;&#378;"
  ]
  node [
    id 299
    label "czynno&#347;&#263;"
  ]
  node [
    id 300
    label "seks"
  ]
  node [
    id 301
    label "pozycja_misjonarska"
  ]
  node [
    id 302
    label "rozmna&#380;anie"
  ]
  node [
    id 303
    label "feblik"
  ]
  node [
    id 304
    label "z&#322;&#261;czenie"
  ]
  node [
    id 305
    label "imisja"
  ]
  node [
    id 306
    label "podniecenie"
  ]
  node [
    id 307
    label "podnieca&#263;"
  ]
  node [
    id 308
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 309
    label "zakochanie"
  ]
  node [
    id 310
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 311
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 312
    label "gra_wst&#281;pna"
  ]
  node [
    id 313
    label "drogi"
  ]
  node [
    id 314
    label "po&#380;&#261;danie"
  ]
  node [
    id 315
    label "podnieci&#263;"
  ]
  node [
    id 316
    label "afekt"
  ]
  node [
    id 317
    label "droga"
  ]
  node [
    id 318
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 319
    label "na_pieska"
  ]
  node [
    id 320
    label "pupa"
  ]
  node [
    id 321
    label "odwaga"
  ]
  node [
    id 322
    label "mind"
  ]
  node [
    id 323
    label "lina"
  ]
  node [
    id 324
    label "sztuka"
  ]
  node [
    id 325
    label "pi&#243;ro"
  ]
  node [
    id 326
    label "marrow"
  ]
  node [
    id 327
    label "rdze&#324;"
  ]
  node [
    id 328
    label "sztabka"
  ]
  node [
    id 329
    label "byt"
  ]
  node [
    id 330
    label "motor"
  ]
  node [
    id 331
    label "piek&#322;o"
  ]
  node [
    id 332
    label "instrument_smyczkowy"
  ]
  node [
    id 333
    label "mi&#281;kisz"
  ]
  node [
    id 334
    label "klocek"
  ]
  node [
    id 335
    label "shape"
  ]
  node [
    id 336
    label "schody"
  ]
  node [
    id 337
    label "&#380;elazko"
  ]
  node [
    id 338
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 339
    label "facjata"
  ]
  node [
    id 340
    label "twarz"
  ]
  node [
    id 341
    label "energia"
  ]
  node [
    id 342
    label "zapa&#322;"
  ]
  node [
    id 343
    label "carillon"
  ]
  node [
    id 344
    label "dzwonek_r&#281;czny"
  ]
  node [
    id 345
    label "sygnalizator"
  ]
  node [
    id 346
    label "dzwonnica"
  ]
  node [
    id 347
    label "ludwisarnia"
  ]
  node [
    id 348
    label "mentalno&#347;&#263;"
  ]
  node [
    id 349
    label "superego"
  ]
  node [
    id 350
    label "self"
  ]
  node [
    id 351
    label "wn&#281;trze"
  ]
  node [
    id 352
    label "podmiot"
  ]
  node [
    id 353
    label "wyj&#261;tkowy"
  ]
  node [
    id 354
    label "cewa_nerwowa"
  ]
  node [
    id 355
    label "zwierz&#281;"
  ]
  node [
    id 356
    label "oczko_Hessego"
  ]
  node [
    id 357
    label "chorda"
  ]
  node [
    id 358
    label "uk&#322;ad_pokarmowy"
  ]
  node [
    id 359
    label "strunowce"
  ]
  node [
    id 360
    label "gardziel"
  ]
  node [
    id 361
    label "ogon"
  ]
  node [
    id 362
    label "psychoanaliza"
  ]
  node [
    id 363
    label "poczucie_w&#322;asnej_warto&#347;ci"
  ]
  node [
    id 364
    label "Freud"
  ]
  node [
    id 365
    label "niewiedza"
  ]
  node [
    id 366
    label "stan"
  ]
  node [
    id 367
    label "_id"
  ]
  node [
    id 368
    label "ignorantness"
  ]
  node [
    id 369
    label "unconsciousness"
  ]
  node [
    id 370
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 371
    label "zamek"
  ]
  node [
    id 372
    label "mechanizm"
  ]
  node [
    id 373
    label "tama"
  ]
  node [
    id 374
    label "izba"
  ]
  node [
    id 375
    label "wyrobisko"
  ]
  node [
    id 376
    label "jaskinia"
  ]
  node [
    id 377
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 378
    label "nora"
  ]
  node [
    id 379
    label "zal&#261;&#380;nia"
  ]
  node [
    id 380
    label "spi&#380;arnia"
  ]
  node [
    id 381
    label "pomieszczenie"
  ]
  node [
    id 382
    label "preview"
  ]
  node [
    id 383
    label "b&#322;&#281;dnik_kostny"
  ]
  node [
    id 384
    label "zapowied&#378;"
  ]
  node [
    id 385
    label "endocardium"
  ]
  node [
    id 386
    label "b&#322;ona"
  ]
  node [
    id 387
    label "jedzenie"
  ]
  node [
    id 388
    label "pi&#261;ta_&#263;wiartka"
  ]
  node [
    id 389
    label "towar"
  ]
  node [
    id 390
    label "mi&#281;so"
  ]
  node [
    id 391
    label "biorca"
  ]
  node [
    id 392
    label "operacja"
  ]
  node [
    id 393
    label "przyjmowa&#263;_si&#281;"
  ]
  node [
    id 394
    label "przyj&#261;&#263;_si&#281;"
  ]
  node [
    id 395
    label "przyjmowanie_si&#281;"
  ]
  node [
    id 396
    label "dawca"
  ]
  node [
    id 397
    label "graft"
  ]
  node [
    id 398
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 399
    label "proces_my&#347;lowy"
  ]
  node [
    id 400
    label "liczenie"
  ]
  node [
    id 401
    label "laparotomia"
  ]
  node [
    id 402
    label "czyn"
  ]
  node [
    id 403
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 404
    label "liczy&#263;"
  ]
  node [
    id 405
    label "matematyka"
  ]
  node [
    id 406
    label "funkcja"
  ]
  node [
    id 407
    label "rzut"
  ]
  node [
    id 408
    label "zabieg"
  ]
  node [
    id 409
    label "mathematical_process"
  ]
  node [
    id 410
    label "strategia"
  ]
  node [
    id 411
    label "szew"
  ]
  node [
    id 412
    label "torakotomia"
  ]
  node [
    id 413
    label "supremum"
  ]
  node [
    id 414
    label "chirurg"
  ]
  node [
    id 415
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 416
    label "infimum"
  ]
  node [
    id 417
    label "wapnie&#263;"
  ]
  node [
    id 418
    label "odwarstwia&#263;"
  ]
  node [
    id 419
    label "odwarstwia&#263;_si&#281;"
  ]
  node [
    id 420
    label "serowacenie"
  ]
  node [
    id 421
    label "trofika"
  ]
  node [
    id 422
    label "kom&#243;rka"
  ]
  node [
    id 423
    label "serowacie&#263;"
  ]
  node [
    id 424
    label "zserowacenie"
  ]
  node [
    id 425
    label "badanie_histopatologiczne"
  ]
  node [
    id 426
    label "element_anatomiczny"
  ]
  node [
    id 427
    label "tissue"
  ]
  node [
    id 428
    label "odwarstwi&#263;"
  ]
  node [
    id 429
    label "histochemia"
  ]
  node [
    id 430
    label "zserowacie&#263;"
  ]
  node [
    id 431
    label "oddychanie_tkankowe"
  ]
  node [
    id 432
    label "odwarstwi&#263;_si&#281;"
  ]
  node [
    id 433
    label "wapnienie"
  ]
  node [
    id 434
    label "pacjent"
  ]
  node [
    id 435
    label "dostrzec"
  ]
  node [
    id 436
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 437
    label "spot"
  ]
  node [
    id 438
    label "go_steady"
  ]
  node [
    id 439
    label "obejrze&#263;"
  ]
  node [
    id 440
    label "spotka&#263;"
  ]
  node [
    id 441
    label "znale&#378;&#263;"
  ]
  node [
    id 442
    label "postrzec"
  ]
  node [
    id 443
    label "pojrze&#263;"
  ]
  node [
    id 444
    label "see"
  ]
  node [
    id 445
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 446
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 447
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 448
    label "popatrze&#263;"
  ]
  node [
    id 449
    label "cognizance"
  ]
  node [
    id 450
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 451
    label "spoziera&#263;"
  ]
  node [
    id 452
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 453
    label "peek"
  ]
  node [
    id 454
    label "zinterpretowa&#263;"
  ]
  node [
    id 455
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 456
    label "think"
  ]
  node [
    id 457
    label "oceni&#263;"
  ]
  node [
    id 458
    label "illustrate"
  ]
  node [
    id 459
    label "zanalizowa&#263;"
  ]
  node [
    id 460
    label "zagra&#263;"
  ]
  node [
    id 461
    label "read"
  ]
  node [
    id 462
    label "visualize"
  ]
  node [
    id 463
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 464
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 465
    label "sta&#263;_si&#281;"
  ]
  node [
    id 466
    label "zrobi&#263;"
  ]
  node [
    id 467
    label "respect"
  ]
  node [
    id 468
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 469
    label "notice"
  ]
  node [
    id 470
    label "spowodowa&#263;"
  ]
  node [
    id 471
    label "pozna&#263;"
  ]
  node [
    id 472
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 473
    label "insert"
  ]
  node [
    id 474
    label "befall"
  ]
  node [
    id 475
    label "wymy&#347;li&#263;"
  ]
  node [
    id 476
    label "znaj&#347;&#263;"
  ]
  node [
    id 477
    label "odzyska&#263;"
  ]
  node [
    id 478
    label "pozyska&#263;"
  ]
  node [
    id 479
    label "dozna&#263;"
  ]
  node [
    id 480
    label "wykry&#263;"
  ]
  node [
    id 481
    label "devise"
  ]
  node [
    id 482
    label "invent"
  ]
  node [
    id 483
    label "discover"
  ]
  node [
    id 484
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 485
    label "radio"
  ]
  node [
    id 486
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 487
    label "lampa"
  ]
  node [
    id 488
    label "fotografia"
  ]
  node [
    id 489
    label "transakcja"
  ]
  node [
    id 490
    label "ekspozycja"
  ]
  node [
    id 491
    label "film"
  ]
  node [
    id 492
    label "reklama"
  ]
  node [
    id 493
    label "booklet"
  ]
  node [
    id 494
    label "pomiar"
  ]
  node [
    id 495
    label "spojrze&#263;"
  ]
  node [
    id 496
    label "spogl&#261;da&#263;"
  ]
  node [
    id 497
    label "chronometria"
  ]
  node [
    id 498
    label "odczyt"
  ]
  node [
    id 499
    label "laba"
  ]
  node [
    id 500
    label "czasoprzestrze&#324;"
  ]
  node [
    id 501
    label "time_period"
  ]
  node [
    id 502
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 503
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 504
    label "Zeitgeist"
  ]
  node [
    id 505
    label "pochodzenie"
  ]
  node [
    id 506
    label "przep&#322;ywanie"
  ]
  node [
    id 507
    label "schy&#322;ek"
  ]
  node [
    id 508
    label "czwarty_wymiar"
  ]
  node [
    id 509
    label "kategoria_gramatyczna"
  ]
  node [
    id 510
    label "poprzedzi&#263;"
  ]
  node [
    id 511
    label "pogoda"
  ]
  node [
    id 512
    label "czasokres"
  ]
  node [
    id 513
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 514
    label "poprzedzenie"
  ]
  node [
    id 515
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 516
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 517
    label "dzieje"
  ]
  node [
    id 518
    label "zegar"
  ]
  node [
    id 519
    label "koniugacja"
  ]
  node [
    id 520
    label "trawi&#263;"
  ]
  node [
    id 521
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 522
    label "poprzedza&#263;"
  ]
  node [
    id 523
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 524
    label "trawienie"
  ]
  node [
    id 525
    label "rachuba_czasu"
  ]
  node [
    id 526
    label "poprzedzanie"
  ]
  node [
    id 527
    label "okres_czasu"
  ]
  node [
    id 528
    label "period"
  ]
  node [
    id 529
    label "odwlekanie_si&#281;"
  ]
  node [
    id 530
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 531
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 532
    label "pochodzi&#263;"
  ]
  node [
    id 533
    label "time"
  ]
  node [
    id 534
    label "blok"
  ]
  node [
    id 535
    label "reading"
  ]
  node [
    id 536
    label "handout"
  ]
  node [
    id 537
    label "podawanie"
  ]
  node [
    id 538
    label "wyk&#322;ad"
  ]
  node [
    id 539
    label "lecture"
  ]
  node [
    id 540
    label "meteorology"
  ]
  node [
    id 541
    label "warunki"
  ]
  node [
    id 542
    label "weather"
  ]
  node [
    id 543
    label "pok&#243;j"
  ]
  node [
    id 544
    label "atak"
  ]
  node [
    id 545
    label "prognoza_meteorologiczna"
  ]
  node [
    id 546
    label "potrzyma&#263;"
  ]
  node [
    id 547
    label "program"
  ]
  node [
    id 548
    label "czas_wolny"
  ]
  node [
    id 549
    label "metrologia"
  ]
  node [
    id 550
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 551
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 552
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 553
    label "czasomierz"
  ]
  node [
    id 554
    label "tyka&#263;"
  ]
  node [
    id 555
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 556
    label "tykn&#261;&#263;"
  ]
  node [
    id 557
    label "nabicie"
  ]
  node [
    id 558
    label "bicie"
  ]
  node [
    id 559
    label "kotwica"
  ]
  node [
    id 560
    label "godzinnik"
  ]
  node [
    id 561
    label "werk"
  ]
  node [
    id 562
    label "wahad&#322;o"
  ]
  node [
    id 563
    label "kurant"
  ]
  node [
    id 564
    label "cyferblat"
  ]
  node [
    id 565
    label "liczba"
  ]
  node [
    id 566
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 567
    label "czasownik"
  ]
  node [
    id 568
    label "tryb"
  ]
  node [
    id 569
    label "coupling"
  ]
  node [
    id 570
    label "fleksja"
  ]
  node [
    id 571
    label "orz&#281;sek"
  ]
  node [
    id 572
    label "lutowa&#263;"
  ]
  node [
    id 573
    label "metal"
  ]
  node [
    id 574
    label "przetrawia&#263;"
  ]
  node [
    id 575
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 576
    label "poch&#322;ania&#263;"
  ]
  node [
    id 577
    label "digest"
  ]
  node [
    id 578
    label "usuwa&#263;"
  ]
  node [
    id 579
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 580
    label "sp&#281;dza&#263;"
  ]
  node [
    id 581
    label "marnowa&#263;"
  ]
  node [
    id 582
    label "marnowanie"
  ]
  node [
    id 583
    label "unicestwianie"
  ]
  node [
    id 584
    label "sp&#281;dzanie"
  ]
  node [
    id 585
    label "digestion"
  ]
  node [
    id 586
    label "perystaltyka"
  ]
  node [
    id 587
    label "proces_fizjologiczny"
  ]
  node [
    id 588
    label "rozk&#322;adanie"
  ]
  node [
    id 589
    label "przetrawianie"
  ]
  node [
    id 590
    label "contemplation"
  ]
  node [
    id 591
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 592
    label "background"
  ]
  node [
    id 593
    label "str&#243;j"
  ]
  node [
    id 594
    label "wynikanie"
  ]
  node [
    id 595
    label "origin"
  ]
  node [
    id 596
    label "zaczynanie_si&#281;"
  ]
  node [
    id 597
    label "beginning"
  ]
  node [
    id 598
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 599
    label "geneza"
  ]
  node [
    id 600
    label "cross"
  ]
  node [
    id 601
    label "swimming"
  ]
  node [
    id 602
    label "min&#261;&#263;"
  ]
  node [
    id 603
    label "przeby&#263;"
  ]
  node [
    id 604
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 605
    label "zago&#347;ci&#263;"
  ]
  node [
    id 606
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 607
    label "pour"
  ]
  node [
    id 608
    label "mija&#263;"
  ]
  node [
    id 609
    label "sail"
  ]
  node [
    id 610
    label "przebywa&#263;"
  ]
  node [
    id 611
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 612
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 613
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 614
    label "go&#347;ci&#263;"
  ]
  node [
    id 615
    label "przebycie"
  ]
  node [
    id 616
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 617
    label "mini&#281;cie"
  ]
  node [
    id 618
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 619
    label "zaistnienie"
  ]
  node [
    id 620
    label "doznanie"
  ]
  node [
    id 621
    label "cruise"
  ]
  node [
    id 622
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 623
    label "zjawianie_si&#281;"
  ]
  node [
    id 624
    label "mijanie"
  ]
  node [
    id 625
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 626
    label "przebywanie"
  ]
  node [
    id 627
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 628
    label "flux"
  ]
  node [
    id 629
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 630
    label "zaznawanie"
  ]
  node [
    id 631
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 632
    label "overwhelm"
  ]
  node [
    id 633
    label "opatrzy&#263;"
  ]
  node [
    id 634
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 635
    label "opatrywanie"
  ]
  node [
    id 636
    label "zanikni&#281;cie"
  ]
  node [
    id 637
    label "departure"
  ]
  node [
    id 638
    label "odej&#347;cie"
  ]
  node [
    id 639
    label "opuszczenie"
  ]
  node [
    id 640
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 641
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 642
    label "ciecz"
  ]
  node [
    id 643
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 644
    label "oddalenie_si&#281;"
  ]
  node [
    id 645
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 646
    label "poby&#263;"
  ]
  node [
    id 647
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 648
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 649
    label "bolt"
  ]
  node [
    id 650
    label "uda&#263;_si&#281;"
  ]
  node [
    id 651
    label "date"
  ]
  node [
    id 652
    label "fall"
  ]
  node [
    id 653
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 654
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 655
    label "wynika&#263;"
  ]
  node [
    id 656
    label "zdarzenie_si&#281;"
  ]
  node [
    id 657
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 658
    label "progress"
  ]
  node [
    id 659
    label "opatrzenie"
  ]
  node [
    id 660
    label "opatrywa&#263;"
  ]
  node [
    id 661
    label "epoka"
  ]
  node [
    id 662
    label "ciota"
  ]
  node [
    id 663
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 664
    label "flow"
  ]
  node [
    id 665
    label "choroba_przyrodzona"
  ]
  node [
    id 666
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 667
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 668
    label "transportowiec"
  ]
  node [
    id 669
    label "statek_handlowy"
  ]
  node [
    id 670
    label "pracownik"
  ]
  node [
    id 671
    label "okr&#281;t_nawodny"
  ]
  node [
    id 672
    label "bran&#380;owiec"
  ]
  node [
    id 673
    label "znak_informacyjny"
  ]
  node [
    id 674
    label "autobus"
  ]
  node [
    id 675
    label "buspas"
  ]
  node [
    id 676
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 677
    label "pas_ruchu"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 677
  ]
]
