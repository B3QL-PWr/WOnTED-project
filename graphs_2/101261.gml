graph [
  node [
    id 0
    label "symbol"
    origin "text"
  ]
  node [
    id 1
    label "kanada"
    origin "text"
  ]
  node [
    id 2
    label "znak_pisarski"
  ]
  node [
    id 3
    label "znak"
  ]
  node [
    id 4
    label "notacja"
  ]
  node [
    id 5
    label "wcielenie"
  ]
  node [
    id 6
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 7
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 8
    label "character"
  ]
  node [
    id 9
    label "symbolizowanie"
  ]
  node [
    id 10
    label "model"
  ]
  node [
    id 11
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 12
    label "involvement"
  ]
  node [
    id 13
    label "wydarzenie"
  ]
  node [
    id 14
    label "pasywa"
  ]
  node [
    id 15
    label "aktywa"
  ]
  node [
    id 16
    label "posta&#263;"
  ]
  node [
    id 17
    label "fuzja"
  ]
  node [
    id 18
    label "osoba"
  ]
  node [
    id 19
    label "podmiot_gospodarczy"
  ]
  node [
    id 20
    label "reinkarnacja"
  ]
  node [
    id 21
    label "imposture"
  ]
  node [
    id 22
    label "zrobienie"
  ]
  node [
    id 23
    label "dow&#243;d"
  ]
  node [
    id 24
    label "oznakowanie"
  ]
  node [
    id 25
    label "fakt"
  ]
  node [
    id 26
    label "stawia&#263;"
  ]
  node [
    id 27
    label "wytw&#243;r"
  ]
  node [
    id 28
    label "point"
  ]
  node [
    id 29
    label "kodzik"
  ]
  node [
    id 30
    label "postawi&#263;"
  ]
  node [
    id 31
    label "mark"
  ]
  node [
    id 32
    label "herb"
  ]
  node [
    id 33
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 34
    label "attribute"
  ]
  node [
    id 35
    label "implikowa&#263;"
  ]
  node [
    id 36
    label "znaczenie"
  ]
  node [
    id 37
    label "j&#281;zyk"
  ]
  node [
    id 38
    label "zapis"
  ]
  node [
    id 39
    label "notation"
  ]
  node [
    id 40
    label "miejsce"
  ]
  node [
    id 41
    label "warunek_lokalowy"
  ]
  node [
    id 42
    label "plac"
  ]
  node [
    id 43
    label "location"
  ]
  node [
    id 44
    label "uwaga"
  ]
  node [
    id 45
    label "przestrze&#324;"
  ]
  node [
    id 46
    label "status"
  ]
  node [
    id 47
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 48
    label "chwila"
  ]
  node [
    id 49
    label "cia&#322;o"
  ]
  node [
    id 50
    label "cecha"
  ]
  node [
    id 51
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 52
    label "praca"
  ]
  node [
    id 53
    label "rz&#261;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
]
