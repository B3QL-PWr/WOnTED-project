graph [
  node [
    id 0
    label "rosyjski"
    origin "text"
  ]
  node [
    id 1
    label "wszystko"
    origin "text"
  ]
  node [
    id 2
    label "wida&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wiadomo"
    origin "text"
  ]
  node [
    id 4
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 5
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 6
    label "kacapski"
  ]
  node [
    id 7
    label "po_rosyjsku"
  ]
  node [
    id 8
    label "wielkoruski"
  ]
  node [
    id 9
    label "Russian"
  ]
  node [
    id 10
    label "j&#281;zyk"
  ]
  node [
    id 11
    label "rusek"
  ]
  node [
    id 12
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 13
    label "artykulator"
  ]
  node [
    id 14
    label "kod"
  ]
  node [
    id 15
    label "kawa&#322;ek"
  ]
  node [
    id 16
    label "przedmiot"
  ]
  node [
    id 17
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 18
    label "gramatyka"
  ]
  node [
    id 19
    label "stylik"
  ]
  node [
    id 20
    label "przet&#322;umaczenie"
  ]
  node [
    id 21
    label "formalizowanie"
  ]
  node [
    id 22
    label "ssa&#263;"
  ]
  node [
    id 23
    label "ssanie"
  ]
  node [
    id 24
    label "language"
  ]
  node [
    id 25
    label "liza&#263;"
  ]
  node [
    id 26
    label "napisa&#263;"
  ]
  node [
    id 27
    label "konsonantyzm"
  ]
  node [
    id 28
    label "wokalizm"
  ]
  node [
    id 29
    label "pisa&#263;"
  ]
  node [
    id 30
    label "fonetyka"
  ]
  node [
    id 31
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 32
    label "jeniec"
  ]
  node [
    id 33
    label "but"
  ]
  node [
    id 34
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 35
    label "po_koroniarsku"
  ]
  node [
    id 36
    label "kultura_duchowa"
  ]
  node [
    id 37
    label "t&#322;umaczenie"
  ]
  node [
    id 38
    label "m&#243;wienie"
  ]
  node [
    id 39
    label "pype&#263;"
  ]
  node [
    id 40
    label "lizanie"
  ]
  node [
    id 41
    label "pismo"
  ]
  node [
    id 42
    label "formalizowa&#263;"
  ]
  node [
    id 43
    label "rozumie&#263;"
  ]
  node [
    id 44
    label "organ"
  ]
  node [
    id 45
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 46
    label "rozumienie"
  ]
  node [
    id 47
    label "spos&#243;b"
  ]
  node [
    id 48
    label "makroglosja"
  ]
  node [
    id 49
    label "m&#243;wi&#263;"
  ]
  node [
    id 50
    label "jama_ustna"
  ]
  node [
    id 51
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 52
    label "formacja_geologiczna"
  ]
  node [
    id 53
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 54
    label "natural_language"
  ]
  node [
    id 55
    label "s&#322;ownictwo"
  ]
  node [
    id 56
    label "urz&#261;dzenie"
  ]
  node [
    id 57
    label "wschodnioeuropejski"
  ]
  node [
    id 58
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 59
    label "poga&#324;ski"
  ]
  node [
    id 60
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 61
    label "topielec"
  ]
  node [
    id 62
    label "europejski"
  ]
  node [
    id 63
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 64
    label "po_kacapsku"
  ]
  node [
    id 65
    label "ruski"
  ]
  node [
    id 66
    label "imperialny"
  ]
  node [
    id 67
    label "po_wielkorusku"
  ]
  node [
    id 68
    label "lock"
  ]
  node [
    id 69
    label "absolut"
  ]
  node [
    id 70
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 71
    label "integer"
  ]
  node [
    id 72
    label "liczba"
  ]
  node [
    id 73
    label "zlewanie_si&#281;"
  ]
  node [
    id 74
    label "ilo&#347;&#263;"
  ]
  node [
    id 75
    label "uk&#322;ad"
  ]
  node [
    id 76
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 77
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 78
    label "pe&#322;ny"
  ]
  node [
    id 79
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 80
    label "olejek_eteryczny"
  ]
  node [
    id 81
    label "byt"
  ]
  node [
    id 82
    label "by&#263;"
  ]
  node [
    id 83
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 84
    label "mie&#263;_miejsce"
  ]
  node [
    id 85
    label "equal"
  ]
  node [
    id 86
    label "trwa&#263;"
  ]
  node [
    id 87
    label "chodzi&#263;"
  ]
  node [
    id 88
    label "si&#281;ga&#263;"
  ]
  node [
    id 89
    label "stan"
  ]
  node [
    id 90
    label "obecno&#347;&#263;"
  ]
  node [
    id 91
    label "stand"
  ]
  node [
    id 92
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 93
    label "uczestniczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
]
