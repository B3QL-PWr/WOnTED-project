graph [
  node [
    id 0
    label "samotno&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "samo"
    origin "text"
  ]
  node [
    id 2
    label "introwersja"
    origin "text"
  ]
  node [
    id 3
    label "l&#281;k"
    origin "text"
  ]
  node [
    id 4
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 5
    label "isolation"
  ]
  node [
    id 6
    label "izolacja"
  ]
  node [
    id 7
    label "samota"
  ]
  node [
    id 8
    label "wra&#380;enie"
  ]
  node [
    id 9
    label "przeczulica"
  ]
  node [
    id 10
    label "czucie"
  ]
  node [
    id 11
    label "proces"
  ]
  node [
    id 12
    label "zmys&#322;"
  ]
  node [
    id 13
    label "zjawisko"
  ]
  node [
    id 14
    label "poczucie"
  ]
  node [
    id 15
    label "odczucia"
  ]
  node [
    id 16
    label "reakcja"
  ]
  node [
    id 17
    label "stan"
  ]
  node [
    id 18
    label "ochrona"
  ]
  node [
    id 19
    label "utrzymywanie"
  ]
  node [
    id 20
    label "separation"
  ]
  node [
    id 21
    label "insulation"
  ]
  node [
    id 22
    label "insulant"
  ]
  node [
    id 23
    label "l&#281;k_separacyjny"
  ]
  node [
    id 24
    label "samotnia"
  ]
  node [
    id 25
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 26
    label "phobia"
  ]
  node [
    id 27
    label "emocja"
  ]
  node [
    id 28
    label "akatyzja"
  ]
  node [
    id 29
    label "zastraszanie"
  ]
  node [
    id 30
    label "zastraszenie"
  ]
  node [
    id 31
    label "ba&#263;_si&#281;"
  ]
  node [
    id 32
    label "zesp&#243;&#322;_chorobowy"
  ]
  node [
    id 33
    label "stygn&#261;&#263;"
  ]
  node [
    id 34
    label "wpada&#263;"
  ]
  node [
    id 35
    label "wpa&#347;&#263;"
  ]
  node [
    id 36
    label "d&#322;awi&#263;"
  ]
  node [
    id 37
    label "iskrzy&#263;"
  ]
  node [
    id 38
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 39
    label "afekt"
  ]
  node [
    id 40
    label "ostygn&#261;&#263;"
  ]
  node [
    id 41
    label "temperatura"
  ]
  node [
    id 42
    label "ogrom"
  ]
  node [
    id 43
    label "bullying"
  ]
  node [
    id 44
    label "oddzia&#322;ywanie"
  ]
  node [
    id 45
    label "presja"
  ]
  node [
    id 46
    label "oddzia&#322;anie"
  ]
  node [
    id 47
    label "publiczny"
  ]
  node [
    id 48
    label "niepubliczny"
  ]
  node [
    id 49
    label "spo&#322;ecznie"
  ]
  node [
    id 50
    label "publicznie"
  ]
  node [
    id 51
    label "upublicznienie"
  ]
  node [
    id 52
    label "upublicznianie"
  ]
  node [
    id 53
    label "jawny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
]
