graph [
  node [
    id 0
    label "rad"
    origin "text"
  ]
  node [
    id 1
    label "vasile"
    origin "text"
  ]
  node [
    id 2
    label "berylowiec"
  ]
  node [
    id 3
    label "jednostka"
  ]
  node [
    id 4
    label "content"
  ]
  node [
    id 5
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 6
    label "jednostka_promieniowania"
  ]
  node [
    id 7
    label "zadowolenie_si&#281;"
  ]
  node [
    id 8
    label "miliradian"
  ]
  node [
    id 9
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 10
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 11
    label "mikroradian"
  ]
  node [
    id 12
    label "przyswoi&#263;"
  ]
  node [
    id 13
    label "ludzko&#347;&#263;"
  ]
  node [
    id 14
    label "one"
  ]
  node [
    id 15
    label "poj&#281;cie"
  ]
  node [
    id 16
    label "ewoluowanie"
  ]
  node [
    id 17
    label "supremum"
  ]
  node [
    id 18
    label "skala"
  ]
  node [
    id 19
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 20
    label "przyswajanie"
  ]
  node [
    id 21
    label "wyewoluowanie"
  ]
  node [
    id 22
    label "reakcja"
  ]
  node [
    id 23
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 24
    label "przeliczy&#263;"
  ]
  node [
    id 25
    label "wyewoluowa&#263;"
  ]
  node [
    id 26
    label "ewoluowa&#263;"
  ]
  node [
    id 27
    label "matematyka"
  ]
  node [
    id 28
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 29
    label "rzut"
  ]
  node [
    id 30
    label "liczba_naturalna"
  ]
  node [
    id 31
    label "czynnik_biotyczny"
  ]
  node [
    id 32
    label "g&#322;owa"
  ]
  node [
    id 33
    label "figura"
  ]
  node [
    id 34
    label "individual"
  ]
  node [
    id 35
    label "portrecista"
  ]
  node [
    id 36
    label "obiekt"
  ]
  node [
    id 37
    label "przyswaja&#263;"
  ]
  node [
    id 38
    label "przyswojenie"
  ]
  node [
    id 39
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 40
    label "profanum"
  ]
  node [
    id 41
    label "mikrokosmos"
  ]
  node [
    id 42
    label "starzenie_si&#281;"
  ]
  node [
    id 43
    label "duch"
  ]
  node [
    id 44
    label "przeliczanie"
  ]
  node [
    id 45
    label "osoba"
  ]
  node [
    id 46
    label "oddzia&#322;ywanie"
  ]
  node [
    id 47
    label "antropochoria"
  ]
  node [
    id 48
    label "funkcja"
  ]
  node [
    id 49
    label "homo_sapiens"
  ]
  node [
    id 50
    label "przelicza&#263;"
  ]
  node [
    id 51
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 52
    label "infimum"
  ]
  node [
    id 53
    label "przeliczenie"
  ]
  node [
    id 54
    label "metal"
  ]
  node [
    id 55
    label "nanoradian"
  ]
  node [
    id 56
    label "radian"
  ]
  node [
    id 57
    label "zadowolony"
  ]
  node [
    id 58
    label "weso&#322;y"
  ]
  node [
    id 59
    label "Vasile"
  ]
  node [
    id 60
    label "Mischiu"
  ]
  node [
    id 61
    label "narodowy"
  ]
  node [
    id 62
    label "partia"
  ]
  node [
    id 63
    label "ch&#322;op"
  ]
  node [
    id 64
    label "demokratyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
]
