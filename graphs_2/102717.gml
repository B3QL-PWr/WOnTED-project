graph [
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "dobra"
    origin "text"
  ]
  node [
    id 2
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "tak"
    origin "text"
  ]
  node [
    id 5
    label "jeszcze"
    origin "text"
  ]
  node [
    id 6
    label "interesowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "cholera"
    origin "text"
  ]
  node [
    id 8
    label "tylko"
    origin "text"
  ]
  node [
    id 9
    label "czy"
    origin "text"
  ]
  node [
    id 10
    label "cokolwiek"
    origin "text"
  ]
  node [
    id 11
    label "tam"
    origin "text"
  ]
  node [
    id 12
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 13
    label "region"
    origin "text"
  ]
  node [
    id 14
    label "kobyla&#324;ski"
    origin "text"
  ]
  node [
    id 15
    label "korzenny"
    origin "text"
  ]
  node [
    id 16
    label "spiralna"
    origin "text"
  ]
  node [
    id 17
    label "drzewo_owocowe"
  ]
  node [
    id 18
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 19
    label "Wilko"
  ]
  node [
    id 20
    label "centym"
  ]
  node [
    id 21
    label "frymark"
  ]
  node [
    id 22
    label "mienie"
  ]
  node [
    id 23
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 24
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 26
    label "jednostka_monetarna"
  ]
  node [
    id 27
    label "commodity"
  ]
  node [
    id 28
    label "liczba"
  ]
  node [
    id 29
    label "uk&#322;ad"
  ]
  node [
    id 30
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 31
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 32
    label "integer"
  ]
  node [
    id 33
    label "zlewanie_si&#281;"
  ]
  node [
    id 34
    label "ilo&#347;&#263;"
  ]
  node [
    id 35
    label "pe&#322;ny"
  ]
  node [
    id 36
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 37
    label "rzecz"
  ]
  node [
    id 38
    label "stan"
  ]
  node [
    id 39
    label "immoblizacja"
  ]
  node [
    id 40
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 41
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 42
    label "possession"
  ]
  node [
    id 43
    label "rodowo&#347;&#263;"
  ]
  node [
    id 44
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 45
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 46
    label "patent"
  ]
  node [
    id 47
    label "przej&#347;&#263;"
  ]
  node [
    id 48
    label "przej&#347;cie"
  ]
  node [
    id 49
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 50
    label "maj&#261;tek"
  ]
  node [
    id 51
    label "zamiana"
  ]
  node [
    id 52
    label "Iwaszkiewicz"
  ]
  node [
    id 53
    label "cognizance"
  ]
  node [
    id 54
    label "murza"
  ]
  node [
    id 55
    label "belfer"
  ]
  node [
    id 56
    label "szkolnik"
  ]
  node [
    id 57
    label "pupil"
  ]
  node [
    id 58
    label "ojciec"
  ]
  node [
    id 59
    label "kszta&#322;ciciel"
  ]
  node [
    id 60
    label "Midas"
  ]
  node [
    id 61
    label "przyw&#243;dca"
  ]
  node [
    id 62
    label "opiekun"
  ]
  node [
    id 63
    label "Mieszko_I"
  ]
  node [
    id 64
    label "doros&#322;y"
  ]
  node [
    id 65
    label "pracodawca"
  ]
  node [
    id 66
    label "profesor"
  ]
  node [
    id 67
    label "m&#261;&#380;"
  ]
  node [
    id 68
    label "rz&#261;dzenie"
  ]
  node [
    id 69
    label "bogaty"
  ]
  node [
    id 70
    label "cz&#322;owiek"
  ]
  node [
    id 71
    label "pa&#324;stwo"
  ]
  node [
    id 72
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 73
    label "w&#322;odarz"
  ]
  node [
    id 74
    label "nabab"
  ]
  node [
    id 75
    label "samiec"
  ]
  node [
    id 76
    label "preceptor"
  ]
  node [
    id 77
    label "pedagog"
  ]
  node [
    id 78
    label "efendi"
  ]
  node [
    id 79
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 80
    label "popularyzator"
  ]
  node [
    id 81
    label "gra_w_karty"
  ]
  node [
    id 82
    label "zwrot"
  ]
  node [
    id 83
    label "jegomo&#347;&#263;"
  ]
  node [
    id 84
    label "androlog"
  ]
  node [
    id 85
    label "bratek"
  ]
  node [
    id 86
    label "andropauza"
  ]
  node [
    id 87
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 88
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 89
    label "ch&#322;opina"
  ]
  node [
    id 90
    label "w&#322;adza"
  ]
  node [
    id 91
    label "Sabataj_Cwi"
  ]
  node [
    id 92
    label "lider"
  ]
  node [
    id 93
    label "Mao"
  ]
  node [
    id 94
    label "Anders"
  ]
  node [
    id 95
    label "Fidel_Castro"
  ]
  node [
    id 96
    label "Miko&#322;ajczyk"
  ]
  node [
    id 97
    label "Tito"
  ]
  node [
    id 98
    label "Ko&#347;ciuszko"
  ]
  node [
    id 99
    label "zwierzchnik"
  ]
  node [
    id 100
    label "p&#322;atnik"
  ]
  node [
    id 101
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 102
    label "nadzorca"
  ]
  node [
    id 103
    label "funkcjonariusz"
  ]
  node [
    id 104
    label "podmiot"
  ]
  node [
    id 105
    label "wykupywanie"
  ]
  node [
    id 106
    label "wykupienie"
  ]
  node [
    id 107
    label "bycie_w_posiadaniu"
  ]
  node [
    id 108
    label "rozszerzyciel"
  ]
  node [
    id 109
    label "asymilowa&#263;"
  ]
  node [
    id 110
    label "nasada"
  ]
  node [
    id 111
    label "profanum"
  ]
  node [
    id 112
    label "wz&#243;r"
  ]
  node [
    id 113
    label "senior"
  ]
  node [
    id 114
    label "asymilowanie"
  ]
  node [
    id 115
    label "os&#322;abia&#263;"
  ]
  node [
    id 116
    label "homo_sapiens"
  ]
  node [
    id 117
    label "osoba"
  ]
  node [
    id 118
    label "ludzko&#347;&#263;"
  ]
  node [
    id 119
    label "Adam"
  ]
  node [
    id 120
    label "hominid"
  ]
  node [
    id 121
    label "posta&#263;"
  ]
  node [
    id 122
    label "portrecista"
  ]
  node [
    id 123
    label "polifag"
  ]
  node [
    id 124
    label "podw&#322;adny"
  ]
  node [
    id 125
    label "dwun&#243;g"
  ]
  node [
    id 126
    label "wapniak"
  ]
  node [
    id 127
    label "duch"
  ]
  node [
    id 128
    label "os&#322;abianie"
  ]
  node [
    id 129
    label "antropochoria"
  ]
  node [
    id 130
    label "figura"
  ]
  node [
    id 131
    label "g&#322;owa"
  ]
  node [
    id 132
    label "mikrokosmos"
  ]
  node [
    id 133
    label "oddzia&#322;ywanie"
  ]
  node [
    id 134
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 135
    label "du&#380;y"
  ]
  node [
    id 136
    label "dojrza&#322;y"
  ]
  node [
    id 137
    label "dojrzale"
  ]
  node [
    id 138
    label "doro&#347;lenie"
  ]
  node [
    id 139
    label "wydoro&#347;lenie"
  ]
  node [
    id 140
    label "m&#261;dry"
  ]
  node [
    id 141
    label "&#378;ra&#322;y"
  ]
  node [
    id 142
    label "doletni"
  ]
  node [
    id 143
    label "doro&#347;le"
  ]
  node [
    id 144
    label "punkt"
  ]
  node [
    id 145
    label "zmiana"
  ]
  node [
    id 146
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 147
    label "turn"
  ]
  node [
    id 148
    label "wyra&#380;enie"
  ]
  node [
    id 149
    label "fraza_czasownikowa"
  ]
  node [
    id 150
    label "turning"
  ]
  node [
    id 151
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 152
    label "skr&#281;t"
  ]
  node [
    id 153
    label "jednostka_leksykalna"
  ]
  node [
    id 154
    label "obr&#243;t"
  ]
  node [
    id 155
    label "starosta"
  ]
  node [
    id 156
    label "w&#322;adca"
  ]
  node [
    id 157
    label "zarz&#261;dca"
  ]
  node [
    id 158
    label "nauczyciel_akademicki"
  ]
  node [
    id 159
    label "tytu&#322;"
  ]
  node [
    id 160
    label "stopie&#324;_naukowy"
  ]
  node [
    id 161
    label "konsulent"
  ]
  node [
    id 162
    label "profesura"
  ]
  node [
    id 163
    label "nauczyciel"
  ]
  node [
    id 164
    label "wirtuoz"
  ]
  node [
    id 165
    label "autor"
  ]
  node [
    id 166
    label "szko&#322;a"
  ]
  node [
    id 167
    label "tarcza"
  ]
  node [
    id 168
    label "klasa"
  ]
  node [
    id 169
    label "elew"
  ]
  node [
    id 170
    label "mundurek"
  ]
  node [
    id 171
    label "absolwent"
  ]
  node [
    id 172
    label "wyprawka"
  ]
  node [
    id 173
    label "ochotnik"
  ]
  node [
    id 174
    label "nauczyciel_muzyki"
  ]
  node [
    id 175
    label "pomocnik"
  ]
  node [
    id 176
    label "zakonnik"
  ]
  node [
    id 177
    label "student"
  ]
  node [
    id 178
    label "ekspert"
  ]
  node [
    id 179
    label "bogacz"
  ]
  node [
    id 180
    label "dostojnik"
  ]
  node [
    id 181
    label "urz&#281;dnik"
  ]
  node [
    id 182
    label "&#347;w"
  ]
  node [
    id 183
    label "rodzic"
  ]
  node [
    id 184
    label "pomys&#322;odawca"
  ]
  node [
    id 185
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 186
    label "rodzice"
  ]
  node [
    id 187
    label "wykonawca"
  ]
  node [
    id 188
    label "stary"
  ]
  node [
    id 189
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 190
    label "kuwada"
  ]
  node [
    id 191
    label "ojczym"
  ]
  node [
    id 192
    label "papa"
  ]
  node [
    id 193
    label "przodek"
  ]
  node [
    id 194
    label "tworzyciel"
  ]
  node [
    id 195
    label "facet"
  ]
  node [
    id 196
    label "kochanek"
  ]
  node [
    id 197
    label "fio&#322;ek"
  ]
  node [
    id 198
    label "brat"
  ]
  node [
    id 199
    label "zwierz&#281;"
  ]
  node [
    id 200
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 201
    label "pan_i_w&#322;adca"
  ]
  node [
    id 202
    label "pan_m&#322;ody"
  ]
  node [
    id 203
    label "ch&#322;op"
  ]
  node [
    id 204
    label "&#347;lubny"
  ]
  node [
    id 205
    label "m&#243;j"
  ]
  node [
    id 206
    label "pan_domu"
  ]
  node [
    id 207
    label "ma&#322;&#380;onek"
  ]
  node [
    id 208
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 209
    label "mo&#347;&#263;"
  ]
  node [
    id 210
    label "Frygia"
  ]
  node [
    id 211
    label "dominowanie"
  ]
  node [
    id 212
    label "reign"
  ]
  node [
    id 213
    label "sprawowanie"
  ]
  node [
    id 214
    label "dominion"
  ]
  node [
    id 215
    label "rule"
  ]
  node [
    id 216
    label "zwierz&#281;_domowe"
  ]
  node [
    id 217
    label "John_Dewey"
  ]
  node [
    id 218
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 219
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 220
    label "J&#281;drzejewicz"
  ]
  node [
    id 221
    label "specjalista"
  ]
  node [
    id 222
    label "&#380;ycie"
  ]
  node [
    id 223
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 224
    label "Turek"
  ]
  node [
    id 225
    label "effendi"
  ]
  node [
    id 226
    label "och&#281;do&#380;ny"
  ]
  node [
    id 227
    label "zapa&#347;ny"
  ]
  node [
    id 228
    label "sytuowany"
  ]
  node [
    id 229
    label "obfituj&#261;cy"
  ]
  node [
    id 230
    label "forsiasty"
  ]
  node [
    id 231
    label "spania&#322;y"
  ]
  node [
    id 232
    label "obficie"
  ]
  node [
    id 233
    label "r&#243;&#380;norodny"
  ]
  node [
    id 234
    label "bogato"
  ]
  node [
    id 235
    label "Japonia"
  ]
  node [
    id 236
    label "Zair"
  ]
  node [
    id 237
    label "Belize"
  ]
  node [
    id 238
    label "San_Marino"
  ]
  node [
    id 239
    label "Tanzania"
  ]
  node [
    id 240
    label "Antigua_i_Barbuda"
  ]
  node [
    id 241
    label "granica_pa&#324;stwa"
  ]
  node [
    id 242
    label "Senegal"
  ]
  node [
    id 243
    label "Indie"
  ]
  node [
    id 244
    label "Seszele"
  ]
  node [
    id 245
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 246
    label "Zimbabwe"
  ]
  node [
    id 247
    label "Filipiny"
  ]
  node [
    id 248
    label "Mauretania"
  ]
  node [
    id 249
    label "Malezja"
  ]
  node [
    id 250
    label "Rumunia"
  ]
  node [
    id 251
    label "Surinam"
  ]
  node [
    id 252
    label "Ukraina"
  ]
  node [
    id 253
    label "Syria"
  ]
  node [
    id 254
    label "Wyspy_Marshalla"
  ]
  node [
    id 255
    label "Burkina_Faso"
  ]
  node [
    id 256
    label "Grecja"
  ]
  node [
    id 257
    label "Polska"
  ]
  node [
    id 258
    label "Wenezuela"
  ]
  node [
    id 259
    label "Nepal"
  ]
  node [
    id 260
    label "Suazi"
  ]
  node [
    id 261
    label "S&#322;owacja"
  ]
  node [
    id 262
    label "Algieria"
  ]
  node [
    id 263
    label "Chiny"
  ]
  node [
    id 264
    label "Grenada"
  ]
  node [
    id 265
    label "Barbados"
  ]
  node [
    id 266
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 267
    label "Pakistan"
  ]
  node [
    id 268
    label "Niemcy"
  ]
  node [
    id 269
    label "Bahrajn"
  ]
  node [
    id 270
    label "Komory"
  ]
  node [
    id 271
    label "Australia"
  ]
  node [
    id 272
    label "Rodezja"
  ]
  node [
    id 273
    label "Malawi"
  ]
  node [
    id 274
    label "Gwinea"
  ]
  node [
    id 275
    label "Wehrlen"
  ]
  node [
    id 276
    label "Meksyk"
  ]
  node [
    id 277
    label "Liechtenstein"
  ]
  node [
    id 278
    label "Czarnog&#243;ra"
  ]
  node [
    id 279
    label "Wielka_Brytania"
  ]
  node [
    id 280
    label "Kuwejt"
  ]
  node [
    id 281
    label "Angola"
  ]
  node [
    id 282
    label "Monako"
  ]
  node [
    id 283
    label "Jemen"
  ]
  node [
    id 284
    label "Etiopia"
  ]
  node [
    id 285
    label "Madagaskar"
  ]
  node [
    id 286
    label "terytorium"
  ]
  node [
    id 287
    label "Kolumbia"
  ]
  node [
    id 288
    label "Portoryko"
  ]
  node [
    id 289
    label "Mauritius"
  ]
  node [
    id 290
    label "Kostaryka"
  ]
  node [
    id 291
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 292
    label "Tajlandia"
  ]
  node [
    id 293
    label "Argentyna"
  ]
  node [
    id 294
    label "Zambia"
  ]
  node [
    id 295
    label "Sri_Lanka"
  ]
  node [
    id 296
    label "Gwatemala"
  ]
  node [
    id 297
    label "Kirgistan"
  ]
  node [
    id 298
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 299
    label "Hiszpania"
  ]
  node [
    id 300
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 301
    label "Salwador"
  ]
  node [
    id 302
    label "Korea"
  ]
  node [
    id 303
    label "Macedonia"
  ]
  node [
    id 304
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 305
    label "Brunei"
  ]
  node [
    id 306
    label "Mozambik"
  ]
  node [
    id 307
    label "Turcja"
  ]
  node [
    id 308
    label "Kambod&#380;a"
  ]
  node [
    id 309
    label "Benin"
  ]
  node [
    id 310
    label "Bhutan"
  ]
  node [
    id 311
    label "Tunezja"
  ]
  node [
    id 312
    label "Austria"
  ]
  node [
    id 313
    label "Izrael"
  ]
  node [
    id 314
    label "Sierra_Leone"
  ]
  node [
    id 315
    label "Jamajka"
  ]
  node [
    id 316
    label "Rosja"
  ]
  node [
    id 317
    label "Rwanda"
  ]
  node [
    id 318
    label "holoarktyka"
  ]
  node [
    id 319
    label "Nigeria"
  ]
  node [
    id 320
    label "USA"
  ]
  node [
    id 321
    label "Oman"
  ]
  node [
    id 322
    label "Luksemburg"
  ]
  node [
    id 323
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 324
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 325
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 326
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 327
    label "Dominikana"
  ]
  node [
    id 328
    label "Irlandia"
  ]
  node [
    id 329
    label "Liban"
  ]
  node [
    id 330
    label "Hanower"
  ]
  node [
    id 331
    label "Estonia"
  ]
  node [
    id 332
    label "Samoa"
  ]
  node [
    id 333
    label "Nowa_Zelandia"
  ]
  node [
    id 334
    label "Gabon"
  ]
  node [
    id 335
    label "Iran"
  ]
  node [
    id 336
    label "S&#322;owenia"
  ]
  node [
    id 337
    label "Egipt"
  ]
  node [
    id 338
    label "Kiribati"
  ]
  node [
    id 339
    label "Togo"
  ]
  node [
    id 340
    label "Mongolia"
  ]
  node [
    id 341
    label "Sudan"
  ]
  node [
    id 342
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 343
    label "Bahamy"
  ]
  node [
    id 344
    label "Bangladesz"
  ]
  node [
    id 345
    label "partia"
  ]
  node [
    id 346
    label "Serbia"
  ]
  node [
    id 347
    label "Czechy"
  ]
  node [
    id 348
    label "Holandia"
  ]
  node [
    id 349
    label "Birma"
  ]
  node [
    id 350
    label "Albania"
  ]
  node [
    id 351
    label "Mikronezja"
  ]
  node [
    id 352
    label "Gambia"
  ]
  node [
    id 353
    label "Kazachstan"
  ]
  node [
    id 354
    label "interior"
  ]
  node [
    id 355
    label "Uzbekistan"
  ]
  node [
    id 356
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 357
    label "Malta"
  ]
  node [
    id 358
    label "Lesoto"
  ]
  node [
    id 359
    label "para"
  ]
  node [
    id 360
    label "Antarktis"
  ]
  node [
    id 361
    label "Andora"
  ]
  node [
    id 362
    label "Nauru"
  ]
  node [
    id 363
    label "Kuba"
  ]
  node [
    id 364
    label "Wietnam"
  ]
  node [
    id 365
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 366
    label "ziemia"
  ]
  node [
    id 367
    label "Chorwacja"
  ]
  node [
    id 368
    label "Kamerun"
  ]
  node [
    id 369
    label "Urugwaj"
  ]
  node [
    id 370
    label "Niger"
  ]
  node [
    id 371
    label "Turkmenistan"
  ]
  node [
    id 372
    label "Szwajcaria"
  ]
  node [
    id 373
    label "organizacja"
  ]
  node [
    id 374
    label "grupa"
  ]
  node [
    id 375
    label "Litwa"
  ]
  node [
    id 376
    label "Palau"
  ]
  node [
    id 377
    label "Gruzja"
  ]
  node [
    id 378
    label "Kongo"
  ]
  node [
    id 379
    label "Tajwan"
  ]
  node [
    id 380
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 381
    label "Honduras"
  ]
  node [
    id 382
    label "Boliwia"
  ]
  node [
    id 383
    label "Uganda"
  ]
  node [
    id 384
    label "Namibia"
  ]
  node [
    id 385
    label "Erytrea"
  ]
  node [
    id 386
    label "Azerbejd&#380;an"
  ]
  node [
    id 387
    label "Panama"
  ]
  node [
    id 388
    label "Gujana"
  ]
  node [
    id 389
    label "Somalia"
  ]
  node [
    id 390
    label "Burundi"
  ]
  node [
    id 391
    label "Tuwalu"
  ]
  node [
    id 392
    label "Libia"
  ]
  node [
    id 393
    label "Katar"
  ]
  node [
    id 394
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 395
    label "Trynidad_i_Tobago"
  ]
  node [
    id 396
    label "Sahara_Zachodnia"
  ]
  node [
    id 397
    label "Gwinea_Bissau"
  ]
  node [
    id 398
    label "Bu&#322;garia"
  ]
  node [
    id 399
    label "Tonga"
  ]
  node [
    id 400
    label "Nikaragua"
  ]
  node [
    id 401
    label "Fid&#380;i"
  ]
  node [
    id 402
    label "Timor_Wschodni"
  ]
  node [
    id 403
    label "Laos"
  ]
  node [
    id 404
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 405
    label "Ghana"
  ]
  node [
    id 406
    label "Brazylia"
  ]
  node [
    id 407
    label "Belgia"
  ]
  node [
    id 408
    label "Irak"
  ]
  node [
    id 409
    label "Peru"
  ]
  node [
    id 410
    label "Arabia_Saudyjska"
  ]
  node [
    id 411
    label "Indonezja"
  ]
  node [
    id 412
    label "Malediwy"
  ]
  node [
    id 413
    label "Afganistan"
  ]
  node [
    id 414
    label "Jordania"
  ]
  node [
    id 415
    label "Kenia"
  ]
  node [
    id 416
    label "Czad"
  ]
  node [
    id 417
    label "Liberia"
  ]
  node [
    id 418
    label "Mali"
  ]
  node [
    id 419
    label "Armenia"
  ]
  node [
    id 420
    label "W&#281;gry"
  ]
  node [
    id 421
    label "Chile"
  ]
  node [
    id 422
    label "Kanada"
  ]
  node [
    id 423
    label "Cypr"
  ]
  node [
    id 424
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 425
    label "Ekwador"
  ]
  node [
    id 426
    label "Mo&#322;dawia"
  ]
  node [
    id 427
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 428
    label "W&#322;ochy"
  ]
  node [
    id 429
    label "Wyspy_Salomona"
  ]
  node [
    id 430
    label "&#321;otwa"
  ]
  node [
    id 431
    label "D&#380;ibuti"
  ]
  node [
    id 432
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 433
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 434
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 435
    label "Portugalia"
  ]
  node [
    id 436
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 437
    label "Maroko"
  ]
  node [
    id 438
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 439
    label "Francja"
  ]
  node [
    id 440
    label "Botswana"
  ]
  node [
    id 441
    label "Dominika"
  ]
  node [
    id 442
    label "Paragwaj"
  ]
  node [
    id 443
    label "Tad&#380;ykistan"
  ]
  node [
    id 444
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 445
    label "Haiti"
  ]
  node [
    id 446
    label "Khitai"
  ]
  node [
    id 447
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 448
    label "ci&#261;gle"
  ]
  node [
    id 449
    label "nieprzerwanie"
  ]
  node [
    id 450
    label "ci&#261;g&#322;y"
  ]
  node [
    id 451
    label "stale"
  ]
  node [
    id 452
    label "sake"
  ]
  node [
    id 453
    label "rozciekawia&#263;"
  ]
  node [
    id 454
    label "alkohol"
  ]
  node [
    id 455
    label "istota_&#380;ywa"
  ]
  node [
    id 456
    label "przekle&#324;stwo"
  ]
  node [
    id 457
    label "cholewa"
  ]
  node [
    id 458
    label "choroba"
  ]
  node [
    id 459
    label "charakternik"
  ]
  node [
    id 460
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 461
    label "wyzwisko"
  ]
  node [
    id 462
    label "gniew"
  ]
  node [
    id 463
    label "holender"
  ]
  node [
    id 464
    label "skurczybyk"
  ]
  node [
    id 465
    label "choroba_bakteryjna"
  ]
  node [
    id 466
    label "przecinkowiec_cholery"
  ]
  node [
    id 467
    label "fury"
  ]
  node [
    id 468
    label "chor&#243;bka"
  ]
  node [
    id 469
    label "wzburzenie"
  ]
  node [
    id 470
    label "rankor"
  ]
  node [
    id 471
    label "temper"
  ]
  node [
    id 472
    label "k&#322;&#243;tnia"
  ]
  node [
    id 473
    label "wykrzyknik"
  ]
  node [
    id 474
    label "bluzg"
  ]
  node [
    id 475
    label "cecha"
  ]
  node [
    id 476
    label "four-letter_word"
  ]
  node [
    id 477
    label "strapienie"
  ]
  node [
    id 478
    label "fakt"
  ]
  node [
    id 479
    label "figura_my&#347;li"
  ]
  node [
    id 480
    label "wulgaryzm"
  ]
  node [
    id 481
    label "bestia"
  ]
  node [
    id 482
    label "szelma"
  ]
  node [
    id 483
    label "pieron"
  ]
  node [
    id 484
    label "diabe&#322;"
  ]
  node [
    id 485
    label "&#322;ajdak"
  ]
  node [
    id 486
    label "chytra_sztuka"
  ]
  node [
    id 487
    label "pies"
  ]
  node [
    id 488
    label "wypowied&#378;"
  ]
  node [
    id 489
    label "chujowy"
  ]
  node [
    id 490
    label "obelga"
  ]
  node [
    id 491
    label "chuj"
  ]
  node [
    id 492
    label "szmata"
  ]
  node [
    id 493
    label "charakter"
  ]
  node [
    id 494
    label "gwa&#322;towny"
  ]
  node [
    id 495
    label "wr&#243;&#380;biarz"
  ]
  node [
    id 496
    label "jasny_gwint"
  ]
  node [
    id 497
    label "chory"
  ]
  node [
    id 498
    label "kamasz"
  ]
  node [
    id 499
    label "cholewkarstwo"
  ]
  node [
    id 500
    label "cholewka"
  ]
  node [
    id 501
    label "but"
  ]
  node [
    id 502
    label "&#347;niegowiec"
  ]
  node [
    id 503
    label "odezwanie_si&#281;"
  ]
  node [
    id 504
    label "zaburzenie"
  ]
  node [
    id 505
    label "ognisko"
  ]
  node [
    id 506
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 507
    label "atakowanie"
  ]
  node [
    id 508
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 509
    label "remisja"
  ]
  node [
    id 510
    label "nabawianie_si&#281;"
  ]
  node [
    id 511
    label "odzywanie_si&#281;"
  ]
  node [
    id 512
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 513
    label "powalenie"
  ]
  node [
    id 514
    label "diagnoza"
  ]
  node [
    id 515
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 516
    label "atakowa&#263;"
  ]
  node [
    id 517
    label "inkubacja"
  ]
  node [
    id 518
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 519
    label "grupa_ryzyka"
  ]
  node [
    id 520
    label "badanie_histopatologiczne"
  ]
  node [
    id 521
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 522
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 523
    label "przypadek"
  ]
  node [
    id 524
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 525
    label "zajmowanie"
  ]
  node [
    id 526
    label "powali&#263;"
  ]
  node [
    id 527
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 528
    label "zajmowa&#263;"
  ]
  node [
    id 529
    label "kryzys"
  ]
  node [
    id 530
    label "bol&#261;czka"
  ]
  node [
    id 531
    label "nabawienie_si&#281;"
  ]
  node [
    id 532
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 533
    label "rower"
  ]
  node [
    id 534
    label "koza"
  ]
  node [
    id 535
    label "co&#347;"
  ]
  node [
    id 536
    label "thing"
  ]
  node [
    id 537
    label "cosik"
  ]
  node [
    id 538
    label "rozmiar"
  ]
  node [
    id 539
    label "part"
  ]
  node [
    id 540
    label "tu"
  ]
  node [
    id 541
    label "proceed"
  ]
  node [
    id 542
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 543
    label "kontynuowa&#263;"
  ]
  node [
    id 544
    label "wykonywa&#263;"
  ]
  node [
    id 545
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 546
    label "ride"
  ]
  node [
    id 547
    label "korzysta&#263;"
  ]
  node [
    id 548
    label "continue"
  ]
  node [
    id 549
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 550
    label "prowadzi&#263;"
  ]
  node [
    id 551
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 552
    label "drive"
  ]
  node [
    id 553
    label "go"
  ]
  node [
    id 554
    label "carry"
  ]
  node [
    id 555
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 556
    label "napada&#263;"
  ]
  node [
    id 557
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 558
    label "odbywa&#263;"
  ]
  node [
    id 559
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 560
    label "overdrive"
  ]
  node [
    id 561
    label "czu&#263;"
  ]
  node [
    id 562
    label "wali&#263;"
  ]
  node [
    id 563
    label "pachnie&#263;"
  ]
  node [
    id 564
    label "jeba&#263;"
  ]
  node [
    id 565
    label "talerz_perkusyjny"
  ]
  node [
    id 566
    label "cover"
  ]
  node [
    id 567
    label "chi&#324;ski"
  ]
  node [
    id 568
    label "goban"
  ]
  node [
    id 569
    label "gra_planszowa"
  ]
  node [
    id 570
    label "sport_umys&#322;owy"
  ]
  node [
    id 571
    label "robi&#263;"
  ]
  node [
    id 572
    label "prosecute"
  ]
  node [
    id 573
    label "przechodzi&#263;"
  ]
  node [
    id 574
    label "hold"
  ]
  node [
    id 575
    label "uczestniczy&#263;"
  ]
  node [
    id 576
    label "muzyka"
  ]
  node [
    id 577
    label "praca"
  ]
  node [
    id 578
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 579
    label "wytwarza&#263;"
  ]
  node [
    id 580
    label "rola"
  ]
  node [
    id 581
    label "work"
  ]
  node [
    id 582
    label "create"
  ]
  node [
    id 583
    label "eksponowa&#263;"
  ]
  node [
    id 584
    label "g&#243;rowa&#263;"
  ]
  node [
    id 585
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 586
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 587
    label "sterowa&#263;"
  ]
  node [
    id 588
    label "kierowa&#263;"
  ]
  node [
    id 589
    label "string"
  ]
  node [
    id 590
    label "control"
  ]
  node [
    id 591
    label "kre&#347;li&#263;"
  ]
  node [
    id 592
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 593
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 594
    label "&#380;y&#263;"
  ]
  node [
    id 595
    label "partner"
  ]
  node [
    id 596
    label "linia_melodyczna"
  ]
  node [
    id 597
    label "prowadzenie"
  ]
  node [
    id 598
    label "ukierunkowywa&#263;"
  ]
  node [
    id 599
    label "przesuwa&#263;"
  ]
  node [
    id 600
    label "tworzy&#263;"
  ]
  node [
    id 601
    label "powodowa&#263;"
  ]
  node [
    id 602
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 603
    label "message"
  ]
  node [
    id 604
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 605
    label "navigate"
  ]
  node [
    id 606
    label "krzywa"
  ]
  node [
    id 607
    label "manipulate"
  ]
  node [
    id 608
    label "jeer"
  ]
  node [
    id 609
    label "traktowa&#263;"
  ]
  node [
    id 610
    label "dopada&#263;"
  ]
  node [
    id 611
    label "piratowa&#263;"
  ]
  node [
    id 612
    label "krytykowa&#263;"
  ]
  node [
    id 613
    label "attack"
  ]
  node [
    id 614
    label "m&#243;wi&#263;"
  ]
  node [
    id 615
    label "u&#380;ywa&#263;"
  ]
  node [
    id 616
    label "use"
  ]
  node [
    id 617
    label "uzyskiwa&#263;"
  ]
  node [
    id 618
    label "uczuwa&#263;"
  ]
  node [
    id 619
    label "smell"
  ]
  node [
    id 620
    label "doznawa&#263;"
  ]
  node [
    id 621
    label "przewidywa&#263;"
  ]
  node [
    id 622
    label "anticipate"
  ]
  node [
    id 623
    label "postrzega&#263;"
  ]
  node [
    id 624
    label "by&#263;"
  ]
  node [
    id 625
    label "spirit"
  ]
  node [
    id 626
    label "Krajina"
  ]
  node [
    id 627
    label "Lotaryngia"
  ]
  node [
    id 628
    label "Lubuskie"
  ]
  node [
    id 629
    label "&#379;mud&#378;"
  ]
  node [
    id 630
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 631
    label "Ko&#322;yma"
  ]
  node [
    id 632
    label "okr&#281;g"
  ]
  node [
    id 633
    label "Skandynawia"
  ]
  node [
    id 634
    label "Kampania"
  ]
  node [
    id 635
    label "Zakarpacie"
  ]
  node [
    id 636
    label "Podlasie"
  ]
  node [
    id 637
    label "Wielkopolska"
  ]
  node [
    id 638
    label "Indochiny"
  ]
  node [
    id 639
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 640
    label "Bo&#347;nia"
  ]
  node [
    id 641
    label "Kaukaz"
  ]
  node [
    id 642
    label "Opolszczyzna"
  ]
  node [
    id 643
    label "Armagnac"
  ]
  node [
    id 644
    label "Polesie"
  ]
  node [
    id 645
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 646
    label "Bawaria"
  ]
  node [
    id 647
    label "Yorkshire"
  ]
  node [
    id 648
    label "Syjon"
  ]
  node [
    id 649
    label "Apulia"
  ]
  node [
    id 650
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 651
    label "Noworosja"
  ]
  node [
    id 652
    label "&#321;&#243;dzkie"
  ]
  node [
    id 653
    label "Nadrenia"
  ]
  node [
    id 654
    label "Kurpie"
  ]
  node [
    id 655
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 656
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 657
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 658
    label "Naddniestrze"
  ]
  node [
    id 659
    label "jednostka_administracyjna"
  ]
  node [
    id 660
    label "Azja_Wschodnia"
  ]
  node [
    id 661
    label "Baszkiria"
  ]
  node [
    id 662
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 663
    label "Afryka_Wschodnia"
  ]
  node [
    id 664
    label "Andaluzja"
  ]
  node [
    id 665
    label "Afryka_Zachodnia"
  ]
  node [
    id 666
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 667
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 668
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 669
    label "Opolskie"
  ]
  node [
    id 670
    label "Kociewie"
  ]
  node [
    id 671
    label "obszar"
  ]
  node [
    id 672
    label "Anglia"
  ]
  node [
    id 673
    label "Bordeaux"
  ]
  node [
    id 674
    label "&#321;emkowszczyzna"
  ]
  node [
    id 675
    label "Mazowsze"
  ]
  node [
    id 676
    label "Laponia"
  ]
  node [
    id 677
    label "Amazonia"
  ]
  node [
    id 678
    label "Lasko"
  ]
  node [
    id 679
    label "Hercegowina"
  ]
  node [
    id 680
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 681
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 682
    label "Liguria"
  ]
  node [
    id 683
    label "Lubelszczyzna"
  ]
  node [
    id 684
    label "Tonkin"
  ]
  node [
    id 685
    label "Ukraina_Zachodnia"
  ]
  node [
    id 686
    label "Oceania"
  ]
  node [
    id 687
    label "Pamir"
  ]
  node [
    id 688
    label "Podkarpacie"
  ]
  node [
    id 689
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 690
    label "Tyrol"
  ]
  node [
    id 691
    label "podregion"
  ]
  node [
    id 692
    label "Bory_Tucholskie"
  ]
  node [
    id 693
    label "Podhale"
  ]
  node [
    id 694
    label "Chiny_Wschodnie"
  ]
  node [
    id 695
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 696
    label "Polinezja"
  ]
  node [
    id 697
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 698
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 699
    label "Mazury"
  ]
  node [
    id 700
    label "Europa_Wschodnia"
  ]
  node [
    id 701
    label "Europa_Zachodnia"
  ]
  node [
    id 702
    label "Zabajkale"
  ]
  node [
    id 703
    label "Turyngia"
  ]
  node [
    id 704
    label "Kielecczyzna"
  ]
  node [
    id 705
    label "Ba&#322;kany"
  ]
  node [
    id 706
    label "Kaszuby"
  ]
  node [
    id 707
    label "Szlezwik"
  ]
  node [
    id 708
    label "Umbria"
  ]
  node [
    id 709
    label "Oksytania"
  ]
  node [
    id 710
    label "Mezoameryka"
  ]
  node [
    id 711
    label "Turkiestan"
  ]
  node [
    id 712
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 713
    label "Kurdystan"
  ]
  node [
    id 714
    label "Karaiby"
  ]
  node [
    id 715
    label "Biskupizna"
  ]
  node [
    id 716
    label "Podbeskidzie"
  ]
  node [
    id 717
    label "Zag&#243;rze"
  ]
  node [
    id 718
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 719
    label "Kalabria"
  ]
  node [
    id 720
    label "Szkocja"
  ]
  node [
    id 721
    label "Ma&#322;opolska"
  ]
  node [
    id 722
    label "subregion"
  ]
  node [
    id 723
    label "Huculszczyzna"
  ]
  node [
    id 724
    label "Kraina"
  ]
  node [
    id 725
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 726
    label "Sand&#380;ak"
  ]
  node [
    id 727
    label "Kerala"
  ]
  node [
    id 728
    label "S&#261;decczyzna"
  ]
  node [
    id 729
    label "Lombardia"
  ]
  node [
    id 730
    label "Toskania"
  ]
  node [
    id 731
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 732
    label "Galicja"
  ]
  node [
    id 733
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 734
    label "Palestyna"
  ]
  node [
    id 735
    label "Kabylia"
  ]
  node [
    id 736
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 737
    label "Pomorze_Zachodnie"
  ]
  node [
    id 738
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 739
    label "country"
  ]
  node [
    id 740
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 741
    label "Lauda"
  ]
  node [
    id 742
    label "Kujawy"
  ]
  node [
    id 743
    label "Warmia"
  ]
  node [
    id 744
    label "Maghreb"
  ]
  node [
    id 745
    label "Kaszmir"
  ]
  node [
    id 746
    label "Chiny_Zachodnie"
  ]
  node [
    id 747
    label "Bojkowszczyzna"
  ]
  node [
    id 748
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 749
    label "Amhara"
  ]
  node [
    id 750
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 751
    label "Zamojszczyzna"
  ]
  node [
    id 752
    label "Walia"
  ]
  node [
    id 753
    label "Flandria"
  ]
  node [
    id 754
    label "&#379;ywiecczyzna"
  ]
  node [
    id 755
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 756
    label "Burgundia"
  ]
  node [
    id 757
    label "Powi&#347;le"
  ]
  node [
    id 758
    label "Kosowo"
  ]
  node [
    id 759
    label "zach&#243;d"
  ]
  node [
    id 760
    label "Zabu&#380;e"
  ]
  node [
    id 761
    label "wymiar"
  ]
  node [
    id 762
    label "antroposfera"
  ]
  node [
    id 763
    label "Arktyka"
  ]
  node [
    id 764
    label "Notogea"
  ]
  node [
    id 765
    label "przestrze&#324;"
  ]
  node [
    id 766
    label "Piotrowo"
  ]
  node [
    id 767
    label "zbi&#243;r"
  ]
  node [
    id 768
    label "akrecja"
  ]
  node [
    id 769
    label "zakres"
  ]
  node [
    id 770
    label "Ludwin&#243;w"
  ]
  node [
    id 771
    label "Ruda_Pabianicka"
  ]
  node [
    id 772
    label "po&#322;udnie"
  ]
  node [
    id 773
    label "miejsce"
  ]
  node [
    id 774
    label "wsch&#243;d"
  ]
  node [
    id 775
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 776
    label "Pow&#261;zki"
  ]
  node [
    id 777
    label "&#321;&#281;g"
  ]
  node [
    id 778
    label "p&#243;&#322;noc"
  ]
  node [
    id 779
    label "Rakowice"
  ]
  node [
    id 780
    label "Syberia_Wschodnia"
  ]
  node [
    id 781
    label "Zab&#322;ocie"
  ]
  node [
    id 782
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 783
    label "Kresy_Zachodnie"
  ]
  node [
    id 784
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 785
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 786
    label "holarktyka"
  ]
  node [
    id 787
    label "pas_planetoid"
  ]
  node [
    id 788
    label "Antarktyka"
  ]
  node [
    id 789
    label "Syberia_Zachodnia"
  ]
  node [
    id 790
    label "Neogea"
  ]
  node [
    id 791
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 792
    label "Olszanica"
  ]
  node [
    id 793
    label "Judea"
  ]
  node [
    id 794
    label "Kanaan"
  ]
  node [
    id 795
    label "moszaw"
  ]
  node [
    id 796
    label "Anglosas"
  ]
  node [
    id 797
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 798
    label "Jerozolima"
  ]
  node [
    id 799
    label "Beskidy_Zachodnie"
  ]
  node [
    id 800
    label "Wiktoria"
  ]
  node [
    id 801
    label "Guernsey"
  ]
  node [
    id 802
    label "Conrad"
  ]
  node [
    id 803
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 804
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 805
    label "funt_szterling"
  ]
  node [
    id 806
    label "NATO"
  ]
  node [
    id 807
    label "Unia_Europejska"
  ]
  node [
    id 808
    label "Portland"
  ]
  node [
    id 809
    label "El&#380;bieta_I"
  ]
  node [
    id 810
    label "Kornwalia"
  ]
  node [
    id 811
    label "Dolna_Frankonia"
  ]
  node [
    id 812
    label "dolar"
  ]
  node [
    id 813
    label "Mariany"
  ]
  node [
    id 814
    label "Karpaty"
  ]
  node [
    id 815
    label "Beskid_Niski"
  ]
  node [
    id 816
    label "Mariensztat"
  ]
  node [
    id 817
    label "Warszawa"
  ]
  node [
    id 818
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 819
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 820
    label "Paj&#281;czno"
  ]
  node [
    id 821
    label "Mogielnica"
  ]
  node [
    id 822
    label "Gop&#322;o"
  ]
  node [
    id 823
    label "Moza"
  ]
  node [
    id 824
    label "Poprad"
  ]
  node [
    id 825
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 826
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 827
    label "Wilkowo_Polskie"
  ]
  node [
    id 828
    label "Obra"
  ]
  node [
    id 829
    label "Dobra"
  ]
  node [
    id 830
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 831
    label "Bojanowo"
  ]
  node [
    id 832
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 833
    label "Hawaje"
  ]
  node [
    id 834
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 835
    label "Etruria"
  ]
  node [
    id 836
    label "Rumelia"
  ]
  node [
    id 837
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 838
    label "Melanezja"
  ]
  node [
    id 839
    label "Ocean_Spokojny"
  ]
  node [
    id 840
    label "Nowy_&#346;wiat"
  ]
  node [
    id 841
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 842
    label "Tar&#322;&#243;w"
  ]
  node [
    id 843
    label "Eurazja"
  ]
  node [
    id 844
    label "Inguszetia"
  ]
  node [
    id 845
    label "Czeczenia"
  ]
  node [
    id 846
    label "Abchazja"
  ]
  node [
    id 847
    label "Sarmata"
  ]
  node [
    id 848
    label "Dagestan"
  ]
  node [
    id 849
    label "Podtatrze"
  ]
  node [
    id 850
    label "Tatry"
  ]
  node [
    id 851
    label "Imperium_Rosyjskie"
  ]
  node [
    id 852
    label "jezioro"
  ]
  node [
    id 853
    label "&#346;l&#261;sk"
  ]
  node [
    id 854
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 855
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 856
    label "Podole"
  ]
  node [
    id 857
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 858
    label "Austro-W&#281;gry"
  ]
  node [
    id 859
    label "funt_szkocki"
  ]
  node [
    id 860
    label "Kaledonia"
  ]
  node [
    id 861
    label "Ziemia_Sandomierska"
  ]
  node [
    id 862
    label "Ropa"
  ]
  node [
    id 863
    label "Rogo&#378;nik"
  ]
  node [
    id 864
    label "Iwanowice"
  ]
  node [
    id 865
    label "Biskupice"
  ]
  node [
    id 866
    label "Buriacja"
  ]
  node [
    id 867
    label "Rozewie"
  ]
  node [
    id 868
    label "Finlandia"
  ]
  node [
    id 869
    label "Norwegia"
  ]
  node [
    id 870
    label "Szwecja"
  ]
  node [
    id 871
    label "Anguilla"
  ]
  node [
    id 872
    label "Antyle"
  ]
  node [
    id 873
    label "Aruba"
  ]
  node [
    id 874
    label "Kajmany"
  ]
  node [
    id 875
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 876
    label "Amazonka"
  ]
  node [
    id 877
    label "Alpy"
  ]
  node [
    id 878
    label "&#379;mujd&#378;"
  ]
  node [
    id 879
    label "j&#281;zyk_flamandzki"
  ]
  node [
    id 880
    label "okr&#281;g_przemys&#322;owy"
  ]
  node [
    id 881
    label "bluegrass"
  ]
  node [
    id 882
    label "muzyka_rozrywkowa"
  ]
  node [
    id 883
    label "korzennie"
  ]
  node [
    id 884
    label "aromatyczny"
  ]
  node [
    id 885
    label "ciep&#322;y"
  ]
  node [
    id 886
    label "klopidogrel"
  ]
  node [
    id 887
    label "aromatycznie"
  ]
  node [
    id 888
    label "organiczny"
  ]
  node [
    id 889
    label "relish"
  ]
  node [
    id 890
    label "tioguanina"
  ]
  node [
    id 891
    label "grzanie"
  ]
  node [
    id 892
    label "ocieplenie"
  ]
  node [
    id 893
    label "korzystny"
  ]
  node [
    id 894
    label "ciep&#322;o"
  ]
  node [
    id 895
    label "zagrzanie"
  ]
  node [
    id 896
    label "ocieplanie"
  ]
  node [
    id 897
    label "dobry"
  ]
  node [
    id 898
    label "przyjemny"
  ]
  node [
    id 899
    label "ocieplenie_si&#281;"
  ]
  node [
    id 900
    label "ocieplanie_si&#281;"
  ]
  node [
    id 901
    label "mi&#322;y"
  ]
  node [
    id 902
    label "korzenno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 284
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 243
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 240
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
]
