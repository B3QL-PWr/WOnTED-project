graph [
  node [
    id 0
    label "depresja"
    origin "text"
  ]
  node [
    id 1
    label "heheszki"
    origin "text"
  ]
  node [
    id 2
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 3
    label "choroba_ducha"
  ]
  node [
    id 4
    label "choroba_psychiczna"
  ]
  node [
    id 5
    label "obni&#380;enie"
  ]
  node [
    id 6
    label "deprecha"
  ]
  node [
    id 7
    label "zjawisko"
  ]
  node [
    id 8
    label "zesp&#243;&#322;_napi&#281;cia_przedmiesi&#261;czkowego"
  ]
  node [
    id 9
    label "zabrzmienie"
  ]
  node [
    id 10
    label "kszta&#322;t"
  ]
  node [
    id 11
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 12
    label "zmniejszenie"
  ]
  node [
    id 13
    label "spowodowanie"
  ]
  node [
    id 14
    label "miejsce"
  ]
  node [
    id 15
    label "ni&#380;szy"
  ]
  node [
    id 16
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 17
    label "niski"
  ]
  node [
    id 18
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 19
    label "suspension"
  ]
  node [
    id 20
    label "pad&#243;&#322;"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "snub"
  ]
  node [
    id 23
    label "proces"
  ]
  node [
    id 24
    label "boski"
  ]
  node [
    id 25
    label "krajobraz"
  ]
  node [
    id 26
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 27
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 28
    label "przywidzenie"
  ]
  node [
    id 29
    label "presence"
  ]
  node [
    id 30
    label "charakter"
  ]
  node [
    id 31
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 32
    label "przygn&#281;bienie"
  ]
  node [
    id 33
    label "za&#322;amanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
]
