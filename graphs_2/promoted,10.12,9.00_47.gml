graph [
  node [
    id 0
    label "sonda"
    origin "text"
  ]
  node [
    id 1
    label "uliczny"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "jork"
    origin "text"
  ]
  node [
    id 4
    label "badanie"
  ]
  node [
    id 5
    label "statek_kosmiczny"
  ]
  node [
    id 6
    label "narz&#281;dzie"
  ]
  node [
    id 7
    label "lead"
  ]
  node [
    id 8
    label "leash"
  ]
  node [
    id 9
    label "przyrz&#261;d"
  ]
  node [
    id 10
    label "sonda&#380;"
  ]
  node [
    id 11
    label "utensylia"
  ]
  node [
    id 12
    label "&#347;rodek"
  ]
  node [
    id 13
    label "niezb&#281;dnik"
  ]
  node [
    id 14
    label "przedmiot"
  ]
  node [
    id 15
    label "spos&#243;b"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 18
    label "tylec"
  ]
  node [
    id 19
    label "urz&#261;dzenie"
  ]
  node [
    id 20
    label "obserwowanie"
  ]
  node [
    id 21
    label "zrecenzowanie"
  ]
  node [
    id 22
    label "kontrola"
  ]
  node [
    id 23
    label "analysis"
  ]
  node [
    id 24
    label "rektalny"
  ]
  node [
    id 25
    label "ustalenie"
  ]
  node [
    id 26
    label "macanie"
  ]
  node [
    id 27
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 28
    label "usi&#322;owanie"
  ]
  node [
    id 29
    label "udowadnianie"
  ]
  node [
    id 30
    label "praca"
  ]
  node [
    id 31
    label "bia&#322;a_niedziela"
  ]
  node [
    id 32
    label "diagnostyka"
  ]
  node [
    id 33
    label "dociekanie"
  ]
  node [
    id 34
    label "rezultat"
  ]
  node [
    id 35
    label "sprawdzanie"
  ]
  node [
    id 36
    label "penetrowanie"
  ]
  node [
    id 37
    label "czynno&#347;&#263;"
  ]
  node [
    id 38
    label "krytykowanie"
  ]
  node [
    id 39
    label "omawianie"
  ]
  node [
    id 40
    label "ustalanie"
  ]
  node [
    id 41
    label "rozpatrywanie"
  ]
  node [
    id 42
    label "investigation"
  ]
  node [
    id 43
    label "wziernikowanie"
  ]
  node [
    id 44
    label "examination"
  ]
  node [
    id 45
    label "d&#243;&#322;"
  ]
  node [
    id 46
    label "artyku&#322;"
  ]
  node [
    id 47
    label "akapit"
  ]
  node [
    id 48
    label "jarmarcznie"
  ]
  node [
    id 49
    label "streetowy"
  ]
  node [
    id 50
    label "prostacki"
  ]
  node [
    id 51
    label "po_prostacku"
  ]
  node [
    id 52
    label "przedpokojowy"
  ]
  node [
    id 53
    label "prosty"
  ]
  node [
    id 54
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 55
    label "niewymy&#347;lny"
  ]
  node [
    id 56
    label "pospolity"
  ]
  node [
    id 57
    label "shoddily"
  ]
  node [
    id 58
    label "typowo"
  ]
  node [
    id 59
    label "tandetnie"
  ]
  node [
    id 60
    label "kolorowo"
  ]
  node [
    id 61
    label "jarmarczny"
  ]
  node [
    id 62
    label "prostacko"
  ]
  node [
    id 63
    label "gwiazda"
  ]
  node [
    id 64
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 65
    label "Arktur"
  ]
  node [
    id 66
    label "kszta&#322;t"
  ]
  node [
    id 67
    label "Gwiazda_Polarna"
  ]
  node [
    id 68
    label "agregatka"
  ]
  node [
    id 69
    label "gromada"
  ]
  node [
    id 70
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 71
    label "S&#322;o&#324;ce"
  ]
  node [
    id 72
    label "Nibiru"
  ]
  node [
    id 73
    label "konstelacja"
  ]
  node [
    id 74
    label "ornament"
  ]
  node [
    id 75
    label "delta_Scuti"
  ]
  node [
    id 76
    label "&#347;wiat&#322;o"
  ]
  node [
    id 77
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 78
    label "obiekt"
  ]
  node [
    id 79
    label "s&#322;awa"
  ]
  node [
    id 80
    label "promie&#324;"
  ]
  node [
    id 81
    label "star"
  ]
  node [
    id 82
    label "gwiazdosz"
  ]
  node [
    id 83
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 84
    label "asocjacja_gwiazd"
  ]
  node [
    id 85
    label "supergrupa"
  ]
  node [
    id 86
    label "pies_miniaturowy"
  ]
  node [
    id 87
    label "pies_do_towarzystwa"
  ]
  node [
    id 88
    label "pies_pokojowy"
  ]
  node [
    id 89
    label "terier"
  ]
  node [
    id 90
    label "pies_ozdobny"
  ]
  node [
    id 91
    label "norowiec"
  ]
  node [
    id 92
    label "terrier"
  ]
  node [
    id 93
    label "Jork"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
]
