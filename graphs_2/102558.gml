graph [
  node [
    id 0
    label "przyznanie"
    origin "text"
  ]
  node [
    id 1
    label "dotacja"
    origin "text"
  ]
  node [
    id 2
    label "podmiot"
    origin "text"
  ]
  node [
    id 3
    label "prowadz&#261;cy"
    origin "text"
  ]
  node [
    id 4
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "po&#380;ytek"
    origin "text"
  ]
  node [
    id 6
    label "publiczny"
    origin "text"
  ]
  node [
    id 7
    label "wsparcie"
    origin "text"
  ]
  node [
    id 8
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 9
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 10
    label "hipoterapeutycznego"
    origin "text"
  ]
  node [
    id 11
    label "gdynia"
    origin "text"
  ]
  node [
    id 12
    label "oznajmienie"
  ]
  node [
    id 13
    label "confession"
  ]
  node [
    id 14
    label "recognition"
  ]
  node [
    id 15
    label "stwierdzenie"
  ]
  node [
    id 16
    label "danie"
  ]
  node [
    id 17
    label "wyposa&#380;enie"
  ]
  node [
    id 18
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 19
    label "jedzenie"
  ]
  node [
    id 20
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 21
    label "posi&#322;ek"
  ]
  node [
    id 22
    label "wyst&#261;pienie"
  ]
  node [
    id 23
    label "zadanie"
  ]
  node [
    id 24
    label "urz&#261;dzenie"
  ]
  node [
    id 25
    label "pobicie"
  ]
  node [
    id 26
    label "uderzenie"
  ]
  node [
    id 27
    label "dodanie"
  ]
  node [
    id 28
    label "potrawa"
  ]
  node [
    id 29
    label "przeznaczenie"
  ]
  node [
    id 30
    label "uderzanie"
  ]
  node [
    id 31
    label "obiecanie"
  ]
  node [
    id 32
    label "uprawianie_seksu"
  ]
  node [
    id 33
    label "czynno&#347;&#263;"
  ]
  node [
    id 34
    label "wyposa&#380;anie"
  ]
  node [
    id 35
    label "allow"
  ]
  node [
    id 36
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 37
    label "pass"
  ]
  node [
    id 38
    label "powierzenie"
  ]
  node [
    id 39
    label "dostanie"
  ]
  node [
    id 40
    label "udost&#281;pnienie"
  ]
  node [
    id 41
    label "give"
  ]
  node [
    id 42
    label "cios"
  ]
  node [
    id 43
    label "zap&#322;acenie"
  ]
  node [
    id 44
    label "eating"
  ]
  node [
    id 45
    label "menu"
  ]
  node [
    id 46
    label "przekazanie"
  ]
  node [
    id 47
    label "rendition"
  ]
  node [
    id 48
    label "hand"
  ]
  node [
    id 49
    label "wymienienie_si&#281;"
  ]
  node [
    id 50
    label "zrobienie"
  ]
  node [
    id 51
    label "odst&#261;pienie"
  ]
  node [
    id 52
    label "coup"
  ]
  node [
    id 53
    label "dostarczenie"
  ]
  node [
    id 54
    label "karta"
  ]
  node [
    id 55
    label "wypowied&#378;"
  ]
  node [
    id 56
    label "statement"
  ]
  node [
    id 57
    label "ustalenie"
  ]
  node [
    id 58
    label "claim"
  ]
  node [
    id 59
    label "announcement"
  ]
  node [
    id 60
    label "poinformowanie"
  ]
  node [
    id 61
    label "Manifest_lipcowy"
  ]
  node [
    id 62
    label "manifesto"
  ]
  node [
    id 63
    label "wypowiedzenie"
  ]
  node [
    id 64
    label "zwiastowanie"
  ]
  node [
    id 65
    label "apel"
  ]
  node [
    id 66
    label "dop&#322;ata"
  ]
  node [
    id 67
    label "doj&#347;cie"
  ]
  node [
    id 68
    label "dochodzenie"
  ]
  node [
    id 69
    label "doch&#243;d"
  ]
  node [
    id 70
    label "doj&#347;&#263;"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "byt"
  ]
  node [
    id 73
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 74
    label "nauka_prawa"
  ]
  node [
    id 75
    label "prawo"
  ]
  node [
    id 76
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 77
    label "organizacja"
  ]
  node [
    id 78
    label "osobowo&#347;&#263;"
  ]
  node [
    id 79
    label "asymilowa&#263;"
  ]
  node [
    id 80
    label "nasada"
  ]
  node [
    id 81
    label "profanum"
  ]
  node [
    id 82
    label "wz&#243;r"
  ]
  node [
    id 83
    label "senior"
  ]
  node [
    id 84
    label "asymilowanie"
  ]
  node [
    id 85
    label "os&#322;abia&#263;"
  ]
  node [
    id 86
    label "homo_sapiens"
  ]
  node [
    id 87
    label "osoba"
  ]
  node [
    id 88
    label "ludzko&#347;&#263;"
  ]
  node [
    id 89
    label "Adam"
  ]
  node [
    id 90
    label "hominid"
  ]
  node [
    id 91
    label "posta&#263;"
  ]
  node [
    id 92
    label "portrecista"
  ]
  node [
    id 93
    label "polifag"
  ]
  node [
    id 94
    label "podw&#322;adny"
  ]
  node [
    id 95
    label "dwun&#243;g"
  ]
  node [
    id 96
    label "wapniak"
  ]
  node [
    id 97
    label "duch"
  ]
  node [
    id 98
    label "os&#322;abianie"
  ]
  node [
    id 99
    label "antropochoria"
  ]
  node [
    id 100
    label "figura"
  ]
  node [
    id 101
    label "g&#322;owa"
  ]
  node [
    id 102
    label "mikrokosmos"
  ]
  node [
    id 103
    label "oddzia&#322;ywanie"
  ]
  node [
    id 104
    label "ontologicznie"
  ]
  node [
    id 105
    label "utrzyma&#263;"
  ]
  node [
    id 106
    label "bycie"
  ]
  node [
    id 107
    label "utrzymanie"
  ]
  node [
    id 108
    label "utrzymywanie"
  ]
  node [
    id 109
    label "entity"
  ]
  node [
    id 110
    label "utrzymywa&#263;"
  ]
  node [
    id 111
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 112
    label "egzystencja"
  ]
  node [
    id 113
    label "wy&#380;ywienie"
  ]
  node [
    id 114
    label "potencja"
  ]
  node [
    id 115
    label "subsystencja"
  ]
  node [
    id 116
    label "przybud&#243;wka"
  ]
  node [
    id 117
    label "struktura"
  ]
  node [
    id 118
    label "organization"
  ]
  node [
    id 119
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 120
    label "od&#322;am"
  ]
  node [
    id 121
    label "TOPR"
  ]
  node [
    id 122
    label "komitet_koordynacyjny"
  ]
  node [
    id 123
    label "przedstawicielstwo"
  ]
  node [
    id 124
    label "ZMP"
  ]
  node [
    id 125
    label "Cepelia"
  ]
  node [
    id 126
    label "GOPR"
  ]
  node [
    id 127
    label "endecki"
  ]
  node [
    id 128
    label "ZBoWiD"
  ]
  node [
    id 129
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 130
    label "boj&#243;wka"
  ]
  node [
    id 131
    label "ZOMO"
  ]
  node [
    id 132
    label "zesp&#243;&#322;"
  ]
  node [
    id 133
    label "jednostka_organizacyjna"
  ]
  node [
    id 134
    label "centrala"
  ]
  node [
    id 135
    label "charakter"
  ]
  node [
    id 136
    label "mentalno&#347;&#263;"
  ]
  node [
    id 137
    label "superego"
  ]
  node [
    id 138
    label "cecha"
  ]
  node [
    id 139
    label "self"
  ]
  node [
    id 140
    label "wn&#281;trze"
  ]
  node [
    id 141
    label "psychika"
  ]
  node [
    id 142
    label "wyj&#261;tkowy"
  ]
  node [
    id 143
    label "status"
  ]
  node [
    id 144
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 145
    label "kanonistyka"
  ]
  node [
    id 146
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 147
    label "kazuistyka"
  ]
  node [
    id 148
    label "legislacyjnie"
  ]
  node [
    id 149
    label "zasada_d'Alemberta"
  ]
  node [
    id 150
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 151
    label "procesualistyka"
  ]
  node [
    id 152
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 153
    label "prawo_karne"
  ]
  node [
    id 154
    label "opis"
  ]
  node [
    id 155
    label "regu&#322;a_Allena"
  ]
  node [
    id 156
    label "kryminalistyka"
  ]
  node [
    id 157
    label "prawo_Mendla"
  ]
  node [
    id 158
    label "criterion"
  ]
  node [
    id 159
    label "standard"
  ]
  node [
    id 160
    label "obserwacja"
  ]
  node [
    id 161
    label "szko&#322;a"
  ]
  node [
    id 162
    label "kultura_duchowa"
  ]
  node [
    id 163
    label "normatywizm"
  ]
  node [
    id 164
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 165
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 166
    label "umocowa&#263;"
  ]
  node [
    id 167
    label "cywilistyka"
  ]
  node [
    id 168
    label "jurisprudence"
  ]
  node [
    id 169
    label "regu&#322;a_Glogera"
  ]
  node [
    id 170
    label "kryminologia"
  ]
  node [
    id 171
    label "zasada"
  ]
  node [
    id 172
    label "law"
  ]
  node [
    id 173
    label "qualification"
  ]
  node [
    id 174
    label "judykatura"
  ]
  node [
    id 175
    label "przepis"
  ]
  node [
    id 176
    label "prawo_karne_procesowe"
  ]
  node [
    id 177
    label "normalizacja"
  ]
  node [
    id 178
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 179
    label "wykonawczy"
  ]
  node [
    id 180
    label "dominion"
  ]
  node [
    id 181
    label "twierdzenie"
  ]
  node [
    id 182
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 183
    label "kierunek"
  ]
  node [
    id 184
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 185
    label "activity"
  ]
  node [
    id 186
    label "absolutorium"
  ]
  node [
    id 187
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 188
    label "dzia&#322;anie"
  ]
  node [
    id 189
    label "nakr&#281;canie"
  ]
  node [
    id 190
    label "nakr&#281;cenie"
  ]
  node [
    id 191
    label "zatrzymanie"
  ]
  node [
    id 192
    label "dzianie_si&#281;"
  ]
  node [
    id 193
    label "liczenie"
  ]
  node [
    id 194
    label "docieranie"
  ]
  node [
    id 195
    label "natural_process"
  ]
  node [
    id 196
    label "skutek"
  ]
  node [
    id 197
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 198
    label "w&#322;&#261;czanie"
  ]
  node [
    id 199
    label "liczy&#263;"
  ]
  node [
    id 200
    label "powodowanie"
  ]
  node [
    id 201
    label "w&#322;&#261;czenie"
  ]
  node [
    id 202
    label "rozpocz&#281;cie"
  ]
  node [
    id 203
    label "rezultat"
  ]
  node [
    id 204
    label "priorytet"
  ]
  node [
    id 205
    label "matematyka"
  ]
  node [
    id 206
    label "czynny"
  ]
  node [
    id 207
    label "uruchomienie"
  ]
  node [
    id 208
    label "podzia&#322;anie"
  ]
  node [
    id 209
    label "impact"
  ]
  node [
    id 210
    label "kampania"
  ]
  node [
    id 211
    label "kres"
  ]
  node [
    id 212
    label "podtrzymywanie"
  ]
  node [
    id 213
    label "tr&#243;jstronny"
  ]
  node [
    id 214
    label "funkcja"
  ]
  node [
    id 215
    label "act"
  ]
  node [
    id 216
    label "uruchamianie"
  ]
  node [
    id 217
    label "oferta"
  ]
  node [
    id 218
    label "rzut"
  ]
  node [
    id 219
    label "zadzia&#322;anie"
  ]
  node [
    id 220
    label "operacja"
  ]
  node [
    id 221
    label "wp&#322;yw"
  ]
  node [
    id 222
    label "zako&#324;czenie"
  ]
  node [
    id 223
    label "jednostka"
  ]
  node [
    id 224
    label "hipnotyzowanie"
  ]
  node [
    id 225
    label "operation"
  ]
  node [
    id 226
    label "supremum"
  ]
  node [
    id 227
    label "reakcja_chemiczna"
  ]
  node [
    id 228
    label "robienie"
  ]
  node [
    id 229
    label "infimum"
  ]
  node [
    id 230
    label "wdzieranie_si&#281;"
  ]
  node [
    id 231
    label "ocena"
  ]
  node [
    id 232
    label "uko&#324;czenie"
  ]
  node [
    id 233
    label "graduation"
  ]
  node [
    id 234
    label "kapita&#322;"
  ]
  node [
    id 235
    label "zaleta"
  ]
  node [
    id 236
    label "korzy&#347;&#263;"
  ]
  node [
    id 237
    label "py&#322;ek"
  ]
  node [
    id 238
    label "nektar"
  ]
  node [
    id 239
    label "surowiec"
  ]
  node [
    id 240
    label "dobro"
  ]
  node [
    id 241
    label "spad&#378;"
  ]
  node [
    id 242
    label "rzecz"
  ]
  node [
    id 243
    label "krzywa_Engla"
  ]
  node [
    id 244
    label "kalokagatia"
  ]
  node [
    id 245
    label "dobra"
  ]
  node [
    id 246
    label "dobro&#263;"
  ]
  node [
    id 247
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 248
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 249
    label "cel"
  ]
  node [
    id 250
    label "go&#322;&#261;bek"
  ]
  node [
    id 251
    label "warto&#347;&#263;"
  ]
  node [
    id 252
    label "despond"
  ]
  node [
    id 253
    label "g&#322;agolica"
  ]
  node [
    id 254
    label "litera"
  ]
  node [
    id 255
    label "tworzywo"
  ]
  node [
    id 256
    label "sk&#322;adnik"
  ]
  node [
    id 257
    label "rewaluowanie"
  ]
  node [
    id 258
    label "zrewaluowa&#263;"
  ]
  node [
    id 259
    label "rewaluowa&#263;"
  ]
  node [
    id 260
    label "wabik"
  ]
  node [
    id 261
    label "strona"
  ]
  node [
    id 262
    label "zrewaluowanie"
  ]
  node [
    id 263
    label "s&#322;odycz"
  ]
  node [
    id 264
    label "mityczny"
  ]
  node [
    id 265
    label "sok"
  ]
  node [
    id 266
    label "nap&#243;j"
  ]
  node [
    id 267
    label "amrita"
  ]
  node [
    id 268
    label "ciecz"
  ]
  node [
    id 269
    label "pylnik"
  ]
  node [
    id 270
    label "drobina"
  ]
  node [
    id 271
    label "py&#322;"
  ]
  node [
    id 272
    label "spot"
  ]
  node [
    id 273
    label "egzyna"
  ]
  node [
    id 274
    label "znami&#281;"
  ]
  node [
    id 275
    label "melitofag"
  ]
  node [
    id 276
    label "paproch"
  ]
  node [
    id 277
    label "upublicznienie"
  ]
  node [
    id 278
    label "publicznie"
  ]
  node [
    id 279
    label "upublicznianie"
  ]
  node [
    id 280
    label "jawny"
  ]
  node [
    id 281
    label "jawnie"
  ]
  node [
    id 282
    label "udost&#281;pnianie"
  ]
  node [
    id 283
    label "ujawnianie"
  ]
  node [
    id 284
    label "ujawnienie_si&#281;"
  ]
  node [
    id 285
    label "ujawnianie_si&#281;"
  ]
  node [
    id 286
    label "ewidentny"
  ]
  node [
    id 287
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 288
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 289
    label "zdecydowany"
  ]
  node [
    id 290
    label "znajomy"
  ]
  node [
    id 291
    label "ujawnienie"
  ]
  node [
    id 292
    label "u&#322;atwienie"
  ]
  node [
    id 293
    label "pocieszenie"
  ]
  node [
    id 294
    label "telefon_zaufania"
  ]
  node [
    id 295
    label "&#347;rodek"
  ]
  node [
    id 296
    label "darowizna"
  ]
  node [
    id 297
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 298
    label "comfort"
  ]
  node [
    id 299
    label "support"
  ]
  node [
    id 300
    label "pomoc"
  ]
  node [
    id 301
    label "dar"
  ]
  node [
    id 302
    label "zapomoga"
  ]
  node [
    id 303
    label "oparcie"
  ]
  node [
    id 304
    label "income"
  ]
  node [
    id 305
    label "stopa_procentowa"
  ]
  node [
    id 306
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 307
    label "da&#324;"
  ]
  node [
    id 308
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 309
    label "dyspozycja"
  ]
  node [
    id 310
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 311
    label "faculty"
  ]
  node [
    id 312
    label "stygmat"
  ]
  node [
    id 313
    label "przedmiot"
  ]
  node [
    id 314
    label "property"
  ]
  node [
    id 315
    label "liga"
  ]
  node [
    id 316
    label "zgodzi&#263;"
  ]
  node [
    id 317
    label "pomocnik"
  ]
  node [
    id 318
    label "grupa"
  ]
  node [
    id 319
    label "pomo&#380;enie"
  ]
  node [
    id 320
    label "ukojenie"
  ]
  node [
    id 321
    label "facilitation"
  ]
  node [
    id 322
    label "ulepszenie"
  ]
  node [
    id 323
    label "punkt"
  ]
  node [
    id 324
    label "spos&#243;b"
  ]
  node [
    id 325
    label "chemikalia"
  ]
  node [
    id 326
    label "abstrakcja"
  ]
  node [
    id 327
    label "miejsce"
  ]
  node [
    id 328
    label "czas"
  ]
  node [
    id 329
    label "substancja"
  ]
  node [
    id 330
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 331
    label "podstawa"
  ]
  node [
    id 332
    label "zaczerpni&#281;cie"
  ]
  node [
    id 333
    label "back"
  ]
  node [
    id 334
    label "anchor"
  ]
  node [
    id 335
    label "ustawienie"
  ]
  node [
    id 336
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 337
    label "podpora"
  ]
  node [
    id 338
    label "transakcja"
  ]
  node [
    id 339
    label "przeniesienie_praw"
  ]
  node [
    id 340
    label "wykonawca"
  ]
  node [
    id 341
    label "interpretator"
  ]
  node [
    id 342
    label "wprowadzanie"
  ]
  node [
    id 343
    label "g&#243;rowanie"
  ]
  node [
    id 344
    label "ukierunkowywanie"
  ]
  node [
    id 345
    label "poprowadzenie"
  ]
  node [
    id 346
    label "eksponowanie"
  ]
  node [
    id 347
    label "doprowadzenie"
  ]
  node [
    id 348
    label "przeci&#261;ganie"
  ]
  node [
    id 349
    label "sterowanie"
  ]
  node [
    id 350
    label "dysponowanie"
  ]
  node [
    id 351
    label "kszta&#322;towanie"
  ]
  node [
    id 352
    label "management"
  ]
  node [
    id 353
    label "trzymanie"
  ]
  node [
    id 354
    label "dawanie"
  ]
  node [
    id 355
    label "linia_melodyczna"
  ]
  node [
    id 356
    label "prowadzi&#263;"
  ]
  node [
    id 357
    label "drive"
  ]
  node [
    id 358
    label "wprowadzenie"
  ]
  node [
    id 359
    label "przywodzenie"
  ]
  node [
    id 360
    label "aim"
  ]
  node [
    id 361
    label "lead"
  ]
  node [
    id 362
    label "oprowadzenie"
  ]
  node [
    id 363
    label "prowadzanie"
  ]
  node [
    id 364
    label "oprowadzanie"
  ]
  node [
    id 365
    label "przewy&#380;szanie"
  ]
  node [
    id 366
    label "kierowanie"
  ]
  node [
    id 367
    label "zwracanie"
  ]
  node [
    id 368
    label "przeci&#281;cie"
  ]
  node [
    id 369
    label "granie"
  ]
  node [
    id 370
    label "ta&#324;czenie"
  ]
  node [
    id 371
    label "kre&#347;lenie"
  ]
  node [
    id 372
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 373
    label "zaprowadzanie"
  ]
  node [
    id 374
    label "pozarz&#261;dzanie"
  ]
  node [
    id 375
    label "krzywa"
  ]
  node [
    id 376
    label "przecinanie"
  ]
  node [
    id 377
    label "doprowadzanie"
  ]
  node [
    id 378
    label "pokre&#347;lenie"
  ]
  node [
    id 379
    label "usuwanie"
  ]
  node [
    id 380
    label "opowiadanie"
  ]
  node [
    id 381
    label "anointing"
  ]
  node [
    id 382
    label "sporz&#261;dzanie"
  ]
  node [
    id 383
    label "skre&#347;lanie"
  ]
  node [
    id 384
    label "uniewa&#380;nianie"
  ]
  node [
    id 385
    label "przygotowywanie"
  ]
  node [
    id 386
    label "skazany"
  ]
  node [
    id 387
    label "zarz&#261;dzanie"
  ]
  node [
    id 388
    label "rozporz&#261;dzanie"
  ]
  node [
    id 389
    label "namaszczenie_chorych"
  ]
  node [
    id 390
    label "dysponowanie_si&#281;"
  ]
  node [
    id 391
    label "disposal"
  ]
  node [
    id 392
    label "rozdysponowywanie"
  ]
  node [
    id 393
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 394
    label "fabrication"
  ]
  node [
    id 395
    label "tentegowanie"
  ]
  node [
    id 396
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 397
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 398
    label "porobienie"
  ]
  node [
    id 399
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 400
    label "creation"
  ]
  node [
    id 401
    label "bezproblemowy"
  ]
  node [
    id 402
    label "wydarzenie"
  ]
  node [
    id 403
    label "podkre&#347;lanie"
  ]
  node [
    id 404
    label "radiation"
  ]
  node [
    id 405
    label "demonstrowanie"
  ]
  node [
    id 406
    label "napromieniowywanie"
  ]
  node [
    id 407
    label "uwydatnianie_si&#281;"
  ]
  node [
    id 408
    label "przeorientowanie"
  ]
  node [
    id 409
    label "oznaczanie"
  ]
  node [
    id 410
    label "orientation"
  ]
  node [
    id 411
    label "strike"
  ]
  node [
    id 412
    label "dominance"
  ]
  node [
    id 413
    label "wygrywanie"
  ]
  node [
    id 414
    label "lepszy"
  ]
  node [
    id 415
    label "przesterowanie"
  ]
  node [
    id 416
    label "steering"
  ]
  node [
    id 417
    label "obs&#322;ugiwanie"
  ]
  node [
    id 418
    label "control"
  ]
  node [
    id 419
    label "powracanie"
  ]
  node [
    id 420
    label "wydalanie"
  ]
  node [
    id 421
    label "haftowanie"
  ]
  node [
    id 422
    label "przekazywanie"
  ]
  node [
    id 423
    label "vomit"
  ]
  node [
    id 424
    label "przeznaczanie"
  ]
  node [
    id 425
    label "ustawianie"
  ]
  node [
    id 426
    label "znajdowanie_si&#281;"
  ]
  node [
    id 427
    label "provision"
  ]
  node [
    id 428
    label "supply"
  ]
  node [
    id 429
    label "spe&#322;nianie"
  ]
  node [
    id 430
    label "montowanie"
  ]
  node [
    id 431
    label "wzbudzanie"
  ]
  node [
    id 432
    label "rozwijanie"
  ]
  node [
    id 433
    label "training"
  ]
  node [
    id 434
    label "formation"
  ]
  node [
    id 435
    label "cause"
  ]
  node [
    id 436
    label "causal_agent"
  ]
  node [
    id 437
    label "nakierowanie_si&#281;"
  ]
  node [
    id 438
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 439
    label "wysy&#322;anie"
  ]
  node [
    id 440
    label "wyre&#380;yserowanie"
  ]
  node [
    id 441
    label "re&#380;yserowanie"
  ]
  node [
    id 442
    label "linia"
  ]
  node [
    id 443
    label "curve"
  ]
  node [
    id 444
    label "curvature"
  ]
  node [
    id 445
    label "figura_geometryczna"
  ]
  node [
    id 446
    label "poprowadzi&#263;"
  ]
  node [
    id 447
    label "sterczenie"
  ]
  node [
    id 448
    label "eksponowa&#263;"
  ]
  node [
    id 449
    label "g&#243;rowa&#263;"
  ]
  node [
    id 450
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 451
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 452
    label "sterowa&#263;"
  ]
  node [
    id 453
    label "kierowa&#263;"
  ]
  node [
    id 454
    label "string"
  ]
  node [
    id 455
    label "kre&#347;li&#263;"
  ]
  node [
    id 456
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 457
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 458
    label "&#380;y&#263;"
  ]
  node [
    id 459
    label "partner"
  ]
  node [
    id 460
    label "ukierunkowywa&#263;"
  ]
  node [
    id 461
    label "przesuwa&#263;"
  ]
  node [
    id 462
    label "tworzy&#263;"
  ]
  node [
    id 463
    label "powodowa&#263;"
  ]
  node [
    id 464
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 465
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 466
    label "message"
  ]
  node [
    id 467
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 468
    label "robi&#263;"
  ]
  node [
    id 469
    label "navigate"
  ]
  node [
    id 470
    label "manipulate"
  ]
  node [
    id 471
    label "akapit"
  ]
  node [
    id 472
    label "artyku&#322;"
  ]
  node [
    id 473
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 474
    label "pra&#380;enie"
  ]
  node [
    id 475
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 476
    label "urz&#261;dzanie"
  ]
  node [
    id 477
    label "p&#322;acenie"
  ]
  node [
    id 478
    label "puszczanie_si&#281;"
  ]
  node [
    id 479
    label "dodawanie"
  ]
  node [
    id 480
    label "wyst&#281;powanie"
  ]
  node [
    id 481
    label "bycie_w_posiadaniu"
  ]
  node [
    id 482
    label "communication"
  ]
  node [
    id 483
    label "&#322;adowanie"
  ]
  node [
    id 484
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 485
    label "nalewanie"
  ]
  node [
    id 486
    label "powierzanie"
  ]
  node [
    id 487
    label "zezwalanie"
  ]
  node [
    id 488
    label "giving"
  ]
  node [
    id 489
    label "obiecywanie"
  ]
  node [
    id 490
    label "odst&#281;powanie"
  ]
  node [
    id 491
    label "dostarczanie"
  ]
  node [
    id 492
    label "wymienianie_si&#281;"
  ]
  node [
    id 493
    label "emission"
  ]
  node [
    id 494
    label "administration"
  ]
  node [
    id 495
    label "umo&#380;liwianie"
  ]
  node [
    id 496
    label "pasowanie"
  ]
  node [
    id 497
    label "otwarcie"
  ]
  node [
    id 498
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 499
    label "playing"
  ]
  node [
    id 500
    label "rozgrywanie"
  ]
  node [
    id 501
    label "pretense"
  ]
  node [
    id 502
    label "dogranie"
  ]
  node [
    id 503
    label "zwalczenie"
  ]
  node [
    id 504
    label "lewa"
  ]
  node [
    id 505
    label "instrumentalizacja"
  ]
  node [
    id 506
    label "migotanie"
  ]
  node [
    id 507
    label "wydawanie"
  ]
  node [
    id 508
    label "ust&#281;powanie"
  ]
  node [
    id 509
    label "odegranie_si&#281;"
  ]
  node [
    id 510
    label "grywanie"
  ]
  node [
    id 511
    label "dogrywanie"
  ]
  node [
    id 512
    label "wybijanie"
  ]
  node [
    id 513
    label "wykonywanie"
  ]
  node [
    id 514
    label "pogranie"
  ]
  node [
    id 515
    label "brzmienie"
  ]
  node [
    id 516
    label "na&#347;ladowanie"
  ]
  node [
    id 517
    label "przygrywanie"
  ]
  node [
    id 518
    label "nagranie_si&#281;"
  ]
  node [
    id 519
    label "wyr&#243;wnywanie"
  ]
  node [
    id 520
    label "rozegranie_si&#281;"
  ]
  node [
    id 521
    label "odgrywanie_si&#281;"
  ]
  node [
    id 522
    label "&#347;ciganie"
  ]
  node [
    id 523
    label "wyr&#243;wnanie"
  ]
  node [
    id 524
    label "pr&#243;bowanie"
  ]
  node [
    id 525
    label "szczekanie"
  ]
  node [
    id 526
    label "gra_w_karty"
  ]
  node [
    id 527
    label "przedstawianie"
  ]
  node [
    id 528
    label "instrument_muzyczny"
  ]
  node [
    id 529
    label "glitter"
  ]
  node [
    id 530
    label "igranie"
  ]
  node [
    id 531
    label "wybicie"
  ]
  node [
    id 532
    label "mienienie_si&#281;"
  ]
  node [
    id 533
    label "prezentowanie"
  ]
  node [
    id 534
    label "rola"
  ]
  node [
    id 535
    label "staranie_si&#281;"
  ]
  node [
    id 536
    label "wytyczenie"
  ]
  node [
    id 537
    label "nakre&#347;lenie"
  ]
  node [
    id 538
    label "proverb"
  ]
  node [
    id 539
    label "guidance"
  ]
  node [
    id 540
    label "nata&#324;czenie_si&#281;"
  ]
  node [
    id 541
    label "dancing"
  ]
  node [
    id 542
    label "poruszanie_si&#281;"
  ]
  node [
    id 543
    label "chwianie_si&#281;"
  ]
  node [
    id 544
    label "rozta&#324;czenie_si&#281;"
  ]
  node [
    id 545
    label "przeta&#324;czenie"
  ]
  node [
    id 546
    label "gibanie"
  ]
  node [
    id 547
    label "pota&#324;czenie"
  ]
  node [
    id 548
    label "pokazywanie"
  ]
  node [
    id 549
    label "zaczynanie"
  ]
  node [
    id 550
    label "initiation"
  ]
  node [
    id 551
    label "umieszczanie"
  ]
  node [
    id 552
    label "zak&#322;&#243;canie"
  ]
  node [
    id 553
    label "retraction"
  ]
  node [
    id 554
    label "zapoznawanie"
  ]
  node [
    id 555
    label "wpisywanie"
  ]
  node [
    id 556
    label "przewietrzanie"
  ]
  node [
    id 557
    label "trigger"
  ]
  node [
    id 558
    label "mental_hospital"
  ]
  node [
    id 559
    label "rynek"
  ]
  node [
    id 560
    label "wchodzenie"
  ]
  node [
    id 561
    label "sp&#281;dzenie"
  ]
  node [
    id 562
    label "spowodowanie"
  ]
  node [
    id 563
    label "wzbudzenie"
  ]
  node [
    id 564
    label "zainstalowanie"
  ]
  node [
    id 565
    label "spe&#322;nienie"
  ]
  node [
    id 566
    label "znalezienie_si&#281;"
  ]
  node [
    id 567
    label "introduction"
  ]
  node [
    id 568
    label "pos&#322;anie"
  ]
  node [
    id 569
    label "adduction"
  ]
  node [
    id 570
    label "sk&#322;anianie"
  ]
  node [
    id 571
    label "przypominanie"
  ]
  node [
    id 572
    label "kojarzenie_si&#281;"
  ]
  node [
    id 573
    label "pokazanie"
  ]
  node [
    id 574
    label "umo&#380;liwienie"
  ]
  node [
    id 575
    label "wej&#347;cie"
  ]
  node [
    id 576
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 577
    label "evocation"
  ]
  node [
    id 578
    label "nuklearyzacja"
  ]
  node [
    id 579
    label "zacz&#281;cie"
  ]
  node [
    id 580
    label "zapoznanie"
  ]
  node [
    id 581
    label "podstawy"
  ]
  node [
    id 582
    label "wst&#281;p"
  ]
  node [
    id 583
    label "entrance"
  ]
  node [
    id 584
    label "wpisanie"
  ]
  node [
    id 585
    label "deduction"
  ]
  node [
    id 586
    label "umieszczenie"
  ]
  node [
    id 587
    label "issue"
  ]
  node [
    id 588
    label "przewietrzenie"
  ]
  node [
    id 589
    label "przed&#322;u&#380;anie"
  ]
  node [
    id 590
    label "przemieszczanie"
  ]
  node [
    id 591
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 592
    label "rozci&#261;ganie"
  ]
  node [
    id 593
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 594
    label "wymawianie"
  ]
  node [
    id 595
    label "j&#261;kanie"
  ]
  node [
    id 596
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 597
    label "przymocowywanie"
  ]
  node [
    id 598
    label "pull"
  ]
  node [
    id 599
    label "przesuwanie"
  ]
  node [
    id 600
    label "zaci&#261;ganie"
  ]
  node [
    id 601
    label "przetykanie"
  ]
  node [
    id 602
    label "podzielenie"
  ]
  node [
    id 603
    label "poprzecinanie"
  ]
  node [
    id 604
    label "przerwanie"
  ]
  node [
    id 605
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 606
    label "w&#281;ze&#322;"
  ]
  node [
    id 607
    label "zranienie"
  ]
  node [
    id 608
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 609
    label "cut"
  ]
  node [
    id 610
    label "snub"
  ]
  node [
    id 611
    label "carving"
  ]
  node [
    id 612
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 613
    label "time"
  ]
  node [
    id 614
    label "kaleczenie"
  ]
  node [
    id 615
    label "przerywanie"
  ]
  node [
    id 616
    label "dzielenie"
  ]
  node [
    id 617
    label "intersection"
  ]
  node [
    id 618
    label "film_editing"
  ]
  node [
    id 619
    label "wypuszczanie"
  ]
  node [
    id 620
    label "noszenie"
  ]
  node [
    id 621
    label "przetrzymanie"
  ]
  node [
    id 622
    label "przetrzymywanie"
  ]
  node [
    id 623
    label "hodowanie"
  ]
  node [
    id 624
    label "wypuszczenie"
  ]
  node [
    id 625
    label "niesienie"
  ]
  node [
    id 626
    label "zmuszanie"
  ]
  node [
    id 627
    label "sprawowanie"
  ]
  node [
    id 628
    label "poise"
  ]
  node [
    id 629
    label "uniemo&#380;liwianie"
  ]
  node [
    id 630
    label "dzier&#380;enie"
  ]
  node [
    id 631
    label "clasp"
  ]
  node [
    id 632
    label "zachowywanie"
  ]
  node [
    id 633
    label "detention"
  ]
  node [
    id 634
    label "potrzymanie"
  ]
  node [
    id 635
    label "retention"
  ]
  node [
    id 636
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 637
    label "warunki"
  ]
  node [
    id 638
    label "zal&#261;&#380;ek"
  ]
  node [
    id 639
    label "skupisko"
  ]
  node [
    id 640
    label "Hollywood"
  ]
  node [
    id 641
    label "center"
  ]
  node [
    id 642
    label "instytucja"
  ]
  node [
    id 643
    label "otoczenie"
  ]
  node [
    id 644
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 645
    label "sytuacja"
  ]
  node [
    id 646
    label "background"
  ]
  node [
    id 647
    label "okrycie"
  ]
  node [
    id 648
    label "zdarzenie_si&#281;"
  ]
  node [
    id 649
    label "cortege"
  ]
  node [
    id 650
    label "huczek"
  ]
  node [
    id 651
    label "okolica"
  ]
  node [
    id 652
    label "class"
  ]
  node [
    id 653
    label "crack"
  ]
  node [
    id 654
    label "Wielki_Atraktor"
  ]
  node [
    id 655
    label "zbi&#243;r"
  ]
  node [
    id 656
    label "przestrze&#324;"
  ]
  node [
    id 657
    label "rz&#261;d"
  ]
  node [
    id 658
    label "uwaga"
  ]
  node [
    id 659
    label "praca"
  ]
  node [
    id 660
    label "plac"
  ]
  node [
    id 661
    label "location"
  ]
  node [
    id 662
    label "warunek_lokalowy"
  ]
  node [
    id 663
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 664
    label "cia&#322;o"
  ]
  node [
    id 665
    label "chwila"
  ]
  node [
    id 666
    label "poj&#281;cie"
  ]
  node [
    id 667
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 668
    label "afiliowa&#263;"
  ]
  node [
    id 669
    label "establishment"
  ]
  node [
    id 670
    label "zamyka&#263;"
  ]
  node [
    id 671
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 672
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 673
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 674
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 675
    label "Fundusze_Unijne"
  ]
  node [
    id 676
    label "biuro"
  ]
  node [
    id 677
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 678
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 679
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 680
    label "zamykanie"
  ]
  node [
    id 681
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 682
    label "osoba_prawna"
  ]
  node [
    id 683
    label "urz&#261;d"
  ]
  node [
    id 684
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 685
    label "zar&#243;d&#378;"
  ]
  node [
    id 686
    label "pocz&#261;tek"
  ]
  node [
    id 687
    label "integument"
  ]
  node [
    id 688
    label "organ"
  ]
  node [
    id 689
    label "Los_Angeles"
  ]
  node [
    id 690
    label "rozgrywaj&#261;cy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 10
    target 11
  ]
]
