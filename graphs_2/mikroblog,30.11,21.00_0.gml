graph [
  node [
    id 0
    label "echo"
    origin "text"
  ]
  node [
    id 1
    label "policja"
    origin "text"
  ]
  node [
    id 2
    label "resonance"
  ]
  node [
    id 3
    label "zjawisko"
  ]
  node [
    id 4
    label "proces"
  ]
  node [
    id 5
    label "boski"
  ]
  node [
    id 6
    label "krajobraz"
  ]
  node [
    id 7
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 8
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 9
    label "przywidzenie"
  ]
  node [
    id 10
    label "presence"
  ]
  node [
    id 11
    label "charakter"
  ]
  node [
    id 12
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 13
    label "organ"
  ]
  node [
    id 14
    label "grupa"
  ]
  node [
    id 15
    label "komisariat"
  ]
  node [
    id 16
    label "s&#322;u&#380;ba"
  ]
  node [
    id 17
    label "posterunek"
  ]
  node [
    id 18
    label "psiarnia"
  ]
  node [
    id 19
    label "awansowa&#263;"
  ]
  node [
    id 20
    label "stawia&#263;"
  ]
  node [
    id 21
    label "wakowa&#263;"
  ]
  node [
    id 22
    label "powierzanie"
  ]
  node [
    id 23
    label "postawi&#263;"
  ]
  node [
    id 24
    label "pozycja"
  ]
  node [
    id 25
    label "agencja"
  ]
  node [
    id 26
    label "awansowanie"
  ]
  node [
    id 27
    label "warta"
  ]
  node [
    id 28
    label "praca"
  ]
  node [
    id 29
    label "tkanka"
  ]
  node [
    id 30
    label "jednostka_organizacyjna"
  ]
  node [
    id 31
    label "budowa"
  ]
  node [
    id 32
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 33
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 34
    label "tw&#243;r"
  ]
  node [
    id 35
    label "organogeneza"
  ]
  node [
    id 36
    label "zesp&#243;&#322;"
  ]
  node [
    id 37
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 38
    label "struktura_anatomiczna"
  ]
  node [
    id 39
    label "uk&#322;ad"
  ]
  node [
    id 40
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 41
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 42
    label "Izba_Konsyliarska"
  ]
  node [
    id 43
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 44
    label "stomia"
  ]
  node [
    id 45
    label "dekortykacja"
  ]
  node [
    id 46
    label "okolica"
  ]
  node [
    id 47
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 48
    label "Komitet_Region&#243;w"
  ]
  node [
    id 49
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 50
    label "instytucja"
  ]
  node [
    id 51
    label "wys&#322;uga"
  ]
  node [
    id 52
    label "service"
  ]
  node [
    id 53
    label "czworak"
  ]
  node [
    id 54
    label "ZOMO"
  ]
  node [
    id 55
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 56
    label "odm&#322;adzanie"
  ]
  node [
    id 57
    label "liga"
  ]
  node [
    id 58
    label "jednostka_systematyczna"
  ]
  node [
    id 59
    label "asymilowanie"
  ]
  node [
    id 60
    label "gromada"
  ]
  node [
    id 61
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 62
    label "asymilowa&#263;"
  ]
  node [
    id 63
    label "egzemplarz"
  ]
  node [
    id 64
    label "Entuzjastki"
  ]
  node [
    id 65
    label "zbi&#243;r"
  ]
  node [
    id 66
    label "kompozycja"
  ]
  node [
    id 67
    label "Terranie"
  ]
  node [
    id 68
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 69
    label "category"
  ]
  node [
    id 70
    label "pakiet_klimatyczny"
  ]
  node [
    id 71
    label "oddzia&#322;"
  ]
  node [
    id 72
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 73
    label "cz&#261;steczka"
  ]
  node [
    id 74
    label "stage_set"
  ]
  node [
    id 75
    label "type"
  ]
  node [
    id 76
    label "specgrupa"
  ]
  node [
    id 77
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 78
    label "&#346;wietliki"
  ]
  node [
    id 79
    label "odm&#322;odzenie"
  ]
  node [
    id 80
    label "Eurogrupa"
  ]
  node [
    id 81
    label "odm&#322;adza&#263;"
  ]
  node [
    id 82
    label "formacja_geologiczna"
  ]
  node [
    id 83
    label "harcerze_starsi"
  ]
  node [
    id 84
    label "urz&#261;d"
  ]
  node [
    id 85
    label "jednostka"
  ]
  node [
    id 86
    label "czasowy"
  ]
  node [
    id 87
    label "commissariat"
  ]
  node [
    id 88
    label "rewir"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
]
