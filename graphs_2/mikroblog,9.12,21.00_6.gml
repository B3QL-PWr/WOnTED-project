graph [
  node [
    id 0
    label "matematyk"
    origin "text"
  ]
  node [
    id 1
    label "magia"
    origin "text"
  ]
  node [
    id 2
    label "nauka"
    origin "text"
  ]
  node [
    id 3
    label "nauczyciel"
  ]
  node [
    id 4
    label "Kartezjusz"
  ]
  node [
    id 5
    label "Ptolemeusz"
  ]
  node [
    id 6
    label "Biot"
  ]
  node [
    id 7
    label "Archimedes"
  ]
  node [
    id 8
    label "Berkeley"
  ]
  node [
    id 9
    label "Gauss"
  ]
  node [
    id 10
    label "Fourier"
  ]
  node [
    id 11
    label "Kepler"
  ]
  node [
    id 12
    label "Bayes"
  ]
  node [
    id 13
    label "Doppler"
  ]
  node [
    id 14
    label "Borel"
  ]
  node [
    id 15
    label "Laplace"
  ]
  node [
    id 16
    label "Euklides"
  ]
  node [
    id 17
    label "naukowiec"
  ]
  node [
    id 18
    label "Galileusz"
  ]
  node [
    id 19
    label "Pitagoras"
  ]
  node [
    id 20
    label "Newton"
  ]
  node [
    id 21
    label "Pascal"
  ]
  node [
    id 22
    label "Maxwell"
  ]
  node [
    id 23
    label "&#347;ledziciel"
  ]
  node [
    id 24
    label "uczony"
  ]
  node [
    id 25
    label "Miczurin"
  ]
  node [
    id 26
    label "belfer"
  ]
  node [
    id 27
    label "kszta&#322;ciciel"
  ]
  node [
    id 28
    label "preceptor"
  ]
  node [
    id 29
    label "pedagog"
  ]
  node [
    id 30
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 31
    label "szkolnik"
  ]
  node [
    id 32
    label "profesor"
  ]
  node [
    id 33
    label "popularyzator"
  ]
  node [
    id 34
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 35
    label "filozofia"
  ]
  node [
    id 36
    label "wyra&#378;no&#347;&#263;"
  ]
  node [
    id 37
    label "rozci&#261;g&#322;y"
  ]
  node [
    id 38
    label "pogl&#261;dy"
  ]
  node [
    id 39
    label "dorobek"
  ]
  node [
    id 40
    label "j&#281;zyk_programowania"
  ]
  node [
    id 41
    label "ideologia"
  ]
  node [
    id 42
    label "pitagorejczyk"
  ]
  node [
    id 43
    label "czar"
  ]
  node [
    id 44
    label "praktyki"
  ]
  node [
    id 45
    label "czarodziej"
  ]
  node [
    id 46
    label "czarodziejka"
  ]
  node [
    id 47
    label "zjawisko"
  ]
  node [
    id 48
    label "cecha"
  ]
  node [
    id 49
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 50
    label "wikkanin"
  ]
  node [
    id 51
    label "agreeableness"
  ]
  node [
    id 52
    label "czarownica"
  ]
  node [
    id 53
    label "practice"
  ]
  node [
    id 54
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 55
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 56
    label "proces"
  ]
  node [
    id 57
    label "boski"
  ]
  node [
    id 58
    label "krajobraz"
  ]
  node [
    id 59
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 60
    label "przywidzenie"
  ]
  node [
    id 61
    label "presence"
  ]
  node [
    id 62
    label "charakter"
  ]
  node [
    id 63
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 64
    label "charakterystyka"
  ]
  node [
    id 65
    label "m&#322;ot"
  ]
  node [
    id 66
    label "znak"
  ]
  node [
    id 67
    label "drzewo"
  ]
  node [
    id 68
    label "pr&#243;ba"
  ]
  node [
    id 69
    label "attribute"
  ]
  node [
    id 70
    label "marka"
  ]
  node [
    id 71
    label "rzuci&#263;"
  ]
  node [
    id 72
    label "zakl&#281;cie"
  ]
  node [
    id 73
    label "rzucenie"
  ]
  node [
    id 74
    label "attraction"
  ]
  node [
    id 75
    label "rzuca&#263;"
  ]
  node [
    id 76
    label "rzucanie"
  ]
  node [
    id 77
    label "Gandalf"
  ]
  node [
    id 78
    label "licz"
  ]
  node [
    id 79
    label "rzadko&#347;&#263;"
  ]
  node [
    id 80
    label "czarownik"
  ]
  node [
    id 81
    label "Saruman"
  ]
  node [
    id 82
    label "Harry_Potter"
  ]
  node [
    id 83
    label "istota_fantastyczna"
  ]
  node [
    id 84
    label "zo&#322;za"
  ]
  node [
    id 85
    label "Baba_Jaga"
  ]
  node [
    id 86
    label "kobieta"
  ]
  node [
    id 87
    label "neopoganin"
  ]
  node [
    id 88
    label "Meluzyna"
  ]
  node [
    id 89
    label "cz&#322;owiek"
  ]
  node [
    id 90
    label "wiedza"
  ]
  node [
    id 91
    label "miasteczko_rowerowe"
  ]
  node [
    id 92
    label "porada"
  ]
  node [
    id 93
    label "fotowoltaika"
  ]
  node [
    id 94
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 95
    label "przem&#243;wienie"
  ]
  node [
    id 96
    label "nauki_o_poznaniu"
  ]
  node [
    id 97
    label "nomotetyczny"
  ]
  node [
    id 98
    label "systematyka"
  ]
  node [
    id 99
    label "typologia"
  ]
  node [
    id 100
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 101
    label "kultura_duchowa"
  ]
  node [
    id 102
    label "&#322;awa_szkolna"
  ]
  node [
    id 103
    label "nauki_penalne"
  ]
  node [
    id 104
    label "dziedzina"
  ]
  node [
    id 105
    label "imagineskopia"
  ]
  node [
    id 106
    label "teoria_naukowa"
  ]
  node [
    id 107
    label "inwentyka"
  ]
  node [
    id 108
    label "metodologia"
  ]
  node [
    id 109
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 110
    label "nauki_o_Ziemi"
  ]
  node [
    id 111
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 112
    label "sfera"
  ]
  node [
    id 113
    label "zbi&#243;r"
  ]
  node [
    id 114
    label "zakres"
  ]
  node [
    id 115
    label "funkcja"
  ]
  node [
    id 116
    label "bezdro&#380;e"
  ]
  node [
    id 117
    label "poddzia&#322;"
  ]
  node [
    id 118
    label "kognicja"
  ]
  node [
    id 119
    label "przebieg"
  ]
  node [
    id 120
    label "rozprawa"
  ]
  node [
    id 121
    label "wydarzenie"
  ]
  node [
    id 122
    label "legislacyjnie"
  ]
  node [
    id 123
    label "przes&#322;anka"
  ]
  node [
    id 124
    label "nast&#281;pstwo"
  ]
  node [
    id 125
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 126
    label "zrozumienie"
  ]
  node [
    id 127
    label "obronienie"
  ]
  node [
    id 128
    label "wydanie"
  ]
  node [
    id 129
    label "wyg&#322;oszenie"
  ]
  node [
    id 130
    label "wypowied&#378;"
  ]
  node [
    id 131
    label "oddzia&#322;anie"
  ]
  node [
    id 132
    label "address"
  ]
  node [
    id 133
    label "wydobycie"
  ]
  node [
    id 134
    label "wyst&#261;pienie"
  ]
  node [
    id 135
    label "talk"
  ]
  node [
    id 136
    label "odzyskanie"
  ]
  node [
    id 137
    label "sermon"
  ]
  node [
    id 138
    label "cognition"
  ]
  node [
    id 139
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 140
    label "intelekt"
  ]
  node [
    id 141
    label "pozwolenie"
  ]
  node [
    id 142
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 143
    label "zaawansowanie"
  ]
  node [
    id 144
    label "wykszta&#322;cenie"
  ]
  node [
    id 145
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 146
    label "wskaz&#243;wka"
  ]
  node [
    id 147
    label "technika"
  ]
  node [
    id 148
    label "typology"
  ]
  node [
    id 149
    label "podzia&#322;"
  ]
  node [
    id 150
    label "kwantyfikacja"
  ]
  node [
    id 151
    label "aparat_krytyczny"
  ]
  node [
    id 152
    label "funkcjonalizm"
  ]
  node [
    id 153
    label "taksonomia"
  ]
  node [
    id 154
    label "biologia"
  ]
  node [
    id 155
    label "biosystematyka"
  ]
  node [
    id 156
    label "kohorta"
  ]
  node [
    id 157
    label "kladystyka"
  ]
  node [
    id 158
    label "wyobra&#378;nia"
  ]
  node [
    id 159
    label "charakterystyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
]
