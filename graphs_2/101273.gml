graph [
  node [
    id 0
    label "zielonka"
    origin "text"
  ]
  node [
    id 1
    label "wiesia"
    origin "text"
  ]
  node [
    id 2
    label "ptak_wodny"
  ]
  node [
    id 3
    label "ptak_w&#281;drowny"
  ]
  node [
    id 4
    label "g&#261;ska_zielonka"
  ]
  node [
    id 5
    label "chru&#347;ciele"
  ]
  node [
    id 6
    label "karma"
  ]
  node [
    id 7
    label "wys&#322;odki"
  ]
  node [
    id 8
    label "fura&#380;er"
  ]
  node [
    id 9
    label "proso"
  ]
  node [
    id 10
    label "feed"
  ]
  node [
    id 11
    label "jas&#322;o"
  ]
  node [
    id 12
    label "jedzenie"
  ]
  node [
    id 13
    label "podsumowanie"
  ]
  node [
    id 14
    label "czynno&#347;&#263;_karmiczna"
  ]
  node [
    id 15
    label "gniotownik"
  ]
  node [
    id 16
    label "&#380;urawiowe"
  ]
  node [
    id 17
    label "wie&#347;"
  ]
  node [
    id 18
    label "stary"
  ]
  node [
    id 19
    label "Babice"
  ]
  node [
    id 20
    label "warszawski"
  ]
  node [
    id 21
    label "zachodni"
  ]
  node [
    id 22
    label "parcela"
  ]
  node [
    id 23
    label "&#380;elazowy"
  ]
  node [
    id 24
    label "woli"
  ]
  node [
    id 25
    label "trakt"
  ]
  node [
    id 26
    label "kr&#243;lewski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
]
