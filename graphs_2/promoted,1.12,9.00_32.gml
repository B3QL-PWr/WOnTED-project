graph [
  node [
    id 0
    label "misia"
    origin "text"
  ]
  node [
    id 1
    label "polarny"
    origin "text"
  ]
  node [
    id 2
    label "opracowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 4
    label "niezawodny"
    origin "text"
  ]
  node [
    id 5
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 6
    label "suszy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wcale"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "r&#281;cznik"
    origin "text"
  ]
  node [
    id 10
    label "invent"
  ]
  node [
    id 11
    label "przygotowa&#263;"
  ]
  node [
    id 12
    label "set"
  ]
  node [
    id 13
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 14
    label "wykona&#263;"
  ]
  node [
    id 15
    label "cook"
  ]
  node [
    id 16
    label "wyszkoli&#263;"
  ]
  node [
    id 17
    label "train"
  ]
  node [
    id 18
    label "arrange"
  ]
  node [
    id 19
    label "zrobi&#263;"
  ]
  node [
    id 20
    label "spowodowa&#263;"
  ]
  node [
    id 21
    label "wytworzy&#263;"
  ]
  node [
    id 22
    label "dress"
  ]
  node [
    id 23
    label "ukierunkowa&#263;"
  ]
  node [
    id 24
    label "samodzielny"
  ]
  node [
    id 25
    label "zwi&#261;zany"
  ]
  node [
    id 26
    label "czyj&#347;"
  ]
  node [
    id 27
    label "swoisty"
  ]
  node [
    id 28
    label "osobny"
  ]
  node [
    id 29
    label "prywatny"
  ]
  node [
    id 30
    label "wyodr&#281;bnienie_si&#281;"
  ]
  node [
    id 31
    label "odr&#281;bny"
  ]
  node [
    id 32
    label "wydzielenie"
  ]
  node [
    id 33
    label "osobno"
  ]
  node [
    id 34
    label "kolejny"
  ]
  node [
    id 35
    label "inszy"
  ]
  node [
    id 36
    label "wyodr&#281;bnianie"
  ]
  node [
    id 37
    label "wyodr&#281;bnianie_si&#281;"
  ]
  node [
    id 38
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 39
    label "po&#322;&#261;czenie"
  ]
  node [
    id 40
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 41
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 42
    label "swoi&#347;cie"
  ]
  node [
    id 43
    label "sw&#243;j"
  ]
  node [
    id 44
    label "sobieradzki"
  ]
  node [
    id 45
    label "niepodleg&#322;y"
  ]
  node [
    id 46
    label "autonomicznie"
  ]
  node [
    id 47
    label "indywidualny"
  ]
  node [
    id 48
    label "samodzielnie"
  ]
  node [
    id 49
    label "pewny"
  ]
  node [
    id 50
    label "rzetelny"
  ]
  node [
    id 51
    label "niezawodnie"
  ]
  node [
    id 52
    label "mo&#380;liwy"
  ]
  node [
    id 53
    label "bezpieczny"
  ]
  node [
    id 54
    label "spokojny"
  ]
  node [
    id 55
    label "pewnie"
  ]
  node [
    id 56
    label "upewnianie_si&#281;"
  ]
  node [
    id 57
    label "ufanie"
  ]
  node [
    id 58
    label "wierzenie"
  ]
  node [
    id 59
    label "upewnienie_si&#281;"
  ]
  node [
    id 60
    label "wiarygodny"
  ]
  node [
    id 61
    label "rzetelnie"
  ]
  node [
    id 62
    label "przekonuj&#261;cy"
  ]
  node [
    id 63
    label "porz&#261;dny"
  ]
  node [
    id 64
    label "dobry"
  ]
  node [
    id 65
    label "model"
  ]
  node [
    id 66
    label "narz&#281;dzie"
  ]
  node [
    id 67
    label "zbi&#243;r"
  ]
  node [
    id 68
    label "tryb"
  ]
  node [
    id 69
    label "nature"
  ]
  node [
    id 70
    label "egzemplarz"
  ]
  node [
    id 71
    label "series"
  ]
  node [
    id 72
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 73
    label "uprawianie"
  ]
  node [
    id 74
    label "praca_rolnicza"
  ]
  node [
    id 75
    label "collection"
  ]
  node [
    id 76
    label "dane"
  ]
  node [
    id 77
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 78
    label "pakiet_klimatyczny"
  ]
  node [
    id 79
    label "poj&#281;cie"
  ]
  node [
    id 80
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 81
    label "sum"
  ]
  node [
    id 82
    label "gathering"
  ]
  node [
    id 83
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 84
    label "album"
  ]
  node [
    id 85
    label "&#347;rodek"
  ]
  node [
    id 86
    label "niezb&#281;dnik"
  ]
  node [
    id 87
    label "przedmiot"
  ]
  node [
    id 88
    label "cz&#322;owiek"
  ]
  node [
    id 89
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 90
    label "tylec"
  ]
  node [
    id 91
    label "urz&#261;dzenie"
  ]
  node [
    id 92
    label "ko&#322;o"
  ]
  node [
    id 93
    label "modalno&#347;&#263;"
  ]
  node [
    id 94
    label "z&#261;b"
  ]
  node [
    id 95
    label "cecha"
  ]
  node [
    id 96
    label "kategoria_gramatyczna"
  ]
  node [
    id 97
    label "skala"
  ]
  node [
    id 98
    label "funkcjonowa&#263;"
  ]
  node [
    id 99
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 100
    label "koniugacja"
  ]
  node [
    id 101
    label "prezenter"
  ]
  node [
    id 102
    label "typ"
  ]
  node [
    id 103
    label "mildew"
  ]
  node [
    id 104
    label "zi&#243;&#322;ko"
  ]
  node [
    id 105
    label "motif"
  ]
  node [
    id 106
    label "pozowanie"
  ]
  node [
    id 107
    label "ideal"
  ]
  node [
    id 108
    label "wz&#243;r"
  ]
  node [
    id 109
    label "matryca"
  ]
  node [
    id 110
    label "adaptation"
  ]
  node [
    id 111
    label "ruch"
  ]
  node [
    id 112
    label "pozowa&#263;"
  ]
  node [
    id 113
    label "imitacja"
  ]
  node [
    id 114
    label "orygina&#322;"
  ]
  node [
    id 115
    label "facet"
  ]
  node [
    id 116
    label "miniatura"
  ]
  node [
    id 117
    label "&#347;cina&#263;"
  ]
  node [
    id 118
    label "robi&#263;"
  ]
  node [
    id 119
    label "drain"
  ]
  node [
    id 120
    label "pemikan"
  ]
  node [
    id 121
    label "zmienia&#263;"
  ]
  node [
    id 122
    label "powodowa&#263;"
  ]
  node [
    id 123
    label "decapitate"
  ]
  node [
    id 124
    label "usuwa&#263;"
  ]
  node [
    id 125
    label "obni&#380;a&#263;"
  ]
  node [
    id 126
    label "unieruchamia&#263;"
  ]
  node [
    id 127
    label "parali&#380;owa&#263;"
  ]
  node [
    id 128
    label "ow&#322;osienie"
  ]
  node [
    id 129
    label "ci&#261;&#263;"
  ]
  node [
    id 130
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 131
    label "opitala&#263;"
  ]
  node [
    id 132
    label "obcina&#263;"
  ]
  node [
    id 133
    label "w&#322;osy"
  ]
  node [
    id 134
    label "pozbawia&#263;"
  ]
  node [
    id 135
    label "ping-pong"
  ]
  node [
    id 136
    label "odbija&#263;"
  ]
  node [
    id 137
    label "hack"
  ]
  node [
    id 138
    label "okrawa&#263;"
  ]
  node [
    id 139
    label "siatk&#243;wka"
  ]
  node [
    id 140
    label "zacina&#263;"
  ]
  node [
    id 141
    label "reduce"
  ]
  node [
    id 142
    label "oblewa&#263;"
  ]
  node [
    id 143
    label "odcina&#263;"
  ]
  node [
    id 144
    label "uderza&#263;"
  ]
  node [
    id 145
    label "tenis"
  ]
  node [
    id 146
    label "zabija&#263;"
  ]
  node [
    id 147
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 148
    label "narusza&#263;"
  ]
  node [
    id 149
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 150
    label "skraca&#263;"
  ]
  node [
    id 151
    label "write_out"
  ]
  node [
    id 152
    label "mie&#263;_miejsce"
  ]
  node [
    id 153
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 154
    label "motywowa&#263;"
  ]
  node [
    id 155
    label "act"
  ]
  node [
    id 156
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 157
    label "organizowa&#263;"
  ]
  node [
    id 158
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 159
    label "czyni&#263;"
  ]
  node [
    id 160
    label "give"
  ]
  node [
    id 161
    label "stylizowa&#263;"
  ]
  node [
    id 162
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 163
    label "falowa&#263;"
  ]
  node [
    id 164
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 165
    label "peddle"
  ]
  node [
    id 166
    label "praca"
  ]
  node [
    id 167
    label "wydala&#263;"
  ]
  node [
    id 168
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 169
    label "tentegowa&#263;"
  ]
  node [
    id 170
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 171
    label "urz&#261;dza&#263;"
  ]
  node [
    id 172
    label "oszukiwa&#263;"
  ]
  node [
    id 173
    label "work"
  ]
  node [
    id 174
    label "ukazywa&#263;"
  ]
  node [
    id 175
    label "przerabia&#263;"
  ]
  node [
    id 176
    label "post&#281;powa&#263;"
  ]
  node [
    id 177
    label "traci&#263;"
  ]
  node [
    id 178
    label "alternate"
  ]
  node [
    id 179
    label "change"
  ]
  node [
    id 180
    label "reengineering"
  ]
  node [
    id 181
    label "zast&#281;powa&#263;"
  ]
  node [
    id 182
    label "sprawia&#263;"
  ]
  node [
    id 183
    label "zyskiwa&#263;"
  ]
  node [
    id 184
    label "przechodzi&#263;"
  ]
  node [
    id 185
    label "mi&#281;so"
  ]
  node [
    id 186
    label "ni_chuja"
  ]
  node [
    id 187
    label "zupe&#322;nie"
  ]
  node [
    id 188
    label "ca&#322;kiem"
  ]
  node [
    id 189
    label "zupe&#322;ny"
  ]
  node [
    id 190
    label "wniwecz"
  ]
  node [
    id 191
    label "kompletny"
  ]
  node [
    id 192
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 193
    label "equal"
  ]
  node [
    id 194
    label "trwa&#263;"
  ]
  node [
    id 195
    label "chodzi&#263;"
  ]
  node [
    id 196
    label "si&#281;ga&#263;"
  ]
  node [
    id 197
    label "stan"
  ]
  node [
    id 198
    label "obecno&#347;&#263;"
  ]
  node [
    id 199
    label "stand"
  ]
  node [
    id 200
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 201
    label "uczestniczy&#263;"
  ]
  node [
    id 202
    label "participate"
  ]
  node [
    id 203
    label "istnie&#263;"
  ]
  node [
    id 204
    label "pozostawa&#263;"
  ]
  node [
    id 205
    label "zostawa&#263;"
  ]
  node [
    id 206
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 207
    label "adhere"
  ]
  node [
    id 208
    label "compass"
  ]
  node [
    id 209
    label "korzysta&#263;"
  ]
  node [
    id 210
    label "appreciation"
  ]
  node [
    id 211
    label "osi&#261;ga&#263;"
  ]
  node [
    id 212
    label "dociera&#263;"
  ]
  node [
    id 213
    label "get"
  ]
  node [
    id 214
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 215
    label "mierzy&#263;"
  ]
  node [
    id 216
    label "u&#380;ywa&#263;"
  ]
  node [
    id 217
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 218
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 219
    label "exsert"
  ]
  node [
    id 220
    label "being"
  ]
  node [
    id 221
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 222
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 223
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 224
    label "p&#322;ywa&#263;"
  ]
  node [
    id 225
    label "run"
  ]
  node [
    id 226
    label "bangla&#263;"
  ]
  node [
    id 227
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 228
    label "przebiega&#263;"
  ]
  node [
    id 229
    label "wk&#322;ada&#263;"
  ]
  node [
    id 230
    label "proceed"
  ]
  node [
    id 231
    label "carry"
  ]
  node [
    id 232
    label "bywa&#263;"
  ]
  node [
    id 233
    label "dziama&#263;"
  ]
  node [
    id 234
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 235
    label "stara&#263;_si&#281;"
  ]
  node [
    id 236
    label "para"
  ]
  node [
    id 237
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 238
    label "str&#243;j"
  ]
  node [
    id 239
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 240
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 241
    label "krok"
  ]
  node [
    id 242
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 243
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 244
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 245
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 246
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 247
    label "continue"
  ]
  node [
    id 248
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 249
    label "Ohio"
  ]
  node [
    id 250
    label "wci&#281;cie"
  ]
  node [
    id 251
    label "Nowy_York"
  ]
  node [
    id 252
    label "warstwa"
  ]
  node [
    id 253
    label "samopoczucie"
  ]
  node [
    id 254
    label "Illinois"
  ]
  node [
    id 255
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 256
    label "state"
  ]
  node [
    id 257
    label "Jukatan"
  ]
  node [
    id 258
    label "Kalifornia"
  ]
  node [
    id 259
    label "Wirginia"
  ]
  node [
    id 260
    label "wektor"
  ]
  node [
    id 261
    label "Goa"
  ]
  node [
    id 262
    label "Teksas"
  ]
  node [
    id 263
    label "Waszyngton"
  ]
  node [
    id 264
    label "miejsce"
  ]
  node [
    id 265
    label "Massachusetts"
  ]
  node [
    id 266
    label "Alaska"
  ]
  node [
    id 267
    label "Arakan"
  ]
  node [
    id 268
    label "Hawaje"
  ]
  node [
    id 269
    label "Maryland"
  ]
  node [
    id 270
    label "punkt"
  ]
  node [
    id 271
    label "Michigan"
  ]
  node [
    id 272
    label "Arizona"
  ]
  node [
    id 273
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 274
    label "Georgia"
  ]
  node [
    id 275
    label "poziom"
  ]
  node [
    id 276
    label "Pensylwania"
  ]
  node [
    id 277
    label "shape"
  ]
  node [
    id 278
    label "Luizjana"
  ]
  node [
    id 279
    label "Nowy_Meksyk"
  ]
  node [
    id 280
    label "Alabama"
  ]
  node [
    id 281
    label "ilo&#347;&#263;"
  ]
  node [
    id 282
    label "Kansas"
  ]
  node [
    id 283
    label "Oregon"
  ]
  node [
    id 284
    label "Oklahoma"
  ]
  node [
    id 285
    label "Floryda"
  ]
  node [
    id 286
    label "jednostka_administracyjna"
  ]
  node [
    id 287
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 288
    label "zboczenie"
  ]
  node [
    id 289
    label "om&#243;wienie"
  ]
  node [
    id 290
    label "sponiewieranie"
  ]
  node [
    id 291
    label "discipline"
  ]
  node [
    id 292
    label "rzecz"
  ]
  node [
    id 293
    label "omawia&#263;"
  ]
  node [
    id 294
    label "kr&#261;&#380;enie"
  ]
  node [
    id 295
    label "tre&#347;&#263;"
  ]
  node [
    id 296
    label "robienie"
  ]
  node [
    id 297
    label "sponiewiera&#263;"
  ]
  node [
    id 298
    label "element"
  ]
  node [
    id 299
    label "entity"
  ]
  node [
    id 300
    label "tematyka"
  ]
  node [
    id 301
    label "w&#261;tek"
  ]
  node [
    id 302
    label "charakter"
  ]
  node [
    id 303
    label "zbaczanie"
  ]
  node [
    id 304
    label "program_nauczania"
  ]
  node [
    id 305
    label "om&#243;wi&#263;"
  ]
  node [
    id 306
    label "omawianie"
  ]
  node [
    id 307
    label "thing"
  ]
  node [
    id 308
    label "kultura"
  ]
  node [
    id 309
    label "istota"
  ]
  node [
    id 310
    label "zbacza&#263;"
  ]
  node [
    id 311
    label "zboczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
]
