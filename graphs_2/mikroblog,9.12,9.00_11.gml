graph [
  node [
    id 0
    label "lea"
    origin "text"
  ]
  node [
    id 1
    label "messi"
    origin "text"
  ]
  node [
    id 2
    label "espanyol"
    origin "text"
  ]
  node [
    id 3
    label "barcelona"
    origin "text"
  ]
  node [
    id 4
    label "Lea"
  ]
  node [
    id 5
    label "Messi"
  ]
  node [
    id 6
    label "Espanyol"
  ]
  node [
    id 7
    label "Barcelona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
]
