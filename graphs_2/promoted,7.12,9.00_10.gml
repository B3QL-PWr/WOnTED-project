graph [
  node [
    id 0
    label "dziewi&#281;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wilk"
    origin "text"
  ]
  node [
    id 2
    label "przechodzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przed"
    origin "text"
  ]
  node [
    id 4
    label "kamera"
    origin "text"
  ]
  node [
    id 5
    label "t&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "wschodz&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "slonca"
    origin "text"
  ]
  node [
    id 8
    label "p&#281;d"
  ]
  node [
    id 9
    label "schorzenie"
  ]
  node [
    id 10
    label "wiecha"
  ]
  node [
    id 11
    label "pies_ratowniczy_lawinowy"
  ]
  node [
    id 12
    label "zaka&#380;enie"
  ]
  node [
    id 13
    label "gruby_zwierz"
  ]
  node [
    id 14
    label "futro"
  ]
  node [
    id 15
    label "srom"
  ]
  node [
    id 16
    label "zawy&#263;"
  ]
  node [
    id 17
    label "tycznia"
  ]
  node [
    id 18
    label "maszyna"
  ]
  node [
    id 19
    label "pies"
  ]
  node [
    id 20
    label "owczarek"
  ]
  node [
    id 21
    label "masarnia"
  ]
  node [
    id 22
    label "wy&#263;"
  ]
  node [
    id 23
    label "wataha"
  ]
  node [
    id 24
    label "lampy"
  ]
  node [
    id 25
    label "p&#281;cherz_moczowy"
  ]
  node [
    id 26
    label "tocze&#324;"
  ]
  node [
    id 27
    label "pies_policyjny"
  ]
  node [
    id 28
    label "k&#322;a&#324;ce"
  ]
  node [
    id 29
    label "surowiec"
  ]
  node [
    id 30
    label "sier&#347;&#263;"
  ]
  node [
    id 31
    label "okrycie"
  ]
  node [
    id 32
    label "haircloth"
  ]
  node [
    id 33
    label "ro&#347;lina"
  ]
  node [
    id 34
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 35
    label "ok&#243;&#322;ek"
  ]
  node [
    id 36
    label "k&#322;&#261;b"
  ]
  node [
    id 37
    label "d&#261;&#380;enie"
  ]
  node [
    id 38
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 39
    label "drive"
  ]
  node [
    id 40
    label "organ_ro&#347;linny"
  ]
  node [
    id 41
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 42
    label "ruch"
  ]
  node [
    id 43
    label "wyci&#261;ganie"
  ]
  node [
    id 44
    label "rozp&#281;d"
  ]
  node [
    id 45
    label "zrzez"
  ]
  node [
    id 46
    label "kormus"
  ]
  node [
    id 47
    label "ognisko"
  ]
  node [
    id 48
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 49
    label "powalenie"
  ]
  node [
    id 50
    label "odezwanie_si&#281;"
  ]
  node [
    id 51
    label "atakowanie"
  ]
  node [
    id 52
    label "grupa_ryzyka"
  ]
  node [
    id 53
    label "przypadek"
  ]
  node [
    id 54
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 55
    label "nabawienie_si&#281;"
  ]
  node [
    id 56
    label "inkubacja"
  ]
  node [
    id 57
    label "kryzys"
  ]
  node [
    id 58
    label "powali&#263;"
  ]
  node [
    id 59
    label "remisja"
  ]
  node [
    id 60
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 61
    label "zajmowa&#263;"
  ]
  node [
    id 62
    label "zaburzenie"
  ]
  node [
    id 63
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 64
    label "badanie_histopatologiczne"
  ]
  node [
    id 65
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 66
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 67
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 68
    label "odzywanie_si&#281;"
  ]
  node [
    id 69
    label "diagnoza"
  ]
  node [
    id 70
    label "atakowa&#263;"
  ]
  node [
    id 71
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 72
    label "nabawianie_si&#281;"
  ]
  node [
    id 73
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 74
    label "zajmowanie"
  ]
  node [
    id 75
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 78
    label "tuleja"
  ]
  node [
    id 79
    label "pracowanie"
  ]
  node [
    id 80
    label "kad&#322;ub"
  ]
  node [
    id 81
    label "n&#243;&#380;"
  ]
  node [
    id 82
    label "b&#281;benek"
  ]
  node [
    id 83
    label "wa&#322;"
  ]
  node [
    id 84
    label "maszyneria"
  ]
  node [
    id 85
    label "prototypownia"
  ]
  node [
    id 86
    label "trawers"
  ]
  node [
    id 87
    label "deflektor"
  ]
  node [
    id 88
    label "kolumna"
  ]
  node [
    id 89
    label "mechanizm"
  ]
  node [
    id 90
    label "wa&#322;ek"
  ]
  node [
    id 91
    label "pracowa&#263;"
  ]
  node [
    id 92
    label "b&#281;ben"
  ]
  node [
    id 93
    label "rz&#281;zi&#263;"
  ]
  node [
    id 94
    label "przyk&#322;adka"
  ]
  node [
    id 95
    label "t&#322;ok"
  ]
  node [
    id 96
    label "dehumanizacja"
  ]
  node [
    id 97
    label "rami&#281;"
  ]
  node [
    id 98
    label "rz&#281;&#380;enie"
  ]
  node [
    id 99
    label "urz&#261;dzenie"
  ]
  node [
    id 100
    label "piese&#322;"
  ]
  node [
    id 101
    label "Cerber"
  ]
  node [
    id 102
    label "szczeka&#263;"
  ]
  node [
    id 103
    label "&#322;ajdak"
  ]
  node [
    id 104
    label "kabanos"
  ]
  node [
    id 105
    label "wyzwisko"
  ]
  node [
    id 106
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 107
    label "samiec"
  ]
  node [
    id 108
    label "spragniony"
  ]
  node [
    id 109
    label "policjant"
  ]
  node [
    id 110
    label "rakarz"
  ]
  node [
    id 111
    label "szczu&#263;"
  ]
  node [
    id 112
    label "wycie"
  ]
  node [
    id 113
    label "istota_&#380;ywa"
  ]
  node [
    id 114
    label "trufla"
  ]
  node [
    id 115
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 116
    label "sobaka"
  ]
  node [
    id 117
    label "dogoterapia"
  ]
  node [
    id 118
    label "s&#322;u&#380;enie"
  ]
  node [
    id 119
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 120
    label "psowate"
  ]
  node [
    id 121
    label "szczucie"
  ]
  node [
    id 122
    label "czworon&#243;g"
  ]
  node [
    id 123
    label "pies_pasterski"
  ]
  node [
    id 124
    label "owczarz"
  ]
  node [
    id 125
    label "pastuszek"
  ]
  node [
    id 126
    label "zara&#380;enie"
  ]
  node [
    id 127
    label "choroba_somatyczna"
  ]
  node [
    id 128
    label "contamination"
  ]
  node [
    id 129
    label "pami&#281;&#263;_immunologiczna"
  ]
  node [
    id 130
    label "zaka&#380;enie_si&#281;"
  ]
  node [
    id 131
    label "oznaka"
  ]
  node [
    id 132
    label "perch"
  ]
  node [
    id 133
    label "kita"
  ]
  node [
    id 134
    label "wieniec"
  ]
  node [
    id 135
    label "kwiatostan"
  ]
  node [
    id 136
    label "p&#281;k"
  ]
  node [
    id 137
    label "ogon"
  ]
  node [
    id 138
    label "dom"
  ]
  node [
    id 139
    label "wi&#261;zka"
  ]
  node [
    id 140
    label "oczy"
  ]
  node [
    id 141
    label "wytw&#243;rnia"
  ]
  node [
    id 142
    label "formacja"
  ]
  node [
    id 143
    label "gang"
  ]
  node [
    id 144
    label "stado"
  ]
  node [
    id 145
    label "package"
  ]
  node [
    id 146
    label "gromada"
  ]
  node [
    id 147
    label "dzik"
  ]
  node [
    id 148
    label "okres_godowy"
  ]
  node [
    id 149
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 150
    label "wydoby&#263;"
  ]
  node [
    id 151
    label "rant"
  ]
  node [
    id 152
    label "rave"
  ]
  node [
    id 153
    label "zabrzmie&#263;"
  ]
  node [
    id 154
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 155
    label "ha&#322;asowa&#263;"
  ]
  node [
    id 156
    label "fa&#322;szowa&#263;"
  ]
  node [
    id 157
    label "p&#322;aka&#263;"
  ]
  node [
    id 158
    label "snivel"
  ]
  node [
    id 159
    label "yip"
  ]
  node [
    id 160
    label "kuciapa"
  ]
  node [
    id 161
    label "myszka"
  ]
  node [
    id 162
    label "warga_sromowa"
  ]
  node [
    id 163
    label "wstyd"
  ]
  node [
    id 164
    label "cunnilingus"
  ]
  node [
    id 165
    label "&#322;echtaczka"
  ]
  node [
    id 166
    label "b&#322;ona_dziewicza"
  ]
  node [
    id 167
    label "cipa"
  ]
  node [
    id 168
    label "uj&#347;cie_pochwy"
  ]
  node [
    id 169
    label "przedsionek_pochwy"
  ]
  node [
    id 170
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 171
    label "mie&#263;_miejsce"
  ]
  node [
    id 172
    label "move"
  ]
  node [
    id 173
    label "zaczyna&#263;"
  ]
  node [
    id 174
    label "przebywa&#263;"
  ]
  node [
    id 175
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 176
    label "conflict"
  ]
  node [
    id 177
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 178
    label "mija&#263;"
  ]
  node [
    id 179
    label "proceed"
  ]
  node [
    id 180
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 181
    label "go"
  ]
  node [
    id 182
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 183
    label "saturate"
  ]
  node [
    id 184
    label "i&#347;&#263;"
  ]
  node [
    id 185
    label "doznawa&#263;"
  ]
  node [
    id 186
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 187
    label "przestawa&#263;"
  ]
  node [
    id 188
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 189
    label "pass"
  ]
  node [
    id 190
    label "zalicza&#263;"
  ]
  node [
    id 191
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 192
    label "zmienia&#263;"
  ]
  node [
    id 193
    label "test"
  ]
  node [
    id 194
    label "podlega&#263;"
  ]
  node [
    id 195
    label "przerabia&#263;"
  ]
  node [
    id 196
    label "continue"
  ]
  node [
    id 197
    label "traci&#263;"
  ]
  node [
    id 198
    label "alternate"
  ]
  node [
    id 199
    label "change"
  ]
  node [
    id 200
    label "reengineering"
  ]
  node [
    id 201
    label "zast&#281;powa&#263;"
  ]
  node [
    id 202
    label "sprawia&#263;"
  ]
  node [
    id 203
    label "zyskiwa&#263;"
  ]
  node [
    id 204
    label "&#380;y&#263;"
  ]
  node [
    id 205
    label "coating"
  ]
  node [
    id 206
    label "determine"
  ]
  node [
    id 207
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 208
    label "ko&#324;czy&#263;"
  ]
  node [
    id 209
    label "finish_up"
  ]
  node [
    id 210
    label "lecie&#263;"
  ]
  node [
    id 211
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 212
    label "bangla&#263;"
  ]
  node [
    id 213
    label "trace"
  ]
  node [
    id 214
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 215
    label "impart"
  ]
  node [
    id 216
    label "by&#263;"
  ]
  node [
    id 217
    label "try"
  ]
  node [
    id 218
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 219
    label "boost"
  ]
  node [
    id 220
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 221
    label "dziama&#263;"
  ]
  node [
    id 222
    label "blend"
  ]
  node [
    id 223
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 224
    label "draw"
  ]
  node [
    id 225
    label "wyrusza&#263;"
  ]
  node [
    id 226
    label "bie&#380;e&#263;"
  ]
  node [
    id 227
    label "dzia&#263;_si&#281;"
  ]
  node [
    id 228
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 229
    label "tryb"
  ]
  node [
    id 230
    label "czas"
  ]
  node [
    id 231
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 232
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 233
    label "describe"
  ]
  node [
    id 234
    label "post&#281;powa&#263;"
  ]
  node [
    id 235
    label "tkwi&#263;"
  ]
  node [
    id 236
    label "istnie&#263;"
  ]
  node [
    id 237
    label "pause"
  ]
  node [
    id 238
    label "hesitate"
  ]
  node [
    id 239
    label "trwa&#263;"
  ]
  node [
    id 240
    label "base_on_balls"
  ]
  node [
    id 241
    label "omija&#263;"
  ]
  node [
    id 242
    label "hurt"
  ]
  node [
    id 243
    label "czu&#263;"
  ]
  node [
    id 244
    label "zale&#380;e&#263;"
  ]
  node [
    id 245
    label "wywi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 246
    label "bra&#263;"
  ]
  node [
    id 247
    label "mark"
  ]
  node [
    id 248
    label "number"
  ]
  node [
    id 249
    label "stwierdza&#263;"
  ]
  node [
    id 250
    label "odb&#281;bnia&#263;"
  ]
  node [
    id 251
    label "wlicza&#263;"
  ]
  node [
    id 252
    label "przyporz&#261;dkowywa&#263;"
  ]
  node [
    id 253
    label "odejmowa&#263;"
  ]
  node [
    id 254
    label "bankrupt"
  ]
  node [
    id 255
    label "open"
  ]
  node [
    id 256
    label "set_about"
  ]
  node [
    id 257
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 258
    label "begin"
  ]
  node [
    id 259
    label "goban"
  ]
  node [
    id 260
    label "gra_planszowa"
  ]
  node [
    id 261
    label "sport_umys&#322;owy"
  ]
  node [
    id 262
    label "chi&#324;ski"
  ]
  node [
    id 263
    label "cover"
  ]
  node [
    id 264
    label "robi&#263;"
  ]
  node [
    id 265
    label "wytwarza&#263;"
  ]
  node [
    id 266
    label "amend"
  ]
  node [
    id 267
    label "overwork"
  ]
  node [
    id 268
    label "convert"
  ]
  node [
    id 269
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 270
    label "zamienia&#263;"
  ]
  node [
    id 271
    label "modyfikowa&#263;"
  ]
  node [
    id 272
    label "radzi&#263;_sobie"
  ]
  node [
    id 273
    label "przetwarza&#263;"
  ]
  node [
    id 274
    label "sp&#281;dza&#263;"
  ]
  node [
    id 275
    label "badanie"
  ]
  node [
    id 276
    label "do&#347;wiadczenie"
  ]
  node [
    id 277
    label "narz&#281;dzie"
  ]
  node [
    id 278
    label "przechodzenie"
  ]
  node [
    id 279
    label "quiz"
  ]
  node [
    id 280
    label "sprawdzian"
  ]
  node [
    id 281
    label "arkusz"
  ]
  node [
    id 282
    label "sytuacja"
  ]
  node [
    id 283
    label "krosownica"
  ]
  node [
    id 284
    label "wideotelefon"
  ]
  node [
    id 285
    label "przyrz&#261;d"
  ]
  node [
    id 286
    label "utensylia"
  ]
  node [
    id 287
    label "telefon"
  ]
  node [
    id 288
    label "mikrofon"
  ]
  node [
    id 289
    label "panel"
  ]
  node [
    id 290
    label "sie&#263;_telekomunikacyjna"
  ]
  node [
    id 291
    label "element"
  ]
  node [
    id 292
    label "przeka&#378;nik"
  ]
  node [
    id 293
    label "p&#322;aszczyzna"
  ]
  node [
    id 294
    label "&#347;rodowisko"
  ]
  node [
    id 295
    label "plan"
  ]
  node [
    id 296
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 297
    label "gestaltyzm"
  ]
  node [
    id 298
    label "background"
  ]
  node [
    id 299
    label "melodia"
  ]
  node [
    id 300
    label "causal_agent"
  ]
  node [
    id 301
    label "dalszoplanowy"
  ]
  node [
    id 302
    label "warunki"
  ]
  node [
    id 303
    label "pod&#322;o&#380;e"
  ]
  node [
    id 304
    label "obiekt"
  ]
  node [
    id 305
    label "informacja"
  ]
  node [
    id 306
    label "obraz"
  ]
  node [
    id 307
    label "layer"
  ]
  node [
    id 308
    label "lingwistyka_kognitywna"
  ]
  node [
    id 309
    label "partia"
  ]
  node [
    id 310
    label "ubarwienie"
  ]
  node [
    id 311
    label "model"
  ]
  node [
    id 312
    label "intencja"
  ]
  node [
    id 313
    label "punkt"
  ]
  node [
    id 314
    label "rysunek"
  ]
  node [
    id 315
    label "miejsce_pracy"
  ]
  node [
    id 316
    label "przestrze&#324;"
  ]
  node [
    id 317
    label "wytw&#243;r"
  ]
  node [
    id 318
    label "device"
  ]
  node [
    id 319
    label "pomys&#322;"
  ]
  node [
    id 320
    label "reprezentacja"
  ]
  node [
    id 321
    label "agreement"
  ]
  node [
    id 322
    label "dekoracja"
  ]
  node [
    id 323
    label "perspektywa"
  ]
  node [
    id 324
    label "status"
  ]
  node [
    id 325
    label "wymiar"
  ]
  node [
    id 326
    label "&#347;ciana"
  ]
  node [
    id 327
    label "surface"
  ]
  node [
    id 328
    label "zakres"
  ]
  node [
    id 329
    label "kwadrant"
  ]
  node [
    id 330
    label "degree"
  ]
  node [
    id 331
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 332
    label "powierzchnia"
  ]
  node [
    id 333
    label "ukszta&#322;towanie"
  ]
  node [
    id 334
    label "cia&#322;o"
  ]
  node [
    id 335
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 336
    label "p&#322;aszczak"
  ]
  node [
    id 337
    label "wygl&#261;d"
  ]
  node [
    id 338
    label "r&#243;&#380;nokolorowy"
  ]
  node [
    id 339
    label "barwny"
  ]
  node [
    id 340
    label "przybranie"
  ]
  node [
    id 341
    label "color"
  ]
  node [
    id 342
    label "cecha"
  ]
  node [
    id 343
    label "tone"
  ]
  node [
    id 344
    label "litosfera"
  ]
  node [
    id 345
    label "dotleni&#263;"
  ]
  node [
    id 346
    label "pr&#243;chnica"
  ]
  node [
    id 347
    label "glej"
  ]
  node [
    id 348
    label "martwica"
  ]
  node [
    id 349
    label "glinowa&#263;"
  ]
  node [
    id 350
    label "podglebie"
  ]
  node [
    id 351
    label "ryzosfera"
  ]
  node [
    id 352
    label "kompleks_sorpcyjny"
  ]
  node [
    id 353
    label "przyczyna"
  ]
  node [
    id 354
    label "geosystem"
  ]
  node [
    id 355
    label "glinowanie"
  ]
  node [
    id 356
    label "publikacja"
  ]
  node [
    id 357
    label "wiedza"
  ]
  node [
    id 358
    label "doj&#347;cie"
  ]
  node [
    id 359
    label "obiega&#263;"
  ]
  node [
    id 360
    label "powzi&#281;cie"
  ]
  node [
    id 361
    label "dane"
  ]
  node [
    id 362
    label "obiegni&#281;cie"
  ]
  node [
    id 363
    label "sygna&#322;"
  ]
  node [
    id 364
    label "obieganie"
  ]
  node [
    id 365
    label "powzi&#261;&#263;"
  ]
  node [
    id 366
    label "obiec"
  ]
  node [
    id 367
    label "doj&#347;&#263;"
  ]
  node [
    id 368
    label "co&#347;"
  ]
  node [
    id 369
    label "budynek"
  ]
  node [
    id 370
    label "thing"
  ]
  node [
    id 371
    label "poj&#281;cie"
  ]
  node [
    id 372
    label "program"
  ]
  node [
    id 373
    label "rzecz"
  ]
  node [
    id 374
    label "strona"
  ]
  node [
    id 375
    label "zanucenie"
  ]
  node [
    id 376
    label "nuta"
  ]
  node [
    id 377
    label "zakosztowa&#263;"
  ]
  node [
    id 378
    label "zajawka"
  ]
  node [
    id 379
    label "zanuci&#263;"
  ]
  node [
    id 380
    label "emocja"
  ]
  node [
    id 381
    label "oskoma"
  ]
  node [
    id 382
    label "melika"
  ]
  node [
    id 383
    label "nucenie"
  ]
  node [
    id 384
    label "nuci&#263;"
  ]
  node [
    id 385
    label "istota"
  ]
  node [
    id 386
    label "brzmienie"
  ]
  node [
    id 387
    label "zjawisko"
  ]
  node [
    id 388
    label "taste"
  ]
  node [
    id 389
    label "muzyka"
  ]
  node [
    id 390
    label "inclination"
  ]
  node [
    id 391
    label "Bund"
  ]
  node [
    id 392
    label "PPR"
  ]
  node [
    id 393
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 394
    label "wybranek"
  ]
  node [
    id 395
    label "Jakobici"
  ]
  node [
    id 396
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 397
    label "SLD"
  ]
  node [
    id 398
    label "Razem"
  ]
  node [
    id 399
    label "PiS"
  ]
  node [
    id 400
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 401
    label "Kuomintang"
  ]
  node [
    id 402
    label "ZSL"
  ]
  node [
    id 403
    label "organizacja"
  ]
  node [
    id 404
    label "AWS"
  ]
  node [
    id 405
    label "gra"
  ]
  node [
    id 406
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 407
    label "game"
  ]
  node [
    id 408
    label "grupa"
  ]
  node [
    id 409
    label "blok"
  ]
  node [
    id 410
    label "materia&#322;"
  ]
  node [
    id 411
    label "PO"
  ]
  node [
    id 412
    label "si&#322;a"
  ]
  node [
    id 413
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 414
    label "niedoczas"
  ]
  node [
    id 415
    label "Federali&#347;ci"
  ]
  node [
    id 416
    label "PSL"
  ]
  node [
    id 417
    label "Wigowie"
  ]
  node [
    id 418
    label "ZChN"
  ]
  node [
    id 419
    label "egzekutywa"
  ]
  node [
    id 420
    label "aktyw"
  ]
  node [
    id 421
    label "wybranka"
  ]
  node [
    id 422
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 423
    label "unit"
  ]
  node [
    id 424
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 425
    label "sk&#322;adnik"
  ]
  node [
    id 426
    label "wydarzenie"
  ]
  node [
    id 427
    label "class"
  ]
  node [
    id 428
    label "zesp&#243;&#322;"
  ]
  node [
    id 429
    label "obiekt_naturalny"
  ]
  node [
    id 430
    label "otoczenie"
  ]
  node [
    id 431
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 432
    label "environment"
  ]
  node [
    id 433
    label "huczek"
  ]
  node [
    id 434
    label "ekosystem"
  ]
  node [
    id 435
    label "wszechstworzenie"
  ]
  node [
    id 436
    label "woda"
  ]
  node [
    id 437
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 438
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 439
    label "teren"
  ]
  node [
    id 440
    label "mikrokosmos"
  ]
  node [
    id 441
    label "stw&#243;r"
  ]
  node [
    id 442
    label "Ziemia"
  ]
  node [
    id 443
    label "fauna"
  ]
  node [
    id 444
    label "biota"
  ]
  node [
    id 445
    label "pocz&#261;tki"
  ]
  node [
    id 446
    label "pochodzenie"
  ]
  node [
    id 447
    label "kontekst"
  ]
  node [
    id 448
    label "representation"
  ]
  node [
    id 449
    label "effigy"
  ]
  node [
    id 450
    label "podobrazie"
  ]
  node [
    id 451
    label "scena"
  ]
  node [
    id 452
    label "human_body"
  ]
  node [
    id 453
    label "projekcja"
  ]
  node [
    id 454
    label "oprawia&#263;"
  ]
  node [
    id 455
    label "postprodukcja"
  ]
  node [
    id 456
    label "inning"
  ]
  node [
    id 457
    label "pulment"
  ]
  node [
    id 458
    label "pogl&#261;d"
  ]
  node [
    id 459
    label "zbi&#243;r"
  ]
  node [
    id 460
    label "plama_barwna"
  ]
  node [
    id 461
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 462
    label "oprawianie"
  ]
  node [
    id 463
    label "sztafa&#380;"
  ]
  node [
    id 464
    label "parkiet"
  ]
  node [
    id 465
    label "opinion"
  ]
  node [
    id 466
    label "uj&#281;cie"
  ]
  node [
    id 467
    label "zaj&#347;cie"
  ]
  node [
    id 468
    label "persona"
  ]
  node [
    id 469
    label "filmoteka"
  ]
  node [
    id 470
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 471
    label "ziarno"
  ]
  node [
    id 472
    label "picture"
  ]
  node [
    id 473
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 474
    label "wypunktowa&#263;"
  ]
  node [
    id 475
    label "ostro&#347;&#263;"
  ]
  node [
    id 476
    label "malarz"
  ]
  node [
    id 477
    label "napisy"
  ]
  node [
    id 478
    label "przeplot"
  ]
  node [
    id 479
    label "punktowa&#263;"
  ]
  node [
    id 480
    label "anamorfoza"
  ]
  node [
    id 481
    label "przedstawienie"
  ]
  node [
    id 482
    label "ty&#322;&#243;wka"
  ]
  node [
    id 483
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 484
    label "widok"
  ]
  node [
    id 485
    label "czo&#322;&#243;wka"
  ]
  node [
    id 486
    label "rola"
  ]
  node [
    id 487
    label "psychologia"
  ]
  node [
    id 488
    label "figura"
  ]
  node [
    id 489
    label "drugoplanowo"
  ]
  node [
    id 490
    label "nieznaczny"
  ]
  node [
    id 491
    label "poboczny"
  ]
  node [
    id 492
    label "dalszy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 6
    target 7
  ]
]
