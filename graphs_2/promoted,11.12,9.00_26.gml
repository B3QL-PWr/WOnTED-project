graph [
  node [
    id 0
    label "nasa"
    origin "text"
  ]
  node [
    id 1
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pierwsza"
    origin "text"
  ]
  node [
    id 3
    label "nagranie"
    origin "text"
  ]
  node [
    id 4
    label "d&#378;wi&#281;k"
    origin "text"
  ]
  node [
    id 5
    label "mars"
    origin "text"
  ]
  node [
    id 6
    label "teatr"
  ]
  node [
    id 7
    label "exhibit"
  ]
  node [
    id 8
    label "podawa&#263;"
  ]
  node [
    id 9
    label "display"
  ]
  node [
    id 10
    label "pokazywa&#263;"
  ]
  node [
    id 11
    label "demonstrowa&#263;"
  ]
  node [
    id 12
    label "przedstawienie"
  ]
  node [
    id 13
    label "zapoznawa&#263;"
  ]
  node [
    id 14
    label "opisywa&#263;"
  ]
  node [
    id 15
    label "ukazywa&#263;"
  ]
  node [
    id 16
    label "represent"
  ]
  node [
    id 17
    label "zg&#322;asza&#263;"
  ]
  node [
    id 18
    label "typify"
  ]
  node [
    id 19
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 20
    label "attest"
  ]
  node [
    id 21
    label "stanowi&#263;"
  ]
  node [
    id 22
    label "warto&#347;&#263;"
  ]
  node [
    id 23
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 24
    label "by&#263;"
  ]
  node [
    id 25
    label "wyraz"
  ]
  node [
    id 26
    label "wyra&#380;a&#263;"
  ]
  node [
    id 27
    label "przeszkala&#263;"
  ]
  node [
    id 28
    label "powodowa&#263;"
  ]
  node [
    id 29
    label "introduce"
  ]
  node [
    id 30
    label "exsert"
  ]
  node [
    id 31
    label "bespeak"
  ]
  node [
    id 32
    label "informowa&#263;"
  ]
  node [
    id 33
    label "indicate"
  ]
  node [
    id 34
    label "mie&#263;_miejsce"
  ]
  node [
    id 35
    label "odst&#281;powa&#263;"
  ]
  node [
    id 36
    label "perform"
  ]
  node [
    id 37
    label "wychodzi&#263;"
  ]
  node [
    id 38
    label "seclude"
  ]
  node [
    id 39
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 40
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 41
    label "nak&#322;ania&#263;"
  ]
  node [
    id 42
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 43
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 44
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "dzia&#322;a&#263;"
  ]
  node [
    id 46
    label "act"
  ]
  node [
    id 47
    label "appear"
  ]
  node [
    id 48
    label "unwrap"
  ]
  node [
    id 49
    label "rezygnowa&#263;"
  ]
  node [
    id 50
    label "overture"
  ]
  node [
    id 51
    label "uczestniczy&#263;"
  ]
  node [
    id 52
    label "report"
  ]
  node [
    id 53
    label "write"
  ]
  node [
    id 54
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 55
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 56
    label "decide"
  ]
  node [
    id 57
    label "pies_my&#347;liwski"
  ]
  node [
    id 58
    label "decydowa&#263;"
  ]
  node [
    id 59
    label "zatrzymywa&#263;"
  ]
  node [
    id 60
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 61
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 62
    label "tenis"
  ]
  node [
    id 63
    label "deal"
  ]
  node [
    id 64
    label "dawa&#263;"
  ]
  node [
    id 65
    label "stawia&#263;"
  ]
  node [
    id 66
    label "rozgrywa&#263;"
  ]
  node [
    id 67
    label "kelner"
  ]
  node [
    id 68
    label "siatk&#243;wka"
  ]
  node [
    id 69
    label "cover"
  ]
  node [
    id 70
    label "tender"
  ]
  node [
    id 71
    label "jedzenie"
  ]
  node [
    id 72
    label "faszerowa&#263;"
  ]
  node [
    id 73
    label "serwowa&#263;"
  ]
  node [
    id 74
    label "zawiera&#263;"
  ]
  node [
    id 75
    label "poznawa&#263;"
  ]
  node [
    id 76
    label "obznajamia&#263;"
  ]
  node [
    id 77
    label "go_steady"
  ]
  node [
    id 78
    label "teren"
  ]
  node [
    id 79
    label "play"
  ]
  node [
    id 80
    label "antyteatr"
  ]
  node [
    id 81
    label "instytucja"
  ]
  node [
    id 82
    label "gra"
  ]
  node [
    id 83
    label "budynek"
  ]
  node [
    id 84
    label "deski"
  ]
  node [
    id 85
    label "sala"
  ]
  node [
    id 86
    label "sztuka"
  ]
  node [
    id 87
    label "literatura"
  ]
  node [
    id 88
    label "przedstawianie"
  ]
  node [
    id 89
    label "dekoratornia"
  ]
  node [
    id 90
    label "modelatornia"
  ]
  node [
    id 91
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 92
    label "widzownia"
  ]
  node [
    id 93
    label "pr&#243;bowanie"
  ]
  node [
    id 94
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 95
    label "zademonstrowanie"
  ]
  node [
    id 96
    label "obgadanie"
  ]
  node [
    id 97
    label "realizacja"
  ]
  node [
    id 98
    label "scena"
  ]
  node [
    id 99
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 100
    label "narration"
  ]
  node [
    id 101
    label "cyrk"
  ]
  node [
    id 102
    label "wytw&#243;r"
  ]
  node [
    id 103
    label "posta&#263;"
  ]
  node [
    id 104
    label "theatrical_performance"
  ]
  node [
    id 105
    label "opisanie"
  ]
  node [
    id 106
    label "malarstwo"
  ]
  node [
    id 107
    label "scenografia"
  ]
  node [
    id 108
    label "ukazanie"
  ]
  node [
    id 109
    label "zapoznanie"
  ]
  node [
    id 110
    label "pokaz"
  ]
  node [
    id 111
    label "podanie"
  ]
  node [
    id 112
    label "spos&#243;b"
  ]
  node [
    id 113
    label "ods&#322;ona"
  ]
  node [
    id 114
    label "pokazanie"
  ]
  node [
    id 115
    label "wyst&#261;pienie"
  ]
  node [
    id 116
    label "przedstawi&#263;"
  ]
  node [
    id 117
    label "rola"
  ]
  node [
    id 118
    label "godzina"
  ]
  node [
    id 119
    label "time"
  ]
  node [
    id 120
    label "doba"
  ]
  node [
    id 121
    label "p&#243;&#322;godzina"
  ]
  node [
    id 122
    label "jednostka_czasu"
  ]
  node [
    id 123
    label "czas"
  ]
  node [
    id 124
    label "minuta"
  ]
  node [
    id 125
    label "kwadrans"
  ]
  node [
    id 126
    label "wys&#322;uchanie"
  ]
  node [
    id 127
    label "utrwalenie"
  ]
  node [
    id 128
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 129
    label "recording"
  ]
  node [
    id 130
    label "ustalenie"
  ]
  node [
    id 131
    label "trwalszy"
  ]
  node [
    id 132
    label "confirmation"
  ]
  node [
    id 133
    label "zachowanie"
  ]
  node [
    id 134
    label "przedmiot"
  ]
  node [
    id 135
    label "p&#322;&#243;d"
  ]
  node [
    id 136
    label "work"
  ]
  node [
    id 137
    label "rezultat"
  ]
  node [
    id 138
    label "pos&#322;uchanie"
  ]
  node [
    id 139
    label "spe&#322;nienie"
  ]
  node [
    id 140
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 141
    label "hearing"
  ]
  node [
    id 142
    label "muzyka"
  ]
  node [
    id 143
    label "spe&#322;ni&#263;"
  ]
  node [
    id 144
    label "phone"
  ]
  node [
    id 145
    label "wpadni&#281;cie"
  ]
  node [
    id 146
    label "wydawa&#263;"
  ]
  node [
    id 147
    label "zjawisko"
  ]
  node [
    id 148
    label "wyda&#263;"
  ]
  node [
    id 149
    label "intonacja"
  ]
  node [
    id 150
    label "wpa&#347;&#263;"
  ]
  node [
    id 151
    label "note"
  ]
  node [
    id 152
    label "onomatopeja"
  ]
  node [
    id 153
    label "modalizm"
  ]
  node [
    id 154
    label "nadlecenie"
  ]
  node [
    id 155
    label "sound"
  ]
  node [
    id 156
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 157
    label "wpada&#263;"
  ]
  node [
    id 158
    label "solmizacja"
  ]
  node [
    id 159
    label "seria"
  ]
  node [
    id 160
    label "dobiec"
  ]
  node [
    id 161
    label "transmiter"
  ]
  node [
    id 162
    label "heksachord"
  ]
  node [
    id 163
    label "akcent"
  ]
  node [
    id 164
    label "wydanie"
  ]
  node [
    id 165
    label "repetycja"
  ]
  node [
    id 166
    label "brzmienie"
  ]
  node [
    id 167
    label "wpadanie"
  ]
  node [
    id 168
    label "proces"
  ]
  node [
    id 169
    label "boski"
  ]
  node [
    id 170
    label "krajobraz"
  ]
  node [
    id 171
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 172
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 173
    label "przywidzenie"
  ]
  node [
    id 174
    label "presence"
  ]
  node [
    id 175
    label "charakter"
  ]
  node [
    id 176
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 177
    label "podkre&#347;lenie"
  ]
  node [
    id 178
    label "implozja"
  ]
  node [
    id 179
    label "znak_diakrytyczny"
  ]
  node [
    id 180
    label "stress"
  ]
  node [
    id 181
    label "wymowa"
  ]
  node [
    id 182
    label "plozja"
  ]
  node [
    id 183
    label "modalno&#347;&#263;"
  ]
  node [
    id 184
    label "interpretacja"
  ]
  node [
    id 185
    label "wyra&#380;anie"
  ]
  node [
    id 186
    label "cecha"
  ]
  node [
    id 187
    label "tone"
  ]
  node [
    id 188
    label "wydawanie"
  ]
  node [
    id 189
    label "spirit"
  ]
  node [
    id 190
    label "rejestr"
  ]
  node [
    id 191
    label "kolorystyka"
  ]
  node [
    id 192
    label "neuron"
  ]
  node [
    id 193
    label "mediator"
  ]
  node [
    id 194
    label "przeka&#378;nik"
  ]
  node [
    id 195
    label "neurotransmitter"
  ]
  node [
    id 196
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 197
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 198
    label "przybiec"
  ]
  node [
    id 199
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 200
    label "approach"
  ]
  node [
    id 201
    label "range"
  ]
  node [
    id 202
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 203
    label "dotrze&#263;"
  ]
  node [
    id 204
    label "supervene"
  ]
  node [
    id 205
    label "overwhelm"
  ]
  node [
    id 206
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 207
    label "dotarcie"
  ]
  node [
    id 208
    label "nadbiegni&#281;cie"
  ]
  node [
    id 209
    label "struktura"
  ]
  node [
    id 210
    label "&#347;piew"
  ]
  node [
    id 211
    label "znak_muzyczny"
  ]
  node [
    id 212
    label "powt&#243;rka"
  ]
  node [
    id 213
    label "wymy&#347;lenie"
  ]
  node [
    id 214
    label "spotkanie"
  ]
  node [
    id 215
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 216
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 217
    label "ulegni&#281;cie"
  ]
  node [
    id 218
    label "collapse"
  ]
  node [
    id 219
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 220
    label "rzecz"
  ]
  node [
    id 221
    label "poniesienie"
  ]
  node [
    id 222
    label "zapach"
  ]
  node [
    id 223
    label "ciecz"
  ]
  node [
    id 224
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 225
    label "odwiedzenie"
  ]
  node [
    id 226
    label "uderzenie"
  ]
  node [
    id 227
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 228
    label "rzeka"
  ]
  node [
    id 229
    label "postrzeganie"
  ]
  node [
    id 230
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 231
    label "&#347;wiat&#322;o"
  ]
  node [
    id 232
    label "dostanie_si&#281;"
  ]
  node [
    id 233
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 234
    label "release"
  ]
  node [
    id 235
    label "rozbicie_si&#281;"
  ]
  node [
    id 236
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 237
    label "uleganie"
  ]
  node [
    id 238
    label "dostawanie_si&#281;"
  ]
  node [
    id 239
    label "odwiedzanie"
  ]
  node [
    id 240
    label "spotykanie"
  ]
  node [
    id 241
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 242
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 243
    label "wymy&#347;lanie"
  ]
  node [
    id 244
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 245
    label "ingress"
  ]
  node [
    id 246
    label "dzianie_si&#281;"
  ]
  node [
    id 247
    label "wp&#322;ywanie"
  ]
  node [
    id 248
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 249
    label "overlap"
  ]
  node [
    id 250
    label "wkl&#281;sanie"
  ]
  node [
    id 251
    label "powierzy&#263;"
  ]
  node [
    id 252
    label "pieni&#261;dze"
  ]
  node [
    id 253
    label "plon"
  ]
  node [
    id 254
    label "give"
  ]
  node [
    id 255
    label "skojarzy&#263;"
  ]
  node [
    id 256
    label "zadenuncjowa&#263;"
  ]
  node [
    id 257
    label "impart"
  ]
  node [
    id 258
    label "da&#263;"
  ]
  node [
    id 259
    label "reszta"
  ]
  node [
    id 260
    label "wydawnictwo"
  ]
  node [
    id 261
    label "zrobi&#263;"
  ]
  node [
    id 262
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 263
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 264
    label "wiano"
  ]
  node [
    id 265
    label "produkcja"
  ]
  node [
    id 266
    label "translate"
  ]
  node [
    id 267
    label "picture"
  ]
  node [
    id 268
    label "poda&#263;"
  ]
  node [
    id 269
    label "wprowadzi&#263;"
  ]
  node [
    id 270
    label "wytworzy&#263;"
  ]
  node [
    id 271
    label "dress"
  ]
  node [
    id 272
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 273
    label "tajemnica"
  ]
  node [
    id 274
    label "panna_na_wydaniu"
  ]
  node [
    id 275
    label "supply"
  ]
  node [
    id 276
    label "ujawni&#263;"
  ]
  node [
    id 277
    label "delivery"
  ]
  node [
    id 278
    label "zdarzenie_si&#281;"
  ]
  node [
    id 279
    label "rendition"
  ]
  node [
    id 280
    label "egzemplarz"
  ]
  node [
    id 281
    label "impression"
  ]
  node [
    id 282
    label "publikacja"
  ]
  node [
    id 283
    label "zadenuncjowanie"
  ]
  node [
    id 284
    label "wytworzenie"
  ]
  node [
    id 285
    label "issue"
  ]
  node [
    id 286
    label "danie"
  ]
  node [
    id 287
    label "czasopismo"
  ]
  node [
    id 288
    label "wprowadzenie"
  ]
  node [
    id 289
    label "odmiana"
  ]
  node [
    id 290
    label "ujawnienie"
  ]
  node [
    id 291
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 292
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 293
    label "urz&#261;dzenie"
  ]
  node [
    id 294
    label "zrobienie"
  ]
  node [
    id 295
    label "robi&#263;"
  ]
  node [
    id 296
    label "surrender"
  ]
  node [
    id 297
    label "kojarzy&#263;"
  ]
  node [
    id 298
    label "wprowadza&#263;"
  ]
  node [
    id 299
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 300
    label "ujawnia&#263;"
  ]
  node [
    id 301
    label "placard"
  ]
  node [
    id 302
    label "powierza&#263;"
  ]
  node [
    id 303
    label "denuncjowa&#263;"
  ]
  node [
    id 304
    label "wytwarza&#263;"
  ]
  node [
    id 305
    label "train"
  ]
  node [
    id 306
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 307
    label "strike"
  ]
  node [
    id 308
    label "zaziera&#263;"
  ]
  node [
    id 309
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 310
    label "czu&#263;"
  ]
  node [
    id 311
    label "spotyka&#263;"
  ]
  node [
    id 312
    label "drop"
  ]
  node [
    id 313
    label "pogo"
  ]
  node [
    id 314
    label "ogrom"
  ]
  node [
    id 315
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 316
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 317
    label "popada&#263;"
  ]
  node [
    id 318
    label "odwiedza&#263;"
  ]
  node [
    id 319
    label "wymy&#347;la&#263;"
  ]
  node [
    id 320
    label "przypomina&#263;"
  ]
  node [
    id 321
    label "ujmowa&#263;"
  ]
  node [
    id 322
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 323
    label "fall"
  ]
  node [
    id 324
    label "chowa&#263;"
  ]
  node [
    id 325
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 326
    label "demaskowa&#263;"
  ]
  node [
    id 327
    label "ulega&#263;"
  ]
  node [
    id 328
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 329
    label "emocja"
  ]
  node [
    id 330
    label "flatten"
  ]
  node [
    id 331
    label "ulec"
  ]
  node [
    id 332
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 333
    label "fall_upon"
  ]
  node [
    id 334
    label "ponie&#347;&#263;"
  ]
  node [
    id 335
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 336
    label "uderzy&#263;"
  ]
  node [
    id 337
    label "wymy&#347;li&#263;"
  ]
  node [
    id 338
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 339
    label "decline"
  ]
  node [
    id 340
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 341
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 342
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 343
    label "spotka&#263;"
  ]
  node [
    id 344
    label "odwiedzi&#263;"
  ]
  node [
    id 345
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 346
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 347
    label "ci&#261;g"
  ]
  node [
    id 348
    label "instrument_muzyczny"
  ]
  node [
    id 349
    label "set"
  ]
  node [
    id 350
    label "przebieg"
  ]
  node [
    id 351
    label "zbi&#243;r"
  ]
  node [
    id 352
    label "jednostka"
  ]
  node [
    id 353
    label "jednostka_systematyczna"
  ]
  node [
    id 354
    label "stage_set"
  ]
  node [
    id 355
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 356
    label "komplet"
  ]
  node [
    id 357
    label "line"
  ]
  node [
    id 358
    label "sekwencja"
  ]
  node [
    id 359
    label "zestawienie"
  ]
  node [
    id 360
    label "partia"
  ]
  node [
    id 361
    label "herezja"
  ]
  node [
    id 362
    label "monarchianizm"
  ]
  node [
    id 363
    label "instrumentacja_g&#322;oskowa"
  ]
  node [
    id 364
    label "leksem"
  ]
  node [
    id 365
    label "figura_s&#322;&#243;w"
  ]
  node [
    id 366
    label "platforma"
  ]
  node [
    id 367
    label "mina"
  ]
  node [
    id 368
    label "maszt"
  ]
  node [
    id 369
    label "ka&#322;"
  ]
  node [
    id 370
    label "wyraz_twarzy"
  ]
  node [
    id 371
    label "air"
  ]
  node [
    id 372
    label "korytarz"
  ]
  node [
    id 373
    label "zacinanie"
  ]
  node [
    id 374
    label "klipa"
  ]
  node [
    id 375
    label "zacina&#263;"
  ]
  node [
    id 376
    label "nab&#243;j"
  ]
  node [
    id 377
    label "pies_przeciwpancerny"
  ]
  node [
    id 378
    label "zaci&#281;cie"
  ]
  node [
    id 379
    label "sanction"
  ]
  node [
    id 380
    label "niespodzianka"
  ]
  node [
    id 381
    label "zapalnik"
  ]
  node [
    id 382
    label "pole_minowe"
  ]
  node [
    id 383
    label "kopalnia"
  ]
  node [
    id 384
    label "p&#322;aszczyzna"
  ]
  node [
    id 385
    label "koturn"
  ]
  node [
    id 386
    label "sfera"
  ]
  node [
    id 387
    label "podeszwa"
  ]
  node [
    id 388
    label "but"
  ]
  node [
    id 389
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 390
    label "skorupa_ziemska"
  ]
  node [
    id 391
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 392
    label "nadwozie"
  ]
  node [
    id 393
    label "top"
  ]
  node [
    id 394
    label "forsztag"
  ]
  node [
    id 395
    label "s&#322;up"
  ]
  node [
    id 396
    label "bombramstenga"
  ]
  node [
    id 397
    label "saling"
  ]
  node [
    id 398
    label "flaglinka"
  ]
  node [
    id 399
    label "stenga"
  ]
  node [
    id 400
    label "drzewce"
  ]
  node [
    id 401
    label "sztag"
  ]
  node [
    id 402
    label "konstrukcja"
  ]
  node [
    id 403
    label "szkuta"
  ]
  node [
    id 404
    label "bramstenga"
  ]
  node [
    id 405
    label "czerwona"
  ]
  node [
    id 406
    label "planet"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 405
    target 406
  ]
]
