graph [
  node [
    id 0
    label "wyrok"
    origin "text"
  ]
  node [
    id 1
    label "zawieszenie"
    origin "text"
  ]
  node [
    id 2
    label "us&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "policjant"
    origin "text"
  ]
  node [
    id 5
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "zaplecze"
    origin "text"
  ]
  node [
    id 8
    label "jeden"
    origin "text"
  ]
  node [
    id 9
    label "supermarket"
    origin "text"
  ]
  node [
    id 10
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "brutalny"
    origin "text"
  ]
  node [
    id 12
    label "interwencja"
    origin "text"
  ]
  node [
    id 13
    label "wobec"
    origin "text"
  ]
  node [
    id 14
    label "letni"
    origin "text"
  ]
  node [
    id 15
    label "kobieta"
    origin "text"
  ]
  node [
    id 16
    label "judgment"
  ]
  node [
    id 17
    label "order"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "kara"
  ]
  node [
    id 20
    label "orzeczenie"
  ]
  node [
    id 21
    label "sentencja"
  ]
  node [
    id 22
    label "charakter"
  ]
  node [
    id 23
    label "przebiegni&#281;cie"
  ]
  node [
    id 24
    label "przebiec"
  ]
  node [
    id 25
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 26
    label "motyw"
  ]
  node [
    id 27
    label "fabu&#322;a"
  ]
  node [
    id 28
    label "czynno&#347;&#263;"
  ]
  node [
    id 29
    label "konsekwencja"
  ]
  node [
    id 30
    label "forfeit"
  ]
  node [
    id 31
    label "roboty_przymusowe"
  ]
  node [
    id 32
    label "kwota"
  ]
  node [
    id 33
    label "punishment"
  ]
  node [
    id 34
    label "nemezis"
  ]
  node [
    id 35
    label "klacz"
  ]
  node [
    id 36
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 37
    label "decyzja"
  ]
  node [
    id 38
    label "wypowied&#378;"
  ]
  node [
    id 39
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 40
    label "kawaler"
  ]
  node [
    id 41
    label "odznaka"
  ]
  node [
    id 42
    label "rule"
  ]
  node [
    id 43
    label "rubrum"
  ]
  node [
    id 44
    label "powiedzenie"
  ]
  node [
    id 45
    label "amortyzator"
  ]
  node [
    id 46
    label "resor"
  ]
  node [
    id 47
    label "przymocowanie"
  ]
  node [
    id 48
    label "podwozie"
  ]
  node [
    id 49
    label "powieszenie"
  ]
  node [
    id 50
    label "reprieve"
  ]
  node [
    id 51
    label "obwieszenie"
  ]
  node [
    id 52
    label "odwieszanie"
  ]
  node [
    id 53
    label "disavowal"
  ]
  node [
    id 54
    label "oduczenie"
  ]
  node [
    id 55
    label "hang"
  ]
  node [
    id 56
    label "odwieszenie"
  ]
  node [
    id 57
    label "zabranie"
  ]
  node [
    id 58
    label "pozawieszanie"
  ]
  node [
    id 59
    label "skomunikowanie"
  ]
  node [
    id 60
    label "wstrzymanie"
  ]
  node [
    id 61
    label "zako&#324;czenie"
  ]
  node [
    id 62
    label "stop"
  ]
  node [
    id 63
    label "umieszczenie"
  ]
  node [
    id 64
    label "suspension"
  ]
  node [
    id 65
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 66
    label "zrezygnowanie"
  ]
  node [
    id 67
    label "conclusion"
  ]
  node [
    id 68
    label "koniec"
  ]
  node [
    id 69
    label "termination"
  ]
  node [
    id 70
    label "adjustment"
  ]
  node [
    id 71
    label "ukszta&#322;towanie"
  ]
  node [
    id 72
    label "closure"
  ]
  node [
    id 73
    label "zrobienie"
  ]
  node [
    id 74
    label "closing"
  ]
  node [
    id 75
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 76
    label "dzia&#322;anie"
  ]
  node [
    id 77
    label "wyniesienie"
  ]
  node [
    id 78
    label "przeniesienie"
  ]
  node [
    id 79
    label "z&#322;apanie"
  ]
  node [
    id 80
    label "spowodowanie"
  ]
  node [
    id 81
    label "pickings"
  ]
  node [
    id 82
    label "pozabieranie"
  ]
  node [
    id 83
    label "zniesienie"
  ]
  node [
    id 84
    label "wzi&#281;cie"
  ]
  node [
    id 85
    label "pousuwanie"
  ]
  node [
    id 86
    label "coitus_interruptus"
  ]
  node [
    id 87
    label "doprowadzenie"
  ]
  node [
    id 88
    label "wywiezienie"
  ]
  node [
    id 89
    label "zaj&#281;cie"
  ]
  node [
    id 90
    label "removal"
  ]
  node [
    id 91
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 92
    label "udanie_si&#281;"
  ]
  node [
    id 93
    label "przesuni&#281;cie"
  ]
  node [
    id 94
    label "zniewolenie"
  ]
  node [
    id 95
    label "layout"
  ]
  node [
    id 96
    label "poumieszczanie"
  ]
  node [
    id 97
    label "ulokowanie_si&#281;"
  ]
  node [
    id 98
    label "siedzenie"
  ]
  node [
    id 99
    label "uplasowanie"
  ]
  node [
    id 100
    label "zakrycie"
  ]
  node [
    id 101
    label "ustalenie"
  ]
  node [
    id 102
    label "pomieszczenie"
  ]
  node [
    id 103
    label "prze&#322;adowanie"
  ]
  node [
    id 104
    label "attachment"
  ]
  node [
    id 105
    label "do&#322;&#261;czenie"
  ]
  node [
    id 106
    label "zabicie"
  ]
  node [
    id 107
    label "hanging"
  ]
  node [
    id 108
    label "szubienica"
  ]
  node [
    id 109
    label "stracenie"
  ]
  node [
    id 110
    label "wiszenie"
  ]
  node [
    id 111
    label "stryczek"
  ]
  node [
    id 112
    label "inhibition"
  ]
  node [
    id 113
    label "check"
  ]
  node [
    id 114
    label "zaczepienie"
  ]
  node [
    id 115
    label "hipostaza"
  ]
  node [
    id 116
    label "przestanie"
  ]
  node [
    id 117
    label "przetrzymanie"
  ]
  node [
    id 118
    label "discontinuance"
  ]
  node [
    id 119
    label "oddzia&#322;anie"
  ]
  node [
    id 120
    label "przywr&#243;cenie"
  ]
  node [
    id 121
    label "wznowienie"
  ]
  node [
    id 122
    label "przewieszanie"
  ]
  node [
    id 123
    label "wznawianie"
  ]
  node [
    id 124
    label "przywracanie"
  ]
  node [
    id 125
    label "przesycanie"
  ]
  node [
    id 126
    label "reflektor"
  ]
  node [
    id 127
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 128
    label "przesyca&#263;"
  ]
  node [
    id 129
    label "przesyci&#263;"
  ]
  node [
    id 130
    label "mieszanina"
  ]
  node [
    id 131
    label "przesycenie"
  ]
  node [
    id 132
    label "struktura_metalu"
  ]
  node [
    id 133
    label "alia&#380;"
  ]
  node [
    id 134
    label "znak_nakazu"
  ]
  node [
    id 135
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 136
    label "pozas&#322;anianie"
  ]
  node [
    id 137
    label "umo&#380;liwienie"
  ]
  node [
    id 138
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 139
    label "mechanizm"
  ]
  node [
    id 140
    label "amortyzacja"
  ]
  node [
    id 141
    label "buffer"
  ]
  node [
    id 142
    label "p&#322;atowiec"
  ]
  node [
    id 143
    label "pojazd"
  ]
  node [
    id 144
    label "bombowiec"
  ]
  node [
    id 145
    label "ko&#322;o"
  ]
  node [
    id 146
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 147
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 148
    label "postrzec"
  ]
  node [
    id 149
    label "oceni&#263;"
  ]
  node [
    id 150
    label "zobaczy&#263;"
  ]
  node [
    id 151
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 152
    label "respect"
  ]
  node [
    id 153
    label "cognizance"
  ]
  node [
    id 154
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 155
    label "notice"
  ]
  node [
    id 156
    label "rozw&#243;d"
  ]
  node [
    id 157
    label "partner"
  ]
  node [
    id 158
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 159
    label "eksprezydent"
  ]
  node [
    id 160
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 161
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 162
    label "wcze&#347;niejszy"
  ]
  node [
    id 163
    label "dawny"
  ]
  node [
    id 164
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 165
    label "cz&#322;owiek"
  ]
  node [
    id 166
    label "aktor"
  ]
  node [
    id 167
    label "sp&#243;lnik"
  ]
  node [
    id 168
    label "kolaborator"
  ]
  node [
    id 169
    label "prowadzi&#263;"
  ]
  node [
    id 170
    label "uczestniczenie"
  ]
  node [
    id 171
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 172
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 173
    label "pracownik"
  ]
  node [
    id 174
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 175
    label "przedsi&#281;biorca"
  ]
  node [
    id 176
    label "od_dawna"
  ]
  node [
    id 177
    label "anachroniczny"
  ]
  node [
    id 178
    label "dawniej"
  ]
  node [
    id 179
    label "odleg&#322;y"
  ]
  node [
    id 180
    label "przesz&#322;y"
  ]
  node [
    id 181
    label "d&#322;ugoletni"
  ]
  node [
    id 182
    label "poprzedni"
  ]
  node [
    id 183
    label "dawno"
  ]
  node [
    id 184
    label "przestarza&#322;y"
  ]
  node [
    id 185
    label "kombatant"
  ]
  node [
    id 186
    label "niegdysiejszy"
  ]
  node [
    id 187
    label "stary"
  ]
  node [
    id 188
    label "wcze&#347;niej"
  ]
  node [
    id 189
    label "rozbita_rodzina"
  ]
  node [
    id 190
    label "rozstanie"
  ]
  node [
    id 191
    label "separation"
  ]
  node [
    id 192
    label "uniewa&#380;nienie"
  ]
  node [
    id 193
    label "ekspartner"
  ]
  node [
    id 194
    label "prezydent"
  ]
  node [
    id 195
    label "blacharz"
  ]
  node [
    id 196
    label "glina"
  ]
  node [
    id 197
    label "str&#243;&#380;"
  ]
  node [
    id 198
    label "pa&#322;a"
  ]
  node [
    id 199
    label "mundurowy"
  ]
  node [
    id 200
    label "policja"
  ]
  node [
    id 201
    label "psiarnia"
  ]
  node [
    id 202
    label "s&#322;u&#380;ba"
  ]
  node [
    id 203
    label "komisariat"
  ]
  node [
    id 204
    label "posterunek"
  ]
  node [
    id 205
    label "grupa"
  ]
  node [
    id 206
    label "organ"
  ]
  node [
    id 207
    label "&#380;o&#322;nierz"
  ]
  node [
    id 208
    label "funkcjonariusz"
  ]
  node [
    id 209
    label "nosiciel"
  ]
  node [
    id 210
    label "anio&#322;"
  ]
  node [
    id 211
    label "opiekun"
  ]
  node [
    id 212
    label "stra&#380;nik"
  ]
  node [
    id 213
    label "obro&#324;ca"
  ]
  node [
    id 214
    label "rzemie&#347;lnik"
  ]
  node [
    id 215
    label "gleba"
  ]
  node [
    id 216
    label "przybitka"
  ]
  node [
    id 217
    label "istota_&#380;ywa"
  ]
  node [
    id 218
    label "dynia"
  ]
  node [
    id 219
    label "dupek"
  ]
  node [
    id 220
    label "skurwysyn"
  ]
  node [
    id 221
    label "conk"
  ]
  node [
    id 222
    label "czaszka"
  ]
  node [
    id 223
    label "niedostateczny"
  ]
  node [
    id 224
    label "&#322;eb"
  ]
  node [
    id 225
    label "cios"
  ]
  node [
    id 226
    label "wyzwisko"
  ]
  node [
    id 227
    label "penis"
  ]
  node [
    id 228
    label "ciul"
  ]
  node [
    id 229
    label "mak&#243;wka"
  ]
  node [
    id 230
    label "wyposa&#380;enie"
  ]
  node [
    id 231
    label "infrastruktura"
  ]
  node [
    id 232
    label "sklep"
  ]
  node [
    id 233
    label "zinformatyzowanie"
  ]
  node [
    id 234
    label "danie"
  ]
  node [
    id 235
    label "zbi&#243;r"
  ]
  node [
    id 236
    label "zainstalowanie"
  ]
  node [
    id 237
    label "fixture"
  ]
  node [
    id 238
    label "urz&#261;dzenie"
  ]
  node [
    id 239
    label "zakamarek"
  ]
  node [
    id 240
    label "amfilada"
  ]
  node [
    id 241
    label "sklepienie"
  ]
  node [
    id 242
    label "apartment"
  ]
  node [
    id 243
    label "udost&#281;pnienie"
  ]
  node [
    id 244
    label "front"
  ]
  node [
    id 245
    label "miejsce"
  ]
  node [
    id 246
    label "sufit"
  ]
  node [
    id 247
    label "pod&#322;oga"
  ]
  node [
    id 248
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 249
    label "radiofonia"
  ]
  node [
    id 250
    label "trasa"
  ]
  node [
    id 251
    label "telefonia"
  ]
  node [
    id 252
    label "obiekt_handlowy"
  ]
  node [
    id 253
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 254
    label "p&#243;&#322;ka"
  ]
  node [
    id 255
    label "stoisko"
  ]
  node [
    id 256
    label "witryna"
  ]
  node [
    id 257
    label "sk&#322;ad"
  ]
  node [
    id 258
    label "firma"
  ]
  node [
    id 259
    label "kieliszek"
  ]
  node [
    id 260
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 261
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 262
    label "w&#243;dka"
  ]
  node [
    id 263
    label "ujednolicenie"
  ]
  node [
    id 264
    label "ten"
  ]
  node [
    id 265
    label "jaki&#347;"
  ]
  node [
    id 266
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 267
    label "jednakowy"
  ]
  node [
    id 268
    label "jednolicie"
  ]
  node [
    id 269
    label "shot"
  ]
  node [
    id 270
    label "naczynie"
  ]
  node [
    id 271
    label "zawarto&#347;&#263;"
  ]
  node [
    id 272
    label "szk&#322;o"
  ]
  node [
    id 273
    label "mohorycz"
  ]
  node [
    id 274
    label "gorza&#322;ka"
  ]
  node [
    id 275
    label "alkohol"
  ]
  node [
    id 276
    label "sznaps"
  ]
  node [
    id 277
    label "nap&#243;j"
  ]
  node [
    id 278
    label "taki&#380;"
  ]
  node [
    id 279
    label "identyczny"
  ]
  node [
    id 280
    label "zr&#243;wnanie"
  ]
  node [
    id 281
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 282
    label "zr&#243;wnywanie"
  ]
  node [
    id 283
    label "mundurowanie"
  ]
  node [
    id 284
    label "mundurowa&#263;"
  ]
  node [
    id 285
    label "jednakowo"
  ]
  node [
    id 286
    label "okre&#347;lony"
  ]
  node [
    id 287
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 288
    label "z&#322;o&#380;ony"
  ]
  node [
    id 289
    label "jako&#347;"
  ]
  node [
    id 290
    label "niez&#322;y"
  ]
  node [
    id 291
    label "charakterystyczny"
  ]
  node [
    id 292
    label "jako_tako"
  ]
  node [
    id 293
    label "ciekawy"
  ]
  node [
    id 294
    label "dziwny"
  ]
  node [
    id 295
    label "przyzwoity"
  ]
  node [
    id 296
    label "g&#322;&#281;bszy"
  ]
  node [
    id 297
    label "drink"
  ]
  node [
    id 298
    label "jednolity"
  ]
  node [
    id 299
    label "upodobnienie"
  ]
  node [
    id 300
    label "calibration"
  ]
  node [
    id 301
    label "market"
  ]
  node [
    id 302
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 303
    label "wykona&#263;"
  ]
  node [
    id 304
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 305
    label "leave"
  ]
  node [
    id 306
    label "przewie&#347;&#263;"
  ]
  node [
    id 307
    label "zbudowa&#263;"
  ]
  node [
    id 308
    label "pom&#243;c"
  ]
  node [
    id 309
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 310
    label "draw"
  ]
  node [
    id 311
    label "carry"
  ]
  node [
    id 312
    label "dotrze&#263;"
  ]
  node [
    id 313
    label "make"
  ]
  node [
    id 314
    label "score"
  ]
  node [
    id 315
    label "uzyska&#263;"
  ]
  node [
    id 316
    label "profit"
  ]
  node [
    id 317
    label "picture"
  ]
  node [
    id 318
    label "manufacture"
  ]
  node [
    id 319
    label "zrobi&#263;"
  ]
  node [
    id 320
    label "wytworzy&#263;"
  ]
  node [
    id 321
    label "go"
  ]
  node [
    id 322
    label "spowodowa&#263;"
  ]
  node [
    id 323
    label "travel"
  ]
  node [
    id 324
    label "establish"
  ]
  node [
    id 325
    label "zaplanowa&#263;"
  ]
  node [
    id 326
    label "budowla"
  ]
  node [
    id 327
    label "stworzy&#263;"
  ]
  node [
    id 328
    label "evolve"
  ]
  node [
    id 329
    label "pokry&#263;"
  ]
  node [
    id 330
    label "zmieni&#263;"
  ]
  node [
    id 331
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 332
    label "plant"
  ]
  node [
    id 333
    label "zacz&#261;&#263;"
  ]
  node [
    id 334
    label "pozostawi&#263;"
  ]
  node [
    id 335
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 336
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 337
    label "stagger"
  ]
  node [
    id 338
    label "wear"
  ]
  node [
    id 339
    label "przygotowa&#263;"
  ]
  node [
    id 340
    label "return"
  ]
  node [
    id 341
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 342
    label "wygra&#263;"
  ]
  node [
    id 343
    label "zepsu&#263;"
  ]
  node [
    id 344
    label "raise"
  ]
  node [
    id 345
    label "umie&#347;ci&#263;"
  ]
  node [
    id 346
    label "znak"
  ]
  node [
    id 347
    label "zaskutkowa&#263;"
  ]
  node [
    id 348
    label "help"
  ]
  node [
    id 349
    label "u&#322;atwi&#263;"
  ]
  node [
    id 350
    label "concur"
  ]
  node [
    id 351
    label "aid"
  ]
  node [
    id 352
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 353
    label "silny"
  ]
  node [
    id 354
    label "niedelikatny"
  ]
  node [
    id 355
    label "okrutny"
  ]
  node [
    id 356
    label "brutalnie"
  ]
  node [
    id 357
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 358
    label "drastycznie"
  ]
  node [
    id 359
    label "szczery"
  ]
  node [
    id 360
    label "przemoc"
  ]
  node [
    id 361
    label "bezlitosny"
  ]
  node [
    id 362
    label "mocny"
  ]
  node [
    id 363
    label "straszny"
  ]
  node [
    id 364
    label "przestrze&#324;"
  ]
  node [
    id 365
    label "z&#322;y"
  ]
  node [
    id 366
    label "nie&#322;askawy"
  ]
  node [
    id 367
    label "niepob&#322;a&#380;liwy"
  ]
  node [
    id 368
    label "prawdziwy"
  ]
  node [
    id 369
    label "bezlito&#347;nie"
  ]
  node [
    id 370
    label "gro&#378;ny"
  ]
  node [
    id 371
    label "okrutnie"
  ]
  node [
    id 372
    label "trudny"
  ]
  node [
    id 373
    label "przykry"
  ]
  node [
    id 374
    label "bezlito&#347;ny"
  ]
  node [
    id 375
    label "bezwzgl&#281;dny"
  ]
  node [
    id 376
    label "pod&#322;y"
  ]
  node [
    id 377
    label "nieludzki"
  ]
  node [
    id 378
    label "niegrzeczny"
  ]
  node [
    id 379
    label "wytrzyma&#322;y"
  ]
  node [
    id 380
    label "niewra&#380;liwy"
  ]
  node [
    id 381
    label "niedelikatnie"
  ]
  node [
    id 382
    label "nieprzyjemny"
  ]
  node [
    id 383
    label "nieoboj&#281;tny"
  ]
  node [
    id 384
    label "czysty"
  ]
  node [
    id 385
    label "prostoduszny"
  ]
  node [
    id 386
    label "s&#322;uszny"
  ]
  node [
    id 387
    label "uczciwy"
  ]
  node [
    id 388
    label "szczodry"
  ]
  node [
    id 389
    label "szczerze"
  ]
  node [
    id 390
    label "przekonuj&#261;cy"
  ]
  node [
    id 391
    label "szczyry"
  ]
  node [
    id 392
    label "zajebisty"
  ]
  node [
    id 393
    label "mocno"
  ]
  node [
    id 394
    label "niepodwa&#380;alny"
  ]
  node [
    id 395
    label "du&#380;y"
  ]
  node [
    id 396
    label "&#380;ywotny"
  ]
  node [
    id 397
    label "konkretny"
  ]
  node [
    id 398
    label "zdrowy"
  ]
  node [
    id 399
    label "meflochina"
  ]
  node [
    id 400
    label "intensywny"
  ]
  node [
    id 401
    label "krzepienie"
  ]
  node [
    id 402
    label "zdecydowany"
  ]
  node [
    id 403
    label "pokrzepienie"
  ]
  node [
    id 404
    label "silnie"
  ]
  node [
    id 405
    label "wzmacnia&#263;"
  ]
  node [
    id 406
    label "wzmocni&#263;"
  ]
  node [
    id 407
    label "krzepki"
  ]
  node [
    id 408
    label "dobry"
  ]
  node [
    id 409
    label "wyrazisty"
  ]
  node [
    id 410
    label "stabilny"
  ]
  node [
    id 411
    label "widoczny"
  ]
  node [
    id 412
    label "intensywnie"
  ]
  node [
    id 413
    label "bezpardonowo"
  ]
  node [
    id 414
    label "cruelly"
  ]
  node [
    id 415
    label "wyrzyna&#263;"
  ]
  node [
    id 416
    label "viciously"
  ]
  node [
    id 417
    label "barbarously"
  ]
  node [
    id 418
    label "radykalnie"
  ]
  node [
    id 419
    label "drastyczny"
  ]
  node [
    id 420
    label "agresja"
  ]
  node [
    id 421
    label "patologia"
  ]
  node [
    id 422
    label "przewaga"
  ]
  node [
    id 423
    label "interposition"
  ]
  node [
    id 424
    label "ingerencja"
  ]
  node [
    id 425
    label "akcja"
  ]
  node [
    id 426
    label "czyn"
  ]
  node [
    id 427
    label "operacja"
  ]
  node [
    id 428
    label "przebieg"
  ]
  node [
    id 429
    label "zagrywka"
  ]
  node [
    id 430
    label "commotion"
  ]
  node [
    id 431
    label "udzia&#322;"
  ]
  node [
    id 432
    label "gra"
  ]
  node [
    id 433
    label "dywidenda"
  ]
  node [
    id 434
    label "w&#281;ze&#322;"
  ]
  node [
    id 435
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 436
    label "instrument_strunowy"
  ]
  node [
    id 437
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 438
    label "stock"
  ]
  node [
    id 439
    label "wysoko&#347;&#263;"
  ]
  node [
    id 440
    label "jazda"
  ]
  node [
    id 441
    label "occupation"
  ]
  node [
    id 442
    label "oboj&#281;tny"
  ]
  node [
    id 443
    label "s&#322;oneczny"
  ]
  node [
    id 444
    label "weso&#322;y"
  ]
  node [
    id 445
    label "typowy"
  ]
  node [
    id 446
    label "letnio"
  ]
  node [
    id 447
    label "sezonowy"
  ]
  node [
    id 448
    label "latowy"
  ]
  node [
    id 449
    label "ciep&#322;y"
  ]
  node [
    id 450
    label "nijaki"
  ]
  node [
    id 451
    label "bezbarwnie"
  ]
  node [
    id 452
    label "szarzenie"
  ]
  node [
    id 453
    label "zwyczajny"
  ]
  node [
    id 454
    label "neutralny"
  ]
  node [
    id 455
    label "&#380;aden"
  ]
  node [
    id 456
    label "poszarzenie"
  ]
  node [
    id 457
    label "niezabawny"
  ]
  node [
    id 458
    label "nijak"
  ]
  node [
    id 459
    label "nieciekawy"
  ]
  node [
    id 460
    label "czasowy"
  ]
  node [
    id 461
    label "sezonowo"
  ]
  node [
    id 462
    label "nieszkodliwy"
  ]
  node [
    id 463
    label "neutralizowanie"
  ]
  node [
    id 464
    label "&#347;ni&#281;ty"
  ]
  node [
    id 465
    label "oboj&#281;tnie"
  ]
  node [
    id 466
    label "zneutralizowanie"
  ]
  node [
    id 467
    label "zoboj&#281;tnienie"
  ]
  node [
    id 468
    label "bierny"
  ]
  node [
    id 469
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 470
    label "niewa&#380;ny"
  ]
  node [
    id 471
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 472
    label "beztroski"
  ]
  node [
    id 473
    label "pozytywny"
  ]
  node [
    id 474
    label "pijany"
  ]
  node [
    id 475
    label "weso&#322;o"
  ]
  node [
    id 476
    label "grzanie"
  ]
  node [
    id 477
    label "ocieplenie"
  ]
  node [
    id 478
    label "korzystny"
  ]
  node [
    id 479
    label "ciep&#322;o"
  ]
  node [
    id 480
    label "zagrzanie"
  ]
  node [
    id 481
    label "ocieplanie"
  ]
  node [
    id 482
    label "przyjemny"
  ]
  node [
    id 483
    label "ocieplenie_si&#281;"
  ]
  node [
    id 484
    label "ocieplanie_si&#281;"
  ]
  node [
    id 485
    label "mi&#322;y"
  ]
  node [
    id 486
    label "s&#322;onecznie"
  ]
  node [
    id 487
    label "pogodny"
  ]
  node [
    id 488
    label "bezchmurny"
  ]
  node [
    id 489
    label "jasny"
  ]
  node [
    id 490
    label "bezdeszczowy"
  ]
  node [
    id 491
    label "fotowoltaiczny"
  ]
  node [
    id 492
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 493
    label "typowo"
  ]
  node [
    id 494
    label "zwyk&#322;y"
  ]
  node [
    id 495
    label "cz&#281;sty"
  ]
  node [
    id 496
    label "ulega&#263;"
  ]
  node [
    id 497
    label "partnerka"
  ]
  node [
    id 498
    label "pa&#324;stwo"
  ]
  node [
    id 499
    label "ulegni&#281;cie"
  ]
  node [
    id 500
    label "&#380;ona"
  ]
  node [
    id 501
    label "m&#281;&#380;yna"
  ]
  node [
    id 502
    label "samica"
  ]
  node [
    id 503
    label "babka"
  ]
  node [
    id 504
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 505
    label "doros&#322;y"
  ]
  node [
    id 506
    label "uleganie"
  ]
  node [
    id 507
    label "&#322;ono"
  ]
  node [
    id 508
    label "przekwitanie"
  ]
  node [
    id 509
    label "menopauza"
  ]
  node [
    id 510
    label "ulec"
  ]
  node [
    id 511
    label "dojrza&#322;y"
  ]
  node [
    id 512
    label "wapniak"
  ]
  node [
    id 513
    label "senior"
  ]
  node [
    id 514
    label "dojrzale"
  ]
  node [
    id 515
    label "doro&#347;lenie"
  ]
  node [
    id 516
    label "wydoro&#347;lenie"
  ]
  node [
    id 517
    label "m&#261;dry"
  ]
  node [
    id 518
    label "&#378;ra&#322;y"
  ]
  node [
    id 519
    label "doletni"
  ]
  node [
    id 520
    label "doro&#347;le"
  ]
  node [
    id 521
    label "asymilowa&#263;"
  ]
  node [
    id 522
    label "nasada"
  ]
  node [
    id 523
    label "profanum"
  ]
  node [
    id 524
    label "wz&#243;r"
  ]
  node [
    id 525
    label "asymilowanie"
  ]
  node [
    id 526
    label "os&#322;abia&#263;"
  ]
  node [
    id 527
    label "homo_sapiens"
  ]
  node [
    id 528
    label "osoba"
  ]
  node [
    id 529
    label "ludzko&#347;&#263;"
  ]
  node [
    id 530
    label "Adam"
  ]
  node [
    id 531
    label "hominid"
  ]
  node [
    id 532
    label "posta&#263;"
  ]
  node [
    id 533
    label "portrecista"
  ]
  node [
    id 534
    label "polifag"
  ]
  node [
    id 535
    label "podw&#322;adny"
  ]
  node [
    id 536
    label "dwun&#243;g"
  ]
  node [
    id 537
    label "duch"
  ]
  node [
    id 538
    label "os&#322;abianie"
  ]
  node [
    id 539
    label "antropochoria"
  ]
  node [
    id 540
    label "figura"
  ]
  node [
    id 541
    label "g&#322;owa"
  ]
  node [
    id 542
    label "mikrokosmos"
  ]
  node [
    id 543
    label "oddzia&#322;ywanie"
  ]
  node [
    id 544
    label "kobita"
  ]
  node [
    id 545
    label "panna_m&#322;oda"
  ]
  node [
    id 546
    label "&#347;lubna"
  ]
  node [
    id 547
    label "ma&#322;&#380;onek"
  ]
  node [
    id 548
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 549
    label "aktorka"
  ]
  node [
    id 550
    label "pozwolenie"
  ]
  node [
    id 551
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 552
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 553
    label "poddanie_si&#281;"
  ]
  node [
    id 554
    label "subjugation"
  ]
  node [
    id 555
    label "poddanie"
  ]
  node [
    id 556
    label "stanie_si&#281;"
  ]
  node [
    id 557
    label "nagi&#281;cie_si&#281;"
  ]
  node [
    id 558
    label "&#380;ycie"
  ]
  node [
    id 559
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 560
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 561
    label "przywo&#322;a&#263;"
  ]
  node [
    id 562
    label "postpone"
  ]
  node [
    id 563
    label "zezwala&#263;"
  ]
  node [
    id 564
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 565
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 566
    label "poddawa&#263;"
  ]
  node [
    id 567
    label "render"
  ]
  node [
    id 568
    label "subject"
  ]
  node [
    id 569
    label "starzenie_si&#281;"
  ]
  node [
    id 570
    label "przemijanie"
  ]
  node [
    id 571
    label "kwitnienie"
  ]
  node [
    id 572
    label "obumieranie"
  ]
  node [
    id 573
    label "dojrzewanie"
  ]
  node [
    id 574
    label "menopause"
  ]
  node [
    id 575
    label "przestawanie"
  ]
  node [
    id 576
    label "burst"
  ]
  node [
    id 577
    label "poddawanie"
  ]
  node [
    id 578
    label "stawanie_si&#281;"
  ]
  node [
    id 579
    label "zezwalanie"
  ]
  node [
    id 580
    label "przywo&#322;anie"
  ]
  node [
    id 581
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 582
    label "zaliczanie"
  ]
  node [
    id 583
    label "naginanie_si&#281;"
  ]
  node [
    id 584
    label "poddawanie_si&#281;"
  ]
  node [
    id 585
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 586
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 587
    label "pozwoli&#263;"
  ]
  node [
    id 588
    label "fall"
  ]
  node [
    id 589
    label "podda&#263;"
  ]
  node [
    id 590
    label "podda&#263;_si&#281;"
  ]
  node [
    id 591
    label "sta&#263;_si&#281;"
  ]
  node [
    id 592
    label "give"
  ]
  node [
    id 593
    label "put_in"
  ]
  node [
    id 594
    label "Japonia"
  ]
  node [
    id 595
    label "Zair"
  ]
  node [
    id 596
    label "Belize"
  ]
  node [
    id 597
    label "San_Marino"
  ]
  node [
    id 598
    label "Tanzania"
  ]
  node [
    id 599
    label "Antigua_i_Barbuda"
  ]
  node [
    id 600
    label "granica_pa&#324;stwa"
  ]
  node [
    id 601
    label "Senegal"
  ]
  node [
    id 602
    label "Seszele"
  ]
  node [
    id 603
    label "Mauretania"
  ]
  node [
    id 604
    label "Indie"
  ]
  node [
    id 605
    label "Filipiny"
  ]
  node [
    id 606
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 607
    label "Zimbabwe"
  ]
  node [
    id 608
    label "Malezja"
  ]
  node [
    id 609
    label "Rumunia"
  ]
  node [
    id 610
    label "Surinam"
  ]
  node [
    id 611
    label "Ukraina"
  ]
  node [
    id 612
    label "Syria"
  ]
  node [
    id 613
    label "Wyspy_Marshalla"
  ]
  node [
    id 614
    label "Burkina_Faso"
  ]
  node [
    id 615
    label "Grecja"
  ]
  node [
    id 616
    label "Polska"
  ]
  node [
    id 617
    label "Wenezuela"
  ]
  node [
    id 618
    label "Suazi"
  ]
  node [
    id 619
    label "Nepal"
  ]
  node [
    id 620
    label "S&#322;owacja"
  ]
  node [
    id 621
    label "Algieria"
  ]
  node [
    id 622
    label "Chiny"
  ]
  node [
    id 623
    label "Grenada"
  ]
  node [
    id 624
    label "Barbados"
  ]
  node [
    id 625
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 626
    label "Pakistan"
  ]
  node [
    id 627
    label "Niemcy"
  ]
  node [
    id 628
    label "Bahrajn"
  ]
  node [
    id 629
    label "Komory"
  ]
  node [
    id 630
    label "Australia"
  ]
  node [
    id 631
    label "Rodezja"
  ]
  node [
    id 632
    label "Malawi"
  ]
  node [
    id 633
    label "Gwinea"
  ]
  node [
    id 634
    label "Wehrlen"
  ]
  node [
    id 635
    label "Meksyk"
  ]
  node [
    id 636
    label "Liechtenstein"
  ]
  node [
    id 637
    label "Czarnog&#243;ra"
  ]
  node [
    id 638
    label "Wielka_Brytania"
  ]
  node [
    id 639
    label "Kuwejt"
  ]
  node [
    id 640
    label "Monako"
  ]
  node [
    id 641
    label "Angola"
  ]
  node [
    id 642
    label "Jemen"
  ]
  node [
    id 643
    label "Etiopia"
  ]
  node [
    id 644
    label "Madagaskar"
  ]
  node [
    id 645
    label "terytorium"
  ]
  node [
    id 646
    label "Kolumbia"
  ]
  node [
    id 647
    label "Portoryko"
  ]
  node [
    id 648
    label "Mauritius"
  ]
  node [
    id 649
    label "Kostaryka"
  ]
  node [
    id 650
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 651
    label "Tajlandia"
  ]
  node [
    id 652
    label "Argentyna"
  ]
  node [
    id 653
    label "Zambia"
  ]
  node [
    id 654
    label "Sri_Lanka"
  ]
  node [
    id 655
    label "Gwatemala"
  ]
  node [
    id 656
    label "Kirgistan"
  ]
  node [
    id 657
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 658
    label "Hiszpania"
  ]
  node [
    id 659
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 660
    label "Salwador"
  ]
  node [
    id 661
    label "Korea"
  ]
  node [
    id 662
    label "Macedonia"
  ]
  node [
    id 663
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 664
    label "Brunei"
  ]
  node [
    id 665
    label "Mozambik"
  ]
  node [
    id 666
    label "Turcja"
  ]
  node [
    id 667
    label "Kambod&#380;a"
  ]
  node [
    id 668
    label "Benin"
  ]
  node [
    id 669
    label "Bhutan"
  ]
  node [
    id 670
    label "Tunezja"
  ]
  node [
    id 671
    label "Austria"
  ]
  node [
    id 672
    label "Izrael"
  ]
  node [
    id 673
    label "Sierra_Leone"
  ]
  node [
    id 674
    label "Jamajka"
  ]
  node [
    id 675
    label "Rosja"
  ]
  node [
    id 676
    label "Rwanda"
  ]
  node [
    id 677
    label "holoarktyka"
  ]
  node [
    id 678
    label "Nigeria"
  ]
  node [
    id 679
    label "USA"
  ]
  node [
    id 680
    label "Oman"
  ]
  node [
    id 681
    label "Luksemburg"
  ]
  node [
    id 682
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 683
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 684
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 685
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 686
    label "Dominikana"
  ]
  node [
    id 687
    label "Irlandia"
  ]
  node [
    id 688
    label "Liban"
  ]
  node [
    id 689
    label "Hanower"
  ]
  node [
    id 690
    label "Estonia"
  ]
  node [
    id 691
    label "Iran"
  ]
  node [
    id 692
    label "Nowa_Zelandia"
  ]
  node [
    id 693
    label "Gabon"
  ]
  node [
    id 694
    label "Samoa"
  ]
  node [
    id 695
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 696
    label "S&#322;owenia"
  ]
  node [
    id 697
    label "Kiribati"
  ]
  node [
    id 698
    label "Egipt"
  ]
  node [
    id 699
    label "Togo"
  ]
  node [
    id 700
    label "Mongolia"
  ]
  node [
    id 701
    label "Sudan"
  ]
  node [
    id 702
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 703
    label "Bahamy"
  ]
  node [
    id 704
    label "Bangladesz"
  ]
  node [
    id 705
    label "partia"
  ]
  node [
    id 706
    label "Serbia"
  ]
  node [
    id 707
    label "Czechy"
  ]
  node [
    id 708
    label "Holandia"
  ]
  node [
    id 709
    label "Birma"
  ]
  node [
    id 710
    label "Albania"
  ]
  node [
    id 711
    label "Mikronezja"
  ]
  node [
    id 712
    label "Gambia"
  ]
  node [
    id 713
    label "Kazachstan"
  ]
  node [
    id 714
    label "interior"
  ]
  node [
    id 715
    label "Uzbekistan"
  ]
  node [
    id 716
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 717
    label "Malta"
  ]
  node [
    id 718
    label "Lesoto"
  ]
  node [
    id 719
    label "para"
  ]
  node [
    id 720
    label "Antarktis"
  ]
  node [
    id 721
    label "Andora"
  ]
  node [
    id 722
    label "Nauru"
  ]
  node [
    id 723
    label "Kuba"
  ]
  node [
    id 724
    label "Wietnam"
  ]
  node [
    id 725
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 726
    label "ziemia"
  ]
  node [
    id 727
    label "Kamerun"
  ]
  node [
    id 728
    label "Chorwacja"
  ]
  node [
    id 729
    label "Urugwaj"
  ]
  node [
    id 730
    label "Niger"
  ]
  node [
    id 731
    label "Turkmenistan"
  ]
  node [
    id 732
    label "Szwajcaria"
  ]
  node [
    id 733
    label "zwrot"
  ]
  node [
    id 734
    label "organizacja"
  ]
  node [
    id 735
    label "Palau"
  ]
  node [
    id 736
    label "Litwa"
  ]
  node [
    id 737
    label "Gruzja"
  ]
  node [
    id 738
    label "Tajwan"
  ]
  node [
    id 739
    label "Kongo"
  ]
  node [
    id 740
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 741
    label "Honduras"
  ]
  node [
    id 742
    label "Boliwia"
  ]
  node [
    id 743
    label "Uganda"
  ]
  node [
    id 744
    label "Namibia"
  ]
  node [
    id 745
    label "Azerbejd&#380;an"
  ]
  node [
    id 746
    label "Erytrea"
  ]
  node [
    id 747
    label "Gujana"
  ]
  node [
    id 748
    label "Panama"
  ]
  node [
    id 749
    label "Somalia"
  ]
  node [
    id 750
    label "Burundi"
  ]
  node [
    id 751
    label "Tuwalu"
  ]
  node [
    id 752
    label "Libia"
  ]
  node [
    id 753
    label "Katar"
  ]
  node [
    id 754
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 755
    label "Sahara_Zachodnia"
  ]
  node [
    id 756
    label "Trynidad_i_Tobago"
  ]
  node [
    id 757
    label "Gwinea_Bissau"
  ]
  node [
    id 758
    label "Bu&#322;garia"
  ]
  node [
    id 759
    label "Fid&#380;i"
  ]
  node [
    id 760
    label "Nikaragua"
  ]
  node [
    id 761
    label "Tonga"
  ]
  node [
    id 762
    label "Timor_Wschodni"
  ]
  node [
    id 763
    label "Laos"
  ]
  node [
    id 764
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 765
    label "Ghana"
  ]
  node [
    id 766
    label "Brazylia"
  ]
  node [
    id 767
    label "Belgia"
  ]
  node [
    id 768
    label "Irak"
  ]
  node [
    id 769
    label "Peru"
  ]
  node [
    id 770
    label "Arabia_Saudyjska"
  ]
  node [
    id 771
    label "Indonezja"
  ]
  node [
    id 772
    label "Malediwy"
  ]
  node [
    id 773
    label "Afganistan"
  ]
  node [
    id 774
    label "Jordania"
  ]
  node [
    id 775
    label "Kenia"
  ]
  node [
    id 776
    label "Czad"
  ]
  node [
    id 777
    label "Liberia"
  ]
  node [
    id 778
    label "W&#281;gry"
  ]
  node [
    id 779
    label "Chile"
  ]
  node [
    id 780
    label "Mali"
  ]
  node [
    id 781
    label "Armenia"
  ]
  node [
    id 782
    label "Kanada"
  ]
  node [
    id 783
    label "Cypr"
  ]
  node [
    id 784
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 785
    label "Ekwador"
  ]
  node [
    id 786
    label "Mo&#322;dawia"
  ]
  node [
    id 787
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 788
    label "W&#322;ochy"
  ]
  node [
    id 789
    label "Wyspy_Salomona"
  ]
  node [
    id 790
    label "&#321;otwa"
  ]
  node [
    id 791
    label "D&#380;ibuti"
  ]
  node [
    id 792
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 793
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 794
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 795
    label "Portugalia"
  ]
  node [
    id 796
    label "Botswana"
  ]
  node [
    id 797
    label "Maroko"
  ]
  node [
    id 798
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 799
    label "Francja"
  ]
  node [
    id 800
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 801
    label "Dominika"
  ]
  node [
    id 802
    label "Paragwaj"
  ]
  node [
    id 803
    label "Tad&#380;ykistan"
  ]
  node [
    id 804
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 805
    label "Haiti"
  ]
  node [
    id 806
    label "Khitai"
  ]
  node [
    id 807
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 808
    label "powierzchnia"
  ]
  node [
    id 809
    label "wzg&#243;rek_&#322;onowy"
  ]
  node [
    id 810
    label "podbrzusze"
  ]
  node [
    id 811
    label "przyroda"
  ]
  node [
    id 812
    label "pochwa"
  ]
  node [
    id 813
    label "klatka_piersiowa"
  ]
  node [
    id 814
    label "l&#281;d&#378;wie"
  ]
  node [
    id 815
    label "dziedzina"
  ]
  node [
    id 816
    label "wn&#281;trze"
  ]
  node [
    id 817
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 818
    label "brzuch"
  ]
  node [
    id 819
    label "cia&#322;o"
  ]
  node [
    id 820
    label "macica"
  ]
  node [
    id 821
    label "zwierz&#281;"
  ]
  node [
    id 822
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 823
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 824
    label "samka"
  ]
  node [
    id 825
    label "female"
  ]
  node [
    id 826
    label "drogi_rodne"
  ]
  node [
    id 827
    label "ryba"
  ]
  node [
    id 828
    label "ro&#347;lina_zielna"
  ]
  node [
    id 829
    label "ko&#378;larz_babka"
  ]
  node [
    id 830
    label "plantain"
  ]
  node [
    id 831
    label "ciasto"
  ]
  node [
    id 832
    label "babkowate"
  ]
  node [
    id 833
    label "przodkini"
  ]
  node [
    id 834
    label "dziadkowie"
  ]
  node [
    id 835
    label "baba"
  ]
  node [
    id 836
    label "babulinka"
  ]
  node [
    id 837
    label "po&#322;o&#380;na"
  ]
  node [
    id 838
    label "moneta"
  ]
  node [
    id 839
    label "starszy_cz&#322;owiek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 408
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 496
  ]
  edge [
    source 15
    target 497
  ]
  edge [
    source 15
    target 498
  ]
  edge [
    source 15
    target 499
  ]
  edge [
    source 15
    target 500
  ]
  edge [
    source 15
    target 501
  ]
  edge [
    source 15
    target 502
  ]
  edge [
    source 15
    target 503
  ]
  edge [
    source 15
    target 504
  ]
  edge [
    source 15
    target 505
  ]
  edge [
    source 15
    target 506
  ]
  edge [
    source 15
    target 507
  ]
  edge [
    source 15
    target 508
  ]
  edge [
    source 15
    target 509
  ]
  edge [
    source 15
    target 510
  ]
  edge [
    source 15
    target 492
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 511
  ]
  edge [
    source 15
    target 512
  ]
  edge [
    source 15
    target 513
  ]
  edge [
    source 15
    target 514
  ]
  edge [
    source 15
    target 515
  ]
  edge [
    source 15
    target 516
  ]
  edge [
    source 15
    target 517
  ]
  edge [
    source 15
    target 518
  ]
  edge [
    source 15
    target 519
  ]
  edge [
    source 15
    target 520
  ]
  edge [
    source 15
    target 521
  ]
  edge [
    source 15
    target 522
  ]
  edge [
    source 15
    target 523
  ]
  edge [
    source 15
    target 524
  ]
  edge [
    source 15
    target 525
  ]
  edge [
    source 15
    target 526
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 528
  ]
  edge [
    source 15
    target 529
  ]
  edge [
    source 15
    target 530
  ]
  edge [
    source 15
    target 531
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 534
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 536
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 538
  ]
  edge [
    source 15
    target 539
  ]
  edge [
    source 15
    target 540
  ]
  edge [
    source 15
    target 541
  ]
  edge [
    source 15
    target 542
  ]
  edge [
    source 15
    target 543
  ]
  edge [
    source 15
    target 544
  ]
  edge [
    source 15
    target 545
  ]
  edge [
    source 15
    target 546
  ]
  edge [
    source 15
    target 547
  ]
  edge [
    source 15
    target 548
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 340
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 15
    target 577
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 15
    target 579
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 582
  ]
  edge [
    source 15
    target 583
  ]
  edge [
    source 15
    target 584
  ]
  edge [
    source 15
    target 585
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 587
  ]
  edge [
    source 15
    target 588
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
]
