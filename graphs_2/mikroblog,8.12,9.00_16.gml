graph [
  node [
    id 0
    label "czemu"
    origin "text"
  ]
  node [
    id 1
    label "poni&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "zabiega&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "rozowepaski"
    origin "text"
  ]
  node [
    id 6
    label "write_down"
  ]
  node [
    id 7
    label "obra&#380;a&#263;"
  ]
  node [
    id 8
    label "dyskredytowa&#263;"
  ]
  node [
    id 9
    label "disrepute"
  ]
  node [
    id 10
    label "kwestionowa&#263;"
  ]
  node [
    id 11
    label "mistreat"
  ]
  node [
    id 12
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 13
    label "narusza&#263;"
  ]
  node [
    id 14
    label "endeavor"
  ]
  node [
    id 15
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 16
    label "podejmowa&#263;"
  ]
  node [
    id 17
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 18
    label "try"
  ]
  node [
    id 19
    label "post&#281;powa&#263;"
  ]
  node [
    id 20
    label "podnosi&#263;"
  ]
  node [
    id 21
    label "robi&#263;"
  ]
  node [
    id 22
    label "draw"
  ]
  node [
    id 23
    label "drive"
  ]
  node [
    id 24
    label "zmienia&#263;"
  ]
  node [
    id 25
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 26
    label "rise"
  ]
  node [
    id 27
    label "admit"
  ]
  node [
    id 28
    label "reagowa&#263;"
  ]
  node [
    id 29
    label "uwaga"
  ]
  node [
    id 30
    label "punkt_widzenia"
  ]
  node [
    id 31
    label "przyczyna"
  ]
  node [
    id 32
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 33
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 34
    label "subject"
  ]
  node [
    id 35
    label "czynnik"
  ]
  node [
    id 36
    label "matuszka"
  ]
  node [
    id 37
    label "rezultat"
  ]
  node [
    id 38
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 39
    label "geneza"
  ]
  node [
    id 40
    label "poci&#261;ganie"
  ]
  node [
    id 41
    label "sk&#322;adnik"
  ]
  node [
    id 42
    label "warunki"
  ]
  node [
    id 43
    label "sytuacja"
  ]
  node [
    id 44
    label "wydarzenie"
  ]
  node [
    id 45
    label "wypowied&#378;"
  ]
  node [
    id 46
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 47
    label "stan"
  ]
  node [
    id 48
    label "nagana"
  ]
  node [
    id 49
    label "tekst"
  ]
  node [
    id 50
    label "upomnienie"
  ]
  node [
    id 51
    label "dzienniczek"
  ]
  node [
    id 52
    label "gossip"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
]
