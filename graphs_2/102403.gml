graph [
  node [
    id 0
    label "og&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "lista"
    origin "text"
  ]
  node [
    id 2
    label "zwyci&#281;zca"
    origin "text"
  ]
  node [
    id 3
    label "internetowy"
    origin "text"
  ]
  node [
    id 4
    label "wirtualny"
    origin "text"
  ]
  node [
    id 5
    label "konkurs"
    origin "text"
  ]
  node [
    id 6
    label "chopinowski"
    origin "text"
  ]
  node [
    id 7
    label "garage"
    origin "text"
  ]
  node [
    id 8
    label "band"
    origin "text"
  ]
  node [
    id 9
    label "podawa&#263;"
  ]
  node [
    id 10
    label "publikowa&#263;"
  ]
  node [
    id 11
    label "post"
  ]
  node [
    id 12
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 13
    label "announce"
  ]
  node [
    id 14
    label "tenis"
  ]
  node [
    id 15
    label "deal"
  ]
  node [
    id 16
    label "dawa&#263;"
  ]
  node [
    id 17
    label "stawia&#263;"
  ]
  node [
    id 18
    label "rozgrywa&#263;"
  ]
  node [
    id 19
    label "kelner"
  ]
  node [
    id 20
    label "siatk&#243;wka"
  ]
  node [
    id 21
    label "cover"
  ]
  node [
    id 22
    label "tender"
  ]
  node [
    id 23
    label "jedzenie"
  ]
  node [
    id 24
    label "faszerowa&#263;"
  ]
  node [
    id 25
    label "introduce"
  ]
  node [
    id 26
    label "informowa&#263;"
  ]
  node [
    id 27
    label "serwowa&#263;"
  ]
  node [
    id 28
    label "hail"
  ]
  node [
    id 29
    label "komunikowa&#263;"
  ]
  node [
    id 30
    label "okre&#347;la&#263;"
  ]
  node [
    id 31
    label "upublicznia&#263;"
  ]
  node [
    id 32
    label "give"
  ]
  node [
    id 33
    label "wydawnictwo"
  ]
  node [
    id 34
    label "wprowadza&#263;"
  ]
  node [
    id 35
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 36
    label "zachowanie"
  ]
  node [
    id 37
    label "zachowywanie"
  ]
  node [
    id 38
    label "rok_ko&#347;cielny"
  ]
  node [
    id 39
    label "tekst"
  ]
  node [
    id 40
    label "czas"
  ]
  node [
    id 41
    label "praktyka"
  ]
  node [
    id 42
    label "zachowa&#263;"
  ]
  node [
    id 43
    label "zachowywa&#263;"
  ]
  node [
    id 44
    label "zbi&#243;r"
  ]
  node [
    id 45
    label "catalog"
  ]
  node [
    id 46
    label "pozycja"
  ]
  node [
    id 47
    label "sumariusz"
  ]
  node [
    id 48
    label "book"
  ]
  node [
    id 49
    label "stock"
  ]
  node [
    id 50
    label "figurowa&#263;"
  ]
  node [
    id 51
    label "wyliczanka"
  ]
  node [
    id 52
    label "ekscerpcja"
  ]
  node [
    id 53
    label "j&#281;zykowo"
  ]
  node [
    id 54
    label "wypowied&#378;"
  ]
  node [
    id 55
    label "redakcja"
  ]
  node [
    id 56
    label "wytw&#243;r"
  ]
  node [
    id 57
    label "pomini&#281;cie"
  ]
  node [
    id 58
    label "dzie&#322;o"
  ]
  node [
    id 59
    label "preparacja"
  ]
  node [
    id 60
    label "odmianka"
  ]
  node [
    id 61
    label "opu&#347;ci&#263;"
  ]
  node [
    id 62
    label "koniektura"
  ]
  node [
    id 63
    label "pisa&#263;"
  ]
  node [
    id 64
    label "obelga"
  ]
  node [
    id 65
    label "egzemplarz"
  ]
  node [
    id 66
    label "series"
  ]
  node [
    id 67
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 68
    label "uprawianie"
  ]
  node [
    id 69
    label "praca_rolnicza"
  ]
  node [
    id 70
    label "collection"
  ]
  node [
    id 71
    label "dane"
  ]
  node [
    id 72
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 73
    label "pakiet_klimatyczny"
  ]
  node [
    id 74
    label "poj&#281;cie"
  ]
  node [
    id 75
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 76
    label "sum"
  ]
  node [
    id 77
    label "gathering"
  ]
  node [
    id 78
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 79
    label "album"
  ]
  node [
    id 80
    label "po&#322;o&#380;enie"
  ]
  node [
    id 81
    label "debit"
  ]
  node [
    id 82
    label "druk"
  ]
  node [
    id 83
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 84
    label "szata_graficzna"
  ]
  node [
    id 85
    label "wydawa&#263;"
  ]
  node [
    id 86
    label "szermierka"
  ]
  node [
    id 87
    label "spis"
  ]
  node [
    id 88
    label "wyda&#263;"
  ]
  node [
    id 89
    label "ustawienie"
  ]
  node [
    id 90
    label "publikacja"
  ]
  node [
    id 91
    label "status"
  ]
  node [
    id 92
    label "miejsce"
  ]
  node [
    id 93
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 94
    label "adres"
  ]
  node [
    id 95
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 96
    label "rozmieszczenie"
  ]
  node [
    id 97
    label "sytuacja"
  ]
  node [
    id 98
    label "rz&#261;d"
  ]
  node [
    id 99
    label "redaktor"
  ]
  node [
    id 100
    label "awansowa&#263;"
  ]
  node [
    id 101
    label "wojsko"
  ]
  node [
    id 102
    label "bearing"
  ]
  node [
    id 103
    label "znaczenie"
  ]
  node [
    id 104
    label "awans"
  ]
  node [
    id 105
    label "awansowanie"
  ]
  node [
    id 106
    label "poster"
  ]
  node [
    id 107
    label "le&#380;e&#263;"
  ]
  node [
    id 108
    label "entliczek"
  ]
  node [
    id 109
    label "zabawa"
  ]
  node [
    id 110
    label "wiersz"
  ]
  node [
    id 111
    label "pentliczek"
  ]
  node [
    id 112
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 113
    label "zwyci&#281;&#380;yciel"
  ]
  node [
    id 114
    label "uczestnik"
  ]
  node [
    id 115
    label "cz&#322;owiek"
  ]
  node [
    id 116
    label "elektroniczny"
  ]
  node [
    id 117
    label "internetowo"
  ]
  node [
    id 118
    label "nowoczesny"
  ]
  node [
    id 119
    label "netowy"
  ]
  node [
    id 120
    label "sieciowo"
  ]
  node [
    id 121
    label "elektronicznie"
  ]
  node [
    id 122
    label "siatkowy"
  ]
  node [
    id 123
    label "sieciowy"
  ]
  node [
    id 124
    label "nowy"
  ]
  node [
    id 125
    label "nowo&#380;ytny"
  ]
  node [
    id 126
    label "otwarty"
  ]
  node [
    id 127
    label "nowocze&#347;nie"
  ]
  node [
    id 128
    label "elektrycznie"
  ]
  node [
    id 129
    label "mo&#380;liwy"
  ]
  node [
    id 130
    label "wirtualnie"
  ]
  node [
    id 131
    label "nieprawdziwy"
  ]
  node [
    id 132
    label "urealnianie"
  ]
  node [
    id 133
    label "mo&#380;ebny"
  ]
  node [
    id 134
    label "umo&#380;liwianie"
  ]
  node [
    id 135
    label "zno&#347;ny"
  ]
  node [
    id 136
    label "umo&#380;liwienie"
  ]
  node [
    id 137
    label "mo&#380;liwie"
  ]
  node [
    id 138
    label "urealnienie"
  ]
  node [
    id 139
    label "dost&#281;pny"
  ]
  node [
    id 140
    label "nieprawdziwie"
  ]
  node [
    id 141
    label "niezgodny"
  ]
  node [
    id 142
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 143
    label "udawany"
  ]
  node [
    id 144
    label "prawda"
  ]
  node [
    id 145
    label "nieszczery"
  ]
  node [
    id 146
    label "niehistoryczny"
  ]
  node [
    id 147
    label "virtually"
  ]
  node [
    id 148
    label "casting"
  ]
  node [
    id 149
    label "nab&#243;r"
  ]
  node [
    id 150
    label "Eurowizja"
  ]
  node [
    id 151
    label "eliminacje"
  ]
  node [
    id 152
    label "impreza"
  ]
  node [
    id 153
    label "emulation"
  ]
  node [
    id 154
    label "Interwizja"
  ]
  node [
    id 155
    label "impra"
  ]
  node [
    id 156
    label "rozrywka"
  ]
  node [
    id 157
    label "przyj&#281;cie"
  ]
  node [
    id 158
    label "okazja"
  ]
  node [
    id 159
    label "party"
  ]
  node [
    id 160
    label "recruitment"
  ]
  node [
    id 161
    label "wyb&#243;r"
  ]
  node [
    id 162
    label "faza"
  ]
  node [
    id 163
    label "runda"
  ]
  node [
    id 164
    label "turniej"
  ]
  node [
    id 165
    label "retirement"
  ]
  node [
    id 166
    label "przes&#322;uchanie"
  ]
  node [
    id 167
    label "w&#281;dkarstwo"
  ]
  node [
    id 168
    label "charakterystyczny"
  ]
  node [
    id 169
    label "szopenowsko"
  ]
  node [
    id 170
    label "chopinowsko"
  ]
  node [
    id 171
    label "charakterystycznie"
  ]
  node [
    id 172
    label "szczeg&#243;lny"
  ]
  node [
    id 173
    label "wyj&#261;tkowy"
  ]
  node [
    id 174
    label "typowy"
  ]
  node [
    id 175
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 176
    label "podobny"
  ]
  node [
    id 177
    label "characteristically"
  ]
  node [
    id 178
    label "zesp&#243;&#322;"
  ]
  node [
    id 179
    label "Mazowsze"
  ]
  node [
    id 180
    label "odm&#322;adzanie"
  ]
  node [
    id 181
    label "&#346;wietliki"
  ]
  node [
    id 182
    label "whole"
  ]
  node [
    id 183
    label "skupienie"
  ]
  node [
    id 184
    label "The_Beatles"
  ]
  node [
    id 185
    label "odm&#322;adza&#263;"
  ]
  node [
    id 186
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 187
    label "zabudowania"
  ]
  node [
    id 188
    label "group"
  ]
  node [
    id 189
    label "zespolik"
  ]
  node [
    id 190
    label "schorzenie"
  ]
  node [
    id 191
    label "ro&#347;lina"
  ]
  node [
    id 192
    label "grupa"
  ]
  node [
    id 193
    label "Depeche_Mode"
  ]
  node [
    id 194
    label "batch"
  ]
  node [
    id 195
    label "odm&#322;odzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
]
