graph [
  node [
    id 0
    label "traci&#263;"
    origin "text"
  ]
  node [
    id 1
    label "redford"
    origin "text"
  ]
  node [
    id 2
    label "mama"
    origin "text"
  ]
  node [
    id 3
    label "pi&#281;cioletni"
    origin "text"
  ]
  node [
    id 4
    label "abcde"
    origin "text"
  ]
  node [
    id 5
    label "wymawia&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ksi&#281;ga_abdiasza"
    origin "text"
  ]
  node [
    id 7
    label "city"
    origin "text"
  ]
  node [
    id 8
    label "umie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 9
    label "media"
    origin "text"
  ]
  node [
    id 10
    label "spo&#322;eczno&#347;ciowy"
    origin "text"
  ]
  node [
    id 11
    label "wpis"
    origin "text"
  ]
  node [
    id 12
    label "przykry"
    origin "text"
  ]
  node [
    id 13
    label "zaj&#347;cie"
    origin "text"
  ]
  node [
    id 14
    label "lotnisko"
    origin "text"
  ]
  node [
    id 15
    label "szasta&#263;"
  ]
  node [
    id 16
    label "zabija&#263;"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "wytraca&#263;"
  ]
  node [
    id 19
    label "pogarsza&#263;_si&#281;"
  ]
  node [
    id 20
    label "omija&#263;"
  ]
  node [
    id 21
    label "przegrywa&#263;"
  ]
  node [
    id 22
    label "forfeit"
  ]
  node [
    id 23
    label "appear"
  ]
  node [
    id 24
    label "execute"
  ]
  node [
    id 25
    label "powodowa&#263;_&#347;mier&#263;"
  ]
  node [
    id 26
    label "dispatch"
  ]
  node [
    id 27
    label "krzywdzi&#263;"
  ]
  node [
    id 28
    label "beat"
  ]
  node [
    id 29
    label "rzuca&#263;_na_kolana"
  ]
  node [
    id 30
    label "os&#322;ania&#263;"
  ]
  node [
    id 31
    label "niszczy&#263;"
  ]
  node [
    id 32
    label "karci&#263;"
  ]
  node [
    id 33
    label "mordowa&#263;"
  ]
  node [
    id 34
    label "bi&#263;"
  ]
  node [
    id 35
    label "zako&#324;cza&#263;"
  ]
  node [
    id 36
    label "rozbraja&#263;"
  ]
  node [
    id 37
    label "przybija&#263;"
  ]
  node [
    id 38
    label "morzy&#263;"
  ]
  node [
    id 39
    label "zakrywa&#263;"
  ]
  node [
    id 40
    label "kill"
  ]
  node [
    id 41
    label "zwalcza&#263;"
  ]
  node [
    id 42
    label "play"
  ]
  node [
    id 43
    label "ponosi&#263;"
  ]
  node [
    id 44
    label "give"
  ]
  node [
    id 45
    label "nadu&#380;ywa&#263;"
  ]
  node [
    id 46
    label "marnowa&#263;"
  ]
  node [
    id 47
    label "lekcewa&#380;y&#263;"
  ]
  node [
    id 48
    label "opuszcza&#263;"
  ]
  node [
    id 49
    label "odpuszcza&#263;"
  ]
  node [
    id 50
    label "obchodzi&#263;"
  ]
  node [
    id 51
    label "ignore"
  ]
  node [
    id 52
    label "evade"
  ]
  node [
    id 53
    label "pomija&#263;"
  ]
  node [
    id 54
    label "wymija&#263;"
  ]
  node [
    id 55
    label "stroni&#263;"
  ]
  node [
    id 56
    label "unika&#263;"
  ]
  node [
    id 57
    label "i&#347;&#263;"
  ]
  node [
    id 58
    label "przechodzi&#263;"
  ]
  node [
    id 59
    label "przodkini"
  ]
  node [
    id 60
    label "matka_zast&#281;pcza"
  ]
  node [
    id 61
    label "matczysko"
  ]
  node [
    id 62
    label "rodzice"
  ]
  node [
    id 63
    label "stara"
  ]
  node [
    id 64
    label "macierz"
  ]
  node [
    id 65
    label "rodzic"
  ]
  node [
    id 66
    label "Matka_Boska"
  ]
  node [
    id 67
    label "macocha"
  ]
  node [
    id 68
    label "starzy"
  ]
  node [
    id 69
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 70
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 71
    label "pokolenie"
  ]
  node [
    id 72
    label "wapniaki"
  ]
  node [
    id 73
    label "krewna"
  ]
  node [
    id 74
    label "opiekun"
  ]
  node [
    id 75
    label "wapniak"
  ]
  node [
    id 76
    label "rodzic_chrzestny"
  ]
  node [
    id 77
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 78
    label "matka"
  ]
  node [
    id 79
    label "&#380;ona"
  ]
  node [
    id 80
    label "kobieta"
  ]
  node [
    id 81
    label "partnerka"
  ]
  node [
    id 82
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 83
    label "matuszka"
  ]
  node [
    id 84
    label "parametryzacja"
  ]
  node [
    id 85
    label "pa&#324;stwo"
  ]
  node [
    id 86
    label "poj&#281;cie"
  ]
  node [
    id 87
    label "mod"
  ]
  node [
    id 88
    label "patriota"
  ]
  node [
    id 89
    label "m&#281;&#380;atka"
  ]
  node [
    id 90
    label "kilkuletni"
  ]
  node [
    id 91
    label "pami&#281;ta&#263;"
  ]
  node [
    id 92
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 93
    label "express"
  ]
  node [
    id 94
    label "werbalizowa&#263;"
  ]
  node [
    id 95
    label "zastrzega&#263;"
  ]
  node [
    id 96
    label "oskar&#380;a&#263;"
  ]
  node [
    id 97
    label "denounce"
  ]
  node [
    id 98
    label "say"
  ]
  node [
    id 99
    label "typify"
  ]
  node [
    id 100
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 101
    label "wydobywa&#263;"
  ]
  node [
    id 102
    label "zapewnia&#263;"
  ]
  node [
    id 103
    label "condition"
  ]
  node [
    id 104
    label "uprzedza&#263;"
  ]
  node [
    id 105
    label "formu&#322;owa&#263;"
  ]
  node [
    id 106
    label "prosecute"
  ]
  node [
    id 107
    label "twierdzi&#263;"
  ]
  node [
    id 108
    label "echo"
  ]
  node [
    id 109
    label "pilnowa&#263;"
  ]
  node [
    id 110
    label "robi&#263;"
  ]
  node [
    id 111
    label "recall"
  ]
  node [
    id 112
    label "si&#281;ga&#263;"
  ]
  node [
    id 113
    label "take_care"
  ]
  node [
    id 114
    label "troska&#263;_si&#281;"
  ]
  node [
    id 115
    label "chowa&#263;"
  ]
  node [
    id 116
    label "zachowywa&#263;"
  ]
  node [
    id 117
    label "zna&#263;"
  ]
  node [
    id 118
    label "think"
  ]
  node [
    id 119
    label "uwydatnia&#263;"
  ]
  node [
    id 120
    label "eksploatowa&#263;"
  ]
  node [
    id 121
    label "uzyskiwa&#263;"
  ]
  node [
    id 122
    label "wydostawa&#263;"
  ]
  node [
    id 123
    label "wyjmowa&#263;"
  ]
  node [
    id 124
    label "train"
  ]
  node [
    id 125
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 126
    label "wydawa&#263;"
  ]
  node [
    id 127
    label "dobywa&#263;"
  ]
  node [
    id 128
    label "ocala&#263;"
  ]
  node [
    id 129
    label "excavate"
  ]
  node [
    id 130
    label "g&#243;rnictwo"
  ]
  node [
    id 131
    label "raise"
  ]
  node [
    id 132
    label "usuwa&#263;"
  ]
  node [
    id 133
    label "odkrywa&#263;"
  ]
  node [
    id 134
    label "urzeczywistnia&#263;"
  ]
  node [
    id 135
    label "undo"
  ]
  node [
    id 136
    label "przestawa&#263;"
  ]
  node [
    id 137
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 138
    label "cope"
  ]
  node [
    id 139
    label "set"
  ]
  node [
    id 140
    label "put"
  ]
  node [
    id 141
    label "uplasowa&#263;"
  ]
  node [
    id 142
    label "wpierniczy&#263;"
  ]
  node [
    id 143
    label "okre&#347;li&#263;"
  ]
  node [
    id 144
    label "zrobi&#263;"
  ]
  node [
    id 145
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 146
    label "zmieni&#263;"
  ]
  node [
    id 147
    label "umieszcza&#263;"
  ]
  node [
    id 148
    label "sprawi&#263;"
  ]
  node [
    id 149
    label "change"
  ]
  node [
    id 150
    label "zast&#261;pi&#263;"
  ]
  node [
    id 151
    label "come_up"
  ]
  node [
    id 152
    label "przej&#347;&#263;"
  ]
  node [
    id 153
    label "straci&#263;"
  ]
  node [
    id 154
    label "zyska&#263;"
  ]
  node [
    id 155
    label "post&#261;pi&#263;"
  ]
  node [
    id 156
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 157
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 158
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 159
    label "zorganizowa&#263;"
  ]
  node [
    id 160
    label "appoint"
  ]
  node [
    id 161
    label "wystylizowa&#263;"
  ]
  node [
    id 162
    label "cause"
  ]
  node [
    id 163
    label "przerobi&#263;"
  ]
  node [
    id 164
    label "nabra&#263;"
  ]
  node [
    id 165
    label "make"
  ]
  node [
    id 166
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 167
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 168
    label "wydali&#263;"
  ]
  node [
    id 169
    label "zdecydowa&#263;"
  ]
  node [
    id 170
    label "spowodowa&#263;"
  ]
  node [
    id 171
    label "situate"
  ]
  node [
    id 172
    label "nominate"
  ]
  node [
    id 173
    label "rozgniewa&#263;"
  ]
  node [
    id 174
    label "wkopa&#263;"
  ]
  node [
    id 175
    label "pobi&#263;"
  ]
  node [
    id 176
    label "wepchn&#261;&#263;"
  ]
  node [
    id 177
    label "zje&#347;&#263;"
  ]
  node [
    id 178
    label "hold"
  ]
  node [
    id 179
    label "udost&#281;pni&#263;"
  ]
  node [
    id 180
    label "plasowa&#263;"
  ]
  node [
    id 181
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 182
    label "pomieszcza&#263;"
  ]
  node [
    id 183
    label "accommodate"
  ]
  node [
    id 184
    label "zmienia&#263;"
  ]
  node [
    id 185
    label "powodowa&#263;"
  ]
  node [
    id 186
    label "venture"
  ]
  node [
    id 187
    label "wpiernicza&#263;"
  ]
  node [
    id 188
    label "okre&#347;la&#263;"
  ]
  node [
    id 189
    label "gem"
  ]
  node [
    id 190
    label "kompozycja"
  ]
  node [
    id 191
    label "runda"
  ]
  node [
    id 192
    label "muzyka"
  ]
  node [
    id 193
    label "zestaw"
  ]
  node [
    id 194
    label "mass-media"
  ]
  node [
    id 195
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 196
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 197
    label "przekazior"
  ]
  node [
    id 198
    label "uzbrajanie"
  ]
  node [
    id 199
    label "medium"
  ]
  node [
    id 200
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 201
    label "&#347;rodek"
  ]
  node [
    id 202
    label "jasnowidz"
  ]
  node [
    id 203
    label "hipnoza"
  ]
  node [
    id 204
    label "cz&#322;owiek"
  ]
  node [
    id 205
    label "spirytysta"
  ]
  node [
    id 206
    label "otoczenie"
  ]
  node [
    id 207
    label "publikator"
  ]
  node [
    id 208
    label "warunki"
  ]
  node [
    id 209
    label "strona"
  ]
  node [
    id 210
    label "przeka&#378;nik"
  ]
  node [
    id 211
    label "&#347;rodek_przekazu"
  ]
  node [
    id 212
    label "armament"
  ]
  node [
    id 213
    label "arming"
  ]
  node [
    id 214
    label "instalacja"
  ]
  node [
    id 215
    label "wyposa&#380;anie"
  ]
  node [
    id 216
    label "dozbrajanie"
  ]
  node [
    id 217
    label "dozbrojenie"
  ]
  node [
    id 218
    label "montowanie"
  ]
  node [
    id 219
    label "medialny"
  ]
  node [
    id 220
    label "publiczny"
  ]
  node [
    id 221
    label "upublicznianie"
  ]
  node [
    id 222
    label "jawny"
  ]
  node [
    id 223
    label "upublicznienie"
  ]
  node [
    id 224
    label "publicznie"
  ]
  node [
    id 225
    label "popularny"
  ]
  node [
    id 226
    label "&#347;rodkowy"
  ]
  node [
    id 227
    label "medialnie"
  ]
  node [
    id 228
    label "nieprawdziwy"
  ]
  node [
    id 229
    label "inscription"
  ]
  node [
    id 230
    label "op&#322;ata"
  ]
  node [
    id 231
    label "akt"
  ]
  node [
    id 232
    label "tekst"
  ]
  node [
    id 233
    label "czynno&#347;&#263;"
  ]
  node [
    id 234
    label "entrance"
  ]
  node [
    id 235
    label "ekscerpcja"
  ]
  node [
    id 236
    label "j&#281;zykowo"
  ]
  node [
    id 237
    label "wypowied&#378;"
  ]
  node [
    id 238
    label "redakcja"
  ]
  node [
    id 239
    label "wytw&#243;r"
  ]
  node [
    id 240
    label "pomini&#281;cie"
  ]
  node [
    id 241
    label "dzie&#322;o"
  ]
  node [
    id 242
    label "preparacja"
  ]
  node [
    id 243
    label "odmianka"
  ]
  node [
    id 244
    label "opu&#347;ci&#263;"
  ]
  node [
    id 245
    label "koniektura"
  ]
  node [
    id 246
    label "pisa&#263;"
  ]
  node [
    id 247
    label "obelga"
  ]
  node [
    id 248
    label "kwota"
  ]
  node [
    id 249
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 250
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 251
    label "podnieci&#263;"
  ]
  node [
    id 252
    label "scena"
  ]
  node [
    id 253
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 254
    label "numer"
  ]
  node [
    id 255
    label "po&#380;ycie"
  ]
  node [
    id 256
    label "podniecenie"
  ]
  node [
    id 257
    label "nago&#347;&#263;"
  ]
  node [
    id 258
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 259
    label "fascyku&#322;"
  ]
  node [
    id 260
    label "seks"
  ]
  node [
    id 261
    label "podniecanie"
  ]
  node [
    id 262
    label "imisja"
  ]
  node [
    id 263
    label "zwyczaj"
  ]
  node [
    id 264
    label "rozmna&#380;anie"
  ]
  node [
    id 265
    label "ruch_frykcyjny"
  ]
  node [
    id 266
    label "ontologia"
  ]
  node [
    id 267
    label "wydarzenie"
  ]
  node [
    id 268
    label "na_pieska"
  ]
  node [
    id 269
    label "pozycja_misjonarska"
  ]
  node [
    id 270
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 271
    label "fragment"
  ]
  node [
    id 272
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 273
    label "z&#322;&#261;czenie"
  ]
  node [
    id 274
    label "gra_wst&#281;pna"
  ]
  node [
    id 275
    label "erotyka"
  ]
  node [
    id 276
    label "urzeczywistnienie"
  ]
  node [
    id 277
    label "baraszki"
  ]
  node [
    id 278
    label "certificate"
  ]
  node [
    id 279
    label "po&#380;&#261;danie"
  ]
  node [
    id 280
    label "wzw&#243;d"
  ]
  node [
    id 281
    label "funkcja"
  ]
  node [
    id 282
    label "act"
  ]
  node [
    id 283
    label "dokument"
  ]
  node [
    id 284
    label "arystotelizm"
  ]
  node [
    id 285
    label "podnieca&#263;"
  ]
  node [
    id 286
    label "activity"
  ]
  node [
    id 287
    label "bezproblemowy"
  ]
  node [
    id 288
    label "niemi&#322;y"
  ]
  node [
    id 289
    label "przykro"
  ]
  node [
    id 290
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 291
    label "nieprzyjemny"
  ]
  node [
    id 292
    label "niegrzeczny"
  ]
  node [
    id 293
    label "dokuczliwy"
  ]
  node [
    id 294
    label "niepo&#380;&#261;danie"
  ]
  node [
    id 295
    label "z&#322;y"
  ]
  node [
    id 296
    label "niezno&#347;ny"
  ]
  node [
    id 297
    label "niegrzecznie"
  ]
  node [
    id 298
    label "trudny"
  ]
  node [
    id 299
    label "niestosowny"
  ]
  node [
    id 300
    label "brzydal"
  ]
  node [
    id 301
    label "niepos&#322;uszny"
  ]
  node [
    id 302
    label "nieprzyjemnie"
  ]
  node [
    id 303
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 304
    label "niezgodny"
  ]
  node [
    id 305
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 306
    label "niemile"
  ]
  node [
    id 307
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 308
    label "dokuczliwie"
  ]
  node [
    id 309
    label "ploy"
  ]
  node [
    id 310
    label "doj&#347;cie"
  ]
  node [
    id 311
    label "skrycie_si&#281;"
  ]
  node [
    id 312
    label "odwiedzenie"
  ]
  node [
    id 313
    label "zakrycie"
  ]
  node [
    id 314
    label "happening"
  ]
  node [
    id 315
    label "porobienie_si&#281;"
  ]
  node [
    id 316
    label "krajobraz"
  ]
  node [
    id 317
    label "zaniesienie"
  ]
  node [
    id 318
    label "przyobleczenie_si&#281;"
  ]
  node [
    id 319
    label "stanie_si&#281;"
  ]
  node [
    id 320
    label "event"
  ]
  node [
    id 321
    label "podej&#347;cie"
  ]
  node [
    id 322
    label "przestanie"
  ]
  node [
    id 323
    label "droga"
  ]
  node [
    id 324
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 325
    label "nabranie"
  ]
  node [
    id 326
    label "nastawienie"
  ]
  node [
    id 327
    label "potraktowanie"
  ]
  node [
    id 328
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 329
    label "cecha"
  ]
  node [
    id 330
    label "powaga"
  ]
  node [
    id 331
    label "dochodzenie"
  ]
  node [
    id 332
    label "uzyskanie"
  ]
  node [
    id 333
    label "skill"
  ]
  node [
    id 334
    label "dochrapanie_si&#281;"
  ]
  node [
    id 335
    label "znajomo&#347;ci"
  ]
  node [
    id 336
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 337
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 338
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 339
    label "powi&#261;zanie"
  ]
  node [
    id 340
    label "affiliation"
  ]
  node [
    id 341
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 342
    label "dor&#281;czenie"
  ]
  node [
    id 343
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 344
    label "spowodowanie"
  ]
  node [
    id 345
    label "bodziec"
  ]
  node [
    id 346
    label "informacja"
  ]
  node [
    id 347
    label "dost&#281;p"
  ]
  node [
    id 348
    label "przesy&#322;ka"
  ]
  node [
    id 349
    label "gotowy"
  ]
  node [
    id 350
    label "avenue"
  ]
  node [
    id 351
    label "postrzeganie"
  ]
  node [
    id 352
    label "dodatek"
  ]
  node [
    id 353
    label "doznanie"
  ]
  node [
    id 354
    label "dojrza&#322;y"
  ]
  node [
    id 355
    label "dojechanie"
  ]
  node [
    id 356
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 357
    label "ingress"
  ]
  node [
    id 358
    label "strzelenie"
  ]
  node [
    id 359
    label "orzekni&#281;cie"
  ]
  node [
    id 360
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 361
    label "orgazm"
  ]
  node [
    id 362
    label "dolecenie"
  ]
  node [
    id 363
    label "rozpowszechnienie"
  ]
  node [
    id 364
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 365
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 366
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 367
    label "dop&#322;ata"
  ]
  node [
    id 368
    label "zrobienie"
  ]
  node [
    id 369
    label "przebiec"
  ]
  node [
    id 370
    label "charakter"
  ]
  node [
    id 371
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 372
    label "motyw"
  ]
  node [
    id 373
    label "przebiegni&#281;cie"
  ]
  node [
    id 374
    label "fabu&#322;a"
  ]
  node [
    id 375
    label "oddalenie"
  ]
  node [
    id 376
    label "zawitanie"
  ]
  node [
    id 377
    label "coitus_interruptus"
  ]
  node [
    id 378
    label "powstrzymanie"
  ]
  node [
    id 379
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 380
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 381
    label "oduczenie"
  ]
  node [
    id 382
    label "disavowal"
  ]
  node [
    id 383
    label "zako&#324;czenie"
  ]
  node [
    id 384
    label "cessation"
  ]
  node [
    id 385
    label "przeczekanie"
  ]
  node [
    id 386
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 387
    label "cover"
  ]
  node [
    id 388
    label "zas&#322;oni&#281;cie"
  ]
  node [
    id 389
    label "eclipse"
  ]
  node [
    id 390
    label "ukrycie"
  ]
  node [
    id 391
    label "zamkni&#281;cie"
  ]
  node [
    id 392
    label "pozas&#322;anianie"
  ]
  node [
    id 393
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 394
    label "przedstawienie"
  ]
  node [
    id 395
    label "odniesienie"
  ]
  node [
    id 396
    label "dostarczenie"
  ]
  node [
    id 397
    label "przeniesienie"
  ]
  node [
    id 398
    label "zasnucie_si&#281;"
  ]
  node [
    id 399
    label "gaze"
  ]
  node [
    id 400
    label "teren"
  ]
  node [
    id 401
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 402
    label "przestrze&#324;"
  ]
  node [
    id 403
    label "human_body"
  ]
  node [
    id 404
    label "obszar"
  ]
  node [
    id 405
    label "przyroda"
  ]
  node [
    id 406
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 407
    label "obraz"
  ]
  node [
    id 408
    label "zjawisko"
  ]
  node [
    id 409
    label "widok"
  ]
  node [
    id 410
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 411
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 412
    label "terminal"
  ]
  node [
    id 413
    label "budowla"
  ]
  node [
    id 414
    label "droga_ko&#322;owania"
  ]
  node [
    id 415
    label "p&#322;yta_postojowa"
  ]
  node [
    id 416
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 417
    label "aerodrom"
  ]
  node [
    id 418
    label "pas_startowy"
  ]
  node [
    id 419
    label "baza"
  ]
  node [
    id 420
    label "hala"
  ]
  node [
    id 421
    label "betonka"
  ]
  node [
    id 422
    label "rekord"
  ]
  node [
    id 423
    label "base"
  ]
  node [
    id 424
    label "stacjonowanie"
  ]
  node [
    id 425
    label "documentation"
  ]
  node [
    id 426
    label "pole"
  ]
  node [
    id 427
    label "zbi&#243;r"
  ]
  node [
    id 428
    label "zasadzenie"
  ]
  node [
    id 429
    label "zasadzi&#263;"
  ]
  node [
    id 430
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 431
    label "miejsce"
  ]
  node [
    id 432
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 433
    label "podstawowy"
  ]
  node [
    id 434
    label "baseball"
  ]
  node [
    id 435
    label "kolumna"
  ]
  node [
    id 436
    label "kosmetyk"
  ]
  node [
    id 437
    label "za&#322;o&#380;enie"
  ]
  node [
    id 438
    label "punkt_odniesienia"
  ]
  node [
    id 439
    label "boisko"
  ]
  node [
    id 440
    label "system_bazy_danych"
  ]
  node [
    id 441
    label "informatyka"
  ]
  node [
    id 442
    label "podstawa"
  ]
  node [
    id 443
    label "obudowanie"
  ]
  node [
    id 444
    label "obudowywa&#263;"
  ]
  node [
    id 445
    label "zbudowa&#263;"
  ]
  node [
    id 446
    label "obudowa&#263;"
  ]
  node [
    id 447
    label "kolumnada"
  ]
  node [
    id 448
    label "korpus"
  ]
  node [
    id 449
    label "Sukiennice"
  ]
  node [
    id 450
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 451
    label "fundament"
  ]
  node [
    id 452
    label "obudowywanie"
  ]
  node [
    id 453
    label "postanie"
  ]
  node [
    id 454
    label "zbudowanie"
  ]
  node [
    id 455
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 456
    label "stan_surowy"
  ]
  node [
    id 457
    label "konstrukcja"
  ]
  node [
    id 458
    label "rzecz"
  ]
  node [
    id 459
    label "pod&#322;oga"
  ]
  node [
    id 460
    label "urz&#261;dzenie"
  ]
  node [
    id 461
    label "port"
  ]
  node [
    id 462
    label "dworzec"
  ]
  node [
    id 463
    label "oczyszczalnia"
  ]
  node [
    id 464
    label "huta"
  ]
  node [
    id 465
    label "budynek"
  ]
  node [
    id 466
    label "kopalnia"
  ]
  node [
    id 467
    label "pomieszczenie"
  ]
  node [
    id 468
    label "pastwisko"
  ]
  node [
    id 469
    label "pi&#281;tro"
  ]
  node [
    id 470
    label "halizna"
  ]
  node [
    id 471
    label "fabryka"
  ]
  node [
    id 472
    label "Redford"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 472
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 241
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 14
    target 412
  ]
  edge [
    source 14
    target 413
  ]
  edge [
    source 14
    target 414
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 416
  ]
  edge [
    source 14
    target 417
  ]
  edge [
    source 14
    target 418
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 420
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
]
