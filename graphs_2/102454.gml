graph [
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "ukaza&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "nowa"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 5
    label "lawrence"
    origin "text"
  ]
  node [
    id 6
    label "aby"
    origin "text"
  ]
  node [
    id 7
    label "sztuka"
    origin "text"
  ]
  node [
    id 8
    label "biznes"
    origin "text"
  ]
  node [
    id 9
    label "rozkwita&#263;"
    origin "text"
  ]
  node [
    id 10
    label "hybrydowy"
    origin "text"
  ]
  node [
    id 11
    label "gospodarka"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 14
    label "druga"
    origin "text"
  ]
  node [
    id 15
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "wydawnictwo"
    origin "text"
  ]
  node [
    id 18
    label "akademicki"
    origin "text"
  ]
  node [
    id 19
    label "profesjonalny"
    origin "text"
  ]
  node [
    id 20
    label "kultura"
    origin "text"
  ]
  node [
    id 21
    label "lata"
    origin "text"
  ]
  node [
    id 22
    label "temu"
    origin "text"
  ]
  node [
    id 23
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 24
    label "unwrap"
  ]
  node [
    id 25
    label "pokaza&#263;"
  ]
  node [
    id 26
    label "testify"
  ]
  node [
    id 27
    label "point"
  ]
  node [
    id 28
    label "przedstawi&#263;"
  ]
  node [
    id 29
    label "poda&#263;"
  ]
  node [
    id 30
    label "poinformowa&#263;"
  ]
  node [
    id 31
    label "udowodni&#263;"
  ]
  node [
    id 32
    label "spowodowa&#263;"
  ]
  node [
    id 33
    label "wyrazi&#263;"
  ]
  node [
    id 34
    label "przeszkoli&#263;"
  ]
  node [
    id 35
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 36
    label "indicate"
  ]
  node [
    id 37
    label "permit"
  ]
  node [
    id 38
    label "gwiazda"
  ]
  node [
    id 39
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 40
    label "Arktur"
  ]
  node [
    id 41
    label "kszta&#322;t"
  ]
  node [
    id 42
    label "Gwiazda_Polarna"
  ]
  node [
    id 43
    label "agregatka"
  ]
  node [
    id 44
    label "gromada"
  ]
  node [
    id 45
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 46
    label "S&#322;o&#324;ce"
  ]
  node [
    id 47
    label "Nibiru"
  ]
  node [
    id 48
    label "konstelacja"
  ]
  node [
    id 49
    label "ornament"
  ]
  node [
    id 50
    label "delta_Scuti"
  ]
  node [
    id 51
    label "&#347;wiat&#322;o"
  ]
  node [
    id 52
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 53
    label "obiekt"
  ]
  node [
    id 54
    label "s&#322;awa"
  ]
  node [
    id 55
    label "promie&#324;"
  ]
  node [
    id 56
    label "star"
  ]
  node [
    id 57
    label "gwiazdosz"
  ]
  node [
    id 58
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 59
    label "asocjacja_gwiazd"
  ]
  node [
    id 60
    label "supergrupa"
  ]
  node [
    id 61
    label "troch&#281;"
  ]
  node [
    id 62
    label "pr&#243;bowanie"
  ]
  node [
    id 63
    label "rola"
  ]
  node [
    id 64
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 65
    label "przedmiot"
  ]
  node [
    id 66
    label "cz&#322;owiek"
  ]
  node [
    id 67
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 68
    label "realizacja"
  ]
  node [
    id 69
    label "scena"
  ]
  node [
    id 70
    label "didaskalia"
  ]
  node [
    id 71
    label "czyn"
  ]
  node [
    id 72
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 73
    label "environment"
  ]
  node [
    id 74
    label "head"
  ]
  node [
    id 75
    label "scenariusz"
  ]
  node [
    id 76
    label "egzemplarz"
  ]
  node [
    id 77
    label "jednostka"
  ]
  node [
    id 78
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 79
    label "utw&#243;r"
  ]
  node [
    id 80
    label "kultura_duchowa"
  ]
  node [
    id 81
    label "fortel"
  ]
  node [
    id 82
    label "theatrical_performance"
  ]
  node [
    id 83
    label "ambala&#380;"
  ]
  node [
    id 84
    label "sprawno&#347;&#263;"
  ]
  node [
    id 85
    label "kobieta"
  ]
  node [
    id 86
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 87
    label "Faust"
  ]
  node [
    id 88
    label "scenografia"
  ]
  node [
    id 89
    label "ods&#322;ona"
  ]
  node [
    id 90
    label "turn"
  ]
  node [
    id 91
    label "pokaz"
  ]
  node [
    id 92
    label "ilo&#347;&#263;"
  ]
  node [
    id 93
    label "przedstawienie"
  ]
  node [
    id 94
    label "Apollo"
  ]
  node [
    id 95
    label "przedstawianie"
  ]
  node [
    id 96
    label "przedstawia&#263;"
  ]
  node [
    id 97
    label "towar"
  ]
  node [
    id 98
    label "przyswoi&#263;"
  ]
  node [
    id 99
    label "ludzko&#347;&#263;"
  ]
  node [
    id 100
    label "one"
  ]
  node [
    id 101
    label "poj&#281;cie"
  ]
  node [
    id 102
    label "ewoluowanie"
  ]
  node [
    id 103
    label "supremum"
  ]
  node [
    id 104
    label "skala"
  ]
  node [
    id 105
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 106
    label "przyswajanie"
  ]
  node [
    id 107
    label "wyewoluowanie"
  ]
  node [
    id 108
    label "reakcja"
  ]
  node [
    id 109
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 110
    label "przeliczy&#263;"
  ]
  node [
    id 111
    label "wyewoluowa&#263;"
  ]
  node [
    id 112
    label "ewoluowa&#263;"
  ]
  node [
    id 113
    label "matematyka"
  ]
  node [
    id 114
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 115
    label "rzut"
  ]
  node [
    id 116
    label "liczba_naturalna"
  ]
  node [
    id 117
    label "czynnik_biotyczny"
  ]
  node [
    id 118
    label "g&#322;owa"
  ]
  node [
    id 119
    label "figura"
  ]
  node [
    id 120
    label "individual"
  ]
  node [
    id 121
    label "portrecista"
  ]
  node [
    id 122
    label "przyswaja&#263;"
  ]
  node [
    id 123
    label "przyswojenie"
  ]
  node [
    id 124
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 125
    label "profanum"
  ]
  node [
    id 126
    label "mikrokosmos"
  ]
  node [
    id 127
    label "starzenie_si&#281;"
  ]
  node [
    id 128
    label "duch"
  ]
  node [
    id 129
    label "przeliczanie"
  ]
  node [
    id 130
    label "osoba"
  ]
  node [
    id 131
    label "oddzia&#322;ywanie"
  ]
  node [
    id 132
    label "antropochoria"
  ]
  node [
    id 133
    label "funkcja"
  ]
  node [
    id 134
    label "homo_sapiens"
  ]
  node [
    id 135
    label "przelicza&#263;"
  ]
  node [
    id 136
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 137
    label "infimum"
  ]
  node [
    id 138
    label "przeliczenie"
  ]
  node [
    id 139
    label "zbi&#243;r"
  ]
  node [
    id 140
    label "dorobek"
  ]
  node [
    id 141
    label "tworzenie"
  ]
  node [
    id 142
    label "kreacja"
  ]
  node [
    id 143
    label "creation"
  ]
  node [
    id 144
    label "asymilowanie"
  ]
  node [
    id 145
    label "wapniak"
  ]
  node [
    id 146
    label "asymilowa&#263;"
  ]
  node [
    id 147
    label "os&#322;abia&#263;"
  ]
  node [
    id 148
    label "posta&#263;"
  ]
  node [
    id 149
    label "hominid"
  ]
  node [
    id 150
    label "podw&#322;adny"
  ]
  node [
    id 151
    label "os&#322;abianie"
  ]
  node [
    id 152
    label "dwun&#243;g"
  ]
  node [
    id 153
    label "nasada"
  ]
  node [
    id 154
    label "wz&#243;r"
  ]
  node [
    id 155
    label "senior"
  ]
  node [
    id 156
    label "Adam"
  ]
  node [
    id 157
    label "polifag"
  ]
  node [
    id 158
    label "act"
  ]
  node [
    id 159
    label "rozmiar"
  ]
  node [
    id 160
    label "part"
  ]
  node [
    id 161
    label "pokaz&#243;wka"
  ]
  node [
    id 162
    label "prezenter"
  ]
  node [
    id 163
    label "wydarzenie"
  ]
  node [
    id 164
    label "wyraz"
  ]
  node [
    id 165
    label "impreza"
  ]
  node [
    id 166
    label "show"
  ]
  node [
    id 167
    label "fabrication"
  ]
  node [
    id 168
    label "scheduling"
  ]
  node [
    id 169
    label "operacja"
  ]
  node [
    id 170
    label "proces"
  ]
  node [
    id 171
    label "dzie&#322;o"
  ]
  node [
    id 172
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 173
    label "monta&#380;"
  ]
  node [
    id 174
    label "postprodukcja"
  ]
  node [
    id 175
    label "performance"
  ]
  node [
    id 176
    label "zboczenie"
  ]
  node [
    id 177
    label "om&#243;wienie"
  ]
  node [
    id 178
    label "sponiewieranie"
  ]
  node [
    id 179
    label "discipline"
  ]
  node [
    id 180
    label "rzecz"
  ]
  node [
    id 181
    label "omawia&#263;"
  ]
  node [
    id 182
    label "kr&#261;&#380;enie"
  ]
  node [
    id 183
    label "tre&#347;&#263;"
  ]
  node [
    id 184
    label "robienie"
  ]
  node [
    id 185
    label "sponiewiera&#263;"
  ]
  node [
    id 186
    label "element"
  ]
  node [
    id 187
    label "entity"
  ]
  node [
    id 188
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 189
    label "tematyka"
  ]
  node [
    id 190
    label "w&#261;tek"
  ]
  node [
    id 191
    label "charakter"
  ]
  node [
    id 192
    label "zbaczanie"
  ]
  node [
    id 193
    label "program_nauczania"
  ]
  node [
    id 194
    label "om&#243;wi&#263;"
  ]
  node [
    id 195
    label "omawianie"
  ]
  node [
    id 196
    label "thing"
  ]
  node [
    id 197
    label "istota"
  ]
  node [
    id 198
    label "zbacza&#263;"
  ]
  node [
    id 199
    label "zboczy&#263;"
  ]
  node [
    id 200
    label "metka"
  ]
  node [
    id 201
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 202
    label "szprycowa&#263;"
  ]
  node [
    id 203
    label "naszprycowa&#263;"
  ]
  node [
    id 204
    label "rzuca&#263;"
  ]
  node [
    id 205
    label "tandeta"
  ]
  node [
    id 206
    label "obr&#243;t_handlowy"
  ]
  node [
    id 207
    label "wyr&#243;b"
  ]
  node [
    id 208
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 209
    label "rzuci&#263;"
  ]
  node [
    id 210
    label "naszprycowanie"
  ]
  node [
    id 211
    label "tkanina"
  ]
  node [
    id 212
    label "szprycowanie"
  ]
  node [
    id 213
    label "za&#322;adownia"
  ]
  node [
    id 214
    label "asortyment"
  ]
  node [
    id 215
    label "&#322;&#243;dzki"
  ]
  node [
    id 216
    label "narkobiznes"
  ]
  node [
    id 217
    label "rzucenie"
  ]
  node [
    id 218
    label "rzucanie"
  ]
  node [
    id 219
    label "doros&#322;y"
  ]
  node [
    id 220
    label "&#380;ona"
  ]
  node [
    id 221
    label "samica"
  ]
  node [
    id 222
    label "uleganie"
  ]
  node [
    id 223
    label "ulec"
  ]
  node [
    id 224
    label "m&#281;&#380;yna"
  ]
  node [
    id 225
    label "partnerka"
  ]
  node [
    id 226
    label "ulegni&#281;cie"
  ]
  node [
    id 227
    label "pa&#324;stwo"
  ]
  node [
    id 228
    label "&#322;ono"
  ]
  node [
    id 229
    label "menopauza"
  ]
  node [
    id 230
    label "przekwitanie"
  ]
  node [
    id 231
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 232
    label "babka"
  ]
  node [
    id 233
    label "ulega&#263;"
  ]
  node [
    id 234
    label "jako&#347;&#263;"
  ]
  node [
    id 235
    label "szybko&#347;&#263;"
  ]
  node [
    id 236
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 237
    label "kondycja_fizyczna"
  ]
  node [
    id 238
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 239
    label "zdrowie"
  ]
  node [
    id 240
    label "stan"
  ]
  node [
    id 241
    label "harcerski"
  ]
  node [
    id 242
    label "cecha"
  ]
  node [
    id 243
    label "odznaka"
  ]
  node [
    id 244
    label "obrazowanie"
  ]
  node [
    id 245
    label "organ"
  ]
  node [
    id 246
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 247
    label "element_anatomiczny"
  ]
  node [
    id 248
    label "tekst"
  ]
  node [
    id 249
    label "komunikat"
  ]
  node [
    id 250
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 251
    label "wytw&#243;r"
  ]
  node [
    id 252
    label "okaz"
  ]
  node [
    id 253
    label "agent"
  ]
  node [
    id 254
    label "nicpo&#324;"
  ]
  node [
    id 255
    label "pean"
  ]
  node [
    id 256
    label "teatr"
  ]
  node [
    id 257
    label "exhibit"
  ]
  node [
    id 258
    label "podawa&#263;"
  ]
  node [
    id 259
    label "display"
  ]
  node [
    id 260
    label "pokazywa&#263;"
  ]
  node [
    id 261
    label "demonstrowa&#263;"
  ]
  node [
    id 262
    label "zapoznawa&#263;"
  ]
  node [
    id 263
    label "opisywa&#263;"
  ]
  node [
    id 264
    label "ukazywa&#263;"
  ]
  node [
    id 265
    label "represent"
  ]
  node [
    id 266
    label "zg&#322;asza&#263;"
  ]
  node [
    id 267
    label "typify"
  ]
  node [
    id 268
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 269
    label "attest"
  ]
  node [
    id 270
    label "stanowi&#263;"
  ]
  node [
    id 271
    label "zademonstrowanie"
  ]
  node [
    id 272
    label "report"
  ]
  node [
    id 273
    label "obgadanie"
  ]
  node [
    id 274
    label "narration"
  ]
  node [
    id 275
    label "cyrk"
  ]
  node [
    id 276
    label "opisanie"
  ]
  node [
    id 277
    label "malarstwo"
  ]
  node [
    id 278
    label "ukazanie"
  ]
  node [
    id 279
    label "zapoznanie"
  ]
  node [
    id 280
    label "podanie"
  ]
  node [
    id 281
    label "spos&#243;b"
  ]
  node [
    id 282
    label "pokazanie"
  ]
  node [
    id 283
    label "wyst&#261;pienie"
  ]
  node [
    id 284
    label "stara&#263;_si&#281;"
  ]
  node [
    id 285
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 286
    label "sprawdza&#263;"
  ]
  node [
    id 287
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 288
    label "feel"
  ]
  node [
    id 289
    label "try"
  ]
  node [
    id 290
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 291
    label "kosztowa&#263;"
  ]
  node [
    id 292
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 293
    label "medialno&#347;&#263;"
  ]
  node [
    id 294
    label "zapozna&#263;"
  ]
  node [
    id 295
    label "express"
  ]
  node [
    id 296
    label "zaproponowa&#263;"
  ]
  node [
    id 297
    label "zademonstrowa&#263;"
  ]
  node [
    id 298
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 299
    label "opisa&#263;"
  ]
  node [
    id 300
    label "badanie"
  ]
  node [
    id 301
    label "jedzenie"
  ]
  node [
    id 302
    label "podejmowanie"
  ]
  node [
    id 303
    label "usi&#322;owanie"
  ]
  node [
    id 304
    label "tasting"
  ]
  node [
    id 305
    label "kiperstwo"
  ]
  node [
    id 306
    label "staranie_si&#281;"
  ]
  node [
    id 307
    label "zaznawanie"
  ]
  node [
    id 308
    label "przygotowywanie_si&#281;"
  ]
  node [
    id 309
    label "essay"
  ]
  node [
    id 310
    label "opisywanie"
  ]
  node [
    id 311
    label "bycie"
  ]
  node [
    id 312
    label "representation"
  ]
  node [
    id 313
    label "obgadywanie"
  ]
  node [
    id 314
    label "zapoznawanie"
  ]
  node [
    id 315
    label "wyst&#281;powanie"
  ]
  node [
    id 316
    label "ukazywanie"
  ]
  node [
    id 317
    label "pokazywanie"
  ]
  node [
    id 318
    label "podawanie"
  ]
  node [
    id 319
    label "demonstrowanie"
  ]
  node [
    id 320
    label "presentation"
  ]
  node [
    id 321
    label "dramat"
  ]
  node [
    id 322
    label "plan"
  ]
  node [
    id 323
    label "prognoza"
  ]
  node [
    id 324
    label "scenario"
  ]
  node [
    id 325
    label "podwy&#380;szenie"
  ]
  node [
    id 326
    label "kurtyna"
  ]
  node [
    id 327
    label "akt"
  ]
  node [
    id 328
    label "widzownia"
  ]
  node [
    id 329
    label "sznurownia"
  ]
  node [
    id 330
    label "dramaturgy"
  ]
  node [
    id 331
    label "sphere"
  ]
  node [
    id 332
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 333
    label "budka_suflera"
  ]
  node [
    id 334
    label "epizod"
  ]
  node [
    id 335
    label "film"
  ]
  node [
    id 336
    label "fragment"
  ]
  node [
    id 337
    label "k&#322;&#243;tnia"
  ]
  node [
    id 338
    label "kiesze&#324;"
  ]
  node [
    id 339
    label "stadium"
  ]
  node [
    id 340
    label "podest"
  ]
  node [
    id 341
    label "horyzont"
  ]
  node [
    id 342
    label "teren"
  ]
  node [
    id 343
    label "instytucja"
  ]
  node [
    id 344
    label "proscenium"
  ]
  node [
    id 345
    label "nadscenie"
  ]
  node [
    id 346
    label "antyteatr"
  ]
  node [
    id 347
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 348
    label "mansjon"
  ]
  node [
    id 349
    label "modelatornia"
  ]
  node [
    id 350
    label "dekoracja"
  ]
  node [
    id 351
    label "uprawienie"
  ]
  node [
    id 352
    label "dialog"
  ]
  node [
    id 353
    label "p&#322;osa"
  ]
  node [
    id 354
    label "wykonywanie"
  ]
  node [
    id 355
    label "plik"
  ]
  node [
    id 356
    label "ziemia"
  ]
  node [
    id 357
    label "wykonywa&#263;"
  ]
  node [
    id 358
    label "ustawienie"
  ]
  node [
    id 359
    label "pole"
  ]
  node [
    id 360
    label "gospodarstwo"
  ]
  node [
    id 361
    label "uprawi&#263;"
  ]
  node [
    id 362
    label "function"
  ]
  node [
    id 363
    label "zreinterpretowa&#263;"
  ]
  node [
    id 364
    label "zastosowanie"
  ]
  node [
    id 365
    label "reinterpretowa&#263;"
  ]
  node [
    id 366
    label "wrench"
  ]
  node [
    id 367
    label "irygowanie"
  ]
  node [
    id 368
    label "ustawi&#263;"
  ]
  node [
    id 369
    label "irygowa&#263;"
  ]
  node [
    id 370
    label "zreinterpretowanie"
  ]
  node [
    id 371
    label "cel"
  ]
  node [
    id 372
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 373
    label "gra&#263;"
  ]
  node [
    id 374
    label "aktorstwo"
  ]
  node [
    id 375
    label "kostium"
  ]
  node [
    id 376
    label "zagon"
  ]
  node [
    id 377
    label "znaczenie"
  ]
  node [
    id 378
    label "zagra&#263;"
  ]
  node [
    id 379
    label "reinterpretowanie"
  ]
  node [
    id 380
    label "sk&#322;ad"
  ]
  node [
    id 381
    label "zagranie"
  ]
  node [
    id 382
    label "radlina"
  ]
  node [
    id 383
    label "granie"
  ]
  node [
    id 384
    label "stage_direction"
  ]
  node [
    id 385
    label "obja&#347;nienie"
  ]
  node [
    id 386
    label "asymilowanie_si&#281;"
  ]
  node [
    id 387
    label "Wsch&#243;d"
  ]
  node [
    id 388
    label "praca_rolnicza"
  ]
  node [
    id 389
    label "przejmowanie"
  ]
  node [
    id 390
    label "zjawisko"
  ]
  node [
    id 391
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 392
    label "makrokosmos"
  ]
  node [
    id 393
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 394
    label "konwencja"
  ]
  node [
    id 395
    label "propriety"
  ]
  node [
    id 396
    label "przejmowa&#263;"
  ]
  node [
    id 397
    label "brzoskwiniarnia"
  ]
  node [
    id 398
    label "zwyczaj"
  ]
  node [
    id 399
    label "kuchnia"
  ]
  node [
    id 400
    label "tradycja"
  ]
  node [
    id 401
    label "populace"
  ]
  node [
    id 402
    label "hodowla"
  ]
  node [
    id 403
    label "religia"
  ]
  node [
    id 404
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 405
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 406
    label "przej&#281;cie"
  ]
  node [
    id 407
    label "przej&#261;&#263;"
  ]
  node [
    id 408
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 409
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 410
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 411
    label "&#347;rodek"
  ]
  node [
    id 412
    label "manewr"
  ]
  node [
    id 413
    label "chwyt"
  ]
  node [
    id 414
    label "game"
  ]
  node [
    id 415
    label "podchwyt"
  ]
  node [
    id 416
    label "sprawa"
  ]
  node [
    id 417
    label "rynek"
  ]
  node [
    id 418
    label "object"
  ]
  node [
    id 419
    label "Apeks"
  ]
  node [
    id 420
    label "zasoby"
  ]
  node [
    id 421
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 422
    label "reengineering"
  ]
  node [
    id 423
    label "Hortex"
  ]
  node [
    id 424
    label "korzy&#347;&#263;"
  ]
  node [
    id 425
    label "podmiot_gospodarczy"
  ]
  node [
    id 426
    label "Orlen"
  ]
  node [
    id 427
    label "interes"
  ]
  node [
    id 428
    label "Google"
  ]
  node [
    id 429
    label "Canon"
  ]
  node [
    id 430
    label "Pewex"
  ]
  node [
    id 431
    label "MAN_SE"
  ]
  node [
    id 432
    label "Spo&#322;em"
  ]
  node [
    id 433
    label "networking"
  ]
  node [
    id 434
    label "MAC"
  ]
  node [
    id 435
    label "zasoby_ludzkie"
  ]
  node [
    id 436
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 437
    label "Baltona"
  ]
  node [
    id 438
    label "Orbis"
  ]
  node [
    id 439
    label "HP"
  ]
  node [
    id 440
    label "absolutorium"
  ]
  node [
    id 441
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 442
    label "dzia&#322;anie"
  ]
  node [
    id 443
    label "activity"
  ]
  node [
    id 444
    label "kognicja"
  ]
  node [
    id 445
    label "rozprawa"
  ]
  node [
    id 446
    label "temat"
  ]
  node [
    id 447
    label "szczeg&#243;&#322;"
  ]
  node [
    id 448
    label "proposition"
  ]
  node [
    id 449
    label "przes&#322;anka"
  ]
  node [
    id 450
    label "idea"
  ]
  node [
    id 451
    label "zaleta"
  ]
  node [
    id 452
    label "dobro"
  ]
  node [
    id 453
    label "consumption"
  ]
  node [
    id 454
    label "zacz&#281;cie"
  ]
  node [
    id 455
    label "startup"
  ]
  node [
    id 456
    label "zrobienie"
  ]
  node [
    id 457
    label "stoisko"
  ]
  node [
    id 458
    label "rynek_podstawowy"
  ]
  node [
    id 459
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 460
    label "konsument"
  ]
  node [
    id 461
    label "pojawienie_si&#281;"
  ]
  node [
    id 462
    label "obiekt_handlowy"
  ]
  node [
    id 463
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 464
    label "wytw&#243;rca"
  ]
  node [
    id 465
    label "rynek_wt&#243;rny"
  ]
  node [
    id 466
    label "wprowadzanie"
  ]
  node [
    id 467
    label "wprowadza&#263;"
  ]
  node [
    id 468
    label "kram"
  ]
  node [
    id 469
    label "plac"
  ]
  node [
    id 470
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 471
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 472
    label "emitowa&#263;"
  ]
  node [
    id 473
    label "wprowadzi&#263;"
  ]
  node [
    id 474
    label "emitowanie"
  ]
  node [
    id 475
    label "segment_rynku"
  ]
  node [
    id 476
    label "wprowadzenie"
  ]
  node [
    id 477
    label "targowica"
  ]
  node [
    id 478
    label "firma"
  ]
  node [
    id 479
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 480
    label "przer&#243;bka"
  ]
  node [
    id 481
    label "odmienienie"
  ]
  node [
    id 482
    label "strategia"
  ]
  node [
    id 483
    label "oprogramowanie"
  ]
  node [
    id 484
    label "zmienia&#263;"
  ]
  node [
    id 485
    label "penis"
  ]
  node [
    id 486
    label "zasoby_kopalin"
  ]
  node [
    id 487
    label "z&#322;o&#380;e"
  ]
  node [
    id 488
    label "informatyka"
  ]
  node [
    id 489
    label "ropa_naftowa"
  ]
  node [
    id 490
    label "paliwo"
  ]
  node [
    id 491
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 492
    label "driveway"
  ]
  node [
    id 493
    label "doro&#347;le&#263;"
  ]
  node [
    id 494
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 495
    label "blow"
  ]
  node [
    id 496
    label "&#347;wita&#263;"
  ]
  node [
    id 497
    label "zapala&#263;_si&#281;"
  ]
  node [
    id 498
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 499
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 500
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 501
    label "b&#322;yska&#263;"
  ]
  node [
    id 502
    label "wygl&#261;da&#263;"
  ]
  node [
    id 503
    label "dawn"
  ]
  node [
    id 504
    label "prze&#347;witywa&#263;"
  ]
  node [
    id 505
    label "przeja&#347;nia&#263;_si&#281;"
  ]
  node [
    id 506
    label "przychodzi&#263;"
  ]
  node [
    id 507
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 508
    label "ripen"
  ]
  node [
    id 509
    label "podrasta&#263;"
  ]
  node [
    id 510
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 511
    label "hybrydowo"
  ]
  node [
    id 512
    label "z&#322;o&#380;ony"
  ]
  node [
    id 513
    label "r&#243;&#380;norodnie"
  ]
  node [
    id 514
    label "inwentarz"
  ]
  node [
    id 515
    label "mieszkalnictwo"
  ]
  node [
    id 516
    label "agregat_ekonomiczny"
  ]
  node [
    id 517
    label "miejsce_pracy"
  ]
  node [
    id 518
    label "farmaceutyka"
  ]
  node [
    id 519
    label "produkowanie"
  ]
  node [
    id 520
    label "rolnictwo"
  ]
  node [
    id 521
    label "transport"
  ]
  node [
    id 522
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 523
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 524
    label "obronno&#347;&#263;"
  ]
  node [
    id 525
    label "sektor_prywatny"
  ]
  node [
    id 526
    label "sch&#322;adza&#263;"
  ]
  node [
    id 527
    label "czerwona_strefa"
  ]
  node [
    id 528
    label "struktura"
  ]
  node [
    id 529
    label "sektor_publiczny"
  ]
  node [
    id 530
    label "bankowo&#347;&#263;"
  ]
  node [
    id 531
    label "gospodarowanie"
  ]
  node [
    id 532
    label "obora"
  ]
  node [
    id 533
    label "gospodarka_wodna"
  ]
  node [
    id 534
    label "gospodarka_le&#347;na"
  ]
  node [
    id 535
    label "gospodarowa&#263;"
  ]
  node [
    id 536
    label "fabryka"
  ]
  node [
    id 537
    label "wytw&#243;rnia"
  ]
  node [
    id 538
    label "stodo&#322;a"
  ]
  node [
    id 539
    label "przemys&#322;"
  ]
  node [
    id 540
    label "spichlerz"
  ]
  node [
    id 541
    label "sch&#322;adzanie"
  ]
  node [
    id 542
    label "administracja"
  ]
  node [
    id 543
    label "sch&#322;odzenie"
  ]
  node [
    id 544
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 545
    label "zasada"
  ]
  node [
    id 546
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 547
    label "regulacja_cen"
  ]
  node [
    id 548
    label "szkolnictwo"
  ]
  node [
    id 549
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 550
    label "mechanika"
  ]
  node [
    id 551
    label "o&#347;"
  ]
  node [
    id 552
    label "usenet"
  ]
  node [
    id 553
    label "rozprz&#261;c"
  ]
  node [
    id 554
    label "zachowanie"
  ]
  node [
    id 555
    label "cybernetyk"
  ]
  node [
    id 556
    label "podsystem"
  ]
  node [
    id 557
    label "system"
  ]
  node [
    id 558
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 559
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 560
    label "systemat"
  ]
  node [
    id 561
    label "konstrukcja"
  ]
  node [
    id 562
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 563
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 564
    label "regu&#322;a_Allena"
  ]
  node [
    id 565
    label "base"
  ]
  node [
    id 566
    label "umowa"
  ]
  node [
    id 567
    label "obserwacja"
  ]
  node [
    id 568
    label "zasada_d'Alemberta"
  ]
  node [
    id 569
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 570
    label "normalizacja"
  ]
  node [
    id 571
    label "moralno&#347;&#263;"
  ]
  node [
    id 572
    label "criterion"
  ]
  node [
    id 573
    label "opis"
  ]
  node [
    id 574
    label "regu&#322;a_Glogera"
  ]
  node [
    id 575
    label "prawo_Mendla"
  ]
  node [
    id 576
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 577
    label "twierdzenie"
  ]
  node [
    id 578
    label "prawo"
  ]
  node [
    id 579
    label "standard"
  ]
  node [
    id 580
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 581
    label "dominion"
  ]
  node [
    id 582
    label "qualification"
  ]
  node [
    id 583
    label "occupation"
  ]
  node [
    id 584
    label "podstawa"
  ]
  node [
    id 585
    label "substancja"
  ]
  node [
    id 586
    label "prawid&#322;o"
  ]
  node [
    id 587
    label "stable"
  ]
  node [
    id 588
    label "budynek_inwentarski"
  ]
  node [
    id 589
    label "krowiarnia"
  ]
  node [
    id 590
    label "building"
  ]
  node [
    id 591
    label "wr&#243;tnia"
  ]
  node [
    id 592
    label "budynek_gospodarczy"
  ]
  node [
    id 593
    label "budynek"
  ]
  node [
    id 594
    label "s&#261;siek"
  ]
  node [
    id 595
    label "szafarnia"
  ]
  node [
    id 596
    label "&#347;pichrz"
  ]
  node [
    id 597
    label "region"
  ]
  node [
    id 598
    label "zbo&#380;e"
  ]
  node [
    id 599
    label "zbiornik"
  ]
  node [
    id 600
    label "magazyn"
  ]
  node [
    id 601
    label "&#347;pichlerz"
  ]
  node [
    id 602
    label "u&#322;o&#380;enie"
  ]
  node [
    id 603
    label "t&#322;o"
  ]
  node [
    id 604
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 605
    label "room"
  ]
  node [
    id 606
    label "dw&#243;r"
  ]
  node [
    id 607
    label "okazja"
  ]
  node [
    id 608
    label "compass"
  ]
  node [
    id 609
    label "square"
  ]
  node [
    id 610
    label "zmienna"
  ]
  node [
    id 611
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 612
    label "socjologia"
  ]
  node [
    id 613
    label "boisko"
  ]
  node [
    id 614
    label "dziedzina"
  ]
  node [
    id 615
    label "baza_danych"
  ]
  node [
    id 616
    label "przestrze&#324;"
  ]
  node [
    id 617
    label "obszar"
  ]
  node [
    id 618
    label "powierzchnia"
  ]
  node [
    id 619
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 620
    label "plane"
  ]
  node [
    id 621
    label "mienie"
  ]
  node [
    id 622
    label "spis"
  ]
  node [
    id 623
    label "stock"
  ]
  node [
    id 624
    label "Karta_Nauczyciela"
  ]
  node [
    id 625
    label "finanse"
  ]
  node [
    id 626
    label "bankowo&#347;&#263;_elektroniczna"
  ]
  node [
    id 627
    label "bankowo&#347;&#263;_detaliczna"
  ]
  node [
    id 628
    label "bankowo&#347;&#263;_inwestycyjna"
  ]
  node [
    id 629
    label "supernadz&#243;r"
  ]
  node [
    id 630
    label "bankowo&#347;&#263;_sp&#243;&#322;dzielcza"
  ]
  node [
    id 631
    label "ekonomia"
  ]
  node [
    id 632
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 633
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 634
    label "uprzemys&#322;owienie"
  ]
  node [
    id 635
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 636
    label "przechowalnictwo"
  ]
  node [
    id 637
    label "uprzemys&#322;awianie"
  ]
  node [
    id 638
    label "mieszkani&#243;wka"
  ]
  node [
    id 639
    label "roz&#322;adunek"
  ]
  node [
    id 640
    label "sprz&#281;t"
  ]
  node [
    id 641
    label "cedu&#322;a"
  ]
  node [
    id 642
    label "jednoszynowy"
  ]
  node [
    id 643
    label "unos"
  ]
  node [
    id 644
    label "traffic"
  ]
  node [
    id 645
    label "prze&#322;adunek"
  ]
  node [
    id 646
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 647
    label "us&#322;uga"
  ]
  node [
    id 648
    label "komunikacja"
  ]
  node [
    id 649
    label "tyfon"
  ]
  node [
    id 650
    label "zawarto&#347;&#263;"
  ]
  node [
    id 651
    label "grupa"
  ]
  node [
    id 652
    label "za&#322;adunek"
  ]
  node [
    id 653
    label "nasiennictwo"
  ]
  node [
    id 654
    label "agrotechnika"
  ]
  node [
    id 655
    label "agroekologia"
  ]
  node [
    id 656
    label "agrobiznes"
  ]
  node [
    id 657
    label "intensyfikacja"
  ]
  node [
    id 658
    label "uprawianie"
  ]
  node [
    id 659
    label "gleboznawstwo"
  ]
  node [
    id 660
    label "ogrodnictwo"
  ]
  node [
    id 661
    label "agronomia"
  ]
  node [
    id 662
    label "agrochemia"
  ]
  node [
    id 663
    label "farmerstwo"
  ]
  node [
    id 664
    label "zootechnika"
  ]
  node [
    id 665
    label "zgarniacz"
  ]
  node [
    id 666
    label "nauka"
  ]
  node [
    id 667
    label "sadownictwo"
  ]
  node [
    id 668
    label "&#322;&#261;karstwo"
  ]
  node [
    id 669
    label "&#322;owiectwo"
  ]
  node [
    id 670
    label "biuro"
  ]
  node [
    id 671
    label "siedziba"
  ]
  node [
    id 672
    label "zarz&#261;d"
  ]
  node [
    id 673
    label "petent"
  ]
  node [
    id 674
    label "dziekanat"
  ]
  node [
    id 675
    label "establishment"
  ]
  node [
    id 676
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 677
    label "pojawianie_si&#281;"
  ]
  node [
    id 678
    label "manage"
  ]
  node [
    id 679
    label "control"
  ]
  node [
    id 680
    label "pracowa&#263;"
  ]
  node [
    id 681
    label "trzyma&#263;"
  ]
  node [
    id 682
    label "dysponowa&#263;"
  ]
  node [
    id 683
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 684
    label "cope"
  ]
  node [
    id 685
    label "dysponowanie"
  ]
  node [
    id 686
    label "dawanie"
  ]
  node [
    id 687
    label "trzymanie"
  ]
  node [
    id 688
    label "rozdysponowywanie"
  ]
  node [
    id 689
    label "zarz&#261;dzanie"
  ]
  node [
    id 690
    label "housework"
  ]
  node [
    id 691
    label "pozarz&#261;dzanie"
  ]
  node [
    id 692
    label "pracowanie"
  ]
  node [
    id 693
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 694
    label "obni&#380;enie"
  ]
  node [
    id 695
    label "wyhamowanie"
  ]
  node [
    id 696
    label "ch&#322;odny"
  ]
  node [
    id 697
    label "cooling"
  ]
  node [
    id 698
    label "obni&#380;a&#263;"
  ]
  node [
    id 699
    label "aplomb"
  ]
  node [
    id 700
    label "squelch"
  ]
  node [
    id 701
    label "hamowa&#263;"
  ]
  node [
    id 702
    label "obni&#380;y&#263;"
  ]
  node [
    id 703
    label "wyhamowa&#263;"
  ]
  node [
    id 704
    label "cool"
  ]
  node [
    id 705
    label "obni&#380;anie"
  ]
  node [
    id 706
    label "hamowanie"
  ]
  node [
    id 707
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 708
    label "refrigeration"
  ]
  node [
    id 709
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 710
    label "prz&#281;dzalnia"
  ]
  node [
    id 711
    label "rurownia"
  ]
  node [
    id 712
    label "wytrawialnia"
  ]
  node [
    id 713
    label "ucieralnia"
  ]
  node [
    id 714
    label "tkalnia"
  ]
  node [
    id 715
    label "farbiarnia"
  ]
  node [
    id 716
    label "szwalnia"
  ]
  node [
    id 717
    label "szlifiernia"
  ]
  node [
    id 718
    label "probiernia"
  ]
  node [
    id 719
    label "fryzernia"
  ]
  node [
    id 720
    label "celulozownia"
  ]
  node [
    id 721
    label "hala"
  ]
  node [
    id 722
    label "dziewiarnia"
  ]
  node [
    id 723
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 724
    label "obieralnia"
  ]
  node [
    id 725
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 726
    label "mie&#263;_miejsce"
  ]
  node [
    id 727
    label "equal"
  ]
  node [
    id 728
    label "trwa&#263;"
  ]
  node [
    id 729
    label "chodzi&#263;"
  ]
  node [
    id 730
    label "si&#281;ga&#263;"
  ]
  node [
    id 731
    label "obecno&#347;&#263;"
  ]
  node [
    id 732
    label "stand"
  ]
  node [
    id 733
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 734
    label "uczestniczy&#263;"
  ]
  node [
    id 735
    label "participate"
  ]
  node [
    id 736
    label "robi&#263;"
  ]
  node [
    id 737
    label "istnie&#263;"
  ]
  node [
    id 738
    label "pozostawa&#263;"
  ]
  node [
    id 739
    label "zostawa&#263;"
  ]
  node [
    id 740
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 741
    label "adhere"
  ]
  node [
    id 742
    label "korzysta&#263;"
  ]
  node [
    id 743
    label "appreciation"
  ]
  node [
    id 744
    label "osi&#261;ga&#263;"
  ]
  node [
    id 745
    label "dociera&#263;"
  ]
  node [
    id 746
    label "get"
  ]
  node [
    id 747
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 748
    label "mierzy&#263;"
  ]
  node [
    id 749
    label "u&#380;ywa&#263;"
  ]
  node [
    id 750
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 751
    label "exsert"
  ]
  node [
    id 752
    label "being"
  ]
  node [
    id 753
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 754
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 755
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 756
    label "p&#322;ywa&#263;"
  ]
  node [
    id 757
    label "run"
  ]
  node [
    id 758
    label "bangla&#263;"
  ]
  node [
    id 759
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 760
    label "przebiega&#263;"
  ]
  node [
    id 761
    label "wk&#322;ada&#263;"
  ]
  node [
    id 762
    label "proceed"
  ]
  node [
    id 763
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 764
    label "carry"
  ]
  node [
    id 765
    label "bywa&#263;"
  ]
  node [
    id 766
    label "dziama&#263;"
  ]
  node [
    id 767
    label "para"
  ]
  node [
    id 768
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 769
    label "str&#243;j"
  ]
  node [
    id 770
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 771
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 772
    label "krok"
  ]
  node [
    id 773
    label "tryb"
  ]
  node [
    id 774
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 775
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 776
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 777
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 778
    label "continue"
  ]
  node [
    id 779
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 780
    label "Ohio"
  ]
  node [
    id 781
    label "wci&#281;cie"
  ]
  node [
    id 782
    label "Nowy_York"
  ]
  node [
    id 783
    label "warstwa"
  ]
  node [
    id 784
    label "samopoczucie"
  ]
  node [
    id 785
    label "Illinois"
  ]
  node [
    id 786
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 787
    label "state"
  ]
  node [
    id 788
    label "Jukatan"
  ]
  node [
    id 789
    label "Kalifornia"
  ]
  node [
    id 790
    label "Wirginia"
  ]
  node [
    id 791
    label "wektor"
  ]
  node [
    id 792
    label "Goa"
  ]
  node [
    id 793
    label "Teksas"
  ]
  node [
    id 794
    label "Waszyngton"
  ]
  node [
    id 795
    label "miejsce"
  ]
  node [
    id 796
    label "Massachusetts"
  ]
  node [
    id 797
    label "Alaska"
  ]
  node [
    id 798
    label "Arakan"
  ]
  node [
    id 799
    label "Hawaje"
  ]
  node [
    id 800
    label "Maryland"
  ]
  node [
    id 801
    label "punkt"
  ]
  node [
    id 802
    label "Michigan"
  ]
  node [
    id 803
    label "Arizona"
  ]
  node [
    id 804
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 805
    label "Georgia"
  ]
  node [
    id 806
    label "poziom"
  ]
  node [
    id 807
    label "Pensylwania"
  ]
  node [
    id 808
    label "shape"
  ]
  node [
    id 809
    label "Luizjana"
  ]
  node [
    id 810
    label "Nowy_Meksyk"
  ]
  node [
    id 811
    label "Alabama"
  ]
  node [
    id 812
    label "Kansas"
  ]
  node [
    id 813
    label "Oregon"
  ]
  node [
    id 814
    label "Oklahoma"
  ]
  node [
    id 815
    label "Floryda"
  ]
  node [
    id 816
    label "jednostka_administracyjna"
  ]
  node [
    id 817
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 818
    label "godzina"
  ]
  node [
    id 819
    label "time"
  ]
  node [
    id 820
    label "doba"
  ]
  node [
    id 821
    label "p&#243;&#322;godzina"
  ]
  node [
    id 822
    label "jednostka_czasu"
  ]
  node [
    id 823
    label "czas"
  ]
  node [
    id 824
    label "minuta"
  ]
  node [
    id 825
    label "kwadrans"
  ]
  node [
    id 826
    label "powierzy&#263;"
  ]
  node [
    id 827
    label "pieni&#261;dze"
  ]
  node [
    id 828
    label "plon"
  ]
  node [
    id 829
    label "give"
  ]
  node [
    id 830
    label "skojarzy&#263;"
  ]
  node [
    id 831
    label "d&#378;wi&#281;k"
  ]
  node [
    id 832
    label "zadenuncjowa&#263;"
  ]
  node [
    id 833
    label "impart"
  ]
  node [
    id 834
    label "da&#263;"
  ]
  node [
    id 835
    label "reszta"
  ]
  node [
    id 836
    label "zapach"
  ]
  node [
    id 837
    label "zrobi&#263;"
  ]
  node [
    id 838
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 839
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 840
    label "wiano"
  ]
  node [
    id 841
    label "produkcja"
  ]
  node [
    id 842
    label "translate"
  ]
  node [
    id 843
    label "picture"
  ]
  node [
    id 844
    label "wytworzy&#263;"
  ]
  node [
    id 845
    label "dress"
  ]
  node [
    id 846
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 847
    label "tajemnica"
  ]
  node [
    id 848
    label "panna_na_wydaniu"
  ]
  node [
    id 849
    label "supply"
  ]
  node [
    id 850
    label "ujawni&#263;"
  ]
  node [
    id 851
    label "doprowadzi&#263;"
  ]
  node [
    id 852
    label "insert"
  ]
  node [
    id 853
    label "wpisa&#263;"
  ]
  node [
    id 854
    label "wej&#347;&#263;"
  ]
  node [
    id 855
    label "zej&#347;&#263;"
  ]
  node [
    id 856
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 857
    label "umie&#347;ci&#263;"
  ]
  node [
    id 858
    label "zacz&#261;&#263;"
  ]
  node [
    id 859
    label "post&#261;pi&#263;"
  ]
  node [
    id 860
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 861
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 862
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 863
    label "zorganizowa&#263;"
  ]
  node [
    id 864
    label "appoint"
  ]
  node [
    id 865
    label "wystylizowa&#263;"
  ]
  node [
    id 866
    label "cause"
  ]
  node [
    id 867
    label "przerobi&#263;"
  ]
  node [
    id 868
    label "nabra&#263;"
  ]
  node [
    id 869
    label "make"
  ]
  node [
    id 870
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 871
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 872
    label "wydali&#263;"
  ]
  node [
    id 873
    label "manufacture"
  ]
  node [
    id 874
    label "tenis"
  ]
  node [
    id 875
    label "siatk&#243;wka"
  ]
  node [
    id 876
    label "introduce"
  ]
  node [
    id 877
    label "nafaszerowa&#263;"
  ]
  node [
    id 878
    label "zaserwowa&#263;"
  ]
  node [
    id 879
    label "discover"
  ]
  node [
    id 880
    label "objawi&#263;"
  ]
  node [
    id 881
    label "dostrzec"
  ]
  node [
    id 882
    label "denounce"
  ]
  node [
    id 883
    label "donie&#347;&#263;"
  ]
  node [
    id 884
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 885
    label "obieca&#263;"
  ]
  node [
    id 886
    label "pozwoli&#263;"
  ]
  node [
    id 887
    label "odst&#261;pi&#263;"
  ]
  node [
    id 888
    label "przywali&#263;"
  ]
  node [
    id 889
    label "wyrzec_si&#281;"
  ]
  node [
    id 890
    label "sztachn&#261;&#263;"
  ]
  node [
    id 891
    label "rap"
  ]
  node [
    id 892
    label "feed"
  ]
  node [
    id 893
    label "convey"
  ]
  node [
    id 894
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 895
    label "udost&#281;pni&#263;"
  ]
  node [
    id 896
    label "przeznaczy&#263;"
  ]
  node [
    id 897
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 898
    label "zada&#263;"
  ]
  node [
    id 899
    label "dostarczy&#263;"
  ]
  node [
    id 900
    label "przekaza&#263;"
  ]
  node [
    id 901
    label "doda&#263;"
  ]
  node [
    id 902
    label "zap&#322;aci&#263;"
  ]
  node [
    id 903
    label "consort"
  ]
  node [
    id 904
    label "powi&#261;za&#263;"
  ]
  node [
    id 905
    label "swat"
  ]
  node [
    id 906
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 907
    label "confide"
  ]
  node [
    id 908
    label "charge"
  ]
  node [
    id 909
    label "ufa&#263;"
  ]
  node [
    id 910
    label "odda&#263;"
  ]
  node [
    id 911
    label "entrust"
  ]
  node [
    id 912
    label "wyzna&#263;"
  ]
  node [
    id 913
    label "zleci&#263;"
  ]
  node [
    id 914
    label "consign"
  ]
  node [
    id 915
    label "phone"
  ]
  node [
    id 916
    label "wpadni&#281;cie"
  ]
  node [
    id 917
    label "wydawa&#263;"
  ]
  node [
    id 918
    label "intonacja"
  ]
  node [
    id 919
    label "wpa&#347;&#263;"
  ]
  node [
    id 920
    label "note"
  ]
  node [
    id 921
    label "onomatopeja"
  ]
  node [
    id 922
    label "modalizm"
  ]
  node [
    id 923
    label "nadlecenie"
  ]
  node [
    id 924
    label "sound"
  ]
  node [
    id 925
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 926
    label "wpada&#263;"
  ]
  node [
    id 927
    label "solmizacja"
  ]
  node [
    id 928
    label "seria"
  ]
  node [
    id 929
    label "dobiec"
  ]
  node [
    id 930
    label "transmiter"
  ]
  node [
    id 931
    label "heksachord"
  ]
  node [
    id 932
    label "akcent"
  ]
  node [
    id 933
    label "wydanie"
  ]
  node [
    id 934
    label "repetycja"
  ]
  node [
    id 935
    label "brzmienie"
  ]
  node [
    id 936
    label "wpadanie"
  ]
  node [
    id 937
    label "liczba_kwantowa"
  ]
  node [
    id 938
    label "kosmetyk"
  ]
  node [
    id 939
    label "ciasto"
  ]
  node [
    id 940
    label "aromat"
  ]
  node [
    id 941
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 942
    label "puff"
  ]
  node [
    id 943
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 944
    label "przyprawa"
  ]
  node [
    id 945
    label "upojno&#347;&#263;"
  ]
  node [
    id 946
    label "owiewanie"
  ]
  node [
    id 947
    label "smak"
  ]
  node [
    id 948
    label "tingel-tangel"
  ]
  node [
    id 949
    label "numer"
  ]
  node [
    id 950
    label "product"
  ]
  node [
    id 951
    label "uzysk"
  ]
  node [
    id 952
    label "rozw&#243;j"
  ]
  node [
    id 953
    label "odtworzenie"
  ]
  node [
    id 954
    label "trema"
  ]
  node [
    id 955
    label "kooperowa&#263;"
  ]
  node [
    id 956
    label "debit"
  ]
  node [
    id 957
    label "redaktor"
  ]
  node [
    id 958
    label "druk"
  ]
  node [
    id 959
    label "publikacja"
  ]
  node [
    id 960
    label "redakcja"
  ]
  node [
    id 961
    label "szata_graficzna"
  ]
  node [
    id 962
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 963
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 964
    label "poster"
  ]
  node [
    id 965
    label "return"
  ]
  node [
    id 966
    label "metr"
  ]
  node [
    id 967
    label "rezultat"
  ]
  node [
    id 968
    label "naturalia"
  ]
  node [
    id 969
    label "wypaplanie"
  ]
  node [
    id 970
    label "enigmat"
  ]
  node [
    id 971
    label "wiedza"
  ]
  node [
    id 972
    label "zachowywanie"
  ]
  node [
    id 973
    label "secret"
  ]
  node [
    id 974
    label "obowi&#261;zek"
  ]
  node [
    id 975
    label "dyskrecja"
  ]
  node [
    id 976
    label "informacja"
  ]
  node [
    id 977
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 978
    label "taj&#324;"
  ]
  node [
    id 979
    label "zachowa&#263;"
  ]
  node [
    id 980
    label "zachowywa&#263;"
  ]
  node [
    id 981
    label "portfel"
  ]
  node [
    id 982
    label "kwota"
  ]
  node [
    id 983
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 984
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 985
    label "forsa"
  ]
  node [
    id 986
    label "kapa&#263;"
  ]
  node [
    id 987
    label "kapn&#261;&#263;"
  ]
  node [
    id 988
    label "kapanie"
  ]
  node [
    id 989
    label "kapita&#322;"
  ]
  node [
    id 990
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 991
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 992
    label "kapni&#281;cie"
  ]
  node [
    id 993
    label "hajs"
  ]
  node [
    id 994
    label "dydki"
  ]
  node [
    id 995
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 996
    label "remainder"
  ]
  node [
    id 997
    label "pozosta&#322;y"
  ]
  node [
    id 998
    label "posa&#380;ek"
  ]
  node [
    id 999
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 1000
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 1001
    label "zaufanie"
  ]
  node [
    id 1002
    label "nazwa_w&#322;asna"
  ]
  node [
    id 1003
    label "paczkarnia"
  ]
  node [
    id 1004
    label "klasa"
  ]
  node [
    id 1005
    label "biurowiec"
  ]
  node [
    id 1006
    label "technika"
  ]
  node [
    id 1007
    label "impression"
  ]
  node [
    id 1008
    label "pismo"
  ]
  node [
    id 1009
    label "glif"
  ]
  node [
    id 1010
    label "dese&#324;"
  ]
  node [
    id 1011
    label "prohibita"
  ]
  node [
    id 1012
    label "cymelium"
  ]
  node [
    id 1013
    label "zaproszenie"
  ]
  node [
    id 1014
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1015
    label "formatowa&#263;"
  ]
  node [
    id 1016
    label "formatowanie"
  ]
  node [
    id 1017
    label "zdobnik"
  ]
  node [
    id 1018
    label "character"
  ]
  node [
    id 1019
    label "printing"
  ]
  node [
    id 1020
    label "notification"
  ]
  node [
    id 1021
    label "radio"
  ]
  node [
    id 1022
    label "zesp&#243;&#322;"
  ]
  node [
    id 1023
    label "composition"
  ]
  node [
    id 1024
    label "redaction"
  ]
  node [
    id 1025
    label "telewizja"
  ]
  node [
    id 1026
    label "obr&#243;bka"
  ]
  node [
    id 1027
    label "surrender"
  ]
  node [
    id 1028
    label "kojarzy&#263;"
  ]
  node [
    id 1029
    label "dawa&#263;"
  ]
  node [
    id 1030
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1031
    label "ujawnia&#263;"
  ]
  node [
    id 1032
    label "placard"
  ]
  node [
    id 1033
    label "powierza&#263;"
  ]
  node [
    id 1034
    label "denuncjowa&#263;"
  ]
  node [
    id 1035
    label "wytwarza&#263;"
  ]
  node [
    id 1036
    label "train"
  ]
  node [
    id 1037
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1038
    label "bran&#380;owiec"
  ]
  node [
    id 1039
    label "edytor"
  ]
  node [
    id 1040
    label "afisz"
  ]
  node [
    id 1041
    label "naukowy"
  ]
  node [
    id 1042
    label "teoretyczny"
  ]
  node [
    id 1043
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1044
    label "studencki"
  ]
  node [
    id 1045
    label "prawid&#322;owy"
  ]
  node [
    id 1046
    label "odpowiedni"
  ]
  node [
    id 1047
    label "szkolny"
  ]
  node [
    id 1048
    label "tradycyjny"
  ]
  node [
    id 1049
    label "intelektualny"
  ]
  node [
    id 1050
    label "akademicko"
  ]
  node [
    id 1051
    label "akademiczny"
  ]
  node [
    id 1052
    label "po_akademicku"
  ]
  node [
    id 1053
    label "zdarzony"
  ]
  node [
    id 1054
    label "odpowiednio"
  ]
  node [
    id 1055
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1056
    label "nale&#380;ny"
  ]
  node [
    id 1057
    label "nale&#380;yty"
  ]
  node [
    id 1058
    label "stosownie"
  ]
  node [
    id 1059
    label "odpowiadanie"
  ]
  node [
    id 1060
    label "specjalny"
  ]
  node [
    id 1061
    label "s&#322;uszny"
  ]
  node [
    id 1062
    label "symetryczny"
  ]
  node [
    id 1063
    label "prawid&#322;owo"
  ]
  node [
    id 1064
    label "po&#380;&#261;dany"
  ]
  node [
    id 1065
    label "dobry"
  ]
  node [
    id 1066
    label "naukowo"
  ]
  node [
    id 1067
    label "edukacyjnie"
  ]
  node [
    id 1068
    label "scjentyficzny"
  ]
  node [
    id 1069
    label "skomplikowany"
  ]
  node [
    id 1070
    label "specjalistyczny"
  ]
  node [
    id 1071
    label "zgodny"
  ]
  node [
    id 1072
    label "szkolnie"
  ]
  node [
    id 1073
    label "podstawowy"
  ]
  node [
    id 1074
    label "prosty"
  ]
  node [
    id 1075
    label "szkoleniowy"
  ]
  node [
    id 1076
    label "modelowy"
  ]
  node [
    id 1077
    label "tradycyjnie"
  ]
  node [
    id 1078
    label "surowy"
  ]
  node [
    id 1079
    label "zwyk&#322;y"
  ]
  node [
    id 1080
    label "zachowawczy"
  ]
  node [
    id 1081
    label "nienowoczesny"
  ]
  node [
    id 1082
    label "przyj&#281;ty"
  ]
  node [
    id 1083
    label "wierny"
  ]
  node [
    id 1084
    label "zwyczajowy"
  ]
  node [
    id 1085
    label "intelektualnie"
  ]
  node [
    id 1086
    label "my&#347;l&#261;cy"
  ]
  node [
    id 1087
    label "wznios&#322;y"
  ]
  node [
    id 1088
    label "g&#322;&#281;boki"
  ]
  node [
    id 1089
    label "umys&#322;owy"
  ]
  node [
    id 1090
    label "inteligentny"
  ]
  node [
    id 1091
    label "nierealny"
  ]
  node [
    id 1092
    label "teoretycznie"
  ]
  node [
    id 1093
    label "typowy"
  ]
  node [
    id 1094
    label "uprawniony"
  ]
  node [
    id 1095
    label "zasadniczy"
  ]
  node [
    id 1096
    label "taki"
  ]
  node [
    id 1097
    label "charakterystyczny"
  ]
  node [
    id 1098
    label "prawdziwy"
  ]
  node [
    id 1099
    label "ten"
  ]
  node [
    id 1100
    label "po_studencku"
  ]
  node [
    id 1101
    label "m&#322;ody"
  ]
  node [
    id 1102
    label "charakterystycznie"
  ]
  node [
    id 1103
    label "trained"
  ]
  node [
    id 1104
    label "porz&#261;dny"
  ]
  node [
    id 1105
    label "fachowy"
  ]
  node [
    id 1106
    label "zawodowo"
  ]
  node [
    id 1107
    label "profesjonalnie"
  ]
  node [
    id 1108
    label "rzetelny"
  ]
  node [
    id 1109
    label "kompetentny"
  ]
  node [
    id 1110
    label "zawodowy"
  ]
  node [
    id 1111
    label "umiej&#281;tny"
  ]
  node [
    id 1112
    label "fachowo"
  ]
  node [
    id 1113
    label "kompetentnie"
  ]
  node [
    id 1114
    label "w&#322;ady"
  ]
  node [
    id 1115
    label "kompetencyjny"
  ]
  node [
    id 1116
    label "rzetelnie"
  ]
  node [
    id 1117
    label "przekonuj&#261;cy"
  ]
  node [
    id 1118
    label "intensywny"
  ]
  node [
    id 1119
    label "przyzwoity"
  ]
  node [
    id 1120
    label "ch&#281;dogi"
  ]
  node [
    id 1121
    label "schludny"
  ]
  node [
    id 1122
    label "porz&#261;dnie"
  ]
  node [
    id 1123
    label "intencjonalny"
  ]
  node [
    id 1124
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1125
    label "niedorozw&#243;j"
  ]
  node [
    id 1126
    label "szczeg&#243;lny"
  ]
  node [
    id 1127
    label "specjalnie"
  ]
  node [
    id 1128
    label "nieetatowy"
  ]
  node [
    id 1129
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1130
    label "nienormalny"
  ]
  node [
    id 1131
    label "umy&#347;lnie"
  ]
  node [
    id 1132
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1133
    label "zi&#281;bienie"
  ]
  node [
    id 1134
    label "niesympatyczny"
  ]
  node [
    id 1135
    label "och&#322;odzenie"
  ]
  node [
    id 1136
    label "opanowany"
  ]
  node [
    id 1137
    label "rozs&#261;dny"
  ]
  node [
    id 1138
    label "ch&#322;odno"
  ]
  node [
    id 1139
    label "czadowo"
  ]
  node [
    id 1140
    label "klawo"
  ]
  node [
    id 1141
    label "formalnie"
  ]
  node [
    id 1142
    label "fajnie"
  ]
  node [
    id 1143
    label "professionally"
  ]
  node [
    id 1144
    label "warto&#347;&#263;"
  ]
  node [
    id 1145
    label "quality"
  ]
  node [
    id 1146
    label "co&#347;"
  ]
  node [
    id 1147
    label "syf"
  ]
  node [
    id 1148
    label "boski"
  ]
  node [
    id 1149
    label "krajobraz"
  ]
  node [
    id 1150
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1151
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1152
    label "przywidzenie"
  ]
  node [
    id 1153
    label "presence"
  ]
  node [
    id 1154
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1155
    label "potrzymanie"
  ]
  node [
    id 1156
    label "pod&#243;j"
  ]
  node [
    id 1157
    label "filiacja"
  ]
  node [
    id 1158
    label "licencjonowanie"
  ]
  node [
    id 1159
    label "opasa&#263;"
  ]
  node [
    id 1160
    label "ch&#243;w"
  ]
  node [
    id 1161
    label "licencja"
  ]
  node [
    id 1162
    label "sokolarnia"
  ]
  node [
    id 1163
    label "potrzyma&#263;"
  ]
  node [
    id 1164
    label "rozp&#322;&#243;d"
  ]
  node [
    id 1165
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1166
    label "wypas"
  ]
  node [
    id 1167
    label "wychowalnia"
  ]
  node [
    id 1168
    label "pstr&#261;garnia"
  ]
  node [
    id 1169
    label "krzy&#380;owanie"
  ]
  node [
    id 1170
    label "licencjonowa&#263;"
  ]
  node [
    id 1171
    label "odch&#243;w"
  ]
  node [
    id 1172
    label "tucz"
  ]
  node [
    id 1173
    label "ud&#243;j"
  ]
  node [
    id 1174
    label "klatka"
  ]
  node [
    id 1175
    label "opasienie"
  ]
  node [
    id 1176
    label "wych&#243;w"
  ]
  node [
    id 1177
    label "obrz&#261;dek"
  ]
  node [
    id 1178
    label "opasanie"
  ]
  node [
    id 1179
    label "polish"
  ]
  node [
    id 1180
    label "akwarium"
  ]
  node [
    id 1181
    label "biotechnika"
  ]
  node [
    id 1182
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 1183
    label "uk&#322;ad"
  ]
  node [
    id 1184
    label "styl"
  ]
  node [
    id 1185
    label "line"
  ]
  node [
    id 1186
    label "kanon"
  ]
  node [
    id 1187
    label "zjazd"
  ]
  node [
    id 1188
    label "charakterystyka"
  ]
  node [
    id 1189
    label "m&#322;ot"
  ]
  node [
    id 1190
    label "znak"
  ]
  node [
    id 1191
    label "drzewo"
  ]
  node [
    id 1192
    label "pr&#243;ba"
  ]
  node [
    id 1193
    label "attribute"
  ]
  node [
    id 1194
    label "marka"
  ]
  node [
    id 1195
    label "biom"
  ]
  node [
    id 1196
    label "szata_ro&#347;linna"
  ]
  node [
    id 1197
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1198
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1199
    label "przyroda"
  ]
  node [
    id 1200
    label "zielono&#347;&#263;"
  ]
  node [
    id 1201
    label "pi&#281;tro"
  ]
  node [
    id 1202
    label "plant"
  ]
  node [
    id 1203
    label "ro&#347;lina"
  ]
  node [
    id 1204
    label "geosystem"
  ]
  node [
    id 1205
    label "kult"
  ]
  node [
    id 1206
    label "wyznanie"
  ]
  node [
    id 1207
    label "mitologia"
  ]
  node [
    id 1208
    label "ideologia"
  ]
  node [
    id 1209
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 1210
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 1211
    label "nawracanie_si&#281;"
  ]
  node [
    id 1212
    label "duchowny"
  ]
  node [
    id 1213
    label "rela"
  ]
  node [
    id 1214
    label "kosmologia"
  ]
  node [
    id 1215
    label "kosmogonia"
  ]
  node [
    id 1216
    label "nawraca&#263;"
  ]
  node [
    id 1217
    label "mistyka"
  ]
  node [
    id 1218
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 1219
    label "ceremony"
  ]
  node [
    id 1220
    label "staro&#347;cina_weselna"
  ]
  node [
    id 1221
    label "folklor"
  ]
  node [
    id 1222
    label "objawienie"
  ]
  node [
    id 1223
    label "zaj&#281;cie"
  ]
  node [
    id 1224
    label "tajniki"
  ]
  node [
    id 1225
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 1226
    label "zaplecze"
  ]
  node [
    id 1227
    label "pomieszczenie"
  ]
  node [
    id 1228
    label "zlewozmywak"
  ]
  node [
    id 1229
    label "gotowa&#263;"
  ]
  node [
    id 1230
    label "ciemna_materia"
  ]
  node [
    id 1231
    label "planeta"
  ]
  node [
    id 1232
    label "ekosfera"
  ]
  node [
    id 1233
    label "czarna_dziura"
  ]
  node [
    id 1234
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 1235
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 1236
    label "kosmos"
  ]
  node [
    id 1237
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1238
    label "poprawno&#347;&#263;"
  ]
  node [
    id 1239
    label "og&#322;ada"
  ]
  node [
    id 1240
    label "service"
  ]
  node [
    id 1241
    label "stosowno&#347;&#263;"
  ]
  node [
    id 1242
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 1243
    label "Ukraina"
  ]
  node [
    id 1244
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1245
    label "blok_wschodni"
  ]
  node [
    id 1246
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1247
    label "wsch&#243;d"
  ]
  node [
    id 1248
    label "Europa_Wschodnia"
  ]
  node [
    id 1249
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 1250
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1251
    label "wra&#380;enie"
  ]
  node [
    id 1252
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 1253
    label "interception"
  ]
  node [
    id 1254
    label "wzbudzenie"
  ]
  node [
    id 1255
    label "emotion"
  ]
  node [
    id 1256
    label "movement"
  ]
  node [
    id 1257
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1258
    label "wzi&#281;cie"
  ]
  node [
    id 1259
    label "bang"
  ]
  node [
    id 1260
    label "wzi&#261;&#263;"
  ]
  node [
    id 1261
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1262
    label "stimulate"
  ]
  node [
    id 1263
    label "ogarn&#261;&#263;"
  ]
  node [
    id 1264
    label "wzbudzi&#263;"
  ]
  node [
    id 1265
    label "thrill"
  ]
  node [
    id 1266
    label "treat"
  ]
  node [
    id 1267
    label "czerpa&#263;"
  ]
  node [
    id 1268
    label "bra&#263;"
  ]
  node [
    id 1269
    label "go"
  ]
  node [
    id 1270
    label "handle"
  ]
  node [
    id 1271
    label "wzbudza&#263;"
  ]
  node [
    id 1272
    label "ogarnia&#263;"
  ]
  node [
    id 1273
    label "czerpanie"
  ]
  node [
    id 1274
    label "acquisition"
  ]
  node [
    id 1275
    label "branie"
  ]
  node [
    id 1276
    label "caparison"
  ]
  node [
    id 1277
    label "wzbudzanie"
  ]
  node [
    id 1278
    label "czynno&#347;&#263;"
  ]
  node [
    id 1279
    label "ogarnianie"
  ]
  node [
    id 1280
    label "uprawa"
  ]
  node [
    id 1281
    label "summer"
  ]
  node [
    id 1282
    label "poprzedzanie"
  ]
  node [
    id 1283
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1284
    label "laba"
  ]
  node [
    id 1285
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1286
    label "chronometria"
  ]
  node [
    id 1287
    label "rachuba_czasu"
  ]
  node [
    id 1288
    label "przep&#322;ywanie"
  ]
  node [
    id 1289
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1290
    label "czasokres"
  ]
  node [
    id 1291
    label "odczyt"
  ]
  node [
    id 1292
    label "chwila"
  ]
  node [
    id 1293
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1294
    label "dzieje"
  ]
  node [
    id 1295
    label "kategoria_gramatyczna"
  ]
  node [
    id 1296
    label "poprzedzenie"
  ]
  node [
    id 1297
    label "trawienie"
  ]
  node [
    id 1298
    label "pochodzi&#263;"
  ]
  node [
    id 1299
    label "period"
  ]
  node [
    id 1300
    label "okres_czasu"
  ]
  node [
    id 1301
    label "poprzedza&#263;"
  ]
  node [
    id 1302
    label "schy&#322;ek"
  ]
  node [
    id 1303
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1304
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1305
    label "zegar"
  ]
  node [
    id 1306
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1307
    label "czwarty_wymiar"
  ]
  node [
    id 1308
    label "pochodzenie"
  ]
  node [
    id 1309
    label "koniugacja"
  ]
  node [
    id 1310
    label "Zeitgeist"
  ]
  node [
    id 1311
    label "trawi&#263;"
  ]
  node [
    id 1312
    label "pogoda"
  ]
  node [
    id 1313
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1314
    label "poprzedzi&#263;"
  ]
  node [
    id 1315
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1316
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1317
    label "time_period"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 818
  ]
  edge [
    source 14
    target 819
  ]
  edge [
    source 14
    target 820
  ]
  edge [
    source 14
    target 821
  ]
  edge [
    source 14
    target 822
  ]
  edge [
    source 14
    target 823
  ]
  edge [
    source 14
    target 824
  ]
  edge [
    source 14
    target 825
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 15
    target 473
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 15
    target 848
  ]
  edge [
    source 15
    target 849
  ]
  edge [
    source 15
    target 850
  ]
  edge [
    source 15
    target 417
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 26
  ]
  edge [
    source 15
    target 852
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 853
  ]
  edge [
    source 15
    target 294
  ]
  edge [
    source 15
    target 854
  ]
  edge [
    source 15
    target 32
  ]
  edge [
    source 15
    target 855
  ]
  edge [
    source 15
    target 856
  ]
  edge [
    source 15
    target 857
  ]
  edge [
    source 15
    target 858
  ]
  edge [
    source 15
    target 36
  ]
  edge [
    source 15
    target 859
  ]
  edge [
    source 15
    target 860
  ]
  edge [
    source 15
    target 861
  ]
  edge [
    source 15
    target 862
  ]
  edge [
    source 15
    target 863
  ]
  edge [
    source 15
    target 864
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 368
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 378
  ]
  edge [
    source 15
    target 301
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 298
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 470
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 908
  ]
  edge [
    source 15
    target 909
  ]
  edge [
    source 15
    target 910
  ]
  edge [
    source 15
    target 911
  ]
  edge [
    source 15
    target 912
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 946
  ]
  edge [
    source 15
    target 947
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 68
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 332
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 478
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 281
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 997
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 419
  ]
  edge [
    source 17
    target 420
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 517
  ]
  edge [
    source 17
    target 421
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 423
  ]
  edge [
    source 17
    target 422
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 425
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 426
  ]
  edge [
    source 17
    target 427
  ]
  edge [
    source 17
    target 428
  ]
  edge [
    source 17
    target 429
  ]
  edge [
    source 17
    target 430
  ]
  edge [
    source 17
    target 431
  ]
  edge [
    source 17
    target 432
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 433
  ]
  edge [
    source 17
    target 434
  ]
  edge [
    source 17
    target 435
  ]
  edge [
    source 17
    target 437
  ]
  edge [
    source 17
    target 438
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 439
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 29
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 1080
  ]
  edge [
    source 18
    target 1081
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1084
  ]
  edge [
    source 18
    target 1085
  ]
  edge [
    source 18
    target 1086
  ]
  edge [
    source 18
    target 1087
  ]
  edge [
    source 18
    target 1088
  ]
  edge [
    source 18
    target 1089
  ]
  edge [
    source 18
    target 1090
  ]
  edge [
    source 18
    target 1091
  ]
  edge [
    source 18
    target 1092
  ]
  edge [
    source 18
    target 1093
  ]
  edge [
    source 18
    target 1094
  ]
  edge [
    source 18
    target 1095
  ]
  edge [
    source 18
    target 1096
  ]
  edge [
    source 18
    target 1097
  ]
  edge [
    source 18
    target 1098
  ]
  edge [
    source 18
    target 1099
  ]
  edge [
    source 18
    target 1100
  ]
  edge [
    source 18
    target 1101
  ]
  edge [
    source 18
    target 1102
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 696
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 19
    target 1130
  ]
  edge [
    source 19
    target 1131
  ]
  edge [
    source 19
    target 1132
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1133
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 693
  ]
  edge [
    source 19
    target 541
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 20
    target 386
  ]
  edge [
    source 20
    target 64
  ]
  edge [
    source 20
    target 387
  ]
  edge [
    source 20
    target 65
  ]
  edge [
    source 20
    target 388
  ]
  edge [
    source 20
    target 389
  ]
  edge [
    source 20
    target 390
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 391
  ]
  edge [
    source 20
    target 392
  ]
  edge [
    source 20
    target 393
  ]
  edge [
    source 20
    target 394
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 395
  ]
  edge [
    source 20
    target 396
  ]
  edge [
    source 20
    target 397
  ]
  edge [
    source 20
    target 332
  ]
  edge [
    source 20
    target 398
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 399
  ]
  edge [
    source 20
    target 400
  ]
  edge [
    source 20
    target 401
  ]
  edge [
    source 20
    target 402
  ]
  edge [
    source 20
    target 403
  ]
  edge [
    source 20
    target 404
  ]
  edge [
    source 20
    target 405
  ]
  edge [
    source 20
    target 406
  ]
  edge [
    source 20
    target 407
  ]
  edge [
    source 20
    target 408
  ]
  edge [
    source 20
    target 409
  ]
  edge [
    source 20
    target 410
  ]
  edge [
    source 20
    target 1144
  ]
  edge [
    source 20
    target 1145
  ]
  edge [
    source 20
    target 1146
  ]
  edge [
    source 20
    target 787
  ]
  edge [
    source 20
    target 1147
  ]
  edge [
    source 20
    target 440
  ]
  edge [
    source 20
    target 441
  ]
  edge [
    source 20
    target 442
  ]
  edge [
    source 20
    target 443
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 1148
  ]
  edge [
    source 20
    target 1149
  ]
  edge [
    source 20
    target 1150
  ]
  edge [
    source 20
    target 1151
  ]
  edge [
    source 20
    target 1152
  ]
  edge [
    source 20
    target 1153
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 1154
  ]
  edge [
    source 20
    target 1155
  ]
  edge [
    source 20
    target 520
  ]
  edge [
    source 20
    target 1156
  ]
  edge [
    source 20
    target 1157
  ]
  edge [
    source 20
    target 1158
  ]
  edge [
    source 20
    target 1159
  ]
  edge [
    source 20
    target 1160
  ]
  edge [
    source 20
    target 1161
  ]
  edge [
    source 20
    target 1162
  ]
  edge [
    source 20
    target 1163
  ]
  edge [
    source 20
    target 1164
  ]
  edge [
    source 20
    target 1165
  ]
  edge [
    source 20
    target 1166
  ]
  edge [
    source 20
    target 1167
  ]
  edge [
    source 20
    target 1168
  ]
  edge [
    source 20
    target 1169
  ]
  edge [
    source 20
    target 1170
  ]
  edge [
    source 20
    target 1171
  ]
  edge [
    source 20
    target 1172
  ]
  edge [
    source 20
    target 1173
  ]
  edge [
    source 20
    target 1174
  ]
  edge [
    source 20
    target 1175
  ]
  edge [
    source 20
    target 1176
  ]
  edge [
    source 20
    target 1177
  ]
  edge [
    source 20
    target 1178
  ]
  edge [
    source 20
    target 1179
  ]
  edge [
    source 20
    target 1180
  ]
  edge [
    source 20
    target 1181
  ]
  edge [
    source 20
    target 1182
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 1183
  ]
  edge [
    source 20
    target 1184
  ]
  edge [
    source 20
    target 1185
  ]
  edge [
    source 20
    target 1186
  ]
  edge [
    source 20
    target 1187
  ]
  edge [
    source 20
    target 1188
  ]
  edge [
    source 20
    target 1189
  ]
  edge [
    source 20
    target 1190
  ]
  edge [
    source 20
    target 1191
  ]
  edge [
    source 20
    target 1192
  ]
  edge [
    source 20
    target 1193
  ]
  edge [
    source 20
    target 1194
  ]
  edge [
    source 20
    target 1195
  ]
  edge [
    source 20
    target 1196
  ]
  edge [
    source 20
    target 1197
  ]
  edge [
    source 20
    target 1198
  ]
  edge [
    source 20
    target 1199
  ]
  edge [
    source 20
    target 1200
  ]
  edge [
    source 20
    target 1201
  ]
  edge [
    source 20
    target 1202
  ]
  edge [
    source 20
    target 1203
  ]
  edge [
    source 20
    target 1204
  ]
  edge [
    source 20
    target 1205
  ]
  edge [
    source 20
    target 1206
  ]
  edge [
    source 20
    target 1207
  ]
  edge [
    source 20
    target 1208
  ]
  edge [
    source 20
    target 1209
  ]
  edge [
    source 20
    target 1210
  ]
  edge [
    source 20
    target 1211
  ]
  edge [
    source 20
    target 1212
  ]
  edge [
    source 20
    target 1213
  ]
  edge [
    source 20
    target 80
  ]
  edge [
    source 20
    target 1214
  ]
  edge [
    source 20
    target 1215
  ]
  edge [
    source 20
    target 1216
  ]
  edge [
    source 20
    target 1217
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 63
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 67
  ]
  edge [
    source 20
    target 68
  ]
  edge [
    source 20
    target 69
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 20
    target 71
  ]
  edge [
    source 20
    target 72
  ]
  edge [
    source 20
    target 73
  ]
  edge [
    source 20
    target 74
  ]
  edge [
    source 20
    target 75
  ]
  edge [
    source 20
    target 76
  ]
  edge [
    source 20
    target 77
  ]
  edge [
    source 20
    target 78
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 20
    target 81
  ]
  edge [
    source 20
    target 82
  ]
  edge [
    source 20
    target 83
  ]
  edge [
    source 20
    target 84
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 20
    target 86
  ]
  edge [
    source 20
    target 87
  ]
  edge [
    source 20
    target 88
  ]
  edge [
    source 20
    target 89
  ]
  edge [
    source 20
    target 90
  ]
  edge [
    source 20
    target 91
  ]
  edge [
    source 20
    target 92
  ]
  edge [
    source 20
    target 93
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 1218
  ]
  edge [
    source 20
    target 554
  ]
  edge [
    source 20
    target 1219
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 1220
  ]
  edge [
    source 20
    target 1221
  ]
  edge [
    source 20
    target 1222
  ]
  edge [
    source 20
    target 1223
  ]
  edge [
    source 20
    target 343
  ]
  edge [
    source 20
    target 1224
  ]
  edge [
    source 20
    target 723
  ]
  edge [
    source 20
    target 1225
  ]
  edge [
    source 20
    target 301
  ]
  edge [
    source 20
    target 1226
  ]
  edge [
    source 20
    target 1227
  ]
  edge [
    source 20
    target 1228
  ]
  edge [
    source 20
    target 1229
  ]
  edge [
    source 20
    target 1230
  ]
  edge [
    source 20
    target 1231
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 1232
  ]
  edge [
    source 20
    target 616
  ]
  edge [
    source 20
    target 1233
  ]
  edge [
    source 20
    target 52
  ]
  edge [
    source 20
    target 1234
  ]
  edge [
    source 20
    target 1235
  ]
  edge [
    source 20
    target 1236
  ]
  edge [
    source 20
    target 1237
  ]
  edge [
    source 20
    target 1238
  ]
  edge [
    source 20
    target 1239
  ]
  edge [
    source 20
    target 1240
  ]
  edge [
    source 20
    target 1241
  ]
  edge [
    source 20
    target 1242
  ]
  edge [
    source 20
    target 1243
  ]
  edge [
    source 20
    target 1244
  ]
  edge [
    source 20
    target 1245
  ]
  edge [
    source 20
    target 1246
  ]
  edge [
    source 20
    target 1247
  ]
  edge [
    source 20
    target 1248
  ]
  edge [
    source 20
    target 1249
  ]
  edge [
    source 20
    target 1250
  ]
  edge [
    source 20
    target 1251
  ]
  edge [
    source 20
    target 1252
  ]
  edge [
    source 20
    target 1253
  ]
  edge [
    source 20
    target 1254
  ]
  edge [
    source 20
    target 1255
  ]
  edge [
    source 20
    target 1256
  ]
  edge [
    source 20
    target 1257
  ]
  edge [
    source 20
    target 1258
  ]
  edge [
    source 20
    target 1259
  ]
  edge [
    source 20
    target 1260
  ]
  edge [
    source 20
    target 1261
  ]
  edge [
    source 20
    target 1262
  ]
  edge [
    source 20
    target 1263
  ]
  edge [
    source 20
    target 1264
  ]
  edge [
    source 20
    target 1265
  ]
  edge [
    source 20
    target 1266
  ]
  edge [
    source 20
    target 1267
  ]
  edge [
    source 20
    target 1268
  ]
  edge [
    source 20
    target 1269
  ]
  edge [
    source 20
    target 1270
  ]
  edge [
    source 20
    target 1271
  ]
  edge [
    source 20
    target 1272
  ]
  edge [
    source 20
    target 1273
  ]
  edge [
    source 20
    target 1274
  ]
  edge [
    source 20
    target 1275
  ]
  edge [
    source 20
    target 1276
  ]
  edge [
    source 20
    target 1277
  ]
  edge [
    source 20
    target 1278
  ]
  edge [
    source 20
    target 1279
  ]
  edge [
    source 20
    target 418
  ]
  edge [
    source 20
    target 446
  ]
  edge [
    source 20
    target 916
  ]
  edge [
    source 20
    target 621
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 53
  ]
  edge [
    source 20
    target 919
  ]
  edge [
    source 20
    target 936
  ]
  edge [
    source 20
    target 926
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 795
  ]
  edge [
    source 20
    target 1280
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1281
  ]
  edge [
    source 21
    target 823
  ]
  edge [
    source 21
    target 1282
  ]
  edge [
    source 21
    target 1283
  ]
  edge [
    source 21
    target 1284
  ]
  edge [
    source 21
    target 1285
  ]
  edge [
    source 21
    target 1286
  ]
  edge [
    source 21
    target 750
  ]
  edge [
    source 21
    target 1287
  ]
  edge [
    source 21
    target 1288
  ]
  edge [
    source 21
    target 1289
  ]
  edge [
    source 21
    target 1290
  ]
  edge [
    source 21
    target 1291
  ]
  edge [
    source 21
    target 1292
  ]
  edge [
    source 21
    target 1293
  ]
  edge [
    source 21
    target 1294
  ]
  edge [
    source 21
    target 1295
  ]
  edge [
    source 21
    target 1296
  ]
  edge [
    source 21
    target 1297
  ]
  edge [
    source 21
    target 1298
  ]
  edge [
    source 21
    target 1299
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 1301
  ]
  edge [
    source 21
    target 1302
  ]
  edge [
    source 21
    target 1303
  ]
  edge [
    source 21
    target 1304
  ]
  edge [
    source 21
    target 1305
  ]
  edge [
    source 21
    target 1306
  ]
  edge [
    source 21
    target 1307
  ]
  edge [
    source 21
    target 1308
  ]
  edge [
    source 21
    target 1309
  ]
  edge [
    source 21
    target 1310
  ]
  edge [
    source 21
    target 1311
  ]
  edge [
    source 21
    target 1312
  ]
  edge [
    source 21
    target 1313
  ]
  edge [
    source 21
    target 1314
  ]
  edge [
    source 21
    target 1315
  ]
  edge [
    source 21
    target 1316
  ]
  edge [
    source 21
    target 1317
  ]
]
