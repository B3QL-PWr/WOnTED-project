graph [
  node [
    id 0
    label "bobrowo"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "drawski"
    origin "text"
  ]
  node [
    id 3
    label "wojew&#243;dztwo"
  ]
  node [
    id 4
    label "gmina"
  ]
  node [
    id 5
    label "jednostka_administracyjna"
  ]
  node [
    id 6
    label "urz&#261;d"
  ]
  node [
    id 7
    label "Karlsbad"
  ]
  node [
    id 8
    label "Dobro&#324;"
  ]
  node [
    id 9
    label "rada_gminy"
  ]
  node [
    id 10
    label "Wielka_Wie&#347;"
  ]
  node [
    id 11
    label "radny"
  ]
  node [
    id 12
    label "organizacja_religijna"
  ]
  node [
    id 13
    label "Biskupice"
  ]
  node [
    id 14
    label "mikroregion"
  ]
  node [
    id 15
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 16
    label "pa&#324;stwo"
  ]
  node [
    id 17
    label "makroregion"
  ]
  node [
    id 18
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 19
    label "Stargard"
  ]
  node [
    id 20
    label "szczeci&#324;ski"
  ]
  node [
    id 21
    label "linia"
  ]
  node [
    id 22
    label "kolejowy"
  ]
  node [
    id 23
    label "nr"
  ]
  node [
    id 24
    label "210"
  ]
  node [
    id 25
    label "drogi"
  ]
  node [
    id 26
    label "krajowy"
  ]
  node [
    id 27
    label "20"
  ]
  node [
    id 28
    label "Konstantyn"
  ]
  node [
    id 29
    label "von"
  ]
  node [
    id 30
    label "knebel"
  ]
  node [
    id 31
    label "Doeberitz"
  ]
  node [
    id 32
    label "ludwik"
  ]
  node [
    id 33
    label "Henriet&#261;"
  ]
  node [
    id 34
    label "Grunburg"
  ]
  node [
    id 35
    label "Artur"
  ]
  node [
    id 36
    label "hassa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 33
    target 34
  ]
]
