graph [
  node [
    id 0
    label "jerzy"
    origin "text"
  ]
  node [
    id 1
    label "sulima"
    origin "text"
  ]
  node [
    id 2
    label "kami&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "Jerzy"
  ]
  node [
    id 4
    label "Sulima"
  ]
  node [
    id 5
    label "Kami&#324;ski"
  ]
  node [
    id 6
    label "Boles&#322;awa"
  ]
  node [
    id 7
    label "ii"
  ]
  node [
    id 8
    label "wojna"
  ]
  node [
    id 9
    label "&#347;wiatowy"
  ]
  node [
    id 10
    label "liceum"
  ]
  node [
    id 11
    label "mechaniczny"
  ]
  node [
    id 12
    label "wieczorowy"
  ]
  node [
    id 13
    label "szko&#322;a"
  ]
  node [
    id 14
    label "in&#380;ynierski"
  ]
  node [
    id 15
    label "bydgoski"
  ]
  node [
    id 16
    label "fabryka"
  ]
  node [
    id 17
    label "maszyna"
  ]
  node [
    id 18
    label "zak&#322;ad"
  ]
  node [
    id 19
    label "rowerowy"
  ]
  node [
    id 20
    label "biuro"
  ]
  node [
    id 21
    label "projekt"
  ]
  node [
    id 22
    label "budownictwo"
  ]
  node [
    id 23
    label "przemys&#322;owy"
  ]
  node [
    id 24
    label "kluba"
  ]
  node [
    id 25
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 26
    label "prasa"
  ]
  node [
    id 27
    label "i"
  ]
  node [
    id 28
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 29
    label "polskie"
  ]
  node [
    id 30
    label "radio"
  ]
  node [
    id 31
    label "nowy"
  ]
  node [
    id 32
    label "tor"
  ]
  node [
    id 33
    label "czy"
  ]
  node [
    id 34
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 35
    label "si&#281;"
  ]
  node [
    id 36
    label "boja"
  ]
  node [
    id 37
    label "zwi&#261;zek"
  ]
  node [
    id 38
    label "literat"
  ]
  node [
    id 39
    label "wojew&#243;dzki"
  ]
  node [
    id 40
    label "rada"
  ]
  node [
    id 41
    label "narodowy"
  ]
  node [
    id 42
    label "zas&#322;u&#380;y&#263;"
  ]
  node [
    id 43
    label "dzia&#322;acz"
  ]
  node [
    id 44
    label "kultura"
  ]
  node [
    id 45
    label "komitet"
  ]
  node [
    id 46
    label "do&#160;spraw"
  ]
  node [
    id 47
    label "telewizja"
  ]
  node [
    id 48
    label "krzy&#380;mo"
  ]
  node [
    id 49
    label "zas&#322;uga"
  ]
  node [
    id 50
    label "order"
  ]
  node [
    id 51
    label "odrodzi&#263;"
  ]
  node [
    id 52
    label "polski"
  ]
  node [
    id 53
    label "lot"
  ]
  node [
    id 54
    label "na"
  ]
  node [
    id 55
    label "uwi&#281;&#378;"
  ]
  node [
    id 56
    label "most"
  ]
  node [
    id 57
    label "kr&#243;lowy"
  ]
  node [
    id 58
    label "Jadwiga"
  ]
  node [
    id 59
    label "bilet"
  ]
  node [
    id 60
    label "do"
  ]
  node [
    id 61
    label "Singapur"
  ]
  node [
    id 62
    label "stan"
  ]
  node [
    id 63
    label "podgor&#261;czkowy"
  ]
  node [
    id 64
    label "raj"
  ]
  node [
    id 65
    label "beza"
  ]
  node [
    id 66
    label "Ewa"
  ]
  node [
    id 67
    label "koktajl"
  ]
  node [
    id 68
    label "dalajlama"
  ]
  node [
    id 69
    label "trzydzie&#347;ci"
  ]
  node [
    id 70
    label "jeden"
  ]
  node [
    id 71
    label "lusterko"
  ]
  node [
    id 72
    label "dok&#261;d"
  ]
  node [
    id 73
    label "biec"
  ]
  node [
    id 74
    label "tw&#243;j"
  ]
  node [
    id 75
    label "ko&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 45
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 47
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 30
    target 45
  ]
  edge [
    source 30
    target 46
  ]
  edge [
    source 30
    target 47
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 72
    target 75
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 75
  ]
  edge [
    source 74
    target 75
  ]
]
