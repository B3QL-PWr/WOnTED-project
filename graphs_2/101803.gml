graph [
  node [
    id 0
    label "nieprawda"
    origin "text"
  ]
  node [
    id 1
    label "parena&#347;cie"
    origin "text"
  ]
  node [
    id 2
    label "milion"
    origin "text"
  ]
  node [
    id 3
    label "tw&#243;rca"
    origin "text"
  ]
  node [
    id 4
    label "wcale"
    origin "text"
  ]
  node [
    id 5
    label "tychy"
    origin "text"
  ]
  node [
    id 6
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 7
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 8
    label "instytucja"
    origin "text"
  ]
  node [
    id 9
    label "dobrze"
    origin "text"
  ]
  node [
    id 10
    label "wiedza"
    origin "text"
  ]
  node [
    id 11
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "literalnie"
    origin "text"
  ]
  node [
    id 14
    label "obowi&#261;zuj&#261;cy"
    origin "text"
  ]
  node [
    id 15
    label "przepis"
    origin "text"
  ]
  node [
    id 16
    label "prawa"
    origin "text"
  ]
  node [
    id 17
    label "autorski"
    origin "text"
  ]
  node [
    id 18
    label "podpisa&#263;by"
    origin "text"
  ]
  node [
    id 19
    label "siebie"
    origin "text"
  ]
  node [
    id 20
    label "wyrok"
    origin "text"
  ]
  node [
    id 21
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 22
    label "cud"
    origin "text"
  ]
  node [
    id 23
    label "kreatywny"
    origin "text"
  ]
  node [
    id 24
    label "interpretacja"
    origin "text"
  ]
  node [
    id 25
    label "trafia&#263;"
    origin "text"
  ]
  node [
    id 26
    label "tylko"
    origin "text"
  ]
  node [
    id 27
    label "niewielki"
    origin "text"
  ]
  node [
    id 28
    label "wybrana"
    origin "text"
  ]
  node [
    id 29
    label "grupa"
    origin "text"
  ]
  node [
    id 30
    label "bogus&#322;aw"
    origin "text"
  ]
  node [
    id 31
    label "pluta"
    origin "text"
  ]
  node [
    id 32
    label "zpav"
    origin "text"
  ]
  node [
    id 33
    label "podczas"
    origin "text"
  ]
  node [
    id 34
    label "debata"
    origin "text"
  ]
  node [
    id 35
    label "wok&#243;&#322;"
    origin "text"
  ]
  node [
    id 36
    label "film"
    origin "text"
  ]
  node [
    id 37
    label "good"
    origin "text"
  ]
  node [
    id 38
    label "copy"
    origin "text"
  ]
  node [
    id 39
    label "bad"
    origin "text"
  ]
  node [
    id 40
    label "wprost"
    origin "text"
  ]
  node [
    id 41
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "tytu&#322;owy"
    origin "text"
  ]
  node [
    id 43
    label "miliard"
    origin "text"
  ]
  node [
    id 44
    label "by&#263;"
    origin "text"
  ]
  node [
    id 45
    label "dla"
    origin "text"
  ]
  node [
    id 46
    label "spory"
    origin "text"
  ]
  node [
    id 47
    label "k&#322;opot"
    origin "text"
  ]
  node [
    id 48
    label "pewnie"
    origin "text"
  ]
  node [
    id 49
    label "tak"
    origin "text"
  ]
  node [
    id 50
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 51
    label "bywa&#263;"
    origin "text"
  ]
  node [
    id 52
    label "rzeczywisto&#347;&#263;"
    origin "text"
  ]
  node [
    id 53
    label "s&#261;d"
  ]
  node [
    id 54
    label "u&#347;mierci&#263;"
  ]
  node [
    id 55
    label "fib"
  ]
  node [
    id 56
    label "u&#347;miercenie"
  ]
  node [
    id 57
    label "fa&#322;szywo&#347;&#263;"
  ]
  node [
    id 58
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 59
    label "zesp&#243;&#322;"
  ]
  node [
    id 60
    label "podejrzany"
  ]
  node [
    id 61
    label "s&#261;downictwo"
  ]
  node [
    id 62
    label "system"
  ]
  node [
    id 63
    label "biuro"
  ]
  node [
    id 64
    label "wytw&#243;r"
  ]
  node [
    id 65
    label "court"
  ]
  node [
    id 66
    label "forum"
  ]
  node [
    id 67
    label "bronienie"
  ]
  node [
    id 68
    label "urz&#261;d"
  ]
  node [
    id 69
    label "wydarzenie"
  ]
  node [
    id 70
    label "oskar&#380;yciel"
  ]
  node [
    id 71
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 72
    label "skazany"
  ]
  node [
    id 73
    label "post&#281;powanie"
  ]
  node [
    id 74
    label "broni&#263;"
  ]
  node [
    id 75
    label "my&#347;l"
  ]
  node [
    id 76
    label "pods&#261;dny"
  ]
  node [
    id 77
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 78
    label "obrona"
  ]
  node [
    id 79
    label "wypowied&#378;"
  ]
  node [
    id 80
    label "antylogizm"
  ]
  node [
    id 81
    label "konektyw"
  ]
  node [
    id 82
    label "&#347;wiadek"
  ]
  node [
    id 83
    label "procesowicz"
  ]
  node [
    id 84
    label "strona"
  ]
  node [
    id 85
    label "sprzecznia"
  ]
  node [
    id 86
    label "przeciwie&#324;stwo"
  ]
  node [
    id 87
    label "relacja"
  ]
  node [
    id 88
    label "sprzeczno&#347;&#263;"
  ]
  node [
    id 89
    label "zjawisko"
  ]
  node [
    id 90
    label "poinformowa&#263;"
  ]
  node [
    id 91
    label "dispatch"
  ]
  node [
    id 92
    label "spowodowa&#263;"
  ]
  node [
    id 93
    label "skrzywdzenie"
  ]
  node [
    id 94
    label "pozabijanie"
  ]
  node [
    id 95
    label "killing"
  ]
  node [
    id 96
    label "poinformowanie"
  ]
  node [
    id 97
    label "zabicie"
  ]
  node [
    id 98
    label "jako&#347;&#263;"
  ]
  node [
    id 99
    label "nieuczciwo&#347;&#263;"
  ]
  node [
    id 100
    label "falsity"
  ]
  node [
    id 101
    label "cecha"
  ]
  node [
    id 102
    label "miljon"
  ]
  node [
    id 103
    label "ba&#324;ka"
  ]
  node [
    id 104
    label "liczba"
  ]
  node [
    id 105
    label "kategoria"
  ]
  node [
    id 106
    label "pierwiastek"
  ]
  node [
    id 107
    label "rozmiar"
  ]
  node [
    id 108
    label "wyra&#380;enie"
  ]
  node [
    id 109
    label "poj&#281;cie"
  ]
  node [
    id 110
    label "number"
  ]
  node [
    id 111
    label "kategoria_gramatyczna"
  ]
  node [
    id 112
    label "kwadrat_magiczny"
  ]
  node [
    id 113
    label "koniugacja"
  ]
  node [
    id 114
    label "gourd"
  ]
  node [
    id 115
    label "narz&#281;dzie"
  ]
  node [
    id 116
    label "kwota"
  ]
  node [
    id 117
    label "naczynie"
  ]
  node [
    id 118
    label "obiekt_naturalny"
  ]
  node [
    id 119
    label "pojemnik"
  ]
  node [
    id 120
    label "niedostateczny"
  ]
  node [
    id 121
    label "&#322;eb"
  ]
  node [
    id 122
    label "mak&#243;wka"
  ]
  node [
    id 123
    label "bubble"
  ]
  node [
    id 124
    label "czaszka"
  ]
  node [
    id 125
    label "dynia"
  ]
  node [
    id 126
    label "kszta&#322;ciciel"
  ]
  node [
    id 127
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 128
    label "tworzyciel"
  ]
  node [
    id 129
    label "wykonawca"
  ]
  node [
    id 130
    label "pomys&#322;odawca"
  ]
  node [
    id 131
    label "&#347;w"
  ]
  node [
    id 132
    label "inicjator"
  ]
  node [
    id 133
    label "podmiot_gospodarczy"
  ]
  node [
    id 134
    label "artysta"
  ]
  node [
    id 135
    label "cz&#322;owiek"
  ]
  node [
    id 136
    label "muzyk"
  ]
  node [
    id 137
    label "nauczyciel"
  ]
  node [
    id 138
    label "autor"
  ]
  node [
    id 139
    label "ni_chuja"
  ]
  node [
    id 140
    label "zupe&#322;nie"
  ]
  node [
    id 141
    label "ca&#322;kiem"
  ]
  node [
    id 142
    label "zupe&#322;ny"
  ]
  node [
    id 143
    label "wniwecz"
  ]
  node [
    id 144
    label "kompletny"
  ]
  node [
    id 145
    label "rozmienia&#263;"
  ]
  node [
    id 146
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 147
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 148
    label "jednostka_monetarna"
  ]
  node [
    id 149
    label "moniak"
  ]
  node [
    id 150
    label "nomina&#322;"
  ]
  node [
    id 151
    label "zdewaluowa&#263;"
  ]
  node [
    id 152
    label "dewaluowanie"
  ]
  node [
    id 153
    label "pieni&#261;dze"
  ]
  node [
    id 154
    label "numizmat"
  ]
  node [
    id 155
    label "rozmienianie"
  ]
  node [
    id 156
    label "rozmieni&#263;"
  ]
  node [
    id 157
    label "dewaluowa&#263;"
  ]
  node [
    id 158
    label "rozmienienie"
  ]
  node [
    id 159
    label "zdewaluowanie"
  ]
  node [
    id 160
    label "coin"
  ]
  node [
    id 161
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 162
    label "przedmiot"
  ]
  node [
    id 163
    label "p&#322;&#243;d"
  ]
  node [
    id 164
    label "work"
  ]
  node [
    id 165
    label "rezultat"
  ]
  node [
    id 166
    label "moneta"
  ]
  node [
    id 167
    label "drobne"
  ]
  node [
    id 168
    label "medal"
  ]
  node [
    id 169
    label "numismatics"
  ]
  node [
    id 170
    label "okaz"
  ]
  node [
    id 171
    label "warto&#347;&#263;"
  ]
  node [
    id 172
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 173
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 174
    label "par_value"
  ]
  node [
    id 175
    label "cena"
  ]
  node [
    id 176
    label "znaczek"
  ]
  node [
    id 177
    label "zast&#281;powanie"
  ]
  node [
    id 178
    label "wymienienie"
  ]
  node [
    id 179
    label "change"
  ]
  node [
    id 180
    label "zmieni&#263;"
  ]
  node [
    id 181
    label "zmienia&#263;"
  ]
  node [
    id 182
    label "alternate"
  ]
  node [
    id 183
    label "obni&#380;anie"
  ]
  node [
    id 184
    label "umniejszanie"
  ]
  node [
    id 185
    label "devaluation"
  ]
  node [
    id 186
    label "obni&#380;a&#263;"
  ]
  node [
    id 187
    label "umniejsza&#263;"
  ]
  node [
    id 188
    label "knock"
  ]
  node [
    id 189
    label "devalue"
  ]
  node [
    id 190
    label "depreciate"
  ]
  node [
    id 191
    label "umniejszy&#263;"
  ]
  node [
    id 192
    label "obni&#380;y&#263;"
  ]
  node [
    id 193
    label "adulteration"
  ]
  node [
    id 194
    label "obni&#380;enie"
  ]
  node [
    id 195
    label "umniejszenie"
  ]
  node [
    id 196
    label "portfel"
  ]
  node [
    id 197
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 198
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 199
    label "forsa"
  ]
  node [
    id 200
    label "kapa&#263;"
  ]
  node [
    id 201
    label "kapn&#261;&#263;"
  ]
  node [
    id 202
    label "kapanie"
  ]
  node [
    id 203
    label "kapita&#322;"
  ]
  node [
    id 204
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 205
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 206
    label "kapni&#281;cie"
  ]
  node [
    id 207
    label "wyda&#263;"
  ]
  node [
    id 208
    label "hajs"
  ]
  node [
    id 209
    label "dydki"
  ]
  node [
    id 210
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 211
    label "zapanowa&#263;"
  ]
  node [
    id 212
    label "develop"
  ]
  node [
    id 213
    label "schorzenie"
  ]
  node [
    id 214
    label "nabawienie_si&#281;"
  ]
  node [
    id 215
    label "obskoczy&#263;"
  ]
  node [
    id 216
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 217
    label "catch"
  ]
  node [
    id 218
    label "zrobi&#263;"
  ]
  node [
    id 219
    label "get"
  ]
  node [
    id 220
    label "zwiastun"
  ]
  node [
    id 221
    label "doczeka&#263;"
  ]
  node [
    id 222
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 223
    label "kupi&#263;"
  ]
  node [
    id 224
    label "wysta&#263;"
  ]
  node [
    id 225
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 226
    label "wystarczy&#263;"
  ]
  node [
    id 227
    label "wzi&#261;&#263;"
  ]
  node [
    id 228
    label "naby&#263;"
  ]
  node [
    id 229
    label "nabawianie_si&#281;"
  ]
  node [
    id 230
    label "range"
  ]
  node [
    id 231
    label "uzyska&#263;"
  ]
  node [
    id 232
    label "suffice"
  ]
  node [
    id 233
    label "stan&#261;&#263;"
  ]
  node [
    id 234
    label "zaspokoi&#263;"
  ]
  node [
    id 235
    label "odziedziczy&#263;"
  ]
  node [
    id 236
    label "ruszy&#263;"
  ]
  node [
    id 237
    label "take"
  ]
  node [
    id 238
    label "zaatakowa&#263;"
  ]
  node [
    id 239
    label "skorzysta&#263;"
  ]
  node [
    id 240
    label "uciec"
  ]
  node [
    id 241
    label "receive"
  ]
  node [
    id 242
    label "nakaza&#263;"
  ]
  node [
    id 243
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 244
    label "bra&#263;"
  ]
  node [
    id 245
    label "u&#380;y&#263;"
  ]
  node [
    id 246
    label "wyrucha&#263;"
  ]
  node [
    id 247
    label "World_Health_Organization"
  ]
  node [
    id 248
    label "wyciupcia&#263;"
  ]
  node [
    id 249
    label "wygra&#263;"
  ]
  node [
    id 250
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 251
    label "withdraw"
  ]
  node [
    id 252
    label "wzi&#281;cie"
  ]
  node [
    id 253
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 254
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 255
    label "poczyta&#263;"
  ]
  node [
    id 256
    label "obj&#261;&#263;"
  ]
  node [
    id 257
    label "seize"
  ]
  node [
    id 258
    label "aim"
  ]
  node [
    id 259
    label "chwyci&#263;"
  ]
  node [
    id 260
    label "przyj&#261;&#263;"
  ]
  node [
    id 261
    label "pokona&#263;"
  ]
  node [
    id 262
    label "arise"
  ]
  node [
    id 263
    label "uda&#263;_si&#281;"
  ]
  node [
    id 264
    label "zacz&#261;&#263;"
  ]
  node [
    id 265
    label "otrzyma&#263;"
  ]
  node [
    id 266
    label "wej&#347;&#263;"
  ]
  node [
    id 267
    label "poruszy&#263;"
  ]
  node [
    id 268
    label "poradzi&#263;_sobie"
  ]
  node [
    id 269
    label "osaczy&#263;"
  ]
  node [
    id 270
    label "okra&#347;&#263;"
  ]
  node [
    id 271
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 272
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 273
    label "obiec"
  ]
  node [
    id 274
    label "powstrzyma&#263;"
  ]
  node [
    id 275
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 276
    label "manipulate"
  ]
  node [
    id 277
    label "rule"
  ]
  node [
    id 278
    label "cope"
  ]
  node [
    id 279
    label "post&#261;pi&#263;"
  ]
  node [
    id 280
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 281
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 282
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 283
    label "zorganizowa&#263;"
  ]
  node [
    id 284
    label "appoint"
  ]
  node [
    id 285
    label "wystylizowa&#263;"
  ]
  node [
    id 286
    label "cause"
  ]
  node [
    id 287
    label "przerobi&#263;"
  ]
  node [
    id 288
    label "nabra&#263;"
  ]
  node [
    id 289
    label "make"
  ]
  node [
    id 290
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 291
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 292
    label "wydali&#263;"
  ]
  node [
    id 293
    label "pozyska&#263;"
  ]
  node [
    id 294
    label "ustawi&#263;"
  ]
  node [
    id 295
    label "uwierzy&#263;"
  ]
  node [
    id 296
    label "zagra&#263;"
  ]
  node [
    id 297
    label "beget"
  ]
  node [
    id 298
    label "uzna&#263;"
  ]
  node [
    id 299
    label "draw"
  ]
  node [
    id 300
    label "pozosta&#263;"
  ]
  node [
    id 301
    label "poczeka&#263;"
  ]
  node [
    id 302
    label "wytrwa&#263;"
  ]
  node [
    id 303
    label "realize"
  ]
  node [
    id 304
    label "promocja"
  ]
  node [
    id 305
    label "wytworzy&#263;"
  ]
  node [
    id 306
    label "give_birth"
  ]
  node [
    id 307
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 308
    label "appreciation"
  ]
  node [
    id 309
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 310
    label "allude"
  ]
  node [
    id 311
    label "dotrze&#263;"
  ]
  node [
    id 312
    label "fall_upon"
  ]
  node [
    id 313
    label "przewidywanie"
  ]
  node [
    id 314
    label "oznaka"
  ]
  node [
    id 315
    label "harbinger"
  ]
  node [
    id 316
    label "obwie&#347;ciciel"
  ]
  node [
    id 317
    label "zapowied&#378;"
  ]
  node [
    id 318
    label "declaration"
  ]
  node [
    id 319
    label "reklama"
  ]
  node [
    id 320
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 321
    label "ognisko"
  ]
  node [
    id 322
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 323
    label "powalenie"
  ]
  node [
    id 324
    label "odezwanie_si&#281;"
  ]
  node [
    id 325
    label "atakowanie"
  ]
  node [
    id 326
    label "grupa_ryzyka"
  ]
  node [
    id 327
    label "przypadek"
  ]
  node [
    id 328
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 329
    label "inkubacja"
  ]
  node [
    id 330
    label "kryzys"
  ]
  node [
    id 331
    label "powali&#263;"
  ]
  node [
    id 332
    label "remisja"
  ]
  node [
    id 333
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 334
    label "zajmowa&#263;"
  ]
  node [
    id 335
    label "zaburzenie"
  ]
  node [
    id 336
    label "badanie_histopatologiczne"
  ]
  node [
    id 337
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 338
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 339
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 340
    label "odzywanie_si&#281;"
  ]
  node [
    id 341
    label "diagnoza"
  ]
  node [
    id 342
    label "atakowa&#263;"
  ]
  node [
    id 343
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 344
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 345
    label "zajmowanie"
  ]
  node [
    id 346
    label "osoba_prawna"
  ]
  node [
    id 347
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 348
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 349
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 350
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 351
    label "organizacja"
  ]
  node [
    id 352
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 353
    label "Fundusze_Unijne"
  ]
  node [
    id 354
    label "zamyka&#263;"
  ]
  node [
    id 355
    label "establishment"
  ]
  node [
    id 356
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 357
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 358
    label "afiliowa&#263;"
  ]
  node [
    id 359
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 360
    label "standard"
  ]
  node [
    id 361
    label "zamykanie"
  ]
  node [
    id 362
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 363
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 364
    label "pos&#322;uchanie"
  ]
  node [
    id 365
    label "skumanie"
  ]
  node [
    id 366
    label "orientacja"
  ]
  node [
    id 367
    label "zorientowanie"
  ]
  node [
    id 368
    label "teoria"
  ]
  node [
    id 369
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 370
    label "clasp"
  ]
  node [
    id 371
    label "forma"
  ]
  node [
    id 372
    label "przem&#243;wienie"
  ]
  node [
    id 373
    label "podmiot"
  ]
  node [
    id 374
    label "jednostka_organizacyjna"
  ]
  node [
    id 375
    label "struktura"
  ]
  node [
    id 376
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 377
    label "TOPR"
  ]
  node [
    id 378
    label "endecki"
  ]
  node [
    id 379
    label "przedstawicielstwo"
  ]
  node [
    id 380
    label "od&#322;am"
  ]
  node [
    id 381
    label "Cepelia"
  ]
  node [
    id 382
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 383
    label "ZBoWiD"
  ]
  node [
    id 384
    label "organization"
  ]
  node [
    id 385
    label "centrala"
  ]
  node [
    id 386
    label "GOPR"
  ]
  node [
    id 387
    label "ZOMO"
  ]
  node [
    id 388
    label "ZMP"
  ]
  node [
    id 389
    label "komitet_koordynacyjny"
  ]
  node [
    id 390
    label "przybud&#243;wka"
  ]
  node [
    id 391
    label "boj&#243;wka"
  ]
  node [
    id 392
    label "model"
  ]
  node [
    id 393
    label "organizowa&#263;"
  ]
  node [
    id 394
    label "ordinariness"
  ]
  node [
    id 395
    label "taniec_towarzyski"
  ]
  node [
    id 396
    label "organizowanie"
  ]
  node [
    id 397
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 398
    label "criterion"
  ]
  node [
    id 399
    label "zorganizowanie"
  ]
  node [
    id 400
    label "boks"
  ]
  node [
    id 401
    label "biurko"
  ]
  node [
    id 402
    label "palestra"
  ]
  node [
    id 403
    label "Biuro_Lustracyjne"
  ]
  node [
    id 404
    label "agency"
  ]
  node [
    id 405
    label "board"
  ]
  node [
    id 406
    label "dzia&#322;"
  ]
  node [
    id 407
    label "pomieszczenie"
  ]
  node [
    id 408
    label "stanowisko"
  ]
  node [
    id 409
    label "position"
  ]
  node [
    id 410
    label "siedziba"
  ]
  node [
    id 411
    label "organ"
  ]
  node [
    id 412
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 413
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 414
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 415
    label "mianowaniec"
  ]
  node [
    id 416
    label "okienko"
  ]
  node [
    id 417
    label "w&#322;adza"
  ]
  node [
    id 418
    label "consort"
  ]
  node [
    id 419
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 420
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 421
    label "umieszczanie"
  ]
  node [
    id 422
    label "powodowanie"
  ]
  node [
    id 423
    label "zamykanie_si&#281;"
  ]
  node [
    id 424
    label "ko&#324;czenie"
  ]
  node [
    id 425
    label "extinction"
  ]
  node [
    id 426
    label "robienie"
  ]
  node [
    id 427
    label "unieruchamianie"
  ]
  node [
    id 428
    label "ukrywanie"
  ]
  node [
    id 429
    label "sk&#322;adanie"
  ]
  node [
    id 430
    label "locking"
  ]
  node [
    id 431
    label "zawieranie"
  ]
  node [
    id 432
    label "wy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 433
    label "closing"
  ]
  node [
    id 434
    label "blocking"
  ]
  node [
    id 435
    label "przytrzaskiwanie"
  ]
  node [
    id 436
    label "czynno&#347;&#263;"
  ]
  node [
    id 437
    label "blokowanie"
  ]
  node [
    id 438
    label "occlusion"
  ]
  node [
    id 439
    label "rozwi&#261;zywanie"
  ]
  node [
    id 440
    label "przygaszanie"
  ]
  node [
    id 441
    label "lock"
  ]
  node [
    id 442
    label "ujmowanie"
  ]
  node [
    id 443
    label "zawiera&#263;"
  ]
  node [
    id 444
    label "suspend"
  ]
  node [
    id 445
    label "ujmowa&#263;"
  ]
  node [
    id 446
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 447
    label "unieruchamia&#263;"
  ]
  node [
    id 448
    label "sk&#322;ada&#263;"
  ]
  node [
    id 449
    label "ko&#324;czy&#263;"
  ]
  node [
    id 450
    label "blokowa&#263;"
  ]
  node [
    id 451
    label "umieszcza&#263;"
  ]
  node [
    id 452
    label "ukrywa&#263;"
  ]
  node [
    id 453
    label "exsert"
  ]
  node [
    id 454
    label "elite"
  ]
  node [
    id 455
    label "&#347;rodowisko"
  ]
  node [
    id 456
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 457
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 458
    label "odpowiednio"
  ]
  node [
    id 459
    label "dobroczynnie"
  ]
  node [
    id 460
    label "moralnie"
  ]
  node [
    id 461
    label "korzystnie"
  ]
  node [
    id 462
    label "pozytywnie"
  ]
  node [
    id 463
    label "lepiej"
  ]
  node [
    id 464
    label "wiele"
  ]
  node [
    id 465
    label "skutecznie"
  ]
  node [
    id 466
    label "pomy&#347;lnie"
  ]
  node [
    id 467
    label "dobry"
  ]
  node [
    id 468
    label "charakterystycznie"
  ]
  node [
    id 469
    label "nale&#380;nie"
  ]
  node [
    id 470
    label "stosowny"
  ]
  node [
    id 471
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 472
    label "nale&#380;ycie"
  ]
  node [
    id 473
    label "prawdziwie"
  ]
  node [
    id 474
    label "auspiciously"
  ]
  node [
    id 475
    label "pomy&#347;lny"
  ]
  node [
    id 476
    label "moralny"
  ]
  node [
    id 477
    label "etyczny"
  ]
  node [
    id 478
    label "skuteczny"
  ]
  node [
    id 479
    label "wiela"
  ]
  node [
    id 480
    label "du&#380;y"
  ]
  node [
    id 481
    label "utylitarnie"
  ]
  node [
    id 482
    label "korzystny"
  ]
  node [
    id 483
    label "beneficially"
  ]
  node [
    id 484
    label "przyjemnie"
  ]
  node [
    id 485
    label "pozytywny"
  ]
  node [
    id 486
    label "ontologicznie"
  ]
  node [
    id 487
    label "dodatni"
  ]
  node [
    id 488
    label "odpowiedni"
  ]
  node [
    id 489
    label "wiersz"
  ]
  node [
    id 490
    label "dobroczynny"
  ]
  node [
    id 491
    label "czw&#243;rka"
  ]
  node [
    id 492
    label "spokojny"
  ]
  node [
    id 493
    label "&#347;mieszny"
  ]
  node [
    id 494
    label "mi&#322;y"
  ]
  node [
    id 495
    label "grzeczny"
  ]
  node [
    id 496
    label "powitanie"
  ]
  node [
    id 497
    label "ca&#322;y"
  ]
  node [
    id 498
    label "zwrot"
  ]
  node [
    id 499
    label "drogi"
  ]
  node [
    id 500
    label "pos&#322;uszny"
  ]
  node [
    id 501
    label "philanthropically"
  ]
  node [
    id 502
    label "spo&#322;ecznie"
  ]
  node [
    id 503
    label "cognition"
  ]
  node [
    id 504
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 505
    label "intelekt"
  ]
  node [
    id 506
    label "pozwolenie"
  ]
  node [
    id 507
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 508
    label "zaawansowanie"
  ]
  node [
    id 509
    label "wykszta&#322;cenie"
  ]
  node [
    id 510
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 511
    label "zdolno&#347;&#263;"
  ]
  node [
    id 512
    label "ekstraspekcja"
  ]
  node [
    id 513
    label "feeling"
  ]
  node [
    id 514
    label "zemdle&#263;"
  ]
  node [
    id 515
    label "psychika"
  ]
  node [
    id 516
    label "stan"
  ]
  node [
    id 517
    label "Freud"
  ]
  node [
    id 518
    label "psychoanaliza"
  ]
  node [
    id 519
    label "conscience"
  ]
  node [
    id 520
    label "integer"
  ]
  node [
    id 521
    label "zlewanie_si&#281;"
  ]
  node [
    id 522
    label "ilo&#347;&#263;"
  ]
  node [
    id 523
    label "uk&#322;ad"
  ]
  node [
    id 524
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 525
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 526
    label "pe&#322;ny"
  ]
  node [
    id 527
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 528
    label "rozwini&#281;cie"
  ]
  node [
    id 529
    label "zapoznanie"
  ]
  node [
    id 530
    label "wys&#322;anie"
  ]
  node [
    id 531
    label "udoskonalenie"
  ]
  node [
    id 532
    label "pomo&#380;enie"
  ]
  node [
    id 533
    label "urszulanki"
  ]
  node [
    id 534
    label "training"
  ]
  node [
    id 535
    label "niepokalanki"
  ]
  node [
    id 536
    label "o&#347;wiecenie"
  ]
  node [
    id 537
    label "kwalifikacje"
  ]
  node [
    id 538
    label "sophistication"
  ]
  node [
    id 539
    label "skolaryzacja"
  ]
  node [
    id 540
    label "form"
  ]
  node [
    id 541
    label "umys&#322;"
  ]
  node [
    id 542
    label "noosfera"
  ]
  node [
    id 543
    label "stopie&#324;"
  ]
  node [
    id 544
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 545
    label "decyzja"
  ]
  node [
    id 546
    label "zwalnianie_si&#281;"
  ]
  node [
    id 547
    label "authorization"
  ]
  node [
    id 548
    label "koncesjonowanie"
  ]
  node [
    id 549
    label "zwolnienie_si&#281;"
  ]
  node [
    id 550
    label "pozwole&#324;stwo"
  ]
  node [
    id 551
    label "bycie_w_stanie"
  ]
  node [
    id 552
    label "odwieszenie"
  ]
  node [
    id 553
    label "odpowied&#378;"
  ]
  node [
    id 554
    label "pofolgowanie"
  ]
  node [
    id 555
    label "license"
  ]
  node [
    id 556
    label "franchise"
  ]
  node [
    id 557
    label "umo&#380;liwienie"
  ]
  node [
    id 558
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 559
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 560
    label "dokument"
  ]
  node [
    id 561
    label "uznanie"
  ]
  node [
    id 562
    label "zrobienie"
  ]
  node [
    id 563
    label "u&#380;ywa&#263;"
  ]
  node [
    id 564
    label "korzysta&#263;"
  ]
  node [
    id 565
    label "distribute"
  ]
  node [
    id 566
    label "give"
  ]
  node [
    id 567
    label "bash"
  ]
  node [
    id 568
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 569
    label "doznawa&#263;"
  ]
  node [
    id 570
    label "dos&#322;owny"
  ]
  node [
    id 571
    label "wiernie"
  ]
  node [
    id 572
    label "literally"
  ]
  node [
    id 573
    label "precisely"
  ]
  node [
    id 574
    label "accurately"
  ]
  node [
    id 575
    label "dok&#322;adnie"
  ]
  node [
    id 576
    label "wierny"
  ]
  node [
    id 577
    label "szczero"
  ]
  node [
    id 578
    label "podobnie"
  ]
  node [
    id 579
    label "zgodnie"
  ]
  node [
    id 580
    label "naprawd&#281;"
  ]
  node [
    id 581
    label "szczerze"
  ]
  node [
    id 582
    label "truly"
  ]
  node [
    id 583
    label "prawdziwy"
  ]
  node [
    id 584
    label "rzeczywisty"
  ]
  node [
    id 585
    label "dos&#322;ownie"
  ]
  node [
    id 586
    label "tekstualny"
  ]
  node [
    id 587
    label "bezpo&#347;redni"
  ]
  node [
    id 588
    label "nieprzeno&#347;ny"
  ]
  node [
    id 589
    label "uprawomocnienie_si&#281;"
  ]
  node [
    id 590
    label "uprawomocnianie_si&#281;"
  ]
  node [
    id 591
    label "obowi&#261;zuj&#261;co"
  ]
  node [
    id 592
    label "aktualny"
  ]
  node [
    id 593
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 594
    label "aktualnie"
  ]
  node [
    id 595
    label "wa&#380;ny"
  ]
  node [
    id 596
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 597
    label "aktualizowanie"
  ]
  node [
    id 598
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 599
    label "uaktualnienie"
  ]
  node [
    id 600
    label "norma_prawna"
  ]
  node [
    id 601
    label "przedawnienie_si&#281;"
  ]
  node [
    id 602
    label "spos&#243;b"
  ]
  node [
    id 603
    label "przedawnianie_si&#281;"
  ]
  node [
    id 604
    label "porada"
  ]
  node [
    id 605
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 606
    label "regulation"
  ]
  node [
    id 607
    label "recepta"
  ]
  node [
    id 608
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 609
    label "prawo"
  ]
  node [
    id 610
    label "kodeks"
  ]
  node [
    id 611
    label "zbi&#243;r"
  ]
  node [
    id 612
    label "tryb"
  ]
  node [
    id 613
    label "nature"
  ]
  node [
    id 614
    label "wskaz&#243;wka"
  ]
  node [
    id 615
    label "zlecenie"
  ]
  node [
    id 616
    label "receipt"
  ]
  node [
    id 617
    label "receptariusz"
  ]
  node [
    id 618
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 619
    label "umocowa&#263;"
  ]
  node [
    id 620
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 621
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 622
    label "procesualistyka"
  ]
  node [
    id 623
    label "regu&#322;a_Allena"
  ]
  node [
    id 624
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 625
    label "kryminalistyka"
  ]
  node [
    id 626
    label "szko&#322;a"
  ]
  node [
    id 627
    label "kierunek"
  ]
  node [
    id 628
    label "zasada_d'Alemberta"
  ]
  node [
    id 629
    label "obserwacja"
  ]
  node [
    id 630
    label "normatywizm"
  ]
  node [
    id 631
    label "jurisprudence"
  ]
  node [
    id 632
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 633
    label "kultura_duchowa"
  ]
  node [
    id 634
    label "prawo_karne_procesowe"
  ]
  node [
    id 635
    label "kazuistyka"
  ]
  node [
    id 636
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 637
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 638
    label "kryminologia"
  ]
  node [
    id 639
    label "opis"
  ]
  node [
    id 640
    label "regu&#322;a_Glogera"
  ]
  node [
    id 641
    label "prawo_Mendla"
  ]
  node [
    id 642
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 643
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 644
    label "prawo_karne"
  ]
  node [
    id 645
    label "legislacyjnie"
  ]
  node [
    id 646
    label "twierdzenie"
  ]
  node [
    id 647
    label "cywilistyka"
  ]
  node [
    id 648
    label "judykatura"
  ]
  node [
    id 649
    label "kanonistyka"
  ]
  node [
    id 650
    label "nauka_prawa"
  ]
  node [
    id 651
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 652
    label "law"
  ]
  node [
    id 653
    label "qualification"
  ]
  node [
    id 654
    label "dominion"
  ]
  node [
    id 655
    label "wykonawczy"
  ]
  node [
    id 656
    label "zasada"
  ]
  node [
    id 657
    label "normalizacja"
  ]
  node [
    id 658
    label "obwiniony"
  ]
  node [
    id 659
    label "r&#281;kopis"
  ]
  node [
    id 660
    label "kodeks_pracy"
  ]
  node [
    id 661
    label "kodeks_morski"
  ]
  node [
    id 662
    label "Justynian"
  ]
  node [
    id 663
    label "code"
  ]
  node [
    id 664
    label "kodeks_drogowy"
  ]
  node [
    id 665
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 666
    label "kodeks_rodzinny"
  ]
  node [
    id 667
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 668
    label "kodeks_cywilny"
  ]
  node [
    id 669
    label "kodeks_karny"
  ]
  node [
    id 670
    label "w&#322;asny"
  ]
  node [
    id 671
    label "oryginalny"
  ]
  node [
    id 672
    label "autorsko"
  ]
  node [
    id 673
    label "samodzielny"
  ]
  node [
    id 674
    label "zwi&#261;zany"
  ]
  node [
    id 675
    label "czyj&#347;"
  ]
  node [
    id 676
    label "swoisty"
  ]
  node [
    id 677
    label "osobny"
  ]
  node [
    id 678
    label "niespotykany"
  ]
  node [
    id 679
    label "o&#380;ywczy"
  ]
  node [
    id 680
    label "ekscentryczny"
  ]
  node [
    id 681
    label "nowy"
  ]
  node [
    id 682
    label "oryginalnie"
  ]
  node [
    id 683
    label "inny"
  ]
  node [
    id 684
    label "pierwotny"
  ]
  node [
    id 685
    label "warto&#347;ciowy"
  ]
  node [
    id 686
    label "prawnie"
  ]
  node [
    id 687
    label "indywidualnie"
  ]
  node [
    id 688
    label "sentencja"
  ]
  node [
    id 689
    label "kara"
  ]
  node [
    id 690
    label "orzeczenie"
  ]
  node [
    id 691
    label "judgment"
  ]
  node [
    id 692
    label "order"
  ]
  node [
    id 693
    label "przebiec"
  ]
  node [
    id 694
    label "charakter"
  ]
  node [
    id 695
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 696
    label "motyw"
  ]
  node [
    id 697
    label "przebiegni&#281;cie"
  ]
  node [
    id 698
    label "fabu&#322;a"
  ]
  node [
    id 699
    label "nemezis"
  ]
  node [
    id 700
    label "konsekwencja"
  ]
  node [
    id 701
    label "punishment"
  ]
  node [
    id 702
    label "klacz"
  ]
  node [
    id 703
    label "forfeit"
  ]
  node [
    id 704
    label "roboty_przymusowe"
  ]
  node [
    id 705
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 706
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 707
    label "odznaka"
  ]
  node [
    id 708
    label "kawaler"
  ]
  node [
    id 709
    label "powiedzenie"
  ]
  node [
    id 710
    label "rubrum"
  ]
  node [
    id 711
    label "defenestracja"
  ]
  node [
    id 712
    label "agonia"
  ]
  node [
    id 713
    label "kres"
  ]
  node [
    id 714
    label "mogi&#322;a"
  ]
  node [
    id 715
    label "&#380;ycie"
  ]
  node [
    id 716
    label "kres_&#380;ycia"
  ]
  node [
    id 717
    label "upadek"
  ]
  node [
    id 718
    label "szeol"
  ]
  node [
    id 719
    label "pogrzebanie"
  ]
  node [
    id 720
    label "istota_nadprzyrodzona"
  ]
  node [
    id 721
    label "&#380;a&#322;oba"
  ]
  node [
    id 722
    label "pogrzeb"
  ]
  node [
    id 723
    label "ostatnie_podrygi"
  ]
  node [
    id 724
    label "punkt"
  ]
  node [
    id 725
    label "dzia&#322;anie"
  ]
  node [
    id 726
    label "chwila"
  ]
  node [
    id 727
    label "koniec"
  ]
  node [
    id 728
    label "gleba"
  ]
  node [
    id 729
    label "kondycja"
  ]
  node [
    id 730
    label "ruch"
  ]
  node [
    id 731
    label "pogorszenie"
  ]
  node [
    id 732
    label "inclination"
  ]
  node [
    id 733
    label "death"
  ]
  node [
    id 734
    label "zmierzch"
  ]
  node [
    id 735
    label "nieuleczalnie_chory"
  ]
  node [
    id 736
    label "spocz&#261;&#263;"
  ]
  node [
    id 737
    label "spocz&#281;cie"
  ]
  node [
    id 738
    label "pochowanie"
  ]
  node [
    id 739
    label "spoczywa&#263;"
  ]
  node [
    id 740
    label "chowanie"
  ]
  node [
    id 741
    label "park_sztywnych"
  ]
  node [
    id 742
    label "pomnik"
  ]
  node [
    id 743
    label "nagrobek"
  ]
  node [
    id 744
    label "prochowisko"
  ]
  node [
    id 745
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 746
    label "spoczywanie"
  ]
  node [
    id 747
    label "za&#347;wiaty"
  ]
  node [
    id 748
    label "piek&#322;o"
  ]
  node [
    id 749
    label "judaizm"
  ]
  node [
    id 750
    label "destruction"
  ]
  node [
    id 751
    label "zabrzmienie"
  ]
  node [
    id 752
    label "zniszczenie"
  ]
  node [
    id 753
    label "zaszkodzenie"
  ]
  node [
    id 754
    label "usuni&#281;cie"
  ]
  node [
    id 755
    label "spowodowanie"
  ]
  node [
    id 756
    label "zdarzenie_si&#281;"
  ]
  node [
    id 757
    label "czyn"
  ]
  node [
    id 758
    label "umarcie"
  ]
  node [
    id 759
    label "granie"
  ]
  node [
    id 760
    label "zamkni&#281;cie"
  ]
  node [
    id 761
    label "compaction"
  ]
  node [
    id 762
    label "&#380;al"
  ]
  node [
    id 763
    label "paznokie&#263;"
  ]
  node [
    id 764
    label "symbol"
  ]
  node [
    id 765
    label "czas"
  ]
  node [
    id 766
    label "kir"
  ]
  node [
    id 767
    label "brud"
  ]
  node [
    id 768
    label "wyrzucenie"
  ]
  node [
    id 769
    label "defenestration"
  ]
  node [
    id 770
    label "zaj&#347;cie"
  ]
  node [
    id 771
    label "burying"
  ]
  node [
    id 772
    label "zasypanie"
  ]
  node [
    id 773
    label "zw&#322;oki"
  ]
  node [
    id 774
    label "burial"
  ]
  node [
    id 775
    label "w&#322;o&#380;enie"
  ]
  node [
    id 776
    label "porobienie"
  ]
  node [
    id 777
    label "gr&#243;b"
  ]
  node [
    id 778
    label "uniemo&#380;liwienie"
  ]
  node [
    id 779
    label "niepowodzenie"
  ]
  node [
    id 780
    label "stypa"
  ]
  node [
    id 781
    label "pusta_noc"
  ]
  node [
    id 782
    label "grabarz"
  ]
  node [
    id 783
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 784
    label "obrz&#281;d"
  ]
  node [
    id 785
    label "raj_utracony"
  ]
  node [
    id 786
    label "umieranie"
  ]
  node [
    id 787
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 788
    label "prze&#380;ywanie"
  ]
  node [
    id 789
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 790
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 791
    label "po&#322;&#243;g"
  ]
  node [
    id 792
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 793
    label "subsistence"
  ]
  node [
    id 794
    label "power"
  ]
  node [
    id 795
    label "okres_noworodkowy"
  ]
  node [
    id 796
    label "prze&#380;ycie"
  ]
  node [
    id 797
    label "wiek_matuzalemowy"
  ]
  node [
    id 798
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 799
    label "entity"
  ]
  node [
    id 800
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 801
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 802
    label "do&#380;ywanie"
  ]
  node [
    id 803
    label "byt"
  ]
  node [
    id 804
    label "andropauza"
  ]
  node [
    id 805
    label "dzieci&#324;stwo"
  ]
  node [
    id 806
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 807
    label "rozw&#243;j"
  ]
  node [
    id 808
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 809
    label "menopauza"
  ]
  node [
    id 810
    label "koleje_losu"
  ]
  node [
    id 811
    label "bycie"
  ]
  node [
    id 812
    label "zegar_biologiczny"
  ]
  node [
    id 813
    label "szwung"
  ]
  node [
    id 814
    label "przebywanie"
  ]
  node [
    id 815
    label "warunki"
  ]
  node [
    id 816
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 817
    label "niemowl&#281;ctwo"
  ]
  node [
    id 818
    label "&#380;ywy"
  ]
  node [
    id 819
    label "life"
  ]
  node [
    id 820
    label "staro&#347;&#263;"
  ]
  node [
    id 821
    label "energy"
  ]
  node [
    id 822
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 823
    label "rzadko&#347;&#263;"
  ]
  node [
    id 824
    label "achiropita"
  ]
  node [
    id 825
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 826
    label "proces"
  ]
  node [
    id 827
    label "boski"
  ]
  node [
    id 828
    label "krajobraz"
  ]
  node [
    id 829
    label "przywidzenie"
  ]
  node [
    id 830
    label "presence"
  ]
  node [
    id 831
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 832
    label "g&#281;sto&#347;&#263;"
  ]
  node [
    id 833
    label "frekwencja"
  ]
  node [
    id 834
    label "niezwyk&#322;o&#347;&#263;"
  ]
  node [
    id 835
    label "rozmieszczenie"
  ]
  node [
    id 836
    label "przedstawienie"
  ]
  node [
    id 837
    label "kreatywnie"
  ]
  node [
    id 838
    label "sprzyjaj&#261;cy"
  ]
  node [
    id 839
    label "inspiruj&#261;cy"
  ]
  node [
    id 840
    label "zmienny"
  ]
  node [
    id 841
    label "tw&#243;rczo"
  ]
  node [
    id 842
    label "pomys&#322;owy"
  ]
  node [
    id 843
    label "stymuluj&#261;cy"
  ]
  node [
    id 844
    label "buduj&#261;cy"
  ]
  node [
    id 845
    label "inspiruj&#261;co"
  ]
  node [
    id 846
    label "przychylny"
  ]
  node [
    id 847
    label "zmiennie"
  ]
  node [
    id 848
    label "chor&#261;giewka_na_wietrze"
  ]
  node [
    id 849
    label "sprytny"
  ]
  node [
    id 850
    label "inteligentny"
  ]
  node [
    id 851
    label "niebanalny"
  ]
  node [
    id 852
    label "pomys&#322;owo"
  ]
  node [
    id 853
    label "tw&#243;rczy"
  ]
  node [
    id 854
    label "explanation"
  ]
  node [
    id 855
    label "hermeneutyka"
  ]
  node [
    id 856
    label "wypracowanie"
  ]
  node [
    id 857
    label "kontekst"
  ]
  node [
    id 858
    label "realizacja"
  ]
  node [
    id 859
    label "interpretation"
  ]
  node [
    id 860
    label "obja&#347;nienie"
  ]
  node [
    id 861
    label "fabrication"
  ]
  node [
    id 862
    label "scheduling"
  ]
  node [
    id 863
    label "operacja"
  ]
  node [
    id 864
    label "dzie&#322;o"
  ]
  node [
    id 865
    label "kreacja"
  ]
  node [
    id 866
    label "monta&#380;"
  ]
  node [
    id 867
    label "postprodukcja"
  ]
  node [
    id 868
    label "performance"
  ]
  node [
    id 869
    label "praca_pisemna"
  ]
  node [
    id 870
    label "draft"
  ]
  node [
    id 871
    label "papier_kancelaryjny"
  ]
  node [
    id 872
    label "sparafrazowanie"
  ]
  node [
    id 873
    label "strawestowa&#263;"
  ]
  node [
    id 874
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 875
    label "trawestowa&#263;"
  ]
  node [
    id 876
    label "sparafrazowa&#263;"
  ]
  node [
    id 877
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 878
    label "sformu&#322;owanie"
  ]
  node [
    id 879
    label "parafrazowanie"
  ]
  node [
    id 880
    label "ozdobnik"
  ]
  node [
    id 881
    label "delimitacja"
  ]
  node [
    id 882
    label "parafrazowa&#263;"
  ]
  node [
    id 883
    label "stylizacja"
  ]
  node [
    id 884
    label "komunikat"
  ]
  node [
    id 885
    label "trawestowanie"
  ]
  node [
    id 886
    label "strawestowanie"
  ]
  node [
    id 887
    label "remark"
  ]
  node [
    id 888
    label "report"
  ]
  node [
    id 889
    label "zrozumia&#322;y"
  ]
  node [
    id 890
    label "informacja"
  ]
  node [
    id 891
    label "odniesienie"
  ]
  node [
    id 892
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 893
    label "otoczenie"
  ]
  node [
    id 894
    label "background"
  ]
  node [
    id 895
    label "causal_agent"
  ]
  node [
    id 896
    label "context"
  ]
  node [
    id 897
    label "fragment"
  ]
  node [
    id 898
    label "hermeneutics"
  ]
  node [
    id 899
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 900
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 901
    label "indicate"
  ]
  node [
    id 902
    label "spotyka&#263;"
  ]
  node [
    id 903
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 904
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 905
    label "pocisk"
  ]
  node [
    id 906
    label "dociera&#263;"
  ]
  node [
    id 907
    label "dolatywa&#263;"
  ]
  node [
    id 908
    label "dopasowywa&#263;_si&#281;"
  ]
  node [
    id 909
    label "znajdowa&#263;"
  ]
  node [
    id 910
    label "hit"
  ]
  node [
    id 911
    label "happen"
  ]
  node [
    id 912
    label "wpada&#263;"
  ]
  node [
    id 913
    label "flow"
  ]
  node [
    id 914
    label "reach"
  ]
  node [
    id 915
    label "dochodzi&#263;"
  ]
  node [
    id 916
    label "odzyskiwa&#263;"
  ]
  node [
    id 917
    label "znachodzi&#263;"
  ]
  node [
    id 918
    label "pozyskiwa&#263;"
  ]
  node [
    id 919
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 920
    label "detect"
  ]
  node [
    id 921
    label "powodowa&#263;"
  ]
  node [
    id 922
    label "unwrap"
  ]
  node [
    id 923
    label "wykrywa&#263;"
  ]
  node [
    id 924
    label "os&#261;dza&#263;"
  ]
  node [
    id 925
    label "wymy&#347;la&#263;"
  ]
  node [
    id 926
    label "poznawa&#263;"
  ]
  node [
    id 927
    label "strike"
  ]
  node [
    id 928
    label "mie&#263;_miejsce"
  ]
  node [
    id 929
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 930
    label "fall"
  ]
  node [
    id 931
    label "styka&#263;_si&#281;"
  ]
  node [
    id 932
    label "silnik"
  ]
  node [
    id 933
    label "dopasowywa&#263;"
  ]
  node [
    id 934
    label "g&#322;adzi&#263;"
  ]
  node [
    id 935
    label "boost"
  ]
  node [
    id 936
    label "dorabia&#263;"
  ]
  node [
    id 937
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 938
    label "trze&#263;"
  ]
  node [
    id 939
    label "zaziera&#263;"
  ]
  node [
    id 940
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 941
    label "czu&#263;"
  ]
  node [
    id 942
    label "drop"
  ]
  node [
    id 943
    label "pogo"
  ]
  node [
    id 944
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 945
    label "wpa&#347;&#263;"
  ]
  node [
    id 946
    label "rzecz"
  ]
  node [
    id 947
    label "d&#378;wi&#281;k"
  ]
  node [
    id 948
    label "ogrom"
  ]
  node [
    id 949
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 950
    label "zapach"
  ]
  node [
    id 951
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 952
    label "popada&#263;"
  ]
  node [
    id 953
    label "odwiedza&#263;"
  ]
  node [
    id 954
    label "przypomina&#263;"
  ]
  node [
    id 955
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 956
    label "&#347;wiat&#322;o"
  ]
  node [
    id 957
    label "chowa&#263;"
  ]
  node [
    id 958
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 959
    label "demaskowa&#263;"
  ]
  node [
    id 960
    label "ulega&#263;"
  ]
  node [
    id 961
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 962
    label "emocja"
  ]
  node [
    id 963
    label "flatten"
  ]
  node [
    id 964
    label "winnings"
  ]
  node [
    id 965
    label "si&#281;ga&#263;"
  ]
  node [
    id 966
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 967
    label "osi&#261;ga&#263;"
  ]
  node [
    id 968
    label "moda"
  ]
  node [
    id 969
    label "popularny"
  ]
  node [
    id 970
    label "utw&#243;r"
  ]
  node [
    id 971
    label "sensacja"
  ]
  node [
    id 972
    label "nowina"
  ]
  node [
    id 973
    label "odkrycie"
  ]
  node [
    id 974
    label "amunicja"
  ]
  node [
    id 975
    label "g&#322;owica"
  ]
  node [
    id 976
    label "trafienie"
  ]
  node [
    id 977
    label "trafianie"
  ]
  node [
    id 978
    label "kulka"
  ]
  node [
    id 979
    label "rdze&#324;"
  ]
  node [
    id 980
    label "prochownia"
  ]
  node [
    id 981
    label "przeniesienie"
  ]
  node [
    id 982
    label "&#322;adunek_bojowy"
  ]
  node [
    id 983
    label "trafi&#263;"
  ]
  node [
    id 984
    label "przenoszenie"
  ]
  node [
    id 985
    label "przenie&#347;&#263;"
  ]
  node [
    id 986
    label "przenosi&#263;"
  ]
  node [
    id 987
    label "bro&#324;"
  ]
  node [
    id 988
    label "nielicznie"
  ]
  node [
    id 989
    label "niewa&#380;ny"
  ]
  node [
    id 990
    label "ma&#322;y"
  ]
  node [
    id 991
    label "ma&#322;o"
  ]
  node [
    id 992
    label "nieznaczny"
  ]
  node [
    id 993
    label "pomiernie"
  ]
  node [
    id 994
    label "kr&#243;tko"
  ]
  node [
    id 995
    label "mikroskopijnie"
  ]
  node [
    id 996
    label "nieliczny"
  ]
  node [
    id 997
    label "mo&#380;liwie"
  ]
  node [
    id 998
    label "nieistotnie"
  ]
  node [
    id 999
    label "szybki"
  ]
  node [
    id 1000
    label "przeci&#281;tny"
  ]
  node [
    id 1001
    label "wstydliwy"
  ]
  node [
    id 1002
    label "s&#322;aby"
  ]
  node [
    id 1003
    label "ch&#322;opiec"
  ]
  node [
    id 1004
    label "m&#322;ody"
  ]
  node [
    id 1005
    label "marny"
  ]
  node [
    id 1006
    label "n&#281;dznie"
  ]
  node [
    id 1007
    label "uniewa&#380;nianie_si&#281;"
  ]
  node [
    id 1008
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 1009
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 1010
    label "odm&#322;adzanie"
  ]
  node [
    id 1011
    label "liga"
  ]
  node [
    id 1012
    label "jednostka_systematyczna"
  ]
  node [
    id 1013
    label "asymilowanie"
  ]
  node [
    id 1014
    label "gromada"
  ]
  node [
    id 1015
    label "asymilowa&#263;"
  ]
  node [
    id 1016
    label "egzemplarz"
  ]
  node [
    id 1017
    label "Entuzjastki"
  ]
  node [
    id 1018
    label "kompozycja"
  ]
  node [
    id 1019
    label "Terranie"
  ]
  node [
    id 1020
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1021
    label "category"
  ]
  node [
    id 1022
    label "pakiet_klimatyczny"
  ]
  node [
    id 1023
    label "oddzia&#322;"
  ]
  node [
    id 1024
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1025
    label "cz&#261;steczka"
  ]
  node [
    id 1026
    label "stage_set"
  ]
  node [
    id 1027
    label "type"
  ]
  node [
    id 1028
    label "specgrupa"
  ]
  node [
    id 1029
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1030
    label "&#346;wietliki"
  ]
  node [
    id 1031
    label "odm&#322;odzenie"
  ]
  node [
    id 1032
    label "Eurogrupa"
  ]
  node [
    id 1033
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1034
    label "formacja_geologiczna"
  ]
  node [
    id 1035
    label "harcerze_starsi"
  ]
  node [
    id 1036
    label "konfiguracja"
  ]
  node [
    id 1037
    label "cz&#261;stka"
  ]
  node [
    id 1038
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 1039
    label "diadochia"
  ]
  node [
    id 1040
    label "substancja"
  ]
  node [
    id 1041
    label "grupa_funkcyjna"
  ]
  node [
    id 1042
    label "series"
  ]
  node [
    id 1043
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1044
    label "uprawianie"
  ]
  node [
    id 1045
    label "praca_rolnicza"
  ]
  node [
    id 1046
    label "collection"
  ]
  node [
    id 1047
    label "dane"
  ]
  node [
    id 1048
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1049
    label "sum"
  ]
  node [
    id 1050
    label "gathering"
  ]
  node [
    id 1051
    label "album"
  ]
  node [
    id 1052
    label "lias"
  ]
  node [
    id 1053
    label "jednostka"
  ]
  node [
    id 1054
    label "pi&#281;tro"
  ]
  node [
    id 1055
    label "klasa"
  ]
  node [
    id 1056
    label "jednostka_geologiczna"
  ]
  node [
    id 1057
    label "filia"
  ]
  node [
    id 1058
    label "malm"
  ]
  node [
    id 1059
    label "whole"
  ]
  node [
    id 1060
    label "dogger"
  ]
  node [
    id 1061
    label "poziom"
  ]
  node [
    id 1062
    label "kurs"
  ]
  node [
    id 1063
    label "bank"
  ]
  node [
    id 1064
    label "formacja"
  ]
  node [
    id 1065
    label "ajencja"
  ]
  node [
    id 1066
    label "wojsko"
  ]
  node [
    id 1067
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 1068
    label "agencja"
  ]
  node [
    id 1069
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 1070
    label "szpital"
  ]
  node [
    id 1071
    label "blend"
  ]
  node [
    id 1072
    label "leksem"
  ]
  node [
    id 1073
    label "figuracja"
  ]
  node [
    id 1074
    label "chwyt"
  ]
  node [
    id 1075
    label "okup"
  ]
  node [
    id 1076
    label "muzykologia"
  ]
  node [
    id 1077
    label "&#347;redniowiecze"
  ]
  node [
    id 1078
    label "czynnik_biotyczny"
  ]
  node [
    id 1079
    label "wyewoluowanie"
  ]
  node [
    id 1080
    label "reakcja"
  ]
  node [
    id 1081
    label "individual"
  ]
  node [
    id 1082
    label "przyswoi&#263;"
  ]
  node [
    id 1083
    label "starzenie_si&#281;"
  ]
  node [
    id 1084
    label "wyewoluowa&#263;"
  ]
  node [
    id 1085
    label "part"
  ]
  node [
    id 1086
    label "przyswojenie"
  ]
  node [
    id 1087
    label "ewoluowanie"
  ]
  node [
    id 1088
    label "ewoluowa&#263;"
  ]
  node [
    id 1089
    label "obiekt"
  ]
  node [
    id 1090
    label "sztuka"
  ]
  node [
    id 1091
    label "agent"
  ]
  node [
    id 1092
    label "przyswaja&#263;"
  ]
  node [
    id 1093
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1094
    label "nicpo&#324;"
  ]
  node [
    id 1095
    label "przyswajanie"
  ]
  node [
    id 1096
    label "feminizm"
  ]
  node [
    id 1097
    label "Unia_Europejska"
  ]
  node [
    id 1098
    label "uatrakcyjni&#263;"
  ]
  node [
    id 1099
    label "przewietrzy&#263;"
  ]
  node [
    id 1100
    label "regenerate"
  ]
  node [
    id 1101
    label "odtworzy&#263;"
  ]
  node [
    id 1102
    label "wymieni&#263;"
  ]
  node [
    id 1103
    label "odbudowa&#263;"
  ]
  node [
    id 1104
    label "odbudowywa&#263;"
  ]
  node [
    id 1105
    label "m&#322;odzi&#263;"
  ]
  node [
    id 1106
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 1107
    label "przewietrza&#263;"
  ]
  node [
    id 1108
    label "wymienia&#263;"
  ]
  node [
    id 1109
    label "odtwarza&#263;"
  ]
  node [
    id 1110
    label "odtwarzanie"
  ]
  node [
    id 1111
    label "uatrakcyjnianie"
  ]
  node [
    id 1112
    label "odbudowywanie"
  ]
  node [
    id 1113
    label "rejuvenation"
  ]
  node [
    id 1114
    label "m&#322;odszy"
  ]
  node [
    id 1115
    label "uatrakcyjnienie"
  ]
  node [
    id 1116
    label "odbudowanie"
  ]
  node [
    id 1117
    label "odtworzenie"
  ]
  node [
    id 1118
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1119
    label "absorption"
  ]
  node [
    id 1120
    label "pobieranie"
  ]
  node [
    id 1121
    label "czerpanie"
  ]
  node [
    id 1122
    label "acquisition"
  ]
  node [
    id 1123
    label "zmienianie"
  ]
  node [
    id 1124
    label "organizm"
  ]
  node [
    id 1125
    label "assimilation"
  ]
  node [
    id 1126
    label "upodabnianie"
  ]
  node [
    id 1127
    label "g&#322;oska"
  ]
  node [
    id 1128
    label "kultura"
  ]
  node [
    id 1129
    label "podobny"
  ]
  node [
    id 1130
    label "fonetyka"
  ]
  node [
    id 1131
    label "mecz_mistrzowski"
  ]
  node [
    id 1132
    label "arrangement"
  ]
  node [
    id 1133
    label "pomoc"
  ]
  node [
    id 1134
    label "rezerwa"
  ]
  node [
    id 1135
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1136
    label "pr&#243;ba"
  ]
  node [
    id 1137
    label "atak"
  ]
  node [
    id 1138
    label "union"
  ]
  node [
    id 1139
    label "assimilate"
  ]
  node [
    id 1140
    label "dostosowywa&#263;"
  ]
  node [
    id 1141
    label "dostosowa&#263;"
  ]
  node [
    id 1142
    label "przejmowa&#263;"
  ]
  node [
    id 1143
    label "upodobni&#263;"
  ]
  node [
    id 1144
    label "przej&#261;&#263;"
  ]
  node [
    id 1145
    label "upodabnia&#263;"
  ]
  node [
    id 1146
    label "pobiera&#263;"
  ]
  node [
    id 1147
    label "pobra&#263;"
  ]
  node [
    id 1148
    label "typ"
  ]
  node [
    id 1149
    label "jednostka_administracyjna"
  ]
  node [
    id 1150
    label "zoologia"
  ]
  node [
    id 1151
    label "skupienie"
  ]
  node [
    id 1152
    label "kr&#243;lestwo"
  ]
  node [
    id 1153
    label "tribe"
  ]
  node [
    id 1154
    label "hurma"
  ]
  node [
    id 1155
    label "botanika"
  ]
  node [
    id 1156
    label "rozmowa"
  ]
  node [
    id 1157
    label "sympozjon"
  ]
  node [
    id 1158
    label "conference"
  ]
  node [
    id 1159
    label "cisza"
  ]
  node [
    id 1160
    label "rozhowor"
  ]
  node [
    id 1161
    label "discussion"
  ]
  node [
    id 1162
    label "esej"
  ]
  node [
    id 1163
    label "sympozjarcha"
  ]
  node [
    id 1164
    label "faza"
  ]
  node [
    id 1165
    label "rozrywka"
  ]
  node [
    id 1166
    label "symposium"
  ]
  node [
    id 1167
    label "przyj&#281;cie"
  ]
  node [
    id 1168
    label "konferencja"
  ]
  node [
    id 1169
    label "dyskusja"
  ]
  node [
    id 1170
    label "wsz&#281;dzie"
  ]
  node [
    id 1171
    label "blisko"
  ]
  node [
    id 1172
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 1173
    label "bliski"
  ]
  node [
    id 1174
    label "silnie"
  ]
  node [
    id 1175
    label "wsz&#281;dy"
  ]
  node [
    id 1176
    label "kompletnie"
  ]
  node [
    id 1177
    label "animatronika"
  ]
  node [
    id 1178
    label "odczulenie"
  ]
  node [
    id 1179
    label "odczula&#263;"
  ]
  node [
    id 1180
    label "blik"
  ]
  node [
    id 1181
    label "odczuli&#263;"
  ]
  node [
    id 1182
    label "scena"
  ]
  node [
    id 1183
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 1184
    label "muza"
  ]
  node [
    id 1185
    label "block"
  ]
  node [
    id 1186
    label "trawiarnia"
  ]
  node [
    id 1187
    label "sklejarka"
  ]
  node [
    id 1188
    label "uj&#281;cie"
  ]
  node [
    id 1189
    label "filmoteka"
  ]
  node [
    id 1190
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1191
    label "klatka"
  ]
  node [
    id 1192
    label "rozbieg&#243;wka"
  ]
  node [
    id 1193
    label "napisy"
  ]
  node [
    id 1194
    label "ta&#347;ma"
  ]
  node [
    id 1195
    label "odczulanie"
  ]
  node [
    id 1196
    label "anamorfoza"
  ]
  node [
    id 1197
    label "dorobek"
  ]
  node [
    id 1198
    label "ty&#322;&#243;wka"
  ]
  node [
    id 1199
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 1200
    label "b&#322;ona"
  ]
  node [
    id 1201
    label "emulsja_fotograficzna"
  ]
  node [
    id 1202
    label "photograph"
  ]
  node [
    id 1203
    label "czo&#322;&#243;wka"
  ]
  node [
    id 1204
    label "rola"
  ]
  node [
    id 1205
    label "&#347;cie&#380;ka"
  ]
  node [
    id 1206
    label "wodorost"
  ]
  node [
    id 1207
    label "webbing"
  ]
  node [
    id 1208
    label "p&#243;&#322;produkt"
  ]
  node [
    id 1209
    label "nagranie"
  ]
  node [
    id 1210
    label "przewija&#263;_si&#281;"
  ]
  node [
    id 1211
    label "kula"
  ]
  node [
    id 1212
    label "pas"
  ]
  node [
    id 1213
    label "watkowce"
  ]
  node [
    id 1214
    label "zielenica"
  ]
  node [
    id 1215
    label "ta&#347;moteka"
  ]
  node [
    id 1216
    label "no&#347;nik_danych"
  ]
  node [
    id 1217
    label "transporter"
  ]
  node [
    id 1218
    label "hutnictwo"
  ]
  node [
    id 1219
    label "klaps"
  ]
  node [
    id 1220
    label "pasek"
  ]
  node [
    id 1221
    label "artyku&#322;"
  ]
  node [
    id 1222
    label "przewijanie_si&#281;"
  ]
  node [
    id 1223
    label "blacha"
  ]
  node [
    id 1224
    label "tkanka"
  ]
  node [
    id 1225
    label "m&#243;zgoczaszka"
  ]
  node [
    id 1226
    label "inspiratorka"
  ]
  node [
    id 1227
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1228
    label "banan"
  ]
  node [
    id 1229
    label "talent"
  ]
  node [
    id 1230
    label "kobieta"
  ]
  node [
    id 1231
    label "Melpomena"
  ]
  node [
    id 1232
    label "natchnienie"
  ]
  node [
    id 1233
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1234
    label "bogini"
  ]
  node [
    id 1235
    label "ro&#347;lina"
  ]
  node [
    id 1236
    label "muzyka"
  ]
  node [
    id 1237
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 1238
    label "palma"
  ]
  node [
    id 1239
    label "pr&#243;bowanie"
  ]
  node [
    id 1240
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1241
    label "didaskalia"
  ]
  node [
    id 1242
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1243
    label "environment"
  ]
  node [
    id 1244
    label "head"
  ]
  node [
    id 1245
    label "scenariusz"
  ]
  node [
    id 1246
    label "fortel"
  ]
  node [
    id 1247
    label "theatrical_performance"
  ]
  node [
    id 1248
    label "ambala&#380;"
  ]
  node [
    id 1249
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1250
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1251
    label "Faust"
  ]
  node [
    id 1252
    label "scenografia"
  ]
  node [
    id 1253
    label "ods&#322;ona"
  ]
  node [
    id 1254
    label "turn"
  ]
  node [
    id 1255
    label "pokaz"
  ]
  node [
    id 1256
    label "przedstawi&#263;"
  ]
  node [
    id 1257
    label "Apollo"
  ]
  node [
    id 1258
    label "przedstawianie"
  ]
  node [
    id 1259
    label "przedstawia&#263;"
  ]
  node [
    id 1260
    label "towar"
  ]
  node [
    id 1261
    label "konto"
  ]
  node [
    id 1262
    label "mienie"
  ]
  node [
    id 1263
    label "wypracowa&#263;"
  ]
  node [
    id 1264
    label "pocz&#261;tek"
  ]
  node [
    id 1265
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 1266
    label "kle&#263;"
  ]
  node [
    id 1267
    label "hodowla"
  ]
  node [
    id 1268
    label "human_body"
  ]
  node [
    id 1269
    label "miejsce"
  ]
  node [
    id 1270
    label "pr&#281;t"
  ]
  node [
    id 1271
    label "kopalnia"
  ]
  node [
    id 1272
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 1273
    label "konstrukcja"
  ]
  node [
    id 1274
    label "ogranicza&#263;"
  ]
  node [
    id 1275
    label "sytuacja"
  ]
  node [
    id 1276
    label "akwarium"
  ]
  node [
    id 1277
    label "d&#378;wig"
  ]
  node [
    id 1278
    label "technika"
  ]
  node [
    id 1279
    label "kinematografia"
  ]
  node [
    id 1280
    label "uprawienie"
  ]
  node [
    id 1281
    label "kszta&#322;t"
  ]
  node [
    id 1282
    label "dialog"
  ]
  node [
    id 1283
    label "p&#322;osa"
  ]
  node [
    id 1284
    label "wykonywanie"
  ]
  node [
    id 1285
    label "plik"
  ]
  node [
    id 1286
    label "ziemia"
  ]
  node [
    id 1287
    label "wykonywa&#263;"
  ]
  node [
    id 1288
    label "ustawienie"
  ]
  node [
    id 1289
    label "pole"
  ]
  node [
    id 1290
    label "gospodarstwo"
  ]
  node [
    id 1291
    label "uprawi&#263;"
  ]
  node [
    id 1292
    label "function"
  ]
  node [
    id 1293
    label "posta&#263;"
  ]
  node [
    id 1294
    label "zreinterpretowa&#263;"
  ]
  node [
    id 1295
    label "zastosowanie"
  ]
  node [
    id 1296
    label "reinterpretowa&#263;"
  ]
  node [
    id 1297
    label "wrench"
  ]
  node [
    id 1298
    label "irygowanie"
  ]
  node [
    id 1299
    label "irygowa&#263;"
  ]
  node [
    id 1300
    label "zreinterpretowanie"
  ]
  node [
    id 1301
    label "cel"
  ]
  node [
    id 1302
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 1303
    label "gra&#263;"
  ]
  node [
    id 1304
    label "aktorstwo"
  ]
  node [
    id 1305
    label "kostium"
  ]
  node [
    id 1306
    label "zagon"
  ]
  node [
    id 1307
    label "znaczenie"
  ]
  node [
    id 1308
    label "reinterpretowanie"
  ]
  node [
    id 1309
    label "sk&#322;ad"
  ]
  node [
    id 1310
    label "tekst"
  ]
  node [
    id 1311
    label "zagranie"
  ]
  node [
    id 1312
    label "radlina"
  ]
  node [
    id 1313
    label "materia&#322;"
  ]
  node [
    id 1314
    label "rz&#261;d"
  ]
  node [
    id 1315
    label "alpinizm"
  ]
  node [
    id 1316
    label "wst&#281;p"
  ]
  node [
    id 1317
    label "bieg"
  ]
  node [
    id 1318
    label "elita"
  ]
  node [
    id 1319
    label "rajd"
  ]
  node [
    id 1320
    label "poligrafia"
  ]
  node [
    id 1321
    label "pododdzia&#322;"
  ]
  node [
    id 1322
    label "latarka_czo&#322;owa"
  ]
  node [
    id 1323
    label "&#347;ciana"
  ]
  node [
    id 1324
    label "zderzenie"
  ]
  node [
    id 1325
    label "front"
  ]
  node [
    id 1326
    label "pochwytanie"
  ]
  node [
    id 1327
    label "wording"
  ]
  node [
    id 1328
    label "wzbudzenie"
  ]
  node [
    id 1329
    label "withdrawal"
  ]
  node [
    id 1330
    label "capture"
  ]
  node [
    id 1331
    label "podniesienie"
  ]
  node [
    id 1332
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1333
    label "zapisanie"
  ]
  node [
    id 1334
    label "prezentacja"
  ]
  node [
    id 1335
    label "rzucenie"
  ]
  node [
    id 1336
    label "zabranie"
  ]
  node [
    id 1337
    label "zaaresztowanie"
  ]
  node [
    id 1338
    label "podwy&#380;szenie"
  ]
  node [
    id 1339
    label "kurtyna"
  ]
  node [
    id 1340
    label "akt"
  ]
  node [
    id 1341
    label "widzownia"
  ]
  node [
    id 1342
    label "sznurownia"
  ]
  node [
    id 1343
    label "dramaturgy"
  ]
  node [
    id 1344
    label "sphere"
  ]
  node [
    id 1345
    label "budka_suflera"
  ]
  node [
    id 1346
    label "epizod"
  ]
  node [
    id 1347
    label "k&#322;&#243;tnia"
  ]
  node [
    id 1348
    label "kiesze&#324;"
  ]
  node [
    id 1349
    label "stadium"
  ]
  node [
    id 1350
    label "podest"
  ]
  node [
    id 1351
    label "horyzont"
  ]
  node [
    id 1352
    label "teren"
  ]
  node [
    id 1353
    label "proscenium"
  ]
  node [
    id 1354
    label "nadscenie"
  ]
  node [
    id 1355
    label "antyteatr"
  ]
  node [
    id 1356
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 1357
    label "fina&#322;"
  ]
  node [
    id 1358
    label "urz&#261;dzenie"
  ]
  node [
    id 1359
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 1360
    label "alergia"
  ]
  node [
    id 1361
    label "leczy&#263;"
  ]
  node [
    id 1362
    label "usuwa&#263;"
  ]
  node [
    id 1363
    label "zmniejsza&#263;"
  ]
  node [
    id 1364
    label "zmniejszenie"
  ]
  node [
    id 1365
    label "wyleczenie"
  ]
  node [
    id 1366
    label "desensitization"
  ]
  node [
    id 1367
    label "farba"
  ]
  node [
    id 1368
    label "odblask"
  ]
  node [
    id 1369
    label "plama"
  ]
  node [
    id 1370
    label "zmniejszanie"
  ]
  node [
    id 1371
    label "usuwanie"
  ]
  node [
    id 1372
    label "terapia"
  ]
  node [
    id 1373
    label "wyleczy&#263;"
  ]
  node [
    id 1374
    label "usun&#261;&#263;"
  ]
  node [
    id 1375
    label "zmniejszy&#263;"
  ]
  node [
    id 1376
    label "proces_biologiczny"
  ]
  node [
    id 1377
    label "zamiana"
  ]
  node [
    id 1378
    label "deformacja"
  ]
  node [
    id 1379
    label "przek&#322;ad"
  ]
  node [
    id 1380
    label "dialogista"
  ]
  node [
    id 1381
    label "archiwum"
  ]
  node [
    id 1382
    label "prosto"
  ]
  node [
    id 1383
    label "otwarcie"
  ]
  node [
    id 1384
    label "naprzeciwko"
  ]
  node [
    id 1385
    label "po_przeciwnej_stronie"
  ]
  node [
    id 1386
    label "z_naprzeciwka"
  ]
  node [
    id 1387
    label "jawno"
  ]
  node [
    id 1388
    label "rozpocz&#281;cie"
  ]
  node [
    id 1389
    label "udost&#281;pnienie"
  ]
  node [
    id 1390
    label "publicznie"
  ]
  node [
    id 1391
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1392
    label "ewidentnie"
  ]
  node [
    id 1393
    label "jawnie"
  ]
  node [
    id 1394
    label "opening"
  ]
  node [
    id 1395
    label "jawny"
  ]
  node [
    id 1396
    label "otwarty"
  ]
  node [
    id 1397
    label "bezpo&#347;rednio"
  ]
  node [
    id 1398
    label "zdecydowanie"
  ]
  node [
    id 1399
    label "&#322;atwo"
  ]
  node [
    id 1400
    label "prosty"
  ]
  node [
    id 1401
    label "skromnie"
  ]
  node [
    id 1402
    label "elementarily"
  ]
  node [
    id 1403
    label "niepozornie"
  ]
  node [
    id 1404
    label "naturalnie"
  ]
  node [
    id 1405
    label "gaworzy&#263;"
  ]
  node [
    id 1406
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1407
    label "rozmawia&#263;"
  ]
  node [
    id 1408
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1409
    label "umie&#263;"
  ]
  node [
    id 1410
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1411
    label "dziama&#263;"
  ]
  node [
    id 1412
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1413
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1414
    label "dysfonia"
  ]
  node [
    id 1415
    label "express"
  ]
  node [
    id 1416
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1417
    label "talk"
  ]
  node [
    id 1418
    label "prawi&#263;"
  ]
  node [
    id 1419
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1420
    label "powiada&#263;"
  ]
  node [
    id 1421
    label "tell"
  ]
  node [
    id 1422
    label "chew_the_fat"
  ]
  node [
    id 1423
    label "say"
  ]
  node [
    id 1424
    label "j&#281;zyk"
  ]
  node [
    id 1425
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1426
    label "informowa&#263;"
  ]
  node [
    id 1427
    label "wydobywa&#263;"
  ]
  node [
    id 1428
    label "okre&#347;la&#263;"
  ]
  node [
    id 1429
    label "decydowa&#263;"
  ]
  node [
    id 1430
    label "signify"
  ]
  node [
    id 1431
    label "style"
  ]
  node [
    id 1432
    label "komunikowa&#263;"
  ]
  node [
    id 1433
    label "inform"
  ]
  node [
    id 1434
    label "znaczy&#263;"
  ]
  node [
    id 1435
    label "give_voice"
  ]
  node [
    id 1436
    label "oznacza&#263;"
  ]
  node [
    id 1437
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 1438
    label "represent"
  ]
  node [
    id 1439
    label "convey"
  ]
  node [
    id 1440
    label "arouse"
  ]
  node [
    id 1441
    label "robi&#263;"
  ]
  node [
    id 1442
    label "determine"
  ]
  node [
    id 1443
    label "reakcja_chemiczna"
  ]
  node [
    id 1444
    label "uwydatnia&#263;"
  ]
  node [
    id 1445
    label "eksploatowa&#263;"
  ]
  node [
    id 1446
    label "uzyskiwa&#263;"
  ]
  node [
    id 1447
    label "wydostawa&#263;"
  ]
  node [
    id 1448
    label "wyjmowa&#263;"
  ]
  node [
    id 1449
    label "train"
  ]
  node [
    id 1450
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 1451
    label "wydawa&#263;"
  ]
  node [
    id 1452
    label "dobywa&#263;"
  ]
  node [
    id 1453
    label "ocala&#263;"
  ]
  node [
    id 1454
    label "excavate"
  ]
  node [
    id 1455
    label "g&#243;rnictwo"
  ]
  node [
    id 1456
    label "raise"
  ]
  node [
    id 1457
    label "wiedzie&#263;"
  ]
  node [
    id 1458
    label "can"
  ]
  node [
    id 1459
    label "m&#243;c"
  ]
  node [
    id 1460
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1461
    label "rozumie&#263;"
  ]
  node [
    id 1462
    label "szczeka&#263;"
  ]
  node [
    id 1463
    label "funkcjonowa&#263;"
  ]
  node [
    id 1464
    label "mawia&#263;"
  ]
  node [
    id 1465
    label "opowiada&#263;"
  ]
  node [
    id 1466
    label "chatter"
  ]
  node [
    id 1467
    label "niemowl&#281;"
  ]
  node [
    id 1468
    label "kosmetyk"
  ]
  node [
    id 1469
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 1470
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1471
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1472
    label "artykulator"
  ]
  node [
    id 1473
    label "kod"
  ]
  node [
    id 1474
    label "kawa&#322;ek"
  ]
  node [
    id 1475
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1476
    label "gramatyka"
  ]
  node [
    id 1477
    label "stylik"
  ]
  node [
    id 1478
    label "przet&#322;umaczenie"
  ]
  node [
    id 1479
    label "formalizowanie"
  ]
  node [
    id 1480
    label "ssa&#263;"
  ]
  node [
    id 1481
    label "ssanie"
  ]
  node [
    id 1482
    label "language"
  ]
  node [
    id 1483
    label "liza&#263;"
  ]
  node [
    id 1484
    label "napisa&#263;"
  ]
  node [
    id 1485
    label "konsonantyzm"
  ]
  node [
    id 1486
    label "wokalizm"
  ]
  node [
    id 1487
    label "pisa&#263;"
  ]
  node [
    id 1488
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1489
    label "jeniec"
  ]
  node [
    id 1490
    label "but"
  ]
  node [
    id 1491
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1492
    label "po_koroniarsku"
  ]
  node [
    id 1493
    label "t&#322;umaczenie"
  ]
  node [
    id 1494
    label "m&#243;wienie"
  ]
  node [
    id 1495
    label "pype&#263;"
  ]
  node [
    id 1496
    label "lizanie"
  ]
  node [
    id 1497
    label "pismo"
  ]
  node [
    id 1498
    label "formalizowa&#263;"
  ]
  node [
    id 1499
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1500
    label "rozumienie"
  ]
  node [
    id 1501
    label "makroglosja"
  ]
  node [
    id 1502
    label "jama_ustna"
  ]
  node [
    id 1503
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1504
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1505
    label "natural_language"
  ]
  node [
    id 1506
    label "s&#322;ownictwo"
  ]
  node [
    id 1507
    label "dysphonia"
  ]
  node [
    id 1508
    label "dysleksja"
  ]
  node [
    id 1509
    label "g&#322;&#243;wny"
  ]
  node [
    id 1510
    label "najwa&#380;niejszy"
  ]
  node [
    id 1511
    label "g&#322;&#243;wnie"
  ]
  node [
    id 1512
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1513
    label "equal"
  ]
  node [
    id 1514
    label "trwa&#263;"
  ]
  node [
    id 1515
    label "chodzi&#263;"
  ]
  node [
    id 1516
    label "obecno&#347;&#263;"
  ]
  node [
    id 1517
    label "stand"
  ]
  node [
    id 1518
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1519
    label "uczestniczy&#263;"
  ]
  node [
    id 1520
    label "participate"
  ]
  node [
    id 1521
    label "istnie&#263;"
  ]
  node [
    id 1522
    label "pozostawa&#263;"
  ]
  node [
    id 1523
    label "zostawa&#263;"
  ]
  node [
    id 1524
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1525
    label "adhere"
  ]
  node [
    id 1526
    label "compass"
  ]
  node [
    id 1527
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1528
    label "mierzy&#263;"
  ]
  node [
    id 1529
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1530
    label "being"
  ]
  node [
    id 1531
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1532
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1533
    label "run"
  ]
  node [
    id 1534
    label "bangla&#263;"
  ]
  node [
    id 1535
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1536
    label "przebiega&#263;"
  ]
  node [
    id 1537
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1538
    label "proceed"
  ]
  node [
    id 1539
    label "carry"
  ]
  node [
    id 1540
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1541
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1542
    label "para"
  ]
  node [
    id 1543
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1544
    label "str&#243;j"
  ]
  node [
    id 1545
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1546
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1547
    label "krok"
  ]
  node [
    id 1548
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1549
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1550
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1551
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1552
    label "continue"
  ]
  node [
    id 1553
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1554
    label "Ohio"
  ]
  node [
    id 1555
    label "wci&#281;cie"
  ]
  node [
    id 1556
    label "Nowy_York"
  ]
  node [
    id 1557
    label "warstwa"
  ]
  node [
    id 1558
    label "samopoczucie"
  ]
  node [
    id 1559
    label "Illinois"
  ]
  node [
    id 1560
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1561
    label "state"
  ]
  node [
    id 1562
    label "Jukatan"
  ]
  node [
    id 1563
    label "Kalifornia"
  ]
  node [
    id 1564
    label "Wirginia"
  ]
  node [
    id 1565
    label "wektor"
  ]
  node [
    id 1566
    label "Goa"
  ]
  node [
    id 1567
    label "Teksas"
  ]
  node [
    id 1568
    label "Waszyngton"
  ]
  node [
    id 1569
    label "Massachusetts"
  ]
  node [
    id 1570
    label "Alaska"
  ]
  node [
    id 1571
    label "Arakan"
  ]
  node [
    id 1572
    label "Hawaje"
  ]
  node [
    id 1573
    label "Maryland"
  ]
  node [
    id 1574
    label "Michigan"
  ]
  node [
    id 1575
    label "Arizona"
  ]
  node [
    id 1576
    label "Georgia"
  ]
  node [
    id 1577
    label "Pensylwania"
  ]
  node [
    id 1578
    label "shape"
  ]
  node [
    id 1579
    label "Luizjana"
  ]
  node [
    id 1580
    label "Nowy_Meksyk"
  ]
  node [
    id 1581
    label "Alabama"
  ]
  node [
    id 1582
    label "Kansas"
  ]
  node [
    id 1583
    label "Oregon"
  ]
  node [
    id 1584
    label "Oklahoma"
  ]
  node [
    id 1585
    label "Floryda"
  ]
  node [
    id 1586
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1587
    label "intensywny"
  ]
  node [
    id 1588
    label "sporo"
  ]
  node [
    id 1589
    label "wynios&#322;y"
  ]
  node [
    id 1590
    label "dono&#347;ny"
  ]
  node [
    id 1591
    label "silny"
  ]
  node [
    id 1592
    label "wa&#380;nie"
  ]
  node [
    id 1593
    label "istotnie"
  ]
  node [
    id 1594
    label "znaczny"
  ]
  node [
    id 1595
    label "eksponowany"
  ]
  node [
    id 1596
    label "znacz&#261;cy"
  ]
  node [
    id 1597
    label "zwarty"
  ]
  node [
    id 1598
    label "efektywny"
  ]
  node [
    id 1599
    label "ogrodnictwo"
  ]
  node [
    id 1600
    label "dynamiczny"
  ]
  node [
    id 1601
    label "intensywnie"
  ]
  node [
    id 1602
    label "nieproporcjonalny"
  ]
  node [
    id 1603
    label "specjalny"
  ]
  node [
    id 1604
    label "problem"
  ]
  node [
    id 1605
    label "subiekcja"
  ]
  node [
    id 1606
    label "sprawa"
  ]
  node [
    id 1607
    label "problemat"
  ]
  node [
    id 1608
    label "jajko_Kolumba"
  ]
  node [
    id 1609
    label "obstruction"
  ]
  node [
    id 1610
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1611
    label "problematyka"
  ]
  node [
    id 1612
    label "trudno&#347;&#263;"
  ]
  node [
    id 1613
    label "pierepa&#322;ka"
  ]
  node [
    id 1614
    label "ambaras"
  ]
  node [
    id 1615
    label "najpewniej"
  ]
  node [
    id 1616
    label "pewny"
  ]
  node [
    id 1617
    label "wiarygodnie"
  ]
  node [
    id 1618
    label "mocno"
  ]
  node [
    id 1619
    label "pewniej"
  ]
  node [
    id 1620
    label "bezpiecznie"
  ]
  node [
    id 1621
    label "zwinnie"
  ]
  node [
    id 1622
    label "bezpieczny"
  ]
  node [
    id 1623
    label "bezpieczno"
  ]
  node [
    id 1624
    label "credibly"
  ]
  node [
    id 1625
    label "wiarygodny"
  ]
  node [
    id 1626
    label "believably"
  ]
  node [
    id 1627
    label "mocny"
  ]
  node [
    id 1628
    label "przekonuj&#261;co"
  ]
  node [
    id 1629
    label "niema&#322;o"
  ]
  node [
    id 1630
    label "powerfully"
  ]
  node [
    id 1631
    label "widocznie"
  ]
  node [
    id 1632
    label "konkretnie"
  ]
  node [
    id 1633
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1634
    label "stabilnie"
  ]
  node [
    id 1635
    label "strongly"
  ]
  node [
    id 1636
    label "mo&#380;liwy"
  ]
  node [
    id 1637
    label "upewnianie_si&#281;"
  ]
  node [
    id 1638
    label "ufanie"
  ]
  node [
    id 1639
    label "wierzenie"
  ]
  node [
    id 1640
    label "upewnienie_si&#281;"
  ]
  node [
    id 1641
    label "zwinny"
  ]
  node [
    id 1642
    label "polotnie"
  ]
  node [
    id 1643
    label "p&#322;ynnie"
  ]
  node [
    id 1644
    label "sprawnie"
  ]
  node [
    id 1645
    label "&#380;egna&#263;"
  ]
  node [
    id 1646
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 1647
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 1648
    label "rozstawa&#263;_si&#281;"
  ]
  node [
    id 1649
    label "pozdrawia&#263;"
  ]
  node [
    id 1650
    label "b&#322;ogos&#322;awi&#263;"
  ]
  node [
    id 1651
    label "bless"
  ]
  node [
    id 1652
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1653
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1654
    label "support"
  ]
  node [
    id 1655
    label "prze&#380;y&#263;"
  ]
  node [
    id 1656
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1657
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 1658
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1659
    label "message"
  ]
  node [
    id 1660
    label "&#347;wiat"
  ]
  node [
    id 1661
    label "dar"
  ]
  node [
    id 1662
    label "real"
  ]
  node [
    id 1663
    label "charakterystyka"
  ]
  node [
    id 1664
    label "m&#322;ot"
  ]
  node [
    id 1665
    label "znak"
  ]
  node [
    id 1666
    label "drzewo"
  ]
  node [
    id 1667
    label "attribute"
  ]
  node [
    id 1668
    label "marka"
  ]
  node [
    id 1669
    label "Stary_&#346;wiat"
  ]
  node [
    id 1670
    label "p&#243;&#322;noc"
  ]
  node [
    id 1671
    label "Wsch&#243;d"
  ]
  node [
    id 1672
    label "class"
  ]
  node [
    id 1673
    label "geosfera"
  ]
  node [
    id 1674
    label "przejmowanie"
  ]
  node [
    id 1675
    label "przyroda"
  ]
  node [
    id 1676
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 1677
    label "po&#322;udnie"
  ]
  node [
    id 1678
    label "makrokosmos"
  ]
  node [
    id 1679
    label "huczek"
  ]
  node [
    id 1680
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1681
    label "morze"
  ]
  node [
    id 1682
    label "rze&#378;ba"
  ]
  node [
    id 1683
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1684
    label "hydrosfera"
  ]
  node [
    id 1685
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 1686
    label "ciemna_materia"
  ]
  node [
    id 1687
    label "ekosystem"
  ]
  node [
    id 1688
    label "biota"
  ]
  node [
    id 1689
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 1690
    label "planeta"
  ]
  node [
    id 1691
    label "geotermia"
  ]
  node [
    id 1692
    label "ekosfera"
  ]
  node [
    id 1693
    label "ozonosfera"
  ]
  node [
    id 1694
    label "wszechstworzenie"
  ]
  node [
    id 1695
    label "woda"
  ]
  node [
    id 1696
    label "kuchnia"
  ]
  node [
    id 1697
    label "biosfera"
  ]
  node [
    id 1698
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1699
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 1700
    label "populace"
  ]
  node [
    id 1701
    label "magnetosfera"
  ]
  node [
    id 1702
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1703
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1704
    label "universe"
  ]
  node [
    id 1705
    label "biegun"
  ]
  node [
    id 1706
    label "litosfera"
  ]
  node [
    id 1707
    label "mikrokosmos"
  ]
  node [
    id 1708
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1709
    label "przestrze&#324;"
  ]
  node [
    id 1710
    label "stw&#243;r"
  ]
  node [
    id 1711
    label "p&#243;&#322;kula"
  ]
  node [
    id 1712
    label "przej&#281;cie"
  ]
  node [
    id 1713
    label "barysfera"
  ]
  node [
    id 1714
    label "obszar"
  ]
  node [
    id 1715
    label "czarna_dziura"
  ]
  node [
    id 1716
    label "atmosfera"
  ]
  node [
    id 1717
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1718
    label "Ziemia"
  ]
  node [
    id 1719
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1720
    label "geoida"
  ]
  node [
    id 1721
    label "zagranica"
  ]
  node [
    id 1722
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1723
    label "fauna"
  ]
  node [
    id 1724
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1725
    label "dyspozycja"
  ]
  node [
    id 1726
    label "da&#324;"
  ]
  node [
    id 1727
    label "faculty"
  ]
  node [
    id 1728
    label "stygmat"
  ]
  node [
    id 1729
    label "dobro"
  ]
  node [
    id 1730
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 1731
    label "centavo"
  ]
  node [
    id 1732
    label "Brazylia"
  ]
  node [
    id 1733
    label "Bogus&#322;awa"
  ]
  node [
    id 1734
    label "Good"
  ]
  node [
    id 1735
    label "Copy"
  ]
  node [
    id 1736
    label "komunikacja"
  ]
  node [
    id 1737
    label "elektroniczny"
  ]
  node [
    id 1738
    label "gazeta"
  ]
  node [
    id 1739
    label "wyborczy"
  ]
  node [
    id 1740
    label "Korwin"
  ]
  node [
    id 1741
    label "Mikke"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 392
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 375
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 360
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 373
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 583
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 688
  ]
  edge [
    source 20
    target 69
  ]
  edge [
    source 20
    target 689
  ]
  edge [
    source 20
    target 690
  ]
  edge [
    source 20
    target 691
  ]
  edge [
    source 20
    target 692
  ]
  edge [
    source 20
    target 693
  ]
  edge [
    source 20
    target 694
  ]
  edge [
    source 20
    target 436
  ]
  edge [
    source 20
    target 695
  ]
  edge [
    source 20
    target 696
  ]
  edge [
    source 20
    target 697
  ]
  edge [
    source 20
    target 698
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 699
  ]
  edge [
    source 20
    target 700
  ]
  edge [
    source 20
    target 701
  ]
  edge [
    source 20
    target 702
  ]
  edge [
    source 20
    target 703
  ]
  edge [
    source 20
    target 704
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 20
    target 705
  ]
  edge [
    source 20
    target 706
  ]
  edge [
    source 20
    target 545
  ]
  edge [
    source 20
    target 707
  ]
  edge [
    source 20
    target 708
  ]
  edge [
    source 20
    target 709
  ]
  edge [
    source 20
    target 710
  ]
  edge [
    source 20
    target 277
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 711
  ]
  edge [
    source 21
    target 712
  ]
  edge [
    source 21
    target 713
  ]
  edge [
    source 21
    target 714
  ]
  edge [
    source 21
    target 715
  ]
  edge [
    source 21
    target 716
  ]
  edge [
    source 21
    target 717
  ]
  edge [
    source 21
    target 718
  ]
  edge [
    source 21
    target 719
  ]
  edge [
    source 21
    target 720
  ]
  edge [
    source 21
    target 721
  ]
  edge [
    source 21
    target 722
  ]
  edge [
    source 21
    target 97
  ]
  edge [
    source 21
    target 723
  ]
  edge [
    source 21
    target 724
  ]
  edge [
    source 21
    target 725
  ]
  edge [
    source 21
    target 726
  ]
  edge [
    source 21
    target 727
  ]
  edge [
    source 21
    target 728
  ]
  edge [
    source 21
    target 729
  ]
  edge [
    source 21
    target 730
  ]
  edge [
    source 21
    target 731
  ]
  edge [
    source 21
    target 732
  ]
  edge [
    source 21
    target 733
  ]
  edge [
    source 21
    target 734
  ]
  edge [
    source 21
    target 516
  ]
  edge [
    source 21
    target 735
  ]
  edge [
    source 21
    target 736
  ]
  edge [
    source 21
    target 737
  ]
  edge [
    source 21
    target 738
  ]
  edge [
    source 21
    target 739
  ]
  edge [
    source 21
    target 740
  ]
  edge [
    source 21
    target 741
  ]
  edge [
    source 21
    target 742
  ]
  edge [
    source 21
    target 743
  ]
  edge [
    source 21
    target 744
  ]
  edge [
    source 21
    target 745
  ]
  edge [
    source 21
    target 746
  ]
  edge [
    source 21
    target 747
  ]
  edge [
    source 21
    target 748
  ]
  edge [
    source 21
    target 749
  ]
  edge [
    source 21
    target 750
  ]
  edge [
    source 21
    target 751
  ]
  edge [
    source 21
    target 93
  ]
  edge [
    source 21
    target 94
  ]
  edge [
    source 21
    target 752
  ]
  edge [
    source 21
    target 753
  ]
  edge [
    source 21
    target 754
  ]
  edge [
    source 21
    target 755
  ]
  edge [
    source 21
    target 95
  ]
  edge [
    source 21
    target 756
  ]
  edge [
    source 21
    target 757
  ]
  edge [
    source 21
    target 758
  ]
  edge [
    source 21
    target 759
  ]
  edge [
    source 21
    target 760
  ]
  edge [
    source 21
    target 761
  ]
  edge [
    source 21
    target 762
  ]
  edge [
    source 21
    target 763
  ]
  edge [
    source 21
    target 764
  ]
  edge [
    source 21
    target 765
  ]
  edge [
    source 21
    target 766
  ]
  edge [
    source 21
    target 767
  ]
  edge [
    source 21
    target 768
  ]
  edge [
    source 21
    target 769
  ]
  edge [
    source 21
    target 770
  ]
  edge [
    source 21
    target 771
  ]
  edge [
    source 21
    target 772
  ]
  edge [
    source 21
    target 773
  ]
  edge [
    source 21
    target 774
  ]
  edge [
    source 21
    target 775
  ]
  edge [
    source 21
    target 776
  ]
  edge [
    source 21
    target 777
  ]
  edge [
    source 21
    target 778
  ]
  edge [
    source 21
    target 779
  ]
  edge [
    source 21
    target 780
  ]
  edge [
    source 21
    target 781
  ]
  edge [
    source 21
    target 782
  ]
  edge [
    source 21
    target 783
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 786
  ]
  edge [
    source 21
    target 787
  ]
  edge [
    source 21
    target 788
  ]
  edge [
    source 21
    target 789
  ]
  edge [
    source 21
    target 790
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 794
  ]
  edge [
    source 21
    target 795
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 797
  ]
  edge [
    source 21
    target 798
  ]
  edge [
    source 21
    target 799
  ]
  edge [
    source 21
    target 800
  ]
  edge [
    source 21
    target 801
  ]
  edge [
    source 21
    target 802
  ]
  edge [
    source 21
    target 803
  ]
  edge [
    source 21
    target 804
  ]
  edge [
    source 21
    target 805
  ]
  edge [
    source 21
    target 806
  ]
  edge [
    source 21
    target 807
  ]
  edge [
    source 21
    target 808
  ]
  edge [
    source 21
    target 809
  ]
  edge [
    source 21
    target 810
  ]
  edge [
    source 21
    target 811
  ]
  edge [
    source 21
    target 812
  ]
  edge [
    source 21
    target 813
  ]
  edge [
    source 21
    target 814
  ]
  edge [
    source 21
    target 815
  ]
  edge [
    source 21
    target 816
  ]
  edge [
    source 21
    target 817
  ]
  edge [
    source 21
    target 818
  ]
  edge [
    source 21
    target 819
  ]
  edge [
    source 21
    target 820
  ]
  edge [
    source 21
    target 821
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 22
    target 823
  ]
  edge [
    source 22
    target 824
  ]
  edge [
    source 22
    target 89
  ]
  edge [
    source 22
    target 825
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 22
    target 827
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 52
  ]
  edge [
    source 22
    target 829
  ]
  edge [
    source 22
    target 830
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 22
    target 375
  ]
  edge [
    source 22
    target 833
  ]
  edge [
    source 22
    target 834
  ]
  edge [
    source 22
    target 835
  ]
  edge [
    source 22
    target 836
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 837
  ]
  edge [
    source 23
    target 838
  ]
  edge [
    source 23
    target 839
  ]
  edge [
    source 23
    target 671
  ]
  edge [
    source 23
    target 840
  ]
  edge [
    source 23
    target 841
  ]
  edge [
    source 23
    target 842
  ]
  edge [
    source 23
    target 678
  ]
  edge [
    source 23
    target 679
  ]
  edge [
    source 23
    target 680
  ]
  edge [
    source 23
    target 681
  ]
  edge [
    source 23
    target 682
  ]
  edge [
    source 23
    target 683
  ]
  edge [
    source 23
    target 684
  ]
  edge [
    source 23
    target 583
  ]
  edge [
    source 23
    target 685
  ]
  edge [
    source 23
    target 843
  ]
  edge [
    source 23
    target 844
  ]
  edge [
    source 23
    target 845
  ]
  edge [
    source 23
    target 846
  ]
  edge [
    source 23
    target 482
  ]
  edge [
    source 23
    target 475
  ]
  edge [
    source 23
    target 847
  ]
  edge [
    source 23
    target 848
  ]
  edge [
    source 23
    target 849
  ]
  edge [
    source 23
    target 850
  ]
  edge [
    source 23
    target 851
  ]
  edge [
    source 23
    target 852
  ]
  edge [
    source 23
    target 461
  ]
  edge [
    source 23
    target 853
  ]
  edge [
    source 23
    target 466
  ]
  edge [
    source 24
    target 854
  ]
  edge [
    source 24
    target 855
  ]
  edge [
    source 24
    target 602
  ]
  edge [
    source 24
    target 856
  ]
  edge [
    source 24
    target 857
  ]
  edge [
    source 24
    target 79
  ]
  edge [
    source 24
    target 64
  ]
  edge [
    source 24
    target 858
  ]
  edge [
    source 24
    target 859
  ]
  edge [
    source 24
    target 860
  ]
  edge [
    source 24
    target 861
  ]
  edge [
    source 24
    target 862
  ]
  edge [
    source 24
    target 863
  ]
  edge [
    source 24
    target 826
  ]
  edge [
    source 24
    target 864
  ]
  edge [
    source 24
    target 382
  ]
  edge [
    source 24
    target 865
  ]
  edge [
    source 24
    target 866
  ]
  edge [
    source 24
    target 867
  ]
  edge [
    source 24
    target 868
  ]
  edge [
    source 24
    target 392
  ]
  edge [
    source 24
    target 115
  ]
  edge [
    source 24
    target 611
  ]
  edge [
    source 24
    target 612
  ]
  edge [
    source 24
    target 613
  ]
  edge [
    source 24
    target 869
  ]
  edge [
    source 24
    target 870
  ]
  edge [
    source 24
    target 544
  ]
  edge [
    source 24
    target 871
  ]
  edge [
    source 24
    target 364
  ]
  edge [
    source 24
    target 53
  ]
  edge [
    source 24
    target 872
  ]
  edge [
    source 24
    target 873
  ]
  edge [
    source 24
    target 874
  ]
  edge [
    source 24
    target 875
  ]
  edge [
    source 24
    target 876
  ]
  edge [
    source 24
    target 877
  ]
  edge [
    source 24
    target 878
  ]
  edge [
    source 24
    target 879
  ]
  edge [
    source 24
    target 880
  ]
  edge [
    source 24
    target 881
  ]
  edge [
    source 24
    target 882
  ]
  edge [
    source 24
    target 883
  ]
  edge [
    source 24
    target 884
  ]
  edge [
    source 24
    target 885
  ]
  edge [
    source 24
    target 886
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 887
  ]
  edge [
    source 24
    target 888
  ]
  edge [
    source 24
    target 889
  ]
  edge [
    source 24
    target 836
  ]
  edge [
    source 24
    target 890
  ]
  edge [
    source 24
    target 96
  ]
  edge [
    source 24
    target 455
  ]
  edge [
    source 24
    target 891
  ]
  edge [
    source 24
    target 892
  ]
  edge [
    source 24
    target 893
  ]
  edge [
    source 24
    target 894
  ]
  edge [
    source 24
    target 895
  ]
  edge [
    source 24
    target 896
  ]
  edge [
    source 24
    target 815
  ]
  edge [
    source 24
    target 897
  ]
  edge [
    source 24
    target 626
  ]
  edge [
    source 24
    target 898
  ]
  edge [
    source 24
    target 52
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 899
  ]
  edge [
    source 25
    target 900
  ]
  edge [
    source 25
    target 901
  ]
  edge [
    source 25
    target 902
  ]
  edge [
    source 25
    target 903
  ]
  edge [
    source 25
    target 904
  ]
  edge [
    source 25
    target 905
  ]
  edge [
    source 25
    target 906
  ]
  edge [
    source 25
    target 907
  ]
  edge [
    source 25
    target 908
  ]
  edge [
    source 25
    target 909
  ]
  edge [
    source 25
    target 910
  ]
  edge [
    source 25
    target 911
  ]
  edge [
    source 25
    target 912
  ]
  edge [
    source 25
    target 913
  ]
  edge [
    source 25
    target 914
  ]
  edge [
    source 25
    target 915
  ]
  edge [
    source 25
    target 916
  ]
  edge [
    source 25
    target 917
  ]
  edge [
    source 25
    target 918
  ]
  edge [
    source 25
    target 919
  ]
  edge [
    source 25
    target 920
  ]
  edge [
    source 25
    target 921
  ]
  edge [
    source 25
    target 922
  ]
  edge [
    source 25
    target 923
  ]
  edge [
    source 25
    target 924
  ]
  edge [
    source 25
    target 569
  ]
  edge [
    source 25
    target 925
  ]
  edge [
    source 25
    target 926
  ]
  edge [
    source 25
    target 927
  ]
  edge [
    source 25
    target 928
  ]
  edge [
    source 25
    target 929
  ]
  edge [
    source 25
    target 930
  ]
  edge [
    source 25
    target 931
  ]
  edge [
    source 25
    target 932
  ]
  edge [
    source 25
    target 933
  ]
  edge [
    source 25
    target 934
  ]
  edge [
    source 25
    target 935
  ]
  edge [
    source 25
    target 936
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 937
  ]
  edge [
    source 25
    target 938
  ]
  edge [
    source 25
    target 939
  ]
  edge [
    source 25
    target 940
  ]
  edge [
    source 25
    target 941
  ]
  edge [
    source 25
    target 942
  ]
  edge [
    source 25
    target 943
  ]
  edge [
    source 25
    target 944
  ]
  edge [
    source 25
    target 945
  ]
  edge [
    source 25
    target 946
  ]
  edge [
    source 25
    target 947
  ]
  edge [
    source 25
    target 948
  ]
  edge [
    source 25
    target 949
  ]
  edge [
    source 25
    target 950
  ]
  edge [
    source 25
    target 951
  ]
  edge [
    source 25
    target 952
  ]
  edge [
    source 25
    target 953
  ]
  edge [
    source 25
    target 954
  ]
  edge [
    source 25
    target 445
  ]
  edge [
    source 25
    target 955
  ]
  edge [
    source 25
    target 956
  ]
  edge [
    source 25
    target 957
  ]
  edge [
    source 25
    target 958
  ]
  edge [
    source 25
    target 959
  ]
  edge [
    source 25
    target 960
  ]
  edge [
    source 25
    target 961
  ]
  edge [
    source 25
    target 962
  ]
  edge [
    source 25
    target 963
  ]
  edge [
    source 25
    target 964
  ]
  edge [
    source 25
    target 965
  ]
  edge [
    source 25
    target 966
  ]
  edge [
    source 25
    target 967
  ]
  edge [
    source 25
    target 453
  ]
  edge [
    source 25
    target 968
  ]
  edge [
    source 25
    target 969
  ]
  edge [
    source 25
    target 970
  ]
  edge [
    source 25
    target 971
  ]
  edge [
    source 25
    target 972
  ]
  edge [
    source 25
    target 973
  ]
  edge [
    source 25
    target 974
  ]
  edge [
    source 25
    target 975
  ]
  edge [
    source 25
    target 976
  ]
  edge [
    source 25
    target 977
  ]
  edge [
    source 25
    target 978
  ]
  edge [
    source 25
    target 979
  ]
  edge [
    source 25
    target 980
  ]
  edge [
    source 25
    target 981
  ]
  edge [
    source 25
    target 982
  ]
  edge [
    source 25
    target 983
  ]
  edge [
    source 25
    target 984
  ]
  edge [
    source 25
    target 985
  ]
  edge [
    source 25
    target 986
  ]
  edge [
    source 25
    target 987
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 988
  ]
  edge [
    source 27
    target 989
  ]
  edge [
    source 27
    target 990
  ]
  edge [
    source 27
    target 991
  ]
  edge [
    source 27
    target 992
  ]
  edge [
    source 27
    target 993
  ]
  edge [
    source 27
    target 994
  ]
  edge [
    source 27
    target 995
  ]
  edge [
    source 27
    target 996
  ]
  edge [
    source 27
    target 997
  ]
  edge [
    source 27
    target 998
  ]
  edge [
    source 27
    target 999
  ]
  edge [
    source 27
    target 1000
  ]
  edge [
    source 27
    target 1001
  ]
  edge [
    source 27
    target 1002
  ]
  edge [
    source 27
    target 1003
  ]
  edge [
    source 27
    target 1004
  ]
  edge [
    source 27
    target 1005
  ]
  edge [
    source 27
    target 1006
  ]
  edge [
    source 27
    target 1007
  ]
  edge [
    source 27
    target 1008
  ]
  edge [
    source 27
    target 1009
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 1010
  ]
  edge [
    source 29
    target 1011
  ]
  edge [
    source 29
    target 1012
  ]
  edge [
    source 29
    target 1013
  ]
  edge [
    source 29
    target 1014
  ]
  edge [
    source 29
    target 510
  ]
  edge [
    source 29
    target 1015
  ]
  edge [
    source 29
    target 1016
  ]
  edge [
    source 29
    target 1017
  ]
  edge [
    source 29
    target 611
  ]
  edge [
    source 29
    target 1018
  ]
  edge [
    source 29
    target 1019
  ]
  edge [
    source 29
    target 1020
  ]
  edge [
    source 29
    target 1021
  ]
  edge [
    source 29
    target 1022
  ]
  edge [
    source 29
    target 1023
  ]
  edge [
    source 29
    target 1024
  ]
  edge [
    source 29
    target 1025
  ]
  edge [
    source 29
    target 1026
  ]
  edge [
    source 29
    target 1027
  ]
  edge [
    source 29
    target 1028
  ]
  edge [
    source 29
    target 1029
  ]
  edge [
    source 29
    target 1030
  ]
  edge [
    source 29
    target 1031
  ]
  edge [
    source 29
    target 1032
  ]
  edge [
    source 29
    target 1033
  ]
  edge [
    source 29
    target 1034
  ]
  edge [
    source 29
    target 1035
  ]
  edge [
    source 29
    target 1036
  ]
  edge [
    source 29
    target 1037
  ]
  edge [
    source 29
    target 1038
  ]
  edge [
    source 29
    target 1039
  ]
  edge [
    source 29
    target 1040
  ]
  edge [
    source 29
    target 1041
  ]
  edge [
    source 29
    target 520
  ]
  edge [
    source 29
    target 104
  ]
  edge [
    source 29
    target 521
  ]
  edge [
    source 29
    target 522
  ]
  edge [
    source 29
    target 523
  ]
  edge [
    source 29
    target 524
  ]
  edge [
    source 29
    target 525
  ]
  edge [
    source 29
    target 526
  ]
  edge [
    source 29
    target 527
  ]
  edge [
    source 29
    target 1042
  ]
  edge [
    source 29
    target 1043
  ]
  edge [
    source 29
    target 1044
  ]
  edge [
    source 29
    target 1045
  ]
  edge [
    source 29
    target 1046
  ]
  edge [
    source 29
    target 1047
  ]
  edge [
    source 29
    target 173
  ]
  edge [
    source 29
    target 109
  ]
  edge [
    source 29
    target 1048
  ]
  edge [
    source 29
    target 1049
  ]
  edge [
    source 29
    target 1050
  ]
  edge [
    source 29
    target 1051
  ]
  edge [
    source 29
    target 59
  ]
  edge [
    source 29
    target 1052
  ]
  edge [
    source 29
    target 406
  ]
  edge [
    source 29
    target 62
  ]
  edge [
    source 29
    target 1053
  ]
  edge [
    source 29
    target 1054
  ]
  edge [
    source 29
    target 1055
  ]
  edge [
    source 29
    target 1056
  ]
  edge [
    source 29
    target 1057
  ]
  edge [
    source 29
    target 1058
  ]
  edge [
    source 29
    target 1059
  ]
  edge [
    source 29
    target 1060
  ]
  edge [
    source 29
    target 1061
  ]
  edge [
    source 29
    target 304
  ]
  edge [
    source 29
    target 1062
  ]
  edge [
    source 29
    target 1063
  ]
  edge [
    source 29
    target 1064
  ]
  edge [
    source 29
    target 1065
  ]
  edge [
    source 29
    target 1066
  ]
  edge [
    source 29
    target 410
  ]
  edge [
    source 29
    target 1067
  ]
  edge [
    source 29
    target 1068
  ]
  edge [
    source 29
    target 1069
  ]
  edge [
    source 29
    target 1070
  ]
  edge [
    source 29
    target 1071
  ]
  edge [
    source 29
    target 375
  ]
  edge [
    source 29
    target 644
  ]
  edge [
    source 29
    target 1072
  ]
  edge [
    source 29
    target 864
  ]
  edge [
    source 29
    target 1073
  ]
  edge [
    source 29
    target 1074
  ]
  edge [
    source 29
    target 1075
  ]
  edge [
    source 29
    target 1076
  ]
  edge [
    source 29
    target 1077
  ]
  edge [
    source 29
    target 1078
  ]
  edge [
    source 29
    target 1079
  ]
  edge [
    source 29
    target 1080
  ]
  edge [
    source 29
    target 1081
  ]
  edge [
    source 29
    target 1082
  ]
  edge [
    source 29
    target 64
  ]
  edge [
    source 29
    target 1083
  ]
  edge [
    source 29
    target 1084
  ]
  edge [
    source 29
    target 170
  ]
  edge [
    source 29
    target 1085
  ]
  edge [
    source 29
    target 1086
  ]
  edge [
    source 29
    target 1087
  ]
  edge [
    source 29
    target 1088
  ]
  edge [
    source 29
    target 1089
  ]
  edge [
    source 29
    target 1090
  ]
  edge [
    source 29
    target 1091
  ]
  edge [
    source 29
    target 1092
  ]
  edge [
    source 29
    target 1093
  ]
  edge [
    source 29
    target 1094
  ]
  edge [
    source 29
    target 1095
  ]
  edge [
    source 29
    target 1096
  ]
  edge [
    source 29
    target 1097
  ]
  edge [
    source 29
    target 1098
  ]
  edge [
    source 29
    target 1099
  ]
  edge [
    source 29
    target 1100
  ]
  edge [
    source 29
    target 1101
  ]
  edge [
    source 29
    target 1102
  ]
  edge [
    source 29
    target 1103
  ]
  edge [
    source 29
    target 1104
  ]
  edge [
    source 29
    target 1105
  ]
  edge [
    source 29
    target 1106
  ]
  edge [
    source 29
    target 1107
  ]
  edge [
    source 29
    target 1108
  ]
  edge [
    source 29
    target 1109
  ]
  edge [
    source 29
    target 1110
  ]
  edge [
    source 29
    target 1111
  ]
  edge [
    source 29
    target 177
  ]
  edge [
    source 29
    target 1112
  ]
  edge [
    source 29
    target 1113
  ]
  edge [
    source 29
    target 1114
  ]
  edge [
    source 29
    target 178
  ]
  edge [
    source 29
    target 1115
  ]
  edge [
    source 29
    target 1116
  ]
  edge [
    source 29
    target 1117
  ]
  edge [
    source 29
    target 1118
  ]
  edge [
    source 29
    target 1119
  ]
  edge [
    source 29
    target 1120
  ]
  edge [
    source 29
    target 1121
  ]
  edge [
    source 29
    target 1122
  ]
  edge [
    source 29
    target 1123
  ]
  edge [
    source 29
    target 135
  ]
  edge [
    source 29
    target 1124
  ]
  edge [
    source 29
    target 1125
  ]
  edge [
    source 29
    target 1126
  ]
  edge [
    source 29
    target 1127
  ]
  edge [
    source 29
    target 1128
  ]
  edge [
    source 29
    target 1129
  ]
  edge [
    source 29
    target 1130
  ]
  edge [
    source 29
    target 1131
  ]
  edge [
    source 29
    target 455
  ]
  edge [
    source 29
    target 1132
  ]
  edge [
    source 29
    target 78
  ]
  edge [
    source 29
    target 1133
  ]
  edge [
    source 29
    target 351
  ]
  edge [
    source 29
    target 1134
  ]
  edge [
    source 29
    target 1135
  ]
  edge [
    source 29
    target 1136
  ]
  edge [
    source 29
    target 1137
  ]
  edge [
    source 29
    target 166
  ]
  edge [
    source 29
    target 1138
  ]
  edge [
    source 29
    target 1139
  ]
  edge [
    source 29
    target 1140
  ]
  edge [
    source 29
    target 1141
  ]
  edge [
    source 29
    target 1142
  ]
  edge [
    source 29
    target 1143
  ]
  edge [
    source 29
    target 1144
  ]
  edge [
    source 29
    target 1145
  ]
  edge [
    source 29
    target 1146
  ]
  edge [
    source 29
    target 1147
  ]
  edge [
    source 29
    target 1148
  ]
  edge [
    source 29
    target 1149
  ]
  edge [
    source 29
    target 1150
  ]
  edge [
    source 29
    target 1151
  ]
  edge [
    source 29
    target 1152
  ]
  edge [
    source 29
    target 1153
  ]
  edge [
    source 29
    target 1154
  ]
  edge [
    source 29
    target 1155
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 29
    target 43
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1733
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1156
  ]
  edge [
    source 34
    target 1157
  ]
  edge [
    source 34
    target 1158
  ]
  edge [
    source 34
    target 1159
  ]
  edge [
    source 34
    target 553
  ]
  edge [
    source 34
    target 1160
  ]
  edge [
    source 34
    target 1161
  ]
  edge [
    source 34
    target 436
  ]
  edge [
    source 34
    target 1162
  ]
  edge [
    source 34
    target 1163
  ]
  edge [
    source 34
    target 611
  ]
  edge [
    source 34
    target 1164
  ]
  edge [
    source 34
    target 1165
  ]
  edge [
    source 34
    target 1166
  ]
  edge [
    source 34
    target 1167
  ]
  edge [
    source 34
    target 970
  ]
  edge [
    source 34
    target 1168
  ]
  edge [
    source 34
    target 1169
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1170
  ]
  edge [
    source 35
    target 1171
  ]
  edge [
    source 35
    target 1172
  ]
  edge [
    source 35
    target 1173
  ]
  edge [
    source 35
    target 575
  ]
  edge [
    source 35
    target 1174
  ]
  edge [
    source 35
    target 1175
  ]
  edge [
    source 35
    target 1176
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1177
  ]
  edge [
    source 36
    target 1178
  ]
  edge [
    source 36
    target 1179
  ]
  edge [
    source 36
    target 1180
  ]
  edge [
    source 36
    target 1181
  ]
  edge [
    source 36
    target 1182
  ]
  edge [
    source 36
    target 1183
  ]
  edge [
    source 36
    target 1184
  ]
  edge [
    source 36
    target 867
  ]
  edge [
    source 36
    target 1185
  ]
  edge [
    source 36
    target 1186
  ]
  edge [
    source 36
    target 1187
  ]
  edge [
    source 36
    target 1090
  ]
  edge [
    source 36
    target 1188
  ]
  edge [
    source 36
    target 1189
  ]
  edge [
    source 36
    target 1190
  ]
  edge [
    source 36
    target 1191
  ]
  edge [
    source 36
    target 1192
  ]
  edge [
    source 36
    target 1193
  ]
  edge [
    source 36
    target 1194
  ]
  edge [
    source 36
    target 1195
  ]
  edge [
    source 36
    target 1196
  ]
  edge [
    source 36
    target 1197
  ]
  edge [
    source 36
    target 1198
  ]
  edge [
    source 36
    target 1199
  ]
  edge [
    source 36
    target 1200
  ]
  edge [
    source 36
    target 1201
  ]
  edge [
    source 36
    target 1202
  ]
  edge [
    source 36
    target 1203
  ]
  edge [
    source 36
    target 1204
  ]
  edge [
    source 36
    target 1205
  ]
  edge [
    source 36
    target 1206
  ]
  edge [
    source 36
    target 1207
  ]
  edge [
    source 36
    target 1208
  ]
  edge [
    source 36
    target 1209
  ]
  edge [
    source 36
    target 1210
  ]
  edge [
    source 36
    target 1211
  ]
  edge [
    source 36
    target 1212
  ]
  edge [
    source 36
    target 1213
  ]
  edge [
    source 36
    target 1214
  ]
  edge [
    source 36
    target 1215
  ]
  edge [
    source 36
    target 1216
  ]
  edge [
    source 36
    target 1217
  ]
  edge [
    source 36
    target 1218
  ]
  edge [
    source 36
    target 1219
  ]
  edge [
    source 36
    target 1220
  ]
  edge [
    source 36
    target 1221
  ]
  edge [
    source 36
    target 1222
  ]
  edge [
    source 36
    target 1223
  ]
  edge [
    source 36
    target 1224
  ]
  edge [
    source 36
    target 1225
  ]
  edge [
    source 36
    target 64
  ]
  edge [
    source 36
    target 1226
  ]
  edge [
    source 36
    target 1227
  ]
  edge [
    source 36
    target 135
  ]
  edge [
    source 36
    target 1228
  ]
  edge [
    source 36
    target 1229
  ]
  edge [
    source 36
    target 1230
  ]
  edge [
    source 36
    target 1231
  ]
  edge [
    source 36
    target 1232
  ]
  edge [
    source 36
    target 1233
  ]
  edge [
    source 36
    target 1234
  ]
  edge [
    source 36
    target 1235
  ]
  edge [
    source 36
    target 1236
  ]
  edge [
    source 36
    target 1237
  ]
  edge [
    source 36
    target 1238
  ]
  edge [
    source 36
    target 1239
  ]
  edge [
    source 36
    target 162
  ]
  edge [
    source 36
    target 1240
  ]
  edge [
    source 36
    target 858
  ]
  edge [
    source 36
    target 1241
  ]
  edge [
    source 36
    target 757
  ]
  edge [
    source 36
    target 1242
  ]
  edge [
    source 36
    target 1243
  ]
  edge [
    source 36
    target 1244
  ]
  edge [
    source 36
    target 1245
  ]
  edge [
    source 36
    target 1016
  ]
  edge [
    source 36
    target 1053
  ]
  edge [
    source 36
    target 970
  ]
  edge [
    source 36
    target 633
  ]
  edge [
    source 36
    target 1246
  ]
  edge [
    source 36
    target 1247
  ]
  edge [
    source 36
    target 1248
  ]
  edge [
    source 36
    target 1249
  ]
  edge [
    source 36
    target 1250
  ]
  edge [
    source 36
    target 1251
  ]
  edge [
    source 36
    target 1252
  ]
  edge [
    source 36
    target 1253
  ]
  edge [
    source 36
    target 1254
  ]
  edge [
    source 36
    target 1255
  ]
  edge [
    source 36
    target 522
  ]
  edge [
    source 36
    target 836
  ]
  edge [
    source 36
    target 1256
  ]
  edge [
    source 36
    target 1257
  ]
  edge [
    source 36
    target 1128
  ]
  edge [
    source 36
    target 1258
  ]
  edge [
    source 36
    target 1259
  ]
  edge [
    source 36
    target 1260
  ]
  edge [
    source 36
    target 1261
  ]
  edge [
    source 36
    target 1262
  ]
  edge [
    source 36
    target 544
  ]
  edge [
    source 36
    target 1263
  ]
  edge [
    source 36
    target 1264
  ]
  edge [
    source 36
    target 1265
  ]
  edge [
    source 36
    target 1266
  ]
  edge [
    source 36
    target 1267
  ]
  edge [
    source 36
    target 1268
  ]
  edge [
    source 36
    target 1269
  ]
  edge [
    source 36
    target 1270
  ]
  edge [
    source 36
    target 1271
  ]
  edge [
    source 36
    target 1272
  ]
  edge [
    source 36
    target 407
  ]
  edge [
    source 36
    target 1273
  ]
  edge [
    source 36
    target 1274
  ]
  edge [
    source 36
    target 1275
  ]
  edge [
    source 36
    target 1276
  ]
  edge [
    source 36
    target 1277
  ]
  edge [
    source 36
    target 1278
  ]
  edge [
    source 36
    target 1279
  ]
  edge [
    source 36
    target 1280
  ]
  edge [
    source 36
    target 1281
  ]
  edge [
    source 36
    target 1282
  ]
  edge [
    source 36
    target 1283
  ]
  edge [
    source 36
    target 1284
  ]
  edge [
    source 36
    target 1285
  ]
  edge [
    source 36
    target 1286
  ]
  edge [
    source 36
    target 1287
  ]
  edge [
    source 36
    target 1288
  ]
  edge [
    source 36
    target 1289
  ]
  edge [
    source 36
    target 1290
  ]
  edge [
    source 36
    target 1291
  ]
  edge [
    source 36
    target 1292
  ]
  edge [
    source 36
    target 1293
  ]
  edge [
    source 36
    target 1294
  ]
  edge [
    source 36
    target 1295
  ]
  edge [
    source 36
    target 1296
  ]
  edge [
    source 36
    target 1297
  ]
  edge [
    source 36
    target 1298
  ]
  edge [
    source 36
    target 294
  ]
  edge [
    source 36
    target 1299
  ]
  edge [
    source 36
    target 1300
  ]
  edge [
    source 36
    target 1301
  ]
  edge [
    source 36
    target 1302
  ]
  edge [
    source 36
    target 1303
  ]
  edge [
    source 36
    target 1304
  ]
  edge [
    source 36
    target 1305
  ]
  edge [
    source 36
    target 1306
  ]
  edge [
    source 36
    target 1307
  ]
  edge [
    source 36
    target 296
  ]
  edge [
    source 36
    target 1308
  ]
  edge [
    source 36
    target 1309
  ]
  edge [
    source 36
    target 1310
  ]
  edge [
    source 36
    target 1311
  ]
  edge [
    source 36
    target 1312
  ]
  edge [
    source 36
    target 759
  ]
  edge [
    source 36
    target 1313
  ]
  edge [
    source 36
    target 1314
  ]
  edge [
    source 36
    target 1315
  ]
  edge [
    source 36
    target 1316
  ]
  edge [
    source 36
    target 1317
  ]
  edge [
    source 36
    target 1318
  ]
  edge [
    source 36
    target 1319
  ]
  edge [
    source 36
    target 1320
  ]
  edge [
    source 36
    target 1321
  ]
  edge [
    source 36
    target 1322
  ]
  edge [
    source 36
    target 1323
  ]
  edge [
    source 36
    target 1324
  ]
  edge [
    source 36
    target 1325
  ]
  edge [
    source 36
    target 1326
  ]
  edge [
    source 36
    target 1327
  ]
  edge [
    source 36
    target 1328
  ]
  edge [
    source 36
    target 1329
  ]
  edge [
    source 36
    target 1330
  ]
  edge [
    source 36
    target 1331
  ]
  edge [
    source 36
    target 1332
  ]
  edge [
    source 36
    target 1333
  ]
  edge [
    source 36
    target 1334
  ]
  edge [
    source 36
    target 1335
  ]
  edge [
    source 36
    target 760
  ]
  edge [
    source 36
    target 1336
  ]
  edge [
    source 36
    target 96
  ]
  edge [
    source 36
    target 1337
  ]
  edge [
    source 36
    target 84
  ]
  edge [
    source 36
    target 252
  ]
  edge [
    source 36
    target 1338
  ]
  edge [
    source 36
    target 1339
  ]
  edge [
    source 36
    target 1340
  ]
  edge [
    source 36
    target 1341
  ]
  edge [
    source 36
    target 1342
  ]
  edge [
    source 36
    target 1343
  ]
  edge [
    source 36
    target 1344
  ]
  edge [
    source 36
    target 792
  ]
  edge [
    source 36
    target 1345
  ]
  edge [
    source 36
    target 1346
  ]
  edge [
    source 36
    target 69
  ]
  edge [
    source 36
    target 897
  ]
  edge [
    source 36
    target 1347
  ]
  edge [
    source 36
    target 1348
  ]
  edge [
    source 36
    target 1349
  ]
  edge [
    source 36
    target 1350
  ]
  edge [
    source 36
    target 1351
  ]
  edge [
    source 36
    target 1352
  ]
  edge [
    source 36
    target 1353
  ]
  edge [
    source 36
    target 1354
  ]
  edge [
    source 36
    target 1355
  ]
  edge [
    source 36
    target 1356
  ]
  edge [
    source 36
    target 1357
  ]
  edge [
    source 36
    target 1358
  ]
  edge [
    source 36
    target 1359
  ]
  edge [
    source 36
    target 1360
  ]
  edge [
    source 36
    target 1361
  ]
  edge [
    source 36
    target 1362
  ]
  edge [
    source 36
    target 1363
  ]
  edge [
    source 36
    target 754
  ]
  edge [
    source 36
    target 1364
  ]
  edge [
    source 36
    target 1365
  ]
  edge [
    source 36
    target 1366
  ]
  edge [
    source 36
    target 1367
  ]
  edge [
    source 36
    target 1368
  ]
  edge [
    source 36
    target 1369
  ]
  edge [
    source 36
    target 1370
  ]
  edge [
    source 36
    target 1371
  ]
  edge [
    source 36
    target 1372
  ]
  edge [
    source 36
    target 1373
  ]
  edge [
    source 36
    target 1374
  ]
  edge [
    source 36
    target 1375
  ]
  edge [
    source 36
    target 1376
  ]
  edge [
    source 36
    target 1377
  ]
  edge [
    source 36
    target 1378
  ]
  edge [
    source 36
    target 1379
  ]
  edge [
    source 36
    target 1380
  ]
  edge [
    source 36
    target 1164
  ]
  edge [
    source 36
    target 1381
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 1734
  ]
  edge [
    source 39
    target 1735
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1382
  ]
  edge [
    source 40
    target 1383
  ]
  edge [
    source 40
    target 1384
  ]
  edge [
    source 40
    target 1385
  ]
  edge [
    source 40
    target 1386
  ]
  edge [
    source 40
    target 1387
  ]
  edge [
    source 40
    target 1388
  ]
  edge [
    source 40
    target 1389
  ]
  edge [
    source 40
    target 1390
  ]
  edge [
    source 40
    target 1391
  ]
  edge [
    source 40
    target 1392
  ]
  edge [
    source 40
    target 1393
  ]
  edge [
    source 40
    target 1394
  ]
  edge [
    source 40
    target 1395
  ]
  edge [
    source 40
    target 1396
  ]
  edge [
    source 40
    target 1303
  ]
  edge [
    source 40
    target 436
  ]
  edge [
    source 40
    target 1397
  ]
  edge [
    source 40
    target 1398
  ]
  edge [
    source 40
    target 759
  ]
  edge [
    source 40
    target 1399
  ]
  edge [
    source 40
    target 1400
  ]
  edge [
    source 40
    target 1401
  ]
  edge [
    source 40
    target 1402
  ]
  edge [
    source 40
    target 1403
  ]
  edge [
    source 40
    target 1404
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1405
  ]
  edge [
    source 41
    target 1406
  ]
  edge [
    source 41
    target 887
  ]
  edge [
    source 41
    target 1407
  ]
  edge [
    source 41
    target 1408
  ]
  edge [
    source 41
    target 1409
  ]
  edge [
    source 41
    target 1410
  ]
  edge [
    source 41
    target 1411
  ]
  edge [
    source 41
    target 1412
  ]
  edge [
    source 41
    target 1413
  ]
  edge [
    source 41
    target 1414
  ]
  edge [
    source 41
    target 1415
  ]
  edge [
    source 41
    target 1416
  ]
  edge [
    source 41
    target 1417
  ]
  edge [
    source 41
    target 563
  ]
  edge [
    source 41
    target 1418
  ]
  edge [
    source 41
    target 1419
  ]
  edge [
    source 41
    target 1420
  ]
  edge [
    source 41
    target 1421
  ]
  edge [
    source 41
    target 1422
  ]
  edge [
    source 41
    target 1423
  ]
  edge [
    source 41
    target 1424
  ]
  edge [
    source 41
    target 1425
  ]
  edge [
    source 41
    target 1426
  ]
  edge [
    source 41
    target 1427
  ]
  edge [
    source 41
    target 1428
  ]
  edge [
    source 41
    target 564
  ]
  edge [
    source 41
    target 565
  ]
  edge [
    source 41
    target 566
  ]
  edge [
    source 41
    target 567
  ]
  edge [
    source 41
    target 568
  ]
  edge [
    source 41
    target 569
  ]
  edge [
    source 41
    target 1429
  ]
  edge [
    source 41
    target 1430
  ]
  edge [
    source 41
    target 1431
  ]
  edge [
    source 41
    target 921
  ]
  edge [
    source 41
    target 1432
  ]
  edge [
    source 41
    target 1433
  ]
  edge [
    source 41
    target 1434
  ]
  edge [
    source 41
    target 1435
  ]
  edge [
    source 41
    target 1436
  ]
  edge [
    source 41
    target 1437
  ]
  edge [
    source 41
    target 1438
  ]
  edge [
    source 41
    target 1439
  ]
  edge [
    source 41
    target 1440
  ]
  edge [
    source 41
    target 1441
  ]
  edge [
    source 41
    target 1442
  ]
  edge [
    source 41
    target 164
  ]
  edge [
    source 41
    target 1443
  ]
  edge [
    source 41
    target 1444
  ]
  edge [
    source 41
    target 1445
  ]
  edge [
    source 41
    target 1446
  ]
  edge [
    source 41
    target 1447
  ]
  edge [
    source 41
    target 1448
  ]
  edge [
    source 41
    target 1449
  ]
  edge [
    source 41
    target 1450
  ]
  edge [
    source 41
    target 1451
  ]
  edge [
    source 41
    target 1452
  ]
  edge [
    source 41
    target 1453
  ]
  edge [
    source 41
    target 1454
  ]
  edge [
    source 41
    target 1455
  ]
  edge [
    source 41
    target 1456
  ]
  edge [
    source 41
    target 1457
  ]
  edge [
    source 41
    target 1458
  ]
  edge [
    source 41
    target 1459
  ]
  edge [
    source 41
    target 1460
  ]
  edge [
    source 41
    target 1461
  ]
  edge [
    source 41
    target 1462
  ]
  edge [
    source 41
    target 1463
  ]
  edge [
    source 41
    target 1464
  ]
  edge [
    source 41
    target 1465
  ]
  edge [
    source 41
    target 1466
  ]
  edge [
    source 41
    target 1467
  ]
  edge [
    source 41
    target 1468
  ]
  edge [
    source 41
    target 1469
  ]
  edge [
    source 41
    target 1470
  ]
  edge [
    source 41
    target 1471
  ]
  edge [
    source 41
    target 1472
  ]
  edge [
    source 41
    target 1473
  ]
  edge [
    source 41
    target 1474
  ]
  edge [
    source 41
    target 162
  ]
  edge [
    source 41
    target 1475
  ]
  edge [
    source 41
    target 1476
  ]
  edge [
    source 41
    target 1477
  ]
  edge [
    source 41
    target 1478
  ]
  edge [
    source 41
    target 1479
  ]
  edge [
    source 41
    target 1480
  ]
  edge [
    source 41
    target 1481
  ]
  edge [
    source 41
    target 1482
  ]
  edge [
    source 41
    target 1483
  ]
  edge [
    source 41
    target 1484
  ]
  edge [
    source 41
    target 1485
  ]
  edge [
    source 41
    target 1486
  ]
  edge [
    source 41
    target 1487
  ]
  edge [
    source 41
    target 1130
  ]
  edge [
    source 41
    target 1488
  ]
  edge [
    source 41
    target 1489
  ]
  edge [
    source 41
    target 1490
  ]
  edge [
    source 41
    target 1491
  ]
  edge [
    source 41
    target 1492
  ]
  edge [
    source 41
    target 633
  ]
  edge [
    source 41
    target 1493
  ]
  edge [
    source 41
    target 1494
  ]
  edge [
    source 41
    target 1495
  ]
  edge [
    source 41
    target 1496
  ]
  edge [
    source 41
    target 1497
  ]
  edge [
    source 41
    target 1498
  ]
  edge [
    source 41
    target 411
  ]
  edge [
    source 41
    target 1499
  ]
  edge [
    source 41
    target 1500
  ]
  edge [
    source 41
    target 602
  ]
  edge [
    source 41
    target 1501
  ]
  edge [
    source 41
    target 1502
  ]
  edge [
    source 41
    target 1503
  ]
  edge [
    source 41
    target 1034
  ]
  edge [
    source 41
    target 1504
  ]
  edge [
    source 41
    target 1505
  ]
  edge [
    source 41
    target 1506
  ]
  edge [
    source 41
    target 1358
  ]
  edge [
    source 41
    target 1507
  ]
  edge [
    source 41
    target 1508
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1509
  ]
  edge [
    source 42
    target 1510
  ]
  edge [
    source 42
    target 1511
  ]
  edge [
    source 43
    target 104
  ]
  edge [
    source 43
    target 105
  ]
  edge [
    source 43
    target 106
  ]
  edge [
    source 43
    target 107
  ]
  edge [
    source 43
    target 108
  ]
  edge [
    source 43
    target 109
  ]
  edge [
    source 43
    target 110
  ]
  edge [
    source 43
    target 101
  ]
  edge [
    source 43
    target 111
  ]
  edge [
    source 43
    target 112
  ]
  edge [
    source 43
    target 113
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 1512
  ]
  edge [
    source 44
    target 928
  ]
  edge [
    source 44
    target 1513
  ]
  edge [
    source 44
    target 1514
  ]
  edge [
    source 44
    target 1515
  ]
  edge [
    source 44
    target 965
  ]
  edge [
    source 44
    target 516
  ]
  edge [
    source 44
    target 1516
  ]
  edge [
    source 44
    target 1517
  ]
  edge [
    source 44
    target 1518
  ]
  edge [
    source 44
    target 1519
  ]
  edge [
    source 44
    target 1520
  ]
  edge [
    source 44
    target 1441
  ]
  edge [
    source 44
    target 1521
  ]
  edge [
    source 44
    target 1522
  ]
  edge [
    source 44
    target 1523
  ]
  edge [
    source 44
    target 1524
  ]
  edge [
    source 44
    target 1525
  ]
  edge [
    source 44
    target 1526
  ]
  edge [
    source 44
    target 564
  ]
  edge [
    source 44
    target 308
  ]
  edge [
    source 44
    target 967
  ]
  edge [
    source 44
    target 906
  ]
  edge [
    source 44
    target 219
  ]
  edge [
    source 44
    target 1527
  ]
  edge [
    source 44
    target 1528
  ]
  edge [
    source 44
    target 563
  ]
  edge [
    source 44
    target 966
  ]
  edge [
    source 44
    target 1529
  ]
  edge [
    source 44
    target 453
  ]
  edge [
    source 44
    target 1530
  ]
  edge [
    source 44
    target 944
  ]
  edge [
    source 44
    target 101
  ]
  edge [
    source 44
    target 1531
  ]
  edge [
    source 44
    target 929
  ]
  edge [
    source 44
    target 1532
  ]
  edge [
    source 44
    target 1533
  ]
  edge [
    source 44
    target 1534
  ]
  edge [
    source 44
    target 1535
  ]
  edge [
    source 44
    target 1536
  ]
  edge [
    source 44
    target 1537
  ]
  edge [
    source 44
    target 1538
  ]
  edge [
    source 44
    target 949
  ]
  edge [
    source 44
    target 1539
  ]
  edge [
    source 44
    target 51
  ]
  edge [
    source 44
    target 1411
  ]
  edge [
    source 44
    target 1540
  ]
  edge [
    source 44
    target 1541
  ]
  edge [
    source 44
    target 1542
  ]
  edge [
    source 44
    target 1543
  ]
  edge [
    source 44
    target 1544
  ]
  edge [
    source 44
    target 1545
  ]
  edge [
    source 44
    target 1546
  ]
  edge [
    source 44
    target 1547
  ]
  edge [
    source 44
    target 612
  ]
  edge [
    source 44
    target 1548
  ]
  edge [
    source 44
    target 1549
  ]
  edge [
    source 44
    target 1550
  ]
  edge [
    source 44
    target 903
  ]
  edge [
    source 44
    target 1551
  ]
  edge [
    source 44
    target 1552
  ]
  edge [
    source 44
    target 1553
  ]
  edge [
    source 44
    target 1554
  ]
  edge [
    source 44
    target 1555
  ]
  edge [
    source 44
    target 1556
  ]
  edge [
    source 44
    target 1557
  ]
  edge [
    source 44
    target 1558
  ]
  edge [
    source 44
    target 1559
  ]
  edge [
    source 44
    target 1560
  ]
  edge [
    source 44
    target 1561
  ]
  edge [
    source 44
    target 1562
  ]
  edge [
    source 44
    target 1563
  ]
  edge [
    source 44
    target 1564
  ]
  edge [
    source 44
    target 1565
  ]
  edge [
    source 44
    target 1566
  ]
  edge [
    source 44
    target 1567
  ]
  edge [
    source 44
    target 1568
  ]
  edge [
    source 44
    target 1269
  ]
  edge [
    source 44
    target 1569
  ]
  edge [
    source 44
    target 1570
  ]
  edge [
    source 44
    target 1571
  ]
  edge [
    source 44
    target 1572
  ]
  edge [
    source 44
    target 1573
  ]
  edge [
    source 44
    target 724
  ]
  edge [
    source 44
    target 1574
  ]
  edge [
    source 44
    target 1575
  ]
  edge [
    source 44
    target 892
  ]
  edge [
    source 44
    target 1576
  ]
  edge [
    source 44
    target 1061
  ]
  edge [
    source 44
    target 1577
  ]
  edge [
    source 44
    target 1578
  ]
  edge [
    source 44
    target 1579
  ]
  edge [
    source 44
    target 1580
  ]
  edge [
    source 44
    target 1581
  ]
  edge [
    source 44
    target 522
  ]
  edge [
    source 44
    target 1582
  ]
  edge [
    source 44
    target 1583
  ]
  edge [
    source 44
    target 1584
  ]
  edge [
    source 44
    target 1585
  ]
  edge [
    source 44
    target 1149
  ]
  edge [
    source 44
    target 1586
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1587
  ]
  edge [
    source 46
    target 1588
  ]
  edge [
    source 46
    target 595
  ]
  edge [
    source 46
    target 1589
  ]
  edge [
    source 46
    target 1590
  ]
  edge [
    source 46
    target 1591
  ]
  edge [
    source 46
    target 1592
  ]
  edge [
    source 46
    target 1593
  ]
  edge [
    source 46
    target 1594
  ]
  edge [
    source 46
    target 1595
  ]
  edge [
    source 46
    target 467
  ]
  edge [
    source 46
    target 999
  ]
  edge [
    source 46
    target 1596
  ]
  edge [
    source 46
    target 1597
  ]
  edge [
    source 46
    target 1598
  ]
  edge [
    source 46
    target 1599
  ]
  edge [
    source 46
    target 1600
  ]
  edge [
    source 46
    target 526
  ]
  edge [
    source 46
    target 1601
  ]
  edge [
    source 46
    target 1602
  ]
  edge [
    source 46
    target 1603
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 1604
  ]
  edge [
    source 47
    target 1605
  ]
  edge [
    source 47
    target 1606
  ]
  edge [
    source 47
    target 1607
  ]
  edge [
    source 47
    target 1608
  ]
  edge [
    source 47
    target 1609
  ]
  edge [
    source 47
    target 1610
  ]
  edge [
    source 47
    target 1611
  ]
  edge [
    source 47
    target 1612
  ]
  edge [
    source 47
    target 1613
  ]
  edge [
    source 47
    target 1614
  ]
  edge [
    source 48
    target 1615
  ]
  edge [
    source 48
    target 1616
  ]
  edge [
    source 48
    target 1617
  ]
  edge [
    source 48
    target 1618
  ]
  edge [
    source 48
    target 1619
  ]
  edge [
    source 48
    target 1620
  ]
  edge [
    source 48
    target 1621
  ]
  edge [
    source 48
    target 1622
  ]
  edge [
    source 48
    target 1399
  ]
  edge [
    source 48
    target 1623
  ]
  edge [
    source 48
    target 1624
  ]
  edge [
    source 48
    target 1625
  ]
  edge [
    source 48
    target 1626
  ]
  edge [
    source 48
    target 1587
  ]
  edge [
    source 48
    target 1627
  ]
  edge [
    source 48
    target 1591
  ]
  edge [
    source 48
    target 1628
  ]
  edge [
    source 48
    target 1629
  ]
  edge [
    source 48
    target 1630
  ]
  edge [
    source 48
    target 1631
  ]
  edge [
    source 48
    target 581
  ]
  edge [
    source 48
    target 1632
  ]
  edge [
    source 48
    target 1633
  ]
  edge [
    source 48
    target 1634
  ]
  edge [
    source 48
    target 1174
  ]
  edge [
    source 48
    target 1398
  ]
  edge [
    source 48
    target 1635
  ]
  edge [
    source 48
    target 1636
  ]
  edge [
    source 48
    target 492
  ]
  edge [
    source 48
    target 1637
  ]
  edge [
    source 48
    target 1638
  ]
  edge [
    source 48
    target 1639
  ]
  edge [
    source 48
    target 1640
  ]
  edge [
    source 48
    target 1641
  ]
  edge [
    source 48
    target 1642
  ]
  edge [
    source 48
    target 1643
  ]
  edge [
    source 48
    target 1644
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 1645
  ]
  edge [
    source 51
    target 300
  ]
  edge [
    source 51
    target 1646
  ]
  edge [
    source 51
    target 1647
  ]
  edge [
    source 51
    target 1538
  ]
  edge [
    source 51
    target 1648
  ]
  edge [
    source 51
    target 1649
  ]
  edge [
    source 51
    target 1650
  ]
  edge [
    source 51
    target 1651
  ]
  edge [
    source 51
    target 1652
  ]
  edge [
    source 51
    target 1653
  ]
  edge [
    source 51
    target 217
  ]
  edge [
    source 51
    target 1654
  ]
  edge [
    source 51
    target 1655
  ]
  edge [
    source 51
    target 1656
  ]
  edge [
    source 51
    target 1657
  ]
  edge [
    source 52
    target 1658
  ]
  edge [
    source 52
    target 857
  ]
  edge [
    source 52
    target 1659
  ]
  edge [
    source 52
    target 1660
  ]
  edge [
    source 52
    target 1661
  ]
  edge [
    source 52
    target 89
  ]
  edge [
    source 52
    target 101
  ]
  edge [
    source 52
    target 1662
  ]
  edge [
    source 52
    target 1663
  ]
  edge [
    source 52
    target 1664
  ]
  edge [
    source 52
    target 1665
  ]
  edge [
    source 52
    target 1666
  ]
  edge [
    source 52
    target 1136
  ]
  edge [
    source 52
    target 1667
  ]
  edge [
    source 52
    target 1668
  ]
  edge [
    source 52
    target 1669
  ]
  edge [
    source 52
    target 1118
  ]
  edge [
    source 52
    target 1670
  ]
  edge [
    source 52
    target 162
  ]
  edge [
    source 52
    target 1671
  ]
  edge [
    source 52
    target 1672
  ]
  edge [
    source 52
    target 1673
  ]
  edge [
    source 52
    target 118
  ]
  edge [
    source 52
    target 1674
  ]
  edge [
    source 52
    target 1675
  ]
  edge [
    source 52
    target 1676
  ]
  edge [
    source 52
    target 1677
  ]
  edge [
    source 52
    target 946
  ]
  edge [
    source 52
    target 1678
  ]
  edge [
    source 52
    target 1679
  ]
  edge [
    source 52
    target 1680
  ]
  edge [
    source 52
    target 510
  ]
  edge [
    source 52
    target 1243
  ]
  edge [
    source 52
    target 1681
  ]
  edge [
    source 52
    target 1682
  ]
  edge [
    source 52
    target 1683
  ]
  edge [
    source 52
    target 1142
  ]
  edge [
    source 52
    target 1684
  ]
  edge [
    source 52
    target 1685
  ]
  edge [
    source 52
    target 1686
  ]
  edge [
    source 52
    target 1687
  ]
  edge [
    source 52
    target 1688
  ]
  edge [
    source 52
    target 1689
  ]
  edge [
    source 52
    target 1690
  ]
  edge [
    source 52
    target 1691
  ]
  edge [
    source 52
    target 1692
  ]
  edge [
    source 52
    target 1693
  ]
  edge [
    source 52
    target 1694
  ]
  edge [
    source 52
    target 1695
  ]
  edge [
    source 52
    target 1696
  ]
  edge [
    source 52
    target 1697
  ]
  edge [
    source 52
    target 1698
  ]
  edge [
    source 52
    target 1699
  ]
  edge [
    source 52
    target 1700
  ]
  edge [
    source 52
    target 1701
  ]
  edge [
    source 52
    target 1702
  ]
  edge [
    source 52
    target 1703
  ]
  edge [
    source 52
    target 1704
  ]
  edge [
    source 52
    target 1705
  ]
  edge [
    source 52
    target 1706
  ]
  edge [
    source 52
    target 1352
  ]
  edge [
    source 52
    target 1707
  ]
  edge [
    source 52
    target 1708
  ]
  edge [
    source 52
    target 1709
  ]
  edge [
    source 52
    target 1710
  ]
  edge [
    source 52
    target 1711
  ]
  edge [
    source 52
    target 1712
  ]
  edge [
    source 52
    target 1713
  ]
  edge [
    source 52
    target 1714
  ]
  edge [
    source 52
    target 1715
  ]
  edge [
    source 52
    target 1716
  ]
  edge [
    source 52
    target 1144
  ]
  edge [
    source 52
    target 1717
  ]
  edge [
    source 52
    target 1718
  ]
  edge [
    source 52
    target 1719
  ]
  edge [
    source 52
    target 1720
  ]
  edge [
    source 52
    target 1721
  ]
  edge [
    source 52
    target 1722
  ]
  edge [
    source 52
    target 1723
  ]
  edge [
    source 52
    target 1724
  ]
  edge [
    source 52
    target 455
  ]
  edge [
    source 52
    target 891
  ]
  edge [
    source 52
    target 892
  ]
  edge [
    source 52
    target 893
  ]
  edge [
    source 52
    target 894
  ]
  edge [
    source 52
    target 895
  ]
  edge [
    source 52
    target 896
  ]
  edge [
    source 52
    target 815
  ]
  edge [
    source 52
    target 897
  ]
  edge [
    source 52
    target 1725
  ]
  edge [
    source 52
    target 1726
  ]
  edge [
    source 52
    target 1727
  ]
  edge [
    source 52
    target 1728
  ]
  edge [
    source 52
    target 1729
  ]
  edge [
    source 52
    target 1730
  ]
  edge [
    source 52
    target 826
  ]
  edge [
    source 52
    target 827
  ]
  edge [
    source 52
    target 828
  ]
  edge [
    source 52
    target 825
  ]
  edge [
    source 52
    target 829
  ]
  edge [
    source 52
    target 830
  ]
  edge [
    source 52
    target 694
  ]
  edge [
    source 52
    target 831
  ]
  edge [
    source 52
    target 148
  ]
  edge [
    source 52
    target 1731
  ]
  edge [
    source 52
    target 166
  ]
  edge [
    source 52
    target 1732
  ]
  edge [
    source 68
    target 1736
  ]
  edge [
    source 68
    target 1737
  ]
  edge [
    source 1734
    target 1735
  ]
  edge [
    source 1735
    target 1735
  ]
  edge [
    source 1736
    target 1737
  ]
  edge [
    source 1738
    target 1739
  ]
  edge [
    source 1740
    target 1741
  ]
]
