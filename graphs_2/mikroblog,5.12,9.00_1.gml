graph [
  node [
    id 0
    label "laserowy"
    origin "text"
  ]
  node [
    id 1
    label "korekcja"
    origin "text"
  ]
  node [
    id 2
    label "wzrok"
    origin "text"
  ]
  node [
    id 3
    label "opinia"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 6
    label "laserowakorekcjawzroku"
    origin "text"
  ]
  node [
    id 7
    label "okokiedys"
    origin "text"
  ]
  node [
    id 8
    label "laserowo"
  ]
  node [
    id 9
    label "&#347;wietlny"
  ]
  node [
    id 10
    label "&#347;wietlnie"
  ]
  node [
    id 11
    label "poprawa"
  ]
  node [
    id 12
    label "correction"
  ]
  node [
    id 13
    label "alteration"
  ]
  node [
    id 14
    label "sprawdzian"
  ]
  node [
    id 15
    label "ulepszenie"
  ]
  node [
    id 16
    label "zmiana"
  ]
  node [
    id 17
    label "m&#281;tnienie"
  ]
  node [
    id 18
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 19
    label "widzenie"
  ]
  node [
    id 20
    label "okulista"
  ]
  node [
    id 21
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 22
    label "zmys&#322;"
  ]
  node [
    id 23
    label "expression"
  ]
  node [
    id 24
    label "widzie&#263;"
  ]
  node [
    id 25
    label "oko"
  ]
  node [
    id 26
    label "m&#281;tnie&#263;"
  ]
  node [
    id 27
    label "kontakt"
  ]
  node [
    id 28
    label "doznanie"
  ]
  node [
    id 29
    label "flare"
  ]
  node [
    id 30
    label "synestezja"
  ]
  node [
    id 31
    label "wdarcie_si&#281;"
  ]
  node [
    id 32
    label "wdzieranie_si&#281;"
  ]
  node [
    id 33
    label "zdolno&#347;&#263;"
  ]
  node [
    id 34
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 35
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 36
    label "communication"
  ]
  node [
    id 37
    label "styk"
  ]
  node [
    id 38
    label "wydarzenie"
  ]
  node [
    id 39
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 40
    label "association"
  ]
  node [
    id 41
    label "&#322;&#261;cznik"
  ]
  node [
    id 42
    label "katalizator"
  ]
  node [
    id 43
    label "socket"
  ]
  node [
    id 44
    label "instalacja_elektryczna"
  ]
  node [
    id 45
    label "soczewka"
  ]
  node [
    id 46
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 47
    label "formacja_geologiczna"
  ]
  node [
    id 48
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 49
    label "linkage"
  ]
  node [
    id 50
    label "regulator"
  ]
  node [
    id 51
    label "z&#322;&#261;czenie"
  ]
  node [
    id 52
    label "zwi&#261;zek"
  ]
  node [
    id 53
    label "contact"
  ]
  node [
    id 54
    label "aprobowanie"
  ]
  node [
    id 55
    label "uznawanie"
  ]
  node [
    id 56
    label "przywidzenie"
  ]
  node [
    id 57
    label "ogl&#261;danie"
  ]
  node [
    id 58
    label "visit"
  ]
  node [
    id 59
    label "dostrzeganie"
  ]
  node [
    id 60
    label "wychodzenie"
  ]
  node [
    id 61
    label "obejrzenie"
  ]
  node [
    id 62
    label "zobaczenie"
  ]
  node [
    id 63
    label "&#347;nienie"
  ]
  node [
    id 64
    label "wyobra&#380;anie_sobie"
  ]
  node [
    id 65
    label "malenie"
  ]
  node [
    id 66
    label "ocenianie"
  ]
  node [
    id 67
    label "vision"
  ]
  node [
    id 68
    label "u&#322;uda"
  ]
  node [
    id 69
    label "patrzenie"
  ]
  node [
    id 70
    label "odwiedziny"
  ]
  node [
    id 71
    label "postrzeganie"
  ]
  node [
    id 72
    label "punkt_widzenia"
  ]
  node [
    id 73
    label "w&#322;&#261;czanie"
  ]
  node [
    id 74
    label "zmalenie"
  ]
  node [
    id 75
    label "przegl&#261;danie"
  ]
  node [
    id 76
    label "zwi&#281;kszanie_si&#281;"
  ]
  node [
    id 77
    label "sojourn"
  ]
  node [
    id 78
    label "realization"
  ]
  node [
    id 79
    label "view"
  ]
  node [
    id 80
    label "reagowanie"
  ]
  node [
    id 81
    label "przejrzenie"
  ]
  node [
    id 82
    label "widywanie"
  ]
  node [
    id 83
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 84
    label "pojmowanie"
  ]
  node [
    id 85
    label "specjalista"
  ]
  node [
    id 86
    label "okulary"
  ]
  node [
    id 87
    label "postrzega&#263;"
  ]
  node [
    id 88
    label "perceive"
  ]
  node [
    id 89
    label "aprobowa&#263;"
  ]
  node [
    id 90
    label "zmale&#263;"
  ]
  node [
    id 91
    label "male&#263;"
  ]
  node [
    id 92
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 93
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 94
    label "spotka&#263;"
  ]
  node [
    id 95
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 96
    label "ogl&#261;da&#263;"
  ]
  node [
    id 97
    label "dostrzega&#263;"
  ]
  node [
    id 98
    label "spowodowa&#263;"
  ]
  node [
    id 99
    label "notice"
  ]
  node [
    id 100
    label "go_steady"
  ]
  node [
    id 101
    label "reagowa&#263;"
  ]
  node [
    id 102
    label "os&#261;dza&#263;"
  ]
  node [
    id 103
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 104
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 105
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 106
    label "rzecz"
  ]
  node [
    id 107
    label "oczy"
  ]
  node [
    id 108
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 109
    label "&#378;renica"
  ]
  node [
    id 110
    label "uwaga"
  ]
  node [
    id 111
    label "spojrzenie"
  ]
  node [
    id 112
    label "&#347;lepko"
  ]
  node [
    id 113
    label "net"
  ]
  node [
    id 114
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 115
    label "twarz"
  ]
  node [
    id 116
    label "siniec"
  ]
  node [
    id 117
    label "powieka"
  ]
  node [
    id 118
    label "spoj&#243;wka"
  ]
  node [
    id 119
    label "organ"
  ]
  node [
    id 120
    label "ga&#322;ka_oczna"
  ]
  node [
    id 121
    label "kaprawienie"
  ]
  node [
    id 122
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 123
    label "coloboma"
  ]
  node [
    id 124
    label "ros&#243;&#322;"
  ]
  node [
    id 125
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 126
    label "wypowied&#378;"
  ]
  node [
    id 127
    label "&#347;lepie"
  ]
  node [
    id 128
    label "nerw_wzrokowy"
  ]
  node [
    id 129
    label "kaprawie&#263;"
  ]
  node [
    id 130
    label "tarnish"
  ]
  node [
    id 131
    label "m&#261;ci&#263;_si&#281;"
  ]
  node [
    id 132
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 133
    label "zaciera&#263;_si&#281;"
  ]
  node [
    id 134
    label "zacieranie_si&#281;"
  ]
  node [
    id 135
    label "stawanie_si&#281;"
  ]
  node [
    id 136
    label "niewyra&#378;ny"
  ]
  node [
    id 137
    label "reputacja"
  ]
  node [
    id 138
    label "pogl&#261;d"
  ]
  node [
    id 139
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 140
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 141
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 142
    label "sofcik"
  ]
  node [
    id 143
    label "wielko&#347;&#263;"
  ]
  node [
    id 144
    label "kryterium"
  ]
  node [
    id 145
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 146
    label "ekspertyza"
  ]
  node [
    id 147
    label "cecha"
  ]
  node [
    id 148
    label "informacja"
  ]
  node [
    id 149
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 150
    label "dokument"
  ]
  node [
    id 151
    label "appraisal"
  ]
  node [
    id 152
    label "charakterystyka"
  ]
  node [
    id 153
    label "m&#322;ot"
  ]
  node [
    id 154
    label "znak"
  ]
  node [
    id 155
    label "drzewo"
  ]
  node [
    id 156
    label "pr&#243;ba"
  ]
  node [
    id 157
    label "attribute"
  ]
  node [
    id 158
    label "marka"
  ]
  node [
    id 159
    label "znaczenie"
  ]
  node [
    id 160
    label "badanie"
  ]
  node [
    id 161
    label "sketch"
  ]
  node [
    id 162
    label "ocena"
  ]
  node [
    id 163
    label "survey"
  ]
  node [
    id 164
    label "zapis"
  ]
  node [
    id 165
    label "&#347;wiadectwo"
  ]
  node [
    id 166
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 167
    label "wytw&#243;r"
  ]
  node [
    id 168
    label "parafa"
  ]
  node [
    id 169
    label "plik"
  ]
  node [
    id 170
    label "raport&#243;wka"
  ]
  node [
    id 171
    label "utw&#243;r"
  ]
  node [
    id 172
    label "record"
  ]
  node [
    id 173
    label "fascyku&#322;"
  ]
  node [
    id 174
    label "dokumentacja"
  ]
  node [
    id 175
    label "registratura"
  ]
  node [
    id 176
    label "artyku&#322;"
  ]
  node [
    id 177
    label "writing"
  ]
  node [
    id 178
    label "sygnatariusz"
  ]
  node [
    id 179
    label "teologicznie"
  ]
  node [
    id 180
    label "s&#261;d"
  ]
  node [
    id 181
    label "belief"
  ]
  node [
    id 182
    label "zderzenie_si&#281;"
  ]
  node [
    id 183
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 184
    label "teoria_Arrheniusa"
  ]
  node [
    id 185
    label "punkt"
  ]
  node [
    id 186
    label "publikacja"
  ]
  node [
    id 187
    label "wiedza"
  ]
  node [
    id 188
    label "doj&#347;cie"
  ]
  node [
    id 189
    label "obiega&#263;"
  ]
  node [
    id 190
    label "powzi&#281;cie"
  ]
  node [
    id 191
    label "dane"
  ]
  node [
    id 192
    label "obiegni&#281;cie"
  ]
  node [
    id 193
    label "sygna&#322;"
  ]
  node [
    id 194
    label "obieganie"
  ]
  node [
    id 195
    label "powzi&#261;&#263;"
  ]
  node [
    id 196
    label "obiec"
  ]
  node [
    id 197
    label "doj&#347;&#263;"
  ]
  node [
    id 198
    label "warunek_lokalowy"
  ]
  node [
    id 199
    label "rozmiar"
  ]
  node [
    id 200
    label "liczba"
  ]
  node [
    id 201
    label "rzadko&#347;&#263;"
  ]
  node [
    id 202
    label "zaleta"
  ]
  node [
    id 203
    label "ilo&#347;&#263;"
  ]
  node [
    id 204
    label "measure"
  ]
  node [
    id 205
    label "dymensja"
  ]
  node [
    id 206
    label "poj&#281;cie"
  ]
  node [
    id 207
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 208
    label "potencja"
  ]
  node [
    id 209
    label "property"
  ]
  node [
    id 210
    label "pornografia_mi&#281;kka"
  ]
  node [
    id 211
    label "drobiazg"
  ]
  node [
    id 212
    label "poziom"
  ]
  node [
    id 213
    label "pornografia"
  ]
  node [
    id 214
    label "czynnik"
  ]
  node [
    id 215
    label "p&#243;&#322;rocze"
  ]
  node [
    id 216
    label "martwy_sezon"
  ]
  node [
    id 217
    label "kalendarz"
  ]
  node [
    id 218
    label "cykl_astronomiczny"
  ]
  node [
    id 219
    label "lata"
  ]
  node [
    id 220
    label "pora_roku"
  ]
  node [
    id 221
    label "stulecie"
  ]
  node [
    id 222
    label "kurs"
  ]
  node [
    id 223
    label "czas"
  ]
  node [
    id 224
    label "jubileusz"
  ]
  node [
    id 225
    label "grupa"
  ]
  node [
    id 226
    label "kwarta&#322;"
  ]
  node [
    id 227
    label "miesi&#261;c"
  ]
  node [
    id 228
    label "summer"
  ]
  node [
    id 229
    label "odm&#322;adzanie"
  ]
  node [
    id 230
    label "liga"
  ]
  node [
    id 231
    label "jednostka_systematyczna"
  ]
  node [
    id 232
    label "asymilowanie"
  ]
  node [
    id 233
    label "gromada"
  ]
  node [
    id 234
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 235
    label "asymilowa&#263;"
  ]
  node [
    id 236
    label "egzemplarz"
  ]
  node [
    id 237
    label "Entuzjastki"
  ]
  node [
    id 238
    label "zbi&#243;r"
  ]
  node [
    id 239
    label "kompozycja"
  ]
  node [
    id 240
    label "Terranie"
  ]
  node [
    id 241
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 242
    label "category"
  ]
  node [
    id 243
    label "pakiet_klimatyczny"
  ]
  node [
    id 244
    label "oddzia&#322;"
  ]
  node [
    id 245
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 246
    label "cz&#261;steczka"
  ]
  node [
    id 247
    label "stage_set"
  ]
  node [
    id 248
    label "type"
  ]
  node [
    id 249
    label "specgrupa"
  ]
  node [
    id 250
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 251
    label "&#346;wietliki"
  ]
  node [
    id 252
    label "odm&#322;odzenie"
  ]
  node [
    id 253
    label "Eurogrupa"
  ]
  node [
    id 254
    label "odm&#322;adza&#263;"
  ]
  node [
    id 255
    label "harcerze_starsi"
  ]
  node [
    id 256
    label "poprzedzanie"
  ]
  node [
    id 257
    label "czasoprzestrze&#324;"
  ]
  node [
    id 258
    label "laba"
  ]
  node [
    id 259
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 260
    label "chronometria"
  ]
  node [
    id 261
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 262
    label "rachuba_czasu"
  ]
  node [
    id 263
    label "przep&#322;ywanie"
  ]
  node [
    id 264
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 265
    label "czasokres"
  ]
  node [
    id 266
    label "odczyt"
  ]
  node [
    id 267
    label "chwila"
  ]
  node [
    id 268
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 269
    label "dzieje"
  ]
  node [
    id 270
    label "kategoria_gramatyczna"
  ]
  node [
    id 271
    label "poprzedzenie"
  ]
  node [
    id 272
    label "trawienie"
  ]
  node [
    id 273
    label "pochodzi&#263;"
  ]
  node [
    id 274
    label "period"
  ]
  node [
    id 275
    label "okres_czasu"
  ]
  node [
    id 276
    label "poprzedza&#263;"
  ]
  node [
    id 277
    label "schy&#322;ek"
  ]
  node [
    id 278
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 279
    label "odwlekanie_si&#281;"
  ]
  node [
    id 280
    label "zegar"
  ]
  node [
    id 281
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 282
    label "czwarty_wymiar"
  ]
  node [
    id 283
    label "pochodzenie"
  ]
  node [
    id 284
    label "koniugacja"
  ]
  node [
    id 285
    label "Zeitgeist"
  ]
  node [
    id 286
    label "trawi&#263;"
  ]
  node [
    id 287
    label "pogoda"
  ]
  node [
    id 288
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 289
    label "poprzedzi&#263;"
  ]
  node [
    id 290
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 291
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 292
    label "time_period"
  ]
  node [
    id 293
    label "term"
  ]
  node [
    id 294
    label "rok_akademicki"
  ]
  node [
    id 295
    label "rok_szkolny"
  ]
  node [
    id 296
    label "semester"
  ]
  node [
    id 297
    label "anniwersarz"
  ]
  node [
    id 298
    label "rocznica"
  ]
  node [
    id 299
    label "obszar"
  ]
  node [
    id 300
    label "tydzie&#324;"
  ]
  node [
    id 301
    label "miech"
  ]
  node [
    id 302
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 303
    label "kalendy"
  ]
  node [
    id 304
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 305
    label "long_time"
  ]
  node [
    id 306
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 307
    label "almanac"
  ]
  node [
    id 308
    label "rozk&#322;ad"
  ]
  node [
    id 309
    label "wydawnictwo"
  ]
  node [
    id 310
    label "Juliusz_Cezar"
  ]
  node [
    id 311
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 312
    label "zwy&#380;kowanie"
  ]
  node [
    id 313
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 314
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 315
    label "zaj&#281;cia"
  ]
  node [
    id 316
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 317
    label "trasa"
  ]
  node [
    id 318
    label "przeorientowywanie"
  ]
  node [
    id 319
    label "przejazd"
  ]
  node [
    id 320
    label "kierunek"
  ]
  node [
    id 321
    label "przeorientowywa&#263;"
  ]
  node [
    id 322
    label "nauka"
  ]
  node [
    id 323
    label "przeorientowanie"
  ]
  node [
    id 324
    label "klasa"
  ]
  node [
    id 325
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 326
    label "przeorientowa&#263;"
  ]
  node [
    id 327
    label "manner"
  ]
  node [
    id 328
    label "course"
  ]
  node [
    id 329
    label "passage"
  ]
  node [
    id 330
    label "zni&#380;kowanie"
  ]
  node [
    id 331
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 332
    label "seria"
  ]
  node [
    id 333
    label "stawka"
  ]
  node [
    id 334
    label "way"
  ]
  node [
    id 335
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 336
    label "spos&#243;b"
  ]
  node [
    id 337
    label "deprecjacja"
  ]
  node [
    id 338
    label "cedu&#322;a"
  ]
  node [
    id 339
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 340
    label "drive"
  ]
  node [
    id 341
    label "bearing"
  ]
  node [
    id 342
    label "Lira"
  ]
  node [
    id 343
    label "p&#243;&#378;ny"
  ]
  node [
    id 344
    label "do_p&#243;&#378;na"
  ]
  node [
    id 345
    label "Ola"
  ]
  node [
    id 346
    label "7"
  ]
  node [
    id 347
    label "OP"
  ]
  node [
    id 348
    label "Pozdro"
  ]
  node [
    id 349
    label "Mirka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 345
    target 346
  ]
  edge [
    source 346
    target 347
  ]
  edge [
    source 348
    target 349
  ]
]
