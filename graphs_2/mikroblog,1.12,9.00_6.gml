graph [
  node [
    id 0
    label "dawno"
    origin "text"
  ]
  node [
    id 1
    label "taki"
    origin "text"
  ]
  node [
    id 2
    label "smutny"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ongi&#347;"
  ]
  node [
    id 5
    label "d&#322;ugotrwale"
  ]
  node [
    id 6
    label "dawnie"
  ]
  node [
    id 7
    label "wcze&#347;niej"
  ]
  node [
    id 8
    label "dawny"
  ]
  node [
    id 9
    label "kiedy&#347;"
  ]
  node [
    id 10
    label "niegdysiejszy"
  ]
  node [
    id 11
    label "drzewiej"
  ]
  node [
    id 12
    label "d&#322;ugo"
  ]
  node [
    id 13
    label "d&#322;ugotrwa&#322;y"
  ]
  node [
    id 14
    label "wcze&#347;niejszy"
  ]
  node [
    id 15
    label "od_dawna"
  ]
  node [
    id 16
    label "anachroniczny"
  ]
  node [
    id 17
    label "dawniej"
  ]
  node [
    id 18
    label "odleg&#322;y"
  ]
  node [
    id 19
    label "przesz&#322;y"
  ]
  node [
    id 20
    label "d&#322;ugoletni"
  ]
  node [
    id 21
    label "poprzedni"
  ]
  node [
    id 22
    label "przestarza&#322;y"
  ]
  node [
    id 23
    label "kombatant"
  ]
  node [
    id 24
    label "stary"
  ]
  node [
    id 25
    label "jaki&#347;"
  ]
  node [
    id 26
    label "okre&#347;lony"
  ]
  node [
    id 27
    label "jako&#347;"
  ]
  node [
    id 28
    label "niez&#322;y"
  ]
  node [
    id 29
    label "charakterystyczny"
  ]
  node [
    id 30
    label "jako_tako"
  ]
  node [
    id 31
    label "ciekawy"
  ]
  node [
    id 32
    label "dziwny"
  ]
  node [
    id 33
    label "przyzwoity"
  ]
  node [
    id 34
    label "wiadomy"
  ]
  node [
    id 35
    label "przykry"
  ]
  node [
    id 36
    label "z&#322;y"
  ]
  node [
    id 37
    label "smutno"
  ]
  node [
    id 38
    label "negatywny"
  ]
  node [
    id 39
    label "negatywnie"
  ]
  node [
    id 40
    label "&#378;le"
  ]
  node [
    id 41
    label "ujemnie"
  ]
  node [
    id 42
    label "nieprzyjemny"
  ]
  node [
    id 43
    label "syf"
  ]
  node [
    id 44
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 45
    label "niepomy&#347;lny"
  ]
  node [
    id 46
    label "niegrzeczny"
  ]
  node [
    id 47
    label "rozgniewanie"
  ]
  node [
    id 48
    label "zdenerwowany"
  ]
  node [
    id 49
    label "niemoralny"
  ]
  node [
    id 50
    label "sierdzisty"
  ]
  node [
    id 51
    label "pieski"
  ]
  node [
    id 52
    label "zez&#322;oszczenie"
  ]
  node [
    id 53
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 54
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 55
    label "z&#322;oszczenie"
  ]
  node [
    id 56
    label "gniewanie"
  ]
  node [
    id 57
    label "niekorzystny"
  ]
  node [
    id 58
    label "dokuczliwy"
  ]
  node [
    id 59
    label "przykro"
  ]
  node [
    id 60
    label "niemi&#322;y"
  ]
  node [
    id 61
    label "nieprzyjemnie"
  ]
  node [
    id 62
    label "smutnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
]
