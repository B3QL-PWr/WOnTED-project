graph [
  node [
    id 0
    label "zabytkowy"
    origin "text"
  ]
  node [
    id 1
    label "nagranie"
    origin "text"
  ]
  node [
    id 2
    label "ukazowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "alpinista"
    origin "text"
  ]
  node [
    id 4
    label "zdobywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "szczyt"
    origin "text"
  ]
  node [
    id 6
    label "dent"
    origin "text"
  ]
  node [
    id 7
    label "geant"
    origin "text"
  ]
  node [
    id 8
    label "uznawa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "jeden"
    origin "text"
  ]
  node [
    id 10
    label "trudny"
    origin "text"
  ]
  node [
    id 11
    label "technicznie"
    origin "text"
  ]
  node [
    id 12
    label "czterotysi&#281;cznik&#243;w"
    origin "text"
  ]
  node [
    id 13
    label "alpy"
    origin "text"
  ]
  node [
    id 14
    label "staromodny"
  ]
  node [
    id 15
    label "cenny"
  ]
  node [
    id 16
    label "zabytkowo"
  ]
  node [
    id 17
    label "stary"
  ]
  node [
    id 18
    label "drogi"
  ]
  node [
    id 19
    label "warto&#347;ciowy"
  ]
  node [
    id 20
    label "cennie"
  ]
  node [
    id 21
    label "wa&#380;ny"
  ]
  node [
    id 22
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 23
    label "ojciec"
  ]
  node [
    id 24
    label "nienowoczesny"
  ]
  node [
    id 25
    label "gruba_ryba"
  ]
  node [
    id 26
    label "zestarzenie_si&#281;"
  ]
  node [
    id 27
    label "poprzedni"
  ]
  node [
    id 28
    label "dawno"
  ]
  node [
    id 29
    label "staro"
  ]
  node [
    id 30
    label "m&#261;&#380;"
  ]
  node [
    id 31
    label "starzy"
  ]
  node [
    id 32
    label "dotychczasowy"
  ]
  node [
    id 33
    label "p&#243;&#378;ny"
  ]
  node [
    id 34
    label "d&#322;ugoletni"
  ]
  node [
    id 35
    label "charakterystyczny"
  ]
  node [
    id 36
    label "brat"
  ]
  node [
    id 37
    label "po_staro&#347;wiecku"
  ]
  node [
    id 38
    label "zwierzchnik"
  ]
  node [
    id 39
    label "znajomy"
  ]
  node [
    id 40
    label "odleg&#322;y"
  ]
  node [
    id 41
    label "starzenie_si&#281;"
  ]
  node [
    id 42
    label "starczo"
  ]
  node [
    id 43
    label "dawniej"
  ]
  node [
    id 44
    label "niegdysiejszy"
  ]
  node [
    id 45
    label "dojrza&#322;y"
  ]
  node [
    id 46
    label "zatabaczony"
  ]
  node [
    id 47
    label "niemodny"
  ]
  node [
    id 48
    label "niedzisiejszy"
  ]
  node [
    id 49
    label "zachowawczy"
  ]
  node [
    id 50
    label "staromodnie"
  ]
  node [
    id 51
    label "tradycyjny"
  ]
  node [
    id 52
    label "wytw&#243;r"
  ]
  node [
    id 53
    label "wys&#322;uchanie"
  ]
  node [
    id 54
    label "utrwalenie"
  ]
  node [
    id 55
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 56
    label "recording"
  ]
  node [
    id 57
    label "ustalenie"
  ]
  node [
    id 58
    label "trwalszy"
  ]
  node [
    id 59
    label "confirmation"
  ]
  node [
    id 60
    label "zachowanie"
  ]
  node [
    id 61
    label "przedmiot"
  ]
  node [
    id 62
    label "p&#322;&#243;d"
  ]
  node [
    id 63
    label "work"
  ]
  node [
    id 64
    label "rezultat"
  ]
  node [
    id 65
    label "pos&#322;uchanie"
  ]
  node [
    id 66
    label "spe&#322;nienie"
  ]
  node [
    id 67
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 68
    label "hearing"
  ]
  node [
    id 69
    label "muzyka"
  ]
  node [
    id 70
    label "spe&#322;ni&#263;"
  ]
  node [
    id 71
    label "wspinacz"
  ]
  node [
    id 72
    label "drwal"
  ]
  node [
    id 73
    label "sportowiec"
  ]
  node [
    id 74
    label "niewoli&#263;"
  ]
  node [
    id 75
    label "robi&#263;"
  ]
  node [
    id 76
    label "uzyskiwa&#263;"
  ]
  node [
    id 77
    label "tease"
  ]
  node [
    id 78
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 79
    label "dostawa&#263;"
  ]
  node [
    id 80
    label "have"
  ]
  node [
    id 81
    label "raise"
  ]
  node [
    id 82
    label "organizowa&#263;"
  ]
  node [
    id 83
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 84
    label "czyni&#263;"
  ]
  node [
    id 85
    label "give"
  ]
  node [
    id 86
    label "stylizowa&#263;"
  ]
  node [
    id 87
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 88
    label "falowa&#263;"
  ]
  node [
    id 89
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 90
    label "peddle"
  ]
  node [
    id 91
    label "praca"
  ]
  node [
    id 92
    label "wydala&#263;"
  ]
  node [
    id 93
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 94
    label "tentegowa&#263;"
  ]
  node [
    id 95
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 96
    label "urz&#261;dza&#263;"
  ]
  node [
    id 97
    label "oszukiwa&#263;"
  ]
  node [
    id 98
    label "ukazywa&#263;"
  ]
  node [
    id 99
    label "przerabia&#263;"
  ]
  node [
    id 100
    label "act"
  ]
  node [
    id 101
    label "post&#281;powa&#263;"
  ]
  node [
    id 102
    label "wytwarza&#263;"
  ]
  node [
    id 103
    label "take"
  ]
  node [
    id 104
    label "get"
  ]
  node [
    id 105
    label "mark"
  ]
  node [
    id 106
    label "powodowa&#263;"
  ]
  node [
    id 107
    label "dostosowywa&#263;"
  ]
  node [
    id 108
    label "subordinate"
  ]
  node [
    id 109
    label "hyponym"
  ]
  node [
    id 110
    label "prowadzi&#263;_na_pasku"
  ]
  node [
    id 111
    label "dyrygowa&#263;"
  ]
  node [
    id 112
    label "uzale&#380;nia&#263;"
  ]
  node [
    id 113
    label "mie&#263;_miejsce"
  ]
  node [
    id 114
    label "by&#263;"
  ]
  node [
    id 115
    label "nabywa&#263;"
  ]
  node [
    id 116
    label "bra&#263;"
  ]
  node [
    id 117
    label "winnings"
  ]
  node [
    id 118
    label "opanowywa&#263;"
  ]
  node [
    id 119
    label "si&#281;ga&#263;"
  ]
  node [
    id 120
    label "otrzymywa&#263;"
  ]
  node [
    id 121
    label "range"
  ]
  node [
    id 122
    label "wystarcza&#263;"
  ]
  node [
    id 123
    label "kupowa&#263;"
  ]
  node [
    id 124
    label "obskakiwa&#263;"
  ]
  node [
    id 125
    label "zmusza&#263;"
  ]
  node [
    id 126
    label "zwie&#324;czenie"
  ]
  node [
    id 127
    label "Wielka_Racza"
  ]
  node [
    id 128
    label "koniec"
  ]
  node [
    id 129
    label "&#346;winica"
  ]
  node [
    id 130
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 131
    label "Che&#322;miec"
  ]
  node [
    id 132
    label "wierzcho&#322;"
  ]
  node [
    id 133
    label "wierzcho&#322;ek"
  ]
  node [
    id 134
    label "Radunia"
  ]
  node [
    id 135
    label "Barania_G&#243;ra"
  ]
  node [
    id 136
    label "Groniczki"
  ]
  node [
    id 137
    label "wierch"
  ]
  node [
    id 138
    label "konferencja"
  ]
  node [
    id 139
    label "Czupel"
  ]
  node [
    id 140
    label "&#347;ciana"
  ]
  node [
    id 141
    label "Jaworz"
  ]
  node [
    id 142
    label "Okr&#261;glica"
  ]
  node [
    id 143
    label "Walig&#243;ra"
  ]
  node [
    id 144
    label "bok"
  ]
  node [
    id 145
    label "Wielka_Sowa"
  ]
  node [
    id 146
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 147
    label "&#321;omnica"
  ]
  node [
    id 148
    label "wzniesienie"
  ]
  node [
    id 149
    label "Beskid"
  ]
  node [
    id 150
    label "fasada"
  ]
  node [
    id 151
    label "Wo&#322;ek"
  ]
  node [
    id 152
    label "summit"
  ]
  node [
    id 153
    label "Rysianka"
  ]
  node [
    id 154
    label "Mody&#324;"
  ]
  node [
    id 155
    label "poziom"
  ]
  node [
    id 156
    label "wzmo&#380;enie"
  ]
  node [
    id 157
    label "czas"
  ]
  node [
    id 158
    label "Obidowa"
  ]
  node [
    id 159
    label "Jaworzyna"
  ]
  node [
    id 160
    label "godzina_szczytu"
  ]
  node [
    id 161
    label "Turbacz"
  ]
  node [
    id 162
    label "Rudawiec"
  ]
  node [
    id 163
    label "g&#243;ra"
  ]
  node [
    id 164
    label "Ja&#322;owiec"
  ]
  node [
    id 165
    label "Wielki_Chocz"
  ]
  node [
    id 166
    label "Orlica"
  ]
  node [
    id 167
    label "Szrenica"
  ]
  node [
    id 168
    label "&#346;nie&#380;nik"
  ]
  node [
    id 169
    label "Cubryna"
  ]
  node [
    id 170
    label "Wielki_Bukowiec"
  ]
  node [
    id 171
    label "Magura"
  ]
  node [
    id 172
    label "korona"
  ]
  node [
    id 173
    label "Czarna_G&#243;ra"
  ]
  node [
    id 174
    label "Lubogoszcz"
  ]
  node [
    id 175
    label "ostatnie_podrygi"
  ]
  node [
    id 176
    label "visitation"
  ]
  node [
    id 177
    label "agonia"
  ]
  node [
    id 178
    label "defenestracja"
  ]
  node [
    id 179
    label "punkt"
  ]
  node [
    id 180
    label "dzia&#322;anie"
  ]
  node [
    id 181
    label "kres"
  ]
  node [
    id 182
    label "wydarzenie"
  ]
  node [
    id 183
    label "mogi&#322;a"
  ]
  node [
    id 184
    label "kres_&#380;ycia"
  ]
  node [
    id 185
    label "szereg"
  ]
  node [
    id 186
    label "szeol"
  ]
  node [
    id 187
    label "pogrzebanie"
  ]
  node [
    id 188
    label "miejsce"
  ]
  node [
    id 189
    label "chwila"
  ]
  node [
    id 190
    label "&#380;a&#322;oba"
  ]
  node [
    id 191
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 192
    label "zabicie"
  ]
  node [
    id 193
    label "przelezienie"
  ]
  node [
    id 194
    label "&#347;piew"
  ]
  node [
    id 195
    label "Synaj"
  ]
  node [
    id 196
    label "Kreml"
  ]
  node [
    id 197
    label "d&#378;wi&#281;k"
  ]
  node [
    id 198
    label "kierunek"
  ]
  node [
    id 199
    label "wysoki"
  ]
  node [
    id 200
    label "element"
  ]
  node [
    id 201
    label "grupa"
  ]
  node [
    id 202
    label "pi&#281;tro"
  ]
  node [
    id 203
    label "Ropa"
  ]
  node [
    id 204
    label "kupa"
  ]
  node [
    id 205
    label "przele&#378;&#263;"
  ]
  node [
    id 206
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 207
    label "karczek"
  ]
  node [
    id 208
    label "rami&#261;czko"
  ]
  node [
    id 209
    label "Jaworze"
  ]
  node [
    id 210
    label "Ja&#322;ta"
  ]
  node [
    id 211
    label "spotkanie"
  ]
  node [
    id 212
    label "konferencyjka"
  ]
  node [
    id 213
    label "conference"
  ]
  node [
    id 214
    label "grusza_pospolita"
  ]
  node [
    id 215
    label "Poczdam"
  ]
  node [
    id 216
    label "graf"
  ]
  node [
    id 217
    label "po&#322;o&#380;enie"
  ]
  node [
    id 218
    label "jako&#347;&#263;"
  ]
  node [
    id 219
    label "p&#322;aszczyzna"
  ]
  node [
    id 220
    label "punkt_widzenia"
  ]
  node [
    id 221
    label "wyk&#322;adnik"
  ]
  node [
    id 222
    label "faza"
  ]
  node [
    id 223
    label "szczebel"
  ]
  node [
    id 224
    label "budynek"
  ]
  node [
    id 225
    label "wysoko&#347;&#263;"
  ]
  node [
    id 226
    label "ranga"
  ]
  node [
    id 227
    label "tu&#322;&#243;w"
  ]
  node [
    id 228
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 229
    label "wielok&#261;t"
  ]
  node [
    id 230
    label "odcinek"
  ]
  node [
    id 231
    label "strzelba"
  ]
  node [
    id 232
    label "lufa"
  ]
  node [
    id 233
    label "strona"
  ]
  node [
    id 234
    label "przybranie"
  ]
  node [
    id 235
    label "maksimum"
  ]
  node [
    id 236
    label "zako&#324;czenie"
  ]
  node [
    id 237
    label "zdobienie"
  ]
  node [
    id 238
    label "consummation"
  ]
  node [
    id 239
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 240
    label "powi&#281;kszenie"
  ]
  node [
    id 241
    label "pobudzenie"
  ]
  node [
    id 242
    label "vivification"
  ]
  node [
    id 243
    label "exploitation"
  ]
  node [
    id 244
    label "poprzedzanie"
  ]
  node [
    id 245
    label "czasoprzestrze&#324;"
  ]
  node [
    id 246
    label "laba"
  ]
  node [
    id 247
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 248
    label "chronometria"
  ]
  node [
    id 249
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 250
    label "rachuba_czasu"
  ]
  node [
    id 251
    label "przep&#322;ywanie"
  ]
  node [
    id 252
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 253
    label "czasokres"
  ]
  node [
    id 254
    label "odczyt"
  ]
  node [
    id 255
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 256
    label "dzieje"
  ]
  node [
    id 257
    label "kategoria_gramatyczna"
  ]
  node [
    id 258
    label "poprzedzenie"
  ]
  node [
    id 259
    label "trawienie"
  ]
  node [
    id 260
    label "pochodzi&#263;"
  ]
  node [
    id 261
    label "period"
  ]
  node [
    id 262
    label "okres_czasu"
  ]
  node [
    id 263
    label "poprzedza&#263;"
  ]
  node [
    id 264
    label "schy&#322;ek"
  ]
  node [
    id 265
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 266
    label "odwlekanie_si&#281;"
  ]
  node [
    id 267
    label "zegar"
  ]
  node [
    id 268
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 269
    label "czwarty_wymiar"
  ]
  node [
    id 270
    label "pochodzenie"
  ]
  node [
    id 271
    label "koniugacja"
  ]
  node [
    id 272
    label "Zeitgeist"
  ]
  node [
    id 273
    label "trawi&#263;"
  ]
  node [
    id 274
    label "pogoda"
  ]
  node [
    id 275
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 276
    label "poprzedzi&#263;"
  ]
  node [
    id 277
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 278
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 279
    label "time_period"
  ]
  node [
    id 280
    label "profil"
  ]
  node [
    id 281
    label "zbocze"
  ]
  node [
    id 282
    label "kszta&#322;t"
  ]
  node [
    id 283
    label "przegroda"
  ]
  node [
    id 284
    label "bariera"
  ]
  node [
    id 285
    label "facebook"
  ]
  node [
    id 286
    label "wielo&#347;cian"
  ]
  node [
    id 287
    label "obstruction"
  ]
  node [
    id 288
    label "pow&#322;oka"
  ]
  node [
    id 289
    label "wyrobisko"
  ]
  node [
    id 290
    label "trudno&#347;&#263;"
  ]
  node [
    id 291
    label "corona"
  ]
  node [
    id 292
    label "zesp&#243;&#322;"
  ]
  node [
    id 293
    label "warkocz"
  ]
  node [
    id 294
    label "regalia"
  ]
  node [
    id 295
    label "drzewo"
  ]
  node [
    id 296
    label "czub"
  ]
  node [
    id 297
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 298
    label "bryd&#380;"
  ]
  node [
    id 299
    label "moneta"
  ]
  node [
    id 300
    label "przepaska"
  ]
  node [
    id 301
    label "r&#243;g"
  ]
  node [
    id 302
    label "wieniec"
  ]
  node [
    id 303
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 304
    label "motyl"
  ]
  node [
    id 305
    label "geofit"
  ]
  node [
    id 306
    label "liliowate"
  ]
  node [
    id 307
    label "pa&#324;stwo"
  ]
  node [
    id 308
    label "kwiat"
  ]
  node [
    id 309
    label "jednostka_monetarna"
  ]
  node [
    id 310
    label "proteza_dentystyczna"
  ]
  node [
    id 311
    label "urz&#261;d"
  ]
  node [
    id 312
    label "kok"
  ]
  node [
    id 313
    label "diadem"
  ]
  node [
    id 314
    label "p&#322;atek"
  ]
  node [
    id 315
    label "z&#261;b"
  ]
  node [
    id 316
    label "genitalia"
  ]
  node [
    id 317
    label "Crown"
  ]
  node [
    id 318
    label "znak_muzyczny"
  ]
  node [
    id 319
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 320
    label "uk&#322;ad"
  ]
  node [
    id 321
    label "Beskid_Ma&#322;y"
  ]
  node [
    id 322
    label "Karkonosze"
  ]
  node [
    id 323
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 324
    label "Tatry"
  ]
  node [
    id 325
    label "Beskid_&#346;l&#261;ski"
  ]
  node [
    id 326
    label "Rudawy_Janowickie"
  ]
  node [
    id 327
    label "G&#243;ry_Kamienne"
  ]
  node [
    id 328
    label "Masyw_&#346;l&#281;&#380;y"
  ]
  node [
    id 329
    label "Beskid_Wyspowy"
  ]
  node [
    id 330
    label "G&#243;ry_Bialskie"
  ]
  node [
    id 331
    label "Gorce"
  ]
  node [
    id 332
    label "Masyw_&#346;nie&#380;nika"
  ]
  node [
    id 333
    label "G&#243;ry_Wa&#322;brzyskie"
  ]
  node [
    id 334
    label "Beskid_Makowski"
  ]
  node [
    id 335
    label "G&#243;ry_Orlickie"
  ]
  node [
    id 336
    label "nabudowanie"
  ]
  node [
    id 337
    label "Skalnik"
  ]
  node [
    id 338
    label "budowla"
  ]
  node [
    id 339
    label "wierzchowina"
  ]
  node [
    id 340
    label "wypi&#281;trzy&#263;_si&#281;"
  ]
  node [
    id 341
    label "Sikornik"
  ]
  node [
    id 342
    label "Bukowiec"
  ]
  node [
    id 343
    label "Izera"
  ]
  node [
    id 344
    label "wyro&#347;ni&#281;cie"
  ]
  node [
    id 345
    label "rise"
  ]
  node [
    id 346
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 347
    label "wypi&#281;trza&#263;_si&#281;"
  ]
  node [
    id 348
    label "podniesienie"
  ]
  node [
    id 349
    label "Zwalisko"
  ]
  node [
    id 350
    label "Bielec"
  ]
  node [
    id 351
    label "construction"
  ]
  node [
    id 352
    label "zrobienie"
  ]
  node [
    id 353
    label "nieprawda"
  ]
  node [
    id 354
    label "semblance"
  ]
  node [
    id 355
    label "elewacja"
  ]
  node [
    id 356
    label "os&#261;dza&#263;"
  ]
  node [
    id 357
    label "consider"
  ]
  node [
    id 358
    label "notice"
  ]
  node [
    id 359
    label "stwierdza&#263;"
  ]
  node [
    id 360
    label "przyznawa&#263;"
  ]
  node [
    id 361
    label "dawa&#263;"
  ]
  node [
    id 362
    label "confer"
  ]
  node [
    id 363
    label "s&#261;dzi&#263;"
  ]
  node [
    id 364
    label "distribute"
  ]
  node [
    id 365
    label "nadawa&#263;"
  ]
  node [
    id 366
    label "strike"
  ]
  node [
    id 367
    label "znajdowa&#263;"
  ]
  node [
    id 368
    label "hold"
  ]
  node [
    id 369
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 370
    label "attest"
  ]
  node [
    id 371
    label "oznajmia&#263;"
  ]
  node [
    id 372
    label "shot"
  ]
  node [
    id 373
    label "jednakowy"
  ]
  node [
    id 374
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 375
    label "ujednolicenie"
  ]
  node [
    id 376
    label "jaki&#347;"
  ]
  node [
    id 377
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 378
    label "jednolicie"
  ]
  node [
    id 379
    label "kieliszek"
  ]
  node [
    id 380
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 381
    label "w&#243;dka"
  ]
  node [
    id 382
    label "ten"
  ]
  node [
    id 383
    label "szk&#322;o"
  ]
  node [
    id 384
    label "zawarto&#347;&#263;"
  ]
  node [
    id 385
    label "naczynie"
  ]
  node [
    id 386
    label "alkohol"
  ]
  node [
    id 387
    label "sznaps"
  ]
  node [
    id 388
    label "nap&#243;j"
  ]
  node [
    id 389
    label "gorza&#322;ka"
  ]
  node [
    id 390
    label "mohorycz"
  ]
  node [
    id 391
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 392
    label "zr&#243;wnanie"
  ]
  node [
    id 393
    label "mundurowanie"
  ]
  node [
    id 394
    label "taki&#380;"
  ]
  node [
    id 395
    label "jednakowo"
  ]
  node [
    id 396
    label "mundurowa&#263;"
  ]
  node [
    id 397
    label "zr&#243;wnywanie"
  ]
  node [
    id 398
    label "identyczny"
  ]
  node [
    id 399
    label "okre&#347;lony"
  ]
  node [
    id 400
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 401
    label "z&#322;o&#380;ony"
  ]
  node [
    id 402
    label "przyzwoity"
  ]
  node [
    id 403
    label "ciekawy"
  ]
  node [
    id 404
    label "jako&#347;"
  ]
  node [
    id 405
    label "jako_tako"
  ]
  node [
    id 406
    label "niez&#322;y"
  ]
  node [
    id 407
    label "dziwny"
  ]
  node [
    id 408
    label "g&#322;&#281;bszy"
  ]
  node [
    id 409
    label "drink"
  ]
  node [
    id 410
    label "jednolity"
  ]
  node [
    id 411
    label "upodobnienie"
  ]
  node [
    id 412
    label "calibration"
  ]
  node [
    id 413
    label "k&#322;opotliwy"
  ]
  node [
    id 414
    label "skomplikowany"
  ]
  node [
    id 415
    label "ci&#281;&#380;ko"
  ]
  node [
    id 416
    label "wymagaj&#261;cy"
  ]
  node [
    id 417
    label "monumentalnie"
  ]
  node [
    id 418
    label "charakterystycznie"
  ]
  node [
    id 419
    label "gro&#378;nie"
  ]
  node [
    id 420
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 421
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 422
    label "nieudanie"
  ]
  node [
    id 423
    label "mocno"
  ]
  node [
    id 424
    label "wolno"
  ]
  node [
    id 425
    label "kompletnie"
  ]
  node [
    id 426
    label "ci&#281;&#380;ki"
  ]
  node [
    id 427
    label "dotkliwie"
  ]
  node [
    id 428
    label "niezgrabnie"
  ]
  node [
    id 429
    label "hard"
  ]
  node [
    id 430
    label "&#378;le"
  ]
  node [
    id 431
    label "masywnie"
  ]
  node [
    id 432
    label "heavily"
  ]
  node [
    id 433
    label "niedelikatnie"
  ]
  node [
    id 434
    label "intensywnie"
  ]
  node [
    id 435
    label "skomplikowanie"
  ]
  node [
    id 436
    label "wymagaj&#261;co"
  ]
  node [
    id 437
    label "k&#322;opotliwie"
  ]
  node [
    id 438
    label "nieprzyjemny"
  ]
  node [
    id 439
    label "niewygodny"
  ]
  node [
    id 440
    label "sucho"
  ]
  node [
    id 441
    label "technically"
  ]
  node [
    id 442
    label "techniczny"
  ]
  node [
    id 443
    label "nieznaczny"
  ]
  node [
    id 444
    label "pozamerytoryczny"
  ]
  node [
    id 445
    label "ambitny"
  ]
  node [
    id 446
    label "suchy"
  ]
  node [
    id 447
    label "specjalny"
  ]
  node [
    id 448
    label "s&#322;abo"
  ]
  node [
    id 449
    label "ch&#322;odnie"
  ]
  node [
    id 450
    label "nieciekawie"
  ]
  node [
    id 451
    label "cicho"
  ]
  node [
    id 452
    label "ch&#322;odny"
  ]
  node [
    id 453
    label "matowo"
  ]
  node [
    id 454
    label "niesympatycznie"
  ]
  node [
    id 455
    label "Dent"
  ]
  node [
    id 456
    label "du"
  ]
  node [
    id 457
    label "Geant"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 455
    target 456
  ]
  edge [
    source 455
    target 457
  ]
  edge [
    source 456
    target 457
  ]
]
