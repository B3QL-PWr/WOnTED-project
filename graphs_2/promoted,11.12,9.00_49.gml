graph [
  node [
    id 0
    label "klinika"
    origin "text"
  ]
  node [
    id 1
    label "budzik"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "doros&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "olsztyn"
    origin "text"
  ]
  node [
    id 5
    label "wybudzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "dziesi&#261;ty"
    origin "text"
  ]
  node [
    id 8
    label "pacjent"
    origin "text"
  ]
  node [
    id 9
    label "centrum_urazowe"
  ]
  node [
    id 10
    label "kostnica"
  ]
  node [
    id 11
    label "izba_chorych"
  ]
  node [
    id 12
    label "instytucja"
  ]
  node [
    id 13
    label "oddzia&#322;_septyczny"
  ]
  node [
    id 14
    label "klinicysta"
  ]
  node [
    id 15
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 16
    label "oddzia&#322;"
  ]
  node [
    id 17
    label "blok_operacyjny"
  ]
  node [
    id 18
    label "zabieg&#243;wka"
  ]
  node [
    id 19
    label "sala_chorych"
  ]
  node [
    id 20
    label "&#322;&#243;&#380;eczko_nadziei"
  ]
  node [
    id 21
    label "szpitalnictwo"
  ]
  node [
    id 22
    label "osoba_prawna"
  ]
  node [
    id 23
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 24
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 25
    label "poj&#281;cie"
  ]
  node [
    id 26
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 27
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 28
    label "biuro"
  ]
  node [
    id 29
    label "organizacja"
  ]
  node [
    id 30
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 31
    label "Fundusze_Unijne"
  ]
  node [
    id 32
    label "zamyka&#263;"
  ]
  node [
    id 33
    label "establishment"
  ]
  node [
    id 34
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 35
    label "urz&#261;d"
  ]
  node [
    id 36
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 37
    label "afiliowa&#263;"
  ]
  node [
    id 38
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 39
    label "standard"
  ]
  node [
    id 40
    label "zamykanie"
  ]
  node [
    id 41
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 42
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 43
    label "szpital"
  ]
  node [
    id 44
    label "gabinet"
  ]
  node [
    id 45
    label "zesp&#243;&#322;"
  ]
  node [
    id 46
    label "lias"
  ]
  node [
    id 47
    label "dzia&#322;"
  ]
  node [
    id 48
    label "system"
  ]
  node [
    id 49
    label "jednostka"
  ]
  node [
    id 50
    label "pi&#281;tro"
  ]
  node [
    id 51
    label "klasa"
  ]
  node [
    id 52
    label "jednostka_geologiczna"
  ]
  node [
    id 53
    label "filia"
  ]
  node [
    id 54
    label "malm"
  ]
  node [
    id 55
    label "whole"
  ]
  node [
    id 56
    label "dogger"
  ]
  node [
    id 57
    label "poziom"
  ]
  node [
    id 58
    label "promocja"
  ]
  node [
    id 59
    label "kurs"
  ]
  node [
    id 60
    label "bank"
  ]
  node [
    id 61
    label "formacja"
  ]
  node [
    id 62
    label "ajencja"
  ]
  node [
    id 63
    label "wojsko"
  ]
  node [
    id 64
    label "siedziba"
  ]
  node [
    id 65
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 66
    label "agencja"
  ]
  node [
    id 67
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 68
    label "trupiarnia"
  ]
  node [
    id 69
    label "opieka_medyczna"
  ]
  node [
    id 70
    label "lekarz"
  ]
  node [
    id 71
    label "ekscytarz"
  ]
  node [
    id 72
    label "zegar"
  ]
  node [
    id 73
    label "godzinnik"
  ]
  node [
    id 74
    label "bicie"
  ]
  node [
    id 75
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 76
    label "wahad&#322;o"
  ]
  node [
    id 77
    label "kurant"
  ]
  node [
    id 78
    label "cyferblat"
  ]
  node [
    id 79
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 80
    label "nabicie"
  ]
  node [
    id 81
    label "werk"
  ]
  node [
    id 82
    label "czasomierz"
  ]
  node [
    id 83
    label "tyka&#263;"
  ]
  node [
    id 84
    label "tykn&#261;&#263;"
  ]
  node [
    id 85
    label "czas"
  ]
  node [
    id 86
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 87
    label "urz&#261;dzenie"
  ]
  node [
    id 88
    label "kotwica"
  ]
  node [
    id 89
    label "obudzi&#263;"
  ]
  node [
    id 90
    label "prompt"
  ]
  node [
    id 91
    label "wyrwa&#263;"
  ]
  node [
    id 92
    label "wywo&#322;a&#263;"
  ]
  node [
    id 93
    label "arouse"
  ]
  node [
    id 94
    label "dzie&#324;"
  ]
  node [
    id 95
    label "ranek"
  ]
  node [
    id 96
    label "doba"
  ]
  node [
    id 97
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 98
    label "noc"
  ]
  node [
    id 99
    label "podwiecz&#243;r"
  ]
  node [
    id 100
    label "po&#322;udnie"
  ]
  node [
    id 101
    label "godzina"
  ]
  node [
    id 102
    label "przedpo&#322;udnie"
  ]
  node [
    id 103
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 104
    label "long_time"
  ]
  node [
    id 105
    label "wiecz&#243;r"
  ]
  node [
    id 106
    label "t&#322;usty_czwartek"
  ]
  node [
    id 107
    label "popo&#322;udnie"
  ]
  node [
    id 108
    label "walentynki"
  ]
  node [
    id 109
    label "czynienie_si&#281;"
  ]
  node [
    id 110
    label "s&#322;o&#324;ce"
  ]
  node [
    id 111
    label "rano"
  ]
  node [
    id 112
    label "tydzie&#324;"
  ]
  node [
    id 113
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 114
    label "wzej&#347;cie"
  ]
  node [
    id 115
    label "wsta&#263;"
  ]
  node [
    id 116
    label "day"
  ]
  node [
    id 117
    label "termin"
  ]
  node [
    id 118
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 119
    label "wstanie"
  ]
  node [
    id 120
    label "przedwiecz&#243;r"
  ]
  node [
    id 121
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 122
    label "Sylwester"
  ]
  node [
    id 123
    label "cz&#322;owiek"
  ]
  node [
    id 124
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 125
    label "klient"
  ]
  node [
    id 126
    label "przypadek"
  ]
  node [
    id 127
    label "piel&#281;gniarz"
  ]
  node [
    id 128
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 129
    label "od&#322;&#261;czanie"
  ]
  node [
    id 130
    label "chory"
  ]
  node [
    id 131
    label "od&#322;&#261;czenie"
  ]
  node [
    id 132
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 133
    label "szpitalnik"
  ]
  node [
    id 134
    label "ludzko&#347;&#263;"
  ]
  node [
    id 135
    label "asymilowanie"
  ]
  node [
    id 136
    label "wapniak"
  ]
  node [
    id 137
    label "asymilowa&#263;"
  ]
  node [
    id 138
    label "os&#322;abia&#263;"
  ]
  node [
    id 139
    label "posta&#263;"
  ]
  node [
    id 140
    label "hominid"
  ]
  node [
    id 141
    label "podw&#322;adny"
  ]
  node [
    id 142
    label "os&#322;abianie"
  ]
  node [
    id 143
    label "g&#322;owa"
  ]
  node [
    id 144
    label "figura"
  ]
  node [
    id 145
    label "portrecista"
  ]
  node [
    id 146
    label "dwun&#243;g"
  ]
  node [
    id 147
    label "profanum"
  ]
  node [
    id 148
    label "mikrokosmos"
  ]
  node [
    id 149
    label "nasada"
  ]
  node [
    id 150
    label "duch"
  ]
  node [
    id 151
    label "antropochoria"
  ]
  node [
    id 152
    label "osoba"
  ]
  node [
    id 153
    label "wz&#243;r"
  ]
  node [
    id 154
    label "senior"
  ]
  node [
    id 155
    label "oddzia&#322;ywanie"
  ]
  node [
    id 156
    label "Adam"
  ]
  node [
    id 157
    label "homo_sapiens"
  ]
  node [
    id 158
    label "polifag"
  ]
  node [
    id 159
    label "agent_rozliczeniowy"
  ]
  node [
    id 160
    label "komputer_cyfrowy"
  ]
  node [
    id 161
    label "us&#322;ugobiorca"
  ]
  node [
    id 162
    label "Rzymianin"
  ]
  node [
    id 163
    label "szlachcic"
  ]
  node [
    id 164
    label "obywatel"
  ]
  node [
    id 165
    label "klientela"
  ]
  node [
    id 166
    label "bratek"
  ]
  node [
    id 167
    label "program"
  ]
  node [
    id 168
    label "krzy&#380;owiec"
  ]
  node [
    id 169
    label "tytu&#322;"
  ]
  node [
    id 170
    label "szpitalnicy"
  ]
  node [
    id 171
    label "kawaler"
  ]
  node [
    id 172
    label "zakonnik"
  ]
  node [
    id 173
    label "wydarzenie"
  ]
  node [
    id 174
    label "happening"
  ]
  node [
    id 175
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 176
    label "schorzenie"
  ]
  node [
    id 177
    label "przyk&#322;ad"
  ]
  node [
    id 178
    label "kategoria_gramatyczna"
  ]
  node [
    id 179
    label "przeznaczenie"
  ]
  node [
    id 180
    label "cutoff"
  ]
  node [
    id 181
    label "od&#322;&#261;czony"
  ]
  node [
    id 182
    label "odbicie"
  ]
  node [
    id 183
    label "przerwanie"
  ]
  node [
    id 184
    label "odci&#281;cie"
  ]
  node [
    id 185
    label "release"
  ]
  node [
    id 186
    label "ablation"
  ]
  node [
    id 187
    label "oddzielenie"
  ]
  node [
    id 188
    label "severance"
  ]
  node [
    id 189
    label "odcinanie"
  ]
  node [
    id 190
    label "oddzielanie"
  ]
  node [
    id 191
    label "odbijanie"
  ]
  node [
    id 192
    label "przerywanie"
  ]
  node [
    id 193
    label "rupture"
  ]
  node [
    id 194
    label "dissociation"
  ]
  node [
    id 195
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 196
    label "take"
  ]
  node [
    id 197
    label "przerywa&#263;"
  ]
  node [
    id 198
    label "odcina&#263;"
  ]
  node [
    id 199
    label "abstract"
  ]
  node [
    id 200
    label "oddziela&#263;"
  ]
  node [
    id 201
    label "challenge"
  ]
  node [
    id 202
    label "publish"
  ]
  node [
    id 203
    label "separate"
  ]
  node [
    id 204
    label "oddzieli&#263;"
  ]
  node [
    id 205
    label "odci&#261;&#263;"
  ]
  node [
    id 206
    label "amputate"
  ]
  node [
    id 207
    label "przerwa&#263;"
  ]
  node [
    id 208
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 209
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 210
    label "choro"
  ]
  node [
    id 211
    label "nieprzytomny"
  ]
  node [
    id 212
    label "skandaliczny"
  ]
  node [
    id 213
    label "chor&#243;bka"
  ]
  node [
    id 214
    label "nienormalny"
  ]
  node [
    id 215
    label "niezdrowy"
  ]
  node [
    id 216
    label "w&#347;ciek&#322;y"
  ]
  node [
    id 217
    label "niezrozumia&#322;y"
  ]
  node [
    id 218
    label "chorowanie"
  ]
  node [
    id 219
    label "le&#380;alnia"
  ]
  node [
    id 220
    label "psychiczny"
  ]
  node [
    id 221
    label "zachorowanie"
  ]
  node [
    id 222
    label "rozchorowywanie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
]
