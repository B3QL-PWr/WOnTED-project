graph [
  node [
    id 0
    label "minione"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "prezes"
    origin "text"
  ]
  node [
    id 3
    label "tauronu"
    origin "text"
  ]
  node [
    id 4
    label "pge"
    origin "text"
  ]
  node [
    id 5
    label "energi"
    origin "text"
  ]
  node [
    id 6
    label "enei"
    origin "text"
  ]
  node [
    id 7
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "premier"
    origin "text"
  ]
  node [
    id 10
    label "mateusz"
    origin "text"
  ]
  node [
    id 11
    label "morawieckim"
    origin "text"
  ]
  node [
    id 12
    label "doba"
  ]
  node [
    id 13
    label "weekend"
  ]
  node [
    id 14
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 15
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "miesi&#261;c"
  ]
  node [
    id 18
    label "poprzedzanie"
  ]
  node [
    id 19
    label "czasoprzestrze&#324;"
  ]
  node [
    id 20
    label "laba"
  ]
  node [
    id 21
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 22
    label "chronometria"
  ]
  node [
    id 23
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 24
    label "rachuba_czasu"
  ]
  node [
    id 25
    label "przep&#322;ywanie"
  ]
  node [
    id 26
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 27
    label "czasokres"
  ]
  node [
    id 28
    label "odczyt"
  ]
  node [
    id 29
    label "chwila"
  ]
  node [
    id 30
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 31
    label "dzieje"
  ]
  node [
    id 32
    label "kategoria_gramatyczna"
  ]
  node [
    id 33
    label "poprzedzenie"
  ]
  node [
    id 34
    label "trawienie"
  ]
  node [
    id 35
    label "pochodzi&#263;"
  ]
  node [
    id 36
    label "period"
  ]
  node [
    id 37
    label "okres_czasu"
  ]
  node [
    id 38
    label "poprzedza&#263;"
  ]
  node [
    id 39
    label "schy&#322;ek"
  ]
  node [
    id 40
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 41
    label "odwlekanie_si&#281;"
  ]
  node [
    id 42
    label "zegar"
  ]
  node [
    id 43
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 44
    label "czwarty_wymiar"
  ]
  node [
    id 45
    label "pochodzenie"
  ]
  node [
    id 46
    label "koniugacja"
  ]
  node [
    id 47
    label "Zeitgeist"
  ]
  node [
    id 48
    label "trawi&#263;"
  ]
  node [
    id 49
    label "pogoda"
  ]
  node [
    id 50
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 51
    label "poprzedzi&#263;"
  ]
  node [
    id 52
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 53
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 54
    label "time_period"
  ]
  node [
    id 55
    label "noc"
  ]
  node [
    id 56
    label "dzie&#324;"
  ]
  node [
    id 57
    label "godzina"
  ]
  node [
    id 58
    label "long_time"
  ]
  node [
    id 59
    label "jednostka_geologiczna"
  ]
  node [
    id 60
    label "niedziela"
  ]
  node [
    id 61
    label "sobota"
  ]
  node [
    id 62
    label "miech"
  ]
  node [
    id 63
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 64
    label "rok"
  ]
  node [
    id 65
    label "kalendy"
  ]
  node [
    id 66
    label "gruba_ryba"
  ]
  node [
    id 67
    label "zwierzchnik"
  ]
  node [
    id 68
    label "pryncypa&#322;"
  ]
  node [
    id 69
    label "kierowa&#263;"
  ]
  node [
    id 70
    label "kierownictwo"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 73
    label "insert"
  ]
  node [
    id 74
    label "visualize"
  ]
  node [
    id 75
    label "pozna&#263;"
  ]
  node [
    id 76
    label "befall"
  ]
  node [
    id 77
    label "spowodowa&#263;"
  ]
  node [
    id 78
    label "go_steady"
  ]
  node [
    id 79
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 80
    label "znale&#378;&#263;"
  ]
  node [
    id 81
    label "act"
  ]
  node [
    id 82
    label "pozyska&#263;"
  ]
  node [
    id 83
    label "oceni&#263;"
  ]
  node [
    id 84
    label "devise"
  ]
  node [
    id 85
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 86
    label "dozna&#263;"
  ]
  node [
    id 87
    label "wykry&#263;"
  ]
  node [
    id 88
    label "odzyska&#263;"
  ]
  node [
    id 89
    label "znaj&#347;&#263;"
  ]
  node [
    id 90
    label "invent"
  ]
  node [
    id 91
    label "wymy&#347;li&#263;"
  ]
  node [
    id 92
    label "zrozumie&#263;"
  ]
  node [
    id 93
    label "feel"
  ]
  node [
    id 94
    label "topographic_point"
  ]
  node [
    id 95
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 96
    label "przyswoi&#263;"
  ]
  node [
    id 97
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 98
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 99
    label "teach"
  ]
  node [
    id 100
    label "experience"
  ]
  node [
    id 101
    label "rz&#261;d"
  ]
  node [
    id 102
    label "Bismarck"
  ]
  node [
    id 103
    label "Sto&#322;ypin"
  ]
  node [
    id 104
    label "Miko&#322;ajczyk"
  ]
  node [
    id 105
    label "Chruszczow"
  ]
  node [
    id 106
    label "Jelcyn"
  ]
  node [
    id 107
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 108
    label "dostojnik"
  ]
  node [
    id 109
    label "w&#322;adza"
  ]
  node [
    id 110
    label "urz&#281;dnik"
  ]
  node [
    id 111
    label "notabl"
  ]
  node [
    id 112
    label "oficja&#322;"
  ]
  node [
    id 113
    label "przybli&#380;enie"
  ]
  node [
    id 114
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 115
    label "kategoria"
  ]
  node [
    id 116
    label "szpaler"
  ]
  node [
    id 117
    label "lon&#380;a"
  ]
  node [
    id 118
    label "uporz&#261;dkowanie"
  ]
  node [
    id 119
    label "egzekutywa"
  ]
  node [
    id 120
    label "jednostka_systematyczna"
  ]
  node [
    id 121
    label "instytucja"
  ]
  node [
    id 122
    label "Londyn"
  ]
  node [
    id 123
    label "gabinet_cieni"
  ]
  node [
    id 124
    label "gromada"
  ]
  node [
    id 125
    label "number"
  ]
  node [
    id 126
    label "Konsulat"
  ]
  node [
    id 127
    label "tract"
  ]
  node [
    id 128
    label "klasa"
  ]
  node [
    id 129
    label "Mateusz"
  ]
  node [
    id 130
    label "Morawieckim"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 129
    target 130
  ]
]
