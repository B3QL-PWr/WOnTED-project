graph [
  node [
    id 0
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 1
    label "bezplanu"
    origin "text"
  ]
  node [
    id 2
    label "zabiera&#263;"
    origin "text"
  ]
  node [
    id 3
    label "petare"
    origin "text"
  ]
  node [
    id 4
    label "przedmie&#347;cie"
    origin "text"
  ]
  node [
    id 5
    label "caracas"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "slams"
    origin "text"
  ]
  node [
    id 8
    label "ameryka"
    origin "text"
  ]
  node [
    id 9
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 12
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 13
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 14
    label "teraz"
  ]
  node [
    id 15
    label "czas"
  ]
  node [
    id 16
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 17
    label "jednocze&#347;nie"
  ]
  node [
    id 18
    label "tydzie&#324;"
  ]
  node [
    id 19
    label "noc"
  ]
  node [
    id 20
    label "dzie&#324;"
  ]
  node [
    id 21
    label "godzina"
  ]
  node [
    id 22
    label "long_time"
  ]
  node [
    id 23
    label "jednostka_geologiczna"
  ]
  node [
    id 24
    label "zajmowa&#263;"
  ]
  node [
    id 25
    label "poci&#261;ga&#263;"
  ]
  node [
    id 26
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 27
    label "fall"
  ]
  node [
    id 28
    label "liszy&#263;"
  ]
  node [
    id 29
    label "&#322;apa&#263;"
  ]
  node [
    id 30
    label "przesuwa&#263;"
  ]
  node [
    id 31
    label "prowadzi&#263;"
  ]
  node [
    id 32
    label "blurt_out"
  ]
  node [
    id 33
    label "konfiskowa&#263;"
  ]
  node [
    id 34
    label "deprive"
  ]
  node [
    id 35
    label "abstract"
  ]
  node [
    id 36
    label "przenosi&#263;"
  ]
  node [
    id 37
    label "pull"
  ]
  node [
    id 38
    label "upija&#263;"
  ]
  node [
    id 39
    label "wsysa&#263;"
  ]
  node [
    id 40
    label "przechyla&#263;"
  ]
  node [
    id 41
    label "pokrywa&#263;"
  ]
  node [
    id 42
    label "rusza&#263;"
  ]
  node [
    id 43
    label "trail"
  ]
  node [
    id 44
    label "skutkowa&#263;"
  ]
  node [
    id 45
    label "powodowa&#263;"
  ]
  node [
    id 46
    label "nos"
  ]
  node [
    id 47
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 48
    label "powiewa&#263;"
  ]
  node [
    id 49
    label "katar"
  ]
  node [
    id 50
    label "mani&#263;"
  ]
  node [
    id 51
    label "force"
  ]
  node [
    id 52
    label "dostarcza&#263;"
  ]
  node [
    id 53
    label "robi&#263;"
  ]
  node [
    id 54
    label "korzysta&#263;"
  ]
  node [
    id 55
    label "schorzenie"
  ]
  node [
    id 56
    label "komornik"
  ]
  node [
    id 57
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "return"
  ]
  node [
    id 59
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 60
    label "trwa&#263;"
  ]
  node [
    id 61
    label "bra&#263;"
  ]
  node [
    id 62
    label "rozciekawia&#263;"
  ]
  node [
    id 63
    label "klasyfikacja"
  ]
  node [
    id 64
    label "zadawa&#263;"
  ]
  node [
    id 65
    label "fill"
  ]
  node [
    id 66
    label "topographic_point"
  ]
  node [
    id 67
    label "obejmowa&#263;"
  ]
  node [
    id 68
    label "pali&#263;_si&#281;"
  ]
  node [
    id 69
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 70
    label "aim"
  ]
  node [
    id 71
    label "anektowa&#263;"
  ]
  node [
    id 72
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 73
    label "prosecute"
  ]
  node [
    id 74
    label "sake"
  ]
  node [
    id 75
    label "do"
  ]
  node [
    id 76
    label "dostosowywa&#263;"
  ]
  node [
    id 77
    label "estrange"
  ]
  node [
    id 78
    label "transfer"
  ]
  node [
    id 79
    label "translate"
  ]
  node [
    id 80
    label "go"
  ]
  node [
    id 81
    label "zmienia&#263;"
  ]
  node [
    id 82
    label "postpone"
  ]
  node [
    id 83
    label "przestawia&#263;"
  ]
  node [
    id 84
    label "kopiowa&#263;"
  ]
  node [
    id 85
    label "ponosi&#263;"
  ]
  node [
    id 86
    label "rozpowszechnia&#263;"
  ]
  node [
    id 87
    label "move"
  ]
  node [
    id 88
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 89
    label "circulate"
  ]
  node [
    id 90
    label "pocisk"
  ]
  node [
    id 91
    label "przemieszcza&#263;"
  ]
  node [
    id 92
    label "wytrzyma&#263;"
  ]
  node [
    id 93
    label "umieszcza&#263;"
  ]
  node [
    id 94
    label "przelatywa&#263;"
  ]
  node [
    id 95
    label "infest"
  ]
  node [
    id 96
    label "strzela&#263;"
  ]
  node [
    id 97
    label "&#380;y&#263;"
  ]
  node [
    id 98
    label "kierowa&#263;"
  ]
  node [
    id 99
    label "g&#243;rowa&#263;"
  ]
  node [
    id 100
    label "tworzy&#263;"
  ]
  node [
    id 101
    label "krzywa"
  ]
  node [
    id 102
    label "linia_melodyczna"
  ]
  node [
    id 103
    label "control"
  ]
  node [
    id 104
    label "string"
  ]
  node [
    id 105
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 106
    label "ukierunkowywa&#263;"
  ]
  node [
    id 107
    label "sterowa&#263;"
  ]
  node [
    id 108
    label "kre&#347;li&#263;"
  ]
  node [
    id 109
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 110
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 111
    label "message"
  ]
  node [
    id 112
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 113
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 114
    label "eksponowa&#263;"
  ]
  node [
    id 115
    label "navigate"
  ]
  node [
    id 116
    label "manipulate"
  ]
  node [
    id 117
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 118
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 119
    label "partner"
  ]
  node [
    id 120
    label "prowadzenie"
  ]
  node [
    id 121
    label "milcze&#263;"
  ]
  node [
    id 122
    label "zostawia&#263;"
  ]
  node [
    id 123
    label "pozostawi&#263;"
  ]
  node [
    id 124
    label "take"
  ]
  node [
    id 125
    label "ujmowa&#263;"
  ]
  node [
    id 126
    label "d&#322;o&#324;"
  ]
  node [
    id 127
    label "rozumie&#263;"
  ]
  node [
    id 128
    label "get"
  ]
  node [
    id 129
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 130
    label "cope"
  ]
  node [
    id 131
    label "ogarnia&#263;"
  ]
  node [
    id 132
    label "zara&#380;a&#263;_si&#281;"
  ]
  node [
    id 133
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 134
    label "obrze&#380;e"
  ]
  node [
    id 135
    label "Rzym_Zachodni"
  ]
  node [
    id 136
    label "whole"
  ]
  node [
    id 137
    label "ilo&#347;&#263;"
  ]
  node [
    id 138
    label "element"
  ]
  node [
    id 139
    label "Rzym_Wschodni"
  ]
  node [
    id 140
    label "urz&#261;dzenie"
  ]
  node [
    id 141
    label "brzeg"
  ]
  node [
    id 142
    label "granica"
  ]
  node [
    id 143
    label "doros&#322;y"
  ]
  node [
    id 144
    label "znaczny"
  ]
  node [
    id 145
    label "niema&#322;o"
  ]
  node [
    id 146
    label "wiele"
  ]
  node [
    id 147
    label "rozwini&#281;ty"
  ]
  node [
    id 148
    label "dorodny"
  ]
  node [
    id 149
    label "wa&#380;ny"
  ]
  node [
    id 150
    label "prawdziwy"
  ]
  node [
    id 151
    label "du&#380;o"
  ]
  node [
    id 152
    label "&#380;ywny"
  ]
  node [
    id 153
    label "szczery"
  ]
  node [
    id 154
    label "naturalny"
  ]
  node [
    id 155
    label "naprawd&#281;"
  ]
  node [
    id 156
    label "realnie"
  ]
  node [
    id 157
    label "podobny"
  ]
  node [
    id 158
    label "zgodny"
  ]
  node [
    id 159
    label "m&#261;dry"
  ]
  node [
    id 160
    label "prawdziwie"
  ]
  node [
    id 161
    label "znacznie"
  ]
  node [
    id 162
    label "zauwa&#380;alny"
  ]
  node [
    id 163
    label "wynios&#322;y"
  ]
  node [
    id 164
    label "dono&#347;ny"
  ]
  node [
    id 165
    label "silny"
  ]
  node [
    id 166
    label "wa&#380;nie"
  ]
  node [
    id 167
    label "istotnie"
  ]
  node [
    id 168
    label "eksponowany"
  ]
  node [
    id 169
    label "dobry"
  ]
  node [
    id 170
    label "ukszta&#322;towany"
  ]
  node [
    id 171
    label "do&#347;cig&#322;y"
  ]
  node [
    id 172
    label "&#378;ra&#322;y"
  ]
  node [
    id 173
    label "zdr&#243;w"
  ]
  node [
    id 174
    label "dorodnie"
  ]
  node [
    id 175
    label "okaza&#322;y"
  ]
  node [
    id 176
    label "mocno"
  ]
  node [
    id 177
    label "wiela"
  ]
  node [
    id 178
    label "bardzo"
  ]
  node [
    id 179
    label "cz&#281;sto"
  ]
  node [
    id 180
    label "wydoro&#347;lenie"
  ]
  node [
    id 181
    label "cz&#322;owiek"
  ]
  node [
    id 182
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 183
    label "doro&#347;lenie"
  ]
  node [
    id 184
    label "doro&#347;le"
  ]
  node [
    id 185
    label "senior"
  ]
  node [
    id 186
    label "dojrzale"
  ]
  node [
    id 187
    label "wapniak"
  ]
  node [
    id 188
    label "dojrza&#322;y"
  ]
  node [
    id 189
    label "doletni"
  ]
  node [
    id 190
    label "dom"
  ]
  node [
    id 191
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 192
    label "rodzina"
  ]
  node [
    id 193
    label "substancja_mieszkaniowa"
  ]
  node [
    id 194
    label "instytucja"
  ]
  node [
    id 195
    label "siedziba"
  ]
  node [
    id 196
    label "dom_rodzinny"
  ]
  node [
    id 197
    label "budynek"
  ]
  node [
    id 198
    label "grupa"
  ]
  node [
    id 199
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 200
    label "poj&#281;cie"
  ]
  node [
    id 201
    label "stead"
  ]
  node [
    id 202
    label "garderoba"
  ]
  node [
    id 203
    label "wiecha"
  ]
  node [
    id 204
    label "fratria"
  ]
  node [
    id 205
    label "gor&#261;cy"
  ]
  node [
    id 206
    label "s&#322;oneczny"
  ]
  node [
    id 207
    label "po&#322;udniowo"
  ]
  node [
    id 208
    label "charakterystycznie"
  ]
  node [
    id 209
    label "stresogenny"
  ]
  node [
    id 210
    label "rozpalenie_si&#281;"
  ]
  node [
    id 211
    label "zdecydowany"
  ]
  node [
    id 212
    label "sensacyjny"
  ]
  node [
    id 213
    label "na_gor&#261;co"
  ]
  node [
    id 214
    label "rozpalanie_si&#281;"
  ]
  node [
    id 215
    label "&#380;arki"
  ]
  node [
    id 216
    label "serdeczny"
  ]
  node [
    id 217
    label "ciep&#322;y"
  ]
  node [
    id 218
    label "g&#322;&#281;boki"
  ]
  node [
    id 219
    label "gor&#261;co"
  ]
  node [
    id 220
    label "seksowny"
  ]
  node [
    id 221
    label "&#347;wie&#380;y"
  ]
  node [
    id 222
    label "s&#322;onecznie"
  ]
  node [
    id 223
    label "letni"
  ]
  node [
    id 224
    label "weso&#322;y"
  ]
  node [
    id 225
    label "bezdeszczowy"
  ]
  node [
    id 226
    label "bezchmurny"
  ]
  node [
    id 227
    label "pogodny"
  ]
  node [
    id 228
    label "fotowoltaiczny"
  ]
  node [
    id 229
    label "jasny"
  ]
  node [
    id 230
    label "Ameryka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
]
