graph [
  node [
    id 0
    label "nastolatek"
    origin "text"
  ]
  node [
    id 1
    label "nowa"
    origin "text"
  ]
  node [
    id 2
    label "skalmierzyce"
    origin "text"
  ]
  node [
    id 3
    label "woj"
    origin "text"
  ]
  node [
    id 4
    label "wielkopolski"
    origin "text"
  ]
  node [
    id 5
    label "zabi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "swoje"
    origin "text"
  ]
  node [
    id 7
    label "letni"
    origin "text"
  ]
  node [
    id 8
    label "ojczym"
    origin "text"
  ]
  node [
    id 9
    label "dorastaj&#261;cy"
  ]
  node [
    id 10
    label "m&#322;odzieniec"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "junior"
  ]
  node [
    id 13
    label "kawaler"
  ]
  node [
    id 14
    label "junak"
  ]
  node [
    id 15
    label "m&#322;odzie&#380;"
  ]
  node [
    id 16
    label "mo&#322;ojec"
  ]
  node [
    id 17
    label "m&#322;ody"
  ]
  node [
    id 18
    label "gwiazda"
  ]
  node [
    id 19
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 20
    label "Arktur"
  ]
  node [
    id 21
    label "kszta&#322;t"
  ]
  node [
    id 22
    label "Gwiazda_Polarna"
  ]
  node [
    id 23
    label "agregatka"
  ]
  node [
    id 24
    label "gromada"
  ]
  node [
    id 25
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 26
    label "S&#322;o&#324;ce"
  ]
  node [
    id 27
    label "Nibiru"
  ]
  node [
    id 28
    label "konstelacja"
  ]
  node [
    id 29
    label "ornament"
  ]
  node [
    id 30
    label "delta_Scuti"
  ]
  node [
    id 31
    label "&#347;wiat&#322;o"
  ]
  node [
    id 32
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 33
    label "obiekt"
  ]
  node [
    id 34
    label "s&#322;awa"
  ]
  node [
    id 35
    label "promie&#324;"
  ]
  node [
    id 36
    label "star"
  ]
  node [
    id 37
    label "gwiazdosz"
  ]
  node [
    id 38
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 39
    label "asocjacja_gwiazd"
  ]
  node [
    id 40
    label "supergrupa"
  ]
  node [
    id 41
    label "&#380;o&#322;nierz"
  ]
  node [
    id 42
    label "wite&#378;"
  ]
  node [
    id 43
    label "wojownik"
  ]
  node [
    id 44
    label "osada"
  ]
  node [
    id 45
    label "walcz&#261;cy"
  ]
  node [
    id 46
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 47
    label "harcap"
  ]
  node [
    id 48
    label "wojsko"
  ]
  node [
    id 49
    label "elew"
  ]
  node [
    id 50
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 51
    label "demobilizowanie"
  ]
  node [
    id 52
    label "demobilizowa&#263;"
  ]
  node [
    id 53
    label "zdemobilizowanie"
  ]
  node [
    id 54
    label "Gurkha"
  ]
  node [
    id 55
    label "so&#322;dat"
  ]
  node [
    id 56
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 57
    label "mundurowy"
  ]
  node [
    id 58
    label "rota"
  ]
  node [
    id 59
    label "zdemobilizowa&#263;"
  ]
  node [
    id 60
    label "&#380;o&#322;dowy"
  ]
  node [
    id 61
    label "motyl_dzienny"
  ]
  node [
    id 62
    label "paziowate"
  ]
  node [
    id 63
    label "kwyrla"
  ]
  node [
    id 64
    label "polski"
  ]
  node [
    id 65
    label "regionalny"
  ]
  node [
    id 66
    label "plyndz"
  ]
  node [
    id 67
    label "bimba"
  ]
  node [
    id 68
    label "knypek"
  ]
  node [
    id 69
    label "myrdyrda"
  ]
  node [
    id 70
    label "po_wielkopolsku"
  ]
  node [
    id 71
    label "hy&#263;ka"
  ]
  node [
    id 72
    label "gzik"
  ]
  node [
    id 73
    label "tradycyjny"
  ]
  node [
    id 74
    label "regionalnie"
  ]
  node [
    id 75
    label "lokalny"
  ]
  node [
    id 76
    label "typowy"
  ]
  node [
    id 77
    label "przedmiot"
  ]
  node [
    id 78
    label "Polish"
  ]
  node [
    id 79
    label "goniony"
  ]
  node [
    id 80
    label "oberek"
  ]
  node [
    id 81
    label "ryba_po_grecku"
  ]
  node [
    id 82
    label "sztajer"
  ]
  node [
    id 83
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 84
    label "krakowiak"
  ]
  node [
    id 85
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 86
    label "pierogi_ruskie"
  ]
  node [
    id 87
    label "lacki"
  ]
  node [
    id 88
    label "polak"
  ]
  node [
    id 89
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 90
    label "chodzony"
  ]
  node [
    id 91
    label "po_polsku"
  ]
  node [
    id 92
    label "mazur"
  ]
  node [
    id 93
    label "polsko"
  ]
  node [
    id 94
    label "skoczny"
  ]
  node [
    id 95
    label "drabant"
  ]
  node [
    id 96
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 97
    label "j&#281;zyk"
  ]
  node [
    id 98
    label "zasma&#380;ka"
  ]
  node [
    id 99
    label "tramwaj"
  ]
  node [
    id 100
    label "placek_ziemniaczany"
  ]
  node [
    id 101
    label "sok"
  ]
  node [
    id 102
    label "bez_czarny"
  ]
  node [
    id 103
    label "bez"
  ]
  node [
    id 104
    label "m&#261;tewka"
  ]
  node [
    id 105
    label "much&#243;wka"
  ]
  node [
    id 106
    label "twar&#243;g"
  ]
  node [
    id 107
    label "gzikowate"
  ]
  node [
    id 108
    label "n&#243;&#380;"
  ]
  node [
    id 109
    label "p&#243;&#322;_dupy_zza_krzaka"
  ]
  node [
    id 110
    label "dwarf"
  ]
  node [
    id 111
    label "karakan"
  ]
  node [
    id 112
    label "zadzwoni&#263;"
  ]
  node [
    id 113
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 114
    label "skarci&#263;"
  ]
  node [
    id 115
    label "skrzywdzi&#263;"
  ]
  node [
    id 116
    label "os&#322;oni&#263;"
  ]
  node [
    id 117
    label "przybi&#263;"
  ]
  node [
    id 118
    label "rozbroi&#263;"
  ]
  node [
    id 119
    label "uderzy&#263;"
  ]
  node [
    id 120
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 121
    label "skrzywi&#263;"
  ]
  node [
    id 122
    label "dispatch"
  ]
  node [
    id 123
    label "zmordowa&#263;"
  ]
  node [
    id 124
    label "zakry&#263;"
  ]
  node [
    id 125
    label "zbi&#263;"
  ]
  node [
    id 126
    label "zapulsowa&#263;"
  ]
  node [
    id 127
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 128
    label "break"
  ]
  node [
    id 129
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 130
    label "zastrzeli&#263;"
  ]
  node [
    id 131
    label "u&#347;mierci&#263;"
  ]
  node [
    id 132
    label "zwalczy&#263;"
  ]
  node [
    id 133
    label "pomacha&#263;"
  ]
  node [
    id 134
    label "kill"
  ]
  node [
    id 135
    label "zako&#324;czy&#263;"
  ]
  node [
    id 136
    label "zniszczy&#263;"
  ]
  node [
    id 137
    label "upomnie&#263;"
  ]
  node [
    id 138
    label "condemn"
  ]
  node [
    id 139
    label "ukara&#263;"
  ]
  node [
    id 140
    label "wybuzowa&#263;"
  ]
  node [
    id 141
    label "urazi&#263;"
  ]
  node [
    id 142
    label "strike"
  ]
  node [
    id 143
    label "wystartowa&#263;"
  ]
  node [
    id 144
    label "przywali&#263;"
  ]
  node [
    id 145
    label "dupn&#261;&#263;"
  ]
  node [
    id 146
    label "skrytykowa&#263;"
  ]
  node [
    id 147
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 148
    label "nast&#261;pi&#263;"
  ]
  node [
    id 149
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 150
    label "sztachn&#261;&#263;"
  ]
  node [
    id 151
    label "rap"
  ]
  node [
    id 152
    label "zrobi&#263;"
  ]
  node [
    id 153
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 154
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 155
    label "crush"
  ]
  node [
    id 156
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 157
    label "postara&#263;_si&#281;"
  ]
  node [
    id 158
    label "fall"
  ]
  node [
    id 159
    label "hopn&#261;&#263;"
  ]
  node [
    id 160
    label "spowodowa&#263;"
  ]
  node [
    id 161
    label "zada&#263;"
  ]
  node [
    id 162
    label "uda&#263;_si&#281;"
  ]
  node [
    id 163
    label "dotkn&#261;&#263;"
  ]
  node [
    id 164
    label "anoint"
  ]
  node [
    id 165
    label "transgress"
  ]
  node [
    id 166
    label "chop"
  ]
  node [
    id 167
    label "jebn&#261;&#263;"
  ]
  node [
    id 168
    label "lumber"
  ]
  node [
    id 169
    label "zdeformowa&#263;"
  ]
  node [
    id 170
    label "flex"
  ]
  node [
    id 171
    label "statek"
  ]
  node [
    id 172
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 173
    label "get_through"
  ]
  node [
    id 174
    label "zatwierdzi&#263;"
  ]
  node [
    id 175
    label "forge"
  ]
  node [
    id 176
    label "dobi&#263;"
  ]
  node [
    id 177
    label "ubi&#263;"
  ]
  node [
    id 178
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 179
    label "wbi&#263;"
  ]
  node [
    id 180
    label "ogarn&#261;&#263;"
  ]
  node [
    id 181
    label "przygnie&#347;&#263;"
  ]
  node [
    id 182
    label "przygn&#281;bi&#263;"
  ]
  node [
    id 183
    label "po&#347;wiadczy&#263;"
  ]
  node [
    id 184
    label "zastuka&#263;"
  ]
  node [
    id 185
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 186
    label "przyby&#263;"
  ]
  node [
    id 187
    label "uwi&#261;za&#263;"
  ]
  node [
    id 188
    label "zdecydowa&#263;"
  ]
  node [
    id 189
    label "przymocowa&#263;"
  ]
  node [
    id 190
    label "unieruchomi&#263;"
  ]
  node [
    id 191
    label "przystawi&#263;"
  ]
  node [
    id 192
    label "sta&#263;_si&#281;"
  ]
  node [
    id 193
    label "pod&#378;wiga&#263;"
  ]
  node [
    id 194
    label "ruszy&#263;"
  ]
  node [
    id 195
    label "wave"
  ]
  node [
    id 196
    label "zaora&#263;"
  ]
  node [
    id 197
    label "try"
  ]
  node [
    id 198
    label "zm&#281;czy&#263;"
  ]
  node [
    id 199
    label "torment"
  ]
  node [
    id 200
    label "znu&#380;y&#263;"
  ]
  node [
    id 201
    label "zamordowa&#263;"
  ]
  node [
    id 202
    label "os&#322;abi&#263;"
  ]
  node [
    id 203
    label "zu&#380;y&#263;"
  ]
  node [
    id 204
    label "cause"
  ]
  node [
    id 205
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 206
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 207
    label "wytworzy&#263;"
  ]
  node [
    id 208
    label "dress"
  ]
  node [
    id 209
    label "zaszkodzi&#263;"
  ]
  node [
    id 210
    label "niesprawiedliwy"
  ]
  node [
    id 211
    label "ukrzywdzi&#263;"
  ]
  node [
    id 212
    label "hurt"
  ]
  node [
    id 213
    label "wrong"
  ]
  node [
    id 214
    label "poinformowa&#263;"
  ]
  node [
    id 215
    label "nieprawda"
  ]
  node [
    id 216
    label "pokona&#263;"
  ]
  node [
    id 217
    label "overwhelm"
  ]
  node [
    id 218
    label "wygra&#263;"
  ]
  node [
    id 219
    label "kondycja_fizyczna"
  ]
  node [
    id 220
    label "spoil"
  ]
  node [
    id 221
    label "zdrowie"
  ]
  node [
    id 222
    label "consume"
  ]
  node [
    id 223
    label "pamper"
  ]
  node [
    id 224
    label "call"
  ]
  node [
    id 225
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 226
    label "jingle"
  ]
  node [
    id 227
    label "dzwonek"
  ]
  node [
    id 228
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 229
    label "sound"
  ]
  node [
    id 230
    label "zadrynda&#263;"
  ]
  node [
    id 231
    label "zabrzmie&#263;"
  ]
  node [
    id 232
    label "telefon"
  ]
  node [
    id 233
    label "nacisn&#261;&#263;"
  ]
  node [
    id 234
    label "ochroni&#263;"
  ]
  node [
    id 235
    label "obroni&#263;"
  ]
  node [
    id 236
    label "odgrodzi&#263;"
  ]
  node [
    id 237
    label "cover"
  ]
  node [
    id 238
    label "brood"
  ]
  node [
    id 239
    label "zabezpieczy&#263;"
  ]
  node [
    id 240
    label "guard"
  ]
  node [
    id 241
    label "shroud"
  ]
  node [
    id 242
    label "ukry&#263;"
  ]
  node [
    id 243
    label "report"
  ]
  node [
    id 244
    label "zas&#322;oni&#263;"
  ]
  node [
    id 245
    label "zamkn&#261;&#263;"
  ]
  node [
    id 246
    label "dispose"
  ]
  node [
    id 247
    label "zrezygnowa&#263;"
  ]
  node [
    id 248
    label "communicate"
  ]
  node [
    id 249
    label "przesta&#263;"
  ]
  node [
    id 250
    label "shoot"
  ]
  node [
    id 251
    label "stukn&#261;&#263;"
  ]
  node [
    id 252
    label "rozwali&#263;"
  ]
  node [
    id 253
    label "zaskoczy&#263;"
  ]
  node [
    id 254
    label "zabra&#263;"
  ]
  node [
    id 255
    label "roz&#322;adowa&#263;"
  ]
  node [
    id 256
    label "rozebra&#263;"
  ]
  node [
    id 257
    label "discharge"
  ]
  node [
    id 258
    label "bro&#324;"
  ]
  node [
    id 259
    label "usun&#261;&#263;"
  ]
  node [
    id 260
    label "nabi&#263;"
  ]
  node [
    id 261
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 262
    label "wy&#322;oi&#263;"
  ]
  node [
    id 263
    label "wyrobi&#263;"
  ]
  node [
    id 264
    label "obni&#380;y&#263;"
  ]
  node [
    id 265
    label "uszkodzi&#263;"
  ]
  node [
    id 266
    label "&#347;cie&#347;ni&#263;"
  ]
  node [
    id 267
    label "str&#261;ci&#263;"
  ]
  node [
    id 268
    label "zebra&#263;"
  ]
  node [
    id 269
    label "obali&#263;"
  ]
  node [
    id 270
    label "zgromadzi&#263;"
  ]
  node [
    id 271
    label "beat"
  ]
  node [
    id 272
    label "pobi&#263;"
  ]
  node [
    id 273
    label "sku&#263;"
  ]
  node [
    id 274
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 275
    label "rozbi&#263;"
  ]
  node [
    id 276
    label "podci&#261;&#263;"
  ]
  node [
    id 277
    label "write_out"
  ]
  node [
    id 278
    label "hiphopowiec"
  ]
  node [
    id 279
    label "skejt"
  ]
  node [
    id 280
    label "taniec"
  ]
  node [
    id 281
    label "latowy"
  ]
  node [
    id 282
    label "weso&#322;y"
  ]
  node [
    id 283
    label "s&#322;oneczny"
  ]
  node [
    id 284
    label "sezonowy"
  ]
  node [
    id 285
    label "ciep&#322;y"
  ]
  node [
    id 286
    label "letnio"
  ]
  node [
    id 287
    label "oboj&#281;tny"
  ]
  node [
    id 288
    label "nijaki"
  ]
  node [
    id 289
    label "nijak"
  ]
  node [
    id 290
    label "niezabawny"
  ]
  node [
    id 291
    label "&#380;aden"
  ]
  node [
    id 292
    label "zwyczajny"
  ]
  node [
    id 293
    label "poszarzenie"
  ]
  node [
    id 294
    label "neutralny"
  ]
  node [
    id 295
    label "szarzenie"
  ]
  node [
    id 296
    label "bezbarwnie"
  ]
  node [
    id 297
    label "nieciekawy"
  ]
  node [
    id 298
    label "czasowy"
  ]
  node [
    id 299
    label "sezonowo"
  ]
  node [
    id 300
    label "zoboj&#281;tnienie"
  ]
  node [
    id 301
    label "nieszkodliwy"
  ]
  node [
    id 302
    label "&#347;ni&#281;ty"
  ]
  node [
    id 303
    label "oboj&#281;tnie"
  ]
  node [
    id 304
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 305
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 306
    label "niewa&#380;ny"
  ]
  node [
    id 307
    label "neutralizowanie"
  ]
  node [
    id 308
    label "bierny"
  ]
  node [
    id 309
    label "zneutralizowanie"
  ]
  node [
    id 310
    label "pijany"
  ]
  node [
    id 311
    label "weso&#322;o"
  ]
  node [
    id 312
    label "pozytywny"
  ]
  node [
    id 313
    label "beztroski"
  ]
  node [
    id 314
    label "dobry"
  ]
  node [
    id 315
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 316
    label "typowo"
  ]
  node [
    id 317
    label "cz&#281;sty"
  ]
  node [
    id 318
    label "zwyk&#322;y"
  ]
  node [
    id 319
    label "mi&#322;y"
  ]
  node [
    id 320
    label "ocieplanie_si&#281;"
  ]
  node [
    id 321
    label "ocieplanie"
  ]
  node [
    id 322
    label "grzanie"
  ]
  node [
    id 323
    label "ocieplenie_si&#281;"
  ]
  node [
    id 324
    label "zagrzanie"
  ]
  node [
    id 325
    label "ocieplenie"
  ]
  node [
    id 326
    label "korzystny"
  ]
  node [
    id 327
    label "przyjemny"
  ]
  node [
    id 328
    label "ciep&#322;o"
  ]
  node [
    id 329
    label "s&#322;onecznie"
  ]
  node [
    id 330
    label "bezdeszczowy"
  ]
  node [
    id 331
    label "bezchmurny"
  ]
  node [
    id 332
    label "pogodny"
  ]
  node [
    id 333
    label "fotowoltaiczny"
  ]
  node [
    id 334
    label "jasny"
  ]
  node [
    id 335
    label "&#380;onaty"
  ]
  node [
    id 336
    label "ojciec"
  ]
  node [
    id 337
    label "stan_cywilny"
  ]
  node [
    id 338
    label "&#380;eniaty"
  ]
  node [
    id 339
    label "m&#261;&#380;"
  ]
  node [
    id 340
    label "kszta&#322;ciciel"
  ]
  node [
    id 341
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 342
    label "kuwada"
  ]
  node [
    id 343
    label "tworzyciel"
  ]
  node [
    id 344
    label "rodzice"
  ]
  node [
    id 345
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 346
    label "&#347;w"
  ]
  node [
    id 347
    label "pomys&#322;odawca"
  ]
  node [
    id 348
    label "rodzic"
  ]
  node [
    id 349
    label "wykonawca"
  ]
  node [
    id 350
    label "samiec"
  ]
  node [
    id 351
    label "przodek"
  ]
  node [
    id 352
    label "papa"
  ]
  node [
    id 353
    label "zakonnik"
  ]
  node [
    id 354
    label "stary"
  ]
  node [
    id 355
    label "Skalmierzyce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
]
