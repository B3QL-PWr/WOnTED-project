graph [
  node [
    id 0
    label "kondygnacja"
    origin "text"
  ]
  node [
    id 1
    label "p&#322;aszczyzna"
  ]
  node [
    id 2
    label "budynek"
  ]
  node [
    id 3
    label "wymiar"
  ]
  node [
    id 4
    label "&#347;ciana"
  ]
  node [
    id 5
    label "surface"
  ]
  node [
    id 6
    label "zakres"
  ]
  node [
    id 7
    label "kwadrant"
  ]
  node [
    id 8
    label "degree"
  ]
  node [
    id 9
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 10
    label "powierzchnia"
  ]
  node [
    id 11
    label "ukszta&#322;towanie"
  ]
  node [
    id 12
    label "cia&#322;o"
  ]
  node [
    id 13
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 14
    label "p&#322;aszczak"
  ]
  node [
    id 15
    label "balkon"
  ]
  node [
    id 16
    label "budowla"
  ]
  node [
    id 17
    label "pod&#322;oga"
  ]
  node [
    id 18
    label "skrzyd&#322;o"
  ]
  node [
    id 19
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 20
    label "dach"
  ]
  node [
    id 21
    label "strop"
  ]
  node [
    id 22
    label "klatka_schodowa"
  ]
  node [
    id 23
    label "przedpro&#380;e"
  ]
  node [
    id 24
    label "Pentagon"
  ]
  node [
    id 25
    label "alkierz"
  ]
  node [
    id 26
    label "front"
  ]
  node [
    id 27
    label "polski"
  ]
  node [
    id 28
    label "norma"
  ]
  node [
    id 29
    label "p&#243;&#322;nocny"
  ]
  node [
    id 30
    label "by&#322;y"
  ]
  node [
    id 31
    label "01025"
  ]
  node [
    id 32
    label "2004"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 32
  ]
]
