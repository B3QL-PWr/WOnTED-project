graph [
  node [
    id 0
    label "zosta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "bez"
    origin "text"
  ]
  node [
    id 2
    label "ogrzewanie"
    origin "text"
  ]
  node [
    id 3
    label "dro&#380;e&#263;"
    origin "text"
  ]
  node [
    id 4
    label "energia"
    origin "text"
  ]
  node [
    id 5
    label "kiedy"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "sko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "program"
    origin "text"
  ]
  node [
    id 10
    label "wiesti"
    origin "text"
  ]
  node [
    id 11
    label "rosyjski"
    origin "text"
  ]
  node [
    id 12
    label "marzn&#261;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 14
    label "osobnik"
    origin "text"
  ]
  node [
    id 15
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 16
    label "jako"
    origin "text"
  ]
  node [
    id 17
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 18
    label "ukrai&#324;ski"
    origin "text"
  ]
  node [
    id 19
    label "r&#243;g"
    origin "text"
  ]
  node [
    id 20
    label "kiri&#322;"
    origin "text"
  ]
  node [
    id 21
    label "czubenko"
    origin "text"
  ]
  node [
    id 22
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 23
    label "krzew"
  ]
  node [
    id 24
    label "delfinidyna"
  ]
  node [
    id 25
    label "pi&#380;maczkowate"
  ]
  node [
    id 26
    label "ki&#347;&#263;"
  ]
  node [
    id 27
    label "hy&#263;ka"
  ]
  node [
    id 28
    label "pestkowiec"
  ]
  node [
    id 29
    label "kwiat"
  ]
  node [
    id 30
    label "ro&#347;lina"
  ]
  node [
    id 31
    label "owoc"
  ]
  node [
    id 32
    label "oliwkowate"
  ]
  node [
    id 33
    label "lilac"
  ]
  node [
    id 34
    label "flakon"
  ]
  node [
    id 35
    label "przykoronek"
  ]
  node [
    id 36
    label "kielich"
  ]
  node [
    id 37
    label "dno_kwiatowe"
  ]
  node [
    id 38
    label "organ_ro&#347;linny"
  ]
  node [
    id 39
    label "ogon"
  ]
  node [
    id 40
    label "warga"
  ]
  node [
    id 41
    label "korona"
  ]
  node [
    id 42
    label "rurka"
  ]
  node [
    id 43
    label "ozdoba"
  ]
  node [
    id 44
    label "kostka"
  ]
  node [
    id 45
    label "kita"
  ]
  node [
    id 46
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 47
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 48
    label "d&#322;o&#324;"
  ]
  node [
    id 49
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 50
    label "powerball"
  ]
  node [
    id 51
    label "&#380;ubr"
  ]
  node [
    id 52
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 53
    label "p&#281;k"
  ]
  node [
    id 54
    label "r&#281;ka"
  ]
  node [
    id 55
    label "zako&#324;czenie"
  ]
  node [
    id 56
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 57
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 58
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 59
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 60
    label "&#322;yko"
  ]
  node [
    id 61
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 62
    label "karczowa&#263;"
  ]
  node [
    id 63
    label "wykarczowanie"
  ]
  node [
    id 64
    label "skupina"
  ]
  node [
    id 65
    label "wykarczowa&#263;"
  ]
  node [
    id 66
    label "karczowanie"
  ]
  node [
    id 67
    label "fanerofit"
  ]
  node [
    id 68
    label "zbiorowisko"
  ]
  node [
    id 69
    label "ro&#347;liny"
  ]
  node [
    id 70
    label "p&#281;d"
  ]
  node [
    id 71
    label "wegetowanie"
  ]
  node [
    id 72
    label "zadziorek"
  ]
  node [
    id 73
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 74
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 75
    label "do&#322;owa&#263;"
  ]
  node [
    id 76
    label "wegetacja"
  ]
  node [
    id 77
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 78
    label "strzyc"
  ]
  node [
    id 79
    label "w&#322;&#243;kno"
  ]
  node [
    id 80
    label "g&#322;uszenie"
  ]
  node [
    id 81
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 82
    label "fitotron"
  ]
  node [
    id 83
    label "bulwka"
  ]
  node [
    id 84
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 85
    label "odn&#243;&#380;ka"
  ]
  node [
    id 86
    label "epiderma"
  ]
  node [
    id 87
    label "gumoza"
  ]
  node [
    id 88
    label "strzy&#380;enie"
  ]
  node [
    id 89
    label "wypotnik"
  ]
  node [
    id 90
    label "flawonoid"
  ]
  node [
    id 91
    label "wyro&#347;le"
  ]
  node [
    id 92
    label "do&#322;owanie"
  ]
  node [
    id 93
    label "g&#322;uszy&#263;"
  ]
  node [
    id 94
    label "pora&#380;a&#263;"
  ]
  node [
    id 95
    label "fitocenoza"
  ]
  node [
    id 96
    label "hodowla"
  ]
  node [
    id 97
    label "fotoautotrof"
  ]
  node [
    id 98
    label "nieuleczalnie_chory"
  ]
  node [
    id 99
    label "wegetowa&#263;"
  ]
  node [
    id 100
    label "pochewka"
  ]
  node [
    id 101
    label "sok"
  ]
  node [
    id 102
    label "system_korzeniowy"
  ]
  node [
    id 103
    label "zawi&#261;zek"
  ]
  node [
    id 104
    label "mi&#261;&#380;sz"
  ]
  node [
    id 105
    label "frukt"
  ]
  node [
    id 106
    label "drylowanie"
  ]
  node [
    id 107
    label "produkt"
  ]
  node [
    id 108
    label "owocnia"
  ]
  node [
    id 109
    label "fruktoza"
  ]
  node [
    id 110
    label "obiekt"
  ]
  node [
    id 111
    label "gniazdo_nasienne"
  ]
  node [
    id 112
    label "rezultat"
  ]
  node [
    id 113
    label "glukoza"
  ]
  node [
    id 114
    label "pestka"
  ]
  node [
    id 115
    label "antocyjanidyn"
  ]
  node [
    id 116
    label "szczeciowce"
  ]
  node [
    id 117
    label "jasnotowce"
  ]
  node [
    id 118
    label "Oleaceae"
  ]
  node [
    id 119
    label "wielkopolski"
  ]
  node [
    id 120
    label "bez_czarny"
  ]
  node [
    id 121
    label "rozdzia&#322;"
  ]
  node [
    id 122
    label "regulator_pogodowy"
  ]
  node [
    id 123
    label "regulator_pokojowy"
  ]
  node [
    id 124
    label "instalacja"
  ]
  node [
    id 125
    label "zw&#281;glenie"
  ]
  node [
    id 126
    label "roztapianie"
  ]
  node [
    id 127
    label "spiekanie"
  ]
  node [
    id 128
    label "wypra&#380;enie"
  ]
  node [
    id 129
    label "podnoszenie"
  ]
  node [
    id 130
    label "ciep&#322;y"
  ]
  node [
    id 131
    label "heater"
  ]
  node [
    id 132
    label "wypra&#380;anie"
  ]
  node [
    id 133
    label "zw&#281;glanie"
  ]
  node [
    id 134
    label "automatyka_pogodowa"
  ]
  node [
    id 135
    label "rozgrzewanie_si&#281;"
  ]
  node [
    id 136
    label "odpuszczanie"
  ]
  node [
    id 137
    label "spieczenie"
  ]
  node [
    id 138
    label "odwadnianie"
  ]
  node [
    id 139
    label "heating"
  ]
  node [
    id 140
    label "wydarzenie"
  ]
  node [
    id 141
    label "faza"
  ]
  node [
    id 142
    label "interruption"
  ]
  node [
    id 143
    label "podzia&#322;"
  ]
  node [
    id 144
    label "podrozdzia&#322;"
  ]
  node [
    id 145
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 146
    label "fragment"
  ]
  node [
    id 147
    label "powodowanie"
  ]
  node [
    id 148
    label "liczenie"
  ]
  node [
    id 149
    label "raise"
  ]
  node [
    id 150
    label "zwi&#281;kszanie"
  ]
  node [
    id 151
    label "chwalenie"
  ]
  node [
    id 152
    label "przewr&#243;cenie"
  ]
  node [
    id 153
    label "pomaganie"
  ]
  node [
    id 154
    label "carry"
  ]
  node [
    id 155
    label "zaczynanie"
  ]
  node [
    id 156
    label "przesuwanie_si&#281;"
  ]
  node [
    id 157
    label "przemieszczanie"
  ]
  node [
    id 158
    label "rozg&#322;aszanie"
  ]
  node [
    id 159
    label "przybli&#380;anie"
  ]
  node [
    id 160
    label "elevation"
  ]
  node [
    id 161
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 162
    label "za&#322;apywanie"
  ]
  node [
    id 163
    label "odbudowywanie"
  ]
  node [
    id 164
    label "pianie"
  ]
  node [
    id 165
    label "praise"
  ]
  node [
    id 166
    label "zmienianie"
  ]
  node [
    id 167
    label "wy&#380;szy"
  ]
  node [
    id 168
    label "ulepszanie"
  ]
  node [
    id 169
    label "lift"
  ]
  node [
    id 170
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 171
    label "proces"
  ]
  node [
    id 172
    label "kompozycja"
  ]
  node [
    id 173
    label "uzbrajanie"
  ]
  node [
    id 174
    label "czynno&#347;&#263;"
  ]
  node [
    id 175
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 176
    label "mi&#322;y"
  ]
  node [
    id 177
    label "ocieplanie_si&#281;"
  ]
  node [
    id 178
    label "ocieplanie"
  ]
  node [
    id 179
    label "grzanie"
  ]
  node [
    id 180
    label "ocieplenie_si&#281;"
  ]
  node [
    id 181
    label "zagrzanie"
  ]
  node [
    id 182
    label "ocieplenie"
  ]
  node [
    id 183
    label "korzystny"
  ]
  node [
    id 184
    label "przyjemny"
  ]
  node [
    id 185
    label "ciep&#322;o"
  ]
  node [
    id 186
    label "dobry"
  ]
  node [
    id 187
    label "spalanie"
  ]
  node [
    id 188
    label "carbonization"
  ]
  node [
    id 189
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 190
    label "proces_chemiczny"
  ]
  node [
    id 191
    label "przypiekanie"
  ]
  node [
    id 192
    label "sinter"
  ]
  node [
    id 193
    label "opalanie"
  ]
  node [
    id 194
    label "sklejanie_si&#281;"
  ]
  node [
    id 195
    label "annealing"
  ]
  node [
    id 196
    label "obr&#243;bka_termiczna"
  ]
  node [
    id 197
    label "rozgrzeszanie"
  ]
  node [
    id 198
    label "absolution"
  ]
  node [
    id 199
    label "robienie"
  ]
  node [
    id 200
    label "puszczanie_p&#322;azem"
  ]
  node [
    id 201
    label "zmi&#281;kczanie"
  ]
  node [
    id 202
    label "obra&#380;anie_si&#281;"
  ]
  node [
    id 203
    label "spowodowanie"
  ]
  node [
    id 204
    label "spalenie"
  ]
  node [
    id 205
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 206
    label "stapianie"
  ]
  node [
    id 207
    label "rozlutowywanie"
  ]
  node [
    id 208
    label "fusion"
  ]
  node [
    id 209
    label "przetapianie"
  ]
  node [
    id 210
    label "przetopienie"
  ]
  node [
    id 211
    label "stopienie"
  ]
  node [
    id 212
    label "odprowadzanie"
  ]
  node [
    id 213
    label "drain"
  ]
  node [
    id 214
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 215
    label "cia&#322;o"
  ]
  node [
    id 216
    label "odci&#261;ganie"
  ]
  node [
    id 217
    label "dehydratacja"
  ]
  node [
    id 218
    label "osuszanie"
  ]
  node [
    id 219
    label "odsuwanie"
  ]
  node [
    id 220
    label "odka&#380;enie"
  ]
  node [
    id 221
    label "wysuszenie"
  ]
  node [
    id 222
    label "wysuszanie"
  ]
  node [
    id 223
    label "rozpra&#380;enie"
  ]
  node [
    id 224
    label "burn"
  ]
  node [
    id 225
    label "odka&#380;anie"
  ]
  node [
    id 226
    label "palenie"
  ]
  node [
    id 227
    label "opalenie"
  ]
  node [
    id 228
    label "sklejenie_si&#281;"
  ]
  node [
    id 229
    label "przypieczenie"
  ]
  node [
    id 230
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 231
    label "monetary_value"
  ]
  node [
    id 232
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 233
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 234
    label "egzergia"
  ]
  node [
    id 235
    label "emitowa&#263;"
  ]
  node [
    id 236
    label "kwant_energii"
  ]
  node [
    id 237
    label "szwung"
  ]
  node [
    id 238
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 239
    label "power"
  ]
  node [
    id 240
    label "zjawisko"
  ]
  node [
    id 241
    label "cecha"
  ]
  node [
    id 242
    label "emitowanie"
  ]
  node [
    id 243
    label "energy"
  ]
  node [
    id 244
    label "boski"
  ]
  node [
    id 245
    label "krajobraz"
  ]
  node [
    id 246
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 247
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 248
    label "przywidzenie"
  ]
  node [
    id 249
    label "presence"
  ]
  node [
    id 250
    label "charakter"
  ]
  node [
    id 251
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 252
    label "ton"
  ]
  node [
    id 253
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 254
    label "charakterystyka"
  ]
  node [
    id 255
    label "m&#322;ot"
  ]
  node [
    id 256
    label "znak"
  ]
  node [
    id 257
    label "drzewo"
  ]
  node [
    id 258
    label "pr&#243;ba"
  ]
  node [
    id 259
    label "attribute"
  ]
  node [
    id 260
    label "marka"
  ]
  node [
    id 261
    label "termodynamika_klasyczna"
  ]
  node [
    id 262
    label "rynek"
  ]
  node [
    id 263
    label "nadawa&#263;"
  ]
  node [
    id 264
    label "wysy&#322;a&#263;"
  ]
  node [
    id 265
    label "nada&#263;"
  ]
  node [
    id 266
    label "tembr"
  ]
  node [
    id 267
    label "air"
  ]
  node [
    id 268
    label "wydoby&#263;"
  ]
  node [
    id 269
    label "emit"
  ]
  node [
    id 270
    label "wys&#322;a&#263;"
  ]
  node [
    id 271
    label "wydzieli&#263;"
  ]
  node [
    id 272
    label "wydziela&#263;"
  ]
  node [
    id 273
    label "wprowadzi&#263;"
  ]
  node [
    id 274
    label "wydobywa&#263;"
  ]
  node [
    id 275
    label "wprowadza&#263;"
  ]
  node [
    id 276
    label "wys&#322;anie"
  ]
  node [
    id 277
    label "wysy&#322;anie"
  ]
  node [
    id 278
    label "wydzielenie"
  ]
  node [
    id 279
    label "wprowadzenie"
  ]
  node [
    id 280
    label "wydobycie"
  ]
  node [
    id 281
    label "wydzielanie"
  ]
  node [
    id 282
    label "wydobywanie"
  ]
  node [
    id 283
    label "nadawanie"
  ]
  node [
    id 284
    label "emission"
  ]
  node [
    id 285
    label "wprowadzanie"
  ]
  node [
    id 286
    label "nadanie"
  ]
  node [
    id 287
    label "issue"
  ]
  node [
    id 288
    label "zapa&#322;"
  ]
  node [
    id 289
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 290
    label "zrobi&#263;"
  ]
  node [
    id 291
    label "end"
  ]
  node [
    id 292
    label "zako&#324;czy&#263;"
  ]
  node [
    id 293
    label "communicate"
  ]
  node [
    id 294
    label "przesta&#263;"
  ]
  node [
    id 295
    label "post&#261;pi&#263;"
  ]
  node [
    id 296
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 297
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 298
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 299
    label "zorganizowa&#263;"
  ]
  node [
    id 300
    label "appoint"
  ]
  node [
    id 301
    label "wystylizowa&#263;"
  ]
  node [
    id 302
    label "cause"
  ]
  node [
    id 303
    label "przerobi&#263;"
  ]
  node [
    id 304
    label "nabra&#263;"
  ]
  node [
    id 305
    label "make"
  ]
  node [
    id 306
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 307
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 308
    label "wydali&#263;"
  ]
  node [
    id 309
    label "dispose"
  ]
  node [
    id 310
    label "zrezygnowa&#263;"
  ]
  node [
    id 311
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 312
    label "wytworzy&#263;"
  ]
  node [
    id 313
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 314
    label "coating"
  ]
  node [
    id 315
    label "drop"
  ]
  node [
    id 316
    label "leave_office"
  ]
  node [
    id 317
    label "fail"
  ]
  node [
    id 318
    label "gaworzy&#263;"
  ]
  node [
    id 319
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 320
    label "remark"
  ]
  node [
    id 321
    label "rozmawia&#263;"
  ]
  node [
    id 322
    label "wyra&#380;a&#263;"
  ]
  node [
    id 323
    label "umie&#263;"
  ]
  node [
    id 324
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 325
    label "dziama&#263;"
  ]
  node [
    id 326
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 327
    label "formu&#322;owa&#263;"
  ]
  node [
    id 328
    label "dysfonia"
  ]
  node [
    id 329
    label "express"
  ]
  node [
    id 330
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 331
    label "talk"
  ]
  node [
    id 332
    label "u&#380;ywa&#263;"
  ]
  node [
    id 333
    label "prawi&#263;"
  ]
  node [
    id 334
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 335
    label "powiada&#263;"
  ]
  node [
    id 336
    label "tell"
  ]
  node [
    id 337
    label "chew_the_fat"
  ]
  node [
    id 338
    label "say"
  ]
  node [
    id 339
    label "j&#281;zyk"
  ]
  node [
    id 340
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 341
    label "informowa&#263;"
  ]
  node [
    id 342
    label "okre&#347;la&#263;"
  ]
  node [
    id 343
    label "korzysta&#263;"
  ]
  node [
    id 344
    label "distribute"
  ]
  node [
    id 345
    label "give"
  ]
  node [
    id 346
    label "bash"
  ]
  node [
    id 347
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 348
    label "doznawa&#263;"
  ]
  node [
    id 349
    label "decydowa&#263;"
  ]
  node [
    id 350
    label "signify"
  ]
  node [
    id 351
    label "style"
  ]
  node [
    id 352
    label "powodowa&#263;"
  ]
  node [
    id 353
    label "komunikowa&#263;"
  ]
  node [
    id 354
    label "inform"
  ]
  node [
    id 355
    label "znaczy&#263;"
  ]
  node [
    id 356
    label "give_voice"
  ]
  node [
    id 357
    label "oznacza&#263;"
  ]
  node [
    id 358
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 359
    label "represent"
  ]
  node [
    id 360
    label "convey"
  ]
  node [
    id 361
    label "arouse"
  ]
  node [
    id 362
    label "robi&#263;"
  ]
  node [
    id 363
    label "determine"
  ]
  node [
    id 364
    label "work"
  ]
  node [
    id 365
    label "reakcja_chemiczna"
  ]
  node [
    id 366
    label "uwydatnia&#263;"
  ]
  node [
    id 367
    label "eksploatowa&#263;"
  ]
  node [
    id 368
    label "uzyskiwa&#263;"
  ]
  node [
    id 369
    label "wydostawa&#263;"
  ]
  node [
    id 370
    label "wyjmowa&#263;"
  ]
  node [
    id 371
    label "train"
  ]
  node [
    id 372
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 373
    label "wydawa&#263;"
  ]
  node [
    id 374
    label "dobywa&#263;"
  ]
  node [
    id 375
    label "ocala&#263;"
  ]
  node [
    id 376
    label "excavate"
  ]
  node [
    id 377
    label "g&#243;rnictwo"
  ]
  node [
    id 378
    label "wiedzie&#263;"
  ]
  node [
    id 379
    label "can"
  ]
  node [
    id 380
    label "m&#243;c"
  ]
  node [
    id 381
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 382
    label "rozumie&#263;"
  ]
  node [
    id 383
    label "szczeka&#263;"
  ]
  node [
    id 384
    label "funkcjonowa&#263;"
  ]
  node [
    id 385
    label "mawia&#263;"
  ]
  node [
    id 386
    label "opowiada&#263;"
  ]
  node [
    id 387
    label "chatter"
  ]
  node [
    id 388
    label "niemowl&#281;"
  ]
  node [
    id 389
    label "kosmetyk"
  ]
  node [
    id 390
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 391
    label "stanowisko_archeologiczne"
  ]
  node [
    id 392
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 393
    label "artykulator"
  ]
  node [
    id 394
    label "kod"
  ]
  node [
    id 395
    label "kawa&#322;ek"
  ]
  node [
    id 396
    label "przedmiot"
  ]
  node [
    id 397
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 398
    label "gramatyka"
  ]
  node [
    id 399
    label "stylik"
  ]
  node [
    id 400
    label "przet&#322;umaczenie"
  ]
  node [
    id 401
    label "formalizowanie"
  ]
  node [
    id 402
    label "ssa&#263;"
  ]
  node [
    id 403
    label "ssanie"
  ]
  node [
    id 404
    label "language"
  ]
  node [
    id 405
    label "liza&#263;"
  ]
  node [
    id 406
    label "napisa&#263;"
  ]
  node [
    id 407
    label "konsonantyzm"
  ]
  node [
    id 408
    label "wokalizm"
  ]
  node [
    id 409
    label "pisa&#263;"
  ]
  node [
    id 410
    label "fonetyka"
  ]
  node [
    id 411
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 412
    label "jeniec"
  ]
  node [
    id 413
    label "but"
  ]
  node [
    id 414
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 415
    label "po_koroniarsku"
  ]
  node [
    id 416
    label "kultura_duchowa"
  ]
  node [
    id 417
    label "t&#322;umaczenie"
  ]
  node [
    id 418
    label "m&#243;wienie"
  ]
  node [
    id 419
    label "pype&#263;"
  ]
  node [
    id 420
    label "lizanie"
  ]
  node [
    id 421
    label "pismo"
  ]
  node [
    id 422
    label "formalizowa&#263;"
  ]
  node [
    id 423
    label "organ"
  ]
  node [
    id 424
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 425
    label "rozumienie"
  ]
  node [
    id 426
    label "spos&#243;b"
  ]
  node [
    id 427
    label "makroglosja"
  ]
  node [
    id 428
    label "jama_ustna"
  ]
  node [
    id 429
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 430
    label "formacja_geologiczna"
  ]
  node [
    id 431
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 432
    label "natural_language"
  ]
  node [
    id 433
    label "s&#322;ownictwo"
  ]
  node [
    id 434
    label "urz&#261;dzenie"
  ]
  node [
    id 435
    label "dysphonia"
  ]
  node [
    id 436
    label "dysleksja"
  ]
  node [
    id 437
    label "instalowa&#263;"
  ]
  node [
    id 438
    label "oprogramowanie"
  ]
  node [
    id 439
    label "odinstalowywa&#263;"
  ]
  node [
    id 440
    label "spis"
  ]
  node [
    id 441
    label "zaprezentowanie"
  ]
  node [
    id 442
    label "podprogram"
  ]
  node [
    id 443
    label "ogranicznik_referencyjny"
  ]
  node [
    id 444
    label "course_of_study"
  ]
  node [
    id 445
    label "booklet"
  ]
  node [
    id 446
    label "dzia&#322;"
  ]
  node [
    id 447
    label "odinstalowanie"
  ]
  node [
    id 448
    label "broszura"
  ]
  node [
    id 449
    label "wytw&#243;r"
  ]
  node [
    id 450
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 451
    label "kana&#322;"
  ]
  node [
    id 452
    label "teleferie"
  ]
  node [
    id 453
    label "zainstalowanie"
  ]
  node [
    id 454
    label "struktura_organizacyjna"
  ]
  node [
    id 455
    label "pirat"
  ]
  node [
    id 456
    label "zaprezentowa&#263;"
  ]
  node [
    id 457
    label "prezentowanie"
  ]
  node [
    id 458
    label "prezentowa&#263;"
  ]
  node [
    id 459
    label "interfejs"
  ]
  node [
    id 460
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 461
    label "okno"
  ]
  node [
    id 462
    label "blok"
  ]
  node [
    id 463
    label "punkt"
  ]
  node [
    id 464
    label "folder"
  ]
  node [
    id 465
    label "zainstalowa&#263;"
  ]
  node [
    id 466
    label "za&#322;o&#380;enie"
  ]
  node [
    id 467
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 468
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 469
    label "ram&#243;wka"
  ]
  node [
    id 470
    label "tryb"
  ]
  node [
    id 471
    label "odinstalowywanie"
  ]
  node [
    id 472
    label "instrukcja"
  ]
  node [
    id 473
    label "informatyka"
  ]
  node [
    id 474
    label "deklaracja"
  ]
  node [
    id 475
    label "sekcja_krytyczna"
  ]
  node [
    id 476
    label "menu"
  ]
  node [
    id 477
    label "furkacja"
  ]
  node [
    id 478
    label "podstawa"
  ]
  node [
    id 479
    label "instalowanie"
  ]
  node [
    id 480
    label "oferta"
  ]
  node [
    id 481
    label "odinstalowa&#263;"
  ]
  node [
    id 482
    label "druk_ulotny"
  ]
  node [
    id 483
    label "wydawnictwo"
  ]
  node [
    id 484
    label "pot&#281;ga"
  ]
  node [
    id 485
    label "documentation"
  ]
  node [
    id 486
    label "column"
  ]
  node [
    id 487
    label "zasadzi&#263;"
  ]
  node [
    id 488
    label "punkt_odniesienia"
  ]
  node [
    id 489
    label "zasadzenie"
  ]
  node [
    id 490
    label "bok"
  ]
  node [
    id 491
    label "d&#243;&#322;"
  ]
  node [
    id 492
    label "dzieci&#281;ctwo"
  ]
  node [
    id 493
    label "background"
  ]
  node [
    id 494
    label "podstawowy"
  ]
  node [
    id 495
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 496
    label "strategia"
  ]
  node [
    id 497
    label "pomys&#322;"
  ]
  node [
    id 498
    label "&#347;ciana"
  ]
  node [
    id 499
    label "rozmiar"
  ]
  node [
    id 500
    label "zakres"
  ]
  node [
    id 501
    label "zasi&#261;g"
  ]
  node [
    id 502
    label "izochronizm"
  ]
  node [
    id 503
    label "bridge"
  ]
  node [
    id 504
    label "distribution"
  ]
  node [
    id 505
    label "p&#322;&#243;d"
  ]
  node [
    id 506
    label "ko&#322;o"
  ]
  node [
    id 507
    label "modalno&#347;&#263;"
  ]
  node [
    id 508
    label "z&#261;b"
  ]
  node [
    id 509
    label "kategoria_gramatyczna"
  ]
  node [
    id 510
    label "skala"
  ]
  node [
    id 511
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 512
    label "koniugacja"
  ]
  node [
    id 513
    label "offer"
  ]
  node [
    id 514
    label "propozycja"
  ]
  node [
    id 515
    label "o&#347;wiadczenie"
  ]
  node [
    id 516
    label "obietnica"
  ]
  node [
    id 517
    label "formularz"
  ]
  node [
    id 518
    label "statement"
  ]
  node [
    id 519
    label "announcement"
  ]
  node [
    id 520
    label "akt"
  ]
  node [
    id 521
    label "digest"
  ]
  node [
    id 522
    label "konstrukcja"
  ]
  node [
    id 523
    label "dokument"
  ]
  node [
    id 524
    label "o&#347;wiadczyny"
  ]
  node [
    id 525
    label "szaniec"
  ]
  node [
    id 526
    label "topologia_magistrali"
  ]
  node [
    id 527
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 528
    label "grodzisko"
  ]
  node [
    id 529
    label "tarapaty"
  ]
  node [
    id 530
    label "piaskownik"
  ]
  node [
    id 531
    label "struktura_anatomiczna"
  ]
  node [
    id 532
    label "miejsce"
  ]
  node [
    id 533
    label "bystrza"
  ]
  node [
    id 534
    label "pit"
  ]
  node [
    id 535
    label "odk&#322;ad"
  ]
  node [
    id 536
    label "chody"
  ]
  node [
    id 537
    label "klarownia"
  ]
  node [
    id 538
    label "kanalizacja"
  ]
  node [
    id 539
    label "przew&#243;d"
  ]
  node [
    id 540
    label "budowa"
  ]
  node [
    id 541
    label "ciek"
  ]
  node [
    id 542
    label "teatr"
  ]
  node [
    id 543
    label "gara&#380;"
  ]
  node [
    id 544
    label "zrzutowy"
  ]
  node [
    id 545
    label "warsztat"
  ]
  node [
    id 546
    label "syfon"
  ]
  node [
    id 547
    label "odwa&#322;"
  ]
  node [
    id 548
    label "zbi&#243;r"
  ]
  node [
    id 549
    label "catalog"
  ]
  node [
    id 550
    label "pozycja"
  ]
  node [
    id 551
    label "tekst"
  ]
  node [
    id 552
    label "sumariusz"
  ]
  node [
    id 553
    label "book"
  ]
  node [
    id 554
    label "stock"
  ]
  node [
    id 555
    label "figurowa&#263;"
  ]
  node [
    id 556
    label "wyliczanka"
  ]
  node [
    id 557
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 558
    label "HP"
  ]
  node [
    id 559
    label "dost&#281;p"
  ]
  node [
    id 560
    label "infa"
  ]
  node [
    id 561
    label "kierunek"
  ]
  node [
    id 562
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 563
    label "kryptologia"
  ]
  node [
    id 564
    label "baza_danych"
  ]
  node [
    id 565
    label "przetwarzanie_informacji"
  ]
  node [
    id 566
    label "sztuczna_inteligencja"
  ]
  node [
    id 567
    label "gramatyka_formalna"
  ]
  node [
    id 568
    label "zamek"
  ]
  node [
    id 569
    label "dziedzina_informatyki"
  ]
  node [
    id 570
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 571
    label "artefakt"
  ]
  node [
    id 572
    label "usuni&#281;cie"
  ]
  node [
    id 573
    label "parapet"
  ]
  node [
    id 574
    label "szyba"
  ]
  node [
    id 575
    label "okiennica"
  ]
  node [
    id 576
    label "prze&#347;wit"
  ]
  node [
    id 577
    label "pulpit"
  ]
  node [
    id 578
    label "transenna"
  ]
  node [
    id 579
    label "kwatera_okienna"
  ]
  node [
    id 580
    label "inspekt"
  ]
  node [
    id 581
    label "nora"
  ]
  node [
    id 582
    label "skrzyd&#322;o"
  ]
  node [
    id 583
    label "nadokiennik"
  ]
  node [
    id 584
    label "futryna"
  ]
  node [
    id 585
    label "lufcik"
  ]
  node [
    id 586
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 587
    label "casement"
  ]
  node [
    id 588
    label "menad&#380;er_okien"
  ]
  node [
    id 589
    label "otw&#243;r"
  ]
  node [
    id 590
    label "dostosowywa&#263;"
  ]
  node [
    id 591
    label "supply"
  ]
  node [
    id 592
    label "accommodate"
  ]
  node [
    id 593
    label "komputer"
  ]
  node [
    id 594
    label "umieszcza&#263;"
  ]
  node [
    id 595
    label "fit"
  ]
  node [
    id 596
    label "zdolno&#347;&#263;"
  ]
  node [
    id 597
    label "usuwa&#263;"
  ]
  node [
    id 598
    label "usuwanie"
  ]
  node [
    id 599
    label "dostosowa&#263;"
  ]
  node [
    id 600
    label "install"
  ]
  node [
    id 601
    label "umie&#347;ci&#263;"
  ]
  node [
    id 602
    label "umieszczanie"
  ]
  node [
    id 603
    label "installation"
  ]
  node [
    id 604
    label "collection"
  ]
  node [
    id 605
    label "wmontowanie"
  ]
  node [
    id 606
    label "wmontowywanie"
  ]
  node [
    id 607
    label "fitting"
  ]
  node [
    id 608
    label "dostosowywanie"
  ]
  node [
    id 609
    label "usun&#261;&#263;"
  ]
  node [
    id 610
    label "dostosowanie"
  ]
  node [
    id 611
    label "pozak&#322;adanie"
  ]
  node [
    id 612
    label "proposition"
  ]
  node [
    id 613
    label "layout"
  ]
  node [
    id 614
    label "umieszczenie"
  ]
  node [
    id 615
    label "zrobienie"
  ]
  node [
    id 616
    label "przest&#281;pca"
  ]
  node [
    id 617
    label "kopiowa&#263;"
  ]
  node [
    id 618
    label "podr&#243;bka"
  ]
  node [
    id 619
    label "kieruj&#261;cy"
  ]
  node [
    id 620
    label "&#380;agl&#243;wka"
  ]
  node [
    id 621
    label "rum"
  ]
  node [
    id 622
    label "rozb&#243;jnik"
  ]
  node [
    id 623
    label "postrzeleniec"
  ]
  node [
    id 624
    label "testify"
  ]
  node [
    id 625
    label "przedstawi&#263;"
  ]
  node [
    id 626
    label "zapozna&#263;"
  ]
  node [
    id 627
    label "pokaza&#263;"
  ]
  node [
    id 628
    label "typify"
  ]
  node [
    id 629
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 630
    label "uprzedzi&#263;"
  ]
  node [
    id 631
    label "attest"
  ]
  node [
    id 632
    label "wyra&#380;anie"
  ]
  node [
    id 633
    label "uprzedzanie"
  ]
  node [
    id 634
    label "representation"
  ]
  node [
    id 635
    label "zapoznawanie"
  ]
  node [
    id 636
    label "present"
  ]
  node [
    id 637
    label "przedstawianie"
  ]
  node [
    id 638
    label "display"
  ]
  node [
    id 639
    label "demonstrowanie"
  ]
  node [
    id 640
    label "presentation"
  ]
  node [
    id 641
    label "granie"
  ]
  node [
    id 642
    label "zapoznanie"
  ]
  node [
    id 643
    label "zapoznanie_si&#281;"
  ]
  node [
    id 644
    label "exhibit"
  ]
  node [
    id 645
    label "pokazanie"
  ]
  node [
    id 646
    label "wyst&#261;pienie"
  ]
  node [
    id 647
    label "uprzedzenie"
  ]
  node [
    id 648
    label "przedstawienie"
  ]
  node [
    id 649
    label "gra&#263;"
  ]
  node [
    id 650
    label "zapoznawa&#263;"
  ]
  node [
    id 651
    label "uprzedza&#263;"
  ]
  node [
    id 652
    label "ulotka"
  ]
  node [
    id 653
    label "wskaz&#243;wka"
  ]
  node [
    id 654
    label "instruktarz"
  ]
  node [
    id 655
    label "danie"
  ]
  node [
    id 656
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 657
    label "restauracja"
  ]
  node [
    id 658
    label "cennik"
  ]
  node [
    id 659
    label "chart"
  ]
  node [
    id 660
    label "karta"
  ]
  node [
    id 661
    label "zestaw"
  ]
  node [
    id 662
    label "bajt"
  ]
  node [
    id 663
    label "bloking"
  ]
  node [
    id 664
    label "j&#261;kanie"
  ]
  node [
    id 665
    label "przeszkoda"
  ]
  node [
    id 666
    label "zesp&#243;&#322;"
  ]
  node [
    id 667
    label "blokada"
  ]
  node [
    id 668
    label "bry&#322;a"
  ]
  node [
    id 669
    label "kontynent"
  ]
  node [
    id 670
    label "nastawnia"
  ]
  node [
    id 671
    label "blockage"
  ]
  node [
    id 672
    label "block"
  ]
  node [
    id 673
    label "organizacja"
  ]
  node [
    id 674
    label "budynek"
  ]
  node [
    id 675
    label "start"
  ]
  node [
    id 676
    label "skorupa_ziemska"
  ]
  node [
    id 677
    label "zeszyt"
  ]
  node [
    id 678
    label "grupa"
  ]
  node [
    id 679
    label "blokowisko"
  ]
  node [
    id 680
    label "artyku&#322;"
  ]
  node [
    id 681
    label "barak"
  ]
  node [
    id 682
    label "stok_kontynentalny"
  ]
  node [
    id 683
    label "whole"
  ]
  node [
    id 684
    label "square"
  ]
  node [
    id 685
    label "siatk&#243;wka"
  ]
  node [
    id 686
    label "kr&#261;g"
  ]
  node [
    id 687
    label "obrona"
  ]
  node [
    id 688
    label "ok&#322;adka"
  ]
  node [
    id 689
    label "bie&#380;nia"
  ]
  node [
    id 690
    label "referat"
  ]
  node [
    id 691
    label "dom_wielorodzinny"
  ]
  node [
    id 692
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 693
    label "routine"
  ]
  node [
    id 694
    label "proceduralnie"
  ]
  node [
    id 695
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 696
    label "jednostka_organizacyjna"
  ]
  node [
    id 697
    label "urz&#261;d"
  ]
  node [
    id 698
    label "sfera"
  ]
  node [
    id 699
    label "miejsce_pracy"
  ]
  node [
    id 700
    label "insourcing"
  ]
  node [
    id 701
    label "stopie&#324;"
  ]
  node [
    id 702
    label "competence"
  ]
  node [
    id 703
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 704
    label "bezdro&#380;e"
  ]
  node [
    id 705
    label "poddzia&#322;"
  ]
  node [
    id 706
    label "podwini&#281;cie"
  ]
  node [
    id 707
    label "zap&#322;acenie"
  ]
  node [
    id 708
    label "przyodzianie"
  ]
  node [
    id 709
    label "budowla"
  ]
  node [
    id 710
    label "pokrycie"
  ]
  node [
    id 711
    label "rozebranie"
  ]
  node [
    id 712
    label "zak&#322;adka"
  ]
  node [
    id 713
    label "struktura"
  ]
  node [
    id 714
    label "poubieranie"
  ]
  node [
    id 715
    label "infliction"
  ]
  node [
    id 716
    label "przebranie"
  ]
  node [
    id 717
    label "przywdzianie"
  ]
  node [
    id 718
    label "obleczenie_si&#281;"
  ]
  node [
    id 719
    label "utworzenie"
  ]
  node [
    id 720
    label "str&#243;j"
  ]
  node [
    id 721
    label "twierdzenie"
  ]
  node [
    id 722
    label "obleczenie"
  ]
  node [
    id 723
    label "przygotowywanie"
  ]
  node [
    id 724
    label "przymierzenie"
  ]
  node [
    id 725
    label "wyko&#324;czenie"
  ]
  node [
    id 726
    label "point"
  ]
  node [
    id 727
    label "przygotowanie"
  ]
  node [
    id 728
    label "przewidzenie"
  ]
  node [
    id 729
    label "po&#322;o&#380;enie"
  ]
  node [
    id 730
    label "sprawa"
  ]
  node [
    id 731
    label "ust&#281;p"
  ]
  node [
    id 732
    label "plan"
  ]
  node [
    id 733
    label "obiekt_matematyczny"
  ]
  node [
    id 734
    label "problemat"
  ]
  node [
    id 735
    label "plamka"
  ]
  node [
    id 736
    label "stopie&#324;_pisma"
  ]
  node [
    id 737
    label "jednostka"
  ]
  node [
    id 738
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 739
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 740
    label "mark"
  ]
  node [
    id 741
    label "chwila"
  ]
  node [
    id 742
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 743
    label "prosta"
  ]
  node [
    id 744
    label "problematyka"
  ]
  node [
    id 745
    label "zapunktowa&#263;"
  ]
  node [
    id 746
    label "podpunkt"
  ]
  node [
    id 747
    label "wojsko"
  ]
  node [
    id 748
    label "kres"
  ]
  node [
    id 749
    label "przestrze&#324;"
  ]
  node [
    id 750
    label "reengineering"
  ]
  node [
    id 751
    label "scheduling"
  ]
  node [
    id 752
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 753
    label "okienko"
  ]
  node [
    id 754
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 755
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 756
    label "kacapski"
  ]
  node [
    id 757
    label "po_rosyjsku"
  ]
  node [
    id 758
    label "wielkoruski"
  ]
  node [
    id 759
    label "Russian"
  ]
  node [
    id 760
    label "rusek"
  ]
  node [
    id 761
    label "wschodnioeuropejski"
  ]
  node [
    id 762
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 763
    label "poga&#324;ski"
  ]
  node [
    id 764
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 765
    label "topielec"
  ]
  node [
    id 766
    label "europejski"
  ]
  node [
    id 767
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 768
    label "po_kacapsku"
  ]
  node [
    id 769
    label "ruski"
  ]
  node [
    id 770
    label "imperialny"
  ]
  node [
    id 771
    label "po_wielkorusku"
  ]
  node [
    id 772
    label "freeze"
  ]
  node [
    id 773
    label "twardnie&#263;"
  ]
  node [
    id 774
    label "ulega&#263;"
  ]
  node [
    id 775
    label "sp&#243;&#322;g&#322;oska_&#347;rodkowoj&#281;zykowa"
  ]
  node [
    id 776
    label "stiffen"
  ]
  node [
    id 777
    label "hurt"
  ]
  node [
    id 778
    label "przywo&#322;a&#263;"
  ]
  node [
    id 779
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 780
    label "kobieta"
  ]
  node [
    id 781
    label "poddawa&#263;"
  ]
  node [
    id 782
    label "postpone"
  ]
  node [
    id 783
    label "render"
  ]
  node [
    id 784
    label "zezwala&#263;"
  ]
  node [
    id 785
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 786
    label "subject"
  ]
  node [
    id 787
    label "adjustment"
  ]
  node [
    id 788
    label "panowanie"
  ]
  node [
    id 789
    label "przebywanie"
  ]
  node [
    id 790
    label "animation"
  ]
  node [
    id 791
    label "kwadrat"
  ]
  node [
    id 792
    label "stanie"
  ]
  node [
    id 793
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 794
    label "pomieszkanie"
  ]
  node [
    id 795
    label "lokal"
  ]
  node [
    id 796
    label "dom"
  ]
  node [
    id 797
    label "zajmowanie"
  ]
  node [
    id 798
    label "sprawowanie"
  ]
  node [
    id 799
    label "bycie"
  ]
  node [
    id 800
    label "kierowanie"
  ]
  node [
    id 801
    label "w&#322;adca"
  ]
  node [
    id 802
    label "dominowanie"
  ]
  node [
    id 803
    label "przewaga"
  ]
  node [
    id 804
    label "przewa&#380;anie"
  ]
  node [
    id 805
    label "znaczenie"
  ]
  node [
    id 806
    label "laterality"
  ]
  node [
    id 807
    label "control"
  ]
  node [
    id 808
    label "dominance"
  ]
  node [
    id 809
    label "kontrolowanie"
  ]
  node [
    id 810
    label "temper"
  ]
  node [
    id 811
    label "rule"
  ]
  node [
    id 812
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 813
    label "prym"
  ]
  node [
    id 814
    label "w&#322;adza"
  ]
  node [
    id 815
    label "ocieranie_si&#281;"
  ]
  node [
    id 816
    label "otoczenie_si&#281;"
  ]
  node [
    id 817
    label "posiedzenie"
  ]
  node [
    id 818
    label "otarcie_si&#281;"
  ]
  node [
    id 819
    label "atakowanie"
  ]
  node [
    id 820
    label "otaczanie_si&#281;"
  ]
  node [
    id 821
    label "wyj&#347;cie"
  ]
  node [
    id 822
    label "zmierzanie"
  ]
  node [
    id 823
    label "residency"
  ]
  node [
    id 824
    label "sojourn"
  ]
  node [
    id 825
    label "wychodzenie"
  ]
  node [
    id 826
    label "tkwienie"
  ]
  node [
    id 827
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 828
    label "lokowanie_si&#281;"
  ]
  node [
    id 829
    label "schorzenie"
  ]
  node [
    id 830
    label "zajmowanie_si&#281;"
  ]
  node [
    id 831
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 832
    label "stosowanie"
  ]
  node [
    id 833
    label "anektowanie"
  ]
  node [
    id 834
    label "ciekawy"
  ]
  node [
    id 835
    label "zabieranie"
  ]
  node [
    id 836
    label "sytuowanie_si&#281;"
  ]
  node [
    id 837
    label "wype&#322;nianie"
  ]
  node [
    id 838
    label "obejmowanie"
  ]
  node [
    id 839
    label "klasyfikacja"
  ]
  node [
    id 840
    label "dzianie_si&#281;"
  ]
  node [
    id 841
    label "branie"
  ]
  node [
    id 842
    label "rz&#261;dzenie"
  ]
  node [
    id 843
    label "occupation"
  ]
  node [
    id 844
    label "zadawanie"
  ]
  node [
    id 845
    label "zaj&#281;ty"
  ]
  node [
    id 846
    label "gastronomia"
  ]
  node [
    id 847
    label "zak&#322;ad"
  ]
  node [
    id 848
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 849
    label "rodzina"
  ]
  node [
    id 850
    label "substancja_mieszkaniowa"
  ]
  node [
    id 851
    label "instytucja"
  ]
  node [
    id 852
    label "siedziba"
  ]
  node [
    id 853
    label "dom_rodzinny"
  ]
  node [
    id 854
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 855
    label "poj&#281;cie"
  ]
  node [
    id 856
    label "stead"
  ]
  node [
    id 857
    label "garderoba"
  ]
  node [
    id 858
    label "wiecha"
  ]
  node [
    id 859
    label "fratria"
  ]
  node [
    id 860
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 861
    label "trwanie"
  ]
  node [
    id 862
    label "ustanie"
  ]
  node [
    id 863
    label "wystanie"
  ]
  node [
    id 864
    label "postanie"
  ]
  node [
    id 865
    label "wystawanie"
  ]
  node [
    id 866
    label "kosztowanie"
  ]
  node [
    id 867
    label "przestanie"
  ]
  node [
    id 868
    label "wielok&#261;t_foremny"
  ]
  node [
    id 869
    label "prostok&#261;t"
  ]
  node [
    id 870
    label "romb"
  ]
  node [
    id 871
    label "justunek"
  ]
  node [
    id 872
    label "dzielnica"
  ]
  node [
    id 873
    label "poletko"
  ]
  node [
    id 874
    label "ekologia"
  ]
  node [
    id 875
    label "tango"
  ]
  node [
    id 876
    label "figura_taneczna"
  ]
  node [
    id 877
    label "czynnik_biotyczny"
  ]
  node [
    id 878
    label "wyewoluowanie"
  ]
  node [
    id 879
    label "reakcja"
  ]
  node [
    id 880
    label "individual"
  ]
  node [
    id 881
    label "przyswoi&#263;"
  ]
  node [
    id 882
    label "starzenie_si&#281;"
  ]
  node [
    id 883
    label "wyewoluowa&#263;"
  ]
  node [
    id 884
    label "ewoluowa&#263;"
  ]
  node [
    id 885
    label "ewoluowanie"
  ]
  node [
    id 886
    label "sztuka"
  ]
  node [
    id 887
    label "przyswojenie"
  ]
  node [
    id 888
    label "przyswaja&#263;"
  ]
  node [
    id 889
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 890
    label "facet"
  ]
  node [
    id 891
    label "przyswajanie"
  ]
  node [
    id 892
    label "co&#347;"
  ]
  node [
    id 893
    label "thing"
  ]
  node [
    id 894
    label "rzecz"
  ]
  node [
    id 895
    label "strona"
  ]
  node [
    id 896
    label "pr&#243;bowanie"
  ]
  node [
    id 897
    label "rola"
  ]
  node [
    id 898
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 899
    label "cz&#322;owiek"
  ]
  node [
    id 900
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 901
    label "realizacja"
  ]
  node [
    id 902
    label "scena"
  ]
  node [
    id 903
    label "didaskalia"
  ]
  node [
    id 904
    label "czyn"
  ]
  node [
    id 905
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 906
    label "environment"
  ]
  node [
    id 907
    label "head"
  ]
  node [
    id 908
    label "scenariusz"
  ]
  node [
    id 909
    label "egzemplarz"
  ]
  node [
    id 910
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 911
    label "utw&#243;r"
  ]
  node [
    id 912
    label "fortel"
  ]
  node [
    id 913
    label "theatrical_performance"
  ]
  node [
    id 914
    label "ambala&#380;"
  ]
  node [
    id 915
    label "sprawno&#347;&#263;"
  ]
  node [
    id 916
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 917
    label "Faust"
  ]
  node [
    id 918
    label "scenografia"
  ]
  node [
    id 919
    label "ods&#322;ona"
  ]
  node [
    id 920
    label "turn"
  ]
  node [
    id 921
    label "pokaz"
  ]
  node [
    id 922
    label "ilo&#347;&#263;"
  ]
  node [
    id 923
    label "Apollo"
  ]
  node [
    id 924
    label "kultura"
  ]
  node [
    id 925
    label "towar"
  ]
  node [
    id 926
    label "bratek"
  ]
  node [
    id 927
    label "organizm"
  ]
  node [
    id 928
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 929
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 930
    label "czerpanie"
  ]
  node [
    id 931
    label "uczenie_si&#281;"
  ]
  node [
    id 932
    label "pobieranie"
  ]
  node [
    id 933
    label "acquisition"
  ]
  node [
    id 934
    label "od&#380;ywianie"
  ]
  node [
    id 935
    label "wynoszenie"
  ]
  node [
    id 936
    label "absorption"
  ]
  node [
    id 937
    label "wdra&#380;anie_si&#281;"
  ]
  node [
    id 938
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 939
    label "translate"
  ]
  node [
    id 940
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 941
    label "pobra&#263;"
  ]
  node [
    id 942
    label "thrill"
  ]
  node [
    id 943
    label "pobranie"
  ]
  node [
    id 944
    label "wyniesienie"
  ]
  node [
    id 945
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 946
    label "assimilation"
  ]
  node [
    id 947
    label "emotion"
  ]
  node [
    id 948
    label "nauczenie_si&#281;"
  ]
  node [
    id 949
    label "zaczerpni&#281;cie"
  ]
  node [
    id 950
    label "mechanizm_obronny"
  ]
  node [
    id 951
    label "convention"
  ]
  node [
    id 952
    label "przeobrazi&#263;_si&#281;"
  ]
  node [
    id 953
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 954
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 955
    label "przeobra&#380;a&#263;_si&#281;"
  ]
  node [
    id 956
    label "react"
  ]
  node [
    id 957
    label "zachowanie"
  ]
  node [
    id 958
    label "reaction"
  ]
  node [
    id 959
    label "rozmowa"
  ]
  node [
    id 960
    label "response"
  ]
  node [
    id 961
    label "respondent"
  ]
  node [
    id 962
    label "treat"
  ]
  node [
    id 963
    label "czerpa&#263;"
  ]
  node [
    id 964
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 965
    label "pobiera&#263;"
  ]
  node [
    id 966
    label "rede"
  ]
  node [
    id 967
    label "podawa&#263;"
  ]
  node [
    id 968
    label "pokazywa&#263;"
  ]
  node [
    id 969
    label "demonstrowa&#263;"
  ]
  node [
    id 970
    label "opisywa&#263;"
  ]
  node [
    id 971
    label "ukazywa&#263;"
  ]
  node [
    id 972
    label "zg&#322;asza&#263;"
  ]
  node [
    id 973
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 974
    label "stanowi&#263;"
  ]
  node [
    id 975
    label "warto&#347;&#263;"
  ]
  node [
    id 976
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 977
    label "by&#263;"
  ]
  node [
    id 978
    label "wyraz"
  ]
  node [
    id 979
    label "przeszkala&#263;"
  ]
  node [
    id 980
    label "introduce"
  ]
  node [
    id 981
    label "exsert"
  ]
  node [
    id 982
    label "bespeak"
  ]
  node [
    id 983
    label "indicate"
  ]
  node [
    id 984
    label "mie&#263;_miejsce"
  ]
  node [
    id 985
    label "odst&#281;powa&#263;"
  ]
  node [
    id 986
    label "perform"
  ]
  node [
    id 987
    label "wychodzi&#263;"
  ]
  node [
    id 988
    label "seclude"
  ]
  node [
    id 989
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 990
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 991
    label "nak&#322;ania&#263;"
  ]
  node [
    id 992
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 993
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 994
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 995
    label "dzia&#322;a&#263;"
  ]
  node [
    id 996
    label "act"
  ]
  node [
    id 997
    label "appear"
  ]
  node [
    id 998
    label "unwrap"
  ]
  node [
    id 999
    label "rezygnowa&#263;"
  ]
  node [
    id 1000
    label "overture"
  ]
  node [
    id 1001
    label "uczestniczy&#263;"
  ]
  node [
    id 1002
    label "report"
  ]
  node [
    id 1003
    label "write"
  ]
  node [
    id 1004
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1005
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1006
    label "decide"
  ]
  node [
    id 1007
    label "pies_my&#347;liwski"
  ]
  node [
    id 1008
    label "zatrzymywa&#263;"
  ]
  node [
    id 1009
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1010
    label "tenis"
  ]
  node [
    id 1011
    label "deal"
  ]
  node [
    id 1012
    label "dawa&#263;"
  ]
  node [
    id 1013
    label "stawia&#263;"
  ]
  node [
    id 1014
    label "rozgrywa&#263;"
  ]
  node [
    id 1015
    label "kelner"
  ]
  node [
    id 1016
    label "cover"
  ]
  node [
    id 1017
    label "tender"
  ]
  node [
    id 1018
    label "jedzenie"
  ]
  node [
    id 1019
    label "faszerowa&#263;"
  ]
  node [
    id 1020
    label "serwowa&#263;"
  ]
  node [
    id 1021
    label "zawiera&#263;"
  ]
  node [
    id 1022
    label "poznawa&#263;"
  ]
  node [
    id 1023
    label "obznajamia&#263;"
  ]
  node [
    id 1024
    label "go_steady"
  ]
  node [
    id 1025
    label "teren"
  ]
  node [
    id 1026
    label "play"
  ]
  node [
    id 1027
    label "antyteatr"
  ]
  node [
    id 1028
    label "gra"
  ]
  node [
    id 1029
    label "deski"
  ]
  node [
    id 1030
    label "sala"
  ]
  node [
    id 1031
    label "literatura"
  ]
  node [
    id 1032
    label "dekoratornia"
  ]
  node [
    id 1033
    label "modelatornia"
  ]
  node [
    id 1034
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 1035
    label "widzownia"
  ]
  node [
    id 1036
    label "zademonstrowanie"
  ]
  node [
    id 1037
    label "obgadanie"
  ]
  node [
    id 1038
    label "narration"
  ]
  node [
    id 1039
    label "cyrk"
  ]
  node [
    id 1040
    label "posta&#263;"
  ]
  node [
    id 1041
    label "opisanie"
  ]
  node [
    id 1042
    label "malarstwo"
  ]
  node [
    id 1043
    label "ukazanie"
  ]
  node [
    id 1044
    label "podanie"
  ]
  node [
    id 1045
    label "ludno&#347;&#263;"
  ]
  node [
    id 1046
    label "zwierz&#281;"
  ]
  node [
    id 1047
    label "degenerat"
  ]
  node [
    id 1048
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 1049
    label "zwyrol"
  ]
  node [
    id 1050
    label "czerniak"
  ]
  node [
    id 1051
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 1052
    label "paszcza"
  ]
  node [
    id 1053
    label "popapraniec"
  ]
  node [
    id 1054
    label "skuba&#263;"
  ]
  node [
    id 1055
    label "skubanie"
  ]
  node [
    id 1056
    label "agresja"
  ]
  node [
    id 1057
    label "skubni&#281;cie"
  ]
  node [
    id 1058
    label "zwierz&#281;ta"
  ]
  node [
    id 1059
    label "fukni&#281;cie"
  ]
  node [
    id 1060
    label "farba"
  ]
  node [
    id 1061
    label "fukanie"
  ]
  node [
    id 1062
    label "istota_&#380;ywa"
  ]
  node [
    id 1063
    label "gad"
  ]
  node [
    id 1064
    label "tresowa&#263;"
  ]
  node [
    id 1065
    label "siedzie&#263;"
  ]
  node [
    id 1066
    label "oswaja&#263;"
  ]
  node [
    id 1067
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 1068
    label "poligamia"
  ]
  node [
    id 1069
    label "oz&#243;r"
  ]
  node [
    id 1070
    label "skubn&#261;&#263;"
  ]
  node [
    id 1071
    label "wios&#322;owa&#263;"
  ]
  node [
    id 1072
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 1073
    label "le&#380;enie"
  ]
  node [
    id 1074
    label "niecz&#322;owiek"
  ]
  node [
    id 1075
    label "wios&#322;owanie"
  ]
  node [
    id 1076
    label "napasienie_si&#281;"
  ]
  node [
    id 1077
    label "wiwarium"
  ]
  node [
    id 1078
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 1079
    label "animalista"
  ]
  node [
    id 1080
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 1081
    label "pasienie_si&#281;"
  ]
  node [
    id 1082
    label "sodomita"
  ]
  node [
    id 1083
    label "monogamia"
  ]
  node [
    id 1084
    label "przyssawka"
  ]
  node [
    id 1085
    label "budowa_cia&#322;a"
  ]
  node [
    id 1086
    label "okrutnik"
  ]
  node [
    id 1087
    label "grzbiet"
  ]
  node [
    id 1088
    label "weterynarz"
  ]
  node [
    id 1089
    label "&#322;eb"
  ]
  node [
    id 1090
    label "wylinka"
  ]
  node [
    id 1091
    label "bestia"
  ]
  node [
    id 1092
    label "poskramia&#263;"
  ]
  node [
    id 1093
    label "fauna"
  ]
  node [
    id 1094
    label "treser"
  ]
  node [
    id 1095
    label "siedzenie"
  ]
  node [
    id 1096
    label "le&#380;e&#263;"
  ]
  node [
    id 1097
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1098
    label "asymilowanie"
  ]
  node [
    id 1099
    label "wapniak"
  ]
  node [
    id 1100
    label "asymilowa&#263;"
  ]
  node [
    id 1101
    label "os&#322;abia&#263;"
  ]
  node [
    id 1102
    label "hominid"
  ]
  node [
    id 1103
    label "podw&#322;adny"
  ]
  node [
    id 1104
    label "os&#322;abianie"
  ]
  node [
    id 1105
    label "g&#322;owa"
  ]
  node [
    id 1106
    label "figura"
  ]
  node [
    id 1107
    label "portrecista"
  ]
  node [
    id 1108
    label "dwun&#243;g"
  ]
  node [
    id 1109
    label "profanum"
  ]
  node [
    id 1110
    label "mikrokosmos"
  ]
  node [
    id 1111
    label "nasada"
  ]
  node [
    id 1112
    label "duch"
  ]
  node [
    id 1113
    label "antropochoria"
  ]
  node [
    id 1114
    label "osoba"
  ]
  node [
    id 1115
    label "wz&#243;r"
  ]
  node [
    id 1116
    label "senior"
  ]
  node [
    id 1117
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1118
    label "Adam"
  ]
  node [
    id 1119
    label "homo_sapiens"
  ]
  node [
    id 1120
    label "polifag"
  ]
  node [
    id 1121
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1122
    label "innowierstwo"
  ]
  node [
    id 1123
    label "ch&#322;opstwo"
  ]
  node [
    id 1124
    label "po_ukrai&#324;sku"
  ]
  node [
    id 1125
    label "ma&#322;oruski"
  ]
  node [
    id 1126
    label "so&#322;omacha"
  ]
  node [
    id 1127
    label "ukrai&#324;sko"
  ]
  node [
    id 1128
    label "pierogi_ruskie"
  ]
  node [
    id 1129
    label "ukrainny"
  ]
  node [
    id 1130
    label "sa&#322;o"
  ]
  node [
    id 1131
    label "po_ma&#322;orusku"
  ]
  node [
    id 1132
    label "przek&#261;ska"
  ]
  node [
    id 1133
    label "s&#322;onina"
  ]
  node [
    id 1134
    label "potrawa"
  ]
  node [
    id 1135
    label "europejsko"
  ]
  node [
    id 1136
    label "wyrostek"
  ]
  node [
    id 1137
    label "aut_bramkowy"
  ]
  node [
    id 1138
    label "naczynie"
  ]
  node [
    id 1139
    label "linia"
  ]
  node [
    id 1140
    label "instrument_d&#281;ty"
  ]
  node [
    id 1141
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 1142
    label "tworzywo"
  ]
  node [
    id 1143
    label "poro&#380;e"
  ]
  node [
    id 1144
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1145
    label "zbieg"
  ]
  node [
    id 1146
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1147
    label "punkt_McBurneya"
  ]
  node [
    id 1148
    label "narz&#261;d_limfoidalny"
  ]
  node [
    id 1149
    label "tw&#243;r"
  ]
  node [
    id 1150
    label "jelito_&#347;lepe"
  ]
  node [
    id 1151
    label "ch&#322;opiec"
  ]
  node [
    id 1152
    label "m&#322;okos"
  ]
  node [
    id 1153
    label "warunek_lokalowy"
  ]
  node [
    id 1154
    label "plac"
  ]
  node [
    id 1155
    label "location"
  ]
  node [
    id 1156
    label "uwaga"
  ]
  node [
    id 1157
    label "status"
  ]
  node [
    id 1158
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1159
    label "praca"
  ]
  node [
    id 1160
    label "rz&#261;d"
  ]
  node [
    id 1161
    label "Rzym_Zachodni"
  ]
  node [
    id 1162
    label "element"
  ]
  node [
    id 1163
    label "Rzym_Wschodni"
  ]
  node [
    id 1164
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 1165
    label "vessel"
  ]
  node [
    id 1166
    label "sprz&#281;t"
  ]
  node [
    id 1167
    label "statki"
  ]
  node [
    id 1168
    label "rewaskularyzacja"
  ]
  node [
    id 1169
    label "ceramika"
  ]
  node [
    id 1170
    label "drewno"
  ]
  node [
    id 1171
    label "unaczyni&#263;"
  ]
  node [
    id 1172
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 1173
    label "receptacle"
  ]
  node [
    id 1174
    label "substancja"
  ]
  node [
    id 1175
    label "styk"
  ]
  node [
    id 1176
    label "ustawienie"
  ]
  node [
    id 1177
    label "narrative"
  ]
  node [
    id 1178
    label "nafaszerowanie"
  ]
  node [
    id 1179
    label "prayer"
  ]
  node [
    id 1180
    label "pi&#322;ka"
  ]
  node [
    id 1181
    label "myth"
  ]
  node [
    id 1182
    label "service"
  ]
  node [
    id 1183
    label "zagranie"
  ]
  node [
    id 1184
    label "poinformowanie"
  ]
  node [
    id 1185
    label "zaserwowanie"
  ]
  node [
    id 1186
    label "opowie&#347;&#263;"
  ]
  node [
    id 1187
    label "pass"
  ]
  node [
    id 1188
    label "graf"
  ]
  node [
    id 1189
    label "para"
  ]
  node [
    id 1190
    label "narta"
  ]
  node [
    id 1191
    label "ochraniacz"
  ]
  node [
    id 1192
    label "koniec"
  ]
  node [
    id 1193
    label "sytuacja"
  ]
  node [
    id 1194
    label "temat"
  ]
  node [
    id 1195
    label "wn&#281;trze"
  ]
  node [
    id 1196
    label "informacja"
  ]
  node [
    id 1197
    label "mursz"
  ]
  node [
    id 1198
    label "element_anatomiczny"
  ]
  node [
    id 1199
    label "kszta&#322;t"
  ]
  node [
    id 1200
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1201
    label "armia"
  ]
  node [
    id 1202
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1203
    label "poprowadzi&#263;"
  ]
  node [
    id 1204
    label "cord"
  ]
  node [
    id 1205
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1206
    label "trasa"
  ]
  node [
    id 1207
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1208
    label "tract"
  ]
  node [
    id 1209
    label "materia&#322;_zecerski"
  ]
  node [
    id 1210
    label "przeorientowywanie"
  ]
  node [
    id 1211
    label "curve"
  ]
  node [
    id 1212
    label "figura_geometryczna"
  ]
  node [
    id 1213
    label "wygl&#261;d"
  ]
  node [
    id 1214
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 1215
    label "jard"
  ]
  node [
    id 1216
    label "szczep"
  ]
  node [
    id 1217
    label "phreaker"
  ]
  node [
    id 1218
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 1219
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1220
    label "prowadzi&#263;"
  ]
  node [
    id 1221
    label "przeorientowywa&#263;"
  ]
  node [
    id 1222
    label "access"
  ]
  node [
    id 1223
    label "przeorientowanie"
  ]
  node [
    id 1224
    label "przeorientowa&#263;"
  ]
  node [
    id 1225
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 1226
    label "billing"
  ]
  node [
    id 1227
    label "granica"
  ]
  node [
    id 1228
    label "szpaler"
  ]
  node [
    id 1229
    label "sztrych"
  ]
  node [
    id 1230
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1231
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1232
    label "drzewo_genealogiczne"
  ]
  node [
    id 1233
    label "transporter"
  ]
  node [
    id 1234
    label "line"
  ]
  node [
    id 1235
    label "kompleksja"
  ]
  node [
    id 1236
    label "granice"
  ]
  node [
    id 1237
    label "kontakt"
  ]
  node [
    id 1238
    label "przewo&#378;nik"
  ]
  node [
    id 1239
    label "przystanek"
  ]
  node [
    id 1240
    label "linijka"
  ]
  node [
    id 1241
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1242
    label "coalescence"
  ]
  node [
    id 1243
    label "Ural"
  ]
  node [
    id 1244
    label "bearing"
  ]
  node [
    id 1245
    label "prowadzenie"
  ]
  node [
    id 1246
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1247
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1248
    label "krzywy"
  ]
  node [
    id 1249
    label "R&#243;g"
  ]
  node [
    id 1250
    label "Kiri&#322;"
  ]
  node [
    id 1251
    label "Czubenko"
  ]
  node [
    id 1252
    label "Wiesti"
  ]
  node [
    id 1253
    label "tv"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 1252
  ]
  edge [
    source 11
    target 1253
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 1252
  ]
  edge [
    source 12
    target 1253
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 880
  ]
  edge [
    source 14
    target 881
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 884
  ]
  edge [
    source 14
    target 885
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 886
  ]
  edge [
    source 14
    target 887
  ]
  edge [
    source 14
    target 888
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 855
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 416
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 637
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 542
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 359
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 322
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 352
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 341
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 997
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 15
    target 1003
  ]
  edge [
    source 15
    target 1004
  ]
  edge [
    source 15
    target 1005
  ]
  edge [
    source 15
    target 1006
  ]
  edge [
    source 15
    target 1007
  ]
  edge [
    source 15
    target 349
  ]
  edge [
    source 15
    target 1008
  ]
  edge [
    source 15
    target 1009
  ]
  edge [
    source 15
    target 334
  ]
  edge [
    source 15
    target 1010
  ]
  edge [
    source 15
    target 1011
  ]
  edge [
    source 15
    target 1012
  ]
  edge [
    source 15
    target 1013
  ]
  edge [
    source 15
    target 1014
  ]
  edge [
    source 15
    target 1015
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 1016
  ]
  edge [
    source 15
    target 1017
  ]
  edge [
    source 15
    target 1018
  ]
  edge [
    source 15
    target 1019
  ]
  edge [
    source 15
    target 1020
  ]
  edge [
    source 15
    target 1021
  ]
  edge [
    source 15
    target 1022
  ]
  edge [
    source 15
    target 1023
  ]
  edge [
    source 15
    target 1024
  ]
  edge [
    source 15
    target 1025
  ]
  edge [
    source 15
    target 1026
  ]
  edge [
    source 15
    target 1027
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 1028
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 1029
  ]
  edge [
    source 15
    target 1030
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 1031
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 1032
  ]
  edge [
    source 15
    target 1033
  ]
  edge [
    source 15
    target 1034
  ]
  edge [
    source 15
    target 1035
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 1036
  ]
  edge [
    source 15
    target 1037
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 1038
  ]
  edge [
    source 15
    target 1039
  ]
  edge [
    source 15
    target 449
  ]
  edge [
    source 15
    target 1040
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 1041
  ]
  edge [
    source 15
    target 1042
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 1043
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 1044
  ]
  edge [
    source 15
    target 426
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1120
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 339
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 392
  ]
  edge [
    source 18
    target 393
  ]
  edge [
    source 18
    target 394
  ]
  edge [
    source 18
    target 395
  ]
  edge [
    source 18
    target 396
  ]
  edge [
    source 18
    target 397
  ]
  edge [
    source 18
    target 398
  ]
  edge [
    source 18
    target 399
  ]
  edge [
    source 18
    target 400
  ]
  edge [
    source 18
    target 401
  ]
  edge [
    source 18
    target 402
  ]
  edge [
    source 18
    target 403
  ]
  edge [
    source 18
    target 404
  ]
  edge [
    source 18
    target 405
  ]
  edge [
    source 18
    target 406
  ]
  edge [
    source 18
    target 407
  ]
  edge [
    source 18
    target 408
  ]
  edge [
    source 18
    target 409
  ]
  edge [
    source 18
    target 410
  ]
  edge [
    source 18
    target 411
  ]
  edge [
    source 18
    target 412
  ]
  edge [
    source 18
    target 413
  ]
  edge [
    source 18
    target 414
  ]
  edge [
    source 18
    target 415
  ]
  edge [
    source 18
    target 416
  ]
  edge [
    source 18
    target 417
  ]
  edge [
    source 18
    target 418
  ]
  edge [
    source 18
    target 419
  ]
  edge [
    source 18
    target 420
  ]
  edge [
    source 18
    target 421
  ]
  edge [
    source 18
    target 422
  ]
  edge [
    source 18
    target 382
  ]
  edge [
    source 18
    target 423
  ]
  edge [
    source 18
    target 424
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 18
    target 426
  ]
  edge [
    source 18
    target 427
  ]
  edge [
    source 18
    target 428
  ]
  edge [
    source 18
    target 429
  ]
  edge [
    source 18
    target 430
  ]
  edge [
    source 18
    target 431
  ]
  edge [
    source 18
    target 432
  ]
  edge [
    source 18
    target 433
  ]
  edge [
    source 18
    target 434
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 532
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 19
    target 1144
  ]
  edge [
    source 19
    target 703
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 1146
  ]
  edge [
    source 19
    target 1147
  ]
  edge [
    source 19
    target 1148
  ]
  edge [
    source 19
    target 1149
  ]
  edge [
    source 19
    target 1150
  ]
  edge [
    source 19
    target 1151
  ]
  edge [
    source 19
    target 1152
  ]
  edge [
    source 19
    target 55
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 749
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 741
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 241
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 683
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 434
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 539
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 899
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 655
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 421
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 685
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 345
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 291
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 548
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 742
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 540
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 426
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 726
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 551
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 1248
    target 1249
  ]
  edge [
    source 1248
    target 1250
  ]
  edge [
    source 1248
    target 1251
  ]
  edge [
    source 1249
    target 1250
  ]
  edge [
    source 1249
    target 1251
  ]
  edge [
    source 1250
    target 1251
  ]
  edge [
    source 1252
    target 1253
  ]
]
