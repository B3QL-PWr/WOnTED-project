graph [
  node [
    id 0
    label "niedziela"
    origin "text"
  ]
  node [
    id 1
    label "wielkanocny"
    origin "text"
  ]
  node [
    id 2
    label "tydzie&#324;"
  ]
  node [
    id 3
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 4
    label "Wielkanoc"
  ]
  node [
    id 5
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 6
    label "Niedziela_Palmowa"
  ]
  node [
    id 7
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 8
    label "weekend"
  ]
  node [
    id 9
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 10
    label "niedziela_przewodnia"
  ]
  node [
    id 11
    label "bia&#322;a_niedziela"
  ]
  node [
    id 12
    label "doba"
  ]
  node [
    id 13
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 14
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 15
    label "czas"
  ]
  node [
    id 16
    label "miesi&#261;c"
  ]
  node [
    id 17
    label "wiosna"
  ]
  node [
    id 18
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 19
    label "&#347;niadanie_wielkanocne"
  ]
  node [
    id 20
    label "rezurekcja"
  ]
  node [
    id 21
    label "sobota"
  ]
  node [
    id 22
    label "&#347;wi&#261;teczny"
  ]
  node [
    id 23
    label "Turki"
  ]
  node [
    id 24
    label "mazurek"
  ]
  node [
    id 25
    label "szpekucha"
  ]
  node [
    id 26
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 27
    label "obrz&#281;dowy"
  ]
  node [
    id 28
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 29
    label "dzie&#324;_wolny"
  ]
  node [
    id 30
    label "wyj&#261;tkowy"
  ]
  node [
    id 31
    label "&#347;wi&#281;tny"
  ]
  node [
    id 32
    label "uroczysty"
  ]
  node [
    id 33
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 34
    label "ciasto"
  ]
  node [
    id 35
    label "przytup"
  ]
  node [
    id 36
    label "polski"
  ]
  node [
    id 37
    label "ho&#322;ubiec"
  ]
  node [
    id 38
    label "melodia"
  ]
  node [
    id 39
    label "taniec"
  ]
  node [
    id 40
    label "wr&#243;bel"
  ]
  node [
    id 41
    label "wariacja"
  ]
  node [
    id 42
    label "taniec_ludowy"
  ]
  node [
    id 43
    label "utw&#243;r"
  ]
  node [
    id 44
    label "tree_sparrow"
  ]
  node [
    id 45
    label "Chopin"
  ]
  node [
    id 46
    label "wodzi&#263;"
  ]
  node [
    id 47
    label "sp&#243;d"
  ]
  node [
    id 48
    label "bu&#322;eczka"
  ]
  node [
    id 49
    label "pier&#243;g"
  ]
  node [
    id 50
    label "litewski"
  ]
  node [
    id 51
    label "warta"
  ]
  node [
    id 52
    label "wielki"
  ]
  node [
    id 53
    label "&#347;wi&#281;to"
  ]
  node [
    id 54
    label "i"
  ]
  node [
    id 55
    label "zmartwychwsta&#263;"
  ]
  node [
    id 56
    label "pa&#324;skie"
  ]
  node [
    id 57
    label "triduum"
  ]
  node [
    id 58
    label "paschalny"
  ]
  node [
    id 59
    label "czwartek"
  ]
  node [
    id 60
    label "gr&#243;b"
  ]
  node [
    id 61
    label "Chrystus"
  ]
  node [
    id 62
    label "zmartwychwsta&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 52
    target 59
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 61
    target 62
  ]
]
