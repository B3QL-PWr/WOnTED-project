graph [
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "ambasador"
    origin "text"
  ]
  node [
    id 2
    label "izrael"
    origin "text"
  ]
  node [
    id 3
    label "ukraina"
    origin "text"
  ]
  node [
    id 4
    label "nazwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "narodowy"
    origin "text"
  ]
  node [
    id 6
    label "bohater"
    origin "text"
  ]
  node [
    id 7
    label "kraj"
    origin "text"
  ]
  node [
    id 8
    label "historycznie"
    origin "text"
  ]
  node [
    id 9
    label "horror"
    origin "text"
  ]
  node [
    id 10
    label "dla"
    origin "text"
  ]
  node [
    id 11
    label "&#380;yd"
    origin "text"
  ]
  node [
    id 12
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "telegraficzny"
    origin "text"
  ]
  node [
    id 14
    label "jta"
    origin "text"
  ]
  node [
    id 15
    label "kolejny"
  ]
  node [
    id 16
    label "nowo"
  ]
  node [
    id 17
    label "cz&#322;owiek"
  ]
  node [
    id 18
    label "bie&#380;&#261;cy"
  ]
  node [
    id 19
    label "drugi"
  ]
  node [
    id 20
    label "narybek"
  ]
  node [
    id 21
    label "obcy"
  ]
  node [
    id 22
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 23
    label "nowotny"
  ]
  node [
    id 24
    label "nadprzyrodzony"
  ]
  node [
    id 25
    label "nieznany"
  ]
  node [
    id 26
    label "pozaludzki"
  ]
  node [
    id 27
    label "obco"
  ]
  node [
    id 28
    label "tameczny"
  ]
  node [
    id 29
    label "osoba"
  ]
  node [
    id 30
    label "nieznajomo"
  ]
  node [
    id 31
    label "inny"
  ]
  node [
    id 32
    label "cudzy"
  ]
  node [
    id 33
    label "istota_&#380;ywa"
  ]
  node [
    id 34
    label "zaziemsko"
  ]
  node [
    id 35
    label "jednoczesny"
  ]
  node [
    id 36
    label "unowocze&#347;nianie"
  ]
  node [
    id 37
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 38
    label "tera&#378;niejszy"
  ]
  node [
    id 39
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 40
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 41
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 42
    label "nast&#281;pnie"
  ]
  node [
    id 43
    label "nastopny"
  ]
  node [
    id 44
    label "kolejno"
  ]
  node [
    id 45
    label "kt&#243;ry&#347;"
  ]
  node [
    id 46
    label "sw&#243;j"
  ]
  node [
    id 47
    label "przeciwny"
  ]
  node [
    id 48
    label "wt&#243;ry"
  ]
  node [
    id 49
    label "dzie&#324;"
  ]
  node [
    id 50
    label "odwrotnie"
  ]
  node [
    id 51
    label "podobny"
  ]
  node [
    id 52
    label "bie&#380;&#261;co"
  ]
  node [
    id 53
    label "ci&#261;g&#322;y"
  ]
  node [
    id 54
    label "aktualny"
  ]
  node [
    id 55
    label "ludzko&#347;&#263;"
  ]
  node [
    id 56
    label "asymilowanie"
  ]
  node [
    id 57
    label "wapniak"
  ]
  node [
    id 58
    label "asymilowa&#263;"
  ]
  node [
    id 59
    label "os&#322;abia&#263;"
  ]
  node [
    id 60
    label "posta&#263;"
  ]
  node [
    id 61
    label "hominid"
  ]
  node [
    id 62
    label "podw&#322;adny"
  ]
  node [
    id 63
    label "os&#322;abianie"
  ]
  node [
    id 64
    label "g&#322;owa"
  ]
  node [
    id 65
    label "figura"
  ]
  node [
    id 66
    label "portrecista"
  ]
  node [
    id 67
    label "dwun&#243;g"
  ]
  node [
    id 68
    label "profanum"
  ]
  node [
    id 69
    label "mikrokosmos"
  ]
  node [
    id 70
    label "nasada"
  ]
  node [
    id 71
    label "duch"
  ]
  node [
    id 72
    label "antropochoria"
  ]
  node [
    id 73
    label "wz&#243;r"
  ]
  node [
    id 74
    label "senior"
  ]
  node [
    id 75
    label "oddzia&#322;ywanie"
  ]
  node [
    id 76
    label "Adam"
  ]
  node [
    id 77
    label "homo_sapiens"
  ]
  node [
    id 78
    label "polifag"
  ]
  node [
    id 79
    label "dopiero_co"
  ]
  node [
    id 80
    label "formacja"
  ]
  node [
    id 81
    label "potomstwo"
  ]
  node [
    id 82
    label "rozsiewca"
  ]
  node [
    id 83
    label "chor&#261;&#380;y"
  ]
  node [
    id 84
    label "tuba"
  ]
  node [
    id 85
    label "hay_fever"
  ]
  node [
    id 86
    label "zwolennik"
  ]
  node [
    id 87
    label "przedstawiciel"
  ]
  node [
    id 88
    label "dyplomata"
  ]
  node [
    id 89
    label "popularyzator"
  ]
  node [
    id 90
    label "mi&#322;y"
  ]
  node [
    id 91
    label "korpus_dyplomatyczny"
  ]
  node [
    id 92
    label "dyplomatyczny"
  ]
  node [
    id 93
    label "takt"
  ]
  node [
    id 94
    label "Metternich"
  ]
  node [
    id 95
    label "dostojnik"
  ]
  node [
    id 96
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 97
    label "cz&#322;onek"
  ]
  node [
    id 98
    label "przyk&#322;ad"
  ]
  node [
    id 99
    label "substytuowa&#263;"
  ]
  node [
    id 100
    label "substytuowanie"
  ]
  node [
    id 101
    label "zast&#281;pca"
  ]
  node [
    id 102
    label "rozszerzyciel"
  ]
  node [
    id 103
    label "g&#322;osiciel"
  ]
  node [
    id 104
    label "urz&#281;dnik"
  ]
  node [
    id 105
    label "&#380;o&#322;nierz"
  ]
  node [
    id 106
    label "tytu&#322;"
  ]
  node [
    id 107
    label "poczet_sztandarowy"
  ]
  node [
    id 108
    label "ziemianin"
  ]
  node [
    id 109
    label "odznaczenie"
  ]
  node [
    id 110
    label "podoficer"
  ]
  node [
    id 111
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 112
    label "oficer"
  ]
  node [
    id 113
    label "luzak"
  ]
  node [
    id 114
    label "kszta&#322;t"
  ]
  node [
    id 115
    label "rulon"
  ]
  node [
    id 116
    label "opakowanie"
  ]
  node [
    id 117
    label "horn"
  ]
  node [
    id 118
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 119
    label "wzmacniacz"
  ]
  node [
    id 120
    label "sukienka"
  ]
  node [
    id 121
    label "rura"
  ]
  node [
    id 122
    label "broadcast"
  ]
  node [
    id 123
    label "nada&#263;"
  ]
  node [
    id 124
    label "okre&#347;li&#263;"
  ]
  node [
    id 125
    label "zdecydowa&#263;"
  ]
  node [
    id 126
    label "zrobi&#263;"
  ]
  node [
    id 127
    label "spowodowa&#263;"
  ]
  node [
    id 128
    label "situate"
  ]
  node [
    id 129
    label "nominate"
  ]
  node [
    id 130
    label "da&#263;"
  ]
  node [
    id 131
    label "za&#322;atwi&#263;"
  ]
  node [
    id 132
    label "give"
  ]
  node [
    id 133
    label "zarekomendowa&#263;"
  ]
  node [
    id 134
    label "przes&#322;a&#263;"
  ]
  node [
    id 135
    label "donie&#347;&#263;"
  ]
  node [
    id 136
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 137
    label "nacjonalistyczny"
  ]
  node [
    id 138
    label "narodowo"
  ]
  node [
    id 139
    label "wa&#380;ny"
  ]
  node [
    id 140
    label "wynios&#322;y"
  ]
  node [
    id 141
    label "dono&#347;ny"
  ]
  node [
    id 142
    label "silny"
  ]
  node [
    id 143
    label "wa&#380;nie"
  ]
  node [
    id 144
    label "istotnie"
  ]
  node [
    id 145
    label "znaczny"
  ]
  node [
    id 146
    label "eksponowany"
  ]
  node [
    id 147
    label "dobry"
  ]
  node [
    id 148
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 149
    label "nale&#380;ny"
  ]
  node [
    id 150
    label "nale&#380;yty"
  ]
  node [
    id 151
    label "typowy"
  ]
  node [
    id 152
    label "uprawniony"
  ]
  node [
    id 153
    label "zasadniczy"
  ]
  node [
    id 154
    label "stosownie"
  ]
  node [
    id 155
    label "taki"
  ]
  node [
    id 156
    label "charakterystyczny"
  ]
  node [
    id 157
    label "prawdziwy"
  ]
  node [
    id 158
    label "ten"
  ]
  node [
    id 159
    label "polityczny"
  ]
  node [
    id 160
    label "nacjonalistycznie"
  ]
  node [
    id 161
    label "narodowo&#347;ciowy"
  ]
  node [
    id 162
    label "Messi"
  ]
  node [
    id 163
    label "Herkules_Poirot"
  ]
  node [
    id 164
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 165
    label "Achilles"
  ]
  node [
    id 166
    label "Harry_Potter"
  ]
  node [
    id 167
    label "Casanova"
  ]
  node [
    id 168
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 169
    label "Zgredek"
  ]
  node [
    id 170
    label "Asterix"
  ]
  node [
    id 171
    label "Winnetou"
  ]
  node [
    id 172
    label "uczestnik"
  ]
  node [
    id 173
    label "&#347;mia&#322;ek"
  ]
  node [
    id 174
    label "Mario"
  ]
  node [
    id 175
    label "Borewicz"
  ]
  node [
    id 176
    label "Quasimodo"
  ]
  node [
    id 177
    label "Sherlock_Holmes"
  ]
  node [
    id 178
    label "Wallenrod"
  ]
  node [
    id 179
    label "Herkules"
  ]
  node [
    id 180
    label "bohaterski"
  ]
  node [
    id 181
    label "Don_Juan"
  ]
  node [
    id 182
    label "podmiot"
  ]
  node [
    id 183
    label "Don_Kiszot"
  ]
  node [
    id 184
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 185
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 186
    label "Hamlet"
  ]
  node [
    id 187
    label "Werter"
  ]
  node [
    id 188
    label "Szwejk"
  ]
  node [
    id 189
    label "charakterystyka"
  ]
  node [
    id 190
    label "zaistnie&#263;"
  ]
  node [
    id 191
    label "Osjan"
  ]
  node [
    id 192
    label "cecha"
  ]
  node [
    id 193
    label "kto&#347;"
  ]
  node [
    id 194
    label "wygl&#261;d"
  ]
  node [
    id 195
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 196
    label "osobowo&#347;&#263;"
  ]
  node [
    id 197
    label "wytw&#243;r"
  ]
  node [
    id 198
    label "trim"
  ]
  node [
    id 199
    label "poby&#263;"
  ]
  node [
    id 200
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 201
    label "Aspazja"
  ]
  node [
    id 202
    label "punkt_widzenia"
  ]
  node [
    id 203
    label "kompleksja"
  ]
  node [
    id 204
    label "wytrzyma&#263;"
  ]
  node [
    id 205
    label "budowa"
  ]
  node [
    id 206
    label "pozosta&#263;"
  ]
  node [
    id 207
    label "point"
  ]
  node [
    id 208
    label "przedstawienie"
  ]
  node [
    id 209
    label "go&#347;&#263;"
  ]
  node [
    id 210
    label "zuch"
  ]
  node [
    id 211
    label "rycerzyk"
  ]
  node [
    id 212
    label "odwa&#380;ny"
  ]
  node [
    id 213
    label "ryzykant"
  ]
  node [
    id 214
    label "morowiec"
  ]
  node [
    id 215
    label "trawa"
  ]
  node [
    id 216
    label "ro&#347;lina"
  ]
  node [
    id 217
    label "twardziel"
  ]
  node [
    id 218
    label "owsowe"
  ]
  node [
    id 219
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 220
    label "byt"
  ]
  node [
    id 221
    label "organizacja"
  ]
  node [
    id 222
    label "prawo"
  ]
  node [
    id 223
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 224
    label "nauka_prawa"
  ]
  node [
    id 225
    label "Szekspir"
  ]
  node [
    id 226
    label "Mickiewicz"
  ]
  node [
    id 227
    label "cierpienie"
  ]
  node [
    id 228
    label "Atomowa_Pch&#322;a"
  ]
  node [
    id 229
    label "bohatersko"
  ]
  node [
    id 230
    label "dzielny"
  ]
  node [
    id 231
    label "waleczny"
  ]
  node [
    id 232
    label "Katar"
  ]
  node [
    id 233
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 234
    label "Mazowsze"
  ]
  node [
    id 235
    label "Libia"
  ]
  node [
    id 236
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 237
    label "Gwatemala"
  ]
  node [
    id 238
    label "Anglia"
  ]
  node [
    id 239
    label "Amazonia"
  ]
  node [
    id 240
    label "Afganistan"
  ]
  node [
    id 241
    label "Ekwador"
  ]
  node [
    id 242
    label "Bordeaux"
  ]
  node [
    id 243
    label "Tad&#380;ykistan"
  ]
  node [
    id 244
    label "Bhutan"
  ]
  node [
    id 245
    label "Argentyna"
  ]
  node [
    id 246
    label "D&#380;ibuti"
  ]
  node [
    id 247
    label "Wenezuela"
  ]
  node [
    id 248
    label "Ukraina"
  ]
  node [
    id 249
    label "Gabon"
  ]
  node [
    id 250
    label "Naddniestrze"
  ]
  node [
    id 251
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 252
    label "Europa_Zachodnia"
  ]
  node [
    id 253
    label "Armagnac"
  ]
  node [
    id 254
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 255
    label "Rwanda"
  ]
  node [
    id 256
    label "Liechtenstein"
  ]
  node [
    id 257
    label "Amhara"
  ]
  node [
    id 258
    label "Sri_Lanka"
  ]
  node [
    id 259
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 260
    label "Zamojszczyzna"
  ]
  node [
    id 261
    label "Madagaskar"
  ]
  node [
    id 262
    label "Tonga"
  ]
  node [
    id 263
    label "Kongo"
  ]
  node [
    id 264
    label "Bangladesz"
  ]
  node [
    id 265
    label "Kanada"
  ]
  node [
    id 266
    label "Ma&#322;opolska"
  ]
  node [
    id 267
    label "Wehrlen"
  ]
  node [
    id 268
    label "Turkiestan"
  ]
  node [
    id 269
    label "Algieria"
  ]
  node [
    id 270
    label "Noworosja"
  ]
  node [
    id 271
    label "Surinam"
  ]
  node [
    id 272
    label "Chile"
  ]
  node [
    id 273
    label "Sahara_Zachodnia"
  ]
  node [
    id 274
    label "Uganda"
  ]
  node [
    id 275
    label "Lubelszczyzna"
  ]
  node [
    id 276
    label "W&#281;gry"
  ]
  node [
    id 277
    label "Mezoameryka"
  ]
  node [
    id 278
    label "Birma"
  ]
  node [
    id 279
    label "Ba&#322;kany"
  ]
  node [
    id 280
    label "Kurdystan"
  ]
  node [
    id 281
    label "Kazachstan"
  ]
  node [
    id 282
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 283
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 284
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 285
    label "Armenia"
  ]
  node [
    id 286
    label "Tuwalu"
  ]
  node [
    id 287
    label "Timor_Wschodni"
  ]
  node [
    id 288
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 289
    label "Szkocja"
  ]
  node [
    id 290
    label "Baszkiria"
  ]
  node [
    id 291
    label "Tonkin"
  ]
  node [
    id 292
    label "Maghreb"
  ]
  node [
    id 293
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 294
    label "Izrael"
  ]
  node [
    id 295
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 296
    label "Nadrenia"
  ]
  node [
    id 297
    label "Estonia"
  ]
  node [
    id 298
    label "Komory"
  ]
  node [
    id 299
    label "Podhale"
  ]
  node [
    id 300
    label "Wielkopolska"
  ]
  node [
    id 301
    label "Zabajkale"
  ]
  node [
    id 302
    label "Kamerun"
  ]
  node [
    id 303
    label "Haiti"
  ]
  node [
    id 304
    label "Belize"
  ]
  node [
    id 305
    label "Sierra_Leone"
  ]
  node [
    id 306
    label "Apulia"
  ]
  node [
    id 307
    label "Luksemburg"
  ]
  node [
    id 308
    label "brzeg"
  ]
  node [
    id 309
    label "USA"
  ]
  node [
    id 310
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 311
    label "Barbados"
  ]
  node [
    id 312
    label "San_Marino"
  ]
  node [
    id 313
    label "Bu&#322;garia"
  ]
  node [
    id 314
    label "Wietnam"
  ]
  node [
    id 315
    label "Indonezja"
  ]
  node [
    id 316
    label "Bojkowszczyzna"
  ]
  node [
    id 317
    label "Malawi"
  ]
  node [
    id 318
    label "Francja"
  ]
  node [
    id 319
    label "Zambia"
  ]
  node [
    id 320
    label "Kujawy"
  ]
  node [
    id 321
    label "Angola"
  ]
  node [
    id 322
    label "Liguria"
  ]
  node [
    id 323
    label "Grenada"
  ]
  node [
    id 324
    label "Pamir"
  ]
  node [
    id 325
    label "Nepal"
  ]
  node [
    id 326
    label "Panama"
  ]
  node [
    id 327
    label "Rumunia"
  ]
  node [
    id 328
    label "Indochiny"
  ]
  node [
    id 329
    label "Podlasie"
  ]
  node [
    id 330
    label "Polinezja"
  ]
  node [
    id 331
    label "Kurpie"
  ]
  node [
    id 332
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 333
    label "S&#261;decczyzna"
  ]
  node [
    id 334
    label "Umbria"
  ]
  node [
    id 335
    label "Czarnog&#243;ra"
  ]
  node [
    id 336
    label "Malediwy"
  ]
  node [
    id 337
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 338
    label "S&#322;owacja"
  ]
  node [
    id 339
    label "Karaiby"
  ]
  node [
    id 340
    label "Ukraina_Zachodnia"
  ]
  node [
    id 341
    label "Kielecczyzna"
  ]
  node [
    id 342
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 343
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 344
    label "Egipt"
  ]
  node [
    id 345
    label "Kolumbia"
  ]
  node [
    id 346
    label "Mozambik"
  ]
  node [
    id 347
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 348
    label "Laos"
  ]
  node [
    id 349
    label "Burundi"
  ]
  node [
    id 350
    label "Suazi"
  ]
  node [
    id 351
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 352
    label "Czechy"
  ]
  node [
    id 353
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 354
    label "Wyspy_Marshalla"
  ]
  node [
    id 355
    label "Trynidad_i_Tobago"
  ]
  node [
    id 356
    label "Dominika"
  ]
  node [
    id 357
    label "Palau"
  ]
  node [
    id 358
    label "Syria"
  ]
  node [
    id 359
    label "Skandynawia"
  ]
  node [
    id 360
    label "Gwinea_Bissau"
  ]
  node [
    id 361
    label "Liberia"
  ]
  node [
    id 362
    label "Zimbabwe"
  ]
  node [
    id 363
    label "Polska"
  ]
  node [
    id 364
    label "Jamajka"
  ]
  node [
    id 365
    label "Tyrol"
  ]
  node [
    id 366
    label "Huculszczyzna"
  ]
  node [
    id 367
    label "Bory_Tucholskie"
  ]
  node [
    id 368
    label "Turyngia"
  ]
  node [
    id 369
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 370
    label "Dominikana"
  ]
  node [
    id 371
    label "Senegal"
  ]
  node [
    id 372
    label "Gruzja"
  ]
  node [
    id 373
    label "Chorwacja"
  ]
  node [
    id 374
    label "Togo"
  ]
  node [
    id 375
    label "Meksyk"
  ]
  node [
    id 376
    label "jednostka_administracyjna"
  ]
  node [
    id 377
    label "Macedonia"
  ]
  node [
    id 378
    label "Gujana"
  ]
  node [
    id 379
    label "Zair"
  ]
  node [
    id 380
    label "Kambod&#380;a"
  ]
  node [
    id 381
    label "Albania"
  ]
  node [
    id 382
    label "Mauritius"
  ]
  node [
    id 383
    label "Monako"
  ]
  node [
    id 384
    label "Gwinea"
  ]
  node [
    id 385
    label "Mali"
  ]
  node [
    id 386
    label "Nigeria"
  ]
  node [
    id 387
    label "Kalabria"
  ]
  node [
    id 388
    label "Hercegowina"
  ]
  node [
    id 389
    label "Kostaryka"
  ]
  node [
    id 390
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 391
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 392
    label "Lotaryngia"
  ]
  node [
    id 393
    label "Hanower"
  ]
  node [
    id 394
    label "Paragwaj"
  ]
  node [
    id 395
    label "W&#322;ochy"
  ]
  node [
    id 396
    label "Wyspy_Salomona"
  ]
  node [
    id 397
    label "Seszele"
  ]
  node [
    id 398
    label "Hiszpania"
  ]
  node [
    id 399
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 400
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 401
    label "Walia"
  ]
  node [
    id 402
    label "Boliwia"
  ]
  node [
    id 403
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 404
    label "Opolskie"
  ]
  node [
    id 405
    label "Kirgistan"
  ]
  node [
    id 406
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 407
    label "Irlandia"
  ]
  node [
    id 408
    label "Kampania"
  ]
  node [
    id 409
    label "Czad"
  ]
  node [
    id 410
    label "Irak"
  ]
  node [
    id 411
    label "Lesoto"
  ]
  node [
    id 412
    label "Malta"
  ]
  node [
    id 413
    label "Andora"
  ]
  node [
    id 414
    label "Sand&#380;ak"
  ]
  node [
    id 415
    label "Chiny"
  ]
  node [
    id 416
    label "Filipiny"
  ]
  node [
    id 417
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 418
    label "Syjon"
  ]
  node [
    id 419
    label "Niemcy"
  ]
  node [
    id 420
    label "Kabylia"
  ]
  node [
    id 421
    label "Lombardia"
  ]
  node [
    id 422
    label "Warmia"
  ]
  node [
    id 423
    label "Brazylia"
  ]
  node [
    id 424
    label "Nikaragua"
  ]
  node [
    id 425
    label "Pakistan"
  ]
  node [
    id 426
    label "&#321;emkowszczyzna"
  ]
  node [
    id 427
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 428
    label "Kaszmir"
  ]
  node [
    id 429
    label "Kenia"
  ]
  node [
    id 430
    label "Niger"
  ]
  node [
    id 431
    label "Tunezja"
  ]
  node [
    id 432
    label "Portugalia"
  ]
  node [
    id 433
    label "Fid&#380;i"
  ]
  node [
    id 434
    label "Maroko"
  ]
  node [
    id 435
    label "Botswana"
  ]
  node [
    id 436
    label "Tajlandia"
  ]
  node [
    id 437
    label "Australia"
  ]
  node [
    id 438
    label "&#321;&#243;dzkie"
  ]
  node [
    id 439
    label "Europa_Wschodnia"
  ]
  node [
    id 440
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 441
    label "Burkina_Faso"
  ]
  node [
    id 442
    label "Benin"
  ]
  node [
    id 443
    label "Tanzania"
  ]
  node [
    id 444
    label "interior"
  ]
  node [
    id 445
    label "Indie"
  ]
  node [
    id 446
    label "&#321;otwa"
  ]
  node [
    id 447
    label "Biskupizna"
  ]
  node [
    id 448
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 449
    label "Kiribati"
  ]
  node [
    id 450
    label "Kaukaz"
  ]
  node [
    id 451
    label "Antigua_i_Barbuda"
  ]
  node [
    id 452
    label "Rodezja"
  ]
  node [
    id 453
    label "Afryka_Wschodnia"
  ]
  node [
    id 454
    label "Cypr"
  ]
  node [
    id 455
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 456
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 457
    label "Podkarpacie"
  ]
  node [
    id 458
    label "obszar"
  ]
  node [
    id 459
    label "Peru"
  ]
  node [
    id 460
    label "Toskania"
  ]
  node [
    id 461
    label "Afryka_Zachodnia"
  ]
  node [
    id 462
    label "Austria"
  ]
  node [
    id 463
    label "Podbeskidzie"
  ]
  node [
    id 464
    label "Urugwaj"
  ]
  node [
    id 465
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 466
    label "Jordania"
  ]
  node [
    id 467
    label "Bo&#347;nia"
  ]
  node [
    id 468
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 469
    label "Grecja"
  ]
  node [
    id 470
    label "Azerbejd&#380;an"
  ]
  node [
    id 471
    label "Oceania"
  ]
  node [
    id 472
    label "Turcja"
  ]
  node [
    id 473
    label "Pomorze_Zachodnie"
  ]
  node [
    id 474
    label "Samoa"
  ]
  node [
    id 475
    label "Powi&#347;le"
  ]
  node [
    id 476
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 477
    label "ziemia"
  ]
  node [
    id 478
    label "Oman"
  ]
  node [
    id 479
    label "Sudan"
  ]
  node [
    id 480
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 481
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 482
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 483
    label "Uzbekistan"
  ]
  node [
    id 484
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 485
    label "Honduras"
  ]
  node [
    id 486
    label "Mongolia"
  ]
  node [
    id 487
    label "Portoryko"
  ]
  node [
    id 488
    label "Kaszuby"
  ]
  node [
    id 489
    label "Ko&#322;yma"
  ]
  node [
    id 490
    label "Szlezwik"
  ]
  node [
    id 491
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 492
    label "Serbia"
  ]
  node [
    id 493
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 494
    label "Tajwan"
  ]
  node [
    id 495
    label "Wielka_Brytania"
  ]
  node [
    id 496
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 497
    label "Liban"
  ]
  node [
    id 498
    label "Japonia"
  ]
  node [
    id 499
    label "Ghana"
  ]
  node [
    id 500
    label "Bahrajn"
  ]
  node [
    id 501
    label "Belgia"
  ]
  node [
    id 502
    label "Etiopia"
  ]
  node [
    id 503
    label "Mikronezja"
  ]
  node [
    id 504
    label "Polesie"
  ]
  node [
    id 505
    label "Kuwejt"
  ]
  node [
    id 506
    label "Kerala"
  ]
  node [
    id 507
    label "Mazury"
  ]
  node [
    id 508
    label "Bahamy"
  ]
  node [
    id 509
    label "Rosja"
  ]
  node [
    id 510
    label "Mo&#322;dawia"
  ]
  node [
    id 511
    label "Palestyna"
  ]
  node [
    id 512
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 513
    label "Lauda"
  ]
  node [
    id 514
    label "Azja_Wschodnia"
  ]
  node [
    id 515
    label "Litwa"
  ]
  node [
    id 516
    label "S&#322;owenia"
  ]
  node [
    id 517
    label "Szwajcaria"
  ]
  node [
    id 518
    label "Erytrea"
  ]
  node [
    id 519
    label "Lubuskie"
  ]
  node [
    id 520
    label "Kuba"
  ]
  node [
    id 521
    label "Arabia_Saudyjska"
  ]
  node [
    id 522
    label "Galicja"
  ]
  node [
    id 523
    label "Zakarpacie"
  ]
  node [
    id 524
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 525
    label "Laponia"
  ]
  node [
    id 526
    label "granica_pa&#324;stwa"
  ]
  node [
    id 527
    label "Malezja"
  ]
  node [
    id 528
    label "Korea"
  ]
  node [
    id 529
    label "Yorkshire"
  ]
  node [
    id 530
    label "Bawaria"
  ]
  node [
    id 531
    label "Zag&#243;rze"
  ]
  node [
    id 532
    label "Jemen"
  ]
  node [
    id 533
    label "Nowa_Zelandia"
  ]
  node [
    id 534
    label "Andaluzja"
  ]
  node [
    id 535
    label "Namibia"
  ]
  node [
    id 536
    label "Nauru"
  ]
  node [
    id 537
    label "&#379;ywiecczyzna"
  ]
  node [
    id 538
    label "Brunei"
  ]
  node [
    id 539
    label "Oksytania"
  ]
  node [
    id 540
    label "Opolszczyzna"
  ]
  node [
    id 541
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 542
    label "Kociewie"
  ]
  node [
    id 543
    label "Khitai"
  ]
  node [
    id 544
    label "Mauretania"
  ]
  node [
    id 545
    label "Iran"
  ]
  node [
    id 546
    label "Gambia"
  ]
  node [
    id 547
    label "Somalia"
  ]
  node [
    id 548
    label "Holandia"
  ]
  node [
    id 549
    label "Lasko"
  ]
  node [
    id 550
    label "Turkmenistan"
  ]
  node [
    id 551
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 552
    label "Salwador"
  ]
  node [
    id 553
    label "woda"
  ]
  node [
    id 554
    label "linia"
  ]
  node [
    id 555
    label "zbi&#243;r"
  ]
  node [
    id 556
    label "ekoton"
  ]
  node [
    id 557
    label "str&#261;d"
  ]
  node [
    id 558
    label "koniec"
  ]
  node [
    id 559
    label "plantowa&#263;"
  ]
  node [
    id 560
    label "zapadnia"
  ]
  node [
    id 561
    label "budynek"
  ]
  node [
    id 562
    label "skorupa_ziemska"
  ]
  node [
    id 563
    label "glinowanie"
  ]
  node [
    id 564
    label "martwica"
  ]
  node [
    id 565
    label "teren"
  ]
  node [
    id 566
    label "litosfera"
  ]
  node [
    id 567
    label "penetrator"
  ]
  node [
    id 568
    label "glinowa&#263;"
  ]
  node [
    id 569
    label "domain"
  ]
  node [
    id 570
    label "podglebie"
  ]
  node [
    id 571
    label "kompleks_sorpcyjny"
  ]
  node [
    id 572
    label "miejsce"
  ]
  node [
    id 573
    label "kort"
  ]
  node [
    id 574
    label "czynnik_produkcji"
  ]
  node [
    id 575
    label "pojazd"
  ]
  node [
    id 576
    label "powierzchnia"
  ]
  node [
    id 577
    label "pr&#243;chnica"
  ]
  node [
    id 578
    label "pomieszczenie"
  ]
  node [
    id 579
    label "ryzosfera"
  ]
  node [
    id 580
    label "p&#322;aszczyzna"
  ]
  node [
    id 581
    label "dotleni&#263;"
  ]
  node [
    id 582
    label "glej"
  ]
  node [
    id 583
    label "pa&#324;stwo"
  ]
  node [
    id 584
    label "posadzka"
  ]
  node [
    id 585
    label "geosystem"
  ]
  node [
    id 586
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 587
    label "przestrze&#324;"
  ]
  node [
    id 588
    label "jednostka_organizacyjna"
  ]
  node [
    id 589
    label "struktura"
  ]
  node [
    id 590
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 591
    label "TOPR"
  ]
  node [
    id 592
    label "endecki"
  ]
  node [
    id 593
    label "zesp&#243;&#322;"
  ]
  node [
    id 594
    label "przedstawicielstwo"
  ]
  node [
    id 595
    label "od&#322;am"
  ]
  node [
    id 596
    label "Cepelia"
  ]
  node [
    id 597
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 598
    label "ZBoWiD"
  ]
  node [
    id 599
    label "organization"
  ]
  node [
    id 600
    label "centrala"
  ]
  node [
    id 601
    label "GOPR"
  ]
  node [
    id 602
    label "ZOMO"
  ]
  node [
    id 603
    label "ZMP"
  ]
  node [
    id 604
    label "komitet_koordynacyjny"
  ]
  node [
    id 605
    label "przybud&#243;wka"
  ]
  node [
    id 606
    label "boj&#243;wka"
  ]
  node [
    id 607
    label "p&#243;&#322;noc"
  ]
  node [
    id 608
    label "Kosowo"
  ]
  node [
    id 609
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 610
    label "Zab&#322;ocie"
  ]
  node [
    id 611
    label "zach&#243;d"
  ]
  node [
    id 612
    label "po&#322;udnie"
  ]
  node [
    id 613
    label "Pow&#261;zki"
  ]
  node [
    id 614
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 615
    label "Piotrowo"
  ]
  node [
    id 616
    label "Olszanica"
  ]
  node [
    id 617
    label "holarktyka"
  ]
  node [
    id 618
    label "Ruda_Pabianicka"
  ]
  node [
    id 619
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 620
    label "Ludwin&#243;w"
  ]
  node [
    id 621
    label "Arktyka"
  ]
  node [
    id 622
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 623
    label "Zabu&#380;e"
  ]
  node [
    id 624
    label "antroposfera"
  ]
  node [
    id 625
    label "terytorium"
  ]
  node [
    id 626
    label "Neogea"
  ]
  node [
    id 627
    label "Syberia_Zachodnia"
  ]
  node [
    id 628
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 629
    label "zakres"
  ]
  node [
    id 630
    label "pas_planetoid"
  ]
  node [
    id 631
    label "Syberia_Wschodnia"
  ]
  node [
    id 632
    label "Antarktyka"
  ]
  node [
    id 633
    label "Rakowice"
  ]
  node [
    id 634
    label "akrecja"
  ]
  node [
    id 635
    label "wymiar"
  ]
  node [
    id 636
    label "&#321;&#281;g"
  ]
  node [
    id 637
    label "Kresy_Zachodnie"
  ]
  node [
    id 638
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 639
    label "wsch&#243;d"
  ]
  node [
    id 640
    label "Notogea"
  ]
  node [
    id 641
    label "inti"
  ]
  node [
    id 642
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 643
    label "sol"
  ]
  node [
    id 644
    label "baht"
  ]
  node [
    id 645
    label "boliviano"
  ]
  node [
    id 646
    label "dong"
  ]
  node [
    id 647
    label "Annam"
  ]
  node [
    id 648
    label "colon"
  ]
  node [
    id 649
    label "Ameryka_Centralna"
  ]
  node [
    id 650
    label "Piemont"
  ]
  node [
    id 651
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 652
    label "NATO"
  ]
  node [
    id 653
    label "Sardynia"
  ]
  node [
    id 654
    label "Italia"
  ]
  node [
    id 655
    label "strefa_euro"
  ]
  node [
    id 656
    label "Ok&#281;cie"
  ]
  node [
    id 657
    label "Karyntia"
  ]
  node [
    id 658
    label "Romania"
  ]
  node [
    id 659
    label "Warszawa"
  ]
  node [
    id 660
    label "lir"
  ]
  node [
    id 661
    label "Sycylia"
  ]
  node [
    id 662
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 663
    label "Ad&#380;aria"
  ]
  node [
    id 664
    label "lari"
  ]
  node [
    id 665
    label "Jukatan"
  ]
  node [
    id 666
    label "dolar_Belize"
  ]
  node [
    id 667
    label "dolar"
  ]
  node [
    id 668
    label "Ohio"
  ]
  node [
    id 669
    label "P&#243;&#322;noc"
  ]
  node [
    id 670
    label "Nowy_York"
  ]
  node [
    id 671
    label "Illinois"
  ]
  node [
    id 672
    label "Po&#322;udnie"
  ]
  node [
    id 673
    label "Kalifornia"
  ]
  node [
    id 674
    label "Wirginia"
  ]
  node [
    id 675
    label "Teksas"
  ]
  node [
    id 676
    label "Waszyngton"
  ]
  node [
    id 677
    label "zielona_karta"
  ]
  node [
    id 678
    label "Alaska"
  ]
  node [
    id 679
    label "Massachusetts"
  ]
  node [
    id 680
    label "Hawaje"
  ]
  node [
    id 681
    label "Maryland"
  ]
  node [
    id 682
    label "Michigan"
  ]
  node [
    id 683
    label "Arizona"
  ]
  node [
    id 684
    label "Georgia"
  ]
  node [
    id 685
    label "stan_wolny"
  ]
  node [
    id 686
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 687
    label "Pensylwania"
  ]
  node [
    id 688
    label "Luizjana"
  ]
  node [
    id 689
    label "Nowy_Meksyk"
  ]
  node [
    id 690
    label "Wuj_Sam"
  ]
  node [
    id 691
    label "Alabama"
  ]
  node [
    id 692
    label "Kansas"
  ]
  node [
    id 693
    label "Oregon"
  ]
  node [
    id 694
    label "Zach&#243;d"
  ]
  node [
    id 695
    label "Oklahoma"
  ]
  node [
    id 696
    label "Floryda"
  ]
  node [
    id 697
    label "Hudson"
  ]
  node [
    id 698
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 699
    label "somoni"
  ]
  node [
    id 700
    label "perper"
  ]
  node [
    id 701
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 702
    label "euro"
  ]
  node [
    id 703
    label "Bengal"
  ]
  node [
    id 704
    label "taka"
  ]
  node [
    id 705
    label "Karelia"
  ]
  node [
    id 706
    label "Mari_El"
  ]
  node [
    id 707
    label "Inguszetia"
  ]
  node [
    id 708
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 709
    label "Udmurcja"
  ]
  node [
    id 710
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 711
    label "Newa"
  ]
  node [
    id 712
    label "&#321;adoga"
  ]
  node [
    id 713
    label "Czeczenia"
  ]
  node [
    id 714
    label "Anadyr"
  ]
  node [
    id 715
    label "Syberia"
  ]
  node [
    id 716
    label "Tatarstan"
  ]
  node [
    id 717
    label "Wszechrosja"
  ]
  node [
    id 718
    label "Azja"
  ]
  node [
    id 719
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 720
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 721
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 722
    label "Kamczatka"
  ]
  node [
    id 723
    label "Jama&#322;"
  ]
  node [
    id 724
    label "Dagestan"
  ]
  node [
    id 725
    label "Witim"
  ]
  node [
    id 726
    label "Tuwa"
  ]
  node [
    id 727
    label "car"
  ]
  node [
    id 728
    label "Komi"
  ]
  node [
    id 729
    label "Czuwaszja"
  ]
  node [
    id 730
    label "Chakasja"
  ]
  node [
    id 731
    label "Perm"
  ]
  node [
    id 732
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 733
    label "Ajon"
  ]
  node [
    id 734
    label "Adygeja"
  ]
  node [
    id 735
    label "Dniepr"
  ]
  node [
    id 736
    label "rubel_rosyjski"
  ]
  node [
    id 737
    label "Don"
  ]
  node [
    id 738
    label "Mordowia"
  ]
  node [
    id 739
    label "s&#322;owianofilstwo"
  ]
  node [
    id 740
    label "gourde"
  ]
  node [
    id 741
    label "escudo_angolskie"
  ]
  node [
    id 742
    label "kwanza"
  ]
  node [
    id 743
    label "ariary"
  ]
  node [
    id 744
    label "Ocean_Indyjski"
  ]
  node [
    id 745
    label "frank_malgaski"
  ]
  node [
    id 746
    label "Unia_Europejska"
  ]
  node [
    id 747
    label "Wile&#324;szczyzna"
  ]
  node [
    id 748
    label "Windawa"
  ]
  node [
    id 749
    label "&#379;mud&#378;"
  ]
  node [
    id 750
    label "lit"
  ]
  node [
    id 751
    label "Synaj"
  ]
  node [
    id 752
    label "paraszyt"
  ]
  node [
    id 753
    label "funt_egipski"
  ]
  node [
    id 754
    label "birr"
  ]
  node [
    id 755
    label "negus"
  ]
  node [
    id 756
    label "peso_kolumbijskie"
  ]
  node [
    id 757
    label "Orinoko"
  ]
  node [
    id 758
    label "rial_katarski"
  ]
  node [
    id 759
    label "dram"
  ]
  node [
    id 760
    label "Limburgia"
  ]
  node [
    id 761
    label "gulden"
  ]
  node [
    id 762
    label "Zelandia"
  ]
  node [
    id 763
    label "Niderlandy"
  ]
  node [
    id 764
    label "Brabancja"
  ]
  node [
    id 765
    label "cedi"
  ]
  node [
    id 766
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 767
    label "milrejs"
  ]
  node [
    id 768
    label "cruzado"
  ]
  node [
    id 769
    label "real"
  ]
  node [
    id 770
    label "frank_monakijski"
  ]
  node [
    id 771
    label "Fryburg"
  ]
  node [
    id 772
    label "Bazylea"
  ]
  node [
    id 773
    label "Alpy"
  ]
  node [
    id 774
    label "frank_szwajcarski"
  ]
  node [
    id 775
    label "Helwecja"
  ]
  node [
    id 776
    label "Berno"
  ]
  node [
    id 777
    label "lej_mo&#322;dawski"
  ]
  node [
    id 778
    label "Dniestr"
  ]
  node [
    id 779
    label "Gagauzja"
  ]
  node [
    id 780
    label "Indie_Zachodnie"
  ]
  node [
    id 781
    label "Sikkim"
  ]
  node [
    id 782
    label "Asam"
  ]
  node [
    id 783
    label "rupia_indyjska"
  ]
  node [
    id 784
    label "Indie_Portugalskie"
  ]
  node [
    id 785
    label "Indie_Wschodnie"
  ]
  node [
    id 786
    label "Bollywood"
  ]
  node [
    id 787
    label "Pend&#380;ab"
  ]
  node [
    id 788
    label "boliwar"
  ]
  node [
    id 789
    label "naira"
  ]
  node [
    id 790
    label "frank_gwinejski"
  ]
  node [
    id 791
    label "sum"
  ]
  node [
    id 792
    label "Karaka&#322;pacja"
  ]
  node [
    id 793
    label "dolar_liberyjski"
  ]
  node [
    id 794
    label "Dacja"
  ]
  node [
    id 795
    label "lej_rumu&#324;ski"
  ]
  node [
    id 796
    label "Siedmiogr&#243;d"
  ]
  node [
    id 797
    label "Dobrud&#380;a"
  ]
  node [
    id 798
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 799
    label "dolar_namibijski"
  ]
  node [
    id 800
    label "kuna"
  ]
  node [
    id 801
    label "Rugia"
  ]
  node [
    id 802
    label "Saksonia"
  ]
  node [
    id 803
    label "Dolna_Saksonia"
  ]
  node [
    id 804
    label "Anglosas"
  ]
  node [
    id 805
    label "Hesja"
  ]
  node [
    id 806
    label "Wirtembergia"
  ]
  node [
    id 807
    label "Po&#322;abie"
  ]
  node [
    id 808
    label "Germania"
  ]
  node [
    id 809
    label "Frankonia"
  ]
  node [
    id 810
    label "Badenia"
  ]
  node [
    id 811
    label "Holsztyn"
  ]
  node [
    id 812
    label "marka"
  ]
  node [
    id 813
    label "Brandenburgia"
  ]
  node [
    id 814
    label "Szwabia"
  ]
  node [
    id 815
    label "Niemcy_Zachodnie"
  ]
  node [
    id 816
    label "Westfalia"
  ]
  node [
    id 817
    label "Helgoland"
  ]
  node [
    id 818
    label "Karlsbad"
  ]
  node [
    id 819
    label "Niemcy_Wschodnie"
  ]
  node [
    id 820
    label "korona_w&#281;gierska"
  ]
  node [
    id 821
    label "forint"
  ]
  node [
    id 822
    label "Lipt&#243;w"
  ]
  node [
    id 823
    label "tenge"
  ]
  node [
    id 824
    label "szach"
  ]
  node [
    id 825
    label "Baktria"
  ]
  node [
    id 826
    label "afgani"
  ]
  node [
    id 827
    label "kip"
  ]
  node [
    id 828
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 829
    label "Salzburg"
  ]
  node [
    id 830
    label "Rakuzy"
  ]
  node [
    id 831
    label "Dyja"
  ]
  node [
    id 832
    label "konsulent"
  ]
  node [
    id 833
    label "szyling_austryjacki"
  ]
  node [
    id 834
    label "peso_urugwajskie"
  ]
  node [
    id 835
    label "rial_jeme&#324;ski"
  ]
  node [
    id 836
    label "korona_esto&#324;ska"
  ]
  node [
    id 837
    label "Inflanty"
  ]
  node [
    id 838
    label "marka_esto&#324;ska"
  ]
  node [
    id 839
    label "tala"
  ]
  node [
    id 840
    label "Podole"
  ]
  node [
    id 841
    label "Wsch&#243;d"
  ]
  node [
    id 842
    label "Naddnieprze"
  ]
  node [
    id 843
    label "Ma&#322;orosja"
  ]
  node [
    id 844
    label "Wo&#322;y&#324;"
  ]
  node [
    id 845
    label "Nadbu&#380;e"
  ]
  node [
    id 846
    label "hrywna"
  ]
  node [
    id 847
    label "Zaporo&#380;e"
  ]
  node [
    id 848
    label "Krym"
  ]
  node [
    id 849
    label "Przykarpacie"
  ]
  node [
    id 850
    label "Kozaczyzna"
  ]
  node [
    id 851
    label "karbowaniec"
  ]
  node [
    id 852
    label "riel"
  ]
  node [
    id 853
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 854
    label "kyat"
  ]
  node [
    id 855
    label "Arakan"
  ]
  node [
    id 856
    label "funt_liba&#324;ski"
  ]
  node [
    id 857
    label "Mariany"
  ]
  node [
    id 858
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 859
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 860
    label "dinar_algierski"
  ]
  node [
    id 861
    label "ringgit"
  ]
  node [
    id 862
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 863
    label "Borneo"
  ]
  node [
    id 864
    label "peso_dominika&#324;skie"
  ]
  node [
    id 865
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 866
    label "peso_kuba&#324;skie"
  ]
  node [
    id 867
    label "lira_izraelska"
  ]
  node [
    id 868
    label "szekel"
  ]
  node [
    id 869
    label "Galilea"
  ]
  node [
    id 870
    label "Judea"
  ]
  node [
    id 871
    label "tolar"
  ]
  node [
    id 872
    label "frank_luksemburski"
  ]
  node [
    id 873
    label "lempira"
  ]
  node [
    id 874
    label "Pozna&#324;"
  ]
  node [
    id 875
    label "lira_malta&#324;ska"
  ]
  node [
    id 876
    label "Gozo"
  ]
  node [
    id 877
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 878
    label "Paros"
  ]
  node [
    id 879
    label "Epir"
  ]
  node [
    id 880
    label "panhellenizm"
  ]
  node [
    id 881
    label "Eubea"
  ]
  node [
    id 882
    label "Rodos"
  ]
  node [
    id 883
    label "Achaja"
  ]
  node [
    id 884
    label "Termopile"
  ]
  node [
    id 885
    label "Attyka"
  ]
  node [
    id 886
    label "Hellada"
  ]
  node [
    id 887
    label "Etolia"
  ]
  node [
    id 888
    label "palestra"
  ]
  node [
    id 889
    label "Kreta"
  ]
  node [
    id 890
    label "drachma"
  ]
  node [
    id 891
    label "Olimp"
  ]
  node [
    id 892
    label "Tesalia"
  ]
  node [
    id 893
    label "Peloponez"
  ]
  node [
    id 894
    label "Eolia"
  ]
  node [
    id 895
    label "Beocja"
  ]
  node [
    id 896
    label "Parnas"
  ]
  node [
    id 897
    label "Lesbos"
  ]
  node [
    id 898
    label "Atlantyk"
  ]
  node [
    id 899
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 900
    label "Ulster"
  ]
  node [
    id 901
    label "funt_irlandzki"
  ]
  node [
    id 902
    label "tugrik"
  ]
  node [
    id 903
    label "Buriaci"
  ]
  node [
    id 904
    label "ajmak"
  ]
  node [
    id 905
    label "denar_macedo&#324;ski"
  ]
  node [
    id 906
    label "Pikardia"
  ]
  node [
    id 907
    label "Masyw_Centralny"
  ]
  node [
    id 908
    label "Akwitania"
  ]
  node [
    id 909
    label "Alzacja"
  ]
  node [
    id 910
    label "Sekwana"
  ]
  node [
    id 911
    label "Langwedocja"
  ]
  node [
    id 912
    label "Martynika"
  ]
  node [
    id 913
    label "Bretania"
  ]
  node [
    id 914
    label "Sabaudia"
  ]
  node [
    id 915
    label "Korsyka"
  ]
  node [
    id 916
    label "Normandia"
  ]
  node [
    id 917
    label "Gaskonia"
  ]
  node [
    id 918
    label "Burgundia"
  ]
  node [
    id 919
    label "frank_francuski"
  ]
  node [
    id 920
    label "Wandea"
  ]
  node [
    id 921
    label "Prowansja"
  ]
  node [
    id 922
    label "Gwadelupa"
  ]
  node [
    id 923
    label "lew"
  ]
  node [
    id 924
    label "c&#243;rdoba"
  ]
  node [
    id 925
    label "dolar_Zimbabwe"
  ]
  node [
    id 926
    label "frank_rwandyjski"
  ]
  node [
    id 927
    label "kwacha_zambijska"
  ]
  node [
    id 928
    label "Kurlandia"
  ]
  node [
    id 929
    label "&#322;at"
  ]
  node [
    id 930
    label "Liwonia"
  ]
  node [
    id 931
    label "rubel_&#322;otewski"
  ]
  node [
    id 932
    label "Himalaje"
  ]
  node [
    id 933
    label "rupia_nepalska"
  ]
  node [
    id 934
    label "funt_suda&#324;ski"
  ]
  node [
    id 935
    label "dolar_bahamski"
  ]
  node [
    id 936
    label "Wielka_Bahama"
  ]
  node [
    id 937
    label "Pa&#322;uki"
  ]
  node [
    id 938
    label "Wolin"
  ]
  node [
    id 939
    label "z&#322;oty"
  ]
  node [
    id 940
    label "So&#322;a"
  ]
  node [
    id 941
    label "Suwalszczyzna"
  ]
  node [
    id 942
    label "Krajna"
  ]
  node [
    id 943
    label "barwy_polskie"
  ]
  node [
    id 944
    label "Izera"
  ]
  node [
    id 945
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 946
    label "Kaczawa"
  ]
  node [
    id 947
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 948
    label "Wis&#322;a"
  ]
  node [
    id 949
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 950
    label "Antyle"
  ]
  node [
    id 951
    label "dolar_Tuvalu"
  ]
  node [
    id 952
    label "dinar_iracki"
  ]
  node [
    id 953
    label "korona_s&#322;owacka"
  ]
  node [
    id 954
    label "Turiec"
  ]
  node [
    id 955
    label "jen"
  ]
  node [
    id 956
    label "jinja"
  ]
  node [
    id 957
    label "Okinawa"
  ]
  node [
    id 958
    label "Japonica"
  ]
  node [
    id 959
    label "manat_turkme&#324;ski"
  ]
  node [
    id 960
    label "szyling_kenijski"
  ]
  node [
    id 961
    label "peso_chilijskie"
  ]
  node [
    id 962
    label "Zanzibar"
  ]
  node [
    id 963
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 964
    label "peso_filipi&#324;skie"
  ]
  node [
    id 965
    label "Cebu"
  ]
  node [
    id 966
    label "Sahara"
  ]
  node [
    id 967
    label "Tasmania"
  ]
  node [
    id 968
    label "Nowy_&#346;wiat"
  ]
  node [
    id 969
    label "dolar_australijski"
  ]
  node [
    id 970
    label "Quebec"
  ]
  node [
    id 971
    label "dolar_kanadyjski"
  ]
  node [
    id 972
    label "Nowa_Fundlandia"
  ]
  node [
    id 973
    label "quetzal"
  ]
  node [
    id 974
    label "Manica"
  ]
  node [
    id 975
    label "escudo_mozambickie"
  ]
  node [
    id 976
    label "Cabo_Delgado"
  ]
  node [
    id 977
    label "Inhambane"
  ]
  node [
    id 978
    label "Maputo"
  ]
  node [
    id 979
    label "Gaza"
  ]
  node [
    id 980
    label "Niasa"
  ]
  node [
    id 981
    label "Nampula"
  ]
  node [
    id 982
    label "metical"
  ]
  node [
    id 983
    label "frank_tunezyjski"
  ]
  node [
    id 984
    label "dinar_tunezyjski"
  ]
  node [
    id 985
    label "lud"
  ]
  node [
    id 986
    label "frank_kongijski"
  ]
  node [
    id 987
    label "peso_argenty&#324;skie"
  ]
  node [
    id 988
    label "dinar_Bahrajnu"
  ]
  node [
    id 989
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 990
    label "escudo_portugalskie"
  ]
  node [
    id 991
    label "Melanezja"
  ]
  node [
    id 992
    label "dolar_Fid&#380;i"
  ]
  node [
    id 993
    label "d&#380;amahirijja"
  ]
  node [
    id 994
    label "dinar_libijski"
  ]
  node [
    id 995
    label "balboa"
  ]
  node [
    id 996
    label "dolar_surinamski"
  ]
  node [
    id 997
    label "dolar_Brunei"
  ]
  node [
    id 998
    label "Estremadura"
  ]
  node [
    id 999
    label "Kastylia"
  ]
  node [
    id 1000
    label "Rzym_Zachodni"
  ]
  node [
    id 1001
    label "Aragonia"
  ]
  node [
    id 1002
    label "hacjender"
  ]
  node [
    id 1003
    label "Asturia"
  ]
  node [
    id 1004
    label "Baskonia"
  ]
  node [
    id 1005
    label "Majorka"
  ]
  node [
    id 1006
    label "Walencja"
  ]
  node [
    id 1007
    label "peseta"
  ]
  node [
    id 1008
    label "Katalonia"
  ]
  node [
    id 1009
    label "Luksemburgia"
  ]
  node [
    id 1010
    label "frank_belgijski"
  ]
  node [
    id 1011
    label "Walonia"
  ]
  node [
    id 1012
    label "Flandria"
  ]
  node [
    id 1013
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1014
    label "dolar_Barbadosu"
  ]
  node [
    id 1015
    label "korona_czeska"
  ]
  node [
    id 1016
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1017
    label "Wojwodina"
  ]
  node [
    id 1018
    label "dinar_serbski"
  ]
  node [
    id 1019
    label "funt_syryjski"
  ]
  node [
    id 1020
    label "alawizm"
  ]
  node [
    id 1021
    label "Szantung"
  ]
  node [
    id 1022
    label "Chiny_Zachodnie"
  ]
  node [
    id 1023
    label "Kuantung"
  ]
  node [
    id 1024
    label "D&#380;ungaria"
  ]
  node [
    id 1025
    label "yuan"
  ]
  node [
    id 1026
    label "Hongkong"
  ]
  node [
    id 1027
    label "Chiny_Wschodnie"
  ]
  node [
    id 1028
    label "Guangdong"
  ]
  node [
    id 1029
    label "Junnan"
  ]
  node [
    id 1030
    label "Mand&#380;uria"
  ]
  node [
    id 1031
    label "Syczuan"
  ]
  node [
    id 1032
    label "zair"
  ]
  node [
    id 1033
    label "Katanga"
  ]
  node [
    id 1034
    label "ugija"
  ]
  node [
    id 1035
    label "dalasi"
  ]
  node [
    id 1036
    label "funt_cypryjski"
  ]
  node [
    id 1037
    label "Afrodyzje"
  ]
  node [
    id 1038
    label "para"
  ]
  node [
    id 1039
    label "frank_alba&#324;ski"
  ]
  node [
    id 1040
    label "lek"
  ]
  node [
    id 1041
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1042
    label "dolar_jamajski"
  ]
  node [
    id 1043
    label "kafar"
  ]
  node [
    id 1044
    label "Ocean_Spokojny"
  ]
  node [
    id 1045
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1046
    label "som"
  ]
  node [
    id 1047
    label "guarani"
  ]
  node [
    id 1048
    label "rial_ira&#324;ski"
  ]
  node [
    id 1049
    label "mu&#322;&#322;a"
  ]
  node [
    id 1050
    label "Persja"
  ]
  node [
    id 1051
    label "Jawa"
  ]
  node [
    id 1052
    label "Sumatra"
  ]
  node [
    id 1053
    label "rupia_indonezyjska"
  ]
  node [
    id 1054
    label "Nowa_Gwinea"
  ]
  node [
    id 1055
    label "Moluki"
  ]
  node [
    id 1056
    label "szyling_somalijski"
  ]
  node [
    id 1057
    label "szyling_ugandyjski"
  ]
  node [
    id 1058
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1059
    label "Ujgur"
  ]
  node [
    id 1060
    label "Azja_Mniejsza"
  ]
  node [
    id 1061
    label "lira_turecka"
  ]
  node [
    id 1062
    label "Pireneje"
  ]
  node [
    id 1063
    label "nakfa"
  ]
  node [
    id 1064
    label "won"
  ]
  node [
    id 1065
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1066
    label "&#346;wite&#378;"
  ]
  node [
    id 1067
    label "dinar_kuwejcki"
  ]
  node [
    id 1068
    label "Nachiczewan"
  ]
  node [
    id 1069
    label "manat_azerski"
  ]
  node [
    id 1070
    label "Karabach"
  ]
  node [
    id 1071
    label "dolar_Kiribati"
  ]
  node [
    id 1072
    label "moszaw"
  ]
  node [
    id 1073
    label "Kanaan"
  ]
  node [
    id 1074
    label "Aruba"
  ]
  node [
    id 1075
    label "Kajmany"
  ]
  node [
    id 1076
    label "Anguilla"
  ]
  node [
    id 1077
    label "Mogielnica"
  ]
  node [
    id 1078
    label "jezioro"
  ]
  node [
    id 1079
    label "Rumelia"
  ]
  node [
    id 1080
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1081
    label "Poprad"
  ]
  node [
    id 1082
    label "Tatry"
  ]
  node [
    id 1083
    label "Podtatrze"
  ]
  node [
    id 1084
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1085
    label "Austro-W&#281;gry"
  ]
  node [
    id 1086
    label "Biskupice"
  ]
  node [
    id 1087
    label "Iwanowice"
  ]
  node [
    id 1088
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1089
    label "Rogo&#378;nik"
  ]
  node [
    id 1090
    label "Ropa"
  ]
  node [
    id 1091
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1092
    label "Karpaty"
  ]
  node [
    id 1093
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1094
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1095
    label "Beskid_Niski"
  ]
  node [
    id 1096
    label "Etruria"
  ]
  node [
    id 1097
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1098
    label "Bojanowo"
  ]
  node [
    id 1099
    label "Obra"
  ]
  node [
    id 1100
    label "Wilkowo_Polskie"
  ]
  node [
    id 1101
    label "Dobra"
  ]
  node [
    id 1102
    label "Buriacja"
  ]
  node [
    id 1103
    label "Rozewie"
  ]
  node [
    id 1104
    label "&#346;l&#261;sk"
  ]
  node [
    id 1105
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1106
    label "Norwegia"
  ]
  node [
    id 1107
    label "Szwecja"
  ]
  node [
    id 1108
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1109
    label "Finlandia"
  ]
  node [
    id 1110
    label "Wiktoria"
  ]
  node [
    id 1111
    label "Guernsey"
  ]
  node [
    id 1112
    label "Conrad"
  ]
  node [
    id 1113
    label "funt_szterling"
  ]
  node [
    id 1114
    label "Portland"
  ]
  node [
    id 1115
    label "El&#380;bieta_I"
  ]
  node [
    id 1116
    label "Kornwalia"
  ]
  node [
    id 1117
    label "Amazonka"
  ]
  node [
    id 1118
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1119
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1120
    label "Moza"
  ]
  node [
    id 1121
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1122
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1123
    label "Paj&#281;czno"
  ]
  node [
    id 1124
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1125
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1126
    label "Gop&#322;o"
  ]
  node [
    id 1127
    label "Jerozolima"
  ]
  node [
    id 1128
    label "Dolna_Frankonia"
  ]
  node [
    id 1129
    label "funt_szkocki"
  ]
  node [
    id 1130
    label "Kaledonia"
  ]
  node [
    id 1131
    label "Abchazja"
  ]
  node [
    id 1132
    label "Sarmata"
  ]
  node [
    id 1133
    label "Eurazja"
  ]
  node [
    id 1134
    label "Mariensztat"
  ]
  node [
    id 1135
    label "dawniej"
  ]
  node [
    id 1136
    label "zgodnie"
  ]
  node [
    id 1137
    label "historyczny"
  ]
  node [
    id 1138
    label "pierwotnie"
  ]
  node [
    id 1139
    label "dawny"
  ]
  node [
    id 1140
    label "wcze&#347;niej"
  ]
  node [
    id 1141
    label "kiedy&#347;"
  ]
  node [
    id 1142
    label "prymitywny"
  ]
  node [
    id 1143
    label "pocz&#261;tkowo"
  ]
  node [
    id 1144
    label "pierwotny"
  ]
  node [
    id 1145
    label "dziko"
  ]
  node [
    id 1146
    label "instynktownie"
  ]
  node [
    id 1147
    label "niezale&#380;nie"
  ]
  node [
    id 1148
    label "dobrze"
  ]
  node [
    id 1149
    label "spokojnie"
  ]
  node [
    id 1150
    label "zbie&#380;nie"
  ]
  node [
    id 1151
    label "zgodny"
  ]
  node [
    id 1152
    label "jednakowo"
  ]
  node [
    id 1153
    label "dziejowo"
  ]
  node [
    id 1154
    label "wiekopomny"
  ]
  node [
    id 1155
    label "dramat"
  ]
  node [
    id 1156
    label "powie&#347;&#263;_fantastyczna"
  ]
  node [
    id 1157
    label "film_fabularny"
  ]
  node [
    id 1158
    label "miscarriage"
  ]
  node [
    id 1159
    label "makabra"
  ]
  node [
    id 1160
    label "czarny_humor"
  ]
  node [
    id 1161
    label "styl"
  ]
  node [
    id 1162
    label "wydarzenie"
  ]
  node [
    id 1163
    label "rodzaj_literacki"
  ]
  node [
    id 1164
    label "drama"
  ]
  node [
    id 1165
    label "scenariusz"
  ]
  node [
    id 1166
    label "cios"
  ]
  node [
    id 1167
    label "film"
  ]
  node [
    id 1168
    label "didaskalia"
  ]
  node [
    id 1169
    label "utw&#243;r"
  ]
  node [
    id 1170
    label "literatura"
  ]
  node [
    id 1171
    label "Faust"
  ]
  node [
    id 1172
    label "sk&#261;py"
  ]
  node [
    id 1173
    label "chciwiec"
  ]
  node [
    id 1174
    label "sk&#261;piarz"
  ]
  node [
    id 1175
    label "mosiek"
  ]
  node [
    id 1176
    label "materialista"
  ]
  node [
    id 1177
    label "szmonces"
  ]
  node [
    id 1178
    label "wyznawca"
  ]
  node [
    id 1179
    label "&#379;ydziak"
  ]
  node [
    id 1180
    label "judenrat"
  ]
  node [
    id 1181
    label "monoteista"
  ]
  node [
    id 1182
    label "czciciel"
  ]
  node [
    id 1183
    label "teista"
  ]
  node [
    id 1184
    label "religia"
  ]
  node [
    id 1185
    label "wierzenie"
  ]
  node [
    id 1186
    label "Lamarck"
  ]
  node [
    id 1187
    label "monista"
  ]
  node [
    id 1188
    label "egoista"
  ]
  node [
    id 1189
    label "&#379;yd"
  ]
  node [
    id 1190
    label "sk&#261;piec"
  ]
  node [
    id 1191
    label "rada"
  ]
  node [
    id 1192
    label "skecz"
  ]
  node [
    id 1193
    label "dowcip"
  ]
  node [
    id 1194
    label "nieobfity"
  ]
  node [
    id 1195
    label "mizerny"
  ]
  node [
    id 1196
    label "sk&#261;po"
  ]
  node [
    id 1197
    label "nienale&#380;yty"
  ]
  node [
    id 1198
    label "niewystarczaj&#261;cy"
  ]
  node [
    id 1199
    label "oszcz&#281;dny"
  ]
  node [
    id 1200
    label "z&#322;y"
  ]
  node [
    id 1201
    label "powiada&#263;"
  ]
  node [
    id 1202
    label "komunikowa&#263;"
  ]
  node [
    id 1203
    label "inform"
  ]
  node [
    id 1204
    label "communicate"
  ]
  node [
    id 1205
    label "powodowa&#263;"
  ]
  node [
    id 1206
    label "mawia&#263;"
  ]
  node [
    id 1207
    label "m&#243;wi&#263;"
  ]
  node [
    id 1208
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1209
    label "telegraficznie"
  ]
  node [
    id 1210
    label "sprawny"
  ]
  node [
    id 1211
    label "efektywny"
  ]
  node [
    id 1212
    label "kr&#243;tki"
  ]
  node [
    id 1213
    label "zwi&#281;&#378;le"
  ]
  node [
    id 1214
    label "kr&#243;tko"
  ]
  node [
    id 1215
    label "&#380;ydowski"
  ]
  node [
    id 1216
    label "agencja"
  ]
  node [
    id 1217
    label "Joel"
  ]
  node [
    id 1218
    label "Lion"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 789
  ]
  edge [
    source 7
    target 790
  ]
  edge [
    source 7
    target 791
  ]
  edge [
    source 7
    target 792
  ]
  edge [
    source 7
    target 793
  ]
  edge [
    source 7
    target 794
  ]
  edge [
    source 7
    target 795
  ]
  edge [
    source 7
    target 796
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 798
  ]
  edge [
    source 7
    target 799
  ]
  edge [
    source 7
    target 800
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 802
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 804
  ]
  edge [
    source 7
    target 805
  ]
  edge [
    source 7
    target 806
  ]
  edge [
    source 7
    target 807
  ]
  edge [
    source 7
    target 808
  ]
  edge [
    source 7
    target 809
  ]
  edge [
    source 7
    target 810
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 7
    target 817
  ]
  edge [
    source 7
    target 818
  ]
  edge [
    source 7
    target 819
  ]
  edge [
    source 7
    target 820
  ]
  edge [
    source 7
    target 821
  ]
  edge [
    source 7
    target 822
  ]
  edge [
    source 7
    target 823
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 7
    target 839
  ]
  edge [
    source 7
    target 840
  ]
  edge [
    source 7
    target 841
  ]
  edge [
    source 7
    target 842
  ]
  edge [
    source 7
    target 843
  ]
  edge [
    source 7
    target 844
  ]
  edge [
    source 7
    target 845
  ]
  edge [
    source 7
    target 846
  ]
  edge [
    source 7
    target 847
  ]
  edge [
    source 7
    target 848
  ]
  edge [
    source 7
    target 849
  ]
  edge [
    source 7
    target 850
  ]
  edge [
    source 7
    target 851
  ]
  edge [
    source 7
    target 852
  ]
  edge [
    source 7
    target 853
  ]
  edge [
    source 7
    target 854
  ]
  edge [
    source 7
    target 855
  ]
  edge [
    source 7
    target 856
  ]
  edge [
    source 7
    target 857
  ]
  edge [
    source 7
    target 858
  ]
  edge [
    source 7
    target 859
  ]
  edge [
    source 7
    target 860
  ]
  edge [
    source 7
    target 861
  ]
  edge [
    source 7
    target 862
  ]
  edge [
    source 7
    target 863
  ]
  edge [
    source 7
    target 864
  ]
  edge [
    source 7
    target 865
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 867
  ]
  edge [
    source 7
    target 868
  ]
  edge [
    source 7
    target 869
  ]
  edge [
    source 7
    target 870
  ]
  edge [
    source 7
    target 871
  ]
  edge [
    source 7
    target 872
  ]
  edge [
    source 7
    target 873
  ]
  edge [
    source 7
    target 874
  ]
  edge [
    source 7
    target 875
  ]
  edge [
    source 7
    target 876
  ]
  edge [
    source 7
    target 877
  ]
  edge [
    source 7
    target 878
  ]
  edge [
    source 7
    target 879
  ]
  edge [
    source 7
    target 880
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 882
  ]
  edge [
    source 7
    target 883
  ]
  edge [
    source 7
    target 884
  ]
  edge [
    source 7
    target 885
  ]
  edge [
    source 7
    target 886
  ]
  edge [
    source 7
    target 887
  ]
  edge [
    source 7
    target 888
  ]
  edge [
    source 7
    target 889
  ]
  edge [
    source 7
    target 890
  ]
  edge [
    source 7
    target 891
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 893
  ]
  edge [
    source 7
    target 894
  ]
  edge [
    source 7
    target 895
  ]
  edge [
    source 7
    target 896
  ]
  edge [
    source 7
    target 897
  ]
  edge [
    source 7
    target 898
  ]
  edge [
    source 7
    target 899
  ]
  edge [
    source 7
    target 900
  ]
  edge [
    source 7
    target 901
  ]
  edge [
    source 7
    target 902
  ]
  edge [
    source 7
    target 903
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 7
    target 907
  ]
  edge [
    source 7
    target 908
  ]
  edge [
    source 7
    target 909
  ]
  edge [
    source 7
    target 910
  ]
  edge [
    source 7
    target 911
  ]
  edge [
    source 7
    target 912
  ]
  edge [
    source 7
    target 913
  ]
  edge [
    source 7
    target 914
  ]
  edge [
    source 7
    target 915
  ]
  edge [
    source 7
    target 916
  ]
  edge [
    source 7
    target 917
  ]
  edge [
    source 7
    target 918
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 921
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 923
  ]
  edge [
    source 7
    target 924
  ]
  edge [
    source 7
    target 925
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 7
    target 928
  ]
  edge [
    source 7
    target 929
  ]
  edge [
    source 7
    target 930
  ]
  edge [
    source 7
    target 931
  ]
  edge [
    source 7
    target 932
  ]
  edge [
    source 7
    target 933
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 7
    target 937
  ]
  edge [
    source 7
    target 938
  ]
  edge [
    source 7
    target 939
  ]
  edge [
    source 7
    target 940
  ]
  edge [
    source 7
    target 941
  ]
  edge [
    source 7
    target 942
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 944
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 974
  ]
  edge [
    source 7
    target 975
  ]
  edge [
    source 7
    target 976
  ]
  edge [
    source 7
    target 977
  ]
  edge [
    source 7
    target 978
  ]
  edge [
    source 7
    target 979
  ]
  edge [
    source 7
    target 980
  ]
  edge [
    source 7
    target 981
  ]
  edge [
    source 7
    target 982
  ]
  edge [
    source 7
    target 983
  ]
  edge [
    source 7
    target 984
  ]
  edge [
    source 7
    target 985
  ]
  edge [
    source 7
    target 986
  ]
  edge [
    source 7
    target 987
  ]
  edge [
    source 7
    target 988
  ]
  edge [
    source 7
    target 989
  ]
  edge [
    source 7
    target 990
  ]
  edge [
    source 7
    target 991
  ]
  edge [
    source 7
    target 992
  ]
  edge [
    source 7
    target 993
  ]
  edge [
    source 7
    target 994
  ]
  edge [
    source 7
    target 995
  ]
  edge [
    source 7
    target 996
  ]
  edge [
    source 7
    target 997
  ]
  edge [
    source 7
    target 998
  ]
  edge [
    source 7
    target 999
  ]
  edge [
    source 7
    target 1000
  ]
  edge [
    source 7
    target 1001
  ]
  edge [
    source 7
    target 1002
  ]
  edge [
    source 7
    target 1003
  ]
  edge [
    source 7
    target 1004
  ]
  edge [
    source 7
    target 1005
  ]
  edge [
    source 7
    target 1006
  ]
  edge [
    source 7
    target 1007
  ]
  edge [
    source 7
    target 1008
  ]
  edge [
    source 7
    target 1009
  ]
  edge [
    source 7
    target 1010
  ]
  edge [
    source 7
    target 1011
  ]
  edge [
    source 7
    target 1012
  ]
  edge [
    source 7
    target 1013
  ]
  edge [
    source 7
    target 1014
  ]
  edge [
    source 7
    target 1015
  ]
  edge [
    source 7
    target 1016
  ]
  edge [
    source 7
    target 1017
  ]
  edge [
    source 7
    target 1018
  ]
  edge [
    source 7
    target 1019
  ]
  edge [
    source 7
    target 1020
  ]
  edge [
    source 7
    target 1021
  ]
  edge [
    source 7
    target 1022
  ]
  edge [
    source 7
    target 1023
  ]
  edge [
    source 7
    target 1024
  ]
  edge [
    source 7
    target 1025
  ]
  edge [
    source 7
    target 1026
  ]
  edge [
    source 7
    target 1027
  ]
  edge [
    source 7
    target 1028
  ]
  edge [
    source 7
    target 1029
  ]
  edge [
    source 7
    target 1030
  ]
  edge [
    source 7
    target 1031
  ]
  edge [
    source 7
    target 1032
  ]
  edge [
    source 7
    target 1033
  ]
  edge [
    source 7
    target 1034
  ]
  edge [
    source 7
    target 1035
  ]
  edge [
    source 7
    target 1036
  ]
  edge [
    source 7
    target 1037
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 7
    target 1039
  ]
  edge [
    source 7
    target 1040
  ]
  edge [
    source 7
    target 1041
  ]
  edge [
    source 7
    target 1042
  ]
  edge [
    source 7
    target 1043
  ]
  edge [
    source 7
    target 1044
  ]
  edge [
    source 7
    target 1045
  ]
  edge [
    source 7
    target 1046
  ]
  edge [
    source 7
    target 1047
  ]
  edge [
    source 7
    target 1048
  ]
  edge [
    source 7
    target 1049
  ]
  edge [
    source 7
    target 1050
  ]
  edge [
    source 7
    target 1051
  ]
  edge [
    source 7
    target 1052
  ]
  edge [
    source 7
    target 1053
  ]
  edge [
    source 7
    target 1054
  ]
  edge [
    source 7
    target 1055
  ]
  edge [
    source 7
    target 1056
  ]
  edge [
    source 7
    target 1057
  ]
  edge [
    source 7
    target 1058
  ]
  edge [
    source 7
    target 1059
  ]
  edge [
    source 7
    target 1060
  ]
  edge [
    source 7
    target 1061
  ]
  edge [
    source 7
    target 1062
  ]
  edge [
    source 7
    target 1063
  ]
  edge [
    source 7
    target 1064
  ]
  edge [
    source 7
    target 1065
  ]
  edge [
    source 7
    target 1066
  ]
  edge [
    source 7
    target 1067
  ]
  edge [
    source 7
    target 1068
  ]
  edge [
    source 7
    target 1069
  ]
  edge [
    source 7
    target 1070
  ]
  edge [
    source 7
    target 1071
  ]
  edge [
    source 7
    target 1072
  ]
  edge [
    source 7
    target 1073
  ]
  edge [
    source 7
    target 1074
  ]
  edge [
    source 7
    target 1075
  ]
  edge [
    source 7
    target 1076
  ]
  edge [
    source 7
    target 1077
  ]
  edge [
    source 7
    target 1078
  ]
  edge [
    source 7
    target 1079
  ]
  edge [
    source 7
    target 1080
  ]
  edge [
    source 7
    target 1081
  ]
  edge [
    source 7
    target 1082
  ]
  edge [
    source 7
    target 1083
  ]
  edge [
    source 7
    target 1084
  ]
  edge [
    source 7
    target 1085
  ]
  edge [
    source 7
    target 1086
  ]
  edge [
    source 7
    target 1087
  ]
  edge [
    source 7
    target 1088
  ]
  edge [
    source 7
    target 1089
  ]
  edge [
    source 7
    target 1090
  ]
  edge [
    source 7
    target 1091
  ]
  edge [
    source 7
    target 1092
  ]
  edge [
    source 7
    target 1093
  ]
  edge [
    source 7
    target 1094
  ]
  edge [
    source 7
    target 1095
  ]
  edge [
    source 7
    target 1096
  ]
  edge [
    source 7
    target 1097
  ]
  edge [
    source 7
    target 1098
  ]
  edge [
    source 7
    target 1099
  ]
  edge [
    source 7
    target 1100
  ]
  edge [
    source 7
    target 1101
  ]
  edge [
    source 7
    target 1102
  ]
  edge [
    source 7
    target 1103
  ]
  edge [
    source 7
    target 1104
  ]
  edge [
    source 7
    target 1105
  ]
  edge [
    source 7
    target 1106
  ]
  edge [
    source 7
    target 1107
  ]
  edge [
    source 7
    target 1108
  ]
  edge [
    source 7
    target 1109
  ]
  edge [
    source 7
    target 1110
  ]
  edge [
    source 7
    target 1111
  ]
  edge [
    source 7
    target 1112
  ]
  edge [
    source 7
    target 1113
  ]
  edge [
    source 7
    target 1114
  ]
  edge [
    source 7
    target 1115
  ]
  edge [
    source 7
    target 1116
  ]
  edge [
    source 7
    target 1117
  ]
  edge [
    source 7
    target 1118
  ]
  edge [
    source 7
    target 1119
  ]
  edge [
    source 7
    target 1120
  ]
  edge [
    source 7
    target 1121
  ]
  edge [
    source 7
    target 1122
  ]
  edge [
    source 7
    target 1123
  ]
  edge [
    source 7
    target 1124
  ]
  edge [
    source 7
    target 1125
  ]
  edge [
    source 7
    target 1126
  ]
  edge [
    source 7
    target 1127
  ]
  edge [
    source 7
    target 1128
  ]
  edge [
    source 7
    target 1129
  ]
  edge [
    source 7
    target 1130
  ]
  edge [
    source 7
    target 1131
  ]
  edge [
    source 7
    target 1132
  ]
  edge [
    source 7
    target 1133
  ]
  edge [
    source 7
    target 1134
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1135
  ]
  edge [
    source 8
    target 1136
  ]
  edge [
    source 8
    target 1137
  ]
  edge [
    source 8
    target 1138
  ]
  edge [
    source 8
    target 1139
  ]
  edge [
    source 8
    target 1140
  ]
  edge [
    source 8
    target 1141
  ]
  edge [
    source 8
    target 1142
  ]
  edge [
    source 8
    target 1143
  ]
  edge [
    source 8
    target 1144
  ]
  edge [
    source 8
    target 1145
  ]
  edge [
    source 8
    target 1146
  ]
  edge [
    source 8
    target 1147
  ]
  edge [
    source 8
    target 1148
  ]
  edge [
    source 8
    target 1149
  ]
  edge [
    source 8
    target 1150
  ]
  edge [
    source 8
    target 1151
  ]
  edge [
    source 8
    target 1152
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 1153
  ]
  edge [
    source 8
    target 1154
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1155
  ]
  edge [
    source 9
    target 1156
  ]
  edge [
    source 9
    target 1157
  ]
  edge [
    source 9
    target 1158
  ]
  edge [
    source 9
    target 1159
  ]
  edge [
    source 9
    target 1160
  ]
  edge [
    source 9
    target 1161
  ]
  edge [
    source 9
    target 1162
  ]
  edge [
    source 9
    target 1163
  ]
  edge [
    source 9
    target 1164
  ]
  edge [
    source 9
    target 1165
  ]
  edge [
    source 9
    target 1166
  ]
  edge [
    source 9
    target 1167
  ]
  edge [
    source 9
    target 1168
  ]
  edge [
    source 9
    target 1169
  ]
  edge [
    source 9
    target 1170
  ]
  edge [
    source 9
    target 1171
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1172
  ]
  edge [
    source 11
    target 1173
  ]
  edge [
    source 11
    target 1174
  ]
  edge [
    source 11
    target 1175
  ]
  edge [
    source 11
    target 1176
  ]
  edge [
    source 11
    target 1177
  ]
  edge [
    source 11
    target 1178
  ]
  edge [
    source 11
    target 1179
  ]
  edge [
    source 11
    target 1180
  ]
  edge [
    source 11
    target 1181
  ]
  edge [
    source 11
    target 1182
  ]
  edge [
    source 11
    target 1183
  ]
  edge [
    source 11
    target 1184
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 1185
  ]
  edge [
    source 11
    target 1186
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 1187
  ]
  edge [
    source 11
    target 1188
  ]
  edge [
    source 11
    target 33
  ]
  edge [
    source 11
    target 1189
  ]
  edge [
    source 11
    target 1190
  ]
  edge [
    source 11
    target 1191
  ]
  edge [
    source 11
    target 1192
  ]
  edge [
    source 11
    target 1193
  ]
  edge [
    source 11
    target 1194
  ]
  edge [
    source 11
    target 1195
  ]
  edge [
    source 11
    target 1196
  ]
  edge [
    source 11
    target 1197
  ]
  edge [
    source 11
    target 1198
  ]
  edge [
    source 11
    target 1199
  ]
  edge [
    source 11
    target 1200
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1208
  ]
  edge [
    source 13
    target 1209
  ]
  edge [
    source 13
    target 1210
  ]
  edge [
    source 13
    target 1211
  ]
  edge [
    source 13
    target 1212
  ]
  edge [
    source 13
    target 1213
  ]
  edge [
    source 13
    target 1199
  ]
  edge [
    source 13
    target 1214
  ]
  edge [
    source 13
    target 1215
  ]
  edge [
    source 13
    target 1216
  ]
  edge [
    source 1215
    target 1216
  ]
  edge [
    source 1217
    target 1218
  ]
]
