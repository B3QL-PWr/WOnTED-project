graph [
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 2
    label "smieszne"
    origin "text"
  ]
  node [
    id 3
    label "pomidor"
    origin "text"
  ]
  node [
    id 4
    label "memy"
    origin "text"
  ]
  node [
    id 5
    label "ro&#347;lina"
  ]
  node [
    id 6
    label "psiankowate"
  ]
  node [
    id 7
    label "tomato"
  ]
  node [
    id 8
    label "zabawa"
  ]
  node [
    id 9
    label "warzywo"
  ]
  node [
    id 10
    label "jagoda"
  ]
  node [
    id 11
    label "wypotnik"
  ]
  node [
    id 12
    label "pochewka"
  ]
  node [
    id 13
    label "strzyc"
  ]
  node [
    id 14
    label "wegetacja"
  ]
  node [
    id 15
    label "zadziorek"
  ]
  node [
    id 16
    label "flawonoid"
  ]
  node [
    id 17
    label "fitotron"
  ]
  node [
    id 18
    label "w&#322;&#243;kno"
  ]
  node [
    id 19
    label "zawi&#261;zek"
  ]
  node [
    id 20
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 21
    label "pora&#380;a&#263;"
  ]
  node [
    id 22
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 23
    label "zbiorowisko"
  ]
  node [
    id 24
    label "do&#322;owa&#263;"
  ]
  node [
    id 25
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 26
    label "hodowla"
  ]
  node [
    id 27
    label "wegetowa&#263;"
  ]
  node [
    id 28
    label "bulwka"
  ]
  node [
    id 29
    label "sok"
  ]
  node [
    id 30
    label "epiderma"
  ]
  node [
    id 31
    label "g&#322;uszy&#263;"
  ]
  node [
    id 32
    label "system_korzeniowy"
  ]
  node [
    id 33
    label "g&#322;uszenie"
  ]
  node [
    id 34
    label "owoc"
  ]
  node [
    id 35
    label "strzy&#380;enie"
  ]
  node [
    id 36
    label "p&#281;d"
  ]
  node [
    id 37
    label "wegetowanie"
  ]
  node [
    id 38
    label "fotoautotrof"
  ]
  node [
    id 39
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 40
    label "gumoza"
  ]
  node [
    id 41
    label "wyro&#347;le"
  ]
  node [
    id 42
    label "fitocenoza"
  ]
  node [
    id 43
    label "ro&#347;liny"
  ]
  node [
    id 44
    label "odn&#243;&#380;ka"
  ]
  node [
    id 45
    label "do&#322;owanie"
  ]
  node [
    id 46
    label "nieuleczalnie_chory"
  ]
  node [
    id 47
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 48
    label "bor&#243;wka"
  ]
  node [
    id 49
    label "chamefit"
  ]
  node [
    id 50
    label "policzek"
  ]
  node [
    id 51
    label "ro&#347;lina_kwasolubna"
  ]
  node [
    id 52
    label "produkt"
  ]
  node [
    id 53
    label "obieralnia"
  ]
  node [
    id 54
    label "blanszownik"
  ]
  node [
    id 55
    label "ogrodowizna"
  ]
  node [
    id 56
    label "zielenina"
  ]
  node [
    id 57
    label "wodzirej"
  ]
  node [
    id 58
    label "igra"
  ]
  node [
    id 59
    label "game"
  ]
  node [
    id 60
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 61
    label "impreza"
  ]
  node [
    id 62
    label "igraszka"
  ]
  node [
    id 63
    label "cecha"
  ]
  node [
    id 64
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 65
    label "taniec"
  ]
  node [
    id 66
    label "gambling"
  ]
  node [
    id 67
    label "rozrywka"
  ]
  node [
    id 68
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 69
    label "nabawienie_si&#281;"
  ]
  node [
    id 70
    label "ubaw"
  ]
  node [
    id 71
    label "chwyt"
  ]
  node [
    id 72
    label "Solanaceae"
  ]
  node [
    id 73
    label "psiankowce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
]
