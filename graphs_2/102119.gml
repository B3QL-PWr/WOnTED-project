graph [
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "test"
    origin "text"
  ]
  node [
    id 2
    label "tor"
    origin "text"
  ]
  node [
    id 3
    label "adam"
    origin "text"
  ]
  node [
    id 4
    label "je&#378;dzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "model"
    origin "text"
  ]
  node [
    id 6
    label "hpi"
    origin "text"
  ]
  node [
    id 7
    label "hellfire"
    origin "text"
  ]
  node [
    id 8
    label "udowodni&#263;"
    origin "text"
  ]
  node [
    id 9
    label "dobrze"
    origin "text"
  ]
  node [
    id 10
    label "ustawi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "truggy"
    origin "text"
  ]
  node [
    id 12
    label "bez"
    origin "text"
  ]
  node [
    id 13
    label "problem"
    origin "text"
  ]
  node [
    id 14
    label "rad"
    origin "text"
  ]
  node [
    id 15
    label "siebie"
    origin "text"
  ]
  node [
    id 16
    label "ciasny"
    origin "text"
  ]
  node [
    id 17
    label "zakr&#281;t"
    origin "text"
  ]
  node [
    id 18
    label "nieco"
    origin "text"
  ]
  node [
    id 19
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 20
    label "k&#322;opot"
    origin "text"
  ]
  node [
    id 21
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 22
    label "dorota"
    origin "text"
  ]
  node [
    id 23
    label "hubert"
    origin "text"
  ]
  node [
    id 24
    label "jarek"
    origin "text"
  ]
  node [
    id 25
    label "hyperem"
    origin "text"
  ]
  node [
    id 26
    label "ale"
    origin "text"
  ]
  node [
    id 27
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 28
    label "przed"
    origin "text"
  ]
  node [
    id 29
    label "wszyscy"
    origin "text"
  ]
  node [
    id 30
    label "mniejszy"
    origin "text"
  ]
  node [
    id 31
    label "umiej&#281;tno&#347;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "koniec"
    origin "text"
  ]
  node [
    id 33
    label "optymalny"
    origin "text"
  ]
  node [
    id 34
    label "ustawienie"
    origin "text"
  ]
  node [
    id 35
    label "godzina"
  ]
  node [
    id 36
    label "time"
  ]
  node [
    id 37
    label "doba"
  ]
  node [
    id 38
    label "p&#243;&#322;godzina"
  ]
  node [
    id 39
    label "jednostka_czasu"
  ]
  node [
    id 40
    label "czas"
  ]
  node [
    id 41
    label "minuta"
  ]
  node [
    id 42
    label "kwadrans"
  ]
  node [
    id 43
    label "badanie"
  ]
  node [
    id 44
    label "do&#347;wiadczenie"
  ]
  node [
    id 45
    label "narz&#281;dzie"
  ]
  node [
    id 46
    label "przechodzenie"
  ]
  node [
    id 47
    label "quiz"
  ]
  node [
    id 48
    label "sprawdzian"
  ]
  node [
    id 49
    label "arkusz"
  ]
  node [
    id 50
    label "sytuacja"
  ]
  node [
    id 51
    label "przechodzi&#263;"
  ]
  node [
    id 52
    label "&#347;rodek"
  ]
  node [
    id 53
    label "niezb&#281;dnik"
  ]
  node [
    id 54
    label "przedmiot"
  ]
  node [
    id 55
    label "spos&#243;b"
  ]
  node [
    id 56
    label "cz&#322;owiek"
  ]
  node [
    id 57
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 58
    label "tylec"
  ]
  node [
    id 59
    label "urz&#261;dzenie"
  ]
  node [
    id 60
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 61
    label "szko&#322;a"
  ]
  node [
    id 62
    label "obserwowanie"
  ]
  node [
    id 63
    label "wiedza"
  ]
  node [
    id 64
    label "wy&#347;wiadczenie"
  ]
  node [
    id 65
    label "wydarzenie"
  ]
  node [
    id 66
    label "assay"
  ]
  node [
    id 67
    label "znawstwo"
  ]
  node [
    id 68
    label "skill"
  ]
  node [
    id 69
    label "checkup"
  ]
  node [
    id 70
    label "spotkanie"
  ]
  node [
    id 71
    label "do&#347;wiadczanie"
  ]
  node [
    id 72
    label "zbadanie"
  ]
  node [
    id 73
    label "potraktowanie"
  ]
  node [
    id 74
    label "eksperiencja"
  ]
  node [
    id 75
    label "poczucie"
  ]
  node [
    id 76
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 77
    label "warunki"
  ]
  node [
    id 78
    label "szczeg&#243;&#322;"
  ]
  node [
    id 79
    label "state"
  ]
  node [
    id 80
    label "motyw"
  ]
  node [
    id 81
    label "realia"
  ]
  node [
    id 82
    label "faza"
  ]
  node [
    id 83
    label "podchodzi&#263;"
  ]
  node [
    id 84
    label "&#263;wiczenie"
  ]
  node [
    id 85
    label "pytanie"
  ]
  node [
    id 86
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 87
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 88
    label "praca_pisemna"
  ]
  node [
    id 89
    label "kontrola"
  ]
  node [
    id 90
    label "dydaktyka"
  ]
  node [
    id 91
    label "pr&#243;ba"
  ]
  node [
    id 92
    label "examination"
  ]
  node [
    id 93
    label "zrecenzowanie"
  ]
  node [
    id 94
    label "analysis"
  ]
  node [
    id 95
    label "rektalny"
  ]
  node [
    id 96
    label "ustalenie"
  ]
  node [
    id 97
    label "macanie"
  ]
  node [
    id 98
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 99
    label "usi&#322;owanie"
  ]
  node [
    id 100
    label "udowadnianie"
  ]
  node [
    id 101
    label "praca"
  ]
  node [
    id 102
    label "bia&#322;a_niedziela"
  ]
  node [
    id 103
    label "diagnostyka"
  ]
  node [
    id 104
    label "dociekanie"
  ]
  node [
    id 105
    label "rezultat"
  ]
  node [
    id 106
    label "sprawdzanie"
  ]
  node [
    id 107
    label "penetrowanie"
  ]
  node [
    id 108
    label "czynno&#347;&#263;"
  ]
  node [
    id 109
    label "krytykowanie"
  ]
  node [
    id 110
    label "omawianie"
  ]
  node [
    id 111
    label "ustalanie"
  ]
  node [
    id 112
    label "rozpatrywanie"
  ]
  node [
    id 113
    label "investigation"
  ]
  node [
    id 114
    label "wziernikowanie"
  ]
  node [
    id 115
    label "p&#322;at"
  ]
  node [
    id 116
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 117
    label "zgaduj-zgadula"
  ]
  node [
    id 118
    label "konkurs"
  ]
  node [
    id 119
    label "program"
  ]
  node [
    id 120
    label "przemakanie"
  ]
  node [
    id 121
    label "przestawanie"
  ]
  node [
    id 122
    label "nasycanie_si&#281;"
  ]
  node [
    id 123
    label "popychanie"
  ]
  node [
    id 124
    label "dostawanie_si&#281;"
  ]
  node [
    id 125
    label "stawanie_si&#281;"
  ]
  node [
    id 126
    label "przep&#322;ywanie"
  ]
  node [
    id 127
    label "przepuszczanie"
  ]
  node [
    id 128
    label "zaliczanie"
  ]
  node [
    id 129
    label "nas&#261;czanie"
  ]
  node [
    id 130
    label "zaczynanie"
  ]
  node [
    id 131
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 132
    label "impregnation"
  ]
  node [
    id 133
    label "uznanie"
  ]
  node [
    id 134
    label "passage"
  ]
  node [
    id 135
    label "trwanie"
  ]
  node [
    id 136
    label "przedostawanie_si&#281;"
  ]
  node [
    id 137
    label "wytyczenie"
  ]
  node [
    id 138
    label "zmierzanie"
  ]
  node [
    id 139
    label "popchni&#281;cie"
  ]
  node [
    id 140
    label "zaznawanie"
  ]
  node [
    id 141
    label "dzianie_si&#281;"
  ]
  node [
    id 142
    label "pass"
  ]
  node [
    id 143
    label "nale&#380;enie"
  ]
  node [
    id 144
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 145
    label "bycie"
  ]
  node [
    id 146
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 147
    label "potr&#261;canie"
  ]
  node [
    id 148
    label "przebywanie"
  ]
  node [
    id 149
    label "zast&#281;powanie"
  ]
  node [
    id 150
    label "passing"
  ]
  node [
    id 151
    label "mijanie"
  ]
  node [
    id 152
    label "przerabianie"
  ]
  node [
    id 153
    label "odmienianie"
  ]
  node [
    id 154
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 155
    label "mie&#263;_miejsce"
  ]
  node [
    id 156
    label "move"
  ]
  node [
    id 157
    label "zaczyna&#263;"
  ]
  node [
    id 158
    label "przebywa&#263;"
  ]
  node [
    id 159
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 160
    label "conflict"
  ]
  node [
    id 161
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 162
    label "mija&#263;"
  ]
  node [
    id 163
    label "proceed"
  ]
  node [
    id 164
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 165
    label "go"
  ]
  node [
    id 166
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 167
    label "saturate"
  ]
  node [
    id 168
    label "i&#347;&#263;"
  ]
  node [
    id 169
    label "doznawa&#263;"
  ]
  node [
    id 170
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 171
    label "przestawa&#263;"
  ]
  node [
    id 172
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 173
    label "zalicza&#263;"
  ]
  node [
    id 174
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 175
    label "zmienia&#263;"
  ]
  node [
    id 176
    label "podlega&#263;"
  ]
  node [
    id 177
    label "przerabia&#263;"
  ]
  node [
    id 178
    label "continue"
  ]
  node [
    id 179
    label "droga"
  ]
  node [
    id 180
    label "podbijarka_torowa"
  ]
  node [
    id 181
    label "kszta&#322;t"
  ]
  node [
    id 182
    label "rozstaw_tr&#243;jstopowy"
  ]
  node [
    id 183
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 184
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 185
    label "torowisko"
  ]
  node [
    id 186
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 187
    label "trasa"
  ]
  node [
    id 188
    label "przeorientowywanie"
  ]
  node [
    id 189
    label "kolej"
  ]
  node [
    id 190
    label "szyna"
  ]
  node [
    id 191
    label "miejsce"
  ]
  node [
    id 192
    label "przeorientowywa&#263;"
  ]
  node [
    id 193
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 194
    label "przeorientowanie"
  ]
  node [
    id 195
    label "przeorientowa&#263;"
  ]
  node [
    id 196
    label "rozstaw_przyl&#261;dkowy"
  ]
  node [
    id 197
    label "linia_kolejowa"
  ]
  node [
    id 198
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 199
    label "lane"
  ]
  node [
    id 200
    label "nawierzchnia_kolejowa"
  ]
  node [
    id 201
    label "podk&#322;ad"
  ]
  node [
    id 202
    label "bearing"
  ]
  node [
    id 203
    label "aktynowiec"
  ]
  node [
    id 204
    label "balastowanie"
  ]
  node [
    id 205
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 206
    label "rozstaw_bo&#347;niacki"
  ]
  node [
    id 207
    label "warunek_lokalowy"
  ]
  node [
    id 208
    label "plac"
  ]
  node [
    id 209
    label "location"
  ]
  node [
    id 210
    label "uwaga"
  ]
  node [
    id 211
    label "przestrze&#324;"
  ]
  node [
    id 212
    label "status"
  ]
  node [
    id 213
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 214
    label "chwila"
  ]
  node [
    id 215
    label "cia&#322;o"
  ]
  node [
    id 216
    label "cecha"
  ]
  node [
    id 217
    label "rz&#261;d"
  ]
  node [
    id 218
    label "Rzym_Zachodni"
  ]
  node [
    id 219
    label "whole"
  ]
  node [
    id 220
    label "ilo&#347;&#263;"
  ]
  node [
    id 221
    label "element"
  ]
  node [
    id 222
    label "Rzym_Wschodni"
  ]
  node [
    id 223
    label "formacja"
  ]
  node [
    id 224
    label "punkt_widzenia"
  ]
  node [
    id 225
    label "wygl&#261;d"
  ]
  node [
    id 226
    label "g&#322;owa"
  ]
  node [
    id 227
    label "spirala"
  ]
  node [
    id 228
    label "comeliness"
  ]
  node [
    id 229
    label "kielich"
  ]
  node [
    id 230
    label "face"
  ]
  node [
    id 231
    label "blaszka"
  ]
  node [
    id 232
    label "charakter"
  ]
  node [
    id 233
    label "p&#281;tla"
  ]
  node [
    id 234
    label "obiekt"
  ]
  node [
    id 235
    label "pasmo"
  ]
  node [
    id 236
    label "linearno&#347;&#263;"
  ]
  node [
    id 237
    label "gwiazda"
  ]
  node [
    id 238
    label "miniatura"
  ]
  node [
    id 239
    label "przebieg"
  ]
  node [
    id 240
    label "infrastruktura"
  ]
  node [
    id 241
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 242
    label "w&#281;ze&#322;"
  ]
  node [
    id 243
    label "marszrutyzacja"
  ]
  node [
    id 244
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 245
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 246
    label "podbieg"
  ]
  node [
    id 247
    label "ekskursja"
  ]
  node [
    id 248
    label "bezsilnikowy"
  ]
  node [
    id 249
    label "budowla"
  ]
  node [
    id 250
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 251
    label "turystyka"
  ]
  node [
    id 252
    label "nawierzchnia"
  ]
  node [
    id 253
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 254
    label "rajza"
  ]
  node [
    id 255
    label "korona_drogi"
  ]
  node [
    id 256
    label "wylot"
  ]
  node [
    id 257
    label "ekwipunek"
  ]
  node [
    id 258
    label "zbior&#243;wka"
  ]
  node [
    id 259
    label "wyb&#243;j"
  ]
  node [
    id 260
    label "drogowskaz"
  ]
  node [
    id 261
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 262
    label "pobocze"
  ]
  node [
    id 263
    label "journey"
  ]
  node [
    id 264
    label "ruch"
  ]
  node [
    id 265
    label "intencja"
  ]
  node [
    id 266
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 267
    label "leaning"
  ]
  node [
    id 268
    label "zbi&#243;r"
  ]
  node [
    id 269
    label "tryb"
  ]
  node [
    id 270
    label "nature"
  ]
  node [
    id 271
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 272
    label "metal"
  ]
  node [
    id 273
    label "actinoid"
  ]
  node [
    id 274
    label "kosmetyk"
  ]
  node [
    id 275
    label "szczep"
  ]
  node [
    id 276
    label "farba"
  ]
  node [
    id 277
    label "substrate"
  ]
  node [
    id 278
    label "layer"
  ]
  node [
    id 279
    label "melodia"
  ]
  node [
    id 280
    label "warstwa"
  ]
  node [
    id 281
    label "ro&#347;lina"
  ]
  node [
    id 282
    label "base"
  ]
  node [
    id 283
    label "partia"
  ]
  node [
    id 284
    label "puder"
  ]
  node [
    id 285
    label "splint"
  ]
  node [
    id 286
    label "prowadnica"
  ]
  node [
    id 287
    label "przyrz&#261;d"
  ]
  node [
    id 288
    label "topologia_magistrali"
  ]
  node [
    id 289
    label "sztaba"
  ]
  node [
    id 290
    label "track"
  ]
  node [
    id 291
    label "wagon"
  ]
  node [
    id 292
    label "lokomotywa"
  ]
  node [
    id 293
    label "trakcja"
  ]
  node [
    id 294
    label "run"
  ]
  node [
    id 295
    label "blokada"
  ]
  node [
    id 296
    label "kolejno&#347;&#263;"
  ]
  node [
    id 297
    label "pojazd_kolejowy"
  ]
  node [
    id 298
    label "linia"
  ]
  node [
    id 299
    label "tender"
  ]
  node [
    id 300
    label "proces"
  ]
  node [
    id 301
    label "cug"
  ]
  node [
    id 302
    label "pocz&#261;tek"
  ]
  node [
    id 303
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 304
    label "poci&#261;g"
  ]
  node [
    id 305
    label "cedu&#322;a"
  ]
  node [
    id 306
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 307
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 308
    label "nast&#281;pstwo"
  ]
  node [
    id 309
    label "wychylanie_si&#281;"
  ]
  node [
    id 310
    label "podsypywanie"
  ]
  node [
    id 311
    label "obci&#261;&#380;anie"
  ]
  node [
    id 312
    label "kierunek"
  ]
  node [
    id 313
    label "zmienienie"
  ]
  node [
    id 314
    label "zmieni&#263;"
  ]
  node [
    id 315
    label "zmienianie"
  ]
  node [
    id 316
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 317
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 318
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 319
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 320
    label "napada&#263;"
  ]
  node [
    id 321
    label "uprawia&#263;"
  ]
  node [
    id 322
    label "drive"
  ]
  node [
    id 323
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 324
    label "carry"
  ]
  node [
    id 325
    label "prowadzi&#263;"
  ]
  node [
    id 326
    label "umie&#263;"
  ]
  node [
    id 327
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 328
    label "ride"
  ]
  node [
    id 329
    label "przybywa&#263;"
  ]
  node [
    id 330
    label "traktowa&#263;"
  ]
  node [
    id 331
    label "jeer"
  ]
  node [
    id 332
    label "attack"
  ]
  node [
    id 333
    label "piratowa&#263;"
  ]
  node [
    id 334
    label "atakowa&#263;"
  ]
  node [
    id 335
    label "m&#243;wi&#263;"
  ]
  node [
    id 336
    label "krytykowa&#263;"
  ]
  node [
    id 337
    label "dopada&#263;"
  ]
  node [
    id 338
    label "&#380;y&#263;"
  ]
  node [
    id 339
    label "robi&#263;"
  ]
  node [
    id 340
    label "kierowa&#263;"
  ]
  node [
    id 341
    label "g&#243;rowa&#263;"
  ]
  node [
    id 342
    label "tworzy&#263;"
  ]
  node [
    id 343
    label "krzywa"
  ]
  node [
    id 344
    label "linia_melodyczna"
  ]
  node [
    id 345
    label "control"
  ]
  node [
    id 346
    label "string"
  ]
  node [
    id 347
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 348
    label "ukierunkowywa&#263;"
  ]
  node [
    id 349
    label "sterowa&#263;"
  ]
  node [
    id 350
    label "kre&#347;li&#263;"
  ]
  node [
    id 351
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 352
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 353
    label "message"
  ]
  node [
    id 354
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 355
    label "eksponowa&#263;"
  ]
  node [
    id 356
    label "navigate"
  ]
  node [
    id 357
    label "manipulate"
  ]
  node [
    id 358
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 359
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 360
    label "przesuwa&#263;"
  ]
  node [
    id 361
    label "partner"
  ]
  node [
    id 362
    label "prowadzenie"
  ]
  node [
    id 363
    label "powodowa&#263;"
  ]
  node [
    id 364
    label "dociera&#263;"
  ]
  node [
    id 365
    label "get"
  ]
  node [
    id 366
    label "zyskiwa&#263;"
  ]
  node [
    id 367
    label "treat"
  ]
  node [
    id 368
    label "zaspokaja&#263;"
  ]
  node [
    id 369
    label "suffice"
  ]
  node [
    id 370
    label "zaspakaja&#263;"
  ]
  node [
    id 371
    label "uprawia&#263;_seks"
  ]
  node [
    id 372
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 373
    label "serve"
  ]
  node [
    id 374
    label "wiedzie&#263;"
  ]
  node [
    id 375
    label "can"
  ]
  node [
    id 376
    label "m&#243;c"
  ]
  node [
    id 377
    label "work"
  ]
  node [
    id 378
    label "hodowa&#263;"
  ]
  node [
    id 379
    label "plantator"
  ]
  node [
    id 380
    label "talerz_perkusyjny"
  ]
  node [
    id 381
    label "cover"
  ]
  node [
    id 382
    label "prezenter"
  ]
  node [
    id 383
    label "typ"
  ]
  node [
    id 384
    label "mildew"
  ]
  node [
    id 385
    label "zi&#243;&#322;ko"
  ]
  node [
    id 386
    label "motif"
  ]
  node [
    id 387
    label "pozowanie"
  ]
  node [
    id 388
    label "ideal"
  ]
  node [
    id 389
    label "wz&#243;r"
  ]
  node [
    id 390
    label "matryca"
  ]
  node [
    id 391
    label "adaptation"
  ]
  node [
    id 392
    label "pozowa&#263;"
  ]
  node [
    id 393
    label "imitacja"
  ]
  node [
    id 394
    label "orygina&#322;"
  ]
  node [
    id 395
    label "facet"
  ]
  node [
    id 396
    label "gablotka"
  ]
  node [
    id 397
    label "pokaz"
  ]
  node [
    id 398
    label "szkatu&#322;ka"
  ]
  node [
    id 399
    label "pude&#322;ko"
  ]
  node [
    id 400
    label "bran&#380;owiec"
  ]
  node [
    id 401
    label "prowadz&#261;cy"
  ]
  node [
    id 402
    label "ludzko&#347;&#263;"
  ]
  node [
    id 403
    label "asymilowanie"
  ]
  node [
    id 404
    label "wapniak"
  ]
  node [
    id 405
    label "asymilowa&#263;"
  ]
  node [
    id 406
    label "os&#322;abia&#263;"
  ]
  node [
    id 407
    label "posta&#263;"
  ]
  node [
    id 408
    label "hominid"
  ]
  node [
    id 409
    label "podw&#322;adny"
  ]
  node [
    id 410
    label "os&#322;abianie"
  ]
  node [
    id 411
    label "figura"
  ]
  node [
    id 412
    label "portrecista"
  ]
  node [
    id 413
    label "dwun&#243;g"
  ]
  node [
    id 414
    label "profanum"
  ]
  node [
    id 415
    label "mikrokosmos"
  ]
  node [
    id 416
    label "nasada"
  ]
  node [
    id 417
    label "duch"
  ]
  node [
    id 418
    label "antropochoria"
  ]
  node [
    id 419
    label "osoba"
  ]
  node [
    id 420
    label "senior"
  ]
  node [
    id 421
    label "oddzia&#322;ywanie"
  ]
  node [
    id 422
    label "Adam"
  ]
  node [
    id 423
    label "homo_sapiens"
  ]
  node [
    id 424
    label "polifag"
  ]
  node [
    id 425
    label "kopia"
  ]
  node [
    id 426
    label "utw&#243;r"
  ]
  node [
    id 427
    label "obraz"
  ]
  node [
    id 428
    label "ilustracja"
  ]
  node [
    id 429
    label "miniature"
  ]
  node [
    id 430
    label "zapis"
  ]
  node [
    id 431
    label "figure"
  ]
  node [
    id 432
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 433
    label "rule"
  ]
  node [
    id 434
    label "dekal"
  ]
  node [
    id 435
    label "projekt"
  ]
  node [
    id 436
    label "technika"
  ]
  node [
    id 437
    label "praktyka"
  ]
  node [
    id 438
    label "na&#347;ladownictwo"
  ]
  node [
    id 439
    label "bratek"
  ]
  node [
    id 440
    label "kod_genetyczny"
  ]
  node [
    id 441
    label "t&#322;ocznik"
  ]
  node [
    id 442
    label "aparat_cyfrowy"
  ]
  node [
    id 443
    label "detector"
  ]
  node [
    id 444
    label "forma"
  ]
  node [
    id 445
    label "jednostka_systematyczna"
  ]
  node [
    id 446
    label "kr&#243;lestwo"
  ]
  node [
    id 447
    label "autorament"
  ]
  node [
    id 448
    label "variety"
  ]
  node [
    id 449
    label "antycypacja"
  ]
  node [
    id 450
    label "przypuszczenie"
  ]
  node [
    id 451
    label "cynk"
  ]
  node [
    id 452
    label "obstawia&#263;"
  ]
  node [
    id 453
    label "gromada"
  ]
  node [
    id 454
    label "sztuka"
  ]
  node [
    id 455
    label "design"
  ]
  node [
    id 456
    label "sfotografowanie_si&#281;"
  ]
  node [
    id 457
    label "na&#347;ladowanie"
  ]
  node [
    id 458
    label "robienie"
  ]
  node [
    id 459
    label "fotografowanie_si&#281;"
  ]
  node [
    id 460
    label "pretense"
  ]
  node [
    id 461
    label "sit"
  ]
  node [
    id 462
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 463
    label "dally"
  ]
  node [
    id 464
    label "mechanika"
  ]
  node [
    id 465
    label "utrzymywanie"
  ]
  node [
    id 466
    label "poruszenie"
  ]
  node [
    id 467
    label "movement"
  ]
  node [
    id 468
    label "myk"
  ]
  node [
    id 469
    label "utrzyma&#263;"
  ]
  node [
    id 470
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 471
    label "zjawisko"
  ]
  node [
    id 472
    label "utrzymanie"
  ]
  node [
    id 473
    label "travel"
  ]
  node [
    id 474
    label "kanciasty"
  ]
  node [
    id 475
    label "commercial_enterprise"
  ]
  node [
    id 476
    label "strumie&#324;"
  ]
  node [
    id 477
    label "aktywno&#347;&#263;"
  ]
  node [
    id 478
    label "kr&#243;tki"
  ]
  node [
    id 479
    label "taktyka"
  ]
  node [
    id 480
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 481
    label "apraksja"
  ]
  node [
    id 482
    label "natural_process"
  ]
  node [
    id 483
    label "utrzymywa&#263;"
  ]
  node [
    id 484
    label "d&#322;ugi"
  ]
  node [
    id 485
    label "dyssypacja_energii"
  ]
  node [
    id 486
    label "tumult"
  ]
  node [
    id 487
    label "stopek"
  ]
  node [
    id 488
    label "zmiana"
  ]
  node [
    id 489
    label "manewr"
  ]
  node [
    id 490
    label "lokomocja"
  ]
  node [
    id 491
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 492
    label "komunikacja"
  ]
  node [
    id 493
    label "drift"
  ]
  node [
    id 494
    label "nicpo&#324;"
  ]
  node [
    id 495
    label "agent"
  ]
  node [
    id 496
    label "uzasadni&#263;"
  ]
  node [
    id 497
    label "testify"
  ]
  node [
    id 498
    label "explain"
  ]
  node [
    id 499
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 500
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 501
    label "odpowiednio"
  ]
  node [
    id 502
    label "dobroczynnie"
  ]
  node [
    id 503
    label "moralnie"
  ]
  node [
    id 504
    label "korzystnie"
  ]
  node [
    id 505
    label "pozytywnie"
  ]
  node [
    id 506
    label "lepiej"
  ]
  node [
    id 507
    label "wiele"
  ]
  node [
    id 508
    label "skutecznie"
  ]
  node [
    id 509
    label "pomy&#347;lnie"
  ]
  node [
    id 510
    label "dobry"
  ]
  node [
    id 511
    label "charakterystycznie"
  ]
  node [
    id 512
    label "nale&#380;nie"
  ]
  node [
    id 513
    label "stosowny"
  ]
  node [
    id 514
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 515
    label "nale&#380;ycie"
  ]
  node [
    id 516
    label "prawdziwie"
  ]
  node [
    id 517
    label "auspiciously"
  ]
  node [
    id 518
    label "pomy&#347;lny"
  ]
  node [
    id 519
    label "moralny"
  ]
  node [
    id 520
    label "etyczny"
  ]
  node [
    id 521
    label "skuteczny"
  ]
  node [
    id 522
    label "wiela"
  ]
  node [
    id 523
    label "du&#380;y"
  ]
  node [
    id 524
    label "utylitarnie"
  ]
  node [
    id 525
    label "korzystny"
  ]
  node [
    id 526
    label "beneficially"
  ]
  node [
    id 527
    label "przyjemnie"
  ]
  node [
    id 528
    label "pozytywny"
  ]
  node [
    id 529
    label "ontologicznie"
  ]
  node [
    id 530
    label "dodatni"
  ]
  node [
    id 531
    label "odpowiedni"
  ]
  node [
    id 532
    label "wiersz"
  ]
  node [
    id 533
    label "dobroczynny"
  ]
  node [
    id 534
    label "czw&#243;rka"
  ]
  node [
    id 535
    label "spokojny"
  ]
  node [
    id 536
    label "&#347;mieszny"
  ]
  node [
    id 537
    label "mi&#322;y"
  ]
  node [
    id 538
    label "grzeczny"
  ]
  node [
    id 539
    label "powitanie"
  ]
  node [
    id 540
    label "ca&#322;y"
  ]
  node [
    id 541
    label "zwrot"
  ]
  node [
    id 542
    label "drogi"
  ]
  node [
    id 543
    label "pos&#322;uszny"
  ]
  node [
    id 544
    label "philanthropically"
  ]
  node [
    id 545
    label "spo&#322;ecznie"
  ]
  node [
    id 546
    label "poprawi&#263;"
  ]
  node [
    id 547
    label "nada&#263;"
  ]
  node [
    id 548
    label "peddle"
  ]
  node [
    id 549
    label "marshal"
  ]
  node [
    id 550
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 551
    label "wyznaczy&#263;"
  ]
  node [
    id 552
    label "stanowisko"
  ]
  node [
    id 553
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 554
    label "spowodowa&#263;"
  ]
  node [
    id 555
    label "zabezpieczy&#263;"
  ]
  node [
    id 556
    label "umie&#347;ci&#263;"
  ]
  node [
    id 557
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 558
    label "zinterpretowa&#263;"
  ]
  node [
    id 559
    label "wskaza&#263;"
  ]
  node [
    id 560
    label "set"
  ]
  node [
    id 561
    label "przyzna&#263;"
  ]
  node [
    id 562
    label "sk&#322;oni&#263;"
  ]
  node [
    id 563
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 564
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 565
    label "zdecydowa&#263;"
  ]
  node [
    id 566
    label "accommodate"
  ]
  node [
    id 567
    label "ustali&#263;"
  ]
  node [
    id 568
    label "situate"
  ]
  node [
    id 569
    label "rola"
  ]
  node [
    id 570
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 571
    label "nak&#322;oni&#263;"
  ]
  node [
    id 572
    label "prompt"
  ]
  node [
    id 573
    label "obni&#380;y&#263;"
  ]
  node [
    id 574
    label "dostosowa&#263;"
  ]
  node [
    id 575
    label "hyponym"
  ]
  node [
    id 576
    label "uzale&#380;ni&#263;"
  ]
  node [
    id 577
    label "da&#263;"
  ]
  node [
    id 578
    label "za&#322;atwi&#263;"
  ]
  node [
    id 579
    label "give"
  ]
  node [
    id 580
    label "zarekomendowa&#263;"
  ]
  node [
    id 581
    label "przes&#322;a&#263;"
  ]
  node [
    id 582
    label "donie&#347;&#263;"
  ]
  node [
    id 583
    label "zadba&#263;"
  ]
  node [
    id 584
    label "zorganizowa&#263;"
  ]
  node [
    id 585
    label "zebra&#263;"
  ]
  node [
    id 586
    label "posprz&#261;ta&#263;"
  ]
  node [
    id 587
    label "order"
  ]
  node [
    id 588
    label "point"
  ]
  node [
    id 589
    label "pokaza&#263;"
  ]
  node [
    id 590
    label "poda&#263;"
  ]
  node [
    id 591
    label "picture"
  ]
  node [
    id 592
    label "aim"
  ]
  node [
    id 593
    label "wybra&#263;"
  ]
  node [
    id 594
    label "podkre&#347;li&#263;"
  ]
  node [
    id 595
    label "indicate"
  ]
  node [
    id 596
    label "sta&#263;_si&#281;"
  ]
  node [
    id 597
    label "podj&#261;&#263;"
  ]
  node [
    id 598
    label "decide"
  ]
  node [
    id 599
    label "determine"
  ]
  node [
    id 600
    label "zrobi&#263;"
  ]
  node [
    id 601
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 602
    label "bro&#324;_palna"
  ]
  node [
    id 603
    label "report"
  ]
  node [
    id 604
    label "zainstalowa&#263;"
  ]
  node [
    id 605
    label "pistolet"
  ]
  node [
    id 606
    label "zapewni&#263;"
  ]
  node [
    id 607
    label "put"
  ]
  node [
    id 608
    label "bind"
  ]
  node [
    id 609
    label "umocni&#263;"
  ]
  node [
    id 610
    label "unwrap"
  ]
  node [
    id 611
    label "pozwoli&#263;"
  ]
  node [
    id 612
    label "stwierdzi&#263;"
  ]
  node [
    id 613
    label "uplasowa&#263;"
  ]
  node [
    id 614
    label "wpierniczy&#263;"
  ]
  node [
    id 615
    label "okre&#347;li&#263;"
  ]
  node [
    id 616
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 617
    label "umieszcza&#263;"
  ]
  node [
    id 618
    label "position"
  ]
  node [
    id 619
    label "zaznaczy&#263;"
  ]
  node [
    id 620
    label "sign"
  ]
  node [
    id 621
    label "wear"
  ]
  node [
    id 622
    label "rozmie&#347;ci&#263;"
  ]
  node [
    id 623
    label "os&#322;abi&#263;"
  ]
  node [
    id 624
    label "zepsu&#263;"
  ]
  node [
    id 625
    label "podzieli&#263;"
  ]
  node [
    id 626
    label "range"
  ]
  node [
    id 627
    label "oddali&#263;"
  ]
  node [
    id 628
    label "stagger"
  ]
  node [
    id 629
    label "note"
  ]
  node [
    id 630
    label "raise"
  ]
  node [
    id 631
    label "wygra&#263;"
  ]
  node [
    id 632
    label "upomnie&#263;"
  ]
  node [
    id 633
    label "correct"
  ]
  node [
    id 634
    label "sprawdzi&#263;"
  ]
  node [
    id 635
    label "amend"
  ]
  node [
    id 636
    label "ulepszy&#263;"
  ]
  node [
    id 637
    label "rectify"
  ]
  node [
    id 638
    label "level"
  ]
  node [
    id 639
    label "oceni&#263;"
  ]
  node [
    id 640
    label "zagra&#263;"
  ]
  node [
    id 641
    label "illustrate"
  ]
  node [
    id 642
    label "zanalizowa&#263;"
  ]
  node [
    id 643
    label "read"
  ]
  node [
    id 644
    label "think"
  ]
  node [
    id 645
    label "act"
  ]
  node [
    id 646
    label "przekaza&#263;"
  ]
  node [
    id 647
    label "return"
  ]
  node [
    id 648
    label "shaftowa&#263;_si&#281;"
  ]
  node [
    id 649
    label "przeznaczy&#263;"
  ]
  node [
    id 650
    label "regenerate"
  ]
  node [
    id 651
    label "direct"
  ]
  node [
    id 652
    label "zespawa&#263;_si&#281;"
  ]
  node [
    id 653
    label "rzygn&#261;&#263;"
  ]
  node [
    id 654
    label "z_powrotem"
  ]
  node [
    id 655
    label "wydali&#263;"
  ]
  node [
    id 656
    label "po&#322;o&#380;enie"
  ]
  node [
    id 657
    label "punkt"
  ]
  node [
    id 658
    label "pogl&#261;d"
  ]
  node [
    id 659
    label "wojsko"
  ]
  node [
    id 660
    label "awansowa&#263;"
  ]
  node [
    id 661
    label "stawia&#263;"
  ]
  node [
    id 662
    label "uprawianie"
  ]
  node [
    id 663
    label "wakowa&#263;"
  ]
  node [
    id 664
    label "powierzanie"
  ]
  node [
    id 665
    label "postawi&#263;"
  ]
  node [
    id 666
    label "awansowanie"
  ]
  node [
    id 667
    label "uprawienie"
  ]
  node [
    id 668
    label "dialog"
  ]
  node [
    id 669
    label "p&#322;osa"
  ]
  node [
    id 670
    label "wykonywanie"
  ]
  node [
    id 671
    label "plik"
  ]
  node [
    id 672
    label "ziemia"
  ]
  node [
    id 673
    label "wykonywa&#263;"
  ]
  node [
    id 674
    label "czyn"
  ]
  node [
    id 675
    label "scenariusz"
  ]
  node [
    id 676
    label "pole"
  ]
  node [
    id 677
    label "gospodarstwo"
  ]
  node [
    id 678
    label "uprawi&#263;"
  ]
  node [
    id 679
    label "function"
  ]
  node [
    id 680
    label "zreinterpretowa&#263;"
  ]
  node [
    id 681
    label "zastosowanie"
  ]
  node [
    id 682
    label "reinterpretowa&#263;"
  ]
  node [
    id 683
    label "wrench"
  ]
  node [
    id 684
    label "irygowanie"
  ]
  node [
    id 685
    label "irygowa&#263;"
  ]
  node [
    id 686
    label "zreinterpretowanie"
  ]
  node [
    id 687
    label "cel"
  ]
  node [
    id 688
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 689
    label "gra&#263;"
  ]
  node [
    id 690
    label "aktorstwo"
  ]
  node [
    id 691
    label "kostium"
  ]
  node [
    id 692
    label "zagon"
  ]
  node [
    id 693
    label "znaczenie"
  ]
  node [
    id 694
    label "reinterpretowanie"
  ]
  node [
    id 695
    label "sk&#322;ad"
  ]
  node [
    id 696
    label "tekst"
  ]
  node [
    id 697
    label "zagranie"
  ]
  node [
    id 698
    label "radlina"
  ]
  node [
    id 699
    label "granie"
  ]
  node [
    id 700
    label "gem"
  ]
  node [
    id 701
    label "kompozycja"
  ]
  node [
    id 702
    label "runda"
  ]
  node [
    id 703
    label "muzyka"
  ]
  node [
    id 704
    label "zestaw"
  ]
  node [
    id 705
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 706
    label "krzew"
  ]
  node [
    id 707
    label "delfinidyna"
  ]
  node [
    id 708
    label "pi&#380;maczkowate"
  ]
  node [
    id 709
    label "ki&#347;&#263;"
  ]
  node [
    id 710
    label "hy&#263;ka"
  ]
  node [
    id 711
    label "pestkowiec"
  ]
  node [
    id 712
    label "kwiat"
  ]
  node [
    id 713
    label "owoc"
  ]
  node [
    id 714
    label "oliwkowate"
  ]
  node [
    id 715
    label "lilac"
  ]
  node [
    id 716
    label "kostka"
  ]
  node [
    id 717
    label "kita"
  ]
  node [
    id 718
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 719
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 720
    label "d&#322;o&#324;"
  ]
  node [
    id 721
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 722
    label "powerball"
  ]
  node [
    id 723
    label "&#380;ubr"
  ]
  node [
    id 724
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 725
    label "p&#281;k"
  ]
  node [
    id 726
    label "r&#281;ka"
  ]
  node [
    id 727
    label "ogon"
  ]
  node [
    id 728
    label "zako&#324;czenie"
  ]
  node [
    id 729
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 730
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 731
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 732
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 733
    label "flakon"
  ]
  node [
    id 734
    label "przykoronek"
  ]
  node [
    id 735
    label "dno_kwiatowe"
  ]
  node [
    id 736
    label "organ_ro&#347;linny"
  ]
  node [
    id 737
    label "warga"
  ]
  node [
    id 738
    label "korona"
  ]
  node [
    id 739
    label "rurka"
  ]
  node [
    id 740
    label "ozdoba"
  ]
  node [
    id 741
    label "&#322;yko"
  ]
  node [
    id 742
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 743
    label "karczowa&#263;"
  ]
  node [
    id 744
    label "wykarczowanie"
  ]
  node [
    id 745
    label "skupina"
  ]
  node [
    id 746
    label "wykarczowa&#263;"
  ]
  node [
    id 747
    label "karczowanie"
  ]
  node [
    id 748
    label "fanerofit"
  ]
  node [
    id 749
    label "zbiorowisko"
  ]
  node [
    id 750
    label "ro&#347;liny"
  ]
  node [
    id 751
    label "p&#281;d"
  ]
  node [
    id 752
    label "wegetowanie"
  ]
  node [
    id 753
    label "zadziorek"
  ]
  node [
    id 754
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 755
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 756
    label "do&#322;owa&#263;"
  ]
  node [
    id 757
    label "wegetacja"
  ]
  node [
    id 758
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 759
    label "strzyc"
  ]
  node [
    id 760
    label "w&#322;&#243;kno"
  ]
  node [
    id 761
    label "g&#322;uszenie"
  ]
  node [
    id 762
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 763
    label "fitotron"
  ]
  node [
    id 764
    label "bulwka"
  ]
  node [
    id 765
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 766
    label "odn&#243;&#380;ka"
  ]
  node [
    id 767
    label "epiderma"
  ]
  node [
    id 768
    label "gumoza"
  ]
  node [
    id 769
    label "strzy&#380;enie"
  ]
  node [
    id 770
    label "wypotnik"
  ]
  node [
    id 771
    label "flawonoid"
  ]
  node [
    id 772
    label "wyro&#347;le"
  ]
  node [
    id 773
    label "do&#322;owanie"
  ]
  node [
    id 774
    label "g&#322;uszy&#263;"
  ]
  node [
    id 775
    label "pora&#380;a&#263;"
  ]
  node [
    id 776
    label "fitocenoza"
  ]
  node [
    id 777
    label "hodowla"
  ]
  node [
    id 778
    label "fotoautotrof"
  ]
  node [
    id 779
    label "nieuleczalnie_chory"
  ]
  node [
    id 780
    label "wegetowa&#263;"
  ]
  node [
    id 781
    label "pochewka"
  ]
  node [
    id 782
    label "sok"
  ]
  node [
    id 783
    label "system_korzeniowy"
  ]
  node [
    id 784
    label "zawi&#261;zek"
  ]
  node [
    id 785
    label "pestka"
  ]
  node [
    id 786
    label "mi&#261;&#380;sz"
  ]
  node [
    id 787
    label "frukt"
  ]
  node [
    id 788
    label "drylowanie"
  ]
  node [
    id 789
    label "produkt"
  ]
  node [
    id 790
    label "owocnia"
  ]
  node [
    id 791
    label "fruktoza"
  ]
  node [
    id 792
    label "gniazdo_nasienne"
  ]
  node [
    id 793
    label "glukoza"
  ]
  node [
    id 794
    label "antocyjanidyn"
  ]
  node [
    id 795
    label "szczeciowce"
  ]
  node [
    id 796
    label "jasnotowce"
  ]
  node [
    id 797
    label "Oleaceae"
  ]
  node [
    id 798
    label "wielkopolski"
  ]
  node [
    id 799
    label "bez_czarny"
  ]
  node [
    id 800
    label "sprawa"
  ]
  node [
    id 801
    label "subiekcja"
  ]
  node [
    id 802
    label "problemat"
  ]
  node [
    id 803
    label "jajko_Kolumba"
  ]
  node [
    id 804
    label "obstruction"
  ]
  node [
    id 805
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 806
    label "problematyka"
  ]
  node [
    id 807
    label "trudno&#347;&#263;"
  ]
  node [
    id 808
    label "pierepa&#322;ka"
  ]
  node [
    id 809
    label "ambaras"
  ]
  node [
    id 810
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 811
    label "napotka&#263;"
  ]
  node [
    id 812
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 813
    label "k&#322;opotliwy"
  ]
  node [
    id 814
    label "napotkanie"
  ]
  node [
    id 815
    label "poziom"
  ]
  node [
    id 816
    label "difficulty"
  ]
  node [
    id 817
    label "obstacle"
  ]
  node [
    id 818
    label "kognicja"
  ]
  node [
    id 819
    label "object"
  ]
  node [
    id 820
    label "rozprawa"
  ]
  node [
    id 821
    label "temat"
  ]
  node [
    id 822
    label "proposition"
  ]
  node [
    id 823
    label "przes&#322;anka"
  ]
  node [
    id 824
    label "rzecz"
  ]
  node [
    id 825
    label "idea"
  ]
  node [
    id 826
    label "berylowiec"
  ]
  node [
    id 827
    label "jednostka"
  ]
  node [
    id 828
    label "content"
  ]
  node [
    id 829
    label "jednostka_promieniowania"
  ]
  node [
    id 830
    label "zadowolenie_si&#281;"
  ]
  node [
    id 831
    label "miliradian"
  ]
  node [
    id 832
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 833
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 834
    label "mikroradian"
  ]
  node [
    id 835
    label "przyswoi&#263;"
  ]
  node [
    id 836
    label "one"
  ]
  node [
    id 837
    label "poj&#281;cie"
  ]
  node [
    id 838
    label "ewoluowanie"
  ]
  node [
    id 839
    label "supremum"
  ]
  node [
    id 840
    label "skala"
  ]
  node [
    id 841
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 842
    label "przyswajanie"
  ]
  node [
    id 843
    label "wyewoluowanie"
  ]
  node [
    id 844
    label "reakcja"
  ]
  node [
    id 845
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 846
    label "przeliczy&#263;"
  ]
  node [
    id 847
    label "wyewoluowa&#263;"
  ]
  node [
    id 848
    label "ewoluowa&#263;"
  ]
  node [
    id 849
    label "matematyka"
  ]
  node [
    id 850
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 851
    label "rzut"
  ]
  node [
    id 852
    label "liczba_naturalna"
  ]
  node [
    id 853
    label "czynnik_biotyczny"
  ]
  node [
    id 854
    label "individual"
  ]
  node [
    id 855
    label "przyswaja&#263;"
  ]
  node [
    id 856
    label "przyswojenie"
  ]
  node [
    id 857
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 858
    label "starzenie_si&#281;"
  ]
  node [
    id 859
    label "przeliczanie"
  ]
  node [
    id 860
    label "funkcja"
  ]
  node [
    id 861
    label "przelicza&#263;"
  ]
  node [
    id 862
    label "infimum"
  ]
  node [
    id 863
    label "przeliczenie"
  ]
  node [
    id 864
    label "nanoradian"
  ]
  node [
    id 865
    label "radian"
  ]
  node [
    id 866
    label "zadowolony"
  ]
  node [
    id 867
    label "weso&#322;y"
  ]
  node [
    id 868
    label "kr&#281;powanie"
  ]
  node [
    id 869
    label "jajognioty"
  ]
  node [
    id 870
    label "ciasno"
  ]
  node [
    id 871
    label "duszny"
  ]
  node [
    id 872
    label "zw&#281;&#380;enie"
  ]
  node [
    id 873
    label "niewygodny"
  ]
  node [
    id 874
    label "niedostateczny"
  ]
  node [
    id 875
    label "w&#261;ski"
  ]
  node [
    id 876
    label "niewystarczaj&#261;cy"
  ]
  node [
    id 877
    label "nieelastyczny"
  ]
  node [
    id 878
    label "obcis&#322;y"
  ]
  node [
    id 879
    label "zwarty"
  ]
  node [
    id 880
    label "ma&#322;y"
  ]
  node [
    id 881
    label "szczup&#322;y"
  ]
  node [
    id 882
    label "ograniczony"
  ]
  node [
    id 883
    label "w&#261;sko"
  ]
  node [
    id 884
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 885
    label "szybki"
  ]
  node [
    id 886
    label "solidarny"
  ]
  node [
    id 887
    label "zwarcie"
  ]
  node [
    id 888
    label "sprawny"
  ]
  node [
    id 889
    label "sp&#243;jny"
  ]
  node [
    id 890
    label "g&#281;sty"
  ]
  node [
    id 891
    label "niewygodnie"
  ]
  node [
    id 892
    label "nieznaczny"
  ]
  node [
    id 893
    label "przeci&#281;tny"
  ]
  node [
    id 894
    label "wstydliwy"
  ]
  node [
    id 895
    label "s&#322;aby"
  ]
  node [
    id 896
    label "niewa&#380;ny"
  ]
  node [
    id 897
    label "ch&#322;opiec"
  ]
  node [
    id 898
    label "m&#322;ody"
  ]
  node [
    id 899
    label "ma&#322;o"
  ]
  node [
    id 900
    label "marny"
  ]
  node [
    id 901
    label "nieliczny"
  ]
  node [
    id 902
    label "n&#281;dznie"
  ]
  node [
    id 903
    label "usztywnianie"
  ]
  node [
    id 904
    label "sztywnienie"
  ]
  node [
    id 905
    label "sta&#322;y"
  ]
  node [
    id 906
    label "usztywnienie"
  ]
  node [
    id 907
    label "zesztywnienie"
  ]
  node [
    id 908
    label "twardo"
  ]
  node [
    id 909
    label "trwa&#322;y"
  ]
  node [
    id 910
    label "obci&#347;le"
  ]
  node [
    id 911
    label "pa&#322;a"
  ]
  node [
    id 912
    label "jedynka"
  ]
  node [
    id 913
    label "niezadowalaj&#261;cy"
  ]
  node [
    id 914
    label "dw&#243;jka"
  ]
  node [
    id 915
    label "niewystarczaj&#261;co"
  ]
  node [
    id 916
    label "niezadowalaj&#261;co"
  ]
  node [
    id 917
    label "tightly"
  ]
  node [
    id 918
    label "stale"
  ]
  node [
    id 919
    label "spodnie"
  ]
  node [
    id 920
    label "constriction"
  ]
  node [
    id 921
    label "condensation"
  ]
  node [
    id 922
    label "spowodowanie"
  ]
  node [
    id 923
    label "stenoza"
  ]
  node [
    id 924
    label "samog&#322;oska_&#347;cie&#347;niona"
  ]
  node [
    id 925
    label "ograniczanie"
  ]
  node [
    id 926
    label "p&#281;tanie"
  ]
  node [
    id 927
    label "zawstydzanie"
  ]
  node [
    id 928
    label "uwieranie"
  ]
  node [
    id 929
    label "duszno"
  ]
  node [
    id 930
    label "wielki"
  ]
  node [
    id 931
    label "ci&#281;&#380;ki"
  ]
  node [
    id 932
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 933
    label "ekscentryk"
  ]
  node [
    id 934
    label "zajob"
  ]
  node [
    id 935
    label "nadsterowno&#347;&#263;"
  ]
  node [
    id 936
    label "odcinek"
  ]
  node [
    id 937
    label "serpentyna"
  ]
  node [
    id 938
    label "zapaleniec"
  ]
  node [
    id 939
    label "szalona_g&#322;owa"
  ]
  node [
    id 940
    label "podsterowno&#347;&#263;"
  ]
  node [
    id 941
    label "poprzedzanie"
  ]
  node [
    id 942
    label "czasoprzestrze&#324;"
  ]
  node [
    id 943
    label "laba"
  ]
  node [
    id 944
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 945
    label "chronometria"
  ]
  node [
    id 946
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 947
    label "rachuba_czasu"
  ]
  node [
    id 948
    label "czasokres"
  ]
  node [
    id 949
    label "odczyt"
  ]
  node [
    id 950
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 951
    label "dzieje"
  ]
  node [
    id 952
    label "kategoria_gramatyczna"
  ]
  node [
    id 953
    label "poprzedzenie"
  ]
  node [
    id 954
    label "trawienie"
  ]
  node [
    id 955
    label "pochodzi&#263;"
  ]
  node [
    id 956
    label "period"
  ]
  node [
    id 957
    label "okres_czasu"
  ]
  node [
    id 958
    label "poprzedza&#263;"
  ]
  node [
    id 959
    label "schy&#322;ek"
  ]
  node [
    id 960
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 961
    label "odwlekanie_si&#281;"
  ]
  node [
    id 962
    label "zegar"
  ]
  node [
    id 963
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 964
    label "czwarty_wymiar"
  ]
  node [
    id 965
    label "pochodzenie"
  ]
  node [
    id 966
    label "koniugacja"
  ]
  node [
    id 967
    label "Zeitgeist"
  ]
  node [
    id 968
    label "trawi&#263;"
  ]
  node [
    id 969
    label "pogoda"
  ]
  node [
    id 970
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 971
    label "poprzedzi&#263;"
  ]
  node [
    id 972
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 973
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 974
    label "time_period"
  ]
  node [
    id 975
    label "maszyna"
  ]
  node [
    id 976
    label "entuzjasta"
  ]
  node [
    id 977
    label "teren"
  ]
  node [
    id 978
    label "kawa&#322;ek"
  ]
  node [
    id 979
    label "part"
  ]
  node [
    id 980
    label "line"
  ]
  node [
    id 981
    label "coupon"
  ]
  node [
    id 982
    label "fragment"
  ]
  node [
    id 983
    label "pokwitowanie"
  ]
  node [
    id 984
    label "moneta"
  ]
  node [
    id 985
    label "epizod"
  ]
  node [
    id 986
    label "turn"
  ]
  node [
    id 987
    label "turning"
  ]
  node [
    id 988
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 989
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 990
    label "skr&#281;t"
  ]
  node [
    id 991
    label "obr&#243;t"
  ]
  node [
    id 992
    label "fraza_czasownikowa"
  ]
  node [
    id 993
    label "jednostka_leksykalna"
  ]
  node [
    id 994
    label "wyra&#380;enie"
  ]
  node [
    id 995
    label "nienormalny"
  ]
  node [
    id 996
    label "obsesjonista"
  ]
  node [
    id 997
    label "obsesja"
  ]
  node [
    id 998
    label "op&#243;&#378;nienie"
  ]
  node [
    id 999
    label "ta&#347;ma"
  ]
  node [
    id 1000
    label "mocno"
  ]
  node [
    id 1001
    label "bardzo"
  ]
  node [
    id 1002
    label "cz&#281;sto"
  ]
  node [
    id 1003
    label "doros&#322;y"
  ]
  node [
    id 1004
    label "znaczny"
  ]
  node [
    id 1005
    label "niema&#322;o"
  ]
  node [
    id 1006
    label "rozwini&#281;ty"
  ]
  node [
    id 1007
    label "dorodny"
  ]
  node [
    id 1008
    label "wa&#380;ny"
  ]
  node [
    id 1009
    label "prawdziwy"
  ]
  node [
    id 1010
    label "intensywny"
  ]
  node [
    id 1011
    label "mocny"
  ]
  node [
    id 1012
    label "silny"
  ]
  node [
    id 1013
    label "przekonuj&#261;co"
  ]
  node [
    id 1014
    label "powerfully"
  ]
  node [
    id 1015
    label "widocznie"
  ]
  node [
    id 1016
    label "szczerze"
  ]
  node [
    id 1017
    label "konkretnie"
  ]
  node [
    id 1018
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1019
    label "stabilnie"
  ]
  node [
    id 1020
    label "silnie"
  ]
  node [
    id 1021
    label "zdecydowanie"
  ]
  node [
    id 1022
    label "strongly"
  ]
  node [
    id 1023
    label "w_chuj"
  ]
  node [
    id 1024
    label "cz&#281;sty"
  ]
  node [
    id 1025
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 1026
    label "hide"
  ]
  node [
    id 1027
    label "czu&#263;"
  ]
  node [
    id 1028
    label "support"
  ]
  node [
    id 1029
    label "need"
  ]
  node [
    id 1030
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1031
    label "wykonawca"
  ]
  node [
    id 1032
    label "interpretator"
  ]
  node [
    id 1033
    label "postrzega&#263;"
  ]
  node [
    id 1034
    label "przewidywa&#263;"
  ]
  node [
    id 1035
    label "by&#263;"
  ]
  node [
    id 1036
    label "smell"
  ]
  node [
    id 1037
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 1038
    label "uczuwa&#263;"
  ]
  node [
    id 1039
    label "spirit"
  ]
  node [
    id 1040
    label "anticipate"
  ]
  node [
    id 1041
    label "piwo"
  ]
  node [
    id 1042
    label "uwarzenie"
  ]
  node [
    id 1043
    label "warzenie"
  ]
  node [
    id 1044
    label "alkohol"
  ]
  node [
    id 1045
    label "nap&#243;j"
  ]
  node [
    id 1046
    label "bacik"
  ]
  node [
    id 1047
    label "wyj&#347;cie"
  ]
  node [
    id 1048
    label "uwarzy&#263;"
  ]
  node [
    id 1049
    label "birofilia"
  ]
  node [
    id 1050
    label "warzy&#263;"
  ]
  node [
    id 1051
    label "nawarzy&#263;"
  ]
  node [
    id 1052
    label "browarnia"
  ]
  node [
    id 1053
    label "nawarzenie"
  ]
  node [
    id 1054
    label "anta&#322;"
  ]
  node [
    id 1055
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 1056
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 1057
    label "appear"
  ]
  node [
    id 1058
    label "rise"
  ]
  node [
    id 1059
    label "zmniejszenie_si&#281;"
  ]
  node [
    id 1060
    label "zmniejszanie"
  ]
  node [
    id 1061
    label "inny"
  ]
  node [
    id 1062
    label "zmniejszanie_si&#281;"
  ]
  node [
    id 1063
    label "chudni&#281;cie"
  ]
  node [
    id 1064
    label "odwadnianie"
  ]
  node [
    id 1065
    label "decrease"
  ]
  node [
    id 1066
    label "spr&#281;&#380;anie"
  ]
  node [
    id 1067
    label "chudszy"
  ]
  node [
    id 1068
    label "kolejny"
  ]
  node [
    id 1069
    label "osobno"
  ]
  node [
    id 1070
    label "r&#243;&#380;ny"
  ]
  node [
    id 1071
    label "inszy"
  ]
  node [
    id 1072
    label "inaczej"
  ]
  node [
    id 1073
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1074
    label "charakterystyka"
  ]
  node [
    id 1075
    label "m&#322;ot"
  ]
  node [
    id 1076
    label "znak"
  ]
  node [
    id 1077
    label "drzewo"
  ]
  node [
    id 1078
    label "attribute"
  ]
  node [
    id 1079
    label "marka"
  ]
  node [
    id 1080
    label "posiada&#263;"
  ]
  node [
    id 1081
    label "potencja&#322;"
  ]
  node [
    id 1082
    label "zapomina&#263;"
  ]
  node [
    id 1083
    label "zapomnienie"
  ]
  node [
    id 1084
    label "zapominanie"
  ]
  node [
    id 1085
    label "ability"
  ]
  node [
    id 1086
    label "obliczeniowo"
  ]
  node [
    id 1087
    label "zapomnie&#263;"
  ]
  node [
    id 1088
    label "ostatnie_podrygi"
  ]
  node [
    id 1089
    label "visitation"
  ]
  node [
    id 1090
    label "agonia"
  ]
  node [
    id 1091
    label "defenestracja"
  ]
  node [
    id 1092
    label "dzia&#322;anie"
  ]
  node [
    id 1093
    label "kres"
  ]
  node [
    id 1094
    label "mogi&#322;a"
  ]
  node [
    id 1095
    label "kres_&#380;ycia"
  ]
  node [
    id 1096
    label "szereg"
  ]
  node [
    id 1097
    label "szeol"
  ]
  node [
    id 1098
    label "pogrzebanie"
  ]
  node [
    id 1099
    label "&#380;a&#322;oba"
  ]
  node [
    id 1100
    label "zabicie"
  ]
  node [
    id 1101
    label "przebiec"
  ]
  node [
    id 1102
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1103
    label "przebiegni&#281;cie"
  ]
  node [
    id 1104
    label "fabu&#322;a"
  ]
  node [
    id 1105
    label "&#347;mier&#263;"
  ]
  node [
    id 1106
    label "death"
  ]
  node [
    id 1107
    label "upadek"
  ]
  node [
    id 1108
    label "zmierzch"
  ]
  node [
    id 1109
    label "stan"
  ]
  node [
    id 1110
    label "spocz&#261;&#263;"
  ]
  node [
    id 1111
    label "spocz&#281;cie"
  ]
  node [
    id 1112
    label "pochowanie"
  ]
  node [
    id 1113
    label "spoczywa&#263;"
  ]
  node [
    id 1114
    label "chowanie"
  ]
  node [
    id 1115
    label "park_sztywnych"
  ]
  node [
    id 1116
    label "pomnik"
  ]
  node [
    id 1117
    label "nagrobek"
  ]
  node [
    id 1118
    label "prochowisko"
  ]
  node [
    id 1119
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 1120
    label "spoczywanie"
  ]
  node [
    id 1121
    label "za&#347;wiaty"
  ]
  node [
    id 1122
    label "piek&#322;o"
  ]
  node [
    id 1123
    label "judaizm"
  ]
  node [
    id 1124
    label "wyrzucenie"
  ]
  node [
    id 1125
    label "defenestration"
  ]
  node [
    id 1126
    label "zaj&#347;cie"
  ]
  node [
    id 1127
    label "&#380;al"
  ]
  node [
    id 1128
    label "paznokie&#263;"
  ]
  node [
    id 1129
    label "symbol"
  ]
  node [
    id 1130
    label "kir"
  ]
  node [
    id 1131
    label "brud"
  ]
  node [
    id 1132
    label "burying"
  ]
  node [
    id 1133
    label "zasypanie"
  ]
  node [
    id 1134
    label "zw&#322;oki"
  ]
  node [
    id 1135
    label "burial"
  ]
  node [
    id 1136
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1137
    label "porobienie"
  ]
  node [
    id 1138
    label "gr&#243;b"
  ]
  node [
    id 1139
    label "uniemo&#380;liwienie"
  ]
  node [
    id 1140
    label "destruction"
  ]
  node [
    id 1141
    label "zabrzmienie"
  ]
  node [
    id 1142
    label "skrzywdzenie"
  ]
  node [
    id 1143
    label "pozabijanie"
  ]
  node [
    id 1144
    label "zniszczenie"
  ]
  node [
    id 1145
    label "zaszkodzenie"
  ]
  node [
    id 1146
    label "usuni&#281;cie"
  ]
  node [
    id 1147
    label "killing"
  ]
  node [
    id 1148
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1149
    label "umarcie"
  ]
  node [
    id 1150
    label "zamkni&#281;cie"
  ]
  node [
    id 1151
    label "compaction"
  ]
  node [
    id 1152
    label "ust&#281;p"
  ]
  node [
    id 1153
    label "plan"
  ]
  node [
    id 1154
    label "obiekt_matematyczny"
  ]
  node [
    id 1155
    label "plamka"
  ]
  node [
    id 1156
    label "stopie&#324;_pisma"
  ]
  node [
    id 1157
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1158
    label "mark"
  ]
  node [
    id 1159
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1160
    label "prosta"
  ]
  node [
    id 1161
    label "zapunktowa&#263;"
  ]
  node [
    id 1162
    label "podpunkt"
  ]
  node [
    id 1163
    label "pozycja"
  ]
  node [
    id 1164
    label "szpaler"
  ]
  node [
    id 1165
    label "column"
  ]
  node [
    id 1166
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1167
    label "mn&#243;stwo"
  ]
  node [
    id 1168
    label "unit"
  ]
  node [
    id 1169
    label "rozmieszczenie"
  ]
  node [
    id 1170
    label "tract"
  ]
  node [
    id 1171
    label "powodowanie"
  ]
  node [
    id 1172
    label "liczenie"
  ]
  node [
    id 1173
    label "skutek"
  ]
  node [
    id 1174
    label "podzia&#322;anie"
  ]
  node [
    id 1175
    label "kampania"
  ]
  node [
    id 1176
    label "uruchamianie"
  ]
  node [
    id 1177
    label "operacja"
  ]
  node [
    id 1178
    label "hipnotyzowanie"
  ]
  node [
    id 1179
    label "uruchomienie"
  ]
  node [
    id 1180
    label "nakr&#281;canie"
  ]
  node [
    id 1181
    label "reakcja_chemiczna"
  ]
  node [
    id 1182
    label "tr&#243;jstronny"
  ]
  node [
    id 1183
    label "nakr&#281;cenie"
  ]
  node [
    id 1184
    label "zatrzymanie"
  ]
  node [
    id 1185
    label "wp&#322;yw"
  ]
  node [
    id 1186
    label "podtrzymywanie"
  ]
  node [
    id 1187
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1188
    label "liczy&#263;"
  ]
  node [
    id 1189
    label "operation"
  ]
  node [
    id 1190
    label "zadzia&#322;anie"
  ]
  node [
    id 1191
    label "priorytet"
  ]
  node [
    id 1192
    label "rozpocz&#281;cie"
  ]
  node [
    id 1193
    label "docieranie"
  ]
  node [
    id 1194
    label "czynny"
  ]
  node [
    id 1195
    label "impact"
  ]
  node [
    id 1196
    label "oferta"
  ]
  node [
    id 1197
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1198
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1199
    label "jedyny"
  ]
  node [
    id 1200
    label "doskona&#322;y"
  ]
  node [
    id 1201
    label "ukochany"
  ]
  node [
    id 1202
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1203
    label "najlepszy"
  ]
  node [
    id 1204
    label "optymalnie"
  ]
  node [
    id 1205
    label "wspania&#322;y"
  ]
  node [
    id 1206
    label "naj"
  ]
  node [
    id 1207
    label "&#347;wietny"
  ]
  node [
    id 1208
    label "pe&#322;ny"
  ]
  node [
    id 1209
    label "doskonale"
  ]
  node [
    id 1210
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1211
    label "erection"
  ]
  node [
    id 1212
    label "setup"
  ]
  node [
    id 1213
    label "erecting"
  ]
  node [
    id 1214
    label "poustawianie"
  ]
  node [
    id 1215
    label "zinterpretowanie"
  ]
  node [
    id 1216
    label "porozstawianie"
  ]
  node [
    id 1217
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 1218
    label "porozmieszczanie"
  ]
  node [
    id 1219
    label "wyst&#281;powanie"
  ]
  node [
    id 1220
    label "uk&#322;ad"
  ]
  node [
    id 1221
    label "layout"
  ]
  node [
    id 1222
    label "umieszczenie"
  ]
  node [
    id 1223
    label "zepsucie"
  ]
  node [
    id 1224
    label "za&#322;amanie_si&#281;"
  ]
  node [
    id 1225
    label "pora&#380;ka"
  ]
  node [
    id 1226
    label "wygranie"
  ]
  node [
    id 1227
    label "dissection"
  ]
  node [
    id 1228
    label "dissolution"
  ]
  node [
    id 1229
    label "adjustment"
  ]
  node [
    id 1230
    label "dislocation"
  ]
  node [
    id 1231
    label "przygn&#281;bienie"
  ]
  node [
    id 1232
    label "zaplanowanie"
  ]
  node [
    id 1233
    label "reading"
  ]
  node [
    id 1234
    label "porozk&#322;adanie"
  ]
  node [
    id 1235
    label "rozdzielenie"
  ]
  node [
    id 1236
    label "podzielenie"
  ]
  node [
    id 1237
    label "remark"
  ]
  node [
    id 1238
    label "appreciation"
  ]
  node [
    id 1239
    label "ocenienie"
  ]
  node [
    id 1240
    label "zanalizowanie"
  ]
  node [
    id 1241
    label "campaign"
  ]
  node [
    id 1242
    label "causing"
  ]
  node [
    id 1243
    label "activity"
  ]
  node [
    id 1244
    label "bezproblemowy"
  ]
  node [
    id 1245
    label "decyzja"
  ]
  node [
    id 1246
    label "umocnienie"
  ]
  node [
    id 1247
    label "appointment"
  ]
  node [
    id 1248
    label "localization"
  ]
  node [
    id 1249
    label "informacja"
  ]
  node [
    id 1250
    label "zrobienie"
  ]
  node [
    id 1251
    label "manner"
  ]
  node [
    id 1252
    label "stworzenie"
  ]
  node [
    id 1253
    label "pomy&#347;lenie"
  ]
  node [
    id 1254
    label "ukszta&#322;towanie"
  ]
  node [
    id 1255
    label "wykszta&#322;cenie"
  ]
  node [
    id 1256
    label "succession"
  ]
  node [
    id 1257
    label "nauczenie"
  ]
  node [
    id 1258
    label "poumieszczanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 826
  ]
  edge [
    source 14
    target 827
  ]
  edge [
    source 14
    target 828
  ]
  edge [
    source 14
    target 271
  ]
  edge [
    source 14
    target 829
  ]
  edge [
    source 14
    target 830
  ]
  edge [
    source 14
    target 831
  ]
  edge [
    source 14
    target 832
  ]
  edge [
    source 14
    target 833
  ]
  edge [
    source 14
    target 834
  ]
  edge [
    source 14
    target 835
  ]
  edge [
    source 14
    target 402
  ]
  edge [
    source 14
    target 836
  ]
  edge [
    source 14
    target 837
  ]
  edge [
    source 14
    target 838
  ]
  edge [
    source 14
    target 839
  ]
  edge [
    source 14
    target 840
  ]
  edge [
    source 14
    target 841
  ]
  edge [
    source 14
    target 842
  ]
  edge [
    source 14
    target 843
  ]
  edge [
    source 14
    target 844
  ]
  edge [
    source 14
    target 845
  ]
  edge [
    source 14
    target 846
  ]
  edge [
    source 14
    target 847
  ]
  edge [
    source 14
    target 848
  ]
  edge [
    source 14
    target 849
  ]
  edge [
    source 14
    target 850
  ]
  edge [
    source 14
    target 851
  ]
  edge [
    source 14
    target 852
  ]
  edge [
    source 14
    target 853
  ]
  edge [
    source 14
    target 226
  ]
  edge [
    source 14
    target 411
  ]
  edge [
    source 14
    target 854
  ]
  edge [
    source 14
    target 412
  ]
  edge [
    source 14
    target 234
  ]
  edge [
    source 14
    target 855
  ]
  edge [
    source 14
    target 856
  ]
  edge [
    source 14
    target 857
  ]
  edge [
    source 14
    target 414
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 858
  ]
  edge [
    source 14
    target 417
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 418
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 510
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 533
  ]
  edge [
    source 16
    target 534
  ]
  edge [
    source 16
    target 535
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 536
  ]
  edge [
    source 16
    target 537
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 514
  ]
  edge [
    source 16
    target 539
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 542
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 313
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 541
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 60
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 394
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 56
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 215
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 59
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 488
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 523
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 522
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 507
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 801
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 802
  ]
  edge [
    source 20
    target 803
  ]
  edge [
    source 20
    target 804
  ]
  edge [
    source 20
    target 805
  ]
  edge [
    source 20
    target 806
  ]
  edge [
    source 20
    target 807
  ]
  edge [
    source 20
    target 808
  ]
  edge [
    source 20
    target 809
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 381
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1041
  ]
  edge [
    source 26
    target 1042
  ]
  edge [
    source 26
    target 1043
  ]
  edge [
    source 26
    target 1044
  ]
  edge [
    source 26
    target 1045
  ]
  edge [
    source 26
    target 1046
  ]
  edge [
    source 26
    target 1047
  ]
  edge [
    source 26
    target 1048
  ]
  edge [
    source 26
    target 1049
  ]
  edge [
    source 26
    target 1050
  ]
  edge [
    source 26
    target 1051
  ]
  edge [
    source 26
    target 1052
  ]
  edge [
    source 26
    target 1053
  ]
  edge [
    source 26
    target 1054
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1055
  ]
  edge [
    source 27
    target 1056
  ]
  edge [
    source 27
    target 1057
  ]
  edge [
    source 27
    target 1058
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1059
  ]
  edge [
    source 30
    target 1060
  ]
  edge [
    source 30
    target 1061
  ]
  edge [
    source 30
    target 1062
  ]
  edge [
    source 30
    target 1063
  ]
  edge [
    source 30
    target 315
  ]
  edge [
    source 30
    target 1064
  ]
  edge [
    source 30
    target 1065
  ]
  edge [
    source 30
    target 108
  ]
  edge [
    source 30
    target 1066
  ]
  edge [
    source 30
    target 1067
  ]
  edge [
    source 30
    target 125
  ]
  edge [
    source 30
    target 1068
  ]
  edge [
    source 30
    target 1069
  ]
  edge [
    source 30
    target 1070
  ]
  edge [
    source 30
    target 1071
  ]
  edge [
    source 30
    target 1072
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1073
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 1074
  ]
  edge [
    source 31
    target 1075
  ]
  edge [
    source 31
    target 1076
  ]
  edge [
    source 31
    target 1077
  ]
  edge [
    source 31
    target 91
  ]
  edge [
    source 31
    target 1078
  ]
  edge [
    source 31
    target 1079
  ]
  edge [
    source 31
    target 1080
  ]
  edge [
    source 31
    target 1081
  ]
  edge [
    source 31
    target 1082
  ]
  edge [
    source 31
    target 1083
  ]
  edge [
    source 31
    target 1084
  ]
  edge [
    source 31
    target 1085
  ]
  edge [
    source 31
    target 1086
  ]
  edge [
    source 31
    target 1087
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1088
  ]
  edge [
    source 32
    target 1089
  ]
  edge [
    source 32
    target 1090
  ]
  edge [
    source 32
    target 1091
  ]
  edge [
    source 32
    target 657
  ]
  edge [
    source 32
    target 1092
  ]
  edge [
    source 32
    target 1093
  ]
  edge [
    source 32
    target 65
  ]
  edge [
    source 32
    target 1094
  ]
  edge [
    source 32
    target 1095
  ]
  edge [
    source 32
    target 1096
  ]
  edge [
    source 32
    target 1097
  ]
  edge [
    source 32
    target 1098
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 214
  ]
  edge [
    source 32
    target 1099
  ]
  edge [
    source 32
    target 205
  ]
  edge [
    source 32
    target 1100
  ]
  edge [
    source 32
    target 1101
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 108
  ]
  edge [
    source 32
    target 1102
  ]
  edge [
    source 32
    target 80
  ]
  edge [
    source 32
    target 1103
  ]
  edge [
    source 32
    target 1104
  ]
  edge [
    source 32
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 59
  ]
  edge [
    source 32
    target 207
  ]
  edge [
    source 32
    target 208
  ]
  edge [
    source 32
    target 209
  ]
  edge [
    source 32
    target 210
  ]
  edge [
    source 32
    target 211
  ]
  edge [
    source 32
    target 212
  ]
  edge [
    source 32
    target 213
  ]
  edge [
    source 32
    target 215
  ]
  edge [
    source 32
    target 216
  ]
  edge [
    source 32
    target 101
  ]
  edge [
    source 32
    target 217
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 32
    target 1105
  ]
  edge [
    source 32
    target 1106
  ]
  edge [
    source 32
    target 1107
  ]
  edge [
    source 32
    target 1108
  ]
  edge [
    source 32
    target 1109
  ]
  edge [
    source 32
    target 779
  ]
  edge [
    source 32
    target 1110
  ]
  edge [
    source 32
    target 1111
  ]
  edge [
    source 32
    target 1112
  ]
  edge [
    source 32
    target 1113
  ]
  edge [
    source 32
    target 1114
  ]
  edge [
    source 32
    target 1115
  ]
  edge [
    source 32
    target 1116
  ]
  edge [
    source 32
    target 1117
  ]
  edge [
    source 32
    target 1118
  ]
  edge [
    source 32
    target 1119
  ]
  edge [
    source 32
    target 1120
  ]
  edge [
    source 32
    target 1121
  ]
  edge [
    source 32
    target 1122
  ]
  edge [
    source 32
    target 1123
  ]
  edge [
    source 32
    target 1124
  ]
  edge [
    source 32
    target 1125
  ]
  edge [
    source 32
    target 1126
  ]
  edge [
    source 32
    target 1127
  ]
  edge [
    source 32
    target 1128
  ]
  edge [
    source 32
    target 1129
  ]
  edge [
    source 32
    target 1130
  ]
  edge [
    source 32
    target 1131
  ]
  edge [
    source 32
    target 1132
  ]
  edge [
    source 32
    target 1133
  ]
  edge [
    source 32
    target 1134
  ]
  edge [
    source 32
    target 1135
  ]
  edge [
    source 32
    target 1136
  ]
  edge [
    source 32
    target 1137
  ]
  edge [
    source 32
    target 1138
  ]
  edge [
    source 32
    target 1139
  ]
  edge [
    source 32
    target 1140
  ]
  edge [
    source 32
    target 1141
  ]
  edge [
    source 32
    target 1142
  ]
  edge [
    source 32
    target 1143
  ]
  edge [
    source 32
    target 1144
  ]
  edge [
    source 32
    target 1145
  ]
  edge [
    source 32
    target 1146
  ]
  edge [
    source 32
    target 922
  ]
  edge [
    source 32
    target 1147
  ]
  edge [
    source 32
    target 1148
  ]
  edge [
    source 32
    target 674
  ]
  edge [
    source 32
    target 1149
  ]
  edge [
    source 32
    target 699
  ]
  edge [
    source 32
    target 1150
  ]
  edge [
    source 32
    target 1151
  ]
  edge [
    source 32
    target 656
  ]
  edge [
    source 32
    target 800
  ]
  edge [
    source 32
    target 1152
  ]
  edge [
    source 32
    target 1153
  ]
  edge [
    source 32
    target 1154
  ]
  edge [
    source 32
    target 802
  ]
  edge [
    source 32
    target 1155
  ]
  edge [
    source 32
    target 1156
  ]
  edge [
    source 32
    target 827
  ]
  edge [
    source 32
    target 805
  ]
  edge [
    source 32
    target 1157
  ]
  edge [
    source 32
    target 1158
  ]
  edge [
    source 32
    target 1159
  ]
  edge [
    source 32
    target 1160
  ]
  edge [
    source 32
    target 806
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 1161
  ]
  edge [
    source 32
    target 1162
  ]
  edge [
    source 32
    target 659
  ]
  edge [
    source 32
    target 588
  ]
  edge [
    source 32
    target 1163
  ]
  edge [
    source 32
    target 1164
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 1165
  ]
  edge [
    source 32
    target 1166
  ]
  edge [
    source 32
    target 1167
  ]
  edge [
    source 32
    target 1168
  ]
  edge [
    source 32
    target 1169
  ]
  edge [
    source 32
    target 1170
  ]
  edge [
    source 32
    target 994
  ]
  edge [
    source 32
    target 862
  ]
  edge [
    source 32
    target 1171
  ]
  edge [
    source 32
    target 1172
  ]
  edge [
    source 32
    target 56
  ]
  edge [
    source 32
    target 1173
  ]
  edge [
    source 32
    target 1174
  ]
  edge [
    source 32
    target 839
  ]
  edge [
    source 32
    target 1175
  ]
  edge [
    source 32
    target 1176
  ]
  edge [
    source 32
    target 845
  ]
  edge [
    source 32
    target 1177
  ]
  edge [
    source 32
    target 1178
  ]
  edge [
    source 32
    target 458
  ]
  edge [
    source 32
    target 1179
  ]
  edge [
    source 32
    target 1180
  ]
  edge [
    source 32
    target 480
  ]
  edge [
    source 32
    target 849
  ]
  edge [
    source 32
    target 1181
  ]
  edge [
    source 32
    target 1182
  ]
  edge [
    source 32
    target 482
  ]
  edge [
    source 32
    target 1183
  ]
  edge [
    source 32
    target 1184
  ]
  edge [
    source 32
    target 1185
  ]
  edge [
    source 32
    target 851
  ]
  edge [
    source 32
    target 1186
  ]
  edge [
    source 32
    target 1187
  ]
  edge [
    source 32
    target 1188
  ]
  edge [
    source 32
    target 1189
  ]
  edge [
    source 32
    target 105
  ]
  edge [
    source 32
    target 141
  ]
  edge [
    source 32
    target 1190
  ]
  edge [
    source 32
    target 1191
  ]
  edge [
    source 32
    target 145
  ]
  edge [
    source 32
    target 1192
  ]
  edge [
    source 32
    target 1193
  ]
  edge [
    source 32
    target 860
  ]
  edge [
    source 32
    target 1194
  ]
  edge [
    source 32
    target 1195
  ]
  edge [
    source 32
    target 1196
  ]
  edge [
    source 32
    target 728
  ]
  edge [
    source 32
    target 645
  ]
  edge [
    source 32
    target 1197
  ]
  edge [
    source 32
    target 1198
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1199
  ]
  edge [
    source 33
    target 1200
  ]
  edge [
    source 33
    target 261
  ]
  edge [
    source 33
    target 1201
  ]
  edge [
    source 33
    target 1202
  ]
  edge [
    source 33
    target 1203
  ]
  edge [
    source 33
    target 1204
  ]
  edge [
    source 33
    target 1205
  ]
  edge [
    source 33
    target 514
  ]
  edge [
    source 33
    target 1206
  ]
  edge [
    source 33
    target 1207
  ]
  edge [
    source 33
    target 1208
  ]
  edge [
    source 33
    target 1209
  ]
  edge [
    source 34
    target 1210
  ]
  edge [
    source 34
    target 96
  ]
  edge [
    source 34
    target 1211
  ]
  edge [
    source 34
    target 1212
  ]
  edge [
    source 34
    target 922
  ]
  edge [
    source 34
    target 1213
  ]
  edge [
    source 34
    target 1169
  ]
  edge [
    source 34
    target 1214
  ]
  edge [
    source 34
    target 1215
  ]
  edge [
    source 34
    target 1216
  ]
  edge [
    source 34
    target 108
  ]
  edge [
    source 34
    target 569
  ]
  edge [
    source 34
    target 1217
  ]
  edge [
    source 34
    target 1218
  ]
  edge [
    source 34
    target 1219
  ]
  edge [
    source 34
    target 1220
  ]
  edge [
    source 34
    target 1221
  ]
  edge [
    source 34
    target 1222
  ]
  edge [
    source 34
    target 1223
  ]
  edge [
    source 34
    target 656
  ]
  edge [
    source 34
    target 1224
  ]
  edge [
    source 34
    target 1225
  ]
  edge [
    source 34
    target 1226
  ]
  edge [
    source 34
    target 1227
  ]
  edge [
    source 34
    target 1228
  ]
  edge [
    source 34
    target 1229
  ]
  edge [
    source 34
    target 1230
  ]
  edge [
    source 34
    target 1231
  ]
  edge [
    source 34
    target 1232
  ]
  edge [
    source 34
    target 1233
  ]
  edge [
    source 34
    target 1234
  ]
  edge [
    source 34
    target 1235
  ]
  edge [
    source 34
    target 1236
  ]
  edge [
    source 34
    target 1237
  ]
  edge [
    source 34
    target 1238
  ]
  edge [
    source 34
    target 697
  ]
  edge [
    source 34
    target 1239
  ]
  edge [
    source 34
    target 1240
  ]
  edge [
    source 34
    target 1241
  ]
  edge [
    source 34
    target 1242
  ]
  edge [
    source 34
    target 1243
  ]
  edge [
    source 34
    target 1244
  ]
  edge [
    source 34
    target 65
  ]
  edge [
    source 34
    target 1245
  ]
  edge [
    source 34
    target 1246
  ]
  edge [
    source 34
    target 1247
  ]
  edge [
    source 34
    target 1248
  ]
  edge [
    source 34
    target 1249
  ]
  edge [
    source 34
    target 1021
  ]
  edge [
    source 34
    target 1250
  ]
  edge [
    source 34
    target 1251
  ]
  edge [
    source 34
    target 560
  ]
  edge [
    source 34
    target 1252
  ]
  edge [
    source 34
    target 1253
  ]
  edge [
    source 34
    target 1254
  ]
  edge [
    source 34
    target 1255
  ]
  edge [
    source 34
    target 216
  ]
  edge [
    source 34
    target 1256
  ]
  edge [
    source 34
    target 1257
  ]
  edge [
    source 34
    target 1258
  ]
  edge [
    source 34
    target 667
  ]
  edge [
    source 34
    target 181
  ]
  edge [
    source 34
    target 668
  ]
  edge [
    source 34
    target 669
  ]
  edge [
    source 34
    target 670
  ]
  edge [
    source 34
    target 671
  ]
  edge [
    source 34
    target 672
  ]
  edge [
    source 34
    target 673
  ]
  edge [
    source 34
    target 674
  ]
  edge [
    source 34
    target 675
  ]
  edge [
    source 34
    target 676
  ]
  edge [
    source 34
    target 677
  ]
  edge [
    source 34
    target 678
  ]
  edge [
    source 34
    target 679
  ]
  edge [
    source 34
    target 407
  ]
  edge [
    source 34
    target 680
  ]
  edge [
    source 34
    target 681
  ]
  edge [
    source 34
    target 682
  ]
  edge [
    source 34
    target 683
  ]
  edge [
    source 34
    target 684
  ]
  edge [
    source 34
    target 685
  ]
  edge [
    source 34
    target 686
  ]
  edge [
    source 34
    target 687
  ]
  edge [
    source 34
    target 688
  ]
  edge [
    source 34
    target 689
  ]
  edge [
    source 34
    target 690
  ]
  edge [
    source 34
    target 691
  ]
  edge [
    source 34
    target 692
  ]
  edge [
    source 34
    target 693
  ]
  edge [
    source 34
    target 640
  ]
  edge [
    source 34
    target 694
  ]
  edge [
    source 34
    target 695
  ]
  edge [
    source 34
    target 696
  ]
  edge [
    source 34
    target 698
  ]
  edge [
    source 34
    target 699
  ]
]
