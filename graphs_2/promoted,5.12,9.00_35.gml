graph [
  node [
    id 0
    label "kilka"
    origin "text"
  ]
  node [
    id 1
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 4
    label "naukowiec"
    origin "text"
  ]
  node [
    id 5
    label "poinformowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 7
    label "poroda"
    origin "text"
  ]
  node [
    id 8
    label "genetycznie"
    origin "text"
  ]
  node [
    id 9
    label "zmodyfikowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "bli&#378;niaczek"
    origin "text"
  ]
  node [
    id 11
    label "ryba"
  ]
  node [
    id 12
    label "&#347;ledziowate"
  ]
  node [
    id 13
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 14
    label "kr&#281;gowiec"
  ]
  node [
    id 15
    label "cz&#322;owiek"
  ]
  node [
    id 16
    label "systemik"
  ]
  node [
    id 17
    label "doniczkowiec"
  ]
  node [
    id 18
    label "mi&#281;so"
  ]
  node [
    id 19
    label "system"
  ]
  node [
    id 20
    label "patroszy&#263;"
  ]
  node [
    id 21
    label "rakowato&#347;&#263;"
  ]
  node [
    id 22
    label "w&#281;dkarstwo"
  ]
  node [
    id 23
    label "ryby"
  ]
  node [
    id 24
    label "fish"
  ]
  node [
    id 25
    label "linia_boczna"
  ]
  node [
    id 26
    label "tar&#322;o"
  ]
  node [
    id 27
    label "wyrostek_filtracyjny"
  ]
  node [
    id 28
    label "m&#281;tnooki"
  ]
  node [
    id 29
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 30
    label "pokrywa_skrzelowa"
  ]
  node [
    id 31
    label "ikra"
  ]
  node [
    id 32
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 33
    label "szczelina_skrzelowa"
  ]
  node [
    id 34
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 35
    label "ranek"
  ]
  node [
    id 36
    label "doba"
  ]
  node [
    id 37
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 38
    label "noc"
  ]
  node [
    id 39
    label "podwiecz&#243;r"
  ]
  node [
    id 40
    label "po&#322;udnie"
  ]
  node [
    id 41
    label "godzina"
  ]
  node [
    id 42
    label "przedpo&#322;udnie"
  ]
  node [
    id 43
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 44
    label "long_time"
  ]
  node [
    id 45
    label "wiecz&#243;r"
  ]
  node [
    id 46
    label "t&#322;usty_czwartek"
  ]
  node [
    id 47
    label "popo&#322;udnie"
  ]
  node [
    id 48
    label "walentynki"
  ]
  node [
    id 49
    label "czynienie_si&#281;"
  ]
  node [
    id 50
    label "s&#322;o&#324;ce"
  ]
  node [
    id 51
    label "rano"
  ]
  node [
    id 52
    label "tydzie&#324;"
  ]
  node [
    id 53
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 54
    label "wzej&#347;cie"
  ]
  node [
    id 55
    label "czas"
  ]
  node [
    id 56
    label "wsta&#263;"
  ]
  node [
    id 57
    label "day"
  ]
  node [
    id 58
    label "termin"
  ]
  node [
    id 59
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 60
    label "wstanie"
  ]
  node [
    id 61
    label "przedwiecz&#243;r"
  ]
  node [
    id 62
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 63
    label "Sylwester"
  ]
  node [
    id 64
    label "poprzedzanie"
  ]
  node [
    id 65
    label "czasoprzestrze&#324;"
  ]
  node [
    id 66
    label "laba"
  ]
  node [
    id 67
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 68
    label "chronometria"
  ]
  node [
    id 69
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 70
    label "rachuba_czasu"
  ]
  node [
    id 71
    label "przep&#322;ywanie"
  ]
  node [
    id 72
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 73
    label "czasokres"
  ]
  node [
    id 74
    label "odczyt"
  ]
  node [
    id 75
    label "chwila"
  ]
  node [
    id 76
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 77
    label "dzieje"
  ]
  node [
    id 78
    label "kategoria_gramatyczna"
  ]
  node [
    id 79
    label "poprzedzenie"
  ]
  node [
    id 80
    label "trawienie"
  ]
  node [
    id 81
    label "pochodzi&#263;"
  ]
  node [
    id 82
    label "period"
  ]
  node [
    id 83
    label "okres_czasu"
  ]
  node [
    id 84
    label "poprzedza&#263;"
  ]
  node [
    id 85
    label "schy&#322;ek"
  ]
  node [
    id 86
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 87
    label "odwlekanie_si&#281;"
  ]
  node [
    id 88
    label "zegar"
  ]
  node [
    id 89
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 90
    label "czwarty_wymiar"
  ]
  node [
    id 91
    label "pochodzenie"
  ]
  node [
    id 92
    label "koniugacja"
  ]
  node [
    id 93
    label "Zeitgeist"
  ]
  node [
    id 94
    label "trawi&#263;"
  ]
  node [
    id 95
    label "pogoda"
  ]
  node [
    id 96
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 97
    label "poprzedzi&#263;"
  ]
  node [
    id 98
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 99
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 100
    label "time_period"
  ]
  node [
    id 101
    label "nazewnictwo"
  ]
  node [
    id 102
    label "term"
  ]
  node [
    id 103
    label "przypadni&#281;cie"
  ]
  node [
    id 104
    label "ekspiracja"
  ]
  node [
    id 105
    label "przypa&#347;&#263;"
  ]
  node [
    id 106
    label "chronogram"
  ]
  node [
    id 107
    label "praktyka"
  ]
  node [
    id 108
    label "nazwa"
  ]
  node [
    id 109
    label "przyj&#281;cie"
  ]
  node [
    id 110
    label "spotkanie"
  ]
  node [
    id 111
    label "night"
  ]
  node [
    id 112
    label "zach&#243;d"
  ]
  node [
    id 113
    label "vesper"
  ]
  node [
    id 114
    label "pora"
  ]
  node [
    id 115
    label "odwieczerz"
  ]
  node [
    id 116
    label "blady_&#347;wit"
  ]
  node [
    id 117
    label "podkurek"
  ]
  node [
    id 118
    label "aurora"
  ]
  node [
    id 119
    label "wsch&#243;d"
  ]
  node [
    id 120
    label "zjawisko"
  ]
  node [
    id 121
    label "&#347;rodek"
  ]
  node [
    id 122
    label "obszar"
  ]
  node [
    id 123
    label "Ziemia"
  ]
  node [
    id 124
    label "dwunasta"
  ]
  node [
    id 125
    label "strona_&#347;wiata"
  ]
  node [
    id 126
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 127
    label "dopo&#322;udnie"
  ]
  node [
    id 128
    label "p&#243;&#322;noc"
  ]
  node [
    id 129
    label "nokturn"
  ]
  node [
    id 130
    label "time"
  ]
  node [
    id 131
    label "p&#243;&#322;godzina"
  ]
  node [
    id 132
    label "jednostka_czasu"
  ]
  node [
    id 133
    label "minuta"
  ]
  node [
    id 134
    label "kwadrans"
  ]
  node [
    id 135
    label "jednostka_geologiczna"
  ]
  node [
    id 136
    label "weekend"
  ]
  node [
    id 137
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 138
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 139
    label "miesi&#261;c"
  ]
  node [
    id 140
    label "S&#322;o&#324;ce"
  ]
  node [
    id 141
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 142
    label "&#347;wiat&#322;o"
  ]
  node [
    id 143
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 144
    label "kochanie"
  ]
  node [
    id 145
    label "sunlight"
  ]
  node [
    id 146
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 147
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 148
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 149
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 150
    label "mount"
  ]
  node [
    id 151
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 152
    label "wzej&#347;&#263;"
  ]
  node [
    id 153
    label "ascend"
  ]
  node [
    id 154
    label "kuca&#263;"
  ]
  node [
    id 155
    label "wyzdrowie&#263;"
  ]
  node [
    id 156
    label "opu&#347;ci&#263;"
  ]
  node [
    id 157
    label "rise"
  ]
  node [
    id 158
    label "arise"
  ]
  node [
    id 159
    label "stan&#261;&#263;"
  ]
  node [
    id 160
    label "przesta&#263;"
  ]
  node [
    id 161
    label "wyzdrowienie"
  ]
  node [
    id 162
    label "le&#380;enie"
  ]
  node [
    id 163
    label "kl&#281;czenie"
  ]
  node [
    id 164
    label "opuszczenie"
  ]
  node [
    id 165
    label "uniesienie_si&#281;"
  ]
  node [
    id 166
    label "siedzenie"
  ]
  node [
    id 167
    label "beginning"
  ]
  node [
    id 168
    label "przestanie"
  ]
  node [
    id 169
    label "grudzie&#324;"
  ]
  node [
    id 170
    label "luty"
  ]
  node [
    id 171
    label "chi&#324;sko"
  ]
  node [
    id 172
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 173
    label "po_chi&#324;sku"
  ]
  node [
    id 174
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 175
    label "kitajski"
  ]
  node [
    id 176
    label "lipny"
  ]
  node [
    id 177
    label "go"
  ]
  node [
    id 178
    label "niedrogi"
  ]
  node [
    id 179
    label "makroj&#281;zyk"
  ]
  node [
    id 180
    label "dziwaczny"
  ]
  node [
    id 181
    label "azjatycki"
  ]
  node [
    id 182
    label "j&#281;zyk"
  ]
  node [
    id 183
    label "tandetny"
  ]
  node [
    id 184
    label "dalekowschodni"
  ]
  node [
    id 185
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 186
    label "artykulator"
  ]
  node [
    id 187
    label "kod"
  ]
  node [
    id 188
    label "kawa&#322;ek"
  ]
  node [
    id 189
    label "przedmiot"
  ]
  node [
    id 190
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 191
    label "gramatyka"
  ]
  node [
    id 192
    label "stylik"
  ]
  node [
    id 193
    label "przet&#322;umaczenie"
  ]
  node [
    id 194
    label "formalizowanie"
  ]
  node [
    id 195
    label "ssa&#263;"
  ]
  node [
    id 196
    label "ssanie"
  ]
  node [
    id 197
    label "language"
  ]
  node [
    id 198
    label "liza&#263;"
  ]
  node [
    id 199
    label "napisa&#263;"
  ]
  node [
    id 200
    label "konsonantyzm"
  ]
  node [
    id 201
    label "wokalizm"
  ]
  node [
    id 202
    label "pisa&#263;"
  ]
  node [
    id 203
    label "fonetyka"
  ]
  node [
    id 204
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 205
    label "jeniec"
  ]
  node [
    id 206
    label "but"
  ]
  node [
    id 207
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 208
    label "po_koroniarsku"
  ]
  node [
    id 209
    label "kultura_duchowa"
  ]
  node [
    id 210
    label "t&#322;umaczenie"
  ]
  node [
    id 211
    label "m&#243;wienie"
  ]
  node [
    id 212
    label "pype&#263;"
  ]
  node [
    id 213
    label "lizanie"
  ]
  node [
    id 214
    label "pismo"
  ]
  node [
    id 215
    label "formalizowa&#263;"
  ]
  node [
    id 216
    label "rozumie&#263;"
  ]
  node [
    id 217
    label "organ"
  ]
  node [
    id 218
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 219
    label "rozumienie"
  ]
  node [
    id 220
    label "spos&#243;b"
  ]
  node [
    id 221
    label "makroglosja"
  ]
  node [
    id 222
    label "m&#243;wi&#263;"
  ]
  node [
    id 223
    label "jama_ustna"
  ]
  node [
    id 224
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 225
    label "formacja_geologiczna"
  ]
  node [
    id 226
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 227
    label "natural_language"
  ]
  node [
    id 228
    label "s&#322;ownictwo"
  ]
  node [
    id 229
    label "urz&#261;dzenie"
  ]
  node [
    id 230
    label "niedrogo"
  ]
  node [
    id 231
    label "kiczowaty"
  ]
  node [
    id 232
    label "banalny"
  ]
  node [
    id 233
    label "nieelegancki"
  ]
  node [
    id 234
    label "&#380;a&#322;osny"
  ]
  node [
    id 235
    label "tani"
  ]
  node [
    id 236
    label "kiepski"
  ]
  node [
    id 237
    label "tandetnie"
  ]
  node [
    id 238
    label "odra&#380;aj&#261;cy"
  ]
  node [
    id 239
    label "nikczemny"
  ]
  node [
    id 240
    label "lipnie"
  ]
  node [
    id 241
    label "siajowy"
  ]
  node [
    id 242
    label "nieprawdziwy"
  ]
  node [
    id 243
    label "z&#322;y"
  ]
  node [
    id 244
    label "po_dalekowschodniemu"
  ]
  node [
    id 245
    label "j&#281;zyk_naturalny"
  ]
  node [
    id 246
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 247
    label "kampong"
  ]
  node [
    id 248
    label "typowy"
  ]
  node [
    id 249
    label "ghaty"
  ]
  node [
    id 250
    label "charakterystyczny"
  ]
  node [
    id 251
    label "balut"
  ]
  node [
    id 252
    label "ka&#322;mucki"
  ]
  node [
    id 253
    label "azjatycko"
  ]
  node [
    id 254
    label "dziwny"
  ]
  node [
    id 255
    label "dziwotworny"
  ]
  node [
    id 256
    label "dziwacznie"
  ]
  node [
    id 257
    label "goban"
  ]
  node [
    id 258
    label "gra_planszowa"
  ]
  node [
    id 259
    label "sport_umys&#322;owy"
  ]
  node [
    id 260
    label "&#347;ledziciel"
  ]
  node [
    id 261
    label "uczony"
  ]
  node [
    id 262
    label "Miczurin"
  ]
  node [
    id 263
    label "wykszta&#322;cony"
  ]
  node [
    id 264
    label "inteligent"
  ]
  node [
    id 265
    label "intelektualista"
  ]
  node [
    id 266
    label "Awerroes"
  ]
  node [
    id 267
    label "uczenie"
  ]
  node [
    id 268
    label "nauczny"
  ]
  node [
    id 269
    label "m&#261;dry"
  ]
  node [
    id 270
    label "agent"
  ]
  node [
    id 271
    label "inform"
  ]
  node [
    id 272
    label "zakomunikowa&#263;"
  ]
  node [
    id 273
    label "spowodowa&#263;"
  ]
  node [
    id 274
    label "Stary_&#346;wiat"
  ]
  node [
    id 275
    label "asymilowanie_si&#281;"
  ]
  node [
    id 276
    label "Wsch&#243;d"
  ]
  node [
    id 277
    label "class"
  ]
  node [
    id 278
    label "geosfera"
  ]
  node [
    id 279
    label "obiekt_naturalny"
  ]
  node [
    id 280
    label "przejmowanie"
  ]
  node [
    id 281
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 282
    label "przyroda"
  ]
  node [
    id 283
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 284
    label "rzecz"
  ]
  node [
    id 285
    label "makrokosmos"
  ]
  node [
    id 286
    label "huczek"
  ]
  node [
    id 287
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 288
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "environment"
  ]
  node [
    id 290
    label "morze"
  ]
  node [
    id 291
    label "rze&#378;ba"
  ]
  node [
    id 292
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 293
    label "przejmowa&#263;"
  ]
  node [
    id 294
    label "hydrosfera"
  ]
  node [
    id 295
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 296
    label "ciemna_materia"
  ]
  node [
    id 297
    label "ekosystem"
  ]
  node [
    id 298
    label "biota"
  ]
  node [
    id 299
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 300
    label "ekosfera"
  ]
  node [
    id 301
    label "geotermia"
  ]
  node [
    id 302
    label "planeta"
  ]
  node [
    id 303
    label "ozonosfera"
  ]
  node [
    id 304
    label "wszechstworzenie"
  ]
  node [
    id 305
    label "grupa"
  ]
  node [
    id 306
    label "woda"
  ]
  node [
    id 307
    label "kuchnia"
  ]
  node [
    id 308
    label "biosfera"
  ]
  node [
    id 309
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 310
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 311
    label "populace"
  ]
  node [
    id 312
    label "magnetosfera"
  ]
  node [
    id 313
    label "Nowy_&#346;wiat"
  ]
  node [
    id 314
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 315
    label "universe"
  ]
  node [
    id 316
    label "biegun"
  ]
  node [
    id 317
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 318
    label "litosfera"
  ]
  node [
    id 319
    label "teren"
  ]
  node [
    id 320
    label "mikrokosmos"
  ]
  node [
    id 321
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 322
    label "przestrze&#324;"
  ]
  node [
    id 323
    label "stw&#243;r"
  ]
  node [
    id 324
    label "p&#243;&#322;kula"
  ]
  node [
    id 325
    label "przej&#281;cie"
  ]
  node [
    id 326
    label "barysfera"
  ]
  node [
    id 327
    label "czarna_dziura"
  ]
  node [
    id 328
    label "atmosfera"
  ]
  node [
    id 329
    label "przej&#261;&#263;"
  ]
  node [
    id 330
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 331
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 332
    label "geoida"
  ]
  node [
    id 333
    label "zagranica"
  ]
  node [
    id 334
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 335
    label "fauna"
  ]
  node [
    id 336
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 337
    label "odm&#322;adzanie"
  ]
  node [
    id 338
    label "liga"
  ]
  node [
    id 339
    label "jednostka_systematyczna"
  ]
  node [
    id 340
    label "asymilowanie"
  ]
  node [
    id 341
    label "gromada"
  ]
  node [
    id 342
    label "asymilowa&#263;"
  ]
  node [
    id 343
    label "egzemplarz"
  ]
  node [
    id 344
    label "Entuzjastki"
  ]
  node [
    id 345
    label "zbi&#243;r"
  ]
  node [
    id 346
    label "kompozycja"
  ]
  node [
    id 347
    label "Terranie"
  ]
  node [
    id 348
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 349
    label "category"
  ]
  node [
    id 350
    label "pakiet_klimatyczny"
  ]
  node [
    id 351
    label "oddzia&#322;"
  ]
  node [
    id 352
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 353
    label "cz&#261;steczka"
  ]
  node [
    id 354
    label "stage_set"
  ]
  node [
    id 355
    label "type"
  ]
  node [
    id 356
    label "specgrupa"
  ]
  node [
    id 357
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 358
    label "&#346;wietliki"
  ]
  node [
    id 359
    label "odm&#322;odzenie"
  ]
  node [
    id 360
    label "Eurogrupa"
  ]
  node [
    id 361
    label "odm&#322;adza&#263;"
  ]
  node [
    id 362
    label "harcerze_starsi"
  ]
  node [
    id 363
    label "Kosowo"
  ]
  node [
    id 364
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 365
    label "Zab&#322;ocie"
  ]
  node [
    id 366
    label "Pow&#261;zki"
  ]
  node [
    id 367
    label "Piotrowo"
  ]
  node [
    id 368
    label "Olszanica"
  ]
  node [
    id 369
    label "Ruda_Pabianicka"
  ]
  node [
    id 370
    label "holarktyka"
  ]
  node [
    id 371
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 372
    label "Ludwin&#243;w"
  ]
  node [
    id 373
    label "Arktyka"
  ]
  node [
    id 374
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 375
    label "Zabu&#380;e"
  ]
  node [
    id 376
    label "miejsce"
  ]
  node [
    id 377
    label "antroposfera"
  ]
  node [
    id 378
    label "Neogea"
  ]
  node [
    id 379
    label "terytorium"
  ]
  node [
    id 380
    label "Syberia_Zachodnia"
  ]
  node [
    id 381
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 382
    label "zakres"
  ]
  node [
    id 383
    label "pas_planetoid"
  ]
  node [
    id 384
    label "Syberia_Wschodnia"
  ]
  node [
    id 385
    label "Antarktyka"
  ]
  node [
    id 386
    label "Rakowice"
  ]
  node [
    id 387
    label "akrecja"
  ]
  node [
    id 388
    label "wymiar"
  ]
  node [
    id 389
    label "&#321;&#281;g"
  ]
  node [
    id 390
    label "Kresy_Zachodnie"
  ]
  node [
    id 391
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 392
    label "Notogea"
  ]
  node [
    id 393
    label "integer"
  ]
  node [
    id 394
    label "liczba"
  ]
  node [
    id 395
    label "zlewanie_si&#281;"
  ]
  node [
    id 396
    label "ilo&#347;&#263;"
  ]
  node [
    id 397
    label "uk&#322;ad"
  ]
  node [
    id 398
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 399
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 400
    label "pe&#322;ny"
  ]
  node [
    id 401
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 402
    label "proces"
  ]
  node [
    id 403
    label "boski"
  ]
  node [
    id 404
    label "krajobraz"
  ]
  node [
    id 405
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 406
    label "przywidzenie"
  ]
  node [
    id 407
    label "presence"
  ]
  node [
    id 408
    label "charakter"
  ]
  node [
    id 409
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 410
    label "rozdzielanie"
  ]
  node [
    id 411
    label "bezbrze&#380;e"
  ]
  node [
    id 412
    label "punkt"
  ]
  node [
    id 413
    label "niezmierzony"
  ]
  node [
    id 414
    label "przedzielenie"
  ]
  node [
    id 415
    label "nielito&#347;ciwy"
  ]
  node [
    id 416
    label "rozdziela&#263;"
  ]
  node [
    id 417
    label "oktant"
  ]
  node [
    id 418
    label "przedzieli&#263;"
  ]
  node [
    id 419
    label "przestw&#243;r"
  ]
  node [
    id 420
    label "&#347;rodowisko"
  ]
  node [
    id 421
    label "rura"
  ]
  node [
    id 422
    label "grzebiuszka"
  ]
  node [
    id 423
    label "odbicie"
  ]
  node [
    id 424
    label "atom"
  ]
  node [
    id 425
    label "kosmos"
  ]
  node [
    id 426
    label "miniatura"
  ]
  node [
    id 427
    label "smok_wawelski"
  ]
  node [
    id 428
    label "niecz&#322;owiek"
  ]
  node [
    id 429
    label "monster"
  ]
  node [
    id 430
    label "istota_&#380;ywa"
  ]
  node [
    id 431
    label "potw&#243;r"
  ]
  node [
    id 432
    label "istota_fantastyczna"
  ]
  node [
    id 433
    label "kultura"
  ]
  node [
    id 434
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 435
    label "ciep&#322;o"
  ]
  node [
    id 436
    label "energia_termiczna"
  ]
  node [
    id 437
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 438
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 439
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 440
    label "aspekt"
  ]
  node [
    id 441
    label "troposfera"
  ]
  node [
    id 442
    label "klimat"
  ]
  node [
    id 443
    label "metasfera"
  ]
  node [
    id 444
    label "atmosferyki"
  ]
  node [
    id 445
    label "homosfera"
  ]
  node [
    id 446
    label "cecha"
  ]
  node [
    id 447
    label "powietrznia"
  ]
  node [
    id 448
    label "jonosfera"
  ]
  node [
    id 449
    label "termosfera"
  ]
  node [
    id 450
    label "egzosfera"
  ]
  node [
    id 451
    label "heterosfera"
  ]
  node [
    id 452
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 453
    label "tropopauza"
  ]
  node [
    id 454
    label "kwas"
  ]
  node [
    id 455
    label "powietrze"
  ]
  node [
    id 456
    label "stratosfera"
  ]
  node [
    id 457
    label "pow&#322;oka"
  ]
  node [
    id 458
    label "mezosfera"
  ]
  node [
    id 459
    label "mezopauza"
  ]
  node [
    id 460
    label "atmosphere"
  ]
  node [
    id 461
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 462
    label "sferoida"
  ]
  node [
    id 463
    label "object"
  ]
  node [
    id 464
    label "temat"
  ]
  node [
    id 465
    label "wpadni&#281;cie"
  ]
  node [
    id 466
    label "mienie"
  ]
  node [
    id 467
    label "istota"
  ]
  node [
    id 468
    label "obiekt"
  ]
  node [
    id 469
    label "wpa&#347;&#263;"
  ]
  node [
    id 470
    label "wpadanie"
  ]
  node [
    id 471
    label "wpada&#263;"
  ]
  node [
    id 472
    label "treat"
  ]
  node [
    id 473
    label "czerpa&#263;"
  ]
  node [
    id 474
    label "bra&#263;"
  ]
  node [
    id 475
    label "handle"
  ]
  node [
    id 476
    label "wzbudza&#263;"
  ]
  node [
    id 477
    label "ogarnia&#263;"
  ]
  node [
    id 478
    label "bang"
  ]
  node [
    id 479
    label "wzi&#261;&#263;"
  ]
  node [
    id 480
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 481
    label "stimulate"
  ]
  node [
    id 482
    label "ogarn&#261;&#263;"
  ]
  node [
    id 483
    label "wzbudzi&#263;"
  ]
  node [
    id 484
    label "thrill"
  ]
  node [
    id 485
    label "czerpanie"
  ]
  node [
    id 486
    label "acquisition"
  ]
  node [
    id 487
    label "branie"
  ]
  node [
    id 488
    label "caparison"
  ]
  node [
    id 489
    label "movement"
  ]
  node [
    id 490
    label "wzbudzanie"
  ]
  node [
    id 491
    label "czynno&#347;&#263;"
  ]
  node [
    id 492
    label "ogarnianie"
  ]
  node [
    id 493
    label "wra&#380;enie"
  ]
  node [
    id 494
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 495
    label "interception"
  ]
  node [
    id 496
    label "wzbudzenie"
  ]
  node [
    id 497
    label "emotion"
  ]
  node [
    id 498
    label "zaczerpni&#281;cie"
  ]
  node [
    id 499
    label "wzi&#281;cie"
  ]
  node [
    id 500
    label "zboczenie"
  ]
  node [
    id 501
    label "om&#243;wienie"
  ]
  node [
    id 502
    label "sponiewieranie"
  ]
  node [
    id 503
    label "discipline"
  ]
  node [
    id 504
    label "omawia&#263;"
  ]
  node [
    id 505
    label "kr&#261;&#380;enie"
  ]
  node [
    id 506
    label "tre&#347;&#263;"
  ]
  node [
    id 507
    label "robienie"
  ]
  node [
    id 508
    label "sponiewiera&#263;"
  ]
  node [
    id 509
    label "element"
  ]
  node [
    id 510
    label "entity"
  ]
  node [
    id 511
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 512
    label "tematyka"
  ]
  node [
    id 513
    label "w&#261;tek"
  ]
  node [
    id 514
    label "zbaczanie"
  ]
  node [
    id 515
    label "program_nauczania"
  ]
  node [
    id 516
    label "om&#243;wi&#263;"
  ]
  node [
    id 517
    label "omawianie"
  ]
  node [
    id 518
    label "thing"
  ]
  node [
    id 519
    label "zbacza&#263;"
  ]
  node [
    id 520
    label "zboczy&#263;"
  ]
  node [
    id 521
    label "performance"
  ]
  node [
    id 522
    label "sztuka"
  ]
  node [
    id 523
    label "Boreasz"
  ]
  node [
    id 524
    label "p&#243;&#322;nocek"
  ]
  node [
    id 525
    label "granica_pa&#324;stwa"
  ]
  node [
    id 526
    label "warstwa"
  ]
  node [
    id 527
    label "kriosfera"
  ]
  node [
    id 528
    label "lej_polarny"
  ]
  node [
    id 529
    label "sfera"
  ]
  node [
    id 530
    label "brzeg"
  ]
  node [
    id 531
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 532
    label "p&#322;oza"
  ]
  node [
    id 533
    label "zawiasy"
  ]
  node [
    id 534
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 535
    label "element_anatomiczny"
  ]
  node [
    id 536
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 537
    label "reda"
  ]
  node [
    id 538
    label "zbiornik_wodny"
  ]
  node [
    id 539
    label "przymorze"
  ]
  node [
    id 540
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 541
    label "bezmiar"
  ]
  node [
    id 542
    label "pe&#322;ne_morze"
  ]
  node [
    id 543
    label "latarnia_morska"
  ]
  node [
    id 544
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 545
    label "nereida"
  ]
  node [
    id 546
    label "okeanida"
  ]
  node [
    id 547
    label "marina"
  ]
  node [
    id 548
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 549
    label "Morze_Czerwone"
  ]
  node [
    id 550
    label "talasoterapia"
  ]
  node [
    id 551
    label "Morze_Bia&#322;e"
  ]
  node [
    id 552
    label "paliszcze"
  ]
  node [
    id 553
    label "Neptun"
  ]
  node [
    id 554
    label "Morze_Czarne"
  ]
  node [
    id 555
    label "laguna"
  ]
  node [
    id 556
    label "Morze_Egejskie"
  ]
  node [
    id 557
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 558
    label "Morze_Adriatyckie"
  ]
  node [
    id 559
    label "rze&#378;biarstwo"
  ]
  node [
    id 560
    label "planacja"
  ]
  node [
    id 561
    label "relief"
  ]
  node [
    id 562
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 563
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 564
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 565
    label "bozzetto"
  ]
  node [
    id 566
    label "plastyka"
  ]
  node [
    id 567
    label "j&#261;dro"
  ]
  node [
    id 568
    label "ozon"
  ]
  node [
    id 569
    label "gleba"
  ]
  node [
    id 570
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 571
    label "sialma"
  ]
  node [
    id 572
    label "skorupa_ziemska"
  ]
  node [
    id 573
    label "warstwa_perydotytowa"
  ]
  node [
    id 574
    label "warstwa_granitowa"
  ]
  node [
    id 575
    label "kula"
  ]
  node [
    id 576
    label "kresom&#243;zgowie"
  ]
  node [
    id 577
    label "przyra"
  ]
  node [
    id 578
    label "biom"
  ]
  node [
    id 579
    label "awifauna"
  ]
  node [
    id 580
    label "ichtiofauna"
  ]
  node [
    id 581
    label "geosystem"
  ]
  node [
    id 582
    label "dotleni&#263;"
  ]
  node [
    id 583
    label "spi&#281;trza&#263;"
  ]
  node [
    id 584
    label "spi&#281;trzenie"
  ]
  node [
    id 585
    label "utylizator"
  ]
  node [
    id 586
    label "p&#322;ycizna"
  ]
  node [
    id 587
    label "nabranie"
  ]
  node [
    id 588
    label "Waruna"
  ]
  node [
    id 589
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 590
    label "przybieranie"
  ]
  node [
    id 591
    label "uci&#261;g"
  ]
  node [
    id 592
    label "bombast"
  ]
  node [
    id 593
    label "fala"
  ]
  node [
    id 594
    label "kryptodepresja"
  ]
  node [
    id 595
    label "water"
  ]
  node [
    id 596
    label "wysi&#281;k"
  ]
  node [
    id 597
    label "pustka"
  ]
  node [
    id 598
    label "ciecz"
  ]
  node [
    id 599
    label "przybrze&#380;e"
  ]
  node [
    id 600
    label "nap&#243;j"
  ]
  node [
    id 601
    label "spi&#281;trzanie"
  ]
  node [
    id 602
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 603
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 604
    label "bicie"
  ]
  node [
    id 605
    label "klarownik"
  ]
  node [
    id 606
    label "chlastanie"
  ]
  node [
    id 607
    label "woda_s&#322;odka"
  ]
  node [
    id 608
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 609
    label "nabra&#263;"
  ]
  node [
    id 610
    label "chlasta&#263;"
  ]
  node [
    id 611
    label "uj&#281;cie_wody"
  ]
  node [
    id 612
    label "zrzut"
  ]
  node [
    id 613
    label "wypowied&#378;"
  ]
  node [
    id 614
    label "wodnik"
  ]
  node [
    id 615
    label "pojazd"
  ]
  node [
    id 616
    label "l&#243;d"
  ]
  node [
    id 617
    label "wybrze&#380;e"
  ]
  node [
    id 618
    label "deklamacja"
  ]
  node [
    id 619
    label "tlenek"
  ]
  node [
    id 620
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 621
    label "biotop"
  ]
  node [
    id 622
    label "biocenoza"
  ]
  node [
    id 623
    label "kontekst"
  ]
  node [
    id 624
    label "miejsce_pracy"
  ]
  node [
    id 625
    label "nation"
  ]
  node [
    id 626
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 627
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 628
    label "w&#322;adza"
  ]
  node [
    id 629
    label "szata_ro&#347;linna"
  ]
  node [
    id 630
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 631
    label "formacja_ro&#347;linna"
  ]
  node [
    id 632
    label "zielono&#347;&#263;"
  ]
  node [
    id 633
    label "pi&#281;tro"
  ]
  node [
    id 634
    label "plant"
  ]
  node [
    id 635
    label "ro&#347;lina"
  ]
  node [
    id 636
    label "iglak"
  ]
  node [
    id 637
    label "cyprysowate"
  ]
  node [
    id 638
    label "zaj&#281;cie"
  ]
  node [
    id 639
    label "instytucja"
  ]
  node [
    id 640
    label "tajniki"
  ]
  node [
    id 641
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 642
    label "jedzenie"
  ]
  node [
    id 643
    label "zaplecze"
  ]
  node [
    id 644
    label "pomieszczenie"
  ]
  node [
    id 645
    label "zlewozmywak"
  ]
  node [
    id 646
    label "gotowa&#263;"
  ]
  node [
    id 647
    label "strefa"
  ]
  node [
    id 648
    label "Jowisz"
  ]
  node [
    id 649
    label "syzygia"
  ]
  node [
    id 650
    label "Saturn"
  ]
  node [
    id 651
    label "Uran"
  ]
  node [
    id 652
    label "message"
  ]
  node [
    id 653
    label "dar"
  ]
  node [
    id 654
    label "real"
  ]
  node [
    id 655
    label "Ukraina"
  ]
  node [
    id 656
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 657
    label "blok_wschodni"
  ]
  node [
    id 658
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 659
    label "Europa_Wschodnia"
  ]
  node [
    id 660
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 661
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 662
    label "genetyczny"
  ]
  node [
    id 663
    label "dziedzicznie"
  ]
  node [
    id 664
    label "dziedziczny"
  ]
  node [
    id 665
    label "podobnie"
  ]
  node [
    id 666
    label "powtarzalnie"
  ]
  node [
    id 667
    label "zmieni&#263;"
  ]
  node [
    id 668
    label "sprawi&#263;"
  ]
  node [
    id 669
    label "change"
  ]
  node [
    id 670
    label "zrobi&#263;"
  ]
  node [
    id 671
    label "zast&#261;pi&#263;"
  ]
  node [
    id 672
    label "come_up"
  ]
  node [
    id 673
    label "przej&#347;&#263;"
  ]
  node [
    id 674
    label "straci&#263;"
  ]
  node [
    id 675
    label "zyska&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
]
