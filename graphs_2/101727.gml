graph [
  node [
    id 0
    label "czynno&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kontrolny"
    origin "text"
  ]
  node [
    id 2
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przy"
    origin "text"
  ]
  node [
    id 5
    label "wsp&#243;&#322;udzia&#322;"
    origin "text"
  ]
  node [
    id 6
    label "kierownik"
    origin "text"
  ]
  node [
    id 7
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 8
    label "opieka"
    origin "text"
  ]
  node [
    id 9
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 10
    label "lub"
    origin "text"
  ]
  node [
    id 11
    label "osoba"
    origin "text"
  ]
  node [
    id 12
    label "przez"
    origin "text"
  ]
  node [
    id 13
    label "upowa&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "activity"
  ]
  node [
    id 16
    label "bezproblemowy"
  ]
  node [
    id 17
    label "wydarzenie"
  ]
  node [
    id 18
    label "przebiec"
  ]
  node [
    id 19
    label "charakter"
  ]
  node [
    id 20
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 21
    label "motyw"
  ]
  node [
    id 22
    label "przebiegni&#281;cie"
  ]
  node [
    id 23
    label "fabu&#322;a"
  ]
  node [
    id 24
    label "udany"
  ]
  node [
    id 25
    label "bezproblemowo"
  ]
  node [
    id 26
    label "wolny"
  ]
  node [
    id 27
    label "supervision"
  ]
  node [
    id 28
    label "kontrolnie"
  ]
  node [
    id 29
    label "pr&#243;bny"
  ]
  node [
    id 30
    label "pr&#243;bnie"
  ]
  node [
    id 31
    label "make"
  ]
  node [
    id 32
    label "robi&#263;"
  ]
  node [
    id 33
    label "determine"
  ]
  node [
    id 34
    label "przestawa&#263;"
  ]
  node [
    id 35
    label "organizowa&#263;"
  ]
  node [
    id 36
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 37
    label "czyni&#263;"
  ]
  node [
    id 38
    label "give"
  ]
  node [
    id 39
    label "stylizowa&#263;"
  ]
  node [
    id 40
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 41
    label "falowa&#263;"
  ]
  node [
    id 42
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 43
    label "peddle"
  ]
  node [
    id 44
    label "praca"
  ]
  node [
    id 45
    label "wydala&#263;"
  ]
  node [
    id 46
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 47
    label "tentegowa&#263;"
  ]
  node [
    id 48
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 49
    label "urz&#261;dza&#263;"
  ]
  node [
    id 50
    label "oszukiwa&#263;"
  ]
  node [
    id 51
    label "work"
  ]
  node [
    id 52
    label "ukazywa&#263;"
  ]
  node [
    id 53
    label "przerabia&#263;"
  ]
  node [
    id 54
    label "act"
  ]
  node [
    id 55
    label "post&#281;powa&#263;"
  ]
  node [
    id 56
    label "&#380;y&#263;"
  ]
  node [
    id 57
    label "coating"
  ]
  node [
    id 58
    label "przebywa&#263;"
  ]
  node [
    id 59
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 60
    label "ko&#324;czy&#263;"
  ]
  node [
    id 61
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 62
    label "finish_up"
  ]
  node [
    id 63
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 64
    label "mie&#263;_miejsce"
  ]
  node [
    id 65
    label "equal"
  ]
  node [
    id 66
    label "trwa&#263;"
  ]
  node [
    id 67
    label "chodzi&#263;"
  ]
  node [
    id 68
    label "si&#281;ga&#263;"
  ]
  node [
    id 69
    label "stan"
  ]
  node [
    id 70
    label "obecno&#347;&#263;"
  ]
  node [
    id 71
    label "stand"
  ]
  node [
    id 72
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 73
    label "uczestniczy&#263;"
  ]
  node [
    id 74
    label "participate"
  ]
  node [
    id 75
    label "istnie&#263;"
  ]
  node [
    id 76
    label "pozostawa&#263;"
  ]
  node [
    id 77
    label "zostawa&#263;"
  ]
  node [
    id 78
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 79
    label "adhere"
  ]
  node [
    id 80
    label "compass"
  ]
  node [
    id 81
    label "korzysta&#263;"
  ]
  node [
    id 82
    label "appreciation"
  ]
  node [
    id 83
    label "osi&#261;ga&#263;"
  ]
  node [
    id 84
    label "dociera&#263;"
  ]
  node [
    id 85
    label "get"
  ]
  node [
    id 86
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 87
    label "mierzy&#263;"
  ]
  node [
    id 88
    label "u&#380;ywa&#263;"
  ]
  node [
    id 89
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 90
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 91
    label "exsert"
  ]
  node [
    id 92
    label "being"
  ]
  node [
    id 93
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 94
    label "cecha"
  ]
  node [
    id 95
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 96
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 97
    label "p&#322;ywa&#263;"
  ]
  node [
    id 98
    label "run"
  ]
  node [
    id 99
    label "bangla&#263;"
  ]
  node [
    id 100
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 101
    label "przebiega&#263;"
  ]
  node [
    id 102
    label "wk&#322;ada&#263;"
  ]
  node [
    id 103
    label "proceed"
  ]
  node [
    id 104
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 105
    label "carry"
  ]
  node [
    id 106
    label "bywa&#263;"
  ]
  node [
    id 107
    label "dziama&#263;"
  ]
  node [
    id 108
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 109
    label "stara&#263;_si&#281;"
  ]
  node [
    id 110
    label "para"
  ]
  node [
    id 111
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 112
    label "str&#243;j"
  ]
  node [
    id 113
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 114
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 115
    label "krok"
  ]
  node [
    id 116
    label "tryb"
  ]
  node [
    id 117
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 118
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 119
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 120
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 121
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 122
    label "continue"
  ]
  node [
    id 123
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 124
    label "Ohio"
  ]
  node [
    id 125
    label "wci&#281;cie"
  ]
  node [
    id 126
    label "Nowy_York"
  ]
  node [
    id 127
    label "warstwa"
  ]
  node [
    id 128
    label "samopoczucie"
  ]
  node [
    id 129
    label "Illinois"
  ]
  node [
    id 130
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 131
    label "state"
  ]
  node [
    id 132
    label "Jukatan"
  ]
  node [
    id 133
    label "Kalifornia"
  ]
  node [
    id 134
    label "Wirginia"
  ]
  node [
    id 135
    label "wektor"
  ]
  node [
    id 136
    label "Teksas"
  ]
  node [
    id 137
    label "Goa"
  ]
  node [
    id 138
    label "Waszyngton"
  ]
  node [
    id 139
    label "miejsce"
  ]
  node [
    id 140
    label "Massachusetts"
  ]
  node [
    id 141
    label "Alaska"
  ]
  node [
    id 142
    label "Arakan"
  ]
  node [
    id 143
    label "Hawaje"
  ]
  node [
    id 144
    label "Maryland"
  ]
  node [
    id 145
    label "punkt"
  ]
  node [
    id 146
    label "Michigan"
  ]
  node [
    id 147
    label "Arizona"
  ]
  node [
    id 148
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 149
    label "Georgia"
  ]
  node [
    id 150
    label "poziom"
  ]
  node [
    id 151
    label "Pensylwania"
  ]
  node [
    id 152
    label "shape"
  ]
  node [
    id 153
    label "Luizjana"
  ]
  node [
    id 154
    label "Nowy_Meksyk"
  ]
  node [
    id 155
    label "Alabama"
  ]
  node [
    id 156
    label "ilo&#347;&#263;"
  ]
  node [
    id 157
    label "Kansas"
  ]
  node [
    id 158
    label "Oregon"
  ]
  node [
    id 159
    label "Floryda"
  ]
  node [
    id 160
    label "Oklahoma"
  ]
  node [
    id 161
    label "jednostka_administracyjna"
  ]
  node [
    id 162
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 163
    label "uczestnictwo"
  ]
  node [
    id 164
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 165
    label "zwierzchnik"
  ]
  node [
    id 166
    label "pryncypa&#322;"
  ]
  node [
    id 167
    label "kierowa&#263;"
  ]
  node [
    id 168
    label "kierownictwo"
  ]
  node [
    id 169
    label "cz&#322;owiek"
  ]
  node [
    id 170
    label "zak&#322;adka"
  ]
  node [
    id 171
    label "jednostka_organizacyjna"
  ]
  node [
    id 172
    label "miejsce_pracy"
  ]
  node [
    id 173
    label "instytucja"
  ]
  node [
    id 174
    label "wyko&#324;czenie"
  ]
  node [
    id 175
    label "firma"
  ]
  node [
    id 176
    label "czyn"
  ]
  node [
    id 177
    label "company"
  ]
  node [
    id 178
    label "instytut"
  ]
  node [
    id 179
    label "umowa"
  ]
  node [
    id 180
    label "bookmark"
  ]
  node [
    id 181
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 182
    label "fa&#322;da"
  ]
  node [
    id 183
    label "znacznik"
  ]
  node [
    id 184
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 185
    label "widok"
  ]
  node [
    id 186
    label "program"
  ]
  node [
    id 187
    label "z&#322;&#261;czenie"
  ]
  node [
    id 188
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 189
    label "strona"
  ]
  node [
    id 190
    label "zu&#380;ycie"
  ]
  node [
    id 191
    label "skonany"
  ]
  node [
    id 192
    label "zniszczenie"
  ]
  node [
    id 193
    label "os&#322;abienie"
  ]
  node [
    id 194
    label "wymordowanie"
  ]
  node [
    id 195
    label "murder"
  ]
  node [
    id 196
    label "pomordowanie"
  ]
  node [
    id 197
    label "znu&#380;enie"
  ]
  node [
    id 198
    label "zrobienie"
  ]
  node [
    id 199
    label "ukszta&#322;towanie"
  ]
  node [
    id 200
    label "zm&#281;czenie"
  ]
  node [
    id 201
    label "adjustment"
  ]
  node [
    id 202
    label "zabicie"
  ]
  node [
    id 203
    label "zawarcie"
  ]
  node [
    id 204
    label "zawrze&#263;"
  ]
  node [
    id 205
    label "warunek"
  ]
  node [
    id 206
    label "gestia_transportowa"
  ]
  node [
    id 207
    label "contract"
  ]
  node [
    id 208
    label "porozumienie"
  ]
  node [
    id 209
    label "klauzula"
  ]
  node [
    id 210
    label "funkcja"
  ]
  node [
    id 211
    label "Apeks"
  ]
  node [
    id 212
    label "zasoby"
  ]
  node [
    id 213
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 214
    label "zaufanie"
  ]
  node [
    id 215
    label "Hortex"
  ]
  node [
    id 216
    label "reengineering"
  ]
  node [
    id 217
    label "nazwa_w&#322;asna"
  ]
  node [
    id 218
    label "podmiot_gospodarczy"
  ]
  node [
    id 219
    label "paczkarnia"
  ]
  node [
    id 220
    label "Orlen"
  ]
  node [
    id 221
    label "interes"
  ]
  node [
    id 222
    label "Google"
  ]
  node [
    id 223
    label "Pewex"
  ]
  node [
    id 224
    label "Canon"
  ]
  node [
    id 225
    label "MAN_SE"
  ]
  node [
    id 226
    label "Spo&#322;em"
  ]
  node [
    id 227
    label "klasa"
  ]
  node [
    id 228
    label "networking"
  ]
  node [
    id 229
    label "MAC"
  ]
  node [
    id 230
    label "zasoby_ludzkie"
  ]
  node [
    id 231
    label "Baltona"
  ]
  node [
    id 232
    label "Orbis"
  ]
  node [
    id 233
    label "biurowiec"
  ]
  node [
    id 234
    label "HP"
  ]
  node [
    id 235
    label "siedziba"
  ]
  node [
    id 236
    label "osoba_prawna"
  ]
  node [
    id 237
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 238
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 239
    label "poj&#281;cie"
  ]
  node [
    id 240
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 241
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 242
    label "biuro"
  ]
  node [
    id 243
    label "organizacja"
  ]
  node [
    id 244
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 245
    label "Fundusze_Unijne"
  ]
  node [
    id 246
    label "zamyka&#263;"
  ]
  node [
    id 247
    label "establishment"
  ]
  node [
    id 248
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 249
    label "urz&#261;d"
  ]
  node [
    id 250
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 251
    label "afiliowa&#263;"
  ]
  node [
    id 252
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 253
    label "standard"
  ]
  node [
    id 254
    label "zamykanie"
  ]
  node [
    id 255
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 256
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 257
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 258
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 259
    label "Ossolineum"
  ]
  node [
    id 260
    label "plac&#243;wka"
  ]
  node [
    id 261
    label "institute"
  ]
  node [
    id 262
    label "pomoc"
  ]
  node [
    id 263
    label "opieku&#324;cze_skrzyd&#322;a"
  ]
  node [
    id 264
    label "staranie"
  ]
  node [
    id 265
    label "nadz&#243;r"
  ]
  node [
    id 266
    label "&#347;rodek"
  ]
  node [
    id 267
    label "darowizna"
  ]
  node [
    id 268
    label "przedmiot"
  ]
  node [
    id 269
    label "liga"
  ]
  node [
    id 270
    label "doch&#243;d"
  ]
  node [
    id 271
    label "telefon_zaufania"
  ]
  node [
    id 272
    label "pomocnik"
  ]
  node [
    id 273
    label "zgodzi&#263;"
  ]
  node [
    id 274
    label "grupa"
  ]
  node [
    id 275
    label "property"
  ]
  node [
    id 276
    label "examination"
  ]
  node [
    id 277
    label "usi&#322;owanie"
  ]
  node [
    id 278
    label "starunek"
  ]
  node [
    id 279
    label "zdrowy"
  ]
  node [
    id 280
    label "zdrowotnie"
  ]
  node [
    id 281
    label "dobry"
  ]
  node [
    id 282
    label "prozdrowotny"
  ]
  node [
    id 283
    label "dobrze"
  ]
  node [
    id 284
    label "zdrowo"
  ]
  node [
    id 285
    label "wyzdrowienie"
  ]
  node [
    id 286
    label "silny"
  ]
  node [
    id 287
    label "uzdrowienie"
  ]
  node [
    id 288
    label "wyleczenie_si&#281;"
  ]
  node [
    id 289
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 290
    label "normalny"
  ]
  node [
    id 291
    label "rozs&#261;dny"
  ]
  node [
    id 292
    label "korzystny"
  ]
  node [
    id 293
    label "zdrowienie"
  ]
  node [
    id 294
    label "solidny"
  ]
  node [
    id 295
    label "uzdrawianie"
  ]
  node [
    id 296
    label "dobroczynny"
  ]
  node [
    id 297
    label "czw&#243;rka"
  ]
  node [
    id 298
    label "spokojny"
  ]
  node [
    id 299
    label "skuteczny"
  ]
  node [
    id 300
    label "&#347;mieszny"
  ]
  node [
    id 301
    label "mi&#322;y"
  ]
  node [
    id 302
    label "grzeczny"
  ]
  node [
    id 303
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 304
    label "powitanie"
  ]
  node [
    id 305
    label "ca&#322;y"
  ]
  node [
    id 306
    label "zwrot"
  ]
  node [
    id 307
    label "pomy&#347;lny"
  ]
  node [
    id 308
    label "moralny"
  ]
  node [
    id 309
    label "drogi"
  ]
  node [
    id 310
    label "pozytywny"
  ]
  node [
    id 311
    label "odpowiedni"
  ]
  node [
    id 312
    label "pos&#322;uszny"
  ]
  node [
    id 313
    label "Chocho&#322;"
  ]
  node [
    id 314
    label "Herkules_Poirot"
  ]
  node [
    id 315
    label "Edyp"
  ]
  node [
    id 316
    label "ludzko&#347;&#263;"
  ]
  node [
    id 317
    label "parali&#380;owa&#263;"
  ]
  node [
    id 318
    label "Harry_Potter"
  ]
  node [
    id 319
    label "Casanova"
  ]
  node [
    id 320
    label "Zgredek"
  ]
  node [
    id 321
    label "Gargantua"
  ]
  node [
    id 322
    label "Winnetou"
  ]
  node [
    id 323
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 324
    label "posta&#263;"
  ]
  node [
    id 325
    label "Dulcynea"
  ]
  node [
    id 326
    label "kategoria_gramatyczna"
  ]
  node [
    id 327
    label "g&#322;owa"
  ]
  node [
    id 328
    label "figura"
  ]
  node [
    id 329
    label "portrecista"
  ]
  node [
    id 330
    label "person"
  ]
  node [
    id 331
    label "Plastu&#347;"
  ]
  node [
    id 332
    label "Quasimodo"
  ]
  node [
    id 333
    label "Sherlock_Holmes"
  ]
  node [
    id 334
    label "Faust"
  ]
  node [
    id 335
    label "Wallenrod"
  ]
  node [
    id 336
    label "Dwukwiat"
  ]
  node [
    id 337
    label "Don_Juan"
  ]
  node [
    id 338
    label "profanum"
  ]
  node [
    id 339
    label "koniugacja"
  ]
  node [
    id 340
    label "Don_Kiszot"
  ]
  node [
    id 341
    label "mikrokosmos"
  ]
  node [
    id 342
    label "duch"
  ]
  node [
    id 343
    label "antropochoria"
  ]
  node [
    id 344
    label "oddzia&#322;ywanie"
  ]
  node [
    id 345
    label "Hamlet"
  ]
  node [
    id 346
    label "Werter"
  ]
  node [
    id 347
    label "istota"
  ]
  node [
    id 348
    label "Szwejk"
  ]
  node [
    id 349
    label "homo_sapiens"
  ]
  node [
    id 350
    label "mentalno&#347;&#263;"
  ]
  node [
    id 351
    label "superego"
  ]
  node [
    id 352
    label "psychika"
  ]
  node [
    id 353
    label "znaczenie"
  ]
  node [
    id 354
    label "wn&#281;trze"
  ]
  node [
    id 355
    label "charakterystyka"
  ]
  node [
    id 356
    label "zaistnie&#263;"
  ]
  node [
    id 357
    label "Osjan"
  ]
  node [
    id 358
    label "kto&#347;"
  ]
  node [
    id 359
    label "wygl&#261;d"
  ]
  node [
    id 360
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 361
    label "osobowo&#347;&#263;"
  ]
  node [
    id 362
    label "wytw&#243;r"
  ]
  node [
    id 363
    label "trim"
  ]
  node [
    id 364
    label "poby&#263;"
  ]
  node [
    id 365
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 366
    label "Aspazja"
  ]
  node [
    id 367
    label "punkt_widzenia"
  ]
  node [
    id 368
    label "kompleksja"
  ]
  node [
    id 369
    label "wytrzyma&#263;"
  ]
  node [
    id 370
    label "budowa"
  ]
  node [
    id 371
    label "formacja"
  ]
  node [
    id 372
    label "pozosta&#263;"
  ]
  node [
    id 373
    label "point"
  ]
  node [
    id 374
    label "przedstawienie"
  ]
  node [
    id 375
    label "go&#347;&#263;"
  ]
  node [
    id 376
    label "hamper"
  ]
  node [
    id 377
    label "spasm"
  ]
  node [
    id 378
    label "mrozi&#263;"
  ]
  node [
    id 379
    label "pora&#380;a&#263;"
  ]
  node [
    id 380
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 381
    label "fleksja"
  ]
  node [
    id 382
    label "liczba"
  ]
  node [
    id 383
    label "coupling"
  ]
  node [
    id 384
    label "czas"
  ]
  node [
    id 385
    label "czasownik"
  ]
  node [
    id 386
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 387
    label "orz&#281;sek"
  ]
  node [
    id 388
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 389
    label "kszta&#322;t"
  ]
  node [
    id 390
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 391
    label "wiedza"
  ]
  node [
    id 392
    label "alkohol"
  ]
  node [
    id 393
    label "zdolno&#347;&#263;"
  ]
  node [
    id 394
    label "&#380;ycie"
  ]
  node [
    id 395
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 396
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 397
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 398
    label "sztuka"
  ]
  node [
    id 399
    label "dekiel"
  ]
  node [
    id 400
    label "ro&#347;lina"
  ]
  node [
    id 401
    label "&#347;ci&#281;cie"
  ]
  node [
    id 402
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 403
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 404
    label "&#347;ci&#281;gno"
  ]
  node [
    id 405
    label "noosfera"
  ]
  node [
    id 406
    label "byd&#322;o"
  ]
  node [
    id 407
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 408
    label "makrocefalia"
  ]
  node [
    id 409
    label "obiekt"
  ]
  node [
    id 410
    label "ucho"
  ]
  node [
    id 411
    label "g&#243;ra"
  ]
  node [
    id 412
    label "m&#243;zg"
  ]
  node [
    id 413
    label "fryzura"
  ]
  node [
    id 414
    label "umys&#322;"
  ]
  node [
    id 415
    label "cia&#322;o"
  ]
  node [
    id 416
    label "cz&#322;onek"
  ]
  node [
    id 417
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 418
    label "czaszka"
  ]
  node [
    id 419
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 420
    label "dziedzina"
  ]
  node [
    id 421
    label "powodowanie"
  ]
  node [
    id 422
    label "hipnotyzowanie"
  ]
  node [
    id 423
    label "&#347;lad"
  ]
  node [
    id 424
    label "docieranie"
  ]
  node [
    id 425
    label "natural_process"
  ]
  node [
    id 426
    label "reakcja_chemiczna"
  ]
  node [
    id 427
    label "wdzieranie_si&#281;"
  ]
  node [
    id 428
    label "zjawisko"
  ]
  node [
    id 429
    label "rezultat"
  ]
  node [
    id 430
    label "lobbysta"
  ]
  node [
    id 431
    label "allochoria"
  ]
  node [
    id 432
    label "fotograf"
  ]
  node [
    id 433
    label "malarz"
  ]
  node [
    id 434
    label "artysta"
  ]
  node [
    id 435
    label "p&#322;aszczyzna"
  ]
  node [
    id 436
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 437
    label "bierka_szachowa"
  ]
  node [
    id 438
    label "obiekt_matematyczny"
  ]
  node [
    id 439
    label "gestaltyzm"
  ]
  node [
    id 440
    label "styl"
  ]
  node [
    id 441
    label "obraz"
  ]
  node [
    id 442
    label "rzecz"
  ]
  node [
    id 443
    label "d&#378;wi&#281;k"
  ]
  node [
    id 444
    label "character"
  ]
  node [
    id 445
    label "rze&#378;ba"
  ]
  node [
    id 446
    label "stylistyka"
  ]
  node [
    id 447
    label "figure"
  ]
  node [
    id 448
    label "antycypacja"
  ]
  node [
    id 449
    label "ornamentyka"
  ]
  node [
    id 450
    label "informacja"
  ]
  node [
    id 451
    label "facet"
  ]
  node [
    id 452
    label "popis"
  ]
  node [
    id 453
    label "wiersz"
  ]
  node [
    id 454
    label "symetria"
  ]
  node [
    id 455
    label "lingwistyka_kognitywna"
  ]
  node [
    id 456
    label "karta"
  ]
  node [
    id 457
    label "podzbi&#243;r"
  ]
  node [
    id 458
    label "perspektywa"
  ]
  node [
    id 459
    label "Szekspir"
  ]
  node [
    id 460
    label "Mickiewicz"
  ]
  node [
    id 461
    label "cierpienie"
  ]
  node [
    id 462
    label "piek&#322;o"
  ]
  node [
    id 463
    label "human_body"
  ]
  node [
    id 464
    label "ofiarowywanie"
  ]
  node [
    id 465
    label "sfera_afektywna"
  ]
  node [
    id 466
    label "nekromancja"
  ]
  node [
    id 467
    label "Po&#347;wist"
  ]
  node [
    id 468
    label "podekscytowanie"
  ]
  node [
    id 469
    label "deformowanie"
  ]
  node [
    id 470
    label "sumienie"
  ]
  node [
    id 471
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 472
    label "deformowa&#263;"
  ]
  node [
    id 473
    label "zjawa"
  ]
  node [
    id 474
    label "zmar&#322;y"
  ]
  node [
    id 475
    label "istota_nadprzyrodzona"
  ]
  node [
    id 476
    label "power"
  ]
  node [
    id 477
    label "entity"
  ]
  node [
    id 478
    label "ofiarowywa&#263;"
  ]
  node [
    id 479
    label "oddech"
  ]
  node [
    id 480
    label "seksualno&#347;&#263;"
  ]
  node [
    id 481
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 482
    label "byt"
  ]
  node [
    id 483
    label "si&#322;a"
  ]
  node [
    id 484
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 485
    label "ego"
  ]
  node [
    id 486
    label "ofiarowanie"
  ]
  node [
    id 487
    label "fizjonomia"
  ]
  node [
    id 488
    label "kompleks"
  ]
  node [
    id 489
    label "zapalno&#347;&#263;"
  ]
  node [
    id 490
    label "T&#281;sknica"
  ]
  node [
    id 491
    label "ofiarowa&#263;"
  ]
  node [
    id 492
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 493
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 494
    label "passion"
  ]
  node [
    id 495
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 496
    label "odbicie"
  ]
  node [
    id 497
    label "atom"
  ]
  node [
    id 498
    label "przyroda"
  ]
  node [
    id 499
    label "Ziemia"
  ]
  node [
    id 500
    label "kosmos"
  ]
  node [
    id 501
    label "miniatura"
  ]
  node [
    id 502
    label "spowodowa&#263;"
  ]
  node [
    id 503
    label "authorize"
  ]
  node [
    id 504
    label "zdarzy&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 54
  ]
]
