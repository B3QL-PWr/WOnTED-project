graph [
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "wydanie"
    origin "text"
  ]
  node [
    id 2
    label "internetowy"
    origin "text"
  ]
  node [
    id 3
    label "pismo"
    origin "text"
  ]
  node [
    id 4
    label "naukowy"
    origin "text"
  ]
  node [
    id 5
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ludologii"
    origin "text"
  ]
  node [
    id 8
    label "czyli"
    origin "text"
  ]
  node [
    id 9
    label "badanie"
    origin "text"
  ]
  node [
    id 10
    label "gra"
    origin "text"
  ]
  node [
    id 11
    label "gwiazda"
  ]
  node [
    id 12
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 13
    label "Arktur"
  ]
  node [
    id 14
    label "kszta&#322;t"
  ]
  node [
    id 15
    label "Gwiazda_Polarna"
  ]
  node [
    id 16
    label "agregatka"
  ]
  node [
    id 17
    label "gromada"
  ]
  node [
    id 18
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 19
    label "S&#322;o&#324;ce"
  ]
  node [
    id 20
    label "Nibiru"
  ]
  node [
    id 21
    label "konstelacja"
  ]
  node [
    id 22
    label "ornament"
  ]
  node [
    id 23
    label "delta_Scuti"
  ]
  node [
    id 24
    label "&#347;wiat&#322;o"
  ]
  node [
    id 25
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 26
    label "obiekt"
  ]
  node [
    id 27
    label "s&#322;awa"
  ]
  node [
    id 28
    label "promie&#324;"
  ]
  node [
    id 29
    label "star"
  ]
  node [
    id 30
    label "gwiazdosz"
  ]
  node [
    id 31
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 32
    label "asocjacja_gwiazd"
  ]
  node [
    id 33
    label "supergrupa"
  ]
  node [
    id 34
    label "delivery"
  ]
  node [
    id 35
    label "zdarzenie_si&#281;"
  ]
  node [
    id 36
    label "rendition"
  ]
  node [
    id 37
    label "d&#378;wi&#281;k"
  ]
  node [
    id 38
    label "egzemplarz"
  ]
  node [
    id 39
    label "impression"
  ]
  node [
    id 40
    label "publikacja"
  ]
  node [
    id 41
    label "zadenuncjowanie"
  ]
  node [
    id 42
    label "zapach"
  ]
  node [
    id 43
    label "reszta"
  ]
  node [
    id 44
    label "wytworzenie"
  ]
  node [
    id 45
    label "issue"
  ]
  node [
    id 46
    label "danie"
  ]
  node [
    id 47
    label "czasopismo"
  ]
  node [
    id 48
    label "podanie"
  ]
  node [
    id 49
    label "wprowadzenie"
  ]
  node [
    id 50
    label "odmiana"
  ]
  node [
    id 51
    label "ujawnienie"
  ]
  node [
    id 52
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 53
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 54
    label "urz&#261;dzenie"
  ]
  node [
    id 55
    label "zrobienie"
  ]
  node [
    id 56
    label "mutant"
  ]
  node [
    id 57
    label "rewizja"
  ]
  node [
    id 58
    label "gramatyka"
  ]
  node [
    id 59
    label "typ"
  ]
  node [
    id 60
    label "paradygmat"
  ]
  node [
    id 61
    label "jednostka_systematyczna"
  ]
  node [
    id 62
    label "change"
  ]
  node [
    id 63
    label "podgatunek"
  ]
  node [
    id 64
    label "ferment"
  ]
  node [
    id 65
    label "posta&#263;"
  ]
  node [
    id 66
    label "rasa"
  ]
  node [
    id 67
    label "zjawisko"
  ]
  node [
    id 68
    label "przedmiot"
  ]
  node [
    id 69
    label "kom&#243;rka"
  ]
  node [
    id 70
    label "furnishing"
  ]
  node [
    id 71
    label "zabezpieczenie"
  ]
  node [
    id 72
    label "wyrz&#261;dzenie"
  ]
  node [
    id 73
    label "zagospodarowanie"
  ]
  node [
    id 74
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 75
    label "ig&#322;a"
  ]
  node [
    id 76
    label "narz&#281;dzie"
  ]
  node [
    id 77
    label "wirnik"
  ]
  node [
    id 78
    label "aparatura"
  ]
  node [
    id 79
    label "system_energetyczny"
  ]
  node [
    id 80
    label "impulsator"
  ]
  node [
    id 81
    label "mechanizm"
  ]
  node [
    id 82
    label "sprz&#281;t"
  ]
  node [
    id 83
    label "czynno&#347;&#263;"
  ]
  node [
    id 84
    label "blokowanie"
  ]
  node [
    id 85
    label "set"
  ]
  node [
    id 86
    label "zablokowanie"
  ]
  node [
    id 87
    label "przygotowanie"
  ]
  node [
    id 88
    label "komora"
  ]
  node [
    id 89
    label "j&#281;zyk"
  ]
  node [
    id 90
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 91
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 92
    label "narobienie"
  ]
  node [
    id 93
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 94
    label "creation"
  ]
  node [
    id 95
    label "porobienie"
  ]
  node [
    id 96
    label "coevals"
  ]
  node [
    id 97
    label "rynek"
  ]
  node [
    id 98
    label "nuklearyzacja"
  ]
  node [
    id 99
    label "deduction"
  ]
  node [
    id 100
    label "entrance"
  ]
  node [
    id 101
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 102
    label "wst&#281;p"
  ]
  node [
    id 103
    label "spowodowanie"
  ]
  node [
    id 104
    label "wej&#347;cie"
  ]
  node [
    id 105
    label "doprowadzenie"
  ]
  node [
    id 106
    label "umieszczenie"
  ]
  node [
    id 107
    label "umo&#380;liwienie"
  ]
  node [
    id 108
    label "wpisanie"
  ]
  node [
    id 109
    label "podstawy"
  ]
  node [
    id 110
    label "evocation"
  ]
  node [
    id 111
    label "zapoznanie"
  ]
  node [
    id 112
    label "w&#322;&#261;czenie"
  ]
  node [
    id 113
    label "zacz&#281;cie"
  ]
  node [
    id 114
    label "przewietrzenie"
  ]
  node [
    id 115
    label "tekst"
  ]
  node [
    id 116
    label "druk"
  ]
  node [
    id 117
    label "produkcja"
  ]
  node [
    id 118
    label "notification"
  ]
  node [
    id 119
    label "denunciation"
  ]
  node [
    id 120
    label "doniesienie"
  ]
  node [
    id 121
    label "detection"
  ]
  node [
    id 122
    label "dostrze&#380;enie"
  ]
  node [
    id 123
    label "disclosure"
  ]
  node [
    id 124
    label "jawny"
  ]
  node [
    id 125
    label "objawienie"
  ]
  node [
    id 126
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 127
    label "poinformowanie"
  ]
  node [
    id 128
    label "ustawienie"
  ]
  node [
    id 129
    label "narrative"
  ]
  node [
    id 130
    label "nafaszerowanie"
  ]
  node [
    id 131
    label "tenis"
  ]
  node [
    id 132
    label "prayer"
  ]
  node [
    id 133
    label "siatk&#243;wka"
  ]
  node [
    id 134
    label "pi&#322;ka"
  ]
  node [
    id 135
    label "give"
  ]
  node [
    id 136
    label "myth"
  ]
  node [
    id 137
    label "service"
  ]
  node [
    id 138
    label "jedzenie"
  ]
  node [
    id 139
    label "zagranie"
  ]
  node [
    id 140
    label "zaserwowanie"
  ]
  node [
    id 141
    label "opowie&#347;&#263;"
  ]
  node [
    id 142
    label "pass"
  ]
  node [
    id 143
    label "obiecanie"
  ]
  node [
    id 144
    label "zap&#322;acenie"
  ]
  node [
    id 145
    label "cios"
  ]
  node [
    id 146
    label "udost&#281;pnienie"
  ]
  node [
    id 147
    label "wymienienie_si&#281;"
  ]
  node [
    id 148
    label "eating"
  ]
  node [
    id 149
    label "coup"
  ]
  node [
    id 150
    label "hand"
  ]
  node [
    id 151
    label "uprawianie_seksu"
  ]
  node [
    id 152
    label "allow"
  ]
  node [
    id 153
    label "dostarczenie"
  ]
  node [
    id 154
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 155
    label "uderzenie"
  ]
  node [
    id 156
    label "zadanie"
  ]
  node [
    id 157
    label "powierzenie"
  ]
  node [
    id 158
    label "przeznaczenie"
  ]
  node [
    id 159
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 160
    label "przekazanie"
  ]
  node [
    id 161
    label "odst&#261;pienie"
  ]
  node [
    id 162
    label "dodanie"
  ]
  node [
    id 163
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 164
    label "wyposa&#380;enie"
  ]
  node [
    id 165
    label "dostanie"
  ]
  node [
    id 166
    label "karta"
  ]
  node [
    id 167
    label "potrawa"
  ]
  node [
    id 168
    label "menu"
  ]
  node [
    id 169
    label "uderzanie"
  ]
  node [
    id 170
    label "wyst&#261;pienie"
  ]
  node [
    id 171
    label "wyposa&#380;anie"
  ]
  node [
    id 172
    label "pobicie"
  ]
  node [
    id 173
    label "posi&#322;ek"
  ]
  node [
    id 174
    label "czynnik_biotyczny"
  ]
  node [
    id 175
    label "wyewoluowanie"
  ]
  node [
    id 176
    label "reakcja"
  ]
  node [
    id 177
    label "individual"
  ]
  node [
    id 178
    label "przyswoi&#263;"
  ]
  node [
    id 179
    label "wytw&#243;r"
  ]
  node [
    id 180
    label "starzenie_si&#281;"
  ]
  node [
    id 181
    label "wyewoluowa&#263;"
  ]
  node [
    id 182
    label "okaz"
  ]
  node [
    id 183
    label "part"
  ]
  node [
    id 184
    label "przyswojenie"
  ]
  node [
    id 185
    label "ewoluowanie"
  ]
  node [
    id 186
    label "ewoluowa&#263;"
  ]
  node [
    id 187
    label "sztuka"
  ]
  node [
    id 188
    label "agent"
  ]
  node [
    id 189
    label "przyswaja&#263;"
  ]
  node [
    id 190
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 191
    label "nicpo&#324;"
  ]
  node [
    id 192
    label "przyswajanie"
  ]
  node [
    id 193
    label "phone"
  ]
  node [
    id 194
    label "wpadni&#281;cie"
  ]
  node [
    id 195
    label "wydawa&#263;"
  ]
  node [
    id 196
    label "wyda&#263;"
  ]
  node [
    id 197
    label "intonacja"
  ]
  node [
    id 198
    label "wpa&#347;&#263;"
  ]
  node [
    id 199
    label "note"
  ]
  node [
    id 200
    label "onomatopeja"
  ]
  node [
    id 201
    label "modalizm"
  ]
  node [
    id 202
    label "nadlecenie"
  ]
  node [
    id 203
    label "sound"
  ]
  node [
    id 204
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 205
    label "wpada&#263;"
  ]
  node [
    id 206
    label "solmizacja"
  ]
  node [
    id 207
    label "seria"
  ]
  node [
    id 208
    label "dobiec"
  ]
  node [
    id 209
    label "transmiter"
  ]
  node [
    id 210
    label "heksachord"
  ]
  node [
    id 211
    label "akcent"
  ]
  node [
    id 212
    label "repetycja"
  ]
  node [
    id 213
    label "brzmienie"
  ]
  node [
    id 214
    label "wpadanie"
  ]
  node [
    id 215
    label "liczba_kwantowa"
  ]
  node [
    id 216
    label "kosmetyk"
  ]
  node [
    id 217
    label "ciasto"
  ]
  node [
    id 218
    label "aromat"
  ]
  node [
    id 219
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 220
    label "puff"
  ]
  node [
    id 221
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 222
    label "przyprawa"
  ]
  node [
    id 223
    label "upojno&#347;&#263;"
  ]
  node [
    id 224
    label "owiewanie"
  ]
  node [
    id 225
    label "smak"
  ]
  node [
    id 226
    label "kwota"
  ]
  node [
    id 227
    label "remainder"
  ]
  node [
    id 228
    label "pozosta&#322;y"
  ]
  node [
    id 229
    label "psychotest"
  ]
  node [
    id 230
    label "communication"
  ]
  node [
    id 231
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 232
    label "wk&#322;ad"
  ]
  node [
    id 233
    label "zajawka"
  ]
  node [
    id 234
    label "ok&#322;adka"
  ]
  node [
    id 235
    label "Zwrotnica"
  ]
  node [
    id 236
    label "dzia&#322;"
  ]
  node [
    id 237
    label "prasa"
  ]
  node [
    id 238
    label "elektroniczny"
  ]
  node [
    id 239
    label "internetowo"
  ]
  node [
    id 240
    label "nowoczesny"
  ]
  node [
    id 241
    label "netowy"
  ]
  node [
    id 242
    label "sieciowo"
  ]
  node [
    id 243
    label "elektronicznie"
  ]
  node [
    id 244
    label "siatkowy"
  ]
  node [
    id 245
    label "sieciowy"
  ]
  node [
    id 246
    label "nowy"
  ]
  node [
    id 247
    label "nowo&#380;ytny"
  ]
  node [
    id 248
    label "otwarty"
  ]
  node [
    id 249
    label "nowocze&#347;nie"
  ]
  node [
    id 250
    label "elektrycznie"
  ]
  node [
    id 251
    label "handwriting"
  ]
  node [
    id 252
    label "przekaz"
  ]
  node [
    id 253
    label "dzie&#322;o"
  ]
  node [
    id 254
    label "paleograf"
  ]
  node [
    id 255
    label "interpunkcja"
  ]
  node [
    id 256
    label "cecha"
  ]
  node [
    id 257
    label "grafia"
  ]
  node [
    id 258
    label "script"
  ]
  node [
    id 259
    label "list"
  ]
  node [
    id 260
    label "adres"
  ]
  node [
    id 261
    label "ortografia"
  ]
  node [
    id 262
    label "letter"
  ]
  node [
    id 263
    label "komunikacja"
  ]
  node [
    id 264
    label "paleografia"
  ]
  node [
    id 265
    label "dokument"
  ]
  node [
    id 266
    label "zapis"
  ]
  node [
    id 267
    label "&#347;wiadectwo"
  ]
  node [
    id 268
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 269
    label "parafa"
  ]
  node [
    id 270
    label "plik"
  ]
  node [
    id 271
    label "raport&#243;wka"
  ]
  node [
    id 272
    label "utw&#243;r"
  ]
  node [
    id 273
    label "record"
  ]
  node [
    id 274
    label "registratura"
  ]
  node [
    id 275
    label "dokumentacja"
  ]
  node [
    id 276
    label "fascyku&#322;"
  ]
  node [
    id 277
    label "artyku&#322;"
  ]
  node [
    id 278
    label "writing"
  ]
  node [
    id 279
    label "sygnatariusz"
  ]
  node [
    id 280
    label "znaczek_pocztowy"
  ]
  node [
    id 281
    label "li&#347;&#263;"
  ]
  node [
    id 282
    label "epistolografia"
  ]
  node [
    id 283
    label "poczta"
  ]
  node [
    id 284
    label "poczta_elektroniczna"
  ]
  node [
    id 285
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 286
    label "przesy&#322;ka"
  ]
  node [
    id 287
    label "charakterystyka"
  ]
  node [
    id 288
    label "m&#322;ot"
  ]
  node [
    id 289
    label "znak"
  ]
  node [
    id 290
    label "drzewo"
  ]
  node [
    id 291
    label "pr&#243;ba"
  ]
  node [
    id 292
    label "attribute"
  ]
  node [
    id 293
    label "marka"
  ]
  node [
    id 294
    label "transportation_system"
  ]
  node [
    id 295
    label "explicite"
  ]
  node [
    id 296
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 297
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 298
    label "wydeptywanie"
  ]
  node [
    id 299
    label "miejsce"
  ]
  node [
    id 300
    label "wydeptanie"
  ]
  node [
    id 301
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 302
    label "implicite"
  ]
  node [
    id 303
    label "ekspedytor"
  ]
  node [
    id 304
    label "obrazowanie"
  ]
  node [
    id 305
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 306
    label "dorobek"
  ]
  node [
    id 307
    label "forma"
  ]
  node [
    id 308
    label "tre&#347;&#263;"
  ]
  node [
    id 309
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 310
    label "retrospektywa"
  ]
  node [
    id 311
    label "works"
  ]
  node [
    id 312
    label "tetralogia"
  ]
  node [
    id 313
    label "komunikat"
  ]
  node [
    id 314
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 315
    label "praca"
  ]
  node [
    id 316
    label "proces"
  ]
  node [
    id 317
    label "blankiet"
  ]
  node [
    id 318
    label "znaczenie"
  ]
  node [
    id 319
    label "draft"
  ]
  node [
    id 320
    label "transakcja"
  ]
  node [
    id 321
    label "order"
  ]
  node [
    id 322
    label "brachygrafia"
  ]
  node [
    id 323
    label "historia"
  ]
  node [
    id 324
    label "historyk"
  ]
  node [
    id 325
    label "blok"
  ]
  node [
    id 326
    label "oprawa"
  ]
  node [
    id 327
    label "boarding"
  ]
  node [
    id 328
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 329
    label "oprawianie"
  ]
  node [
    id 330
    label "os&#322;ona"
  ]
  node [
    id 331
    label "oprawia&#263;"
  ]
  node [
    id 332
    label "zeszyt"
  ]
  node [
    id 333
    label "streszczenie"
  ]
  node [
    id 334
    label "harbinger"
  ]
  node [
    id 335
    label "ch&#281;&#263;"
  ]
  node [
    id 336
    label "zapowied&#378;"
  ]
  node [
    id 337
    label "zami&#322;owanie"
  ]
  node [
    id 338
    label "reklama"
  ]
  node [
    id 339
    label "gadka"
  ]
  node [
    id 340
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 341
    label "test_psychologiczny"
  ]
  node [
    id 342
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 343
    label "jednostka_organizacyjna"
  ]
  node [
    id 344
    label "urz&#261;d"
  ]
  node [
    id 345
    label "sfera"
  ]
  node [
    id 346
    label "zakres"
  ]
  node [
    id 347
    label "miejsce_pracy"
  ]
  node [
    id 348
    label "zesp&#243;&#322;"
  ]
  node [
    id 349
    label "insourcing"
  ]
  node [
    id 350
    label "whole"
  ]
  node [
    id 351
    label "column"
  ]
  node [
    id 352
    label "distribution"
  ]
  node [
    id 353
    label "stopie&#324;"
  ]
  node [
    id 354
    label "competence"
  ]
  node [
    id 355
    label "bezdro&#380;e"
  ]
  node [
    id 356
    label "poddzia&#322;"
  ]
  node [
    id 357
    label "kartka"
  ]
  node [
    id 358
    label "uczestnictwo"
  ]
  node [
    id 359
    label "element"
  ]
  node [
    id 360
    label "input"
  ]
  node [
    id 361
    label "lokata"
  ]
  node [
    id 362
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 363
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 364
    label "pisanie_si&#281;"
  ]
  node [
    id 365
    label "fonetyzacja"
  ]
  node [
    id 366
    label "wyj&#261;tek"
  ]
  node [
    id 367
    label "ortoepia"
  ]
  node [
    id 368
    label "t&#322;oczysko"
  ]
  node [
    id 369
    label "depesza"
  ]
  node [
    id 370
    label "maszyna"
  ]
  node [
    id 371
    label "media"
  ]
  node [
    id 372
    label "napisa&#263;"
  ]
  node [
    id 373
    label "dziennikarz_prasowy"
  ]
  node [
    id 374
    label "pisa&#263;"
  ]
  node [
    id 375
    label "kiosk"
  ]
  node [
    id 376
    label "maszyna_rolnicza"
  ]
  node [
    id 377
    label "gazeta"
  ]
  node [
    id 378
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 379
    label "artykulator"
  ]
  node [
    id 380
    label "kod"
  ]
  node [
    id 381
    label "kawa&#322;ek"
  ]
  node [
    id 382
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 383
    label "stylik"
  ]
  node [
    id 384
    label "przet&#322;umaczenie"
  ]
  node [
    id 385
    label "formalizowanie"
  ]
  node [
    id 386
    label "ssa&#263;"
  ]
  node [
    id 387
    label "ssanie"
  ]
  node [
    id 388
    label "language"
  ]
  node [
    id 389
    label "liza&#263;"
  ]
  node [
    id 390
    label "konsonantyzm"
  ]
  node [
    id 391
    label "wokalizm"
  ]
  node [
    id 392
    label "fonetyka"
  ]
  node [
    id 393
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 394
    label "jeniec"
  ]
  node [
    id 395
    label "but"
  ]
  node [
    id 396
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 397
    label "po_koroniarsku"
  ]
  node [
    id 398
    label "kultura_duchowa"
  ]
  node [
    id 399
    label "t&#322;umaczenie"
  ]
  node [
    id 400
    label "m&#243;wienie"
  ]
  node [
    id 401
    label "pype&#263;"
  ]
  node [
    id 402
    label "lizanie"
  ]
  node [
    id 403
    label "formalizowa&#263;"
  ]
  node [
    id 404
    label "rozumie&#263;"
  ]
  node [
    id 405
    label "organ"
  ]
  node [
    id 406
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 407
    label "rozumienie"
  ]
  node [
    id 408
    label "spos&#243;b"
  ]
  node [
    id 409
    label "makroglosja"
  ]
  node [
    id 410
    label "m&#243;wi&#263;"
  ]
  node [
    id 411
    label "jama_ustna"
  ]
  node [
    id 412
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 413
    label "formacja_geologiczna"
  ]
  node [
    id 414
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 415
    label "natural_language"
  ]
  node [
    id 416
    label "s&#322;ownictwo"
  ]
  node [
    id 417
    label "po&#322;o&#380;enie"
  ]
  node [
    id 418
    label "personalia"
  ]
  node [
    id 419
    label "domena"
  ]
  node [
    id 420
    label "dane"
  ]
  node [
    id 421
    label "siedziba"
  ]
  node [
    id 422
    label "kod_pocztowy"
  ]
  node [
    id 423
    label "adres_elektroniczny"
  ]
  node [
    id 424
    label "dziedzina"
  ]
  node [
    id 425
    label "strona"
  ]
  node [
    id 426
    label "naukowo"
  ]
  node [
    id 427
    label "teoretyczny"
  ]
  node [
    id 428
    label "edukacyjnie"
  ]
  node [
    id 429
    label "scjentyficzny"
  ]
  node [
    id 430
    label "skomplikowany"
  ]
  node [
    id 431
    label "specjalistyczny"
  ]
  node [
    id 432
    label "zgodny"
  ]
  node [
    id 433
    label "intelektualny"
  ]
  node [
    id 434
    label "specjalny"
  ]
  node [
    id 435
    label "nierealny"
  ]
  node [
    id 436
    label "teoretycznie"
  ]
  node [
    id 437
    label "zgodnie"
  ]
  node [
    id 438
    label "zbie&#380;ny"
  ]
  node [
    id 439
    label "spokojny"
  ]
  node [
    id 440
    label "dobry"
  ]
  node [
    id 441
    label "specjalistycznie"
  ]
  node [
    id 442
    label "fachowo"
  ]
  node [
    id 443
    label "fachowy"
  ]
  node [
    id 444
    label "intencjonalny"
  ]
  node [
    id 445
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 446
    label "niedorozw&#243;j"
  ]
  node [
    id 447
    label "szczeg&#243;lny"
  ]
  node [
    id 448
    label "specjalnie"
  ]
  node [
    id 449
    label "nieetatowy"
  ]
  node [
    id 450
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 451
    label "nienormalny"
  ]
  node [
    id 452
    label "umy&#347;lnie"
  ]
  node [
    id 453
    label "odpowiedni"
  ]
  node [
    id 454
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 455
    label "trudny"
  ]
  node [
    id 456
    label "skomplikowanie"
  ]
  node [
    id 457
    label "intelektualnie"
  ]
  node [
    id 458
    label "my&#347;l&#261;cy"
  ]
  node [
    id 459
    label "wznios&#322;y"
  ]
  node [
    id 460
    label "g&#322;&#281;boki"
  ]
  node [
    id 461
    label "umys&#322;owy"
  ]
  node [
    id 462
    label "inteligentny"
  ]
  node [
    id 463
    label "oddany"
  ]
  node [
    id 464
    label "wierny"
  ]
  node [
    id 465
    label "ofiarny"
  ]
  node [
    id 466
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 467
    label "mie&#263;_miejsce"
  ]
  node [
    id 468
    label "equal"
  ]
  node [
    id 469
    label "trwa&#263;"
  ]
  node [
    id 470
    label "chodzi&#263;"
  ]
  node [
    id 471
    label "si&#281;ga&#263;"
  ]
  node [
    id 472
    label "stan"
  ]
  node [
    id 473
    label "obecno&#347;&#263;"
  ]
  node [
    id 474
    label "stand"
  ]
  node [
    id 475
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 476
    label "uczestniczy&#263;"
  ]
  node [
    id 477
    label "participate"
  ]
  node [
    id 478
    label "robi&#263;"
  ]
  node [
    id 479
    label "istnie&#263;"
  ]
  node [
    id 480
    label "pozostawa&#263;"
  ]
  node [
    id 481
    label "zostawa&#263;"
  ]
  node [
    id 482
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 483
    label "adhere"
  ]
  node [
    id 484
    label "compass"
  ]
  node [
    id 485
    label "korzysta&#263;"
  ]
  node [
    id 486
    label "appreciation"
  ]
  node [
    id 487
    label "osi&#261;ga&#263;"
  ]
  node [
    id 488
    label "dociera&#263;"
  ]
  node [
    id 489
    label "get"
  ]
  node [
    id 490
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 491
    label "mierzy&#263;"
  ]
  node [
    id 492
    label "u&#380;ywa&#263;"
  ]
  node [
    id 493
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 494
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 495
    label "exsert"
  ]
  node [
    id 496
    label "being"
  ]
  node [
    id 497
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 498
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 499
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 500
    label "p&#322;ywa&#263;"
  ]
  node [
    id 501
    label "run"
  ]
  node [
    id 502
    label "bangla&#263;"
  ]
  node [
    id 503
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 504
    label "przebiega&#263;"
  ]
  node [
    id 505
    label "wk&#322;ada&#263;"
  ]
  node [
    id 506
    label "proceed"
  ]
  node [
    id 507
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 508
    label "carry"
  ]
  node [
    id 509
    label "bywa&#263;"
  ]
  node [
    id 510
    label "dziama&#263;"
  ]
  node [
    id 511
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 512
    label "stara&#263;_si&#281;"
  ]
  node [
    id 513
    label "para"
  ]
  node [
    id 514
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 515
    label "str&#243;j"
  ]
  node [
    id 516
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 517
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 518
    label "krok"
  ]
  node [
    id 519
    label "tryb"
  ]
  node [
    id 520
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 521
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 522
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 523
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 524
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 525
    label "continue"
  ]
  node [
    id 526
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 527
    label "Ohio"
  ]
  node [
    id 528
    label "wci&#281;cie"
  ]
  node [
    id 529
    label "Nowy_York"
  ]
  node [
    id 530
    label "warstwa"
  ]
  node [
    id 531
    label "samopoczucie"
  ]
  node [
    id 532
    label "Illinois"
  ]
  node [
    id 533
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 534
    label "state"
  ]
  node [
    id 535
    label "Jukatan"
  ]
  node [
    id 536
    label "Kalifornia"
  ]
  node [
    id 537
    label "Wirginia"
  ]
  node [
    id 538
    label "wektor"
  ]
  node [
    id 539
    label "Teksas"
  ]
  node [
    id 540
    label "Goa"
  ]
  node [
    id 541
    label "Waszyngton"
  ]
  node [
    id 542
    label "Massachusetts"
  ]
  node [
    id 543
    label "Alaska"
  ]
  node [
    id 544
    label "Arakan"
  ]
  node [
    id 545
    label "Hawaje"
  ]
  node [
    id 546
    label "Maryland"
  ]
  node [
    id 547
    label "punkt"
  ]
  node [
    id 548
    label "Michigan"
  ]
  node [
    id 549
    label "Arizona"
  ]
  node [
    id 550
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 551
    label "Georgia"
  ]
  node [
    id 552
    label "poziom"
  ]
  node [
    id 553
    label "Pensylwania"
  ]
  node [
    id 554
    label "shape"
  ]
  node [
    id 555
    label "Luizjana"
  ]
  node [
    id 556
    label "Nowy_Meksyk"
  ]
  node [
    id 557
    label "Alabama"
  ]
  node [
    id 558
    label "ilo&#347;&#263;"
  ]
  node [
    id 559
    label "Kansas"
  ]
  node [
    id 560
    label "Oregon"
  ]
  node [
    id 561
    label "Floryda"
  ]
  node [
    id 562
    label "Oklahoma"
  ]
  node [
    id 563
    label "jednostka_administracyjna"
  ]
  node [
    id 564
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 565
    label "obserwowanie"
  ]
  node [
    id 566
    label "zrecenzowanie"
  ]
  node [
    id 567
    label "kontrola"
  ]
  node [
    id 568
    label "analysis"
  ]
  node [
    id 569
    label "rektalny"
  ]
  node [
    id 570
    label "ustalenie"
  ]
  node [
    id 571
    label "macanie"
  ]
  node [
    id 572
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 573
    label "usi&#322;owanie"
  ]
  node [
    id 574
    label "udowadnianie"
  ]
  node [
    id 575
    label "bia&#322;a_niedziela"
  ]
  node [
    id 576
    label "diagnostyka"
  ]
  node [
    id 577
    label "dociekanie"
  ]
  node [
    id 578
    label "rezultat"
  ]
  node [
    id 579
    label "sprawdzanie"
  ]
  node [
    id 580
    label "penetrowanie"
  ]
  node [
    id 581
    label "krytykowanie"
  ]
  node [
    id 582
    label "omawianie"
  ]
  node [
    id 583
    label "ustalanie"
  ]
  node [
    id 584
    label "rozpatrywanie"
  ]
  node [
    id 585
    label "investigation"
  ]
  node [
    id 586
    label "wziernikowanie"
  ]
  node [
    id 587
    label "examination"
  ]
  node [
    id 588
    label "discussion"
  ]
  node [
    id 589
    label "dyskutowanie"
  ]
  node [
    id 590
    label "temat"
  ]
  node [
    id 591
    label "czepianie_si&#281;"
  ]
  node [
    id 592
    label "opiniowanie"
  ]
  node [
    id 593
    label "ocenianie"
  ]
  node [
    id 594
    label "zaopiniowanie"
  ]
  node [
    id 595
    label "przeszukiwanie"
  ]
  node [
    id 596
    label "docieranie"
  ]
  node [
    id 597
    label "penetration"
  ]
  node [
    id 598
    label "decyzja"
  ]
  node [
    id 599
    label "umocnienie"
  ]
  node [
    id 600
    label "appointment"
  ]
  node [
    id 601
    label "localization"
  ]
  node [
    id 602
    label "informacja"
  ]
  node [
    id 603
    label "zdecydowanie"
  ]
  node [
    id 604
    label "colony"
  ]
  node [
    id 605
    label "powodowanie"
  ]
  node [
    id 606
    label "robienie"
  ]
  node [
    id 607
    label "colonization"
  ]
  node [
    id 608
    label "decydowanie"
  ]
  node [
    id 609
    label "umacnianie"
  ]
  node [
    id 610
    label "liquidation"
  ]
  node [
    id 611
    label "przemy&#347;liwanie"
  ]
  node [
    id 612
    label "dzia&#322;anie"
  ]
  node [
    id 613
    label "event"
  ]
  node [
    id 614
    label "przyczyna"
  ]
  node [
    id 615
    label "legalizacja_ponowna"
  ]
  node [
    id 616
    label "instytucja"
  ]
  node [
    id 617
    label "w&#322;adza"
  ]
  node [
    id 618
    label "perlustracja"
  ]
  node [
    id 619
    label "legalizacja_pierwotna"
  ]
  node [
    id 620
    label "activity"
  ]
  node [
    id 621
    label "bezproblemowy"
  ]
  node [
    id 622
    label "wydarzenie"
  ]
  node [
    id 623
    label "podejmowanie"
  ]
  node [
    id 624
    label "effort"
  ]
  node [
    id 625
    label "staranie_si&#281;"
  ]
  node [
    id 626
    label "essay"
  ]
  node [
    id 627
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 628
    label "redagowanie"
  ]
  node [
    id 629
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 630
    label "przymierzanie"
  ]
  node [
    id 631
    label "przymierzenie"
  ]
  node [
    id 632
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 633
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 634
    label "najem"
  ]
  node [
    id 635
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 636
    label "zak&#322;ad"
  ]
  node [
    id 637
    label "stosunek_pracy"
  ]
  node [
    id 638
    label "benedykty&#324;ski"
  ]
  node [
    id 639
    label "poda&#380;_pracy"
  ]
  node [
    id 640
    label "pracowanie"
  ]
  node [
    id 641
    label "tyrka"
  ]
  node [
    id 642
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 643
    label "zaw&#243;d"
  ]
  node [
    id 644
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 645
    label "tynkarski"
  ]
  node [
    id 646
    label "pracowa&#263;"
  ]
  node [
    id 647
    label "zmiana"
  ]
  node [
    id 648
    label "czynnik_produkcji"
  ]
  node [
    id 649
    label "zobowi&#261;zanie"
  ]
  node [
    id 650
    label "kierownictwo"
  ]
  node [
    id 651
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 652
    label "analizowanie"
  ]
  node [
    id 653
    label "uzasadnianie"
  ]
  node [
    id 654
    label "presentation"
  ]
  node [
    id 655
    label "pokazywanie"
  ]
  node [
    id 656
    label "endoscopy"
  ]
  node [
    id 657
    label "rozmy&#347;lanie"
  ]
  node [
    id 658
    label "quest"
  ]
  node [
    id 659
    label "dop&#322;ywanie"
  ]
  node [
    id 660
    label "examen"
  ]
  node [
    id 661
    label "diagnosis"
  ]
  node [
    id 662
    label "medycyna"
  ]
  node [
    id 663
    label "anamneza"
  ]
  node [
    id 664
    label "dotykanie"
  ]
  node [
    id 665
    label "dr&#243;b"
  ]
  node [
    id 666
    label "pomacanie"
  ]
  node [
    id 667
    label "feel"
  ]
  node [
    id 668
    label "palpation"
  ]
  node [
    id 669
    label "namacanie"
  ]
  node [
    id 670
    label "hodowanie"
  ]
  node [
    id 671
    label "patrzenie"
  ]
  node [
    id 672
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 673
    label "doszukanie_si&#281;"
  ]
  node [
    id 674
    label "dostrzeganie"
  ]
  node [
    id 675
    label "poobserwowanie"
  ]
  node [
    id 676
    label "observation"
  ]
  node [
    id 677
    label "bocianie_gniazdo"
  ]
  node [
    id 678
    label "zmienno&#347;&#263;"
  ]
  node [
    id 679
    label "play"
  ]
  node [
    id 680
    label "rozgrywka"
  ]
  node [
    id 681
    label "apparent_motion"
  ]
  node [
    id 682
    label "contest"
  ]
  node [
    id 683
    label "akcja"
  ]
  node [
    id 684
    label "komplet"
  ]
  node [
    id 685
    label "zabawa"
  ]
  node [
    id 686
    label "zasada"
  ]
  node [
    id 687
    label "rywalizacja"
  ]
  node [
    id 688
    label "zbijany"
  ]
  node [
    id 689
    label "post&#281;powanie"
  ]
  node [
    id 690
    label "game"
  ]
  node [
    id 691
    label "odg&#322;os"
  ]
  node [
    id 692
    label "Pok&#233;mon"
  ]
  node [
    id 693
    label "synteza"
  ]
  node [
    id 694
    label "odtworzenie"
  ]
  node [
    id 695
    label "rekwizyt_do_gry"
  ]
  node [
    id 696
    label "resonance"
  ]
  node [
    id 697
    label "s&#261;d"
  ]
  node [
    id 698
    label "kognicja"
  ]
  node [
    id 699
    label "campaign"
  ]
  node [
    id 700
    label "rozprawa"
  ]
  node [
    id 701
    label "zachowanie"
  ]
  node [
    id 702
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 703
    label "fashion"
  ]
  node [
    id 704
    label "zmierzanie"
  ]
  node [
    id 705
    label "przes&#322;anka"
  ]
  node [
    id 706
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 707
    label "kazanie"
  ]
  node [
    id 708
    label "trafienie"
  ]
  node [
    id 709
    label "rewan&#380;owy"
  ]
  node [
    id 710
    label "zagrywka"
  ]
  node [
    id 711
    label "faza"
  ]
  node [
    id 712
    label "euroliga"
  ]
  node [
    id 713
    label "interliga"
  ]
  node [
    id 714
    label "runda"
  ]
  node [
    id 715
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 716
    label "rozrywka"
  ]
  node [
    id 717
    label "impreza"
  ]
  node [
    id 718
    label "igraszka"
  ]
  node [
    id 719
    label "taniec"
  ]
  node [
    id 720
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 721
    label "gambling"
  ]
  node [
    id 722
    label "chwyt"
  ]
  node [
    id 723
    label "igra"
  ]
  node [
    id 724
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 725
    label "nabawienie_si&#281;"
  ]
  node [
    id 726
    label "ubaw"
  ]
  node [
    id 727
    label "wodzirej"
  ]
  node [
    id 728
    label "przebiec"
  ]
  node [
    id 729
    label "charakter"
  ]
  node [
    id 730
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 731
    label "motyw"
  ]
  node [
    id 732
    label "przebiegni&#281;cie"
  ]
  node [
    id 733
    label "fabu&#322;a"
  ]
  node [
    id 734
    label "proces_technologiczny"
  ]
  node [
    id 735
    label "mieszanina"
  ]
  node [
    id 736
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 737
    label "fusion"
  ]
  node [
    id 738
    label "poj&#281;cie"
  ]
  node [
    id 739
    label "reakcja_chemiczna"
  ]
  node [
    id 740
    label "zestawienie"
  ]
  node [
    id 741
    label "uog&#243;lnienie"
  ]
  node [
    id 742
    label "puszczenie"
  ]
  node [
    id 743
    label "wyst&#281;p"
  ]
  node [
    id 744
    label "reproduction"
  ]
  node [
    id 745
    label "przedstawienie"
  ]
  node [
    id 746
    label "przywr&#243;cenie"
  ]
  node [
    id 747
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 748
    label "restoration"
  ]
  node [
    id 749
    label "odbudowanie"
  ]
  node [
    id 750
    label "lekcja"
  ]
  node [
    id 751
    label "ensemble"
  ]
  node [
    id 752
    label "grupa"
  ]
  node [
    id 753
    label "klasa"
  ]
  node [
    id 754
    label "zestaw"
  ]
  node [
    id 755
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 756
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 757
    label "regu&#322;a_Allena"
  ]
  node [
    id 758
    label "base"
  ]
  node [
    id 759
    label "umowa"
  ]
  node [
    id 760
    label "obserwacja"
  ]
  node [
    id 761
    label "zasada_d'Alemberta"
  ]
  node [
    id 762
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 763
    label "normalizacja"
  ]
  node [
    id 764
    label "moralno&#347;&#263;"
  ]
  node [
    id 765
    label "criterion"
  ]
  node [
    id 766
    label "opis"
  ]
  node [
    id 767
    label "regu&#322;a_Glogera"
  ]
  node [
    id 768
    label "prawo_Mendla"
  ]
  node [
    id 769
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 770
    label "twierdzenie"
  ]
  node [
    id 771
    label "prawo"
  ]
  node [
    id 772
    label "standard"
  ]
  node [
    id 773
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 774
    label "qualification"
  ]
  node [
    id 775
    label "dominion"
  ]
  node [
    id 776
    label "occupation"
  ]
  node [
    id 777
    label "podstawa"
  ]
  node [
    id 778
    label "substancja"
  ]
  node [
    id 779
    label "prawid&#322;o"
  ]
  node [
    id 780
    label "dywidenda"
  ]
  node [
    id 781
    label "przebieg"
  ]
  node [
    id 782
    label "operacja"
  ]
  node [
    id 783
    label "udzia&#322;"
  ]
  node [
    id 784
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 785
    label "commotion"
  ]
  node [
    id 786
    label "jazda"
  ]
  node [
    id 787
    label "czyn"
  ]
  node [
    id 788
    label "stock"
  ]
  node [
    id 789
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 790
    label "w&#281;ze&#322;"
  ]
  node [
    id 791
    label "wysoko&#347;&#263;"
  ]
  node [
    id 792
    label "instrument_strunowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 134
  ]
]
