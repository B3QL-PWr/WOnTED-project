graph [
  node [
    id 0
    label "zabawa"
    origin "text"
  ]
  node [
    id 1
    label "blokowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 3
    label "rozrywka"
  ]
  node [
    id 4
    label "impreza"
  ]
  node [
    id 5
    label "igraszka"
  ]
  node [
    id 6
    label "taniec"
  ]
  node [
    id 7
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 8
    label "gambling"
  ]
  node [
    id 9
    label "chwyt"
  ]
  node [
    id 10
    label "game"
  ]
  node [
    id 11
    label "igra"
  ]
  node [
    id 12
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 13
    label "cecha"
  ]
  node [
    id 14
    label "nabawienie_si&#281;"
  ]
  node [
    id 15
    label "ubaw"
  ]
  node [
    id 16
    label "wodzirej"
  ]
  node [
    id 17
    label "charakterystyka"
  ]
  node [
    id 18
    label "m&#322;ot"
  ]
  node [
    id 19
    label "znak"
  ]
  node [
    id 20
    label "drzewo"
  ]
  node [
    id 21
    label "pr&#243;ba"
  ]
  node [
    id 22
    label "attribute"
  ]
  node [
    id 23
    label "marka"
  ]
  node [
    id 24
    label "impra"
  ]
  node [
    id 25
    label "przyj&#281;cie"
  ]
  node [
    id 26
    label "okazja"
  ]
  node [
    id 27
    label "party"
  ]
  node [
    id 28
    label "czasoumilacz"
  ]
  node [
    id 29
    label "odpoczynek"
  ]
  node [
    id 30
    label "spos&#243;b"
  ]
  node [
    id 31
    label "kompozycja"
  ]
  node [
    id 32
    label "zacisk"
  ]
  node [
    id 33
    label "strategia"
  ]
  node [
    id 34
    label "zabieg"
  ]
  node [
    id 35
    label "ruch"
  ]
  node [
    id 36
    label "uchwyt"
  ]
  node [
    id 37
    label "uj&#281;cie"
  ]
  node [
    id 38
    label "zbi&#243;r"
  ]
  node [
    id 39
    label "karnet"
  ]
  node [
    id 40
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 41
    label "utw&#243;r"
  ]
  node [
    id 42
    label "parkiet"
  ]
  node [
    id 43
    label "choreologia"
  ]
  node [
    id 44
    label "czynno&#347;&#263;"
  ]
  node [
    id 45
    label "krok_taneczny"
  ]
  node [
    id 46
    label "twardzioszek_przydro&#380;ny"
  ]
  node [
    id 47
    label "rado&#347;&#263;"
  ]
  node [
    id 48
    label "narz&#281;dzie"
  ]
  node [
    id 49
    label "Fidel_Castro"
  ]
  node [
    id 50
    label "Anders"
  ]
  node [
    id 51
    label "Ko&#347;ciuszko"
  ]
  node [
    id 52
    label "Tito"
  ]
  node [
    id 53
    label "Miko&#322;ajczyk"
  ]
  node [
    id 54
    label "lider"
  ]
  node [
    id 55
    label "Mao"
  ]
  node [
    id 56
    label "Sabataj_Cwi"
  ]
  node [
    id 57
    label "mistrz_ceremonii"
  ]
  node [
    id 58
    label "wesele"
  ]
  node [
    id 59
    label "przeszkadza&#263;"
  ]
  node [
    id 60
    label "zajmowa&#263;"
  ]
  node [
    id 61
    label "wstrzymywa&#263;"
  ]
  node [
    id 62
    label "throng"
  ]
  node [
    id 63
    label "unieruchamia&#263;"
  ]
  node [
    id 64
    label "kiblowa&#263;"
  ]
  node [
    id 65
    label "sk&#322;ada&#263;"
  ]
  node [
    id 66
    label "walczy&#263;"
  ]
  node [
    id 67
    label "zablokowywa&#263;"
  ]
  node [
    id 68
    label "zatrzymywa&#263;"
  ]
  node [
    id 69
    label "interlock"
  ]
  node [
    id 70
    label "parry"
  ]
  node [
    id 71
    label "powodowa&#263;"
  ]
  node [
    id 72
    label "dostarcza&#263;"
  ]
  node [
    id 73
    label "robi&#263;"
  ]
  node [
    id 74
    label "korzysta&#263;"
  ]
  node [
    id 75
    label "schorzenie"
  ]
  node [
    id 76
    label "komornik"
  ]
  node [
    id 77
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 78
    label "return"
  ]
  node [
    id 79
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 80
    label "trwa&#263;"
  ]
  node [
    id 81
    label "bra&#263;"
  ]
  node [
    id 82
    label "rozciekawia&#263;"
  ]
  node [
    id 83
    label "klasyfikacja"
  ]
  node [
    id 84
    label "zadawa&#263;"
  ]
  node [
    id 85
    label "fill"
  ]
  node [
    id 86
    label "zabiera&#263;"
  ]
  node [
    id 87
    label "topographic_point"
  ]
  node [
    id 88
    label "obejmowa&#263;"
  ]
  node [
    id 89
    label "pali&#263;_si&#281;"
  ]
  node [
    id 90
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 91
    label "aim"
  ]
  node [
    id 92
    label "anektowa&#263;"
  ]
  node [
    id 93
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 94
    label "prosecute"
  ]
  node [
    id 95
    label "sake"
  ]
  node [
    id 96
    label "do"
  ]
  node [
    id 97
    label "suspend"
  ]
  node [
    id 98
    label "zgarnia&#263;"
  ]
  node [
    id 99
    label "przerywa&#263;"
  ]
  node [
    id 100
    label "przechowywa&#263;"
  ]
  node [
    id 101
    label "&#322;apa&#263;"
  ]
  node [
    id 102
    label "przetrzymywa&#263;"
  ]
  node [
    id 103
    label "allude"
  ]
  node [
    id 104
    label "zaczepia&#263;"
  ]
  node [
    id 105
    label "hold"
  ]
  node [
    id 106
    label "zamyka&#263;"
  ]
  node [
    id 107
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 108
    label "stara&#263;_si&#281;"
  ]
  node [
    id 109
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 110
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 111
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 112
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 113
    label "dzia&#322;a&#263;"
  ]
  node [
    id 114
    label "fight"
  ]
  node [
    id 115
    label "wrestle"
  ]
  node [
    id 116
    label "zawody"
  ]
  node [
    id 117
    label "cope"
  ]
  node [
    id 118
    label "contend"
  ]
  node [
    id 119
    label "argue"
  ]
  node [
    id 120
    label "my&#347;lenie"
  ]
  node [
    id 121
    label "przekazywa&#263;"
  ]
  node [
    id 122
    label "zbiera&#263;"
  ]
  node [
    id 123
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 124
    label "przywraca&#263;"
  ]
  node [
    id 125
    label "dawa&#263;"
  ]
  node [
    id 126
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 127
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 128
    label "convey"
  ]
  node [
    id 129
    label "publicize"
  ]
  node [
    id 130
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 131
    label "render"
  ]
  node [
    id 132
    label "uk&#322;ada&#263;"
  ]
  node [
    id 133
    label "opracowywa&#263;"
  ]
  node [
    id 134
    label "set"
  ]
  node [
    id 135
    label "oddawa&#263;"
  ]
  node [
    id 136
    label "train"
  ]
  node [
    id 137
    label "zmienia&#263;"
  ]
  node [
    id 138
    label "dzieli&#263;"
  ]
  node [
    id 139
    label "scala&#263;"
  ]
  node [
    id 140
    label "zestaw"
  ]
  node [
    id 141
    label "handicap"
  ]
  node [
    id 142
    label "przestawa&#263;"
  ]
  node [
    id 143
    label "transgress"
  ]
  node [
    id 144
    label "wadzi&#263;"
  ]
  node [
    id 145
    label "utrudnia&#263;"
  ]
  node [
    id 146
    label "kwitn&#261;&#263;"
  ]
  node [
    id 147
    label "siedzie&#263;"
  ]
  node [
    id 148
    label "repetowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
]
