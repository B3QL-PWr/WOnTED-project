graph [
  node [
    id 0
    label "zaras"
    origin "text"
  ]
  node [
    id 1
    label "spuszczem"
    origin "text"
  ]
  node [
    id 2
    label "pies"
    origin "text"
  ]
  node [
    id 3
    label "siem"
    origin "text"
  ]
  node [
    id 4
    label "uspokoi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "piese&#322;"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "Cerber"
  ]
  node [
    id 8
    label "szczeka&#263;"
  ]
  node [
    id 9
    label "&#322;ajdak"
  ]
  node [
    id 10
    label "kabanos"
  ]
  node [
    id 11
    label "wyzwisko"
  ]
  node [
    id 12
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 13
    label "samiec"
  ]
  node [
    id 14
    label "spragniony"
  ]
  node [
    id 15
    label "policjant"
  ]
  node [
    id 16
    label "rakarz"
  ]
  node [
    id 17
    label "szczu&#263;"
  ]
  node [
    id 18
    label "wycie"
  ]
  node [
    id 19
    label "istota_&#380;ywa"
  ]
  node [
    id 20
    label "trufla"
  ]
  node [
    id 21
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 22
    label "zawy&#263;"
  ]
  node [
    id 23
    label "sobaka"
  ]
  node [
    id 24
    label "dogoterapia"
  ]
  node [
    id 25
    label "s&#322;u&#380;enie"
  ]
  node [
    id 26
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 27
    label "psowate"
  ]
  node [
    id 28
    label "wy&#263;"
  ]
  node [
    id 29
    label "szczucie"
  ]
  node [
    id 30
    label "czworon&#243;g"
  ]
  node [
    id 31
    label "sympatyk"
  ]
  node [
    id 32
    label "entuzjasta"
  ]
  node [
    id 33
    label "critter"
  ]
  node [
    id 34
    label "zwierz&#281;_domowe"
  ]
  node [
    id 35
    label "kr&#281;gowiec"
  ]
  node [
    id 36
    label "tetrapody"
  ]
  node [
    id 37
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 38
    label "zwierz&#281;"
  ]
  node [
    id 39
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 40
    label "ludzko&#347;&#263;"
  ]
  node [
    id 41
    label "asymilowanie"
  ]
  node [
    id 42
    label "wapniak"
  ]
  node [
    id 43
    label "asymilowa&#263;"
  ]
  node [
    id 44
    label "os&#322;abia&#263;"
  ]
  node [
    id 45
    label "posta&#263;"
  ]
  node [
    id 46
    label "hominid"
  ]
  node [
    id 47
    label "podw&#322;adny"
  ]
  node [
    id 48
    label "os&#322;abianie"
  ]
  node [
    id 49
    label "g&#322;owa"
  ]
  node [
    id 50
    label "figura"
  ]
  node [
    id 51
    label "portrecista"
  ]
  node [
    id 52
    label "dwun&#243;g"
  ]
  node [
    id 53
    label "profanum"
  ]
  node [
    id 54
    label "mikrokosmos"
  ]
  node [
    id 55
    label "nasada"
  ]
  node [
    id 56
    label "duch"
  ]
  node [
    id 57
    label "antropochoria"
  ]
  node [
    id 58
    label "osoba"
  ]
  node [
    id 59
    label "wz&#243;r"
  ]
  node [
    id 60
    label "senior"
  ]
  node [
    id 61
    label "oddzia&#322;ywanie"
  ]
  node [
    id 62
    label "Adam"
  ]
  node [
    id 63
    label "homo_sapiens"
  ]
  node [
    id 64
    label "polifag"
  ]
  node [
    id 65
    label "palconogie"
  ]
  node [
    id 66
    label "stra&#380;nik"
  ]
  node [
    id 67
    label "wielog&#322;owy"
  ]
  node [
    id 68
    label "zooterapia"
  ]
  node [
    id 69
    label "przek&#261;ska"
  ]
  node [
    id 70
    label "w&#281;dzi&#263;"
  ]
  node [
    id 71
    label "przysmak"
  ]
  node [
    id 72
    label "kie&#322;basa"
  ]
  node [
    id 73
    label "cygaro"
  ]
  node [
    id 74
    label "kot"
  ]
  node [
    id 75
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 76
    label "bark"
  ]
  node [
    id 77
    label "m&#243;wi&#263;"
  ]
  node [
    id 78
    label "hum"
  ]
  node [
    id 79
    label "obgadywa&#263;"
  ]
  node [
    id 80
    label "kozio&#322;"
  ]
  node [
    id 81
    label "szcz&#281;ka&#263;"
  ]
  node [
    id 82
    label "karabin"
  ]
  node [
    id 83
    label "wymy&#347;la&#263;"
  ]
  node [
    id 84
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 85
    label "wydoby&#263;"
  ]
  node [
    id 86
    label "wilk"
  ]
  node [
    id 87
    label "rant"
  ]
  node [
    id 88
    label "rave"
  ]
  node [
    id 89
    label "zabrzmie&#263;"
  ]
  node [
    id 90
    label "&#380;o&#322;nierz"
  ]
  node [
    id 91
    label "robi&#263;"
  ]
  node [
    id 92
    label "by&#263;"
  ]
  node [
    id 93
    label "trwa&#263;"
  ]
  node [
    id 94
    label "use"
  ]
  node [
    id 95
    label "suffice"
  ]
  node [
    id 96
    label "cel"
  ]
  node [
    id 97
    label "pracowa&#263;"
  ]
  node [
    id 98
    label "match"
  ]
  node [
    id 99
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 100
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 101
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 102
    label "wait"
  ]
  node [
    id 103
    label "pomaga&#263;"
  ]
  node [
    id 104
    label "tease"
  ]
  node [
    id 105
    label "pod&#380;ega&#263;"
  ]
  node [
    id 106
    label "podjudza&#263;"
  ]
  node [
    id 107
    label "pracownik_komunalny"
  ]
  node [
    id 108
    label "powodowanie"
  ]
  node [
    id 109
    label "pod&#380;eganie"
  ]
  node [
    id 110
    label "atakowanie"
  ]
  node [
    id 111
    label "fomentation"
  ]
  node [
    id 112
    label "uczynny"
  ]
  node [
    id 113
    label "s&#322;ugiwanie"
  ]
  node [
    id 114
    label "pomaganie"
  ]
  node [
    id 115
    label "bycie"
  ]
  node [
    id 116
    label "wys&#322;u&#380;enie_si&#281;"
  ]
  node [
    id 117
    label "request"
  ]
  node [
    id 118
    label "trwanie"
  ]
  node [
    id 119
    label "robienie"
  ]
  node [
    id 120
    label "service"
  ]
  node [
    id 121
    label "przydawanie_si&#281;"
  ]
  node [
    id 122
    label "czynno&#347;&#263;"
  ]
  node [
    id 123
    label "pracowanie"
  ]
  node [
    id 124
    label "czekoladka"
  ]
  node [
    id 125
    label "afrodyzjak"
  ]
  node [
    id 126
    label "workowiec"
  ]
  node [
    id 127
    label "nos"
  ]
  node [
    id 128
    label "grzyb_owocnikowy"
  ]
  node [
    id 129
    label "truflowate"
  ]
  node [
    id 130
    label "grzyb_mikoryzowy"
  ]
  node [
    id 131
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 132
    label "wo&#322;anie"
  ]
  node [
    id 133
    label "wydobywanie"
  ]
  node [
    id 134
    label "brzmienie"
  ]
  node [
    id 135
    label "wydawanie"
  ]
  node [
    id 136
    label "d&#378;wi&#281;k"
  ]
  node [
    id 137
    label "whimper"
  ]
  node [
    id 138
    label "yip"
  ]
  node [
    id 139
    label "ha&#322;asowa&#263;"
  ]
  node [
    id 140
    label "fa&#322;szowa&#263;"
  ]
  node [
    id 141
    label "p&#322;aka&#263;"
  ]
  node [
    id 142
    label "snivel"
  ]
  node [
    id 143
    label "cholera"
  ]
  node [
    id 144
    label "wypowied&#378;"
  ]
  node [
    id 145
    label "chuj"
  ]
  node [
    id 146
    label "bluzg"
  ]
  node [
    id 147
    label "chujowy"
  ]
  node [
    id 148
    label "obelga"
  ]
  node [
    id 149
    label "szmata"
  ]
  node [
    id 150
    label "ch&#281;tny"
  ]
  node [
    id 151
    label "z&#322;akniony"
  ]
  node [
    id 152
    label "upodlenie_si&#281;"
  ]
  node [
    id 153
    label "skurwysyn"
  ]
  node [
    id 154
    label "upadlanie_si&#281;"
  ]
  node [
    id 155
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 156
    label "psubrat"
  ]
  node [
    id 157
    label "policja"
  ]
  node [
    id 158
    label "blacharz"
  ]
  node [
    id 159
    label "str&#243;&#380;"
  ]
  node [
    id 160
    label "pa&#322;a"
  ]
  node [
    id 161
    label "mundurowy"
  ]
  node [
    id 162
    label "glina"
  ]
  node [
    id 163
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 164
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 165
    label "accommodate"
  ]
  node [
    id 166
    label "zapanowa&#263;"
  ]
  node [
    id 167
    label "work"
  ]
  node [
    id 168
    label "chemia"
  ]
  node [
    id 169
    label "spowodowa&#263;"
  ]
  node [
    id 170
    label "reakcja_chemiczna"
  ]
  node [
    id 171
    label "act"
  ]
  node [
    id 172
    label "doprowadzi&#263;"
  ]
  node [
    id 173
    label "powstrzyma&#263;"
  ]
  node [
    id 174
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 175
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 176
    label "manipulate"
  ]
  node [
    id 177
    label "rule"
  ]
  node [
    id 178
    label "cope"
  ]
  node [
    id 179
    label "dosta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
]
