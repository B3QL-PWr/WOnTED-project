graph [
  node [
    id 0
    label "orze&#322;"
    origin "text"
  ]
  node [
    id 1
    label "murowany"
    origin "text"
  ]
  node [
    id 2
    label "gapa"
  ]
  node [
    id 3
    label "awers"
  ]
  node [
    id 4
    label "talent"
  ]
  node [
    id 5
    label "ptak_drapie&#380;ny"
  ]
  node [
    id 6
    label "or&#322;y"
  ]
  node [
    id 7
    label "bystrzak"
  ]
  node [
    id 8
    label "eagle"
  ]
  node [
    id 9
    label "brylant"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "dyspozycja"
  ]
  node [
    id 12
    label "gigant"
  ]
  node [
    id 13
    label "faculty"
  ]
  node [
    id 14
    label "stygmat"
  ]
  node [
    id 15
    label "moneta"
  ]
  node [
    id 16
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 17
    label "spryciarz"
  ]
  node [
    id 18
    label "wierzch"
  ]
  node [
    id 19
    label "strona"
  ]
  node [
    id 20
    label "jastrz&#281;biowate"
  ]
  node [
    id 21
    label "gamo&#324;"
  ]
  node [
    id 22
    label "wrona"
  ]
  node [
    id 23
    label "kania"
  ]
  node [
    id 24
    label "odznaka"
  ]
  node [
    id 25
    label "gawron"
  ]
  node [
    id 26
    label "murowanie"
  ]
  node [
    id 27
    label "pewny"
  ]
  node [
    id 28
    label "mo&#380;liwy"
  ]
  node [
    id 29
    label "bezpieczny"
  ]
  node [
    id 30
    label "spokojny"
  ]
  node [
    id 31
    label "pewnie"
  ]
  node [
    id 32
    label "upewnianie_si&#281;"
  ]
  node [
    id 33
    label "ufanie"
  ]
  node [
    id 34
    label "wierzenie"
  ]
  node [
    id 35
    label "upewnienie_si&#281;"
  ]
  node [
    id 36
    label "wiarygodny"
  ]
  node [
    id 37
    label "zamurowanie"
  ]
  node [
    id 38
    label "obmurowywanie"
  ]
  node [
    id 39
    label "budowanie"
  ]
  node [
    id 40
    label "murowa&#263;"
  ]
  node [
    id 41
    label "poczta"
  ]
  node [
    id 42
    label "polski"
  ]
  node [
    id 43
    label "powsta&#263;"
  ]
  node [
    id 44
    label "Chmielnicki"
  ]
  node [
    id 45
    label "&#347;wi&#281;ty"
  ]
  node [
    id 46
    label "Jan"
  ]
  node [
    id 47
    label "i"
  ]
  node [
    id 48
    label "W&#346;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
]
