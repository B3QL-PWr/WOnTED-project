graph [
  node [
    id 0
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 1
    label "minister"
    origin "text"
  ]
  node [
    id 2
    label "rolnictwo"
    origin "text"
  ]
  node [
    id 3
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 4
    label "wsi"
    origin "text"
  ]
  node [
    id 5
    label "arrangement"
  ]
  node [
    id 6
    label "zarz&#261;dzenie"
  ]
  node [
    id 7
    label "polecenie"
  ]
  node [
    id 8
    label "commission"
  ]
  node [
    id 9
    label "ordonans"
  ]
  node [
    id 10
    label "akt"
  ]
  node [
    id 11
    label "rule"
  ]
  node [
    id 12
    label "danie"
  ]
  node [
    id 13
    label "stipulation"
  ]
  node [
    id 14
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 15
    label "podnieci&#263;"
  ]
  node [
    id 16
    label "scena"
  ]
  node [
    id 17
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 18
    label "numer"
  ]
  node [
    id 19
    label "po&#380;ycie"
  ]
  node [
    id 20
    label "poj&#281;cie"
  ]
  node [
    id 21
    label "podniecenie"
  ]
  node [
    id 22
    label "nago&#347;&#263;"
  ]
  node [
    id 23
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 24
    label "fascyku&#322;"
  ]
  node [
    id 25
    label "seks"
  ]
  node [
    id 26
    label "podniecanie"
  ]
  node [
    id 27
    label "imisja"
  ]
  node [
    id 28
    label "zwyczaj"
  ]
  node [
    id 29
    label "rozmna&#380;anie"
  ]
  node [
    id 30
    label "ruch_frykcyjny"
  ]
  node [
    id 31
    label "ontologia"
  ]
  node [
    id 32
    label "wydarzenie"
  ]
  node [
    id 33
    label "na_pieska"
  ]
  node [
    id 34
    label "pozycja_misjonarska"
  ]
  node [
    id 35
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 36
    label "fragment"
  ]
  node [
    id 37
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 38
    label "z&#322;&#261;czenie"
  ]
  node [
    id 39
    label "czynno&#347;&#263;"
  ]
  node [
    id 40
    label "gra_wst&#281;pna"
  ]
  node [
    id 41
    label "erotyka"
  ]
  node [
    id 42
    label "urzeczywistnienie"
  ]
  node [
    id 43
    label "baraszki"
  ]
  node [
    id 44
    label "certificate"
  ]
  node [
    id 45
    label "po&#380;&#261;danie"
  ]
  node [
    id 46
    label "wzw&#243;d"
  ]
  node [
    id 47
    label "funkcja"
  ]
  node [
    id 48
    label "act"
  ]
  node [
    id 49
    label "dokument"
  ]
  node [
    id 50
    label "arystotelizm"
  ]
  node [
    id 51
    label "podnieca&#263;"
  ]
  node [
    id 52
    label "ukaz"
  ]
  node [
    id 53
    label "pognanie"
  ]
  node [
    id 54
    label "rekomendacja"
  ]
  node [
    id 55
    label "wypowied&#378;"
  ]
  node [
    id 56
    label "pobiegni&#281;cie"
  ]
  node [
    id 57
    label "education"
  ]
  node [
    id 58
    label "doradzenie"
  ]
  node [
    id 59
    label "statement"
  ]
  node [
    id 60
    label "recommendation"
  ]
  node [
    id 61
    label "zadanie"
  ]
  node [
    id 62
    label "zaordynowanie"
  ]
  node [
    id 63
    label "powierzenie"
  ]
  node [
    id 64
    label "przesadzenie"
  ]
  node [
    id 65
    label "consign"
  ]
  node [
    id 66
    label "dekret"
  ]
  node [
    id 67
    label "dostojnik"
  ]
  node [
    id 68
    label "Goebbels"
  ]
  node [
    id 69
    label "Sto&#322;ypin"
  ]
  node [
    id 70
    label "rz&#261;d"
  ]
  node [
    id 71
    label "przybli&#380;enie"
  ]
  node [
    id 72
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 73
    label "kategoria"
  ]
  node [
    id 74
    label "szpaler"
  ]
  node [
    id 75
    label "lon&#380;a"
  ]
  node [
    id 76
    label "uporz&#261;dkowanie"
  ]
  node [
    id 77
    label "egzekutywa"
  ]
  node [
    id 78
    label "jednostka_systematyczna"
  ]
  node [
    id 79
    label "instytucja"
  ]
  node [
    id 80
    label "premier"
  ]
  node [
    id 81
    label "Londyn"
  ]
  node [
    id 82
    label "gabinet_cieni"
  ]
  node [
    id 83
    label "gromada"
  ]
  node [
    id 84
    label "number"
  ]
  node [
    id 85
    label "Konsulat"
  ]
  node [
    id 86
    label "tract"
  ]
  node [
    id 87
    label "klasa"
  ]
  node [
    id 88
    label "w&#322;adza"
  ]
  node [
    id 89
    label "urz&#281;dnik"
  ]
  node [
    id 90
    label "notabl"
  ]
  node [
    id 91
    label "oficja&#322;"
  ]
  node [
    id 92
    label "nasiennictwo"
  ]
  node [
    id 93
    label "agrotechnika"
  ]
  node [
    id 94
    label "agroekologia"
  ]
  node [
    id 95
    label "agrobiznes"
  ]
  node [
    id 96
    label "intensyfikacja"
  ]
  node [
    id 97
    label "uprawianie"
  ]
  node [
    id 98
    label "gleboznawstwo"
  ]
  node [
    id 99
    label "gospodarka"
  ]
  node [
    id 100
    label "ogrodnictwo"
  ]
  node [
    id 101
    label "agronomia"
  ]
  node [
    id 102
    label "agrochemia"
  ]
  node [
    id 103
    label "farmerstwo"
  ]
  node [
    id 104
    label "zootechnika"
  ]
  node [
    id 105
    label "zgarniacz"
  ]
  node [
    id 106
    label "nauka"
  ]
  node [
    id 107
    label "hodowla"
  ]
  node [
    id 108
    label "sadownictwo"
  ]
  node [
    id 109
    label "&#322;&#261;karstwo"
  ]
  node [
    id 110
    label "&#322;owiectwo"
  ]
  node [
    id 111
    label "wytropienie"
  ]
  node [
    id 112
    label "podkurza&#263;"
  ]
  node [
    id 113
    label "wytropi&#263;"
  ]
  node [
    id 114
    label "polowanie"
  ]
  node [
    id 115
    label "tropi&#263;"
  ]
  node [
    id 116
    label "tropienie"
  ]
  node [
    id 117
    label "blood_sport"
  ]
  node [
    id 118
    label "defilowa&#263;"
  ]
  node [
    id 119
    label "gospodarka_le&#347;na"
  ]
  node [
    id 120
    label "pielenie"
  ]
  node [
    id 121
    label "culture"
  ]
  node [
    id 122
    label "sianie"
  ]
  node [
    id 123
    label "zbi&#243;r"
  ]
  node [
    id 124
    label "stanowisko"
  ]
  node [
    id 125
    label "sadzenie"
  ]
  node [
    id 126
    label "oprysk"
  ]
  node [
    id 127
    label "szczepienie"
  ]
  node [
    id 128
    label "orka"
  ]
  node [
    id 129
    label "siew"
  ]
  node [
    id 130
    label "exercise"
  ]
  node [
    id 131
    label "koszenie"
  ]
  node [
    id 132
    label "obrabianie"
  ]
  node [
    id 133
    label "zajmowanie_si&#281;"
  ]
  node [
    id 134
    label "use"
  ]
  node [
    id 135
    label "biotechnika"
  ]
  node [
    id 136
    label "hodowanie"
  ]
  node [
    id 137
    label "potrzymanie"
  ]
  node [
    id 138
    label "praca_rolnicza"
  ]
  node [
    id 139
    label "pod&#243;j"
  ]
  node [
    id 140
    label "filiacja"
  ]
  node [
    id 141
    label "licencjonowanie"
  ]
  node [
    id 142
    label "opasa&#263;"
  ]
  node [
    id 143
    label "ch&#243;w"
  ]
  node [
    id 144
    label "licencja"
  ]
  node [
    id 145
    label "sokolarnia"
  ]
  node [
    id 146
    label "potrzyma&#263;"
  ]
  node [
    id 147
    label "rozp&#322;&#243;d"
  ]
  node [
    id 148
    label "grupa_organizm&#243;w"
  ]
  node [
    id 149
    label "wypas"
  ]
  node [
    id 150
    label "wychowalnia"
  ]
  node [
    id 151
    label "pstr&#261;garnia"
  ]
  node [
    id 152
    label "krzy&#380;owanie"
  ]
  node [
    id 153
    label "licencjonowa&#263;"
  ]
  node [
    id 154
    label "odch&#243;w"
  ]
  node [
    id 155
    label "tucz"
  ]
  node [
    id 156
    label "ud&#243;j"
  ]
  node [
    id 157
    label "klatka"
  ]
  node [
    id 158
    label "opasienie"
  ]
  node [
    id 159
    label "wych&#243;w"
  ]
  node [
    id 160
    label "obrz&#261;dek"
  ]
  node [
    id 161
    label "opasanie"
  ]
  node [
    id 162
    label "polish"
  ]
  node [
    id 163
    label "akwarium"
  ]
  node [
    id 164
    label "produkcja"
  ]
  node [
    id 165
    label "poligonizacja"
  ]
  node [
    id 166
    label "intensywny"
  ]
  node [
    id 167
    label "&#347;ci&#243;&#322;kowanie"
  ]
  node [
    id 168
    label "szk&#243;&#322;karstwo"
  ]
  node [
    id 169
    label "hydro&#380;el"
  ]
  node [
    id 170
    label "ekstensywny"
  ]
  node [
    id 171
    label "ekologia"
  ]
  node [
    id 172
    label "agrologia"
  ]
  node [
    id 173
    label "agrofizyka"
  ]
  node [
    id 174
    label "zoohigiena"
  ]
  node [
    id 175
    label "pomologia"
  ]
  node [
    id 176
    label "przemys&#322;_spo&#380;ywczy"
  ]
  node [
    id 177
    label "agroturystyka"
  ]
  node [
    id 178
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 179
    label "biznes"
  ]
  node [
    id 180
    label "inwentarz"
  ]
  node [
    id 181
    label "rynek"
  ]
  node [
    id 182
    label "mieszkalnictwo"
  ]
  node [
    id 183
    label "agregat_ekonomiczny"
  ]
  node [
    id 184
    label "miejsce_pracy"
  ]
  node [
    id 185
    label "produkowanie"
  ]
  node [
    id 186
    label "farmaceutyka"
  ]
  node [
    id 187
    label "transport"
  ]
  node [
    id 188
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 189
    label "obronno&#347;&#263;"
  ]
  node [
    id 190
    label "sektor_prywatny"
  ]
  node [
    id 191
    label "sch&#322;adza&#263;"
  ]
  node [
    id 192
    label "czerwona_strefa"
  ]
  node [
    id 193
    label "struktura"
  ]
  node [
    id 194
    label "pole"
  ]
  node [
    id 195
    label "sektor_publiczny"
  ]
  node [
    id 196
    label "bankowo&#347;&#263;"
  ]
  node [
    id 197
    label "gospodarowanie"
  ]
  node [
    id 198
    label "obora"
  ]
  node [
    id 199
    label "gospodarka_wodna"
  ]
  node [
    id 200
    label "gospodarowa&#263;"
  ]
  node [
    id 201
    label "fabryka"
  ]
  node [
    id 202
    label "wytw&#243;rnia"
  ]
  node [
    id 203
    label "stodo&#322;a"
  ]
  node [
    id 204
    label "przemys&#322;"
  ]
  node [
    id 205
    label "spichlerz"
  ]
  node [
    id 206
    label "sch&#322;adzanie"
  ]
  node [
    id 207
    label "administracja"
  ]
  node [
    id 208
    label "sch&#322;odzenie"
  ]
  node [
    id 209
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 210
    label "zasada"
  ]
  node [
    id 211
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 212
    label "regulacja_cen"
  ]
  node [
    id 213
    label "szkolnictwo"
  ]
  node [
    id 214
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 215
    label "o&#380;ywienie"
  ]
  node [
    id 216
    label "zwi&#281;kszenie"
  ]
  node [
    id 217
    label "budownictwo"
  ]
  node [
    id 218
    label "scraper"
  ]
  node [
    id 219
    label "g&#243;rnictwo"
  ]
  node [
    id 220
    label "urz&#261;dzenie"
  ]
  node [
    id 221
    label "wiedza"
  ]
  node [
    id 222
    label "miasteczko_rowerowe"
  ]
  node [
    id 223
    label "porada"
  ]
  node [
    id 224
    label "fotowoltaika"
  ]
  node [
    id 225
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 226
    label "przem&#243;wienie"
  ]
  node [
    id 227
    label "nauki_o_poznaniu"
  ]
  node [
    id 228
    label "nomotetyczny"
  ]
  node [
    id 229
    label "systematyka"
  ]
  node [
    id 230
    label "proces"
  ]
  node [
    id 231
    label "typologia"
  ]
  node [
    id 232
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 233
    label "kultura_duchowa"
  ]
  node [
    id 234
    label "&#322;awa_szkolna"
  ]
  node [
    id 235
    label "nauki_penalne"
  ]
  node [
    id 236
    label "dziedzina"
  ]
  node [
    id 237
    label "imagineskopia"
  ]
  node [
    id 238
    label "teoria_naukowa"
  ]
  node [
    id 239
    label "inwentyka"
  ]
  node [
    id 240
    label "metodologia"
  ]
  node [
    id 241
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 242
    label "nauki_o_Ziemi"
  ]
  node [
    id 243
    label "procedura"
  ]
  node [
    id 244
    label "&#380;ycie"
  ]
  node [
    id 245
    label "proces_biologiczny"
  ]
  node [
    id 246
    label "z&#322;ote_czasy"
  ]
  node [
    id 247
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 248
    label "process"
  ]
  node [
    id 249
    label "cycle"
  ]
  node [
    id 250
    label "kognicja"
  ]
  node [
    id 251
    label "przebieg"
  ]
  node [
    id 252
    label "rozprawa"
  ]
  node [
    id 253
    label "legislacyjnie"
  ]
  node [
    id 254
    label "przes&#322;anka"
  ]
  node [
    id 255
    label "zjawisko"
  ]
  node [
    id 256
    label "nast&#281;pstwo"
  ]
  node [
    id 257
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 258
    label "raj_utracony"
  ]
  node [
    id 259
    label "umieranie"
  ]
  node [
    id 260
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 261
    label "prze&#380;ywanie"
  ]
  node [
    id 262
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 263
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 264
    label "po&#322;&#243;g"
  ]
  node [
    id 265
    label "umarcie"
  ]
  node [
    id 266
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 267
    label "subsistence"
  ]
  node [
    id 268
    label "power"
  ]
  node [
    id 269
    label "okres_noworodkowy"
  ]
  node [
    id 270
    label "prze&#380;ycie"
  ]
  node [
    id 271
    label "wiek_matuzalemowy"
  ]
  node [
    id 272
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 273
    label "entity"
  ]
  node [
    id 274
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 275
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 276
    label "do&#380;ywanie"
  ]
  node [
    id 277
    label "byt"
  ]
  node [
    id 278
    label "andropauza"
  ]
  node [
    id 279
    label "dzieci&#324;stwo"
  ]
  node [
    id 280
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 281
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 282
    label "czas"
  ]
  node [
    id 283
    label "menopauza"
  ]
  node [
    id 284
    label "&#347;mier&#263;"
  ]
  node [
    id 285
    label "koleje_losu"
  ]
  node [
    id 286
    label "bycie"
  ]
  node [
    id 287
    label "zegar_biologiczny"
  ]
  node [
    id 288
    label "szwung"
  ]
  node [
    id 289
    label "przebywanie"
  ]
  node [
    id 290
    label "warunki"
  ]
  node [
    id 291
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 292
    label "niemowl&#281;ctwo"
  ]
  node [
    id 293
    label "&#380;ywy"
  ]
  node [
    id 294
    label "life"
  ]
  node [
    id 295
    label "staro&#347;&#263;"
  ]
  node [
    id 296
    label "energy"
  ]
  node [
    id 297
    label "brak"
  ]
  node [
    id 298
    label "s&#261;d"
  ]
  node [
    id 299
    label "facylitator"
  ]
  node [
    id 300
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 301
    label "tryb"
  ]
  node [
    id 302
    label "metodyka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
]
