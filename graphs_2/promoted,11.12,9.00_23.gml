graph [
  node [
    id 0
    label "fallout"
    origin "text"
  ]
  node [
    id 1
    label "padaka"
    origin "text"
  ]
  node [
    id 2
    label "wy&#347;miewanie_si&#281;"
  ]
  node [
    id 3
    label "bankructwo"
  ]
  node [
    id 4
    label "ubaw"
  ]
  node [
    id 5
    label "&#347;miech"
  ]
  node [
    id 6
    label "szyderstwo"
  ]
  node [
    id 7
    label "odg&#322;os"
  ]
  node [
    id 8
    label "reakcja"
  ]
  node [
    id 9
    label "zabawa"
  ]
  node [
    id 10
    label "rado&#347;&#263;"
  ]
  node [
    id 11
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 12
    label "syndyk"
  ]
  node [
    id 13
    label "rujnowanie"
  ]
  node [
    id 14
    label "cecha"
  ]
  node [
    id 15
    label "propozycja_uk&#322;adowa"
  ]
  node [
    id 16
    label "sytuacja"
  ]
  node [
    id 17
    label "Newa"
  ]
  node [
    id 18
    label "Vegas"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 17
    target 18
  ]
]
