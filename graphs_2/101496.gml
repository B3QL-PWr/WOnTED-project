graph [
  node [
    id 0
    label "kultura"
    origin "text"
  ]
  node [
    id 1
    label "lendzielski"
    origin "text"
  ]
  node [
    id 2
    label "asymilowanie_si&#281;"
  ]
  node [
    id 3
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 4
    label "Wsch&#243;d"
  ]
  node [
    id 5
    label "przedmiot"
  ]
  node [
    id 6
    label "praca_rolnicza"
  ]
  node [
    id 7
    label "przejmowanie"
  ]
  node [
    id 8
    label "zjawisko"
  ]
  node [
    id 9
    label "cecha"
  ]
  node [
    id 10
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 11
    label "makrokosmos"
  ]
  node [
    id 12
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "konwencja"
  ]
  node [
    id 14
    label "rzecz"
  ]
  node [
    id 15
    label "propriety"
  ]
  node [
    id 16
    label "przejmowa&#263;"
  ]
  node [
    id 17
    label "brzoskwiniarnia"
  ]
  node [
    id 18
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 19
    label "sztuka"
  ]
  node [
    id 20
    label "zwyczaj"
  ]
  node [
    id 21
    label "jako&#347;&#263;"
  ]
  node [
    id 22
    label "kuchnia"
  ]
  node [
    id 23
    label "tradycja"
  ]
  node [
    id 24
    label "populace"
  ]
  node [
    id 25
    label "hodowla"
  ]
  node [
    id 26
    label "religia"
  ]
  node [
    id 27
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 28
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 29
    label "przej&#281;cie"
  ]
  node [
    id 30
    label "przej&#261;&#263;"
  ]
  node [
    id 31
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 33
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 34
    label "warto&#347;&#263;"
  ]
  node [
    id 35
    label "quality"
  ]
  node [
    id 36
    label "co&#347;"
  ]
  node [
    id 37
    label "state"
  ]
  node [
    id 38
    label "syf"
  ]
  node [
    id 39
    label "absolutorium"
  ]
  node [
    id 40
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 41
    label "dzia&#322;anie"
  ]
  node [
    id 42
    label "activity"
  ]
  node [
    id 43
    label "proces"
  ]
  node [
    id 44
    label "boski"
  ]
  node [
    id 45
    label "krajobraz"
  ]
  node [
    id 46
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 47
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 48
    label "przywidzenie"
  ]
  node [
    id 49
    label "presence"
  ]
  node [
    id 50
    label "charakter"
  ]
  node [
    id 51
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 52
    label "potrzymanie"
  ]
  node [
    id 53
    label "rolnictwo"
  ]
  node [
    id 54
    label "pod&#243;j"
  ]
  node [
    id 55
    label "filiacja"
  ]
  node [
    id 56
    label "licencjonowanie"
  ]
  node [
    id 57
    label "opasa&#263;"
  ]
  node [
    id 58
    label "ch&#243;w"
  ]
  node [
    id 59
    label "licencja"
  ]
  node [
    id 60
    label "sokolarnia"
  ]
  node [
    id 61
    label "potrzyma&#263;"
  ]
  node [
    id 62
    label "rozp&#322;&#243;d"
  ]
  node [
    id 63
    label "grupa_organizm&#243;w"
  ]
  node [
    id 64
    label "wypas"
  ]
  node [
    id 65
    label "wychowalnia"
  ]
  node [
    id 66
    label "pstr&#261;garnia"
  ]
  node [
    id 67
    label "krzy&#380;owanie"
  ]
  node [
    id 68
    label "licencjonowa&#263;"
  ]
  node [
    id 69
    label "odch&#243;w"
  ]
  node [
    id 70
    label "tucz"
  ]
  node [
    id 71
    label "ud&#243;j"
  ]
  node [
    id 72
    label "klatka"
  ]
  node [
    id 73
    label "opasienie"
  ]
  node [
    id 74
    label "wych&#243;w"
  ]
  node [
    id 75
    label "obrz&#261;dek"
  ]
  node [
    id 76
    label "opasanie"
  ]
  node [
    id 77
    label "polish"
  ]
  node [
    id 78
    label "akwarium"
  ]
  node [
    id 79
    label "biotechnika"
  ]
  node [
    id 80
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 81
    label "zbi&#243;r"
  ]
  node [
    id 82
    label "uk&#322;ad"
  ]
  node [
    id 83
    label "styl"
  ]
  node [
    id 84
    label "line"
  ]
  node [
    id 85
    label "kanon"
  ]
  node [
    id 86
    label "zjazd"
  ]
  node [
    id 87
    label "charakterystyka"
  ]
  node [
    id 88
    label "m&#322;ot"
  ]
  node [
    id 89
    label "znak"
  ]
  node [
    id 90
    label "drzewo"
  ]
  node [
    id 91
    label "pr&#243;ba"
  ]
  node [
    id 92
    label "attribute"
  ]
  node [
    id 93
    label "marka"
  ]
  node [
    id 94
    label "biom"
  ]
  node [
    id 95
    label "szata_ro&#347;linna"
  ]
  node [
    id 96
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 97
    label "formacja_ro&#347;linna"
  ]
  node [
    id 98
    label "przyroda"
  ]
  node [
    id 99
    label "zielono&#347;&#263;"
  ]
  node [
    id 100
    label "pi&#281;tro"
  ]
  node [
    id 101
    label "plant"
  ]
  node [
    id 102
    label "ro&#347;lina"
  ]
  node [
    id 103
    label "geosystem"
  ]
  node [
    id 104
    label "kult"
  ]
  node [
    id 105
    label "wyznanie"
  ]
  node [
    id 106
    label "mitologia"
  ]
  node [
    id 107
    label "ideologia"
  ]
  node [
    id 108
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 109
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 110
    label "nawracanie_si&#281;"
  ]
  node [
    id 111
    label "duchowny"
  ]
  node [
    id 112
    label "rela"
  ]
  node [
    id 113
    label "kultura_duchowa"
  ]
  node [
    id 114
    label "kosmologia"
  ]
  node [
    id 115
    label "kosmogonia"
  ]
  node [
    id 116
    label "nawraca&#263;"
  ]
  node [
    id 117
    label "mistyka"
  ]
  node [
    id 118
    label "pr&#243;bowanie"
  ]
  node [
    id 119
    label "rola"
  ]
  node [
    id 120
    label "cz&#322;owiek"
  ]
  node [
    id 121
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 122
    label "realizacja"
  ]
  node [
    id 123
    label "scena"
  ]
  node [
    id 124
    label "didaskalia"
  ]
  node [
    id 125
    label "czyn"
  ]
  node [
    id 126
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 127
    label "environment"
  ]
  node [
    id 128
    label "head"
  ]
  node [
    id 129
    label "scenariusz"
  ]
  node [
    id 130
    label "egzemplarz"
  ]
  node [
    id 131
    label "jednostka"
  ]
  node [
    id 132
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 133
    label "utw&#243;r"
  ]
  node [
    id 134
    label "fortel"
  ]
  node [
    id 135
    label "theatrical_performance"
  ]
  node [
    id 136
    label "ambala&#380;"
  ]
  node [
    id 137
    label "sprawno&#347;&#263;"
  ]
  node [
    id 138
    label "kobieta"
  ]
  node [
    id 139
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 140
    label "Faust"
  ]
  node [
    id 141
    label "scenografia"
  ]
  node [
    id 142
    label "ods&#322;ona"
  ]
  node [
    id 143
    label "turn"
  ]
  node [
    id 144
    label "pokaz"
  ]
  node [
    id 145
    label "ilo&#347;&#263;"
  ]
  node [
    id 146
    label "przedstawienie"
  ]
  node [
    id 147
    label "przedstawi&#263;"
  ]
  node [
    id 148
    label "Apollo"
  ]
  node [
    id 149
    label "przedstawianie"
  ]
  node [
    id 150
    label "przedstawia&#263;"
  ]
  node [
    id 151
    label "towar"
  ]
  node [
    id 152
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 153
    label "zachowanie"
  ]
  node [
    id 154
    label "ceremony"
  ]
  node [
    id 155
    label "dorobek"
  ]
  node [
    id 156
    label "tworzenie"
  ]
  node [
    id 157
    label "kreacja"
  ]
  node [
    id 158
    label "creation"
  ]
  node [
    id 159
    label "staro&#347;cina_weselna"
  ]
  node [
    id 160
    label "folklor"
  ]
  node [
    id 161
    label "objawienie"
  ]
  node [
    id 162
    label "zaj&#281;cie"
  ]
  node [
    id 163
    label "instytucja"
  ]
  node [
    id 164
    label "tajniki"
  ]
  node [
    id 165
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 166
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 167
    label "jedzenie"
  ]
  node [
    id 168
    label "zaplecze"
  ]
  node [
    id 169
    label "pomieszczenie"
  ]
  node [
    id 170
    label "zlewozmywak"
  ]
  node [
    id 171
    label "gotowa&#263;"
  ]
  node [
    id 172
    label "ciemna_materia"
  ]
  node [
    id 173
    label "planeta"
  ]
  node [
    id 174
    label "mikrokosmos"
  ]
  node [
    id 175
    label "ekosfera"
  ]
  node [
    id 176
    label "przestrze&#324;"
  ]
  node [
    id 177
    label "czarna_dziura"
  ]
  node [
    id 178
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 179
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 180
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 181
    label "kosmos"
  ]
  node [
    id 182
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 183
    label "poprawno&#347;&#263;"
  ]
  node [
    id 184
    label "og&#322;ada"
  ]
  node [
    id 185
    label "service"
  ]
  node [
    id 186
    label "stosowno&#347;&#263;"
  ]
  node [
    id 187
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 188
    label "Ukraina"
  ]
  node [
    id 189
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 190
    label "blok_wschodni"
  ]
  node [
    id 191
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 192
    label "wsch&#243;d"
  ]
  node [
    id 193
    label "Europa_Wschodnia"
  ]
  node [
    id 194
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 195
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 196
    label "wra&#380;enie"
  ]
  node [
    id 197
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 198
    label "interception"
  ]
  node [
    id 199
    label "wzbudzenie"
  ]
  node [
    id 200
    label "emotion"
  ]
  node [
    id 201
    label "movement"
  ]
  node [
    id 202
    label "zaczerpni&#281;cie"
  ]
  node [
    id 203
    label "wzi&#281;cie"
  ]
  node [
    id 204
    label "bang"
  ]
  node [
    id 205
    label "wzi&#261;&#263;"
  ]
  node [
    id 206
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 207
    label "stimulate"
  ]
  node [
    id 208
    label "ogarn&#261;&#263;"
  ]
  node [
    id 209
    label "wzbudzi&#263;"
  ]
  node [
    id 210
    label "thrill"
  ]
  node [
    id 211
    label "treat"
  ]
  node [
    id 212
    label "czerpa&#263;"
  ]
  node [
    id 213
    label "bra&#263;"
  ]
  node [
    id 214
    label "go"
  ]
  node [
    id 215
    label "handle"
  ]
  node [
    id 216
    label "wzbudza&#263;"
  ]
  node [
    id 217
    label "ogarnia&#263;"
  ]
  node [
    id 218
    label "czerpanie"
  ]
  node [
    id 219
    label "acquisition"
  ]
  node [
    id 220
    label "branie"
  ]
  node [
    id 221
    label "caparison"
  ]
  node [
    id 222
    label "wzbudzanie"
  ]
  node [
    id 223
    label "czynno&#347;&#263;"
  ]
  node [
    id 224
    label "ogarnianie"
  ]
  node [
    id 225
    label "object"
  ]
  node [
    id 226
    label "temat"
  ]
  node [
    id 227
    label "wpadni&#281;cie"
  ]
  node [
    id 228
    label "mienie"
  ]
  node [
    id 229
    label "istota"
  ]
  node [
    id 230
    label "obiekt"
  ]
  node [
    id 231
    label "wpa&#347;&#263;"
  ]
  node [
    id 232
    label "wpadanie"
  ]
  node [
    id 233
    label "wpada&#263;"
  ]
  node [
    id 234
    label "zboczenie"
  ]
  node [
    id 235
    label "om&#243;wienie"
  ]
  node [
    id 236
    label "sponiewieranie"
  ]
  node [
    id 237
    label "discipline"
  ]
  node [
    id 238
    label "omawia&#263;"
  ]
  node [
    id 239
    label "kr&#261;&#380;enie"
  ]
  node [
    id 240
    label "tre&#347;&#263;"
  ]
  node [
    id 241
    label "robienie"
  ]
  node [
    id 242
    label "sponiewiera&#263;"
  ]
  node [
    id 243
    label "element"
  ]
  node [
    id 244
    label "entity"
  ]
  node [
    id 245
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 246
    label "tematyka"
  ]
  node [
    id 247
    label "w&#261;tek"
  ]
  node [
    id 248
    label "zbaczanie"
  ]
  node [
    id 249
    label "program_nauczania"
  ]
  node [
    id 250
    label "om&#243;wi&#263;"
  ]
  node [
    id 251
    label "omawianie"
  ]
  node [
    id 252
    label "thing"
  ]
  node [
    id 253
    label "zbacza&#263;"
  ]
  node [
    id 254
    label "zboczy&#263;"
  ]
  node [
    id 255
    label "miejsce"
  ]
  node [
    id 256
    label "uprawa"
  ]
  node [
    id 257
    label "g&#243;rny"
  ]
  node [
    id 258
    label "&#346;l&#261;sk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 257
    target 258
  ]
]
