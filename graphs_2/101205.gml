graph [
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "fundacja"
    origin "text"
  ]
  node [
    id 2
    label "upowszechnia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nauka"
    origin "text"
  ]
  node [
    id 4
    label "darowizna"
  ]
  node [
    id 5
    label "foundation"
  ]
  node [
    id 6
    label "instytucja"
  ]
  node [
    id 7
    label "dar"
  ]
  node [
    id 8
    label "pocz&#261;tek"
  ]
  node [
    id 9
    label "osoba_prawna"
  ]
  node [
    id 10
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 11
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 12
    label "poj&#281;cie"
  ]
  node [
    id 13
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 14
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 15
    label "biuro"
  ]
  node [
    id 16
    label "organizacja"
  ]
  node [
    id 17
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 18
    label "Fundusze_Unijne"
  ]
  node [
    id 19
    label "zamyka&#263;"
  ]
  node [
    id 20
    label "establishment"
  ]
  node [
    id 21
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 22
    label "urz&#261;d"
  ]
  node [
    id 23
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 24
    label "afiliowa&#263;"
  ]
  node [
    id 25
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 26
    label "standard"
  ]
  node [
    id 27
    label "zamykanie"
  ]
  node [
    id 28
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 29
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 30
    label "pierworodztwo"
  ]
  node [
    id 31
    label "faza"
  ]
  node [
    id 32
    label "miejsce"
  ]
  node [
    id 33
    label "upgrade"
  ]
  node [
    id 34
    label "nast&#281;pstwo"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 36
    label "przeniesienie_praw"
  ]
  node [
    id 37
    label "zapomoga"
  ]
  node [
    id 38
    label "transakcja"
  ]
  node [
    id 39
    label "dyspozycja"
  ]
  node [
    id 40
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 41
    label "da&#324;"
  ]
  node [
    id 42
    label "faculty"
  ]
  node [
    id 43
    label "stygmat"
  ]
  node [
    id 44
    label "dobro"
  ]
  node [
    id 45
    label "rzecz"
  ]
  node [
    id 46
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 47
    label "generalize"
  ]
  node [
    id 48
    label "sprawia&#263;"
  ]
  node [
    id 49
    label "kupywa&#263;"
  ]
  node [
    id 50
    label "bra&#263;"
  ]
  node [
    id 51
    label "bind"
  ]
  node [
    id 52
    label "get"
  ]
  node [
    id 53
    label "act"
  ]
  node [
    id 54
    label "powodowa&#263;"
  ]
  node [
    id 55
    label "przygotowywa&#263;"
  ]
  node [
    id 56
    label "wiedza"
  ]
  node [
    id 57
    label "miasteczko_rowerowe"
  ]
  node [
    id 58
    label "porada"
  ]
  node [
    id 59
    label "fotowoltaika"
  ]
  node [
    id 60
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 61
    label "przem&#243;wienie"
  ]
  node [
    id 62
    label "nauki_o_poznaniu"
  ]
  node [
    id 63
    label "nomotetyczny"
  ]
  node [
    id 64
    label "systematyka"
  ]
  node [
    id 65
    label "proces"
  ]
  node [
    id 66
    label "typologia"
  ]
  node [
    id 67
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 68
    label "kultura_duchowa"
  ]
  node [
    id 69
    label "&#322;awa_szkolna"
  ]
  node [
    id 70
    label "nauki_penalne"
  ]
  node [
    id 71
    label "dziedzina"
  ]
  node [
    id 72
    label "imagineskopia"
  ]
  node [
    id 73
    label "teoria_naukowa"
  ]
  node [
    id 74
    label "inwentyka"
  ]
  node [
    id 75
    label "metodologia"
  ]
  node [
    id 76
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 77
    label "nauki_o_Ziemi"
  ]
  node [
    id 78
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 79
    label "sfera"
  ]
  node [
    id 80
    label "zbi&#243;r"
  ]
  node [
    id 81
    label "zakres"
  ]
  node [
    id 82
    label "funkcja"
  ]
  node [
    id 83
    label "bezdro&#380;e"
  ]
  node [
    id 84
    label "poddzia&#322;"
  ]
  node [
    id 85
    label "kognicja"
  ]
  node [
    id 86
    label "przebieg"
  ]
  node [
    id 87
    label "rozprawa"
  ]
  node [
    id 88
    label "wydarzenie"
  ]
  node [
    id 89
    label "legislacyjnie"
  ]
  node [
    id 90
    label "przes&#322;anka"
  ]
  node [
    id 91
    label "zjawisko"
  ]
  node [
    id 92
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 93
    label "zrozumienie"
  ]
  node [
    id 94
    label "obronienie"
  ]
  node [
    id 95
    label "wydanie"
  ]
  node [
    id 96
    label "wyg&#322;oszenie"
  ]
  node [
    id 97
    label "wypowied&#378;"
  ]
  node [
    id 98
    label "oddzia&#322;anie"
  ]
  node [
    id 99
    label "address"
  ]
  node [
    id 100
    label "wydobycie"
  ]
  node [
    id 101
    label "wyst&#261;pienie"
  ]
  node [
    id 102
    label "talk"
  ]
  node [
    id 103
    label "odzyskanie"
  ]
  node [
    id 104
    label "sermon"
  ]
  node [
    id 105
    label "cognition"
  ]
  node [
    id 106
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 107
    label "intelekt"
  ]
  node [
    id 108
    label "pozwolenie"
  ]
  node [
    id 109
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 110
    label "zaawansowanie"
  ]
  node [
    id 111
    label "wykszta&#322;cenie"
  ]
  node [
    id 112
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 113
    label "wskaz&#243;wka"
  ]
  node [
    id 114
    label "technika"
  ]
  node [
    id 115
    label "typology"
  ]
  node [
    id 116
    label "podzia&#322;"
  ]
  node [
    id 117
    label "kwantyfikacja"
  ]
  node [
    id 118
    label "taksonomia"
  ]
  node [
    id 119
    label "biosystematyka"
  ]
  node [
    id 120
    label "biologia"
  ]
  node [
    id 121
    label "kohorta"
  ]
  node [
    id 122
    label "kladystyka"
  ]
  node [
    id 123
    label "aparat_krytyczny"
  ]
  node [
    id 124
    label "funkcjonalizm"
  ]
  node [
    id 125
    label "wyobra&#378;nia"
  ]
  node [
    id 126
    label "charakterystyczny"
  ]
  node [
    id 127
    label "polski"
  ]
  node [
    id 128
    label "akademia"
  ]
  node [
    id 129
    label "towarzystwo"
  ]
  node [
    id 130
    label "popiera&#263;"
  ]
  node [
    id 131
    label "i"
  ]
  node [
    id 132
    label "krzewi&#263;"
  ]
  node [
    id 133
    label "wolny"
  ]
  node [
    id 134
    label "wszechnica"
  ]
  node [
    id 135
    label "polskie"
  ]
  node [
    id 136
    label "stowarzyszy&#263;"
  ]
  node [
    id 137
    label "film"
  ]
  node [
    id 138
    label "naukowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 129
  ]
  edge [
    source 127
    target 133
  ]
  edge [
    source 127
    target 134
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 131
  ]
  edge [
    source 129
    target 132
  ]
  edge [
    source 129
    target 133
  ]
  edge [
    source 129
    target 134
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 132
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 137
  ]
  edge [
    source 135
    target 138
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 136
    target 138
  ]
  edge [
    source 137
    target 138
  ]
]
