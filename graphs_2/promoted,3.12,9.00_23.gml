graph [
  node [
    id 0
    label "staro&#380;ytno&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "epoka"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "kojarzy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "bardzo"
    origin "text"
  ]
  node [
    id 7
    label "pompatycznie"
    origin "text"
  ]
  node [
    id 8
    label "styl_kompozytowy"
  ]
  node [
    id 9
    label "aretalogia"
  ]
  node [
    id 10
    label "styl_dorycki"
  ]
  node [
    id 11
    label "klasycyzm"
  ]
  node [
    id 12
    label "styl_jo&#324;ski"
  ]
  node [
    id 13
    label "antyk"
  ]
  node [
    id 14
    label "okres_klasyczny"
  ]
  node [
    id 15
    label "styl_koryncki"
  ]
  node [
    id 16
    label "staro&#263;"
  ]
  node [
    id 17
    label "bajos"
  ]
  node [
    id 18
    label "kelowej"
  ]
  node [
    id 19
    label "paleocen"
  ]
  node [
    id 20
    label "&#347;rodkowy_trias"
  ]
  node [
    id 21
    label "miocen"
  ]
  node [
    id 22
    label "Zeitgeist"
  ]
  node [
    id 23
    label "schy&#322;ek"
  ]
  node [
    id 24
    label "plejstocen"
  ]
  node [
    id 25
    label "aalen"
  ]
  node [
    id 26
    label "jura_&#347;rodkowa"
  ]
  node [
    id 27
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 28
    label "jura_wczesna"
  ]
  node [
    id 29
    label "jednostka_geologiczna"
  ]
  node [
    id 30
    label "eocen"
  ]
  node [
    id 31
    label "term"
  ]
  node [
    id 32
    label "dzieje"
  ]
  node [
    id 33
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 34
    label "wczesny_trias"
  ]
  node [
    id 35
    label "okres"
  ]
  node [
    id 36
    label "holocen"
  ]
  node [
    id 37
    label "pliocen"
  ]
  node [
    id 38
    label "czas"
  ]
  node [
    id 39
    label "oligocen"
  ]
  node [
    id 40
    label "szko&#322;a"
  ]
  node [
    id 41
    label "classicism"
  ]
  node [
    id 42
    label "prostota"
  ]
  node [
    id 43
    label "narracja"
  ]
  node [
    id 44
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 45
    label "stan"
  ]
  node [
    id 46
    label "stand"
  ]
  node [
    id 47
    label "trwa&#263;"
  ]
  node [
    id 48
    label "equal"
  ]
  node [
    id 49
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 50
    label "chodzi&#263;"
  ]
  node [
    id 51
    label "uczestniczy&#263;"
  ]
  node [
    id 52
    label "obecno&#347;&#263;"
  ]
  node [
    id 53
    label "si&#281;ga&#263;"
  ]
  node [
    id 54
    label "mie&#263;_miejsce"
  ]
  node [
    id 55
    label "robi&#263;"
  ]
  node [
    id 56
    label "participate"
  ]
  node [
    id 57
    label "adhere"
  ]
  node [
    id 58
    label "pozostawa&#263;"
  ]
  node [
    id 59
    label "zostawa&#263;"
  ]
  node [
    id 60
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 61
    label "istnie&#263;"
  ]
  node [
    id 62
    label "compass"
  ]
  node [
    id 63
    label "exsert"
  ]
  node [
    id 64
    label "get"
  ]
  node [
    id 65
    label "u&#380;ywa&#263;"
  ]
  node [
    id 66
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 67
    label "osi&#261;ga&#263;"
  ]
  node [
    id 68
    label "korzysta&#263;"
  ]
  node [
    id 69
    label "appreciation"
  ]
  node [
    id 70
    label "dociera&#263;"
  ]
  node [
    id 71
    label "mierzy&#263;"
  ]
  node [
    id 72
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 73
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 74
    label "being"
  ]
  node [
    id 75
    label "cecha"
  ]
  node [
    id 76
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 77
    label "proceed"
  ]
  node [
    id 78
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 79
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 80
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 81
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 82
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 83
    label "str&#243;j"
  ]
  node [
    id 84
    label "para"
  ]
  node [
    id 85
    label "krok"
  ]
  node [
    id 86
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 87
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 88
    label "przebiega&#263;"
  ]
  node [
    id 89
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 90
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 91
    label "continue"
  ]
  node [
    id 92
    label "carry"
  ]
  node [
    id 93
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 94
    label "wk&#322;ada&#263;"
  ]
  node [
    id 95
    label "p&#322;ywa&#263;"
  ]
  node [
    id 96
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 97
    label "bangla&#263;"
  ]
  node [
    id 98
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 99
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 100
    label "bywa&#263;"
  ]
  node [
    id 101
    label "tryb"
  ]
  node [
    id 102
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 103
    label "dziama&#263;"
  ]
  node [
    id 104
    label "run"
  ]
  node [
    id 105
    label "stara&#263;_si&#281;"
  ]
  node [
    id 106
    label "Arakan"
  ]
  node [
    id 107
    label "Teksas"
  ]
  node [
    id 108
    label "Georgia"
  ]
  node [
    id 109
    label "Maryland"
  ]
  node [
    id 110
    label "warstwa"
  ]
  node [
    id 111
    label "Michigan"
  ]
  node [
    id 112
    label "Massachusetts"
  ]
  node [
    id 113
    label "Luizjana"
  ]
  node [
    id 114
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 115
    label "samopoczucie"
  ]
  node [
    id 116
    label "Floryda"
  ]
  node [
    id 117
    label "Ohio"
  ]
  node [
    id 118
    label "Alaska"
  ]
  node [
    id 119
    label "Nowy_Meksyk"
  ]
  node [
    id 120
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 121
    label "wci&#281;cie"
  ]
  node [
    id 122
    label "Kansas"
  ]
  node [
    id 123
    label "Alabama"
  ]
  node [
    id 124
    label "miejsce"
  ]
  node [
    id 125
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 126
    label "Kalifornia"
  ]
  node [
    id 127
    label "Wirginia"
  ]
  node [
    id 128
    label "punkt"
  ]
  node [
    id 129
    label "Nowy_York"
  ]
  node [
    id 130
    label "Waszyngton"
  ]
  node [
    id 131
    label "Pensylwania"
  ]
  node [
    id 132
    label "wektor"
  ]
  node [
    id 133
    label "Hawaje"
  ]
  node [
    id 134
    label "state"
  ]
  node [
    id 135
    label "poziom"
  ]
  node [
    id 136
    label "jednostka_administracyjna"
  ]
  node [
    id 137
    label "Illinois"
  ]
  node [
    id 138
    label "Oklahoma"
  ]
  node [
    id 139
    label "Oregon"
  ]
  node [
    id 140
    label "Arizona"
  ]
  node [
    id 141
    label "ilo&#347;&#263;"
  ]
  node [
    id 142
    label "Jukatan"
  ]
  node [
    id 143
    label "shape"
  ]
  node [
    id 144
    label "Goa"
  ]
  node [
    id 145
    label "chronometria"
  ]
  node [
    id 146
    label "odczyt"
  ]
  node [
    id 147
    label "laba"
  ]
  node [
    id 148
    label "czasoprzestrze&#324;"
  ]
  node [
    id 149
    label "time_period"
  ]
  node [
    id 150
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 151
    label "pochodzenie"
  ]
  node [
    id 152
    label "przep&#322;ywanie"
  ]
  node [
    id 153
    label "czwarty_wymiar"
  ]
  node [
    id 154
    label "kategoria_gramatyczna"
  ]
  node [
    id 155
    label "poprzedzi&#263;"
  ]
  node [
    id 156
    label "pogoda"
  ]
  node [
    id 157
    label "czasokres"
  ]
  node [
    id 158
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 159
    label "poprzedzenie"
  ]
  node [
    id 160
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 161
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 162
    label "zegar"
  ]
  node [
    id 163
    label "koniugacja"
  ]
  node [
    id 164
    label "trawi&#263;"
  ]
  node [
    id 165
    label "poprzedza&#263;"
  ]
  node [
    id 166
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 167
    label "trawienie"
  ]
  node [
    id 168
    label "chwila"
  ]
  node [
    id 169
    label "rachuba_czasu"
  ]
  node [
    id 170
    label "poprzedzanie"
  ]
  node [
    id 171
    label "okres_czasu"
  ]
  node [
    id 172
    label "period"
  ]
  node [
    id 173
    label "odwlekanie_si&#281;"
  ]
  node [
    id 174
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 175
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 176
    label "pochodzi&#263;"
  ]
  node [
    id 177
    label "czwartorz&#281;d"
  ]
  node [
    id 178
    label "kalabr"
  ]
  node [
    id 179
    label "gelas"
  ]
  node [
    id 180
    label "megaterium"
  ]
  node [
    id 181
    label "formacja_geologiczna"
  ]
  node [
    id 182
    label "priabon"
  ]
  node [
    id 183
    label "paleogen"
  ]
  node [
    id 184
    label "barton"
  ]
  node [
    id 185
    label "iprez"
  ]
  node [
    id 186
    label "lutet"
  ]
  node [
    id 187
    label "aluwium"
  ]
  node [
    id 188
    label "zeland"
  ]
  node [
    id 189
    label "tanet"
  ]
  node [
    id 190
    label "dan"
  ]
  node [
    id 191
    label "szat"
  ]
  node [
    id 192
    label "rupel"
  ]
  node [
    id 193
    label "torton"
  ]
  node [
    id 194
    label "lang"
  ]
  node [
    id 195
    label "neogen"
  ]
  node [
    id 196
    label "serrawal"
  ]
  node [
    id 197
    label "messyn"
  ]
  node [
    id 198
    label "burdyga&#322;"
  ]
  node [
    id 199
    label "akwitan"
  ]
  node [
    id 200
    label "piacent"
  ]
  node [
    id 201
    label "zankl"
  ]
  node [
    id 202
    label "zdanie"
  ]
  node [
    id 203
    label "podokres"
  ]
  node [
    id 204
    label "trias"
  ]
  node [
    id 205
    label "riak"
  ]
  node [
    id 206
    label "trzeciorz&#281;d"
  ]
  node [
    id 207
    label "kreda"
  ]
  node [
    id 208
    label "orosir"
  ]
  node [
    id 209
    label "okres_noachijski"
  ]
  node [
    id 210
    label "preglacja&#322;"
  ]
  node [
    id 211
    label "cykl"
  ]
  node [
    id 212
    label "rok_akademicki"
  ]
  node [
    id 213
    label "stater"
  ]
  node [
    id 214
    label "interstadia&#322;"
  ]
  node [
    id 215
    label "fala"
  ]
  node [
    id 216
    label "rok_szkolny"
  ]
  node [
    id 217
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 218
    label "choroba_przyrodzona"
  ]
  node [
    id 219
    label "sider"
  ]
  node [
    id 220
    label "izochronizm"
  ]
  node [
    id 221
    label "pierwszorz&#281;d"
  ]
  node [
    id 222
    label "ciota"
  ]
  node [
    id 223
    label "spell"
  ]
  node [
    id 224
    label "condition"
  ]
  node [
    id 225
    label "postglacja&#322;"
  ]
  node [
    id 226
    label "semester"
  ]
  node [
    id 227
    label "dewon"
  ]
  node [
    id 228
    label "era"
  ]
  node [
    id 229
    label "okres_hesperyjski"
  ]
  node [
    id 230
    label "prekambr"
  ]
  node [
    id 231
    label "kalim"
  ]
  node [
    id 232
    label "p&#243;&#322;okres"
  ]
  node [
    id 233
    label "sten"
  ]
  node [
    id 234
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 235
    label "nast&#281;pnik"
  ]
  node [
    id 236
    label "flow"
  ]
  node [
    id 237
    label "sylur"
  ]
  node [
    id 238
    label "karbon"
  ]
  node [
    id 239
    label "jura"
  ]
  node [
    id 240
    label "proces_fizjologiczny"
  ]
  node [
    id 241
    label "poprzednik"
  ]
  node [
    id 242
    label "glacja&#322;"
  ]
  node [
    id 243
    label "pulsacja"
  ]
  node [
    id 244
    label "drugorz&#281;d"
  ]
  node [
    id 245
    label "kriogen"
  ]
  node [
    id 246
    label "okres_amazo&#324;ski"
  ]
  node [
    id 247
    label "okres_halsztacki"
  ]
  node [
    id 248
    label "ordowik"
  ]
  node [
    id 249
    label "ton"
  ]
  node [
    id 250
    label "kambr"
  ]
  node [
    id 251
    label "retoryka"
  ]
  node [
    id 252
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 253
    label "ektas"
  ]
  node [
    id 254
    label "ediakar"
  ]
  node [
    id 255
    label "faza"
  ]
  node [
    id 256
    label "perm"
  ]
  node [
    id 257
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 258
    label "poj&#281;cie"
  ]
  node [
    id 259
    label "kres"
  ]
  node [
    id 260
    label "charakter"
  ]
  node [
    id 261
    label "cover"
  ]
  node [
    id 262
    label "relate"
  ]
  node [
    id 263
    label "zaskakiwa&#263;"
  ]
  node [
    id 264
    label "rozumie&#263;"
  ]
  node [
    id 265
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 266
    label "swat"
  ]
  node [
    id 267
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 268
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 269
    label "powodowa&#263;"
  ]
  node [
    id 270
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 271
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 272
    label "match"
  ]
  node [
    id 273
    label "see"
  ]
  node [
    id 274
    label "kuma&#263;"
  ]
  node [
    id 275
    label "zna&#263;"
  ]
  node [
    id 276
    label "give"
  ]
  node [
    id 277
    label "empatia"
  ]
  node [
    id 278
    label "czu&#263;"
  ]
  node [
    id 279
    label "j&#281;zyk"
  ]
  node [
    id 280
    label "wiedzie&#263;"
  ]
  node [
    id 281
    label "odbiera&#263;"
  ]
  node [
    id 282
    label "Fox"
  ]
  node [
    id 283
    label "wpada&#263;"
  ]
  node [
    id 284
    label "obejmowa&#263;"
  ]
  node [
    id 285
    label "dziwi&#263;"
  ]
  node [
    id 286
    label "surprise"
  ]
  node [
    id 287
    label "aran&#380;acja"
  ]
  node [
    id 288
    label "kawa&#322;ek"
  ]
  node [
    id 289
    label "po&#347;rednik"
  ]
  node [
    id 290
    label "skojarzy&#263;"
  ]
  node [
    id 291
    label "dziewos&#322;&#281;b"
  ]
  node [
    id 292
    label "swatowie"
  ]
  node [
    id 293
    label "te&#347;&#263;"
  ]
  node [
    id 294
    label "w_chuj"
  ]
  node [
    id 295
    label "podnio&#347;le"
  ]
  node [
    id 296
    label "pompatyczny"
  ]
  node [
    id 297
    label "nadmiernie"
  ]
  node [
    id 298
    label "portentously"
  ]
  node [
    id 299
    label "nadmierny"
  ]
  node [
    id 300
    label "podnios&#322;y"
  ]
  node [
    id 301
    label "spokojnie"
  ]
  node [
    id 302
    label "wznio&#347;le"
  ]
  node [
    id 303
    label "szczeg&#243;lnie"
  ]
  node [
    id 304
    label "wzruszaj&#261;co"
  ]
  node [
    id 305
    label "patetyczny"
  ]
  node [
    id 306
    label "powa&#380;ny"
  ]
  node [
    id 307
    label "przej&#281;ty"
  ]
  node [
    id 308
    label "elegancki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
]
