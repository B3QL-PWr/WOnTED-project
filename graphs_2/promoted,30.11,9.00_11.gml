graph [
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "zatytu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "lekarz"
    origin "text"
  ]
  node [
    id 4
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "europa"
    origin "text"
  ]
  node [
    id 7
    label "ostatni"
    origin "text"
  ]
  node [
    id 8
    label "miejsce"
    origin "text"
  ]
  node [
    id 9
    label "kolejny"
  ]
  node [
    id 10
    label "nowo"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "bie&#380;&#261;cy"
  ]
  node [
    id 13
    label "drugi"
  ]
  node [
    id 14
    label "narybek"
  ]
  node [
    id 15
    label "obcy"
  ]
  node [
    id 16
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 17
    label "nowotny"
  ]
  node [
    id 18
    label "nadprzyrodzony"
  ]
  node [
    id 19
    label "nieznany"
  ]
  node [
    id 20
    label "pozaludzki"
  ]
  node [
    id 21
    label "obco"
  ]
  node [
    id 22
    label "tameczny"
  ]
  node [
    id 23
    label "osoba"
  ]
  node [
    id 24
    label "nieznajomo"
  ]
  node [
    id 25
    label "inny"
  ]
  node [
    id 26
    label "cudzy"
  ]
  node [
    id 27
    label "istota_&#380;ywa"
  ]
  node [
    id 28
    label "zaziemsko"
  ]
  node [
    id 29
    label "jednoczesny"
  ]
  node [
    id 30
    label "unowocze&#347;nianie"
  ]
  node [
    id 31
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 32
    label "tera&#378;niejszy"
  ]
  node [
    id 33
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 34
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 35
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 36
    label "nast&#281;pnie"
  ]
  node [
    id 37
    label "nastopny"
  ]
  node [
    id 38
    label "kolejno"
  ]
  node [
    id 39
    label "kt&#243;ry&#347;"
  ]
  node [
    id 40
    label "sw&#243;j"
  ]
  node [
    id 41
    label "przeciwny"
  ]
  node [
    id 42
    label "wt&#243;ry"
  ]
  node [
    id 43
    label "dzie&#324;"
  ]
  node [
    id 44
    label "odwrotnie"
  ]
  node [
    id 45
    label "podobny"
  ]
  node [
    id 46
    label "bie&#380;&#261;co"
  ]
  node [
    id 47
    label "ci&#261;g&#322;y"
  ]
  node [
    id 48
    label "aktualny"
  ]
  node [
    id 49
    label "ludzko&#347;&#263;"
  ]
  node [
    id 50
    label "asymilowanie"
  ]
  node [
    id 51
    label "wapniak"
  ]
  node [
    id 52
    label "asymilowa&#263;"
  ]
  node [
    id 53
    label "os&#322;abia&#263;"
  ]
  node [
    id 54
    label "posta&#263;"
  ]
  node [
    id 55
    label "hominid"
  ]
  node [
    id 56
    label "podw&#322;adny"
  ]
  node [
    id 57
    label "os&#322;abianie"
  ]
  node [
    id 58
    label "g&#322;owa"
  ]
  node [
    id 59
    label "figura"
  ]
  node [
    id 60
    label "portrecista"
  ]
  node [
    id 61
    label "dwun&#243;g"
  ]
  node [
    id 62
    label "profanum"
  ]
  node [
    id 63
    label "mikrokosmos"
  ]
  node [
    id 64
    label "nasada"
  ]
  node [
    id 65
    label "duch"
  ]
  node [
    id 66
    label "antropochoria"
  ]
  node [
    id 67
    label "wz&#243;r"
  ]
  node [
    id 68
    label "senior"
  ]
  node [
    id 69
    label "oddzia&#322;ywanie"
  ]
  node [
    id 70
    label "Adam"
  ]
  node [
    id 71
    label "homo_sapiens"
  ]
  node [
    id 72
    label "polifag"
  ]
  node [
    id 73
    label "dopiero_co"
  ]
  node [
    id 74
    label "formacja"
  ]
  node [
    id 75
    label "potomstwo"
  ]
  node [
    id 76
    label "title"
  ]
  node [
    id 77
    label "nazwa&#263;"
  ]
  node [
    id 78
    label "broadcast"
  ]
  node [
    id 79
    label "nada&#263;"
  ]
  node [
    id 80
    label "okre&#347;li&#263;"
  ]
  node [
    id 81
    label "Mesmer"
  ]
  node [
    id 82
    label "pracownik"
  ]
  node [
    id 83
    label "Galen"
  ]
  node [
    id 84
    label "zbada&#263;"
  ]
  node [
    id 85
    label "medyk"
  ]
  node [
    id 86
    label "eskulap"
  ]
  node [
    id 87
    label "lekarze"
  ]
  node [
    id 88
    label "Hipokrates"
  ]
  node [
    id 89
    label "dokt&#243;r"
  ]
  node [
    id 90
    label "&#347;rodowisko"
  ]
  node [
    id 91
    label "student"
  ]
  node [
    id 92
    label "praktyk"
  ]
  node [
    id 93
    label "salariat"
  ]
  node [
    id 94
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 95
    label "delegowanie"
  ]
  node [
    id 96
    label "pracu&#347;"
  ]
  node [
    id 97
    label "r&#281;ka"
  ]
  node [
    id 98
    label "delegowa&#263;"
  ]
  node [
    id 99
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 100
    label "Aesculapius"
  ]
  node [
    id 101
    label "sprawdzi&#263;"
  ]
  node [
    id 102
    label "pozna&#263;"
  ]
  node [
    id 103
    label "zdecydowa&#263;"
  ]
  node [
    id 104
    label "zrobi&#263;"
  ]
  node [
    id 105
    label "wybada&#263;"
  ]
  node [
    id 106
    label "examine"
  ]
  node [
    id 107
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 108
    label "pozyska&#263;"
  ]
  node [
    id 109
    label "oceni&#263;"
  ]
  node [
    id 110
    label "devise"
  ]
  node [
    id 111
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 112
    label "dozna&#263;"
  ]
  node [
    id 113
    label "wykry&#263;"
  ]
  node [
    id 114
    label "odzyska&#263;"
  ]
  node [
    id 115
    label "znaj&#347;&#263;"
  ]
  node [
    id 116
    label "invent"
  ]
  node [
    id 117
    label "wymy&#347;li&#263;"
  ]
  node [
    id 118
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 119
    label "stage"
  ]
  node [
    id 120
    label "uzyska&#263;"
  ]
  node [
    id 121
    label "wytworzy&#263;"
  ]
  node [
    id 122
    label "give_birth"
  ]
  node [
    id 123
    label "feel"
  ]
  node [
    id 124
    label "discover"
  ]
  node [
    id 125
    label "dostrzec"
  ]
  node [
    id 126
    label "odkry&#263;"
  ]
  node [
    id 127
    label "concoct"
  ]
  node [
    id 128
    label "sta&#263;_si&#281;"
  ]
  node [
    id 129
    label "recapture"
  ]
  node [
    id 130
    label "odnowi&#263;_si&#281;"
  ]
  node [
    id 131
    label "visualize"
  ]
  node [
    id 132
    label "wystawi&#263;"
  ]
  node [
    id 133
    label "evaluate"
  ]
  node [
    id 134
    label "pomy&#347;le&#263;"
  ]
  node [
    id 135
    label "niedawno"
  ]
  node [
    id 136
    label "poprzedni"
  ]
  node [
    id 137
    label "pozosta&#322;y"
  ]
  node [
    id 138
    label "ostatnio"
  ]
  node [
    id 139
    label "sko&#324;czony"
  ]
  node [
    id 140
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 141
    label "najgorszy"
  ]
  node [
    id 142
    label "w&#261;tpliwy"
  ]
  node [
    id 143
    label "przesz&#322;y"
  ]
  node [
    id 144
    label "wcze&#347;niejszy"
  ]
  node [
    id 145
    label "poprzednio"
  ]
  node [
    id 146
    label "w&#261;tpliwie"
  ]
  node [
    id 147
    label "pozorny"
  ]
  node [
    id 148
    label "&#380;ywy"
  ]
  node [
    id 149
    label "ostateczny"
  ]
  node [
    id 150
    label "wa&#380;ny"
  ]
  node [
    id 151
    label "wykszta&#322;cony"
  ]
  node [
    id 152
    label "dyplomowany"
  ]
  node [
    id 153
    label "wykwalifikowany"
  ]
  node [
    id 154
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 155
    label "kompletny"
  ]
  node [
    id 156
    label "sko&#324;czenie"
  ]
  node [
    id 157
    label "okre&#347;lony"
  ]
  node [
    id 158
    label "wielki"
  ]
  node [
    id 159
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 160
    label "aktualnie"
  ]
  node [
    id 161
    label "aktualizowanie"
  ]
  node [
    id 162
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 163
    label "uaktualnienie"
  ]
  node [
    id 164
    label "warunek_lokalowy"
  ]
  node [
    id 165
    label "plac"
  ]
  node [
    id 166
    label "location"
  ]
  node [
    id 167
    label "uwaga"
  ]
  node [
    id 168
    label "przestrze&#324;"
  ]
  node [
    id 169
    label "status"
  ]
  node [
    id 170
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 171
    label "chwila"
  ]
  node [
    id 172
    label "cia&#322;o"
  ]
  node [
    id 173
    label "cecha"
  ]
  node [
    id 174
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 175
    label "praca"
  ]
  node [
    id 176
    label "rz&#261;d"
  ]
  node [
    id 177
    label "charakterystyka"
  ]
  node [
    id 178
    label "m&#322;ot"
  ]
  node [
    id 179
    label "znak"
  ]
  node [
    id 180
    label "drzewo"
  ]
  node [
    id 181
    label "pr&#243;ba"
  ]
  node [
    id 182
    label "attribute"
  ]
  node [
    id 183
    label "marka"
  ]
  node [
    id 184
    label "Rzym_Zachodni"
  ]
  node [
    id 185
    label "whole"
  ]
  node [
    id 186
    label "ilo&#347;&#263;"
  ]
  node [
    id 187
    label "element"
  ]
  node [
    id 188
    label "Rzym_Wschodni"
  ]
  node [
    id 189
    label "urz&#261;dzenie"
  ]
  node [
    id 190
    label "wypowied&#378;"
  ]
  node [
    id 191
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 192
    label "stan"
  ]
  node [
    id 193
    label "nagana"
  ]
  node [
    id 194
    label "tekst"
  ]
  node [
    id 195
    label "upomnienie"
  ]
  node [
    id 196
    label "dzienniczek"
  ]
  node [
    id 197
    label "wzgl&#261;d"
  ]
  node [
    id 198
    label "gossip"
  ]
  node [
    id 199
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 200
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 201
    label "najem"
  ]
  node [
    id 202
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 203
    label "zak&#322;ad"
  ]
  node [
    id 204
    label "stosunek_pracy"
  ]
  node [
    id 205
    label "benedykty&#324;ski"
  ]
  node [
    id 206
    label "poda&#380;_pracy"
  ]
  node [
    id 207
    label "pracowanie"
  ]
  node [
    id 208
    label "tyrka"
  ]
  node [
    id 209
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 210
    label "wytw&#243;r"
  ]
  node [
    id 211
    label "zaw&#243;d"
  ]
  node [
    id 212
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 213
    label "tynkarski"
  ]
  node [
    id 214
    label "pracowa&#263;"
  ]
  node [
    id 215
    label "czynno&#347;&#263;"
  ]
  node [
    id 216
    label "zmiana"
  ]
  node [
    id 217
    label "czynnik_produkcji"
  ]
  node [
    id 218
    label "zobowi&#261;zanie"
  ]
  node [
    id 219
    label "kierownictwo"
  ]
  node [
    id 220
    label "siedziba"
  ]
  node [
    id 221
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 222
    label "rozdzielanie"
  ]
  node [
    id 223
    label "bezbrze&#380;e"
  ]
  node [
    id 224
    label "punkt"
  ]
  node [
    id 225
    label "czasoprzestrze&#324;"
  ]
  node [
    id 226
    label "zbi&#243;r"
  ]
  node [
    id 227
    label "niezmierzony"
  ]
  node [
    id 228
    label "przedzielenie"
  ]
  node [
    id 229
    label "nielito&#347;ciwy"
  ]
  node [
    id 230
    label "rozdziela&#263;"
  ]
  node [
    id 231
    label "oktant"
  ]
  node [
    id 232
    label "przedzieli&#263;"
  ]
  node [
    id 233
    label "przestw&#243;r"
  ]
  node [
    id 234
    label "condition"
  ]
  node [
    id 235
    label "awansowa&#263;"
  ]
  node [
    id 236
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 237
    label "znaczenie"
  ]
  node [
    id 238
    label "awans"
  ]
  node [
    id 239
    label "podmiotowo"
  ]
  node [
    id 240
    label "awansowanie"
  ]
  node [
    id 241
    label "sytuacja"
  ]
  node [
    id 242
    label "time"
  ]
  node [
    id 243
    label "czas"
  ]
  node [
    id 244
    label "rozmiar"
  ]
  node [
    id 245
    label "liczba"
  ]
  node [
    id 246
    label "circumference"
  ]
  node [
    id 247
    label "leksem"
  ]
  node [
    id 248
    label "cyrkumferencja"
  ]
  node [
    id 249
    label "strona"
  ]
  node [
    id 250
    label "ekshumowanie"
  ]
  node [
    id 251
    label "uk&#322;ad"
  ]
  node [
    id 252
    label "jednostka_organizacyjna"
  ]
  node [
    id 253
    label "p&#322;aszczyzna"
  ]
  node [
    id 254
    label "odwadnia&#263;"
  ]
  node [
    id 255
    label "zabalsamowanie"
  ]
  node [
    id 256
    label "zesp&#243;&#322;"
  ]
  node [
    id 257
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 258
    label "odwodni&#263;"
  ]
  node [
    id 259
    label "sk&#243;ra"
  ]
  node [
    id 260
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 261
    label "staw"
  ]
  node [
    id 262
    label "ow&#322;osienie"
  ]
  node [
    id 263
    label "mi&#281;so"
  ]
  node [
    id 264
    label "zabalsamowa&#263;"
  ]
  node [
    id 265
    label "Izba_Konsyliarska"
  ]
  node [
    id 266
    label "unerwienie"
  ]
  node [
    id 267
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 268
    label "kremacja"
  ]
  node [
    id 269
    label "biorytm"
  ]
  node [
    id 270
    label "sekcja"
  ]
  node [
    id 271
    label "otworzy&#263;"
  ]
  node [
    id 272
    label "otwiera&#263;"
  ]
  node [
    id 273
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 274
    label "otworzenie"
  ]
  node [
    id 275
    label "materia"
  ]
  node [
    id 276
    label "pochowanie"
  ]
  node [
    id 277
    label "otwieranie"
  ]
  node [
    id 278
    label "szkielet"
  ]
  node [
    id 279
    label "ty&#322;"
  ]
  node [
    id 280
    label "tanatoplastyk"
  ]
  node [
    id 281
    label "odwadnianie"
  ]
  node [
    id 282
    label "Komitet_Region&#243;w"
  ]
  node [
    id 283
    label "odwodnienie"
  ]
  node [
    id 284
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 285
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 286
    label "pochowa&#263;"
  ]
  node [
    id 287
    label "tanatoplastyka"
  ]
  node [
    id 288
    label "balsamowa&#263;"
  ]
  node [
    id 289
    label "nieumar&#322;y"
  ]
  node [
    id 290
    label "temperatura"
  ]
  node [
    id 291
    label "balsamowanie"
  ]
  node [
    id 292
    label "ekshumowa&#263;"
  ]
  node [
    id 293
    label "l&#281;d&#378;wie"
  ]
  node [
    id 294
    label "prz&#243;d"
  ]
  node [
    id 295
    label "cz&#322;onek"
  ]
  node [
    id 296
    label "pogrzeb"
  ]
  node [
    id 297
    label "&#321;ubianka"
  ]
  node [
    id 298
    label "area"
  ]
  node [
    id 299
    label "Majdan"
  ]
  node [
    id 300
    label "pole_bitwy"
  ]
  node [
    id 301
    label "stoisko"
  ]
  node [
    id 302
    label "obszar"
  ]
  node [
    id 303
    label "pierzeja"
  ]
  node [
    id 304
    label "obiekt_handlowy"
  ]
  node [
    id 305
    label "zgromadzenie"
  ]
  node [
    id 306
    label "miasto"
  ]
  node [
    id 307
    label "targowica"
  ]
  node [
    id 308
    label "kram"
  ]
  node [
    id 309
    label "przybli&#380;enie"
  ]
  node [
    id 310
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 311
    label "kategoria"
  ]
  node [
    id 312
    label "szpaler"
  ]
  node [
    id 313
    label "lon&#380;a"
  ]
  node [
    id 314
    label "uporz&#261;dkowanie"
  ]
  node [
    id 315
    label "egzekutywa"
  ]
  node [
    id 316
    label "jednostka_systematyczna"
  ]
  node [
    id 317
    label "instytucja"
  ]
  node [
    id 318
    label "premier"
  ]
  node [
    id 319
    label "Londyn"
  ]
  node [
    id 320
    label "gabinet_cieni"
  ]
  node [
    id 321
    label "gromada"
  ]
  node [
    id 322
    label "number"
  ]
  node [
    id 323
    label "Konsulat"
  ]
  node [
    id 324
    label "tract"
  ]
  node [
    id 325
    label "klasa"
  ]
  node [
    id 326
    label "w&#322;adza"
  ]
  node [
    id 327
    label "Health"
  ]
  node [
    id 328
    label "at"
  ]
  node [
    id 329
    label "a"
  ]
  node [
    id 330
    label "glanc"
  ]
  node [
    id 331
    label "2018"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 327
    target 328
  ]
  edge [
    source 327
    target 329
  ]
  edge [
    source 327
    target 330
  ]
  edge [
    source 327
    target 331
  ]
  edge [
    source 328
    target 329
  ]
  edge [
    source 328
    target 330
  ]
  edge [
    source 328
    target 331
  ]
  edge [
    source 329
    target 330
  ]
  edge [
    source 329
    target 331
  ]
  edge [
    source 330
    target 331
  ]
]
