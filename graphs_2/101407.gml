graph [
  node [
    id 0
    label "polsko"
    origin "text"
  ]
  node [
    id 1
    label "niemiecki"
    origin "text"
  ]
  node [
    id 2
    label "nagroda"
    origin "text"
  ]
  node [
    id 3
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 4
    label "polski"
  ]
  node [
    id 5
    label "po_polsku"
  ]
  node [
    id 6
    label "europejsko"
  ]
  node [
    id 7
    label "po_europejsku"
  ]
  node [
    id 8
    label "europejski"
  ]
  node [
    id 9
    label "przedmiot"
  ]
  node [
    id 10
    label "Polish"
  ]
  node [
    id 11
    label "goniony"
  ]
  node [
    id 12
    label "oberek"
  ]
  node [
    id 13
    label "ryba_po_grecku"
  ]
  node [
    id 14
    label "sztajer"
  ]
  node [
    id 15
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 16
    label "krakowiak"
  ]
  node [
    id 17
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 18
    label "pierogi_ruskie"
  ]
  node [
    id 19
    label "lacki"
  ]
  node [
    id 20
    label "polak"
  ]
  node [
    id 21
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 22
    label "chodzony"
  ]
  node [
    id 23
    label "mazur"
  ]
  node [
    id 24
    label "skoczny"
  ]
  node [
    id 25
    label "drabant"
  ]
  node [
    id 26
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 27
    label "j&#281;zyk"
  ]
  node [
    id 28
    label "po_niemiecku"
  ]
  node [
    id 29
    label "German"
  ]
  node [
    id 30
    label "niemiecko"
  ]
  node [
    id 31
    label "cenar"
  ]
  node [
    id 32
    label "strudel"
  ]
  node [
    id 33
    label "niemiec"
  ]
  node [
    id 34
    label "pionier"
  ]
  node [
    id 35
    label "zachodnioeuropejski"
  ]
  node [
    id 36
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 37
    label "junkers"
  ]
  node [
    id 38
    label "szwabski"
  ]
  node [
    id 39
    label "szwabsko"
  ]
  node [
    id 40
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 41
    label "po_szwabsku"
  ]
  node [
    id 42
    label "platt"
  ]
  node [
    id 43
    label "ciasto"
  ]
  node [
    id 44
    label "&#380;o&#322;nierz"
  ]
  node [
    id 45
    label "saper"
  ]
  node [
    id 46
    label "prekursor"
  ]
  node [
    id 47
    label "osadnik"
  ]
  node [
    id 48
    label "skaut"
  ]
  node [
    id 49
    label "g&#322;osiciel"
  ]
  node [
    id 50
    label "taniec_ludowy"
  ]
  node [
    id 51
    label "melodia"
  ]
  node [
    id 52
    label "taniec"
  ]
  node [
    id 53
    label "podgrzewacz"
  ]
  node [
    id 54
    label "samolot_wojskowy"
  ]
  node [
    id 55
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 56
    label "langosz"
  ]
  node [
    id 57
    label "moreska"
  ]
  node [
    id 58
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 59
    label "zachodni"
  ]
  node [
    id 60
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 61
    label "European"
  ]
  node [
    id 62
    label "typowy"
  ]
  node [
    id 63
    label "charakterystyczny"
  ]
  node [
    id 64
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 65
    label "artykulator"
  ]
  node [
    id 66
    label "kod"
  ]
  node [
    id 67
    label "kawa&#322;ek"
  ]
  node [
    id 68
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 69
    label "gramatyka"
  ]
  node [
    id 70
    label "stylik"
  ]
  node [
    id 71
    label "przet&#322;umaczenie"
  ]
  node [
    id 72
    label "formalizowanie"
  ]
  node [
    id 73
    label "ssanie"
  ]
  node [
    id 74
    label "ssa&#263;"
  ]
  node [
    id 75
    label "language"
  ]
  node [
    id 76
    label "liza&#263;"
  ]
  node [
    id 77
    label "napisa&#263;"
  ]
  node [
    id 78
    label "konsonantyzm"
  ]
  node [
    id 79
    label "wokalizm"
  ]
  node [
    id 80
    label "pisa&#263;"
  ]
  node [
    id 81
    label "fonetyka"
  ]
  node [
    id 82
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 83
    label "jeniec"
  ]
  node [
    id 84
    label "but"
  ]
  node [
    id 85
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 86
    label "po_koroniarsku"
  ]
  node [
    id 87
    label "kultura_duchowa"
  ]
  node [
    id 88
    label "t&#322;umaczenie"
  ]
  node [
    id 89
    label "m&#243;wienie"
  ]
  node [
    id 90
    label "pype&#263;"
  ]
  node [
    id 91
    label "lizanie"
  ]
  node [
    id 92
    label "pismo"
  ]
  node [
    id 93
    label "formalizowa&#263;"
  ]
  node [
    id 94
    label "rozumie&#263;"
  ]
  node [
    id 95
    label "organ"
  ]
  node [
    id 96
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 97
    label "rozumienie"
  ]
  node [
    id 98
    label "spos&#243;b"
  ]
  node [
    id 99
    label "makroglosja"
  ]
  node [
    id 100
    label "m&#243;wi&#263;"
  ]
  node [
    id 101
    label "jama_ustna"
  ]
  node [
    id 102
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 103
    label "formacja_geologiczna"
  ]
  node [
    id 104
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 105
    label "natural_language"
  ]
  node [
    id 106
    label "s&#322;ownictwo"
  ]
  node [
    id 107
    label "urz&#261;dzenie"
  ]
  node [
    id 108
    label "oskar"
  ]
  node [
    id 109
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 110
    label "return"
  ]
  node [
    id 111
    label "konsekwencja"
  ]
  node [
    id 112
    label "prize"
  ]
  node [
    id 113
    label "trophy"
  ]
  node [
    id 114
    label "oznaczenie"
  ]
  node [
    id 115
    label "potraktowanie"
  ]
  node [
    id 116
    label "nagrodzenie"
  ]
  node [
    id 117
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 118
    label "zrobienie"
  ]
  node [
    id 119
    label "odczuwa&#263;"
  ]
  node [
    id 120
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 121
    label "skrupienie_si&#281;"
  ]
  node [
    id 122
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 123
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 124
    label "odczucie"
  ]
  node [
    id 125
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 126
    label "koszula_Dejaniry"
  ]
  node [
    id 127
    label "odczuwanie"
  ]
  node [
    id 128
    label "event"
  ]
  node [
    id 129
    label "rezultat"
  ]
  node [
    id 130
    label "skrupianie_si&#281;"
  ]
  node [
    id 131
    label "odczu&#263;"
  ]
  node [
    id 132
    label "zawodowy"
  ]
  node [
    id 133
    label "dziennikarsko"
  ]
  node [
    id 134
    label "tre&#347;ciwy"
  ]
  node [
    id 135
    label "po_dziennikarsku"
  ]
  node [
    id 136
    label "wzorowy"
  ]
  node [
    id 137
    label "obiektywny"
  ]
  node [
    id 138
    label "rzetelny"
  ]
  node [
    id 139
    label "doskona&#322;y"
  ]
  node [
    id 140
    label "przyk&#322;adny"
  ]
  node [
    id 141
    label "&#322;adny"
  ]
  node [
    id 142
    label "dobry"
  ]
  node [
    id 143
    label "wzorowo"
  ]
  node [
    id 144
    label "czadowy"
  ]
  node [
    id 145
    label "fachowy"
  ]
  node [
    id 146
    label "fajny"
  ]
  node [
    id 147
    label "klawy"
  ]
  node [
    id 148
    label "zawodowo"
  ]
  node [
    id 149
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 150
    label "formalny"
  ]
  node [
    id 151
    label "zawo&#322;any"
  ]
  node [
    id 152
    label "profesjonalny"
  ]
  node [
    id 153
    label "zwyczajny"
  ]
  node [
    id 154
    label "typowo"
  ]
  node [
    id 155
    label "cz&#281;sty"
  ]
  node [
    id 156
    label "zwyk&#322;y"
  ]
  node [
    id 157
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 158
    label "nale&#380;ny"
  ]
  node [
    id 159
    label "nale&#380;yty"
  ]
  node [
    id 160
    label "uprawniony"
  ]
  node [
    id 161
    label "zasadniczy"
  ]
  node [
    id 162
    label "stosownie"
  ]
  node [
    id 163
    label "taki"
  ]
  node [
    id 164
    label "prawdziwy"
  ]
  node [
    id 165
    label "ten"
  ]
  node [
    id 166
    label "syc&#261;cy"
  ]
  node [
    id 167
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 168
    label "tre&#347;ciwie"
  ]
  node [
    id 169
    label "zgrabny"
  ]
  node [
    id 170
    label "g&#281;sty"
  ]
  node [
    id 171
    label "rzetelnie"
  ]
  node [
    id 172
    label "przekonuj&#261;cy"
  ]
  node [
    id 173
    label "porz&#261;dny"
  ]
  node [
    id 174
    label "uczciwy"
  ]
  node [
    id 175
    label "obiektywizowanie"
  ]
  node [
    id 176
    label "zobiektywizowanie"
  ]
  node [
    id 177
    label "niezale&#380;ny"
  ]
  node [
    id 178
    label "bezsporny"
  ]
  node [
    id 179
    label "obiektywnie"
  ]
  node [
    id 180
    label "neutralny"
  ]
  node [
    id 181
    label "faktyczny"
  ]
  node [
    id 182
    label "unia"
  ]
  node [
    id 183
    label "Meklemburgia"
  ]
  node [
    id 184
    label "pomorze"
  ]
  node [
    id 185
    label "przedni"
  ]
  node [
    id 186
    label "fundacja"
  ]
  node [
    id 187
    label "Roberto"
  ]
  node [
    id 188
    label "Boscha"
  ]
  node [
    id 189
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 190
    label "dzie&#324;"
  ]
  node [
    id 191
    label "medium"
  ]
  node [
    id 192
    label "W&#322;odzimierz"
  ]
  node [
    id 193
    label "Kalicki"
  ]
  node [
    id 194
    label "Magdalena"
  ]
  node [
    id 195
    label "Grzeba&#322;kowska"
  ]
  node [
    id 196
    label "Adam"
  ]
  node [
    id 197
    label "Soboczy&#324;ski"
  ]
  node [
    id 198
    label "Helga"
  ]
  node [
    id 199
    label "Hirsch"
  ]
  node [
    id 200
    label "Maria"
  ]
  node [
    id 201
    label "Zmarz"
  ]
  node [
    id 202
    label "Koczanowicz"
  ]
  node [
    id 203
    label "nowak"
  ]
  node [
    id 204
    label "Tomasz"
  ]
  node [
    id 205
    label "sikora"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 183
    target 185
  ]
  edge [
    source 184
    target 185
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 186
    target 188
  ]
  edge [
    source 186
    target 189
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 203
  ]
  edge [
    source 194
    target 195
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 200
    target 201
  ]
  edge [
    source 200
    target 202
  ]
  edge [
    source 201
    target 202
  ]
  edge [
    source 204
    target 205
  ]
]
