graph [
  node [
    id 0
    label "pocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "czech"
    origin "text"
  ]
  node [
    id 4
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 6
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 7
    label "glifosat"
    origin "text"
  ]
  node [
    id 8
    label "ulec"
    origin "text"
  ]
  node [
    id 9
    label "ograniczenie"
    origin "text"
  ]
  node [
    id 10
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 11
    label "poniedzia&#322;ek"
    origin "text"
  ]
  node [
    id 12
    label "komunikat"
    origin "text"
  ]
  node [
    id 13
    label "czeski"
    origin "text"
  ]
  node [
    id 14
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 15
    label "rolnictwo"
    origin "text"
  ]
  node [
    id 16
    label "zrobi&#263;"
  ]
  node [
    id 17
    label "do"
  ]
  node [
    id 18
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 19
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 20
    label "post&#261;pi&#263;"
  ]
  node [
    id 21
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 22
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 23
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 24
    label "zorganizowa&#263;"
  ]
  node [
    id 25
    label "appoint"
  ]
  node [
    id 26
    label "wystylizowa&#263;"
  ]
  node [
    id 27
    label "cause"
  ]
  node [
    id 28
    label "przerobi&#263;"
  ]
  node [
    id 29
    label "nabra&#263;"
  ]
  node [
    id 30
    label "make"
  ]
  node [
    id 31
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 32
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 33
    label "wydali&#263;"
  ]
  node [
    id 34
    label "ut"
  ]
  node [
    id 35
    label "d&#378;wi&#281;k"
  ]
  node [
    id 36
    label "C"
  ]
  node [
    id 37
    label "his"
  ]
  node [
    id 38
    label "kolejny"
  ]
  node [
    id 39
    label "nast&#281;pnie"
  ]
  node [
    id 40
    label "inny"
  ]
  node [
    id 41
    label "nastopny"
  ]
  node [
    id 42
    label "kolejno"
  ]
  node [
    id 43
    label "kt&#243;ry&#347;"
  ]
  node [
    id 44
    label "p&#243;&#322;rocze"
  ]
  node [
    id 45
    label "martwy_sezon"
  ]
  node [
    id 46
    label "kalendarz"
  ]
  node [
    id 47
    label "cykl_astronomiczny"
  ]
  node [
    id 48
    label "lata"
  ]
  node [
    id 49
    label "pora_roku"
  ]
  node [
    id 50
    label "stulecie"
  ]
  node [
    id 51
    label "kurs"
  ]
  node [
    id 52
    label "czas"
  ]
  node [
    id 53
    label "jubileusz"
  ]
  node [
    id 54
    label "grupa"
  ]
  node [
    id 55
    label "kwarta&#322;"
  ]
  node [
    id 56
    label "miesi&#261;c"
  ]
  node [
    id 57
    label "summer"
  ]
  node [
    id 58
    label "odm&#322;adzanie"
  ]
  node [
    id 59
    label "liga"
  ]
  node [
    id 60
    label "jednostka_systematyczna"
  ]
  node [
    id 61
    label "asymilowanie"
  ]
  node [
    id 62
    label "gromada"
  ]
  node [
    id 63
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 64
    label "asymilowa&#263;"
  ]
  node [
    id 65
    label "egzemplarz"
  ]
  node [
    id 66
    label "Entuzjastki"
  ]
  node [
    id 67
    label "zbi&#243;r"
  ]
  node [
    id 68
    label "kompozycja"
  ]
  node [
    id 69
    label "Terranie"
  ]
  node [
    id 70
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 71
    label "category"
  ]
  node [
    id 72
    label "pakiet_klimatyczny"
  ]
  node [
    id 73
    label "oddzia&#322;"
  ]
  node [
    id 74
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 75
    label "cz&#261;steczka"
  ]
  node [
    id 76
    label "stage_set"
  ]
  node [
    id 77
    label "type"
  ]
  node [
    id 78
    label "specgrupa"
  ]
  node [
    id 79
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 80
    label "&#346;wietliki"
  ]
  node [
    id 81
    label "odm&#322;odzenie"
  ]
  node [
    id 82
    label "Eurogrupa"
  ]
  node [
    id 83
    label "odm&#322;adza&#263;"
  ]
  node [
    id 84
    label "formacja_geologiczna"
  ]
  node [
    id 85
    label "harcerze_starsi"
  ]
  node [
    id 86
    label "poprzedzanie"
  ]
  node [
    id 87
    label "czasoprzestrze&#324;"
  ]
  node [
    id 88
    label "laba"
  ]
  node [
    id 89
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 90
    label "chronometria"
  ]
  node [
    id 91
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 92
    label "rachuba_czasu"
  ]
  node [
    id 93
    label "przep&#322;ywanie"
  ]
  node [
    id 94
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 95
    label "czasokres"
  ]
  node [
    id 96
    label "odczyt"
  ]
  node [
    id 97
    label "chwila"
  ]
  node [
    id 98
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 99
    label "dzieje"
  ]
  node [
    id 100
    label "kategoria_gramatyczna"
  ]
  node [
    id 101
    label "poprzedzenie"
  ]
  node [
    id 102
    label "trawienie"
  ]
  node [
    id 103
    label "pochodzi&#263;"
  ]
  node [
    id 104
    label "period"
  ]
  node [
    id 105
    label "okres_czasu"
  ]
  node [
    id 106
    label "poprzedza&#263;"
  ]
  node [
    id 107
    label "schy&#322;ek"
  ]
  node [
    id 108
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 109
    label "odwlekanie_si&#281;"
  ]
  node [
    id 110
    label "zegar"
  ]
  node [
    id 111
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 112
    label "czwarty_wymiar"
  ]
  node [
    id 113
    label "pochodzenie"
  ]
  node [
    id 114
    label "koniugacja"
  ]
  node [
    id 115
    label "Zeitgeist"
  ]
  node [
    id 116
    label "trawi&#263;"
  ]
  node [
    id 117
    label "pogoda"
  ]
  node [
    id 118
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 119
    label "poprzedzi&#263;"
  ]
  node [
    id 120
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 121
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 122
    label "time_period"
  ]
  node [
    id 123
    label "term"
  ]
  node [
    id 124
    label "rok_akademicki"
  ]
  node [
    id 125
    label "rok_szkolny"
  ]
  node [
    id 126
    label "semester"
  ]
  node [
    id 127
    label "anniwersarz"
  ]
  node [
    id 128
    label "rocznica"
  ]
  node [
    id 129
    label "obszar"
  ]
  node [
    id 130
    label "tydzie&#324;"
  ]
  node [
    id 131
    label "miech"
  ]
  node [
    id 132
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 133
    label "kalendy"
  ]
  node [
    id 134
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 135
    label "long_time"
  ]
  node [
    id 136
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 137
    label "almanac"
  ]
  node [
    id 138
    label "rozk&#322;ad"
  ]
  node [
    id 139
    label "wydawnictwo"
  ]
  node [
    id 140
    label "Juliusz_Cezar"
  ]
  node [
    id 141
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 142
    label "zwy&#380;kowanie"
  ]
  node [
    id 143
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 144
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 145
    label "zaj&#281;cia"
  ]
  node [
    id 146
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 147
    label "trasa"
  ]
  node [
    id 148
    label "przeorientowywanie"
  ]
  node [
    id 149
    label "przejazd"
  ]
  node [
    id 150
    label "kierunek"
  ]
  node [
    id 151
    label "przeorientowywa&#263;"
  ]
  node [
    id 152
    label "nauka"
  ]
  node [
    id 153
    label "przeorientowanie"
  ]
  node [
    id 154
    label "klasa"
  ]
  node [
    id 155
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 156
    label "przeorientowa&#263;"
  ]
  node [
    id 157
    label "manner"
  ]
  node [
    id 158
    label "course"
  ]
  node [
    id 159
    label "passage"
  ]
  node [
    id 160
    label "zni&#380;kowanie"
  ]
  node [
    id 161
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 162
    label "seria"
  ]
  node [
    id 163
    label "stawka"
  ]
  node [
    id 164
    label "way"
  ]
  node [
    id 165
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 166
    label "spos&#243;b"
  ]
  node [
    id 167
    label "deprecjacja"
  ]
  node [
    id 168
    label "cedu&#322;a"
  ]
  node [
    id 169
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 170
    label "drive"
  ]
  node [
    id 171
    label "bearing"
  ]
  node [
    id 172
    label "Lira"
  ]
  node [
    id 173
    label "u&#380;ywa&#263;"
  ]
  node [
    id 174
    label "korzysta&#263;"
  ]
  node [
    id 175
    label "distribute"
  ]
  node [
    id 176
    label "give"
  ]
  node [
    id 177
    label "bash"
  ]
  node [
    id 178
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 179
    label "doznawa&#263;"
  ]
  node [
    id 180
    label "punkt"
  ]
  node [
    id 181
    label "miejsce"
  ]
  node [
    id 182
    label "abstrakcja"
  ]
  node [
    id 183
    label "chemikalia"
  ]
  node [
    id 184
    label "substancja"
  ]
  node [
    id 185
    label "model"
  ]
  node [
    id 186
    label "narz&#281;dzie"
  ]
  node [
    id 187
    label "tryb"
  ]
  node [
    id 188
    label "nature"
  ]
  node [
    id 189
    label "po&#322;o&#380;enie"
  ]
  node [
    id 190
    label "sprawa"
  ]
  node [
    id 191
    label "ust&#281;p"
  ]
  node [
    id 192
    label "plan"
  ]
  node [
    id 193
    label "obiekt_matematyczny"
  ]
  node [
    id 194
    label "problemat"
  ]
  node [
    id 195
    label "plamka"
  ]
  node [
    id 196
    label "stopie&#324;_pisma"
  ]
  node [
    id 197
    label "jednostka"
  ]
  node [
    id 198
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 199
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 200
    label "mark"
  ]
  node [
    id 201
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 202
    label "prosta"
  ]
  node [
    id 203
    label "problematyka"
  ]
  node [
    id 204
    label "obiekt"
  ]
  node [
    id 205
    label "zapunktowa&#263;"
  ]
  node [
    id 206
    label "podpunkt"
  ]
  node [
    id 207
    label "wojsko"
  ]
  node [
    id 208
    label "kres"
  ]
  node [
    id 209
    label "przestrze&#324;"
  ]
  node [
    id 210
    label "point"
  ]
  node [
    id 211
    label "pozycja"
  ]
  node [
    id 212
    label "warunek_lokalowy"
  ]
  node [
    id 213
    label "plac"
  ]
  node [
    id 214
    label "location"
  ]
  node [
    id 215
    label "uwaga"
  ]
  node [
    id 216
    label "status"
  ]
  node [
    id 217
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 218
    label "cia&#322;o"
  ]
  node [
    id 219
    label "cecha"
  ]
  node [
    id 220
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 221
    label "praca"
  ]
  node [
    id 222
    label "rz&#261;d"
  ]
  node [
    id 223
    label "przenikanie"
  ]
  node [
    id 224
    label "byt"
  ]
  node [
    id 225
    label "materia"
  ]
  node [
    id 226
    label "temperatura_krytyczna"
  ]
  node [
    id 227
    label "przenika&#263;"
  ]
  node [
    id 228
    label "smolisty"
  ]
  node [
    id 229
    label "proces_my&#347;lowy"
  ]
  node [
    id 230
    label "abstractedness"
  ]
  node [
    id 231
    label "abstraction"
  ]
  node [
    id 232
    label "poj&#281;cie"
  ]
  node [
    id 233
    label "obraz"
  ]
  node [
    id 234
    label "sytuacja"
  ]
  node [
    id 235
    label "spalenie"
  ]
  node [
    id 236
    label "spalanie"
  ]
  node [
    id 237
    label "poznawa&#263;"
  ]
  node [
    id 238
    label "fold"
  ]
  node [
    id 239
    label "obejmowa&#263;"
  ]
  node [
    id 240
    label "mie&#263;"
  ]
  node [
    id 241
    label "lock"
  ]
  node [
    id 242
    label "ustala&#263;"
  ]
  node [
    id 243
    label "zamyka&#263;"
  ]
  node [
    id 244
    label "suspend"
  ]
  node [
    id 245
    label "ujmowa&#263;"
  ]
  node [
    id 246
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 247
    label "instytucja"
  ]
  node [
    id 248
    label "unieruchamia&#263;"
  ]
  node [
    id 249
    label "sk&#322;ada&#263;"
  ]
  node [
    id 250
    label "ko&#324;czy&#263;"
  ]
  node [
    id 251
    label "blokowa&#263;"
  ]
  node [
    id 252
    label "umieszcza&#263;"
  ]
  node [
    id 253
    label "ukrywa&#263;"
  ]
  node [
    id 254
    label "exsert"
  ]
  node [
    id 255
    label "hide"
  ]
  node [
    id 256
    label "czu&#263;"
  ]
  node [
    id 257
    label "support"
  ]
  node [
    id 258
    label "need"
  ]
  node [
    id 259
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 260
    label "cognizance"
  ]
  node [
    id 261
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 262
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 263
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 264
    label "go_steady"
  ]
  node [
    id 265
    label "detect"
  ]
  node [
    id 266
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 267
    label "hurt"
  ]
  node [
    id 268
    label "styka&#263;_si&#281;"
  ]
  node [
    id 269
    label "zaskakiwa&#263;"
  ]
  node [
    id 270
    label "podejmowa&#263;"
  ]
  node [
    id 271
    label "cover"
  ]
  node [
    id 272
    label "rozumie&#263;"
  ]
  node [
    id 273
    label "senator"
  ]
  node [
    id 274
    label "obj&#261;&#263;"
  ]
  node [
    id 275
    label "meet"
  ]
  node [
    id 276
    label "obejmowanie"
  ]
  node [
    id 277
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 278
    label "powodowa&#263;"
  ]
  node [
    id 279
    label "involve"
  ]
  node [
    id 280
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 281
    label "dotyczy&#263;"
  ]
  node [
    id 282
    label "zagarnia&#263;"
  ]
  node [
    id 283
    label "embrace"
  ]
  node [
    id 284
    label "dotyka&#263;"
  ]
  node [
    id 285
    label "robi&#263;"
  ]
  node [
    id 286
    label "peddle"
  ]
  node [
    id 287
    label "unwrap"
  ]
  node [
    id 288
    label "decydowa&#263;"
  ]
  node [
    id 289
    label "zmienia&#263;"
  ]
  node [
    id 290
    label "umacnia&#263;"
  ]
  node [
    id 291
    label "arrange"
  ]
  node [
    id 292
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 293
    label "sta&#263;_si&#281;"
  ]
  node [
    id 294
    label "fall"
  ]
  node [
    id 295
    label "kobieta"
  ]
  node [
    id 296
    label "pozwoli&#263;"
  ]
  node [
    id 297
    label "podda&#263;"
  ]
  node [
    id 298
    label "put_in"
  ]
  node [
    id 299
    label "podda&#263;_si&#281;"
  ]
  node [
    id 300
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 301
    label "pofolgowa&#263;"
  ]
  node [
    id 302
    label "assent"
  ]
  node [
    id 303
    label "uzna&#263;"
  ]
  node [
    id 304
    label "leave"
  ]
  node [
    id 305
    label "zrezygnowa&#263;"
  ]
  node [
    id 306
    label "zdecydowa&#263;"
  ]
  node [
    id 307
    label "podpowiedzie&#263;"
  ]
  node [
    id 308
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 309
    label "push"
  ]
  node [
    id 310
    label "submit"
  ]
  node [
    id 311
    label "doros&#322;y"
  ]
  node [
    id 312
    label "&#380;ona"
  ]
  node [
    id 313
    label "cz&#322;owiek"
  ]
  node [
    id 314
    label "samica"
  ]
  node [
    id 315
    label "uleganie"
  ]
  node [
    id 316
    label "m&#281;&#380;yna"
  ]
  node [
    id 317
    label "partnerka"
  ]
  node [
    id 318
    label "ulegni&#281;cie"
  ]
  node [
    id 319
    label "pa&#324;stwo"
  ]
  node [
    id 320
    label "&#322;ono"
  ]
  node [
    id 321
    label "menopauza"
  ]
  node [
    id 322
    label "przekwitanie"
  ]
  node [
    id 323
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 324
    label "babka"
  ]
  node [
    id 325
    label "ulega&#263;"
  ]
  node [
    id 326
    label "g&#322;upstwo"
  ]
  node [
    id 327
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 328
    label "prevention"
  ]
  node [
    id 329
    label "pomiarkowanie"
  ]
  node [
    id 330
    label "przeszkoda"
  ]
  node [
    id 331
    label "intelekt"
  ]
  node [
    id 332
    label "zmniejszenie"
  ]
  node [
    id 333
    label "reservation"
  ]
  node [
    id 334
    label "przekroczenie"
  ]
  node [
    id 335
    label "finlandyzacja"
  ]
  node [
    id 336
    label "otoczenie"
  ]
  node [
    id 337
    label "osielstwo"
  ]
  node [
    id 338
    label "zdyskryminowanie"
  ]
  node [
    id 339
    label "warunek"
  ]
  node [
    id 340
    label "limitation"
  ]
  node [
    id 341
    label "przekroczy&#263;"
  ]
  node [
    id 342
    label "przekraczanie"
  ]
  node [
    id 343
    label "przekracza&#263;"
  ]
  node [
    id 344
    label "barrier"
  ]
  node [
    id 345
    label "dzielenie"
  ]
  node [
    id 346
    label "je&#378;dziectwo"
  ]
  node [
    id 347
    label "obstruction"
  ]
  node [
    id 348
    label "trudno&#347;&#263;"
  ]
  node [
    id 349
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 350
    label "podzielenie"
  ]
  node [
    id 351
    label "condition"
  ]
  node [
    id 352
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 353
    label "za&#322;o&#380;enie"
  ]
  node [
    id 354
    label "faktor"
  ]
  node [
    id 355
    label "agent"
  ]
  node [
    id 356
    label "ekspozycja"
  ]
  node [
    id 357
    label "umowa"
  ]
  node [
    id 358
    label "charakterystyka"
  ]
  node [
    id 359
    label "m&#322;ot"
  ]
  node [
    id 360
    label "znak"
  ]
  node [
    id 361
    label "drzewo"
  ]
  node [
    id 362
    label "pr&#243;ba"
  ]
  node [
    id 363
    label "attribute"
  ]
  node [
    id 364
    label "marka"
  ]
  node [
    id 365
    label "umys&#322;"
  ]
  node [
    id 366
    label "wiedza"
  ]
  node [
    id 367
    label "noosfera"
  ]
  node [
    id 368
    label "odwodnienie"
  ]
  node [
    id 369
    label "relaxation"
  ]
  node [
    id 370
    label "zmiana"
  ]
  node [
    id 371
    label "zmienienie"
  ]
  node [
    id 372
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 373
    label "okrycie"
  ]
  node [
    id 374
    label "class"
  ]
  node [
    id 375
    label "spowodowanie"
  ]
  node [
    id 376
    label "background"
  ]
  node [
    id 377
    label "zdarzenie_si&#281;"
  ]
  node [
    id 378
    label "crack"
  ]
  node [
    id 379
    label "cortege"
  ]
  node [
    id 380
    label "okolica"
  ]
  node [
    id 381
    label "czynno&#347;&#263;"
  ]
  node [
    id 382
    label "huczek"
  ]
  node [
    id 383
    label "zrobienie"
  ]
  node [
    id 384
    label "zjawisko"
  ]
  node [
    id 385
    label "proces"
  ]
  node [
    id 386
    label "przebywa&#263;"
  ]
  node [
    id 387
    label "conflict"
  ]
  node [
    id 388
    label "transgress"
  ]
  node [
    id 389
    label "appear"
  ]
  node [
    id 390
    label "osi&#261;ga&#263;"
  ]
  node [
    id 391
    label "mija&#263;"
  ]
  node [
    id 392
    label "mini&#281;cie"
  ]
  node [
    id 393
    label "przepuszczenie"
  ]
  node [
    id 394
    label "discourtesy"
  ]
  node [
    id 395
    label "przebycie"
  ]
  node [
    id 396
    label "transgression"
  ]
  node [
    id 397
    label "transgresja"
  ]
  node [
    id 398
    label "emergence"
  ]
  node [
    id 399
    label "offense"
  ]
  node [
    id 400
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 401
    label "przeby&#263;"
  ]
  node [
    id 402
    label "open"
  ]
  node [
    id 403
    label "min&#261;&#263;"
  ]
  node [
    id 404
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 405
    label "cut"
  ]
  node [
    id 406
    label "pique"
  ]
  node [
    id 407
    label "przepuszczanie"
  ]
  node [
    id 408
    label "powodowanie"
  ]
  node [
    id 409
    label "przebywanie"
  ]
  node [
    id 410
    label "robienie"
  ]
  node [
    id 411
    label "misdemeanor"
  ]
  node [
    id 412
    label "passing"
  ]
  node [
    id 413
    label "egress"
  ]
  node [
    id 414
    label "mijanie"
  ]
  node [
    id 415
    label "osi&#261;ganie"
  ]
  node [
    id 416
    label "g&#322;upota"
  ]
  node [
    id 417
    label "farmazon"
  ]
  node [
    id 418
    label "wypowied&#378;"
  ]
  node [
    id 419
    label "banalny"
  ]
  node [
    id 420
    label "sofcik"
  ]
  node [
    id 421
    label "czyn"
  ]
  node [
    id 422
    label "szczeg&#243;&#322;"
  ]
  node [
    id 423
    label "baj&#281;da"
  ]
  node [
    id 424
    label "nonsense"
  ]
  node [
    id 425
    label "g&#243;wno"
  ]
  node [
    id 426
    label "stupidity"
  ]
  node [
    id 427
    label "furda"
  ]
  node [
    id 428
    label "zorientowanie_si&#281;"
  ]
  node [
    id 429
    label "skrzywdzenie"
  ]
  node [
    id 430
    label "tenis"
  ]
  node [
    id 431
    label "supply"
  ]
  node [
    id 432
    label "da&#263;"
  ]
  node [
    id 433
    label "ustawi&#263;"
  ]
  node [
    id 434
    label "siatk&#243;wka"
  ]
  node [
    id 435
    label "zagra&#263;"
  ]
  node [
    id 436
    label "jedzenie"
  ]
  node [
    id 437
    label "poinformowa&#263;"
  ]
  node [
    id 438
    label "introduce"
  ]
  node [
    id 439
    label "nafaszerowa&#263;"
  ]
  node [
    id 440
    label "zaserwowa&#263;"
  ]
  node [
    id 441
    label "pi&#322;ka"
  ]
  node [
    id 442
    label "poprawi&#263;"
  ]
  node [
    id 443
    label "nada&#263;"
  ]
  node [
    id 444
    label "marshal"
  ]
  node [
    id 445
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 446
    label "wyznaczy&#263;"
  ]
  node [
    id 447
    label "stanowisko"
  ]
  node [
    id 448
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 449
    label "spowodowa&#263;"
  ]
  node [
    id 450
    label "zabezpieczy&#263;"
  ]
  node [
    id 451
    label "umie&#347;ci&#263;"
  ]
  node [
    id 452
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 453
    label "zinterpretowa&#263;"
  ]
  node [
    id 454
    label "wskaza&#263;"
  ]
  node [
    id 455
    label "set"
  ]
  node [
    id 456
    label "przyzna&#263;"
  ]
  node [
    id 457
    label "sk&#322;oni&#263;"
  ]
  node [
    id 458
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 459
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 460
    label "accommodate"
  ]
  node [
    id 461
    label "ustali&#263;"
  ]
  node [
    id 462
    label "situate"
  ]
  node [
    id 463
    label "rola"
  ]
  node [
    id 464
    label "inform"
  ]
  node [
    id 465
    label "zakomunikowa&#263;"
  ]
  node [
    id 466
    label "powierzy&#263;"
  ]
  node [
    id 467
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 468
    label "obieca&#263;"
  ]
  node [
    id 469
    label "odst&#261;pi&#263;"
  ]
  node [
    id 470
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 471
    label "przywali&#263;"
  ]
  node [
    id 472
    label "wyrzec_si&#281;"
  ]
  node [
    id 473
    label "sztachn&#261;&#263;"
  ]
  node [
    id 474
    label "rap"
  ]
  node [
    id 475
    label "feed"
  ]
  node [
    id 476
    label "convey"
  ]
  node [
    id 477
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 478
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 479
    label "testify"
  ]
  node [
    id 480
    label "udost&#281;pni&#263;"
  ]
  node [
    id 481
    label "przeznaczy&#263;"
  ]
  node [
    id 482
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 483
    label "picture"
  ]
  node [
    id 484
    label "zada&#263;"
  ]
  node [
    id 485
    label "dress"
  ]
  node [
    id 486
    label "dostarczy&#263;"
  ]
  node [
    id 487
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 488
    label "przekaza&#263;"
  ]
  node [
    id 489
    label "doda&#263;"
  ]
  node [
    id 490
    label "zap&#322;aci&#263;"
  ]
  node [
    id 491
    label "play"
  ]
  node [
    id 492
    label "zabrzmie&#263;"
  ]
  node [
    id 493
    label "instrument_muzyczny"
  ]
  node [
    id 494
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 495
    label "flare"
  ]
  node [
    id 496
    label "rozegra&#263;"
  ]
  node [
    id 497
    label "zaszczeka&#263;"
  ]
  node [
    id 498
    label "sound"
  ]
  node [
    id 499
    label "represent"
  ]
  node [
    id 500
    label "wykorzysta&#263;"
  ]
  node [
    id 501
    label "zatokowa&#263;"
  ]
  node [
    id 502
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 503
    label "uda&#263;_si&#281;"
  ]
  node [
    id 504
    label "zacz&#261;&#263;"
  ]
  node [
    id 505
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 506
    label "wykona&#263;"
  ]
  node [
    id 507
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 508
    label "typify"
  ]
  node [
    id 509
    label "blok"
  ]
  node [
    id 510
    label "lobowanie"
  ]
  node [
    id 511
    label "&#347;cina&#263;"
  ]
  node [
    id 512
    label "retinopatia"
  ]
  node [
    id 513
    label "podanie"
  ]
  node [
    id 514
    label "plamka_&#380;&#243;&#322;ta"
  ]
  node [
    id 515
    label "cia&#322;o_szkliste"
  ]
  node [
    id 516
    label "&#347;cinanie"
  ]
  node [
    id 517
    label "zeaksantyna"
  ]
  node [
    id 518
    label "podawa&#263;"
  ]
  node [
    id 519
    label "przelobowa&#263;"
  ]
  node [
    id 520
    label "lobowa&#263;"
  ]
  node [
    id 521
    label "dno_oka"
  ]
  node [
    id 522
    label "przelobowanie"
  ]
  node [
    id 523
    label "barwnik_&#347;wiat&#322;oczu&#322;y"
  ]
  node [
    id 524
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 525
    label "&#347;ci&#281;cie"
  ]
  node [
    id 526
    label "podawanie"
  ]
  node [
    id 527
    label "pelota"
  ]
  node [
    id 528
    label "sport_rakietowy"
  ]
  node [
    id 529
    label "wolej"
  ]
  node [
    id 530
    label "supervisor"
  ]
  node [
    id 531
    label "ubrani&#243;wka"
  ]
  node [
    id 532
    label "singlista"
  ]
  node [
    id 533
    label "bekhend"
  ]
  node [
    id 534
    label "forhend"
  ]
  node [
    id 535
    label "p&#243;&#322;wolej"
  ]
  node [
    id 536
    label "pr&#261;&#380;kowany"
  ]
  node [
    id 537
    label "singlowy"
  ]
  node [
    id 538
    label "tkanina_we&#322;niana"
  ]
  node [
    id 539
    label "deblowy"
  ]
  node [
    id 540
    label "tkanina"
  ]
  node [
    id 541
    label "mikst"
  ]
  node [
    id 542
    label "slajs"
  ]
  node [
    id 543
    label "deblista"
  ]
  node [
    id 544
    label "miksista"
  ]
  node [
    id 545
    label "Wimbledon"
  ]
  node [
    id 546
    label "zatruwanie_si&#281;"
  ]
  node [
    id 547
    label "przejadanie_si&#281;"
  ]
  node [
    id 548
    label "szama"
  ]
  node [
    id 549
    label "koryto"
  ]
  node [
    id 550
    label "rzecz"
  ]
  node [
    id 551
    label "odpasanie_si&#281;"
  ]
  node [
    id 552
    label "eating"
  ]
  node [
    id 553
    label "jadanie"
  ]
  node [
    id 554
    label "posilenie"
  ]
  node [
    id 555
    label "wpieprzanie"
  ]
  node [
    id 556
    label "wmuszanie"
  ]
  node [
    id 557
    label "wiwenda"
  ]
  node [
    id 558
    label "polowanie"
  ]
  node [
    id 559
    label "ufetowanie_si&#281;"
  ]
  node [
    id 560
    label "wyjadanie"
  ]
  node [
    id 561
    label "smakowanie"
  ]
  node [
    id 562
    label "przejedzenie"
  ]
  node [
    id 563
    label "jad&#322;o"
  ]
  node [
    id 564
    label "mlaskanie"
  ]
  node [
    id 565
    label "papusianie"
  ]
  node [
    id 566
    label "posilanie"
  ]
  node [
    id 567
    label "przejedzenie_si&#281;"
  ]
  node [
    id 568
    label "&#380;arcie"
  ]
  node [
    id 569
    label "odpasienie_si&#281;"
  ]
  node [
    id 570
    label "wyjedzenie"
  ]
  node [
    id 571
    label "przejadanie"
  ]
  node [
    id 572
    label "objadanie"
  ]
  node [
    id 573
    label "przesadzi&#263;"
  ]
  node [
    id 574
    label "nadzia&#263;"
  ]
  node [
    id 575
    label "stuff"
  ]
  node [
    id 576
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 577
    label "dzie&#324;_powszedni"
  ]
  node [
    id 578
    label "doba"
  ]
  node [
    id 579
    label "weekend"
  ]
  node [
    id 580
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 581
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 582
    label "communication"
  ]
  node [
    id 583
    label "kreacjonista"
  ]
  node [
    id 584
    label "roi&#263;_si&#281;"
  ]
  node [
    id 585
    label "wytw&#243;r"
  ]
  node [
    id 586
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 587
    label "przedmiot"
  ]
  node [
    id 588
    label "p&#322;&#243;d"
  ]
  node [
    id 589
    label "work"
  ]
  node [
    id 590
    label "rezultat"
  ]
  node [
    id 591
    label "zwolennik"
  ]
  node [
    id 592
    label "artysta"
  ]
  node [
    id 593
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 594
    label "j&#281;zyk_zachodnios&#322;owia&#324;ski"
  ]
  node [
    id 595
    label "Czech"
  ]
  node [
    id 596
    label "po_czesku"
  ]
  node [
    id 597
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 598
    label "j&#281;zyk"
  ]
  node [
    id 599
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 600
    label "artykulator"
  ]
  node [
    id 601
    label "kod"
  ]
  node [
    id 602
    label "kawa&#322;ek"
  ]
  node [
    id 603
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 604
    label "gramatyka"
  ]
  node [
    id 605
    label "stylik"
  ]
  node [
    id 606
    label "przet&#322;umaczenie"
  ]
  node [
    id 607
    label "formalizowanie"
  ]
  node [
    id 608
    label "ssanie"
  ]
  node [
    id 609
    label "ssa&#263;"
  ]
  node [
    id 610
    label "language"
  ]
  node [
    id 611
    label "liza&#263;"
  ]
  node [
    id 612
    label "napisa&#263;"
  ]
  node [
    id 613
    label "konsonantyzm"
  ]
  node [
    id 614
    label "wokalizm"
  ]
  node [
    id 615
    label "pisa&#263;"
  ]
  node [
    id 616
    label "fonetyka"
  ]
  node [
    id 617
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 618
    label "jeniec"
  ]
  node [
    id 619
    label "but"
  ]
  node [
    id 620
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 621
    label "po_koroniarsku"
  ]
  node [
    id 622
    label "kultura_duchowa"
  ]
  node [
    id 623
    label "t&#322;umaczenie"
  ]
  node [
    id 624
    label "m&#243;wienie"
  ]
  node [
    id 625
    label "pype&#263;"
  ]
  node [
    id 626
    label "lizanie"
  ]
  node [
    id 627
    label "pismo"
  ]
  node [
    id 628
    label "formalizowa&#263;"
  ]
  node [
    id 629
    label "organ"
  ]
  node [
    id 630
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 631
    label "rozumienie"
  ]
  node [
    id 632
    label "makroglosja"
  ]
  node [
    id 633
    label "m&#243;wi&#263;"
  ]
  node [
    id 634
    label "jama_ustna"
  ]
  node [
    id 635
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 636
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 637
    label "natural_language"
  ]
  node [
    id 638
    label "s&#322;ownictwo"
  ]
  node [
    id 639
    label "urz&#261;dzenie"
  ]
  node [
    id 640
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 641
    label "wschodnioeuropejski"
  ]
  node [
    id 642
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 643
    label "poga&#324;ski"
  ]
  node [
    id 644
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 645
    label "topielec"
  ]
  node [
    id 646
    label "europejski"
  ]
  node [
    id 647
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 648
    label "langosz"
  ]
  node [
    id 649
    label "Forman"
  ]
  node [
    id 650
    label "mieszkaniec"
  ]
  node [
    id 651
    label "Szwejk"
  ]
  node [
    id 652
    label "Hus"
  ]
  node [
    id 653
    label "S&#322;owianin"
  ]
  node [
    id 654
    label "departament"
  ]
  node [
    id 655
    label "urz&#261;d"
  ]
  node [
    id 656
    label "NKWD"
  ]
  node [
    id 657
    label "ministerium"
  ]
  node [
    id 658
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 659
    label "MSW"
  ]
  node [
    id 660
    label "resort"
  ]
  node [
    id 661
    label "position"
  ]
  node [
    id 662
    label "siedziba"
  ]
  node [
    id 663
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 664
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 665
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 666
    label "mianowaniec"
  ]
  node [
    id 667
    label "dzia&#322;"
  ]
  node [
    id 668
    label "okienko"
  ]
  node [
    id 669
    label "w&#322;adza"
  ]
  node [
    id 670
    label "jednostka_organizacyjna"
  ]
  node [
    id 671
    label "relation"
  ]
  node [
    id 672
    label "podsekcja"
  ]
  node [
    id 673
    label "ministry"
  ]
  node [
    id 674
    label "Martynika"
  ]
  node [
    id 675
    label "Gwadelupa"
  ]
  node [
    id 676
    label "Moza"
  ]
  node [
    id 677
    label "jednostka_administracyjna"
  ]
  node [
    id 678
    label "nasiennictwo"
  ]
  node [
    id 679
    label "agrotechnika"
  ]
  node [
    id 680
    label "agroekologia"
  ]
  node [
    id 681
    label "agrobiznes"
  ]
  node [
    id 682
    label "intensyfikacja"
  ]
  node [
    id 683
    label "uprawianie"
  ]
  node [
    id 684
    label "gleboznawstwo"
  ]
  node [
    id 685
    label "gospodarka"
  ]
  node [
    id 686
    label "ogrodnictwo"
  ]
  node [
    id 687
    label "agronomia"
  ]
  node [
    id 688
    label "agrochemia"
  ]
  node [
    id 689
    label "farmerstwo"
  ]
  node [
    id 690
    label "zootechnika"
  ]
  node [
    id 691
    label "zgarniacz"
  ]
  node [
    id 692
    label "hodowla"
  ]
  node [
    id 693
    label "sadownictwo"
  ]
  node [
    id 694
    label "&#322;&#261;karstwo"
  ]
  node [
    id 695
    label "&#322;owiectwo"
  ]
  node [
    id 696
    label "wytropienie"
  ]
  node [
    id 697
    label "podkurza&#263;"
  ]
  node [
    id 698
    label "wytropi&#263;"
  ]
  node [
    id 699
    label "tropi&#263;"
  ]
  node [
    id 700
    label "tropienie"
  ]
  node [
    id 701
    label "blood_sport"
  ]
  node [
    id 702
    label "defilowa&#263;"
  ]
  node [
    id 703
    label "gospodarka_le&#347;na"
  ]
  node [
    id 704
    label "pielenie"
  ]
  node [
    id 705
    label "culture"
  ]
  node [
    id 706
    label "sianie"
  ]
  node [
    id 707
    label "sadzenie"
  ]
  node [
    id 708
    label "oprysk"
  ]
  node [
    id 709
    label "szczepienie"
  ]
  node [
    id 710
    label "orka"
  ]
  node [
    id 711
    label "siew"
  ]
  node [
    id 712
    label "exercise"
  ]
  node [
    id 713
    label "koszenie"
  ]
  node [
    id 714
    label "obrabianie"
  ]
  node [
    id 715
    label "zajmowanie_si&#281;"
  ]
  node [
    id 716
    label "use"
  ]
  node [
    id 717
    label "biotechnika"
  ]
  node [
    id 718
    label "hodowanie"
  ]
  node [
    id 719
    label "potrzymanie"
  ]
  node [
    id 720
    label "praca_rolnicza"
  ]
  node [
    id 721
    label "pod&#243;j"
  ]
  node [
    id 722
    label "filiacja"
  ]
  node [
    id 723
    label "licencjonowanie"
  ]
  node [
    id 724
    label "opasa&#263;"
  ]
  node [
    id 725
    label "ch&#243;w"
  ]
  node [
    id 726
    label "licencja"
  ]
  node [
    id 727
    label "sokolarnia"
  ]
  node [
    id 728
    label "potrzyma&#263;"
  ]
  node [
    id 729
    label "rozp&#322;&#243;d"
  ]
  node [
    id 730
    label "grupa_organizm&#243;w"
  ]
  node [
    id 731
    label "wypas"
  ]
  node [
    id 732
    label "wychowalnia"
  ]
  node [
    id 733
    label "pstr&#261;garnia"
  ]
  node [
    id 734
    label "krzy&#380;owanie"
  ]
  node [
    id 735
    label "licencjonowa&#263;"
  ]
  node [
    id 736
    label "odch&#243;w"
  ]
  node [
    id 737
    label "tucz"
  ]
  node [
    id 738
    label "ud&#243;j"
  ]
  node [
    id 739
    label "klatka"
  ]
  node [
    id 740
    label "opasienie"
  ]
  node [
    id 741
    label "wych&#243;w"
  ]
  node [
    id 742
    label "obrz&#261;dek"
  ]
  node [
    id 743
    label "opasanie"
  ]
  node [
    id 744
    label "polish"
  ]
  node [
    id 745
    label "akwarium"
  ]
  node [
    id 746
    label "produkcja"
  ]
  node [
    id 747
    label "poligonizacja"
  ]
  node [
    id 748
    label "intensywny"
  ]
  node [
    id 749
    label "&#347;ci&#243;&#322;kowanie"
  ]
  node [
    id 750
    label "szk&#243;&#322;karstwo"
  ]
  node [
    id 751
    label "hydro&#380;el"
  ]
  node [
    id 752
    label "ekstensywny"
  ]
  node [
    id 753
    label "ekologia"
  ]
  node [
    id 754
    label "agrofizyka"
  ]
  node [
    id 755
    label "agrologia"
  ]
  node [
    id 756
    label "zoohigiena"
  ]
  node [
    id 757
    label "pomologia"
  ]
  node [
    id 758
    label "przemys&#322;_spo&#380;ywczy"
  ]
  node [
    id 759
    label "agroturystyka"
  ]
  node [
    id 760
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 761
    label "biznes"
  ]
  node [
    id 762
    label "inwentarz"
  ]
  node [
    id 763
    label "rynek"
  ]
  node [
    id 764
    label "mieszkalnictwo"
  ]
  node [
    id 765
    label "agregat_ekonomiczny"
  ]
  node [
    id 766
    label "miejsce_pracy"
  ]
  node [
    id 767
    label "farmaceutyka"
  ]
  node [
    id 768
    label "produkowanie"
  ]
  node [
    id 769
    label "transport"
  ]
  node [
    id 770
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 771
    label "obronno&#347;&#263;"
  ]
  node [
    id 772
    label "sektor_prywatny"
  ]
  node [
    id 773
    label "sch&#322;adza&#263;"
  ]
  node [
    id 774
    label "czerwona_strefa"
  ]
  node [
    id 775
    label "struktura"
  ]
  node [
    id 776
    label "pole"
  ]
  node [
    id 777
    label "sektor_publiczny"
  ]
  node [
    id 778
    label "bankowo&#347;&#263;"
  ]
  node [
    id 779
    label "gospodarowanie"
  ]
  node [
    id 780
    label "obora"
  ]
  node [
    id 781
    label "gospodarka_wodna"
  ]
  node [
    id 782
    label "gospodarowa&#263;"
  ]
  node [
    id 783
    label "fabryka"
  ]
  node [
    id 784
    label "wytw&#243;rnia"
  ]
  node [
    id 785
    label "stodo&#322;a"
  ]
  node [
    id 786
    label "przemys&#322;"
  ]
  node [
    id 787
    label "spichlerz"
  ]
  node [
    id 788
    label "sch&#322;adzanie"
  ]
  node [
    id 789
    label "administracja"
  ]
  node [
    id 790
    label "sch&#322;odzenie"
  ]
  node [
    id 791
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 792
    label "zasada"
  ]
  node [
    id 793
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 794
    label "regulacja_cen"
  ]
  node [
    id 795
    label "szkolnictwo"
  ]
  node [
    id 796
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 797
    label "o&#380;ywienie"
  ]
  node [
    id 798
    label "zwi&#281;kszenie"
  ]
  node [
    id 799
    label "budownictwo"
  ]
  node [
    id 800
    label "scraper"
  ]
  node [
    id 801
    label "g&#243;rnictwo"
  ]
  node [
    id 802
    label "miasteczko_rowerowe"
  ]
  node [
    id 803
    label "porada"
  ]
  node [
    id 804
    label "fotowoltaika"
  ]
  node [
    id 805
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 806
    label "przem&#243;wienie"
  ]
  node [
    id 807
    label "nauki_o_poznaniu"
  ]
  node [
    id 808
    label "nomotetyczny"
  ]
  node [
    id 809
    label "systematyka"
  ]
  node [
    id 810
    label "typologia"
  ]
  node [
    id 811
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 812
    label "&#322;awa_szkolna"
  ]
  node [
    id 813
    label "nauki_penalne"
  ]
  node [
    id 814
    label "dziedzina"
  ]
  node [
    id 815
    label "imagineskopia"
  ]
  node [
    id 816
    label "teoria_naukowa"
  ]
  node [
    id 817
    label "inwentyka"
  ]
  node [
    id 818
    label "metodologia"
  ]
  node [
    id 819
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 820
    label "nauki_o_Ziemi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 247
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 15
    target 447
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 366
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 385
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
]
