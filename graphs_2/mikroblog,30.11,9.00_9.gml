graph [
  node [
    id 0
    label "mame"
    origin "text"
  ]
  node [
    id 1
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 2
    label "obiad"
    origin "text"
  ]
  node [
    id 3
    label "doba"
  ]
  node [
    id 4
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 5
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 6
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 7
    label "teraz"
  ]
  node [
    id 8
    label "czas"
  ]
  node [
    id 9
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 10
    label "jednocze&#347;nie"
  ]
  node [
    id 11
    label "tydzie&#324;"
  ]
  node [
    id 12
    label "noc"
  ]
  node [
    id 13
    label "dzie&#324;"
  ]
  node [
    id 14
    label "godzina"
  ]
  node [
    id 15
    label "long_time"
  ]
  node [
    id 16
    label "jednostka_geologiczna"
  ]
  node [
    id 17
    label "meal"
  ]
  node [
    id 18
    label "posi&#322;ek"
  ]
  node [
    id 19
    label "danie"
  ]
  node [
    id 20
    label "jedzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
]
