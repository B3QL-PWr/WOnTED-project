graph [
  node [
    id 0
    label "brawa"
    origin "text"
  ]
  node [
    id 1
    label "dla"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "policjant"
    origin "text"
  ]
  node [
    id 4
    label "belfer"
  ]
  node [
    id 5
    label "murza"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "ojciec"
  ]
  node [
    id 8
    label "samiec"
  ]
  node [
    id 9
    label "androlog"
  ]
  node [
    id 10
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 11
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 12
    label "efendi"
  ]
  node [
    id 13
    label "opiekun"
  ]
  node [
    id 14
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 15
    label "pa&#324;stwo"
  ]
  node [
    id 16
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 17
    label "bratek"
  ]
  node [
    id 18
    label "Mieszko_I"
  ]
  node [
    id 19
    label "Midas"
  ]
  node [
    id 20
    label "m&#261;&#380;"
  ]
  node [
    id 21
    label "bogaty"
  ]
  node [
    id 22
    label "popularyzator"
  ]
  node [
    id 23
    label "pracodawca"
  ]
  node [
    id 24
    label "kszta&#322;ciciel"
  ]
  node [
    id 25
    label "preceptor"
  ]
  node [
    id 26
    label "nabab"
  ]
  node [
    id 27
    label "pupil"
  ]
  node [
    id 28
    label "andropauza"
  ]
  node [
    id 29
    label "zwrot"
  ]
  node [
    id 30
    label "przyw&#243;dca"
  ]
  node [
    id 31
    label "doros&#322;y"
  ]
  node [
    id 32
    label "pedagog"
  ]
  node [
    id 33
    label "rz&#261;dzenie"
  ]
  node [
    id 34
    label "jegomo&#347;&#263;"
  ]
  node [
    id 35
    label "szkolnik"
  ]
  node [
    id 36
    label "ch&#322;opina"
  ]
  node [
    id 37
    label "w&#322;odarz"
  ]
  node [
    id 38
    label "profesor"
  ]
  node [
    id 39
    label "gra_w_karty"
  ]
  node [
    id 40
    label "w&#322;adza"
  ]
  node [
    id 41
    label "Fidel_Castro"
  ]
  node [
    id 42
    label "Anders"
  ]
  node [
    id 43
    label "Ko&#347;ciuszko"
  ]
  node [
    id 44
    label "Tito"
  ]
  node [
    id 45
    label "Miko&#322;ajczyk"
  ]
  node [
    id 46
    label "Sabataj_Cwi"
  ]
  node [
    id 47
    label "lider"
  ]
  node [
    id 48
    label "Mao"
  ]
  node [
    id 49
    label "p&#322;atnik"
  ]
  node [
    id 50
    label "zwierzchnik"
  ]
  node [
    id 51
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 52
    label "nadzorca"
  ]
  node [
    id 53
    label "funkcjonariusz"
  ]
  node [
    id 54
    label "podmiot"
  ]
  node [
    id 55
    label "wykupienie"
  ]
  node [
    id 56
    label "bycie_w_posiadaniu"
  ]
  node [
    id 57
    label "wykupywanie"
  ]
  node [
    id 58
    label "rozszerzyciel"
  ]
  node [
    id 59
    label "ludzko&#347;&#263;"
  ]
  node [
    id 60
    label "asymilowanie"
  ]
  node [
    id 61
    label "wapniak"
  ]
  node [
    id 62
    label "asymilowa&#263;"
  ]
  node [
    id 63
    label "os&#322;abia&#263;"
  ]
  node [
    id 64
    label "posta&#263;"
  ]
  node [
    id 65
    label "hominid"
  ]
  node [
    id 66
    label "podw&#322;adny"
  ]
  node [
    id 67
    label "os&#322;abianie"
  ]
  node [
    id 68
    label "g&#322;owa"
  ]
  node [
    id 69
    label "figura"
  ]
  node [
    id 70
    label "portrecista"
  ]
  node [
    id 71
    label "dwun&#243;g"
  ]
  node [
    id 72
    label "profanum"
  ]
  node [
    id 73
    label "mikrokosmos"
  ]
  node [
    id 74
    label "nasada"
  ]
  node [
    id 75
    label "duch"
  ]
  node [
    id 76
    label "antropochoria"
  ]
  node [
    id 77
    label "osoba"
  ]
  node [
    id 78
    label "wz&#243;r"
  ]
  node [
    id 79
    label "senior"
  ]
  node [
    id 80
    label "oddzia&#322;ywanie"
  ]
  node [
    id 81
    label "Adam"
  ]
  node [
    id 82
    label "homo_sapiens"
  ]
  node [
    id 83
    label "polifag"
  ]
  node [
    id 84
    label "wydoro&#347;lenie"
  ]
  node [
    id 85
    label "du&#380;y"
  ]
  node [
    id 86
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 87
    label "doro&#347;lenie"
  ]
  node [
    id 88
    label "&#378;ra&#322;y"
  ]
  node [
    id 89
    label "doro&#347;le"
  ]
  node [
    id 90
    label "dojrzale"
  ]
  node [
    id 91
    label "dojrza&#322;y"
  ]
  node [
    id 92
    label "m&#261;dry"
  ]
  node [
    id 93
    label "doletni"
  ]
  node [
    id 94
    label "punkt"
  ]
  node [
    id 95
    label "turn"
  ]
  node [
    id 96
    label "turning"
  ]
  node [
    id 97
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 98
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 99
    label "skr&#281;t"
  ]
  node [
    id 100
    label "obr&#243;t"
  ]
  node [
    id 101
    label "fraza_czasownikowa"
  ]
  node [
    id 102
    label "jednostka_leksykalna"
  ]
  node [
    id 103
    label "zmiana"
  ]
  node [
    id 104
    label "wyra&#380;enie"
  ]
  node [
    id 105
    label "starosta"
  ]
  node [
    id 106
    label "zarz&#261;dca"
  ]
  node [
    id 107
    label "w&#322;adca"
  ]
  node [
    id 108
    label "nauczyciel"
  ]
  node [
    id 109
    label "stopie&#324;_naukowy"
  ]
  node [
    id 110
    label "nauczyciel_akademicki"
  ]
  node [
    id 111
    label "tytu&#322;"
  ]
  node [
    id 112
    label "profesura"
  ]
  node [
    id 113
    label "konsulent"
  ]
  node [
    id 114
    label "wirtuoz"
  ]
  node [
    id 115
    label "autor"
  ]
  node [
    id 116
    label "wyprawka"
  ]
  node [
    id 117
    label "mundurek"
  ]
  node [
    id 118
    label "szko&#322;a"
  ]
  node [
    id 119
    label "tarcza"
  ]
  node [
    id 120
    label "elew"
  ]
  node [
    id 121
    label "absolwent"
  ]
  node [
    id 122
    label "klasa"
  ]
  node [
    id 123
    label "ekspert"
  ]
  node [
    id 124
    label "ochotnik"
  ]
  node [
    id 125
    label "pomocnik"
  ]
  node [
    id 126
    label "student"
  ]
  node [
    id 127
    label "nauczyciel_muzyki"
  ]
  node [
    id 128
    label "zakonnik"
  ]
  node [
    id 129
    label "urz&#281;dnik"
  ]
  node [
    id 130
    label "bogacz"
  ]
  node [
    id 131
    label "dostojnik"
  ]
  node [
    id 132
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 133
    label "kuwada"
  ]
  node [
    id 134
    label "tworzyciel"
  ]
  node [
    id 135
    label "rodzice"
  ]
  node [
    id 136
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 137
    label "&#347;w"
  ]
  node [
    id 138
    label "pomys&#322;odawca"
  ]
  node [
    id 139
    label "rodzic"
  ]
  node [
    id 140
    label "wykonawca"
  ]
  node [
    id 141
    label "ojczym"
  ]
  node [
    id 142
    label "przodek"
  ]
  node [
    id 143
    label "papa"
  ]
  node [
    id 144
    label "stary"
  ]
  node [
    id 145
    label "kochanek"
  ]
  node [
    id 146
    label "fio&#322;ek"
  ]
  node [
    id 147
    label "facet"
  ]
  node [
    id 148
    label "brat"
  ]
  node [
    id 149
    label "zwierz&#281;"
  ]
  node [
    id 150
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 151
    label "ma&#322;&#380;onek"
  ]
  node [
    id 152
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 153
    label "m&#243;j"
  ]
  node [
    id 154
    label "ch&#322;op"
  ]
  node [
    id 155
    label "pan_m&#322;ody"
  ]
  node [
    id 156
    label "&#347;lubny"
  ]
  node [
    id 157
    label "pan_domu"
  ]
  node [
    id 158
    label "pan_i_w&#322;adca"
  ]
  node [
    id 159
    label "mo&#347;&#263;"
  ]
  node [
    id 160
    label "Frygia"
  ]
  node [
    id 161
    label "sprawowanie"
  ]
  node [
    id 162
    label "dominion"
  ]
  node [
    id 163
    label "dominowanie"
  ]
  node [
    id 164
    label "reign"
  ]
  node [
    id 165
    label "rule"
  ]
  node [
    id 166
    label "zwierz&#281;_domowe"
  ]
  node [
    id 167
    label "J&#281;drzejewicz"
  ]
  node [
    id 168
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 169
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 170
    label "John_Dewey"
  ]
  node [
    id 171
    label "specjalista"
  ]
  node [
    id 172
    label "&#380;ycie"
  ]
  node [
    id 173
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 174
    label "Turek"
  ]
  node [
    id 175
    label "effendi"
  ]
  node [
    id 176
    label "obfituj&#261;cy"
  ]
  node [
    id 177
    label "r&#243;&#380;norodny"
  ]
  node [
    id 178
    label "spania&#322;y"
  ]
  node [
    id 179
    label "obficie"
  ]
  node [
    id 180
    label "sytuowany"
  ]
  node [
    id 181
    label "och&#281;do&#380;ny"
  ]
  node [
    id 182
    label "forsiasty"
  ]
  node [
    id 183
    label "zapa&#347;ny"
  ]
  node [
    id 184
    label "bogato"
  ]
  node [
    id 185
    label "Katar"
  ]
  node [
    id 186
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 187
    label "Libia"
  ]
  node [
    id 188
    label "Gwatemala"
  ]
  node [
    id 189
    label "Afganistan"
  ]
  node [
    id 190
    label "Ekwador"
  ]
  node [
    id 191
    label "Tad&#380;ykistan"
  ]
  node [
    id 192
    label "Bhutan"
  ]
  node [
    id 193
    label "Argentyna"
  ]
  node [
    id 194
    label "D&#380;ibuti"
  ]
  node [
    id 195
    label "Wenezuela"
  ]
  node [
    id 196
    label "Ukraina"
  ]
  node [
    id 197
    label "Gabon"
  ]
  node [
    id 198
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 199
    label "Rwanda"
  ]
  node [
    id 200
    label "Liechtenstein"
  ]
  node [
    id 201
    label "organizacja"
  ]
  node [
    id 202
    label "Sri_Lanka"
  ]
  node [
    id 203
    label "Madagaskar"
  ]
  node [
    id 204
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 205
    label "Tonga"
  ]
  node [
    id 206
    label "Kongo"
  ]
  node [
    id 207
    label "Bangladesz"
  ]
  node [
    id 208
    label "Kanada"
  ]
  node [
    id 209
    label "Wehrlen"
  ]
  node [
    id 210
    label "Algieria"
  ]
  node [
    id 211
    label "Surinam"
  ]
  node [
    id 212
    label "Chile"
  ]
  node [
    id 213
    label "Sahara_Zachodnia"
  ]
  node [
    id 214
    label "Uganda"
  ]
  node [
    id 215
    label "W&#281;gry"
  ]
  node [
    id 216
    label "Birma"
  ]
  node [
    id 217
    label "Kazachstan"
  ]
  node [
    id 218
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 219
    label "Armenia"
  ]
  node [
    id 220
    label "Tuwalu"
  ]
  node [
    id 221
    label "Timor_Wschodni"
  ]
  node [
    id 222
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 223
    label "Izrael"
  ]
  node [
    id 224
    label "Estonia"
  ]
  node [
    id 225
    label "Komory"
  ]
  node [
    id 226
    label "Kamerun"
  ]
  node [
    id 227
    label "Haiti"
  ]
  node [
    id 228
    label "Belize"
  ]
  node [
    id 229
    label "Sierra_Leone"
  ]
  node [
    id 230
    label "Luksemburg"
  ]
  node [
    id 231
    label "USA"
  ]
  node [
    id 232
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 233
    label "Barbados"
  ]
  node [
    id 234
    label "San_Marino"
  ]
  node [
    id 235
    label "Bu&#322;garia"
  ]
  node [
    id 236
    label "Wietnam"
  ]
  node [
    id 237
    label "Indonezja"
  ]
  node [
    id 238
    label "Malawi"
  ]
  node [
    id 239
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 240
    label "Francja"
  ]
  node [
    id 241
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 242
    label "partia"
  ]
  node [
    id 243
    label "Zambia"
  ]
  node [
    id 244
    label "Angola"
  ]
  node [
    id 245
    label "Grenada"
  ]
  node [
    id 246
    label "Nepal"
  ]
  node [
    id 247
    label "Panama"
  ]
  node [
    id 248
    label "Rumunia"
  ]
  node [
    id 249
    label "Czarnog&#243;ra"
  ]
  node [
    id 250
    label "Malediwy"
  ]
  node [
    id 251
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 252
    label "S&#322;owacja"
  ]
  node [
    id 253
    label "para"
  ]
  node [
    id 254
    label "Egipt"
  ]
  node [
    id 255
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 256
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 257
    label "Kolumbia"
  ]
  node [
    id 258
    label "Mozambik"
  ]
  node [
    id 259
    label "Laos"
  ]
  node [
    id 260
    label "Burundi"
  ]
  node [
    id 261
    label "Suazi"
  ]
  node [
    id 262
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 263
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 264
    label "Czechy"
  ]
  node [
    id 265
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 266
    label "Wyspy_Marshalla"
  ]
  node [
    id 267
    label "Trynidad_i_Tobago"
  ]
  node [
    id 268
    label "Dominika"
  ]
  node [
    id 269
    label "Palau"
  ]
  node [
    id 270
    label "Syria"
  ]
  node [
    id 271
    label "Gwinea_Bissau"
  ]
  node [
    id 272
    label "Liberia"
  ]
  node [
    id 273
    label "Zimbabwe"
  ]
  node [
    id 274
    label "Polska"
  ]
  node [
    id 275
    label "Jamajka"
  ]
  node [
    id 276
    label "Dominikana"
  ]
  node [
    id 277
    label "Senegal"
  ]
  node [
    id 278
    label "Gruzja"
  ]
  node [
    id 279
    label "Togo"
  ]
  node [
    id 280
    label "Chorwacja"
  ]
  node [
    id 281
    label "Meksyk"
  ]
  node [
    id 282
    label "Macedonia"
  ]
  node [
    id 283
    label "Gujana"
  ]
  node [
    id 284
    label "Zair"
  ]
  node [
    id 285
    label "Albania"
  ]
  node [
    id 286
    label "Kambod&#380;a"
  ]
  node [
    id 287
    label "Mauritius"
  ]
  node [
    id 288
    label "Monako"
  ]
  node [
    id 289
    label "Gwinea"
  ]
  node [
    id 290
    label "Mali"
  ]
  node [
    id 291
    label "Nigeria"
  ]
  node [
    id 292
    label "Kostaryka"
  ]
  node [
    id 293
    label "Hanower"
  ]
  node [
    id 294
    label "Paragwaj"
  ]
  node [
    id 295
    label "W&#322;ochy"
  ]
  node [
    id 296
    label "Wyspy_Salomona"
  ]
  node [
    id 297
    label "Seszele"
  ]
  node [
    id 298
    label "Hiszpania"
  ]
  node [
    id 299
    label "Boliwia"
  ]
  node [
    id 300
    label "Kirgistan"
  ]
  node [
    id 301
    label "Irlandia"
  ]
  node [
    id 302
    label "Czad"
  ]
  node [
    id 303
    label "Irak"
  ]
  node [
    id 304
    label "Lesoto"
  ]
  node [
    id 305
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 306
    label "Malta"
  ]
  node [
    id 307
    label "Andora"
  ]
  node [
    id 308
    label "Chiny"
  ]
  node [
    id 309
    label "Filipiny"
  ]
  node [
    id 310
    label "Antarktis"
  ]
  node [
    id 311
    label "Niemcy"
  ]
  node [
    id 312
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 313
    label "Brazylia"
  ]
  node [
    id 314
    label "terytorium"
  ]
  node [
    id 315
    label "Nikaragua"
  ]
  node [
    id 316
    label "Pakistan"
  ]
  node [
    id 317
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 318
    label "Kenia"
  ]
  node [
    id 319
    label "Niger"
  ]
  node [
    id 320
    label "Tunezja"
  ]
  node [
    id 321
    label "Portugalia"
  ]
  node [
    id 322
    label "Fid&#380;i"
  ]
  node [
    id 323
    label "Maroko"
  ]
  node [
    id 324
    label "Botswana"
  ]
  node [
    id 325
    label "Tajlandia"
  ]
  node [
    id 326
    label "Australia"
  ]
  node [
    id 327
    label "Burkina_Faso"
  ]
  node [
    id 328
    label "interior"
  ]
  node [
    id 329
    label "Benin"
  ]
  node [
    id 330
    label "Tanzania"
  ]
  node [
    id 331
    label "Indie"
  ]
  node [
    id 332
    label "&#321;otwa"
  ]
  node [
    id 333
    label "Kiribati"
  ]
  node [
    id 334
    label "Antigua_i_Barbuda"
  ]
  node [
    id 335
    label "Rodezja"
  ]
  node [
    id 336
    label "Cypr"
  ]
  node [
    id 337
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 338
    label "Peru"
  ]
  node [
    id 339
    label "Austria"
  ]
  node [
    id 340
    label "Urugwaj"
  ]
  node [
    id 341
    label "Jordania"
  ]
  node [
    id 342
    label "Grecja"
  ]
  node [
    id 343
    label "Azerbejd&#380;an"
  ]
  node [
    id 344
    label "Turcja"
  ]
  node [
    id 345
    label "Samoa"
  ]
  node [
    id 346
    label "Sudan"
  ]
  node [
    id 347
    label "Oman"
  ]
  node [
    id 348
    label "ziemia"
  ]
  node [
    id 349
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 350
    label "Uzbekistan"
  ]
  node [
    id 351
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 352
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 353
    label "Honduras"
  ]
  node [
    id 354
    label "Mongolia"
  ]
  node [
    id 355
    label "Portoryko"
  ]
  node [
    id 356
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 357
    label "Serbia"
  ]
  node [
    id 358
    label "Tajwan"
  ]
  node [
    id 359
    label "Wielka_Brytania"
  ]
  node [
    id 360
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 361
    label "Liban"
  ]
  node [
    id 362
    label "Japonia"
  ]
  node [
    id 363
    label "Ghana"
  ]
  node [
    id 364
    label "Bahrajn"
  ]
  node [
    id 365
    label "Belgia"
  ]
  node [
    id 366
    label "Etiopia"
  ]
  node [
    id 367
    label "Mikronezja"
  ]
  node [
    id 368
    label "Kuwejt"
  ]
  node [
    id 369
    label "grupa"
  ]
  node [
    id 370
    label "Bahamy"
  ]
  node [
    id 371
    label "Rosja"
  ]
  node [
    id 372
    label "Mo&#322;dawia"
  ]
  node [
    id 373
    label "Litwa"
  ]
  node [
    id 374
    label "S&#322;owenia"
  ]
  node [
    id 375
    label "Szwajcaria"
  ]
  node [
    id 376
    label "Erytrea"
  ]
  node [
    id 377
    label "Kuba"
  ]
  node [
    id 378
    label "Arabia_Saudyjska"
  ]
  node [
    id 379
    label "granica_pa&#324;stwa"
  ]
  node [
    id 380
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 381
    label "Malezja"
  ]
  node [
    id 382
    label "Korea"
  ]
  node [
    id 383
    label "Jemen"
  ]
  node [
    id 384
    label "Nowa_Zelandia"
  ]
  node [
    id 385
    label "Namibia"
  ]
  node [
    id 386
    label "Nauru"
  ]
  node [
    id 387
    label "holoarktyka"
  ]
  node [
    id 388
    label "Brunei"
  ]
  node [
    id 389
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 390
    label "Khitai"
  ]
  node [
    id 391
    label "Mauretania"
  ]
  node [
    id 392
    label "Iran"
  ]
  node [
    id 393
    label "Gambia"
  ]
  node [
    id 394
    label "Somalia"
  ]
  node [
    id 395
    label "Holandia"
  ]
  node [
    id 396
    label "Turkmenistan"
  ]
  node [
    id 397
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 398
    label "Salwador"
  ]
  node [
    id 399
    label "policja"
  ]
  node [
    id 400
    label "blacharz"
  ]
  node [
    id 401
    label "str&#243;&#380;"
  ]
  node [
    id 402
    label "pa&#322;a"
  ]
  node [
    id 403
    label "mundurowy"
  ]
  node [
    id 404
    label "glina"
  ]
  node [
    id 405
    label "organ"
  ]
  node [
    id 406
    label "komisariat"
  ]
  node [
    id 407
    label "s&#322;u&#380;ba"
  ]
  node [
    id 408
    label "posterunek"
  ]
  node [
    id 409
    label "psiarnia"
  ]
  node [
    id 410
    label "&#380;o&#322;nierz"
  ]
  node [
    id 411
    label "nosiciel"
  ]
  node [
    id 412
    label "stra&#380;nik"
  ]
  node [
    id 413
    label "obro&#324;ca"
  ]
  node [
    id 414
    label "anio&#322;"
  ]
  node [
    id 415
    label "rzemie&#347;lnik"
  ]
  node [
    id 416
    label "conk"
  ]
  node [
    id 417
    label "penis"
  ]
  node [
    id 418
    label "cios"
  ]
  node [
    id 419
    label "ciul"
  ]
  node [
    id 420
    label "wyzwisko"
  ]
  node [
    id 421
    label "niedostateczny"
  ]
  node [
    id 422
    label "mak&#243;wka"
  ]
  node [
    id 423
    label "&#322;eb"
  ]
  node [
    id 424
    label "skurwysyn"
  ]
  node [
    id 425
    label "istota_&#380;ywa"
  ]
  node [
    id 426
    label "dupek"
  ]
  node [
    id 427
    label "czaszka"
  ]
  node [
    id 428
    label "dynia"
  ]
  node [
    id 429
    label "przybitka"
  ]
  node [
    id 430
    label "gleba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
]
