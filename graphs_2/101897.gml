graph [
  node [
    id 0
    label "europa"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tym"
    origin "text"
  ]
  node [
    id 3
    label "kontynent"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "wyst&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "odsetek"
    origin "text"
  ]
  node [
    id 8
    label "grunt"
    origin "text"
  ]
  node [
    id 9
    label "orny"
    origin "text"
  ]
  node [
    id 10
    label "przeznaczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "pod"
    origin "text"
  ]
  node [
    id 12
    label "uprawa"
    origin "text"
  ]
  node [
    id 13
    label "ponad"
    origin "text"
  ]
  node [
    id 14
    label "powierzchnia"
    origin "text"
  ]
  node [
    id 15
    label "charakteryzowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "si&#281;"
    origin "text"
  ]
  node [
    id 17
    label "typ"
    origin "text"
  ]
  node [
    id 18
    label "u&#380;ytkowanie"
    origin "text"
  ]
  node [
    id 19
    label "ziemia"
    origin "text"
  ]
  node [
    id 20
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 21
    label "znaczny"
    origin "text"
  ]
  node [
    id 22
    label "obszar"
    origin "text"
  ]
  node [
    id 23
    label "maja"
    origin "text"
  ]
  node [
    id 24
    label "dogodny"
    origin "text"
  ]
  node [
    id 25
    label "warunek"
    origin "text"
  ]
  node [
    id 26
    label "dla"
    origin "text"
  ]
  node [
    id 27
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 28
    label "gospodarstwo"
    origin "text"
  ]
  node [
    id 29
    label "rolny"
    origin "text"
  ]
  node [
    id 30
    label "rozleg&#322;y"
    origin "text"
  ]
  node [
    id 31
    label "nizina"
    origin "text"
  ]
  node [
    id 32
    label "zach&#243;d"
    origin "text"
  ]
  node [
    id 33
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "strefa"
    origin "text"
  ]
  node [
    id 35
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 36
    label "klimat"
    origin "text"
  ]
  node [
    id 37
    label "umiarkowany"
    origin "text"
  ]
  node [
    id 38
    label "ciep&#322;y"
    origin "text"
  ]
  node [
    id 39
    label "morski"
    origin "text"
  ]
  node [
    id 40
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 41
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 42
    label "pada&#324;ski"
    origin "text"
  ]
  node [
    id 43
    label "w&#281;gierski"
    origin "text"
  ]
  node [
    id 44
    label "sprzyja&#263;"
    origin "text"
  ]
  node [
    id 45
    label "zbo&#380;e"
    origin "text"
  ]
  node [
    id 46
    label "pszenica"
    origin "text"
  ]
  node [
    id 47
    label "cukrowy"
    origin "text"
  ]
  node [
    id 48
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 49
    label "stan"
  ]
  node [
    id 50
    label "stand"
  ]
  node [
    id 51
    label "trwa&#263;"
  ]
  node [
    id 52
    label "equal"
  ]
  node [
    id 53
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 54
    label "chodzi&#263;"
  ]
  node [
    id 55
    label "uczestniczy&#263;"
  ]
  node [
    id 56
    label "obecno&#347;&#263;"
  ]
  node [
    id 57
    label "si&#281;ga&#263;"
  ]
  node [
    id 58
    label "mie&#263;_miejsce"
  ]
  node [
    id 59
    label "robi&#263;"
  ]
  node [
    id 60
    label "participate"
  ]
  node [
    id 61
    label "adhere"
  ]
  node [
    id 62
    label "pozostawa&#263;"
  ]
  node [
    id 63
    label "zostawa&#263;"
  ]
  node [
    id 64
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 65
    label "istnie&#263;"
  ]
  node [
    id 66
    label "compass"
  ]
  node [
    id 67
    label "exsert"
  ]
  node [
    id 68
    label "get"
  ]
  node [
    id 69
    label "u&#380;ywa&#263;"
  ]
  node [
    id 70
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 71
    label "osi&#261;ga&#263;"
  ]
  node [
    id 72
    label "korzysta&#263;"
  ]
  node [
    id 73
    label "appreciation"
  ]
  node [
    id 74
    label "dociera&#263;"
  ]
  node [
    id 75
    label "mierzy&#263;"
  ]
  node [
    id 76
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 77
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 78
    label "being"
  ]
  node [
    id 79
    label "cecha"
  ]
  node [
    id 80
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "proceed"
  ]
  node [
    id 82
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 83
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 84
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 85
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 86
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 87
    label "str&#243;j"
  ]
  node [
    id 88
    label "para"
  ]
  node [
    id 89
    label "krok"
  ]
  node [
    id 90
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 91
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 92
    label "przebiega&#263;"
  ]
  node [
    id 93
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 94
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 95
    label "continue"
  ]
  node [
    id 96
    label "carry"
  ]
  node [
    id 97
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 98
    label "wk&#322;ada&#263;"
  ]
  node [
    id 99
    label "p&#322;ywa&#263;"
  ]
  node [
    id 100
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 101
    label "bangla&#263;"
  ]
  node [
    id 102
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 103
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 104
    label "bywa&#263;"
  ]
  node [
    id 105
    label "tryb"
  ]
  node [
    id 106
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 107
    label "dziama&#263;"
  ]
  node [
    id 108
    label "run"
  ]
  node [
    id 109
    label "stara&#263;_si&#281;"
  ]
  node [
    id 110
    label "Arakan"
  ]
  node [
    id 111
    label "Teksas"
  ]
  node [
    id 112
    label "Georgia"
  ]
  node [
    id 113
    label "Maryland"
  ]
  node [
    id 114
    label "warstwa"
  ]
  node [
    id 115
    label "Michigan"
  ]
  node [
    id 116
    label "Massachusetts"
  ]
  node [
    id 117
    label "Luizjana"
  ]
  node [
    id 118
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 119
    label "samopoczucie"
  ]
  node [
    id 120
    label "Floryda"
  ]
  node [
    id 121
    label "Ohio"
  ]
  node [
    id 122
    label "Alaska"
  ]
  node [
    id 123
    label "Nowy_Meksyk"
  ]
  node [
    id 124
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 125
    label "wci&#281;cie"
  ]
  node [
    id 126
    label "Kansas"
  ]
  node [
    id 127
    label "Alabama"
  ]
  node [
    id 128
    label "miejsce"
  ]
  node [
    id 129
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 130
    label "Kalifornia"
  ]
  node [
    id 131
    label "Wirginia"
  ]
  node [
    id 132
    label "punkt"
  ]
  node [
    id 133
    label "Nowy_York"
  ]
  node [
    id 134
    label "Waszyngton"
  ]
  node [
    id 135
    label "Pensylwania"
  ]
  node [
    id 136
    label "wektor"
  ]
  node [
    id 137
    label "Hawaje"
  ]
  node [
    id 138
    label "state"
  ]
  node [
    id 139
    label "poziom"
  ]
  node [
    id 140
    label "jednostka_administracyjna"
  ]
  node [
    id 141
    label "Illinois"
  ]
  node [
    id 142
    label "Oklahoma"
  ]
  node [
    id 143
    label "Oregon"
  ]
  node [
    id 144
    label "Arizona"
  ]
  node [
    id 145
    label "ilo&#347;&#263;"
  ]
  node [
    id 146
    label "Jukatan"
  ]
  node [
    id 147
    label "shape"
  ]
  node [
    id 148
    label "Goa"
  ]
  node [
    id 149
    label "palearktyka"
  ]
  node [
    id 150
    label "Dunaj"
  ]
  node [
    id 151
    label "Europa"
  ]
  node [
    id 152
    label "l&#261;d"
  ]
  node [
    id 153
    label "Eurazja"
  ]
  node [
    id 154
    label "Azja"
  ]
  node [
    id 155
    label "epejroforeza"
  ]
  node [
    id 156
    label "Ameryka"
  ]
  node [
    id 157
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 158
    label "Ren"
  ]
  node [
    id 159
    label "Antarktyda"
  ]
  node [
    id 160
    label "Australia"
  ]
  node [
    id 161
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 162
    label "Afryka"
  ]
  node [
    id 163
    label "Ameryka_Centralna"
  ]
  node [
    id 164
    label "blok_kontynentalny"
  ]
  node [
    id 165
    label "Germania"
  ]
  node [
    id 166
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 167
    label "Stary_&#346;wiat"
  ]
  node [
    id 168
    label "Europa_Wschodnia"
  ]
  node [
    id 169
    label "Europa_Zachodnia"
  ]
  node [
    id 170
    label "Moza"
  ]
  node [
    id 171
    label "pojazd"
  ]
  node [
    id 172
    label "skorupa_ziemska"
  ]
  node [
    id 173
    label "Hudson"
  ]
  node [
    id 174
    label "NATO"
  ]
  node [
    id 175
    label "dolar"
  ]
  node [
    id 176
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 177
    label "stan_wolny"
  ]
  node [
    id 178
    label "Po&#322;udnie"
  ]
  node [
    id 179
    label "Wuj_Sam"
  ]
  node [
    id 180
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 181
    label "zielona_karta"
  ]
  node [
    id 182
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 183
    label "P&#243;&#322;noc"
  ]
  node [
    id 184
    label "Nowy_&#346;wiat"
  ]
  node [
    id 185
    label "Zach&#243;d"
  ]
  node [
    id 186
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 187
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 188
    label "Tasmania"
  ]
  node [
    id 189
    label "dolar_australijski"
  ]
  node [
    id 190
    label "eurosceptyczny"
  ]
  node [
    id 191
    label "Bruksela"
  ]
  node [
    id 192
    label "euroentuzjazm"
  ]
  node [
    id 193
    label "euroko&#322;choz"
  ]
  node [
    id 194
    label "eurosceptycyzm"
  ]
  node [
    id 195
    label "Fundusze_Unijne"
  ]
  node [
    id 196
    label "eurorealizm"
  ]
  node [
    id 197
    label "Wsp&#243;lnota_Europejska"
  ]
  node [
    id 198
    label "strefa_euro"
  ]
  node [
    id 199
    label "euroentuzjasta"
  ]
  node [
    id 200
    label "Eurogrupa"
  ]
  node [
    id 201
    label "p&#322;atnik_netto"
  ]
  node [
    id 202
    label "eurosceptyk"
  ]
  node [
    id 203
    label "eurorealista"
  ]
  node [
    id 204
    label "prawo_unijne"
  ]
  node [
    id 205
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 206
    label "Azja_Wschodnia"
  ]
  node [
    id 207
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 208
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 209
    label "G&#243;ry_Ko&#322;ymskie"
  ]
  node [
    id 210
    label "Ussuri"
  ]
  node [
    id 211
    label "wsch&#243;d"
  ]
  node [
    id 212
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 213
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 214
    label "Rosja"
  ]
  node [
    id 215
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 216
    label "Antarktyka"
  ]
  node [
    id 217
    label "dolar_Antarktyki"
  ]
  node [
    id 218
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 219
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 220
    label "Nil"
  ]
  node [
    id 221
    label "Afryka_Wschodnia"
  ]
  node [
    id 222
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 223
    label "Afryka_Zachodnia"
  ]
  node [
    id 224
    label "ruchy"
  ]
  node [
    id 225
    label "sta&#322;y_l&#261;d"
  ]
  node [
    id 226
    label "tektonika"
  ]
  node [
    id 227
    label "warstwa_granitowa"
  ]
  node [
    id 228
    label "Niemcy"
  ]
  node [
    id 229
    label "Lotaryngia"
  ]
  node [
    id 230
    label "Kaukaz"
  ]
  node [
    id 231
    label "Wielki_Step"
  ]
  node [
    id 232
    label "Ural"
  ]
  node [
    id 233
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 234
    label "holarktyka"
  ]
  node [
    id 235
    label "wychodzi&#263;"
  ]
  node [
    id 236
    label "seclude"
  ]
  node [
    id 237
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 238
    label "overture"
  ]
  node [
    id 239
    label "perform"
  ]
  node [
    id 240
    label "dzia&#322;a&#263;"
  ]
  node [
    id 241
    label "odst&#281;powa&#263;"
  ]
  node [
    id 242
    label "act"
  ]
  node [
    id 243
    label "appear"
  ]
  node [
    id 244
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 245
    label "unwrap"
  ]
  node [
    id 246
    label "rezygnowa&#263;"
  ]
  node [
    id 247
    label "nak&#322;ania&#263;"
  ]
  node [
    id 248
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 249
    label "zach&#281;ca&#263;"
  ]
  node [
    id 250
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 251
    label "za&#322;atwi&#263;"
  ]
  node [
    id 252
    label "impart"
  ]
  node [
    id 253
    label "schodzi&#263;"
  ]
  node [
    id 254
    label "blend"
  ]
  node [
    id 255
    label "przedstawia&#263;"
  ]
  node [
    id 256
    label "publish"
  ]
  node [
    id 257
    label "ko&#324;czy&#263;"
  ]
  node [
    id 258
    label "wystarcza&#263;"
  ]
  node [
    id 259
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 260
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 261
    label "gra&#263;"
  ]
  node [
    id 262
    label "opuszcza&#263;"
  ]
  node [
    id 263
    label "wygl&#261;da&#263;"
  ]
  node [
    id 264
    label "give"
  ]
  node [
    id 265
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 266
    label "ograniczenie"
  ]
  node [
    id 267
    label "uzyskiwa&#263;"
  ]
  node [
    id 268
    label "wyrusza&#263;"
  ]
  node [
    id 269
    label "wypada&#263;"
  ]
  node [
    id 270
    label "heighten"
  ]
  node [
    id 271
    label "strona_&#347;wiata"
  ]
  node [
    id 272
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 273
    label "pochodzi&#263;"
  ]
  node [
    id 274
    label "przestawa&#263;"
  ]
  node [
    id 275
    label "charge"
  ]
  node [
    id 276
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 277
    label "odwr&#243;t"
  ]
  node [
    id 278
    label "surrender"
  ]
  node [
    id 279
    label "odsuwa&#263;_si&#281;"
  ]
  node [
    id 280
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 281
    label "commit"
  ]
  node [
    id 282
    label "function"
  ]
  node [
    id 283
    label "powodowa&#263;"
  ]
  node [
    id 284
    label "reakcja_chemiczna"
  ]
  node [
    id 285
    label "determine"
  ]
  node [
    id 286
    label "work"
  ]
  node [
    id 287
    label "du&#380;o"
  ]
  node [
    id 288
    label "wa&#380;ny"
  ]
  node [
    id 289
    label "niema&#322;o"
  ]
  node [
    id 290
    label "wiele"
  ]
  node [
    id 291
    label "prawdziwy"
  ]
  node [
    id 292
    label "rozwini&#281;ty"
  ]
  node [
    id 293
    label "doros&#322;y"
  ]
  node [
    id 294
    label "dorodny"
  ]
  node [
    id 295
    label "zgodny"
  ]
  node [
    id 296
    label "prawdziwie"
  ]
  node [
    id 297
    label "podobny"
  ]
  node [
    id 298
    label "m&#261;dry"
  ]
  node [
    id 299
    label "szczery"
  ]
  node [
    id 300
    label "naprawd&#281;"
  ]
  node [
    id 301
    label "naturalny"
  ]
  node [
    id 302
    label "&#380;ywny"
  ]
  node [
    id 303
    label "realnie"
  ]
  node [
    id 304
    label "zauwa&#380;alny"
  ]
  node [
    id 305
    label "znacznie"
  ]
  node [
    id 306
    label "silny"
  ]
  node [
    id 307
    label "wa&#380;nie"
  ]
  node [
    id 308
    label "eksponowany"
  ]
  node [
    id 309
    label "wynios&#322;y"
  ]
  node [
    id 310
    label "dobry"
  ]
  node [
    id 311
    label "istotnie"
  ]
  node [
    id 312
    label "dono&#347;ny"
  ]
  node [
    id 313
    label "do&#347;cig&#322;y"
  ]
  node [
    id 314
    label "ukszta&#322;towany"
  ]
  node [
    id 315
    label "&#378;ra&#322;y"
  ]
  node [
    id 316
    label "zdr&#243;w"
  ]
  node [
    id 317
    label "dorodnie"
  ]
  node [
    id 318
    label "okaza&#322;y"
  ]
  node [
    id 319
    label "mocno"
  ]
  node [
    id 320
    label "cz&#281;sto"
  ]
  node [
    id 321
    label "bardzo"
  ]
  node [
    id 322
    label "wiela"
  ]
  node [
    id 323
    label "cz&#322;owiek"
  ]
  node [
    id 324
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 325
    label "dojrza&#322;y"
  ]
  node [
    id 326
    label "wapniak"
  ]
  node [
    id 327
    label "senior"
  ]
  node [
    id 328
    label "dojrzale"
  ]
  node [
    id 329
    label "doro&#347;lenie"
  ]
  node [
    id 330
    label "wydoro&#347;lenie"
  ]
  node [
    id 331
    label "doletni"
  ]
  node [
    id 332
    label "doro&#347;le"
  ]
  node [
    id 333
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 334
    label "part"
  ]
  node [
    id 335
    label "rozmiar"
  ]
  node [
    id 336
    label "za&#322;o&#380;enie"
  ]
  node [
    id 337
    label "punkt_odniesienia"
  ]
  node [
    id 338
    label "glinowanie"
  ]
  node [
    id 339
    label "podglebie"
  ]
  node [
    id 340
    label "przestrze&#324;"
  ]
  node [
    id 341
    label "podk&#322;ad"
  ]
  node [
    id 342
    label "documentation"
  ]
  node [
    id 343
    label "czynnik_produkcji"
  ]
  node [
    id 344
    label "kompleks_sorpcyjny"
  ]
  node [
    id 345
    label "geosystem"
  ]
  node [
    id 346
    label "zasadzi&#263;"
  ]
  node [
    id 347
    label "zasadzenie"
  ]
  node [
    id 348
    label "glinowa&#263;"
  ]
  node [
    id 349
    label "litosfera"
  ]
  node [
    id 350
    label "penetrator"
  ]
  node [
    id 351
    label "teren"
  ]
  node [
    id 352
    label "glej"
  ]
  node [
    id 353
    label "ryzosfera"
  ]
  node [
    id 354
    label "dno"
  ]
  node [
    id 355
    label "plantowa&#263;"
  ]
  node [
    id 356
    label "podstawowy"
  ]
  node [
    id 357
    label "martwica"
  ]
  node [
    id 358
    label "pr&#243;chnica"
  ]
  node [
    id 359
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 360
    label "dotleni&#263;"
  ]
  node [
    id 361
    label "infliction"
  ]
  node [
    id 362
    label "spowodowanie"
  ]
  node [
    id 363
    label "proposition"
  ]
  node [
    id 364
    label "przygotowanie"
  ]
  node [
    id 365
    label "pozak&#322;adanie"
  ]
  node [
    id 366
    label "point"
  ]
  node [
    id 367
    label "program"
  ]
  node [
    id 368
    label "poubieranie"
  ]
  node [
    id 369
    label "rozebranie"
  ]
  node [
    id 370
    label "budowla"
  ]
  node [
    id 371
    label "przewidzenie"
  ]
  node [
    id 372
    label "zak&#322;adka"
  ]
  node [
    id 373
    label "czynno&#347;&#263;"
  ]
  node [
    id 374
    label "twierdzenie"
  ]
  node [
    id 375
    label "przygotowywanie"
  ]
  node [
    id 376
    label "podwini&#281;cie"
  ]
  node [
    id 377
    label "zap&#322;acenie"
  ]
  node [
    id 378
    label "wyko&#324;czenie"
  ]
  node [
    id 379
    label "struktura"
  ]
  node [
    id 380
    label "utworzenie"
  ]
  node [
    id 381
    label "przebranie"
  ]
  node [
    id 382
    label "obleczenie"
  ]
  node [
    id 383
    label "przymierzenie"
  ]
  node [
    id 384
    label "obleczenie_si&#281;"
  ]
  node [
    id 385
    label "przywdzianie"
  ]
  node [
    id 386
    label "umieszczenie"
  ]
  node [
    id 387
    label "zrobienie"
  ]
  node [
    id 388
    label "przyodzianie"
  ]
  node [
    id 389
    label "pokrycie"
  ]
  node [
    id 390
    label "liczba"
  ]
  node [
    id 391
    label "uk&#322;ad"
  ]
  node [
    id 392
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 393
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 394
    label "integer"
  ]
  node [
    id 395
    label "zlewanie_si&#281;"
  ]
  node [
    id 396
    label "pe&#322;ny"
  ]
  node [
    id 397
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 398
    label "kosmetyk"
  ]
  node [
    id 399
    label "substrate"
  ]
  node [
    id 400
    label "layer"
  ]
  node [
    id 401
    label "ro&#347;lina"
  ]
  node [
    id 402
    label "szczep"
  ]
  node [
    id 403
    label "base"
  ]
  node [
    id 404
    label "puder"
  ]
  node [
    id 405
    label "partia"
  ]
  node [
    id 406
    label "tor"
  ]
  node [
    id 407
    label "melodia"
  ]
  node [
    id 408
    label "farba"
  ]
  node [
    id 409
    label "rzecz"
  ]
  node [
    id 410
    label "poszycie_denne"
  ]
  node [
    id 411
    label "mato&#322;"
  ]
  node [
    id 412
    label "ground"
  ]
  node [
    id 413
    label "zero"
  ]
  node [
    id 414
    label "p&#322;aszczyzna"
  ]
  node [
    id 415
    label "osady_denne"
  ]
  node [
    id 416
    label "sp&#243;d"
  ]
  node [
    id 417
    label "immoblizacja"
  ]
  node [
    id 418
    label "mienie"
  ]
  node [
    id 419
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 420
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 421
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 422
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 423
    label "zakres"
  ]
  node [
    id 424
    label "miejsce_pracy"
  ]
  node [
    id 425
    label "przyroda"
  ]
  node [
    id 426
    label "wymiar"
  ]
  node [
    id 427
    label "nation"
  ]
  node [
    id 428
    label "w&#322;adza"
  ]
  node [
    id 429
    label "kontekst"
  ]
  node [
    id 430
    label "krajobraz"
  ]
  node [
    id 431
    label "przedzieli&#263;"
  ]
  node [
    id 432
    label "oktant"
  ]
  node [
    id 433
    label "przedzielenie"
  ]
  node [
    id 434
    label "zbi&#243;r"
  ]
  node [
    id 435
    label "przestw&#243;r"
  ]
  node [
    id 436
    label "rozdziela&#263;"
  ]
  node [
    id 437
    label "nielito&#347;ciwy"
  ]
  node [
    id 438
    label "czasoprzestrze&#324;"
  ]
  node [
    id 439
    label "niezmierzony"
  ]
  node [
    id 440
    label "bezbrze&#380;e"
  ]
  node [
    id 441
    label "rozdzielanie"
  ]
  node [
    id 442
    label "poj&#281;cie"
  ]
  node [
    id 443
    label "capacity"
  ]
  node [
    id 444
    label "plane"
  ]
  node [
    id 445
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 446
    label "zwierciad&#322;o"
  ]
  node [
    id 447
    label "pocz&#261;tkowy"
  ]
  node [
    id 448
    label "najwa&#380;niejszy"
  ]
  node [
    id 449
    label "podstawowo"
  ]
  node [
    id 450
    label "niezaawansowany"
  ]
  node [
    id 451
    label "plant"
  ]
  node [
    id 452
    label "przymocowa&#263;"
  ]
  node [
    id 453
    label "podstawa"
  ]
  node [
    id 454
    label "establish"
  ]
  node [
    id 455
    label "osnowa&#263;"
  ]
  node [
    id 456
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 457
    label "umie&#347;ci&#263;"
  ]
  node [
    id 458
    label "wetkn&#261;&#263;"
  ]
  node [
    id 459
    label "przetkanie"
  ]
  node [
    id 460
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 461
    label "odm&#322;odzenie"
  ]
  node [
    id 462
    label "zaczerpni&#281;cie"
  ]
  node [
    id 463
    label "anchor"
  ]
  node [
    id 464
    label "wetkni&#281;cie"
  ]
  node [
    id 465
    label "interposition"
  ]
  node [
    id 466
    label "przymocowanie"
  ]
  node [
    id 467
    label "aluminize"
  ]
  node [
    id 468
    label "gleba"
  ]
  node [
    id 469
    label "pokrywanie"
  ]
  node [
    id 470
    label "metalizowanie"
  ]
  node [
    id 471
    label "zabezpieczanie"
  ]
  node [
    id 472
    label "wzbogacanie"
  ]
  node [
    id 473
    label "wzbogaca&#263;"
  ]
  node [
    id 474
    label "pokrywa&#263;"
  ]
  node [
    id 475
    label "zabezpiecza&#263;"
  ]
  node [
    id 476
    label "metalizowa&#263;"
  ]
  node [
    id 477
    label "dostarczy&#263;"
  ]
  node [
    id 478
    label "woda"
  ]
  node [
    id 479
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 480
    label "nasyci&#263;"
  ]
  node [
    id 481
    label "r&#243;wna&#263;"
  ]
  node [
    id 482
    label "level"
  ]
  node [
    id 483
    label "uprawia&#263;"
  ]
  node [
    id 484
    label "urz&#261;dzenie"
  ]
  node [
    id 485
    label "bakteria"
  ]
  node [
    id 486
    label "system_korzeniowy"
  ]
  node [
    id 487
    label "choroba_bakteryjna"
  ]
  node [
    id 488
    label "fleczer"
  ]
  node [
    id 489
    label "kwas_huminowy"
  ]
  node [
    id 490
    label "schorzenie"
  ]
  node [
    id 491
    label "ubytek"
  ]
  node [
    id 492
    label "neuroglia"
  ]
  node [
    id 493
    label "kom&#243;rka_glejowa"
  ]
  node [
    id 494
    label "substancja_szara"
  ]
  node [
    id 495
    label "tkanka"
  ]
  node [
    id 496
    label "odle&#380;yna"
  ]
  node [
    id 497
    label "ska&#322;a_osadowa"
  ]
  node [
    id 498
    label "kamfenol"
  ]
  node [
    id 499
    label "zanikni&#281;cie"
  ]
  node [
    id 500
    label "&#322;yko"
  ]
  node [
    id 501
    label "korek"
  ]
  node [
    id 502
    label "necrosis"
  ]
  node [
    id 503
    label "zmiana_wsteczna"
  ]
  node [
    id 504
    label "warstwa_perydotytowa"
  ]
  node [
    id 505
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 506
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 507
    label "Ziemia"
  ]
  node [
    id 508
    label "sialma"
  ]
  node [
    id 509
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 510
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 511
    label "fauna"
  ]
  node [
    id 512
    label "powietrze"
  ]
  node [
    id 513
    label "uprawny"
  ]
  node [
    id 514
    label "oromy"
  ]
  node [
    id 515
    label "u&#380;ytkowy"
  ]
  node [
    id 516
    label "zrobi&#263;"
  ]
  node [
    id 517
    label "oblat"
  ]
  node [
    id 518
    label "appoint"
  ]
  node [
    id 519
    label "sta&#263;_si&#281;"
  ]
  node [
    id 520
    label "ustali&#263;"
  ]
  node [
    id 521
    label "zorganizowa&#263;"
  ]
  node [
    id 522
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 523
    label "wydali&#263;"
  ]
  node [
    id 524
    label "make"
  ]
  node [
    id 525
    label "wystylizowa&#263;"
  ]
  node [
    id 526
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 527
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 528
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 529
    label "post&#261;pi&#263;"
  ]
  node [
    id 530
    label "przerobi&#263;"
  ]
  node [
    id 531
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 532
    label "cause"
  ]
  node [
    id 533
    label "nabra&#263;"
  ]
  node [
    id 534
    label "zdecydowa&#263;"
  ]
  node [
    id 535
    label "put"
  ]
  node [
    id 536
    label "bind"
  ]
  node [
    id 537
    label "umocni&#263;"
  ]
  node [
    id 538
    label "spowodowa&#263;"
  ]
  node [
    id 539
    label "nowicjusz"
  ]
  node [
    id 540
    label "zakonnik"
  ]
  node [
    id 541
    label "oblaci"
  ]
  node [
    id 542
    label "dziecko"
  ]
  node [
    id 543
    label "przeznaczenie"
  ]
  node [
    id 544
    label "andrut"
  ]
  node [
    id 545
    label "&#347;wiecki"
  ]
  node [
    id 546
    label "bezglebowy"
  ]
  node [
    id 547
    label "brzoskwiniarnia"
  ]
  node [
    id 548
    label "praca_rolnicza"
  ]
  node [
    id 549
    label "uprawa_roli"
  ]
  node [
    id 550
    label "formacja_ro&#347;linna"
  ]
  node [
    id 551
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 552
    label "biom"
  ]
  node [
    id 553
    label "szata_ro&#347;linna"
  ]
  node [
    id 554
    label "zielono&#347;&#263;"
  ]
  node [
    id 555
    label "pi&#281;tro"
  ]
  node [
    id 556
    label "hydroponika"
  ]
  node [
    id 557
    label "orientacja"
  ]
  node [
    id 558
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 559
    label "skumanie"
  ]
  node [
    id 560
    label "pos&#322;uchanie"
  ]
  node [
    id 561
    label "wytw&#243;r"
  ]
  node [
    id 562
    label "teoria"
  ]
  node [
    id 563
    label "forma"
  ]
  node [
    id 564
    label "zorientowanie"
  ]
  node [
    id 565
    label "clasp"
  ]
  node [
    id 566
    label "przem&#243;wienie"
  ]
  node [
    id 567
    label "Kosowo"
  ]
  node [
    id 568
    label "Zabu&#380;e"
  ]
  node [
    id 569
    label "antroposfera"
  ]
  node [
    id 570
    label "Arktyka"
  ]
  node [
    id 571
    label "Notogea"
  ]
  node [
    id 572
    label "Piotrowo"
  ]
  node [
    id 573
    label "akrecja"
  ]
  node [
    id 574
    label "Ruda_Pabianicka"
  ]
  node [
    id 575
    label "Ludwin&#243;w"
  ]
  node [
    id 576
    label "po&#322;udnie"
  ]
  node [
    id 577
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 578
    label "Pow&#261;zki"
  ]
  node [
    id 579
    label "&#321;&#281;g"
  ]
  node [
    id 580
    label "p&#243;&#322;noc"
  ]
  node [
    id 581
    label "Rakowice"
  ]
  node [
    id 582
    label "Syberia_Wschodnia"
  ]
  node [
    id 583
    label "Zab&#322;ocie"
  ]
  node [
    id 584
    label "Kresy_Zachodnie"
  ]
  node [
    id 585
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 586
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 587
    label "terytorium"
  ]
  node [
    id 588
    label "pas_planetoid"
  ]
  node [
    id 589
    label "Syberia_Zachodnia"
  ]
  node [
    id 590
    label "Neogea"
  ]
  node [
    id 591
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 592
    label "Olszanica"
  ]
  node [
    id 593
    label "dymensja"
  ]
  node [
    id 594
    label "odzie&#380;"
  ]
  node [
    id 595
    label "znaczenie"
  ]
  node [
    id 596
    label "circumference"
  ]
  node [
    id 597
    label "warunek_lokalowy"
  ]
  node [
    id 598
    label "odbi&#263;"
  ]
  node [
    id 599
    label "plama"
  ]
  node [
    id 600
    label "odbija&#263;"
  ]
  node [
    id 601
    label "przedmiot"
  ]
  node [
    id 602
    label "przegl&#261;da&#263;_si&#281;"
  ]
  node [
    id 603
    label "wyraz"
  ]
  node [
    id 604
    label "odbicie"
  ]
  node [
    id 605
    label "zwierciad&#322;o_p&#322;askie"
  ]
  node [
    id 606
    label "odbijanie"
  ]
  node [
    id 607
    label "skrzyd&#322;o"
  ]
  node [
    id 608
    label "g&#322;ad&#378;"
  ]
  node [
    id 609
    label "polimer"
  ]
  node [
    id 610
    label "roztw&#243;r_koloidowy"
  ]
  node [
    id 611
    label "mark"
  ]
  node [
    id 612
    label "przygotowywa&#263;"
  ]
  node [
    id 613
    label "report"
  ]
  node [
    id 614
    label "cechowa&#263;"
  ]
  node [
    id 615
    label "opisywa&#263;"
  ]
  node [
    id 616
    label "arrange"
  ]
  node [
    id 617
    label "usposabia&#263;"
  ]
  node [
    id 618
    label "sposobi&#263;"
  ]
  node [
    id 619
    label "szkoli&#263;"
  ]
  node [
    id 620
    label "wytwarza&#263;"
  ]
  node [
    id 621
    label "pryczy&#263;"
  ]
  node [
    id 622
    label "wykonywa&#263;"
  ]
  node [
    id 623
    label "train"
  ]
  node [
    id 624
    label "zapoznawa&#263;"
  ]
  node [
    id 625
    label "represent"
  ]
  node [
    id 626
    label "umowa"
  ]
  node [
    id 627
    label "cover"
  ]
  node [
    id 628
    label "antycypacja"
  ]
  node [
    id 629
    label "facet"
  ]
  node [
    id 630
    label "przypuszczenie"
  ]
  node [
    id 631
    label "kr&#243;lestwo"
  ]
  node [
    id 632
    label "autorament"
  ]
  node [
    id 633
    label "rezultat"
  ]
  node [
    id 634
    label "sztuka"
  ]
  node [
    id 635
    label "cynk"
  ]
  node [
    id 636
    label "variety"
  ]
  node [
    id 637
    label "gromada"
  ]
  node [
    id 638
    label "jednostka_systematyczna"
  ]
  node [
    id 639
    label "obstawia&#263;"
  ]
  node [
    id 640
    label "design"
  ]
  node [
    id 641
    label "pob&#243;r"
  ]
  node [
    id 642
    label "wojsko"
  ]
  node [
    id 643
    label "type"
  ]
  node [
    id 644
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 645
    label "wygl&#261;d"
  ]
  node [
    id 646
    label "proces"
  ]
  node [
    id 647
    label "zjawisko"
  ]
  node [
    id 648
    label "pogl&#261;d"
  ]
  node [
    id 649
    label "narracja"
  ]
  node [
    id 650
    label "prediction"
  ]
  node [
    id 651
    label "zapowied&#378;"
  ]
  node [
    id 652
    label "upodobnienie"
  ]
  node [
    id 653
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 654
    label "ods&#322;ona"
  ]
  node [
    id 655
    label "scenariusz"
  ]
  node [
    id 656
    label "fortel"
  ]
  node [
    id 657
    label "kultura"
  ]
  node [
    id 658
    label "utw&#243;r"
  ]
  node [
    id 659
    label "kobieta"
  ]
  node [
    id 660
    label "ambala&#380;"
  ]
  node [
    id 661
    label "Apollo"
  ]
  node [
    id 662
    label "egzemplarz"
  ]
  node [
    id 663
    label "didaskalia"
  ]
  node [
    id 664
    label "czyn"
  ]
  node [
    id 665
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 666
    label "turn"
  ]
  node [
    id 667
    label "towar"
  ]
  node [
    id 668
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 669
    label "head"
  ]
  node [
    id 670
    label "scena"
  ]
  node [
    id 671
    label "kultura_duchowa"
  ]
  node [
    id 672
    label "przedstawienie"
  ]
  node [
    id 673
    label "theatrical_performance"
  ]
  node [
    id 674
    label "pokaz"
  ]
  node [
    id 675
    label "pr&#243;bowanie"
  ]
  node [
    id 676
    label "przedstawianie"
  ]
  node [
    id 677
    label "sprawno&#347;&#263;"
  ]
  node [
    id 678
    label "jednostka"
  ]
  node [
    id 679
    label "environment"
  ]
  node [
    id 680
    label "scenografia"
  ]
  node [
    id 681
    label "realizacja"
  ]
  node [
    id 682
    label "rola"
  ]
  node [
    id 683
    label "Faust"
  ]
  node [
    id 684
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 685
    label "przedstawi&#263;"
  ]
  node [
    id 686
    label "bratek"
  ]
  node [
    id 687
    label "dopuszczenie"
  ]
  node [
    id 688
    label "conjecture"
  ]
  node [
    id 689
    label "poszlaka"
  ]
  node [
    id 690
    label "koniektura"
  ]
  node [
    id 691
    label "datum"
  ]
  node [
    id 692
    label "asymilowa&#263;"
  ]
  node [
    id 693
    label "nasada"
  ]
  node [
    id 694
    label "profanum"
  ]
  node [
    id 695
    label "wz&#243;r"
  ]
  node [
    id 696
    label "asymilowanie"
  ]
  node [
    id 697
    label "os&#322;abia&#263;"
  ]
  node [
    id 698
    label "homo_sapiens"
  ]
  node [
    id 699
    label "osoba"
  ]
  node [
    id 700
    label "ludzko&#347;&#263;"
  ]
  node [
    id 701
    label "Adam"
  ]
  node [
    id 702
    label "hominid"
  ]
  node [
    id 703
    label "posta&#263;"
  ]
  node [
    id 704
    label "portrecista"
  ]
  node [
    id 705
    label "polifag"
  ]
  node [
    id 706
    label "podw&#322;adny"
  ]
  node [
    id 707
    label "dwun&#243;g"
  ]
  node [
    id 708
    label "duch"
  ]
  node [
    id 709
    label "os&#322;abianie"
  ]
  node [
    id 710
    label "antropochoria"
  ]
  node [
    id 711
    label "figura"
  ]
  node [
    id 712
    label "g&#322;owa"
  ]
  node [
    id 713
    label "mikrokosmos"
  ]
  node [
    id 714
    label "oddzia&#322;ywanie"
  ]
  node [
    id 715
    label "sygna&#322;"
  ]
  node [
    id 716
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 717
    label "tip"
  ]
  node [
    id 718
    label "cynkowiec"
  ]
  node [
    id 719
    label "mikroelement"
  ]
  node [
    id 720
    label "metal_kolorowy"
  ]
  node [
    id 721
    label "tip-off"
  ]
  node [
    id 722
    label "otacza&#263;"
  ]
  node [
    id 723
    label "obejmowa&#263;"
  ]
  node [
    id 724
    label "budowa&#263;"
  ]
  node [
    id 725
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 726
    label "powierza&#263;"
  ]
  node [
    id 727
    label "ochrona"
  ]
  node [
    id 728
    label "bramka"
  ]
  node [
    id 729
    label "zastawia&#263;"
  ]
  node [
    id 730
    label "przewidywa&#263;"
  ]
  node [
    id 731
    label "wysy&#322;a&#263;"
  ]
  node [
    id 732
    label "ubezpiecza&#263;"
  ]
  node [
    id 733
    label "broni&#263;"
  ]
  node [
    id 734
    label "venture"
  ]
  node [
    id 735
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 736
    label "os&#322;ania&#263;"
  ]
  node [
    id 737
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 738
    label "typowa&#263;"
  ]
  node [
    id 739
    label "frame"
  ]
  node [
    id 740
    label "zajmowa&#263;"
  ]
  node [
    id 741
    label "zapewnia&#263;"
  ]
  node [
    id 742
    label "przyczyna"
  ]
  node [
    id 743
    label "dzia&#322;anie"
  ]
  node [
    id 744
    label "event"
  ]
  node [
    id 745
    label "hurma"
  ]
  node [
    id 746
    label "botanika"
  ]
  node [
    id 747
    label "zoologia"
  ]
  node [
    id 748
    label "tribe"
  ]
  node [
    id 749
    label "skupienie"
  ]
  node [
    id 750
    label "grupa"
  ]
  node [
    id 751
    label "stage_set"
  ]
  node [
    id 752
    label "zwierz&#281;ta"
  ]
  node [
    id 753
    label "Arktogea"
  ]
  node [
    id 754
    label "pa&#324;stwo"
  ]
  node [
    id 755
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 756
    label "protisty"
  ]
  node [
    id 757
    label "domena"
  ]
  node [
    id 758
    label "kategoria_systematyczna"
  ]
  node [
    id 759
    label "ro&#347;liny"
  ]
  node [
    id 760
    label "grzyby"
  ]
  node [
    id 761
    label "prokarioty"
  ]
  node [
    id 762
    label "anektowanie"
  ]
  node [
    id 763
    label "stosowanie"
  ]
  node [
    id 764
    label "wydobywanie"
  ]
  node [
    id 765
    label "use"
  ]
  node [
    id 766
    label "occupation"
  ]
  node [
    id 767
    label "zaje&#380;d&#380;anie"
  ]
  node [
    id 768
    label "zu&#380;ywanie"
  ]
  node [
    id 769
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 770
    label "zniszczenie"
  ]
  node [
    id 771
    label "robienie"
  ]
  node [
    id 772
    label "przejaskrawianie"
  ]
  node [
    id 773
    label "wydostawanie"
  ]
  node [
    id 774
    label "extraction"
  ]
  node [
    id 775
    label "eksploatowanie"
  ]
  node [
    id 776
    label "g&#243;rnictwo"
  ]
  node [
    id 777
    label "powodowanie"
  ]
  node [
    id 778
    label "uwydatnianie"
  ]
  node [
    id 779
    label "evocation"
  ]
  node [
    id 780
    label "ratowanie"
  ]
  node [
    id 781
    label "dobywanie"
  ]
  node [
    id 782
    label "uzyskiwanie"
  ]
  node [
    id 783
    label "draw"
  ]
  node [
    id 784
    label "wyjmowanie"
  ]
  node [
    id 785
    label "zajmowanie"
  ]
  node [
    id 786
    label "u&#380;ycie"
  ]
  node [
    id 787
    label "zaj&#281;cie"
  ]
  node [
    id 788
    label "inkorporowanie"
  ]
  node [
    id 789
    label "dziwienie"
  ]
  node [
    id 790
    label "zatrzymywanie_si&#281;"
  ]
  node [
    id 791
    label "zam&#281;czanie"
  ]
  node [
    id 792
    label "zaginanie"
  ]
  node [
    id 793
    label "zaje&#380;d&#380;enie"
  ]
  node [
    id 794
    label "zabijanie"
  ]
  node [
    id 795
    label "przyje&#380;d&#380;anie"
  ]
  node [
    id 796
    label "blokowanie"
  ]
  node [
    id 797
    label "docieranie"
  ]
  node [
    id 798
    label "nudzenie"
  ]
  node [
    id 799
    label "dochodzenie"
  ]
  node [
    id 800
    label "niszczenie"
  ]
  node [
    id 801
    label "docinanie"
  ]
  node [
    id 802
    label "&#347;mierdzenie"
  ]
  node [
    id 803
    label "posadzka"
  ]
  node [
    id 804
    label "Lubuskie"
  ]
  node [
    id 805
    label "Ko&#322;yma"
  ]
  node [
    id 806
    label "Skandynawia"
  ]
  node [
    id 807
    label "Kampania"
  ]
  node [
    id 808
    label "Zakarpacie"
  ]
  node [
    id 809
    label "Podlasie"
  ]
  node [
    id 810
    label "Wielkopolska"
  ]
  node [
    id 811
    label "Indochiny"
  ]
  node [
    id 812
    label "Bo&#347;nia"
  ]
  node [
    id 813
    label "Opolszczyzna"
  ]
  node [
    id 814
    label "Armagnac"
  ]
  node [
    id 815
    label "kort"
  ]
  node [
    id 816
    label "Polesie"
  ]
  node [
    id 817
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 818
    label "Bawaria"
  ]
  node [
    id 819
    label "Yorkshire"
  ]
  node [
    id 820
    label "Syjon"
  ]
  node [
    id 821
    label "zapadnia"
  ]
  node [
    id 822
    label "Apulia"
  ]
  node [
    id 823
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 824
    label "Noworosja"
  ]
  node [
    id 825
    label "&#321;&#243;dzkie"
  ]
  node [
    id 826
    label "Nadrenia"
  ]
  node [
    id 827
    label "Kurpie"
  ]
  node [
    id 828
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 829
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 830
    label "Naddniestrze"
  ]
  node [
    id 831
    label "Baszkiria"
  ]
  node [
    id 832
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 833
    label "Andaluzja"
  ]
  node [
    id 834
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 835
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 836
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 837
    label "Opolskie"
  ]
  node [
    id 838
    label "Kociewie"
  ]
  node [
    id 839
    label "Anglia"
  ]
  node [
    id 840
    label "Bordeaux"
  ]
  node [
    id 841
    label "&#321;emkowszczyzna"
  ]
  node [
    id 842
    label "Mazowsze"
  ]
  node [
    id 843
    label "Laponia"
  ]
  node [
    id 844
    label "Amazonia"
  ]
  node [
    id 845
    label "Lasko"
  ]
  node [
    id 846
    label "Hercegowina"
  ]
  node [
    id 847
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 848
    label "Liguria"
  ]
  node [
    id 849
    label "Lubelszczyzna"
  ]
  node [
    id 850
    label "Tonkin"
  ]
  node [
    id 851
    label "Ukraina_Zachodnia"
  ]
  node [
    id 852
    label "Oceania"
  ]
  node [
    id 853
    label "Pamir"
  ]
  node [
    id 854
    label "Podkarpacie"
  ]
  node [
    id 855
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 856
    label "Tyrol"
  ]
  node [
    id 857
    label "Bory_Tucholskie"
  ]
  node [
    id 858
    label "Podhale"
  ]
  node [
    id 859
    label "Polinezja"
  ]
  node [
    id 860
    label "pomieszczenie"
  ]
  node [
    id 861
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 862
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 863
    label "Mazury"
  ]
  node [
    id 864
    label "Zabajkale"
  ]
  node [
    id 865
    label "Turyngia"
  ]
  node [
    id 866
    label "Kielecczyzna"
  ]
  node [
    id 867
    label "Ba&#322;kany"
  ]
  node [
    id 868
    label "Kaszuby"
  ]
  node [
    id 869
    label "Szlezwik"
  ]
  node [
    id 870
    label "Mikronezja"
  ]
  node [
    id 871
    label "Umbria"
  ]
  node [
    id 872
    label "Oksytania"
  ]
  node [
    id 873
    label "Mezoameryka"
  ]
  node [
    id 874
    label "Turkiestan"
  ]
  node [
    id 875
    label "Kurdystan"
  ]
  node [
    id 876
    label "Karaiby"
  ]
  node [
    id 877
    label "Biskupizna"
  ]
  node [
    id 878
    label "Podbeskidzie"
  ]
  node [
    id 879
    label "Zag&#243;rze"
  ]
  node [
    id 880
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 881
    label "Kalabria"
  ]
  node [
    id 882
    label "Szkocja"
  ]
  node [
    id 883
    label "Ma&#322;opolska"
  ]
  node [
    id 884
    label "domain"
  ]
  node [
    id 885
    label "Huculszczyzna"
  ]
  node [
    id 886
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 887
    label "Sand&#380;ak"
  ]
  node [
    id 888
    label "Kerala"
  ]
  node [
    id 889
    label "budynek"
  ]
  node [
    id 890
    label "S&#261;decczyzna"
  ]
  node [
    id 891
    label "Lombardia"
  ]
  node [
    id 892
    label "Toskania"
  ]
  node [
    id 893
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 894
    label "Galicja"
  ]
  node [
    id 895
    label "Palestyna"
  ]
  node [
    id 896
    label "Kabylia"
  ]
  node [
    id 897
    label "Pomorze_Zachodnie"
  ]
  node [
    id 898
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 899
    label "Lauda"
  ]
  node [
    id 900
    label "Kujawy"
  ]
  node [
    id 901
    label "Warmia"
  ]
  node [
    id 902
    label "Maghreb"
  ]
  node [
    id 903
    label "Kaszmir"
  ]
  node [
    id 904
    label "Bojkowszczyzna"
  ]
  node [
    id 905
    label "Amhara"
  ]
  node [
    id 906
    label "Zamojszczyzna"
  ]
  node [
    id 907
    label "Walia"
  ]
  node [
    id 908
    label "&#379;ywiecczyzna"
  ]
  node [
    id 909
    label "Powi&#347;le"
  ]
  node [
    id 910
    label "rz&#261;d"
  ]
  node [
    id 911
    label "uwaga"
  ]
  node [
    id 912
    label "praca"
  ]
  node [
    id 913
    label "plac"
  ]
  node [
    id 914
    label "location"
  ]
  node [
    id 915
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 916
    label "cia&#322;o"
  ]
  node [
    id 917
    label "status"
  ]
  node [
    id 918
    label "chwila"
  ]
  node [
    id 919
    label "siatka"
  ]
  node [
    id 920
    label "ubrani&#243;wka"
  ]
  node [
    id 921
    label "boisko"
  ]
  node [
    id 922
    label "tkanina_we&#322;niana"
  ]
  node [
    id 923
    label "p&#322;aszczak"
  ]
  node [
    id 924
    label "&#347;ciana"
  ]
  node [
    id 925
    label "ukszta&#322;towanie"
  ]
  node [
    id 926
    label "kwadrant"
  ]
  node [
    id 927
    label "degree"
  ]
  node [
    id 928
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 929
    label "surface"
  ]
  node [
    id 930
    label "kondygnacja"
  ]
  node [
    id 931
    label "klatka_schodowa"
  ]
  node [
    id 932
    label "front"
  ]
  node [
    id 933
    label "alkierz"
  ]
  node [
    id 934
    label "strop"
  ]
  node [
    id 935
    label "przedpro&#380;e"
  ]
  node [
    id 936
    label "dach"
  ]
  node [
    id 937
    label "pod&#322;oga"
  ]
  node [
    id 938
    label "Pentagon"
  ]
  node [
    id 939
    label "balkon"
  ]
  node [
    id 940
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 941
    label "zakamarek"
  ]
  node [
    id 942
    label "amfilada"
  ]
  node [
    id 943
    label "sklepienie"
  ]
  node [
    id 944
    label "apartment"
  ]
  node [
    id 945
    label "udost&#281;pnienie"
  ]
  node [
    id 946
    label "sufit"
  ]
  node [
    id 947
    label "odholowywa&#263;"
  ]
  node [
    id 948
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 949
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 950
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 951
    label "test_zderzeniowy"
  ]
  node [
    id 952
    label "nadwozie"
  ]
  node [
    id 953
    label "odholowa&#263;"
  ]
  node [
    id 954
    label "przeszklenie"
  ]
  node [
    id 955
    label "fukni&#281;cie"
  ]
  node [
    id 956
    label "tabor"
  ]
  node [
    id 957
    label "odzywka"
  ]
  node [
    id 958
    label "podwozie"
  ]
  node [
    id 959
    label "przyholowywanie"
  ]
  node [
    id 960
    label "przyholowanie"
  ]
  node [
    id 961
    label "przyholowywa&#263;"
  ]
  node [
    id 962
    label "przyholowa&#263;"
  ]
  node [
    id 963
    label "odholowywanie"
  ]
  node [
    id 964
    label "prowadzenie_si&#281;"
  ]
  node [
    id 965
    label "odholowanie"
  ]
  node [
    id 966
    label "hamulec"
  ]
  node [
    id 967
    label "fukanie"
  ]
  node [
    id 968
    label "Judea"
  ]
  node [
    id 969
    label "Kanaan"
  ]
  node [
    id 970
    label "moszaw"
  ]
  node [
    id 971
    label "Anglosas"
  ]
  node [
    id 972
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 973
    label "Etiopia"
  ]
  node [
    id 974
    label "Jerozolima"
  ]
  node [
    id 975
    label "Beskidy_Zachodnie"
  ]
  node [
    id 976
    label "Wiktoria"
  ]
  node [
    id 977
    label "Guernsey"
  ]
  node [
    id 978
    label "Conrad"
  ]
  node [
    id 979
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 980
    label "funt_szterling"
  ]
  node [
    id 981
    label "Unia_Europejska"
  ]
  node [
    id 982
    label "Portland"
  ]
  node [
    id 983
    label "El&#380;bieta_I"
  ]
  node [
    id 984
    label "Kornwalia"
  ]
  node [
    id 985
    label "Wielka_Brytania"
  ]
  node [
    id 986
    label "Dolna_Frankonia"
  ]
  node [
    id 987
    label "W&#322;ochy"
  ]
  node [
    id 988
    label "Ukraina"
  ]
  node [
    id 989
    label "Nauru"
  ]
  node [
    id 990
    label "Wyspy_Marshalla"
  ]
  node [
    id 991
    label "Mariany"
  ]
  node [
    id 992
    label "Karpaty"
  ]
  node [
    id 993
    label "Beskid_Niski"
  ]
  node [
    id 994
    label "Mariensztat"
  ]
  node [
    id 995
    label "Polska"
  ]
  node [
    id 996
    label "Warszawa"
  ]
  node [
    id 997
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 998
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 999
    label "Paj&#281;czno"
  ]
  node [
    id 1000
    label "Mogielnica"
  ]
  node [
    id 1001
    label "Gop&#322;o"
  ]
  node [
    id 1002
    label "Francja"
  ]
  node [
    id 1003
    label "Poprad"
  ]
  node [
    id 1004
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1005
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1006
    label "Wilkowo_Polskie"
  ]
  node [
    id 1007
    label "Obra"
  ]
  node [
    id 1008
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1009
    label "Bojanowo"
  ]
  node [
    id 1010
    label "Dobra"
  ]
  node [
    id 1011
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1012
    label "Tuwalu"
  ]
  node [
    id 1013
    label "Samoa"
  ]
  node [
    id 1014
    label "Tonga"
  ]
  node [
    id 1015
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1016
    label "Etruria"
  ]
  node [
    id 1017
    label "Rumelia"
  ]
  node [
    id 1018
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1019
    label "Melanezja"
  ]
  node [
    id 1020
    label "Nowa_Zelandia"
  ]
  node [
    id 1021
    label "Ocean_Spokojny"
  ]
  node [
    id 1022
    label "Palau"
  ]
  node [
    id 1023
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1024
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1025
    label "Inguszetia"
  ]
  node [
    id 1026
    label "Czeczenia"
  ]
  node [
    id 1027
    label "Abchazja"
  ]
  node [
    id 1028
    label "Sarmata"
  ]
  node [
    id 1029
    label "Dagestan"
  ]
  node [
    id 1030
    label "Pakistan"
  ]
  node [
    id 1031
    label "Indie"
  ]
  node [
    id 1032
    label "Czarnog&#243;ra"
  ]
  node [
    id 1033
    label "Serbia"
  ]
  node [
    id 1034
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1035
    label "Podtatrze"
  ]
  node [
    id 1036
    label "Tatry"
  ]
  node [
    id 1037
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1038
    label "jezioro"
  ]
  node [
    id 1039
    label "&#346;l&#261;sk"
  ]
  node [
    id 1040
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1041
    label "Mo&#322;dawia"
  ]
  node [
    id 1042
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1043
    label "Podole"
  ]
  node [
    id 1044
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1045
    label "Austro-W&#281;gry"
  ]
  node [
    id 1046
    label "Hiszpania"
  ]
  node [
    id 1047
    label "Algieria"
  ]
  node [
    id 1048
    label "funt_szkocki"
  ]
  node [
    id 1049
    label "Kaledonia"
  ]
  node [
    id 1050
    label "Libia"
  ]
  node [
    id 1051
    label "Maroko"
  ]
  node [
    id 1052
    label "Sahara_Zachodnia"
  ]
  node [
    id 1053
    label "Tunezja"
  ]
  node [
    id 1054
    label "Mauretania"
  ]
  node [
    id 1055
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1056
    label "Ropa"
  ]
  node [
    id 1057
    label "Rogo&#378;nik"
  ]
  node [
    id 1058
    label "Iwanowice"
  ]
  node [
    id 1059
    label "Biskupice"
  ]
  node [
    id 1060
    label "Buriacja"
  ]
  node [
    id 1061
    label "Rozewie"
  ]
  node [
    id 1062
    label "Finlandia"
  ]
  node [
    id 1063
    label "Norwegia"
  ]
  node [
    id 1064
    label "Szwecja"
  ]
  node [
    id 1065
    label "Anguilla"
  ]
  node [
    id 1066
    label "Portoryko"
  ]
  node [
    id 1067
    label "Kuba"
  ]
  node [
    id 1068
    label "Bahamy"
  ]
  node [
    id 1069
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1070
    label "Antyle"
  ]
  node [
    id 1071
    label "Aruba"
  ]
  node [
    id 1072
    label "Kajmany"
  ]
  node [
    id 1073
    label "Haiti"
  ]
  node [
    id 1074
    label "Jamajka"
  ]
  node [
    id 1075
    label "Czechy"
  ]
  node [
    id 1076
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1077
    label "Amazonka"
  ]
  node [
    id 1078
    label "Wietnam"
  ]
  node [
    id 1079
    label "Alpy"
  ]
  node [
    id 1080
    label "Austria"
  ]
  node [
    id 1081
    label "Japonia"
  ]
  node [
    id 1082
    label "Zair"
  ]
  node [
    id 1083
    label "Belize"
  ]
  node [
    id 1084
    label "San_Marino"
  ]
  node [
    id 1085
    label "Tanzania"
  ]
  node [
    id 1086
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1087
    label "Senegal"
  ]
  node [
    id 1088
    label "Seszele"
  ]
  node [
    id 1089
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1090
    label "Zimbabwe"
  ]
  node [
    id 1091
    label "Filipiny"
  ]
  node [
    id 1092
    label "Malezja"
  ]
  node [
    id 1093
    label "Rumunia"
  ]
  node [
    id 1094
    label "Surinam"
  ]
  node [
    id 1095
    label "Syria"
  ]
  node [
    id 1096
    label "Burkina_Faso"
  ]
  node [
    id 1097
    label "Grecja"
  ]
  node [
    id 1098
    label "Wenezuela"
  ]
  node [
    id 1099
    label "Nepal"
  ]
  node [
    id 1100
    label "Suazi"
  ]
  node [
    id 1101
    label "S&#322;owacja"
  ]
  node [
    id 1102
    label "Chiny"
  ]
  node [
    id 1103
    label "Grenada"
  ]
  node [
    id 1104
    label "Barbados"
  ]
  node [
    id 1105
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1106
    label "Bahrajn"
  ]
  node [
    id 1107
    label "Komory"
  ]
  node [
    id 1108
    label "Rodezja"
  ]
  node [
    id 1109
    label "Malawi"
  ]
  node [
    id 1110
    label "Gwinea"
  ]
  node [
    id 1111
    label "Wehrlen"
  ]
  node [
    id 1112
    label "Meksyk"
  ]
  node [
    id 1113
    label "Liechtenstein"
  ]
  node [
    id 1114
    label "Kuwejt"
  ]
  node [
    id 1115
    label "Angola"
  ]
  node [
    id 1116
    label "Monako"
  ]
  node [
    id 1117
    label "Jemen"
  ]
  node [
    id 1118
    label "Madagaskar"
  ]
  node [
    id 1119
    label "Kolumbia"
  ]
  node [
    id 1120
    label "Mauritius"
  ]
  node [
    id 1121
    label "Kostaryka"
  ]
  node [
    id 1122
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1123
    label "Tajlandia"
  ]
  node [
    id 1124
    label "Argentyna"
  ]
  node [
    id 1125
    label "Zambia"
  ]
  node [
    id 1126
    label "Sri_Lanka"
  ]
  node [
    id 1127
    label "Gwatemala"
  ]
  node [
    id 1128
    label "Kirgistan"
  ]
  node [
    id 1129
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1130
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1131
    label "Salwador"
  ]
  node [
    id 1132
    label "Korea"
  ]
  node [
    id 1133
    label "Macedonia"
  ]
  node [
    id 1134
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1135
    label "Brunei"
  ]
  node [
    id 1136
    label "Mozambik"
  ]
  node [
    id 1137
    label "Turcja"
  ]
  node [
    id 1138
    label "Kambod&#380;a"
  ]
  node [
    id 1139
    label "Benin"
  ]
  node [
    id 1140
    label "Bhutan"
  ]
  node [
    id 1141
    label "Izrael"
  ]
  node [
    id 1142
    label "Sierra_Leone"
  ]
  node [
    id 1143
    label "Rwanda"
  ]
  node [
    id 1144
    label "holoarktyka"
  ]
  node [
    id 1145
    label "Nigeria"
  ]
  node [
    id 1146
    label "USA"
  ]
  node [
    id 1147
    label "Oman"
  ]
  node [
    id 1148
    label "Luksemburg"
  ]
  node [
    id 1149
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1150
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1151
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1152
    label "Dominikana"
  ]
  node [
    id 1153
    label "Irlandia"
  ]
  node [
    id 1154
    label "Liban"
  ]
  node [
    id 1155
    label "Hanower"
  ]
  node [
    id 1156
    label "Estonia"
  ]
  node [
    id 1157
    label "Gabon"
  ]
  node [
    id 1158
    label "Iran"
  ]
  node [
    id 1159
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1160
    label "S&#322;owenia"
  ]
  node [
    id 1161
    label "Egipt"
  ]
  node [
    id 1162
    label "Kiribati"
  ]
  node [
    id 1163
    label "Togo"
  ]
  node [
    id 1164
    label "Mongolia"
  ]
  node [
    id 1165
    label "Sudan"
  ]
  node [
    id 1166
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1167
    label "Bangladesz"
  ]
  node [
    id 1168
    label "Holandia"
  ]
  node [
    id 1169
    label "Birma"
  ]
  node [
    id 1170
    label "Albania"
  ]
  node [
    id 1171
    label "Gambia"
  ]
  node [
    id 1172
    label "Kazachstan"
  ]
  node [
    id 1173
    label "interior"
  ]
  node [
    id 1174
    label "Uzbekistan"
  ]
  node [
    id 1175
    label "Malta"
  ]
  node [
    id 1176
    label "Lesoto"
  ]
  node [
    id 1177
    label "Antarktis"
  ]
  node [
    id 1178
    label "Andora"
  ]
  node [
    id 1179
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1180
    label "Chorwacja"
  ]
  node [
    id 1181
    label "Kamerun"
  ]
  node [
    id 1182
    label "Urugwaj"
  ]
  node [
    id 1183
    label "Niger"
  ]
  node [
    id 1184
    label "Turkmenistan"
  ]
  node [
    id 1185
    label "Szwajcaria"
  ]
  node [
    id 1186
    label "zwrot"
  ]
  node [
    id 1187
    label "organizacja"
  ]
  node [
    id 1188
    label "Litwa"
  ]
  node [
    id 1189
    label "Gruzja"
  ]
  node [
    id 1190
    label "Kongo"
  ]
  node [
    id 1191
    label "Tajwan"
  ]
  node [
    id 1192
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1193
    label "Honduras"
  ]
  node [
    id 1194
    label "Boliwia"
  ]
  node [
    id 1195
    label "Uganda"
  ]
  node [
    id 1196
    label "Namibia"
  ]
  node [
    id 1197
    label "Erytrea"
  ]
  node [
    id 1198
    label "Azerbejd&#380;an"
  ]
  node [
    id 1199
    label "Panama"
  ]
  node [
    id 1200
    label "Gujana"
  ]
  node [
    id 1201
    label "Somalia"
  ]
  node [
    id 1202
    label "Burundi"
  ]
  node [
    id 1203
    label "Katar"
  ]
  node [
    id 1204
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1205
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1206
    label "Gwinea_Bissau"
  ]
  node [
    id 1207
    label "Bu&#322;garia"
  ]
  node [
    id 1208
    label "Nikaragua"
  ]
  node [
    id 1209
    label "Fid&#380;i"
  ]
  node [
    id 1210
    label "Timor_Wschodni"
  ]
  node [
    id 1211
    label "Laos"
  ]
  node [
    id 1212
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1213
    label "Ghana"
  ]
  node [
    id 1214
    label "Brazylia"
  ]
  node [
    id 1215
    label "Belgia"
  ]
  node [
    id 1216
    label "Irak"
  ]
  node [
    id 1217
    label "Peru"
  ]
  node [
    id 1218
    label "Arabia_Saudyjska"
  ]
  node [
    id 1219
    label "Indonezja"
  ]
  node [
    id 1220
    label "Malediwy"
  ]
  node [
    id 1221
    label "Afganistan"
  ]
  node [
    id 1222
    label "Jordania"
  ]
  node [
    id 1223
    label "Kenia"
  ]
  node [
    id 1224
    label "Czad"
  ]
  node [
    id 1225
    label "Liberia"
  ]
  node [
    id 1226
    label "Mali"
  ]
  node [
    id 1227
    label "Armenia"
  ]
  node [
    id 1228
    label "W&#281;gry"
  ]
  node [
    id 1229
    label "Chile"
  ]
  node [
    id 1230
    label "Kanada"
  ]
  node [
    id 1231
    label "Cypr"
  ]
  node [
    id 1232
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1233
    label "Ekwador"
  ]
  node [
    id 1234
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1235
    label "Wyspy_Salomona"
  ]
  node [
    id 1236
    label "&#321;otwa"
  ]
  node [
    id 1237
    label "D&#380;ibuti"
  ]
  node [
    id 1238
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1239
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1240
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1241
    label "Portugalia"
  ]
  node [
    id 1242
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1243
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1244
    label "Botswana"
  ]
  node [
    id 1245
    label "Dominika"
  ]
  node [
    id 1246
    label "Paragwaj"
  ]
  node [
    id 1247
    label "Tad&#380;ykistan"
  ]
  node [
    id 1248
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1249
    label "Khitai"
  ]
  node [
    id 1250
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1251
    label "pu&#322;apka"
  ]
  node [
    id 1252
    label "okre&#347;la&#263;"
  ]
  node [
    id 1253
    label "stanowi&#263;"
  ]
  node [
    id 1254
    label "ustala&#263;"
  ]
  node [
    id 1255
    label "set"
  ]
  node [
    id 1256
    label "wskazywa&#263;"
  ]
  node [
    id 1257
    label "signify"
  ]
  node [
    id 1258
    label "wybiera&#263;"
  ]
  node [
    id 1259
    label "podawa&#263;"
  ]
  node [
    id 1260
    label "pokazywa&#263;"
  ]
  node [
    id 1261
    label "warto&#347;&#263;"
  ]
  node [
    id 1262
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1263
    label "podkre&#347;la&#263;"
  ]
  node [
    id 1264
    label "indicate"
  ]
  node [
    id 1265
    label "style"
  ]
  node [
    id 1266
    label "decydowa&#263;"
  ]
  node [
    id 1267
    label "zmienia&#263;"
  ]
  node [
    id 1268
    label "umacnia&#263;"
  ]
  node [
    id 1269
    label "peddle"
  ]
  node [
    id 1270
    label "pies_my&#347;liwski"
  ]
  node [
    id 1271
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1272
    label "decide"
  ]
  node [
    id 1273
    label "typify"
  ]
  node [
    id 1274
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1275
    label "zatrzymywa&#263;"
  ]
  node [
    id 1276
    label "kompozycja"
  ]
  node [
    id 1277
    label "gem"
  ]
  node [
    id 1278
    label "muzyka"
  ]
  node [
    id 1279
    label "runda"
  ]
  node [
    id 1280
    label "zestaw"
  ]
  node [
    id 1281
    label "leksem"
  ]
  node [
    id 1282
    label "oznaka"
  ]
  node [
    id 1283
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 1284
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1285
    label "term"
  ]
  node [
    id 1286
    label "element"
  ]
  node [
    id 1287
    label "&#347;wiadczenie"
  ]
  node [
    id 1288
    label "zauwa&#380;alnie"
  ]
  node [
    id 1289
    label "postrzegalny"
  ]
  node [
    id 1290
    label "pakiet_klimatyczny"
  ]
  node [
    id 1291
    label "uprawianie"
  ]
  node [
    id 1292
    label "collection"
  ]
  node [
    id 1293
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1294
    label "gathering"
  ]
  node [
    id 1295
    label "album"
  ]
  node [
    id 1296
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1297
    label "sum"
  ]
  node [
    id 1298
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1299
    label "series"
  ]
  node [
    id 1300
    label "dane"
  ]
  node [
    id 1301
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1302
    label "Jukon"
  ]
  node [
    id 1303
    label "wielko&#347;&#263;"
  ]
  node [
    id 1304
    label "parametr"
  ]
  node [
    id 1305
    label "strona"
  ]
  node [
    id 1306
    label "sunset"
  ]
  node [
    id 1307
    label "trud"
  ]
  node [
    id 1308
    label "s&#322;o&#324;ce"
  ]
  node [
    id 1309
    label "szar&#243;wka"
  ]
  node [
    id 1310
    label "wiecz&#243;r"
  ]
  node [
    id 1311
    label "usi&#322;owanie"
  ]
  node [
    id 1312
    label "pora"
  ]
  node [
    id 1313
    label "&#347;rodek"
  ]
  node [
    id 1314
    label "dwunasta"
  ]
  node [
    id 1315
    label "dzie&#324;"
  ]
  node [
    id 1316
    label "godzina"
  ]
  node [
    id 1317
    label "szabas"
  ]
  node [
    id 1318
    label "pocz&#261;tek"
  ]
  node [
    id 1319
    label "rano"
  ]
  node [
    id 1320
    label "brzask"
  ]
  node [
    id 1321
    label "&#347;wiat"
  ]
  node [
    id 1322
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1323
    label "noc"
  ]
  node [
    id 1324
    label "Boreasz"
  ]
  node [
    id 1325
    label "Podg&#243;rze"
  ]
  node [
    id 1326
    label "nearktyka"
  ]
  node [
    id 1327
    label "euro"
  ]
  node [
    id 1328
    label "Bie&#380;an&#243;w-Prokocim"
  ]
  node [
    id 1329
    label "Kaw&#281;czyn"
  ]
  node [
    id 1330
    label "Kresy"
  ]
  node [
    id 1331
    label "biosfera"
  ]
  node [
    id 1332
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1333
    label "D&#281;bniki"
  ]
  node [
    id 1334
    label "lodowiec_kontynentalny"
  ]
  node [
    id 1335
    label "Pr&#261;dnik_Czerwony"
  ]
  node [
    id 1336
    label "Czy&#380;yny"
  ]
  node [
    id 1337
    label "Zwierzyniec"
  ]
  node [
    id 1338
    label "G&#322;uszyna"
  ]
  node [
    id 1339
    label "Rataje"
  ]
  node [
    id 1340
    label "wzrost"
  ]
  node [
    id 1341
    label "rozrost"
  ]
  node [
    id 1342
    label "dysk_akrecyjny"
  ]
  node [
    id 1343
    label "proces_biologiczny"
  ]
  node [
    id 1344
    label "accretion"
  ]
  node [
    id 1345
    label "podzakres"
  ]
  node [
    id 1346
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1347
    label "granica"
  ]
  node [
    id 1348
    label "circle"
  ]
  node [
    id 1349
    label "desygnat"
  ]
  node [
    id 1350
    label "dziedzina"
  ]
  node [
    id 1351
    label "sfera"
  ]
  node [
    id 1352
    label "energia"
  ]
  node [
    id 1353
    label "wedyzm"
  ]
  node [
    id 1354
    label "buddyzm"
  ]
  node [
    id 1355
    label "egzergia"
  ]
  node [
    id 1356
    label "emitowanie"
  ]
  node [
    id 1357
    label "power"
  ]
  node [
    id 1358
    label "emitowa&#263;"
  ]
  node [
    id 1359
    label "szwung"
  ]
  node [
    id 1360
    label "energy"
  ]
  node [
    id 1361
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1362
    label "kwant_energii"
  ]
  node [
    id 1363
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1364
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1365
    label "Buddhism"
  ]
  node [
    id 1366
    label "wad&#378;rajana"
  ]
  node [
    id 1367
    label "arahant"
  ]
  node [
    id 1368
    label "tantryzm"
  ]
  node [
    id 1369
    label "therawada"
  ]
  node [
    id 1370
    label "mahajana"
  ]
  node [
    id 1371
    label "kalpa"
  ]
  node [
    id 1372
    label "li"
  ]
  node [
    id 1373
    label "bardo"
  ]
  node [
    id 1374
    label "dana"
  ]
  node [
    id 1375
    label "ahinsa"
  ]
  node [
    id 1376
    label "religia"
  ]
  node [
    id 1377
    label "lampka_ma&#347;lana"
  ]
  node [
    id 1378
    label "asura"
  ]
  node [
    id 1379
    label "hinajana"
  ]
  node [
    id 1380
    label "bonzo"
  ]
  node [
    id 1381
    label "hinduizm"
  ]
  node [
    id 1382
    label "odpowiedni"
  ]
  node [
    id 1383
    label "dogodnie"
  ]
  node [
    id 1384
    label "skuteczny"
  ]
  node [
    id 1385
    label "ca&#322;y"
  ]
  node [
    id 1386
    label "czw&#243;rka"
  ]
  node [
    id 1387
    label "spokojny"
  ]
  node [
    id 1388
    label "pos&#322;uszny"
  ]
  node [
    id 1389
    label "korzystny"
  ]
  node [
    id 1390
    label "drogi"
  ]
  node [
    id 1391
    label "pozytywny"
  ]
  node [
    id 1392
    label "moralny"
  ]
  node [
    id 1393
    label "pomy&#347;lny"
  ]
  node [
    id 1394
    label "powitanie"
  ]
  node [
    id 1395
    label "grzeczny"
  ]
  node [
    id 1396
    label "&#347;mieszny"
  ]
  node [
    id 1397
    label "dobrze"
  ]
  node [
    id 1398
    label "dobroczynny"
  ]
  node [
    id 1399
    label "mi&#322;y"
  ]
  node [
    id 1400
    label "stosownie"
  ]
  node [
    id 1401
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1402
    label "nale&#380;yty"
  ]
  node [
    id 1403
    label "zdarzony"
  ]
  node [
    id 1404
    label "odpowiednio"
  ]
  node [
    id 1405
    label "specjalny"
  ]
  node [
    id 1406
    label "odpowiadanie"
  ]
  node [
    id 1407
    label "nale&#380;ny"
  ]
  node [
    id 1408
    label "opportunely"
  ]
  node [
    id 1409
    label "faktor"
  ]
  node [
    id 1410
    label "ekspozycja"
  ]
  node [
    id 1411
    label "condition"
  ]
  node [
    id 1412
    label "agent"
  ]
  node [
    id 1413
    label "sytuacja"
  ]
  node [
    id 1414
    label "sk&#322;adnik"
  ]
  node [
    id 1415
    label "wydarzenie"
  ]
  node [
    id 1416
    label "warunki"
  ]
  node [
    id 1417
    label "zawarcie"
  ]
  node [
    id 1418
    label "zawrze&#263;"
  ]
  node [
    id 1419
    label "contract"
  ]
  node [
    id 1420
    label "porozumienie"
  ]
  node [
    id 1421
    label "gestia_transportowa"
  ]
  node [
    id 1422
    label "klauzula"
  ]
  node [
    id 1423
    label "programowanie_agentowe"
  ]
  node [
    id 1424
    label "system_wieloagentowy"
  ]
  node [
    id 1425
    label "kontrakt"
  ]
  node [
    id 1426
    label "wywiad"
  ]
  node [
    id 1427
    label "orygina&#322;"
  ]
  node [
    id 1428
    label "dzier&#380;awca"
  ]
  node [
    id 1429
    label "rep"
  ]
  node [
    id 1430
    label "&#347;ledziciel"
  ]
  node [
    id 1431
    label "informator"
  ]
  node [
    id 1432
    label "przedstawiciel"
  ]
  node [
    id 1433
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1434
    label "agentura"
  ]
  node [
    id 1435
    label "funkcjonariusz"
  ]
  node [
    id 1436
    label "detektyw"
  ]
  node [
    id 1437
    label "po&#347;rednik"
  ]
  node [
    id 1438
    label "czynnik"
  ]
  node [
    id 1439
    label "filtr"
  ]
  node [
    id 1440
    label "kustosz"
  ]
  node [
    id 1441
    label "galeria"
  ]
  node [
    id 1442
    label "fotografia"
  ]
  node [
    id 1443
    label "spot"
  ]
  node [
    id 1444
    label "wprowadzenie"
  ]
  node [
    id 1445
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1446
    label "muzeum"
  ]
  node [
    id 1447
    label "impreza"
  ]
  node [
    id 1448
    label "Arsena&#322;"
  ]
  node [
    id 1449
    label "akcja"
  ]
  node [
    id 1450
    label "wystawienie"
  ]
  node [
    id 1451
    label "wst&#281;p"
  ]
  node [
    id 1452
    label "kurator"
  ]
  node [
    id 1453
    label "operacja"
  ]
  node [
    id 1454
    label "kolekcja"
  ]
  node [
    id 1455
    label "wernisa&#380;"
  ]
  node [
    id 1456
    label "Agropromocja"
  ]
  node [
    id 1457
    label "wystawa"
  ]
  node [
    id 1458
    label "wspinaczka"
  ]
  node [
    id 1459
    label "wprowadzanie"
  ]
  node [
    id 1460
    label "g&#243;rowanie"
  ]
  node [
    id 1461
    label "ukierunkowywanie"
  ]
  node [
    id 1462
    label "poprowadzenie"
  ]
  node [
    id 1463
    label "eksponowanie"
  ]
  node [
    id 1464
    label "doprowadzenie"
  ]
  node [
    id 1465
    label "przeci&#261;ganie"
  ]
  node [
    id 1466
    label "sterowanie"
  ]
  node [
    id 1467
    label "dysponowanie"
  ]
  node [
    id 1468
    label "kszta&#322;towanie"
  ]
  node [
    id 1469
    label "management"
  ]
  node [
    id 1470
    label "trzymanie"
  ]
  node [
    id 1471
    label "dawanie"
  ]
  node [
    id 1472
    label "linia_melodyczna"
  ]
  node [
    id 1473
    label "prowadzi&#263;"
  ]
  node [
    id 1474
    label "drive"
  ]
  node [
    id 1475
    label "przywodzenie"
  ]
  node [
    id 1476
    label "aim"
  ]
  node [
    id 1477
    label "lead"
  ]
  node [
    id 1478
    label "oprowadzenie"
  ]
  node [
    id 1479
    label "prowadzanie"
  ]
  node [
    id 1480
    label "oprowadzanie"
  ]
  node [
    id 1481
    label "przewy&#380;szanie"
  ]
  node [
    id 1482
    label "kierowanie"
  ]
  node [
    id 1483
    label "zwracanie"
  ]
  node [
    id 1484
    label "przeci&#281;cie"
  ]
  node [
    id 1485
    label "granie"
  ]
  node [
    id 1486
    label "ta&#324;czenie"
  ]
  node [
    id 1487
    label "kre&#347;lenie"
  ]
  node [
    id 1488
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 1489
    label "zaprowadzanie"
  ]
  node [
    id 1490
    label "pozarz&#261;dzanie"
  ]
  node [
    id 1491
    label "krzywa"
  ]
  node [
    id 1492
    label "przecinanie"
  ]
  node [
    id 1493
    label "doprowadzanie"
  ]
  node [
    id 1494
    label "pokre&#347;lenie"
  ]
  node [
    id 1495
    label "usuwanie"
  ]
  node [
    id 1496
    label "opowiadanie"
  ]
  node [
    id 1497
    label "anointing"
  ]
  node [
    id 1498
    label "sporz&#261;dzanie"
  ]
  node [
    id 1499
    label "skre&#347;lanie"
  ]
  node [
    id 1500
    label "uniewa&#380;nianie"
  ]
  node [
    id 1501
    label "skazany"
  ]
  node [
    id 1502
    label "zarz&#261;dzanie"
  ]
  node [
    id 1503
    label "rozporz&#261;dzanie"
  ]
  node [
    id 1504
    label "namaszczenie_chorych"
  ]
  node [
    id 1505
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1506
    label "disposal"
  ]
  node [
    id 1507
    label "rozdysponowywanie"
  ]
  node [
    id 1508
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1509
    label "bycie"
  ]
  node [
    id 1510
    label "fabrication"
  ]
  node [
    id 1511
    label "tentegowanie"
  ]
  node [
    id 1512
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1513
    label "porobienie"
  ]
  node [
    id 1514
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1515
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1516
    label "creation"
  ]
  node [
    id 1517
    label "bezproblemowy"
  ]
  node [
    id 1518
    label "activity"
  ]
  node [
    id 1519
    label "podkre&#347;lanie"
  ]
  node [
    id 1520
    label "radiation"
  ]
  node [
    id 1521
    label "demonstrowanie"
  ]
  node [
    id 1522
    label "napromieniowywanie"
  ]
  node [
    id 1523
    label "uwydatnianie_si&#281;"
  ]
  node [
    id 1524
    label "przeorientowanie"
  ]
  node [
    id 1525
    label "oznaczanie"
  ]
  node [
    id 1526
    label "orientation"
  ]
  node [
    id 1527
    label "strike"
  ]
  node [
    id 1528
    label "dominance"
  ]
  node [
    id 1529
    label "wygrywanie"
  ]
  node [
    id 1530
    label "lepszy"
  ]
  node [
    id 1531
    label "przesterowanie"
  ]
  node [
    id 1532
    label "steering"
  ]
  node [
    id 1533
    label "obs&#322;ugiwanie"
  ]
  node [
    id 1534
    label "control"
  ]
  node [
    id 1535
    label "rendition"
  ]
  node [
    id 1536
    label "powracanie"
  ]
  node [
    id 1537
    label "wydalanie"
  ]
  node [
    id 1538
    label "haftowanie"
  ]
  node [
    id 1539
    label "przekazywanie"
  ]
  node [
    id 1540
    label "vomit"
  ]
  node [
    id 1541
    label "przeznaczanie"
  ]
  node [
    id 1542
    label "ustawianie"
  ]
  node [
    id 1543
    label "znajdowanie_si&#281;"
  ]
  node [
    id 1544
    label "provision"
  ]
  node [
    id 1545
    label "supply"
  ]
  node [
    id 1546
    label "spe&#322;nianie"
  ]
  node [
    id 1547
    label "wzbudzanie"
  ]
  node [
    id 1548
    label "montowanie"
  ]
  node [
    id 1549
    label "rozwijanie"
  ]
  node [
    id 1550
    label "training"
  ]
  node [
    id 1551
    label "formation"
  ]
  node [
    id 1552
    label "causal_agent"
  ]
  node [
    id 1553
    label "nakierowanie_si&#281;"
  ]
  node [
    id 1554
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1555
    label "wysy&#322;anie"
  ]
  node [
    id 1556
    label "wyre&#380;yserowanie"
  ]
  node [
    id 1557
    label "re&#380;yserowanie"
  ]
  node [
    id 1558
    label "linia"
  ]
  node [
    id 1559
    label "curve"
  ]
  node [
    id 1560
    label "curvature"
  ]
  node [
    id 1561
    label "figura_geometryczna"
  ]
  node [
    id 1562
    label "poprowadzi&#263;"
  ]
  node [
    id 1563
    label "sterczenie"
  ]
  node [
    id 1564
    label "eksponowa&#263;"
  ]
  node [
    id 1565
    label "g&#243;rowa&#263;"
  ]
  node [
    id 1566
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 1567
    label "sterowa&#263;"
  ]
  node [
    id 1568
    label "kierowa&#263;"
  ]
  node [
    id 1569
    label "string"
  ]
  node [
    id 1570
    label "kre&#347;li&#263;"
  ]
  node [
    id 1571
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1572
    label "&#380;y&#263;"
  ]
  node [
    id 1573
    label "partner"
  ]
  node [
    id 1574
    label "ukierunkowywa&#263;"
  ]
  node [
    id 1575
    label "przesuwa&#263;"
  ]
  node [
    id 1576
    label "tworzy&#263;"
  ]
  node [
    id 1577
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1578
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 1579
    label "message"
  ]
  node [
    id 1580
    label "navigate"
  ]
  node [
    id 1581
    label "manipulate"
  ]
  node [
    id 1582
    label "akapit"
  ]
  node [
    id 1583
    label "artyku&#322;"
  ]
  node [
    id 1584
    label "zesp&#243;&#322;"
  ]
  node [
    id 1585
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1586
    label "udost&#281;pnianie"
  ]
  node [
    id 1587
    label "pra&#380;enie"
  ]
  node [
    id 1588
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 1589
    label "urz&#261;dzanie"
  ]
  node [
    id 1590
    label "p&#322;acenie"
  ]
  node [
    id 1591
    label "puszczanie_si&#281;"
  ]
  node [
    id 1592
    label "dodawanie"
  ]
  node [
    id 1593
    label "wyst&#281;powanie"
  ]
  node [
    id 1594
    label "bycie_w_posiadaniu"
  ]
  node [
    id 1595
    label "communication"
  ]
  node [
    id 1596
    label "&#322;adowanie"
  ]
  node [
    id 1597
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 1598
    label "nalewanie"
  ]
  node [
    id 1599
    label "powierzanie"
  ]
  node [
    id 1600
    label "zezwalanie"
  ]
  node [
    id 1601
    label "giving"
  ]
  node [
    id 1602
    label "obiecywanie"
  ]
  node [
    id 1603
    label "odst&#281;powanie"
  ]
  node [
    id 1604
    label "dostarczanie"
  ]
  node [
    id 1605
    label "wymienianie_si&#281;"
  ]
  node [
    id 1606
    label "emission"
  ]
  node [
    id 1607
    label "administration"
  ]
  node [
    id 1608
    label "umo&#380;liwianie"
  ]
  node [
    id 1609
    label "pasowanie"
  ]
  node [
    id 1610
    label "otwarcie"
  ]
  node [
    id 1611
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 1612
    label "playing"
  ]
  node [
    id 1613
    label "dzianie_si&#281;"
  ]
  node [
    id 1614
    label "rozgrywanie"
  ]
  node [
    id 1615
    label "pretense"
  ]
  node [
    id 1616
    label "dogranie"
  ]
  node [
    id 1617
    label "uderzenie"
  ]
  node [
    id 1618
    label "zwalczenie"
  ]
  node [
    id 1619
    label "lewa"
  ]
  node [
    id 1620
    label "instrumentalizacja"
  ]
  node [
    id 1621
    label "migotanie"
  ]
  node [
    id 1622
    label "wydawanie"
  ]
  node [
    id 1623
    label "ust&#281;powanie"
  ]
  node [
    id 1624
    label "odegranie_si&#281;"
  ]
  node [
    id 1625
    label "grywanie"
  ]
  node [
    id 1626
    label "dogrywanie"
  ]
  node [
    id 1627
    label "wybijanie"
  ]
  node [
    id 1628
    label "wykonywanie"
  ]
  node [
    id 1629
    label "pogranie"
  ]
  node [
    id 1630
    label "brzmienie"
  ]
  node [
    id 1631
    label "na&#347;ladowanie"
  ]
  node [
    id 1632
    label "przygrywanie"
  ]
  node [
    id 1633
    label "nagranie_si&#281;"
  ]
  node [
    id 1634
    label "wyr&#243;wnywanie"
  ]
  node [
    id 1635
    label "rozegranie_si&#281;"
  ]
  node [
    id 1636
    label "odgrywanie_si&#281;"
  ]
  node [
    id 1637
    label "&#347;ciganie"
  ]
  node [
    id 1638
    label "wyr&#243;wnanie"
  ]
  node [
    id 1639
    label "szczekanie"
  ]
  node [
    id 1640
    label "gra_w_karty"
  ]
  node [
    id 1641
    label "instrument_muzyczny"
  ]
  node [
    id 1642
    label "glitter"
  ]
  node [
    id 1643
    label "igranie"
  ]
  node [
    id 1644
    label "wybicie"
  ]
  node [
    id 1645
    label "mienienie_si&#281;"
  ]
  node [
    id 1646
    label "prezentowanie"
  ]
  node [
    id 1647
    label "staranie_si&#281;"
  ]
  node [
    id 1648
    label "wytyczenie"
  ]
  node [
    id 1649
    label "nakre&#347;lenie"
  ]
  node [
    id 1650
    label "proverb"
  ]
  node [
    id 1651
    label "guidance"
  ]
  node [
    id 1652
    label "nata&#324;czenie_si&#281;"
  ]
  node [
    id 1653
    label "dancing"
  ]
  node [
    id 1654
    label "poruszanie_si&#281;"
  ]
  node [
    id 1655
    label "chwianie_si&#281;"
  ]
  node [
    id 1656
    label "rozta&#324;czenie_si&#281;"
  ]
  node [
    id 1657
    label "przeta&#324;czenie"
  ]
  node [
    id 1658
    label "gibanie"
  ]
  node [
    id 1659
    label "pota&#324;czenie"
  ]
  node [
    id 1660
    label "pokazywanie"
  ]
  node [
    id 1661
    label "zaczynanie"
  ]
  node [
    id 1662
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1663
    label "initiation"
  ]
  node [
    id 1664
    label "umieszczanie"
  ]
  node [
    id 1665
    label "zak&#322;&#243;canie"
  ]
  node [
    id 1666
    label "retraction"
  ]
  node [
    id 1667
    label "zapoznawanie"
  ]
  node [
    id 1668
    label "wpisywanie"
  ]
  node [
    id 1669
    label "przewietrzanie"
  ]
  node [
    id 1670
    label "trigger"
  ]
  node [
    id 1671
    label "mental_hospital"
  ]
  node [
    id 1672
    label "rynek"
  ]
  node [
    id 1673
    label "wchodzenie"
  ]
  node [
    id 1674
    label "sp&#281;dzenie"
  ]
  node [
    id 1675
    label "wzbudzenie"
  ]
  node [
    id 1676
    label "zainstalowanie"
  ]
  node [
    id 1677
    label "spe&#322;nienie"
  ]
  node [
    id 1678
    label "znalezienie_si&#281;"
  ]
  node [
    id 1679
    label "introduction"
  ]
  node [
    id 1680
    label "pos&#322;anie"
  ]
  node [
    id 1681
    label "adduction"
  ]
  node [
    id 1682
    label "sk&#322;anianie"
  ]
  node [
    id 1683
    label "przypominanie"
  ]
  node [
    id 1684
    label "kojarzenie_si&#281;"
  ]
  node [
    id 1685
    label "pokazanie"
  ]
  node [
    id 1686
    label "umo&#380;liwienie"
  ]
  node [
    id 1687
    label "wej&#347;cie"
  ]
  node [
    id 1688
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 1689
    label "nuklearyzacja"
  ]
  node [
    id 1690
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1691
    label "zacz&#281;cie"
  ]
  node [
    id 1692
    label "zapoznanie"
  ]
  node [
    id 1693
    label "podstawy"
  ]
  node [
    id 1694
    label "entrance"
  ]
  node [
    id 1695
    label "wpisanie"
  ]
  node [
    id 1696
    label "deduction"
  ]
  node [
    id 1697
    label "issue"
  ]
  node [
    id 1698
    label "przewietrzenie"
  ]
  node [
    id 1699
    label "przed&#322;u&#380;anie"
  ]
  node [
    id 1700
    label "przemieszczanie"
  ]
  node [
    id 1701
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1702
    label "rozci&#261;ganie"
  ]
  node [
    id 1703
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 1704
    label "wymawianie"
  ]
  node [
    id 1705
    label "j&#261;kanie"
  ]
  node [
    id 1706
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1707
    label "przymocowywanie"
  ]
  node [
    id 1708
    label "pull"
  ]
  node [
    id 1709
    label "przesuwanie"
  ]
  node [
    id 1710
    label "zaci&#261;ganie"
  ]
  node [
    id 1711
    label "przetykanie"
  ]
  node [
    id 1712
    label "podzielenie"
  ]
  node [
    id 1713
    label "poprzecinanie"
  ]
  node [
    id 1714
    label "przerwanie"
  ]
  node [
    id 1715
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1716
    label "w&#281;ze&#322;"
  ]
  node [
    id 1717
    label "zranienie"
  ]
  node [
    id 1718
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1719
    label "cut"
  ]
  node [
    id 1720
    label "snub"
  ]
  node [
    id 1721
    label "carving"
  ]
  node [
    id 1722
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1723
    label "time"
  ]
  node [
    id 1724
    label "kaleczenie"
  ]
  node [
    id 1725
    label "przerywanie"
  ]
  node [
    id 1726
    label "dzielenie"
  ]
  node [
    id 1727
    label "intersection"
  ]
  node [
    id 1728
    label "film_editing"
  ]
  node [
    id 1729
    label "utrzymywanie"
  ]
  node [
    id 1730
    label "wypuszczanie"
  ]
  node [
    id 1731
    label "noszenie"
  ]
  node [
    id 1732
    label "przetrzymanie"
  ]
  node [
    id 1733
    label "przetrzymywanie"
  ]
  node [
    id 1734
    label "hodowanie"
  ]
  node [
    id 1735
    label "wypuszczenie"
  ]
  node [
    id 1736
    label "niesienie"
  ]
  node [
    id 1737
    label "utrzymanie"
  ]
  node [
    id 1738
    label "zmuszanie"
  ]
  node [
    id 1739
    label "podtrzymywanie"
  ]
  node [
    id 1740
    label "sprawowanie"
  ]
  node [
    id 1741
    label "poise"
  ]
  node [
    id 1742
    label "uniemo&#380;liwianie"
  ]
  node [
    id 1743
    label "dzier&#380;enie"
  ]
  node [
    id 1744
    label "zachowywanie"
  ]
  node [
    id 1745
    label "detention"
  ]
  node [
    id 1746
    label "potrzymanie"
  ]
  node [
    id 1747
    label "retention"
  ]
  node [
    id 1748
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 1749
    label "pole"
  ]
  node [
    id 1750
    label "dom_rodzinny"
  ]
  node [
    id 1751
    label "stodo&#322;a"
  ]
  node [
    id 1752
    label "inwentarz"
  ]
  node [
    id 1753
    label "gospodarowanie"
  ]
  node [
    id 1754
    label "gospodarowa&#263;"
  ]
  node [
    id 1755
    label "spichlerz"
  ]
  node [
    id 1756
    label "obora"
  ]
  node [
    id 1757
    label "dom"
  ]
  node [
    id 1758
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1759
    label "cz&#261;steczka"
  ]
  node [
    id 1760
    label "specgrupa"
  ]
  node [
    id 1761
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1762
    label "harcerze_starsi"
  ]
  node [
    id 1763
    label "oddzia&#322;"
  ]
  node [
    id 1764
    label "category"
  ]
  node [
    id 1765
    label "liga"
  ]
  node [
    id 1766
    label "&#346;wietliki"
  ]
  node [
    id 1767
    label "formacja_geologiczna"
  ]
  node [
    id 1768
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1769
    label "Terranie"
  ]
  node [
    id 1770
    label "odm&#322;adzanie"
  ]
  node [
    id 1771
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1772
    label "Entuzjastki"
  ]
  node [
    id 1773
    label "possession"
  ]
  node [
    id 1774
    label "rodowo&#347;&#263;"
  ]
  node [
    id 1775
    label "dobra"
  ]
  node [
    id 1776
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1777
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1778
    label "patent"
  ]
  node [
    id 1779
    label "przej&#347;&#263;"
  ]
  node [
    id 1780
    label "przej&#347;cie"
  ]
  node [
    id 1781
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 1782
    label "stable"
  ]
  node [
    id 1783
    label "budynek_inwentarski"
  ]
  node [
    id 1784
    label "krowiarnia"
  ]
  node [
    id 1785
    label "budynek_gospodarczy"
  ]
  node [
    id 1786
    label "s&#261;siek"
  ]
  node [
    id 1787
    label "wr&#243;tnia"
  ]
  node [
    id 1788
    label "building"
  ]
  node [
    id 1789
    label "&#347;pichlerz"
  ]
  node [
    id 1790
    label "szafarnia"
  ]
  node [
    id 1791
    label "magazyn"
  ]
  node [
    id 1792
    label "region"
  ]
  node [
    id 1793
    label "zbiornik"
  ]
  node [
    id 1794
    label "&#347;pichrz"
  ]
  node [
    id 1795
    label "radlina"
  ]
  node [
    id 1796
    label "uprawienie"
  ]
  node [
    id 1797
    label "irygowanie"
  ]
  node [
    id 1798
    label "socjologia"
  ]
  node [
    id 1799
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1800
    label "square"
  ]
  node [
    id 1801
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 1802
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1803
    label "zmienna"
  ]
  node [
    id 1804
    label "dw&#243;r"
  ]
  node [
    id 1805
    label "irygowa&#263;"
  ]
  node [
    id 1806
    label "p&#322;osa"
  ]
  node [
    id 1807
    label "baza_danych"
  ]
  node [
    id 1808
    label "t&#322;o"
  ]
  node [
    id 1809
    label "room"
  ]
  node [
    id 1810
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 1811
    label "sk&#322;ad"
  ]
  node [
    id 1812
    label "uprawi&#263;"
  ]
  node [
    id 1813
    label "okazja"
  ]
  node [
    id 1814
    label "zagon"
  ]
  node [
    id 1815
    label "stock"
  ]
  node [
    id 1816
    label "spis"
  ]
  node [
    id 1817
    label "ekonomia"
  ]
  node [
    id 1818
    label "pracowa&#263;"
  ]
  node [
    id 1819
    label "manage"
  ]
  node [
    id 1820
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1821
    label "cope"
  ]
  node [
    id 1822
    label "trzyma&#263;"
  ]
  node [
    id 1823
    label "dysponowa&#263;"
  ]
  node [
    id 1824
    label "pracowanie"
  ]
  node [
    id 1825
    label "housework"
  ]
  node [
    id 1826
    label "substancja_mieszkaniowa"
  ]
  node [
    id 1827
    label "rodzina"
  ]
  node [
    id 1828
    label "siedziba"
  ]
  node [
    id 1829
    label "garderoba"
  ]
  node [
    id 1830
    label "fratria"
  ]
  node [
    id 1831
    label "stead"
  ]
  node [
    id 1832
    label "instytucja"
  ]
  node [
    id 1833
    label "wiecha"
  ]
  node [
    id 1834
    label "rolniczo"
  ]
  node [
    id 1835
    label "rolniczy"
  ]
  node [
    id 1836
    label "przyrodniczo"
  ]
  node [
    id 1837
    label "specjalnie"
  ]
  node [
    id 1838
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1839
    label "niedorozw&#243;j"
  ]
  node [
    id 1840
    label "nienormalny"
  ]
  node [
    id 1841
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1842
    label "umy&#347;lnie"
  ]
  node [
    id 1843
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1844
    label "nieetatowy"
  ]
  node [
    id 1845
    label "szczeg&#243;lny"
  ]
  node [
    id 1846
    label "intencjonalny"
  ]
  node [
    id 1847
    label "rozlegle"
  ]
  node [
    id 1848
    label "szeroki"
  ]
  node [
    id 1849
    label "rozci&#261;gle"
  ]
  node [
    id 1850
    label "across_the_board"
  ]
  node [
    id 1851
    label "rozdeptywanie"
  ]
  node [
    id 1852
    label "rozdeptanie"
  ]
  node [
    id 1853
    label "lu&#378;no"
  ]
  node [
    id 1854
    label "szeroko"
  ]
  node [
    id 1855
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 1856
    label "Pampa"
  ]
  node [
    id 1857
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 1858
    label "pampa"
  ]
  node [
    id 1859
    label "Rzym_Zachodni"
  ]
  node [
    id 1860
    label "Rzym_Wschodni"
  ]
  node [
    id 1861
    label "whole"
  ]
  node [
    id 1862
    label "czas"
  ]
  node [
    id 1863
    label "okres_czasu"
  ]
  node [
    id 1864
    label "charakter"
  ]
  node [
    id 1865
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1866
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1867
    label "przywidzenie"
  ]
  node [
    id 1868
    label "boski"
  ]
  node [
    id 1869
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1870
    label "presence"
  ]
  node [
    id 1871
    label "effort"
  ]
  node [
    id 1872
    label "podejmowanie"
  ]
  node [
    id 1873
    label "essay"
  ]
  node [
    id 1874
    label "spotkanie"
  ]
  node [
    id 1875
    label "night"
  ]
  node [
    id 1876
    label "przyj&#281;cie"
  ]
  node [
    id 1877
    label "vesper"
  ]
  node [
    id 1878
    label "trudzi&#263;"
  ]
  node [
    id 1879
    label "trudzenie"
  ]
  node [
    id 1880
    label "przytoczenie_si&#281;"
  ]
  node [
    id 1881
    label "wlec_si&#281;"
  ]
  node [
    id 1882
    label "przytaczanie_si&#281;"
  ]
  node [
    id 1883
    label "wleczenie_si&#281;"
  ]
  node [
    id 1884
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 1885
    label "sunlight"
  ]
  node [
    id 1886
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1887
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 1888
    label "pogoda"
  ]
  node [
    id 1889
    label "kochanie"
  ]
  node [
    id 1890
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1891
    label "pozyskiwa&#263;"
  ]
  node [
    id 1892
    label "doznawa&#263;"
  ]
  node [
    id 1893
    label "detect"
  ]
  node [
    id 1894
    label "znachodzi&#263;"
  ]
  node [
    id 1895
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1896
    label "os&#261;dza&#263;"
  ]
  node [
    id 1897
    label "odzyskiwa&#263;"
  ]
  node [
    id 1898
    label "wykrywa&#263;"
  ]
  node [
    id 1899
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 1900
    label "obra&#380;a&#263;"
  ]
  node [
    id 1901
    label "mistreat"
  ]
  node [
    id 1902
    label "debunk"
  ]
  node [
    id 1903
    label "dostrzega&#263;"
  ]
  node [
    id 1904
    label "odkrywa&#263;"
  ]
  node [
    id 1905
    label "motywowa&#263;"
  ]
  node [
    id 1906
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1907
    label "tease"
  ]
  node [
    id 1908
    label "take"
  ]
  node [
    id 1909
    label "hurt"
  ]
  node [
    id 1910
    label "sum_up"
  ]
  node [
    id 1911
    label "recur"
  ]
  node [
    id 1912
    label "przychodzi&#263;"
  ]
  node [
    id 1913
    label "hold"
  ]
  node [
    id 1914
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 1915
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1916
    label "obrona_strefowa"
  ]
  node [
    id 1917
    label "&#347;lad"
  ]
  node [
    id 1918
    label "kwota"
  ]
  node [
    id 1919
    label "lobbysta"
  ]
  node [
    id 1920
    label "doch&#243;d_narodowy"
  ]
  node [
    id 1921
    label "pieni&#261;dze"
  ]
  node [
    id 1922
    label "wynie&#347;&#263;"
  ]
  node [
    id 1923
    label "limit"
  ]
  node [
    id 1924
    label "wynosi&#263;"
  ]
  node [
    id 1925
    label "odrobina"
  ]
  node [
    id 1926
    label "sznurowanie"
  ]
  node [
    id 1927
    label "odcisk"
  ]
  node [
    id 1928
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1929
    label "sznurowa&#263;"
  ]
  node [
    id 1930
    label "attribute"
  ]
  node [
    id 1931
    label "skutek"
  ]
  node [
    id 1932
    label "grupa_nacisku"
  ]
  node [
    id 1933
    label "styl"
  ]
  node [
    id 1934
    label "atmosfera"
  ]
  node [
    id 1935
    label "pisa&#263;"
  ]
  node [
    id 1936
    label "spos&#243;b"
  ]
  node [
    id 1937
    label "zachowanie"
  ]
  node [
    id 1938
    label "dyscyplina_sportowa"
  ]
  node [
    id 1939
    label "natural_language"
  ]
  node [
    id 1940
    label "line"
  ]
  node [
    id 1941
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1942
    label "handle"
  ]
  node [
    id 1943
    label "kanon"
  ]
  node [
    id 1944
    label "reakcja"
  ]
  node [
    id 1945
    label "napisa&#263;"
  ]
  node [
    id 1946
    label "stylik"
  ]
  node [
    id 1947
    label "stroke"
  ]
  node [
    id 1948
    label "trzonek"
  ]
  node [
    id 1949
    label "narz&#281;dzie"
  ]
  node [
    id 1950
    label "behawior"
  ]
  node [
    id 1951
    label "group"
  ]
  node [
    id 1952
    label "The_Beatles"
  ]
  node [
    id 1953
    label "Depeche_Mode"
  ]
  node [
    id 1954
    label "zespolik"
  ]
  node [
    id 1955
    label "batch"
  ]
  node [
    id 1956
    label "zabudowania"
  ]
  node [
    id 1957
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 1958
    label "powietrznia"
  ]
  node [
    id 1959
    label "egzosfera"
  ]
  node [
    id 1960
    label "mezopauza"
  ]
  node [
    id 1961
    label "mezosfera"
  ]
  node [
    id 1962
    label "planeta"
  ]
  node [
    id 1963
    label "pow&#322;oka"
  ]
  node [
    id 1964
    label "termosfera"
  ]
  node [
    id 1965
    label "stratosfera"
  ]
  node [
    id 1966
    label "kwas"
  ]
  node [
    id 1967
    label "atmosphere"
  ]
  node [
    id 1968
    label "homosfera"
  ]
  node [
    id 1969
    label "metasfera"
  ]
  node [
    id 1970
    label "tropopauza"
  ]
  node [
    id 1971
    label "heterosfera"
  ]
  node [
    id 1972
    label "obiekt_naturalny"
  ]
  node [
    id 1973
    label "atmosferyki"
  ]
  node [
    id 1974
    label "jonosfera"
  ]
  node [
    id 1975
    label "troposfera"
  ]
  node [
    id 1976
    label "ostro&#380;ny"
  ]
  node [
    id 1977
    label "umiarkowanie"
  ]
  node [
    id 1978
    label "rozwa&#380;ny"
  ]
  node [
    id 1979
    label "&#347;rednio"
  ]
  node [
    id 1980
    label "rozwa&#380;nie"
  ]
  node [
    id 1981
    label "rozs&#261;dny"
  ]
  node [
    id 1982
    label "ostro&#380;nie"
  ]
  node [
    id 1983
    label "&#347;redni"
  ]
  node [
    id 1984
    label "ma&#322;o"
  ]
  node [
    id 1985
    label "continence"
  ]
  node [
    id 1986
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1987
    label "orientacyjnie"
  ]
  node [
    id 1988
    label "bezbarwnie"
  ]
  node [
    id 1989
    label "tak_sobie"
  ]
  node [
    id 1990
    label "grzanie"
  ]
  node [
    id 1991
    label "ocieplenie"
  ]
  node [
    id 1992
    label "ciep&#322;o"
  ]
  node [
    id 1993
    label "zagrzanie"
  ]
  node [
    id 1994
    label "ocieplanie"
  ]
  node [
    id 1995
    label "przyjemny"
  ]
  node [
    id 1996
    label "ocieplenie_si&#281;"
  ]
  node [
    id 1997
    label "ocieplanie_si&#281;"
  ]
  node [
    id 1998
    label "korzystnie"
  ]
  node [
    id 1999
    label "wybranek"
  ]
  node [
    id 2000
    label "sk&#322;onny"
  ]
  node [
    id 2001
    label "kochanek"
  ]
  node [
    id 2002
    label "mi&#322;o"
  ]
  node [
    id 2003
    label "dyplomata"
  ]
  node [
    id 2004
    label "umi&#322;owany"
  ]
  node [
    id 2005
    label "przyjemnie"
  ]
  node [
    id 2006
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 2007
    label "heat"
  ]
  node [
    id 2008
    label "emocja"
  ]
  node [
    id 2009
    label "geotermia"
  ]
  node [
    id 2010
    label "emisyjno&#347;&#263;"
  ]
  node [
    id 2011
    label "temperatura"
  ]
  node [
    id 2012
    label "wydzielanie"
  ]
  node [
    id 2013
    label "roztapianie"
  ]
  node [
    id 2014
    label "zat&#322;uczenie"
  ]
  node [
    id 2015
    label "heating"
  ]
  node [
    id 2016
    label "odwadnianie"
  ]
  node [
    id 2017
    label "zw&#281;glanie"
  ]
  node [
    id 2018
    label "rozgrzewanie_si&#281;"
  ]
  node [
    id 2019
    label "wypra&#380;anie"
  ]
  node [
    id 2020
    label "spiekanie"
  ]
  node [
    id 2021
    label "fracture"
  ]
  node [
    id 2022
    label "odpuszczanie"
  ]
  node [
    id 2023
    label "podnoszenie"
  ]
  node [
    id 2024
    label "zw&#281;glenie"
  ]
  node [
    id 2025
    label "pobudzanie"
  ]
  node [
    id 2026
    label "strzelanie"
  ]
  node [
    id 2027
    label "bicie"
  ]
  node [
    id 2028
    label "spieczenie"
  ]
  node [
    id 2029
    label "gnanie"
  ]
  node [
    id 2030
    label "wypra&#380;enie"
  ]
  node [
    id 2031
    label "picie"
  ]
  node [
    id 2032
    label "narkotyzowanie_si&#281;"
  ]
  node [
    id 2033
    label "gassing"
  ]
  node [
    id 2034
    label "podniesienie"
  ]
  node [
    id 2035
    label "odpuszczenie"
  ]
  node [
    id 2036
    label "rozgrzanie_si&#281;"
  ]
  node [
    id 2037
    label "roztopienie"
  ]
  node [
    id 2038
    label "zach&#281;cenie"
  ]
  node [
    id 2039
    label "odwodnienie"
  ]
  node [
    id 2040
    label "poprawianie"
  ]
  node [
    id 2041
    label "poprawienie"
  ]
  node [
    id 2042
    label "podszycie"
  ]
  node [
    id 2043
    label "zabezpieczenie"
  ]
  node [
    id 2044
    label "poprawa"
  ]
  node [
    id 2045
    label "morsko"
  ]
  node [
    id 2046
    label "przypominaj&#261;cy"
  ]
  node [
    id 2047
    label "zielony"
  ]
  node [
    id 2048
    label "typowy"
  ]
  node [
    id 2049
    label "nadmorski"
  ]
  node [
    id 2050
    label "wodny"
  ]
  node [
    id 2051
    label "s&#322;ony"
  ]
  node [
    id 2052
    label "niebieski"
  ]
  node [
    id 2053
    label "wyg&#243;rowany"
  ]
  node [
    id 2054
    label "&#347;wie&#380;y"
  ]
  node [
    id 2055
    label "wysoki"
  ]
  node [
    id 2056
    label "mineralny"
  ]
  node [
    id 2057
    label "dotkliwy"
  ]
  node [
    id 2058
    label "s&#322;ono"
  ]
  node [
    id 2059
    label "zdzieranie"
  ]
  node [
    id 2060
    label "zdarcie"
  ]
  node [
    id 2061
    label "zbli&#380;ony"
  ]
  node [
    id 2062
    label "typowo"
  ]
  node [
    id 2063
    label "zwyk&#322;y"
  ]
  node [
    id 2064
    label "zwyczajny"
  ]
  node [
    id 2065
    label "cz&#281;sty"
  ]
  node [
    id 2066
    label "niedojrza&#322;y"
  ]
  node [
    id 2067
    label "Saba"
  ]
  node [
    id 2068
    label "zwolennik"
  ]
  node [
    id 2069
    label "Sint_Eustatius"
  ]
  node [
    id 2070
    label "polityk"
  ]
  node [
    id 2071
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 2072
    label "zielono"
  ]
  node [
    id 2073
    label "ch&#322;odny"
  ]
  node [
    id 2074
    label "zieloni"
  ]
  node [
    id 2075
    label "socjalista"
  ]
  node [
    id 2076
    label "zazielenianie"
  ]
  node [
    id 2077
    label "zielenienie"
  ]
  node [
    id 2078
    label "dzia&#322;acz"
  ]
  node [
    id 2079
    label "Bonaire"
  ]
  node [
    id 2080
    label "pokryty"
  ]
  node [
    id 2081
    label "blady"
  ]
  node [
    id 2082
    label "majny"
  ]
  node [
    id 2083
    label "&#380;ywy"
  ]
  node [
    id 2084
    label "zazielenienie"
  ]
  node [
    id 2085
    label "zzielenienie"
  ]
  node [
    id 2086
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 2087
    label "siny"
  ]
  node [
    id 2088
    label "niebiesko"
  ]
  node [
    id 2089
    label "niebieszczenie"
  ]
  node [
    id 2090
    label "s&#322;oneczny"
  ]
  node [
    id 2091
    label "gor&#261;cy"
  ]
  node [
    id 2092
    label "po&#322;udniowo"
  ]
  node [
    id 2093
    label "charakterystycznie"
  ]
  node [
    id 2094
    label "na_gor&#261;co"
  ]
  node [
    id 2095
    label "rozpalenie_si&#281;"
  ]
  node [
    id 2096
    label "&#380;arki"
  ]
  node [
    id 2097
    label "sensacyjny"
  ]
  node [
    id 2098
    label "stresogenny"
  ]
  node [
    id 2099
    label "seksowny"
  ]
  node [
    id 2100
    label "rozpalanie_si&#281;"
  ]
  node [
    id 2101
    label "g&#322;&#281;boki"
  ]
  node [
    id 2102
    label "serdeczny"
  ]
  node [
    id 2103
    label "zdecydowany"
  ]
  node [
    id 2104
    label "gor&#261;co"
  ]
  node [
    id 2105
    label "s&#322;onecznie"
  ]
  node [
    id 2106
    label "pogodny"
  ]
  node [
    id 2107
    label "letni"
  ]
  node [
    id 2108
    label "weso&#322;y"
  ]
  node [
    id 2109
    label "bezchmurny"
  ]
  node [
    id 2110
    label "jasny"
  ]
  node [
    id 2111
    label "bezdeszczowy"
  ]
  node [
    id 2112
    label "fotowoltaiczny"
  ]
  node [
    id 2113
    label "materia"
  ]
  node [
    id 2114
    label "&#347;rodowisko"
  ]
  node [
    id 2115
    label "szkodnik"
  ]
  node [
    id 2116
    label "gangsterski"
  ]
  node [
    id 2117
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 2118
    label "underworld"
  ]
  node [
    id 2119
    label "szambo"
  ]
  node [
    id 2120
    label "component"
  ]
  node [
    id 2121
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 2122
    label "r&#243;&#380;niczka"
  ]
  node [
    id 2123
    label "aspo&#322;eczny"
  ]
  node [
    id 2124
    label "komora"
  ]
  node [
    id 2125
    label "wyrz&#261;dzenie"
  ]
  node [
    id 2126
    label "kom&#243;rka"
  ]
  node [
    id 2127
    label "impulsator"
  ]
  node [
    id 2128
    label "furnishing"
  ]
  node [
    id 2129
    label "sprz&#281;t"
  ]
  node [
    id 2130
    label "aparatura"
  ]
  node [
    id 2131
    label "ig&#322;a"
  ]
  node [
    id 2132
    label "wirnik"
  ]
  node [
    id 2133
    label "zablokowanie"
  ]
  node [
    id 2134
    label "j&#281;zyk"
  ]
  node [
    id 2135
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 2136
    label "system_energetyczny"
  ]
  node [
    id 2137
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 2138
    label "zagospodarowanie"
  ]
  node [
    id 2139
    label "mechanizm"
  ]
  node [
    id 2140
    label "w&#281;giersko"
  ]
  node [
    id 2141
    label "europejski"
  ]
  node [
    id 2142
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 2143
    label "czardasz"
  ]
  node [
    id 2144
    label "Hungarian"
  ]
  node [
    id 2145
    label "nale&#347;nik_Gundel"
  ]
  node [
    id 2146
    label "toka&#324;"
  ]
  node [
    id 2147
    label "salami"
  ]
  node [
    id 2148
    label "po_w&#281;giersku"
  ]
  node [
    id 2149
    label "turanizm"
  ]
  node [
    id 2150
    label "j&#281;zyk_ugryjski"
  ]
  node [
    id 2151
    label "europejsko"
  ]
  node [
    id 2152
    label "charakterystyczny"
  ]
  node [
    id 2153
    label "po_europejsku"
  ]
  node [
    id 2154
    label "European"
  ]
  node [
    id 2155
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 2156
    label "langosz"
  ]
  node [
    id 2157
    label "kod"
  ]
  node [
    id 2158
    label "pype&#263;"
  ]
  node [
    id 2159
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 2160
    label "gramatyka"
  ]
  node [
    id 2161
    label "language"
  ]
  node [
    id 2162
    label "fonetyka"
  ]
  node [
    id 2163
    label "t&#322;umaczenie"
  ]
  node [
    id 2164
    label "artykulator"
  ]
  node [
    id 2165
    label "rozumienie"
  ]
  node [
    id 2166
    label "jama_ustna"
  ]
  node [
    id 2167
    label "organ"
  ]
  node [
    id 2168
    label "ssanie"
  ]
  node [
    id 2169
    label "lizanie"
  ]
  node [
    id 2170
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 2171
    label "liza&#263;"
  ]
  node [
    id 2172
    label "makroglosja"
  ]
  node [
    id 2173
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 2174
    label "m&#243;wienie"
  ]
  node [
    id 2175
    label "s&#322;ownictwo"
  ]
  node [
    id 2176
    label "konsonantyzm"
  ]
  node [
    id 2177
    label "ssa&#263;"
  ]
  node [
    id 2178
    label "wokalizm"
  ]
  node [
    id 2179
    label "formalizowanie"
  ]
  node [
    id 2180
    label "jeniec"
  ]
  node [
    id 2181
    label "m&#243;wi&#263;"
  ]
  node [
    id 2182
    label "kawa&#322;ek"
  ]
  node [
    id 2183
    label "po_koroniarsku"
  ]
  node [
    id 2184
    label "rozumie&#263;"
  ]
  node [
    id 2185
    label "przet&#322;umaczenie"
  ]
  node [
    id 2186
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 2187
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 2188
    label "but"
  ]
  node [
    id 2189
    label "pismo"
  ]
  node [
    id 2190
    label "formalizowa&#263;"
  ]
  node [
    id 2191
    label "koncepcja"
  ]
  node [
    id 2192
    label "nacjonalizm"
  ]
  node [
    id 2193
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 2194
    label "kie&#322;basa"
  ]
  node [
    id 2195
    label "rago&#251;t"
  ]
  node [
    id 2196
    label "taniec"
  ]
  node [
    id 2197
    label "taniec_ludowy"
  ]
  node [
    id 2198
    label "czu&#263;"
  ]
  node [
    id 2199
    label "chowa&#263;"
  ]
  node [
    id 2200
    label "uczuwa&#263;"
  ]
  node [
    id 2201
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 2202
    label "smell"
  ]
  node [
    id 2203
    label "anticipate"
  ]
  node [
    id 2204
    label "postrzega&#263;"
  ]
  node [
    id 2205
    label "spirit"
  ]
  node [
    id 2206
    label "ukrywa&#263;"
  ]
  node [
    id 2207
    label "hodowa&#263;"
  ]
  node [
    id 2208
    label "hide"
  ]
  node [
    id 2209
    label "meliniarz"
  ]
  node [
    id 2210
    label "umieszcza&#263;"
  ]
  node [
    id 2211
    label "przetrzymywa&#263;"
  ]
  node [
    id 2212
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 2213
    label "znosi&#263;"
  ]
  node [
    id 2214
    label "produkt"
  ]
  node [
    id 2215
    label "k&#322;os"
  ]
  node [
    id 2216
    label "ziarno"
  ]
  node [
    id 2217
    label "gluten"
  ]
  node [
    id 2218
    label "mlewnik"
  ]
  node [
    id 2219
    label "wypotnik"
  ]
  node [
    id 2220
    label "pochewka"
  ]
  node [
    id 2221
    label "strzyc"
  ]
  node [
    id 2222
    label "wegetacja"
  ]
  node [
    id 2223
    label "zadziorek"
  ]
  node [
    id 2224
    label "flawonoid"
  ]
  node [
    id 2225
    label "fitotron"
  ]
  node [
    id 2226
    label "w&#322;&#243;kno"
  ]
  node [
    id 2227
    label "zawi&#261;zek"
  ]
  node [
    id 2228
    label "pora&#380;a&#263;"
  ]
  node [
    id 2229
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 2230
    label "zbiorowisko"
  ]
  node [
    id 2231
    label "do&#322;owa&#263;"
  ]
  node [
    id 2232
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 2233
    label "hodowla"
  ]
  node [
    id 2234
    label "wegetowa&#263;"
  ]
  node [
    id 2235
    label "bulwka"
  ]
  node [
    id 2236
    label "sok"
  ]
  node [
    id 2237
    label "epiderma"
  ]
  node [
    id 2238
    label "g&#322;uszy&#263;"
  ]
  node [
    id 2239
    label "g&#322;uszenie"
  ]
  node [
    id 2240
    label "owoc"
  ]
  node [
    id 2241
    label "strzy&#380;enie"
  ]
  node [
    id 2242
    label "p&#281;d"
  ]
  node [
    id 2243
    label "wegetowanie"
  ]
  node [
    id 2244
    label "fotoautotrof"
  ]
  node [
    id 2245
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 2246
    label "gumoza"
  ]
  node [
    id 2247
    label "wyro&#347;le"
  ]
  node [
    id 2248
    label "fitocenoza"
  ]
  node [
    id 2249
    label "odn&#243;&#380;ka"
  ]
  node [
    id 2250
    label "do&#322;owanie"
  ]
  node [
    id 2251
    label "nieuleczalnie_chory"
  ]
  node [
    id 2252
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 2253
    label "substancja"
  ]
  node [
    id 2254
    label "production"
  ]
  node [
    id 2255
    label "zalewnia"
  ]
  node [
    id 2256
    label "ziarko"
  ]
  node [
    id 2257
    label "faktura"
  ]
  node [
    id 2258
    label "nasiono"
  ]
  node [
    id 2259
    label "obraz"
  ]
  node [
    id 2260
    label "bry&#322;ka"
  ]
  node [
    id 2261
    label "nie&#322;upka"
  ]
  node [
    id 2262
    label "dekortykacja"
  ]
  node [
    id 2263
    label "grain"
  ]
  node [
    id 2264
    label "k&#322;osek"
  ]
  node [
    id 2265
    label "plewa"
  ]
  node [
    id 2266
    label "kita"
  ]
  node [
    id 2267
    label "warkocz"
  ]
  node [
    id 2268
    label "k&#322;osie"
  ]
  node [
    id 2269
    label "kwiatostan"
  ]
  node [
    id 2270
    label "mieszanina"
  ]
  node [
    id 2271
    label "seitan"
  ]
  node [
    id 2272
    label "s&#322;oma"
  ]
  node [
    id 2273
    label "siano"
  ]
  node [
    id 2274
    label "maszyna_rolnicza"
  ]
  node [
    id 2275
    label "kruszarka"
  ]
  node [
    id 2276
    label "trawa"
  ]
  node [
    id 2277
    label "pszen&#380;yto"
  ]
  node [
    id 2278
    label "wiechlinowate"
  ]
  node [
    id 2279
    label "rastaman"
  ]
  node [
    id 2280
    label "koleoryza"
  ]
  node [
    id 2281
    label "narkotyk_mi&#281;kki"
  ]
  node [
    id 2282
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 2283
    label "&#380;yto"
  ]
  node [
    id 2284
    label "s&#322;odki"
  ]
  node [
    id 2285
    label "uroczy"
  ]
  node [
    id 2286
    label "s&#322;odko"
  ]
  node [
    id 2287
    label "wspania&#322;y"
  ]
  node [
    id 2288
    label "europ"
  ]
  node [
    id 2289
    label "wschodni"
  ]
  node [
    id 2290
    label "wielki"
  ]
  node [
    id 2291
    label "brytania"
  ]
  node [
    id 2292
    label "p&#243;&#322;wysep"
  ]
  node [
    id 2293
    label "skandynawski"
  ]
  node [
    id 2294
    label "wschodnioeuropejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 335
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 340
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 333
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 234
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 216
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 390
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 59
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 628
  ]
  edge [
    source 17
    target 323
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 632
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 635
  ]
  edge [
    source 17
    target 636
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 638
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 561
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 601
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 327
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 703
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 706
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 326
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 768
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 373
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 339
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 812
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 814
  ]
  edge [
    source 19
    target 343
  ]
  edge [
    source 19
    target 815
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 348
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 349
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 19
    target 832
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 833
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 834
  ]
  edge [
    source 19
    target 835
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 838
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 839
  ]
  edge [
    source 19
    target 840
  ]
  edge [
    source 19
    target 841
  ]
  edge [
    source 19
    target 842
  ]
  edge [
    source 19
    target 843
  ]
  edge [
    source 19
    target 844
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 846
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 340
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 414
  ]
  edge [
    source 19
    target 351
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 355
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 359
  ]
  edge [
    source 19
    target 360
  ]
  edge [
    source 19
    target 864
  ]
  edge [
    source 19
    target 865
  ]
  edge [
    source 19
    target 866
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 867
  ]
  edge [
    source 19
    target 338
  ]
  edge [
    source 19
    target 868
  ]
  edge [
    source 19
    target 869
  ]
  edge [
    source 19
    target 870
  ]
  edge [
    source 19
    target 871
  ]
  edge [
    source 19
    target 344
  ]
  edge [
    source 19
    target 872
  ]
  edge [
    source 19
    target 873
  ]
  edge [
    source 19
    target 874
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 875
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 352
  ]
  edge [
    source 19
    target 877
  ]
  edge [
    source 19
    target 878
  ]
  edge [
    source 19
    target 879
  ]
  edge [
    source 19
    target 880
  ]
  edge [
    source 19
    target 881
  ]
  edge [
    source 19
    target 882
  ]
  edge [
    source 19
    target 883
  ]
  edge [
    source 19
    target 884
  ]
  edge [
    source 19
    target 885
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 886
  ]
  edge [
    source 19
    target 887
  ]
  edge [
    source 19
    target 888
  ]
  edge [
    source 19
    target 889
  ]
  edge [
    source 19
    target 890
  ]
  edge [
    source 19
    target 891
  ]
  edge [
    source 19
    target 892
  ]
  edge [
    source 19
    target 893
  ]
  edge [
    source 19
    target 894
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 895
  ]
  edge [
    source 19
    target 896
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 345
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 898
  ]
  edge [
    source 19
    target 899
  ]
  edge [
    source 19
    target 900
  ]
  edge [
    source 19
    target 754
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 350
  ]
  edge [
    source 19
    target 903
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 19
    target 353
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 905
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 357
  ]
  edge [
    source 19
    target 358
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 79
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 597
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 41
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 19
    target 919
  ]
  edge [
    source 19
    target 920
  ]
  edge [
    source 19
    target 921
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 567
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 568
  ]
  edge [
    source 19
    target 426
  ]
  edge [
    source 19
    target 569
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 19
    target 572
  ]
  edge [
    source 19
    target 434
  ]
  edge [
    source 19
    target 573
  ]
  edge [
    source 19
    target 423
  ]
  edge [
    source 19
    target 575
  ]
  edge [
    source 19
    target 574
  ]
  edge [
    source 19
    target 576
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 577
  ]
  edge [
    source 19
    target 578
  ]
  edge [
    source 19
    target 579
  ]
  edge [
    source 19
    target 580
  ]
  edge [
    source 19
    target 581
  ]
  edge [
    source 19
    target 582
  ]
  edge [
    source 19
    target 583
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 584
  ]
  edge [
    source 19
    target 333
  ]
  edge [
    source 19
    target 585
  ]
  edge [
    source 19
    target 586
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 587
  ]
  edge [
    source 19
    target 588
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 589
  ]
  edge [
    source 19
    target 590
  ]
  edge [
    source 19
    target 591
  ]
  edge [
    source 19
    target 592
  ]
  edge [
    source 19
    target 409
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 19
    target 417
  ]
  edge [
    source 19
    target 418
  ]
  edge [
    source 19
    target 419
  ]
  edge [
    source 19
    target 420
  ]
  edge [
    source 19
    target 923
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 19
    target 927
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 929
  ]
  edge [
    source 19
    target 442
  ]
  edge [
    source 19
    target 443
  ]
  edge [
    source 19
    target 444
  ]
  edge [
    source 19
    target 445
  ]
  edge [
    source 19
    target 446
  ]
  edge [
    source 19
    target 335
  ]
  edge [
    source 19
    target 422
  ]
  edge [
    source 19
    target 421
  ]
  edge [
    source 19
    target 424
  ]
  edge [
    source 19
    target 425
  ]
  edge [
    source 19
    target 427
  ]
  edge [
    source 19
    target 428
  ]
  edge [
    source 19
    target 429
  ]
  edge [
    source 19
    target 430
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 431
  ]
  edge [
    source 19
    target 432
  ]
  edge [
    source 19
    target 433
  ]
  edge [
    source 19
    target 435
  ]
  edge [
    source 19
    target 436
  ]
  edge [
    source 19
    target 437
  ]
  edge [
    source 19
    target 438
  ]
  edge [
    source 19
    target 439
  ]
  edge [
    source 19
    target 440
  ]
  edge [
    source 19
    target 441
  ]
  edge [
    source 19
    target 504
  ]
  edge [
    source 19
    target 468
  ]
  edge [
    source 19
    target 505
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 506
  ]
  edge [
    source 19
    target 507
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 508
  ]
  edge [
    source 19
    target 509
  ]
  edge [
    source 19
    target 510
  ]
  edge [
    source 19
    target 511
  ]
  edge [
    source 19
    target 512
  ]
  edge [
    source 19
    target 930
  ]
  edge [
    source 19
    target 607
  ]
  edge [
    source 19
    target 931
  ]
  edge [
    source 19
    target 932
  ]
  edge [
    source 19
    target 370
  ]
  edge [
    source 19
    target 933
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 386
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 952
  ]
  edge [
    source 19
    target 953
  ]
  edge [
    source 19
    target 954
  ]
  edge [
    source 19
    target 955
  ]
  edge [
    source 19
    target 956
  ]
  edge [
    source 19
    target 957
  ]
  edge [
    source 19
    target 958
  ]
  edge [
    source 19
    target 959
  ]
  edge [
    source 19
    target 960
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 961
  ]
  edge [
    source 19
    target 962
  ]
  edge [
    source 19
    target 963
  ]
  edge [
    source 19
    target 964
  ]
  edge [
    source 19
    target 478
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 966
  ]
  edge [
    source 19
    target 967
  ]
  edge [
    source 19
    target 477
  ]
  edge [
    source 19
    target 479
  ]
  edge [
    source 19
    target 480
  ]
  edge [
    source 19
    target 467
  ]
  edge [
    source 19
    target 473
  ]
  edge [
    source 19
    target 474
  ]
  edge [
    source 19
    target 475
  ]
  edge [
    source 19
    target 476
  ]
  edge [
    source 19
    target 469
  ]
  edge [
    source 19
    target 470
  ]
  edge [
    source 19
    target 471
  ]
  edge [
    source 19
    target 472
  ]
  edge [
    source 19
    target 481
  ]
  edge [
    source 19
    target 482
  ]
  edge [
    source 19
    target 483
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 19
    target 968
  ]
  edge [
    source 19
    target 969
  ]
  edge [
    source 19
    target 970
  ]
  edge [
    source 19
    target 971
  ]
  edge [
    source 19
    target 972
  ]
  edge [
    source 19
    target 973
  ]
  edge [
    source 19
    target 974
  ]
  edge [
    source 19
    target 975
  ]
  edge [
    source 19
    target 976
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 19
    target 1130
  ]
  edge [
    source 19
    target 1131
  ]
  edge [
    source 19
    target 1132
  ]
  edge [
    source 19
    target 1133
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 19
    target 1144
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 1146
  ]
  edge [
    source 19
    target 1147
  ]
  edge [
    source 19
    target 1148
  ]
  edge [
    source 19
    target 1149
  ]
  edge [
    source 19
    target 1150
  ]
  edge [
    source 19
    target 1151
  ]
  edge [
    source 19
    target 1152
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 405
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 88
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 750
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 492
  ]
  edge [
    source 19
    target 493
  ]
  edge [
    source 19
    target 494
  ]
  edge [
    source 19
    target 495
  ]
  edge [
    source 19
    target 487
  ]
  edge [
    source 19
    target 488
  ]
  edge [
    source 19
    target 489
  ]
  edge [
    source 19
    target 490
  ]
  edge [
    source 19
    target 491
  ]
  edge [
    source 19
    target 496
  ]
  edge [
    source 19
    target 497
  ]
  edge [
    source 19
    target 498
  ]
  edge [
    source 19
    target 499
  ]
  edge [
    source 19
    target 500
  ]
  edge [
    source 19
    target 501
  ]
  edge [
    source 19
    target 502
  ]
  edge [
    source 19
    target 503
  ]
  edge [
    source 19
    target 485
  ]
  edge [
    source 19
    target 486
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1252
  ]
  edge [
    source 20
    target 1253
  ]
  edge [
    source 20
    target 603
  ]
  edge [
    source 20
    target 1254
  ]
  edge [
    source 20
    target 1255
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 1256
  ]
  edge [
    source 20
    target 1257
  ]
  edge [
    source 20
    target 48
  ]
  edge [
    source 20
    target 49
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 20
    target 51
  ]
  edge [
    source 20
    target 52
  ]
  edge [
    source 20
    target 53
  ]
  edge [
    source 20
    target 54
  ]
  edge [
    source 20
    target 55
  ]
  edge [
    source 20
    target 56
  ]
  edge [
    source 20
    target 57
  ]
  edge [
    source 20
    target 58
  ]
  edge [
    source 20
    target 1258
  ]
  edge [
    source 20
    target 1259
  ]
  edge [
    source 20
    target 1260
  ]
  edge [
    source 20
    target 1261
  ]
  edge [
    source 20
    target 1262
  ]
  edge [
    source 20
    target 1263
  ]
  edge [
    source 20
    target 1264
  ]
  edge [
    source 20
    target 1265
  ]
  edge [
    source 20
    target 1266
  ]
  edge [
    source 20
    target 283
  ]
  edge [
    source 20
    target 59
  ]
  edge [
    source 20
    target 1267
  ]
  edge [
    source 20
    target 616
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 1268
  ]
  edge [
    source 20
    target 1269
  ]
  edge [
    source 20
    target 1270
  ]
  edge [
    source 20
    target 1271
  ]
  edge [
    source 20
    target 1272
  ]
  edge [
    source 20
    target 1273
  ]
  edge [
    source 20
    target 1274
  ]
  edge [
    source 20
    target 1275
  ]
  edge [
    source 20
    target 1276
  ]
  edge [
    source 20
    target 1277
  ]
  edge [
    source 20
    target 1278
  ]
  edge [
    source 20
    target 1279
  ]
  edge [
    source 20
    target 1280
  ]
  edge [
    source 20
    target 1281
  ]
  edge [
    source 20
    target 1282
  ]
  edge [
    source 20
    target 1283
  ]
  edge [
    source 20
    target 1284
  ]
  edge [
    source 20
    target 1285
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 20
    target 1286
  ]
  edge [
    source 20
    target 1287
  ]
  edge [
    source 20
    target 703
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 304
  ]
  edge [
    source 21
    target 305
  ]
  edge [
    source 21
    target 288
  ]
  edge [
    source 21
    target 306
  ]
  edge [
    source 21
    target 307
  ]
  edge [
    source 21
    target 308
  ]
  edge [
    source 21
    target 309
  ]
  edge [
    source 21
    target 310
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 312
  ]
  edge [
    source 21
    target 1288
  ]
  edge [
    source 21
    target 1289
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 567
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 568
  ]
  edge [
    source 22
    target 426
  ]
  edge [
    source 22
    target 569
  ]
  edge [
    source 22
    target 570
  ]
  edge [
    source 22
    target 571
  ]
  edge [
    source 22
    target 340
  ]
  edge [
    source 22
    target 572
  ]
  edge [
    source 22
    target 434
  ]
  edge [
    source 22
    target 573
  ]
  edge [
    source 22
    target 423
  ]
  edge [
    source 22
    target 575
  ]
  edge [
    source 22
    target 574
  ]
  edge [
    source 22
    target 576
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 577
  ]
  edge [
    source 22
    target 578
  ]
  edge [
    source 22
    target 579
  ]
  edge [
    source 22
    target 580
  ]
  edge [
    source 22
    target 581
  ]
  edge [
    source 22
    target 582
  ]
  edge [
    source 22
    target 583
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 584
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 585
  ]
  edge [
    source 22
    target 586
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 587
  ]
  edge [
    source 22
    target 588
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 589
  ]
  edge [
    source 22
    target 590
  ]
  edge [
    source 22
    target 591
  ]
  edge [
    source 22
    target 592
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 431
  ]
  edge [
    source 22
    target 432
  ]
  edge [
    source 22
    target 433
  ]
  edge [
    source 22
    target 435
  ]
  edge [
    source 22
    target 436
  ]
  edge [
    source 22
    target 437
  ]
  edge [
    source 22
    target 438
  ]
  edge [
    source 22
    target 439
  ]
  edge [
    source 22
    target 440
  ]
  edge [
    source 22
    target 441
  ]
  edge [
    source 22
    target 442
  ]
  edge [
    source 22
    target 1290
  ]
  edge [
    source 22
    target 1291
  ]
  edge [
    source 22
    target 1292
  ]
  edge [
    source 22
    target 1293
  ]
  edge [
    source 22
    target 1294
  ]
  edge [
    source 22
    target 1295
  ]
  edge [
    source 22
    target 548
  ]
  edge [
    source 22
    target 1296
  ]
  edge [
    source 22
    target 1297
  ]
  edge [
    source 22
    target 662
  ]
  edge [
    source 22
    target 1298
  ]
  edge [
    source 22
    target 1299
  ]
  edge [
    source 22
    target 1300
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 1230
  ]
  edge [
    source 22
    target 1301
  ]
  edge [
    source 22
    target 1302
  ]
  edge [
    source 22
    target 910
  ]
  edge [
    source 22
    target 911
  ]
  edge [
    source 22
    target 79
  ]
  edge [
    source 22
    target 912
  ]
  edge [
    source 22
    target 913
  ]
  edge [
    source 22
    target 914
  ]
  edge [
    source 22
    target 597
  ]
  edge [
    source 22
    target 915
  ]
  edge [
    source 22
    target 41
  ]
  edge [
    source 22
    target 916
  ]
  edge [
    source 22
    target 917
  ]
  edge [
    source 22
    target 918
  ]
  edge [
    source 22
    target 1303
  ]
  edge [
    source 22
    target 390
  ]
  edge [
    source 22
    target 593
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 1304
  ]
  edge [
    source 22
    target 595
  ]
  edge [
    source 22
    target 1305
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 391
  ]
  edge [
    source 22
    target 392
  ]
  edge [
    source 22
    target 393
  ]
  edge [
    source 22
    target 394
  ]
  edge [
    source 22
    target 395
  ]
  edge [
    source 22
    target 396
  ]
  edge [
    source 22
    target 397
  ]
  edge [
    source 22
    target 1306
  ]
  edge [
    source 22
    target 1307
  ]
  edge [
    source 22
    target 647
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 271
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 1310
  ]
  edge [
    source 22
    target 1311
  ]
  edge [
    source 22
    target 1312
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 22
    target 507
  ]
  edge [
    source 22
    target 1317
  ]
  edge [
    source 22
    target 1318
  ]
  edge [
    source 22
    target 1319
  ]
  edge [
    source 22
    target 1320
  ]
  edge [
    source 22
    target 1321
  ]
  edge [
    source 22
    target 1322
  ]
  edge [
    source 22
    target 1323
  ]
  edge [
    source 22
    target 1324
  ]
  edge [
    source 22
    target 1325
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 1326
  ]
  edge [
    source 22
    target 1327
  ]
  edge [
    source 22
    target 1033
  ]
  edge [
    source 22
    target 1328
  ]
  edge [
    source 22
    target 1329
  ]
  edge [
    source 22
    target 996
  ]
  edge [
    source 22
    target 1330
  ]
  edge [
    source 22
    target 1331
  ]
  edge [
    source 22
    target 1332
  ]
  edge [
    source 22
    target 1333
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 1334
  ]
  edge [
    source 22
    target 1335
  ]
  edge [
    source 22
    target 1336
  ]
  edge [
    source 22
    target 1337
  ]
  edge [
    source 22
    target 1338
  ]
  edge [
    source 22
    target 1339
  ]
  edge [
    source 22
    target 1340
  ]
  edge [
    source 22
    target 1341
  ]
  edge [
    source 22
    target 1342
  ]
  edge [
    source 22
    target 1343
  ]
  edge [
    source 22
    target 1344
  ]
  edge [
    source 22
    target 1345
  ]
  edge [
    source 22
    target 1346
  ]
  edge [
    source 22
    target 1347
  ]
  edge [
    source 22
    target 1348
  ]
  edge [
    source 22
    target 1349
  ]
  edge [
    source 22
    target 1350
  ]
  edge [
    source 22
    target 1351
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 31
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1352
  ]
  edge [
    source 23
    target 1353
  ]
  edge [
    source 23
    target 1354
  ]
  edge [
    source 23
    target 1355
  ]
  edge [
    source 23
    target 1356
  ]
  edge [
    source 23
    target 79
  ]
  edge [
    source 23
    target 1357
  ]
  edge [
    source 23
    target 647
  ]
  edge [
    source 23
    target 1358
  ]
  edge [
    source 23
    target 1359
  ]
  edge [
    source 23
    target 1360
  ]
  edge [
    source 23
    target 1361
  ]
  edge [
    source 23
    target 1362
  ]
  edge [
    source 23
    target 1363
  ]
  edge [
    source 23
    target 1364
  ]
  edge [
    source 23
    target 1365
  ]
  edge [
    source 23
    target 1366
  ]
  edge [
    source 23
    target 1367
  ]
  edge [
    source 23
    target 1368
  ]
  edge [
    source 23
    target 1369
  ]
  edge [
    source 23
    target 1370
  ]
  edge [
    source 23
    target 1371
  ]
  edge [
    source 23
    target 1372
  ]
  edge [
    source 23
    target 1373
  ]
  edge [
    source 23
    target 1374
  ]
  edge [
    source 23
    target 1375
  ]
  edge [
    source 23
    target 1376
  ]
  edge [
    source 23
    target 1377
  ]
  edge [
    source 23
    target 1378
  ]
  edge [
    source 23
    target 1379
  ]
  edge [
    source 23
    target 1380
  ]
  edge [
    source 23
    target 1381
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 310
  ]
  edge [
    source 24
    target 1382
  ]
  edge [
    source 24
    target 1383
  ]
  edge [
    source 24
    target 324
  ]
  edge [
    source 24
    target 1384
  ]
  edge [
    source 24
    target 1385
  ]
  edge [
    source 24
    target 1386
  ]
  edge [
    source 24
    target 1387
  ]
  edge [
    source 24
    target 1388
  ]
  edge [
    source 24
    target 1389
  ]
  edge [
    source 24
    target 1390
  ]
  edge [
    source 24
    target 1391
  ]
  edge [
    source 24
    target 1392
  ]
  edge [
    source 24
    target 1393
  ]
  edge [
    source 24
    target 1394
  ]
  edge [
    source 24
    target 1395
  ]
  edge [
    source 24
    target 1396
  ]
  edge [
    source 24
    target 1186
  ]
  edge [
    source 24
    target 1397
  ]
  edge [
    source 24
    target 1398
  ]
  edge [
    source 24
    target 1399
  ]
  edge [
    source 24
    target 1400
  ]
  edge [
    source 24
    target 1401
  ]
  edge [
    source 24
    target 1402
  ]
  edge [
    source 24
    target 1403
  ]
  edge [
    source 24
    target 1404
  ]
  edge [
    source 24
    target 1405
  ]
  edge [
    source 24
    target 1406
  ]
  edge [
    source 24
    target 1407
  ]
  edge [
    source 24
    target 1408
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 336
  ]
  edge [
    source 25
    target 1409
  ]
  edge [
    source 25
    target 626
  ]
  edge [
    source 25
    target 1410
  ]
  edge [
    source 25
    target 1411
  ]
  edge [
    source 25
    target 129
  ]
  edge [
    source 25
    target 1412
  ]
  edge [
    source 25
    target 361
  ]
  edge [
    source 25
    target 362
  ]
  edge [
    source 25
    target 363
  ]
  edge [
    source 25
    target 364
  ]
  edge [
    source 25
    target 365
  ]
  edge [
    source 25
    target 366
  ]
  edge [
    source 25
    target 367
  ]
  edge [
    source 25
    target 368
  ]
  edge [
    source 25
    target 369
  ]
  edge [
    source 25
    target 87
  ]
  edge [
    source 25
    target 370
  ]
  edge [
    source 25
    target 371
  ]
  edge [
    source 25
    target 372
  ]
  edge [
    source 25
    target 373
  ]
  edge [
    source 25
    target 374
  ]
  edge [
    source 25
    target 375
  ]
  edge [
    source 25
    target 376
  ]
  edge [
    source 25
    target 377
  ]
  edge [
    source 25
    target 378
  ]
  edge [
    source 25
    target 379
  ]
  edge [
    source 25
    target 380
  ]
  edge [
    source 25
    target 381
  ]
  edge [
    source 25
    target 382
  ]
  edge [
    source 25
    target 383
  ]
  edge [
    source 25
    target 384
  ]
  edge [
    source 25
    target 385
  ]
  edge [
    source 25
    target 386
  ]
  edge [
    source 25
    target 387
  ]
  edge [
    source 25
    target 388
  ]
  edge [
    source 25
    target 389
  ]
  edge [
    source 25
    target 1413
  ]
  edge [
    source 25
    target 1414
  ]
  edge [
    source 25
    target 1415
  ]
  edge [
    source 25
    target 1416
  ]
  edge [
    source 25
    target 664
  ]
  edge [
    source 25
    target 1417
  ]
  edge [
    source 25
    target 1418
  ]
  edge [
    source 25
    target 1419
  ]
  edge [
    source 25
    target 1420
  ]
  edge [
    source 25
    target 1421
  ]
  edge [
    source 25
    target 1422
  ]
  edge [
    source 25
    target 1423
  ]
  edge [
    source 25
    target 629
  ]
  edge [
    source 25
    target 642
  ]
  edge [
    source 25
    target 1424
  ]
  edge [
    source 25
    target 1425
  ]
  edge [
    source 25
    target 1426
  ]
  edge [
    source 25
    target 1427
  ]
  edge [
    source 25
    target 1428
  ]
  edge [
    source 25
    target 561
  ]
  edge [
    source 25
    target 1429
  ]
  edge [
    source 25
    target 1430
  ]
  edge [
    source 25
    target 1431
  ]
  edge [
    source 25
    target 1432
  ]
  edge [
    source 25
    target 1433
  ]
  edge [
    source 25
    target 1434
  ]
  edge [
    source 25
    target 1435
  ]
  edge [
    source 25
    target 1436
  ]
  edge [
    source 25
    target 1437
  ]
  edge [
    source 25
    target 1438
  ]
  edge [
    source 25
    target 1439
  ]
  edge [
    source 25
    target 1440
  ]
  edge [
    source 25
    target 1441
  ]
  edge [
    source 25
    target 1442
  ]
  edge [
    source 25
    target 1443
  ]
  edge [
    source 25
    target 1444
  ]
  edge [
    source 25
    target 1445
  ]
  edge [
    source 25
    target 1304
  ]
  edge [
    source 25
    target 128
  ]
  edge [
    source 25
    target 1363
  ]
  edge [
    source 25
    target 670
  ]
  edge [
    source 25
    target 1446
  ]
  edge [
    source 25
    target 1447
  ]
  edge [
    source 25
    target 1448
  ]
  edge [
    source 25
    target 1449
  ]
  edge [
    source 25
    target 1450
  ]
  edge [
    source 25
    target 1451
  ]
  edge [
    source 25
    target 1452
  ]
  edge [
    source 25
    target 1453
  ]
  edge [
    source 25
    target 271
  ]
  edge [
    source 25
    target 1454
  ]
  edge [
    source 25
    target 1455
  ]
  edge [
    source 25
    target 1456
  ]
  edge [
    source 25
    target 1457
  ]
  edge [
    source 25
    target 1458
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1459
  ]
  edge [
    source 27
    target 1460
  ]
  edge [
    source 27
    target 1461
  ]
  edge [
    source 27
    target 1462
  ]
  edge [
    source 27
    target 1463
  ]
  edge [
    source 27
    target 1464
  ]
  edge [
    source 27
    target 1465
  ]
  edge [
    source 27
    target 1466
  ]
  edge [
    source 27
    target 1467
  ]
  edge [
    source 27
    target 1468
  ]
  edge [
    source 27
    target 1469
  ]
  edge [
    source 27
    target 777
  ]
  edge [
    source 27
    target 1470
  ]
  edge [
    source 27
    target 1471
  ]
  edge [
    source 27
    target 1472
  ]
  edge [
    source 27
    target 1473
  ]
  edge [
    source 27
    target 1474
  ]
  edge [
    source 27
    target 1475
  ]
  edge [
    source 27
    target 1444
  ]
  edge [
    source 27
    target 1476
  ]
  edge [
    source 27
    target 373
  ]
  edge [
    source 27
    target 1477
  ]
  edge [
    source 27
    target 1478
  ]
  edge [
    source 27
    target 1479
  ]
  edge [
    source 27
    target 1480
  ]
  edge [
    source 27
    target 1481
  ]
  edge [
    source 27
    target 1482
  ]
  edge [
    source 27
    target 1483
  ]
  edge [
    source 27
    target 1484
  ]
  edge [
    source 27
    target 1485
  ]
  edge [
    source 27
    target 1486
  ]
  edge [
    source 27
    target 1487
  ]
  edge [
    source 27
    target 1488
  ]
  edge [
    source 27
    target 1489
  ]
  edge [
    source 27
    target 1490
  ]
  edge [
    source 27
    target 771
  ]
  edge [
    source 27
    target 1491
  ]
  edge [
    source 27
    target 1492
  ]
  edge [
    source 27
    target 1493
  ]
  edge [
    source 27
    target 1494
  ]
  edge [
    source 27
    target 1495
  ]
  edge [
    source 27
    target 1496
  ]
  edge [
    source 27
    target 1497
  ]
  edge [
    source 27
    target 1498
  ]
  edge [
    source 27
    target 1499
  ]
  edge [
    source 27
    target 1500
  ]
  edge [
    source 27
    target 375
  ]
  edge [
    source 27
    target 1501
  ]
  edge [
    source 27
    target 1502
  ]
  edge [
    source 27
    target 1503
  ]
  edge [
    source 27
    target 1504
  ]
  edge [
    source 27
    target 1505
  ]
  edge [
    source 27
    target 1506
  ]
  edge [
    source 27
    target 1507
  ]
  edge [
    source 27
    target 1508
  ]
  edge [
    source 27
    target 1509
  ]
  edge [
    source 27
    target 601
  ]
  edge [
    source 27
    target 242
  ]
  edge [
    source 27
    target 1510
  ]
  edge [
    source 27
    target 1511
  ]
  edge [
    source 27
    target 1512
  ]
  edge [
    source 27
    target 1513
  ]
  edge [
    source 27
    target 1514
  ]
  edge [
    source 27
    target 1515
  ]
  edge [
    source 27
    target 1516
  ]
  edge [
    source 27
    target 1517
  ]
  edge [
    source 27
    target 1415
  ]
  edge [
    source 27
    target 1518
  ]
  edge [
    source 27
    target 1519
  ]
  edge [
    source 27
    target 1520
  ]
  edge [
    source 27
    target 1521
  ]
  edge [
    source 27
    target 1522
  ]
  edge [
    source 27
    target 1523
  ]
  edge [
    source 27
    target 1524
  ]
  edge [
    source 27
    target 1525
  ]
  edge [
    source 27
    target 1526
  ]
  edge [
    source 27
    target 1527
  ]
  edge [
    source 27
    target 1528
  ]
  edge [
    source 27
    target 1529
  ]
  edge [
    source 27
    target 1530
  ]
  edge [
    source 27
    target 1531
  ]
  edge [
    source 27
    target 1532
  ]
  edge [
    source 27
    target 1533
  ]
  edge [
    source 27
    target 1534
  ]
  edge [
    source 27
    target 1535
  ]
  edge [
    source 27
    target 1536
  ]
  edge [
    source 27
    target 1537
  ]
  edge [
    source 27
    target 1538
  ]
  edge [
    source 27
    target 1539
  ]
  edge [
    source 27
    target 1540
  ]
  edge [
    source 27
    target 1541
  ]
  edge [
    source 27
    target 1542
  ]
  edge [
    source 27
    target 1543
  ]
  edge [
    source 27
    target 1544
  ]
  edge [
    source 27
    target 1545
  ]
  edge [
    source 27
    target 1546
  ]
  edge [
    source 27
    target 1547
  ]
  edge [
    source 27
    target 1548
  ]
  edge [
    source 27
    target 1549
  ]
  edge [
    source 27
    target 1550
  ]
  edge [
    source 27
    target 1551
  ]
  edge [
    source 27
    target 532
  ]
  edge [
    source 27
    target 1552
  ]
  edge [
    source 27
    target 1553
  ]
  edge [
    source 27
    target 1554
  ]
  edge [
    source 27
    target 1555
  ]
  edge [
    source 27
    target 1556
  ]
  edge [
    source 27
    target 714
  ]
  edge [
    source 27
    target 1557
  ]
  edge [
    source 27
    target 1558
  ]
  edge [
    source 27
    target 1559
  ]
  edge [
    source 27
    target 1560
  ]
  edge [
    source 27
    target 1561
  ]
  edge [
    source 27
    target 1562
  ]
  edge [
    source 27
    target 1563
  ]
  edge [
    source 27
    target 1564
  ]
  edge [
    source 27
    target 1565
  ]
  edge [
    source 27
    target 1566
  ]
  edge [
    source 27
    target 948
  ]
  edge [
    source 27
    target 1567
  ]
  edge [
    source 27
    target 1568
  ]
  edge [
    source 27
    target 1569
  ]
  edge [
    source 27
    target 1570
  ]
  edge [
    source 27
    target 1571
  ]
  edge [
    source 27
    target 950
  ]
  edge [
    source 27
    target 1572
  ]
  edge [
    source 27
    target 1573
  ]
  edge [
    source 27
    target 1574
  ]
  edge [
    source 27
    target 1575
  ]
  edge [
    source 27
    target 1576
  ]
  edge [
    source 27
    target 283
  ]
  edge [
    source 27
    target 1577
  ]
  edge [
    source 27
    target 1578
  ]
  edge [
    source 27
    target 1579
  ]
  edge [
    source 27
    target 735
  ]
  edge [
    source 27
    target 59
  ]
  edge [
    source 27
    target 1580
  ]
  edge [
    source 27
    target 1581
  ]
  edge [
    source 27
    target 1582
  ]
  edge [
    source 27
    target 1583
  ]
  edge [
    source 27
    target 1584
  ]
  edge [
    source 27
    target 1585
  ]
  edge [
    source 27
    target 1586
  ]
  edge [
    source 27
    target 1587
  ]
  edge [
    source 27
    target 1588
  ]
  edge [
    source 27
    target 1589
  ]
  edge [
    source 27
    target 1590
  ]
  edge [
    source 27
    target 1591
  ]
  edge [
    source 27
    target 1592
  ]
  edge [
    source 27
    target 1593
  ]
  edge [
    source 27
    target 1594
  ]
  edge [
    source 27
    target 1595
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 1597
  ]
  edge [
    source 27
    target 1598
  ]
  edge [
    source 27
    target 1599
  ]
  edge [
    source 27
    target 1600
  ]
  edge [
    source 27
    target 1601
  ]
  edge [
    source 27
    target 1602
  ]
  edge [
    source 27
    target 1603
  ]
  edge [
    source 27
    target 1604
  ]
  edge [
    source 27
    target 1605
  ]
  edge [
    source 27
    target 1606
  ]
  edge [
    source 27
    target 1607
  ]
  edge [
    source 27
    target 1608
  ]
  edge [
    source 27
    target 1609
  ]
  edge [
    source 27
    target 1610
  ]
  edge [
    source 27
    target 1611
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 1615
  ]
  edge [
    source 27
    target 1616
  ]
  edge [
    source 27
    target 1617
  ]
  edge [
    source 27
    target 1618
  ]
  edge [
    source 27
    target 1619
  ]
  edge [
    source 27
    target 1620
  ]
  edge [
    source 27
    target 1621
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1623
  ]
  edge [
    source 27
    target 1624
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 1626
  ]
  edge [
    source 27
    target 1627
  ]
  edge [
    source 27
    target 1628
  ]
  edge [
    source 27
    target 1629
  ]
  edge [
    source 27
    target 1630
  ]
  edge [
    source 27
    target 1631
  ]
  edge [
    source 27
    target 1632
  ]
  edge [
    source 27
    target 1633
  ]
  edge [
    source 27
    target 1634
  ]
  edge [
    source 27
    target 1635
  ]
  edge [
    source 27
    target 1636
  ]
  edge [
    source 27
    target 1637
  ]
  edge [
    source 27
    target 1638
  ]
  edge [
    source 27
    target 675
  ]
  edge [
    source 27
    target 1639
  ]
  edge [
    source 27
    target 1640
  ]
  edge [
    source 27
    target 676
  ]
  edge [
    source 27
    target 1641
  ]
  edge [
    source 27
    target 1642
  ]
  edge [
    source 27
    target 1643
  ]
  edge [
    source 27
    target 1644
  ]
  edge [
    source 27
    target 1645
  ]
  edge [
    source 27
    target 1646
  ]
  edge [
    source 27
    target 682
  ]
  edge [
    source 27
    target 1647
  ]
  edge [
    source 27
    target 1648
  ]
  edge [
    source 27
    target 1649
  ]
  edge [
    source 27
    target 1650
  ]
  edge [
    source 27
    target 1651
  ]
  edge [
    source 27
    target 1652
  ]
  edge [
    source 27
    target 1653
  ]
  edge [
    source 27
    target 1654
  ]
  edge [
    source 27
    target 1655
  ]
  edge [
    source 27
    target 1656
  ]
  edge [
    source 27
    target 1657
  ]
  edge [
    source 27
    target 1658
  ]
  edge [
    source 27
    target 1659
  ]
  edge [
    source 27
    target 1660
  ]
  edge [
    source 27
    target 1661
  ]
  edge [
    source 27
    target 1662
  ]
  edge [
    source 27
    target 1663
  ]
  edge [
    source 27
    target 1664
  ]
  edge [
    source 27
    target 1665
  ]
  edge [
    source 27
    target 1666
  ]
  edge [
    source 27
    target 1667
  ]
  edge [
    source 27
    target 1668
  ]
  edge [
    source 27
    target 1669
  ]
  edge [
    source 27
    target 1670
  ]
  edge [
    source 27
    target 1671
  ]
  edge [
    source 27
    target 1672
  ]
  edge [
    source 27
    target 1673
  ]
  edge [
    source 27
    target 1674
  ]
  edge [
    source 27
    target 362
  ]
  edge [
    source 27
    target 1675
  ]
  edge [
    source 27
    target 1676
  ]
  edge [
    source 27
    target 1677
  ]
  edge [
    source 27
    target 1678
  ]
  edge [
    source 27
    target 1679
  ]
  edge [
    source 27
    target 1680
  ]
  edge [
    source 27
    target 1681
  ]
  edge [
    source 27
    target 1682
  ]
  edge [
    source 27
    target 1683
  ]
  edge [
    source 27
    target 1684
  ]
  edge [
    source 27
    target 1685
  ]
  edge [
    source 27
    target 1686
  ]
  edge [
    source 27
    target 1687
  ]
  edge [
    source 27
    target 1688
  ]
  edge [
    source 27
    target 779
  ]
  edge [
    source 27
    target 1689
  ]
  edge [
    source 27
    target 1690
  ]
  edge [
    source 27
    target 1691
  ]
  edge [
    source 27
    target 1692
  ]
  edge [
    source 27
    target 1693
  ]
  edge [
    source 27
    target 1451
  ]
  edge [
    source 27
    target 1694
  ]
  edge [
    source 27
    target 1695
  ]
  edge [
    source 27
    target 1696
  ]
  edge [
    source 27
    target 386
  ]
  edge [
    source 27
    target 1697
  ]
  edge [
    source 27
    target 387
  ]
  edge [
    source 27
    target 1698
  ]
  edge [
    source 27
    target 1699
  ]
  edge [
    source 27
    target 1700
  ]
  edge [
    source 27
    target 1701
  ]
  edge [
    source 27
    target 1702
  ]
  edge [
    source 27
    target 1703
  ]
  edge [
    source 27
    target 1704
  ]
  edge [
    source 27
    target 1705
  ]
  edge [
    source 27
    target 1706
  ]
  edge [
    source 27
    target 1707
  ]
  edge [
    source 27
    target 1708
  ]
  edge [
    source 27
    target 1709
  ]
  edge [
    source 27
    target 1710
  ]
  edge [
    source 27
    target 1711
  ]
  edge [
    source 27
    target 1712
  ]
  edge [
    source 27
    target 1713
  ]
  edge [
    source 27
    target 1714
  ]
  edge [
    source 27
    target 1715
  ]
  edge [
    source 27
    target 1716
  ]
  edge [
    source 27
    target 1717
  ]
  edge [
    source 27
    target 1718
  ]
  edge [
    source 27
    target 1719
  ]
  edge [
    source 27
    target 128
  ]
  edge [
    source 27
    target 1720
  ]
  edge [
    source 27
    target 1721
  ]
  edge [
    source 27
    target 1722
  ]
  edge [
    source 27
    target 1723
  ]
  edge [
    source 27
    target 1724
  ]
  edge [
    source 27
    target 1725
  ]
  edge [
    source 27
    target 1726
  ]
  edge [
    source 27
    target 1727
  ]
  edge [
    source 27
    target 1728
  ]
  edge [
    source 27
    target 1729
  ]
  edge [
    source 27
    target 1730
  ]
  edge [
    source 27
    target 1731
  ]
  edge [
    source 27
    target 1732
  ]
  edge [
    source 27
    target 1733
  ]
  edge [
    source 27
    target 1734
  ]
  edge [
    source 27
    target 1735
  ]
  edge [
    source 27
    target 1736
  ]
  edge [
    source 27
    target 1737
  ]
  edge [
    source 27
    target 1738
  ]
  edge [
    source 27
    target 1739
  ]
  edge [
    source 27
    target 1740
  ]
  edge [
    source 27
    target 1741
  ]
  edge [
    source 27
    target 1742
  ]
  edge [
    source 27
    target 1743
  ]
  edge [
    source 27
    target 565
  ]
  edge [
    source 27
    target 1744
  ]
  edge [
    source 27
    target 1745
  ]
  edge [
    source 27
    target 1746
  ]
  edge [
    source 27
    target 1747
  ]
  edge [
    source 27
    target 1748
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1749
  ]
  edge [
    source 28
    target 424
  ]
  edge [
    source 28
    target 1750
  ]
  edge [
    source 28
    target 1751
  ]
  edge [
    source 28
    target 1752
  ]
  edge [
    source 28
    target 1753
  ]
  edge [
    source 28
    target 1754
  ]
  edge [
    source 28
    target 418
  ]
  edge [
    source 28
    target 1755
  ]
  edge [
    source 28
    target 1756
  ]
  edge [
    source 28
    target 750
  ]
  edge [
    source 28
    target 1757
  ]
  edge [
    source 28
    target 692
  ]
  edge [
    source 28
    target 1276
  ]
  edge [
    source 28
    target 1290
  ]
  edge [
    source 28
    target 1758
  ]
  edge [
    source 28
    target 643
  ]
  edge [
    source 28
    target 1759
  ]
  edge [
    source 28
    target 637
  ]
  edge [
    source 28
    target 1760
  ]
  edge [
    source 28
    target 662
  ]
  edge [
    source 28
    target 751
  ]
  edge [
    source 28
    target 696
  ]
  edge [
    source 28
    target 434
  ]
  edge [
    source 28
    target 461
  ]
  edge [
    source 28
    target 1761
  ]
  edge [
    source 28
    target 1762
  ]
  edge [
    source 28
    target 638
  ]
  edge [
    source 28
    target 1763
  ]
  edge [
    source 28
    target 1764
  ]
  edge [
    source 28
    target 1765
  ]
  edge [
    source 28
    target 1766
  ]
  edge [
    source 28
    target 333
  ]
  edge [
    source 28
    target 1767
  ]
  edge [
    source 28
    target 1768
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 1769
  ]
  edge [
    source 28
    target 1770
  ]
  edge [
    source 28
    target 1771
  ]
  edge [
    source 28
    target 1772
  ]
  edge [
    source 28
    target 1773
  ]
  edge [
    source 28
    target 49
  ]
  edge [
    source 28
    target 1774
  ]
  edge [
    source 28
    target 1775
  ]
  edge [
    source 28
    target 1776
  ]
  edge [
    source 28
    target 1777
  ]
  edge [
    source 28
    target 1778
  ]
  edge [
    source 28
    target 1779
  ]
  edge [
    source 28
    target 1780
  ]
  edge [
    source 28
    target 1781
  ]
  edge [
    source 28
    target 1782
  ]
  edge [
    source 28
    target 1783
  ]
  edge [
    source 28
    target 1784
  ]
  edge [
    source 28
    target 1785
  ]
  edge [
    source 28
    target 1786
  ]
  edge [
    source 28
    target 889
  ]
  edge [
    source 28
    target 1787
  ]
  edge [
    source 28
    target 1788
  ]
  edge [
    source 28
    target 1789
  ]
  edge [
    source 28
    target 1790
  ]
  edge [
    source 28
    target 1791
  ]
  edge [
    source 28
    target 1792
  ]
  edge [
    source 28
    target 1793
  ]
  edge [
    source 28
    target 45
  ]
  edge [
    source 28
    target 1794
  ]
  edge [
    source 28
    target 1795
  ]
  edge [
    source 28
    target 1796
  ]
  edge [
    source 28
    target 1797
  ]
  edge [
    source 28
    target 1798
  ]
  edge [
    source 28
    target 1350
  ]
  edge [
    source 28
    target 1799
  ]
  edge [
    source 28
    target 1800
  ]
  edge [
    source 28
    target 1801
  ]
  edge [
    source 28
    target 340
  ]
  edge [
    source 28
    target 1802
  ]
  edge [
    source 28
    target 1803
  ]
  edge [
    source 28
    target 444
  ]
  edge [
    source 28
    target 1804
  ]
  edge [
    source 28
    target 921
  ]
  edge [
    source 28
    target 1805
  ]
  edge [
    source 28
    target 1806
  ]
  edge [
    source 28
    target 79
  ]
  edge [
    source 28
    target 335
  ]
  edge [
    source 28
    target 1807
  ]
  edge [
    source 28
    target 1808
  ]
  edge [
    source 28
    target 1809
  ]
  edge [
    source 28
    target 66
  ]
  edge [
    source 28
    target 1810
  ]
  edge [
    source 28
    target 1811
  ]
  edge [
    source 28
    target 1812
  ]
  edge [
    source 28
    target 1813
  ]
  edge [
    source 28
    target 1814
  ]
  edge [
    source 28
    target 1815
  ]
  edge [
    source 28
    target 1816
  ]
  edge [
    source 28
    target 1817
  ]
  edge [
    source 28
    target 1818
  ]
  edge [
    source 28
    target 1577
  ]
  edge [
    source 28
    target 1819
  ]
  edge [
    source 28
    target 1534
  ]
  edge [
    source 28
    target 1820
  ]
  edge [
    source 28
    target 1821
  ]
  edge [
    source 28
    target 1822
  ]
  edge [
    source 28
    target 1823
  ]
  edge [
    source 28
    target 1467
  ]
  edge [
    source 28
    target 1502
  ]
  edge [
    source 28
    target 1824
  ]
  edge [
    source 28
    target 1470
  ]
  edge [
    source 28
    target 1490
  ]
  edge [
    source 28
    target 1471
  ]
  edge [
    source 28
    target 1507
  ]
  edge [
    source 28
    target 1825
  ]
  edge [
    source 28
    target 442
  ]
  edge [
    source 28
    target 1826
  ]
  edge [
    source 28
    target 1827
  ]
  edge [
    source 28
    target 1239
  ]
  edge [
    source 28
    target 1828
  ]
  edge [
    source 28
    target 1829
  ]
  edge [
    source 28
    target 1830
  ]
  edge [
    source 28
    target 359
  ]
  edge [
    source 28
    target 1831
  ]
  edge [
    source 28
    target 1832
  ]
  edge [
    source 28
    target 1833
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1834
  ]
  edge [
    source 29
    target 1405
  ]
  edge [
    source 29
    target 1835
  ]
  edge [
    source 29
    target 1836
  ]
  edge [
    source 29
    target 1837
  ]
  edge [
    source 29
    target 1838
  ]
  edge [
    source 29
    target 1839
  ]
  edge [
    source 29
    target 1840
  ]
  edge [
    source 29
    target 1841
  ]
  edge [
    source 29
    target 1842
  ]
  edge [
    source 29
    target 1843
  ]
  edge [
    source 29
    target 1382
  ]
  edge [
    source 29
    target 1844
  ]
  edge [
    source 29
    target 1845
  ]
  edge [
    source 29
    target 1846
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1847
  ]
  edge [
    source 30
    target 1848
  ]
  edge [
    source 30
    target 1849
  ]
  edge [
    source 30
    target 1850
  ]
  edge [
    source 30
    target 1851
  ]
  edge [
    source 30
    target 1852
  ]
  edge [
    source 30
    target 1853
  ]
  edge [
    source 30
    target 1854
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 1855
  ]
  edge [
    source 31
    target 1856
  ]
  edge [
    source 31
    target 152
  ]
  edge [
    source 31
    target 1857
  ]
  edge [
    source 31
    target 567
  ]
  edge [
    source 31
    target 568
  ]
  edge [
    source 31
    target 426
  ]
  edge [
    source 31
    target 569
  ]
  edge [
    source 31
    target 570
  ]
  edge [
    source 31
    target 571
  ]
  edge [
    source 31
    target 340
  ]
  edge [
    source 31
    target 572
  ]
  edge [
    source 31
    target 434
  ]
  edge [
    source 31
    target 573
  ]
  edge [
    source 31
    target 423
  ]
  edge [
    source 31
    target 574
  ]
  edge [
    source 31
    target 575
  ]
  edge [
    source 31
    target 576
  ]
  edge [
    source 31
    target 128
  ]
  edge [
    source 31
    target 211
  ]
  edge [
    source 31
    target 577
  ]
  edge [
    source 31
    target 578
  ]
  edge [
    source 31
    target 579
  ]
  edge [
    source 31
    target 580
  ]
  edge [
    source 31
    target 581
  ]
  edge [
    source 31
    target 582
  ]
  edge [
    source 31
    target 583
  ]
  edge [
    source 31
    target 182
  ]
  edge [
    source 31
    target 584
  ]
  edge [
    source 31
    target 333
  ]
  edge [
    source 31
    target 585
  ]
  edge [
    source 31
    target 586
  ]
  edge [
    source 31
    target 234
  ]
  edge [
    source 31
    target 587
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 588
  ]
  edge [
    source 31
    target 589
  ]
  edge [
    source 31
    target 590
  ]
  edge [
    source 31
    target 591
  ]
  edge [
    source 31
    target 592
  ]
  edge [
    source 31
    target 1076
  ]
  edge [
    source 31
    target 1858
  ]
  edge [
    source 31
    target 171
  ]
  edge [
    source 31
    target 172
  ]
  edge [
    source 31
    target 2294
  ]
  edge [
    source 32
    target 1306
  ]
  edge [
    source 32
    target 1307
  ]
  edge [
    source 32
    target 647
  ]
  edge [
    source 32
    target 1308
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 32
    target 176
  ]
  edge [
    source 32
    target 1309
  ]
  edge [
    source 32
    target 1310
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 32
    target 1311
  ]
  edge [
    source 32
    target 1312
  ]
  edge [
    source 32
    target 1859
  ]
  edge [
    source 32
    target 1860
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 145
  ]
  edge [
    source 32
    target 1861
  ]
  edge [
    source 32
    target 484
  ]
  edge [
    source 32
    target 1862
  ]
  edge [
    source 32
    target 1863
  ]
  edge [
    source 32
    target 108
  ]
  edge [
    source 32
    target 1864
  ]
  edge [
    source 32
    target 1865
  ]
  edge [
    source 32
    target 646
  ]
  edge [
    source 32
    target 1866
  ]
  edge [
    source 32
    target 1867
  ]
  edge [
    source 32
    target 1868
  ]
  edge [
    source 32
    target 1869
  ]
  edge [
    source 32
    target 430
  ]
  edge [
    source 32
    target 1870
  ]
  edge [
    source 32
    target 1871
  ]
  edge [
    source 32
    target 1872
  ]
  edge [
    source 32
    target 1873
  ]
  edge [
    source 32
    target 1647
  ]
  edge [
    source 32
    target 373
  ]
  edge [
    source 32
    target 567
  ]
  edge [
    source 32
    target 568
  ]
  edge [
    source 32
    target 426
  ]
  edge [
    source 32
    target 569
  ]
  edge [
    source 32
    target 570
  ]
  edge [
    source 32
    target 571
  ]
  edge [
    source 32
    target 340
  ]
  edge [
    source 32
    target 572
  ]
  edge [
    source 32
    target 434
  ]
  edge [
    source 32
    target 573
  ]
  edge [
    source 32
    target 423
  ]
  edge [
    source 32
    target 574
  ]
  edge [
    source 32
    target 575
  ]
  edge [
    source 32
    target 576
  ]
  edge [
    source 32
    target 128
  ]
  edge [
    source 32
    target 211
  ]
  edge [
    source 32
    target 577
  ]
  edge [
    source 32
    target 578
  ]
  edge [
    source 32
    target 579
  ]
  edge [
    source 32
    target 580
  ]
  edge [
    source 32
    target 581
  ]
  edge [
    source 32
    target 582
  ]
  edge [
    source 32
    target 583
  ]
  edge [
    source 32
    target 182
  ]
  edge [
    source 32
    target 584
  ]
  edge [
    source 32
    target 333
  ]
  edge [
    source 32
    target 585
  ]
  edge [
    source 32
    target 586
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 587
  ]
  edge [
    source 32
    target 216
  ]
  edge [
    source 32
    target 588
  ]
  edge [
    source 32
    target 589
  ]
  edge [
    source 32
    target 590
  ]
  edge [
    source 32
    target 591
  ]
  edge [
    source 32
    target 592
  ]
  edge [
    source 32
    target 1874
  ]
  edge [
    source 32
    target 1875
  ]
  edge [
    source 32
    target 1876
  ]
  edge [
    source 32
    target 1315
  ]
  edge [
    source 32
    target 1877
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 1878
  ]
  edge [
    source 32
    target 1879
  ]
  edge [
    source 32
    target 1880
  ]
  edge [
    source 32
    target 1881
  ]
  edge [
    source 32
    target 1882
  ]
  edge [
    source 32
    target 79
  ]
  edge [
    source 32
    target 1883
  ]
  edge [
    source 32
    target 1884
  ]
  edge [
    source 32
    target 1885
  ]
  edge [
    source 32
    target 1886
  ]
  edge [
    source 32
    target 1887
  ]
  edge [
    source 32
    target 1888
  ]
  edge [
    source 32
    target 1889
  ]
  edge [
    source 32
    target 1890
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 33
    target 1891
  ]
  edge [
    source 33
    target 1892
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 245
  ]
  edge [
    source 33
    target 1893
  ]
  edge [
    source 33
    target 1894
  ]
  edge [
    source 33
    target 1895
  ]
  edge [
    source 33
    target 1896
  ]
  edge [
    source 33
    target 1897
  ]
  edge [
    source 33
    target 1898
  ]
  edge [
    source 33
    target 1899
  ]
  edge [
    source 33
    target 1900
  ]
  edge [
    source 33
    target 1901
  ]
  edge [
    source 33
    target 1252
  ]
  edge [
    source 33
    target 1902
  ]
  edge [
    source 33
    target 1903
  ]
  edge [
    source 33
    target 1904
  ]
  edge [
    source 33
    target 1271
  ]
  edge [
    source 33
    target 1905
  ]
  edge [
    source 33
    target 242
  ]
  edge [
    source 33
    target 58
  ]
  edge [
    source 33
    target 1906
  ]
  edge [
    source 33
    target 1907
  ]
  edge [
    source 33
    target 1908
  ]
  edge [
    source 33
    target 620
  ]
  edge [
    source 33
    target 267
  ]
  edge [
    source 33
    target 1909
  ]
  edge [
    source 33
    target 1910
  ]
  edge [
    source 33
    target 1911
  ]
  edge [
    source 33
    target 1912
  ]
  edge [
    source 33
    target 59
  ]
  edge [
    source 33
    target 1913
  ]
  edge [
    source 33
    target 1527
  ]
  edge [
    source 33
    target 1914
  ]
  edge [
    source 33
    target 1915
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 1916
  ]
  edge [
    source 34
    target 1859
  ]
  edge [
    source 34
    target 1860
  ]
  edge [
    source 34
    target 1286
  ]
  edge [
    source 34
    target 145
  ]
  edge [
    source 34
    target 1861
  ]
  edge [
    source 34
    target 484
  ]
  edge [
    source 34
    target 567
  ]
  edge [
    source 34
    target 568
  ]
  edge [
    source 34
    target 426
  ]
  edge [
    source 34
    target 569
  ]
  edge [
    source 34
    target 570
  ]
  edge [
    source 34
    target 571
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 572
  ]
  edge [
    source 34
    target 434
  ]
  edge [
    source 34
    target 573
  ]
  edge [
    source 34
    target 423
  ]
  edge [
    source 34
    target 574
  ]
  edge [
    source 34
    target 575
  ]
  edge [
    source 34
    target 576
  ]
  edge [
    source 34
    target 128
  ]
  edge [
    source 34
    target 211
  ]
  edge [
    source 34
    target 577
  ]
  edge [
    source 34
    target 578
  ]
  edge [
    source 34
    target 579
  ]
  edge [
    source 34
    target 580
  ]
  edge [
    source 34
    target 581
  ]
  edge [
    source 34
    target 582
  ]
  edge [
    source 34
    target 583
  ]
  edge [
    source 34
    target 182
  ]
  edge [
    source 34
    target 584
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 34
    target 585
  ]
  edge [
    source 34
    target 586
  ]
  edge [
    source 34
    target 234
  ]
  edge [
    source 34
    target 587
  ]
  edge [
    source 34
    target 216
  ]
  edge [
    source 34
    target 588
  ]
  edge [
    source 34
    target 589
  ]
  edge [
    source 34
    target 590
  ]
  edge [
    source 34
    target 591
  ]
  edge [
    source 34
    target 592
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 647
  ]
  edge [
    source 35
    target 1917
  ]
  edge [
    source 35
    target 633
  ]
  edge [
    source 35
    target 1918
  ]
  edge [
    source 35
    target 1919
  ]
  edge [
    source 35
    target 1920
  ]
  edge [
    source 35
    target 742
  ]
  edge [
    source 35
    target 743
  ]
  edge [
    source 35
    target 744
  ]
  edge [
    source 35
    target 1864
  ]
  edge [
    source 35
    target 1865
  ]
  edge [
    source 35
    target 646
  ]
  edge [
    source 35
    target 1866
  ]
  edge [
    source 35
    target 1867
  ]
  edge [
    source 35
    target 1868
  ]
  edge [
    source 35
    target 1869
  ]
  edge [
    source 35
    target 430
  ]
  edge [
    source 35
    target 1870
  ]
  edge [
    source 35
    target 1921
  ]
  edge [
    source 35
    target 1922
  ]
  edge [
    source 35
    target 145
  ]
  edge [
    source 35
    target 1923
  ]
  edge [
    source 35
    target 1924
  ]
  edge [
    source 35
    target 1925
  ]
  edge [
    source 35
    target 1926
  ]
  edge [
    source 35
    target 1927
  ]
  edge [
    source 35
    target 1928
  ]
  edge [
    source 35
    target 1929
  ]
  edge [
    source 35
    target 1930
  ]
  edge [
    source 35
    target 1931
  ]
  edge [
    source 35
    target 323
  ]
  edge [
    source 35
    target 1932
  ]
  edge [
    source 35
    target 1432
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1584
  ]
  edge [
    source 36
    target 1933
  ]
  edge [
    source 36
    target 1934
  ]
  edge [
    source 36
    target 1935
  ]
  edge [
    source 36
    target 1936
  ]
  edge [
    source 36
    target 1864
  ]
  edge [
    source 36
    target 434
  ]
  edge [
    source 36
    target 1937
  ]
  edge [
    source 36
    target 1938
  ]
  edge [
    source 36
    target 1939
  ]
  edge [
    source 36
    target 1940
  ]
  edge [
    source 36
    target 1941
  ]
  edge [
    source 36
    target 1942
  ]
  edge [
    source 36
    target 1943
  ]
  edge [
    source 36
    target 1944
  ]
  edge [
    source 36
    target 1945
  ]
  edge [
    source 36
    target 1946
  ]
  edge [
    source 36
    target 1947
  ]
  edge [
    source 36
    target 1948
  ]
  edge [
    source 36
    target 1949
  ]
  edge [
    source 36
    target 1950
  ]
  edge [
    source 36
    target 1951
  ]
  edge [
    source 36
    target 1758
  ]
  edge [
    source 36
    target 1952
  ]
  edge [
    source 36
    target 461
  ]
  edge [
    source 36
    target 750
  ]
  edge [
    source 36
    target 401
  ]
  edge [
    source 36
    target 1770
  ]
  edge [
    source 36
    target 1953
  ]
  edge [
    source 36
    target 1761
  ]
  edge [
    source 36
    target 1766
  ]
  edge [
    source 36
    target 1954
  ]
  edge [
    source 36
    target 1861
  ]
  edge [
    source 36
    target 842
  ]
  edge [
    source 36
    target 490
  ]
  edge [
    source 36
    target 749
  ]
  edge [
    source 36
    target 1955
  ]
  edge [
    source 36
    target 1956
  ]
  edge [
    source 36
    target 1957
  ]
  edge [
    source 36
    target 1958
  ]
  edge [
    source 36
    target 1959
  ]
  edge [
    source 36
    target 1960
  ]
  edge [
    source 36
    target 1961
  ]
  edge [
    source 36
    target 512
  ]
  edge [
    source 36
    target 1962
  ]
  edge [
    source 36
    target 1963
  ]
  edge [
    source 36
    target 1964
  ]
  edge [
    source 36
    target 1965
  ]
  edge [
    source 36
    target 1966
  ]
  edge [
    source 36
    target 1967
  ]
  edge [
    source 36
    target 1968
  ]
  edge [
    source 36
    target 79
  ]
  edge [
    source 36
    target 1969
  ]
  edge [
    source 36
    target 507
  ]
  edge [
    source 36
    target 1970
  ]
  edge [
    source 36
    target 1971
  ]
  edge [
    source 36
    target 1972
  ]
  edge [
    source 36
    target 1973
  ]
  edge [
    source 36
    target 1974
  ]
  edge [
    source 36
    target 1975
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1976
  ]
  edge [
    source 37
    target 1977
  ]
  edge [
    source 37
    target 1978
  ]
  edge [
    source 37
    target 1979
  ]
  edge [
    source 37
    target 1980
  ]
  edge [
    source 37
    target 1981
  ]
  edge [
    source 37
    target 1982
  ]
  edge [
    source 37
    target 1983
  ]
  edge [
    source 37
    target 1984
  ]
  edge [
    source 37
    target 1985
  ]
  edge [
    source 37
    target 1986
  ]
  edge [
    source 37
    target 1987
  ]
  edge [
    source 37
    target 1988
  ]
  edge [
    source 37
    target 1989
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1990
  ]
  edge [
    source 38
    target 1991
  ]
  edge [
    source 38
    target 1389
  ]
  edge [
    source 38
    target 1992
  ]
  edge [
    source 38
    target 1993
  ]
  edge [
    source 38
    target 1994
  ]
  edge [
    source 38
    target 310
  ]
  edge [
    source 38
    target 1995
  ]
  edge [
    source 38
    target 1996
  ]
  edge [
    source 38
    target 1997
  ]
  edge [
    source 38
    target 1399
  ]
  edge [
    source 38
    target 1998
  ]
  edge [
    source 38
    target 1999
  ]
  edge [
    source 38
    target 2000
  ]
  edge [
    source 38
    target 2001
  ]
  edge [
    source 38
    target 1390
  ]
  edge [
    source 38
    target 2002
  ]
  edge [
    source 38
    target 2003
  ]
  edge [
    source 38
    target 2004
  ]
  edge [
    source 38
    target 1889
  ]
  edge [
    source 38
    target 2005
  ]
  edge [
    source 38
    target 324
  ]
  edge [
    source 38
    target 1384
  ]
  edge [
    source 38
    target 1385
  ]
  edge [
    source 38
    target 1386
  ]
  edge [
    source 38
    target 1387
  ]
  edge [
    source 38
    target 1388
  ]
  edge [
    source 38
    target 1391
  ]
  edge [
    source 38
    target 1392
  ]
  edge [
    source 38
    target 1393
  ]
  edge [
    source 38
    target 1394
  ]
  edge [
    source 38
    target 1395
  ]
  edge [
    source 38
    target 1396
  ]
  edge [
    source 38
    target 1382
  ]
  edge [
    source 38
    target 1186
  ]
  edge [
    source 38
    target 1397
  ]
  edge [
    source 38
    target 1398
  ]
  edge [
    source 38
    target 2006
  ]
  edge [
    source 38
    target 2007
  ]
  edge [
    source 38
    target 79
  ]
  edge [
    source 38
    target 647
  ]
  edge [
    source 38
    target 2008
  ]
  edge [
    source 38
    target 1363
  ]
  edge [
    source 38
    target 2009
  ]
  edge [
    source 38
    target 1888
  ]
  edge [
    source 38
    target 2010
  ]
  edge [
    source 38
    target 2011
  ]
  edge [
    source 38
    target 2012
  ]
  edge [
    source 38
    target 2013
  ]
  edge [
    source 38
    target 2014
  ]
  edge [
    source 38
    target 2015
  ]
  edge [
    source 38
    target 2016
  ]
  edge [
    source 38
    target 2017
  ]
  edge [
    source 38
    target 2018
  ]
  edge [
    source 38
    target 2019
  ]
  edge [
    source 38
    target 2020
  ]
  edge [
    source 38
    target 2021
  ]
  edge [
    source 38
    target 2022
  ]
  edge [
    source 38
    target 2023
  ]
  edge [
    source 38
    target 2024
  ]
  edge [
    source 38
    target 2025
  ]
  edge [
    source 38
    target 2026
  ]
  edge [
    source 38
    target 2027
  ]
  edge [
    source 38
    target 2028
  ]
  edge [
    source 38
    target 2029
  ]
  edge [
    source 38
    target 2030
  ]
  edge [
    source 38
    target 2031
  ]
  edge [
    source 38
    target 2032
  ]
  edge [
    source 38
    target 2033
  ]
  edge [
    source 38
    target 2034
  ]
  edge [
    source 38
    target 2035
  ]
  edge [
    source 38
    target 2036
  ]
  edge [
    source 38
    target 2037
  ]
  edge [
    source 38
    target 2038
  ]
  edge [
    source 38
    target 2039
  ]
  edge [
    source 38
    target 2040
  ]
  edge [
    source 38
    target 471
  ]
  edge [
    source 38
    target 2041
  ]
  edge [
    source 38
    target 2042
  ]
  edge [
    source 38
    target 2043
  ]
  edge [
    source 38
    target 2044
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 2045
  ]
  edge [
    source 39
    target 2046
  ]
  edge [
    source 39
    target 2047
  ]
  edge [
    source 39
    target 2048
  ]
  edge [
    source 39
    target 2049
  ]
  edge [
    source 39
    target 2050
  ]
  edge [
    source 39
    target 1405
  ]
  edge [
    source 39
    target 2051
  ]
  edge [
    source 39
    target 2052
  ]
  edge [
    source 39
    target 2053
  ]
  edge [
    source 39
    target 2054
  ]
  edge [
    source 39
    target 2055
  ]
  edge [
    source 39
    target 2056
  ]
  edge [
    source 39
    target 2057
  ]
  edge [
    source 39
    target 2058
  ]
  edge [
    source 39
    target 2059
  ]
  edge [
    source 39
    target 2060
  ]
  edge [
    source 39
    target 2061
  ]
  edge [
    source 39
    target 297
  ]
  edge [
    source 39
    target 324
  ]
  edge [
    source 39
    target 2062
  ]
  edge [
    source 39
    target 2063
  ]
  edge [
    source 39
    target 2064
  ]
  edge [
    source 39
    target 2065
  ]
  edge [
    source 39
    target 1837
  ]
  edge [
    source 39
    target 1838
  ]
  edge [
    source 39
    target 1839
  ]
  edge [
    source 39
    target 1840
  ]
  edge [
    source 39
    target 1841
  ]
  edge [
    source 39
    target 1842
  ]
  edge [
    source 39
    target 1843
  ]
  edge [
    source 39
    target 1382
  ]
  edge [
    source 39
    target 1844
  ]
  edge [
    source 39
    target 1845
  ]
  edge [
    source 39
    target 1846
  ]
  edge [
    source 39
    target 175
  ]
  edge [
    source 39
    target 2066
  ]
  edge [
    source 39
    target 1210
  ]
  edge [
    source 39
    target 1131
  ]
  edge [
    source 39
    target 301
  ]
  edge [
    source 39
    target 870
  ]
  edge [
    source 39
    target 1090
  ]
  edge [
    source 39
    target 2067
  ]
  edge [
    source 39
    target 2068
  ]
  edge [
    source 39
    target 990
  ]
  edge [
    source 39
    target 2069
  ]
  edge [
    source 39
    target 2070
  ]
  edge [
    source 39
    target 2071
  ]
  edge [
    source 39
    target 2072
  ]
  edge [
    source 39
    target 2073
  ]
  edge [
    source 39
    target 2074
  ]
  edge [
    source 39
    target 1233
  ]
  edge [
    source 39
    target 1146
  ]
  edge [
    source 39
    target 2075
  ]
  edge [
    source 39
    target 2076
  ]
  edge [
    source 39
    target 2077
  ]
  edge [
    source 39
    target 1022
  ]
  edge [
    source 39
    target 2078
  ]
  edge [
    source 39
    target 2079
  ]
  edge [
    source 39
    target 1066
  ]
  edge [
    source 39
    target 2080
  ]
  edge [
    source 39
    target 2081
  ]
  edge [
    source 39
    target 1199
  ]
  edge [
    source 39
    target 2082
  ]
  edge [
    source 39
    target 2083
  ]
  edge [
    source 39
    target 2084
  ]
  edge [
    source 39
    target 2085
  ]
  edge [
    source 39
    target 2086
  ]
  edge [
    source 39
    target 2087
  ]
  edge [
    source 39
    target 2088
  ]
  edge [
    source 39
    target 2089
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 2090
  ]
  edge [
    source 40
    target 2091
  ]
  edge [
    source 40
    target 2092
  ]
  edge [
    source 40
    target 2093
  ]
  edge [
    source 40
    target 2054
  ]
  edge [
    source 40
    target 2094
  ]
  edge [
    source 40
    target 2095
  ]
  edge [
    source 40
    target 2096
  ]
  edge [
    source 40
    target 2097
  ]
  edge [
    source 40
    target 2098
  ]
  edge [
    source 40
    target 2099
  ]
  edge [
    source 40
    target 2100
  ]
  edge [
    source 40
    target 2101
  ]
  edge [
    source 40
    target 299
  ]
  edge [
    source 40
    target 2102
  ]
  edge [
    source 40
    target 2103
  ]
  edge [
    source 40
    target 2104
  ]
  edge [
    source 40
    target 2105
  ]
  edge [
    source 40
    target 2106
  ]
  edge [
    source 40
    target 2107
  ]
  edge [
    source 40
    target 2108
  ]
  edge [
    source 40
    target 2109
  ]
  edge [
    source 40
    target 2110
  ]
  edge [
    source 40
    target 2111
  ]
  edge [
    source 40
    target 2112
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1859
  ]
  edge [
    source 41
    target 1860
  ]
  edge [
    source 41
    target 1286
  ]
  edge [
    source 41
    target 145
  ]
  edge [
    source 41
    target 1861
  ]
  edge [
    source 41
    target 484
  ]
  edge [
    source 41
    target 442
  ]
  edge [
    source 41
    target 601
  ]
  edge [
    source 41
    target 2113
  ]
  edge [
    source 41
    target 2114
  ]
  edge [
    source 41
    target 2115
  ]
  edge [
    source 41
    target 2116
  ]
  edge [
    source 41
    target 2117
  ]
  edge [
    source 41
    target 2118
  ]
  edge [
    source 41
    target 2119
  ]
  edge [
    source 41
    target 2120
  ]
  edge [
    source 41
    target 2121
  ]
  edge [
    source 41
    target 2122
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 2123
  ]
  edge [
    source 41
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 41
    target 2124
  ]
  edge [
    source 41
    target 2125
  ]
  edge [
    source 41
    target 2126
  ]
  edge [
    source 41
    target 2127
  ]
  edge [
    source 41
    target 364
  ]
  edge [
    source 41
    target 2128
  ]
  edge [
    source 41
    target 2043
  ]
  edge [
    source 41
    target 2129
  ]
  edge [
    source 41
    target 2130
  ]
  edge [
    source 41
    target 2131
  ]
  edge [
    source 41
    target 2132
  ]
  edge [
    source 41
    target 2133
  ]
  edge [
    source 41
    target 796
  ]
  edge [
    source 41
    target 2134
  ]
  edge [
    source 41
    target 373
  ]
  edge [
    source 41
    target 2135
  ]
  edge [
    source 41
    target 2136
  ]
  edge [
    source 41
    target 1949
  ]
  edge [
    source 41
    target 2137
  ]
  edge [
    source 41
    target 1255
  ]
  edge [
    source 41
    target 387
  ]
  edge [
    source 41
    target 2138
  ]
  edge [
    source 41
    target 2139
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2140
  ]
  edge [
    source 43
    target 2141
  ]
  edge [
    source 43
    target 2142
  ]
  edge [
    source 43
    target 2143
  ]
  edge [
    source 43
    target 2144
  ]
  edge [
    source 43
    target 2145
  ]
  edge [
    source 43
    target 2146
  ]
  edge [
    source 43
    target 2147
  ]
  edge [
    source 43
    target 2148
  ]
  edge [
    source 43
    target 2134
  ]
  edge [
    source 43
    target 2149
  ]
  edge [
    source 43
    target 2150
  ]
  edge [
    source 43
    target 324
  ]
  edge [
    source 43
    target 2151
  ]
  edge [
    source 43
    target 2048
  ]
  edge [
    source 43
    target 2152
  ]
  edge [
    source 43
    target 2153
  ]
  edge [
    source 43
    target 2154
  ]
  edge [
    source 43
    target 2155
  ]
  edge [
    source 43
    target 2156
  ]
  edge [
    source 43
    target 1935
  ]
  edge [
    source 43
    target 2157
  ]
  edge [
    source 43
    target 2158
  ]
  edge [
    source 43
    target 2159
  ]
  edge [
    source 43
    target 2160
  ]
  edge [
    source 43
    target 2161
  ]
  edge [
    source 43
    target 2162
  ]
  edge [
    source 43
    target 2163
  ]
  edge [
    source 43
    target 2164
  ]
  edge [
    source 43
    target 2165
  ]
  edge [
    source 43
    target 2166
  ]
  edge [
    source 43
    target 484
  ]
  edge [
    source 43
    target 2167
  ]
  edge [
    source 43
    target 2168
  ]
  edge [
    source 43
    target 2169
  ]
  edge [
    source 43
    target 2170
  ]
  edge [
    source 43
    target 601
  ]
  edge [
    source 43
    target 2171
  ]
  edge [
    source 43
    target 2172
  ]
  edge [
    source 43
    target 1939
  ]
  edge [
    source 43
    target 1941
  ]
  edge [
    source 43
    target 2173
  ]
  edge [
    source 43
    target 1945
  ]
  edge [
    source 43
    target 2174
  ]
  edge [
    source 43
    target 2175
  ]
  edge [
    source 43
    target 1262
  ]
  edge [
    source 43
    target 2176
  ]
  edge [
    source 43
    target 2177
  ]
  edge [
    source 43
    target 2178
  ]
  edge [
    source 43
    target 671
  ]
  edge [
    source 43
    target 2179
  ]
  edge [
    source 43
    target 2180
  ]
  edge [
    source 43
    target 2181
  ]
  edge [
    source 43
    target 2182
  ]
  edge [
    source 43
    target 2183
  ]
  edge [
    source 43
    target 2184
  ]
  edge [
    source 43
    target 1946
  ]
  edge [
    source 43
    target 2185
  ]
  edge [
    source 43
    target 2186
  ]
  edge [
    source 43
    target 1767
  ]
  edge [
    source 43
    target 2187
  ]
  edge [
    source 43
    target 1936
  ]
  edge [
    source 43
    target 2188
  ]
  edge [
    source 43
    target 2189
  ]
  edge [
    source 43
    target 2190
  ]
  edge [
    source 43
    target 2191
  ]
  edge [
    source 43
    target 2192
  ]
  edge [
    source 43
    target 648
  ]
  edge [
    source 43
    target 2193
  ]
  edge [
    source 43
    target 2194
  ]
  edge [
    source 43
    target 2195
  ]
  edge [
    source 43
    target 658
  ]
  edge [
    source 43
    target 2196
  ]
  edge [
    source 43
    target 2197
  ]
  edge [
    source 43
    target 407
  ]
  edge [
    source 44
    target 1253
  ]
  edge [
    source 44
    target 2198
  ]
  edge [
    source 44
    target 2199
  ]
  edge [
    source 44
    target 2200
  ]
  edge [
    source 44
    target 2201
  ]
  edge [
    source 44
    target 2202
  ]
  edge [
    source 44
    target 1892
  ]
  edge [
    source 44
    target 730
  ]
  edge [
    source 44
    target 2203
  ]
  edge [
    source 44
    target 2204
  ]
  edge [
    source 44
    target 2205
  ]
  edge [
    source 44
    target 2206
  ]
  edge [
    source 44
    target 98
  ]
  edge [
    source 44
    target 2207
  ]
  edge [
    source 44
    target 613
  ]
  edge [
    source 44
    target 95
  ]
  edge [
    source 44
    target 2208
  ]
  edge [
    source 44
    target 2209
  ]
  edge [
    source 44
    target 2210
  ]
  edge [
    source 44
    target 2211
  ]
  edge [
    source 44
    target 2212
  ]
  edge [
    source 44
    target 623
  ]
  edge [
    source 44
    target 2213
  ]
  edge [
    source 44
    target 1270
  ]
  edge [
    source 44
    target 1271
  ]
  edge [
    source 44
    target 1266
  ]
  edge [
    source 44
    target 1272
  ]
  edge [
    source 44
    target 1273
  ]
  edge [
    source 44
    target 1274
  ]
  edge [
    source 44
    target 625
  ]
  edge [
    source 44
    target 1275
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 2214
  ]
  edge [
    source 45
    target 401
  ]
  edge [
    source 45
    target 1786
  ]
  edge [
    source 45
    target 2215
  ]
  edge [
    source 45
    target 2216
  ]
  edge [
    source 45
    target 2217
  ]
  edge [
    source 45
    target 1755
  ]
  edge [
    source 45
    target 2218
  ]
  edge [
    source 45
    target 2219
  ]
  edge [
    source 45
    target 2220
  ]
  edge [
    source 45
    target 2221
  ]
  edge [
    source 45
    target 2222
  ]
  edge [
    source 45
    target 2223
  ]
  edge [
    source 45
    target 2224
  ]
  edge [
    source 45
    target 2225
  ]
  edge [
    source 45
    target 2226
  ]
  edge [
    source 45
    target 2227
  ]
  edge [
    source 45
    target 509
  ]
  edge [
    source 45
    target 2228
  ]
  edge [
    source 45
    target 2229
  ]
  edge [
    source 45
    target 2230
  ]
  edge [
    source 45
    target 2231
  ]
  edge [
    source 45
    target 2232
  ]
  edge [
    source 45
    target 2233
  ]
  edge [
    source 45
    target 2234
  ]
  edge [
    source 45
    target 2235
  ]
  edge [
    source 45
    target 2236
  ]
  edge [
    source 45
    target 2237
  ]
  edge [
    source 45
    target 2238
  ]
  edge [
    source 45
    target 486
  ]
  edge [
    source 45
    target 2239
  ]
  edge [
    source 45
    target 2240
  ]
  edge [
    source 45
    target 2241
  ]
  edge [
    source 45
    target 2242
  ]
  edge [
    source 45
    target 2243
  ]
  edge [
    source 45
    target 2244
  ]
  edge [
    source 45
    target 2245
  ]
  edge [
    source 45
    target 2246
  ]
  edge [
    source 45
    target 2247
  ]
  edge [
    source 45
    target 2248
  ]
  edge [
    source 45
    target 759
  ]
  edge [
    source 45
    target 2249
  ]
  edge [
    source 45
    target 2250
  ]
  edge [
    source 45
    target 2251
  ]
  edge [
    source 45
    target 2252
  ]
  edge [
    source 45
    target 633
  ]
  edge [
    source 45
    target 561
  ]
  edge [
    source 45
    target 2253
  ]
  edge [
    source 45
    target 2254
  ]
  edge [
    source 45
    target 1925
  ]
  edge [
    source 45
    target 2255
  ]
  edge [
    source 45
    target 2256
  ]
  edge [
    source 45
    target 1442
  ]
  edge [
    source 45
    target 2257
  ]
  edge [
    source 45
    target 2258
  ]
  edge [
    source 45
    target 2259
  ]
  edge [
    source 45
    target 2260
  ]
  edge [
    source 45
    target 2261
  ]
  edge [
    source 45
    target 2262
  ]
  edge [
    source 45
    target 2263
  ]
  edge [
    source 45
    target 2264
  ]
  edge [
    source 45
    target 2265
  ]
  edge [
    source 45
    target 2266
  ]
  edge [
    source 45
    target 2267
  ]
  edge [
    source 45
    target 2268
  ]
  edge [
    source 45
    target 2269
  ]
  edge [
    source 45
    target 2270
  ]
  edge [
    source 45
    target 2271
  ]
  edge [
    source 45
    target 1751
  ]
  edge [
    source 45
    target 2272
  ]
  edge [
    source 45
    target 2273
  ]
  edge [
    source 45
    target 1785
  ]
  edge [
    source 45
    target 1789
  ]
  edge [
    source 45
    target 1790
  ]
  edge [
    source 45
    target 1791
  ]
  edge [
    source 45
    target 1792
  ]
  edge [
    source 45
    target 1793
  ]
  edge [
    source 45
    target 1794
  ]
  edge [
    source 45
    target 2274
  ]
  edge [
    source 45
    target 2275
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2276
  ]
  edge [
    source 46
    target 2277
  ]
  edge [
    source 46
    target 2214
  ]
  edge [
    source 46
    target 401
  ]
  edge [
    source 46
    target 1786
  ]
  edge [
    source 46
    target 2215
  ]
  edge [
    source 46
    target 2216
  ]
  edge [
    source 46
    target 2217
  ]
  edge [
    source 46
    target 1755
  ]
  edge [
    source 46
    target 2218
  ]
  edge [
    source 46
    target 340
  ]
  edge [
    source 46
    target 2278
  ]
  edge [
    source 46
    target 2279
  ]
  edge [
    source 46
    target 351
  ]
  edge [
    source 46
    target 2230
  ]
  edge [
    source 46
    target 2280
  ]
  edge [
    source 46
    target 2281
  ]
  edge [
    source 46
    target 2282
  ]
  edge [
    source 46
    target 2283
  ]
  edge [
    source 47
    target 2284
  ]
  edge [
    source 47
    target 1405
  ]
  edge [
    source 47
    target 2285
  ]
  edge [
    source 47
    target 2286
  ]
  edge [
    source 47
    target 1995
  ]
  edge [
    source 47
    target 2287
  ]
  edge [
    source 47
    target 1399
  ]
  edge [
    source 47
    target 1837
  ]
  edge [
    source 47
    target 1838
  ]
  edge [
    source 47
    target 1839
  ]
  edge [
    source 47
    target 1840
  ]
  edge [
    source 47
    target 1841
  ]
  edge [
    source 47
    target 1842
  ]
  edge [
    source 47
    target 1843
  ]
  edge [
    source 47
    target 1382
  ]
  edge [
    source 47
    target 1844
  ]
  edge [
    source 47
    target 1845
  ]
  edge [
    source 47
    target 1846
  ]
  edge [
    source 2288
    target 2289
  ]
  edge [
    source 2290
    target 2291
  ]
  edge [
    source 2292
    target 2293
  ]
]
