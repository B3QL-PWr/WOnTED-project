graph [
  node [
    id 0
    label "unia"
    origin "text"
  ]
  node [
    id 1
    label "europejski"
    origin "text"
  ]
  node [
    id 2
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "obecnie"
    origin "text"
  ]
  node [
    id 4
    label "nad"
    origin "text"
  ]
  node [
    id 5
    label "nowa"
    origin "text"
  ]
  node [
    id 6
    label "strategia"
    origin "text"
  ]
  node [
    id 7
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 8
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 9
    label "informacyjny"
    origin "text"
  ]
  node [
    id 10
    label "lata"
    origin "text"
  ]
  node [
    id 11
    label "rama"
    origin "text"
  ]
  node [
    id 12
    label "praca"
    origin "text"
  ]
  node [
    id 13
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "konsultacja"
    origin "text"
  ]
  node [
    id 15
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "wynik"
    origin "text"
  ]
  node [
    id 18
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 19
    label "uwzgl&#281;dni&#263;"
    origin "text"
  ]
  node [
    id 20
    label "podczas"
    origin "text"
  ]
  node [
    id 21
    label "formu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "organizacja"
  ]
  node [
    id 23
    label "uk&#322;ad"
  ]
  node [
    id 24
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 25
    label "Unia"
  ]
  node [
    id 26
    label "combination"
  ]
  node [
    id 27
    label "Unia_Europejska"
  ]
  node [
    id 28
    label "union"
  ]
  node [
    id 29
    label "partia"
  ]
  node [
    id 30
    label "podmiot"
  ]
  node [
    id 31
    label "jednostka_organizacyjna"
  ]
  node [
    id 32
    label "struktura"
  ]
  node [
    id 33
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 34
    label "TOPR"
  ]
  node [
    id 35
    label "endecki"
  ]
  node [
    id 36
    label "zesp&#243;&#322;"
  ]
  node [
    id 37
    label "przedstawicielstwo"
  ]
  node [
    id 38
    label "od&#322;am"
  ]
  node [
    id 39
    label "Cepelia"
  ]
  node [
    id 40
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 41
    label "ZBoWiD"
  ]
  node [
    id 42
    label "organization"
  ]
  node [
    id 43
    label "centrala"
  ]
  node [
    id 44
    label "GOPR"
  ]
  node [
    id 45
    label "ZOMO"
  ]
  node [
    id 46
    label "ZMP"
  ]
  node [
    id 47
    label "komitet_koordynacyjny"
  ]
  node [
    id 48
    label "przybud&#243;wka"
  ]
  node [
    id 49
    label "boj&#243;wka"
  ]
  node [
    id 50
    label "Bund"
  ]
  node [
    id 51
    label "PPR"
  ]
  node [
    id 52
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 53
    label "wybranek"
  ]
  node [
    id 54
    label "Jakobici"
  ]
  node [
    id 55
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 56
    label "SLD"
  ]
  node [
    id 57
    label "Razem"
  ]
  node [
    id 58
    label "PiS"
  ]
  node [
    id 59
    label "package"
  ]
  node [
    id 60
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 61
    label "Kuomintang"
  ]
  node [
    id 62
    label "ZSL"
  ]
  node [
    id 63
    label "AWS"
  ]
  node [
    id 64
    label "gra"
  ]
  node [
    id 65
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 66
    label "game"
  ]
  node [
    id 67
    label "grupa"
  ]
  node [
    id 68
    label "blok"
  ]
  node [
    id 69
    label "materia&#322;"
  ]
  node [
    id 70
    label "PO"
  ]
  node [
    id 71
    label "si&#322;a"
  ]
  node [
    id 72
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 73
    label "niedoczas"
  ]
  node [
    id 74
    label "Federali&#347;ci"
  ]
  node [
    id 75
    label "PSL"
  ]
  node [
    id 76
    label "Wigowie"
  ]
  node [
    id 77
    label "ZChN"
  ]
  node [
    id 78
    label "egzekutywa"
  ]
  node [
    id 79
    label "aktyw"
  ]
  node [
    id 80
    label "wybranka"
  ]
  node [
    id 81
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 82
    label "unit"
  ]
  node [
    id 83
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 84
    label "rozprz&#261;c"
  ]
  node [
    id 85
    label "treaty"
  ]
  node [
    id 86
    label "systemat"
  ]
  node [
    id 87
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 88
    label "system"
  ]
  node [
    id 89
    label "umowa"
  ]
  node [
    id 90
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 91
    label "usenet"
  ]
  node [
    id 92
    label "przestawi&#263;"
  ]
  node [
    id 93
    label "zbi&#243;r"
  ]
  node [
    id 94
    label "alliance"
  ]
  node [
    id 95
    label "ONZ"
  ]
  node [
    id 96
    label "NATO"
  ]
  node [
    id 97
    label "konstelacja"
  ]
  node [
    id 98
    label "o&#347;"
  ]
  node [
    id 99
    label "podsystem"
  ]
  node [
    id 100
    label "zawarcie"
  ]
  node [
    id 101
    label "zawrze&#263;"
  ]
  node [
    id 102
    label "organ"
  ]
  node [
    id 103
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 104
    label "wi&#281;&#378;"
  ]
  node [
    id 105
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 106
    label "zachowanie"
  ]
  node [
    id 107
    label "cybernetyk"
  ]
  node [
    id 108
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 109
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 110
    label "sk&#322;ad"
  ]
  node [
    id 111
    label "traktat_wersalski"
  ]
  node [
    id 112
    label "cia&#322;o"
  ]
  node [
    id 113
    label "eurosceptycyzm"
  ]
  node [
    id 114
    label "euroentuzjasta"
  ]
  node [
    id 115
    label "euroentuzjazm"
  ]
  node [
    id 116
    label "euroko&#322;choz"
  ]
  node [
    id 117
    label "strefa_euro"
  ]
  node [
    id 118
    label "eurorealizm"
  ]
  node [
    id 119
    label "Bruksela"
  ]
  node [
    id 120
    label "Eurogrupa"
  ]
  node [
    id 121
    label "eurorealista"
  ]
  node [
    id 122
    label "eurosceptyczny"
  ]
  node [
    id 123
    label "eurosceptyk"
  ]
  node [
    id 124
    label "Wsp&#243;lnota_Europejska"
  ]
  node [
    id 125
    label "prawo_unijne"
  ]
  node [
    id 126
    label "Fundusze_Unijne"
  ]
  node [
    id 127
    label "p&#322;atnik_netto"
  ]
  node [
    id 128
    label "po_europejsku"
  ]
  node [
    id 129
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 130
    label "European"
  ]
  node [
    id 131
    label "typowy"
  ]
  node [
    id 132
    label "charakterystyczny"
  ]
  node [
    id 133
    label "europejsko"
  ]
  node [
    id 134
    label "charakterystycznie"
  ]
  node [
    id 135
    label "szczeg&#243;lny"
  ]
  node [
    id 136
    label "wyj&#261;tkowy"
  ]
  node [
    id 137
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 138
    label "podobny"
  ]
  node [
    id 139
    label "zwyczajny"
  ]
  node [
    id 140
    label "typowo"
  ]
  node [
    id 141
    label "cz&#281;sty"
  ]
  node [
    id 142
    label "zwyk&#322;y"
  ]
  node [
    id 143
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 144
    label "nale&#380;ny"
  ]
  node [
    id 145
    label "nale&#380;yty"
  ]
  node [
    id 146
    label "uprawniony"
  ]
  node [
    id 147
    label "zasadniczy"
  ]
  node [
    id 148
    label "stosownie"
  ]
  node [
    id 149
    label "taki"
  ]
  node [
    id 150
    label "prawdziwy"
  ]
  node [
    id 151
    label "ten"
  ]
  node [
    id 152
    label "dobry"
  ]
  node [
    id 153
    label "endeavor"
  ]
  node [
    id 154
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 155
    label "mie&#263;_miejsce"
  ]
  node [
    id 156
    label "podejmowa&#263;"
  ]
  node [
    id 157
    label "dziama&#263;"
  ]
  node [
    id 158
    label "do"
  ]
  node [
    id 159
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 160
    label "bangla&#263;"
  ]
  node [
    id 161
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 162
    label "work"
  ]
  node [
    id 163
    label "maszyna"
  ]
  node [
    id 164
    label "dzia&#322;a&#263;"
  ]
  node [
    id 165
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 166
    label "tryb"
  ]
  node [
    id 167
    label "funkcjonowa&#263;"
  ]
  node [
    id 168
    label "podnosi&#263;"
  ]
  node [
    id 169
    label "robi&#263;"
  ]
  node [
    id 170
    label "draw"
  ]
  node [
    id 171
    label "drive"
  ]
  node [
    id 172
    label "zmienia&#263;"
  ]
  node [
    id 173
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 174
    label "rise"
  ]
  node [
    id 175
    label "admit"
  ]
  node [
    id 176
    label "reagowa&#263;"
  ]
  node [
    id 177
    label "try"
  ]
  node [
    id 178
    label "post&#281;powa&#263;"
  ]
  node [
    id 179
    label "istnie&#263;"
  ]
  node [
    id 180
    label "function"
  ]
  node [
    id 181
    label "determine"
  ]
  node [
    id 182
    label "powodowa&#263;"
  ]
  node [
    id 183
    label "reakcja_chemiczna"
  ]
  node [
    id 184
    label "commit"
  ]
  node [
    id 185
    label "ut"
  ]
  node [
    id 186
    label "d&#378;wi&#281;k"
  ]
  node [
    id 187
    label "C"
  ]
  node [
    id 188
    label "his"
  ]
  node [
    id 189
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 190
    label "cz&#322;owiek"
  ]
  node [
    id 191
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 192
    label "tuleja"
  ]
  node [
    id 193
    label "pracowanie"
  ]
  node [
    id 194
    label "kad&#322;ub"
  ]
  node [
    id 195
    label "n&#243;&#380;"
  ]
  node [
    id 196
    label "b&#281;benek"
  ]
  node [
    id 197
    label "wa&#322;"
  ]
  node [
    id 198
    label "maszyneria"
  ]
  node [
    id 199
    label "prototypownia"
  ]
  node [
    id 200
    label "trawers"
  ]
  node [
    id 201
    label "deflektor"
  ]
  node [
    id 202
    label "kolumna"
  ]
  node [
    id 203
    label "mechanizm"
  ]
  node [
    id 204
    label "wa&#322;ek"
  ]
  node [
    id 205
    label "b&#281;ben"
  ]
  node [
    id 206
    label "rz&#281;zi&#263;"
  ]
  node [
    id 207
    label "przyk&#322;adka"
  ]
  node [
    id 208
    label "t&#322;ok"
  ]
  node [
    id 209
    label "dehumanizacja"
  ]
  node [
    id 210
    label "rami&#281;"
  ]
  node [
    id 211
    label "rz&#281;&#380;enie"
  ]
  node [
    id 212
    label "urz&#261;dzenie"
  ]
  node [
    id 213
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 214
    label "najem"
  ]
  node [
    id 215
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 216
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 217
    label "zak&#322;ad"
  ]
  node [
    id 218
    label "stosunek_pracy"
  ]
  node [
    id 219
    label "benedykty&#324;ski"
  ]
  node [
    id 220
    label "poda&#380;_pracy"
  ]
  node [
    id 221
    label "tyrka"
  ]
  node [
    id 222
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 223
    label "wytw&#243;r"
  ]
  node [
    id 224
    label "miejsce"
  ]
  node [
    id 225
    label "zaw&#243;d"
  ]
  node [
    id 226
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 227
    label "tynkarski"
  ]
  node [
    id 228
    label "czynno&#347;&#263;"
  ]
  node [
    id 229
    label "zmiana"
  ]
  node [
    id 230
    label "czynnik_produkcji"
  ]
  node [
    id 231
    label "zobowi&#261;zanie"
  ]
  node [
    id 232
    label "kierownictwo"
  ]
  node [
    id 233
    label "siedziba"
  ]
  node [
    id 234
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 235
    label "ko&#322;o"
  ]
  node [
    id 236
    label "spos&#243;b"
  ]
  node [
    id 237
    label "modalno&#347;&#263;"
  ]
  node [
    id 238
    label "z&#261;b"
  ]
  node [
    id 239
    label "cecha"
  ]
  node [
    id 240
    label "kategoria_gramatyczna"
  ]
  node [
    id 241
    label "skala"
  ]
  node [
    id 242
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 243
    label "koniugacja"
  ]
  node [
    id 244
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 245
    label "rozumie&#263;"
  ]
  node [
    id 246
    label "szczeka&#263;"
  ]
  node [
    id 247
    label "rozmawia&#263;"
  ]
  node [
    id 248
    label "m&#243;wi&#263;"
  ]
  node [
    id 249
    label "ninie"
  ]
  node [
    id 250
    label "aktualny"
  ]
  node [
    id 251
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 252
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 253
    label "jednocze&#347;nie"
  ]
  node [
    id 254
    label "aktualnie"
  ]
  node [
    id 255
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 256
    label "wa&#380;ny"
  ]
  node [
    id 257
    label "aktualizowanie"
  ]
  node [
    id 258
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 259
    label "uaktualnienie"
  ]
  node [
    id 260
    label "gwiazda"
  ]
  node [
    id 261
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 262
    label "Arktur"
  ]
  node [
    id 263
    label "kszta&#322;t"
  ]
  node [
    id 264
    label "Gwiazda_Polarna"
  ]
  node [
    id 265
    label "agregatka"
  ]
  node [
    id 266
    label "gromada"
  ]
  node [
    id 267
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 268
    label "S&#322;o&#324;ce"
  ]
  node [
    id 269
    label "Nibiru"
  ]
  node [
    id 270
    label "ornament"
  ]
  node [
    id 271
    label "delta_Scuti"
  ]
  node [
    id 272
    label "&#347;wiat&#322;o"
  ]
  node [
    id 273
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 274
    label "obiekt"
  ]
  node [
    id 275
    label "s&#322;awa"
  ]
  node [
    id 276
    label "promie&#324;"
  ]
  node [
    id 277
    label "star"
  ]
  node [
    id 278
    label "gwiazdosz"
  ]
  node [
    id 279
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 280
    label "asocjacja_gwiazd"
  ]
  node [
    id 281
    label "supergrupa"
  ]
  node [
    id 282
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 283
    label "plan"
  ]
  node [
    id 284
    label "operacja"
  ]
  node [
    id 285
    label "metoda"
  ]
  node [
    id 286
    label "pocz&#261;tki"
  ]
  node [
    id 287
    label "wzorzec_projektowy"
  ]
  node [
    id 288
    label "dziedzina"
  ]
  node [
    id 289
    label "program"
  ]
  node [
    id 290
    label "doktryna"
  ]
  node [
    id 291
    label "wrinkle"
  ]
  node [
    id 292
    label "dokument"
  ]
  node [
    id 293
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 294
    label "zmienno&#347;&#263;"
  ]
  node [
    id 295
    label "play"
  ]
  node [
    id 296
    label "rozgrywka"
  ]
  node [
    id 297
    label "apparent_motion"
  ]
  node [
    id 298
    label "wydarzenie"
  ]
  node [
    id 299
    label "contest"
  ]
  node [
    id 300
    label "akcja"
  ]
  node [
    id 301
    label "komplet"
  ]
  node [
    id 302
    label "zabawa"
  ]
  node [
    id 303
    label "zasada"
  ]
  node [
    id 304
    label "rywalizacja"
  ]
  node [
    id 305
    label "zbijany"
  ]
  node [
    id 306
    label "post&#281;powanie"
  ]
  node [
    id 307
    label "odg&#322;os"
  ]
  node [
    id 308
    label "Pok&#233;mon"
  ]
  node [
    id 309
    label "synteza"
  ]
  node [
    id 310
    label "odtworzenie"
  ]
  node [
    id 311
    label "rekwizyt_do_gry"
  ]
  node [
    id 312
    label "instalowa&#263;"
  ]
  node [
    id 313
    label "oprogramowanie"
  ]
  node [
    id 314
    label "odinstalowywa&#263;"
  ]
  node [
    id 315
    label "spis"
  ]
  node [
    id 316
    label "zaprezentowanie"
  ]
  node [
    id 317
    label "podprogram"
  ]
  node [
    id 318
    label "ogranicznik_referencyjny"
  ]
  node [
    id 319
    label "course_of_study"
  ]
  node [
    id 320
    label "booklet"
  ]
  node [
    id 321
    label "dzia&#322;"
  ]
  node [
    id 322
    label "odinstalowanie"
  ]
  node [
    id 323
    label "broszura"
  ]
  node [
    id 324
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 325
    label "kana&#322;"
  ]
  node [
    id 326
    label "teleferie"
  ]
  node [
    id 327
    label "zainstalowanie"
  ]
  node [
    id 328
    label "struktura_organizacyjna"
  ]
  node [
    id 329
    label "pirat"
  ]
  node [
    id 330
    label "zaprezentowa&#263;"
  ]
  node [
    id 331
    label "prezentowanie"
  ]
  node [
    id 332
    label "prezentowa&#263;"
  ]
  node [
    id 333
    label "interfejs"
  ]
  node [
    id 334
    label "okno"
  ]
  node [
    id 335
    label "punkt"
  ]
  node [
    id 336
    label "folder"
  ]
  node [
    id 337
    label "zainstalowa&#263;"
  ]
  node [
    id 338
    label "za&#322;o&#380;enie"
  ]
  node [
    id 339
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 340
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 341
    label "ram&#243;wka"
  ]
  node [
    id 342
    label "emitowa&#263;"
  ]
  node [
    id 343
    label "emitowanie"
  ]
  node [
    id 344
    label "odinstalowywanie"
  ]
  node [
    id 345
    label "instrukcja"
  ]
  node [
    id 346
    label "informatyka"
  ]
  node [
    id 347
    label "deklaracja"
  ]
  node [
    id 348
    label "menu"
  ]
  node [
    id 349
    label "sekcja_krytyczna"
  ]
  node [
    id 350
    label "furkacja"
  ]
  node [
    id 351
    label "podstawa"
  ]
  node [
    id 352
    label "instalowanie"
  ]
  node [
    id 353
    label "oferta"
  ]
  node [
    id 354
    label "odinstalowa&#263;"
  ]
  node [
    id 355
    label "zapis"
  ]
  node [
    id 356
    label "&#347;wiadectwo"
  ]
  node [
    id 357
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 358
    label "parafa"
  ]
  node [
    id 359
    label "plik"
  ]
  node [
    id 360
    label "raport&#243;wka"
  ]
  node [
    id 361
    label "utw&#243;r"
  ]
  node [
    id 362
    label "record"
  ]
  node [
    id 363
    label "fascyku&#322;"
  ]
  node [
    id 364
    label "dokumentacja"
  ]
  node [
    id 365
    label "registratura"
  ]
  node [
    id 366
    label "artyku&#322;"
  ]
  node [
    id 367
    label "writing"
  ]
  node [
    id 368
    label "sygnatariusz"
  ]
  node [
    id 369
    label "model"
  ]
  node [
    id 370
    label "intencja"
  ]
  node [
    id 371
    label "rysunek"
  ]
  node [
    id 372
    label "miejsce_pracy"
  ]
  node [
    id 373
    label "przestrze&#324;"
  ]
  node [
    id 374
    label "device"
  ]
  node [
    id 375
    label "pomys&#322;"
  ]
  node [
    id 376
    label "obraz"
  ]
  node [
    id 377
    label "reprezentacja"
  ]
  node [
    id 378
    label "agreement"
  ]
  node [
    id 379
    label "dekoracja"
  ]
  node [
    id 380
    label "perspektywa"
  ]
  node [
    id 381
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 382
    label "sfera"
  ]
  node [
    id 383
    label "zakres"
  ]
  node [
    id 384
    label "funkcja"
  ]
  node [
    id 385
    label "bezdro&#380;e"
  ]
  node [
    id 386
    label "poddzia&#322;"
  ]
  node [
    id 387
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 388
    label "method"
  ]
  node [
    id 389
    label "proces_my&#347;lowy"
  ]
  node [
    id 390
    label "liczenie"
  ]
  node [
    id 391
    label "czyn"
  ]
  node [
    id 392
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 393
    label "supremum"
  ]
  node [
    id 394
    label "laparotomia"
  ]
  node [
    id 395
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 396
    label "jednostka"
  ]
  node [
    id 397
    label "matematyka"
  ]
  node [
    id 398
    label "rzut"
  ]
  node [
    id 399
    label "liczy&#263;"
  ]
  node [
    id 400
    label "torakotomia"
  ]
  node [
    id 401
    label "chirurg"
  ]
  node [
    id 402
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 403
    label "zabieg"
  ]
  node [
    id 404
    label "szew"
  ]
  node [
    id 405
    label "mathematical_process"
  ]
  node [
    id 406
    label "infimum"
  ]
  node [
    id 407
    label "teoria"
  ]
  node [
    id 408
    label "doctrine"
  ]
  node [
    id 409
    label "background"
  ]
  node [
    id 410
    label "dzieci&#281;ctwo"
  ]
  node [
    id 411
    label "podsektor"
  ]
  node [
    id 412
    label "fortyfikacja"
  ]
  node [
    id 413
    label "balistyka"
  ]
  node [
    id 414
    label "taktyka"
  ]
  node [
    id 415
    label "or&#281;&#380;"
  ]
  node [
    id 416
    label "procedura"
  ]
  node [
    id 417
    label "proces"
  ]
  node [
    id 418
    label "&#380;ycie"
  ]
  node [
    id 419
    label "proces_biologiczny"
  ]
  node [
    id 420
    label "z&#322;ote_czasy"
  ]
  node [
    id 421
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 422
    label "process"
  ]
  node [
    id 423
    label "cycle"
  ]
  node [
    id 424
    label "kognicja"
  ]
  node [
    id 425
    label "przebieg"
  ]
  node [
    id 426
    label "rozprawa"
  ]
  node [
    id 427
    label "legislacyjnie"
  ]
  node [
    id 428
    label "przes&#322;anka"
  ]
  node [
    id 429
    label "zjawisko"
  ]
  node [
    id 430
    label "nast&#281;pstwo"
  ]
  node [
    id 431
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 432
    label "raj_utracony"
  ]
  node [
    id 433
    label "umieranie"
  ]
  node [
    id 434
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 435
    label "prze&#380;ywanie"
  ]
  node [
    id 436
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 437
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 438
    label "po&#322;&#243;g"
  ]
  node [
    id 439
    label "umarcie"
  ]
  node [
    id 440
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 441
    label "subsistence"
  ]
  node [
    id 442
    label "power"
  ]
  node [
    id 443
    label "okres_noworodkowy"
  ]
  node [
    id 444
    label "prze&#380;ycie"
  ]
  node [
    id 445
    label "wiek_matuzalemowy"
  ]
  node [
    id 446
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 447
    label "entity"
  ]
  node [
    id 448
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 449
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 450
    label "do&#380;ywanie"
  ]
  node [
    id 451
    label "byt"
  ]
  node [
    id 452
    label "dzieci&#324;stwo"
  ]
  node [
    id 453
    label "andropauza"
  ]
  node [
    id 454
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 455
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 456
    label "czas"
  ]
  node [
    id 457
    label "menopauza"
  ]
  node [
    id 458
    label "&#347;mier&#263;"
  ]
  node [
    id 459
    label "koleje_losu"
  ]
  node [
    id 460
    label "bycie"
  ]
  node [
    id 461
    label "zegar_biologiczny"
  ]
  node [
    id 462
    label "szwung"
  ]
  node [
    id 463
    label "przebywanie"
  ]
  node [
    id 464
    label "warunki"
  ]
  node [
    id 465
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 466
    label "niemowl&#281;ctwo"
  ]
  node [
    id 467
    label "&#380;ywy"
  ]
  node [
    id 468
    label "life"
  ]
  node [
    id 469
    label "staro&#347;&#263;"
  ]
  node [
    id 470
    label "energy"
  ]
  node [
    id 471
    label "brak"
  ]
  node [
    id 472
    label "s&#261;d"
  ]
  node [
    id 473
    label "facylitator"
  ]
  node [
    id 474
    label "metodyka"
  ]
  node [
    id 475
    label "cywilizacja"
  ]
  node [
    id 476
    label "pole"
  ]
  node [
    id 477
    label "elita"
  ]
  node [
    id 478
    label "status"
  ]
  node [
    id 479
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 480
    label "aspo&#322;eczny"
  ]
  node [
    id 481
    label "ludzie_pracy"
  ]
  node [
    id 482
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 483
    label "pozaklasowy"
  ]
  node [
    id 484
    label "uwarstwienie"
  ]
  node [
    id 485
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 486
    label "community"
  ]
  node [
    id 487
    label "klasa"
  ]
  node [
    id 488
    label "kastowo&#347;&#263;"
  ]
  node [
    id 489
    label "facylitacja"
  ]
  node [
    id 490
    label "uprawienie"
  ]
  node [
    id 491
    label "u&#322;o&#380;enie"
  ]
  node [
    id 492
    label "p&#322;osa"
  ]
  node [
    id 493
    label "ziemia"
  ]
  node [
    id 494
    label "t&#322;o"
  ]
  node [
    id 495
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 496
    label "gospodarstwo"
  ]
  node [
    id 497
    label "uprawi&#263;"
  ]
  node [
    id 498
    label "room"
  ]
  node [
    id 499
    label "dw&#243;r"
  ]
  node [
    id 500
    label "okazja"
  ]
  node [
    id 501
    label "rozmiar"
  ]
  node [
    id 502
    label "irygowanie"
  ]
  node [
    id 503
    label "compass"
  ]
  node [
    id 504
    label "square"
  ]
  node [
    id 505
    label "zmienna"
  ]
  node [
    id 506
    label "irygowa&#263;"
  ]
  node [
    id 507
    label "socjologia"
  ]
  node [
    id 508
    label "boisko"
  ]
  node [
    id 509
    label "baza_danych"
  ]
  node [
    id 510
    label "region"
  ]
  node [
    id 511
    label "zagon"
  ]
  node [
    id 512
    label "obszar"
  ]
  node [
    id 513
    label "powierzchnia"
  ]
  node [
    id 514
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 515
    label "plane"
  ]
  node [
    id 516
    label "radlina"
  ]
  node [
    id 517
    label "Fremeni"
  ]
  node [
    id 518
    label "wagon"
  ]
  node [
    id 519
    label "mecz_mistrzowski"
  ]
  node [
    id 520
    label "przedmiot"
  ]
  node [
    id 521
    label "arrangement"
  ]
  node [
    id 522
    label "class"
  ]
  node [
    id 523
    label "&#322;awka"
  ]
  node [
    id 524
    label "wykrzyknik"
  ]
  node [
    id 525
    label "zaleta"
  ]
  node [
    id 526
    label "jednostka_systematyczna"
  ]
  node [
    id 527
    label "programowanie_obiektowe"
  ]
  node [
    id 528
    label "tablica"
  ]
  node [
    id 529
    label "warstwa"
  ]
  node [
    id 530
    label "rezerwa"
  ]
  node [
    id 531
    label "Ekwici"
  ]
  node [
    id 532
    label "&#347;rodowisko"
  ]
  node [
    id 533
    label "szko&#322;a"
  ]
  node [
    id 534
    label "sala"
  ]
  node [
    id 535
    label "pomoc"
  ]
  node [
    id 536
    label "form"
  ]
  node [
    id 537
    label "przepisa&#263;"
  ]
  node [
    id 538
    label "jako&#347;&#263;"
  ]
  node [
    id 539
    label "znak_jako&#347;ci"
  ]
  node [
    id 540
    label "poziom"
  ]
  node [
    id 541
    label "type"
  ]
  node [
    id 542
    label "promocja"
  ]
  node [
    id 543
    label "przepisanie"
  ]
  node [
    id 544
    label "kurs"
  ]
  node [
    id 545
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 546
    label "dziennik_lekcyjny"
  ]
  node [
    id 547
    label "typ"
  ]
  node [
    id 548
    label "fakcja"
  ]
  node [
    id 549
    label "obrona"
  ]
  node [
    id 550
    label "atak"
  ]
  node [
    id 551
    label "botanika"
  ]
  node [
    id 552
    label "elite"
  ]
  node [
    id 553
    label "condition"
  ]
  node [
    id 554
    label "awansowa&#263;"
  ]
  node [
    id 555
    label "znaczenie"
  ]
  node [
    id 556
    label "stan"
  ]
  node [
    id 557
    label "awans"
  ]
  node [
    id 558
    label "podmiotowo"
  ]
  node [
    id 559
    label "awansowanie"
  ]
  node [
    id 560
    label "sytuacja"
  ]
  node [
    id 561
    label "niekorzystny"
  ]
  node [
    id 562
    label "aspo&#322;ecznie"
  ]
  node [
    id 563
    label "niech&#281;tny"
  ]
  node [
    id 564
    label "niskogatunkowy"
  ]
  node [
    id 565
    label "asymilowanie_si&#281;"
  ]
  node [
    id 566
    label "Wsch&#243;d"
  ]
  node [
    id 567
    label "przejmowanie"
  ]
  node [
    id 568
    label "rzecz"
  ]
  node [
    id 569
    label "makrokosmos"
  ]
  node [
    id 570
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 571
    label "civilization"
  ]
  node [
    id 572
    label "przejmowa&#263;"
  ]
  node [
    id 573
    label "faza"
  ]
  node [
    id 574
    label "technika"
  ]
  node [
    id 575
    label "kuchnia"
  ]
  node [
    id 576
    label "populace"
  ]
  node [
    id 577
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 578
    label "przej&#281;cie"
  ]
  node [
    id 579
    label "przej&#261;&#263;"
  ]
  node [
    id 580
    label "cywilizowanie"
  ]
  node [
    id 581
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 582
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 583
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 584
    label "stratification"
  ]
  node [
    id 585
    label "lamination"
  ]
  node [
    id 586
    label "podzia&#322;"
  ]
  node [
    id 587
    label "informacyjnie"
  ]
  node [
    id 588
    label "znaczeniowo"
  ]
  node [
    id 589
    label "summer"
  ]
  node [
    id 590
    label "poprzedzanie"
  ]
  node [
    id 591
    label "czasoprzestrze&#324;"
  ]
  node [
    id 592
    label "laba"
  ]
  node [
    id 593
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 594
    label "chronometria"
  ]
  node [
    id 595
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 596
    label "rachuba_czasu"
  ]
  node [
    id 597
    label "przep&#322;ywanie"
  ]
  node [
    id 598
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 599
    label "czasokres"
  ]
  node [
    id 600
    label "odczyt"
  ]
  node [
    id 601
    label "chwila"
  ]
  node [
    id 602
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 603
    label "dzieje"
  ]
  node [
    id 604
    label "poprzedzenie"
  ]
  node [
    id 605
    label "trawienie"
  ]
  node [
    id 606
    label "pochodzi&#263;"
  ]
  node [
    id 607
    label "period"
  ]
  node [
    id 608
    label "okres_czasu"
  ]
  node [
    id 609
    label "poprzedza&#263;"
  ]
  node [
    id 610
    label "schy&#322;ek"
  ]
  node [
    id 611
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 612
    label "odwlekanie_si&#281;"
  ]
  node [
    id 613
    label "zegar"
  ]
  node [
    id 614
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 615
    label "czwarty_wymiar"
  ]
  node [
    id 616
    label "pochodzenie"
  ]
  node [
    id 617
    label "Zeitgeist"
  ]
  node [
    id 618
    label "trawi&#263;"
  ]
  node [
    id 619
    label "pogoda"
  ]
  node [
    id 620
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 621
    label "poprzedzi&#263;"
  ]
  node [
    id 622
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 623
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 624
    label "time_period"
  ]
  node [
    id 625
    label "dodatek"
  ]
  node [
    id 626
    label "oprawa"
  ]
  node [
    id 627
    label "stela&#380;"
  ]
  node [
    id 628
    label "human_body"
  ]
  node [
    id 629
    label "pojazd"
  ]
  node [
    id 630
    label "paczka"
  ]
  node [
    id 631
    label "obramowanie"
  ]
  node [
    id 632
    label "postawa"
  ]
  node [
    id 633
    label "element_konstrukcyjny"
  ]
  node [
    id 634
    label "szablon"
  ]
  node [
    id 635
    label "dochodzenie"
  ]
  node [
    id 636
    label "doj&#347;cie"
  ]
  node [
    id 637
    label "doch&#243;d"
  ]
  node [
    id 638
    label "dziennik"
  ]
  node [
    id 639
    label "element"
  ]
  node [
    id 640
    label "galanteria"
  ]
  node [
    id 641
    label "doj&#347;&#263;"
  ]
  node [
    id 642
    label "aneks"
  ]
  node [
    id 643
    label "prevention"
  ]
  node [
    id 644
    label "otoczenie"
  ]
  node [
    id 645
    label "framing"
  ]
  node [
    id 646
    label "boarding"
  ]
  node [
    id 647
    label "binda"
  ]
  node [
    id 648
    label "filet"
  ]
  node [
    id 649
    label "Rzym_Zachodni"
  ]
  node [
    id 650
    label "whole"
  ]
  node [
    id 651
    label "ilo&#347;&#263;"
  ]
  node [
    id 652
    label "Rzym_Wschodni"
  ]
  node [
    id 653
    label "granica"
  ]
  node [
    id 654
    label "wielko&#347;&#263;"
  ]
  node [
    id 655
    label "podzakres"
  ]
  node [
    id 656
    label "desygnat"
  ]
  node [
    id 657
    label "circle"
  ]
  node [
    id 658
    label "konstrukcja"
  ]
  node [
    id 659
    label "towarzystwo"
  ]
  node [
    id 660
    label "str&#243;j"
  ]
  node [
    id 661
    label "granda"
  ]
  node [
    id 662
    label "pakunek"
  ]
  node [
    id 663
    label "poczta"
  ]
  node [
    id 664
    label "pakiet"
  ]
  node [
    id 665
    label "baletnica"
  ]
  node [
    id 666
    label "tract"
  ]
  node [
    id 667
    label "przesy&#322;ka"
  ]
  node [
    id 668
    label "opakowanie"
  ]
  node [
    id 669
    label "podwini&#281;cie"
  ]
  node [
    id 670
    label "zap&#322;acenie"
  ]
  node [
    id 671
    label "przyodzianie"
  ]
  node [
    id 672
    label "budowla"
  ]
  node [
    id 673
    label "pokrycie"
  ]
  node [
    id 674
    label "rozebranie"
  ]
  node [
    id 675
    label "zak&#322;adka"
  ]
  node [
    id 676
    label "poubieranie"
  ]
  node [
    id 677
    label "infliction"
  ]
  node [
    id 678
    label "spowodowanie"
  ]
  node [
    id 679
    label "pozak&#322;adanie"
  ]
  node [
    id 680
    label "przebranie"
  ]
  node [
    id 681
    label "przywdzianie"
  ]
  node [
    id 682
    label "obleczenie_si&#281;"
  ]
  node [
    id 683
    label "utworzenie"
  ]
  node [
    id 684
    label "twierdzenie"
  ]
  node [
    id 685
    label "obleczenie"
  ]
  node [
    id 686
    label "umieszczenie"
  ]
  node [
    id 687
    label "przygotowywanie"
  ]
  node [
    id 688
    label "przymierzenie"
  ]
  node [
    id 689
    label "wyko&#324;czenie"
  ]
  node [
    id 690
    label "point"
  ]
  node [
    id 691
    label "przygotowanie"
  ]
  node [
    id 692
    label "proposition"
  ]
  node [
    id 693
    label "przewidzenie"
  ]
  node [
    id 694
    label "zrobienie"
  ]
  node [
    id 695
    label "mechanika"
  ]
  node [
    id 696
    label "nastawienie"
  ]
  node [
    id 697
    label "pozycja"
  ]
  node [
    id 698
    label "attitude"
  ]
  node [
    id 699
    label "mildew"
  ]
  node [
    id 700
    label "jig"
  ]
  node [
    id 701
    label "drabina_analgetyczna"
  ]
  node [
    id 702
    label "wz&#243;r"
  ]
  node [
    id 703
    label "D"
  ]
  node [
    id 704
    label "exemplar"
  ]
  node [
    id 705
    label "odholowa&#263;"
  ]
  node [
    id 706
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 707
    label "tabor"
  ]
  node [
    id 708
    label "przyholowywanie"
  ]
  node [
    id 709
    label "przyholowa&#263;"
  ]
  node [
    id 710
    label "przyholowanie"
  ]
  node [
    id 711
    label "fukni&#281;cie"
  ]
  node [
    id 712
    label "l&#261;d"
  ]
  node [
    id 713
    label "zielona_karta"
  ]
  node [
    id 714
    label "fukanie"
  ]
  node [
    id 715
    label "przyholowywa&#263;"
  ]
  node [
    id 716
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 717
    label "woda"
  ]
  node [
    id 718
    label "przeszklenie"
  ]
  node [
    id 719
    label "test_zderzeniowy"
  ]
  node [
    id 720
    label "powietrze"
  ]
  node [
    id 721
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 722
    label "odzywka"
  ]
  node [
    id 723
    label "nadwozie"
  ]
  node [
    id 724
    label "odholowanie"
  ]
  node [
    id 725
    label "prowadzenie_si&#281;"
  ]
  node [
    id 726
    label "odholowywa&#263;"
  ]
  node [
    id 727
    label "pod&#322;oga"
  ]
  node [
    id 728
    label "odholowywanie"
  ]
  node [
    id 729
    label "hamulec"
  ]
  node [
    id 730
    label "podwozie"
  ]
  node [
    id 731
    label "p&#322;&#243;d"
  ]
  node [
    id 732
    label "rezultat"
  ]
  node [
    id 733
    label "activity"
  ]
  node [
    id 734
    label "bezproblemowy"
  ]
  node [
    id 735
    label "warunek_lokalowy"
  ]
  node [
    id 736
    label "plac"
  ]
  node [
    id 737
    label "location"
  ]
  node [
    id 738
    label "uwaga"
  ]
  node [
    id 739
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 740
    label "rz&#261;d"
  ]
  node [
    id 741
    label "stosunek_prawny"
  ]
  node [
    id 742
    label "oblig"
  ]
  node [
    id 743
    label "uregulowa&#263;"
  ]
  node [
    id 744
    label "oddzia&#322;anie"
  ]
  node [
    id 745
    label "occupation"
  ]
  node [
    id 746
    label "duty"
  ]
  node [
    id 747
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 748
    label "zapowied&#378;"
  ]
  node [
    id 749
    label "obowi&#261;zek"
  ]
  node [
    id 750
    label "statement"
  ]
  node [
    id 751
    label "zapewnienie"
  ]
  node [
    id 752
    label "&#321;ubianka"
  ]
  node [
    id 753
    label "dzia&#322;_personalny"
  ]
  node [
    id 754
    label "Kreml"
  ]
  node [
    id 755
    label "Bia&#322;y_Dom"
  ]
  node [
    id 756
    label "budynek"
  ]
  node [
    id 757
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 758
    label "sadowisko"
  ]
  node [
    id 759
    label "instytucja"
  ]
  node [
    id 760
    label "firma"
  ]
  node [
    id 761
    label "company"
  ]
  node [
    id 762
    label "instytut"
  ]
  node [
    id 763
    label "cierpliwy"
  ]
  node [
    id 764
    label "mozolny"
  ]
  node [
    id 765
    label "wytrwa&#322;y"
  ]
  node [
    id 766
    label "benedykty&#324;sko"
  ]
  node [
    id 767
    label "po_benedykty&#324;sku"
  ]
  node [
    id 768
    label "rewizja"
  ]
  node [
    id 769
    label "passage"
  ]
  node [
    id 770
    label "oznaka"
  ]
  node [
    id 771
    label "change"
  ]
  node [
    id 772
    label "ferment"
  ]
  node [
    id 773
    label "anatomopatolog"
  ]
  node [
    id 774
    label "zmianka"
  ]
  node [
    id 775
    label "amendment"
  ]
  node [
    id 776
    label "odmienianie"
  ]
  node [
    id 777
    label "tura"
  ]
  node [
    id 778
    label "przepracowanie_si&#281;"
  ]
  node [
    id 779
    label "zarz&#261;dzanie"
  ]
  node [
    id 780
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 781
    label "podlizanie_si&#281;"
  ]
  node [
    id 782
    label "dopracowanie"
  ]
  node [
    id 783
    label "podlizywanie_si&#281;"
  ]
  node [
    id 784
    label "uruchamianie"
  ]
  node [
    id 785
    label "dzia&#322;anie"
  ]
  node [
    id 786
    label "d&#261;&#380;enie"
  ]
  node [
    id 787
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 788
    label "uruchomienie"
  ]
  node [
    id 789
    label "nakr&#281;canie"
  ]
  node [
    id 790
    label "funkcjonowanie"
  ]
  node [
    id 791
    label "tr&#243;jstronny"
  ]
  node [
    id 792
    label "postaranie_si&#281;"
  ]
  node [
    id 793
    label "odpocz&#281;cie"
  ]
  node [
    id 794
    label "nakr&#281;cenie"
  ]
  node [
    id 795
    label "zatrzymanie"
  ]
  node [
    id 796
    label "spracowanie_si&#281;"
  ]
  node [
    id 797
    label "skakanie"
  ]
  node [
    id 798
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 799
    label "podtrzymywanie"
  ]
  node [
    id 800
    label "w&#322;&#261;czanie"
  ]
  node [
    id 801
    label "zaprz&#281;ganie"
  ]
  node [
    id 802
    label "podejmowanie"
  ]
  node [
    id 803
    label "wyrabianie"
  ]
  node [
    id 804
    label "dzianie_si&#281;"
  ]
  node [
    id 805
    label "use"
  ]
  node [
    id 806
    label "przepracowanie"
  ]
  node [
    id 807
    label "poruszanie_si&#281;"
  ]
  node [
    id 808
    label "impact"
  ]
  node [
    id 809
    label "przepracowywanie"
  ]
  node [
    id 810
    label "courtship"
  ]
  node [
    id 811
    label "zapracowanie"
  ]
  node [
    id 812
    label "wyrobienie"
  ]
  node [
    id 813
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 814
    label "w&#322;&#261;czenie"
  ]
  node [
    id 815
    label "zawodoznawstwo"
  ]
  node [
    id 816
    label "emocja"
  ]
  node [
    id 817
    label "office"
  ]
  node [
    id 818
    label "kwalifikacje"
  ]
  node [
    id 819
    label "craft"
  ]
  node [
    id 820
    label "transakcja"
  ]
  node [
    id 821
    label "biuro"
  ]
  node [
    id 822
    label "lead"
  ]
  node [
    id 823
    label "w&#322;adza"
  ]
  node [
    id 824
    label "&#380;y&#263;"
  ]
  node [
    id 825
    label "kierowa&#263;"
  ]
  node [
    id 826
    label "g&#243;rowa&#263;"
  ]
  node [
    id 827
    label "tworzy&#263;"
  ]
  node [
    id 828
    label "krzywa"
  ]
  node [
    id 829
    label "linia_melodyczna"
  ]
  node [
    id 830
    label "control"
  ]
  node [
    id 831
    label "string"
  ]
  node [
    id 832
    label "ukierunkowywa&#263;"
  ]
  node [
    id 833
    label "sterowa&#263;"
  ]
  node [
    id 834
    label "kre&#347;li&#263;"
  ]
  node [
    id 835
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 836
    label "message"
  ]
  node [
    id 837
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 838
    label "eksponowa&#263;"
  ]
  node [
    id 839
    label "navigate"
  ]
  node [
    id 840
    label "manipulate"
  ]
  node [
    id 841
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 842
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 843
    label "przesuwa&#263;"
  ]
  node [
    id 844
    label "partner"
  ]
  node [
    id 845
    label "prowadzenie"
  ]
  node [
    id 846
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 847
    label "motywowa&#263;"
  ]
  node [
    id 848
    label "act"
  ]
  node [
    id 849
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 850
    label "organizowa&#263;"
  ]
  node [
    id 851
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 852
    label "czyni&#263;"
  ]
  node [
    id 853
    label "give"
  ]
  node [
    id 854
    label "stylizowa&#263;"
  ]
  node [
    id 855
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 856
    label "falowa&#263;"
  ]
  node [
    id 857
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 858
    label "peddle"
  ]
  node [
    id 859
    label "wydala&#263;"
  ]
  node [
    id 860
    label "tentegowa&#263;"
  ]
  node [
    id 861
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 862
    label "urz&#261;dza&#263;"
  ]
  node [
    id 863
    label "oszukiwa&#263;"
  ]
  node [
    id 864
    label "ukazywa&#263;"
  ]
  node [
    id 865
    label "przerabia&#263;"
  ]
  node [
    id 866
    label "clear"
  ]
  node [
    id 867
    label "usuwa&#263;"
  ]
  node [
    id 868
    label "report"
  ]
  node [
    id 869
    label "rysowa&#263;"
  ]
  node [
    id 870
    label "describe"
  ]
  node [
    id 871
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 872
    label "przedstawia&#263;"
  ]
  node [
    id 873
    label "delineate"
  ]
  node [
    id 874
    label "pope&#322;nia&#263;"
  ]
  node [
    id 875
    label "wytwarza&#263;"
  ]
  node [
    id 876
    label "get"
  ]
  node [
    id 877
    label "consist"
  ]
  node [
    id 878
    label "stanowi&#263;"
  ]
  node [
    id 879
    label "raise"
  ]
  node [
    id 880
    label "podkre&#347;la&#263;"
  ]
  node [
    id 881
    label "demonstrowa&#263;"
  ]
  node [
    id 882
    label "unwrap"
  ]
  node [
    id 883
    label "napromieniowywa&#263;"
  ]
  node [
    id 884
    label "trzyma&#263;"
  ]
  node [
    id 885
    label "manipulowa&#263;"
  ]
  node [
    id 886
    label "wysy&#322;a&#263;"
  ]
  node [
    id 887
    label "zwierzchnik"
  ]
  node [
    id 888
    label "ustawia&#263;"
  ]
  node [
    id 889
    label "przeznacza&#263;"
  ]
  node [
    id 890
    label "match"
  ]
  node [
    id 891
    label "administrowa&#263;"
  ]
  node [
    id 892
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 893
    label "order"
  ]
  node [
    id 894
    label "indicate"
  ]
  node [
    id 895
    label "undertaking"
  ]
  node [
    id 896
    label "base_on_balls"
  ]
  node [
    id 897
    label "wyprzedza&#263;"
  ]
  node [
    id 898
    label "wygrywa&#263;"
  ]
  node [
    id 899
    label "chop"
  ]
  node [
    id 900
    label "przekracza&#263;"
  ]
  node [
    id 901
    label "treat"
  ]
  node [
    id 902
    label "zaspokaja&#263;"
  ]
  node [
    id 903
    label "suffice"
  ]
  node [
    id 904
    label "zaspakaja&#263;"
  ]
  node [
    id 905
    label "uprawia&#263;_seks"
  ]
  node [
    id 906
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 907
    label "serve"
  ]
  node [
    id 908
    label "dostosowywa&#263;"
  ]
  node [
    id 909
    label "estrange"
  ]
  node [
    id 910
    label "transfer"
  ]
  node [
    id 911
    label "translate"
  ]
  node [
    id 912
    label "go"
  ]
  node [
    id 913
    label "postpone"
  ]
  node [
    id 914
    label "przestawia&#263;"
  ]
  node [
    id 915
    label "rusza&#263;"
  ]
  node [
    id 916
    label "przenosi&#263;"
  ]
  node [
    id 917
    label "marshal"
  ]
  node [
    id 918
    label "wyznacza&#263;"
  ]
  node [
    id 919
    label "nadawa&#263;"
  ]
  node [
    id 920
    label "shape"
  ]
  node [
    id 921
    label "pause"
  ]
  node [
    id 922
    label "stay"
  ]
  node [
    id 923
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 924
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 925
    label "dane"
  ]
  node [
    id 926
    label "klawisz"
  ]
  node [
    id 927
    label "figura_geometryczna"
  ]
  node [
    id 928
    label "linia"
  ]
  node [
    id 929
    label "poprowadzi&#263;"
  ]
  node [
    id 930
    label "curvature"
  ]
  node [
    id 931
    label "curve"
  ]
  node [
    id 932
    label "wystawa&#263;"
  ]
  node [
    id 933
    label "sprout"
  ]
  node [
    id 934
    label "dysponowanie"
  ]
  node [
    id 935
    label "sterowanie"
  ]
  node [
    id 936
    label "powodowanie"
  ]
  node [
    id 937
    label "management"
  ]
  node [
    id 938
    label "kierowanie"
  ]
  node [
    id 939
    label "ukierunkowywanie"
  ]
  node [
    id 940
    label "przywodzenie"
  ]
  node [
    id 941
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 942
    label "doprowadzanie"
  ]
  node [
    id 943
    label "kre&#347;lenie"
  ]
  node [
    id 944
    label "eksponowanie"
  ]
  node [
    id 945
    label "robienie"
  ]
  node [
    id 946
    label "prowadzanie"
  ]
  node [
    id 947
    label "wprowadzanie"
  ]
  node [
    id 948
    label "doprowadzenie"
  ]
  node [
    id 949
    label "poprowadzenie"
  ]
  node [
    id 950
    label "kszta&#322;towanie"
  ]
  node [
    id 951
    label "aim"
  ]
  node [
    id 952
    label "zwracanie"
  ]
  node [
    id 953
    label "przecinanie"
  ]
  node [
    id 954
    label "ta&#324;czenie"
  ]
  node [
    id 955
    label "przewy&#380;szanie"
  ]
  node [
    id 956
    label "g&#243;rowanie"
  ]
  node [
    id 957
    label "zaprowadzanie"
  ]
  node [
    id 958
    label "dawanie"
  ]
  node [
    id 959
    label "trzymanie"
  ]
  node [
    id 960
    label "oprowadzanie"
  ]
  node [
    id 961
    label "wprowadzenie"
  ]
  node [
    id 962
    label "oprowadzenie"
  ]
  node [
    id 963
    label "przeci&#281;cie"
  ]
  node [
    id 964
    label "przeci&#261;ganie"
  ]
  node [
    id 965
    label "pozarz&#261;dzanie"
  ]
  node [
    id 966
    label "granie"
  ]
  node [
    id 967
    label "pracownik"
  ]
  node [
    id 968
    label "przedsi&#281;biorca"
  ]
  node [
    id 969
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 970
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 971
    label "kolaborator"
  ]
  node [
    id 972
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 973
    label "sp&#243;lnik"
  ]
  node [
    id 974
    label "aktor"
  ]
  node [
    id 975
    label "uczestniczenie"
  ]
  node [
    id 976
    label "narada"
  ]
  node [
    id 977
    label "ocena"
  ]
  node [
    id 978
    label "porada"
  ]
  node [
    id 979
    label "pogl&#261;d"
  ]
  node [
    id 980
    label "decyzja"
  ]
  node [
    id 981
    label "sofcik"
  ]
  node [
    id 982
    label "kryterium"
  ]
  node [
    id 983
    label "informacja"
  ]
  node [
    id 984
    label "appraisal"
  ]
  node [
    id 985
    label "wskaz&#243;wka"
  ]
  node [
    id 986
    label "dyskusja"
  ]
  node [
    id 987
    label "conference"
  ]
  node [
    id 988
    label "zgromadzenie"
  ]
  node [
    id 989
    label "konsylium"
  ]
  node [
    id 990
    label "spo&#322;ecznie"
  ]
  node [
    id 991
    label "publiczny"
  ]
  node [
    id 992
    label "niepubliczny"
  ]
  node [
    id 993
    label "publicznie"
  ]
  node [
    id 994
    label "upublicznianie"
  ]
  node [
    id 995
    label "jawny"
  ]
  node [
    id 996
    label "upublicznienie"
  ]
  node [
    id 997
    label "zaokr&#261;glenie"
  ]
  node [
    id 998
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 999
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1000
    label "event"
  ]
  node [
    id 1001
    label "przyczyna"
  ]
  node [
    id 1002
    label "round"
  ]
  node [
    id 1003
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1004
    label "zrobi&#263;"
  ]
  node [
    id 1005
    label "przybli&#380;enie"
  ]
  node [
    id 1006
    label "rounding"
  ]
  node [
    id 1007
    label "zaokr&#261;glenie_si&#281;"
  ]
  node [
    id 1008
    label "okr&#261;g&#322;y"
  ]
  node [
    id 1009
    label "zaokr&#261;glony"
  ]
  node [
    id 1010
    label "ukszta&#322;towanie"
  ]
  node [
    id 1011
    label "labializacja"
  ]
  node [
    id 1012
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1013
    label "subject"
  ]
  node [
    id 1014
    label "czynnik"
  ]
  node [
    id 1015
    label "matuszka"
  ]
  node [
    id 1016
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1017
    label "geneza"
  ]
  node [
    id 1018
    label "poci&#261;ganie"
  ]
  node [
    id 1019
    label "facet"
  ]
  node [
    id 1020
    label "kr&#243;lestwo"
  ]
  node [
    id 1021
    label "autorament"
  ]
  node [
    id 1022
    label "variety"
  ]
  node [
    id 1023
    label "antycypacja"
  ]
  node [
    id 1024
    label "przypuszczenie"
  ]
  node [
    id 1025
    label "cynk"
  ]
  node [
    id 1026
    label "obstawia&#263;"
  ]
  node [
    id 1027
    label "sztuka"
  ]
  node [
    id 1028
    label "design"
  ]
  node [
    id 1029
    label "skutek"
  ]
  node [
    id 1030
    label "podzia&#322;anie"
  ]
  node [
    id 1031
    label "kampania"
  ]
  node [
    id 1032
    label "hipnotyzowanie"
  ]
  node [
    id 1033
    label "natural_process"
  ]
  node [
    id 1034
    label "wp&#322;yw"
  ]
  node [
    id 1035
    label "operation"
  ]
  node [
    id 1036
    label "zadzia&#322;anie"
  ]
  node [
    id 1037
    label "priorytet"
  ]
  node [
    id 1038
    label "kres"
  ]
  node [
    id 1039
    label "rozpocz&#281;cie"
  ]
  node [
    id 1040
    label "docieranie"
  ]
  node [
    id 1041
    label "czynny"
  ]
  node [
    id 1042
    label "zako&#324;czenie"
  ]
  node [
    id 1043
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1044
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1045
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1046
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1047
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1048
    label "pozosta&#263;"
  ]
  node [
    id 1049
    label "catch"
  ]
  node [
    id 1050
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1051
    label "proceed"
  ]
  node [
    id 1052
    label "support"
  ]
  node [
    id 1053
    label "prze&#380;y&#263;"
  ]
  node [
    id 1054
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 1055
    label "include"
  ]
  node [
    id 1056
    label "wzi&#261;&#263;"
  ]
  node [
    id 1057
    label "odziedziczy&#263;"
  ]
  node [
    id 1058
    label "ruszy&#263;"
  ]
  node [
    id 1059
    label "take"
  ]
  node [
    id 1060
    label "zaatakowa&#263;"
  ]
  node [
    id 1061
    label "skorzysta&#263;"
  ]
  node [
    id 1062
    label "uciec"
  ]
  node [
    id 1063
    label "receive"
  ]
  node [
    id 1064
    label "nakaza&#263;"
  ]
  node [
    id 1065
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1066
    label "obskoczy&#263;"
  ]
  node [
    id 1067
    label "bra&#263;"
  ]
  node [
    id 1068
    label "u&#380;y&#263;"
  ]
  node [
    id 1069
    label "wyrucha&#263;"
  ]
  node [
    id 1070
    label "World_Health_Organization"
  ]
  node [
    id 1071
    label "wyciupcia&#263;"
  ]
  node [
    id 1072
    label "wygra&#263;"
  ]
  node [
    id 1073
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 1074
    label "withdraw"
  ]
  node [
    id 1075
    label "wzi&#281;cie"
  ]
  node [
    id 1076
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 1077
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 1078
    label "poczyta&#263;"
  ]
  node [
    id 1079
    label "obj&#261;&#263;"
  ]
  node [
    id 1080
    label "seize"
  ]
  node [
    id 1081
    label "chwyci&#263;"
  ]
  node [
    id 1082
    label "przyj&#261;&#263;"
  ]
  node [
    id 1083
    label "pokona&#263;"
  ]
  node [
    id 1084
    label "arise"
  ]
  node [
    id 1085
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1086
    label "zacz&#261;&#263;"
  ]
  node [
    id 1087
    label "otrzyma&#263;"
  ]
  node [
    id 1088
    label "wej&#347;&#263;"
  ]
  node [
    id 1089
    label "poruszy&#263;"
  ]
  node [
    id 1090
    label "dosta&#263;"
  ]
  node [
    id 1091
    label "komunikowa&#263;"
  ]
  node [
    id 1092
    label "convey"
  ]
  node [
    id 1093
    label "communicate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 239
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 971
  ]
  edge [
    source 13
    target 972
  ]
  edge [
    source 13
    target 973
  ]
  edge [
    source 13
    target 974
  ]
  edge [
    source 13
    target 975
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 547
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 390
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 526
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 266
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 406
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 393
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 395
  ]
  edge [
    source 17
    target 284
  ]
  edge [
    source 17
    target 396
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 440
  ]
  edge [
    source 17
    target 397
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 398
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 399
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 384
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 353
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 298
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 182
  ]
]
