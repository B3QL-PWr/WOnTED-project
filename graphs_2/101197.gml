graph [
  node [
    id 0
    label "samo&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "self-consciousness"
  ]
  node [
    id 2
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 3
    label "depersonalizacja"
  ]
  node [
    id 4
    label "ekstraspekcja"
  ]
  node [
    id 5
    label "feeling"
  ]
  node [
    id 6
    label "wiedza"
  ]
  node [
    id 7
    label "zemdle&#263;"
  ]
  node [
    id 8
    label "psychika"
  ]
  node [
    id 9
    label "stan"
  ]
  node [
    id 10
    label "Freud"
  ]
  node [
    id 11
    label "psychoanaliza"
  ]
  node [
    id 12
    label "conscience"
  ]
  node [
    id 13
    label "strata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
]
