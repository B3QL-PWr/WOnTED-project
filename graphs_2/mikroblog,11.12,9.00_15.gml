graph [
  node [
    id 0
    label "wszyscy"
    origin "text"
  ]
  node [
    id 1
    label "ochoczo"
    origin "text"
  ]
  node [
    id 2
    label "&#347;mieszkuj&#261;"
    origin "text"
  ]
  node [
    id 3
    label "mirka"
    origin "text"
  ]
  node [
    id 4
    label "prawda"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "taka"
    origin "text"
  ]
  node [
    id 7
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "smutny"
    origin "text"
  ]
  node [
    id 10
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 11
    label "ochoczy"
  ]
  node [
    id 12
    label "ch&#281;tnie"
  ]
  node [
    id 13
    label "weso&#322;o"
  ]
  node [
    id 14
    label "dziarsko"
  ]
  node [
    id 15
    label "positively"
  ]
  node [
    id 16
    label "ch&#281;tny"
  ]
  node [
    id 17
    label "enthusiastically"
  ]
  node [
    id 18
    label "&#380;wawo"
  ]
  node [
    id 19
    label "dziarski"
  ]
  node [
    id 20
    label "pozytywnie"
  ]
  node [
    id 21
    label "weso&#322;y"
  ]
  node [
    id 22
    label "beztrosko"
  ]
  node [
    id 23
    label "przyjemnie"
  ]
  node [
    id 24
    label "dobrze"
  ]
  node [
    id 25
    label "za&#322;o&#380;enie"
  ]
  node [
    id 26
    label "prawdziwy"
  ]
  node [
    id 27
    label "truth"
  ]
  node [
    id 28
    label "nieprawdziwy"
  ]
  node [
    id 29
    label "s&#261;d"
  ]
  node [
    id 30
    label "realia"
  ]
  node [
    id 31
    label "message"
  ]
  node [
    id 32
    label "kontekst"
  ]
  node [
    id 33
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 34
    label "infliction"
  ]
  node [
    id 35
    label "spowodowanie"
  ]
  node [
    id 36
    label "proposition"
  ]
  node [
    id 37
    label "przygotowanie"
  ]
  node [
    id 38
    label "pozak&#322;adanie"
  ]
  node [
    id 39
    label "point"
  ]
  node [
    id 40
    label "program"
  ]
  node [
    id 41
    label "poubieranie"
  ]
  node [
    id 42
    label "rozebranie"
  ]
  node [
    id 43
    label "str&#243;j"
  ]
  node [
    id 44
    label "budowla"
  ]
  node [
    id 45
    label "przewidzenie"
  ]
  node [
    id 46
    label "zak&#322;adka"
  ]
  node [
    id 47
    label "czynno&#347;&#263;"
  ]
  node [
    id 48
    label "twierdzenie"
  ]
  node [
    id 49
    label "przygotowywanie"
  ]
  node [
    id 50
    label "podwini&#281;cie"
  ]
  node [
    id 51
    label "zap&#322;acenie"
  ]
  node [
    id 52
    label "wyko&#324;czenie"
  ]
  node [
    id 53
    label "struktura"
  ]
  node [
    id 54
    label "utworzenie"
  ]
  node [
    id 55
    label "przebranie"
  ]
  node [
    id 56
    label "obleczenie"
  ]
  node [
    id 57
    label "przymierzenie"
  ]
  node [
    id 58
    label "obleczenie_si&#281;"
  ]
  node [
    id 59
    label "przywdzianie"
  ]
  node [
    id 60
    label "umieszczenie"
  ]
  node [
    id 61
    label "zrobienie"
  ]
  node [
    id 62
    label "przyodzianie"
  ]
  node [
    id 63
    label "pokrycie"
  ]
  node [
    id 64
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 65
    label "forum"
  ]
  node [
    id 66
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 67
    label "s&#261;downictwo"
  ]
  node [
    id 68
    label "wydarzenie"
  ]
  node [
    id 69
    label "podejrzany"
  ]
  node [
    id 70
    label "&#347;wiadek"
  ]
  node [
    id 71
    label "instytucja"
  ]
  node [
    id 72
    label "biuro"
  ]
  node [
    id 73
    label "post&#281;powanie"
  ]
  node [
    id 74
    label "court"
  ]
  node [
    id 75
    label "my&#347;l"
  ]
  node [
    id 76
    label "obrona"
  ]
  node [
    id 77
    label "system"
  ]
  node [
    id 78
    label "broni&#263;"
  ]
  node [
    id 79
    label "antylogizm"
  ]
  node [
    id 80
    label "strona"
  ]
  node [
    id 81
    label "oskar&#380;yciel"
  ]
  node [
    id 82
    label "urz&#261;d"
  ]
  node [
    id 83
    label "skazany"
  ]
  node [
    id 84
    label "konektyw"
  ]
  node [
    id 85
    label "wypowied&#378;"
  ]
  node [
    id 86
    label "bronienie"
  ]
  node [
    id 87
    label "wytw&#243;r"
  ]
  node [
    id 88
    label "pods&#261;dny"
  ]
  node [
    id 89
    label "zesp&#243;&#322;"
  ]
  node [
    id 90
    label "procesowicz"
  ]
  node [
    id 91
    label "zgodny"
  ]
  node [
    id 92
    label "prawdziwie"
  ]
  node [
    id 93
    label "podobny"
  ]
  node [
    id 94
    label "m&#261;dry"
  ]
  node [
    id 95
    label "szczery"
  ]
  node [
    id 96
    label "naprawd&#281;"
  ]
  node [
    id 97
    label "naturalny"
  ]
  node [
    id 98
    label "&#380;ywny"
  ]
  node [
    id 99
    label "realnie"
  ]
  node [
    id 100
    label "nieszczery"
  ]
  node [
    id 101
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 102
    label "niehistoryczny"
  ]
  node [
    id 103
    label "nieprawdziwie"
  ]
  node [
    id 104
    label "udawany"
  ]
  node [
    id 105
    label "niezgodny"
  ]
  node [
    id 106
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 107
    label "stan"
  ]
  node [
    id 108
    label "stand"
  ]
  node [
    id 109
    label "trwa&#263;"
  ]
  node [
    id 110
    label "equal"
  ]
  node [
    id 111
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "chodzi&#263;"
  ]
  node [
    id 113
    label "uczestniczy&#263;"
  ]
  node [
    id 114
    label "obecno&#347;&#263;"
  ]
  node [
    id 115
    label "si&#281;ga&#263;"
  ]
  node [
    id 116
    label "mie&#263;_miejsce"
  ]
  node [
    id 117
    label "robi&#263;"
  ]
  node [
    id 118
    label "participate"
  ]
  node [
    id 119
    label "adhere"
  ]
  node [
    id 120
    label "pozostawa&#263;"
  ]
  node [
    id 121
    label "zostawa&#263;"
  ]
  node [
    id 122
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 123
    label "istnie&#263;"
  ]
  node [
    id 124
    label "compass"
  ]
  node [
    id 125
    label "exsert"
  ]
  node [
    id 126
    label "get"
  ]
  node [
    id 127
    label "u&#380;ywa&#263;"
  ]
  node [
    id 128
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 129
    label "osi&#261;ga&#263;"
  ]
  node [
    id 130
    label "korzysta&#263;"
  ]
  node [
    id 131
    label "appreciation"
  ]
  node [
    id 132
    label "dociera&#263;"
  ]
  node [
    id 133
    label "mierzy&#263;"
  ]
  node [
    id 134
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 135
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 136
    label "being"
  ]
  node [
    id 137
    label "cecha"
  ]
  node [
    id 138
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 139
    label "proceed"
  ]
  node [
    id 140
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 141
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 142
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 143
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 144
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 145
    label "para"
  ]
  node [
    id 146
    label "krok"
  ]
  node [
    id 147
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 148
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 149
    label "przebiega&#263;"
  ]
  node [
    id 150
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 151
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 152
    label "continue"
  ]
  node [
    id 153
    label "carry"
  ]
  node [
    id 154
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 155
    label "wk&#322;ada&#263;"
  ]
  node [
    id 156
    label "p&#322;ywa&#263;"
  ]
  node [
    id 157
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 158
    label "bangla&#263;"
  ]
  node [
    id 159
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 160
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 161
    label "bywa&#263;"
  ]
  node [
    id 162
    label "tryb"
  ]
  node [
    id 163
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 164
    label "dziama&#263;"
  ]
  node [
    id 165
    label "run"
  ]
  node [
    id 166
    label "stara&#263;_si&#281;"
  ]
  node [
    id 167
    label "Arakan"
  ]
  node [
    id 168
    label "Teksas"
  ]
  node [
    id 169
    label "Georgia"
  ]
  node [
    id 170
    label "Maryland"
  ]
  node [
    id 171
    label "warstwa"
  ]
  node [
    id 172
    label "Luizjana"
  ]
  node [
    id 173
    label "Massachusetts"
  ]
  node [
    id 174
    label "Michigan"
  ]
  node [
    id 175
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 176
    label "samopoczucie"
  ]
  node [
    id 177
    label "Floryda"
  ]
  node [
    id 178
    label "Ohio"
  ]
  node [
    id 179
    label "Alaska"
  ]
  node [
    id 180
    label "Nowy_Meksyk"
  ]
  node [
    id 181
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 182
    label "wci&#281;cie"
  ]
  node [
    id 183
    label "Kansas"
  ]
  node [
    id 184
    label "Alabama"
  ]
  node [
    id 185
    label "miejsce"
  ]
  node [
    id 186
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 187
    label "Kalifornia"
  ]
  node [
    id 188
    label "Wirginia"
  ]
  node [
    id 189
    label "punkt"
  ]
  node [
    id 190
    label "Nowy_York"
  ]
  node [
    id 191
    label "Waszyngton"
  ]
  node [
    id 192
    label "Pensylwania"
  ]
  node [
    id 193
    label "wektor"
  ]
  node [
    id 194
    label "Hawaje"
  ]
  node [
    id 195
    label "state"
  ]
  node [
    id 196
    label "poziom"
  ]
  node [
    id 197
    label "jednostka_administracyjna"
  ]
  node [
    id 198
    label "Illinois"
  ]
  node [
    id 199
    label "Oklahoma"
  ]
  node [
    id 200
    label "Jukatan"
  ]
  node [
    id 201
    label "Arizona"
  ]
  node [
    id 202
    label "ilo&#347;&#263;"
  ]
  node [
    id 203
    label "Oregon"
  ]
  node [
    id 204
    label "shape"
  ]
  node [
    id 205
    label "Goa"
  ]
  node [
    id 206
    label "jednostka_monetarna"
  ]
  node [
    id 207
    label "Bangladesz"
  ]
  node [
    id 208
    label "Bengal"
  ]
  node [
    id 209
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 210
    label "majority"
  ]
  node [
    id 211
    label "Rzym_Zachodni"
  ]
  node [
    id 212
    label "Rzym_Wschodni"
  ]
  node [
    id 213
    label "element"
  ]
  node [
    id 214
    label "whole"
  ]
  node [
    id 215
    label "urz&#261;dzenie"
  ]
  node [
    id 216
    label "pause"
  ]
  node [
    id 217
    label "garowa&#263;"
  ]
  node [
    id 218
    label "brood"
  ]
  node [
    id 219
    label "zwierz&#281;"
  ]
  node [
    id 220
    label "przebywa&#263;"
  ]
  node [
    id 221
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 222
    label "sit"
  ]
  node [
    id 223
    label "doprowadza&#263;"
  ]
  node [
    id 224
    label "mieszka&#263;"
  ]
  node [
    id 225
    label "ptak"
  ]
  node [
    id 226
    label "spoczywa&#263;"
  ]
  node [
    id 227
    label "tkwi&#263;"
  ]
  node [
    id 228
    label "hesitate"
  ]
  node [
    id 229
    label "przestawa&#263;"
  ]
  node [
    id 230
    label "nadzieja"
  ]
  node [
    id 231
    label "lie"
  ]
  node [
    id 232
    label "odpoczywa&#263;"
  ]
  node [
    id 233
    label "gr&#243;b"
  ]
  node [
    id 234
    label "panowa&#263;"
  ]
  node [
    id 235
    label "sta&#263;"
  ]
  node [
    id 236
    label "fall"
  ]
  node [
    id 237
    label "zajmowa&#263;"
  ]
  node [
    id 238
    label "room"
  ]
  node [
    id 239
    label "fix"
  ]
  node [
    id 240
    label "polega&#263;"
  ]
  node [
    id 241
    label "moderate"
  ]
  node [
    id 242
    label "wprowadza&#263;"
  ]
  node [
    id 243
    label "powodowa&#263;"
  ]
  node [
    id 244
    label "prowadzi&#263;"
  ]
  node [
    id 245
    label "wykonywa&#263;"
  ]
  node [
    id 246
    label "rig"
  ]
  node [
    id 247
    label "wzbudza&#263;"
  ]
  node [
    id 248
    label "deliver"
  ]
  node [
    id 249
    label "&#347;l&#281;cze&#263;"
  ]
  node [
    id 250
    label "tankowa&#263;"
  ]
  node [
    id 251
    label "le&#380;akowa&#263;"
  ]
  node [
    id 252
    label "rosn&#261;&#263;"
  ]
  node [
    id 253
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 254
    label "harowa&#263;"
  ]
  node [
    id 255
    label "monogamia"
  ]
  node [
    id 256
    label "grzbiet"
  ]
  node [
    id 257
    label "bestia"
  ]
  node [
    id 258
    label "treser"
  ]
  node [
    id 259
    label "niecz&#322;owiek"
  ]
  node [
    id 260
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 261
    label "agresja"
  ]
  node [
    id 262
    label "skubni&#281;cie"
  ]
  node [
    id 263
    label "skuba&#263;"
  ]
  node [
    id 264
    label "tresowa&#263;"
  ]
  node [
    id 265
    label "oz&#243;r"
  ]
  node [
    id 266
    label "istota_&#380;ywa"
  ]
  node [
    id 267
    label "wylinka"
  ]
  node [
    id 268
    label "poskramia&#263;"
  ]
  node [
    id 269
    label "fukni&#281;cie"
  ]
  node [
    id 270
    label "siedzenie"
  ]
  node [
    id 271
    label "wios&#322;owa&#263;"
  ]
  node [
    id 272
    label "zwyrol"
  ]
  node [
    id 273
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 274
    label "budowa_cia&#322;a"
  ]
  node [
    id 275
    label "sodomita"
  ]
  node [
    id 276
    label "wiwarium"
  ]
  node [
    id 277
    label "oswaja&#263;"
  ]
  node [
    id 278
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 279
    label "degenerat"
  ]
  node [
    id 280
    label "le&#380;e&#263;"
  ]
  node [
    id 281
    label "przyssawka"
  ]
  node [
    id 282
    label "animalista"
  ]
  node [
    id 283
    label "fauna"
  ]
  node [
    id 284
    label "hodowla"
  ]
  node [
    id 285
    label "popapraniec"
  ]
  node [
    id 286
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 287
    label "le&#380;enie"
  ]
  node [
    id 288
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 289
    label "poligamia"
  ]
  node [
    id 290
    label "budowa"
  ]
  node [
    id 291
    label "napasienie_si&#281;"
  ]
  node [
    id 292
    label "&#322;eb"
  ]
  node [
    id 293
    label "paszcza"
  ]
  node [
    id 294
    label "czerniak"
  ]
  node [
    id 295
    label "zwierz&#281;ta"
  ]
  node [
    id 296
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 297
    label "zachowanie"
  ]
  node [
    id 298
    label "skubn&#261;&#263;"
  ]
  node [
    id 299
    label "wios&#322;owanie"
  ]
  node [
    id 300
    label "skubanie"
  ]
  node [
    id 301
    label "okrutnik"
  ]
  node [
    id 302
    label "pasienie_si&#281;"
  ]
  node [
    id 303
    label "farba"
  ]
  node [
    id 304
    label "weterynarz"
  ]
  node [
    id 305
    label "gad"
  ]
  node [
    id 306
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 307
    label "fukanie"
  ]
  node [
    id 308
    label "bird"
  ]
  node [
    id 309
    label "roz&#263;wierkanie_si&#281;"
  ]
  node [
    id 310
    label "hukni&#281;cie"
  ]
  node [
    id 311
    label "upierzenie"
  ]
  node [
    id 312
    label "grzebie&#324;"
  ]
  node [
    id 313
    label "pi&#243;ro"
  ]
  node [
    id 314
    label "skok"
  ]
  node [
    id 315
    label "zaklekotanie"
  ]
  node [
    id 316
    label "ptaki"
  ]
  node [
    id 317
    label "owodniowiec"
  ]
  node [
    id 318
    label "kloaka"
  ]
  node [
    id 319
    label "dzi&#243;bni&#281;cie"
  ]
  node [
    id 320
    label "kuper"
  ]
  node [
    id 321
    label "ko&#347;&#263;_potyliczna"
  ]
  node [
    id 322
    label "wysiadywa&#263;"
  ]
  node [
    id 323
    label "dziobn&#261;&#263;"
  ]
  node [
    id 324
    label "ptactwo"
  ]
  node [
    id 325
    label "ptasz&#281;"
  ]
  node [
    id 326
    label "dziobni&#281;cie"
  ]
  node [
    id 327
    label "dzi&#243;b"
  ]
  node [
    id 328
    label "dziobanie"
  ]
  node [
    id 329
    label "skrzyd&#322;o"
  ]
  node [
    id 330
    label "dzioba&#263;"
  ]
  node [
    id 331
    label "pogwizdywanie"
  ]
  node [
    id 332
    label "dzi&#243;bn&#261;&#263;"
  ]
  node [
    id 333
    label "wysiedzie&#263;"
  ]
  node [
    id 334
    label "ro&#347;lina_zielna"
  ]
  node [
    id 335
    label "sitowate"
  ]
  node [
    id 336
    label "cover"
  ]
  node [
    id 337
    label "przykry"
  ]
  node [
    id 338
    label "z&#322;y"
  ]
  node [
    id 339
    label "smutno"
  ]
  node [
    id 340
    label "negatywny"
  ]
  node [
    id 341
    label "negatywnie"
  ]
  node [
    id 342
    label "&#378;le"
  ]
  node [
    id 343
    label "ujemnie"
  ]
  node [
    id 344
    label "nieprzyjemny"
  ]
  node [
    id 345
    label "syf"
  ]
  node [
    id 346
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 347
    label "niepomy&#347;lny"
  ]
  node [
    id 348
    label "niegrzeczny"
  ]
  node [
    id 349
    label "rozgniewanie"
  ]
  node [
    id 350
    label "zdenerwowany"
  ]
  node [
    id 351
    label "niemoralny"
  ]
  node [
    id 352
    label "sierdzisty"
  ]
  node [
    id 353
    label "pieski"
  ]
  node [
    id 354
    label "zez&#322;oszczenie"
  ]
  node [
    id 355
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 356
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 357
    label "z&#322;oszczenie"
  ]
  node [
    id 358
    label "gniewanie"
  ]
  node [
    id 359
    label "niekorzystny"
  ]
  node [
    id 360
    label "dokuczliwy"
  ]
  node [
    id 361
    label "przykro"
  ]
  node [
    id 362
    label "niemi&#322;y"
  ]
  node [
    id 363
    label "nieprzyjemnie"
  ]
  node [
    id 364
    label "smutnie"
  ]
  node [
    id 365
    label "asymilowa&#263;"
  ]
  node [
    id 366
    label "nasada"
  ]
  node [
    id 367
    label "profanum"
  ]
  node [
    id 368
    label "wz&#243;r"
  ]
  node [
    id 369
    label "senior"
  ]
  node [
    id 370
    label "asymilowanie"
  ]
  node [
    id 371
    label "os&#322;abia&#263;"
  ]
  node [
    id 372
    label "homo_sapiens"
  ]
  node [
    id 373
    label "osoba"
  ]
  node [
    id 374
    label "ludzko&#347;&#263;"
  ]
  node [
    id 375
    label "Adam"
  ]
  node [
    id 376
    label "hominid"
  ]
  node [
    id 377
    label "posta&#263;"
  ]
  node [
    id 378
    label "portrecista"
  ]
  node [
    id 379
    label "polifag"
  ]
  node [
    id 380
    label "podw&#322;adny"
  ]
  node [
    id 381
    label "dwun&#243;g"
  ]
  node [
    id 382
    label "wapniak"
  ]
  node [
    id 383
    label "duch"
  ]
  node [
    id 384
    label "os&#322;abianie"
  ]
  node [
    id 385
    label "antropochoria"
  ]
  node [
    id 386
    label "figura"
  ]
  node [
    id 387
    label "g&#322;owa"
  ]
  node [
    id 388
    label "mikrokosmos"
  ]
  node [
    id 389
    label "oddzia&#322;ywanie"
  ]
  node [
    id 390
    label "konsument"
  ]
  node [
    id 391
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 392
    label "cz&#322;owiekowate"
  ]
  node [
    id 393
    label "pracownik"
  ]
  node [
    id 394
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 395
    label "Gargantua"
  ]
  node [
    id 396
    label "Chocho&#322;"
  ]
  node [
    id 397
    label "Hamlet"
  ]
  node [
    id 398
    label "Wallenrod"
  ]
  node [
    id 399
    label "Quasimodo"
  ]
  node [
    id 400
    label "parali&#380;owa&#263;"
  ]
  node [
    id 401
    label "Plastu&#347;"
  ]
  node [
    id 402
    label "kategoria_gramatyczna"
  ]
  node [
    id 403
    label "istota"
  ]
  node [
    id 404
    label "Casanova"
  ]
  node [
    id 405
    label "Szwejk"
  ]
  node [
    id 406
    label "Edyp"
  ]
  node [
    id 407
    label "Don_Juan"
  ]
  node [
    id 408
    label "koniugacja"
  ]
  node [
    id 409
    label "Werter"
  ]
  node [
    id 410
    label "person"
  ]
  node [
    id 411
    label "Harry_Potter"
  ]
  node [
    id 412
    label "Sherlock_Holmes"
  ]
  node [
    id 413
    label "Dwukwiat"
  ]
  node [
    id 414
    label "Winnetou"
  ]
  node [
    id 415
    label "Don_Kiszot"
  ]
  node [
    id 416
    label "Herkules_Poirot"
  ]
  node [
    id 417
    label "Faust"
  ]
  node [
    id 418
    label "Zgredek"
  ]
  node [
    id 419
    label "Dulcynea"
  ]
  node [
    id 420
    label "rodzic"
  ]
  node [
    id 421
    label "jajko"
  ]
  node [
    id 422
    label "wapniaki"
  ]
  node [
    id 423
    label "doros&#322;y"
  ]
  node [
    id 424
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 425
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 426
    label "zawodnik"
  ]
  node [
    id 427
    label "starzec"
  ]
  node [
    id 428
    label "zwierzchnik"
  ]
  node [
    id 429
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 430
    label "komendancja"
  ]
  node [
    id 431
    label "feuda&#322;"
  ]
  node [
    id 432
    label "g&#322;oska"
  ]
  node [
    id 433
    label "acquisition"
  ]
  node [
    id 434
    label "assimilation"
  ]
  node [
    id 435
    label "czerpanie"
  ]
  node [
    id 436
    label "upodabnianie"
  ]
  node [
    id 437
    label "organizm"
  ]
  node [
    id 438
    label "fonetyka"
  ]
  node [
    id 439
    label "pobieranie"
  ]
  node [
    id 440
    label "kultura"
  ]
  node [
    id 441
    label "asymilowanie_si&#281;"
  ]
  node [
    id 442
    label "zmienianie"
  ]
  node [
    id 443
    label "grupa"
  ]
  node [
    id 444
    label "absorption"
  ]
  node [
    id 445
    label "bate"
  ]
  node [
    id 446
    label "suppress"
  ]
  node [
    id 447
    label "kondycja_fizyczna"
  ]
  node [
    id 448
    label "zmniejsza&#263;"
  ]
  node [
    id 449
    label "os&#322;abi&#263;"
  ]
  node [
    id 450
    label "os&#322;abienie"
  ]
  node [
    id 451
    label "zdrowie"
  ]
  node [
    id 452
    label "debilitation"
  ]
  node [
    id 453
    label "powodowanie"
  ]
  node [
    id 454
    label "s&#322;abszy"
  ]
  node [
    id 455
    label "de-escalation"
  ]
  node [
    id 456
    label "zmniejszanie"
  ]
  node [
    id 457
    label "pogarszanie"
  ]
  node [
    id 458
    label "dostosowywa&#263;"
  ]
  node [
    id 459
    label "przej&#261;&#263;"
  ]
  node [
    id 460
    label "pobra&#263;"
  ]
  node [
    id 461
    label "assimilate"
  ]
  node [
    id 462
    label "przejmowa&#263;"
  ]
  node [
    id 463
    label "upodobni&#263;"
  ]
  node [
    id 464
    label "pobiera&#263;"
  ]
  node [
    id 465
    label "upodabnia&#263;"
  ]
  node [
    id 466
    label "dostosowa&#263;"
  ]
  node [
    id 467
    label "spos&#243;b"
  ]
  node [
    id 468
    label "projekt"
  ]
  node [
    id 469
    label "mildew"
  ]
  node [
    id 470
    label "ideal"
  ]
  node [
    id 471
    label "zapis"
  ]
  node [
    id 472
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 473
    label "typ"
  ]
  node [
    id 474
    label "ruch"
  ]
  node [
    id 475
    label "rule"
  ]
  node [
    id 476
    label "dekal"
  ]
  node [
    id 477
    label "figure"
  ]
  node [
    id 478
    label "motyw"
  ]
  node [
    id 479
    label "wytrzyma&#263;"
  ]
  node [
    id 480
    label "trim"
  ]
  node [
    id 481
    label "Osjan"
  ]
  node [
    id 482
    label "formacja"
  ]
  node [
    id 483
    label "kto&#347;"
  ]
  node [
    id 484
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 485
    label "pozosta&#263;"
  ]
  node [
    id 486
    label "poby&#263;"
  ]
  node [
    id 487
    label "przedstawienie"
  ]
  node [
    id 488
    label "Aspazja"
  ]
  node [
    id 489
    label "go&#347;&#263;"
  ]
  node [
    id 490
    label "osobowo&#347;&#263;"
  ]
  node [
    id 491
    label "charakterystyka"
  ]
  node [
    id 492
    label "kompleksja"
  ]
  node [
    id 493
    label "wygl&#261;d"
  ]
  node [
    id 494
    label "punkt_widzenia"
  ]
  node [
    id 495
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 496
    label "zaistnie&#263;"
  ]
  node [
    id 497
    label "malarz"
  ]
  node [
    id 498
    label "artysta"
  ]
  node [
    id 499
    label "fotograf"
  ]
  node [
    id 500
    label "hipnotyzowanie"
  ]
  node [
    id 501
    label "act"
  ]
  node [
    id 502
    label "zjawisko"
  ]
  node [
    id 503
    label "&#347;lad"
  ]
  node [
    id 504
    label "rezultat"
  ]
  node [
    id 505
    label "reakcja_chemiczna"
  ]
  node [
    id 506
    label "docieranie"
  ]
  node [
    id 507
    label "lobbysta"
  ]
  node [
    id 508
    label "natural_process"
  ]
  node [
    id 509
    label "wdzieranie_si&#281;"
  ]
  node [
    id 510
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 511
    label "zdolno&#347;&#263;"
  ]
  node [
    id 512
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 513
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 514
    label "umys&#322;"
  ]
  node [
    id 515
    label "kierowa&#263;"
  ]
  node [
    id 516
    label "obiekt"
  ]
  node [
    id 517
    label "sztuka"
  ]
  node [
    id 518
    label "czaszka"
  ]
  node [
    id 519
    label "g&#243;ra"
  ]
  node [
    id 520
    label "wiedza"
  ]
  node [
    id 521
    label "fryzura"
  ]
  node [
    id 522
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 523
    label "pryncypa&#322;"
  ]
  node [
    id 524
    label "ro&#347;lina"
  ]
  node [
    id 525
    label "ucho"
  ]
  node [
    id 526
    label "byd&#322;o"
  ]
  node [
    id 527
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 528
    label "alkohol"
  ]
  node [
    id 529
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 530
    label "kierownictwo"
  ]
  node [
    id 531
    label "&#347;ci&#281;cie"
  ]
  node [
    id 532
    label "cz&#322;onek"
  ]
  node [
    id 533
    label "makrocefalia"
  ]
  node [
    id 534
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 535
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 536
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 537
    label "&#380;ycie"
  ]
  node [
    id 538
    label "dekiel"
  ]
  node [
    id 539
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 540
    label "m&#243;zg"
  ]
  node [
    id 541
    label "&#347;ci&#281;gno"
  ]
  node [
    id 542
    label "cia&#322;o"
  ]
  node [
    id 543
    label "kszta&#322;t"
  ]
  node [
    id 544
    label "noosfera"
  ]
  node [
    id 545
    label "allochoria"
  ]
  node [
    id 546
    label "obiekt_matematyczny"
  ]
  node [
    id 547
    label "gestaltyzm"
  ]
  node [
    id 548
    label "d&#378;wi&#281;k"
  ]
  node [
    id 549
    label "ornamentyka"
  ]
  node [
    id 550
    label "stylistyka"
  ]
  node [
    id 551
    label "podzbi&#243;r"
  ]
  node [
    id 552
    label "styl"
  ]
  node [
    id 553
    label "antycypacja"
  ]
  node [
    id 554
    label "przedmiot"
  ]
  node [
    id 555
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 556
    label "wiersz"
  ]
  node [
    id 557
    label "facet"
  ]
  node [
    id 558
    label "popis"
  ]
  node [
    id 559
    label "obraz"
  ]
  node [
    id 560
    label "p&#322;aszczyzna"
  ]
  node [
    id 561
    label "informacja"
  ]
  node [
    id 562
    label "symetria"
  ]
  node [
    id 563
    label "rzecz"
  ]
  node [
    id 564
    label "perspektywa"
  ]
  node [
    id 565
    label "lingwistyka_kognitywna"
  ]
  node [
    id 566
    label "character"
  ]
  node [
    id 567
    label "rze&#378;ba"
  ]
  node [
    id 568
    label "bierka_szachowa"
  ]
  node [
    id 569
    label "karta"
  ]
  node [
    id 570
    label "dziedzina"
  ]
  node [
    id 571
    label "nak&#322;adka"
  ]
  node [
    id 572
    label "jama_gard&#322;owa"
  ]
  node [
    id 573
    label "podstawa"
  ]
  node [
    id 574
    label "base"
  ]
  node [
    id 575
    label "li&#347;&#263;"
  ]
  node [
    id 576
    label "rezonator"
  ]
  node [
    id 577
    label "deformowa&#263;"
  ]
  node [
    id 578
    label "deformowanie"
  ]
  node [
    id 579
    label "sfera_afektywna"
  ]
  node [
    id 580
    label "sumienie"
  ]
  node [
    id 581
    label "entity"
  ]
  node [
    id 582
    label "psychika"
  ]
  node [
    id 583
    label "istota_nadprzyrodzona"
  ]
  node [
    id 584
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 585
    label "charakter"
  ]
  node [
    id 586
    label "fizjonomia"
  ]
  node [
    id 587
    label "power"
  ]
  node [
    id 588
    label "byt"
  ]
  node [
    id 589
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 590
    label "human_body"
  ]
  node [
    id 591
    label "podekscytowanie"
  ]
  node [
    id 592
    label "kompleks"
  ]
  node [
    id 593
    label "piek&#322;o"
  ]
  node [
    id 594
    label "oddech"
  ]
  node [
    id 595
    label "ofiarowywa&#263;"
  ]
  node [
    id 596
    label "nekromancja"
  ]
  node [
    id 597
    label "si&#322;a"
  ]
  node [
    id 598
    label "seksualno&#347;&#263;"
  ]
  node [
    id 599
    label "zjawa"
  ]
  node [
    id 600
    label "zapalno&#347;&#263;"
  ]
  node [
    id 601
    label "ego"
  ]
  node [
    id 602
    label "ofiarowa&#263;"
  ]
  node [
    id 603
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 604
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 605
    label "Po&#347;wist"
  ]
  node [
    id 606
    label "passion"
  ]
  node [
    id 607
    label "zmar&#322;y"
  ]
  node [
    id 608
    label "ofiarowanie"
  ]
  node [
    id 609
    label "ofiarowywanie"
  ]
  node [
    id 610
    label "T&#281;sknica"
  ]
  node [
    id 611
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 612
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 613
    label "miniatura"
  ]
  node [
    id 614
    label "przyroda"
  ]
  node [
    id 615
    label "odbicie"
  ]
  node [
    id 616
    label "atom"
  ]
  node [
    id 617
    label "kosmos"
  ]
  node [
    id 618
    label "Ziemia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
]
