graph [
  node [
    id 0
    label "wczorajszy"
    origin "text"
  ]
  node [
    id 1
    label "gazeta"
    origin "text"
  ]
  node [
    id 2
    label "wyborczy"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "o&#380;ywiaj&#261;cy"
    origin "text"
  ]
  node [
    id 5
    label "kultura"
    origin "text"
  ]
  node [
    id 6
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "prewencja"
    origin "text"
  ]
  node [
    id 9
    label "zabijaj&#261;cy"
    origin "text"
  ]
  node [
    id 10
    label "poradzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "dawny"
  ]
  node [
    id 12
    label "zestarzenie_si&#281;"
  ]
  node [
    id 13
    label "starzenie_si&#281;"
  ]
  node [
    id 14
    label "archaicznie"
  ]
  node [
    id 15
    label "zgrzybienie"
  ]
  node [
    id 16
    label "niedzisiejszy"
  ]
  node [
    id 17
    label "przestarzale"
  ]
  node [
    id 18
    label "stary"
  ]
  node [
    id 19
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 20
    label "ojciec"
  ]
  node [
    id 21
    label "nienowoczesny"
  ]
  node [
    id 22
    label "gruba_ryba"
  ]
  node [
    id 23
    label "poprzedni"
  ]
  node [
    id 24
    label "dawno"
  ]
  node [
    id 25
    label "staro"
  ]
  node [
    id 26
    label "m&#261;&#380;"
  ]
  node [
    id 27
    label "starzy"
  ]
  node [
    id 28
    label "dotychczasowy"
  ]
  node [
    id 29
    label "p&#243;&#378;ny"
  ]
  node [
    id 30
    label "d&#322;ugoletni"
  ]
  node [
    id 31
    label "charakterystyczny"
  ]
  node [
    id 32
    label "brat"
  ]
  node [
    id 33
    label "po_staro&#347;wiecku"
  ]
  node [
    id 34
    label "zwierzchnik"
  ]
  node [
    id 35
    label "znajomy"
  ]
  node [
    id 36
    label "odleg&#322;y"
  ]
  node [
    id 37
    label "starczo"
  ]
  node [
    id 38
    label "dawniej"
  ]
  node [
    id 39
    label "niegdysiejszy"
  ]
  node [
    id 40
    label "dojrza&#322;y"
  ]
  node [
    id 41
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 42
    label "przestarza&#322;y"
  ]
  node [
    id 43
    label "przesz&#322;y"
  ]
  node [
    id 44
    label "od_dawna"
  ]
  node [
    id 45
    label "anachroniczny"
  ]
  node [
    id 46
    label "wcze&#347;niejszy"
  ]
  node [
    id 47
    label "kombatant"
  ]
  node [
    id 48
    label "niemodnie"
  ]
  node [
    id 49
    label "zdezaktualizowanie_si&#281;"
  ]
  node [
    id 50
    label "zniedo&#322;&#281;&#380;nienie"
  ]
  node [
    id 51
    label "zramolenie"
  ]
  node [
    id 52
    label "postarzenie_si&#281;"
  ]
  node [
    id 53
    label "archaiczny"
  ]
  node [
    id 54
    label "tytu&#322;"
  ]
  node [
    id 55
    label "redakcja"
  ]
  node [
    id 56
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 57
    label "czasopismo"
  ]
  node [
    id 58
    label "prasa"
  ]
  node [
    id 59
    label "egzemplarz"
  ]
  node [
    id 60
    label "psychotest"
  ]
  node [
    id 61
    label "pismo"
  ]
  node [
    id 62
    label "communication"
  ]
  node [
    id 63
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 64
    label "wk&#322;ad"
  ]
  node [
    id 65
    label "zajawka"
  ]
  node [
    id 66
    label "ok&#322;adka"
  ]
  node [
    id 67
    label "Zwrotnica"
  ]
  node [
    id 68
    label "dzia&#322;"
  ]
  node [
    id 69
    label "redaktor"
  ]
  node [
    id 70
    label "radio"
  ]
  node [
    id 71
    label "zesp&#243;&#322;"
  ]
  node [
    id 72
    label "siedziba"
  ]
  node [
    id 73
    label "composition"
  ]
  node [
    id 74
    label "wydawnictwo"
  ]
  node [
    id 75
    label "redaction"
  ]
  node [
    id 76
    label "tekst"
  ]
  node [
    id 77
    label "telewizja"
  ]
  node [
    id 78
    label "obr&#243;bka"
  ]
  node [
    id 79
    label "debit"
  ]
  node [
    id 80
    label "druk"
  ]
  node [
    id 81
    label "publikacja"
  ]
  node [
    id 82
    label "nadtytu&#322;"
  ]
  node [
    id 83
    label "szata_graficzna"
  ]
  node [
    id 84
    label "tytulatura"
  ]
  node [
    id 85
    label "wydawa&#263;"
  ]
  node [
    id 86
    label "elevation"
  ]
  node [
    id 87
    label "wyda&#263;"
  ]
  node [
    id 88
    label "mianowaniec"
  ]
  node [
    id 89
    label "poster"
  ]
  node [
    id 90
    label "nazwa"
  ]
  node [
    id 91
    label "podtytu&#322;"
  ]
  node [
    id 92
    label "centerfold"
  ]
  node [
    id 93
    label "t&#322;oczysko"
  ]
  node [
    id 94
    label "depesza"
  ]
  node [
    id 95
    label "maszyna"
  ]
  node [
    id 96
    label "media"
  ]
  node [
    id 97
    label "napisa&#263;"
  ]
  node [
    id 98
    label "dziennikarz_prasowy"
  ]
  node [
    id 99
    label "pisa&#263;"
  ]
  node [
    id 100
    label "kiosk"
  ]
  node [
    id 101
    label "maszyna_rolnicza"
  ]
  node [
    id 102
    label "gaworzy&#263;"
  ]
  node [
    id 103
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 104
    label "remark"
  ]
  node [
    id 105
    label "rozmawia&#263;"
  ]
  node [
    id 106
    label "wyra&#380;a&#263;"
  ]
  node [
    id 107
    label "umie&#263;"
  ]
  node [
    id 108
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 109
    label "dziama&#263;"
  ]
  node [
    id 110
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 111
    label "formu&#322;owa&#263;"
  ]
  node [
    id 112
    label "dysfonia"
  ]
  node [
    id 113
    label "express"
  ]
  node [
    id 114
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 115
    label "talk"
  ]
  node [
    id 116
    label "u&#380;ywa&#263;"
  ]
  node [
    id 117
    label "prawi&#263;"
  ]
  node [
    id 118
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 119
    label "powiada&#263;"
  ]
  node [
    id 120
    label "tell"
  ]
  node [
    id 121
    label "chew_the_fat"
  ]
  node [
    id 122
    label "say"
  ]
  node [
    id 123
    label "j&#281;zyk"
  ]
  node [
    id 124
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 125
    label "informowa&#263;"
  ]
  node [
    id 126
    label "wydobywa&#263;"
  ]
  node [
    id 127
    label "okre&#347;la&#263;"
  ]
  node [
    id 128
    label "korzysta&#263;"
  ]
  node [
    id 129
    label "distribute"
  ]
  node [
    id 130
    label "give"
  ]
  node [
    id 131
    label "bash"
  ]
  node [
    id 132
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 133
    label "doznawa&#263;"
  ]
  node [
    id 134
    label "decydowa&#263;"
  ]
  node [
    id 135
    label "signify"
  ]
  node [
    id 136
    label "style"
  ]
  node [
    id 137
    label "powodowa&#263;"
  ]
  node [
    id 138
    label "komunikowa&#263;"
  ]
  node [
    id 139
    label "inform"
  ]
  node [
    id 140
    label "znaczy&#263;"
  ]
  node [
    id 141
    label "give_voice"
  ]
  node [
    id 142
    label "oznacza&#263;"
  ]
  node [
    id 143
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 144
    label "represent"
  ]
  node [
    id 145
    label "convey"
  ]
  node [
    id 146
    label "arouse"
  ]
  node [
    id 147
    label "robi&#263;"
  ]
  node [
    id 148
    label "determine"
  ]
  node [
    id 149
    label "work"
  ]
  node [
    id 150
    label "reakcja_chemiczna"
  ]
  node [
    id 151
    label "uwydatnia&#263;"
  ]
  node [
    id 152
    label "eksploatowa&#263;"
  ]
  node [
    id 153
    label "uzyskiwa&#263;"
  ]
  node [
    id 154
    label "wydostawa&#263;"
  ]
  node [
    id 155
    label "wyjmowa&#263;"
  ]
  node [
    id 156
    label "train"
  ]
  node [
    id 157
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 158
    label "dobywa&#263;"
  ]
  node [
    id 159
    label "ocala&#263;"
  ]
  node [
    id 160
    label "excavate"
  ]
  node [
    id 161
    label "g&#243;rnictwo"
  ]
  node [
    id 162
    label "raise"
  ]
  node [
    id 163
    label "wiedzie&#263;"
  ]
  node [
    id 164
    label "can"
  ]
  node [
    id 165
    label "m&#243;c"
  ]
  node [
    id 166
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 167
    label "rozumie&#263;"
  ]
  node [
    id 168
    label "szczeka&#263;"
  ]
  node [
    id 169
    label "funkcjonowa&#263;"
  ]
  node [
    id 170
    label "mawia&#263;"
  ]
  node [
    id 171
    label "opowiada&#263;"
  ]
  node [
    id 172
    label "chatter"
  ]
  node [
    id 173
    label "niemowl&#281;"
  ]
  node [
    id 174
    label "kosmetyk"
  ]
  node [
    id 175
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 176
    label "stanowisko_archeologiczne"
  ]
  node [
    id 177
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 178
    label "artykulator"
  ]
  node [
    id 179
    label "kod"
  ]
  node [
    id 180
    label "kawa&#322;ek"
  ]
  node [
    id 181
    label "przedmiot"
  ]
  node [
    id 182
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 183
    label "gramatyka"
  ]
  node [
    id 184
    label "stylik"
  ]
  node [
    id 185
    label "przet&#322;umaczenie"
  ]
  node [
    id 186
    label "formalizowanie"
  ]
  node [
    id 187
    label "ssanie"
  ]
  node [
    id 188
    label "ssa&#263;"
  ]
  node [
    id 189
    label "language"
  ]
  node [
    id 190
    label "liza&#263;"
  ]
  node [
    id 191
    label "konsonantyzm"
  ]
  node [
    id 192
    label "wokalizm"
  ]
  node [
    id 193
    label "fonetyka"
  ]
  node [
    id 194
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 195
    label "jeniec"
  ]
  node [
    id 196
    label "but"
  ]
  node [
    id 197
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 198
    label "po_koroniarsku"
  ]
  node [
    id 199
    label "kultura_duchowa"
  ]
  node [
    id 200
    label "t&#322;umaczenie"
  ]
  node [
    id 201
    label "m&#243;wienie"
  ]
  node [
    id 202
    label "pype&#263;"
  ]
  node [
    id 203
    label "lizanie"
  ]
  node [
    id 204
    label "formalizowa&#263;"
  ]
  node [
    id 205
    label "organ"
  ]
  node [
    id 206
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 207
    label "rozumienie"
  ]
  node [
    id 208
    label "spos&#243;b"
  ]
  node [
    id 209
    label "makroglosja"
  ]
  node [
    id 210
    label "jama_ustna"
  ]
  node [
    id 211
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 212
    label "formacja_geologiczna"
  ]
  node [
    id 213
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 214
    label "natural_language"
  ]
  node [
    id 215
    label "s&#322;ownictwo"
  ]
  node [
    id 216
    label "urz&#261;dzenie"
  ]
  node [
    id 217
    label "dysphonia"
  ]
  node [
    id 218
    label "dysleksja"
  ]
  node [
    id 219
    label "asymilowanie_si&#281;"
  ]
  node [
    id 220
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 221
    label "Wsch&#243;d"
  ]
  node [
    id 222
    label "praca_rolnicza"
  ]
  node [
    id 223
    label "przejmowanie"
  ]
  node [
    id 224
    label "zjawisko"
  ]
  node [
    id 225
    label "cecha"
  ]
  node [
    id 226
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 227
    label "makrokosmos"
  ]
  node [
    id 228
    label "rzecz"
  ]
  node [
    id 229
    label "konwencja"
  ]
  node [
    id 230
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 231
    label "propriety"
  ]
  node [
    id 232
    label "przejmowa&#263;"
  ]
  node [
    id 233
    label "brzoskwiniarnia"
  ]
  node [
    id 234
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 235
    label "sztuka"
  ]
  node [
    id 236
    label "zwyczaj"
  ]
  node [
    id 237
    label "jako&#347;&#263;"
  ]
  node [
    id 238
    label "kuchnia"
  ]
  node [
    id 239
    label "tradycja"
  ]
  node [
    id 240
    label "populace"
  ]
  node [
    id 241
    label "hodowla"
  ]
  node [
    id 242
    label "religia"
  ]
  node [
    id 243
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 244
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 245
    label "przej&#281;cie"
  ]
  node [
    id 246
    label "przej&#261;&#263;"
  ]
  node [
    id 247
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 248
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 249
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 250
    label "warto&#347;&#263;"
  ]
  node [
    id 251
    label "quality"
  ]
  node [
    id 252
    label "co&#347;"
  ]
  node [
    id 253
    label "state"
  ]
  node [
    id 254
    label "syf"
  ]
  node [
    id 255
    label "absolutorium"
  ]
  node [
    id 256
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 257
    label "dzia&#322;anie"
  ]
  node [
    id 258
    label "activity"
  ]
  node [
    id 259
    label "proces"
  ]
  node [
    id 260
    label "boski"
  ]
  node [
    id 261
    label "krajobraz"
  ]
  node [
    id 262
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 263
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 264
    label "przywidzenie"
  ]
  node [
    id 265
    label "presence"
  ]
  node [
    id 266
    label "charakter"
  ]
  node [
    id 267
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 268
    label "potrzymanie"
  ]
  node [
    id 269
    label "rolnictwo"
  ]
  node [
    id 270
    label "pod&#243;j"
  ]
  node [
    id 271
    label "filiacja"
  ]
  node [
    id 272
    label "licencjonowanie"
  ]
  node [
    id 273
    label "opasa&#263;"
  ]
  node [
    id 274
    label "ch&#243;w"
  ]
  node [
    id 275
    label "licencja"
  ]
  node [
    id 276
    label "sokolarnia"
  ]
  node [
    id 277
    label "potrzyma&#263;"
  ]
  node [
    id 278
    label "rozp&#322;&#243;d"
  ]
  node [
    id 279
    label "grupa_organizm&#243;w"
  ]
  node [
    id 280
    label "wypas"
  ]
  node [
    id 281
    label "wychowalnia"
  ]
  node [
    id 282
    label "pstr&#261;garnia"
  ]
  node [
    id 283
    label "krzy&#380;owanie"
  ]
  node [
    id 284
    label "licencjonowa&#263;"
  ]
  node [
    id 285
    label "odch&#243;w"
  ]
  node [
    id 286
    label "tucz"
  ]
  node [
    id 287
    label "ud&#243;j"
  ]
  node [
    id 288
    label "klatka"
  ]
  node [
    id 289
    label "opasienie"
  ]
  node [
    id 290
    label "wych&#243;w"
  ]
  node [
    id 291
    label "obrz&#261;dek"
  ]
  node [
    id 292
    label "opasanie"
  ]
  node [
    id 293
    label "polish"
  ]
  node [
    id 294
    label "akwarium"
  ]
  node [
    id 295
    label "biotechnika"
  ]
  node [
    id 296
    label "charakterystyka"
  ]
  node [
    id 297
    label "m&#322;ot"
  ]
  node [
    id 298
    label "znak"
  ]
  node [
    id 299
    label "drzewo"
  ]
  node [
    id 300
    label "pr&#243;ba"
  ]
  node [
    id 301
    label "attribute"
  ]
  node [
    id 302
    label "marka"
  ]
  node [
    id 303
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 304
    label "zbi&#243;r"
  ]
  node [
    id 305
    label "uk&#322;ad"
  ]
  node [
    id 306
    label "styl"
  ]
  node [
    id 307
    label "line"
  ]
  node [
    id 308
    label "kanon"
  ]
  node [
    id 309
    label "zjazd"
  ]
  node [
    id 310
    label "biom"
  ]
  node [
    id 311
    label "szata_ro&#347;linna"
  ]
  node [
    id 312
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 313
    label "formacja_ro&#347;linna"
  ]
  node [
    id 314
    label "przyroda"
  ]
  node [
    id 315
    label "zielono&#347;&#263;"
  ]
  node [
    id 316
    label "pi&#281;tro"
  ]
  node [
    id 317
    label "plant"
  ]
  node [
    id 318
    label "ro&#347;lina"
  ]
  node [
    id 319
    label "geosystem"
  ]
  node [
    id 320
    label "pr&#243;bowanie"
  ]
  node [
    id 321
    label "rola"
  ]
  node [
    id 322
    label "cz&#322;owiek"
  ]
  node [
    id 323
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 324
    label "realizacja"
  ]
  node [
    id 325
    label "scena"
  ]
  node [
    id 326
    label "didaskalia"
  ]
  node [
    id 327
    label "czyn"
  ]
  node [
    id 328
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 329
    label "environment"
  ]
  node [
    id 330
    label "head"
  ]
  node [
    id 331
    label "scenariusz"
  ]
  node [
    id 332
    label "jednostka"
  ]
  node [
    id 333
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 334
    label "utw&#243;r"
  ]
  node [
    id 335
    label "fortel"
  ]
  node [
    id 336
    label "theatrical_performance"
  ]
  node [
    id 337
    label "ambala&#380;"
  ]
  node [
    id 338
    label "sprawno&#347;&#263;"
  ]
  node [
    id 339
    label "kobieta"
  ]
  node [
    id 340
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 341
    label "Faust"
  ]
  node [
    id 342
    label "scenografia"
  ]
  node [
    id 343
    label "ods&#322;ona"
  ]
  node [
    id 344
    label "turn"
  ]
  node [
    id 345
    label "pokaz"
  ]
  node [
    id 346
    label "ilo&#347;&#263;"
  ]
  node [
    id 347
    label "przedstawienie"
  ]
  node [
    id 348
    label "przedstawi&#263;"
  ]
  node [
    id 349
    label "Apollo"
  ]
  node [
    id 350
    label "przedstawianie"
  ]
  node [
    id 351
    label "przedstawia&#263;"
  ]
  node [
    id 352
    label "towar"
  ]
  node [
    id 353
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 354
    label "zachowanie"
  ]
  node [
    id 355
    label "ceremony"
  ]
  node [
    id 356
    label "kult"
  ]
  node [
    id 357
    label "wyznanie"
  ]
  node [
    id 358
    label "mitologia"
  ]
  node [
    id 359
    label "ideologia"
  ]
  node [
    id 360
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 361
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 362
    label "nawracanie_si&#281;"
  ]
  node [
    id 363
    label "duchowny"
  ]
  node [
    id 364
    label "rela"
  ]
  node [
    id 365
    label "kosmologia"
  ]
  node [
    id 366
    label "kosmogonia"
  ]
  node [
    id 367
    label "nawraca&#263;"
  ]
  node [
    id 368
    label "mistyka"
  ]
  node [
    id 369
    label "staro&#347;cina_weselna"
  ]
  node [
    id 370
    label "folklor"
  ]
  node [
    id 371
    label "objawienie"
  ]
  node [
    id 372
    label "dorobek"
  ]
  node [
    id 373
    label "tworzenie"
  ]
  node [
    id 374
    label "kreacja"
  ]
  node [
    id 375
    label "creation"
  ]
  node [
    id 376
    label "zaj&#281;cie"
  ]
  node [
    id 377
    label "instytucja"
  ]
  node [
    id 378
    label "tajniki"
  ]
  node [
    id 379
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 380
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 381
    label "jedzenie"
  ]
  node [
    id 382
    label "zaplecze"
  ]
  node [
    id 383
    label "pomieszczenie"
  ]
  node [
    id 384
    label "zlewozmywak"
  ]
  node [
    id 385
    label "gotowa&#263;"
  ]
  node [
    id 386
    label "ciemna_materia"
  ]
  node [
    id 387
    label "planeta"
  ]
  node [
    id 388
    label "mikrokosmos"
  ]
  node [
    id 389
    label "ekosfera"
  ]
  node [
    id 390
    label "przestrze&#324;"
  ]
  node [
    id 391
    label "czarna_dziura"
  ]
  node [
    id 392
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 393
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 394
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 395
    label "kosmos"
  ]
  node [
    id 396
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 397
    label "poprawno&#347;&#263;"
  ]
  node [
    id 398
    label "og&#322;ada"
  ]
  node [
    id 399
    label "service"
  ]
  node [
    id 400
    label "stosowno&#347;&#263;"
  ]
  node [
    id 401
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 402
    label "Ukraina"
  ]
  node [
    id 403
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 404
    label "blok_wschodni"
  ]
  node [
    id 405
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 406
    label "wsch&#243;d"
  ]
  node [
    id 407
    label "Europa_Wschodnia"
  ]
  node [
    id 408
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 409
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 410
    label "treat"
  ]
  node [
    id 411
    label "czerpa&#263;"
  ]
  node [
    id 412
    label "bra&#263;"
  ]
  node [
    id 413
    label "go"
  ]
  node [
    id 414
    label "handle"
  ]
  node [
    id 415
    label "wzbudza&#263;"
  ]
  node [
    id 416
    label "ogarnia&#263;"
  ]
  node [
    id 417
    label "bang"
  ]
  node [
    id 418
    label "wzi&#261;&#263;"
  ]
  node [
    id 419
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 420
    label "stimulate"
  ]
  node [
    id 421
    label "ogarn&#261;&#263;"
  ]
  node [
    id 422
    label "wzbudzi&#263;"
  ]
  node [
    id 423
    label "thrill"
  ]
  node [
    id 424
    label "czerpanie"
  ]
  node [
    id 425
    label "acquisition"
  ]
  node [
    id 426
    label "branie"
  ]
  node [
    id 427
    label "caparison"
  ]
  node [
    id 428
    label "movement"
  ]
  node [
    id 429
    label "wzbudzanie"
  ]
  node [
    id 430
    label "czynno&#347;&#263;"
  ]
  node [
    id 431
    label "ogarnianie"
  ]
  node [
    id 432
    label "wra&#380;enie"
  ]
  node [
    id 433
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 434
    label "interception"
  ]
  node [
    id 435
    label "wzbudzenie"
  ]
  node [
    id 436
    label "emotion"
  ]
  node [
    id 437
    label "zaczerpni&#281;cie"
  ]
  node [
    id 438
    label "wzi&#281;cie"
  ]
  node [
    id 439
    label "zboczenie"
  ]
  node [
    id 440
    label "om&#243;wienie"
  ]
  node [
    id 441
    label "sponiewieranie"
  ]
  node [
    id 442
    label "discipline"
  ]
  node [
    id 443
    label "omawia&#263;"
  ]
  node [
    id 444
    label "kr&#261;&#380;enie"
  ]
  node [
    id 445
    label "tre&#347;&#263;"
  ]
  node [
    id 446
    label "robienie"
  ]
  node [
    id 447
    label "sponiewiera&#263;"
  ]
  node [
    id 448
    label "element"
  ]
  node [
    id 449
    label "entity"
  ]
  node [
    id 450
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 451
    label "tematyka"
  ]
  node [
    id 452
    label "w&#261;tek"
  ]
  node [
    id 453
    label "zbaczanie"
  ]
  node [
    id 454
    label "program_nauczania"
  ]
  node [
    id 455
    label "om&#243;wi&#263;"
  ]
  node [
    id 456
    label "omawianie"
  ]
  node [
    id 457
    label "thing"
  ]
  node [
    id 458
    label "istota"
  ]
  node [
    id 459
    label "zbacza&#263;"
  ]
  node [
    id 460
    label "zboczy&#263;"
  ]
  node [
    id 461
    label "object"
  ]
  node [
    id 462
    label "temat"
  ]
  node [
    id 463
    label "wpadni&#281;cie"
  ]
  node [
    id 464
    label "mienie"
  ]
  node [
    id 465
    label "obiekt"
  ]
  node [
    id 466
    label "wpa&#347;&#263;"
  ]
  node [
    id 467
    label "wpadanie"
  ]
  node [
    id 468
    label "wpada&#263;"
  ]
  node [
    id 469
    label "miejsce"
  ]
  node [
    id 470
    label "uprawa"
  ]
  node [
    id 471
    label "post&#261;pi&#263;"
  ]
  node [
    id 472
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 473
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 474
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 475
    label "zorganizowa&#263;"
  ]
  node [
    id 476
    label "appoint"
  ]
  node [
    id 477
    label "wystylizowa&#263;"
  ]
  node [
    id 478
    label "cause"
  ]
  node [
    id 479
    label "przerobi&#263;"
  ]
  node [
    id 480
    label "nabra&#263;"
  ]
  node [
    id 481
    label "make"
  ]
  node [
    id 482
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 483
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 484
    label "wydali&#263;"
  ]
  node [
    id 485
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 486
    label "advance"
  ]
  node [
    id 487
    label "act"
  ]
  node [
    id 488
    label "see"
  ]
  node [
    id 489
    label "usun&#261;&#263;"
  ]
  node [
    id 490
    label "sack"
  ]
  node [
    id 491
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 492
    label "restore"
  ]
  node [
    id 493
    label "dostosowa&#263;"
  ]
  node [
    id 494
    label "pozyska&#263;"
  ]
  node [
    id 495
    label "stworzy&#263;"
  ]
  node [
    id 496
    label "plan"
  ]
  node [
    id 497
    label "stage"
  ]
  node [
    id 498
    label "urobi&#263;"
  ]
  node [
    id 499
    label "ensnare"
  ]
  node [
    id 500
    label "wprowadzi&#263;"
  ]
  node [
    id 501
    label "zaplanowa&#263;"
  ]
  node [
    id 502
    label "przygotowa&#263;"
  ]
  node [
    id 503
    label "standard"
  ]
  node [
    id 504
    label "skupi&#263;"
  ]
  node [
    id 505
    label "podbi&#263;"
  ]
  node [
    id 506
    label "umocni&#263;"
  ]
  node [
    id 507
    label "doprowadzi&#263;"
  ]
  node [
    id 508
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 509
    label "zadowoli&#263;"
  ]
  node [
    id 510
    label "accommodate"
  ]
  node [
    id 511
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 512
    label "zabezpieczy&#263;"
  ]
  node [
    id 513
    label "wytworzy&#263;"
  ]
  node [
    id 514
    label "pomy&#347;le&#263;"
  ]
  node [
    id 515
    label "woda"
  ]
  node [
    id 516
    label "hoax"
  ]
  node [
    id 517
    label "deceive"
  ]
  node [
    id 518
    label "oszwabi&#263;"
  ]
  node [
    id 519
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 520
    label "objecha&#263;"
  ]
  node [
    id 521
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 522
    label "gull"
  ]
  node [
    id 523
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 524
    label "naby&#263;"
  ]
  node [
    id 525
    label "fraud"
  ]
  node [
    id 526
    label "kupi&#263;"
  ]
  node [
    id 527
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 528
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 529
    label "stylize"
  ]
  node [
    id 530
    label "nada&#263;"
  ]
  node [
    id 531
    label "upodobni&#263;"
  ]
  node [
    id 532
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 533
    label "zaliczy&#263;"
  ]
  node [
    id 534
    label "overwork"
  ]
  node [
    id 535
    label "zamieni&#263;"
  ]
  node [
    id 536
    label "zmodyfikowa&#263;"
  ]
  node [
    id 537
    label "change"
  ]
  node [
    id 538
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 539
    label "przej&#347;&#263;"
  ]
  node [
    id 540
    label "zmieni&#263;"
  ]
  node [
    id 541
    label "convert"
  ]
  node [
    id 542
    label "prze&#380;y&#263;"
  ]
  node [
    id 543
    label "przetworzy&#263;"
  ]
  node [
    id 544
    label "upora&#263;_si&#281;"
  ]
  node [
    id 545
    label "sprawi&#263;"
  ]
  node [
    id 546
    label "restraint"
  ]
  node [
    id 547
    label "ochrona"
  ]
  node [
    id 548
    label "formacja"
  ]
  node [
    id 549
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 550
    label "obstawianie"
  ]
  node [
    id 551
    label "obstawienie"
  ]
  node [
    id 552
    label "tarcza"
  ]
  node [
    id 553
    label "ubezpieczenie"
  ]
  node [
    id 554
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 555
    label "transportacja"
  ]
  node [
    id 556
    label "obstawia&#263;"
  ]
  node [
    id 557
    label "borowiec"
  ]
  node [
    id 558
    label "chemical_bond"
  ]
  node [
    id 559
    label "guidance"
  ]
  node [
    id 560
    label "rede"
  ]
  node [
    id 561
    label "pom&#243;c"
  ]
  node [
    id 562
    label "udzieli&#263;"
  ]
  node [
    id 563
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 564
    label "aid"
  ]
  node [
    id 565
    label "concur"
  ]
  node [
    id 566
    label "help"
  ]
  node [
    id 567
    label "u&#322;atwi&#263;"
  ]
  node [
    id 568
    label "zaskutkowa&#263;"
  ]
  node [
    id 569
    label "da&#263;"
  ]
  node [
    id 570
    label "udost&#281;pni&#263;"
  ]
  node [
    id 571
    label "przyzna&#263;"
  ]
  node [
    id 572
    label "picture"
  ]
  node [
    id 573
    label "promocja"
  ]
  node [
    id 574
    label "odst&#261;pi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
]
