graph [
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "dyplomacja"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zmobilizowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "obrzuca&#263;"
    origin "text"
  ]
  node [
    id 5
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "inwektywa"
    origin "text"
  ]
  node [
    id 7
    label "niemiecki"
    origin "text"
  ]
  node [
    id 8
    label "prasa"
    origin "text"
  ]
  node [
    id 9
    label "domaga&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "sankcja"
    origin "text"
  ]
  node [
    id 12
    label "wobec"
    origin "text"
  ]
  node [
    id 13
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 14
    label "jak"
    origin "text"
  ]
  node [
    id 15
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 16
    label "kraj"
    origin "text"
  ]
  node [
    id 17
    label "cz&#322;onkowski"
    origin "text"
  ]
  node [
    id 18
    label "unia"
    origin "text"
  ]
  node [
    id 19
    label "europejski"
    origin "text"
  ]
  node [
    id 20
    label "powa&#380;nie"
    origin "text"
  ]
  node [
    id 21
    label "grozi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "wi&#281;zienie"
    origin "text"
  ]
  node [
    id 23
    label "krytyka"
    origin "text"
  ]
  node [
    id 24
    label "osobisto&#347;&#263;"
    origin "text"
  ]
  node [
    id 25
    label "polityczny"
    origin "text"
  ]
  node [
    id 26
    label "polski"
    origin "text"
  ]
  node [
    id 27
    label "prawo"
    origin "text"
  ]
  node [
    id 28
    label "przewiduj&#261;cy"
    origin "text"
  ]
  node [
    id 29
    label "kar"
    origin "text"
  ]
  node [
    id 30
    label "wykroczenie"
    origin "text"
  ]
  node [
    id 31
    label "prasowy"
    origin "text"
  ]
  node [
    id 32
    label "by&#263;"
    origin "text"
  ]
  node [
    id 33
    label "sprzeczny"
    origin "text"
  ]
  node [
    id 34
    label "standard"
    origin "text"
  ]
  node [
    id 35
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 36
    label "znowelizowa&#263;"
    origin "text"
  ]
  node [
    id 37
    label "zacofany"
    origin "text"
  ]
  node [
    id 38
    label "przepis"
    origin "text"
  ]
  node [
    id 39
    label "bez"
    origin "text"
  ]
  node [
    id 40
    label "granica"
    origin "text"
  ]
  node [
    id 41
    label "statesmanship"
  ]
  node [
    id 42
    label "notyfikowa&#263;"
  ]
  node [
    id 43
    label "corps"
  ]
  node [
    id 44
    label "notyfikowanie"
  ]
  node [
    id 45
    label "korpus_dyplomatyczny"
  ]
  node [
    id 46
    label "nastawienie"
  ]
  node [
    id 47
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 48
    label "grupa"
  ]
  node [
    id 49
    label "polityka"
  ]
  node [
    id 50
    label "absolutorium"
  ]
  node [
    id 51
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 52
    label "dzia&#322;anie"
  ]
  node [
    id 53
    label "activity"
  ]
  node [
    id 54
    label "odm&#322;adzanie"
  ]
  node [
    id 55
    label "liga"
  ]
  node [
    id 56
    label "jednostka_systematyczna"
  ]
  node [
    id 57
    label "asymilowanie"
  ]
  node [
    id 58
    label "gromada"
  ]
  node [
    id 59
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 60
    label "asymilowa&#263;"
  ]
  node [
    id 61
    label "egzemplarz"
  ]
  node [
    id 62
    label "Entuzjastki"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "kompozycja"
  ]
  node [
    id 65
    label "Terranie"
  ]
  node [
    id 66
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 67
    label "category"
  ]
  node [
    id 68
    label "pakiet_klimatyczny"
  ]
  node [
    id 69
    label "oddzia&#322;"
  ]
  node [
    id 70
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 71
    label "cz&#261;steczka"
  ]
  node [
    id 72
    label "stage_set"
  ]
  node [
    id 73
    label "type"
  ]
  node [
    id 74
    label "specgrupa"
  ]
  node [
    id 75
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 76
    label "&#346;wietliki"
  ]
  node [
    id 77
    label "odm&#322;odzenie"
  ]
  node [
    id 78
    label "Eurogrupa"
  ]
  node [
    id 79
    label "odm&#322;adza&#263;"
  ]
  node [
    id 80
    label "formacja_geologiczna"
  ]
  node [
    id 81
    label "harcerze_starsi"
  ]
  node [
    id 82
    label "ustawienie"
  ]
  node [
    id 83
    label "z&#322;amanie"
  ]
  node [
    id 84
    label "set"
  ]
  node [
    id 85
    label "gotowanie_si&#281;"
  ]
  node [
    id 86
    label "oddzia&#322;anie"
  ]
  node [
    id 87
    label "ponastawianie"
  ]
  node [
    id 88
    label "bearing"
  ]
  node [
    id 89
    label "powaga"
  ]
  node [
    id 90
    label "z&#322;o&#380;enie"
  ]
  node [
    id 91
    label "podej&#347;cie"
  ]
  node [
    id 92
    label "umieszczenie"
  ]
  node [
    id 93
    label "cecha"
  ]
  node [
    id 94
    label "w&#322;&#261;czenie"
  ]
  node [
    id 95
    label "ukierunkowanie"
  ]
  node [
    id 96
    label "metoda"
  ]
  node [
    id 97
    label "policy"
  ]
  node [
    id 98
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 99
    label "czek"
  ]
  node [
    id 100
    label "poinformowa&#263;"
  ]
  node [
    id 101
    label "donosi&#263;"
  ]
  node [
    id 102
    label "donie&#347;&#263;"
  ]
  node [
    id 103
    label "informowa&#263;"
  ]
  node [
    id 104
    label "odm&#243;wienie"
  ]
  node [
    id 105
    label "advise"
  ]
  node [
    id 106
    label "donoszenie"
  ]
  node [
    id 107
    label "informowanie"
  ]
  node [
    id 108
    label "poinformowanie"
  ]
  node [
    id 109
    label "doniesienie"
  ]
  node [
    id 110
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 111
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 112
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 113
    label "osta&#263;_si&#281;"
  ]
  node [
    id 114
    label "change"
  ]
  node [
    id 115
    label "pozosta&#263;"
  ]
  node [
    id 116
    label "catch"
  ]
  node [
    id 117
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 118
    label "proceed"
  ]
  node [
    id 119
    label "support"
  ]
  node [
    id 120
    label "prze&#380;y&#263;"
  ]
  node [
    id 121
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 122
    label "nastawi&#263;"
  ]
  node [
    id 123
    label "wojsko"
  ]
  node [
    id 124
    label "wake_up"
  ]
  node [
    id 125
    label "powo&#322;a&#263;"
  ]
  node [
    id 126
    label "postawi&#263;_w_stan_pogotowia"
  ]
  node [
    id 127
    label "pool"
  ]
  node [
    id 128
    label "pobudzi&#263;"
  ]
  node [
    id 129
    label "przygotowa&#263;"
  ]
  node [
    id 130
    label "umie&#347;ci&#263;"
  ]
  node [
    id 131
    label "plant"
  ]
  node [
    id 132
    label "ustawi&#263;"
  ]
  node [
    id 133
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 134
    label "direct"
  ]
  node [
    id 135
    label "aim"
  ]
  node [
    id 136
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 137
    label "poprawi&#263;"
  ]
  node [
    id 138
    label "przyrz&#261;dzi&#263;"
  ]
  node [
    id 139
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 140
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 141
    label "wyznaczy&#263;"
  ]
  node [
    id 142
    label "move"
  ]
  node [
    id 143
    label "zach&#281;ci&#263;"
  ]
  node [
    id 144
    label "go"
  ]
  node [
    id 145
    label "spowodowa&#263;"
  ]
  node [
    id 146
    label "wzm&#243;c"
  ]
  node [
    id 147
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 148
    label "wykona&#263;"
  ]
  node [
    id 149
    label "cook"
  ]
  node [
    id 150
    label "wyszkoli&#263;"
  ]
  node [
    id 151
    label "train"
  ]
  node [
    id 152
    label "arrange"
  ]
  node [
    id 153
    label "zrobi&#263;"
  ]
  node [
    id 154
    label "wytworzy&#263;"
  ]
  node [
    id 155
    label "dress"
  ]
  node [
    id 156
    label "ukierunkowa&#263;"
  ]
  node [
    id 157
    label "appoint"
  ]
  node [
    id 158
    label "poborowy"
  ]
  node [
    id 159
    label "wskaza&#263;"
  ]
  node [
    id 160
    label "porozumienie"
  ]
  node [
    id 161
    label "zrejterowanie"
  ]
  node [
    id 162
    label "przedmiot"
  ]
  node [
    id 163
    label "dezerter"
  ]
  node [
    id 164
    label "oddzia&#322;_karny"
  ]
  node [
    id 165
    label "rezerwa"
  ]
  node [
    id 166
    label "tabor"
  ]
  node [
    id 167
    label "wermacht"
  ]
  node [
    id 168
    label "cofni&#281;cie"
  ]
  node [
    id 169
    label "potencja"
  ]
  node [
    id 170
    label "fala"
  ]
  node [
    id 171
    label "struktura"
  ]
  node [
    id 172
    label "szko&#322;a"
  ]
  node [
    id 173
    label "korpus"
  ]
  node [
    id 174
    label "soldateska"
  ]
  node [
    id 175
    label "ods&#322;ugiwanie"
  ]
  node [
    id 176
    label "werbowanie_si&#281;"
  ]
  node [
    id 177
    label "zdemobilizowanie"
  ]
  node [
    id 178
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 179
    label "s&#322;u&#380;ba"
  ]
  node [
    id 180
    label "or&#281;&#380;"
  ]
  node [
    id 181
    label "Legia_Cudzoziemska"
  ]
  node [
    id 182
    label "Armia_Czerwona"
  ]
  node [
    id 183
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 184
    label "rejterowanie"
  ]
  node [
    id 185
    label "Czerwona_Gwardia"
  ]
  node [
    id 186
    label "si&#322;a"
  ]
  node [
    id 187
    label "zrejterowa&#263;"
  ]
  node [
    id 188
    label "sztabslekarz"
  ]
  node [
    id 189
    label "zmobilizowanie"
  ]
  node [
    id 190
    label "wojo"
  ]
  node [
    id 191
    label "pospolite_ruszenie"
  ]
  node [
    id 192
    label "Eurokorpus"
  ]
  node [
    id 193
    label "mobilizowanie"
  ]
  node [
    id 194
    label "rejterowa&#263;"
  ]
  node [
    id 195
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 196
    label "mobilizowa&#263;"
  ]
  node [
    id 197
    label "Armia_Krajowa"
  ]
  node [
    id 198
    label "obrona"
  ]
  node [
    id 199
    label "dryl"
  ]
  node [
    id 200
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 201
    label "petarda"
  ]
  node [
    id 202
    label "pozycja"
  ]
  node [
    id 203
    label "zdemobilizowa&#263;"
  ]
  node [
    id 204
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 205
    label "bombardowa&#263;"
  ]
  node [
    id 206
    label "obsypywa&#263;"
  ]
  node [
    id 207
    label "hide"
  ]
  node [
    id 208
    label "napada&#263;"
  ]
  node [
    id 209
    label "pipe"
  ]
  node [
    id 210
    label "pepper"
  ]
  node [
    id 211
    label "obr&#281;bia&#263;"
  ]
  node [
    id 212
    label "rzuca&#263;"
  ]
  node [
    id 213
    label "pour"
  ]
  node [
    id 214
    label "pokrywa&#263;"
  ]
  node [
    id 215
    label "m&#243;wi&#263;"
  ]
  node [
    id 216
    label "overwhelm"
  ]
  node [
    id 217
    label "obdarowywa&#263;"
  ]
  node [
    id 218
    label "attack"
  ]
  node [
    id 219
    label "piratowa&#263;"
  ]
  node [
    id 220
    label "atakowa&#263;"
  ]
  node [
    id 221
    label "krytykowa&#263;"
  ]
  node [
    id 222
    label "dopada&#263;"
  ]
  node [
    id 223
    label "zabezpiecza&#263;"
  ]
  node [
    id 224
    label "hem"
  ]
  node [
    id 225
    label "obrze&#380;a&#263;"
  ]
  node [
    id 226
    label "obrabia&#263;"
  ]
  node [
    id 227
    label "opuszcza&#263;"
  ]
  node [
    id 228
    label "porusza&#263;"
  ]
  node [
    id 229
    label "grzmoci&#263;"
  ]
  node [
    id 230
    label "most"
  ]
  node [
    id 231
    label "wyzwanie"
  ]
  node [
    id 232
    label "konstruowa&#263;"
  ]
  node [
    id 233
    label "spring"
  ]
  node [
    id 234
    label "rush"
  ]
  node [
    id 235
    label "odchodzi&#263;"
  ]
  node [
    id 236
    label "unwrap"
  ]
  node [
    id 237
    label "rusza&#263;"
  ]
  node [
    id 238
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 239
    label "&#347;wiat&#322;o"
  ]
  node [
    id 240
    label "przestawa&#263;"
  ]
  node [
    id 241
    label "przemieszcza&#263;"
  ]
  node [
    id 242
    label "flip"
  ]
  node [
    id 243
    label "bequeath"
  ]
  node [
    id 244
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 245
    label "podejrzenie"
  ]
  node [
    id 246
    label "przewraca&#263;"
  ]
  node [
    id 247
    label "czar"
  ]
  node [
    id 248
    label "cie&#324;"
  ]
  node [
    id 249
    label "zmienia&#263;"
  ]
  node [
    id 250
    label "syga&#263;"
  ]
  node [
    id 251
    label "tug"
  ]
  node [
    id 252
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 253
    label "towar"
  ]
  node [
    id 254
    label "cover"
  ]
  node [
    id 255
    label "bombard"
  ]
  node [
    id 256
    label "niepokoi&#263;"
  ]
  node [
    id 257
    label "bomb_calorimeter"
  ]
  node [
    id 258
    label "pieski"
  ]
  node [
    id 259
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 260
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 261
    label "niekorzystny"
  ]
  node [
    id 262
    label "z&#322;oszczenie"
  ]
  node [
    id 263
    label "sierdzisty"
  ]
  node [
    id 264
    label "niegrzeczny"
  ]
  node [
    id 265
    label "zez&#322;oszczenie"
  ]
  node [
    id 266
    label "zdenerwowany"
  ]
  node [
    id 267
    label "negatywny"
  ]
  node [
    id 268
    label "rozgniewanie"
  ]
  node [
    id 269
    label "gniewanie"
  ]
  node [
    id 270
    label "niemoralny"
  ]
  node [
    id 271
    label "&#378;le"
  ]
  node [
    id 272
    label "niepomy&#347;lny"
  ]
  node [
    id 273
    label "syf"
  ]
  node [
    id 274
    label "niespokojny"
  ]
  node [
    id 275
    label "niekorzystnie"
  ]
  node [
    id 276
    label "ujemny"
  ]
  node [
    id 277
    label "nagannie"
  ]
  node [
    id 278
    label "niemoralnie"
  ]
  node [
    id 279
    label "nieprzyzwoity"
  ]
  node [
    id 280
    label "niepomy&#347;lnie"
  ]
  node [
    id 281
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 282
    label "nieodpowiednio"
  ]
  node [
    id 283
    label "r&#243;&#380;ny"
  ]
  node [
    id 284
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 285
    label "swoisty"
  ]
  node [
    id 286
    label "nienale&#380;yty"
  ]
  node [
    id 287
    label "dziwny"
  ]
  node [
    id 288
    label "niezno&#347;ny"
  ]
  node [
    id 289
    label "niegrzecznie"
  ]
  node [
    id 290
    label "trudny"
  ]
  node [
    id 291
    label "niestosowny"
  ]
  node [
    id 292
    label "brzydal"
  ]
  node [
    id 293
    label "niepos&#322;uszny"
  ]
  node [
    id 294
    label "negatywnie"
  ]
  node [
    id 295
    label "nieprzyjemny"
  ]
  node [
    id 296
    label "ujemnie"
  ]
  node [
    id 297
    label "gniewny"
  ]
  node [
    id 298
    label "serdeczny"
  ]
  node [
    id 299
    label "srogi"
  ]
  node [
    id 300
    label "sierdzi&#347;cie"
  ]
  node [
    id 301
    label "piesko"
  ]
  node [
    id 302
    label "wzbudzanie"
  ]
  node [
    id 303
    label "z&#322;oszczenie_si&#281;"
  ]
  node [
    id 304
    label "wzbudzenie"
  ]
  node [
    id 305
    label "rozgniewanie_si&#281;"
  ]
  node [
    id 306
    label "zagniewanie"
  ]
  node [
    id 307
    label "gniewanie_si&#281;"
  ]
  node [
    id 308
    label "niezgodnie"
  ]
  node [
    id 309
    label "gorzej"
  ]
  node [
    id 310
    label "jako&#347;&#263;"
  ]
  node [
    id 311
    label "syphilis"
  ]
  node [
    id 312
    label "tragedia"
  ]
  node [
    id 313
    label "nieporz&#261;dek"
  ]
  node [
    id 314
    label "kr&#281;tek_blady"
  ]
  node [
    id 315
    label "krosta"
  ]
  node [
    id 316
    label "choroba_dworska"
  ]
  node [
    id 317
    label "choroba_bakteryjna"
  ]
  node [
    id 318
    label "zabrudzenie"
  ]
  node [
    id 319
    label "substancja"
  ]
  node [
    id 320
    label "sk&#322;ad"
  ]
  node [
    id 321
    label "choroba_weneryczna"
  ]
  node [
    id 322
    label "ki&#322;a_wrodzona"
  ]
  node [
    id 323
    label "szankier_twardy"
  ]
  node [
    id 324
    label "spot"
  ]
  node [
    id 325
    label "zanieczyszczenie"
  ]
  node [
    id 326
    label "cholera"
  ]
  node [
    id 327
    label "wypowied&#378;"
  ]
  node [
    id 328
    label "chuj"
  ]
  node [
    id 329
    label "bluzg"
  ]
  node [
    id 330
    label "pies"
  ]
  node [
    id 331
    label "chujowy"
  ]
  node [
    id 332
    label "obelga"
  ]
  node [
    id 333
    label "szmata"
  ]
  node [
    id 334
    label "pos&#322;uchanie"
  ]
  node [
    id 335
    label "s&#261;d"
  ]
  node [
    id 336
    label "sparafrazowanie"
  ]
  node [
    id 337
    label "strawestowa&#263;"
  ]
  node [
    id 338
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 339
    label "trawestowa&#263;"
  ]
  node [
    id 340
    label "sparafrazowa&#263;"
  ]
  node [
    id 341
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 342
    label "sformu&#322;owanie"
  ]
  node [
    id 343
    label "parafrazowanie"
  ]
  node [
    id 344
    label "ozdobnik"
  ]
  node [
    id 345
    label "delimitacja"
  ]
  node [
    id 346
    label "parafrazowa&#263;"
  ]
  node [
    id 347
    label "stylizacja"
  ]
  node [
    id 348
    label "komunikat"
  ]
  node [
    id 349
    label "trawestowanie"
  ]
  node [
    id 350
    label "strawestowanie"
  ]
  node [
    id 351
    label "rezultat"
  ]
  node [
    id 352
    label "materia&#322;"
  ]
  node [
    id 353
    label "&#322;achmyta"
  ]
  node [
    id 354
    label "zo&#322;za"
  ]
  node [
    id 355
    label "skurwienie_si&#281;"
  ]
  node [
    id 356
    label "kawa&#322;ek"
  ]
  node [
    id 357
    label "element"
  ]
  node [
    id 358
    label "wyzwisko"
  ]
  node [
    id 359
    label "zeszmacenie_si&#281;"
  ]
  node [
    id 360
    label "bramka"
  ]
  node [
    id 361
    label "zeszmacanie_si&#281;"
  ]
  node [
    id 362
    label "piese&#322;"
  ]
  node [
    id 363
    label "cz&#322;owiek"
  ]
  node [
    id 364
    label "Cerber"
  ]
  node [
    id 365
    label "szczeka&#263;"
  ]
  node [
    id 366
    label "&#322;ajdak"
  ]
  node [
    id 367
    label "kabanos"
  ]
  node [
    id 368
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 369
    label "samiec"
  ]
  node [
    id 370
    label "spragniony"
  ]
  node [
    id 371
    label "policjant"
  ]
  node [
    id 372
    label "rakarz"
  ]
  node [
    id 373
    label "szczu&#263;"
  ]
  node [
    id 374
    label "wycie"
  ]
  node [
    id 375
    label "istota_&#380;ywa"
  ]
  node [
    id 376
    label "trufla"
  ]
  node [
    id 377
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 378
    label "zawy&#263;"
  ]
  node [
    id 379
    label "sobaka"
  ]
  node [
    id 380
    label "dogoterapia"
  ]
  node [
    id 381
    label "s&#322;u&#380;enie"
  ]
  node [
    id 382
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 383
    label "psowate"
  ]
  node [
    id 384
    label "wy&#263;"
  ]
  node [
    id 385
    label "szczucie"
  ]
  node [
    id 386
    label "czworon&#243;g"
  ]
  node [
    id 387
    label "skurczybyk"
  ]
  node [
    id 388
    label "holender"
  ]
  node [
    id 389
    label "chor&#243;bka"
  ]
  node [
    id 390
    label "przecinkowiec_cholery"
  ]
  node [
    id 391
    label "gniew"
  ]
  node [
    id 392
    label "charakternik"
  ]
  node [
    id 393
    label "cholewa"
  ]
  node [
    id 394
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 395
    label "przekle&#324;stwo"
  ]
  node [
    id 396
    label "fury"
  ]
  node [
    id 397
    label "choroba"
  ]
  node [
    id 398
    label "do_dupy"
  ]
  node [
    id 399
    label "chujowo"
  ]
  node [
    id 400
    label "beznadziejny"
  ]
  node [
    id 401
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 402
    label "ubliga"
  ]
  node [
    id 403
    label "niedorobek"
  ]
  node [
    id 404
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 405
    label "indignation"
  ]
  node [
    id 406
    label "tekst"
  ]
  node [
    id 407
    label "wrzuta"
  ]
  node [
    id 408
    label "krzywda"
  ]
  node [
    id 409
    label "penis"
  ]
  node [
    id 410
    label "ciul"
  ]
  node [
    id 411
    label "skurwysyn"
  ]
  node [
    id 412
    label "dupek"
  ]
  node [
    id 413
    label "po_niemiecku"
  ]
  node [
    id 414
    label "German"
  ]
  node [
    id 415
    label "niemiecko"
  ]
  node [
    id 416
    label "cenar"
  ]
  node [
    id 417
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 418
    label "strudel"
  ]
  node [
    id 419
    label "niemiec"
  ]
  node [
    id 420
    label "pionier"
  ]
  node [
    id 421
    label "zachodnioeuropejski"
  ]
  node [
    id 422
    label "j&#281;zyk"
  ]
  node [
    id 423
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 424
    label "junkers"
  ]
  node [
    id 425
    label "szwabski"
  ]
  node [
    id 426
    label "szwabsko"
  ]
  node [
    id 427
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 428
    label "po_szwabsku"
  ]
  node [
    id 429
    label "platt"
  ]
  node [
    id 430
    label "europejsko"
  ]
  node [
    id 431
    label "ciasto"
  ]
  node [
    id 432
    label "&#380;o&#322;nierz"
  ]
  node [
    id 433
    label "saper"
  ]
  node [
    id 434
    label "prekursor"
  ]
  node [
    id 435
    label "osadnik"
  ]
  node [
    id 436
    label "skaut"
  ]
  node [
    id 437
    label "g&#322;osiciel"
  ]
  node [
    id 438
    label "taniec_ludowy"
  ]
  node [
    id 439
    label "melodia"
  ]
  node [
    id 440
    label "taniec"
  ]
  node [
    id 441
    label "podgrzewacz"
  ]
  node [
    id 442
    label "samolot_wojskowy"
  ]
  node [
    id 443
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 444
    label "langosz"
  ]
  node [
    id 445
    label "moreska"
  ]
  node [
    id 446
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 447
    label "zachodni"
  ]
  node [
    id 448
    label "po_europejsku"
  ]
  node [
    id 449
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 450
    label "European"
  ]
  node [
    id 451
    label "typowy"
  ]
  node [
    id 452
    label "charakterystyczny"
  ]
  node [
    id 453
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 454
    label "artykulator"
  ]
  node [
    id 455
    label "kod"
  ]
  node [
    id 456
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 457
    label "gramatyka"
  ]
  node [
    id 458
    label "stylik"
  ]
  node [
    id 459
    label "przet&#322;umaczenie"
  ]
  node [
    id 460
    label "formalizowanie"
  ]
  node [
    id 461
    label "ssanie"
  ]
  node [
    id 462
    label "ssa&#263;"
  ]
  node [
    id 463
    label "language"
  ]
  node [
    id 464
    label "liza&#263;"
  ]
  node [
    id 465
    label "napisa&#263;"
  ]
  node [
    id 466
    label "konsonantyzm"
  ]
  node [
    id 467
    label "wokalizm"
  ]
  node [
    id 468
    label "pisa&#263;"
  ]
  node [
    id 469
    label "fonetyka"
  ]
  node [
    id 470
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 471
    label "jeniec"
  ]
  node [
    id 472
    label "but"
  ]
  node [
    id 473
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 474
    label "po_koroniarsku"
  ]
  node [
    id 475
    label "kultura_duchowa"
  ]
  node [
    id 476
    label "t&#322;umaczenie"
  ]
  node [
    id 477
    label "m&#243;wienie"
  ]
  node [
    id 478
    label "pype&#263;"
  ]
  node [
    id 479
    label "lizanie"
  ]
  node [
    id 480
    label "pismo"
  ]
  node [
    id 481
    label "formalizowa&#263;"
  ]
  node [
    id 482
    label "rozumie&#263;"
  ]
  node [
    id 483
    label "organ"
  ]
  node [
    id 484
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 485
    label "rozumienie"
  ]
  node [
    id 486
    label "spos&#243;b"
  ]
  node [
    id 487
    label "makroglosja"
  ]
  node [
    id 488
    label "jama_ustna"
  ]
  node [
    id 489
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 490
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 491
    label "natural_language"
  ]
  node [
    id 492
    label "s&#322;ownictwo"
  ]
  node [
    id 493
    label "urz&#261;dzenie"
  ]
  node [
    id 494
    label "zesp&#243;&#322;"
  ]
  node [
    id 495
    label "t&#322;oczysko"
  ]
  node [
    id 496
    label "depesza"
  ]
  node [
    id 497
    label "maszyna"
  ]
  node [
    id 498
    label "media"
  ]
  node [
    id 499
    label "czasopismo"
  ]
  node [
    id 500
    label "dziennikarz_prasowy"
  ]
  node [
    id 501
    label "kiosk"
  ]
  node [
    id 502
    label "maszyna_rolnicza"
  ]
  node [
    id 503
    label "gazeta"
  ]
  node [
    id 504
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 505
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 506
    label "tuleja"
  ]
  node [
    id 507
    label "pracowanie"
  ]
  node [
    id 508
    label "kad&#322;ub"
  ]
  node [
    id 509
    label "n&#243;&#380;"
  ]
  node [
    id 510
    label "b&#281;benek"
  ]
  node [
    id 511
    label "wa&#322;"
  ]
  node [
    id 512
    label "maszyneria"
  ]
  node [
    id 513
    label "prototypownia"
  ]
  node [
    id 514
    label "trawers"
  ]
  node [
    id 515
    label "deflektor"
  ]
  node [
    id 516
    label "mechanizm"
  ]
  node [
    id 517
    label "kolumna"
  ]
  node [
    id 518
    label "wa&#322;ek"
  ]
  node [
    id 519
    label "pracowa&#263;"
  ]
  node [
    id 520
    label "b&#281;ben"
  ]
  node [
    id 521
    label "rz&#281;zi&#263;"
  ]
  node [
    id 522
    label "przyk&#322;adka"
  ]
  node [
    id 523
    label "t&#322;ok"
  ]
  node [
    id 524
    label "dehumanizacja"
  ]
  node [
    id 525
    label "rami&#281;"
  ]
  node [
    id 526
    label "rz&#281;&#380;enie"
  ]
  node [
    id 527
    label "Mazowsze"
  ]
  node [
    id 528
    label "whole"
  ]
  node [
    id 529
    label "skupienie"
  ]
  node [
    id 530
    label "The_Beatles"
  ]
  node [
    id 531
    label "zabudowania"
  ]
  node [
    id 532
    label "group"
  ]
  node [
    id 533
    label "zespolik"
  ]
  node [
    id 534
    label "schorzenie"
  ]
  node [
    id 535
    label "ro&#347;lina"
  ]
  node [
    id 536
    label "Depeche_Mode"
  ]
  node [
    id 537
    label "batch"
  ]
  node [
    id 538
    label "mass-media"
  ]
  node [
    id 539
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 540
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 541
    label "przekazior"
  ]
  node [
    id 542
    label "uzbrajanie"
  ]
  node [
    id 543
    label "medium"
  ]
  node [
    id 544
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 545
    label "maszyna_t&#322;okowa"
  ]
  node [
    id 546
    label "tytu&#322;"
  ]
  node [
    id 547
    label "redakcja"
  ]
  node [
    id 548
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 549
    label "psychotest"
  ]
  node [
    id 550
    label "communication"
  ]
  node [
    id 551
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 552
    label "wk&#322;ad"
  ]
  node [
    id 553
    label "zajawka"
  ]
  node [
    id 554
    label "ok&#322;adka"
  ]
  node [
    id 555
    label "Zwrotnica"
  ]
  node [
    id 556
    label "dzia&#322;"
  ]
  node [
    id 557
    label "przesy&#322;ka"
  ]
  node [
    id 558
    label "cable"
  ]
  node [
    id 559
    label "budka"
  ]
  node [
    id 560
    label "punkt"
  ]
  node [
    id 561
    label "okr&#281;t_podwodny"
  ]
  node [
    id 562
    label "nadbud&#243;wka"
  ]
  node [
    id 563
    label "sklep"
  ]
  node [
    id 564
    label "stworzy&#263;"
  ]
  node [
    id 565
    label "read"
  ]
  node [
    id 566
    label "styl"
  ]
  node [
    id 567
    label "postawi&#263;"
  ]
  node [
    id 568
    label "write"
  ]
  node [
    id 569
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 570
    label "formu&#322;owa&#263;"
  ]
  node [
    id 571
    label "ozdabia&#263;"
  ]
  node [
    id 572
    label "stawia&#263;"
  ]
  node [
    id 573
    label "spell"
  ]
  node [
    id 574
    label "skryba"
  ]
  node [
    id 575
    label "code"
  ]
  node [
    id 576
    label "dysgrafia"
  ]
  node [
    id 577
    label "dysortografia"
  ]
  node [
    id 578
    label "tworzy&#263;"
  ]
  node [
    id 579
    label "restrykcja"
  ]
  node [
    id 580
    label "nemezis"
  ]
  node [
    id 581
    label "konsekwencja"
  ]
  node [
    id 582
    label "punishment"
  ]
  node [
    id 583
    label "roboty_przymusowe"
  ]
  node [
    id 584
    label "authority"
  ]
  node [
    id 585
    label "kara"
  ]
  node [
    id 586
    label "zatwierdzenie"
  ]
  node [
    id 587
    label "podpisanie"
  ]
  node [
    id 588
    label "confirmation"
  ]
  node [
    id 589
    label "spowodowanie"
  ]
  node [
    id 590
    label "konwalidacja"
  ]
  node [
    id 591
    label "pozwolenie"
  ]
  node [
    id 592
    label "czynno&#347;&#263;"
  ]
  node [
    id 593
    label "nadanie"
  ]
  node [
    id 594
    label "odczuwa&#263;"
  ]
  node [
    id 595
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 596
    label "skrupienie_si&#281;"
  ]
  node [
    id 597
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 598
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 599
    label "odczucie"
  ]
  node [
    id 600
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 601
    label "koszula_Dejaniry"
  ]
  node [
    id 602
    label "odczuwanie"
  ]
  node [
    id 603
    label "event"
  ]
  node [
    id 604
    label "skrupianie_si&#281;"
  ]
  node [
    id 605
    label "odczu&#263;"
  ]
  node [
    id 606
    label "kwota"
  ]
  node [
    id 607
    label "klacz"
  ]
  node [
    id 608
    label "forfeit"
  ]
  node [
    id 609
    label "ograniczenie"
  ]
  node [
    id 610
    label "mental_reservation"
  ]
  node [
    id 611
    label "publicysta"
  ]
  node [
    id 612
    label "nowiniarz"
  ]
  node [
    id 613
    label "bran&#380;owiec"
  ]
  node [
    id 614
    label "akredytowanie"
  ]
  node [
    id 615
    label "akredytowa&#263;"
  ]
  node [
    id 616
    label "Korwin"
  ]
  node [
    id 617
    label "Michnik"
  ]
  node [
    id 618
    label "Conrad"
  ]
  node [
    id 619
    label "intelektualista"
  ]
  node [
    id 620
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 621
    label "autor"
  ]
  node [
    id 622
    label "Gogol"
  ]
  node [
    id 623
    label "pracownik"
  ]
  node [
    id 624
    label "fachowiec"
  ]
  node [
    id 625
    label "zwi&#261;zkowiec"
  ]
  node [
    id 626
    label "nowinkarz"
  ]
  node [
    id 627
    label "uwiarygodnia&#263;"
  ]
  node [
    id 628
    label "pozwoli&#263;"
  ]
  node [
    id 629
    label "attest"
  ]
  node [
    id 630
    label "uwiarygodni&#263;"
  ]
  node [
    id 631
    label "zezwala&#263;"
  ]
  node [
    id 632
    label "upowa&#380;nia&#263;"
  ]
  node [
    id 633
    label "upowa&#380;ni&#263;"
  ]
  node [
    id 634
    label "zezwalanie"
  ]
  node [
    id 635
    label "uwiarygodnienie"
  ]
  node [
    id 636
    label "akredytowanie_si&#281;"
  ]
  node [
    id 637
    label "upowa&#380;nianie"
  ]
  node [
    id 638
    label "accreditation"
  ]
  node [
    id 639
    label "uwiarygodnianie"
  ]
  node [
    id 640
    label "upowa&#380;nienie"
  ]
  node [
    id 641
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 642
    label "zobo"
  ]
  node [
    id 643
    label "yakalo"
  ]
  node [
    id 644
    label "byd&#322;o"
  ]
  node [
    id 645
    label "dzo"
  ]
  node [
    id 646
    label "kr&#281;torogie"
  ]
  node [
    id 647
    label "g&#322;owa"
  ]
  node [
    id 648
    label "czochrad&#322;o"
  ]
  node [
    id 649
    label "posp&#243;lstwo"
  ]
  node [
    id 650
    label "kraal"
  ]
  node [
    id 651
    label "livestock"
  ]
  node [
    id 652
    label "prze&#380;uwacz"
  ]
  node [
    id 653
    label "zebu"
  ]
  node [
    id 654
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 655
    label "bizon"
  ]
  node [
    id 656
    label "byd&#322;o_domowe"
  ]
  node [
    id 657
    label "free"
  ]
  node [
    id 658
    label "Katar"
  ]
  node [
    id 659
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 660
    label "Libia"
  ]
  node [
    id 661
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 662
    label "Gwatemala"
  ]
  node [
    id 663
    label "Anglia"
  ]
  node [
    id 664
    label "Amazonia"
  ]
  node [
    id 665
    label "Afganistan"
  ]
  node [
    id 666
    label "Ekwador"
  ]
  node [
    id 667
    label "Bordeaux"
  ]
  node [
    id 668
    label "Tad&#380;ykistan"
  ]
  node [
    id 669
    label "Bhutan"
  ]
  node [
    id 670
    label "Argentyna"
  ]
  node [
    id 671
    label "D&#380;ibuti"
  ]
  node [
    id 672
    label "Wenezuela"
  ]
  node [
    id 673
    label "Ukraina"
  ]
  node [
    id 674
    label "Gabon"
  ]
  node [
    id 675
    label "Naddniestrze"
  ]
  node [
    id 676
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 677
    label "Europa_Zachodnia"
  ]
  node [
    id 678
    label "Armagnac"
  ]
  node [
    id 679
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 680
    label "Rwanda"
  ]
  node [
    id 681
    label "Liechtenstein"
  ]
  node [
    id 682
    label "Amhara"
  ]
  node [
    id 683
    label "organizacja"
  ]
  node [
    id 684
    label "Sri_Lanka"
  ]
  node [
    id 685
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 686
    label "Zamojszczyzna"
  ]
  node [
    id 687
    label "Madagaskar"
  ]
  node [
    id 688
    label "Tonga"
  ]
  node [
    id 689
    label "Kongo"
  ]
  node [
    id 690
    label "Bangladesz"
  ]
  node [
    id 691
    label "Kanada"
  ]
  node [
    id 692
    label "Ma&#322;opolska"
  ]
  node [
    id 693
    label "Wehrlen"
  ]
  node [
    id 694
    label "Turkiestan"
  ]
  node [
    id 695
    label "Algieria"
  ]
  node [
    id 696
    label "Noworosja"
  ]
  node [
    id 697
    label "Surinam"
  ]
  node [
    id 698
    label "Chile"
  ]
  node [
    id 699
    label "Sahara_Zachodnia"
  ]
  node [
    id 700
    label "Uganda"
  ]
  node [
    id 701
    label "Lubelszczyzna"
  ]
  node [
    id 702
    label "W&#281;gry"
  ]
  node [
    id 703
    label "Mezoameryka"
  ]
  node [
    id 704
    label "Birma"
  ]
  node [
    id 705
    label "Ba&#322;kany"
  ]
  node [
    id 706
    label "Kurdystan"
  ]
  node [
    id 707
    label "Kazachstan"
  ]
  node [
    id 708
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 709
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 710
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 711
    label "Armenia"
  ]
  node [
    id 712
    label "Tuwalu"
  ]
  node [
    id 713
    label "Timor_Wschodni"
  ]
  node [
    id 714
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 715
    label "Szkocja"
  ]
  node [
    id 716
    label "Baszkiria"
  ]
  node [
    id 717
    label "Tonkin"
  ]
  node [
    id 718
    label "Maghreb"
  ]
  node [
    id 719
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 720
    label "Izrael"
  ]
  node [
    id 721
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 722
    label "Nadrenia"
  ]
  node [
    id 723
    label "Estonia"
  ]
  node [
    id 724
    label "Komory"
  ]
  node [
    id 725
    label "Podhale"
  ]
  node [
    id 726
    label "Wielkopolska"
  ]
  node [
    id 727
    label "Zabajkale"
  ]
  node [
    id 728
    label "Kamerun"
  ]
  node [
    id 729
    label "Haiti"
  ]
  node [
    id 730
    label "Belize"
  ]
  node [
    id 731
    label "Sierra_Leone"
  ]
  node [
    id 732
    label "Apulia"
  ]
  node [
    id 733
    label "Luksemburg"
  ]
  node [
    id 734
    label "brzeg"
  ]
  node [
    id 735
    label "USA"
  ]
  node [
    id 736
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 737
    label "Barbados"
  ]
  node [
    id 738
    label "San_Marino"
  ]
  node [
    id 739
    label "Bu&#322;garia"
  ]
  node [
    id 740
    label "Wietnam"
  ]
  node [
    id 741
    label "Indonezja"
  ]
  node [
    id 742
    label "Bojkowszczyzna"
  ]
  node [
    id 743
    label "Malawi"
  ]
  node [
    id 744
    label "Francja"
  ]
  node [
    id 745
    label "Zambia"
  ]
  node [
    id 746
    label "Kujawy"
  ]
  node [
    id 747
    label "Angola"
  ]
  node [
    id 748
    label "Liguria"
  ]
  node [
    id 749
    label "Grenada"
  ]
  node [
    id 750
    label "Pamir"
  ]
  node [
    id 751
    label "Nepal"
  ]
  node [
    id 752
    label "Panama"
  ]
  node [
    id 753
    label "Rumunia"
  ]
  node [
    id 754
    label "Indochiny"
  ]
  node [
    id 755
    label "Podlasie"
  ]
  node [
    id 756
    label "Polinezja"
  ]
  node [
    id 757
    label "Kurpie"
  ]
  node [
    id 758
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 759
    label "S&#261;decczyzna"
  ]
  node [
    id 760
    label "Umbria"
  ]
  node [
    id 761
    label "Czarnog&#243;ra"
  ]
  node [
    id 762
    label "Malediwy"
  ]
  node [
    id 763
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 764
    label "S&#322;owacja"
  ]
  node [
    id 765
    label "Karaiby"
  ]
  node [
    id 766
    label "Ukraina_Zachodnia"
  ]
  node [
    id 767
    label "Kielecczyzna"
  ]
  node [
    id 768
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 769
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 770
    label "Egipt"
  ]
  node [
    id 771
    label "Kolumbia"
  ]
  node [
    id 772
    label "Mozambik"
  ]
  node [
    id 773
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 774
    label "Laos"
  ]
  node [
    id 775
    label "Burundi"
  ]
  node [
    id 776
    label "Suazi"
  ]
  node [
    id 777
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 778
    label "Czechy"
  ]
  node [
    id 779
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 780
    label "Wyspy_Marshalla"
  ]
  node [
    id 781
    label "Trynidad_i_Tobago"
  ]
  node [
    id 782
    label "Dominika"
  ]
  node [
    id 783
    label "Palau"
  ]
  node [
    id 784
    label "Syria"
  ]
  node [
    id 785
    label "Skandynawia"
  ]
  node [
    id 786
    label "Gwinea_Bissau"
  ]
  node [
    id 787
    label "Liberia"
  ]
  node [
    id 788
    label "Zimbabwe"
  ]
  node [
    id 789
    label "Polska"
  ]
  node [
    id 790
    label "Jamajka"
  ]
  node [
    id 791
    label "Tyrol"
  ]
  node [
    id 792
    label "Huculszczyzna"
  ]
  node [
    id 793
    label "Bory_Tucholskie"
  ]
  node [
    id 794
    label "Turyngia"
  ]
  node [
    id 795
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 796
    label "Dominikana"
  ]
  node [
    id 797
    label "Senegal"
  ]
  node [
    id 798
    label "Gruzja"
  ]
  node [
    id 799
    label "Chorwacja"
  ]
  node [
    id 800
    label "Togo"
  ]
  node [
    id 801
    label "Meksyk"
  ]
  node [
    id 802
    label "jednostka_administracyjna"
  ]
  node [
    id 803
    label "Macedonia"
  ]
  node [
    id 804
    label "Gujana"
  ]
  node [
    id 805
    label "Zair"
  ]
  node [
    id 806
    label "Kambod&#380;a"
  ]
  node [
    id 807
    label "Albania"
  ]
  node [
    id 808
    label "Mauritius"
  ]
  node [
    id 809
    label "Monako"
  ]
  node [
    id 810
    label "Gwinea"
  ]
  node [
    id 811
    label "Mali"
  ]
  node [
    id 812
    label "Nigeria"
  ]
  node [
    id 813
    label "Kalabria"
  ]
  node [
    id 814
    label "Hercegowina"
  ]
  node [
    id 815
    label "Kostaryka"
  ]
  node [
    id 816
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 817
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 818
    label "Lotaryngia"
  ]
  node [
    id 819
    label "Hanower"
  ]
  node [
    id 820
    label "Paragwaj"
  ]
  node [
    id 821
    label "W&#322;ochy"
  ]
  node [
    id 822
    label "Wyspy_Salomona"
  ]
  node [
    id 823
    label "Seszele"
  ]
  node [
    id 824
    label "Hiszpania"
  ]
  node [
    id 825
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 826
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 827
    label "Walia"
  ]
  node [
    id 828
    label "Boliwia"
  ]
  node [
    id 829
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 830
    label "Opolskie"
  ]
  node [
    id 831
    label "Kirgistan"
  ]
  node [
    id 832
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 833
    label "Irlandia"
  ]
  node [
    id 834
    label "Kampania"
  ]
  node [
    id 835
    label "Czad"
  ]
  node [
    id 836
    label "Irak"
  ]
  node [
    id 837
    label "Lesoto"
  ]
  node [
    id 838
    label "Malta"
  ]
  node [
    id 839
    label "Andora"
  ]
  node [
    id 840
    label "Sand&#380;ak"
  ]
  node [
    id 841
    label "Chiny"
  ]
  node [
    id 842
    label "Filipiny"
  ]
  node [
    id 843
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 844
    label "Syjon"
  ]
  node [
    id 845
    label "Niemcy"
  ]
  node [
    id 846
    label "Kabylia"
  ]
  node [
    id 847
    label "Lombardia"
  ]
  node [
    id 848
    label "Warmia"
  ]
  node [
    id 849
    label "Brazylia"
  ]
  node [
    id 850
    label "Nikaragua"
  ]
  node [
    id 851
    label "Pakistan"
  ]
  node [
    id 852
    label "&#321;emkowszczyzna"
  ]
  node [
    id 853
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 854
    label "Kaszmir"
  ]
  node [
    id 855
    label "Kenia"
  ]
  node [
    id 856
    label "Niger"
  ]
  node [
    id 857
    label "Tunezja"
  ]
  node [
    id 858
    label "Portugalia"
  ]
  node [
    id 859
    label "Fid&#380;i"
  ]
  node [
    id 860
    label "Maroko"
  ]
  node [
    id 861
    label "Botswana"
  ]
  node [
    id 862
    label "Tajlandia"
  ]
  node [
    id 863
    label "Australia"
  ]
  node [
    id 864
    label "&#321;&#243;dzkie"
  ]
  node [
    id 865
    label "Europa_Wschodnia"
  ]
  node [
    id 866
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 867
    label "Burkina_Faso"
  ]
  node [
    id 868
    label "Benin"
  ]
  node [
    id 869
    label "Tanzania"
  ]
  node [
    id 870
    label "interior"
  ]
  node [
    id 871
    label "Indie"
  ]
  node [
    id 872
    label "&#321;otwa"
  ]
  node [
    id 873
    label "Biskupizna"
  ]
  node [
    id 874
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 875
    label "Kiribati"
  ]
  node [
    id 876
    label "Kaukaz"
  ]
  node [
    id 877
    label "Antigua_i_Barbuda"
  ]
  node [
    id 878
    label "Rodezja"
  ]
  node [
    id 879
    label "Afryka_Wschodnia"
  ]
  node [
    id 880
    label "Cypr"
  ]
  node [
    id 881
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 882
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 883
    label "Podkarpacie"
  ]
  node [
    id 884
    label "obszar"
  ]
  node [
    id 885
    label "Peru"
  ]
  node [
    id 886
    label "Toskania"
  ]
  node [
    id 887
    label "Afryka_Zachodnia"
  ]
  node [
    id 888
    label "Austria"
  ]
  node [
    id 889
    label "Podbeskidzie"
  ]
  node [
    id 890
    label "Urugwaj"
  ]
  node [
    id 891
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 892
    label "Jordania"
  ]
  node [
    id 893
    label "Bo&#347;nia"
  ]
  node [
    id 894
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 895
    label "Grecja"
  ]
  node [
    id 896
    label "Azerbejd&#380;an"
  ]
  node [
    id 897
    label "Oceania"
  ]
  node [
    id 898
    label "Turcja"
  ]
  node [
    id 899
    label "Pomorze_Zachodnie"
  ]
  node [
    id 900
    label "Samoa"
  ]
  node [
    id 901
    label "Powi&#347;le"
  ]
  node [
    id 902
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 903
    label "ziemia"
  ]
  node [
    id 904
    label "Oman"
  ]
  node [
    id 905
    label "Sudan"
  ]
  node [
    id 906
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 907
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 908
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 909
    label "Uzbekistan"
  ]
  node [
    id 910
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 911
    label "Honduras"
  ]
  node [
    id 912
    label "Mongolia"
  ]
  node [
    id 913
    label "Portoryko"
  ]
  node [
    id 914
    label "Kaszuby"
  ]
  node [
    id 915
    label "Ko&#322;yma"
  ]
  node [
    id 916
    label "Szlezwik"
  ]
  node [
    id 917
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 918
    label "Serbia"
  ]
  node [
    id 919
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 920
    label "Tajwan"
  ]
  node [
    id 921
    label "Wielka_Brytania"
  ]
  node [
    id 922
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 923
    label "Liban"
  ]
  node [
    id 924
    label "Japonia"
  ]
  node [
    id 925
    label "Ghana"
  ]
  node [
    id 926
    label "Bahrajn"
  ]
  node [
    id 927
    label "Belgia"
  ]
  node [
    id 928
    label "Etiopia"
  ]
  node [
    id 929
    label "Mikronezja"
  ]
  node [
    id 930
    label "Polesie"
  ]
  node [
    id 931
    label "Kuwejt"
  ]
  node [
    id 932
    label "Kerala"
  ]
  node [
    id 933
    label "Mazury"
  ]
  node [
    id 934
    label "Bahamy"
  ]
  node [
    id 935
    label "Rosja"
  ]
  node [
    id 936
    label "Mo&#322;dawia"
  ]
  node [
    id 937
    label "Palestyna"
  ]
  node [
    id 938
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 939
    label "Lauda"
  ]
  node [
    id 940
    label "Azja_Wschodnia"
  ]
  node [
    id 941
    label "Litwa"
  ]
  node [
    id 942
    label "S&#322;owenia"
  ]
  node [
    id 943
    label "Szwajcaria"
  ]
  node [
    id 944
    label "Erytrea"
  ]
  node [
    id 945
    label "Lubuskie"
  ]
  node [
    id 946
    label "Kuba"
  ]
  node [
    id 947
    label "Arabia_Saudyjska"
  ]
  node [
    id 948
    label "Galicja"
  ]
  node [
    id 949
    label "Zakarpacie"
  ]
  node [
    id 950
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 951
    label "Laponia"
  ]
  node [
    id 952
    label "granica_pa&#324;stwa"
  ]
  node [
    id 953
    label "Malezja"
  ]
  node [
    id 954
    label "Korea"
  ]
  node [
    id 955
    label "Yorkshire"
  ]
  node [
    id 956
    label "Bawaria"
  ]
  node [
    id 957
    label "Zag&#243;rze"
  ]
  node [
    id 958
    label "Jemen"
  ]
  node [
    id 959
    label "Nowa_Zelandia"
  ]
  node [
    id 960
    label "Andaluzja"
  ]
  node [
    id 961
    label "Namibia"
  ]
  node [
    id 962
    label "Nauru"
  ]
  node [
    id 963
    label "&#379;ywiecczyzna"
  ]
  node [
    id 964
    label "Brunei"
  ]
  node [
    id 965
    label "Oksytania"
  ]
  node [
    id 966
    label "Opolszczyzna"
  ]
  node [
    id 967
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 968
    label "Kociewie"
  ]
  node [
    id 969
    label "Khitai"
  ]
  node [
    id 970
    label "Mauretania"
  ]
  node [
    id 971
    label "Iran"
  ]
  node [
    id 972
    label "Gambia"
  ]
  node [
    id 973
    label "Somalia"
  ]
  node [
    id 974
    label "Holandia"
  ]
  node [
    id 975
    label "Lasko"
  ]
  node [
    id 976
    label "Turkmenistan"
  ]
  node [
    id 977
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 978
    label "Salwador"
  ]
  node [
    id 979
    label "woda"
  ]
  node [
    id 980
    label "linia"
  ]
  node [
    id 981
    label "ekoton"
  ]
  node [
    id 982
    label "str&#261;d"
  ]
  node [
    id 983
    label "koniec"
  ]
  node [
    id 984
    label "plantowa&#263;"
  ]
  node [
    id 985
    label "zapadnia"
  ]
  node [
    id 986
    label "budynek"
  ]
  node [
    id 987
    label "skorupa_ziemska"
  ]
  node [
    id 988
    label "glinowanie"
  ]
  node [
    id 989
    label "martwica"
  ]
  node [
    id 990
    label "teren"
  ]
  node [
    id 991
    label "litosfera"
  ]
  node [
    id 992
    label "penetrator"
  ]
  node [
    id 993
    label "glinowa&#263;"
  ]
  node [
    id 994
    label "domain"
  ]
  node [
    id 995
    label "podglebie"
  ]
  node [
    id 996
    label "kompleks_sorpcyjny"
  ]
  node [
    id 997
    label "miejsce"
  ]
  node [
    id 998
    label "kort"
  ]
  node [
    id 999
    label "czynnik_produkcji"
  ]
  node [
    id 1000
    label "pojazd"
  ]
  node [
    id 1001
    label "powierzchnia"
  ]
  node [
    id 1002
    label "pr&#243;chnica"
  ]
  node [
    id 1003
    label "pomieszczenie"
  ]
  node [
    id 1004
    label "ryzosfera"
  ]
  node [
    id 1005
    label "p&#322;aszczyzna"
  ]
  node [
    id 1006
    label "dotleni&#263;"
  ]
  node [
    id 1007
    label "glej"
  ]
  node [
    id 1008
    label "pa&#324;stwo"
  ]
  node [
    id 1009
    label "posadzka"
  ]
  node [
    id 1010
    label "geosystem"
  ]
  node [
    id 1011
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1012
    label "przestrze&#324;"
  ]
  node [
    id 1013
    label "podmiot"
  ]
  node [
    id 1014
    label "jednostka_organizacyjna"
  ]
  node [
    id 1015
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1016
    label "TOPR"
  ]
  node [
    id 1017
    label "endecki"
  ]
  node [
    id 1018
    label "od&#322;am"
  ]
  node [
    id 1019
    label "przedstawicielstwo"
  ]
  node [
    id 1020
    label "Cepelia"
  ]
  node [
    id 1021
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1022
    label "ZBoWiD"
  ]
  node [
    id 1023
    label "organization"
  ]
  node [
    id 1024
    label "centrala"
  ]
  node [
    id 1025
    label "GOPR"
  ]
  node [
    id 1026
    label "ZOMO"
  ]
  node [
    id 1027
    label "ZMP"
  ]
  node [
    id 1028
    label "komitet_koordynacyjny"
  ]
  node [
    id 1029
    label "przybud&#243;wka"
  ]
  node [
    id 1030
    label "boj&#243;wka"
  ]
  node [
    id 1031
    label "p&#243;&#322;noc"
  ]
  node [
    id 1032
    label "Kosowo"
  ]
  node [
    id 1033
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1034
    label "Zab&#322;ocie"
  ]
  node [
    id 1035
    label "zach&#243;d"
  ]
  node [
    id 1036
    label "po&#322;udnie"
  ]
  node [
    id 1037
    label "Pow&#261;zki"
  ]
  node [
    id 1038
    label "Piotrowo"
  ]
  node [
    id 1039
    label "Olszanica"
  ]
  node [
    id 1040
    label "holarktyka"
  ]
  node [
    id 1041
    label "Ruda_Pabianicka"
  ]
  node [
    id 1042
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1043
    label "Ludwin&#243;w"
  ]
  node [
    id 1044
    label "Arktyka"
  ]
  node [
    id 1045
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1046
    label "Zabu&#380;e"
  ]
  node [
    id 1047
    label "antroposfera"
  ]
  node [
    id 1048
    label "terytorium"
  ]
  node [
    id 1049
    label "Neogea"
  ]
  node [
    id 1050
    label "Syberia_Zachodnia"
  ]
  node [
    id 1051
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1052
    label "zakres"
  ]
  node [
    id 1053
    label "pas_planetoid"
  ]
  node [
    id 1054
    label "Syberia_Wschodnia"
  ]
  node [
    id 1055
    label "Antarktyka"
  ]
  node [
    id 1056
    label "Rakowice"
  ]
  node [
    id 1057
    label "akrecja"
  ]
  node [
    id 1058
    label "wymiar"
  ]
  node [
    id 1059
    label "&#321;&#281;g"
  ]
  node [
    id 1060
    label "Kresy_Zachodnie"
  ]
  node [
    id 1061
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1062
    label "wsch&#243;d"
  ]
  node [
    id 1063
    label "Notogea"
  ]
  node [
    id 1064
    label "inti"
  ]
  node [
    id 1065
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1066
    label "sol"
  ]
  node [
    id 1067
    label "baht"
  ]
  node [
    id 1068
    label "boliviano"
  ]
  node [
    id 1069
    label "dong"
  ]
  node [
    id 1070
    label "Annam"
  ]
  node [
    id 1071
    label "colon"
  ]
  node [
    id 1072
    label "Ameryka_Centralna"
  ]
  node [
    id 1073
    label "Piemont"
  ]
  node [
    id 1074
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1075
    label "NATO"
  ]
  node [
    id 1076
    label "Italia"
  ]
  node [
    id 1077
    label "Sardynia"
  ]
  node [
    id 1078
    label "strefa_euro"
  ]
  node [
    id 1079
    label "Ok&#281;cie"
  ]
  node [
    id 1080
    label "Karyntia"
  ]
  node [
    id 1081
    label "Romania"
  ]
  node [
    id 1082
    label "Sycylia"
  ]
  node [
    id 1083
    label "Warszawa"
  ]
  node [
    id 1084
    label "lir"
  ]
  node [
    id 1085
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1086
    label "Ad&#380;aria"
  ]
  node [
    id 1087
    label "lari"
  ]
  node [
    id 1088
    label "Jukatan"
  ]
  node [
    id 1089
    label "dolar_Belize"
  ]
  node [
    id 1090
    label "dolar"
  ]
  node [
    id 1091
    label "Ohio"
  ]
  node [
    id 1092
    label "P&#243;&#322;noc"
  ]
  node [
    id 1093
    label "Nowy_York"
  ]
  node [
    id 1094
    label "Illinois"
  ]
  node [
    id 1095
    label "Po&#322;udnie"
  ]
  node [
    id 1096
    label "Kalifornia"
  ]
  node [
    id 1097
    label "Wirginia"
  ]
  node [
    id 1098
    label "Teksas"
  ]
  node [
    id 1099
    label "Waszyngton"
  ]
  node [
    id 1100
    label "zielona_karta"
  ]
  node [
    id 1101
    label "Massachusetts"
  ]
  node [
    id 1102
    label "Alaska"
  ]
  node [
    id 1103
    label "Hawaje"
  ]
  node [
    id 1104
    label "Maryland"
  ]
  node [
    id 1105
    label "Michigan"
  ]
  node [
    id 1106
    label "Arizona"
  ]
  node [
    id 1107
    label "Georgia"
  ]
  node [
    id 1108
    label "stan_wolny"
  ]
  node [
    id 1109
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1110
    label "Pensylwania"
  ]
  node [
    id 1111
    label "Luizjana"
  ]
  node [
    id 1112
    label "Nowy_Meksyk"
  ]
  node [
    id 1113
    label "Wuj_Sam"
  ]
  node [
    id 1114
    label "Alabama"
  ]
  node [
    id 1115
    label "Kansas"
  ]
  node [
    id 1116
    label "Oregon"
  ]
  node [
    id 1117
    label "Zach&#243;d"
  ]
  node [
    id 1118
    label "Floryda"
  ]
  node [
    id 1119
    label "Oklahoma"
  ]
  node [
    id 1120
    label "Hudson"
  ]
  node [
    id 1121
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1122
    label "somoni"
  ]
  node [
    id 1123
    label "perper"
  ]
  node [
    id 1124
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1125
    label "euro"
  ]
  node [
    id 1126
    label "Bengal"
  ]
  node [
    id 1127
    label "taka"
  ]
  node [
    id 1128
    label "Karelia"
  ]
  node [
    id 1129
    label "Mari_El"
  ]
  node [
    id 1130
    label "Inguszetia"
  ]
  node [
    id 1131
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1132
    label "Udmurcja"
  ]
  node [
    id 1133
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1134
    label "Newa"
  ]
  node [
    id 1135
    label "&#321;adoga"
  ]
  node [
    id 1136
    label "Czeczenia"
  ]
  node [
    id 1137
    label "Anadyr"
  ]
  node [
    id 1138
    label "Syberia"
  ]
  node [
    id 1139
    label "Tatarstan"
  ]
  node [
    id 1140
    label "Wszechrosja"
  ]
  node [
    id 1141
    label "Azja"
  ]
  node [
    id 1142
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1143
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1144
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1145
    label "Kamczatka"
  ]
  node [
    id 1146
    label "Jama&#322;"
  ]
  node [
    id 1147
    label "Dagestan"
  ]
  node [
    id 1148
    label "Witim"
  ]
  node [
    id 1149
    label "Tuwa"
  ]
  node [
    id 1150
    label "car"
  ]
  node [
    id 1151
    label "Komi"
  ]
  node [
    id 1152
    label "Czuwaszja"
  ]
  node [
    id 1153
    label "Chakasja"
  ]
  node [
    id 1154
    label "Perm"
  ]
  node [
    id 1155
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1156
    label "Ajon"
  ]
  node [
    id 1157
    label "Adygeja"
  ]
  node [
    id 1158
    label "Dniepr"
  ]
  node [
    id 1159
    label "rubel_rosyjski"
  ]
  node [
    id 1160
    label "Don"
  ]
  node [
    id 1161
    label "Mordowia"
  ]
  node [
    id 1162
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1163
    label "gourde"
  ]
  node [
    id 1164
    label "escudo_angolskie"
  ]
  node [
    id 1165
    label "kwanza"
  ]
  node [
    id 1166
    label "ariary"
  ]
  node [
    id 1167
    label "Ocean_Indyjski"
  ]
  node [
    id 1168
    label "frank_malgaski"
  ]
  node [
    id 1169
    label "Unia_Europejska"
  ]
  node [
    id 1170
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1171
    label "Windawa"
  ]
  node [
    id 1172
    label "&#379;mud&#378;"
  ]
  node [
    id 1173
    label "lit"
  ]
  node [
    id 1174
    label "Synaj"
  ]
  node [
    id 1175
    label "paraszyt"
  ]
  node [
    id 1176
    label "funt_egipski"
  ]
  node [
    id 1177
    label "birr"
  ]
  node [
    id 1178
    label "negus"
  ]
  node [
    id 1179
    label "peso_kolumbijskie"
  ]
  node [
    id 1180
    label "Orinoko"
  ]
  node [
    id 1181
    label "rial_katarski"
  ]
  node [
    id 1182
    label "dram"
  ]
  node [
    id 1183
    label "Limburgia"
  ]
  node [
    id 1184
    label "gulden"
  ]
  node [
    id 1185
    label "Zelandia"
  ]
  node [
    id 1186
    label "Niderlandy"
  ]
  node [
    id 1187
    label "Brabancja"
  ]
  node [
    id 1188
    label "cedi"
  ]
  node [
    id 1189
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1190
    label "milrejs"
  ]
  node [
    id 1191
    label "cruzado"
  ]
  node [
    id 1192
    label "real"
  ]
  node [
    id 1193
    label "frank_monakijski"
  ]
  node [
    id 1194
    label "Fryburg"
  ]
  node [
    id 1195
    label "Bazylea"
  ]
  node [
    id 1196
    label "Alpy"
  ]
  node [
    id 1197
    label "frank_szwajcarski"
  ]
  node [
    id 1198
    label "Helwecja"
  ]
  node [
    id 1199
    label "Berno"
  ]
  node [
    id 1200
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1201
    label "Dniestr"
  ]
  node [
    id 1202
    label "Gagauzja"
  ]
  node [
    id 1203
    label "Indie_Zachodnie"
  ]
  node [
    id 1204
    label "Sikkim"
  ]
  node [
    id 1205
    label "Asam"
  ]
  node [
    id 1206
    label "rupia_indyjska"
  ]
  node [
    id 1207
    label "Indie_Portugalskie"
  ]
  node [
    id 1208
    label "Indie_Wschodnie"
  ]
  node [
    id 1209
    label "Bollywood"
  ]
  node [
    id 1210
    label "Pend&#380;ab"
  ]
  node [
    id 1211
    label "boliwar"
  ]
  node [
    id 1212
    label "naira"
  ]
  node [
    id 1213
    label "frank_gwinejski"
  ]
  node [
    id 1214
    label "sum"
  ]
  node [
    id 1215
    label "Karaka&#322;pacja"
  ]
  node [
    id 1216
    label "dolar_liberyjski"
  ]
  node [
    id 1217
    label "Dacja"
  ]
  node [
    id 1218
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1219
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1220
    label "Dobrud&#380;a"
  ]
  node [
    id 1221
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1222
    label "dolar_namibijski"
  ]
  node [
    id 1223
    label "kuna"
  ]
  node [
    id 1224
    label "Rugia"
  ]
  node [
    id 1225
    label "Saksonia"
  ]
  node [
    id 1226
    label "Dolna_Saksonia"
  ]
  node [
    id 1227
    label "Anglosas"
  ]
  node [
    id 1228
    label "Hesja"
  ]
  node [
    id 1229
    label "Wirtembergia"
  ]
  node [
    id 1230
    label "Po&#322;abie"
  ]
  node [
    id 1231
    label "Germania"
  ]
  node [
    id 1232
    label "Frankonia"
  ]
  node [
    id 1233
    label "Badenia"
  ]
  node [
    id 1234
    label "Holsztyn"
  ]
  node [
    id 1235
    label "marka"
  ]
  node [
    id 1236
    label "Szwabia"
  ]
  node [
    id 1237
    label "Brandenburgia"
  ]
  node [
    id 1238
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1239
    label "Westfalia"
  ]
  node [
    id 1240
    label "Helgoland"
  ]
  node [
    id 1241
    label "Karlsbad"
  ]
  node [
    id 1242
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1243
    label "korona_w&#281;gierska"
  ]
  node [
    id 1244
    label "forint"
  ]
  node [
    id 1245
    label "Lipt&#243;w"
  ]
  node [
    id 1246
    label "tenge"
  ]
  node [
    id 1247
    label "szach"
  ]
  node [
    id 1248
    label "Baktria"
  ]
  node [
    id 1249
    label "afgani"
  ]
  node [
    id 1250
    label "kip"
  ]
  node [
    id 1251
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1252
    label "Salzburg"
  ]
  node [
    id 1253
    label "Rakuzy"
  ]
  node [
    id 1254
    label "Dyja"
  ]
  node [
    id 1255
    label "konsulent"
  ]
  node [
    id 1256
    label "szyling_austryjacki"
  ]
  node [
    id 1257
    label "peso_urugwajskie"
  ]
  node [
    id 1258
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1259
    label "korona_esto&#324;ska"
  ]
  node [
    id 1260
    label "Inflanty"
  ]
  node [
    id 1261
    label "marka_esto&#324;ska"
  ]
  node [
    id 1262
    label "tala"
  ]
  node [
    id 1263
    label "Podole"
  ]
  node [
    id 1264
    label "Wsch&#243;d"
  ]
  node [
    id 1265
    label "Naddnieprze"
  ]
  node [
    id 1266
    label "Ma&#322;orosja"
  ]
  node [
    id 1267
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1268
    label "Nadbu&#380;e"
  ]
  node [
    id 1269
    label "hrywna"
  ]
  node [
    id 1270
    label "Zaporo&#380;e"
  ]
  node [
    id 1271
    label "Krym"
  ]
  node [
    id 1272
    label "Przykarpacie"
  ]
  node [
    id 1273
    label "Kozaczyzna"
  ]
  node [
    id 1274
    label "karbowaniec"
  ]
  node [
    id 1275
    label "riel"
  ]
  node [
    id 1276
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1277
    label "kyat"
  ]
  node [
    id 1278
    label "Arakan"
  ]
  node [
    id 1279
    label "funt_liba&#324;ski"
  ]
  node [
    id 1280
    label "Mariany"
  ]
  node [
    id 1281
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1282
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1283
    label "dinar_algierski"
  ]
  node [
    id 1284
    label "ringgit"
  ]
  node [
    id 1285
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1286
    label "Borneo"
  ]
  node [
    id 1287
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1288
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1289
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1290
    label "lira_izraelska"
  ]
  node [
    id 1291
    label "szekel"
  ]
  node [
    id 1292
    label "Galilea"
  ]
  node [
    id 1293
    label "Judea"
  ]
  node [
    id 1294
    label "tolar"
  ]
  node [
    id 1295
    label "frank_luksemburski"
  ]
  node [
    id 1296
    label "lempira"
  ]
  node [
    id 1297
    label "Pozna&#324;"
  ]
  node [
    id 1298
    label "lira_malta&#324;ska"
  ]
  node [
    id 1299
    label "Gozo"
  ]
  node [
    id 1300
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1301
    label "Paros"
  ]
  node [
    id 1302
    label "Epir"
  ]
  node [
    id 1303
    label "panhellenizm"
  ]
  node [
    id 1304
    label "Eubea"
  ]
  node [
    id 1305
    label "Rodos"
  ]
  node [
    id 1306
    label "Achaja"
  ]
  node [
    id 1307
    label "Termopile"
  ]
  node [
    id 1308
    label "Attyka"
  ]
  node [
    id 1309
    label "Hellada"
  ]
  node [
    id 1310
    label "Etolia"
  ]
  node [
    id 1311
    label "palestra"
  ]
  node [
    id 1312
    label "Kreta"
  ]
  node [
    id 1313
    label "drachma"
  ]
  node [
    id 1314
    label "Olimp"
  ]
  node [
    id 1315
    label "Tesalia"
  ]
  node [
    id 1316
    label "Peloponez"
  ]
  node [
    id 1317
    label "Eolia"
  ]
  node [
    id 1318
    label "Beocja"
  ]
  node [
    id 1319
    label "Parnas"
  ]
  node [
    id 1320
    label "Lesbos"
  ]
  node [
    id 1321
    label "Atlantyk"
  ]
  node [
    id 1322
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1323
    label "Ulster"
  ]
  node [
    id 1324
    label "funt_irlandzki"
  ]
  node [
    id 1325
    label "tugrik"
  ]
  node [
    id 1326
    label "Buriaci"
  ]
  node [
    id 1327
    label "ajmak"
  ]
  node [
    id 1328
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1329
    label "Pikardia"
  ]
  node [
    id 1330
    label "Masyw_Centralny"
  ]
  node [
    id 1331
    label "Akwitania"
  ]
  node [
    id 1332
    label "Alzacja"
  ]
  node [
    id 1333
    label "Sekwana"
  ]
  node [
    id 1334
    label "Langwedocja"
  ]
  node [
    id 1335
    label "Martynika"
  ]
  node [
    id 1336
    label "Bretania"
  ]
  node [
    id 1337
    label "Sabaudia"
  ]
  node [
    id 1338
    label "Korsyka"
  ]
  node [
    id 1339
    label "Normandia"
  ]
  node [
    id 1340
    label "Gaskonia"
  ]
  node [
    id 1341
    label "Burgundia"
  ]
  node [
    id 1342
    label "frank_francuski"
  ]
  node [
    id 1343
    label "Wandea"
  ]
  node [
    id 1344
    label "Prowansja"
  ]
  node [
    id 1345
    label "Gwadelupa"
  ]
  node [
    id 1346
    label "lew"
  ]
  node [
    id 1347
    label "c&#243;rdoba"
  ]
  node [
    id 1348
    label "dolar_Zimbabwe"
  ]
  node [
    id 1349
    label "frank_rwandyjski"
  ]
  node [
    id 1350
    label "kwacha_zambijska"
  ]
  node [
    id 1351
    label "Kurlandia"
  ]
  node [
    id 1352
    label "&#322;at"
  ]
  node [
    id 1353
    label "Liwonia"
  ]
  node [
    id 1354
    label "rubel_&#322;otewski"
  ]
  node [
    id 1355
    label "Himalaje"
  ]
  node [
    id 1356
    label "rupia_nepalska"
  ]
  node [
    id 1357
    label "funt_suda&#324;ski"
  ]
  node [
    id 1358
    label "dolar_bahamski"
  ]
  node [
    id 1359
    label "Wielka_Bahama"
  ]
  node [
    id 1360
    label "Pa&#322;uki"
  ]
  node [
    id 1361
    label "Wolin"
  ]
  node [
    id 1362
    label "z&#322;oty"
  ]
  node [
    id 1363
    label "So&#322;a"
  ]
  node [
    id 1364
    label "Suwalszczyzna"
  ]
  node [
    id 1365
    label "Krajna"
  ]
  node [
    id 1366
    label "barwy_polskie"
  ]
  node [
    id 1367
    label "Izera"
  ]
  node [
    id 1368
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1369
    label "Kaczawa"
  ]
  node [
    id 1370
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1371
    label "Wis&#322;a"
  ]
  node [
    id 1372
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1373
    label "Antyle"
  ]
  node [
    id 1374
    label "dolar_Tuvalu"
  ]
  node [
    id 1375
    label "dinar_iracki"
  ]
  node [
    id 1376
    label "korona_s&#322;owacka"
  ]
  node [
    id 1377
    label "Turiec"
  ]
  node [
    id 1378
    label "jen"
  ]
  node [
    id 1379
    label "jinja"
  ]
  node [
    id 1380
    label "Okinawa"
  ]
  node [
    id 1381
    label "Japonica"
  ]
  node [
    id 1382
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1383
    label "szyling_kenijski"
  ]
  node [
    id 1384
    label "peso_chilijskie"
  ]
  node [
    id 1385
    label "Zanzibar"
  ]
  node [
    id 1386
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1387
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1388
    label "Cebu"
  ]
  node [
    id 1389
    label "Sahara"
  ]
  node [
    id 1390
    label "Tasmania"
  ]
  node [
    id 1391
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1392
    label "dolar_australijski"
  ]
  node [
    id 1393
    label "Quebec"
  ]
  node [
    id 1394
    label "dolar_kanadyjski"
  ]
  node [
    id 1395
    label "Nowa_Fundlandia"
  ]
  node [
    id 1396
    label "quetzal"
  ]
  node [
    id 1397
    label "Manica"
  ]
  node [
    id 1398
    label "escudo_mozambickie"
  ]
  node [
    id 1399
    label "Cabo_Delgado"
  ]
  node [
    id 1400
    label "Inhambane"
  ]
  node [
    id 1401
    label "Maputo"
  ]
  node [
    id 1402
    label "Gaza"
  ]
  node [
    id 1403
    label "Niasa"
  ]
  node [
    id 1404
    label "Nampula"
  ]
  node [
    id 1405
    label "metical"
  ]
  node [
    id 1406
    label "frank_tunezyjski"
  ]
  node [
    id 1407
    label "dinar_tunezyjski"
  ]
  node [
    id 1408
    label "lud"
  ]
  node [
    id 1409
    label "frank_kongijski"
  ]
  node [
    id 1410
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1411
    label "dinar_Bahrajnu"
  ]
  node [
    id 1412
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1413
    label "escudo_portugalskie"
  ]
  node [
    id 1414
    label "Melanezja"
  ]
  node [
    id 1415
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1416
    label "d&#380;amahirijja"
  ]
  node [
    id 1417
    label "dinar_libijski"
  ]
  node [
    id 1418
    label "balboa"
  ]
  node [
    id 1419
    label "dolar_surinamski"
  ]
  node [
    id 1420
    label "dolar_Brunei"
  ]
  node [
    id 1421
    label "Estremadura"
  ]
  node [
    id 1422
    label "Kastylia"
  ]
  node [
    id 1423
    label "Rzym_Zachodni"
  ]
  node [
    id 1424
    label "Aragonia"
  ]
  node [
    id 1425
    label "hacjender"
  ]
  node [
    id 1426
    label "Asturia"
  ]
  node [
    id 1427
    label "Baskonia"
  ]
  node [
    id 1428
    label "Majorka"
  ]
  node [
    id 1429
    label "Walencja"
  ]
  node [
    id 1430
    label "peseta"
  ]
  node [
    id 1431
    label "Katalonia"
  ]
  node [
    id 1432
    label "Luksemburgia"
  ]
  node [
    id 1433
    label "frank_belgijski"
  ]
  node [
    id 1434
    label "Walonia"
  ]
  node [
    id 1435
    label "Flandria"
  ]
  node [
    id 1436
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1437
    label "dolar_Barbadosu"
  ]
  node [
    id 1438
    label "korona_czeska"
  ]
  node [
    id 1439
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1440
    label "Wojwodina"
  ]
  node [
    id 1441
    label "dinar_serbski"
  ]
  node [
    id 1442
    label "funt_syryjski"
  ]
  node [
    id 1443
    label "alawizm"
  ]
  node [
    id 1444
    label "Szantung"
  ]
  node [
    id 1445
    label "Chiny_Zachodnie"
  ]
  node [
    id 1446
    label "Kuantung"
  ]
  node [
    id 1447
    label "D&#380;ungaria"
  ]
  node [
    id 1448
    label "yuan"
  ]
  node [
    id 1449
    label "Hongkong"
  ]
  node [
    id 1450
    label "Chiny_Wschodnie"
  ]
  node [
    id 1451
    label "Guangdong"
  ]
  node [
    id 1452
    label "Junnan"
  ]
  node [
    id 1453
    label "Mand&#380;uria"
  ]
  node [
    id 1454
    label "Syczuan"
  ]
  node [
    id 1455
    label "zair"
  ]
  node [
    id 1456
    label "Katanga"
  ]
  node [
    id 1457
    label "ugija"
  ]
  node [
    id 1458
    label "dalasi"
  ]
  node [
    id 1459
    label "funt_cypryjski"
  ]
  node [
    id 1460
    label "Afrodyzje"
  ]
  node [
    id 1461
    label "para"
  ]
  node [
    id 1462
    label "lek"
  ]
  node [
    id 1463
    label "frank_alba&#324;ski"
  ]
  node [
    id 1464
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1465
    label "kafar"
  ]
  node [
    id 1466
    label "dolar_jamajski"
  ]
  node [
    id 1467
    label "Ocean_Spokojny"
  ]
  node [
    id 1468
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1469
    label "som"
  ]
  node [
    id 1470
    label "guarani"
  ]
  node [
    id 1471
    label "rial_ira&#324;ski"
  ]
  node [
    id 1472
    label "mu&#322;&#322;a"
  ]
  node [
    id 1473
    label "Persja"
  ]
  node [
    id 1474
    label "Jawa"
  ]
  node [
    id 1475
    label "Sumatra"
  ]
  node [
    id 1476
    label "rupia_indonezyjska"
  ]
  node [
    id 1477
    label "Nowa_Gwinea"
  ]
  node [
    id 1478
    label "Moluki"
  ]
  node [
    id 1479
    label "szyling_somalijski"
  ]
  node [
    id 1480
    label "szyling_ugandyjski"
  ]
  node [
    id 1481
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1482
    label "Ujgur"
  ]
  node [
    id 1483
    label "Azja_Mniejsza"
  ]
  node [
    id 1484
    label "lira_turecka"
  ]
  node [
    id 1485
    label "Pireneje"
  ]
  node [
    id 1486
    label "nakfa"
  ]
  node [
    id 1487
    label "won"
  ]
  node [
    id 1488
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1489
    label "&#346;wite&#378;"
  ]
  node [
    id 1490
    label "dinar_kuwejcki"
  ]
  node [
    id 1491
    label "Nachiczewan"
  ]
  node [
    id 1492
    label "manat_azerski"
  ]
  node [
    id 1493
    label "Karabach"
  ]
  node [
    id 1494
    label "dolar_Kiribati"
  ]
  node [
    id 1495
    label "moszaw"
  ]
  node [
    id 1496
    label "Kanaan"
  ]
  node [
    id 1497
    label "Aruba"
  ]
  node [
    id 1498
    label "Kajmany"
  ]
  node [
    id 1499
    label "Anguilla"
  ]
  node [
    id 1500
    label "Mogielnica"
  ]
  node [
    id 1501
    label "jezioro"
  ]
  node [
    id 1502
    label "Rumelia"
  ]
  node [
    id 1503
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1504
    label "Poprad"
  ]
  node [
    id 1505
    label "Tatry"
  ]
  node [
    id 1506
    label "Podtatrze"
  ]
  node [
    id 1507
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1508
    label "Austro-W&#281;gry"
  ]
  node [
    id 1509
    label "Biskupice"
  ]
  node [
    id 1510
    label "Iwanowice"
  ]
  node [
    id 1511
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1512
    label "Rogo&#378;nik"
  ]
  node [
    id 1513
    label "Ropa"
  ]
  node [
    id 1514
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1515
    label "Karpaty"
  ]
  node [
    id 1516
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1517
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1518
    label "Beskid_Niski"
  ]
  node [
    id 1519
    label "Etruria"
  ]
  node [
    id 1520
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1521
    label "Bojanowo"
  ]
  node [
    id 1522
    label "Obra"
  ]
  node [
    id 1523
    label "Wilkowo_Polskie"
  ]
  node [
    id 1524
    label "Dobra"
  ]
  node [
    id 1525
    label "Buriacja"
  ]
  node [
    id 1526
    label "Rozewie"
  ]
  node [
    id 1527
    label "&#346;l&#261;sk"
  ]
  node [
    id 1528
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1529
    label "Norwegia"
  ]
  node [
    id 1530
    label "Szwecja"
  ]
  node [
    id 1531
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1532
    label "Finlandia"
  ]
  node [
    id 1533
    label "Wiktoria"
  ]
  node [
    id 1534
    label "Guernsey"
  ]
  node [
    id 1535
    label "funt_szterling"
  ]
  node [
    id 1536
    label "Portland"
  ]
  node [
    id 1537
    label "El&#380;bieta_I"
  ]
  node [
    id 1538
    label "Kornwalia"
  ]
  node [
    id 1539
    label "Amazonka"
  ]
  node [
    id 1540
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1541
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1542
    label "Moza"
  ]
  node [
    id 1543
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1544
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1545
    label "Paj&#281;czno"
  ]
  node [
    id 1546
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1547
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1548
    label "Gop&#322;o"
  ]
  node [
    id 1549
    label "Jerozolima"
  ]
  node [
    id 1550
    label "Dolna_Frankonia"
  ]
  node [
    id 1551
    label "funt_szkocki"
  ]
  node [
    id 1552
    label "Kaledonia"
  ]
  node [
    id 1553
    label "Abchazja"
  ]
  node [
    id 1554
    label "Sarmata"
  ]
  node [
    id 1555
    label "Eurazja"
  ]
  node [
    id 1556
    label "Mariensztat"
  ]
  node [
    id 1557
    label "Unia"
  ]
  node [
    id 1558
    label "uk&#322;ad"
  ]
  node [
    id 1559
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 1560
    label "combination"
  ]
  node [
    id 1561
    label "union"
  ]
  node [
    id 1562
    label "partia"
  ]
  node [
    id 1563
    label "Bund"
  ]
  node [
    id 1564
    label "PPR"
  ]
  node [
    id 1565
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1566
    label "wybranek"
  ]
  node [
    id 1567
    label "Jakobici"
  ]
  node [
    id 1568
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1569
    label "SLD"
  ]
  node [
    id 1570
    label "Razem"
  ]
  node [
    id 1571
    label "PiS"
  ]
  node [
    id 1572
    label "package"
  ]
  node [
    id 1573
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1574
    label "Kuomintang"
  ]
  node [
    id 1575
    label "ZSL"
  ]
  node [
    id 1576
    label "AWS"
  ]
  node [
    id 1577
    label "gra"
  ]
  node [
    id 1578
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1579
    label "game"
  ]
  node [
    id 1580
    label "blok"
  ]
  node [
    id 1581
    label "PO"
  ]
  node [
    id 1582
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1583
    label "niedoczas"
  ]
  node [
    id 1584
    label "Federali&#347;ci"
  ]
  node [
    id 1585
    label "PSL"
  ]
  node [
    id 1586
    label "Wigowie"
  ]
  node [
    id 1587
    label "ZChN"
  ]
  node [
    id 1588
    label "egzekutywa"
  ]
  node [
    id 1589
    label "aktyw"
  ]
  node [
    id 1590
    label "wybranka"
  ]
  node [
    id 1591
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1592
    label "unit"
  ]
  node [
    id 1593
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1594
    label "rozprz&#261;c"
  ]
  node [
    id 1595
    label "treaty"
  ]
  node [
    id 1596
    label "systemat"
  ]
  node [
    id 1597
    label "system"
  ]
  node [
    id 1598
    label "umowa"
  ]
  node [
    id 1599
    label "usenet"
  ]
  node [
    id 1600
    label "przestawi&#263;"
  ]
  node [
    id 1601
    label "alliance"
  ]
  node [
    id 1602
    label "ONZ"
  ]
  node [
    id 1603
    label "konstelacja"
  ]
  node [
    id 1604
    label "o&#347;"
  ]
  node [
    id 1605
    label "podsystem"
  ]
  node [
    id 1606
    label "zawarcie"
  ]
  node [
    id 1607
    label "zawrze&#263;"
  ]
  node [
    id 1608
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1609
    label "wi&#281;&#378;"
  ]
  node [
    id 1610
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 1611
    label "zachowanie"
  ]
  node [
    id 1612
    label "cybernetyk"
  ]
  node [
    id 1613
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1614
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1615
    label "traktat_wersalski"
  ]
  node [
    id 1616
    label "cia&#322;o"
  ]
  node [
    id 1617
    label "eurosceptycyzm"
  ]
  node [
    id 1618
    label "euroentuzjasta"
  ]
  node [
    id 1619
    label "euroentuzjazm"
  ]
  node [
    id 1620
    label "euroko&#322;choz"
  ]
  node [
    id 1621
    label "eurorealizm"
  ]
  node [
    id 1622
    label "p&#322;atnik_netto"
  ]
  node [
    id 1623
    label "Bruksela"
  ]
  node [
    id 1624
    label "eurorealista"
  ]
  node [
    id 1625
    label "eurosceptyczny"
  ]
  node [
    id 1626
    label "eurosceptyk"
  ]
  node [
    id 1627
    label "Fundusze_Unijne"
  ]
  node [
    id 1628
    label "prawo_unijne"
  ]
  node [
    id 1629
    label "Wsp&#243;lnota_Europejska"
  ]
  node [
    id 1630
    label "zwyczajny"
  ]
  node [
    id 1631
    label "typowo"
  ]
  node [
    id 1632
    label "cz&#281;sty"
  ]
  node [
    id 1633
    label "zwyk&#322;y"
  ]
  node [
    id 1634
    label "charakterystycznie"
  ]
  node [
    id 1635
    label "szczeg&#243;lny"
  ]
  node [
    id 1636
    label "wyj&#261;tkowy"
  ]
  node [
    id 1637
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1638
    label "podobny"
  ]
  node [
    id 1639
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1640
    label "nale&#380;ny"
  ]
  node [
    id 1641
    label "nale&#380;yty"
  ]
  node [
    id 1642
    label "uprawniony"
  ]
  node [
    id 1643
    label "zasadniczy"
  ]
  node [
    id 1644
    label "stosownie"
  ]
  node [
    id 1645
    label "taki"
  ]
  node [
    id 1646
    label "prawdziwy"
  ]
  node [
    id 1647
    label "ten"
  ]
  node [
    id 1648
    label "dobry"
  ]
  node [
    id 1649
    label "gro&#378;nie"
  ]
  node [
    id 1650
    label "ci&#281;&#380;ko"
  ]
  node [
    id 1651
    label "niema&#322;o"
  ]
  node [
    id 1652
    label "powa&#380;ny"
  ]
  node [
    id 1653
    label "ci&#281;&#380;ki"
  ]
  node [
    id 1654
    label "bardzo"
  ]
  node [
    id 1655
    label "du&#380;y"
  ]
  node [
    id 1656
    label "spowa&#380;nienie"
  ]
  node [
    id 1657
    label "gro&#378;ny"
  ]
  node [
    id 1658
    label "powa&#380;nienie"
  ]
  node [
    id 1659
    label "monumentalny"
  ]
  node [
    id 1660
    label "mocny"
  ]
  node [
    id 1661
    label "kompletny"
  ]
  node [
    id 1662
    label "masywny"
  ]
  node [
    id 1663
    label "wielki"
  ]
  node [
    id 1664
    label "wymagaj&#261;cy"
  ]
  node [
    id 1665
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 1666
    label "przyswajalny"
  ]
  node [
    id 1667
    label "niezgrabny"
  ]
  node [
    id 1668
    label "liczny"
  ]
  node [
    id 1669
    label "nieprzejrzysty"
  ]
  node [
    id 1670
    label "niedelikatny"
  ]
  node [
    id 1671
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 1672
    label "intensywny"
  ]
  node [
    id 1673
    label "wolny"
  ]
  node [
    id 1674
    label "nieudany"
  ]
  node [
    id 1675
    label "zbrojny"
  ]
  node [
    id 1676
    label "dotkliwy"
  ]
  node [
    id 1677
    label "bojowy"
  ]
  node [
    id 1678
    label "k&#322;opotliwy"
  ]
  node [
    id 1679
    label "ambitny"
  ]
  node [
    id 1680
    label "grubo"
  ]
  node [
    id 1681
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 1682
    label "niebezpiecznie"
  ]
  node [
    id 1683
    label "sternly"
  ]
  node [
    id 1684
    label "surowie"
  ]
  node [
    id 1685
    label "w_chuj"
  ]
  node [
    id 1686
    label "monumentalnie"
  ]
  node [
    id 1687
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 1688
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 1689
    label "nieudanie"
  ]
  node [
    id 1690
    label "mocno"
  ]
  node [
    id 1691
    label "wolno"
  ]
  node [
    id 1692
    label "kompletnie"
  ]
  node [
    id 1693
    label "dotkliwie"
  ]
  node [
    id 1694
    label "niezgrabnie"
  ]
  node [
    id 1695
    label "hard"
  ]
  node [
    id 1696
    label "masywnie"
  ]
  node [
    id 1697
    label "heavily"
  ]
  node [
    id 1698
    label "niedelikatnie"
  ]
  node [
    id 1699
    label "intensywnie"
  ]
  node [
    id 1700
    label "hazard"
  ]
  node [
    id 1701
    label "zapowiada&#263;"
  ]
  node [
    id 1702
    label "boast"
  ]
  node [
    id 1703
    label "ostrzega&#263;"
  ]
  node [
    id 1704
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1705
    label "harbinger"
  ]
  node [
    id 1706
    label "og&#322;asza&#263;"
  ]
  node [
    id 1707
    label "bode"
  ]
  node [
    id 1708
    label "post"
  ]
  node [
    id 1709
    label "play"
  ]
  node [
    id 1710
    label "prawdopodobie&#324;stwo"
  ]
  node [
    id 1711
    label "rozrywka"
  ]
  node [
    id 1712
    label "wideoloteria"
  ]
  node [
    id 1713
    label "poj&#281;cie"
  ]
  node [
    id 1714
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 1715
    label "pierdel"
  ]
  node [
    id 1716
    label "&#321;ubianka"
  ]
  node [
    id 1717
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 1718
    label "ciupa"
  ]
  node [
    id 1719
    label "reedukator"
  ]
  node [
    id 1720
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 1721
    label "Butyrki"
  ]
  node [
    id 1722
    label "imprisonment"
  ]
  node [
    id 1723
    label "miejsce_odosobnienia"
  ]
  node [
    id 1724
    label "ogranicza&#263;"
  ]
  node [
    id 1725
    label "uniemo&#380;liwianie"
  ]
  node [
    id 1726
    label "sytuacja"
  ]
  node [
    id 1727
    label "przeszkadzanie"
  ]
  node [
    id 1728
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1729
    label "warunki"
  ]
  node [
    id 1730
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1731
    label "state"
  ]
  node [
    id 1732
    label "motyw"
  ]
  node [
    id 1733
    label "realia"
  ]
  node [
    id 1734
    label "alfabet_wi&#281;zienny"
  ]
  node [
    id 1735
    label "odsiadka"
  ]
  node [
    id 1736
    label "nauczyciel"
  ]
  node [
    id 1737
    label "dom_poprawczy"
  ]
  node [
    id 1738
    label "Monar"
  ]
  node [
    id 1739
    label "rehabilitant"
  ]
  node [
    id 1740
    label "suppress"
  ]
  node [
    id 1741
    label "wytycza&#263;"
  ]
  node [
    id 1742
    label "zmniejsza&#263;"
  ]
  node [
    id 1743
    label "environment"
  ]
  node [
    id 1744
    label "bound"
  ]
  node [
    id 1745
    label "stanowi&#263;"
  ]
  node [
    id 1746
    label "streszczenie"
  ]
  node [
    id 1747
    label "publicystyka"
  ]
  node [
    id 1748
    label "criticism"
  ]
  node [
    id 1749
    label "publiczno&#347;&#263;"
  ]
  node [
    id 1750
    label "cenzura"
  ]
  node [
    id 1751
    label "diatryba"
  ]
  node [
    id 1752
    label "review"
  ]
  node [
    id 1753
    label "ocena"
  ]
  node [
    id 1754
    label "krytyka_literacka"
  ]
  node [
    id 1755
    label "ekscerpcja"
  ]
  node [
    id 1756
    label "j&#281;zykowo"
  ]
  node [
    id 1757
    label "wytw&#243;r"
  ]
  node [
    id 1758
    label "pomini&#281;cie"
  ]
  node [
    id 1759
    label "dzie&#322;o"
  ]
  node [
    id 1760
    label "preparacja"
  ]
  node [
    id 1761
    label "odmianka"
  ]
  node [
    id 1762
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1763
    label "koniektura"
  ]
  node [
    id 1764
    label "pogl&#261;d"
  ]
  node [
    id 1765
    label "decyzja"
  ]
  node [
    id 1766
    label "sofcik"
  ]
  node [
    id 1767
    label "kryterium"
  ]
  node [
    id 1768
    label "informacja"
  ]
  node [
    id 1769
    label "appraisal"
  ]
  node [
    id 1770
    label "odbiorca"
  ]
  node [
    id 1771
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1772
    label "audience"
  ]
  node [
    id 1773
    label "publiczka"
  ]
  node [
    id 1774
    label "widzownia"
  ]
  node [
    id 1775
    label "literatura"
  ]
  node [
    id 1776
    label "skr&#243;t"
  ]
  node [
    id 1777
    label "podsumowanie"
  ]
  node [
    id 1778
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 1779
    label "overview"
  ]
  node [
    id 1780
    label "urz&#261;d"
  ]
  node [
    id 1781
    label "&#347;wiadectwo"
  ]
  node [
    id 1782
    label "proces"
  ]
  node [
    id 1783
    label "drugi_obieg"
  ]
  node [
    id 1784
    label "zdejmowanie"
  ]
  node [
    id 1785
    label "bell_ringer"
  ]
  node [
    id 1786
    label "crisscross"
  ]
  node [
    id 1787
    label "p&#243;&#322;kownik"
  ]
  node [
    id 1788
    label "ekskomunikowa&#263;"
  ]
  node [
    id 1789
    label "kontrola"
  ]
  node [
    id 1790
    label "mark"
  ]
  node [
    id 1791
    label "zdejmowa&#263;"
  ]
  node [
    id 1792
    label "zjawisko"
  ]
  node [
    id 1793
    label "zdj&#281;cie"
  ]
  node [
    id 1794
    label "zdj&#261;&#263;"
  ]
  node [
    id 1795
    label "ekskomunikowanie"
  ]
  node [
    id 1796
    label "harangue"
  ]
  node [
    id 1797
    label "rozmowa"
  ]
  node [
    id 1798
    label "utw&#243;r"
  ]
  node [
    id 1799
    label "pamflet"
  ]
  node [
    id 1800
    label "sprzeciw"
  ]
  node [
    id 1801
    label "przem&#243;wienie"
  ]
  node [
    id 1802
    label "kto&#347;"
  ]
  node [
    id 1803
    label "posta&#263;"
  ]
  node [
    id 1804
    label "osoba"
  ]
  node [
    id 1805
    label "znaczenie"
  ]
  node [
    id 1806
    label "go&#347;&#263;"
  ]
  node [
    id 1807
    label "internowanie"
  ]
  node [
    id 1808
    label "prorz&#261;dowy"
  ]
  node [
    id 1809
    label "internowa&#263;"
  ]
  node [
    id 1810
    label "politycznie"
  ]
  node [
    id 1811
    label "wi&#281;zie&#324;"
  ]
  node [
    id 1812
    label "ideologiczny"
  ]
  node [
    id 1813
    label "kiciarz"
  ]
  node [
    id 1814
    label "pasiak"
  ]
  node [
    id 1815
    label "ideologicznie"
  ]
  node [
    id 1816
    label "internat"
  ]
  node [
    id 1817
    label "zamkn&#261;&#263;"
  ]
  node [
    id 1818
    label "wi&#281;zie&#324;_polityczny"
  ]
  node [
    id 1819
    label "zamyka&#263;"
  ]
  node [
    id 1820
    label "zamykanie"
  ]
  node [
    id 1821
    label "zamkni&#281;cie"
  ]
  node [
    id 1822
    label "oportunistyczny"
  ]
  node [
    id 1823
    label "przychylny"
  ]
  node [
    id 1824
    label "prorz&#261;dowo"
  ]
  node [
    id 1825
    label "Polish"
  ]
  node [
    id 1826
    label "goniony"
  ]
  node [
    id 1827
    label "oberek"
  ]
  node [
    id 1828
    label "ryba_po_grecku"
  ]
  node [
    id 1829
    label "sztajer"
  ]
  node [
    id 1830
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 1831
    label "krakowiak"
  ]
  node [
    id 1832
    label "pierogi_ruskie"
  ]
  node [
    id 1833
    label "lacki"
  ]
  node [
    id 1834
    label "polak"
  ]
  node [
    id 1835
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 1836
    label "chodzony"
  ]
  node [
    id 1837
    label "po_polsku"
  ]
  node [
    id 1838
    label "mazur"
  ]
  node [
    id 1839
    label "polsko"
  ]
  node [
    id 1840
    label "skoczny"
  ]
  node [
    id 1841
    label "drabant"
  ]
  node [
    id 1842
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 1843
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 1844
    label "wschodnioeuropejski"
  ]
  node [
    id 1845
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 1846
    label "poga&#324;ski"
  ]
  node [
    id 1847
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 1848
    label "topielec"
  ]
  node [
    id 1849
    label "zboczenie"
  ]
  node [
    id 1850
    label "om&#243;wienie"
  ]
  node [
    id 1851
    label "sponiewieranie"
  ]
  node [
    id 1852
    label "discipline"
  ]
  node [
    id 1853
    label "rzecz"
  ]
  node [
    id 1854
    label "omawia&#263;"
  ]
  node [
    id 1855
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1856
    label "tre&#347;&#263;"
  ]
  node [
    id 1857
    label "robienie"
  ]
  node [
    id 1858
    label "sponiewiera&#263;"
  ]
  node [
    id 1859
    label "entity"
  ]
  node [
    id 1860
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1861
    label "tematyka"
  ]
  node [
    id 1862
    label "w&#261;tek"
  ]
  node [
    id 1863
    label "charakter"
  ]
  node [
    id 1864
    label "zbaczanie"
  ]
  node [
    id 1865
    label "program_nauczania"
  ]
  node [
    id 1866
    label "om&#243;wi&#263;"
  ]
  node [
    id 1867
    label "omawianie"
  ]
  node [
    id 1868
    label "thing"
  ]
  node [
    id 1869
    label "kultura"
  ]
  node [
    id 1870
    label "istota"
  ]
  node [
    id 1871
    label "zbacza&#263;"
  ]
  node [
    id 1872
    label "zboczy&#263;"
  ]
  node [
    id 1873
    label "gwardzista"
  ]
  node [
    id 1874
    label "&#347;redniowieczny"
  ]
  node [
    id 1875
    label "specjalny"
  ]
  node [
    id 1876
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 1877
    label "weso&#322;y"
  ]
  node [
    id 1878
    label "sprawny"
  ]
  node [
    id 1879
    label "rytmiczny"
  ]
  node [
    id 1880
    label "skocznie"
  ]
  node [
    id 1881
    label "energiczny"
  ]
  node [
    id 1882
    label "lendler"
  ]
  node [
    id 1883
    label "austriacki"
  ]
  node [
    id 1884
    label "polka"
  ]
  node [
    id 1885
    label "przytup"
  ]
  node [
    id 1886
    label "ho&#322;ubiec"
  ]
  node [
    id 1887
    label "wodzi&#263;"
  ]
  node [
    id 1888
    label "ludowy"
  ]
  node [
    id 1889
    label "pie&#347;&#324;"
  ]
  node [
    id 1890
    label "mieszkaniec"
  ]
  node [
    id 1891
    label "centu&#347;"
  ]
  node [
    id 1892
    label "lalka"
  ]
  node [
    id 1893
    label "Ma&#322;opolanin"
  ]
  node [
    id 1894
    label "krakauer"
  ]
  node [
    id 1895
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 1896
    label "umocowa&#263;"
  ]
  node [
    id 1897
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 1898
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1899
    label "procesualistyka"
  ]
  node [
    id 1900
    label "regu&#322;a_Allena"
  ]
  node [
    id 1901
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1902
    label "kryminalistyka"
  ]
  node [
    id 1903
    label "kierunek"
  ]
  node [
    id 1904
    label "zasada_d'Alemberta"
  ]
  node [
    id 1905
    label "obserwacja"
  ]
  node [
    id 1906
    label "normatywizm"
  ]
  node [
    id 1907
    label "jurisprudence"
  ]
  node [
    id 1908
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 1909
    label "prawo_karne_procesowe"
  ]
  node [
    id 1910
    label "criterion"
  ]
  node [
    id 1911
    label "kazuistyka"
  ]
  node [
    id 1912
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1913
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 1914
    label "kryminologia"
  ]
  node [
    id 1915
    label "opis"
  ]
  node [
    id 1916
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1917
    label "prawo_Mendla"
  ]
  node [
    id 1918
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1919
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 1920
    label "prawo_karne"
  ]
  node [
    id 1921
    label "legislacyjnie"
  ]
  node [
    id 1922
    label "twierdzenie"
  ]
  node [
    id 1923
    label "cywilistyka"
  ]
  node [
    id 1924
    label "judykatura"
  ]
  node [
    id 1925
    label "kanonistyka"
  ]
  node [
    id 1926
    label "nauka_prawa"
  ]
  node [
    id 1927
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1928
    label "law"
  ]
  node [
    id 1929
    label "qualification"
  ]
  node [
    id 1930
    label "dominion"
  ]
  node [
    id 1931
    label "wykonawczy"
  ]
  node [
    id 1932
    label "zasada"
  ]
  node [
    id 1933
    label "normalizacja"
  ]
  node [
    id 1934
    label "exposition"
  ]
  node [
    id 1935
    label "obja&#347;nienie"
  ]
  node [
    id 1936
    label "model"
  ]
  node [
    id 1937
    label "organizowa&#263;"
  ]
  node [
    id 1938
    label "ordinariness"
  ]
  node [
    id 1939
    label "instytucja"
  ]
  node [
    id 1940
    label "zorganizowa&#263;"
  ]
  node [
    id 1941
    label "taniec_towarzyski"
  ]
  node [
    id 1942
    label "organizowanie"
  ]
  node [
    id 1943
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1944
    label "zorganizowanie"
  ]
  node [
    id 1945
    label "mechanika"
  ]
  node [
    id 1946
    label "konstrukcja"
  ]
  node [
    id 1947
    label "przebieg"
  ]
  node [
    id 1948
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1949
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1950
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1951
    label "praktyka"
  ]
  node [
    id 1952
    label "przeorientowywanie"
  ]
  node [
    id 1953
    label "studia"
  ]
  node [
    id 1954
    label "bok"
  ]
  node [
    id 1955
    label "skr&#281;canie"
  ]
  node [
    id 1956
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1957
    label "przeorientowywa&#263;"
  ]
  node [
    id 1958
    label "orientowanie"
  ]
  node [
    id 1959
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1960
    label "przeorientowanie"
  ]
  node [
    id 1961
    label "zorientowanie"
  ]
  node [
    id 1962
    label "przeorientowa&#263;"
  ]
  node [
    id 1963
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1964
    label "ty&#322;"
  ]
  node [
    id 1965
    label "zorientowa&#263;"
  ]
  node [
    id 1966
    label "g&#243;ra"
  ]
  node [
    id 1967
    label "orientowa&#263;"
  ]
  node [
    id 1968
    label "ideologia"
  ]
  node [
    id 1969
    label "orientacja"
  ]
  node [
    id 1970
    label "prz&#243;d"
  ]
  node [
    id 1971
    label "skr&#281;cenie"
  ]
  node [
    id 1972
    label "do&#347;wiadczenie"
  ]
  node [
    id 1973
    label "teren_szko&#322;y"
  ]
  node [
    id 1974
    label "wiedza"
  ]
  node [
    id 1975
    label "Mickiewicz"
  ]
  node [
    id 1976
    label "kwalifikacje"
  ]
  node [
    id 1977
    label "podr&#281;cznik"
  ]
  node [
    id 1978
    label "absolwent"
  ]
  node [
    id 1979
    label "school"
  ]
  node [
    id 1980
    label "zda&#263;"
  ]
  node [
    id 1981
    label "gabinet"
  ]
  node [
    id 1982
    label "urszulanki"
  ]
  node [
    id 1983
    label "sztuba"
  ]
  node [
    id 1984
    label "&#322;awa_szkolna"
  ]
  node [
    id 1985
    label "nauka"
  ]
  node [
    id 1986
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1987
    label "przepisa&#263;"
  ]
  node [
    id 1988
    label "muzyka"
  ]
  node [
    id 1989
    label "form"
  ]
  node [
    id 1990
    label "klasa"
  ]
  node [
    id 1991
    label "lekcja"
  ]
  node [
    id 1992
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1993
    label "przepisanie"
  ]
  node [
    id 1994
    label "czas"
  ]
  node [
    id 1995
    label "skolaryzacja"
  ]
  node [
    id 1996
    label "zdanie"
  ]
  node [
    id 1997
    label "stopek"
  ]
  node [
    id 1998
    label "sekretariat"
  ]
  node [
    id 1999
    label "lesson"
  ]
  node [
    id 2000
    label "niepokalanki"
  ]
  node [
    id 2001
    label "siedziba"
  ]
  node [
    id 2002
    label "szkolenie"
  ]
  node [
    id 2003
    label "tablica"
  ]
  node [
    id 2004
    label "posiada&#263;"
  ]
  node [
    id 2005
    label "wydarzenie"
  ]
  node [
    id 2006
    label "potencja&#322;"
  ]
  node [
    id 2007
    label "wyb&#243;r"
  ]
  node [
    id 2008
    label "prospect"
  ]
  node [
    id 2009
    label "ability"
  ]
  node [
    id 2010
    label "obliczeniowo"
  ]
  node [
    id 2011
    label "alternatywa"
  ]
  node [
    id 2012
    label "operator_modalny"
  ]
  node [
    id 2013
    label "badanie"
  ]
  node [
    id 2014
    label "proces_my&#347;lowy"
  ]
  node [
    id 2015
    label "remark"
  ]
  node [
    id 2016
    label "stwierdzenie"
  ]
  node [
    id 2017
    label "observation"
  ]
  node [
    id 2018
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 2019
    label "alternatywa_Fredholma"
  ]
  node [
    id 2020
    label "oznajmianie"
  ]
  node [
    id 2021
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 2022
    label "teoria"
  ]
  node [
    id 2023
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 2024
    label "paradoks_Leontiefa"
  ]
  node [
    id 2025
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 2026
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 2027
    label "teza"
  ]
  node [
    id 2028
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 2029
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 2030
    label "twierdzenie_Pettisa"
  ]
  node [
    id 2031
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 2032
    label "twierdzenie_Maya"
  ]
  node [
    id 2033
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 2034
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 2035
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 2036
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 2037
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 2038
    label "zapewnianie"
  ]
  node [
    id 2039
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 2040
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 2041
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 2042
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 2043
    label "twierdzenie_Stokesa"
  ]
  node [
    id 2044
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 2045
    label "twierdzenie_Cevy"
  ]
  node [
    id 2046
    label "twierdzenie_Pascala"
  ]
  node [
    id 2047
    label "proposition"
  ]
  node [
    id 2048
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 2049
    label "komunikowanie"
  ]
  node [
    id 2050
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 2051
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 2052
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 2053
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 2054
    label "relacja"
  ]
  node [
    id 2055
    label "calibration"
  ]
  node [
    id 2056
    label "operacja"
  ]
  node [
    id 2057
    label "porz&#261;dek"
  ]
  node [
    id 2058
    label "dominance"
  ]
  node [
    id 2059
    label "zabieg"
  ]
  node [
    id 2060
    label "standardization"
  ]
  node [
    id 2061
    label "zmiana"
  ]
  node [
    id 2062
    label "orzecznictwo"
  ]
  node [
    id 2063
    label "wykonawczo"
  ]
  node [
    id 2064
    label "byt"
  ]
  node [
    id 2065
    label "osobowo&#347;&#263;"
  ]
  node [
    id 2066
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 2067
    label "nada&#263;"
  ]
  node [
    id 2068
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 2069
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 2070
    label "status"
  ]
  node [
    id 2071
    label "procedura"
  ]
  node [
    id 2072
    label "norma_prawna"
  ]
  node [
    id 2073
    label "przedawnienie_si&#281;"
  ]
  node [
    id 2074
    label "przedawnianie_si&#281;"
  ]
  node [
    id 2075
    label "porada"
  ]
  node [
    id 2076
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 2077
    label "regulation"
  ]
  node [
    id 2078
    label "recepta"
  ]
  node [
    id 2079
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 2080
    label "kodeks"
  ]
  node [
    id 2081
    label "base"
  ]
  node [
    id 2082
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 2083
    label "moralno&#347;&#263;"
  ]
  node [
    id 2084
    label "occupation"
  ]
  node [
    id 2085
    label "podstawa"
  ]
  node [
    id 2086
    label "prawid&#322;o"
  ]
  node [
    id 2087
    label "casuistry"
  ]
  node [
    id 2088
    label "manipulacja"
  ]
  node [
    id 2089
    label "probabilizm"
  ]
  node [
    id 2090
    label "dermatoglifika"
  ]
  node [
    id 2091
    label "mikro&#347;lad"
  ]
  node [
    id 2092
    label "technika_&#347;ledcza"
  ]
  node [
    id 2093
    label "provident"
  ]
  node [
    id 2094
    label "rozwa&#380;ny"
  ]
  node [
    id 2095
    label "zabiegliwy"
  ]
  node [
    id 2096
    label "opatrzny"
  ]
  node [
    id 2097
    label "ostro&#380;ny"
  ]
  node [
    id 2098
    label "rozs&#261;dny"
  ]
  node [
    id 2099
    label "rozwa&#380;nie"
  ]
  node [
    id 2100
    label "Boski"
  ]
  node [
    id 2101
    label "opatrznie"
  ]
  node [
    id 2102
    label "przezorny"
  ]
  node [
    id 2103
    label "zwrot"
  ]
  node [
    id 2104
    label "wn&#281;ka"
  ]
  node [
    id 2105
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 2106
    label "discourtesy"
  ]
  node [
    id 2107
    label "post&#281;pek"
  ]
  node [
    id 2108
    label "transgresja"
  ]
  node [
    id 2109
    label "zrobienie"
  ]
  node [
    id 2110
    label "narobienie"
  ]
  node [
    id 2111
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 2112
    label "creation"
  ]
  node [
    id 2113
    label "porobienie"
  ]
  node [
    id 2114
    label "action"
  ]
  node [
    id 2115
    label "czyn"
  ]
  node [
    id 2116
    label "funkcja"
  ]
  node [
    id 2117
    label "act"
  ]
  node [
    id 2118
    label "proces_biologiczny"
  ]
  node [
    id 2119
    label "zamiana"
  ]
  node [
    id 2120
    label "transgression"
  ]
  node [
    id 2121
    label "naruszenie"
  ]
  node [
    id 2122
    label "proces_geologiczny"
  ]
  node [
    id 2123
    label "roztapianie_si&#281;"
  ]
  node [
    id 2124
    label "medialnie"
  ]
  node [
    id 2125
    label "medialny"
  ]
  node [
    id 2126
    label "popularny"
  ]
  node [
    id 2127
    label "&#347;rodkowy"
  ]
  node [
    id 2128
    label "nieprawdziwy"
  ]
  node [
    id 2129
    label "popularnie"
  ]
  node [
    id 2130
    label "centralnie"
  ]
  node [
    id 2131
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 2132
    label "mie&#263;_miejsce"
  ]
  node [
    id 2133
    label "equal"
  ]
  node [
    id 2134
    label "trwa&#263;"
  ]
  node [
    id 2135
    label "chodzi&#263;"
  ]
  node [
    id 2136
    label "si&#281;ga&#263;"
  ]
  node [
    id 2137
    label "stan"
  ]
  node [
    id 2138
    label "obecno&#347;&#263;"
  ]
  node [
    id 2139
    label "stand"
  ]
  node [
    id 2140
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 2141
    label "uczestniczy&#263;"
  ]
  node [
    id 2142
    label "participate"
  ]
  node [
    id 2143
    label "robi&#263;"
  ]
  node [
    id 2144
    label "istnie&#263;"
  ]
  node [
    id 2145
    label "pozostawa&#263;"
  ]
  node [
    id 2146
    label "zostawa&#263;"
  ]
  node [
    id 2147
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 2148
    label "adhere"
  ]
  node [
    id 2149
    label "compass"
  ]
  node [
    id 2150
    label "korzysta&#263;"
  ]
  node [
    id 2151
    label "appreciation"
  ]
  node [
    id 2152
    label "osi&#261;ga&#263;"
  ]
  node [
    id 2153
    label "dociera&#263;"
  ]
  node [
    id 2154
    label "get"
  ]
  node [
    id 2155
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 2156
    label "mierzy&#263;"
  ]
  node [
    id 2157
    label "u&#380;ywa&#263;"
  ]
  node [
    id 2158
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 2159
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 2160
    label "exsert"
  ]
  node [
    id 2161
    label "being"
  ]
  node [
    id 2162
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 2163
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 2164
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 2165
    label "p&#322;ywa&#263;"
  ]
  node [
    id 2166
    label "run"
  ]
  node [
    id 2167
    label "bangla&#263;"
  ]
  node [
    id 2168
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 2169
    label "przebiega&#263;"
  ]
  node [
    id 2170
    label "wk&#322;ada&#263;"
  ]
  node [
    id 2171
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 2172
    label "carry"
  ]
  node [
    id 2173
    label "bywa&#263;"
  ]
  node [
    id 2174
    label "dziama&#263;"
  ]
  node [
    id 2175
    label "stara&#263;_si&#281;"
  ]
  node [
    id 2176
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 2177
    label "str&#243;j"
  ]
  node [
    id 2178
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 2179
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 2180
    label "krok"
  ]
  node [
    id 2181
    label "tryb"
  ]
  node [
    id 2182
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 2183
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 2184
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 2185
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 2186
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 2187
    label "continue"
  ]
  node [
    id 2188
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 2189
    label "wci&#281;cie"
  ]
  node [
    id 2190
    label "warstwa"
  ]
  node [
    id 2191
    label "samopoczucie"
  ]
  node [
    id 2192
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 2193
    label "wektor"
  ]
  node [
    id 2194
    label "Goa"
  ]
  node [
    id 2195
    label "poziom"
  ]
  node [
    id 2196
    label "shape"
  ]
  node [
    id 2197
    label "ilo&#347;&#263;"
  ]
  node [
    id 2198
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 2199
    label "niezgodny"
  ]
  node [
    id 2200
    label "sprzecznie"
  ]
  node [
    id 2201
    label "odmienny"
  ]
  node [
    id 2202
    label "k&#322;&#243;tny"
  ]
  node [
    id 2203
    label "napi&#281;ty"
  ]
  node [
    id 2204
    label "prezenter"
  ]
  node [
    id 2205
    label "typ"
  ]
  node [
    id 2206
    label "mildew"
  ]
  node [
    id 2207
    label "zi&#243;&#322;ko"
  ]
  node [
    id 2208
    label "motif"
  ]
  node [
    id 2209
    label "pozowanie"
  ]
  node [
    id 2210
    label "ideal"
  ]
  node [
    id 2211
    label "wz&#243;r"
  ]
  node [
    id 2212
    label "matryca"
  ]
  node [
    id 2213
    label "adaptation"
  ]
  node [
    id 2214
    label "ruch"
  ]
  node [
    id 2215
    label "pozowa&#263;"
  ]
  node [
    id 2216
    label "imitacja"
  ]
  node [
    id 2217
    label "orygina&#322;"
  ]
  node [
    id 2218
    label "facet"
  ]
  node [
    id 2219
    label "miniatura"
  ]
  node [
    id 2220
    label "osoba_prawna"
  ]
  node [
    id 2221
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 2222
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 2223
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 2224
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 2225
    label "biuro"
  ]
  node [
    id 2226
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 2227
    label "establishment"
  ]
  node [
    id 2228
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 2229
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 2230
    label "afiliowa&#263;"
  ]
  node [
    id 2231
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 2232
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 2233
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 2234
    label "dostosowa&#263;"
  ]
  node [
    id 2235
    label "pozyska&#263;"
  ]
  node [
    id 2236
    label "plan"
  ]
  node [
    id 2237
    label "stage"
  ]
  node [
    id 2238
    label "urobi&#263;"
  ]
  node [
    id 2239
    label "ensnare"
  ]
  node [
    id 2240
    label "wprowadzi&#263;"
  ]
  node [
    id 2241
    label "zaplanowa&#263;"
  ]
  node [
    id 2242
    label "skupi&#263;"
  ]
  node [
    id 2243
    label "tworzenie"
  ]
  node [
    id 2244
    label "organizowanie_si&#281;"
  ]
  node [
    id 2245
    label "dyscyplinowanie"
  ]
  node [
    id 2246
    label "uregulowanie"
  ]
  node [
    id 2247
    label "handling"
  ]
  node [
    id 2248
    label "pozyskiwanie"
  ]
  node [
    id 2249
    label "szykowanie"
  ]
  node [
    id 2250
    label "wprowadzanie"
  ]
  node [
    id 2251
    label "skupianie"
  ]
  node [
    id 2252
    label "planowa&#263;"
  ]
  node [
    id 2253
    label "dostosowywa&#263;"
  ]
  node [
    id 2254
    label "treat"
  ]
  node [
    id 2255
    label "pozyskiwa&#263;"
  ]
  node [
    id 2256
    label "skupia&#263;"
  ]
  node [
    id 2257
    label "create"
  ]
  node [
    id 2258
    label "przygotowywa&#263;"
  ]
  node [
    id 2259
    label "wprowadza&#263;"
  ]
  node [
    id 2260
    label "zorganizowanie_si&#281;"
  ]
  node [
    id 2261
    label "stworzenie"
  ]
  node [
    id 2262
    label "zdyscyplinowanie"
  ]
  node [
    id 2263
    label "wprowadzenie"
  ]
  node [
    id 2264
    label "bargain"
  ]
  node [
    id 2265
    label "pozyskanie"
  ]
  node [
    id 2266
    label "constitution"
  ]
  node [
    id 2267
    label "necessity"
  ]
  node [
    id 2268
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 2269
    label "trza"
  ]
  node [
    id 2270
    label "trzeba"
  ]
  node [
    id 2271
    label "pair"
  ]
  node [
    id 2272
    label "odparowywanie"
  ]
  node [
    id 2273
    label "gaz_cieplarniany"
  ]
  node [
    id 2274
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 2275
    label "poker"
  ]
  node [
    id 2276
    label "moneta"
  ]
  node [
    id 2277
    label "parowanie"
  ]
  node [
    id 2278
    label "damp"
  ]
  node [
    id 2279
    label "sztuka"
  ]
  node [
    id 2280
    label "odparowanie"
  ]
  node [
    id 2281
    label "odparowa&#263;"
  ]
  node [
    id 2282
    label "dodatek"
  ]
  node [
    id 2283
    label "jednostka_monetarna"
  ]
  node [
    id 2284
    label "smoke"
  ]
  node [
    id 2285
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 2286
    label "odparowywa&#263;"
  ]
  node [
    id 2287
    label "gaz"
  ]
  node [
    id 2288
    label "wyparowanie"
  ]
  node [
    id 2289
    label "uaktualni&#263;"
  ]
  node [
    id 2290
    label "update"
  ]
  node [
    id 2291
    label "uwsp&#243;&#322;cze&#347;ni&#263;"
  ]
  node [
    id 2292
    label "nienowoczesny"
  ]
  node [
    id 2293
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 2294
    label "zatabaczony"
  ]
  node [
    id 2295
    label "niewsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 2296
    label "zdezaktualizowanie_si&#281;"
  ]
  node [
    id 2297
    label "dezaktualizowanie_si&#281;"
  ]
  node [
    id 2298
    label "nier&#243;wnolegle"
  ]
  node [
    id 2299
    label "miniony"
  ]
  node [
    id 2300
    label "stary"
  ]
  node [
    id 2301
    label "niedzisiejszy"
  ]
  node [
    id 2302
    label "staro&#347;wiecki"
  ]
  node [
    id 2303
    label "poplamiony"
  ]
  node [
    id 2304
    label "zaniedbany"
  ]
  node [
    id 2305
    label "narz&#281;dzie"
  ]
  node [
    id 2306
    label "nature"
  ]
  node [
    id 2307
    label "wskaz&#243;wka"
  ]
  node [
    id 2308
    label "zlecenie"
  ]
  node [
    id 2309
    label "receipt"
  ]
  node [
    id 2310
    label "receptariusz"
  ]
  node [
    id 2311
    label "obwiniony"
  ]
  node [
    id 2312
    label "r&#281;kopis"
  ]
  node [
    id 2313
    label "kodeks_pracy"
  ]
  node [
    id 2314
    label "kodeks_morski"
  ]
  node [
    id 2315
    label "Justynian"
  ]
  node [
    id 2316
    label "kodeks_drogowy"
  ]
  node [
    id 2317
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 2318
    label "kodeks_rodzinny"
  ]
  node [
    id 2319
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 2320
    label "kodeks_cywilny"
  ]
  node [
    id 2321
    label "kodeks_karny"
  ]
  node [
    id 2322
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 2323
    label "krzew"
  ]
  node [
    id 2324
    label "delfinidyna"
  ]
  node [
    id 2325
    label "pi&#380;maczkowate"
  ]
  node [
    id 2326
    label "ki&#347;&#263;"
  ]
  node [
    id 2327
    label "hy&#263;ka"
  ]
  node [
    id 2328
    label "pestkowiec"
  ]
  node [
    id 2329
    label "kwiat"
  ]
  node [
    id 2330
    label "owoc"
  ]
  node [
    id 2331
    label "oliwkowate"
  ]
  node [
    id 2332
    label "lilac"
  ]
  node [
    id 2333
    label "flakon"
  ]
  node [
    id 2334
    label "przykoronek"
  ]
  node [
    id 2335
    label "kielich"
  ]
  node [
    id 2336
    label "dno_kwiatowe"
  ]
  node [
    id 2337
    label "organ_ro&#347;linny"
  ]
  node [
    id 2338
    label "ogon"
  ]
  node [
    id 2339
    label "warga"
  ]
  node [
    id 2340
    label "korona"
  ]
  node [
    id 2341
    label "rurka"
  ]
  node [
    id 2342
    label "ozdoba"
  ]
  node [
    id 2343
    label "kostka"
  ]
  node [
    id 2344
    label "kita"
  ]
  node [
    id 2345
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 2346
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 2347
    label "d&#322;o&#324;"
  ]
  node [
    id 2348
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 2349
    label "powerball"
  ]
  node [
    id 2350
    label "&#380;ubr"
  ]
  node [
    id 2351
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 2352
    label "p&#281;k"
  ]
  node [
    id 2353
    label "r&#281;ka"
  ]
  node [
    id 2354
    label "zako&#324;czenie"
  ]
  node [
    id 2355
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 2356
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 2357
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 2358
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 2359
    label "&#322;yko"
  ]
  node [
    id 2360
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 2361
    label "karczowa&#263;"
  ]
  node [
    id 2362
    label "wykarczowanie"
  ]
  node [
    id 2363
    label "skupina"
  ]
  node [
    id 2364
    label "wykarczowa&#263;"
  ]
  node [
    id 2365
    label "karczowanie"
  ]
  node [
    id 2366
    label "fanerofit"
  ]
  node [
    id 2367
    label "zbiorowisko"
  ]
  node [
    id 2368
    label "ro&#347;liny"
  ]
  node [
    id 2369
    label "p&#281;d"
  ]
  node [
    id 2370
    label "wegetowanie"
  ]
  node [
    id 2371
    label "zadziorek"
  ]
  node [
    id 2372
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 2373
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 2374
    label "do&#322;owa&#263;"
  ]
  node [
    id 2375
    label "wegetacja"
  ]
  node [
    id 2376
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 2377
    label "strzyc"
  ]
  node [
    id 2378
    label "w&#322;&#243;kno"
  ]
  node [
    id 2379
    label "g&#322;uszenie"
  ]
  node [
    id 2380
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 2381
    label "fitotron"
  ]
  node [
    id 2382
    label "bulwka"
  ]
  node [
    id 2383
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 2384
    label "odn&#243;&#380;ka"
  ]
  node [
    id 2385
    label "epiderma"
  ]
  node [
    id 2386
    label "gumoza"
  ]
  node [
    id 2387
    label "strzy&#380;enie"
  ]
  node [
    id 2388
    label "wypotnik"
  ]
  node [
    id 2389
    label "flawonoid"
  ]
  node [
    id 2390
    label "wyro&#347;le"
  ]
  node [
    id 2391
    label "do&#322;owanie"
  ]
  node [
    id 2392
    label "g&#322;uszy&#263;"
  ]
  node [
    id 2393
    label "pora&#380;a&#263;"
  ]
  node [
    id 2394
    label "fitocenoza"
  ]
  node [
    id 2395
    label "hodowla"
  ]
  node [
    id 2396
    label "fotoautotrof"
  ]
  node [
    id 2397
    label "nieuleczalnie_chory"
  ]
  node [
    id 2398
    label "wegetowa&#263;"
  ]
  node [
    id 2399
    label "pochewka"
  ]
  node [
    id 2400
    label "sok"
  ]
  node [
    id 2401
    label "system_korzeniowy"
  ]
  node [
    id 2402
    label "zawi&#261;zek"
  ]
  node [
    id 2403
    label "mi&#261;&#380;sz"
  ]
  node [
    id 2404
    label "frukt"
  ]
  node [
    id 2405
    label "drylowanie"
  ]
  node [
    id 2406
    label "produkt"
  ]
  node [
    id 2407
    label "owocnia"
  ]
  node [
    id 2408
    label "fruktoza"
  ]
  node [
    id 2409
    label "obiekt"
  ]
  node [
    id 2410
    label "gniazdo_nasienne"
  ]
  node [
    id 2411
    label "glukoza"
  ]
  node [
    id 2412
    label "pestka"
  ]
  node [
    id 2413
    label "antocyjanidyn"
  ]
  node [
    id 2414
    label "szczeciowce"
  ]
  node [
    id 2415
    label "jasnotowce"
  ]
  node [
    id 2416
    label "Oleaceae"
  ]
  node [
    id 2417
    label "wielkopolski"
  ]
  node [
    id 2418
    label "bez_czarny"
  ]
  node [
    id 2419
    label "przej&#347;cie"
  ]
  node [
    id 2420
    label "kres"
  ]
  node [
    id 2421
    label "Ural"
  ]
  node [
    id 2422
    label "miara"
  ]
  node [
    id 2423
    label "end"
  ]
  node [
    id 2424
    label "pu&#322;ap"
  ]
  node [
    id 2425
    label "granice"
  ]
  node [
    id 2426
    label "frontier"
  ]
  node [
    id 2427
    label "mini&#281;cie"
  ]
  node [
    id 2428
    label "ustawa"
  ]
  node [
    id 2429
    label "wymienienie"
  ]
  node [
    id 2430
    label "zaliczenie"
  ]
  node [
    id 2431
    label "traversal"
  ]
  node [
    id 2432
    label "zdarzenie_si&#281;"
  ]
  node [
    id 2433
    label "przewy&#380;szenie"
  ]
  node [
    id 2434
    label "experience"
  ]
  node [
    id 2435
    label "przepuszczenie"
  ]
  node [
    id 2436
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 2437
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 2438
    label "strain"
  ]
  node [
    id 2439
    label "faza"
  ]
  node [
    id 2440
    label "przerobienie"
  ]
  node [
    id 2441
    label "wydeptywanie"
  ]
  node [
    id 2442
    label "crack"
  ]
  node [
    id 2443
    label "wydeptanie"
  ]
  node [
    id 2444
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 2445
    label "wstawka"
  ]
  node [
    id 2446
    label "prze&#380;ycie"
  ]
  node [
    id 2447
    label "uznanie"
  ]
  node [
    id 2448
    label "doznanie"
  ]
  node [
    id 2449
    label "dostanie_si&#281;"
  ]
  node [
    id 2450
    label "trwanie"
  ]
  node [
    id 2451
    label "przebycie"
  ]
  node [
    id 2452
    label "wytyczenie"
  ]
  node [
    id 2453
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 2454
    label "przepojenie"
  ]
  node [
    id 2455
    label "nas&#261;czenie"
  ]
  node [
    id 2456
    label "nale&#380;enie"
  ]
  node [
    id 2457
    label "mienie"
  ]
  node [
    id 2458
    label "odmienienie"
  ]
  node [
    id 2459
    label "przedostanie_si&#281;"
  ]
  node [
    id 2460
    label "przemokni&#281;cie"
  ]
  node [
    id 2461
    label "nasycenie_si&#281;"
  ]
  node [
    id 2462
    label "zacz&#281;cie"
  ]
  node [
    id 2463
    label "stanie_si&#281;"
  ]
  node [
    id 2464
    label "offense"
  ]
  node [
    id 2465
    label "przestanie"
  ]
  node [
    id 2466
    label "skumanie"
  ]
  node [
    id 2467
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 2468
    label "clasp"
  ]
  node [
    id 2469
    label "forma"
  ]
  node [
    id 2470
    label "strop"
  ]
  node [
    id 2471
    label "powa&#322;a"
  ]
  node [
    id 2472
    label "wysoko&#347;&#263;"
  ]
  node [
    id 2473
    label "ostatnie_podrygi"
  ]
  node [
    id 2474
    label "chwila"
  ]
  node [
    id 2475
    label "visitation"
  ]
  node [
    id 2476
    label "agonia"
  ]
  node [
    id 2477
    label "defenestracja"
  ]
  node [
    id 2478
    label "mogi&#322;a"
  ]
  node [
    id 2479
    label "kres_&#380;ycia"
  ]
  node [
    id 2480
    label "szereg"
  ]
  node [
    id 2481
    label "szeol"
  ]
  node [
    id 2482
    label "pogrzebanie"
  ]
  node [
    id 2483
    label "&#380;a&#322;oba"
  ]
  node [
    id 2484
    label "zabicie"
  ]
  node [
    id 2485
    label "proportion"
  ]
  node [
    id 2486
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 2487
    label "wielko&#347;&#263;"
  ]
  node [
    id 2488
    label "continence"
  ]
  node [
    id 2489
    label "supremum"
  ]
  node [
    id 2490
    label "skala"
  ]
  node [
    id 2491
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 2492
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 2493
    label "jednostka"
  ]
  node [
    id 2494
    label "przeliczy&#263;"
  ]
  node [
    id 2495
    label "matematyka"
  ]
  node [
    id 2496
    label "rzut"
  ]
  node [
    id 2497
    label "odwiedziny"
  ]
  node [
    id 2498
    label "liczba"
  ]
  node [
    id 2499
    label "warunek_lokalowy"
  ]
  node [
    id 2500
    label "przeliczanie"
  ]
  node [
    id 2501
    label "dymensja"
  ]
  node [
    id 2502
    label "przelicza&#263;"
  ]
  node [
    id 2503
    label "infimum"
  ]
  node [
    id 2504
    label "przeliczenie"
  ]
  node [
    id 2505
    label "sfera"
  ]
  node [
    id 2506
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 2507
    label "podzakres"
  ]
  node [
    id 2508
    label "dziedzina"
  ]
  node [
    id 2509
    label "desygnat"
  ]
  node [
    id 2510
    label "circle"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 643
  ]
  edge [
    source 14
    target 644
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 651
  ]
  edge [
    source 14
    target 652
  ]
  edge [
    source 14
    target 653
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 63
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 983
  ]
  edge [
    source 16
    target 984
  ]
  edge [
    source 16
    target 985
  ]
  edge [
    source 16
    target 986
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 988
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 990
  ]
  edge [
    source 16
    target 991
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 993
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 995
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 16
    target 997
  ]
  edge [
    source 16
    target 998
  ]
  edge [
    source 16
    target 999
  ]
  edge [
    source 16
    target 1000
  ]
  edge [
    source 16
    target 1001
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 1007
  ]
  edge [
    source 16
    target 1008
  ]
  edge [
    source 16
    target 1009
  ]
  edge [
    source 16
    target 1010
  ]
  edge [
    source 16
    target 1011
  ]
  edge [
    source 16
    target 1012
  ]
  edge [
    source 16
    target 1013
  ]
  edge [
    source 16
    target 1014
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 1015
  ]
  edge [
    source 16
    target 1016
  ]
  edge [
    source 16
    target 1017
  ]
  edge [
    source 16
    target 494
  ]
  edge [
    source 16
    target 1018
  ]
  edge [
    source 16
    target 1019
  ]
  edge [
    source 16
    target 1020
  ]
  edge [
    source 16
    target 1021
  ]
  edge [
    source 16
    target 1022
  ]
  edge [
    source 16
    target 1023
  ]
  edge [
    source 16
    target 1024
  ]
  edge [
    source 16
    target 1025
  ]
  edge [
    source 16
    target 1026
  ]
  edge [
    source 16
    target 1027
  ]
  edge [
    source 16
    target 1028
  ]
  edge [
    source 16
    target 1029
  ]
  edge [
    source 16
    target 1030
  ]
  edge [
    source 16
    target 1031
  ]
  edge [
    source 16
    target 1032
  ]
  edge [
    source 16
    target 1033
  ]
  edge [
    source 16
    target 1034
  ]
  edge [
    source 16
    target 1035
  ]
  edge [
    source 16
    target 1036
  ]
  edge [
    source 16
    target 1037
  ]
  edge [
    source 16
    target 59
  ]
  edge [
    source 16
    target 1038
  ]
  edge [
    source 16
    target 1039
  ]
  edge [
    source 16
    target 1040
  ]
  edge [
    source 16
    target 1041
  ]
  edge [
    source 16
    target 1042
  ]
  edge [
    source 16
    target 1043
  ]
  edge [
    source 16
    target 1044
  ]
  edge [
    source 16
    target 1045
  ]
  edge [
    source 16
    target 1046
  ]
  edge [
    source 16
    target 1047
  ]
  edge [
    source 16
    target 1048
  ]
  edge [
    source 16
    target 1049
  ]
  edge [
    source 16
    target 1050
  ]
  edge [
    source 16
    target 1051
  ]
  edge [
    source 16
    target 1052
  ]
  edge [
    source 16
    target 1053
  ]
  edge [
    source 16
    target 1054
  ]
  edge [
    source 16
    target 1055
  ]
  edge [
    source 16
    target 1056
  ]
  edge [
    source 16
    target 1057
  ]
  edge [
    source 16
    target 1058
  ]
  edge [
    source 16
    target 1059
  ]
  edge [
    source 16
    target 1060
  ]
  edge [
    source 16
    target 1061
  ]
  edge [
    source 16
    target 1062
  ]
  edge [
    source 16
    target 1063
  ]
  edge [
    source 16
    target 1064
  ]
  edge [
    source 16
    target 1065
  ]
  edge [
    source 16
    target 1066
  ]
  edge [
    source 16
    target 1067
  ]
  edge [
    source 16
    target 1068
  ]
  edge [
    source 16
    target 1069
  ]
  edge [
    source 16
    target 1070
  ]
  edge [
    source 16
    target 1071
  ]
  edge [
    source 16
    target 1072
  ]
  edge [
    source 16
    target 1073
  ]
  edge [
    source 16
    target 1074
  ]
  edge [
    source 16
    target 1075
  ]
  edge [
    source 16
    target 1076
  ]
  edge [
    source 16
    target 1077
  ]
  edge [
    source 16
    target 1078
  ]
  edge [
    source 16
    target 1079
  ]
  edge [
    source 16
    target 1080
  ]
  edge [
    source 16
    target 1081
  ]
  edge [
    source 16
    target 1082
  ]
  edge [
    source 16
    target 1083
  ]
  edge [
    source 16
    target 1084
  ]
  edge [
    source 16
    target 1085
  ]
  edge [
    source 16
    target 1086
  ]
  edge [
    source 16
    target 1087
  ]
  edge [
    source 16
    target 1088
  ]
  edge [
    source 16
    target 1089
  ]
  edge [
    source 16
    target 1090
  ]
  edge [
    source 16
    target 1091
  ]
  edge [
    source 16
    target 1092
  ]
  edge [
    source 16
    target 1093
  ]
  edge [
    source 16
    target 1094
  ]
  edge [
    source 16
    target 1095
  ]
  edge [
    source 16
    target 1096
  ]
  edge [
    source 16
    target 1097
  ]
  edge [
    source 16
    target 1098
  ]
  edge [
    source 16
    target 1099
  ]
  edge [
    source 16
    target 1100
  ]
  edge [
    source 16
    target 1101
  ]
  edge [
    source 16
    target 1102
  ]
  edge [
    source 16
    target 1103
  ]
  edge [
    source 16
    target 1104
  ]
  edge [
    source 16
    target 1105
  ]
  edge [
    source 16
    target 1106
  ]
  edge [
    source 16
    target 1107
  ]
  edge [
    source 16
    target 1108
  ]
  edge [
    source 16
    target 1109
  ]
  edge [
    source 16
    target 1110
  ]
  edge [
    source 16
    target 1111
  ]
  edge [
    source 16
    target 1112
  ]
  edge [
    source 16
    target 1113
  ]
  edge [
    source 16
    target 1114
  ]
  edge [
    source 16
    target 1115
  ]
  edge [
    source 16
    target 1116
  ]
  edge [
    source 16
    target 1117
  ]
  edge [
    source 16
    target 1118
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 1120
  ]
  edge [
    source 16
    target 1121
  ]
  edge [
    source 16
    target 1122
  ]
  edge [
    source 16
    target 1123
  ]
  edge [
    source 16
    target 1124
  ]
  edge [
    source 16
    target 1125
  ]
  edge [
    source 16
    target 1126
  ]
  edge [
    source 16
    target 1127
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 1129
  ]
  edge [
    source 16
    target 1130
  ]
  edge [
    source 16
    target 1131
  ]
  edge [
    source 16
    target 1132
  ]
  edge [
    source 16
    target 1133
  ]
  edge [
    source 16
    target 1134
  ]
  edge [
    source 16
    target 1135
  ]
  edge [
    source 16
    target 1136
  ]
  edge [
    source 16
    target 1137
  ]
  edge [
    source 16
    target 1138
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1140
  ]
  edge [
    source 16
    target 1141
  ]
  edge [
    source 16
    target 1142
  ]
  edge [
    source 16
    target 1143
  ]
  edge [
    source 16
    target 1144
  ]
  edge [
    source 16
    target 1145
  ]
  edge [
    source 16
    target 1146
  ]
  edge [
    source 16
    target 1147
  ]
  edge [
    source 16
    target 1148
  ]
  edge [
    source 16
    target 1149
  ]
  edge [
    source 16
    target 1150
  ]
  edge [
    source 16
    target 1151
  ]
  edge [
    source 16
    target 1152
  ]
  edge [
    source 16
    target 1153
  ]
  edge [
    source 16
    target 1154
  ]
  edge [
    source 16
    target 1155
  ]
  edge [
    source 16
    target 1156
  ]
  edge [
    source 16
    target 1157
  ]
  edge [
    source 16
    target 1158
  ]
  edge [
    source 16
    target 1159
  ]
  edge [
    source 16
    target 1160
  ]
  edge [
    source 16
    target 1161
  ]
  edge [
    source 16
    target 1162
  ]
  edge [
    source 16
    target 1163
  ]
  edge [
    source 16
    target 1164
  ]
  edge [
    source 16
    target 1165
  ]
  edge [
    source 16
    target 1166
  ]
  edge [
    source 16
    target 1167
  ]
  edge [
    source 16
    target 1168
  ]
  edge [
    source 16
    target 1169
  ]
  edge [
    source 16
    target 1170
  ]
  edge [
    source 16
    target 1171
  ]
  edge [
    source 16
    target 1172
  ]
  edge [
    source 16
    target 1173
  ]
  edge [
    source 16
    target 1174
  ]
  edge [
    source 16
    target 1175
  ]
  edge [
    source 16
    target 1176
  ]
  edge [
    source 16
    target 1177
  ]
  edge [
    source 16
    target 1178
  ]
  edge [
    source 16
    target 1179
  ]
  edge [
    source 16
    target 1180
  ]
  edge [
    source 16
    target 1181
  ]
  edge [
    source 16
    target 1182
  ]
  edge [
    source 16
    target 1183
  ]
  edge [
    source 16
    target 1184
  ]
  edge [
    source 16
    target 1185
  ]
  edge [
    source 16
    target 1186
  ]
  edge [
    source 16
    target 1187
  ]
  edge [
    source 16
    target 1188
  ]
  edge [
    source 16
    target 1189
  ]
  edge [
    source 16
    target 1190
  ]
  edge [
    source 16
    target 1191
  ]
  edge [
    source 16
    target 1192
  ]
  edge [
    source 16
    target 1193
  ]
  edge [
    source 16
    target 1194
  ]
  edge [
    source 16
    target 1195
  ]
  edge [
    source 16
    target 1196
  ]
  edge [
    source 16
    target 1197
  ]
  edge [
    source 16
    target 1198
  ]
  edge [
    source 16
    target 1199
  ]
  edge [
    source 16
    target 1200
  ]
  edge [
    source 16
    target 1201
  ]
  edge [
    source 16
    target 1202
  ]
  edge [
    source 16
    target 1203
  ]
  edge [
    source 16
    target 1204
  ]
  edge [
    source 16
    target 1205
  ]
  edge [
    source 16
    target 1206
  ]
  edge [
    source 16
    target 1207
  ]
  edge [
    source 16
    target 1208
  ]
  edge [
    source 16
    target 1209
  ]
  edge [
    source 16
    target 1210
  ]
  edge [
    source 16
    target 1211
  ]
  edge [
    source 16
    target 1212
  ]
  edge [
    source 16
    target 1213
  ]
  edge [
    source 16
    target 1214
  ]
  edge [
    source 16
    target 1215
  ]
  edge [
    source 16
    target 1216
  ]
  edge [
    source 16
    target 1217
  ]
  edge [
    source 16
    target 1218
  ]
  edge [
    source 16
    target 1219
  ]
  edge [
    source 16
    target 1220
  ]
  edge [
    source 16
    target 1221
  ]
  edge [
    source 16
    target 1222
  ]
  edge [
    source 16
    target 1223
  ]
  edge [
    source 16
    target 1224
  ]
  edge [
    source 16
    target 1225
  ]
  edge [
    source 16
    target 1226
  ]
  edge [
    source 16
    target 1227
  ]
  edge [
    source 16
    target 1228
  ]
  edge [
    source 16
    target 1229
  ]
  edge [
    source 16
    target 1230
  ]
  edge [
    source 16
    target 1231
  ]
  edge [
    source 16
    target 1232
  ]
  edge [
    source 16
    target 1233
  ]
  edge [
    source 16
    target 1234
  ]
  edge [
    source 16
    target 1235
  ]
  edge [
    source 16
    target 1236
  ]
  edge [
    source 16
    target 1237
  ]
  edge [
    source 16
    target 1238
  ]
  edge [
    source 16
    target 1239
  ]
  edge [
    source 16
    target 1240
  ]
  edge [
    source 16
    target 1241
  ]
  edge [
    source 16
    target 1242
  ]
  edge [
    source 16
    target 1243
  ]
  edge [
    source 16
    target 1244
  ]
  edge [
    source 16
    target 1245
  ]
  edge [
    source 16
    target 1246
  ]
  edge [
    source 16
    target 1247
  ]
  edge [
    source 16
    target 1248
  ]
  edge [
    source 16
    target 1249
  ]
  edge [
    source 16
    target 1250
  ]
  edge [
    source 16
    target 1251
  ]
  edge [
    source 16
    target 1252
  ]
  edge [
    source 16
    target 1253
  ]
  edge [
    source 16
    target 1254
  ]
  edge [
    source 16
    target 1255
  ]
  edge [
    source 16
    target 1256
  ]
  edge [
    source 16
    target 1257
  ]
  edge [
    source 16
    target 1258
  ]
  edge [
    source 16
    target 1259
  ]
  edge [
    source 16
    target 1260
  ]
  edge [
    source 16
    target 1261
  ]
  edge [
    source 16
    target 1262
  ]
  edge [
    source 16
    target 1263
  ]
  edge [
    source 16
    target 1264
  ]
  edge [
    source 16
    target 1265
  ]
  edge [
    source 16
    target 1266
  ]
  edge [
    source 16
    target 1267
  ]
  edge [
    source 16
    target 1268
  ]
  edge [
    source 16
    target 1269
  ]
  edge [
    source 16
    target 1270
  ]
  edge [
    source 16
    target 1271
  ]
  edge [
    source 16
    target 1272
  ]
  edge [
    source 16
    target 1273
  ]
  edge [
    source 16
    target 1274
  ]
  edge [
    source 16
    target 1275
  ]
  edge [
    source 16
    target 1276
  ]
  edge [
    source 16
    target 1277
  ]
  edge [
    source 16
    target 1278
  ]
  edge [
    source 16
    target 1279
  ]
  edge [
    source 16
    target 1280
  ]
  edge [
    source 16
    target 1281
  ]
  edge [
    source 16
    target 1282
  ]
  edge [
    source 16
    target 1283
  ]
  edge [
    source 16
    target 1284
  ]
  edge [
    source 16
    target 1285
  ]
  edge [
    source 16
    target 1286
  ]
  edge [
    source 16
    target 1287
  ]
  edge [
    source 16
    target 1288
  ]
  edge [
    source 16
    target 1289
  ]
  edge [
    source 16
    target 1290
  ]
  edge [
    source 16
    target 1291
  ]
  edge [
    source 16
    target 1292
  ]
  edge [
    source 16
    target 1293
  ]
  edge [
    source 16
    target 1294
  ]
  edge [
    source 16
    target 1295
  ]
  edge [
    source 16
    target 1296
  ]
  edge [
    source 16
    target 1297
  ]
  edge [
    source 16
    target 1298
  ]
  edge [
    source 16
    target 1299
  ]
  edge [
    source 16
    target 1300
  ]
  edge [
    source 16
    target 1301
  ]
  edge [
    source 16
    target 1302
  ]
  edge [
    source 16
    target 1303
  ]
  edge [
    source 16
    target 1304
  ]
  edge [
    source 16
    target 1305
  ]
  edge [
    source 16
    target 1306
  ]
  edge [
    source 16
    target 1307
  ]
  edge [
    source 16
    target 1308
  ]
  edge [
    source 16
    target 1309
  ]
  edge [
    source 16
    target 1310
  ]
  edge [
    source 16
    target 1311
  ]
  edge [
    source 16
    target 1312
  ]
  edge [
    source 16
    target 1313
  ]
  edge [
    source 16
    target 1314
  ]
  edge [
    source 16
    target 1315
  ]
  edge [
    source 16
    target 1316
  ]
  edge [
    source 16
    target 1317
  ]
  edge [
    source 16
    target 1318
  ]
  edge [
    source 16
    target 1319
  ]
  edge [
    source 16
    target 1320
  ]
  edge [
    source 16
    target 1321
  ]
  edge [
    source 16
    target 1322
  ]
  edge [
    source 16
    target 1323
  ]
  edge [
    source 16
    target 1324
  ]
  edge [
    source 16
    target 1325
  ]
  edge [
    source 16
    target 1326
  ]
  edge [
    source 16
    target 1327
  ]
  edge [
    source 16
    target 1328
  ]
  edge [
    source 16
    target 1329
  ]
  edge [
    source 16
    target 1330
  ]
  edge [
    source 16
    target 1331
  ]
  edge [
    source 16
    target 1332
  ]
  edge [
    source 16
    target 1333
  ]
  edge [
    source 16
    target 1334
  ]
  edge [
    source 16
    target 1335
  ]
  edge [
    source 16
    target 1336
  ]
  edge [
    source 16
    target 1337
  ]
  edge [
    source 16
    target 1338
  ]
  edge [
    source 16
    target 1339
  ]
  edge [
    source 16
    target 1340
  ]
  edge [
    source 16
    target 1341
  ]
  edge [
    source 16
    target 1342
  ]
  edge [
    source 16
    target 1343
  ]
  edge [
    source 16
    target 1344
  ]
  edge [
    source 16
    target 1345
  ]
  edge [
    source 16
    target 1346
  ]
  edge [
    source 16
    target 1347
  ]
  edge [
    source 16
    target 1348
  ]
  edge [
    source 16
    target 1349
  ]
  edge [
    source 16
    target 1350
  ]
  edge [
    source 16
    target 1351
  ]
  edge [
    source 16
    target 1352
  ]
  edge [
    source 16
    target 1353
  ]
  edge [
    source 16
    target 1354
  ]
  edge [
    source 16
    target 1355
  ]
  edge [
    source 16
    target 1356
  ]
  edge [
    source 16
    target 1357
  ]
  edge [
    source 16
    target 1358
  ]
  edge [
    source 16
    target 1359
  ]
  edge [
    source 16
    target 1360
  ]
  edge [
    source 16
    target 1361
  ]
  edge [
    source 16
    target 1362
  ]
  edge [
    source 16
    target 1363
  ]
  edge [
    source 16
    target 1364
  ]
  edge [
    source 16
    target 1365
  ]
  edge [
    source 16
    target 1366
  ]
  edge [
    source 16
    target 1367
  ]
  edge [
    source 16
    target 1368
  ]
  edge [
    source 16
    target 1369
  ]
  edge [
    source 16
    target 1370
  ]
  edge [
    source 16
    target 1371
  ]
  edge [
    source 16
    target 1372
  ]
  edge [
    source 16
    target 1373
  ]
  edge [
    source 16
    target 1374
  ]
  edge [
    source 16
    target 1375
  ]
  edge [
    source 16
    target 1376
  ]
  edge [
    source 16
    target 1377
  ]
  edge [
    source 16
    target 1378
  ]
  edge [
    source 16
    target 1379
  ]
  edge [
    source 16
    target 1380
  ]
  edge [
    source 16
    target 1381
  ]
  edge [
    source 16
    target 1382
  ]
  edge [
    source 16
    target 1383
  ]
  edge [
    source 16
    target 1384
  ]
  edge [
    source 16
    target 1385
  ]
  edge [
    source 16
    target 1386
  ]
  edge [
    source 16
    target 1387
  ]
  edge [
    source 16
    target 1388
  ]
  edge [
    source 16
    target 1389
  ]
  edge [
    source 16
    target 1390
  ]
  edge [
    source 16
    target 1391
  ]
  edge [
    source 16
    target 1392
  ]
  edge [
    source 16
    target 1393
  ]
  edge [
    source 16
    target 1394
  ]
  edge [
    source 16
    target 1395
  ]
  edge [
    source 16
    target 1396
  ]
  edge [
    source 16
    target 1397
  ]
  edge [
    source 16
    target 1398
  ]
  edge [
    source 16
    target 1399
  ]
  edge [
    source 16
    target 1400
  ]
  edge [
    source 16
    target 1401
  ]
  edge [
    source 16
    target 1402
  ]
  edge [
    source 16
    target 1403
  ]
  edge [
    source 16
    target 1404
  ]
  edge [
    source 16
    target 1405
  ]
  edge [
    source 16
    target 1406
  ]
  edge [
    source 16
    target 1407
  ]
  edge [
    source 16
    target 1408
  ]
  edge [
    source 16
    target 1409
  ]
  edge [
    source 16
    target 1410
  ]
  edge [
    source 16
    target 1411
  ]
  edge [
    source 16
    target 1412
  ]
  edge [
    source 16
    target 1413
  ]
  edge [
    source 16
    target 1414
  ]
  edge [
    source 16
    target 1415
  ]
  edge [
    source 16
    target 1416
  ]
  edge [
    source 16
    target 1417
  ]
  edge [
    source 16
    target 1418
  ]
  edge [
    source 16
    target 1419
  ]
  edge [
    source 16
    target 1420
  ]
  edge [
    source 16
    target 1421
  ]
  edge [
    source 16
    target 1422
  ]
  edge [
    source 16
    target 1423
  ]
  edge [
    source 16
    target 1424
  ]
  edge [
    source 16
    target 1425
  ]
  edge [
    source 16
    target 1426
  ]
  edge [
    source 16
    target 1427
  ]
  edge [
    source 16
    target 1428
  ]
  edge [
    source 16
    target 1429
  ]
  edge [
    source 16
    target 1430
  ]
  edge [
    source 16
    target 1431
  ]
  edge [
    source 16
    target 1432
  ]
  edge [
    source 16
    target 1433
  ]
  edge [
    source 16
    target 1434
  ]
  edge [
    source 16
    target 1435
  ]
  edge [
    source 16
    target 1436
  ]
  edge [
    source 16
    target 1437
  ]
  edge [
    source 16
    target 1438
  ]
  edge [
    source 16
    target 1439
  ]
  edge [
    source 16
    target 1440
  ]
  edge [
    source 16
    target 1441
  ]
  edge [
    source 16
    target 1442
  ]
  edge [
    source 16
    target 1443
  ]
  edge [
    source 16
    target 1444
  ]
  edge [
    source 16
    target 1445
  ]
  edge [
    source 16
    target 1446
  ]
  edge [
    source 16
    target 1447
  ]
  edge [
    source 16
    target 1448
  ]
  edge [
    source 16
    target 1449
  ]
  edge [
    source 16
    target 1450
  ]
  edge [
    source 16
    target 1451
  ]
  edge [
    source 16
    target 1452
  ]
  edge [
    source 16
    target 1453
  ]
  edge [
    source 16
    target 1454
  ]
  edge [
    source 16
    target 1455
  ]
  edge [
    source 16
    target 1456
  ]
  edge [
    source 16
    target 1457
  ]
  edge [
    source 16
    target 1458
  ]
  edge [
    source 16
    target 1459
  ]
  edge [
    source 16
    target 1460
  ]
  edge [
    source 16
    target 1461
  ]
  edge [
    source 16
    target 1462
  ]
  edge [
    source 16
    target 1463
  ]
  edge [
    source 16
    target 1464
  ]
  edge [
    source 16
    target 1465
  ]
  edge [
    source 16
    target 1466
  ]
  edge [
    source 16
    target 1467
  ]
  edge [
    source 16
    target 1468
  ]
  edge [
    source 16
    target 1469
  ]
  edge [
    source 16
    target 1470
  ]
  edge [
    source 16
    target 1471
  ]
  edge [
    source 16
    target 1472
  ]
  edge [
    source 16
    target 1473
  ]
  edge [
    source 16
    target 1474
  ]
  edge [
    source 16
    target 1475
  ]
  edge [
    source 16
    target 1476
  ]
  edge [
    source 16
    target 1477
  ]
  edge [
    source 16
    target 1478
  ]
  edge [
    source 16
    target 1479
  ]
  edge [
    source 16
    target 1480
  ]
  edge [
    source 16
    target 1481
  ]
  edge [
    source 16
    target 1482
  ]
  edge [
    source 16
    target 1483
  ]
  edge [
    source 16
    target 1484
  ]
  edge [
    source 16
    target 1485
  ]
  edge [
    source 16
    target 1486
  ]
  edge [
    source 16
    target 1487
  ]
  edge [
    source 16
    target 1488
  ]
  edge [
    source 16
    target 1489
  ]
  edge [
    source 16
    target 1490
  ]
  edge [
    source 16
    target 1491
  ]
  edge [
    source 16
    target 1492
  ]
  edge [
    source 16
    target 1493
  ]
  edge [
    source 16
    target 1494
  ]
  edge [
    source 16
    target 1495
  ]
  edge [
    source 16
    target 1496
  ]
  edge [
    source 16
    target 1497
  ]
  edge [
    source 16
    target 1498
  ]
  edge [
    source 16
    target 1499
  ]
  edge [
    source 16
    target 1500
  ]
  edge [
    source 16
    target 1501
  ]
  edge [
    source 16
    target 1502
  ]
  edge [
    source 16
    target 1503
  ]
  edge [
    source 16
    target 1504
  ]
  edge [
    source 16
    target 1505
  ]
  edge [
    source 16
    target 1506
  ]
  edge [
    source 16
    target 1507
  ]
  edge [
    source 16
    target 1508
  ]
  edge [
    source 16
    target 1509
  ]
  edge [
    source 16
    target 1510
  ]
  edge [
    source 16
    target 1511
  ]
  edge [
    source 16
    target 1512
  ]
  edge [
    source 16
    target 1513
  ]
  edge [
    source 16
    target 1514
  ]
  edge [
    source 16
    target 1515
  ]
  edge [
    source 16
    target 1516
  ]
  edge [
    source 16
    target 1517
  ]
  edge [
    source 16
    target 1518
  ]
  edge [
    source 16
    target 1519
  ]
  edge [
    source 16
    target 1520
  ]
  edge [
    source 16
    target 1521
  ]
  edge [
    source 16
    target 1522
  ]
  edge [
    source 16
    target 1523
  ]
  edge [
    source 16
    target 1524
  ]
  edge [
    source 16
    target 1525
  ]
  edge [
    source 16
    target 1526
  ]
  edge [
    source 16
    target 1527
  ]
  edge [
    source 16
    target 1528
  ]
  edge [
    source 16
    target 1529
  ]
  edge [
    source 16
    target 1530
  ]
  edge [
    source 16
    target 1531
  ]
  edge [
    source 16
    target 1532
  ]
  edge [
    source 16
    target 1533
  ]
  edge [
    source 16
    target 1534
  ]
  edge [
    source 16
    target 618
  ]
  edge [
    source 16
    target 1535
  ]
  edge [
    source 16
    target 1536
  ]
  edge [
    source 16
    target 1537
  ]
  edge [
    source 16
    target 1538
  ]
  edge [
    source 16
    target 1539
  ]
  edge [
    source 16
    target 1540
  ]
  edge [
    source 16
    target 1541
  ]
  edge [
    source 16
    target 1542
  ]
  edge [
    source 16
    target 1543
  ]
  edge [
    source 16
    target 1544
  ]
  edge [
    source 16
    target 1545
  ]
  edge [
    source 16
    target 1546
  ]
  edge [
    source 16
    target 1547
  ]
  edge [
    source 16
    target 1548
  ]
  edge [
    source 16
    target 1549
  ]
  edge [
    source 16
    target 1550
  ]
  edge [
    source 16
    target 1551
  ]
  edge [
    source 16
    target 1552
  ]
  edge [
    source 16
    target 1553
  ]
  edge [
    source 16
    target 1554
  ]
  edge [
    source 16
    target 1555
  ]
  edge [
    source 16
    target 1556
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 1557
  ]
  edge [
    source 18
    target 1558
  ]
  edge [
    source 18
    target 1559
  ]
  edge [
    source 18
    target 1560
  ]
  edge [
    source 18
    target 1169
  ]
  edge [
    source 18
    target 1561
  ]
  edge [
    source 18
    target 1562
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1563
  ]
  edge [
    source 18
    target 1564
  ]
  edge [
    source 18
    target 1565
  ]
  edge [
    source 18
    target 1566
  ]
  edge [
    source 18
    target 1567
  ]
  edge [
    source 18
    target 1568
  ]
  edge [
    source 18
    target 1569
  ]
  edge [
    source 18
    target 1570
  ]
  edge [
    source 18
    target 1571
  ]
  edge [
    source 18
    target 1572
  ]
  edge [
    source 18
    target 1573
  ]
  edge [
    source 18
    target 1574
  ]
  edge [
    source 18
    target 1575
  ]
  edge [
    source 18
    target 1576
  ]
  edge [
    source 18
    target 1577
  ]
  edge [
    source 18
    target 1578
  ]
  edge [
    source 18
    target 1579
  ]
  edge [
    source 18
    target 48
  ]
  edge [
    source 18
    target 1580
  ]
  edge [
    source 18
    target 352
  ]
  edge [
    source 18
    target 1581
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 1582
  ]
  edge [
    source 18
    target 1583
  ]
  edge [
    source 18
    target 1584
  ]
  edge [
    source 18
    target 1585
  ]
  edge [
    source 18
    target 1586
  ]
  edge [
    source 18
    target 1587
  ]
  edge [
    source 18
    target 1588
  ]
  edge [
    source 18
    target 1589
  ]
  edge [
    source 18
    target 1590
  ]
  edge [
    source 18
    target 1591
  ]
  edge [
    source 18
    target 1592
  ]
  edge [
    source 18
    target 1593
  ]
  edge [
    source 18
    target 1594
  ]
  edge [
    source 18
    target 1595
  ]
  edge [
    source 18
    target 1596
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 18
    target 1597
  ]
  edge [
    source 18
    target 1598
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 18
    target 1599
  ]
  edge [
    source 18
    target 1600
  ]
  edge [
    source 18
    target 63
  ]
  edge [
    source 18
    target 1601
  ]
  edge [
    source 18
    target 1602
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1603
  ]
  edge [
    source 18
    target 1604
  ]
  edge [
    source 18
    target 1605
  ]
  edge [
    source 18
    target 1606
  ]
  edge [
    source 18
    target 1607
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 1608
  ]
  edge [
    source 18
    target 1609
  ]
  edge [
    source 18
    target 1610
  ]
  edge [
    source 18
    target 1611
  ]
  edge [
    source 18
    target 1612
  ]
  edge [
    source 18
    target 1613
  ]
  edge [
    source 18
    target 1614
  ]
  edge [
    source 18
    target 320
  ]
  edge [
    source 18
    target 1615
  ]
  edge [
    source 18
    target 1616
  ]
  edge [
    source 18
    target 1617
  ]
  edge [
    source 18
    target 1618
  ]
  edge [
    source 18
    target 1619
  ]
  edge [
    source 18
    target 1620
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1621
  ]
  edge [
    source 18
    target 1622
  ]
  edge [
    source 18
    target 1623
  ]
  edge [
    source 18
    target 78
  ]
  edge [
    source 18
    target 1624
  ]
  edge [
    source 18
    target 1625
  ]
  edge [
    source 18
    target 1626
  ]
  edge [
    source 18
    target 1627
  ]
  edge [
    source 18
    target 1628
  ]
  edge [
    source 18
    target 1629
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 448
  ]
  edge [
    source 19
    target 449
  ]
  edge [
    source 19
    target 450
  ]
  edge [
    source 19
    target 451
  ]
  edge [
    source 19
    target 452
  ]
  edge [
    source 19
    target 430
  ]
  edge [
    source 19
    target 1630
  ]
  edge [
    source 19
    target 1631
  ]
  edge [
    source 19
    target 1632
  ]
  edge [
    source 19
    target 1633
  ]
  edge [
    source 19
    target 1634
  ]
  edge [
    source 19
    target 1635
  ]
  edge [
    source 19
    target 1636
  ]
  edge [
    source 19
    target 1637
  ]
  edge [
    source 19
    target 1638
  ]
  edge [
    source 19
    target 1639
  ]
  edge [
    source 19
    target 1640
  ]
  edge [
    source 19
    target 1641
  ]
  edge [
    source 19
    target 1642
  ]
  edge [
    source 19
    target 1643
  ]
  edge [
    source 19
    target 1644
  ]
  edge [
    source 19
    target 1645
  ]
  edge [
    source 19
    target 1646
  ]
  edge [
    source 19
    target 1647
  ]
  edge [
    source 19
    target 1648
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1649
  ]
  edge [
    source 20
    target 1650
  ]
  edge [
    source 20
    target 1651
  ]
  edge [
    source 20
    target 1652
  ]
  edge [
    source 20
    target 1653
  ]
  edge [
    source 20
    target 1654
  ]
  edge [
    source 20
    target 1655
  ]
  edge [
    source 20
    target 290
  ]
  edge [
    source 20
    target 1646
  ]
  edge [
    source 20
    target 1656
  ]
  edge [
    source 20
    target 1657
  ]
  edge [
    source 20
    target 1658
  ]
  edge [
    source 20
    target 1659
  ]
  edge [
    source 20
    target 1660
  ]
  edge [
    source 20
    target 1661
  ]
  edge [
    source 20
    target 1662
  ]
  edge [
    source 20
    target 1663
  ]
  edge [
    source 20
    target 1664
  ]
  edge [
    source 20
    target 1665
  ]
  edge [
    source 20
    target 1666
  ]
  edge [
    source 20
    target 1667
  ]
  edge [
    source 20
    target 1668
  ]
  edge [
    source 20
    target 1669
  ]
  edge [
    source 20
    target 1670
  ]
  edge [
    source 20
    target 1671
  ]
  edge [
    source 20
    target 1672
  ]
  edge [
    source 20
    target 1673
  ]
  edge [
    source 20
    target 1674
  ]
  edge [
    source 20
    target 1675
  ]
  edge [
    source 20
    target 1676
  ]
  edge [
    source 20
    target 452
  ]
  edge [
    source 20
    target 1677
  ]
  edge [
    source 20
    target 1678
  ]
  edge [
    source 20
    target 1679
  ]
  edge [
    source 20
    target 1680
  ]
  edge [
    source 20
    target 1681
  ]
  edge [
    source 20
    target 1682
  ]
  edge [
    source 20
    target 1683
  ]
  edge [
    source 20
    target 1684
  ]
  edge [
    source 20
    target 1685
  ]
  edge [
    source 20
    target 1686
  ]
  edge [
    source 20
    target 1634
  ]
  edge [
    source 20
    target 1687
  ]
  edge [
    source 20
    target 1688
  ]
  edge [
    source 20
    target 1689
  ]
  edge [
    source 20
    target 1690
  ]
  edge [
    source 20
    target 1691
  ]
  edge [
    source 20
    target 1692
  ]
  edge [
    source 20
    target 1693
  ]
  edge [
    source 20
    target 1694
  ]
  edge [
    source 20
    target 1695
  ]
  edge [
    source 20
    target 271
  ]
  edge [
    source 20
    target 1696
  ]
  edge [
    source 20
    target 1697
  ]
  edge [
    source 20
    target 1698
  ]
  edge [
    source 20
    target 1699
  ]
  edge [
    source 21
    target 1700
  ]
  edge [
    source 21
    target 1701
  ]
  edge [
    source 21
    target 1702
  ]
  edge [
    source 21
    target 1703
  ]
  edge [
    source 21
    target 1704
  ]
  edge [
    source 21
    target 1705
  ]
  edge [
    source 21
    target 1706
  ]
  edge [
    source 21
    target 1707
  ]
  edge [
    source 21
    target 1708
  ]
  edge [
    source 21
    target 103
  ]
  edge [
    source 21
    target 1709
  ]
  edge [
    source 21
    target 1710
  ]
  edge [
    source 21
    target 1711
  ]
  edge [
    source 21
    target 1712
  ]
  edge [
    source 21
    target 1713
  ]
  edge [
    source 21
    target 1714
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 1715
  ]
  edge [
    source 22
    target 1716
  ]
  edge [
    source 22
    target 1717
  ]
  edge [
    source 22
    target 1718
  ]
  edge [
    source 22
    target 1719
  ]
  edge [
    source 22
    target 1720
  ]
  edge [
    source 22
    target 1721
  ]
  edge [
    source 22
    target 1722
  ]
  edge [
    source 22
    target 1723
  ]
  edge [
    source 22
    target 1724
  ]
  edge [
    source 22
    target 1725
  ]
  edge [
    source 22
    target 1726
  ]
  edge [
    source 22
    target 1727
  ]
  edge [
    source 22
    target 1728
  ]
  edge [
    source 22
    target 1729
  ]
  edge [
    source 22
    target 1730
  ]
  edge [
    source 22
    target 1731
  ]
  edge [
    source 22
    target 1732
  ]
  edge [
    source 22
    target 1733
  ]
  edge [
    source 22
    target 1734
  ]
  edge [
    source 22
    target 1735
  ]
  edge [
    source 22
    target 1736
  ]
  edge [
    source 22
    target 623
  ]
  edge [
    source 22
    target 1737
  ]
  edge [
    source 22
    target 1738
  ]
  edge [
    source 22
    target 1739
  ]
  edge [
    source 22
    target 1740
  ]
  edge [
    source 22
    target 1741
  ]
  edge [
    source 22
    target 1742
  ]
  edge [
    source 22
    target 244
  ]
  edge [
    source 22
    target 1743
  ]
  edge [
    source 22
    target 1744
  ]
  edge [
    source 22
    target 1745
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1746
  ]
  edge [
    source 23
    target 1747
  ]
  edge [
    source 23
    target 1748
  ]
  edge [
    source 23
    target 406
  ]
  edge [
    source 23
    target 1749
  ]
  edge [
    source 23
    target 1750
  ]
  edge [
    source 23
    target 1751
  ]
  edge [
    source 23
    target 1752
  ]
  edge [
    source 23
    target 1753
  ]
  edge [
    source 23
    target 1754
  ]
  edge [
    source 23
    target 1755
  ]
  edge [
    source 23
    target 1756
  ]
  edge [
    source 23
    target 327
  ]
  edge [
    source 23
    target 547
  ]
  edge [
    source 23
    target 1757
  ]
  edge [
    source 23
    target 1758
  ]
  edge [
    source 23
    target 1759
  ]
  edge [
    source 23
    target 1760
  ]
  edge [
    source 23
    target 1761
  ]
  edge [
    source 23
    target 1762
  ]
  edge [
    source 23
    target 1763
  ]
  edge [
    source 23
    target 468
  ]
  edge [
    source 23
    target 332
  ]
  edge [
    source 23
    target 1764
  ]
  edge [
    source 23
    target 1765
  ]
  edge [
    source 23
    target 1766
  ]
  edge [
    source 23
    target 1767
  ]
  edge [
    source 23
    target 1768
  ]
  edge [
    source 23
    target 1769
  ]
  edge [
    source 23
    target 1770
  ]
  edge [
    source 23
    target 1771
  ]
  edge [
    source 23
    target 1772
  ]
  edge [
    source 23
    target 1773
  ]
  edge [
    source 23
    target 1774
  ]
  edge [
    source 23
    target 1775
  ]
  edge [
    source 23
    target 1776
  ]
  edge [
    source 23
    target 1777
  ]
  edge [
    source 23
    target 1778
  ]
  edge [
    source 23
    target 1779
  ]
  edge [
    source 23
    target 1780
  ]
  edge [
    source 23
    target 1781
  ]
  edge [
    source 23
    target 1782
  ]
  edge [
    source 23
    target 1783
  ]
  edge [
    source 23
    target 1784
  ]
  edge [
    source 23
    target 1785
  ]
  edge [
    source 23
    target 1786
  ]
  edge [
    source 23
    target 1787
  ]
  edge [
    source 23
    target 1788
  ]
  edge [
    source 23
    target 1789
  ]
  edge [
    source 23
    target 1790
  ]
  edge [
    source 23
    target 1791
  ]
  edge [
    source 23
    target 1792
  ]
  edge [
    source 23
    target 1793
  ]
  edge [
    source 23
    target 1794
  ]
  edge [
    source 23
    target 585
  ]
  edge [
    source 23
    target 1795
  ]
  edge [
    source 23
    target 1796
  ]
  edge [
    source 23
    target 1797
  ]
  edge [
    source 23
    target 1798
  ]
  edge [
    source 23
    target 1799
  ]
  edge [
    source 23
    target 1800
  ]
  edge [
    source 23
    target 1801
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1802
  ]
  edge [
    source 24
    target 363
  ]
  edge [
    source 24
    target 1803
  ]
  edge [
    source 24
    target 1804
  ]
  edge [
    source 24
    target 1805
  ]
  edge [
    source 24
    target 1806
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1807
  ]
  edge [
    source 25
    target 1808
  ]
  edge [
    source 25
    target 1809
  ]
  edge [
    source 25
    target 1810
  ]
  edge [
    source 25
    target 1811
  ]
  edge [
    source 25
    target 1812
  ]
  edge [
    source 25
    target 1715
  ]
  edge [
    source 25
    target 1716
  ]
  edge [
    source 25
    target 363
  ]
  edge [
    source 25
    target 1717
  ]
  edge [
    source 25
    target 1813
  ]
  edge [
    source 25
    target 1718
  ]
  edge [
    source 25
    target 1719
  ]
  edge [
    source 25
    target 1814
  ]
  edge [
    source 25
    target 1720
  ]
  edge [
    source 25
    target 1721
  ]
  edge [
    source 25
    target 1723
  ]
  edge [
    source 25
    target 1815
  ]
  edge [
    source 25
    target 1652
  ]
  edge [
    source 25
    target 1816
  ]
  edge [
    source 25
    target 471
  ]
  edge [
    source 25
    target 1817
  ]
  edge [
    source 25
    target 1818
  ]
  edge [
    source 25
    target 1819
  ]
  edge [
    source 25
    target 1820
  ]
  edge [
    source 25
    target 1722
  ]
  edge [
    source 25
    target 1821
  ]
  edge [
    source 25
    target 1822
  ]
  edge [
    source 25
    target 1823
  ]
  edge [
    source 25
    target 1824
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 26
    target 1825
  ]
  edge [
    source 26
    target 1826
  ]
  edge [
    source 26
    target 1827
  ]
  edge [
    source 26
    target 1828
  ]
  edge [
    source 26
    target 1829
  ]
  edge [
    source 26
    target 1830
  ]
  edge [
    source 26
    target 1831
  ]
  edge [
    source 26
    target 417
  ]
  edge [
    source 26
    target 1832
  ]
  edge [
    source 26
    target 1833
  ]
  edge [
    source 26
    target 1834
  ]
  edge [
    source 26
    target 1835
  ]
  edge [
    source 26
    target 1836
  ]
  edge [
    source 26
    target 1837
  ]
  edge [
    source 26
    target 1838
  ]
  edge [
    source 26
    target 1839
  ]
  edge [
    source 26
    target 1840
  ]
  edge [
    source 26
    target 1841
  ]
  edge [
    source 26
    target 1842
  ]
  edge [
    source 26
    target 422
  ]
  edge [
    source 26
    target 453
  ]
  edge [
    source 26
    target 454
  ]
  edge [
    source 26
    target 455
  ]
  edge [
    source 26
    target 356
  ]
  edge [
    source 26
    target 456
  ]
  edge [
    source 26
    target 457
  ]
  edge [
    source 26
    target 458
  ]
  edge [
    source 26
    target 459
  ]
  edge [
    source 26
    target 460
  ]
  edge [
    source 26
    target 462
  ]
  edge [
    source 26
    target 461
  ]
  edge [
    source 26
    target 463
  ]
  edge [
    source 26
    target 464
  ]
  edge [
    source 26
    target 465
  ]
  edge [
    source 26
    target 466
  ]
  edge [
    source 26
    target 467
  ]
  edge [
    source 26
    target 468
  ]
  edge [
    source 26
    target 469
  ]
  edge [
    source 26
    target 470
  ]
  edge [
    source 26
    target 471
  ]
  edge [
    source 26
    target 472
  ]
  edge [
    source 26
    target 473
  ]
  edge [
    source 26
    target 474
  ]
  edge [
    source 26
    target 475
  ]
  edge [
    source 26
    target 476
  ]
  edge [
    source 26
    target 477
  ]
  edge [
    source 26
    target 478
  ]
  edge [
    source 26
    target 479
  ]
  edge [
    source 26
    target 480
  ]
  edge [
    source 26
    target 481
  ]
  edge [
    source 26
    target 482
  ]
  edge [
    source 26
    target 483
  ]
  edge [
    source 26
    target 484
  ]
  edge [
    source 26
    target 485
  ]
  edge [
    source 26
    target 486
  ]
  edge [
    source 26
    target 487
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 488
  ]
  edge [
    source 26
    target 489
  ]
  edge [
    source 26
    target 80
  ]
  edge [
    source 26
    target 490
  ]
  edge [
    source 26
    target 491
  ]
  edge [
    source 26
    target 492
  ]
  edge [
    source 26
    target 493
  ]
  edge [
    source 26
    target 1843
  ]
  edge [
    source 26
    target 1844
  ]
  edge [
    source 26
    target 1845
  ]
  edge [
    source 26
    target 1846
  ]
  edge [
    source 26
    target 1847
  ]
  edge [
    source 26
    target 1848
  ]
  edge [
    source 26
    target 443
  ]
  edge [
    source 26
    target 444
  ]
  edge [
    source 26
    target 1849
  ]
  edge [
    source 26
    target 1850
  ]
  edge [
    source 26
    target 1851
  ]
  edge [
    source 26
    target 1852
  ]
  edge [
    source 26
    target 1853
  ]
  edge [
    source 26
    target 1854
  ]
  edge [
    source 26
    target 1855
  ]
  edge [
    source 26
    target 1856
  ]
  edge [
    source 26
    target 1857
  ]
  edge [
    source 26
    target 1858
  ]
  edge [
    source 26
    target 357
  ]
  edge [
    source 26
    target 1859
  ]
  edge [
    source 26
    target 1860
  ]
  edge [
    source 26
    target 1861
  ]
  edge [
    source 26
    target 1862
  ]
  edge [
    source 26
    target 1863
  ]
  edge [
    source 26
    target 1864
  ]
  edge [
    source 26
    target 1865
  ]
  edge [
    source 26
    target 1866
  ]
  edge [
    source 26
    target 1867
  ]
  edge [
    source 26
    target 1868
  ]
  edge [
    source 26
    target 1869
  ]
  edge [
    source 26
    target 1870
  ]
  edge [
    source 26
    target 1871
  ]
  edge [
    source 26
    target 1872
  ]
  edge [
    source 26
    target 1873
  ]
  edge [
    source 26
    target 439
  ]
  edge [
    source 26
    target 440
  ]
  edge [
    source 26
    target 438
  ]
  edge [
    source 26
    target 1874
  ]
  edge [
    source 26
    target 1875
  ]
  edge [
    source 26
    target 1876
  ]
  edge [
    source 26
    target 1877
  ]
  edge [
    source 26
    target 1878
  ]
  edge [
    source 26
    target 1879
  ]
  edge [
    source 26
    target 1880
  ]
  edge [
    source 26
    target 1881
  ]
  edge [
    source 26
    target 1882
  ]
  edge [
    source 26
    target 1883
  ]
  edge [
    source 26
    target 1884
  ]
  edge [
    source 26
    target 430
  ]
  edge [
    source 26
    target 1885
  ]
  edge [
    source 26
    target 1886
  ]
  edge [
    source 26
    target 1887
  ]
  edge [
    source 26
    target 1888
  ]
  edge [
    source 26
    target 1889
  ]
  edge [
    source 26
    target 1890
  ]
  edge [
    source 26
    target 1891
  ]
  edge [
    source 26
    target 1892
  ]
  edge [
    source 26
    target 1893
  ]
  edge [
    source 26
    target 1894
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1895
  ]
  edge [
    source 27
    target 1896
  ]
  edge [
    source 27
    target 1897
  ]
  edge [
    source 27
    target 1898
  ]
  edge [
    source 27
    target 1899
  ]
  edge [
    source 27
    target 1900
  ]
  edge [
    source 27
    target 1901
  ]
  edge [
    source 27
    target 1902
  ]
  edge [
    source 27
    target 171
  ]
  edge [
    source 27
    target 172
  ]
  edge [
    source 27
    target 1903
  ]
  edge [
    source 27
    target 1904
  ]
  edge [
    source 27
    target 1905
  ]
  edge [
    source 27
    target 1906
  ]
  edge [
    source 27
    target 1907
  ]
  edge [
    source 27
    target 1908
  ]
  edge [
    source 27
    target 475
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 27
    target 1909
  ]
  edge [
    source 27
    target 1910
  ]
  edge [
    source 27
    target 1911
  ]
  edge [
    source 27
    target 1912
  ]
  edge [
    source 27
    target 1913
  ]
  edge [
    source 27
    target 1914
  ]
  edge [
    source 27
    target 1915
  ]
  edge [
    source 27
    target 1916
  ]
  edge [
    source 27
    target 1917
  ]
  edge [
    source 27
    target 1918
  ]
  edge [
    source 27
    target 1919
  ]
  edge [
    source 27
    target 1920
  ]
  edge [
    source 27
    target 1921
  ]
  edge [
    source 27
    target 1922
  ]
  edge [
    source 27
    target 1923
  ]
  edge [
    source 27
    target 1924
  ]
  edge [
    source 27
    target 1925
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 1926
  ]
  edge [
    source 27
    target 1927
  ]
  edge [
    source 27
    target 1013
  ]
  edge [
    source 27
    target 1928
  ]
  edge [
    source 27
    target 1929
  ]
  edge [
    source 27
    target 1930
  ]
  edge [
    source 27
    target 1931
  ]
  edge [
    source 27
    target 1932
  ]
  edge [
    source 27
    target 1933
  ]
  edge [
    source 27
    target 327
  ]
  edge [
    source 27
    target 1934
  ]
  edge [
    source 27
    target 592
  ]
  edge [
    source 27
    target 1935
  ]
  edge [
    source 27
    target 1936
  ]
  edge [
    source 27
    target 1937
  ]
  edge [
    source 27
    target 1938
  ]
  edge [
    source 27
    target 1939
  ]
  edge [
    source 27
    target 1940
  ]
  edge [
    source 27
    target 1941
  ]
  edge [
    source 27
    target 1942
  ]
  edge [
    source 27
    target 1943
  ]
  edge [
    source 27
    target 1944
  ]
  edge [
    source 27
    target 1945
  ]
  edge [
    source 27
    target 1604
  ]
  edge [
    source 27
    target 1599
  ]
  edge [
    source 27
    target 1594
  ]
  edge [
    source 27
    target 1611
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 1605
  ]
  edge [
    source 27
    target 1597
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 320
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 93
  ]
  edge [
    source 27
    target 1946
  ]
  edge [
    source 27
    target 59
  ]
  edge [
    source 27
    target 1603
  ]
  edge [
    source 27
    target 1947
  ]
  edge [
    source 27
    target 1948
  ]
  edge [
    source 27
    target 1949
  ]
  edge [
    source 27
    target 1950
  ]
  edge [
    source 27
    target 1951
  ]
  edge [
    source 27
    target 1952
  ]
  edge [
    source 27
    target 1953
  ]
  edge [
    source 27
    target 980
  ]
  edge [
    source 27
    target 1954
  ]
  edge [
    source 27
    target 1955
  ]
  edge [
    source 27
    target 1956
  ]
  edge [
    source 27
    target 1957
  ]
  edge [
    source 27
    target 1958
  ]
  edge [
    source 27
    target 1959
  ]
  edge [
    source 27
    target 1960
  ]
  edge [
    source 27
    target 1961
  ]
  edge [
    source 27
    target 1962
  ]
  edge [
    source 27
    target 1963
  ]
  edge [
    source 27
    target 96
  ]
  edge [
    source 27
    target 1964
  ]
  edge [
    source 27
    target 1965
  ]
  edge [
    source 27
    target 1966
  ]
  edge [
    source 27
    target 1967
  ]
  edge [
    source 27
    target 486
  ]
  edge [
    source 27
    target 1968
  ]
  edge [
    source 27
    target 1969
  ]
  edge [
    source 27
    target 1970
  ]
  edge [
    source 27
    target 88
  ]
  edge [
    source 27
    target 1971
  ]
  edge [
    source 27
    target 1972
  ]
  edge [
    source 27
    target 1973
  ]
  edge [
    source 27
    target 1974
  ]
  edge [
    source 27
    target 1975
  ]
  edge [
    source 27
    target 1976
  ]
  edge [
    source 27
    target 1977
  ]
  edge [
    source 27
    target 1978
  ]
  edge [
    source 27
    target 1979
  ]
  edge [
    source 27
    target 1980
  ]
  edge [
    source 27
    target 1981
  ]
  edge [
    source 27
    target 1982
  ]
  edge [
    source 27
    target 1983
  ]
  edge [
    source 27
    target 1984
  ]
  edge [
    source 27
    target 1985
  ]
  edge [
    source 27
    target 1986
  ]
  edge [
    source 27
    target 1987
  ]
  edge [
    source 27
    target 1988
  ]
  edge [
    source 27
    target 48
  ]
  edge [
    source 27
    target 1989
  ]
  edge [
    source 27
    target 1990
  ]
  edge [
    source 27
    target 1991
  ]
  edge [
    source 27
    target 1992
  ]
  edge [
    source 27
    target 1993
  ]
  edge [
    source 27
    target 1994
  ]
  edge [
    source 27
    target 1995
  ]
  edge [
    source 27
    target 1996
  ]
  edge [
    source 27
    target 1997
  ]
  edge [
    source 27
    target 1998
  ]
  edge [
    source 27
    target 1999
  ]
  edge [
    source 27
    target 2000
  ]
  edge [
    source 27
    target 2001
  ]
  edge [
    source 27
    target 2002
  ]
  edge [
    source 27
    target 585
  ]
  edge [
    source 27
    target 2003
  ]
  edge [
    source 27
    target 2004
  ]
  edge [
    source 27
    target 1728
  ]
  edge [
    source 27
    target 2005
  ]
  edge [
    source 27
    target 1588
  ]
  edge [
    source 27
    target 2006
  ]
  edge [
    source 27
    target 2007
  ]
  edge [
    source 27
    target 2008
  ]
  edge [
    source 27
    target 2009
  ]
  edge [
    source 27
    target 2010
  ]
  edge [
    source 27
    target 2011
  ]
  edge [
    source 27
    target 2012
  ]
  edge [
    source 27
    target 2013
  ]
  edge [
    source 27
    target 2014
  ]
  edge [
    source 27
    target 2015
  ]
  edge [
    source 27
    target 2016
  ]
  edge [
    source 27
    target 2017
  ]
  edge [
    source 27
    target 2018
  ]
  edge [
    source 27
    target 2019
  ]
  edge [
    source 27
    target 2020
  ]
  edge [
    source 27
    target 2021
  ]
  edge [
    source 27
    target 2022
  ]
  edge [
    source 27
    target 2023
  ]
  edge [
    source 27
    target 2024
  ]
  edge [
    source 27
    target 335
  ]
  edge [
    source 27
    target 2025
  ]
  edge [
    source 27
    target 2026
  ]
  edge [
    source 27
    target 2027
  ]
  edge [
    source 27
    target 2028
  ]
  edge [
    source 27
    target 2029
  ]
  edge [
    source 27
    target 2030
  ]
  edge [
    source 27
    target 2031
  ]
  edge [
    source 27
    target 2032
  ]
  edge [
    source 27
    target 2033
  ]
  edge [
    source 27
    target 2034
  ]
  edge [
    source 27
    target 2035
  ]
  edge [
    source 27
    target 2036
  ]
  edge [
    source 27
    target 2037
  ]
  edge [
    source 27
    target 2038
  ]
  edge [
    source 27
    target 2039
  ]
  edge [
    source 27
    target 2040
  ]
  edge [
    source 27
    target 2041
  ]
  edge [
    source 27
    target 2042
  ]
  edge [
    source 27
    target 2043
  ]
  edge [
    source 27
    target 2044
  ]
  edge [
    source 27
    target 2045
  ]
  edge [
    source 27
    target 2046
  ]
  edge [
    source 27
    target 2047
  ]
  edge [
    source 27
    target 2048
  ]
  edge [
    source 27
    target 2049
  ]
  edge [
    source 27
    target 2050
  ]
  edge [
    source 27
    target 2051
  ]
  edge [
    source 27
    target 2052
  ]
  edge [
    source 27
    target 2053
  ]
  edge [
    source 27
    target 2054
  ]
  edge [
    source 27
    target 2055
  ]
  edge [
    source 27
    target 2056
  ]
  edge [
    source 27
    target 1782
  ]
  edge [
    source 27
    target 2057
  ]
  edge [
    source 27
    target 2058
  ]
  edge [
    source 27
    target 2059
  ]
  edge [
    source 27
    target 2060
  ]
  edge [
    source 27
    target 2061
  ]
  edge [
    source 27
    target 2062
  ]
  edge [
    source 27
    target 2063
  ]
  edge [
    source 27
    target 2064
  ]
  edge [
    source 27
    target 363
  ]
  edge [
    source 27
    target 2065
  ]
  edge [
    source 27
    target 683
  ]
  edge [
    source 27
    target 2066
  ]
  edge [
    source 27
    target 84
  ]
  edge [
    source 27
    target 2067
  ]
  edge [
    source 27
    target 2068
  ]
  edge [
    source 27
    target 2069
  ]
  edge [
    source 27
    target 149
  ]
  edge [
    source 27
    target 2070
  ]
  edge [
    source 27
    target 2071
  ]
  edge [
    source 27
    target 2072
  ]
  edge [
    source 27
    target 2073
  ]
  edge [
    source 27
    target 2074
  ]
  edge [
    source 27
    target 2075
  ]
  edge [
    source 27
    target 2076
  ]
  edge [
    source 27
    target 2077
  ]
  edge [
    source 27
    target 2078
  ]
  edge [
    source 27
    target 2079
  ]
  edge [
    source 27
    target 2080
  ]
  edge [
    source 27
    target 2081
  ]
  edge [
    source 27
    target 1598
  ]
  edge [
    source 27
    target 2082
  ]
  edge [
    source 27
    target 2083
  ]
  edge [
    source 27
    target 2084
  ]
  edge [
    source 27
    target 2085
  ]
  edge [
    source 27
    target 319
  ]
  edge [
    source 27
    target 2086
  ]
  edge [
    source 27
    target 2087
  ]
  edge [
    source 27
    target 2088
  ]
  edge [
    source 27
    target 2089
  ]
  edge [
    source 27
    target 2090
  ]
  edge [
    source 27
    target 2091
  ]
  edge [
    source 27
    target 2092
  ]
  edge [
    source 27
    target 556
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 2093
  ]
  edge [
    source 28
    target 2094
  ]
  edge [
    source 28
    target 2095
  ]
  edge [
    source 28
    target 2096
  ]
  edge [
    source 28
    target 2097
  ]
  edge [
    source 28
    target 2098
  ]
  edge [
    source 28
    target 2099
  ]
  edge [
    source 28
    target 2100
  ]
  edge [
    source 28
    target 546
  ]
  edge [
    source 28
    target 2101
  ]
  edge [
    source 28
    target 2102
  ]
  edge [
    source 28
    target 2103
  ]
  edge [
    source 29
    target 2104
  ]
  edge [
    source 29
    target 2105
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 2106
  ]
  edge [
    source 30
    target 2107
  ]
  edge [
    source 30
    target 2108
  ]
  edge [
    source 30
    target 2109
  ]
  edge [
    source 30
    target 2110
  ]
  edge [
    source 30
    target 2111
  ]
  edge [
    source 30
    target 2112
  ]
  edge [
    source 30
    target 2113
  ]
  edge [
    source 30
    target 592
  ]
  edge [
    source 30
    target 2114
  ]
  edge [
    source 30
    target 2115
  ]
  edge [
    source 30
    target 2116
  ]
  edge [
    source 30
    target 2117
  ]
  edge [
    source 30
    target 1853
  ]
  edge [
    source 30
    target 2118
  ]
  edge [
    source 30
    target 2119
  ]
  edge [
    source 30
    target 2120
  ]
  edge [
    source 30
    target 2121
  ]
  edge [
    source 30
    target 2122
  ]
  edge [
    source 30
    target 2123
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 2124
  ]
  edge [
    source 31
    target 2125
  ]
  edge [
    source 31
    target 2126
  ]
  edge [
    source 31
    target 2127
  ]
  edge [
    source 31
    target 2128
  ]
  edge [
    source 31
    target 2129
  ]
  edge [
    source 31
    target 2130
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 2131
  ]
  edge [
    source 32
    target 2132
  ]
  edge [
    source 32
    target 2133
  ]
  edge [
    source 32
    target 2134
  ]
  edge [
    source 32
    target 2135
  ]
  edge [
    source 32
    target 2136
  ]
  edge [
    source 32
    target 2137
  ]
  edge [
    source 32
    target 2138
  ]
  edge [
    source 32
    target 2139
  ]
  edge [
    source 32
    target 2140
  ]
  edge [
    source 32
    target 2141
  ]
  edge [
    source 32
    target 2142
  ]
  edge [
    source 32
    target 2143
  ]
  edge [
    source 32
    target 2144
  ]
  edge [
    source 32
    target 2145
  ]
  edge [
    source 32
    target 2146
  ]
  edge [
    source 32
    target 2147
  ]
  edge [
    source 32
    target 2148
  ]
  edge [
    source 32
    target 2149
  ]
  edge [
    source 32
    target 2150
  ]
  edge [
    source 32
    target 2151
  ]
  edge [
    source 32
    target 2152
  ]
  edge [
    source 32
    target 2153
  ]
  edge [
    source 32
    target 2154
  ]
  edge [
    source 32
    target 2155
  ]
  edge [
    source 32
    target 2156
  ]
  edge [
    source 32
    target 2157
  ]
  edge [
    source 32
    target 2158
  ]
  edge [
    source 32
    target 2159
  ]
  edge [
    source 32
    target 2160
  ]
  edge [
    source 32
    target 2161
  ]
  edge [
    source 32
    target 2162
  ]
  edge [
    source 32
    target 93
  ]
  edge [
    source 32
    target 2163
  ]
  edge [
    source 32
    target 2164
  ]
  edge [
    source 32
    target 2165
  ]
  edge [
    source 32
    target 2166
  ]
  edge [
    source 32
    target 2167
  ]
  edge [
    source 32
    target 2168
  ]
  edge [
    source 32
    target 2169
  ]
  edge [
    source 32
    target 2170
  ]
  edge [
    source 32
    target 118
  ]
  edge [
    source 32
    target 2171
  ]
  edge [
    source 32
    target 2172
  ]
  edge [
    source 32
    target 2173
  ]
  edge [
    source 32
    target 2174
  ]
  edge [
    source 32
    target 1860
  ]
  edge [
    source 32
    target 2175
  ]
  edge [
    source 32
    target 1461
  ]
  edge [
    source 32
    target 2176
  ]
  edge [
    source 32
    target 2177
  ]
  edge [
    source 32
    target 2178
  ]
  edge [
    source 32
    target 2179
  ]
  edge [
    source 32
    target 2180
  ]
  edge [
    source 32
    target 2181
  ]
  edge [
    source 32
    target 2182
  ]
  edge [
    source 32
    target 2183
  ]
  edge [
    source 32
    target 2184
  ]
  edge [
    source 32
    target 2185
  ]
  edge [
    source 32
    target 2186
  ]
  edge [
    source 32
    target 2187
  ]
  edge [
    source 32
    target 2188
  ]
  edge [
    source 32
    target 1091
  ]
  edge [
    source 32
    target 2189
  ]
  edge [
    source 32
    target 1093
  ]
  edge [
    source 32
    target 2190
  ]
  edge [
    source 32
    target 2191
  ]
  edge [
    source 32
    target 1094
  ]
  edge [
    source 32
    target 2192
  ]
  edge [
    source 32
    target 1731
  ]
  edge [
    source 32
    target 1088
  ]
  edge [
    source 32
    target 1096
  ]
  edge [
    source 32
    target 1097
  ]
  edge [
    source 32
    target 2193
  ]
  edge [
    source 32
    target 1098
  ]
  edge [
    source 32
    target 2194
  ]
  edge [
    source 32
    target 1099
  ]
  edge [
    source 32
    target 997
  ]
  edge [
    source 32
    target 1101
  ]
  edge [
    source 32
    target 1102
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1103
  ]
  edge [
    source 32
    target 1104
  ]
  edge [
    source 32
    target 560
  ]
  edge [
    source 32
    target 1105
  ]
  edge [
    source 32
    target 1106
  ]
  edge [
    source 32
    target 1728
  ]
  edge [
    source 32
    target 1107
  ]
  edge [
    source 32
    target 2195
  ]
  edge [
    source 32
    target 1110
  ]
  edge [
    source 32
    target 2196
  ]
  edge [
    source 32
    target 1111
  ]
  edge [
    source 32
    target 1112
  ]
  edge [
    source 32
    target 1114
  ]
  edge [
    source 32
    target 2197
  ]
  edge [
    source 32
    target 1115
  ]
  edge [
    source 32
    target 1116
  ]
  edge [
    source 32
    target 1118
  ]
  edge [
    source 32
    target 1119
  ]
  edge [
    source 32
    target 802
  ]
  edge [
    source 32
    target 2198
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 2199
  ]
  edge [
    source 33
    target 2200
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 2201
  ]
  edge [
    source 33
    target 2202
  ]
  edge [
    source 33
    target 308
  ]
  edge [
    source 33
    target 2203
  ]
  edge [
    source 34
    target 1936
  ]
  edge [
    source 34
    target 1937
  ]
  edge [
    source 34
    target 1938
  ]
  edge [
    source 34
    target 1939
  ]
  edge [
    source 34
    target 1940
  ]
  edge [
    source 34
    target 1941
  ]
  edge [
    source 34
    target 1942
  ]
  edge [
    source 34
    target 1943
  ]
  edge [
    source 34
    target 1910
  ]
  edge [
    source 34
    target 1944
  ]
  edge [
    source 34
    target 486
  ]
  edge [
    source 34
    target 363
  ]
  edge [
    source 34
    target 2204
  ]
  edge [
    source 34
    target 2205
  ]
  edge [
    source 34
    target 2206
  ]
  edge [
    source 34
    target 2207
  ]
  edge [
    source 34
    target 2208
  ]
  edge [
    source 34
    target 2209
  ]
  edge [
    source 34
    target 2210
  ]
  edge [
    source 34
    target 2211
  ]
  edge [
    source 34
    target 2212
  ]
  edge [
    source 34
    target 2213
  ]
  edge [
    source 34
    target 2214
  ]
  edge [
    source 34
    target 2215
  ]
  edge [
    source 34
    target 2216
  ]
  edge [
    source 34
    target 2217
  ]
  edge [
    source 34
    target 2218
  ]
  edge [
    source 34
    target 2219
  ]
  edge [
    source 34
    target 1863
  ]
  edge [
    source 34
    target 2220
  ]
  edge [
    source 34
    target 2221
  ]
  edge [
    source 34
    target 2222
  ]
  edge [
    source 34
    target 1713
  ]
  edge [
    source 34
    target 2223
  ]
  edge [
    source 34
    target 2224
  ]
  edge [
    source 34
    target 2225
  ]
  edge [
    source 34
    target 683
  ]
  edge [
    source 34
    target 2226
  ]
  edge [
    source 34
    target 1627
  ]
  edge [
    source 34
    target 1819
  ]
  edge [
    source 34
    target 2227
  ]
  edge [
    source 34
    target 2228
  ]
  edge [
    source 34
    target 1780
  ]
  edge [
    source 34
    target 2229
  ]
  edge [
    source 34
    target 2230
  ]
  edge [
    source 34
    target 2231
  ]
  edge [
    source 34
    target 1820
  ]
  edge [
    source 34
    target 2232
  ]
  edge [
    source 34
    target 2233
  ]
  edge [
    source 34
    target 2234
  ]
  edge [
    source 34
    target 2235
  ]
  edge [
    source 34
    target 564
  ]
  edge [
    source 34
    target 2236
  ]
  edge [
    source 34
    target 2237
  ]
  edge [
    source 34
    target 2238
  ]
  edge [
    source 34
    target 2239
  ]
  edge [
    source 34
    target 2240
  ]
  edge [
    source 34
    target 2241
  ]
  edge [
    source 34
    target 129
  ]
  edge [
    source 34
    target 2242
  ]
  edge [
    source 34
    target 2243
  ]
  edge [
    source 34
    target 2244
  ]
  edge [
    source 34
    target 2245
  ]
  edge [
    source 34
    target 1023
  ]
  edge [
    source 34
    target 2246
  ]
  edge [
    source 34
    target 2247
  ]
  edge [
    source 34
    target 2248
  ]
  edge [
    source 34
    target 2249
  ]
  edge [
    source 34
    target 2250
  ]
  edge [
    source 34
    target 2251
  ]
  edge [
    source 34
    target 2252
  ]
  edge [
    source 34
    target 2253
  ]
  edge [
    source 34
    target 2254
  ]
  edge [
    source 34
    target 2255
  ]
  edge [
    source 34
    target 2256
  ]
  edge [
    source 34
    target 2257
  ]
  edge [
    source 34
    target 2258
  ]
  edge [
    source 34
    target 578
  ]
  edge [
    source 34
    target 2259
  ]
  edge [
    source 34
    target 2260
  ]
  edge [
    source 34
    target 2261
  ]
  edge [
    source 34
    target 2262
  ]
  edge [
    source 34
    target 529
  ]
  edge [
    source 34
    target 2263
  ]
  edge [
    source 34
    target 2264
  ]
  edge [
    source 34
    target 2265
  ]
  edge [
    source 34
    target 2266
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1461
  ]
  edge [
    source 35
    target 2267
  ]
  edge [
    source 35
    target 2268
  ]
  edge [
    source 35
    target 2269
  ]
  edge [
    source 35
    target 2141
  ]
  edge [
    source 35
    target 2142
  ]
  edge [
    source 35
    target 2143
  ]
  edge [
    source 35
    target 2270
  ]
  edge [
    source 35
    target 2271
  ]
  edge [
    source 35
    target 494
  ]
  edge [
    source 35
    target 2272
  ]
  edge [
    source 35
    target 2273
  ]
  edge [
    source 35
    target 2135
  ]
  edge [
    source 35
    target 2274
  ]
  edge [
    source 35
    target 2275
  ]
  edge [
    source 35
    target 2276
  ]
  edge [
    source 35
    target 2277
  ]
  edge [
    source 35
    target 63
  ]
  edge [
    source 35
    target 2278
  ]
  edge [
    source 35
    target 2279
  ]
  edge [
    source 35
    target 2280
  ]
  edge [
    source 35
    target 48
  ]
  edge [
    source 35
    target 2281
  ]
  edge [
    source 35
    target 2282
  ]
  edge [
    source 35
    target 2283
  ]
  edge [
    source 35
    target 2284
  ]
  edge [
    source 35
    target 2285
  ]
  edge [
    source 35
    target 2286
  ]
  edge [
    source 35
    target 1558
  ]
  edge [
    source 35
    target 807
  ]
  edge [
    source 35
    target 2287
  ]
  edge [
    source 35
    target 2288
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 2289
  ]
  edge [
    source 36
    target 145
  ]
  edge [
    source 36
    target 2290
  ]
  edge [
    source 36
    target 2291
  ]
  edge [
    source 36
    target 111
  ]
  edge [
    source 36
    target 2117
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 2292
  ]
  edge [
    source 37
    target 2293
  ]
  edge [
    source 37
    target 2294
  ]
  edge [
    source 37
    target 2295
  ]
  edge [
    source 37
    target 2296
  ]
  edge [
    source 37
    target 2297
  ]
  edge [
    source 37
    target 2298
  ]
  edge [
    source 37
    target 2299
  ]
  edge [
    source 37
    target 2300
  ]
  edge [
    source 37
    target 2301
  ]
  edge [
    source 37
    target 2302
  ]
  edge [
    source 37
    target 2303
  ]
  edge [
    source 37
    target 2304
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 2072
  ]
  edge [
    source 38
    target 2073
  ]
  edge [
    source 38
    target 486
  ]
  edge [
    source 38
    target 2074
  ]
  edge [
    source 38
    target 2075
  ]
  edge [
    source 38
    target 2076
  ]
  edge [
    source 38
    target 2077
  ]
  edge [
    source 38
    target 2078
  ]
  edge [
    source 38
    target 2079
  ]
  edge [
    source 38
    target 2080
  ]
  edge [
    source 38
    target 1936
  ]
  edge [
    source 38
    target 2305
  ]
  edge [
    source 38
    target 63
  ]
  edge [
    source 38
    target 2181
  ]
  edge [
    source 38
    target 2306
  ]
  edge [
    source 38
    target 2307
  ]
  edge [
    source 38
    target 2308
  ]
  edge [
    source 38
    target 2309
  ]
  edge [
    source 38
    target 2310
  ]
  edge [
    source 38
    target 1895
  ]
  edge [
    source 38
    target 1896
  ]
  edge [
    source 38
    target 1897
  ]
  edge [
    source 38
    target 1898
  ]
  edge [
    source 38
    target 1899
  ]
  edge [
    source 38
    target 1900
  ]
  edge [
    source 38
    target 1901
  ]
  edge [
    source 38
    target 1902
  ]
  edge [
    source 38
    target 171
  ]
  edge [
    source 38
    target 172
  ]
  edge [
    source 38
    target 1903
  ]
  edge [
    source 38
    target 1904
  ]
  edge [
    source 38
    target 1905
  ]
  edge [
    source 38
    target 1906
  ]
  edge [
    source 38
    target 1907
  ]
  edge [
    source 38
    target 1908
  ]
  edge [
    source 38
    target 475
  ]
  edge [
    source 38
    target 1909
  ]
  edge [
    source 38
    target 1910
  ]
  edge [
    source 38
    target 1911
  ]
  edge [
    source 38
    target 1912
  ]
  edge [
    source 38
    target 1913
  ]
  edge [
    source 38
    target 1914
  ]
  edge [
    source 38
    target 1915
  ]
  edge [
    source 38
    target 1916
  ]
  edge [
    source 38
    target 1917
  ]
  edge [
    source 38
    target 1918
  ]
  edge [
    source 38
    target 1919
  ]
  edge [
    source 38
    target 1920
  ]
  edge [
    source 38
    target 1921
  ]
  edge [
    source 38
    target 1922
  ]
  edge [
    source 38
    target 1923
  ]
  edge [
    source 38
    target 1924
  ]
  edge [
    source 38
    target 1925
  ]
  edge [
    source 38
    target 1926
  ]
  edge [
    source 38
    target 1927
  ]
  edge [
    source 38
    target 1013
  ]
  edge [
    source 38
    target 1928
  ]
  edge [
    source 38
    target 1929
  ]
  edge [
    source 38
    target 1930
  ]
  edge [
    source 38
    target 1931
  ]
  edge [
    source 38
    target 1932
  ]
  edge [
    source 38
    target 1933
  ]
  edge [
    source 38
    target 2311
  ]
  edge [
    source 38
    target 2312
  ]
  edge [
    source 38
    target 2313
  ]
  edge [
    source 38
    target 2314
  ]
  edge [
    source 38
    target 2315
  ]
  edge [
    source 38
    target 575
  ]
  edge [
    source 38
    target 2316
  ]
  edge [
    source 38
    target 2317
  ]
  edge [
    source 38
    target 2318
  ]
  edge [
    source 38
    target 2319
  ]
  edge [
    source 38
    target 2320
  ]
  edge [
    source 38
    target 2321
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 2322
  ]
  edge [
    source 39
    target 2323
  ]
  edge [
    source 39
    target 2324
  ]
  edge [
    source 39
    target 2325
  ]
  edge [
    source 39
    target 2326
  ]
  edge [
    source 39
    target 2327
  ]
  edge [
    source 39
    target 2328
  ]
  edge [
    source 39
    target 2329
  ]
  edge [
    source 39
    target 535
  ]
  edge [
    source 39
    target 2330
  ]
  edge [
    source 39
    target 2331
  ]
  edge [
    source 39
    target 2332
  ]
  edge [
    source 39
    target 2333
  ]
  edge [
    source 39
    target 2334
  ]
  edge [
    source 39
    target 2335
  ]
  edge [
    source 39
    target 2336
  ]
  edge [
    source 39
    target 2337
  ]
  edge [
    source 39
    target 2338
  ]
  edge [
    source 39
    target 2339
  ]
  edge [
    source 39
    target 2340
  ]
  edge [
    source 39
    target 2341
  ]
  edge [
    source 39
    target 2342
  ]
  edge [
    source 39
    target 2343
  ]
  edge [
    source 39
    target 2344
  ]
  edge [
    source 39
    target 2345
  ]
  edge [
    source 39
    target 2346
  ]
  edge [
    source 39
    target 2347
  ]
  edge [
    source 39
    target 2348
  ]
  edge [
    source 39
    target 2349
  ]
  edge [
    source 39
    target 2350
  ]
  edge [
    source 39
    target 2351
  ]
  edge [
    source 39
    target 2352
  ]
  edge [
    source 39
    target 2353
  ]
  edge [
    source 39
    target 2354
  ]
  edge [
    source 39
    target 2355
  ]
  edge [
    source 39
    target 2356
  ]
  edge [
    source 39
    target 2357
  ]
  edge [
    source 39
    target 2358
  ]
  edge [
    source 39
    target 2359
  ]
  edge [
    source 39
    target 2360
  ]
  edge [
    source 39
    target 2361
  ]
  edge [
    source 39
    target 2362
  ]
  edge [
    source 39
    target 2363
  ]
  edge [
    source 39
    target 2364
  ]
  edge [
    source 39
    target 2365
  ]
  edge [
    source 39
    target 2366
  ]
  edge [
    source 39
    target 2367
  ]
  edge [
    source 39
    target 2368
  ]
  edge [
    source 39
    target 2369
  ]
  edge [
    source 39
    target 2370
  ]
  edge [
    source 39
    target 2371
  ]
  edge [
    source 39
    target 2372
  ]
  edge [
    source 39
    target 2373
  ]
  edge [
    source 39
    target 2374
  ]
  edge [
    source 39
    target 2375
  ]
  edge [
    source 39
    target 2376
  ]
  edge [
    source 39
    target 2377
  ]
  edge [
    source 39
    target 2378
  ]
  edge [
    source 39
    target 2379
  ]
  edge [
    source 39
    target 2380
  ]
  edge [
    source 39
    target 2381
  ]
  edge [
    source 39
    target 2382
  ]
  edge [
    source 39
    target 2383
  ]
  edge [
    source 39
    target 2384
  ]
  edge [
    source 39
    target 2385
  ]
  edge [
    source 39
    target 2386
  ]
  edge [
    source 39
    target 2387
  ]
  edge [
    source 39
    target 2388
  ]
  edge [
    source 39
    target 2389
  ]
  edge [
    source 39
    target 2390
  ]
  edge [
    source 39
    target 2391
  ]
  edge [
    source 39
    target 2392
  ]
  edge [
    source 39
    target 2393
  ]
  edge [
    source 39
    target 2394
  ]
  edge [
    source 39
    target 2395
  ]
  edge [
    source 39
    target 2396
  ]
  edge [
    source 39
    target 2397
  ]
  edge [
    source 39
    target 2398
  ]
  edge [
    source 39
    target 2399
  ]
  edge [
    source 39
    target 2400
  ]
  edge [
    source 39
    target 2401
  ]
  edge [
    source 39
    target 2402
  ]
  edge [
    source 39
    target 2403
  ]
  edge [
    source 39
    target 2404
  ]
  edge [
    source 39
    target 2405
  ]
  edge [
    source 39
    target 2406
  ]
  edge [
    source 39
    target 2407
  ]
  edge [
    source 39
    target 2408
  ]
  edge [
    source 39
    target 2409
  ]
  edge [
    source 39
    target 2410
  ]
  edge [
    source 39
    target 351
  ]
  edge [
    source 39
    target 2411
  ]
  edge [
    source 39
    target 2412
  ]
  edge [
    source 39
    target 2413
  ]
  edge [
    source 39
    target 2414
  ]
  edge [
    source 39
    target 2415
  ]
  edge [
    source 39
    target 2416
  ]
  edge [
    source 39
    target 2417
  ]
  edge [
    source 39
    target 2418
  ]
  edge [
    source 40
    target 2419
  ]
  edge [
    source 40
    target 1052
  ]
  edge [
    source 40
    target 2420
  ]
  edge [
    source 40
    target 952
  ]
  edge [
    source 40
    target 2421
  ]
  edge [
    source 40
    target 2422
  ]
  edge [
    source 40
    target 1713
  ]
  edge [
    source 40
    target 2423
  ]
  edge [
    source 40
    target 2424
  ]
  edge [
    source 40
    target 983
  ]
  edge [
    source 40
    target 2425
  ]
  edge [
    source 40
    target 2426
  ]
  edge [
    source 40
    target 2427
  ]
  edge [
    source 40
    target 2428
  ]
  edge [
    source 40
    target 2429
  ]
  edge [
    source 40
    target 2430
  ]
  edge [
    source 40
    target 2431
  ]
  edge [
    source 40
    target 2432
  ]
  edge [
    source 40
    target 2433
  ]
  edge [
    source 40
    target 2434
  ]
  edge [
    source 40
    target 2435
  ]
  edge [
    source 40
    target 2436
  ]
  edge [
    source 40
    target 2437
  ]
  edge [
    source 40
    target 2438
  ]
  edge [
    source 40
    target 2439
  ]
  edge [
    source 40
    target 2440
  ]
  edge [
    source 40
    target 2441
  ]
  edge [
    source 40
    target 997
  ]
  edge [
    source 40
    target 2442
  ]
  edge [
    source 40
    target 2443
  ]
  edge [
    source 40
    target 2444
  ]
  edge [
    source 40
    target 2445
  ]
  edge [
    source 40
    target 2446
  ]
  edge [
    source 40
    target 2447
  ]
  edge [
    source 40
    target 2448
  ]
  edge [
    source 40
    target 2449
  ]
  edge [
    source 40
    target 2450
  ]
  edge [
    source 40
    target 2451
  ]
  edge [
    source 40
    target 2452
  ]
  edge [
    source 40
    target 2453
  ]
  edge [
    source 40
    target 2454
  ]
  edge [
    source 40
    target 2455
  ]
  edge [
    source 40
    target 2456
  ]
  edge [
    source 40
    target 2457
  ]
  edge [
    source 40
    target 2458
  ]
  edge [
    source 40
    target 2459
  ]
  edge [
    source 40
    target 2460
  ]
  edge [
    source 40
    target 2461
  ]
  edge [
    source 40
    target 2462
  ]
  edge [
    source 40
    target 2463
  ]
  edge [
    source 40
    target 2464
  ]
  edge [
    source 40
    target 2465
  ]
  edge [
    source 40
    target 334
  ]
  edge [
    source 40
    target 2466
  ]
  edge [
    source 40
    target 1969
  ]
  edge [
    source 40
    target 1757
  ]
  edge [
    source 40
    target 1961
  ]
  edge [
    source 40
    target 2022
  ]
  edge [
    source 40
    target 2467
  ]
  edge [
    source 40
    target 2468
  ]
  edge [
    source 40
    target 2469
  ]
  edge [
    source 40
    target 1801
  ]
  edge [
    source 40
    target 2470
  ]
  edge [
    source 40
    target 2195
  ]
  edge [
    source 40
    target 2471
  ]
  edge [
    source 40
    target 2472
  ]
  edge [
    source 40
    target 2473
  ]
  edge [
    source 40
    target 560
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 40
    target 2474
  ]
  edge [
    source 40
    target 2475
  ]
  edge [
    source 40
    target 2476
  ]
  edge [
    source 40
    target 2477
  ]
  edge [
    source 40
    target 2005
  ]
  edge [
    source 40
    target 2478
  ]
  edge [
    source 40
    target 2479
  ]
  edge [
    source 40
    target 2480
  ]
  edge [
    source 40
    target 2481
  ]
  edge [
    source 40
    target 2482
  ]
  edge [
    source 40
    target 2483
  ]
  edge [
    source 40
    target 1593
  ]
  edge [
    source 40
    target 2484
  ]
  edge [
    source 40
    target 884
  ]
  edge [
    source 40
    target 2485
  ]
  edge [
    source 40
    target 2486
  ]
  edge [
    source 40
    target 2487
  ]
  edge [
    source 40
    target 2488
  ]
  edge [
    source 40
    target 2489
  ]
  edge [
    source 40
    target 93
  ]
  edge [
    source 40
    target 2490
  ]
  edge [
    source 40
    target 2491
  ]
  edge [
    source 40
    target 2492
  ]
  edge [
    source 40
    target 2493
  ]
  edge [
    source 40
    target 2494
  ]
  edge [
    source 40
    target 2495
  ]
  edge [
    source 40
    target 2496
  ]
  edge [
    source 40
    target 2497
  ]
  edge [
    source 40
    target 2498
  ]
  edge [
    source 40
    target 2499
  ]
  edge [
    source 40
    target 2197
  ]
  edge [
    source 40
    target 2500
  ]
  edge [
    source 40
    target 2501
  ]
  edge [
    source 40
    target 2116
  ]
  edge [
    source 40
    target 2502
  ]
  edge [
    source 40
    target 2503
  ]
  edge [
    source 40
    target 2504
  ]
  edge [
    source 40
    target 63
  ]
  edge [
    source 40
    target 2505
  ]
  edge [
    source 40
    target 2506
  ]
  edge [
    source 40
    target 2507
  ]
  edge [
    source 40
    target 2508
  ]
  edge [
    source 40
    target 2509
  ]
  edge [
    source 40
    target 2510
  ]
  edge [
    source 40
    target 1555
  ]
]
