graph [
  node [
    id 0
    label "reprezentacja"
    origin "text"
  ]
  node [
    id 1
    label "polska"
    origin "text"
  ]
  node [
    id 2
    label "awans"
    origin "text"
  ]
  node [
    id 3
    label "turniej"
    origin "text"
  ]
  node [
    id 4
    label "powalczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "grupa"
    origin "text"
  ]
  node [
    id 6
    label "gram"
    origin "text"
  ]
  node [
    id 7
    label "austria"
    origin "text"
  ]
  node [
    id 8
    label "izrael"
    origin "text"
  ]
  node [
    id 9
    label "s&#322;owenia"
    origin "text"
  ]
  node [
    id 10
    label "macedonia"
    origin "text"
  ]
  node [
    id 11
    label "&#322;otwa"
    origin "text"
  ]
  node [
    id 12
    label "zesp&#243;&#322;"
  ]
  node [
    id 13
    label "dru&#380;yna"
  ]
  node [
    id 14
    label "emblemat"
  ]
  node [
    id 15
    label "deputation"
  ]
  node [
    id 16
    label "Mazowsze"
  ]
  node [
    id 17
    label "odm&#322;adzanie"
  ]
  node [
    id 18
    label "&#346;wietliki"
  ]
  node [
    id 19
    label "zbi&#243;r"
  ]
  node [
    id 20
    label "whole"
  ]
  node [
    id 21
    label "skupienie"
  ]
  node [
    id 22
    label "The_Beatles"
  ]
  node [
    id 23
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 24
    label "odm&#322;adza&#263;"
  ]
  node [
    id 25
    label "zabudowania"
  ]
  node [
    id 26
    label "group"
  ]
  node [
    id 27
    label "zespolik"
  ]
  node [
    id 28
    label "schorzenie"
  ]
  node [
    id 29
    label "ro&#347;lina"
  ]
  node [
    id 30
    label "Depeche_Mode"
  ]
  node [
    id 31
    label "batch"
  ]
  node [
    id 32
    label "odm&#322;odzenie"
  ]
  node [
    id 33
    label "formacja"
  ]
  node [
    id 34
    label "dublet"
  ]
  node [
    id 35
    label "szczep"
  ]
  node [
    id 36
    label "zast&#281;p"
  ]
  node [
    id 37
    label "pododdzia&#322;"
  ]
  node [
    id 38
    label "pluton"
  ]
  node [
    id 39
    label "force"
  ]
  node [
    id 40
    label "dzie&#322;o"
  ]
  node [
    id 41
    label "gest"
  ]
  node [
    id 42
    label "symbol"
  ]
  node [
    id 43
    label "position"
  ]
  node [
    id 44
    label "preferment"
  ]
  node [
    id 45
    label "status"
  ]
  node [
    id 46
    label "wzrost"
  ]
  node [
    id 47
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 48
    label "korzy&#347;&#263;"
  ]
  node [
    id 49
    label "kariera"
  ]
  node [
    id 50
    label "nagroda"
  ]
  node [
    id 51
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 52
    label "zaliczka"
  ]
  node [
    id 53
    label "kwota"
  ]
  node [
    id 54
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 55
    label "oskar"
  ]
  node [
    id 56
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 57
    label "return"
  ]
  node [
    id 58
    label "konsekwencja"
  ]
  node [
    id 59
    label "uzyskanie"
  ]
  node [
    id 60
    label "dochrapanie_si&#281;"
  ]
  node [
    id 61
    label "skill"
  ]
  node [
    id 62
    label "accomplishment"
  ]
  node [
    id 63
    label "zdarzenie_si&#281;"
  ]
  node [
    id 64
    label "sukces"
  ]
  node [
    id 65
    label "zaawansowanie"
  ]
  node [
    id 66
    label "dotarcie"
  ]
  node [
    id 67
    label "act"
  ]
  node [
    id 68
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 69
    label "zaleta"
  ]
  node [
    id 70
    label "dobro"
  ]
  node [
    id 71
    label "increase"
  ]
  node [
    id 72
    label "rozw&#243;j"
  ]
  node [
    id 73
    label "wegetacja"
  ]
  node [
    id 74
    label "wysoko&#347;&#263;"
  ]
  node [
    id 75
    label "zmiana"
  ]
  node [
    id 76
    label "przebieg"
  ]
  node [
    id 77
    label "awansowa&#263;"
  ]
  node [
    id 78
    label "pozycja_spo&#322;eczna"
  ]
  node [
    id 79
    label "awansowanie"
  ]
  node [
    id 80
    label "degradacja"
  ]
  node [
    id 81
    label "condition"
  ]
  node [
    id 82
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 83
    label "znaczenie"
  ]
  node [
    id 84
    label "stan"
  ]
  node [
    id 85
    label "podmiotowo"
  ]
  node [
    id 86
    label "sytuacja"
  ]
  node [
    id 87
    label "impreza"
  ]
  node [
    id 88
    label "Wielki_Szlem"
  ]
  node [
    id 89
    label "pojedynek"
  ]
  node [
    id 90
    label "drive"
  ]
  node [
    id 91
    label "runda"
  ]
  node [
    id 92
    label "rywalizacja"
  ]
  node [
    id 93
    label "zawody"
  ]
  node [
    id 94
    label "eliminacje"
  ]
  node [
    id 95
    label "tournament"
  ]
  node [
    id 96
    label "contest"
  ]
  node [
    id 97
    label "wydarzenie"
  ]
  node [
    id 98
    label "impra"
  ]
  node [
    id 99
    label "rozrywka"
  ]
  node [
    id 100
    label "przyj&#281;cie"
  ]
  node [
    id 101
    label "okazja"
  ]
  node [
    id 102
    label "party"
  ]
  node [
    id 103
    label "walczy&#263;"
  ]
  node [
    id 104
    label "walczenie"
  ]
  node [
    id 105
    label "tysi&#281;cznik"
  ]
  node [
    id 106
    label "champion"
  ]
  node [
    id 107
    label "spadochroniarstwo"
  ]
  node [
    id 108
    label "kategoria_open"
  ]
  node [
    id 109
    label "engagement"
  ]
  node [
    id 110
    label "walka"
  ]
  node [
    id 111
    label "wyzwanie"
  ]
  node [
    id 112
    label "wyzwa&#263;"
  ]
  node [
    id 113
    label "odyniec"
  ]
  node [
    id 114
    label "wyzywa&#263;"
  ]
  node [
    id 115
    label "sekundant"
  ]
  node [
    id 116
    label "competitiveness"
  ]
  node [
    id 117
    label "sp&#243;r"
  ]
  node [
    id 118
    label "bout"
  ]
  node [
    id 119
    label "wyzywanie"
  ]
  node [
    id 120
    label "konkurs"
  ]
  node [
    id 121
    label "faza"
  ]
  node [
    id 122
    label "retirement"
  ]
  node [
    id 123
    label "rozgrywka"
  ]
  node [
    id 124
    label "seria"
  ]
  node [
    id 125
    label "rhythm"
  ]
  node [
    id 126
    label "czas"
  ]
  node [
    id 127
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 128
    label "okr&#261;&#380;enie"
  ]
  node [
    id 129
    label "porobi&#263;"
  ]
  node [
    id 130
    label "bash"
  ]
  node [
    id 131
    label "Doctor_of_Osteopathy"
  ]
  node [
    id 132
    label "liga"
  ]
  node [
    id 133
    label "jednostka_systematyczna"
  ]
  node [
    id 134
    label "asymilowanie"
  ]
  node [
    id 135
    label "gromada"
  ]
  node [
    id 136
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 137
    label "asymilowa&#263;"
  ]
  node [
    id 138
    label "egzemplarz"
  ]
  node [
    id 139
    label "Entuzjastki"
  ]
  node [
    id 140
    label "kompozycja"
  ]
  node [
    id 141
    label "Terranie"
  ]
  node [
    id 142
    label "category"
  ]
  node [
    id 143
    label "pakiet_klimatyczny"
  ]
  node [
    id 144
    label "oddzia&#322;"
  ]
  node [
    id 145
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 146
    label "cz&#261;steczka"
  ]
  node [
    id 147
    label "stage_set"
  ]
  node [
    id 148
    label "type"
  ]
  node [
    id 149
    label "specgrupa"
  ]
  node [
    id 150
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 151
    label "Eurogrupa"
  ]
  node [
    id 152
    label "formacja_geologiczna"
  ]
  node [
    id 153
    label "harcerze_starsi"
  ]
  node [
    id 154
    label "konfiguracja"
  ]
  node [
    id 155
    label "cz&#261;stka"
  ]
  node [
    id 156
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 157
    label "diadochia"
  ]
  node [
    id 158
    label "substancja"
  ]
  node [
    id 159
    label "grupa_funkcyjna"
  ]
  node [
    id 160
    label "integer"
  ]
  node [
    id 161
    label "liczba"
  ]
  node [
    id 162
    label "zlewanie_si&#281;"
  ]
  node [
    id 163
    label "ilo&#347;&#263;"
  ]
  node [
    id 164
    label "uk&#322;ad"
  ]
  node [
    id 165
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 166
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 167
    label "pe&#322;ny"
  ]
  node [
    id 168
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 169
    label "series"
  ]
  node [
    id 170
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 171
    label "uprawianie"
  ]
  node [
    id 172
    label "praca_rolnicza"
  ]
  node [
    id 173
    label "collection"
  ]
  node [
    id 174
    label "dane"
  ]
  node [
    id 175
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 176
    label "poj&#281;cie"
  ]
  node [
    id 177
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 178
    label "sum"
  ]
  node [
    id 179
    label "gathering"
  ]
  node [
    id 180
    label "album"
  ]
  node [
    id 181
    label "lias"
  ]
  node [
    id 182
    label "dzia&#322;"
  ]
  node [
    id 183
    label "system"
  ]
  node [
    id 184
    label "jednostka"
  ]
  node [
    id 185
    label "pi&#281;tro"
  ]
  node [
    id 186
    label "klasa"
  ]
  node [
    id 187
    label "jednostka_geologiczna"
  ]
  node [
    id 188
    label "filia"
  ]
  node [
    id 189
    label "malm"
  ]
  node [
    id 190
    label "dogger"
  ]
  node [
    id 191
    label "poziom"
  ]
  node [
    id 192
    label "promocja"
  ]
  node [
    id 193
    label "kurs"
  ]
  node [
    id 194
    label "bank"
  ]
  node [
    id 195
    label "ajencja"
  ]
  node [
    id 196
    label "wojsko"
  ]
  node [
    id 197
    label "siedziba"
  ]
  node [
    id 198
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 199
    label "agencja"
  ]
  node [
    id 200
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 201
    label "szpital"
  ]
  node [
    id 202
    label "blend"
  ]
  node [
    id 203
    label "struktura"
  ]
  node [
    id 204
    label "prawo_karne"
  ]
  node [
    id 205
    label "leksem"
  ]
  node [
    id 206
    label "figuracja"
  ]
  node [
    id 207
    label "chwyt"
  ]
  node [
    id 208
    label "okup"
  ]
  node [
    id 209
    label "muzykologia"
  ]
  node [
    id 210
    label "&#347;redniowiecze"
  ]
  node [
    id 211
    label "czynnik_biotyczny"
  ]
  node [
    id 212
    label "wyewoluowanie"
  ]
  node [
    id 213
    label "reakcja"
  ]
  node [
    id 214
    label "individual"
  ]
  node [
    id 215
    label "przyswoi&#263;"
  ]
  node [
    id 216
    label "wytw&#243;r"
  ]
  node [
    id 217
    label "starzenie_si&#281;"
  ]
  node [
    id 218
    label "wyewoluowa&#263;"
  ]
  node [
    id 219
    label "okaz"
  ]
  node [
    id 220
    label "part"
  ]
  node [
    id 221
    label "ewoluowa&#263;"
  ]
  node [
    id 222
    label "przyswojenie"
  ]
  node [
    id 223
    label "ewoluowanie"
  ]
  node [
    id 224
    label "obiekt"
  ]
  node [
    id 225
    label "sztuka"
  ]
  node [
    id 226
    label "agent"
  ]
  node [
    id 227
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 228
    label "przyswaja&#263;"
  ]
  node [
    id 229
    label "nicpo&#324;"
  ]
  node [
    id 230
    label "przyswajanie"
  ]
  node [
    id 231
    label "feminizm"
  ]
  node [
    id 232
    label "Unia_Europejska"
  ]
  node [
    id 233
    label "odtwarzanie"
  ]
  node [
    id 234
    label "uatrakcyjnianie"
  ]
  node [
    id 235
    label "zast&#281;powanie"
  ]
  node [
    id 236
    label "odbudowywanie"
  ]
  node [
    id 237
    label "rejuvenation"
  ]
  node [
    id 238
    label "m&#322;odszy"
  ]
  node [
    id 239
    label "odbudowywa&#263;"
  ]
  node [
    id 240
    label "m&#322;odzi&#263;"
  ]
  node [
    id 241
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 242
    label "przewietrza&#263;"
  ]
  node [
    id 243
    label "wymienia&#263;"
  ]
  node [
    id 244
    label "odtwarza&#263;"
  ]
  node [
    id 245
    label "uatrakcyjni&#263;"
  ]
  node [
    id 246
    label "przewietrzy&#263;"
  ]
  node [
    id 247
    label "regenerate"
  ]
  node [
    id 248
    label "odtworzy&#263;"
  ]
  node [
    id 249
    label "wymieni&#263;"
  ]
  node [
    id 250
    label "odbudowa&#263;"
  ]
  node [
    id 251
    label "wymienienie"
  ]
  node [
    id 252
    label "uatrakcyjnienie"
  ]
  node [
    id 253
    label "odbudowanie"
  ]
  node [
    id 254
    label "odtworzenie"
  ]
  node [
    id 255
    label "asymilowanie_si&#281;"
  ]
  node [
    id 256
    label "absorption"
  ]
  node [
    id 257
    label "pobieranie"
  ]
  node [
    id 258
    label "czerpanie"
  ]
  node [
    id 259
    label "acquisition"
  ]
  node [
    id 260
    label "cz&#322;owiek"
  ]
  node [
    id 261
    label "zmienianie"
  ]
  node [
    id 262
    label "organizm"
  ]
  node [
    id 263
    label "assimilation"
  ]
  node [
    id 264
    label "upodabnianie"
  ]
  node [
    id 265
    label "g&#322;oska"
  ]
  node [
    id 266
    label "kultura"
  ]
  node [
    id 267
    label "podobny"
  ]
  node [
    id 268
    label "fonetyka"
  ]
  node [
    id 269
    label "mecz_mistrzowski"
  ]
  node [
    id 270
    label "&#347;rodowisko"
  ]
  node [
    id 271
    label "arrangement"
  ]
  node [
    id 272
    label "obrona"
  ]
  node [
    id 273
    label "pomoc"
  ]
  node [
    id 274
    label "organizacja"
  ]
  node [
    id 275
    label "rezerwa"
  ]
  node [
    id 276
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 277
    label "pr&#243;ba"
  ]
  node [
    id 278
    label "atak"
  ]
  node [
    id 279
    label "moneta"
  ]
  node [
    id 280
    label "union"
  ]
  node [
    id 281
    label "assimilate"
  ]
  node [
    id 282
    label "dostosowywa&#263;"
  ]
  node [
    id 283
    label "dostosowa&#263;"
  ]
  node [
    id 284
    label "przejmowa&#263;"
  ]
  node [
    id 285
    label "upodobni&#263;"
  ]
  node [
    id 286
    label "przej&#261;&#263;"
  ]
  node [
    id 287
    label "upodabnia&#263;"
  ]
  node [
    id 288
    label "pobiera&#263;"
  ]
  node [
    id 289
    label "pobra&#263;"
  ]
  node [
    id 290
    label "typ"
  ]
  node [
    id 291
    label "jednostka_administracyjna"
  ]
  node [
    id 292
    label "zoologia"
  ]
  node [
    id 293
    label "kr&#243;lestwo"
  ]
  node [
    id 294
    label "tribe"
  ]
  node [
    id 295
    label "hurma"
  ]
  node [
    id 296
    label "botanika"
  ]
  node [
    id 297
    label "centygram"
  ]
  node [
    id 298
    label "megagram"
  ]
  node [
    id 299
    label "metryczna_jednostka_masy"
  ]
  node [
    id 300
    label "miligram"
  ]
  node [
    id 301
    label "dekagram"
  ]
  node [
    id 302
    label "decygram"
  ]
  node [
    id 303
    label "mikrogram"
  ]
  node [
    id 304
    label "hektogram"
  ]
  node [
    id 305
    label "kilogram"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
]
