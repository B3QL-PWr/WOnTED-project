graph [
  node [
    id 0
    label "teraz"
    origin "text"
  ]
  node [
    id 1
    label "musza"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "ukrywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 5
    label "chwila"
  ]
  node [
    id 6
    label "time"
  ]
  node [
    id 7
    label "czas"
  ]
  node [
    id 8
    label "zawiera&#263;"
  ]
  node [
    id 9
    label "suppress"
  ]
  node [
    id 10
    label "report"
  ]
  node [
    id 11
    label "meliniarz"
  ]
  node [
    id 12
    label "cache"
  ]
  node [
    id 13
    label "umieszcza&#263;"
  ]
  node [
    id 14
    label "zachowywa&#263;"
  ]
  node [
    id 15
    label "tajemnica"
  ]
  node [
    id 16
    label "robi&#263;"
  ]
  node [
    id 17
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 18
    label "zdyscyplinowanie"
  ]
  node [
    id 19
    label "podtrzymywa&#263;"
  ]
  node [
    id 20
    label "post"
  ]
  node [
    id 21
    label "control"
  ]
  node [
    id 22
    label "przechowywa&#263;"
  ]
  node [
    id 23
    label "behave"
  ]
  node [
    id 24
    label "dieta"
  ]
  node [
    id 25
    label "hold"
  ]
  node [
    id 26
    label "post&#281;powa&#263;"
  ]
  node [
    id 27
    label "plasowa&#263;"
  ]
  node [
    id 28
    label "umie&#347;ci&#263;"
  ]
  node [
    id 29
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 30
    label "pomieszcza&#263;"
  ]
  node [
    id 31
    label "accommodate"
  ]
  node [
    id 32
    label "zmienia&#263;"
  ]
  node [
    id 33
    label "powodowa&#263;"
  ]
  node [
    id 34
    label "venture"
  ]
  node [
    id 35
    label "wpiernicza&#263;"
  ]
  node [
    id 36
    label "okre&#347;la&#263;"
  ]
  node [
    id 37
    label "poznawa&#263;"
  ]
  node [
    id 38
    label "fold"
  ]
  node [
    id 39
    label "obejmowa&#263;"
  ]
  node [
    id 40
    label "mie&#263;"
  ]
  node [
    id 41
    label "lock"
  ]
  node [
    id 42
    label "make"
  ]
  node [
    id 43
    label "ustala&#263;"
  ]
  node [
    id 44
    label "zamyka&#263;"
  ]
  node [
    id 45
    label "umowa"
  ]
  node [
    id 46
    label "cover"
  ]
  node [
    id 47
    label "pami&#281;&#263;"
  ]
  node [
    id 48
    label "przest&#281;pca"
  ]
  node [
    id 49
    label "bimbrownik"
  ]
  node [
    id 50
    label "p&#281;dzi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
]
