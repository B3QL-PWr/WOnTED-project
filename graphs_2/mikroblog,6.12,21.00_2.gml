graph [
  node [
    id 0
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 1
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 3
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 4
    label "sam"
    origin "text"
  ]
  node [
    id 5
    label "zadba&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#347;wi&#261;teczny"
    origin "text"
  ]
  node [
    id 7
    label "prezent"
    origin "text"
  ]
  node [
    id 8
    label "line_up"
  ]
  node [
    id 9
    label "sta&#263;_si&#281;"
  ]
  node [
    id 10
    label "przyby&#263;"
  ]
  node [
    id 11
    label "zaistnie&#263;"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "doj&#347;&#263;"
  ]
  node [
    id 14
    label "become"
  ]
  node [
    id 15
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 16
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 17
    label "appear"
  ]
  node [
    id 18
    label "get"
  ]
  node [
    id 19
    label "dotrze&#263;"
  ]
  node [
    id 20
    label "zyska&#263;"
  ]
  node [
    id 21
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 22
    label "supervene"
  ]
  node [
    id 23
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 24
    label "zaj&#347;&#263;"
  ]
  node [
    id 25
    label "catch"
  ]
  node [
    id 26
    label "bodziec"
  ]
  node [
    id 27
    label "informacja"
  ]
  node [
    id 28
    label "przesy&#322;ka"
  ]
  node [
    id 29
    label "dodatek"
  ]
  node [
    id 30
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 31
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 32
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 33
    label "heed"
  ]
  node [
    id 34
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 35
    label "spowodowa&#263;"
  ]
  node [
    id 36
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 37
    label "dozna&#263;"
  ]
  node [
    id 38
    label "dokoptowa&#263;"
  ]
  node [
    id 39
    label "postrzega&#263;"
  ]
  node [
    id 40
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 41
    label "orgazm"
  ]
  node [
    id 42
    label "dolecie&#263;"
  ]
  node [
    id 43
    label "drive"
  ]
  node [
    id 44
    label "uzyska&#263;"
  ]
  node [
    id 45
    label "dop&#322;ata"
  ]
  node [
    id 46
    label "poprzedzanie"
  ]
  node [
    id 47
    label "czasoprzestrze&#324;"
  ]
  node [
    id 48
    label "laba"
  ]
  node [
    id 49
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 50
    label "chronometria"
  ]
  node [
    id 51
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 52
    label "rachuba_czasu"
  ]
  node [
    id 53
    label "przep&#322;ywanie"
  ]
  node [
    id 54
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 55
    label "czasokres"
  ]
  node [
    id 56
    label "odczyt"
  ]
  node [
    id 57
    label "chwila"
  ]
  node [
    id 58
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 59
    label "dzieje"
  ]
  node [
    id 60
    label "kategoria_gramatyczna"
  ]
  node [
    id 61
    label "poprzedzenie"
  ]
  node [
    id 62
    label "trawienie"
  ]
  node [
    id 63
    label "pochodzi&#263;"
  ]
  node [
    id 64
    label "period"
  ]
  node [
    id 65
    label "okres_czasu"
  ]
  node [
    id 66
    label "poprzedza&#263;"
  ]
  node [
    id 67
    label "schy&#322;ek"
  ]
  node [
    id 68
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 69
    label "odwlekanie_si&#281;"
  ]
  node [
    id 70
    label "zegar"
  ]
  node [
    id 71
    label "czwarty_wymiar"
  ]
  node [
    id 72
    label "pochodzenie"
  ]
  node [
    id 73
    label "koniugacja"
  ]
  node [
    id 74
    label "Zeitgeist"
  ]
  node [
    id 75
    label "trawi&#263;"
  ]
  node [
    id 76
    label "pogoda"
  ]
  node [
    id 77
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 78
    label "poprzedzi&#263;"
  ]
  node [
    id 79
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 80
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 81
    label "time_period"
  ]
  node [
    id 82
    label "doba"
  ]
  node [
    id 83
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 84
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 85
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 86
    label "teraz"
  ]
  node [
    id 87
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 88
    label "jednocze&#347;nie"
  ]
  node [
    id 89
    label "tydzie&#324;"
  ]
  node [
    id 90
    label "noc"
  ]
  node [
    id 91
    label "dzie&#324;"
  ]
  node [
    id 92
    label "godzina"
  ]
  node [
    id 93
    label "long_time"
  ]
  node [
    id 94
    label "jednostka_geologiczna"
  ]
  node [
    id 95
    label "sklep"
  ]
  node [
    id 96
    label "p&#243;&#322;ka"
  ]
  node [
    id 97
    label "firma"
  ]
  node [
    id 98
    label "stoisko"
  ]
  node [
    id 99
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 100
    label "sk&#322;ad"
  ]
  node [
    id 101
    label "obiekt_handlowy"
  ]
  node [
    id 102
    label "zaplecze"
  ]
  node [
    id 103
    label "witryna"
  ]
  node [
    id 104
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 105
    label "postara&#263;_si&#281;"
  ]
  node [
    id 106
    label "zatroszczy&#263;_si&#281;"
  ]
  node [
    id 107
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 108
    label "obrz&#281;dowy"
  ]
  node [
    id 109
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 110
    label "dzie&#324;_wolny"
  ]
  node [
    id 111
    label "wyj&#261;tkowy"
  ]
  node [
    id 112
    label "&#347;wi&#281;tny"
  ]
  node [
    id 113
    label "uroczysty"
  ]
  node [
    id 114
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 115
    label "specjalnie"
  ]
  node [
    id 116
    label "&#347;wi&#261;teczno"
  ]
  node [
    id 117
    label "specjalny"
  ]
  node [
    id 118
    label "obrz&#281;dowo"
  ]
  node [
    id 119
    label "tradycyjny"
  ]
  node [
    id 120
    label "powierzchowny"
  ]
  node [
    id 121
    label "niezwyczajny"
  ]
  node [
    id 122
    label "powa&#380;ny"
  ]
  node [
    id 123
    label "formalny"
  ]
  node [
    id 124
    label "podnios&#322;y"
  ]
  node [
    id 125
    label "uroczy&#347;cie"
  ]
  node [
    id 126
    label "wyj&#261;tkowo"
  ]
  node [
    id 127
    label "inny"
  ]
  node [
    id 128
    label "&#347;wi&#281;ty"
  ]
  node [
    id 129
    label "presentation"
  ]
  node [
    id 130
    label "dar"
  ]
  node [
    id 131
    label "dyspozycja"
  ]
  node [
    id 132
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 133
    label "da&#324;"
  ]
  node [
    id 134
    label "faculty"
  ]
  node [
    id 135
    label "stygmat"
  ]
  node [
    id 136
    label "dobro"
  ]
  node [
    id 137
    label "rzecz"
  ]
  node [
    id 138
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 139
    label "lo&#380;a"
  ]
  node [
    id 140
    label "szyderca"
  ]
  node [
    id 141
    label "White"
  ]
  node [
    id 142
    label "spos&#243;b"
  ]
  node [
    id 143
    label "nagroda"
  ]
  node [
    id 144
    label "1"
  ]
  node [
    id 145
    label "na"
  ]
  node [
    id 146
    label "wygrana"
  ]
  node [
    id 147
    label "2"
  ]
  node [
    id 148
    label "Black"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 141
  ]
  edge [
    source 139
    target 142
  ]
  edge [
    source 139
    target 145
  ]
  edge [
    source 139
    target 146
  ]
  edge [
    source 139
    target 148
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 142
  ]
  edge [
    source 140
    target 145
  ]
  edge [
    source 140
    target 146
  ]
  edge [
    source 140
    target 148
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 145
  ]
  edge [
    source 141
    target 146
  ]
  edge [
    source 142
    target 145
  ]
  edge [
    source 142
    target 146
  ]
  edge [
    source 142
    target 148
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 147
  ]
  edge [
    source 145
    target 146
  ]
]
