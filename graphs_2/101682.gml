graph [
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "aleksander"
    origin "text"
  ]
  node [
    id 2
    label "marek"
    origin "text"
  ]
  node [
    id 3
    label "skorupa"
    origin "text"
  ]
  node [
    id 4
    label "ablegat"
  ]
  node [
    id 5
    label "izba_ni&#380;sza"
  ]
  node [
    id 6
    label "Korwin"
  ]
  node [
    id 7
    label "dyscyplina_partyjna"
  ]
  node [
    id 8
    label "Miko&#322;ajczyk"
  ]
  node [
    id 9
    label "kurier_dyplomatyczny"
  ]
  node [
    id 10
    label "wys&#322;annik"
  ]
  node [
    id 11
    label "poselstwo"
  ]
  node [
    id 12
    label "parlamentarzysta"
  ]
  node [
    id 13
    label "przedstawiciel"
  ]
  node [
    id 14
    label "dyplomata"
  ]
  node [
    id 15
    label "klubista"
  ]
  node [
    id 16
    label "reprezentacja"
  ]
  node [
    id 17
    label "klub"
  ]
  node [
    id 18
    label "cz&#322;onek"
  ]
  node [
    id 19
    label "mandatariusz"
  ]
  node [
    id 20
    label "grupa_bilateralna"
  ]
  node [
    id 21
    label "polityk"
  ]
  node [
    id 22
    label "parlament"
  ]
  node [
    id 23
    label "mi&#322;y"
  ]
  node [
    id 24
    label "cz&#322;owiek"
  ]
  node [
    id 25
    label "korpus_dyplomatyczny"
  ]
  node [
    id 26
    label "dyplomatyczny"
  ]
  node [
    id 27
    label "takt"
  ]
  node [
    id 28
    label "Metternich"
  ]
  node [
    id 29
    label "dostojnik"
  ]
  node [
    id 30
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 31
    label "przyk&#322;ad"
  ]
  node [
    id 32
    label "substytuowa&#263;"
  ]
  node [
    id 33
    label "substytuowanie"
  ]
  node [
    id 34
    label "zast&#281;pca"
  ]
  node [
    id 35
    label "hygrofit"
  ]
  node [
    id 36
    label "bylina"
  ]
  node [
    id 37
    label "selerowate"
  ]
  node [
    id 38
    label "higrofil"
  ]
  node [
    id 39
    label "ro&#347;lina"
  ]
  node [
    id 40
    label "ludowy"
  ]
  node [
    id 41
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 42
    label "utw&#243;r_epicki"
  ]
  node [
    id 43
    label "pie&#347;&#324;"
  ]
  node [
    id 44
    label "selerowce"
  ]
  node [
    id 45
    label "naczynie"
  ]
  node [
    id 46
    label "kawa&#322;ek"
  ]
  node [
    id 47
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 48
    label "hull"
  ]
  node [
    id 49
    label "j&#261;dro_soczewkowate"
  ]
  node [
    id 50
    label "warstwa"
  ]
  node [
    id 51
    label "pr&#261;&#380;kowie"
  ]
  node [
    id 52
    label "scale"
  ]
  node [
    id 53
    label "&#347;mie&#263;"
  ]
  node [
    id 54
    label "wstydliwo&#347;&#263;"
  ]
  node [
    id 55
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 56
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 57
    label "vessel"
  ]
  node [
    id 58
    label "sprz&#281;t"
  ]
  node [
    id 59
    label "statki"
  ]
  node [
    id 60
    label "rewaskularyzacja"
  ]
  node [
    id 61
    label "ceramika"
  ]
  node [
    id 62
    label "drewno"
  ]
  node [
    id 63
    label "przew&#243;d"
  ]
  node [
    id 64
    label "unaczyni&#263;"
  ]
  node [
    id 65
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 66
    label "receptacle"
  ]
  node [
    id 67
    label "continence"
  ]
  node [
    id 68
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 69
    label "cecha"
  ]
  node [
    id 70
    label "kawa&#322;"
  ]
  node [
    id 71
    label "plot"
  ]
  node [
    id 72
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 73
    label "utw&#243;r"
  ]
  node [
    id 74
    label "piece"
  ]
  node [
    id 75
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 76
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 77
    label "podp&#322;ywanie"
  ]
  node [
    id 78
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 79
    label "p&#322;aszczyzna"
  ]
  node [
    id 80
    label "przek&#322;adaniec"
  ]
  node [
    id 81
    label "zbi&#243;r"
  ]
  node [
    id 82
    label "covering"
  ]
  node [
    id 83
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 84
    label "podwarstwa"
  ]
  node [
    id 85
    label "abashment"
  ]
  node [
    id 86
    label "nie&#347;mia&#322;o&#347;&#263;"
  ]
  node [
    id 87
    label "k&#322;opotliwo&#347;&#263;"
  ]
  node [
    id 88
    label "pancerz"
  ]
  node [
    id 89
    label "nieczysto&#347;ci"
  ]
  node [
    id 90
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 91
    label "istota_&#380;ywa"
  ]
  node [
    id 92
    label "venture"
  ]
  node [
    id 93
    label "odpad"
  ]
  node [
    id 94
    label "garbage"
  ]
  node [
    id 95
    label "sozolog"
  ]
  node [
    id 96
    label "cia&#322;o_pr&#261;&#380;kowane"
  ]
  node [
    id 97
    label "j&#261;dro_ogoniaste"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
]
