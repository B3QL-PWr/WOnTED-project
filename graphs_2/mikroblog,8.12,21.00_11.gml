graph [
  node [
    id 0
    label "nostalgia"
    origin "text"
  ]
  node [
    id 1
    label "dopa&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "sklep"
    origin "text"
  ]
  node [
    id 3
    label "musie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "smutek"
  ]
  node [
    id 6
    label "homesickness"
  ]
  node [
    id 7
    label "wspominki"
  ]
  node [
    id 8
    label "t&#281;sknota"
  ]
  node [
    id 9
    label "&#380;al"
  ]
  node [
    id 10
    label "t&#281;skno&#347;&#263;"
  ]
  node [
    id 11
    label "pragnienie"
  ]
  node [
    id 12
    label "t&#281;sknica"
  ]
  node [
    id 13
    label "ut&#281;sknienie"
  ]
  node [
    id 14
    label "desire"
  ]
  node [
    id 15
    label "sm&#281;tek"
  ]
  node [
    id 16
    label "emocja"
  ]
  node [
    id 17
    label "przep&#322;akiwanie"
  ]
  node [
    id 18
    label "przep&#322;akanie"
  ]
  node [
    id 19
    label "przep&#322;akiwa&#263;"
  ]
  node [
    id 20
    label "nastr&#243;j"
  ]
  node [
    id 21
    label "przep&#322;aka&#263;"
  ]
  node [
    id 22
    label "pami&#281;tnik"
  ]
  node [
    id 23
    label "dorwa&#263;"
  ]
  node [
    id 24
    label "fall"
  ]
  node [
    id 25
    label "chwyci&#263;"
  ]
  node [
    id 26
    label "zaatakowa&#263;"
  ]
  node [
    id 27
    label "dotrze&#263;"
  ]
  node [
    id 28
    label "dobra&#263;_si&#281;"
  ]
  node [
    id 29
    label "rzuci&#263;_si&#281;"
  ]
  node [
    id 30
    label "zerwa&#263;"
  ]
  node [
    id 31
    label "porwa&#263;"
  ]
  node [
    id 32
    label "znale&#378;&#263;"
  ]
  node [
    id 33
    label "utrze&#263;"
  ]
  node [
    id 34
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "silnik"
  ]
  node [
    id 36
    label "catch"
  ]
  node [
    id 37
    label "dopasowa&#263;"
  ]
  node [
    id 38
    label "advance"
  ]
  node [
    id 39
    label "get"
  ]
  node [
    id 40
    label "spowodowa&#263;"
  ]
  node [
    id 41
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 42
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 43
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 44
    label "dorobi&#263;"
  ]
  node [
    id 45
    label "become"
  ]
  node [
    id 46
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 47
    label "attack"
  ]
  node [
    id 48
    label "zrozumie&#263;"
  ]
  node [
    id 49
    label "wzi&#261;&#263;"
  ]
  node [
    id 50
    label "fascinate"
  ]
  node [
    id 51
    label "notice"
  ]
  node [
    id 52
    label "ogarn&#261;&#263;"
  ]
  node [
    id 53
    label "nast&#261;pi&#263;"
  ]
  node [
    id 54
    label "przeby&#263;"
  ]
  node [
    id 55
    label "spell"
  ]
  node [
    id 56
    label "postara&#263;_si&#281;"
  ]
  node [
    id 57
    label "rozegra&#263;"
  ]
  node [
    id 58
    label "zrobi&#263;"
  ]
  node [
    id 59
    label "powiedzie&#263;"
  ]
  node [
    id 60
    label "anoint"
  ]
  node [
    id 61
    label "sport"
  ]
  node [
    id 62
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 63
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 64
    label "skrytykowa&#263;"
  ]
  node [
    id 65
    label "p&#243;&#322;ka"
  ]
  node [
    id 66
    label "firma"
  ]
  node [
    id 67
    label "stoisko"
  ]
  node [
    id 68
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 69
    label "sk&#322;ad"
  ]
  node [
    id 70
    label "obiekt_handlowy"
  ]
  node [
    id 71
    label "zaplecze"
  ]
  node [
    id 72
    label "witryna"
  ]
  node [
    id 73
    label "Apeks"
  ]
  node [
    id 74
    label "zasoby"
  ]
  node [
    id 75
    label "cz&#322;owiek"
  ]
  node [
    id 76
    label "miejsce_pracy"
  ]
  node [
    id 77
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 78
    label "zaufanie"
  ]
  node [
    id 79
    label "Hortex"
  ]
  node [
    id 80
    label "reengineering"
  ]
  node [
    id 81
    label "nazwa_w&#322;asna"
  ]
  node [
    id 82
    label "podmiot_gospodarczy"
  ]
  node [
    id 83
    label "paczkarnia"
  ]
  node [
    id 84
    label "Orlen"
  ]
  node [
    id 85
    label "interes"
  ]
  node [
    id 86
    label "Google"
  ]
  node [
    id 87
    label "Canon"
  ]
  node [
    id 88
    label "Pewex"
  ]
  node [
    id 89
    label "MAN_SE"
  ]
  node [
    id 90
    label "Spo&#322;em"
  ]
  node [
    id 91
    label "klasa"
  ]
  node [
    id 92
    label "networking"
  ]
  node [
    id 93
    label "MAC"
  ]
  node [
    id 94
    label "zasoby_ludzkie"
  ]
  node [
    id 95
    label "Baltona"
  ]
  node [
    id 96
    label "Orbis"
  ]
  node [
    id 97
    label "biurowiec"
  ]
  node [
    id 98
    label "HP"
  ]
  node [
    id 99
    label "siedziba"
  ]
  node [
    id 100
    label "stela&#380;"
  ]
  node [
    id 101
    label "szafa"
  ]
  node [
    id 102
    label "mebel"
  ]
  node [
    id 103
    label "meblo&#347;cianka"
  ]
  node [
    id 104
    label "infrastruktura"
  ]
  node [
    id 105
    label "wyposa&#380;enie"
  ]
  node [
    id 106
    label "pomieszczenie"
  ]
  node [
    id 107
    label "szyba"
  ]
  node [
    id 108
    label "okno"
  ]
  node [
    id 109
    label "YouTube"
  ]
  node [
    id 110
    label "wytw&#243;r"
  ]
  node [
    id 111
    label "gablota"
  ]
  node [
    id 112
    label "strona"
  ]
  node [
    id 113
    label "zesp&#243;&#322;"
  ]
  node [
    id 114
    label "blokada"
  ]
  node [
    id 115
    label "hurtownia"
  ]
  node [
    id 116
    label "struktura"
  ]
  node [
    id 117
    label "pole"
  ]
  node [
    id 118
    label "pas"
  ]
  node [
    id 119
    label "miejsce"
  ]
  node [
    id 120
    label "basic"
  ]
  node [
    id 121
    label "sk&#322;adnik"
  ]
  node [
    id 122
    label "obr&#243;bka"
  ]
  node [
    id 123
    label "constitution"
  ]
  node [
    id 124
    label "fabryka"
  ]
  node [
    id 125
    label "&#347;wiat&#322;o"
  ]
  node [
    id 126
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 127
    label "syf"
  ]
  node [
    id 128
    label "rank_and_file"
  ]
  node [
    id 129
    label "set"
  ]
  node [
    id 130
    label "tabulacja"
  ]
  node [
    id 131
    label "tekst"
  ]
  node [
    id 132
    label "pozyska&#263;"
  ]
  node [
    id 133
    label "ustawi&#263;"
  ]
  node [
    id 134
    label "uwierzy&#263;"
  ]
  node [
    id 135
    label "zagra&#263;"
  ]
  node [
    id 136
    label "beget"
  ]
  node [
    id 137
    label "przyj&#261;&#263;"
  ]
  node [
    id 138
    label "uzna&#263;"
  ]
  node [
    id 139
    label "play"
  ]
  node [
    id 140
    label "zabrzmie&#263;"
  ]
  node [
    id 141
    label "leave"
  ]
  node [
    id 142
    label "instrument_muzyczny"
  ]
  node [
    id 143
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 144
    label "flare"
  ]
  node [
    id 145
    label "zaszczeka&#263;"
  ]
  node [
    id 146
    label "sound"
  ]
  node [
    id 147
    label "represent"
  ]
  node [
    id 148
    label "wykorzysta&#263;"
  ]
  node [
    id 149
    label "zatokowa&#263;"
  ]
  node [
    id 150
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 151
    label "uda&#263;_si&#281;"
  ]
  node [
    id 152
    label "zacz&#261;&#263;"
  ]
  node [
    id 153
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 154
    label "wykona&#263;"
  ]
  node [
    id 155
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 156
    label "typify"
  ]
  node [
    id 157
    label "rola"
  ]
  node [
    id 158
    label "poprawi&#263;"
  ]
  node [
    id 159
    label "nada&#263;"
  ]
  node [
    id 160
    label "peddle"
  ]
  node [
    id 161
    label "marshal"
  ]
  node [
    id 162
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 163
    label "wyznaczy&#263;"
  ]
  node [
    id 164
    label "stanowisko"
  ]
  node [
    id 165
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 166
    label "zabezpieczy&#263;"
  ]
  node [
    id 167
    label "umie&#347;ci&#263;"
  ]
  node [
    id 168
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 169
    label "zinterpretowa&#263;"
  ]
  node [
    id 170
    label "wskaza&#263;"
  ]
  node [
    id 171
    label "przyzna&#263;"
  ]
  node [
    id 172
    label "sk&#322;oni&#263;"
  ]
  node [
    id 173
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 174
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 175
    label "zdecydowa&#263;"
  ]
  node [
    id 176
    label "accommodate"
  ]
  node [
    id 177
    label "ustali&#263;"
  ]
  node [
    id 178
    label "situate"
  ]
  node [
    id 179
    label "odziedziczy&#263;"
  ]
  node [
    id 180
    label "ruszy&#263;"
  ]
  node [
    id 181
    label "take"
  ]
  node [
    id 182
    label "skorzysta&#263;"
  ]
  node [
    id 183
    label "uciec"
  ]
  node [
    id 184
    label "receive"
  ]
  node [
    id 185
    label "nakaza&#263;"
  ]
  node [
    id 186
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 187
    label "obskoczy&#263;"
  ]
  node [
    id 188
    label "bra&#263;"
  ]
  node [
    id 189
    label "u&#380;y&#263;"
  ]
  node [
    id 190
    label "wyrucha&#263;"
  ]
  node [
    id 191
    label "World_Health_Organization"
  ]
  node [
    id 192
    label "wyciupcia&#263;"
  ]
  node [
    id 193
    label "wygra&#263;"
  ]
  node [
    id 194
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 195
    label "withdraw"
  ]
  node [
    id 196
    label "wzi&#281;cie"
  ]
  node [
    id 197
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 198
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 199
    label "poczyta&#263;"
  ]
  node [
    id 200
    label "obj&#261;&#263;"
  ]
  node [
    id 201
    label "seize"
  ]
  node [
    id 202
    label "aim"
  ]
  node [
    id 203
    label "pokona&#263;"
  ]
  node [
    id 204
    label "arise"
  ]
  node [
    id 205
    label "otrzyma&#263;"
  ]
  node [
    id 206
    label "wej&#347;&#263;"
  ]
  node [
    id 207
    label "poruszy&#263;"
  ]
  node [
    id 208
    label "dosta&#263;"
  ]
  node [
    id 209
    label "stage"
  ]
  node [
    id 210
    label "uzyska&#263;"
  ]
  node [
    id 211
    label "wytworzy&#263;"
  ]
  node [
    id 212
    label "give_birth"
  ]
  node [
    id 213
    label "oceni&#263;"
  ]
  node [
    id 214
    label "stwierdzi&#263;"
  ]
  node [
    id 215
    label "assent"
  ]
  node [
    id 216
    label "rede"
  ]
  node [
    id 217
    label "see"
  ]
  node [
    id 218
    label "przybra&#263;"
  ]
  node [
    id 219
    label "strike"
  ]
  node [
    id 220
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 221
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 222
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 223
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 224
    label "obra&#263;"
  ]
  node [
    id 225
    label "draw"
  ]
  node [
    id 226
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 227
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 228
    label "przyj&#281;cie"
  ]
  node [
    id 229
    label "swallow"
  ]
  node [
    id 230
    label "odebra&#263;"
  ]
  node [
    id 231
    label "dostarczy&#263;"
  ]
  node [
    id 232
    label "absorb"
  ]
  node [
    id 233
    label "undertake"
  ]
  node [
    id 234
    label "trust"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
]
