graph [
  node [
    id 0
    label "czas"
    origin "text"
  ]
  node [
    id 1
    label "pierwsza"
    origin "text"
  ]
  node [
    id 2
    label "wojna"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 4
    label "imperium"
    origin "text"
  ]
  node [
    id 5
    label "osma&#324;ski"
    origin "text"
  ]
  node [
    id 6
    label "pope&#322;ni&#263;"
    origin "text"
  ]
  node [
    id 7
    label "straszliwy"
    origin "text"
  ]
  node [
    id 8
    label "zbrodnia"
    origin "text"
  ]
  node [
    id 9
    label "poprzedzanie"
  ]
  node [
    id 10
    label "czasoprzestrze&#324;"
  ]
  node [
    id 11
    label "laba"
  ]
  node [
    id 12
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 13
    label "chronometria"
  ]
  node [
    id 14
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 15
    label "rachuba_czasu"
  ]
  node [
    id 16
    label "przep&#322;ywanie"
  ]
  node [
    id 17
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 18
    label "czasokres"
  ]
  node [
    id 19
    label "odczyt"
  ]
  node [
    id 20
    label "chwila"
  ]
  node [
    id 21
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 22
    label "dzieje"
  ]
  node [
    id 23
    label "kategoria_gramatyczna"
  ]
  node [
    id 24
    label "poprzedzenie"
  ]
  node [
    id 25
    label "trawienie"
  ]
  node [
    id 26
    label "pochodzi&#263;"
  ]
  node [
    id 27
    label "period"
  ]
  node [
    id 28
    label "okres_czasu"
  ]
  node [
    id 29
    label "poprzedza&#263;"
  ]
  node [
    id 30
    label "schy&#322;ek"
  ]
  node [
    id 31
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 32
    label "odwlekanie_si&#281;"
  ]
  node [
    id 33
    label "zegar"
  ]
  node [
    id 34
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 35
    label "czwarty_wymiar"
  ]
  node [
    id 36
    label "pochodzenie"
  ]
  node [
    id 37
    label "koniugacja"
  ]
  node [
    id 38
    label "Zeitgeist"
  ]
  node [
    id 39
    label "trawi&#263;"
  ]
  node [
    id 40
    label "pogoda"
  ]
  node [
    id 41
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 42
    label "poprzedzi&#263;"
  ]
  node [
    id 43
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 44
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 45
    label "time_period"
  ]
  node [
    id 46
    label "time"
  ]
  node [
    id 47
    label "blok"
  ]
  node [
    id 48
    label "handout"
  ]
  node [
    id 49
    label "pomiar"
  ]
  node [
    id 50
    label "lecture"
  ]
  node [
    id 51
    label "reading"
  ]
  node [
    id 52
    label "podawanie"
  ]
  node [
    id 53
    label "wyk&#322;ad"
  ]
  node [
    id 54
    label "potrzyma&#263;"
  ]
  node [
    id 55
    label "warunki"
  ]
  node [
    id 56
    label "pok&#243;j"
  ]
  node [
    id 57
    label "atak"
  ]
  node [
    id 58
    label "program"
  ]
  node [
    id 59
    label "zjawisko"
  ]
  node [
    id 60
    label "meteorology"
  ]
  node [
    id 61
    label "weather"
  ]
  node [
    id 62
    label "prognoza_meteorologiczna"
  ]
  node [
    id 63
    label "czas_wolny"
  ]
  node [
    id 64
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 65
    label "metrologia"
  ]
  node [
    id 66
    label "godzinnik"
  ]
  node [
    id 67
    label "bicie"
  ]
  node [
    id 68
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 69
    label "wahad&#322;o"
  ]
  node [
    id 70
    label "kurant"
  ]
  node [
    id 71
    label "cyferblat"
  ]
  node [
    id 72
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 73
    label "nabicie"
  ]
  node [
    id 74
    label "werk"
  ]
  node [
    id 75
    label "czasomierz"
  ]
  node [
    id 76
    label "tyka&#263;"
  ]
  node [
    id 77
    label "tykn&#261;&#263;"
  ]
  node [
    id 78
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 79
    label "urz&#261;dzenie"
  ]
  node [
    id 80
    label "kotwica"
  ]
  node [
    id 81
    label "fleksja"
  ]
  node [
    id 82
    label "liczba"
  ]
  node [
    id 83
    label "coupling"
  ]
  node [
    id 84
    label "osoba"
  ]
  node [
    id 85
    label "tryb"
  ]
  node [
    id 86
    label "czasownik"
  ]
  node [
    id 87
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 88
    label "orz&#281;sek"
  ]
  node [
    id 89
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 90
    label "zaczynanie_si&#281;"
  ]
  node [
    id 91
    label "str&#243;j"
  ]
  node [
    id 92
    label "wynikanie"
  ]
  node [
    id 93
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 94
    label "origin"
  ]
  node [
    id 95
    label "background"
  ]
  node [
    id 96
    label "geneza"
  ]
  node [
    id 97
    label "beginning"
  ]
  node [
    id 98
    label "digestion"
  ]
  node [
    id 99
    label "unicestwianie"
  ]
  node [
    id 100
    label "sp&#281;dzanie"
  ]
  node [
    id 101
    label "contemplation"
  ]
  node [
    id 102
    label "rozk&#322;adanie"
  ]
  node [
    id 103
    label "marnowanie"
  ]
  node [
    id 104
    label "proces_fizjologiczny"
  ]
  node [
    id 105
    label "przetrawianie"
  ]
  node [
    id 106
    label "perystaltyka"
  ]
  node [
    id 107
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 108
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 109
    label "przebywa&#263;"
  ]
  node [
    id 110
    label "pour"
  ]
  node [
    id 111
    label "carry"
  ]
  node [
    id 112
    label "sail"
  ]
  node [
    id 113
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 114
    label "go&#347;ci&#263;"
  ]
  node [
    id 115
    label "mija&#263;"
  ]
  node [
    id 116
    label "proceed"
  ]
  node [
    id 117
    label "odej&#347;cie"
  ]
  node [
    id 118
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 119
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 120
    label "zanikni&#281;cie"
  ]
  node [
    id 121
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 122
    label "ciecz"
  ]
  node [
    id 123
    label "opuszczenie"
  ]
  node [
    id 124
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 125
    label "departure"
  ]
  node [
    id 126
    label "oddalenie_si&#281;"
  ]
  node [
    id 127
    label "przeby&#263;"
  ]
  node [
    id 128
    label "min&#261;&#263;"
  ]
  node [
    id 129
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 130
    label "swimming"
  ]
  node [
    id 131
    label "zago&#347;ci&#263;"
  ]
  node [
    id 132
    label "cross"
  ]
  node [
    id 133
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 134
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 135
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 136
    label "zrobi&#263;"
  ]
  node [
    id 137
    label "opatrzy&#263;"
  ]
  node [
    id 138
    label "overwhelm"
  ]
  node [
    id 139
    label "opatrywa&#263;"
  ]
  node [
    id 140
    label "date"
  ]
  node [
    id 141
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 142
    label "wynika&#263;"
  ]
  node [
    id 143
    label "fall"
  ]
  node [
    id 144
    label "poby&#263;"
  ]
  node [
    id 145
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 146
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 147
    label "bolt"
  ]
  node [
    id 148
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 149
    label "spowodowa&#263;"
  ]
  node [
    id 150
    label "uda&#263;_si&#281;"
  ]
  node [
    id 151
    label "opatrzenie"
  ]
  node [
    id 152
    label "zdarzenie_si&#281;"
  ]
  node [
    id 153
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 154
    label "progress"
  ]
  node [
    id 155
    label "opatrywanie"
  ]
  node [
    id 156
    label "mini&#281;cie"
  ]
  node [
    id 157
    label "doznanie"
  ]
  node [
    id 158
    label "zaistnienie"
  ]
  node [
    id 159
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 160
    label "przebycie"
  ]
  node [
    id 161
    label "cruise"
  ]
  node [
    id 162
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 163
    label "usuwa&#263;"
  ]
  node [
    id 164
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 165
    label "lutowa&#263;"
  ]
  node [
    id 166
    label "marnowa&#263;"
  ]
  node [
    id 167
    label "przetrawia&#263;"
  ]
  node [
    id 168
    label "poch&#322;ania&#263;"
  ]
  node [
    id 169
    label "digest"
  ]
  node [
    id 170
    label "metal"
  ]
  node [
    id 171
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 172
    label "sp&#281;dza&#263;"
  ]
  node [
    id 173
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 174
    label "zjawianie_si&#281;"
  ]
  node [
    id 175
    label "przebywanie"
  ]
  node [
    id 176
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 177
    label "mijanie"
  ]
  node [
    id 178
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 179
    label "zaznawanie"
  ]
  node [
    id 180
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 181
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 182
    label "flux"
  ]
  node [
    id 183
    label "epoka"
  ]
  node [
    id 184
    label "charakter"
  ]
  node [
    id 185
    label "flow"
  ]
  node [
    id 186
    label "choroba_przyrodzona"
  ]
  node [
    id 187
    label "ciota"
  ]
  node [
    id 188
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 189
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 190
    label "kres"
  ]
  node [
    id 191
    label "przestrze&#324;"
  ]
  node [
    id 192
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 193
    label "godzina"
  ]
  node [
    id 194
    label "doba"
  ]
  node [
    id 195
    label "p&#243;&#322;godzina"
  ]
  node [
    id 196
    label "jednostka_czasu"
  ]
  node [
    id 197
    label "minuta"
  ]
  node [
    id 198
    label "kwadrans"
  ]
  node [
    id 199
    label "war"
  ]
  node [
    id 200
    label "walka"
  ]
  node [
    id 201
    label "angaria"
  ]
  node [
    id 202
    label "zimna_wojna"
  ]
  node [
    id 203
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 204
    label "konflikt"
  ]
  node [
    id 205
    label "sp&#243;r"
  ]
  node [
    id 206
    label "wojna_stuletnia"
  ]
  node [
    id 207
    label "wr&#243;g"
  ]
  node [
    id 208
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 209
    label "gra_w_karty"
  ]
  node [
    id 210
    label "burza"
  ]
  node [
    id 211
    label "zbrodnia_wojenna"
  ]
  node [
    id 212
    label "clash"
  ]
  node [
    id 213
    label "wsp&#243;r"
  ]
  node [
    id 214
    label "obrona"
  ]
  node [
    id 215
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 216
    label "wydarzenie"
  ]
  node [
    id 217
    label "zaatakowanie"
  ]
  node [
    id 218
    label "konfrontacyjny"
  ]
  node [
    id 219
    label "contest"
  ]
  node [
    id 220
    label "action"
  ]
  node [
    id 221
    label "sambo"
  ]
  node [
    id 222
    label "czyn"
  ]
  node [
    id 223
    label "rywalizacja"
  ]
  node [
    id 224
    label "trudno&#347;&#263;"
  ]
  node [
    id 225
    label "wrestle"
  ]
  node [
    id 226
    label "military_action"
  ]
  node [
    id 227
    label "przeciwnik"
  ]
  node [
    id 228
    label "czynnik"
  ]
  node [
    id 229
    label "posta&#263;"
  ]
  node [
    id 230
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 231
    label "prawo"
  ]
  node [
    id 232
    label "grzmienie"
  ]
  node [
    id 233
    label "pogrzmot"
  ]
  node [
    id 234
    label "nieporz&#261;dek"
  ]
  node [
    id 235
    label "rioting"
  ]
  node [
    id 236
    label "scene"
  ]
  node [
    id 237
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 238
    label "zagrzmie&#263;"
  ]
  node [
    id 239
    label "mn&#243;stwo"
  ]
  node [
    id 240
    label "grzmie&#263;"
  ]
  node [
    id 241
    label "burza_piaskowa"
  ]
  node [
    id 242
    label "deszcz"
  ]
  node [
    id 243
    label "piorun"
  ]
  node [
    id 244
    label "zaj&#347;cie"
  ]
  node [
    id 245
    label "chmura"
  ]
  node [
    id 246
    label "nawa&#322;"
  ]
  node [
    id 247
    label "zagrzmienie"
  ]
  node [
    id 248
    label "fire"
  ]
  node [
    id 249
    label "wrz&#261;tek"
  ]
  node [
    id 250
    label "ciep&#322;o"
  ]
  node [
    id 251
    label "gor&#261;co"
  ]
  node [
    id 252
    label "&#347;wiatowo"
  ]
  node [
    id 253
    label "kulturalny"
  ]
  node [
    id 254
    label "generalny"
  ]
  node [
    id 255
    label "og&#243;lnie"
  ]
  node [
    id 256
    label "zwierzchni"
  ]
  node [
    id 257
    label "porz&#261;dny"
  ]
  node [
    id 258
    label "nadrz&#281;dny"
  ]
  node [
    id 259
    label "podstawowy"
  ]
  node [
    id 260
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 261
    label "zasadniczy"
  ]
  node [
    id 262
    label "generalnie"
  ]
  node [
    id 263
    label "wykszta&#322;cony"
  ]
  node [
    id 264
    label "stosowny"
  ]
  node [
    id 265
    label "elegancki"
  ]
  node [
    id 266
    label "kulturalnie"
  ]
  node [
    id 267
    label "dobrze_wychowany"
  ]
  node [
    id 268
    label "kulturny"
  ]
  node [
    id 269
    label "internationally"
  ]
  node [
    id 270
    label "pot&#281;ga"
  ]
  node [
    id 271
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 272
    label "pa&#324;stwo"
  ]
  node [
    id 273
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 274
    label "Katar"
  ]
  node [
    id 275
    label "Libia"
  ]
  node [
    id 276
    label "Gwatemala"
  ]
  node [
    id 277
    label "Ekwador"
  ]
  node [
    id 278
    label "Afganistan"
  ]
  node [
    id 279
    label "Tad&#380;ykistan"
  ]
  node [
    id 280
    label "Bhutan"
  ]
  node [
    id 281
    label "Argentyna"
  ]
  node [
    id 282
    label "D&#380;ibuti"
  ]
  node [
    id 283
    label "Wenezuela"
  ]
  node [
    id 284
    label "Gabon"
  ]
  node [
    id 285
    label "Ukraina"
  ]
  node [
    id 286
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 287
    label "Rwanda"
  ]
  node [
    id 288
    label "Liechtenstein"
  ]
  node [
    id 289
    label "organizacja"
  ]
  node [
    id 290
    label "Sri_Lanka"
  ]
  node [
    id 291
    label "Madagaskar"
  ]
  node [
    id 292
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 293
    label "Kongo"
  ]
  node [
    id 294
    label "Tonga"
  ]
  node [
    id 295
    label "Bangladesz"
  ]
  node [
    id 296
    label "Kanada"
  ]
  node [
    id 297
    label "Wehrlen"
  ]
  node [
    id 298
    label "Algieria"
  ]
  node [
    id 299
    label "Uganda"
  ]
  node [
    id 300
    label "Surinam"
  ]
  node [
    id 301
    label "Sahara_Zachodnia"
  ]
  node [
    id 302
    label "Chile"
  ]
  node [
    id 303
    label "W&#281;gry"
  ]
  node [
    id 304
    label "Birma"
  ]
  node [
    id 305
    label "Kazachstan"
  ]
  node [
    id 306
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 307
    label "Armenia"
  ]
  node [
    id 308
    label "Tuwalu"
  ]
  node [
    id 309
    label "Timor_Wschodni"
  ]
  node [
    id 310
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 311
    label "Izrael"
  ]
  node [
    id 312
    label "Estonia"
  ]
  node [
    id 313
    label "Komory"
  ]
  node [
    id 314
    label "Kamerun"
  ]
  node [
    id 315
    label "Haiti"
  ]
  node [
    id 316
    label "Belize"
  ]
  node [
    id 317
    label "Sierra_Leone"
  ]
  node [
    id 318
    label "Luksemburg"
  ]
  node [
    id 319
    label "USA"
  ]
  node [
    id 320
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 321
    label "Barbados"
  ]
  node [
    id 322
    label "San_Marino"
  ]
  node [
    id 323
    label "Bu&#322;garia"
  ]
  node [
    id 324
    label "Indonezja"
  ]
  node [
    id 325
    label "Wietnam"
  ]
  node [
    id 326
    label "Malawi"
  ]
  node [
    id 327
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 328
    label "Francja"
  ]
  node [
    id 329
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 330
    label "partia"
  ]
  node [
    id 331
    label "Zambia"
  ]
  node [
    id 332
    label "Angola"
  ]
  node [
    id 333
    label "Grenada"
  ]
  node [
    id 334
    label "Nepal"
  ]
  node [
    id 335
    label "Panama"
  ]
  node [
    id 336
    label "Rumunia"
  ]
  node [
    id 337
    label "Czarnog&#243;ra"
  ]
  node [
    id 338
    label "Malediwy"
  ]
  node [
    id 339
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 340
    label "S&#322;owacja"
  ]
  node [
    id 341
    label "para"
  ]
  node [
    id 342
    label "Egipt"
  ]
  node [
    id 343
    label "zwrot"
  ]
  node [
    id 344
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 345
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 346
    label "Mozambik"
  ]
  node [
    id 347
    label "Kolumbia"
  ]
  node [
    id 348
    label "Laos"
  ]
  node [
    id 349
    label "Burundi"
  ]
  node [
    id 350
    label "Suazi"
  ]
  node [
    id 351
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 352
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 353
    label "Czechy"
  ]
  node [
    id 354
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 355
    label "Wyspy_Marshalla"
  ]
  node [
    id 356
    label "Dominika"
  ]
  node [
    id 357
    label "Trynidad_i_Tobago"
  ]
  node [
    id 358
    label "Syria"
  ]
  node [
    id 359
    label "Palau"
  ]
  node [
    id 360
    label "Gwinea_Bissau"
  ]
  node [
    id 361
    label "Liberia"
  ]
  node [
    id 362
    label "Jamajka"
  ]
  node [
    id 363
    label "Zimbabwe"
  ]
  node [
    id 364
    label "Polska"
  ]
  node [
    id 365
    label "Dominikana"
  ]
  node [
    id 366
    label "Senegal"
  ]
  node [
    id 367
    label "Togo"
  ]
  node [
    id 368
    label "Gujana"
  ]
  node [
    id 369
    label "Gruzja"
  ]
  node [
    id 370
    label "Albania"
  ]
  node [
    id 371
    label "Zair"
  ]
  node [
    id 372
    label "Meksyk"
  ]
  node [
    id 373
    label "Macedonia"
  ]
  node [
    id 374
    label "Chorwacja"
  ]
  node [
    id 375
    label "Kambod&#380;a"
  ]
  node [
    id 376
    label "Monako"
  ]
  node [
    id 377
    label "Mauritius"
  ]
  node [
    id 378
    label "Gwinea"
  ]
  node [
    id 379
    label "Mali"
  ]
  node [
    id 380
    label "Nigeria"
  ]
  node [
    id 381
    label "Kostaryka"
  ]
  node [
    id 382
    label "Hanower"
  ]
  node [
    id 383
    label "Paragwaj"
  ]
  node [
    id 384
    label "W&#322;ochy"
  ]
  node [
    id 385
    label "Seszele"
  ]
  node [
    id 386
    label "Wyspy_Salomona"
  ]
  node [
    id 387
    label "Hiszpania"
  ]
  node [
    id 388
    label "Boliwia"
  ]
  node [
    id 389
    label "Kirgistan"
  ]
  node [
    id 390
    label "Irlandia"
  ]
  node [
    id 391
    label "Czad"
  ]
  node [
    id 392
    label "Irak"
  ]
  node [
    id 393
    label "Lesoto"
  ]
  node [
    id 394
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 395
    label "Malta"
  ]
  node [
    id 396
    label "Andora"
  ]
  node [
    id 397
    label "Chiny"
  ]
  node [
    id 398
    label "Filipiny"
  ]
  node [
    id 399
    label "Antarktis"
  ]
  node [
    id 400
    label "Niemcy"
  ]
  node [
    id 401
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 402
    label "Pakistan"
  ]
  node [
    id 403
    label "terytorium"
  ]
  node [
    id 404
    label "Nikaragua"
  ]
  node [
    id 405
    label "Brazylia"
  ]
  node [
    id 406
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 407
    label "Maroko"
  ]
  node [
    id 408
    label "Portugalia"
  ]
  node [
    id 409
    label "Niger"
  ]
  node [
    id 410
    label "Kenia"
  ]
  node [
    id 411
    label "Botswana"
  ]
  node [
    id 412
    label "Fid&#380;i"
  ]
  node [
    id 413
    label "Tunezja"
  ]
  node [
    id 414
    label "Australia"
  ]
  node [
    id 415
    label "Tajlandia"
  ]
  node [
    id 416
    label "Burkina_Faso"
  ]
  node [
    id 417
    label "interior"
  ]
  node [
    id 418
    label "Tanzania"
  ]
  node [
    id 419
    label "Benin"
  ]
  node [
    id 420
    label "Indie"
  ]
  node [
    id 421
    label "&#321;otwa"
  ]
  node [
    id 422
    label "Kiribati"
  ]
  node [
    id 423
    label "Antigua_i_Barbuda"
  ]
  node [
    id 424
    label "Rodezja"
  ]
  node [
    id 425
    label "Cypr"
  ]
  node [
    id 426
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 427
    label "Peru"
  ]
  node [
    id 428
    label "Austria"
  ]
  node [
    id 429
    label "Urugwaj"
  ]
  node [
    id 430
    label "Jordania"
  ]
  node [
    id 431
    label "Grecja"
  ]
  node [
    id 432
    label "Azerbejd&#380;an"
  ]
  node [
    id 433
    label "Turcja"
  ]
  node [
    id 434
    label "Samoa"
  ]
  node [
    id 435
    label "Sudan"
  ]
  node [
    id 436
    label "Oman"
  ]
  node [
    id 437
    label "ziemia"
  ]
  node [
    id 438
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 439
    label "Uzbekistan"
  ]
  node [
    id 440
    label "Portoryko"
  ]
  node [
    id 441
    label "Honduras"
  ]
  node [
    id 442
    label "Mongolia"
  ]
  node [
    id 443
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 444
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 445
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 446
    label "Serbia"
  ]
  node [
    id 447
    label "Tajwan"
  ]
  node [
    id 448
    label "Wielka_Brytania"
  ]
  node [
    id 449
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 450
    label "Liban"
  ]
  node [
    id 451
    label "Japonia"
  ]
  node [
    id 452
    label "Ghana"
  ]
  node [
    id 453
    label "Belgia"
  ]
  node [
    id 454
    label "Bahrajn"
  ]
  node [
    id 455
    label "Mikronezja"
  ]
  node [
    id 456
    label "Etiopia"
  ]
  node [
    id 457
    label "Kuwejt"
  ]
  node [
    id 458
    label "grupa"
  ]
  node [
    id 459
    label "Bahamy"
  ]
  node [
    id 460
    label "Rosja"
  ]
  node [
    id 461
    label "Mo&#322;dawia"
  ]
  node [
    id 462
    label "Litwa"
  ]
  node [
    id 463
    label "S&#322;owenia"
  ]
  node [
    id 464
    label "Szwajcaria"
  ]
  node [
    id 465
    label "Erytrea"
  ]
  node [
    id 466
    label "Arabia_Saudyjska"
  ]
  node [
    id 467
    label "Kuba"
  ]
  node [
    id 468
    label "granica_pa&#324;stwa"
  ]
  node [
    id 469
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 470
    label "Malezja"
  ]
  node [
    id 471
    label "Korea"
  ]
  node [
    id 472
    label "Jemen"
  ]
  node [
    id 473
    label "Nowa_Zelandia"
  ]
  node [
    id 474
    label "Namibia"
  ]
  node [
    id 475
    label "Nauru"
  ]
  node [
    id 476
    label "holoarktyka"
  ]
  node [
    id 477
    label "Brunei"
  ]
  node [
    id 478
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 479
    label "Khitai"
  ]
  node [
    id 480
    label "Mauretania"
  ]
  node [
    id 481
    label "Iran"
  ]
  node [
    id 482
    label "Gambia"
  ]
  node [
    id 483
    label "Somalia"
  ]
  node [
    id 484
    label "Holandia"
  ]
  node [
    id 485
    label "Turkmenistan"
  ]
  node [
    id 486
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 487
    label "Salwador"
  ]
  node [
    id 488
    label "wojsko"
  ]
  node [
    id 489
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 490
    label "violence"
  ]
  node [
    id 491
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 492
    label "zdolno&#347;&#263;"
  ]
  node [
    id 493
    label "potencja"
  ]
  node [
    id 494
    label "iloczyn"
  ]
  node [
    id 495
    label "stworzy&#263;"
  ]
  node [
    id 496
    label "post&#261;pi&#263;"
  ]
  node [
    id 497
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 498
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 499
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 500
    label "zorganizowa&#263;"
  ]
  node [
    id 501
    label "appoint"
  ]
  node [
    id 502
    label "wystylizowa&#263;"
  ]
  node [
    id 503
    label "cause"
  ]
  node [
    id 504
    label "przerobi&#263;"
  ]
  node [
    id 505
    label "nabra&#263;"
  ]
  node [
    id 506
    label "make"
  ]
  node [
    id 507
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 508
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 509
    label "wydali&#263;"
  ]
  node [
    id 510
    label "create"
  ]
  node [
    id 511
    label "specjalista_od_public_relations"
  ]
  node [
    id 512
    label "wizerunek"
  ]
  node [
    id 513
    label "przygotowa&#263;"
  ]
  node [
    id 514
    label "straszny"
  ]
  node [
    id 515
    label "straszliwie"
  ]
  node [
    id 516
    label "olbrzymi"
  ]
  node [
    id 517
    label "kurewski"
  ]
  node [
    id 518
    label "strasznie"
  ]
  node [
    id 519
    label "jak_cholera"
  ]
  node [
    id 520
    label "fearsomely"
  ]
  node [
    id 521
    label "direfully"
  ]
  node [
    id 522
    label "kurewsko"
  ]
  node [
    id 523
    label "frighteningly"
  ]
  node [
    id 524
    label "ogromnie"
  ]
  node [
    id 525
    label "fearfully"
  ]
  node [
    id 526
    label "dreadfully"
  ]
  node [
    id 527
    label "okropno"
  ]
  node [
    id 528
    label "jebitny"
  ]
  node [
    id 529
    label "olbrzymio"
  ]
  node [
    id 530
    label "niegrzeczny"
  ]
  node [
    id 531
    label "niemoralny"
  ]
  node [
    id 532
    label "wulgarny"
  ]
  node [
    id 533
    label "zdzirowaty"
  ]
  node [
    id 534
    label "przekl&#281;ty"
  ]
  node [
    id 535
    label "niesamowity"
  ]
  node [
    id 536
    label "crime"
  ]
  node [
    id 537
    label "post&#281;pek"
  ]
  node [
    id 538
    label "przest&#281;pstwo"
  ]
  node [
    id 539
    label "brudny"
  ]
  node [
    id 540
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 541
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 542
    label "sprawstwo"
  ]
  node [
    id 543
    label "funkcja"
  ]
  node [
    id 544
    label "act"
  ]
  node [
    id 545
    label "rzecz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
]
