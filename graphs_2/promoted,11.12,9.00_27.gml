graph [
  node [
    id 0
    label "rozstrzygni&#281;cie"
    origin "text"
  ]
  node [
    id 1
    label "plebiscyt"
    origin "text"
  ]
  node [
    id 2
    label "m&#322;odzie&#380;owy"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "xdd"
    origin "text"
  ]
  node [
    id 6
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 7
    label "decyzja"
  ]
  node [
    id 8
    label "oddzia&#322;anie"
  ]
  node [
    id 9
    label "resoluteness"
  ]
  node [
    id 10
    label "rezultat"
  ]
  node [
    id 11
    label "zdecydowanie"
  ]
  node [
    id 12
    label "adjudication"
  ]
  node [
    id 13
    label "pewnie"
  ]
  node [
    id 14
    label "zdecydowany"
  ]
  node [
    id 15
    label "zauwa&#380;alnie"
  ]
  node [
    id 16
    label "podj&#281;cie"
  ]
  node [
    id 17
    label "cecha"
  ]
  node [
    id 18
    label "judgment"
  ]
  node [
    id 19
    label "zrobienie"
  ]
  node [
    id 20
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 21
    label "management"
  ]
  node [
    id 22
    label "resolution"
  ]
  node [
    id 23
    label "wytw&#243;r"
  ]
  node [
    id 24
    label "dokument"
  ]
  node [
    id 25
    label "dzia&#322;anie"
  ]
  node [
    id 26
    label "typ"
  ]
  node [
    id 27
    label "event"
  ]
  node [
    id 28
    label "przyczyna"
  ]
  node [
    id 29
    label "reply"
  ]
  node [
    id 30
    label "zahipnotyzowanie"
  ]
  node [
    id 31
    label "spowodowanie"
  ]
  node [
    id 32
    label "zdarzenie_si&#281;"
  ]
  node [
    id 33
    label "chemia"
  ]
  node [
    id 34
    label "wdarcie_si&#281;"
  ]
  node [
    id 35
    label "dotarcie"
  ]
  node [
    id 36
    label "reakcja_chemiczna"
  ]
  node [
    id 37
    label "czynno&#347;&#263;"
  ]
  node [
    id 38
    label "konkurs"
  ]
  node [
    id 39
    label "g&#322;osowanie"
  ]
  node [
    id 40
    label "reasumowa&#263;"
  ]
  node [
    id 41
    label "przeg&#322;osowanie"
  ]
  node [
    id 42
    label "powodowanie"
  ]
  node [
    id 43
    label "reasumowanie"
  ]
  node [
    id 44
    label "akcja"
  ]
  node [
    id 45
    label "wybieranie"
  ]
  node [
    id 46
    label "poll"
  ]
  node [
    id 47
    label "vote"
  ]
  node [
    id 48
    label "decydowanie"
  ]
  node [
    id 49
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 50
    label "przeg&#322;osowywanie"
  ]
  node [
    id 51
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 52
    label "wi&#281;kszo&#347;&#263;_bezwzgl&#281;dna"
  ]
  node [
    id 53
    label "wybranie"
  ]
  node [
    id 54
    label "casting"
  ]
  node [
    id 55
    label "nab&#243;r"
  ]
  node [
    id 56
    label "Eurowizja"
  ]
  node [
    id 57
    label "eliminacje"
  ]
  node [
    id 58
    label "impreza"
  ]
  node [
    id 59
    label "emulation"
  ]
  node [
    id 60
    label "Interwizja"
  ]
  node [
    id 61
    label "m&#322;odzie&#380;owo"
  ]
  node [
    id 62
    label "obietnica"
  ]
  node [
    id 63
    label "wordnet"
  ]
  node [
    id 64
    label "jednostka_informacji"
  ]
  node [
    id 65
    label "wypowiedzenie"
  ]
  node [
    id 66
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 67
    label "morfem"
  ]
  node [
    id 68
    label "s&#322;ownictwo"
  ]
  node [
    id 69
    label "wykrzyknik"
  ]
  node [
    id 70
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 71
    label "pole_semantyczne"
  ]
  node [
    id 72
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 73
    label "pisanie_si&#281;"
  ]
  node [
    id 74
    label "komunikat"
  ]
  node [
    id 75
    label "nag&#322;os"
  ]
  node [
    id 76
    label "wyg&#322;os"
  ]
  node [
    id 77
    label "jednostka_leksykalna"
  ]
  node [
    id 78
    label "bit"
  ]
  node [
    id 79
    label "czasownik"
  ]
  node [
    id 80
    label "communication"
  ]
  node [
    id 81
    label "kreacjonista"
  ]
  node [
    id 82
    label "roi&#263;_si&#281;"
  ]
  node [
    id 83
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 84
    label "zapowied&#378;"
  ]
  node [
    id 85
    label "statement"
  ]
  node [
    id 86
    label "zapewnienie"
  ]
  node [
    id 87
    label "konwersja"
  ]
  node [
    id 88
    label "notice"
  ]
  node [
    id 89
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 90
    label "przepowiedzenie"
  ]
  node [
    id 91
    label "rozwi&#261;zanie"
  ]
  node [
    id 92
    label "generowa&#263;"
  ]
  node [
    id 93
    label "wydanie"
  ]
  node [
    id 94
    label "message"
  ]
  node [
    id 95
    label "generowanie"
  ]
  node [
    id 96
    label "wydobycie"
  ]
  node [
    id 97
    label "zwerbalizowanie"
  ]
  node [
    id 98
    label "szyk"
  ]
  node [
    id 99
    label "notification"
  ]
  node [
    id 100
    label "powiedzenie"
  ]
  node [
    id 101
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 102
    label "denunciation"
  ]
  node [
    id 103
    label "wyra&#380;enie"
  ]
  node [
    id 104
    label "j&#281;zyk"
  ]
  node [
    id 105
    label "terminology"
  ]
  node [
    id 106
    label "termin"
  ]
  node [
    id 107
    label "pocz&#261;tek"
  ]
  node [
    id 108
    label "leksem"
  ]
  node [
    id 109
    label "koniec"
  ]
  node [
    id 110
    label "&#347;rodek"
  ]
  node [
    id 111
    label "morpheme"
  ]
  node [
    id 112
    label "forma"
  ]
  node [
    id 113
    label "bajt"
  ]
  node [
    id 114
    label "s&#322;owo_maszynowe"
  ]
  node [
    id 115
    label "oktet"
  ]
  node [
    id 116
    label "p&#243;&#322;bajt"
  ]
  node [
    id 117
    label "cyfra"
  ]
  node [
    id 118
    label "system_dw&#243;jkowy"
  ]
  node [
    id 119
    label "rytm"
  ]
  node [
    id 120
    label "baza_danych"
  ]
  node [
    id 121
    label "S&#322;owosie&#263;"
  ]
  node [
    id 122
    label "WordNet"
  ]
  node [
    id 123
    label "exclamation_mark"
  ]
  node [
    id 124
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 125
    label "znak_interpunkcyjny"
  ]
  node [
    id 126
    label "rozmiar"
  ]
  node [
    id 127
    label "liczba"
  ]
  node [
    id 128
    label "circumference"
  ]
  node [
    id 129
    label "cyrkumferencja"
  ]
  node [
    id 130
    label "miejsce"
  ]
  node [
    id 131
    label "strona"
  ]
  node [
    id 132
    label "nieprzechodnio&#347;&#263;"
  ]
  node [
    id 133
    label "przechodnio&#347;&#263;"
  ]
  node [
    id 134
    label "koniugacja"
  ]
  node [
    id 135
    label "p&#243;&#322;rocze"
  ]
  node [
    id 136
    label "martwy_sezon"
  ]
  node [
    id 137
    label "kalendarz"
  ]
  node [
    id 138
    label "cykl_astronomiczny"
  ]
  node [
    id 139
    label "lata"
  ]
  node [
    id 140
    label "pora_roku"
  ]
  node [
    id 141
    label "stulecie"
  ]
  node [
    id 142
    label "kurs"
  ]
  node [
    id 143
    label "czas"
  ]
  node [
    id 144
    label "jubileusz"
  ]
  node [
    id 145
    label "grupa"
  ]
  node [
    id 146
    label "kwarta&#322;"
  ]
  node [
    id 147
    label "miesi&#261;c"
  ]
  node [
    id 148
    label "summer"
  ]
  node [
    id 149
    label "odm&#322;adzanie"
  ]
  node [
    id 150
    label "liga"
  ]
  node [
    id 151
    label "jednostka_systematyczna"
  ]
  node [
    id 152
    label "asymilowanie"
  ]
  node [
    id 153
    label "gromada"
  ]
  node [
    id 154
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 155
    label "asymilowa&#263;"
  ]
  node [
    id 156
    label "egzemplarz"
  ]
  node [
    id 157
    label "Entuzjastki"
  ]
  node [
    id 158
    label "zbi&#243;r"
  ]
  node [
    id 159
    label "kompozycja"
  ]
  node [
    id 160
    label "Terranie"
  ]
  node [
    id 161
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 162
    label "category"
  ]
  node [
    id 163
    label "pakiet_klimatyczny"
  ]
  node [
    id 164
    label "oddzia&#322;"
  ]
  node [
    id 165
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 166
    label "cz&#261;steczka"
  ]
  node [
    id 167
    label "stage_set"
  ]
  node [
    id 168
    label "type"
  ]
  node [
    id 169
    label "specgrupa"
  ]
  node [
    id 170
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 171
    label "&#346;wietliki"
  ]
  node [
    id 172
    label "odm&#322;odzenie"
  ]
  node [
    id 173
    label "Eurogrupa"
  ]
  node [
    id 174
    label "odm&#322;adza&#263;"
  ]
  node [
    id 175
    label "formacja_geologiczna"
  ]
  node [
    id 176
    label "harcerze_starsi"
  ]
  node [
    id 177
    label "poprzedzanie"
  ]
  node [
    id 178
    label "czasoprzestrze&#324;"
  ]
  node [
    id 179
    label "laba"
  ]
  node [
    id 180
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 181
    label "chronometria"
  ]
  node [
    id 182
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 183
    label "rachuba_czasu"
  ]
  node [
    id 184
    label "przep&#322;ywanie"
  ]
  node [
    id 185
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 186
    label "czasokres"
  ]
  node [
    id 187
    label "odczyt"
  ]
  node [
    id 188
    label "chwila"
  ]
  node [
    id 189
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 190
    label "dzieje"
  ]
  node [
    id 191
    label "kategoria_gramatyczna"
  ]
  node [
    id 192
    label "poprzedzenie"
  ]
  node [
    id 193
    label "trawienie"
  ]
  node [
    id 194
    label "pochodzi&#263;"
  ]
  node [
    id 195
    label "period"
  ]
  node [
    id 196
    label "okres_czasu"
  ]
  node [
    id 197
    label "poprzedza&#263;"
  ]
  node [
    id 198
    label "schy&#322;ek"
  ]
  node [
    id 199
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 200
    label "odwlekanie_si&#281;"
  ]
  node [
    id 201
    label "zegar"
  ]
  node [
    id 202
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 203
    label "czwarty_wymiar"
  ]
  node [
    id 204
    label "pochodzenie"
  ]
  node [
    id 205
    label "Zeitgeist"
  ]
  node [
    id 206
    label "trawi&#263;"
  ]
  node [
    id 207
    label "pogoda"
  ]
  node [
    id 208
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 209
    label "poprzedzi&#263;"
  ]
  node [
    id 210
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 211
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 212
    label "time_period"
  ]
  node [
    id 213
    label "tydzie&#324;"
  ]
  node [
    id 214
    label "miech"
  ]
  node [
    id 215
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 216
    label "kalendy"
  ]
  node [
    id 217
    label "term"
  ]
  node [
    id 218
    label "rok_akademicki"
  ]
  node [
    id 219
    label "rok_szkolny"
  ]
  node [
    id 220
    label "semester"
  ]
  node [
    id 221
    label "anniwersarz"
  ]
  node [
    id 222
    label "rocznica"
  ]
  node [
    id 223
    label "obszar"
  ]
  node [
    id 224
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 225
    label "long_time"
  ]
  node [
    id 226
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 227
    label "almanac"
  ]
  node [
    id 228
    label "rozk&#322;ad"
  ]
  node [
    id 229
    label "wydawnictwo"
  ]
  node [
    id 230
    label "Juliusz_Cezar"
  ]
  node [
    id 231
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 232
    label "zwy&#380;kowanie"
  ]
  node [
    id 233
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 234
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 235
    label "zaj&#281;cia"
  ]
  node [
    id 236
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 237
    label "trasa"
  ]
  node [
    id 238
    label "przeorientowywanie"
  ]
  node [
    id 239
    label "przejazd"
  ]
  node [
    id 240
    label "kierunek"
  ]
  node [
    id 241
    label "przeorientowywa&#263;"
  ]
  node [
    id 242
    label "nauka"
  ]
  node [
    id 243
    label "przeorientowanie"
  ]
  node [
    id 244
    label "klasa"
  ]
  node [
    id 245
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 246
    label "przeorientowa&#263;"
  ]
  node [
    id 247
    label "manner"
  ]
  node [
    id 248
    label "course"
  ]
  node [
    id 249
    label "passage"
  ]
  node [
    id 250
    label "zni&#380;kowanie"
  ]
  node [
    id 251
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 252
    label "seria"
  ]
  node [
    id 253
    label "stawka"
  ]
  node [
    id 254
    label "way"
  ]
  node [
    id 255
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 256
    label "spos&#243;b"
  ]
  node [
    id 257
    label "deprecjacja"
  ]
  node [
    id 258
    label "cedu&#322;a"
  ]
  node [
    id 259
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 260
    label "drive"
  ]
  node [
    id 261
    label "bearing"
  ]
  node [
    id 262
    label "Lira"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
]
