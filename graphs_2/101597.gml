graph [
  node [
    id 0
    label "istotnie"
    origin "text"
  ]
  node [
    id 1
    label "winny"
    origin "text"
  ]
  node [
    id 2
    label "kulka"
    origin "text"
  ]
  node [
    id 3
    label "&#322;eba"
    origin "text"
  ]
  node [
    id 4
    label "istotny"
  ]
  node [
    id 5
    label "realnie"
  ]
  node [
    id 6
    label "importantly"
  ]
  node [
    id 7
    label "wa&#380;ny"
  ]
  node [
    id 8
    label "wynios&#322;y"
  ]
  node [
    id 9
    label "dono&#347;ny"
  ]
  node [
    id 10
    label "silny"
  ]
  node [
    id 11
    label "wa&#380;nie"
  ]
  node [
    id 12
    label "znaczny"
  ]
  node [
    id 13
    label "eksponowany"
  ]
  node [
    id 14
    label "dobry"
  ]
  node [
    id 15
    label "realny"
  ]
  node [
    id 16
    label "du&#380;y"
  ]
  node [
    id 17
    label "naprawd&#281;"
  ]
  node [
    id 18
    label "podobnie"
  ]
  node [
    id 19
    label "mo&#380;liwie"
  ]
  node [
    id 20
    label "rzeczywisty"
  ]
  node [
    id 21
    label "prawdziwie"
  ]
  node [
    id 22
    label "alkoholowy"
  ]
  node [
    id 23
    label "sprawca"
  ]
  node [
    id 24
    label "przewinienie"
  ]
  node [
    id 25
    label "odpowiadanie"
  ]
  node [
    id 26
    label "cierpki"
  ]
  node [
    id 27
    label "reagowanie"
  ]
  node [
    id 28
    label "dawanie"
  ]
  node [
    id 29
    label "powodowanie"
  ]
  node [
    id 30
    label "bycie"
  ]
  node [
    id 31
    label "pokutowanie"
  ]
  node [
    id 32
    label "pytanie"
  ]
  node [
    id 33
    label "odpowiedzialny"
  ]
  node [
    id 34
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 35
    label "picie_piwa"
  ]
  node [
    id 36
    label "odpowiedni"
  ]
  node [
    id 37
    label "parry"
  ]
  node [
    id 38
    label "fit"
  ]
  node [
    id 39
    label "dzianie_si&#281;"
  ]
  node [
    id 40
    label "rendition"
  ]
  node [
    id 41
    label "ponoszenie"
  ]
  node [
    id 42
    label "rozmawianie"
  ]
  node [
    id 43
    label "czyn"
  ]
  node [
    id 44
    label "pope&#322;nienie"
  ]
  node [
    id 45
    label "discourtesy"
  ]
  node [
    id 46
    label "alkoholowo"
  ]
  node [
    id 47
    label "spirytualny"
  ]
  node [
    id 48
    label "cierpko"
  ]
  node [
    id 49
    label "osch&#322;y"
  ]
  node [
    id 50
    label "kwaskowy"
  ]
  node [
    id 51
    label "gorzki"
  ]
  node [
    id 52
    label "gorzkawy"
  ]
  node [
    id 53
    label "kwa&#347;ny"
  ]
  node [
    id 54
    label "sprawiciel"
  ]
  node [
    id 55
    label "cz&#322;owiek"
  ]
  node [
    id 56
    label "ball"
  ]
  node [
    id 57
    label "kula"
  ]
  node [
    id 58
    label "czasza"
  ]
  node [
    id 59
    label "marmurki"
  ]
  node [
    id 60
    label "sphere"
  ]
  node [
    id 61
    label "bry&#322;a"
  ]
  node [
    id 62
    label "pocisk"
  ]
  node [
    id 63
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 64
    label "porcja"
  ]
  node [
    id 65
    label "zas&#243;b"
  ]
  node [
    id 66
    label "ilo&#347;&#263;"
  ]
  node [
    id 67
    label "&#380;o&#322;d"
  ]
  node [
    id 68
    label "kszta&#322;t"
  ]
  node [
    id 69
    label "figura_geometryczna"
  ]
  node [
    id 70
    label "bezkszta&#322;tno&#347;&#263;"
  ]
  node [
    id 71
    label "kawa&#322;ek"
  ]
  node [
    id 72
    label "block"
  ]
  node [
    id 73
    label "solid"
  ]
  node [
    id 74
    label "&#322;uska"
  ]
  node [
    id 75
    label "przedmiot"
  ]
  node [
    id 76
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 77
    label "figura_heraldyczna"
  ]
  node [
    id 78
    label "podpora"
  ]
  node [
    id 79
    label "pile"
  ]
  node [
    id 80
    label "musket_ball"
  ]
  node [
    id 81
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 82
    label "p&#243;&#322;sfera"
  ]
  node [
    id 83
    label "sfera"
  ]
  node [
    id 84
    label "warstwa_kulista"
  ]
  node [
    id 85
    label "wycinek_kuli"
  ]
  node [
    id 86
    label "amunicja"
  ]
  node [
    id 87
    label "ta&#347;ma"
  ]
  node [
    id 88
    label "o&#322;&#243;w"
  ]
  node [
    id 89
    label "kartuza"
  ]
  node [
    id 90
    label "p&#243;&#322;kula"
  ]
  node [
    id 91
    label "nab&#243;j"
  ]
  node [
    id 92
    label "komora_nabojowa"
  ]
  node [
    id 93
    label "akwarium"
  ]
  node [
    id 94
    label "naczynie"
  ]
  node [
    id 95
    label "bowl"
  ]
  node [
    id 96
    label "kielich"
  ]
  node [
    id 97
    label "element"
  ]
  node [
    id 98
    label "miska"
  ]
  node [
    id 99
    label "spadochron"
  ]
  node [
    id 100
    label "wzniesienie"
  ]
  node [
    id 101
    label "stadium"
  ]
  node [
    id 102
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 103
    label "czara"
  ]
  node [
    id 104
    label "g&#322;owica"
  ]
  node [
    id 105
    label "trafienie"
  ]
  node [
    id 106
    label "trafianie"
  ]
  node [
    id 107
    label "rdze&#324;"
  ]
  node [
    id 108
    label "prochownia"
  ]
  node [
    id 109
    label "przeniesienie"
  ]
  node [
    id 110
    label "&#322;adunek_bojowy"
  ]
  node [
    id 111
    label "trafi&#263;"
  ]
  node [
    id 112
    label "przenoszenie"
  ]
  node [
    id 113
    label "przenie&#347;&#263;"
  ]
  node [
    id 114
    label "trafia&#263;"
  ]
  node [
    id 115
    label "przenosi&#263;"
  ]
  node [
    id 116
    label "bro&#324;"
  ]
  node [
    id 117
    label "zabawka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
]
