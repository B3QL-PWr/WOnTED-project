graph [
  node [
    id 0
    label "gmina"
    origin "text"
  ]
  node [
    id 1
    label "o&#322;yka"
    origin "text"
  ]
  node [
    id 2
    label "urz&#261;d"
  ]
  node [
    id 3
    label "jednostka_administracyjna"
  ]
  node [
    id 4
    label "Karlsbad"
  ]
  node [
    id 5
    label "Dobro&#324;"
  ]
  node [
    id 6
    label "rada_gminy"
  ]
  node [
    id 7
    label "Wielka_Wie&#347;"
  ]
  node [
    id 8
    label "radny"
  ]
  node [
    id 9
    label "organizacja_religijna"
  ]
  node [
    id 10
    label "Biskupice"
  ]
  node [
    id 11
    label "powiat"
  ]
  node [
    id 12
    label "siedziba"
  ]
  node [
    id 13
    label "dzia&#322;"
  ]
  node [
    id 14
    label "mianowaniec"
  ]
  node [
    id 15
    label "okienko"
  ]
  node [
    id 16
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 17
    label "position"
  ]
  node [
    id 18
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 19
    label "stanowisko"
  ]
  node [
    id 20
    label "w&#322;adza"
  ]
  node [
    id 21
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 22
    label "instytucja"
  ]
  node [
    id 23
    label "organ"
  ]
  node [
    id 24
    label "Ma&#322;opolska"
  ]
  node [
    id 25
    label "Zabrze"
  ]
  node [
    id 26
    label "Niemcy"
  ]
  node [
    id 27
    label "wojew&#243;dztwo"
  ]
  node [
    id 28
    label "samorz&#261;dowiec"
  ]
  node [
    id 29
    label "rajca"
  ]
  node [
    id 30
    label "rada"
  ]
  node [
    id 31
    label "przedstawiciel"
  ]
  node [
    id 32
    label "zwi&#261;zek"
  ]
  node [
    id 33
    label "radziecki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 32
    target 33
  ]
]
