graph [
  node [
    id 0
    label "ojciec"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "kilkukrotnie"
    origin "text"
  ]
  node [
    id 3
    label "wzywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "personel"
    origin "text"
  ]
  node [
    id 5
    label "dzwonek"
    origin "text"
  ]
  node [
    id 6
    label "kszta&#322;ciciel"
  ]
  node [
    id 7
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 8
    label "kuwada"
  ]
  node [
    id 9
    label "tworzyciel"
  ]
  node [
    id 10
    label "rodzice"
  ]
  node [
    id 11
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 12
    label "&#347;w"
  ]
  node [
    id 13
    label "pomys&#322;odawca"
  ]
  node [
    id 14
    label "rodzic"
  ]
  node [
    id 15
    label "wykonawca"
  ]
  node [
    id 16
    label "ojczym"
  ]
  node [
    id 17
    label "samiec"
  ]
  node [
    id 18
    label "przodek"
  ]
  node [
    id 19
    label "papa"
  ]
  node [
    id 20
    label "zakonnik"
  ]
  node [
    id 21
    label "stary"
  ]
  node [
    id 22
    label "br"
  ]
  node [
    id 23
    label "mnich"
  ]
  node [
    id 24
    label "zakon"
  ]
  node [
    id 25
    label "wyznawca"
  ]
  node [
    id 26
    label "zwierz&#281;"
  ]
  node [
    id 27
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 28
    label "opiekun"
  ]
  node [
    id 29
    label "wapniak"
  ]
  node [
    id 30
    label "rodzic_chrzestny"
  ]
  node [
    id 31
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 32
    label "ojcowie"
  ]
  node [
    id 33
    label "linea&#380;"
  ]
  node [
    id 34
    label "krewny"
  ]
  node [
    id 35
    label "chodnik"
  ]
  node [
    id 36
    label "w&#243;z"
  ]
  node [
    id 37
    label "p&#322;ug"
  ]
  node [
    id 38
    label "wyrobisko"
  ]
  node [
    id 39
    label "dziad"
  ]
  node [
    id 40
    label "antecesor"
  ]
  node [
    id 41
    label "post&#281;p"
  ]
  node [
    id 42
    label "inicjator"
  ]
  node [
    id 43
    label "podmiot_gospodarczy"
  ]
  node [
    id 44
    label "artysta"
  ]
  node [
    id 45
    label "cz&#322;owiek"
  ]
  node [
    id 46
    label "muzyk"
  ]
  node [
    id 47
    label "materia&#322;_budowlany"
  ]
  node [
    id 48
    label "twarz"
  ]
  node [
    id 49
    label "gun_muzzle"
  ]
  node [
    id 50
    label "izolacja"
  ]
  node [
    id 51
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 52
    label "nienowoczesny"
  ]
  node [
    id 53
    label "gruba_ryba"
  ]
  node [
    id 54
    label "zestarzenie_si&#281;"
  ]
  node [
    id 55
    label "poprzedni"
  ]
  node [
    id 56
    label "dawno"
  ]
  node [
    id 57
    label "staro"
  ]
  node [
    id 58
    label "m&#261;&#380;"
  ]
  node [
    id 59
    label "starzy"
  ]
  node [
    id 60
    label "dotychczasowy"
  ]
  node [
    id 61
    label "p&#243;&#378;ny"
  ]
  node [
    id 62
    label "d&#322;ugoletni"
  ]
  node [
    id 63
    label "charakterystyczny"
  ]
  node [
    id 64
    label "brat"
  ]
  node [
    id 65
    label "po_staro&#347;wiecku"
  ]
  node [
    id 66
    label "zwierzchnik"
  ]
  node [
    id 67
    label "znajomy"
  ]
  node [
    id 68
    label "odleg&#322;y"
  ]
  node [
    id 69
    label "starzenie_si&#281;"
  ]
  node [
    id 70
    label "starczo"
  ]
  node [
    id 71
    label "dawniej"
  ]
  node [
    id 72
    label "niegdysiejszy"
  ]
  node [
    id 73
    label "dojrza&#322;y"
  ]
  node [
    id 74
    label "nauczyciel"
  ]
  node [
    id 75
    label "autor"
  ]
  node [
    id 76
    label "doros&#322;y"
  ]
  node [
    id 77
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 78
    label "jegomo&#347;&#263;"
  ]
  node [
    id 79
    label "andropauza"
  ]
  node [
    id 80
    label "pa&#324;stwo"
  ]
  node [
    id 81
    label "bratek"
  ]
  node [
    id 82
    label "ch&#322;opina"
  ]
  node [
    id 83
    label "twardziel"
  ]
  node [
    id 84
    label "androlog"
  ]
  node [
    id 85
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 86
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 87
    label "pokolenie"
  ]
  node [
    id 88
    label "wapniaki"
  ]
  node [
    id 89
    label "syndrom_kuwady"
  ]
  node [
    id 90
    label "na&#347;ladownictwo"
  ]
  node [
    id 91
    label "zwyczaj"
  ]
  node [
    id 92
    label "ci&#261;&#380;a"
  ]
  node [
    id 93
    label "&#380;onaty"
  ]
  node [
    id 94
    label "utulenie"
  ]
  node [
    id 95
    label "pediatra"
  ]
  node [
    id 96
    label "dzieciak"
  ]
  node [
    id 97
    label "utulanie"
  ]
  node [
    id 98
    label "dzieciarnia"
  ]
  node [
    id 99
    label "niepe&#322;noletni"
  ]
  node [
    id 100
    label "organizm"
  ]
  node [
    id 101
    label "utula&#263;"
  ]
  node [
    id 102
    label "cz&#322;owieczek"
  ]
  node [
    id 103
    label "fledgling"
  ]
  node [
    id 104
    label "utuli&#263;"
  ]
  node [
    id 105
    label "m&#322;odzik"
  ]
  node [
    id 106
    label "pedofil"
  ]
  node [
    id 107
    label "m&#322;odziak"
  ]
  node [
    id 108
    label "potomek"
  ]
  node [
    id 109
    label "entliczek-pentliczek"
  ]
  node [
    id 110
    label "potomstwo"
  ]
  node [
    id 111
    label "sraluch"
  ]
  node [
    id 112
    label "zbi&#243;r"
  ]
  node [
    id 113
    label "czeladka"
  ]
  node [
    id 114
    label "dzietno&#347;&#263;"
  ]
  node [
    id 115
    label "bawienie_si&#281;"
  ]
  node [
    id 116
    label "pomiot"
  ]
  node [
    id 117
    label "grupa"
  ]
  node [
    id 118
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 119
    label "kinderbal"
  ]
  node [
    id 120
    label "ludzko&#347;&#263;"
  ]
  node [
    id 121
    label "asymilowanie"
  ]
  node [
    id 122
    label "asymilowa&#263;"
  ]
  node [
    id 123
    label "os&#322;abia&#263;"
  ]
  node [
    id 124
    label "posta&#263;"
  ]
  node [
    id 125
    label "hominid"
  ]
  node [
    id 126
    label "podw&#322;adny"
  ]
  node [
    id 127
    label "os&#322;abianie"
  ]
  node [
    id 128
    label "g&#322;owa"
  ]
  node [
    id 129
    label "figura"
  ]
  node [
    id 130
    label "portrecista"
  ]
  node [
    id 131
    label "dwun&#243;g"
  ]
  node [
    id 132
    label "profanum"
  ]
  node [
    id 133
    label "mikrokosmos"
  ]
  node [
    id 134
    label "nasada"
  ]
  node [
    id 135
    label "duch"
  ]
  node [
    id 136
    label "antropochoria"
  ]
  node [
    id 137
    label "osoba"
  ]
  node [
    id 138
    label "wz&#243;r"
  ]
  node [
    id 139
    label "senior"
  ]
  node [
    id 140
    label "oddzia&#322;ywanie"
  ]
  node [
    id 141
    label "Adam"
  ]
  node [
    id 142
    label "homo_sapiens"
  ]
  node [
    id 143
    label "polifag"
  ]
  node [
    id 144
    label "ma&#322;oletny"
  ]
  node [
    id 145
    label "m&#322;ody"
  ]
  node [
    id 146
    label "degenerat"
  ]
  node [
    id 147
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 148
    label "zwyrol"
  ]
  node [
    id 149
    label "czerniak"
  ]
  node [
    id 150
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 151
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 152
    label "paszcza"
  ]
  node [
    id 153
    label "popapraniec"
  ]
  node [
    id 154
    label "skuba&#263;"
  ]
  node [
    id 155
    label "skubanie"
  ]
  node [
    id 156
    label "skubni&#281;cie"
  ]
  node [
    id 157
    label "agresja"
  ]
  node [
    id 158
    label "zwierz&#281;ta"
  ]
  node [
    id 159
    label "fukni&#281;cie"
  ]
  node [
    id 160
    label "farba"
  ]
  node [
    id 161
    label "fukanie"
  ]
  node [
    id 162
    label "istota_&#380;ywa"
  ]
  node [
    id 163
    label "gad"
  ]
  node [
    id 164
    label "siedzie&#263;"
  ]
  node [
    id 165
    label "oswaja&#263;"
  ]
  node [
    id 166
    label "tresowa&#263;"
  ]
  node [
    id 167
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 168
    label "poligamia"
  ]
  node [
    id 169
    label "oz&#243;r"
  ]
  node [
    id 170
    label "skubn&#261;&#263;"
  ]
  node [
    id 171
    label "wios&#322;owa&#263;"
  ]
  node [
    id 172
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 173
    label "le&#380;enie"
  ]
  node [
    id 174
    label "niecz&#322;owiek"
  ]
  node [
    id 175
    label "wios&#322;owanie"
  ]
  node [
    id 176
    label "napasienie_si&#281;"
  ]
  node [
    id 177
    label "wiwarium"
  ]
  node [
    id 178
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 179
    label "animalista"
  ]
  node [
    id 180
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 181
    label "budowa"
  ]
  node [
    id 182
    label "hodowla"
  ]
  node [
    id 183
    label "pasienie_si&#281;"
  ]
  node [
    id 184
    label "sodomita"
  ]
  node [
    id 185
    label "monogamia"
  ]
  node [
    id 186
    label "przyssawka"
  ]
  node [
    id 187
    label "zachowanie"
  ]
  node [
    id 188
    label "budowa_cia&#322;a"
  ]
  node [
    id 189
    label "okrutnik"
  ]
  node [
    id 190
    label "grzbiet"
  ]
  node [
    id 191
    label "weterynarz"
  ]
  node [
    id 192
    label "&#322;eb"
  ]
  node [
    id 193
    label "wylinka"
  ]
  node [
    id 194
    label "bestia"
  ]
  node [
    id 195
    label "poskramia&#263;"
  ]
  node [
    id 196
    label "fauna"
  ]
  node [
    id 197
    label "treser"
  ]
  node [
    id 198
    label "siedzenie"
  ]
  node [
    id 199
    label "le&#380;e&#263;"
  ]
  node [
    id 200
    label "p&#322;aszczyzna"
  ]
  node [
    id 201
    label "odwadnia&#263;"
  ]
  node [
    id 202
    label "przyswoi&#263;"
  ]
  node [
    id 203
    label "sk&#243;ra"
  ]
  node [
    id 204
    label "odwodni&#263;"
  ]
  node [
    id 205
    label "ewoluowanie"
  ]
  node [
    id 206
    label "staw"
  ]
  node [
    id 207
    label "ow&#322;osienie"
  ]
  node [
    id 208
    label "unerwienie"
  ]
  node [
    id 209
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 210
    label "reakcja"
  ]
  node [
    id 211
    label "wyewoluowanie"
  ]
  node [
    id 212
    label "przyswajanie"
  ]
  node [
    id 213
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 214
    label "wyewoluowa&#263;"
  ]
  node [
    id 215
    label "miejsce"
  ]
  node [
    id 216
    label "biorytm"
  ]
  node [
    id 217
    label "ewoluowa&#263;"
  ]
  node [
    id 218
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 219
    label "otworzy&#263;"
  ]
  node [
    id 220
    label "otwiera&#263;"
  ]
  node [
    id 221
    label "czynnik_biotyczny"
  ]
  node [
    id 222
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 223
    label "otworzenie"
  ]
  node [
    id 224
    label "otwieranie"
  ]
  node [
    id 225
    label "individual"
  ]
  node [
    id 226
    label "szkielet"
  ]
  node [
    id 227
    label "ty&#322;"
  ]
  node [
    id 228
    label "obiekt"
  ]
  node [
    id 229
    label "przyswaja&#263;"
  ]
  node [
    id 230
    label "przyswojenie"
  ]
  node [
    id 231
    label "odwadnianie"
  ]
  node [
    id 232
    label "odwodnienie"
  ]
  node [
    id 233
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 234
    label "prz&#243;d"
  ]
  node [
    id 235
    label "uk&#322;ad"
  ]
  node [
    id 236
    label "temperatura"
  ]
  node [
    id 237
    label "l&#281;d&#378;wie"
  ]
  node [
    id 238
    label "cia&#322;o"
  ]
  node [
    id 239
    label "cz&#322;onek"
  ]
  node [
    id 240
    label "utulanie_si&#281;"
  ]
  node [
    id 241
    label "usypianie"
  ]
  node [
    id 242
    label "pocieszanie"
  ]
  node [
    id 243
    label "uspokajanie"
  ]
  node [
    id 244
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 245
    label "uspokoi&#263;"
  ]
  node [
    id 246
    label "uspokojenie"
  ]
  node [
    id 247
    label "utulenie_si&#281;"
  ]
  node [
    id 248
    label "u&#347;pienie"
  ]
  node [
    id 249
    label "usypia&#263;"
  ]
  node [
    id 250
    label "uspokaja&#263;"
  ]
  node [
    id 251
    label "dewiant"
  ]
  node [
    id 252
    label "specjalista"
  ]
  node [
    id 253
    label "wyliczanka"
  ]
  node [
    id 254
    label "harcerz"
  ]
  node [
    id 255
    label "ch&#322;opta&#347;"
  ]
  node [
    id 256
    label "zawodnik"
  ]
  node [
    id 257
    label "go&#322;ow&#261;s"
  ]
  node [
    id 258
    label "m&#322;ode"
  ]
  node [
    id 259
    label "stopie&#324;_harcerski"
  ]
  node [
    id 260
    label "g&#243;wniarz"
  ]
  node [
    id 261
    label "beniaminek"
  ]
  node [
    id 262
    label "istotka"
  ]
  node [
    id 263
    label "bech"
  ]
  node [
    id 264
    label "dziecinny"
  ]
  node [
    id 265
    label "naiwniak"
  ]
  node [
    id 266
    label "kilkukrotny"
  ]
  node [
    id 267
    label "kilkakro&#263;"
  ]
  node [
    id 268
    label "parokrotny"
  ]
  node [
    id 269
    label "parokrotnie"
  ]
  node [
    id 270
    label "invite"
  ]
  node [
    id 271
    label "cry"
  ]
  node [
    id 272
    label "address"
  ]
  node [
    id 273
    label "nakazywa&#263;"
  ]
  node [
    id 274
    label "donosi&#263;"
  ]
  node [
    id 275
    label "pobudza&#263;"
  ]
  node [
    id 276
    label "prosi&#263;"
  ]
  node [
    id 277
    label "order"
  ]
  node [
    id 278
    label "poleca&#263;"
  ]
  node [
    id 279
    label "trwa&#263;"
  ]
  node [
    id 280
    label "zaprasza&#263;"
  ]
  node [
    id 281
    label "zach&#281;ca&#263;"
  ]
  node [
    id 282
    label "suffice"
  ]
  node [
    id 283
    label "preach"
  ]
  node [
    id 284
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 285
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 286
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 287
    label "pies"
  ]
  node [
    id 288
    label "zezwala&#263;"
  ]
  node [
    id 289
    label "ask"
  ]
  node [
    id 290
    label "spill_the_beans"
  ]
  node [
    id 291
    label "przeby&#263;"
  ]
  node [
    id 292
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 293
    label "zanosi&#263;"
  ]
  node [
    id 294
    label "inform"
  ]
  node [
    id 295
    label "give"
  ]
  node [
    id 296
    label "zu&#380;y&#263;"
  ]
  node [
    id 297
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 298
    label "get"
  ]
  node [
    id 299
    label "introduce"
  ]
  node [
    id 300
    label "render"
  ]
  node [
    id 301
    label "informowa&#263;"
  ]
  node [
    id 302
    label "go"
  ]
  node [
    id 303
    label "nak&#322;ania&#263;"
  ]
  node [
    id 304
    label "powodowa&#263;"
  ]
  node [
    id 305
    label "boost"
  ]
  node [
    id 306
    label "wzmaga&#263;"
  ]
  node [
    id 307
    label "wymaga&#263;"
  ]
  node [
    id 308
    label "pakowa&#263;"
  ]
  node [
    id 309
    label "inflict"
  ]
  node [
    id 310
    label "command"
  ]
  node [
    id 311
    label "cover"
  ]
  node [
    id 312
    label "odznaka"
  ]
  node [
    id 313
    label "kawaler"
  ]
  node [
    id 314
    label "zesp&#243;&#322;"
  ]
  node [
    id 315
    label "persona&#322;"
  ]
  node [
    id 316
    label "force"
  ]
  node [
    id 317
    label "Mazowsze"
  ]
  node [
    id 318
    label "odm&#322;adzanie"
  ]
  node [
    id 319
    label "&#346;wietliki"
  ]
  node [
    id 320
    label "whole"
  ]
  node [
    id 321
    label "skupienie"
  ]
  node [
    id 322
    label "The_Beatles"
  ]
  node [
    id 323
    label "odm&#322;adza&#263;"
  ]
  node [
    id 324
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 325
    label "zabudowania"
  ]
  node [
    id 326
    label "group"
  ]
  node [
    id 327
    label "zespolik"
  ]
  node [
    id 328
    label "schorzenie"
  ]
  node [
    id 329
    label "ro&#347;lina"
  ]
  node [
    id 330
    label "Depeche_Mode"
  ]
  node [
    id 331
    label "batch"
  ]
  node [
    id 332
    label "odm&#322;odzenie"
  ]
  node [
    id 333
    label "zadzwoni&#263;"
  ]
  node [
    id 334
    label "&#322;&#261;cznik_instalacyjny"
  ]
  node [
    id 335
    label "ro&#347;lina_zielna"
  ]
  node [
    id 336
    label "kolor"
  ]
  node [
    id 337
    label "dzwoni&#263;"
  ]
  node [
    id 338
    label "dzwonkowate"
  ]
  node [
    id 339
    label "karo"
  ]
  node [
    id 340
    label "sygna&#322;_d&#378;wi&#281;kowy"
  ]
  node [
    id 341
    label "dzwonienie"
  ]
  node [
    id 342
    label "przycisk"
  ]
  node [
    id 343
    label "campanula"
  ]
  node [
    id 344
    label "sygnalizator"
  ]
  node [
    id 345
    label "karta"
  ]
  node [
    id 346
    label "kartka"
  ]
  node [
    id 347
    label "danie"
  ]
  node [
    id 348
    label "menu"
  ]
  node [
    id 349
    label "zezwolenie"
  ]
  node [
    id 350
    label "restauracja"
  ]
  node [
    id 351
    label "chart"
  ]
  node [
    id 352
    label "p&#322;ytka"
  ]
  node [
    id 353
    label "formularz"
  ]
  node [
    id 354
    label "ticket"
  ]
  node [
    id 355
    label "cennik"
  ]
  node [
    id 356
    label "oferta"
  ]
  node [
    id 357
    label "komputer"
  ]
  node [
    id 358
    label "charter"
  ]
  node [
    id 359
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 360
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 361
    label "kartonik"
  ]
  node [
    id 362
    label "urz&#261;dzenie"
  ]
  node [
    id 363
    label "circuit_board"
  ]
  node [
    id 364
    label "liczba_kwantowa"
  ]
  node [
    id 365
    label "&#347;wieci&#263;"
  ]
  node [
    id 366
    label "poker"
  ]
  node [
    id 367
    label "cecha"
  ]
  node [
    id 368
    label "ubarwienie"
  ]
  node [
    id 369
    label "blakn&#261;&#263;"
  ]
  node [
    id 370
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 371
    label "struktura"
  ]
  node [
    id 372
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 373
    label "zblakni&#281;cie"
  ]
  node [
    id 374
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 375
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 376
    label "prze&#322;amanie"
  ]
  node [
    id 377
    label "prze&#322;amywanie"
  ]
  node [
    id 378
    label "&#347;wiecenie"
  ]
  node [
    id 379
    label "prze&#322;ama&#263;"
  ]
  node [
    id 380
    label "zblakn&#261;&#263;"
  ]
  node [
    id 381
    label "symbol"
  ]
  node [
    id 382
    label "blakni&#281;cie"
  ]
  node [
    id 383
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 384
    label "interfejs"
  ]
  node [
    id 385
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 386
    label "pole"
  ]
  node [
    id 387
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 388
    label "nacisk"
  ]
  node [
    id 389
    label "wymowa"
  ]
  node [
    id 390
    label "d&#378;wi&#281;k"
  ]
  node [
    id 391
    label "przedmiot"
  ]
  node [
    id 392
    label "indicator"
  ]
  node [
    id 393
    label "dekolt"
  ]
  node [
    id 394
    label "dodzwanianie_si&#281;"
  ]
  node [
    id 395
    label "jingle"
  ]
  node [
    id 396
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 397
    label "wydzwanianie"
  ]
  node [
    id 398
    label "naciskanie"
  ]
  node [
    id 399
    label "sound"
  ]
  node [
    id 400
    label "telefon"
  ]
  node [
    id 401
    label "brzmienie"
  ]
  node [
    id 402
    label "wybijanie"
  ]
  node [
    id 403
    label "dryndanie"
  ]
  node [
    id 404
    label "dodzwonienie_si&#281;"
  ]
  node [
    id 405
    label "wydzwonienie"
  ]
  node [
    id 406
    label "call"
  ]
  node [
    id 407
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 408
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 409
    label "zabi&#263;"
  ]
  node [
    id 410
    label "zadrynda&#263;"
  ]
  node [
    id 411
    label "zabrzmie&#263;"
  ]
  node [
    id 412
    label "nacisn&#261;&#263;"
  ]
  node [
    id 413
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 414
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 415
    label "bi&#263;"
  ]
  node [
    id 416
    label "brzmie&#263;"
  ]
  node [
    id 417
    label "drynda&#263;"
  ]
  node [
    id 418
    label "brz&#281;cze&#263;"
  ]
  node [
    id 419
    label "astrowce"
  ]
  node [
    id 420
    label "Campanulaceae"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
]
