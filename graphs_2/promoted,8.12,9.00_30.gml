graph [
  node [
    id 0
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 1
    label "danuta"
    origin "text"
  ]
  node [
    id 2
    label "ga&#322;kowy"
    origin "text"
  ]
  node [
    id 3
    label "post_scriptum"
    origin "text"
  ]
  node [
    id 4
    label "&#322;&#261;czniczka"
    origin "text"
  ]
  node [
    id 5
    label "powstanie"
    origin "text"
  ]
  node [
    id 6
    label "warszawski"
    origin "text"
  ]
  node [
    id 7
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 8
    label "pause"
  ]
  node [
    id 9
    label "consist"
  ]
  node [
    id 10
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 11
    label "istnie&#263;"
  ]
  node [
    id 12
    label "stay"
  ]
  node [
    id 13
    label "wychodzi&#263;"
  ]
  node [
    id 14
    label "seclude"
  ]
  node [
    id 15
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 16
    label "overture"
  ]
  node [
    id 17
    label "perform"
  ]
  node [
    id 18
    label "dzia&#322;a&#263;"
  ]
  node [
    id 19
    label "odst&#281;powa&#263;"
  ]
  node [
    id 20
    label "act"
  ]
  node [
    id 21
    label "appear"
  ]
  node [
    id 22
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 23
    label "unwrap"
  ]
  node [
    id 24
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 25
    label "rezygnowa&#263;"
  ]
  node [
    id 26
    label "uczestniczy&#263;"
  ]
  node [
    id 27
    label "nak&#322;ania&#263;"
  ]
  node [
    id 28
    label "mie&#263;_miejsce"
  ]
  node [
    id 29
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 30
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 31
    label "stand"
  ]
  node [
    id 32
    label "korzenny"
  ]
  node [
    id 33
    label "korzennie"
  ]
  node [
    id 34
    label "aromatyczny"
  ]
  node [
    id 35
    label "ciep&#322;y"
  ]
  node [
    id 36
    label "kl&#281;czenie"
  ]
  node [
    id 37
    label "powstanie_listopadowe"
  ]
  node [
    id 38
    label "potworzenie_si&#281;"
  ]
  node [
    id 39
    label "stworzenie"
  ]
  node [
    id 40
    label "beginning"
  ]
  node [
    id 41
    label "powstanie_tambowskie"
  ]
  node [
    id 42
    label "origin"
  ]
  node [
    id 43
    label "siedzenie"
  ]
  node [
    id 44
    label "odbudowanie_si&#281;"
  ]
  node [
    id 45
    label "walka"
  ]
  node [
    id 46
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 47
    label "pierwocina"
  ]
  node [
    id 48
    label "chmielnicczyzna"
  ]
  node [
    id 49
    label "le&#380;enie"
  ]
  node [
    id 50
    label "&#380;akieria"
  ]
  node [
    id 51
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 52
    label "geneza"
  ]
  node [
    id 53
    label "utworzenie"
  ]
  node [
    id 54
    label "orgy"
  ]
  node [
    id 55
    label "zaistnienie"
  ]
  node [
    id 56
    label "powstanie_warszawskie"
  ]
  node [
    id 57
    label "koliszczyzna"
  ]
  node [
    id 58
    label "uniesienie_si&#281;"
  ]
  node [
    id 59
    label "Ko&#347;ciuszko"
  ]
  node [
    id 60
    label "czyn"
  ]
  node [
    id 61
    label "zaatakowanie"
  ]
  node [
    id 62
    label "trudno&#347;&#263;"
  ]
  node [
    id 63
    label "action"
  ]
  node [
    id 64
    label "wydarzenie"
  ]
  node [
    id 65
    label "obrona"
  ]
  node [
    id 66
    label "military_action"
  ]
  node [
    id 67
    label "contest"
  ]
  node [
    id 68
    label "rywalizacja"
  ]
  node [
    id 69
    label "sp&#243;r"
  ]
  node [
    id 70
    label "konfrontacyjny"
  ]
  node [
    id 71
    label "wrestle"
  ]
  node [
    id 72
    label "sambo"
  ]
  node [
    id 73
    label "wojna_stuletnia"
  ]
  node [
    id 74
    label "trwanie"
  ]
  node [
    id 75
    label "przele&#380;enie"
  ]
  node [
    id 76
    label "tarzanie_si&#281;"
  ]
  node [
    id 77
    label "bycie"
  ]
  node [
    id 78
    label "pobyczenie_si&#281;"
  ]
  node [
    id 79
    label "zwierz&#281;"
  ]
  node [
    id 80
    label "po&#322;o&#380;enie"
  ]
  node [
    id 81
    label "fit"
  ]
  node [
    id 82
    label "wstanie"
  ]
  node [
    id 83
    label "odpowiedni"
  ]
  node [
    id 84
    label "spoczywanie"
  ]
  node [
    id 85
    label "pole&#380;enie"
  ]
  node [
    id 86
    label "zlegni&#281;cie"
  ]
  node [
    id 87
    label "pupa"
  ]
  node [
    id 88
    label "przedzia&#322;"
  ]
  node [
    id 89
    label "position"
  ]
  node [
    id 90
    label "otarcie_si&#281;"
  ]
  node [
    id 91
    label "jadalnia"
  ]
  node [
    id 92
    label "otaczanie_si&#281;"
  ]
  node [
    id 93
    label "wysiedzenie"
  ]
  node [
    id 94
    label "otoczenie_si&#281;"
  ]
  node [
    id 95
    label "trybuna"
  ]
  node [
    id 96
    label "przedmiot"
  ]
  node [
    id 97
    label "autobus"
  ]
  node [
    id 98
    label "posadzenie"
  ]
  node [
    id 99
    label "ujmowanie"
  ]
  node [
    id 100
    label "sadzanie"
  ]
  node [
    id 101
    label "miejsce"
  ]
  node [
    id 102
    label "samolot"
  ]
  node [
    id 103
    label "wyj&#347;cie"
  ]
  node [
    id 104
    label "wysiadywanie"
  ]
  node [
    id 105
    label "przebywanie"
  ]
  node [
    id 106
    label "odsiadywanie"
  ]
  node [
    id 107
    label "touch"
  ]
  node [
    id 108
    label "ocieranie_si&#281;"
  ]
  node [
    id 109
    label "tkwienie"
  ]
  node [
    id 110
    label "residency"
  ]
  node [
    id 111
    label "mieszkanie"
  ]
  node [
    id 112
    label "room"
  ]
  node [
    id 113
    label "odsiedzenie"
  ]
  node [
    id 114
    label "posiedzenie"
  ]
  node [
    id 115
    label "zajmowanie_si&#281;"
  ]
  node [
    id 116
    label "umieszczenie"
  ]
  node [
    id 117
    label "wychodzenie"
  ]
  node [
    id 118
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 119
    label "czynnik"
  ]
  node [
    id 120
    label "proces"
  ]
  node [
    id 121
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 122
    label "przyczyna"
  ]
  node [
    id 123
    label "pocz&#261;tek"
  ]
  node [
    id 124
    label "give"
  ]
  node [
    id 125
    label "rodny"
  ]
  node [
    id 126
    label "monogeneza"
  ]
  node [
    id 127
    label "zesp&#243;&#322;"
  ]
  node [
    id 128
    label "zorganizowanie"
  ]
  node [
    id 129
    label "erecting"
  ]
  node [
    id 130
    label "ukszta&#322;towanie"
  ]
  node [
    id 131
    label "stanie_si&#281;"
  ]
  node [
    id 132
    label "zrobienie"
  ]
  node [
    id 133
    label "foundation"
  ]
  node [
    id 134
    label "potworzenie"
  ]
  node [
    id 135
    label "apparition"
  ]
  node [
    id 136
    label "nap&#281;dzenie"
  ]
  node [
    id 137
    label "wizerunek"
  ]
  node [
    id 138
    label "organizm"
  ]
  node [
    id 139
    label "ekosystem"
  ]
  node [
    id 140
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 141
    label "wszechstworzenie"
  ]
  node [
    id 142
    label "fauna"
  ]
  node [
    id 143
    label "work"
  ]
  node [
    id 144
    label "stw&#243;r"
  ]
  node [
    id 145
    label "czynno&#347;&#263;"
  ]
  node [
    id 146
    label "istota"
  ]
  node [
    id 147
    label "pope&#322;nienie"
  ]
  node [
    id 148
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 149
    label "teren"
  ]
  node [
    id 150
    label "Ziemia"
  ]
  node [
    id 151
    label "rzecz"
  ]
  node [
    id 152
    label "mikrokosmos"
  ]
  node [
    id 153
    label "woda"
  ]
  node [
    id 154
    label "biota"
  ]
  node [
    id 155
    label "environment"
  ]
  node [
    id 156
    label "obiekt_naturalny"
  ]
  node [
    id 157
    label "cia&#322;o"
  ]
  node [
    id 158
    label "debiut"
  ]
  node [
    id 159
    label "marmuzela"
  ]
  node [
    id 160
    label "po_warszawsku"
  ]
  node [
    id 161
    label "mazowiecki"
  ]
  node [
    id 162
    label "polski"
  ]
  node [
    id 163
    label "po_mazowiecku"
  ]
  node [
    id 164
    label "regionalny"
  ]
  node [
    id 165
    label "cz&#322;owiek"
  ]
  node [
    id 166
    label "istota_&#380;ywa"
  ]
  node [
    id 167
    label "kobieta"
  ]
  node [
    id 168
    label "Danuta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
]
