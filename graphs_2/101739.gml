graph [
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "art"
    origin "text"
  ]
  node [
    id 2
    label "usta"
    origin "text"
  ]
  node [
    id 3
    label "pkt"
    origin "text"
  ]
  node [
    id 4
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 5
    label "naruszenie"
    origin "text"
  ]
  node [
    id 6
    label "zakaz"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "mowa"
    origin "text"
  ]
  node [
    id 9
    label "na&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pzmot"
    origin "text"
  ]
  node [
    id 11
    label "kara"
    origin "text"
  ]
  node [
    id 12
    label "pieni&#281;&#380;ny"
    origin "text"
  ]
  node [
    id 13
    label "wysoko&#347;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 15
    label "p&#322;atny"
    origin "text"
  ]
  node [
    id 16
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 17
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 18
    label "za&#322;o&#380;enie"
  ]
  node [
    id 19
    label "strategia"
  ]
  node [
    id 20
    label "background"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "punkt_odniesienia"
  ]
  node [
    id 23
    label "zasadzenie"
  ]
  node [
    id 24
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 25
    label "&#347;ciana"
  ]
  node [
    id 26
    label "podstawowy"
  ]
  node [
    id 27
    label "dzieci&#281;ctwo"
  ]
  node [
    id 28
    label "d&#243;&#322;"
  ]
  node [
    id 29
    label "documentation"
  ]
  node [
    id 30
    label "bok"
  ]
  node [
    id 31
    label "pomys&#322;"
  ]
  node [
    id 32
    label "zasadzi&#263;"
  ]
  node [
    id 33
    label "column"
  ]
  node [
    id 34
    label "pot&#281;ga"
  ]
  node [
    id 35
    label "infliction"
  ]
  node [
    id 36
    label "spowodowanie"
  ]
  node [
    id 37
    label "proposition"
  ]
  node [
    id 38
    label "przygotowanie"
  ]
  node [
    id 39
    label "pozak&#322;adanie"
  ]
  node [
    id 40
    label "point"
  ]
  node [
    id 41
    label "program"
  ]
  node [
    id 42
    label "poubieranie"
  ]
  node [
    id 43
    label "rozebranie"
  ]
  node [
    id 44
    label "str&#243;j"
  ]
  node [
    id 45
    label "budowla"
  ]
  node [
    id 46
    label "przewidzenie"
  ]
  node [
    id 47
    label "zak&#322;adka"
  ]
  node [
    id 48
    label "czynno&#347;&#263;"
  ]
  node [
    id 49
    label "twierdzenie"
  ]
  node [
    id 50
    label "przygotowywanie"
  ]
  node [
    id 51
    label "podwini&#281;cie"
  ]
  node [
    id 52
    label "zap&#322;acenie"
  ]
  node [
    id 53
    label "wyko&#324;czenie"
  ]
  node [
    id 54
    label "struktura"
  ]
  node [
    id 55
    label "utworzenie"
  ]
  node [
    id 56
    label "przebranie"
  ]
  node [
    id 57
    label "obleczenie"
  ]
  node [
    id 58
    label "przymierzenie"
  ]
  node [
    id 59
    label "obleczenie_si&#281;"
  ]
  node [
    id 60
    label "przywdzianie"
  ]
  node [
    id 61
    label "umieszczenie"
  ]
  node [
    id 62
    label "zrobienie"
  ]
  node [
    id 63
    label "przyodzianie"
  ]
  node [
    id 64
    label "pokrycie"
  ]
  node [
    id 65
    label "odcinek"
  ]
  node [
    id 66
    label "strzelba"
  ]
  node [
    id 67
    label "wielok&#261;t"
  ]
  node [
    id 68
    label "tu&#322;&#243;w"
  ]
  node [
    id 69
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 70
    label "strona"
  ]
  node [
    id 71
    label "lufa"
  ]
  node [
    id 72
    label "kierunek"
  ]
  node [
    id 73
    label "wyrobisko"
  ]
  node [
    id 74
    label "trudno&#347;&#263;"
  ]
  node [
    id 75
    label "kres"
  ]
  node [
    id 76
    label "bariera"
  ]
  node [
    id 77
    label "przegroda"
  ]
  node [
    id 78
    label "p&#322;aszczyzna"
  ]
  node [
    id 79
    label "profil"
  ]
  node [
    id 80
    label "facebook"
  ]
  node [
    id 81
    label "zbocze"
  ]
  node [
    id 82
    label "miejsce"
  ]
  node [
    id 83
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 84
    label "obstruction"
  ]
  node [
    id 85
    label "kszta&#322;t"
  ]
  node [
    id 86
    label "pow&#322;oka"
  ]
  node [
    id 87
    label "wielo&#347;cian"
  ]
  node [
    id 88
    label "discipline"
  ]
  node [
    id 89
    label "zboczy&#263;"
  ]
  node [
    id 90
    label "w&#261;tek"
  ]
  node [
    id 91
    label "kultura"
  ]
  node [
    id 92
    label "entity"
  ]
  node [
    id 93
    label "sponiewiera&#263;"
  ]
  node [
    id 94
    label "zboczenie"
  ]
  node [
    id 95
    label "zbaczanie"
  ]
  node [
    id 96
    label "charakter"
  ]
  node [
    id 97
    label "thing"
  ]
  node [
    id 98
    label "om&#243;wi&#263;"
  ]
  node [
    id 99
    label "tre&#347;&#263;"
  ]
  node [
    id 100
    label "element"
  ]
  node [
    id 101
    label "kr&#261;&#380;enie"
  ]
  node [
    id 102
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 103
    label "istota"
  ]
  node [
    id 104
    label "zbacza&#263;"
  ]
  node [
    id 105
    label "om&#243;wienie"
  ]
  node [
    id 106
    label "rzecz"
  ]
  node [
    id 107
    label "tematyka"
  ]
  node [
    id 108
    label "omawianie"
  ]
  node [
    id 109
    label "omawia&#263;"
  ]
  node [
    id 110
    label "robienie"
  ]
  node [
    id 111
    label "program_nauczania"
  ]
  node [
    id 112
    label "sponiewieranie"
  ]
  node [
    id 113
    label "low"
  ]
  node [
    id 114
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 115
    label "d&#378;wi&#281;k"
  ]
  node [
    id 116
    label "wykopanie"
  ]
  node [
    id 117
    label "hole"
  ]
  node [
    id 118
    label "wykopywa&#263;"
  ]
  node [
    id 119
    label "za&#322;amanie"
  ]
  node [
    id 120
    label "wykopa&#263;"
  ]
  node [
    id 121
    label "&#347;piew"
  ]
  node [
    id 122
    label "wykopywanie"
  ]
  node [
    id 123
    label "depressive_disorder"
  ]
  node [
    id 124
    label "niski"
  ]
  node [
    id 125
    label "pocz&#261;tkowy"
  ]
  node [
    id 126
    label "najwa&#380;niejszy"
  ]
  node [
    id 127
    label "podstawowo"
  ]
  node [
    id 128
    label "niezaawansowany"
  ]
  node [
    id 129
    label "przetkanie"
  ]
  node [
    id 130
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 131
    label "odm&#322;odzenie"
  ]
  node [
    id 132
    label "zaczerpni&#281;cie"
  ]
  node [
    id 133
    label "anchor"
  ]
  node [
    id 134
    label "wetkni&#281;cie"
  ]
  node [
    id 135
    label "interposition"
  ]
  node [
    id 136
    label "przymocowanie"
  ]
  node [
    id 137
    label "plant"
  ]
  node [
    id 138
    label "przymocowa&#263;"
  ]
  node [
    id 139
    label "establish"
  ]
  node [
    id 140
    label "osnowa&#263;"
  ]
  node [
    id 141
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 142
    label "umie&#347;ci&#263;"
  ]
  node [
    id 143
    label "wetkn&#261;&#263;"
  ]
  node [
    id 144
    label "pocz&#261;tki"
  ]
  node [
    id 145
    label "dzieci&#324;stwo"
  ]
  node [
    id 146
    label "pochodzenie"
  ]
  node [
    id 147
    label "kontekst"
  ]
  node [
    id 148
    label "ukradzenie"
  ]
  node [
    id 149
    label "ukra&#347;&#263;"
  ]
  node [
    id 150
    label "idea"
  ]
  node [
    id 151
    label "wytw&#243;r"
  ]
  node [
    id 152
    label "system"
  ]
  node [
    id 153
    label "operacja"
  ]
  node [
    id 154
    label "doktryna"
  ]
  node [
    id 155
    label "plan"
  ]
  node [
    id 156
    label "gra"
  ]
  node [
    id 157
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 158
    label "dokument"
  ]
  node [
    id 159
    label "dziedzina"
  ]
  node [
    id 160
    label "metoda"
  ]
  node [
    id 161
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 162
    label "wrinkle"
  ]
  node [
    id 163
    label "wzorzec_projektowy"
  ]
  node [
    id 164
    label "zdolno&#347;&#263;"
  ]
  node [
    id 165
    label "iloczyn"
  ]
  node [
    id 166
    label "violence"
  ]
  node [
    id 167
    label "wojsko"
  ]
  node [
    id 168
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 169
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 170
    label "potencja"
  ]
  node [
    id 171
    label "organizacja"
  ]
  node [
    id 172
    label "ssa&#263;"
  ]
  node [
    id 173
    label "warga_dolna"
  ]
  node [
    id 174
    label "zaci&#281;cie"
  ]
  node [
    id 175
    label "zacinanie"
  ]
  node [
    id 176
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 177
    label "ryjek"
  ]
  node [
    id 178
    label "warga_g&#243;rna"
  ]
  node [
    id 179
    label "dzi&#243;b"
  ]
  node [
    id 180
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 181
    label "zacina&#263;"
  ]
  node [
    id 182
    label "jama_ustna"
  ]
  node [
    id 183
    label "jadaczka"
  ]
  node [
    id 184
    label "zaci&#261;&#263;"
  ]
  node [
    id 185
    label "organ"
  ]
  node [
    id 186
    label "ssanie"
  ]
  node [
    id 187
    label "twarz"
  ]
  node [
    id 188
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 189
    label "uk&#322;ad"
  ]
  node [
    id 190
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 191
    label "Komitet_Region&#243;w"
  ]
  node [
    id 192
    label "struktura_anatomiczna"
  ]
  node [
    id 193
    label "organogeneza"
  ]
  node [
    id 194
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 195
    label "tw&#243;r"
  ]
  node [
    id 196
    label "tkanka"
  ]
  node [
    id 197
    label "stomia"
  ]
  node [
    id 198
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 199
    label "budowa"
  ]
  node [
    id 200
    label "dekortykacja"
  ]
  node [
    id 201
    label "okolica"
  ]
  node [
    id 202
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 203
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 204
    label "Izba_Konsyliarska"
  ]
  node [
    id 205
    label "zesp&#243;&#322;"
  ]
  node [
    id 206
    label "jednostka_organizacyjna"
  ]
  node [
    id 207
    label "statek"
  ]
  node [
    id 208
    label "zako&#324;czenie"
  ]
  node [
    id 209
    label "bow"
  ]
  node [
    id 210
    label "ustnik"
  ]
  node [
    id 211
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 212
    label "blizna"
  ]
  node [
    id 213
    label "dziob&#243;wka"
  ]
  node [
    id 214
    label "grzebie&#324;"
  ]
  node [
    id 215
    label "samolot"
  ]
  node [
    id 216
    label "ptak"
  ]
  node [
    id 217
    label "ostry"
  ]
  node [
    id 218
    label "wyraz_twarzy"
  ]
  node [
    id 219
    label "p&#243;&#322;profil"
  ]
  node [
    id 220
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 221
    label "rys"
  ]
  node [
    id 222
    label "brew"
  ]
  node [
    id 223
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 224
    label "micha"
  ]
  node [
    id 225
    label "ucho"
  ]
  node [
    id 226
    label "podbr&#243;dek"
  ]
  node [
    id 227
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 228
    label "policzek"
  ]
  node [
    id 229
    label "posta&#263;"
  ]
  node [
    id 230
    label "cera"
  ]
  node [
    id 231
    label "cz&#322;owiek"
  ]
  node [
    id 232
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 233
    label "czo&#322;o"
  ]
  node [
    id 234
    label "oko"
  ]
  node [
    id 235
    label "uj&#281;cie"
  ]
  node [
    id 236
    label "maskowato&#347;&#263;"
  ]
  node [
    id 237
    label "powieka"
  ]
  node [
    id 238
    label "nos"
  ]
  node [
    id 239
    label "wielko&#347;&#263;"
  ]
  node [
    id 240
    label "prz&#243;d"
  ]
  node [
    id 241
    label "zas&#322;ona"
  ]
  node [
    id 242
    label "twarzyczka"
  ]
  node [
    id 243
    label "pysk"
  ]
  node [
    id 244
    label "reputacja"
  ]
  node [
    id 245
    label "liczko"
  ]
  node [
    id 246
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 247
    label "przedstawiciel"
  ]
  node [
    id 248
    label "p&#322;e&#263;"
  ]
  node [
    id 249
    label "mina"
  ]
  node [
    id 250
    label "podrywa&#263;"
  ]
  node [
    id 251
    label "zaciska&#263;"
  ]
  node [
    id 252
    label "struga&#263;"
  ]
  node [
    id 253
    label "w&#281;dka"
  ]
  node [
    id 254
    label "zaciera&#263;"
  ]
  node [
    id 255
    label "&#347;cina&#263;"
  ]
  node [
    id 256
    label "reduce"
  ]
  node [
    id 257
    label "lejce"
  ]
  node [
    id 258
    label "blokowa&#263;"
  ]
  node [
    id 259
    label "wprawia&#263;"
  ]
  node [
    id 260
    label "zakrawa&#263;"
  ]
  node [
    id 261
    label "uderza&#263;"
  ]
  node [
    id 262
    label "przestawa&#263;"
  ]
  node [
    id 263
    label "nacina&#263;"
  ]
  node [
    id 264
    label "kaleczy&#263;"
  ]
  node [
    id 265
    label "hack"
  ]
  node [
    id 266
    label "pocina&#263;"
  ]
  node [
    id 267
    label "psu&#263;"
  ]
  node [
    id 268
    label "odbiera&#263;"
  ]
  node [
    id 269
    label "przerywa&#263;"
  ]
  node [
    id 270
    label "robi&#263;"
  ]
  node [
    id 271
    label "bat"
  ]
  node [
    id 272
    label "cut"
  ]
  node [
    id 273
    label "ch&#322;osta&#263;"
  ]
  node [
    id 274
    label "write_out"
  ]
  node [
    id 275
    label "ch&#322;o&#347;ni&#281;cie"
  ]
  node [
    id 276
    label "ko&#324;"
  ]
  node [
    id 277
    label "stanowczo"
  ]
  node [
    id 278
    label "ostruganie"
  ]
  node [
    id 279
    label "zranienie"
  ]
  node [
    id 280
    label "nieust&#281;pliwie"
  ]
  node [
    id 281
    label "uwa&#380;nie"
  ]
  node [
    id 282
    label "formacja_skalna"
  ]
  node [
    id 283
    label "dash"
  ]
  node [
    id 284
    label "poderwanie"
  ]
  node [
    id 285
    label "talent"
  ]
  node [
    id 286
    label "turn"
  ]
  node [
    id 287
    label "capability"
  ]
  node [
    id 288
    label "powo&#380;enie"
  ]
  node [
    id 289
    label "go"
  ]
  node [
    id 290
    label "potkni&#281;cie"
  ]
  node [
    id 291
    label "w&#281;dkowanie"
  ]
  node [
    id 292
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 293
    label "zaci&#281;ty"
  ]
  node [
    id 294
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 295
    label "naci&#281;cie"
  ]
  node [
    id 296
    label "ch&#322;osn&#261;&#263;"
  ]
  node [
    id 297
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 298
    label "odebra&#263;"
  ]
  node [
    id 299
    label "naci&#261;&#263;"
  ]
  node [
    id 300
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 301
    label "zablokowa&#263;"
  ]
  node [
    id 302
    label "zepsu&#263;"
  ]
  node [
    id 303
    label "nadci&#261;&#263;"
  ]
  node [
    id 304
    label "poderwa&#263;"
  ]
  node [
    id 305
    label "zrani&#263;"
  ]
  node [
    id 306
    label "ostruga&#263;"
  ]
  node [
    id 307
    label "uderzy&#263;"
  ]
  node [
    id 308
    label "przerwa&#263;"
  ]
  node [
    id 309
    label "wprawi&#263;"
  ]
  node [
    id 310
    label "struganie"
  ]
  node [
    id 311
    label "padanie"
  ]
  node [
    id 312
    label "kaleczenie"
  ]
  node [
    id 313
    label "podrywanie"
  ]
  node [
    id 314
    label "nacinanie"
  ]
  node [
    id 315
    label "zaciskanie"
  ]
  node [
    id 316
    label "ch&#322;ostanie"
  ]
  node [
    id 317
    label "narz&#261;d_g&#281;bowy"
  ]
  node [
    id 318
    label "rozpuszczanie"
  ]
  node [
    id 319
    label "&#347;lina"
  ]
  node [
    id 320
    label "wyssanie"
  ]
  node [
    id 321
    label "wci&#261;ganie"
  ]
  node [
    id 322
    label "wessanie"
  ]
  node [
    id 323
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 324
    label "ga&#378;nik"
  ]
  node [
    id 325
    label "ruszanie"
  ]
  node [
    id 326
    label "picie"
  ]
  node [
    id 327
    label "consumption"
  ]
  node [
    id 328
    label "aspiration"
  ]
  node [
    id 329
    label "odci&#261;ganie"
  ]
  node [
    id 330
    label "wysysanie"
  ]
  node [
    id 331
    label "mechanizm"
  ]
  node [
    id 332
    label "j&#281;zyk"
  ]
  node [
    id 333
    label "rusza&#263;"
  ]
  node [
    id 334
    label "sponge"
  ]
  node [
    id 335
    label "sucking"
  ]
  node [
    id 336
    label "wci&#261;ga&#263;"
  ]
  node [
    id 337
    label "pi&#263;"
  ]
  node [
    id 338
    label "smoczek"
  ]
  node [
    id 339
    label "rozpuszcza&#263;"
  ]
  node [
    id 340
    label "mleko"
  ]
  node [
    id 341
    label "zwi&#261;zanie"
  ]
  node [
    id 342
    label "odwadnianie"
  ]
  node [
    id 343
    label "azeotrop"
  ]
  node [
    id 344
    label "odwodni&#263;"
  ]
  node [
    id 345
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 346
    label "lokant"
  ]
  node [
    id 347
    label "marriage"
  ]
  node [
    id 348
    label "bratnia_dusza"
  ]
  node [
    id 349
    label "zwi&#261;za&#263;"
  ]
  node [
    id 350
    label "koligacja"
  ]
  node [
    id 351
    label "odwodnienie"
  ]
  node [
    id 352
    label "marketing_afiliacyjny"
  ]
  node [
    id 353
    label "substancja_chemiczna"
  ]
  node [
    id 354
    label "wi&#261;zanie"
  ]
  node [
    id 355
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 356
    label "powi&#261;zanie"
  ]
  node [
    id 357
    label "odwadnia&#263;"
  ]
  node [
    id 358
    label "bearing"
  ]
  node [
    id 359
    label "konstytucja"
  ]
  node [
    id 360
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 361
    label "odci&#261;ga&#263;"
  ]
  node [
    id 362
    label "powodowa&#263;"
  ]
  node [
    id 363
    label "drain"
  ]
  node [
    id 364
    label "odprowadza&#263;"
  ]
  node [
    id 365
    label "cia&#322;o"
  ]
  node [
    id 366
    label "odsuwa&#263;"
  ]
  node [
    id 367
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 368
    label "osusza&#263;"
  ]
  node [
    id 369
    label "zbi&#243;r"
  ]
  node [
    id 370
    label "akt"
  ]
  node [
    id 371
    label "uchwa&#322;a"
  ]
  node [
    id 372
    label "cezar"
  ]
  node [
    id 373
    label "numeracja"
  ]
  node [
    id 374
    label "osuszanie"
  ]
  node [
    id 375
    label "proces_chemiczny"
  ]
  node [
    id 376
    label "dehydratacja"
  ]
  node [
    id 377
    label "powodowanie"
  ]
  node [
    id 378
    label "odprowadzanie"
  ]
  node [
    id 379
    label "odsuwanie"
  ]
  node [
    id 380
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 381
    label "osuszy&#263;"
  ]
  node [
    id 382
    label "odsun&#261;&#263;"
  ]
  node [
    id 383
    label "spowodowa&#263;"
  ]
  node [
    id 384
    label "odprowadzi&#263;"
  ]
  node [
    id 385
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 386
    label "odsuni&#281;cie"
  ]
  node [
    id 387
    label "oznaka"
  ]
  node [
    id 388
    label "odprowadzenie"
  ]
  node [
    id 389
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 390
    label "osuszenie"
  ]
  node [
    id 391
    label "dehydration"
  ]
  node [
    id 392
    label "twardnienie"
  ]
  node [
    id 393
    label "zmiana"
  ]
  node [
    id 394
    label "przywi&#261;zanie"
  ]
  node [
    id 395
    label "narta"
  ]
  node [
    id 396
    label "pakowanie"
  ]
  node [
    id 397
    label "uchwyt"
  ]
  node [
    id 398
    label "szcz&#281;ka"
  ]
  node [
    id 399
    label "anga&#380;owanie"
  ]
  node [
    id 400
    label "podwi&#261;zywanie"
  ]
  node [
    id 401
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 402
    label "socket"
  ]
  node [
    id 403
    label "wi&#261;za&#263;"
  ]
  node [
    id 404
    label "zawi&#261;zek"
  ]
  node [
    id 405
    label "my&#347;lenie"
  ]
  node [
    id 406
    label "manewr"
  ]
  node [
    id 407
    label "wytwarzanie"
  ]
  node [
    id 408
    label "scalanie"
  ]
  node [
    id 409
    label "do&#322;&#261;czanie"
  ]
  node [
    id 410
    label "fusion"
  ]
  node [
    id 411
    label "rozmieszczenie"
  ]
  node [
    id 412
    label "communication"
  ]
  node [
    id 413
    label "obwi&#261;zanie"
  ]
  node [
    id 414
    label "element_konstrukcyjny"
  ]
  node [
    id 415
    label "mezomeria"
  ]
  node [
    id 416
    label "wi&#281;&#378;"
  ]
  node [
    id 417
    label "combination"
  ]
  node [
    id 418
    label "szermierka"
  ]
  node [
    id 419
    label "obezw&#322;adnianie"
  ]
  node [
    id 420
    label "podwi&#261;zanie"
  ]
  node [
    id 421
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 422
    label "tobo&#322;ek"
  ]
  node [
    id 423
    label "przywi&#261;zywanie"
  ]
  node [
    id 424
    label "zobowi&#261;zywanie"
  ]
  node [
    id 425
    label "cement"
  ]
  node [
    id 426
    label "dressing"
  ]
  node [
    id 427
    label "obwi&#261;zywanie"
  ]
  node [
    id 428
    label "ceg&#322;a"
  ]
  node [
    id 429
    label "przymocowywanie"
  ]
  node [
    id 430
    label "oddzia&#322;ywanie"
  ]
  node [
    id 431
    label "kojarzenie_si&#281;"
  ]
  node [
    id 432
    label "miecz"
  ]
  node [
    id 433
    label "&#322;&#261;czenie"
  ]
  node [
    id 434
    label "incorporate"
  ]
  node [
    id 435
    label "w&#281;ze&#322;"
  ]
  node [
    id 436
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 437
    label "bind"
  ]
  node [
    id 438
    label "opakowa&#263;"
  ]
  node [
    id 439
    label "scali&#263;"
  ]
  node [
    id 440
    label "unify"
  ]
  node [
    id 441
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 442
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 443
    label "zatrzyma&#263;"
  ]
  node [
    id 444
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 445
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 446
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 447
    label "zawi&#261;za&#263;"
  ]
  node [
    id 448
    label "zaprawa"
  ]
  node [
    id 449
    label "powi&#261;za&#263;"
  ]
  node [
    id 450
    label "relate"
  ]
  node [
    id 451
    label "consort"
  ]
  node [
    id 452
    label "form"
  ]
  node [
    id 453
    label "fastening"
  ]
  node [
    id 454
    label "affiliation"
  ]
  node [
    id 455
    label "attachment"
  ]
  node [
    id 456
    label "obezw&#322;adnienie"
  ]
  node [
    id 457
    label "opakowanie"
  ]
  node [
    id 458
    label "z&#322;&#261;czenie"
  ]
  node [
    id 459
    label "do&#322;&#261;czenie"
  ]
  node [
    id 460
    label "tying"
  ]
  node [
    id 461
    label "po&#322;&#261;czenie"
  ]
  node [
    id 462
    label "st&#281;&#380;enie"
  ]
  node [
    id 463
    label "zobowi&#261;zanie"
  ]
  node [
    id 464
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 465
    label "ograniczenie"
  ]
  node [
    id 466
    label "zawi&#261;zanie"
  ]
  node [
    id 467
    label "roztw&#243;r"
  ]
  node [
    id 468
    label "przybud&#243;wka"
  ]
  node [
    id 469
    label "organization"
  ]
  node [
    id 470
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 471
    label "od&#322;am"
  ]
  node [
    id 472
    label "TOPR"
  ]
  node [
    id 473
    label "komitet_koordynacyjny"
  ]
  node [
    id 474
    label "przedstawicielstwo"
  ]
  node [
    id 475
    label "ZMP"
  ]
  node [
    id 476
    label "Cepelia"
  ]
  node [
    id 477
    label "GOPR"
  ]
  node [
    id 478
    label "endecki"
  ]
  node [
    id 479
    label "ZBoWiD"
  ]
  node [
    id 480
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 481
    label "podmiot"
  ]
  node [
    id 482
    label "boj&#243;wka"
  ]
  node [
    id 483
    label "ZOMO"
  ]
  node [
    id 484
    label "centrala"
  ]
  node [
    id 485
    label "relatywizowanie"
  ]
  node [
    id 486
    label "zrelatywizowa&#263;"
  ]
  node [
    id 487
    label "mention"
  ]
  node [
    id 488
    label "zrelatywizowanie"
  ]
  node [
    id 489
    label "pomy&#347;lenie"
  ]
  node [
    id 490
    label "relatywizowa&#263;"
  ]
  node [
    id 491
    label "kontakt"
  ]
  node [
    id 492
    label "odj&#281;cie"
  ]
  node [
    id 493
    label "transgresja"
  ]
  node [
    id 494
    label "zacz&#281;cie"
  ]
  node [
    id 495
    label "zepsucie"
  ]
  node [
    id 496
    label "discourtesy"
  ]
  node [
    id 497
    label "opening"
  ]
  node [
    id 498
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 499
    label "wyra&#380;enie"
  ]
  node [
    id 500
    label "post&#261;pienie"
  ]
  node [
    id 501
    label "demoralizacja"
  ]
  node [
    id 502
    label "pogorszenie"
  ]
  node [
    id 503
    label "spierdolenie"
  ]
  node [
    id 504
    label "uszkodzenie"
  ]
  node [
    id 505
    label "inclination"
  ]
  node [
    id 506
    label "spoil"
  ]
  node [
    id 507
    label "zjebanie"
  ]
  node [
    id 508
    label "kondycja"
  ]
  node [
    id 509
    label "spapranie"
  ]
  node [
    id 510
    label "zdeformowanie"
  ]
  node [
    id 511
    label "depravity"
  ]
  node [
    id 512
    label "demoralization"
  ]
  node [
    id 513
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 514
    label "narobienie"
  ]
  node [
    id 515
    label "porobienie"
  ]
  node [
    id 516
    label "creation"
  ]
  node [
    id 517
    label "policzenie"
  ]
  node [
    id 518
    label "oddalenie"
  ]
  node [
    id 519
    label "ablation"
  ]
  node [
    id 520
    label "withdrawal"
  ]
  node [
    id 521
    label "od&#322;&#261;czony"
  ]
  node [
    id 522
    label "amputation"
  ]
  node [
    id 523
    label "oddzielenie"
  ]
  node [
    id 524
    label "zabranie"
  ]
  node [
    id 525
    label "proces_geologiczny"
  ]
  node [
    id 526
    label "transgression"
  ]
  node [
    id 527
    label "proces_biologiczny"
  ]
  node [
    id 528
    label "zamiana"
  ]
  node [
    id 529
    label "roztapianie_si&#281;"
  ]
  node [
    id 530
    label "rozporz&#261;dzenie"
  ]
  node [
    id 531
    label "polecenie"
  ]
  node [
    id 532
    label "consign"
  ]
  node [
    id 533
    label "pognanie"
  ]
  node [
    id 534
    label "recommendation"
  ]
  node [
    id 535
    label "pobiegni&#281;cie"
  ]
  node [
    id 536
    label "powierzenie"
  ]
  node [
    id 537
    label "doradzenie"
  ]
  node [
    id 538
    label "education"
  ]
  node [
    id 539
    label "wypowied&#378;"
  ]
  node [
    id 540
    label "zaordynowanie"
  ]
  node [
    id 541
    label "przesadzenie"
  ]
  node [
    id 542
    label "ukaz"
  ]
  node [
    id 543
    label "rekomendacja"
  ]
  node [
    id 544
    label "statement"
  ]
  node [
    id 545
    label "zadanie"
  ]
  node [
    id 546
    label "ordonans"
  ]
  node [
    id 547
    label "zarz&#261;dzenie"
  ]
  node [
    id 548
    label "rule"
  ]
  node [
    id 549
    label "arrangement"
  ]
  node [
    id 550
    label "commission"
  ]
  node [
    id 551
    label "kod"
  ]
  node [
    id 552
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 553
    label "gramatyka"
  ]
  node [
    id 554
    label "fonetyka"
  ]
  node [
    id 555
    label "t&#322;umaczenie"
  ]
  node [
    id 556
    label "rozumienie"
  ]
  node [
    id 557
    label "address"
  ]
  node [
    id 558
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 559
    label "m&#243;wienie"
  ]
  node [
    id 560
    label "s&#322;ownictwo"
  ]
  node [
    id 561
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 562
    label "konsonantyzm"
  ]
  node [
    id 563
    label "wokalizm"
  ]
  node [
    id 564
    label "komunikacja"
  ]
  node [
    id 565
    label "m&#243;wi&#263;"
  ]
  node [
    id 566
    label "po_koroniarsku"
  ]
  node [
    id 567
    label "rozumie&#263;"
  ]
  node [
    id 568
    label "przet&#322;umaczenie"
  ]
  node [
    id 569
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 570
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 571
    label "tongue"
  ]
  node [
    id 572
    label "pismo"
  ]
  node [
    id 573
    label "parafrazowanie"
  ]
  node [
    id 574
    label "komunikat"
  ]
  node [
    id 575
    label "stylizacja"
  ]
  node [
    id 576
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 577
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 578
    label "strawestowanie"
  ]
  node [
    id 579
    label "sparafrazowanie"
  ]
  node [
    id 580
    label "sformu&#322;owanie"
  ]
  node [
    id 581
    label "pos&#322;uchanie"
  ]
  node [
    id 582
    label "strawestowa&#263;"
  ]
  node [
    id 583
    label "parafrazowa&#263;"
  ]
  node [
    id 584
    label "delimitacja"
  ]
  node [
    id 585
    label "rezultat"
  ]
  node [
    id 586
    label "ozdobnik"
  ]
  node [
    id 587
    label "trawestowa&#263;"
  ]
  node [
    id 588
    label "s&#261;d"
  ]
  node [
    id 589
    label "sparafrazowa&#263;"
  ]
  node [
    id 590
    label "trawestowanie"
  ]
  node [
    id 591
    label "zapomnie&#263;"
  ]
  node [
    id 592
    label "cecha"
  ]
  node [
    id 593
    label "zapominanie"
  ]
  node [
    id 594
    label "zapomnienie"
  ]
  node [
    id 595
    label "potencja&#322;"
  ]
  node [
    id 596
    label "obliczeniowo"
  ]
  node [
    id 597
    label "ability"
  ]
  node [
    id 598
    label "posiada&#263;"
  ]
  node [
    id 599
    label "zapomina&#263;"
  ]
  node [
    id 600
    label "transportation_system"
  ]
  node [
    id 601
    label "implicite"
  ]
  node [
    id 602
    label "explicite"
  ]
  node [
    id 603
    label "wydeptanie"
  ]
  node [
    id 604
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 605
    label "wydeptywanie"
  ]
  node [
    id 606
    label "ekspedytor"
  ]
  node [
    id 607
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 608
    label "bezproblemowy"
  ]
  node [
    id 609
    label "wydarzenie"
  ]
  node [
    id 610
    label "activity"
  ]
  node [
    id 611
    label "ci&#261;g"
  ]
  node [
    id 612
    label "language"
  ]
  node [
    id 613
    label "szyfrowanie"
  ]
  node [
    id 614
    label "szablon"
  ]
  node [
    id 615
    label "code"
  ]
  node [
    id 616
    label "cover"
  ]
  node [
    id 617
    label "opowiedzenie"
  ]
  node [
    id 618
    label "zwracanie_si&#281;"
  ]
  node [
    id 619
    label "zapeszanie"
  ]
  node [
    id 620
    label "formu&#322;owanie"
  ]
  node [
    id 621
    label "wypowiadanie"
  ]
  node [
    id 622
    label "powiadanie"
  ]
  node [
    id 623
    label "dysfonia"
  ]
  node [
    id 624
    label "ozywanie_si&#281;"
  ]
  node [
    id 625
    label "public_speaking"
  ]
  node [
    id 626
    label "wyznawanie"
  ]
  node [
    id 627
    label "dodawanie"
  ]
  node [
    id 628
    label "wydawanie"
  ]
  node [
    id 629
    label "wygadanie"
  ]
  node [
    id 630
    label "informowanie"
  ]
  node [
    id 631
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 632
    label "dowalenie"
  ]
  node [
    id 633
    label "prawienie"
  ]
  node [
    id 634
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 635
    label "przerywanie"
  ]
  node [
    id 636
    label "dogadanie_si&#281;"
  ]
  node [
    id 637
    label "wydobywanie"
  ]
  node [
    id 638
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 639
    label "wyra&#380;anie"
  ]
  node [
    id 640
    label "opowiadanie"
  ]
  node [
    id 641
    label "mawianie"
  ]
  node [
    id 642
    label "stosowanie"
  ]
  node [
    id 643
    label "zauwa&#380;enie"
  ]
  node [
    id 644
    label "speaking"
  ]
  node [
    id 645
    label "gaworzenie"
  ]
  node [
    id 646
    label "przepowiadanie"
  ]
  node [
    id 647
    label "dogadywanie_si&#281;"
  ]
  node [
    id 648
    label "ortografia"
  ]
  node [
    id 649
    label "dzia&#322;"
  ]
  node [
    id 650
    label "zajawka"
  ]
  node [
    id 651
    label "paleografia"
  ]
  node [
    id 652
    label "dzie&#322;o"
  ]
  node [
    id 653
    label "wk&#322;ad"
  ]
  node [
    id 654
    label "egzemplarz"
  ]
  node [
    id 655
    label "ok&#322;adka"
  ]
  node [
    id 656
    label "letter"
  ]
  node [
    id 657
    label "prasa"
  ]
  node [
    id 658
    label "psychotest"
  ]
  node [
    id 659
    label "Zwrotnica"
  ]
  node [
    id 660
    label "script"
  ]
  node [
    id 661
    label "czasopismo"
  ]
  node [
    id 662
    label "adres"
  ]
  node [
    id 663
    label "przekaz"
  ]
  node [
    id 664
    label "grafia"
  ]
  node [
    id 665
    label "interpunkcja"
  ]
  node [
    id 666
    label "handwriting"
  ]
  node [
    id 667
    label "paleograf"
  ]
  node [
    id 668
    label "list"
  ]
  node [
    id 669
    label "termin"
  ]
  node [
    id 670
    label "terminology"
  ]
  node [
    id 671
    label "sk&#322;adnia"
  ]
  node [
    id 672
    label "morfologia"
  ]
  node [
    id 673
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 674
    label "kategoria_gramatyczna"
  ]
  node [
    id 675
    label "fleksja"
  ]
  node [
    id 676
    label "asymilowa&#263;"
  ]
  node [
    id 677
    label "zasymilowa&#263;"
  ]
  node [
    id 678
    label "transkrypcja"
  ]
  node [
    id 679
    label "asymilowanie"
  ]
  node [
    id 680
    label "g&#322;osownia"
  ]
  node [
    id 681
    label "phonetics"
  ]
  node [
    id 682
    label "zasymilowanie"
  ]
  node [
    id 683
    label "palatogram"
  ]
  node [
    id 684
    label "przek&#322;adanie"
  ]
  node [
    id 685
    label "zrozumia&#322;y"
  ]
  node [
    id 686
    label "tekst"
  ]
  node [
    id 687
    label "remark"
  ]
  node [
    id 688
    label "rendition"
  ]
  node [
    id 689
    label "uzasadnianie"
  ]
  node [
    id 690
    label "rozwianie"
  ]
  node [
    id 691
    label "explanation"
  ]
  node [
    id 692
    label "kr&#281;ty"
  ]
  node [
    id 693
    label "bronienie"
  ]
  node [
    id 694
    label "gossip"
  ]
  node [
    id 695
    label "przekonywanie"
  ]
  node [
    id 696
    label "rozwiewanie"
  ]
  node [
    id 697
    label "przedstawianie"
  ]
  node [
    id 698
    label "przekona&#263;"
  ]
  node [
    id 699
    label "zrobi&#263;"
  ]
  node [
    id 700
    label "put"
  ]
  node [
    id 701
    label "frame"
  ]
  node [
    id 702
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 703
    label "zinterpretowa&#263;"
  ]
  node [
    id 704
    label "przekonywa&#263;"
  ]
  node [
    id 705
    label "u&#322;atwia&#263;"
  ]
  node [
    id 706
    label "sprawowa&#263;"
  ]
  node [
    id 707
    label "suplikowa&#263;"
  ]
  node [
    id 708
    label "interpretowa&#263;"
  ]
  node [
    id 709
    label "uzasadnia&#263;"
  ]
  node [
    id 710
    label "give"
  ]
  node [
    id 711
    label "przedstawia&#263;"
  ]
  node [
    id 712
    label "explain"
  ]
  node [
    id 713
    label "poja&#347;nia&#263;"
  ]
  node [
    id 714
    label "broni&#263;"
  ]
  node [
    id 715
    label "przek&#322;ada&#263;"
  ]
  node [
    id 716
    label "elaborate"
  ]
  node [
    id 717
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 718
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 719
    label "prawi&#263;"
  ]
  node [
    id 720
    label "express"
  ]
  node [
    id 721
    label "chew_the_fat"
  ]
  node [
    id 722
    label "talk"
  ]
  node [
    id 723
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 724
    label "say"
  ]
  node [
    id 725
    label "wyra&#380;a&#263;"
  ]
  node [
    id 726
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 727
    label "tell"
  ]
  node [
    id 728
    label "informowa&#263;"
  ]
  node [
    id 729
    label "rozmawia&#263;"
  ]
  node [
    id 730
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 731
    label "powiada&#263;"
  ]
  node [
    id 732
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 733
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 734
    label "okre&#347;la&#263;"
  ]
  node [
    id 735
    label "u&#380;ywa&#263;"
  ]
  node [
    id 736
    label "gaworzy&#263;"
  ]
  node [
    id 737
    label "formu&#322;owa&#263;"
  ]
  node [
    id 738
    label "dziama&#263;"
  ]
  node [
    id 739
    label "umie&#263;"
  ]
  node [
    id 740
    label "wydobywa&#263;"
  ]
  node [
    id 741
    label "czucie"
  ]
  node [
    id 742
    label "bycie"
  ]
  node [
    id 743
    label "interpretation"
  ]
  node [
    id 744
    label "realization"
  ]
  node [
    id 745
    label "hermeneutyka"
  ]
  node [
    id 746
    label "kumanie"
  ]
  node [
    id 747
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 748
    label "apprehension"
  ]
  node [
    id 749
    label "wnioskowanie"
  ]
  node [
    id 750
    label "obja&#347;nienie"
  ]
  node [
    id 751
    label "match"
  ]
  node [
    id 752
    label "see"
  ]
  node [
    id 753
    label "kuma&#263;"
  ]
  node [
    id 754
    label "zna&#263;"
  ]
  node [
    id 755
    label "empatia"
  ]
  node [
    id 756
    label "czu&#263;"
  ]
  node [
    id 757
    label "wiedzie&#263;"
  ]
  node [
    id 758
    label "oblec"
  ]
  node [
    id 759
    label "ubra&#263;"
  ]
  node [
    id 760
    label "load"
  ]
  node [
    id 761
    label "oblec_si&#281;"
  ]
  node [
    id 762
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 763
    label "przyodzia&#263;"
  ]
  node [
    id 764
    label "insert"
  ]
  node [
    id 765
    label "deal"
  ]
  node [
    id 766
    label "zmieni&#263;"
  ]
  node [
    id 767
    label "okre&#347;li&#263;"
  ]
  node [
    id 768
    label "wpierniczy&#263;"
  ]
  node [
    id 769
    label "uplasowa&#263;"
  ]
  node [
    id 770
    label "set"
  ]
  node [
    id 771
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 772
    label "umieszcza&#263;"
  ]
  node [
    id 773
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 774
    label "act"
  ]
  node [
    id 775
    label "pokry&#263;"
  ]
  node [
    id 776
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 777
    label "zacz&#261;&#263;"
  ]
  node [
    id 778
    label "pozostawi&#263;"
  ]
  node [
    id 779
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 780
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 781
    label "stagger"
  ]
  node [
    id 782
    label "wear"
  ]
  node [
    id 783
    label "przygotowa&#263;"
  ]
  node [
    id 784
    label "return"
  ]
  node [
    id 785
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 786
    label "wygra&#263;"
  ]
  node [
    id 787
    label "raise"
  ]
  node [
    id 788
    label "znak"
  ]
  node [
    id 789
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 790
    label "po&#347;ciel"
  ]
  node [
    id 791
    label "otoczy&#263;"
  ]
  node [
    id 792
    label "wystrychn&#261;&#263;"
  ]
  node [
    id 793
    label "assume"
  ]
  node [
    id 794
    label "przedstawi&#263;"
  ]
  node [
    id 795
    label "upi&#281;kszy&#263;"
  ]
  node [
    id 796
    label "odziewek"
  ]
  node [
    id 797
    label "pasmanteria"
  ]
  node [
    id 798
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 799
    label "znoszenie"
  ]
  node [
    id 800
    label "zrzuci&#263;"
  ]
  node [
    id 801
    label "garderoba"
  ]
  node [
    id 802
    label "odzie&#380;"
  ]
  node [
    id 803
    label "kr&#243;j"
  ]
  node [
    id 804
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 805
    label "nosi&#263;"
  ]
  node [
    id 806
    label "zasada"
  ]
  node [
    id 807
    label "znosi&#263;"
  ]
  node [
    id 808
    label "w&#322;o&#380;enie"
  ]
  node [
    id 809
    label "ubranie_si&#281;"
  ]
  node [
    id 810
    label "gorset"
  ]
  node [
    id 811
    label "zrzucenie"
  ]
  node [
    id 812
    label "pochodzi&#263;"
  ]
  node [
    id 813
    label "konsekwencja"
  ]
  node [
    id 814
    label "forfeit"
  ]
  node [
    id 815
    label "roboty_przymusowe"
  ]
  node [
    id 816
    label "kwota"
  ]
  node [
    id 817
    label "punishment"
  ]
  node [
    id 818
    label "nemezis"
  ]
  node [
    id 819
    label "klacz"
  ]
  node [
    id 820
    label "odczuwa&#263;"
  ]
  node [
    id 821
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 822
    label "skrupienie_si&#281;"
  ]
  node [
    id 823
    label "odczu&#263;"
  ]
  node [
    id 824
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 825
    label "odczucie"
  ]
  node [
    id 826
    label "skrupianie_si&#281;"
  ]
  node [
    id 827
    label "event"
  ]
  node [
    id 828
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 829
    label "koszula_Dejaniry"
  ]
  node [
    id 830
    label "odczuwanie"
  ]
  node [
    id 831
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 832
    label "mare"
  ]
  node [
    id 833
    label "samica"
  ]
  node [
    id 834
    label "pieni&#261;dze"
  ]
  node [
    id 835
    label "wynie&#347;&#263;"
  ]
  node [
    id 836
    label "ilo&#347;&#263;"
  ]
  node [
    id 837
    label "limit"
  ]
  node [
    id 838
    label "wynosi&#263;"
  ]
  node [
    id 839
    label "monetarny"
  ]
  node [
    id 840
    label "altitude"
  ]
  node [
    id 841
    label "brzmienie"
  ]
  node [
    id 842
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 843
    label "rozmiar"
  ]
  node [
    id 844
    label "degree"
  ]
  node [
    id 845
    label "tallness"
  ]
  node [
    id 846
    label "sum"
  ]
  node [
    id 847
    label "k&#261;t"
  ]
  node [
    id 848
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 849
    label "ton"
  ]
  node [
    id 850
    label "ambitus"
  ]
  node [
    id 851
    label "skala"
  ]
  node [
    id 852
    label "czas"
  ]
  node [
    id 853
    label "pole"
  ]
  node [
    id 854
    label "part"
  ]
  node [
    id 855
    label "line"
  ]
  node [
    id 856
    label "fragment"
  ]
  node [
    id 857
    label "kawa&#322;ek"
  ]
  node [
    id 858
    label "teren"
  ]
  node [
    id 859
    label "coupon"
  ]
  node [
    id 860
    label "epizod"
  ]
  node [
    id 861
    label "moneta"
  ]
  node [
    id 862
    label "pokwitowanie"
  ]
  node [
    id 863
    label "liczba"
  ]
  node [
    id 864
    label "dymensja"
  ]
  node [
    id 865
    label "znaczenie"
  ]
  node [
    id 866
    label "circumference"
  ]
  node [
    id 867
    label "warunek_lokalowy"
  ]
  node [
    id 868
    label "tone"
  ]
  node [
    id 869
    label "rejestr"
  ]
  node [
    id 870
    label "kolorystyka"
  ]
  node [
    id 871
    label "spirit"
  ]
  node [
    id 872
    label "sound"
  ]
  node [
    id 873
    label "obiekt_matematyczny"
  ]
  node [
    id 874
    label "ubocze"
  ]
  node [
    id 875
    label "siedziba"
  ]
  node [
    id 876
    label "rami&#281;_k&#261;ta"
  ]
  node [
    id 877
    label "dom"
  ]
  node [
    id 878
    label "poj&#281;cie"
  ]
  node [
    id 879
    label "zaleta"
  ]
  node [
    id 880
    label "property"
  ]
  node [
    id 881
    label "measure"
  ]
  node [
    id 882
    label "opinia"
  ]
  node [
    id 883
    label "rzadko&#347;&#263;"
  ]
  node [
    id 884
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 885
    label "ryba"
  ]
  node [
    id 886
    label "Uzbekistan"
  ]
  node [
    id 887
    label "catfish"
  ]
  node [
    id 888
    label "jednostka_monetarna"
  ]
  node [
    id 889
    label "sumowate"
  ]
  node [
    id 890
    label "fala_elektromagnetyczna"
  ]
  node [
    id 891
    label "d&#322;ugo&#347;&#263;"
  ]
  node [
    id 892
    label "radiokomunikacja"
  ]
  node [
    id 893
    label "cyclicity"
  ]
  node [
    id 894
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 895
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 896
    label "grosz"
  ]
  node [
    id 897
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 898
    label "doskona&#322;y"
  ]
  node [
    id 899
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 900
    label "szlachetny"
  ]
  node [
    id 901
    label "utytu&#322;owany"
  ]
  node [
    id 902
    label "kochany"
  ]
  node [
    id 903
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 904
    label "Polska"
  ]
  node [
    id 905
    label "z&#322;ocenie"
  ]
  node [
    id 906
    label "metaliczny"
  ]
  node [
    id 907
    label "wspania&#322;y"
  ]
  node [
    id 908
    label "poz&#322;ocenie"
  ]
  node [
    id 909
    label "wybitny"
  ]
  node [
    id 910
    label "prominentny"
  ]
  node [
    id 911
    label "znany"
  ]
  node [
    id 912
    label "&#347;wietny"
  ]
  node [
    id 913
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 914
    label "naj"
  ]
  node [
    id 915
    label "doskonale"
  ]
  node [
    id 916
    label "pe&#322;ny"
  ]
  node [
    id 917
    label "uczciwy"
  ]
  node [
    id 918
    label "dobry"
  ]
  node [
    id 919
    label "harmonijny"
  ]
  node [
    id 920
    label "zacny"
  ]
  node [
    id 921
    label "pi&#281;kny"
  ]
  node [
    id 922
    label "szlachetnie"
  ]
  node [
    id 923
    label "gatunkowy"
  ]
  node [
    id 924
    label "metalicznie"
  ]
  node [
    id 925
    label "typowy"
  ]
  node [
    id 926
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 927
    label "metaloplastyczny"
  ]
  node [
    id 928
    label "wybranek"
  ]
  node [
    id 929
    label "kochanek"
  ]
  node [
    id 930
    label "drogi"
  ]
  node [
    id 931
    label "umi&#322;owany"
  ]
  node [
    id 932
    label "kochanie"
  ]
  node [
    id 933
    label "och&#281;do&#380;ny"
  ]
  node [
    id 934
    label "zajebisty"
  ]
  node [
    id 935
    label "&#347;wietnie"
  ]
  node [
    id 936
    label "warto&#347;ciowy"
  ]
  node [
    id 937
    label "spania&#322;y"
  ]
  node [
    id 938
    label "pozytywny"
  ]
  node [
    id 939
    label "pomy&#347;lny"
  ]
  node [
    id 940
    label "wspaniale"
  ]
  node [
    id 941
    label "bogato"
  ]
  node [
    id 942
    label "kolorowy"
  ]
  node [
    id 943
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 944
    label "typ_mongoloidalny"
  ]
  node [
    id 945
    label "jasny"
  ]
  node [
    id 946
    label "ciep&#322;y"
  ]
  node [
    id 947
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 948
    label "groszak"
  ]
  node [
    id 949
    label "szyling_austryjacki"
  ]
  node [
    id 950
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 951
    label "Krajna"
  ]
  node [
    id 952
    label "Kielecczyzna"
  ]
  node [
    id 953
    label "NATO"
  ]
  node [
    id 954
    label "Lubuskie"
  ]
  node [
    id 955
    label "Opolskie"
  ]
  node [
    id 956
    label "Mazowsze"
  ]
  node [
    id 957
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 958
    label "Podlasie"
  ]
  node [
    id 959
    label "Kaczawa"
  ]
  node [
    id 960
    label "Wolin"
  ]
  node [
    id 961
    label "Wielkopolska"
  ]
  node [
    id 962
    label "Lubelszczyzna"
  ]
  node [
    id 963
    label "Izera"
  ]
  node [
    id 964
    label "Wis&#322;a"
  ]
  node [
    id 965
    label "So&#322;a"
  ]
  node [
    id 966
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 967
    label "Pa&#322;uki"
  ]
  node [
    id 968
    label "Pomorze_Zachodnie"
  ]
  node [
    id 969
    label "Podkarpacie"
  ]
  node [
    id 970
    label "barwy_polskie"
  ]
  node [
    id 971
    label "Kujawy"
  ]
  node [
    id 972
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 973
    label "Nadbu&#380;e"
  ]
  node [
    id 974
    label "Warmia"
  ]
  node [
    id 975
    label "&#321;&#243;dzkie"
  ]
  node [
    id 976
    label "Bory_Tucholskie"
  ]
  node [
    id 977
    label "Suwalszczyzna"
  ]
  node [
    id 978
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 979
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 980
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 981
    label "Ma&#322;opolska"
  ]
  node [
    id 982
    label "Unia_Europejska"
  ]
  node [
    id 983
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 984
    label "Mazury"
  ]
  node [
    id 985
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 986
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 987
    label "Powi&#347;le"
  ]
  node [
    id 988
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 989
    label "z&#322;ocisty"
  ]
  node [
    id 990
    label "powleczenie"
  ]
  node [
    id 991
    label "zabarwienie"
  ]
  node [
    id 992
    label "gilt"
  ]
  node [
    id 993
    label "club"
  ]
  node [
    id 994
    label "plating"
  ]
  node [
    id 995
    label "barwienie"
  ]
  node [
    id 996
    label "zdobienie"
  ]
  node [
    id 997
    label "platerowanie"
  ]
  node [
    id 998
    label "najemny"
  ]
  node [
    id 999
    label "p&#322;atnie"
  ]
  node [
    id 1000
    label "najemnie"
  ]
  node [
    id 1001
    label "przekupny"
  ]
  node [
    id 1002
    label "udzielny"
  ]
  node [
    id 1003
    label "etat"
  ]
  node [
    id 1004
    label "portfel"
  ]
  node [
    id 1005
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 1006
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 1007
    label "zas&#243;b"
  ]
  node [
    id 1008
    label "pugilares"
  ]
  node [
    id 1009
    label "pojemnik"
  ]
  node [
    id 1010
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1011
    label "bag"
  ]
  node [
    id 1012
    label "galanteria"
  ]
  node [
    id 1013
    label "wymiar"
  ]
  node [
    id 1014
    label "posada"
  ]
  node [
    id 1015
    label "Japonia"
  ]
  node [
    id 1016
    label "Zair"
  ]
  node [
    id 1017
    label "Belize"
  ]
  node [
    id 1018
    label "San_Marino"
  ]
  node [
    id 1019
    label "Tanzania"
  ]
  node [
    id 1020
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1021
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1022
    label "Senegal"
  ]
  node [
    id 1023
    label "Indie"
  ]
  node [
    id 1024
    label "Seszele"
  ]
  node [
    id 1025
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1026
    label "Zimbabwe"
  ]
  node [
    id 1027
    label "Filipiny"
  ]
  node [
    id 1028
    label "Mauretania"
  ]
  node [
    id 1029
    label "Malezja"
  ]
  node [
    id 1030
    label "Rumunia"
  ]
  node [
    id 1031
    label "Surinam"
  ]
  node [
    id 1032
    label "Ukraina"
  ]
  node [
    id 1033
    label "Syria"
  ]
  node [
    id 1034
    label "Wyspy_Marshalla"
  ]
  node [
    id 1035
    label "Burkina_Faso"
  ]
  node [
    id 1036
    label "Grecja"
  ]
  node [
    id 1037
    label "Wenezuela"
  ]
  node [
    id 1038
    label "Nepal"
  ]
  node [
    id 1039
    label "Suazi"
  ]
  node [
    id 1040
    label "S&#322;owacja"
  ]
  node [
    id 1041
    label "Algieria"
  ]
  node [
    id 1042
    label "Chiny"
  ]
  node [
    id 1043
    label "Grenada"
  ]
  node [
    id 1044
    label "Barbados"
  ]
  node [
    id 1045
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1046
    label "Pakistan"
  ]
  node [
    id 1047
    label "Niemcy"
  ]
  node [
    id 1048
    label "Bahrajn"
  ]
  node [
    id 1049
    label "Komory"
  ]
  node [
    id 1050
    label "Australia"
  ]
  node [
    id 1051
    label "Rodezja"
  ]
  node [
    id 1052
    label "Malawi"
  ]
  node [
    id 1053
    label "Gwinea"
  ]
  node [
    id 1054
    label "Wehrlen"
  ]
  node [
    id 1055
    label "Meksyk"
  ]
  node [
    id 1056
    label "Liechtenstein"
  ]
  node [
    id 1057
    label "Czarnog&#243;ra"
  ]
  node [
    id 1058
    label "Wielka_Brytania"
  ]
  node [
    id 1059
    label "Kuwejt"
  ]
  node [
    id 1060
    label "Angola"
  ]
  node [
    id 1061
    label "Monako"
  ]
  node [
    id 1062
    label "Jemen"
  ]
  node [
    id 1063
    label "Etiopia"
  ]
  node [
    id 1064
    label "Madagaskar"
  ]
  node [
    id 1065
    label "terytorium"
  ]
  node [
    id 1066
    label "Kolumbia"
  ]
  node [
    id 1067
    label "Portoryko"
  ]
  node [
    id 1068
    label "Mauritius"
  ]
  node [
    id 1069
    label "Kostaryka"
  ]
  node [
    id 1070
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1071
    label "Tajlandia"
  ]
  node [
    id 1072
    label "Argentyna"
  ]
  node [
    id 1073
    label "Zambia"
  ]
  node [
    id 1074
    label "Sri_Lanka"
  ]
  node [
    id 1075
    label "Gwatemala"
  ]
  node [
    id 1076
    label "Kirgistan"
  ]
  node [
    id 1077
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1078
    label "Hiszpania"
  ]
  node [
    id 1079
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1080
    label "Salwador"
  ]
  node [
    id 1081
    label "Korea"
  ]
  node [
    id 1082
    label "Macedonia"
  ]
  node [
    id 1083
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1084
    label "Brunei"
  ]
  node [
    id 1085
    label "Mozambik"
  ]
  node [
    id 1086
    label "Turcja"
  ]
  node [
    id 1087
    label "Kambod&#380;a"
  ]
  node [
    id 1088
    label "Benin"
  ]
  node [
    id 1089
    label "Bhutan"
  ]
  node [
    id 1090
    label "Tunezja"
  ]
  node [
    id 1091
    label "Austria"
  ]
  node [
    id 1092
    label "Izrael"
  ]
  node [
    id 1093
    label "Sierra_Leone"
  ]
  node [
    id 1094
    label "Jamajka"
  ]
  node [
    id 1095
    label "Rosja"
  ]
  node [
    id 1096
    label "Rwanda"
  ]
  node [
    id 1097
    label "holoarktyka"
  ]
  node [
    id 1098
    label "Nigeria"
  ]
  node [
    id 1099
    label "USA"
  ]
  node [
    id 1100
    label "Oman"
  ]
  node [
    id 1101
    label "Luksemburg"
  ]
  node [
    id 1102
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1103
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1104
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1105
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1106
    label "Dominikana"
  ]
  node [
    id 1107
    label "Irlandia"
  ]
  node [
    id 1108
    label "Liban"
  ]
  node [
    id 1109
    label "Hanower"
  ]
  node [
    id 1110
    label "Estonia"
  ]
  node [
    id 1111
    label "Samoa"
  ]
  node [
    id 1112
    label "Nowa_Zelandia"
  ]
  node [
    id 1113
    label "Gabon"
  ]
  node [
    id 1114
    label "Iran"
  ]
  node [
    id 1115
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1116
    label "S&#322;owenia"
  ]
  node [
    id 1117
    label "Egipt"
  ]
  node [
    id 1118
    label "Kiribati"
  ]
  node [
    id 1119
    label "Togo"
  ]
  node [
    id 1120
    label "Mongolia"
  ]
  node [
    id 1121
    label "Sudan"
  ]
  node [
    id 1122
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1123
    label "Bahamy"
  ]
  node [
    id 1124
    label "Bangladesz"
  ]
  node [
    id 1125
    label "partia"
  ]
  node [
    id 1126
    label "Serbia"
  ]
  node [
    id 1127
    label "Czechy"
  ]
  node [
    id 1128
    label "Holandia"
  ]
  node [
    id 1129
    label "Birma"
  ]
  node [
    id 1130
    label "Albania"
  ]
  node [
    id 1131
    label "Mikronezja"
  ]
  node [
    id 1132
    label "Gambia"
  ]
  node [
    id 1133
    label "Kazachstan"
  ]
  node [
    id 1134
    label "interior"
  ]
  node [
    id 1135
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1136
    label "Malta"
  ]
  node [
    id 1137
    label "Lesoto"
  ]
  node [
    id 1138
    label "para"
  ]
  node [
    id 1139
    label "Antarktis"
  ]
  node [
    id 1140
    label "Andora"
  ]
  node [
    id 1141
    label "Nauru"
  ]
  node [
    id 1142
    label "Kuba"
  ]
  node [
    id 1143
    label "Wietnam"
  ]
  node [
    id 1144
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1145
    label "ziemia"
  ]
  node [
    id 1146
    label "Chorwacja"
  ]
  node [
    id 1147
    label "Kamerun"
  ]
  node [
    id 1148
    label "Urugwaj"
  ]
  node [
    id 1149
    label "Niger"
  ]
  node [
    id 1150
    label "Turkmenistan"
  ]
  node [
    id 1151
    label "Szwajcaria"
  ]
  node [
    id 1152
    label "zwrot"
  ]
  node [
    id 1153
    label "grupa"
  ]
  node [
    id 1154
    label "Litwa"
  ]
  node [
    id 1155
    label "Palau"
  ]
  node [
    id 1156
    label "Gruzja"
  ]
  node [
    id 1157
    label "Kongo"
  ]
  node [
    id 1158
    label "Tajwan"
  ]
  node [
    id 1159
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1160
    label "Honduras"
  ]
  node [
    id 1161
    label "Boliwia"
  ]
  node [
    id 1162
    label "Uganda"
  ]
  node [
    id 1163
    label "Namibia"
  ]
  node [
    id 1164
    label "Erytrea"
  ]
  node [
    id 1165
    label "Azerbejd&#380;an"
  ]
  node [
    id 1166
    label "Panama"
  ]
  node [
    id 1167
    label "Gujana"
  ]
  node [
    id 1168
    label "Somalia"
  ]
  node [
    id 1169
    label "Burundi"
  ]
  node [
    id 1170
    label "Tuwalu"
  ]
  node [
    id 1171
    label "Libia"
  ]
  node [
    id 1172
    label "Katar"
  ]
  node [
    id 1173
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1174
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1175
    label "Sahara_Zachodnia"
  ]
  node [
    id 1176
    label "Gwinea_Bissau"
  ]
  node [
    id 1177
    label "Bu&#322;garia"
  ]
  node [
    id 1178
    label "Tonga"
  ]
  node [
    id 1179
    label "Nikaragua"
  ]
  node [
    id 1180
    label "Fid&#380;i"
  ]
  node [
    id 1181
    label "Timor_Wschodni"
  ]
  node [
    id 1182
    label "Laos"
  ]
  node [
    id 1183
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1184
    label "Ghana"
  ]
  node [
    id 1185
    label "Brazylia"
  ]
  node [
    id 1186
    label "Belgia"
  ]
  node [
    id 1187
    label "Irak"
  ]
  node [
    id 1188
    label "Peru"
  ]
  node [
    id 1189
    label "Arabia_Saudyjska"
  ]
  node [
    id 1190
    label "Indonezja"
  ]
  node [
    id 1191
    label "Malediwy"
  ]
  node [
    id 1192
    label "Afganistan"
  ]
  node [
    id 1193
    label "Jordania"
  ]
  node [
    id 1194
    label "Kenia"
  ]
  node [
    id 1195
    label "Czad"
  ]
  node [
    id 1196
    label "Liberia"
  ]
  node [
    id 1197
    label "Mali"
  ]
  node [
    id 1198
    label "Armenia"
  ]
  node [
    id 1199
    label "W&#281;gry"
  ]
  node [
    id 1200
    label "Chile"
  ]
  node [
    id 1201
    label "Kanada"
  ]
  node [
    id 1202
    label "Cypr"
  ]
  node [
    id 1203
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1204
    label "Ekwador"
  ]
  node [
    id 1205
    label "Mo&#322;dawia"
  ]
  node [
    id 1206
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1207
    label "W&#322;ochy"
  ]
  node [
    id 1208
    label "Wyspy_Salomona"
  ]
  node [
    id 1209
    label "&#321;otwa"
  ]
  node [
    id 1210
    label "D&#380;ibuti"
  ]
  node [
    id 1211
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1212
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1213
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1214
    label "Portugalia"
  ]
  node [
    id 1215
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1216
    label "Maroko"
  ]
  node [
    id 1217
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1218
    label "Francja"
  ]
  node [
    id 1219
    label "Botswana"
  ]
  node [
    id 1220
    label "Dominika"
  ]
  node [
    id 1221
    label "Paragwaj"
  ]
  node [
    id 1222
    label "Tad&#380;ykistan"
  ]
  node [
    id 1223
    label "Haiti"
  ]
  node [
    id 1224
    label "Khitai"
  ]
  node [
    id 1225
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1226
    label "poker"
  ]
  node [
    id 1227
    label "nale&#380;e&#263;"
  ]
  node [
    id 1228
    label "odparowanie"
  ]
  node [
    id 1229
    label "sztuka"
  ]
  node [
    id 1230
    label "smoke"
  ]
  node [
    id 1231
    label "odparowa&#263;"
  ]
  node [
    id 1232
    label "parowanie"
  ]
  node [
    id 1233
    label "chodzi&#263;"
  ]
  node [
    id 1234
    label "pair"
  ]
  node [
    id 1235
    label "odparowywa&#263;"
  ]
  node [
    id 1236
    label "dodatek"
  ]
  node [
    id 1237
    label "odparowywanie"
  ]
  node [
    id 1238
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1239
    label "damp"
  ]
  node [
    id 1240
    label "wyparowanie"
  ]
  node [
    id 1241
    label "gaz_cieplarniany"
  ]
  node [
    id 1242
    label "gaz"
  ]
  node [
    id 1243
    label "jednostka_administracyjna"
  ]
  node [
    id 1244
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1245
    label "obszar"
  ]
  node [
    id 1246
    label "Jukon"
  ]
  node [
    id 1247
    label "punkt"
  ]
  node [
    id 1248
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1249
    label "fraza_czasownikowa"
  ]
  node [
    id 1250
    label "turning"
  ]
  node [
    id 1251
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1252
    label "skr&#281;t"
  ]
  node [
    id 1253
    label "jednostka_leksykalna"
  ]
  node [
    id 1254
    label "obr&#243;t"
  ]
  node [
    id 1255
    label "kompozycja"
  ]
  node [
    id 1256
    label "pakiet_klimatyczny"
  ]
  node [
    id 1257
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1258
    label "type"
  ]
  node [
    id 1259
    label "cz&#261;steczka"
  ]
  node [
    id 1260
    label "gromada"
  ]
  node [
    id 1261
    label "specgrupa"
  ]
  node [
    id 1262
    label "stage_set"
  ]
  node [
    id 1263
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1264
    label "harcerze_starsi"
  ]
  node [
    id 1265
    label "jednostka_systematyczna"
  ]
  node [
    id 1266
    label "oddzia&#322;"
  ]
  node [
    id 1267
    label "category"
  ]
  node [
    id 1268
    label "liga"
  ]
  node [
    id 1269
    label "&#346;wietliki"
  ]
  node [
    id 1270
    label "formacja_geologiczna"
  ]
  node [
    id 1271
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1272
    label "Eurogrupa"
  ]
  node [
    id 1273
    label "Terranie"
  ]
  node [
    id 1274
    label "odm&#322;adzanie"
  ]
  node [
    id 1275
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1276
    label "Entuzjastki"
  ]
  node [
    id 1277
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1278
    label "AWS"
  ]
  node [
    id 1279
    label "ZChN"
  ]
  node [
    id 1280
    label "Bund"
  ]
  node [
    id 1281
    label "PPR"
  ]
  node [
    id 1282
    label "blok"
  ]
  node [
    id 1283
    label "egzekutywa"
  ]
  node [
    id 1284
    label "Wigowie"
  ]
  node [
    id 1285
    label "aktyw"
  ]
  node [
    id 1286
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1287
    label "Razem"
  ]
  node [
    id 1288
    label "unit"
  ]
  node [
    id 1289
    label "wybranka"
  ]
  node [
    id 1290
    label "SLD"
  ]
  node [
    id 1291
    label "ZSL"
  ]
  node [
    id 1292
    label "Kuomintang"
  ]
  node [
    id 1293
    label "si&#322;a"
  ]
  node [
    id 1294
    label "PiS"
  ]
  node [
    id 1295
    label "Jakobici"
  ]
  node [
    id 1296
    label "materia&#322;"
  ]
  node [
    id 1297
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1298
    label "package"
  ]
  node [
    id 1299
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1300
    label "PO"
  ]
  node [
    id 1301
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1302
    label "game"
  ]
  node [
    id 1303
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1304
    label "niedoczas"
  ]
  node [
    id 1305
    label "Federali&#347;ci"
  ]
  node [
    id 1306
    label "PSL"
  ]
  node [
    id 1307
    label "przyroda"
  ]
  node [
    id 1308
    label "ro&#347;lina"
  ]
  node [
    id 1309
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1310
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1311
    label "biom"
  ]
  node [
    id 1312
    label "geosystem"
  ]
  node [
    id 1313
    label "szata_ro&#347;linna"
  ]
  node [
    id 1314
    label "zielono&#347;&#263;"
  ]
  node [
    id 1315
    label "pi&#281;tro"
  ]
  node [
    id 1316
    label "sol"
  ]
  node [
    id 1317
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1318
    label "inti"
  ]
  node [
    id 1319
    label "Afryka_Zachodnia"
  ]
  node [
    id 1320
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1321
    label "baht"
  ]
  node [
    id 1322
    label "boliviano"
  ]
  node [
    id 1323
    label "dong"
  ]
  node [
    id 1324
    label "Annam"
  ]
  node [
    id 1325
    label "Tonkin"
  ]
  node [
    id 1326
    label "Ameryka_Centralna"
  ]
  node [
    id 1327
    label "colon"
  ]
  node [
    id 1328
    label "Romania"
  ]
  node [
    id 1329
    label "Ok&#281;cie"
  ]
  node [
    id 1330
    label "Kalabria"
  ]
  node [
    id 1331
    label "Liguria"
  ]
  node [
    id 1332
    label "Apulia"
  ]
  node [
    id 1333
    label "Piemont"
  ]
  node [
    id 1334
    label "Umbria"
  ]
  node [
    id 1335
    label "lir"
  ]
  node [
    id 1336
    label "Lombardia"
  ]
  node [
    id 1337
    label "Warszawa"
  ]
  node [
    id 1338
    label "Karyntia"
  ]
  node [
    id 1339
    label "Sardynia"
  ]
  node [
    id 1340
    label "Toskania"
  ]
  node [
    id 1341
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1342
    label "Italia"
  ]
  node [
    id 1343
    label "Kampania"
  ]
  node [
    id 1344
    label "strefa_euro"
  ]
  node [
    id 1345
    label "Sycylia"
  ]
  node [
    id 1346
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1347
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1348
    label "lari"
  ]
  node [
    id 1349
    label "Ad&#380;aria"
  ]
  node [
    id 1350
    label "dolar_Belize"
  ]
  node [
    id 1351
    label "Jukatan"
  ]
  node [
    id 1352
    label "Hudson"
  ]
  node [
    id 1353
    label "Teksas"
  ]
  node [
    id 1354
    label "Georgia"
  ]
  node [
    id 1355
    label "dolar"
  ]
  node [
    id 1356
    label "Maryland"
  ]
  node [
    id 1357
    label "Michigan"
  ]
  node [
    id 1358
    label "Massachusetts"
  ]
  node [
    id 1359
    label "Luizjana"
  ]
  node [
    id 1360
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1361
    label "stan_wolny"
  ]
  node [
    id 1362
    label "Floryda"
  ]
  node [
    id 1363
    label "Po&#322;udnie"
  ]
  node [
    id 1364
    label "Ohio"
  ]
  node [
    id 1365
    label "Alaska"
  ]
  node [
    id 1366
    label "Nowy_Meksyk"
  ]
  node [
    id 1367
    label "Wuj_Sam"
  ]
  node [
    id 1368
    label "Kansas"
  ]
  node [
    id 1369
    label "Alabama"
  ]
  node [
    id 1370
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1371
    label "Kalifornia"
  ]
  node [
    id 1372
    label "Wirginia"
  ]
  node [
    id 1373
    label "Nowy_York"
  ]
  node [
    id 1374
    label "Waszyngton"
  ]
  node [
    id 1375
    label "Pensylwania"
  ]
  node [
    id 1376
    label "zielona_karta"
  ]
  node [
    id 1377
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1378
    label "P&#243;&#322;noc"
  ]
  node [
    id 1379
    label "Hawaje"
  ]
  node [
    id 1380
    label "Zach&#243;d"
  ]
  node [
    id 1381
    label "Illinois"
  ]
  node [
    id 1382
    label "Oklahoma"
  ]
  node [
    id 1383
    label "Oregon"
  ]
  node [
    id 1384
    label "Arizona"
  ]
  node [
    id 1385
    label "somoni"
  ]
  node [
    id 1386
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1387
    label "Sand&#380;ak"
  ]
  node [
    id 1388
    label "euro"
  ]
  node [
    id 1389
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1390
    label "perper"
  ]
  node [
    id 1391
    label "Bengal"
  ]
  node [
    id 1392
    label "taka"
  ]
  node [
    id 1393
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1394
    label "&#321;adoga"
  ]
  node [
    id 1395
    label "Dniepr"
  ]
  node [
    id 1396
    label "Kamczatka"
  ]
  node [
    id 1397
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1398
    label "Witim"
  ]
  node [
    id 1399
    label "Tuwa"
  ]
  node [
    id 1400
    label "Czeczenia"
  ]
  node [
    id 1401
    label "Ajon"
  ]
  node [
    id 1402
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1403
    label "car"
  ]
  node [
    id 1404
    label "Karelia"
  ]
  node [
    id 1405
    label "Don"
  ]
  node [
    id 1406
    label "Mordowia"
  ]
  node [
    id 1407
    label "Czuwaszja"
  ]
  node [
    id 1408
    label "Udmurcja"
  ]
  node [
    id 1409
    label "Jama&#322;"
  ]
  node [
    id 1410
    label "Azja"
  ]
  node [
    id 1411
    label "Newa"
  ]
  node [
    id 1412
    label "Adygeja"
  ]
  node [
    id 1413
    label "Inguszetia"
  ]
  node [
    id 1414
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1415
    label "Mari_El"
  ]
  node [
    id 1416
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1417
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1418
    label "Wszechrosja"
  ]
  node [
    id 1419
    label "rubel_rosyjski"
  ]
  node [
    id 1420
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1421
    label "Dagestan"
  ]
  node [
    id 1422
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1423
    label "Komi"
  ]
  node [
    id 1424
    label "Tatarstan"
  ]
  node [
    id 1425
    label "Baszkiria"
  ]
  node [
    id 1426
    label "Perm"
  ]
  node [
    id 1427
    label "Syberia"
  ]
  node [
    id 1428
    label "Chakasja"
  ]
  node [
    id 1429
    label "Europa_Wschodnia"
  ]
  node [
    id 1430
    label "Anadyr"
  ]
  node [
    id 1431
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1432
    label "Karaiby"
  ]
  node [
    id 1433
    label "gourde"
  ]
  node [
    id 1434
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1435
    label "kwanza"
  ]
  node [
    id 1436
    label "escudo_angolskie"
  ]
  node [
    id 1437
    label "Afryka_Wschodnia"
  ]
  node [
    id 1438
    label "ariary"
  ]
  node [
    id 1439
    label "Ocean_Indyjski"
  ]
  node [
    id 1440
    label "frank_malgaski"
  ]
  node [
    id 1441
    label "&#379;mud&#378;"
  ]
  node [
    id 1442
    label "Windawa"
  ]
  node [
    id 1443
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1444
    label "lit"
  ]
  node [
    id 1445
    label "funt_egipski"
  ]
  node [
    id 1446
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1447
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1448
    label "Synaj"
  ]
  node [
    id 1449
    label "paraszyt"
  ]
  node [
    id 1450
    label "Amhara"
  ]
  node [
    id 1451
    label "negus"
  ]
  node [
    id 1452
    label "birr"
  ]
  node [
    id 1453
    label "Syjon"
  ]
  node [
    id 1454
    label "peso_kolumbijskie"
  ]
  node [
    id 1455
    label "Orinoko"
  ]
  node [
    id 1456
    label "rial_katarski"
  ]
  node [
    id 1457
    label "dram"
  ]
  node [
    id 1458
    label "Brabancja"
  ]
  node [
    id 1459
    label "Limburgia"
  ]
  node [
    id 1460
    label "Zelandia"
  ]
  node [
    id 1461
    label "gulden"
  ]
  node [
    id 1462
    label "Niderlandy"
  ]
  node [
    id 1463
    label "cedi"
  ]
  node [
    id 1464
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1465
    label "real"
  ]
  node [
    id 1466
    label "cruzado"
  ]
  node [
    id 1467
    label "milrejs"
  ]
  node [
    id 1468
    label "frank_monakijski"
  ]
  node [
    id 1469
    label "Fryburg"
  ]
  node [
    id 1470
    label "frank_szwajcarski"
  ]
  node [
    id 1471
    label "Bazylea"
  ]
  node [
    id 1472
    label "Helwecja"
  ]
  node [
    id 1473
    label "Alpy"
  ]
  node [
    id 1474
    label "Berno"
  ]
  node [
    id 1475
    label "Europa_Zachodnia"
  ]
  node [
    id 1476
    label "Naddniestrze"
  ]
  node [
    id 1477
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1478
    label "Ba&#322;kany"
  ]
  node [
    id 1479
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1480
    label "Dniestr"
  ]
  node [
    id 1481
    label "Gagauzja"
  ]
  node [
    id 1482
    label "rupia_indyjska"
  ]
  node [
    id 1483
    label "Kerala"
  ]
  node [
    id 1484
    label "Indie_Zachodnie"
  ]
  node [
    id 1485
    label "Kaszmir"
  ]
  node [
    id 1486
    label "Indie_Wschodnie"
  ]
  node [
    id 1487
    label "Pend&#380;ab"
  ]
  node [
    id 1488
    label "Asam"
  ]
  node [
    id 1489
    label "Indie_Portugalskie"
  ]
  node [
    id 1490
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1491
    label "Bollywood"
  ]
  node [
    id 1492
    label "Sikkim"
  ]
  node [
    id 1493
    label "boliwar"
  ]
  node [
    id 1494
    label "naira"
  ]
  node [
    id 1495
    label "frank_gwinejski"
  ]
  node [
    id 1496
    label "Karaka&#322;pacja"
  ]
  node [
    id 1497
    label "dolar_liberyjski"
  ]
  node [
    id 1498
    label "Dacja"
  ]
  node [
    id 1499
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1500
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1501
    label "Dobrud&#380;a"
  ]
  node [
    id 1502
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1503
    label "dolar_namibijski"
  ]
  node [
    id 1504
    label "kuna"
  ]
  node [
    id 1505
    label "Karlsbad"
  ]
  node [
    id 1506
    label "Turyngia"
  ]
  node [
    id 1507
    label "Brandenburgia"
  ]
  node [
    id 1508
    label "marka"
  ]
  node [
    id 1509
    label "Saksonia"
  ]
  node [
    id 1510
    label "Szlezwik"
  ]
  node [
    id 1511
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1512
    label "Frankonia"
  ]
  node [
    id 1513
    label "Rugia"
  ]
  node [
    id 1514
    label "Helgoland"
  ]
  node [
    id 1515
    label "Bawaria"
  ]
  node [
    id 1516
    label "Holsztyn"
  ]
  node [
    id 1517
    label "Badenia"
  ]
  node [
    id 1518
    label "Wirtembergia"
  ]
  node [
    id 1519
    label "Nadrenia"
  ]
  node [
    id 1520
    label "Anglosas"
  ]
  node [
    id 1521
    label "Hesja"
  ]
  node [
    id 1522
    label "Dolna_Saksonia"
  ]
  node [
    id 1523
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1524
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1525
    label "Germania"
  ]
  node [
    id 1526
    label "Po&#322;abie"
  ]
  node [
    id 1527
    label "Szwabia"
  ]
  node [
    id 1528
    label "Westfalia"
  ]
  node [
    id 1529
    label "Lipt&#243;w"
  ]
  node [
    id 1530
    label "korona_w&#281;gierska"
  ]
  node [
    id 1531
    label "forint"
  ]
  node [
    id 1532
    label "tenge"
  ]
  node [
    id 1533
    label "afgani"
  ]
  node [
    id 1534
    label "Baktria"
  ]
  node [
    id 1535
    label "szach"
  ]
  node [
    id 1536
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1537
    label "kip"
  ]
  node [
    id 1538
    label "Dyja"
  ]
  node [
    id 1539
    label "Tyrol"
  ]
  node [
    id 1540
    label "Salzburg"
  ]
  node [
    id 1541
    label "konsulent"
  ]
  node [
    id 1542
    label "Rakuzy"
  ]
  node [
    id 1543
    label "peso_urugwajskie"
  ]
  node [
    id 1544
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1545
    label "marka_esto&#324;ska"
  ]
  node [
    id 1546
    label "Inflanty"
  ]
  node [
    id 1547
    label "Skandynawia"
  ]
  node [
    id 1548
    label "korona_esto&#324;ska"
  ]
  node [
    id 1549
    label "Oceania"
  ]
  node [
    id 1550
    label "tala"
  ]
  node [
    id 1551
    label "Polinezja"
  ]
  node [
    id 1552
    label "Ma&#322;orosja"
  ]
  node [
    id 1553
    label "Podole"
  ]
  node [
    id 1554
    label "Przykarpacie"
  ]
  node [
    id 1555
    label "Zaporo&#380;e"
  ]
  node [
    id 1556
    label "Kozaczyzna"
  ]
  node [
    id 1557
    label "Naddnieprze"
  ]
  node [
    id 1558
    label "Wsch&#243;d"
  ]
  node [
    id 1559
    label "karbowaniec"
  ]
  node [
    id 1560
    label "hrywna"
  ]
  node [
    id 1561
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1562
    label "Krym"
  ]
  node [
    id 1563
    label "Zakarpacie"
  ]
  node [
    id 1564
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1565
    label "riel"
  ]
  node [
    id 1566
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1567
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1568
    label "Arakan"
  ]
  node [
    id 1569
    label "kyat"
  ]
  node [
    id 1570
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1571
    label "funt_liba&#324;ski"
  ]
  node [
    id 1572
    label "Mariany"
  ]
  node [
    id 1573
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1574
    label "Maghreb"
  ]
  node [
    id 1575
    label "dinar_algierski"
  ]
  node [
    id 1576
    label "Kabylia"
  ]
  node [
    id 1577
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1578
    label "Borneo"
  ]
  node [
    id 1579
    label "ringgit"
  ]
  node [
    id 1580
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1581
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1582
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1583
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1584
    label "Judea"
  ]
  node [
    id 1585
    label "lira_izraelska"
  ]
  node [
    id 1586
    label "Galilea"
  ]
  node [
    id 1587
    label "szekel"
  ]
  node [
    id 1588
    label "tolar"
  ]
  node [
    id 1589
    label "frank_luksemburski"
  ]
  node [
    id 1590
    label "lempira"
  ]
  node [
    id 1591
    label "Pozna&#324;"
  ]
  node [
    id 1592
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1593
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1594
    label "Gozo"
  ]
  node [
    id 1595
    label "lira_malta&#324;ska"
  ]
  node [
    id 1596
    label "Lesbos"
  ]
  node [
    id 1597
    label "Tesalia"
  ]
  node [
    id 1598
    label "Eolia"
  ]
  node [
    id 1599
    label "panhellenizm"
  ]
  node [
    id 1600
    label "Achaja"
  ]
  node [
    id 1601
    label "Kreta"
  ]
  node [
    id 1602
    label "Peloponez"
  ]
  node [
    id 1603
    label "Olimp"
  ]
  node [
    id 1604
    label "drachma"
  ]
  node [
    id 1605
    label "Termopile"
  ]
  node [
    id 1606
    label "Rodos"
  ]
  node [
    id 1607
    label "palestra"
  ]
  node [
    id 1608
    label "Eubea"
  ]
  node [
    id 1609
    label "Paros"
  ]
  node [
    id 1610
    label "Hellada"
  ]
  node [
    id 1611
    label "Beocja"
  ]
  node [
    id 1612
    label "Parnas"
  ]
  node [
    id 1613
    label "Etolia"
  ]
  node [
    id 1614
    label "Attyka"
  ]
  node [
    id 1615
    label "Epir"
  ]
  node [
    id 1616
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1617
    label "Ulster"
  ]
  node [
    id 1618
    label "Atlantyk"
  ]
  node [
    id 1619
    label "funt_irlandzki"
  ]
  node [
    id 1620
    label "tugrik"
  ]
  node [
    id 1621
    label "Azja_Wschodnia"
  ]
  node [
    id 1622
    label "ajmak"
  ]
  node [
    id 1623
    label "Buriaci"
  ]
  node [
    id 1624
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1625
    label "frank_francuski"
  ]
  node [
    id 1626
    label "Lotaryngia"
  ]
  node [
    id 1627
    label "Alzacja"
  ]
  node [
    id 1628
    label "Gwadelupa"
  ]
  node [
    id 1629
    label "Bordeaux"
  ]
  node [
    id 1630
    label "Pikardia"
  ]
  node [
    id 1631
    label "Sabaudia"
  ]
  node [
    id 1632
    label "Korsyka"
  ]
  node [
    id 1633
    label "Bretania"
  ]
  node [
    id 1634
    label "Masyw_Centralny"
  ]
  node [
    id 1635
    label "Armagnac"
  ]
  node [
    id 1636
    label "Akwitania"
  ]
  node [
    id 1637
    label "Wandea"
  ]
  node [
    id 1638
    label "Martynika"
  ]
  node [
    id 1639
    label "Prowansja"
  ]
  node [
    id 1640
    label "Sekwana"
  ]
  node [
    id 1641
    label "Normandia"
  ]
  node [
    id 1642
    label "Burgundia"
  ]
  node [
    id 1643
    label "Gaskonia"
  ]
  node [
    id 1644
    label "Langwedocja"
  ]
  node [
    id 1645
    label "lew"
  ]
  node [
    id 1646
    label "c&#243;rdoba"
  ]
  node [
    id 1647
    label "dolar_Zimbabwe"
  ]
  node [
    id 1648
    label "frank_rwandyjski"
  ]
  node [
    id 1649
    label "kwacha_zambijska"
  ]
  node [
    id 1650
    label "Liwonia"
  ]
  node [
    id 1651
    label "Kurlandia"
  ]
  node [
    id 1652
    label "rubel_&#322;otewski"
  ]
  node [
    id 1653
    label "&#322;at"
  ]
  node [
    id 1654
    label "rupia_nepalska"
  ]
  node [
    id 1655
    label "Himalaje"
  ]
  node [
    id 1656
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1657
    label "funt_suda&#324;ski"
  ]
  node [
    id 1658
    label "Wielka_Bahama"
  ]
  node [
    id 1659
    label "dolar_bahamski"
  ]
  node [
    id 1660
    label "Antyle"
  ]
  node [
    id 1661
    label "dolar_Tuvalu"
  ]
  node [
    id 1662
    label "dinar_iracki"
  ]
  node [
    id 1663
    label "korona_s&#322;owacka"
  ]
  node [
    id 1664
    label "Turiec"
  ]
  node [
    id 1665
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1666
    label "jen"
  ]
  node [
    id 1667
    label "Okinawa"
  ]
  node [
    id 1668
    label "jinja"
  ]
  node [
    id 1669
    label "Japonica"
  ]
  node [
    id 1670
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1671
    label "szyling_kenijski"
  ]
  node [
    id 1672
    label "peso_chilijskie"
  ]
  node [
    id 1673
    label "Zanzibar"
  ]
  node [
    id 1674
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1675
    label "Cebu"
  ]
  node [
    id 1676
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1677
    label "Sahara"
  ]
  node [
    id 1678
    label "Tasmania"
  ]
  node [
    id 1679
    label "dolar_australijski"
  ]
  node [
    id 1680
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1681
    label "Nowa_Fundlandia"
  ]
  node [
    id 1682
    label "Quebec"
  ]
  node [
    id 1683
    label "dolar_kanadyjski"
  ]
  node [
    id 1684
    label "quetzal"
  ]
  node [
    id 1685
    label "Manica"
  ]
  node [
    id 1686
    label "Inhambane"
  ]
  node [
    id 1687
    label "Maputo"
  ]
  node [
    id 1688
    label "Nampula"
  ]
  node [
    id 1689
    label "metical"
  ]
  node [
    id 1690
    label "escudo_mozambickie"
  ]
  node [
    id 1691
    label "Gaza"
  ]
  node [
    id 1692
    label "Niasa"
  ]
  node [
    id 1693
    label "Cabo_Delgado"
  ]
  node [
    id 1694
    label "dinar_tunezyjski"
  ]
  node [
    id 1695
    label "frank_tunezyjski"
  ]
  node [
    id 1696
    label "lud"
  ]
  node [
    id 1697
    label "frank_kongijski"
  ]
  node [
    id 1698
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1699
    label "dinar_Bahrajnu"
  ]
  node [
    id 1700
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1701
    label "escudo_portugalskie"
  ]
  node [
    id 1702
    label "Melanezja"
  ]
  node [
    id 1703
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1704
    label "dinar_libijski"
  ]
  node [
    id 1705
    label "d&#380;amahirijja"
  ]
  node [
    id 1706
    label "balboa"
  ]
  node [
    id 1707
    label "dolar_surinamski"
  ]
  node [
    id 1708
    label "dolar_Brunei"
  ]
  node [
    id 1709
    label "Walencja"
  ]
  node [
    id 1710
    label "Galicja"
  ]
  node [
    id 1711
    label "Aragonia"
  ]
  node [
    id 1712
    label "Estremadura"
  ]
  node [
    id 1713
    label "Rzym_Zachodni"
  ]
  node [
    id 1714
    label "Baskonia"
  ]
  node [
    id 1715
    label "Katalonia"
  ]
  node [
    id 1716
    label "Majorka"
  ]
  node [
    id 1717
    label "Asturia"
  ]
  node [
    id 1718
    label "Andaluzja"
  ]
  node [
    id 1719
    label "Kastylia"
  ]
  node [
    id 1720
    label "peseta"
  ]
  node [
    id 1721
    label "hacjender"
  ]
  node [
    id 1722
    label "Luksemburgia"
  ]
  node [
    id 1723
    label "Flandria"
  ]
  node [
    id 1724
    label "Walonia"
  ]
  node [
    id 1725
    label "frank_belgijski"
  ]
  node [
    id 1726
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1727
    label "dolar_Barbadosu"
  ]
  node [
    id 1728
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1729
    label "Lasko"
  ]
  node [
    id 1730
    label "korona_czeska"
  ]
  node [
    id 1731
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1732
    label "Wojwodina"
  ]
  node [
    id 1733
    label "dinar_serbski"
  ]
  node [
    id 1734
    label "alawizm"
  ]
  node [
    id 1735
    label "funt_syryjski"
  ]
  node [
    id 1736
    label "Szantung"
  ]
  node [
    id 1737
    label "Mand&#380;uria"
  ]
  node [
    id 1738
    label "Hongkong"
  ]
  node [
    id 1739
    label "Guangdong"
  ]
  node [
    id 1740
    label "yuan"
  ]
  node [
    id 1741
    label "D&#380;ungaria"
  ]
  node [
    id 1742
    label "Chiny_Zachodnie"
  ]
  node [
    id 1743
    label "Junnan"
  ]
  node [
    id 1744
    label "Kuantung"
  ]
  node [
    id 1745
    label "Chiny_Wschodnie"
  ]
  node [
    id 1746
    label "Syczuan"
  ]
  node [
    id 1747
    label "Katanga"
  ]
  node [
    id 1748
    label "zair"
  ]
  node [
    id 1749
    label "ugija"
  ]
  node [
    id 1750
    label "dalasi"
  ]
  node [
    id 1751
    label "Afrodyzje"
  ]
  node [
    id 1752
    label "funt_cypryjski"
  ]
  node [
    id 1753
    label "lek"
  ]
  node [
    id 1754
    label "frank_alba&#324;ski"
  ]
  node [
    id 1755
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1756
    label "kafar"
  ]
  node [
    id 1757
    label "dolar_jamajski"
  ]
  node [
    id 1758
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1759
    label "Ocean_Spokojny"
  ]
  node [
    id 1760
    label "som"
  ]
  node [
    id 1761
    label "guarani"
  ]
  node [
    id 1762
    label "Persja"
  ]
  node [
    id 1763
    label "rial_ira&#324;ski"
  ]
  node [
    id 1764
    label "mu&#322;&#322;a"
  ]
  node [
    id 1765
    label "rupia_indonezyjska"
  ]
  node [
    id 1766
    label "Jawa"
  ]
  node [
    id 1767
    label "Moluki"
  ]
  node [
    id 1768
    label "Nowa_Gwinea"
  ]
  node [
    id 1769
    label "Sumatra"
  ]
  node [
    id 1770
    label "szyling_somalijski"
  ]
  node [
    id 1771
    label "szyling_ugandyjski"
  ]
  node [
    id 1772
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1773
    label "Azja_Mniejsza"
  ]
  node [
    id 1774
    label "lira_turecka"
  ]
  node [
    id 1775
    label "Ujgur"
  ]
  node [
    id 1776
    label "Pireneje"
  ]
  node [
    id 1777
    label "nakfa"
  ]
  node [
    id 1778
    label "won"
  ]
  node [
    id 1779
    label "&#346;wite&#378;"
  ]
  node [
    id 1780
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1781
    label "dinar_kuwejcki"
  ]
  node [
    id 1782
    label "Karabach"
  ]
  node [
    id 1783
    label "manat_azerski"
  ]
  node [
    id 1784
    label "Nachiczewan"
  ]
  node [
    id 1785
    label "dolar_Kiribati"
  ]
  node [
    id 1786
    label "posadzka"
  ]
  node [
    id 1787
    label "podglebie"
  ]
  node [
    id 1788
    label "Ko&#322;yma"
  ]
  node [
    id 1789
    label "Indochiny"
  ]
  node [
    id 1790
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1791
    label "Bo&#347;nia"
  ]
  node [
    id 1792
    label "Kaukaz"
  ]
  node [
    id 1793
    label "Opolszczyzna"
  ]
  node [
    id 1794
    label "czynnik_produkcji"
  ]
  node [
    id 1795
    label "kort"
  ]
  node [
    id 1796
    label "Polesie"
  ]
  node [
    id 1797
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1798
    label "Yorkshire"
  ]
  node [
    id 1799
    label "zapadnia"
  ]
  node [
    id 1800
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1801
    label "Noworosja"
  ]
  node [
    id 1802
    label "glinowa&#263;"
  ]
  node [
    id 1803
    label "litosfera"
  ]
  node [
    id 1804
    label "Kurpie"
  ]
  node [
    id 1805
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1806
    label "Kociewie"
  ]
  node [
    id 1807
    label "Anglia"
  ]
  node [
    id 1808
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1809
    label "Laponia"
  ]
  node [
    id 1810
    label "Amazonia"
  ]
  node [
    id 1811
    label "Hercegowina"
  ]
  node [
    id 1812
    label "przestrze&#324;"
  ]
  node [
    id 1813
    label "Pamir"
  ]
  node [
    id 1814
    label "powierzchnia"
  ]
  node [
    id 1815
    label "Podhale"
  ]
  node [
    id 1816
    label "pomieszczenie"
  ]
  node [
    id 1817
    label "plantowa&#263;"
  ]
  node [
    id 1818
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1819
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1820
    label "dotleni&#263;"
  ]
  node [
    id 1821
    label "Zabajkale"
  ]
  node [
    id 1822
    label "skorupa_ziemska"
  ]
  node [
    id 1823
    label "glinowanie"
  ]
  node [
    id 1824
    label "Kaszuby"
  ]
  node [
    id 1825
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1826
    label "Oksytania"
  ]
  node [
    id 1827
    label "Mezoameryka"
  ]
  node [
    id 1828
    label "Turkiestan"
  ]
  node [
    id 1829
    label "Kurdystan"
  ]
  node [
    id 1830
    label "glej"
  ]
  node [
    id 1831
    label "Biskupizna"
  ]
  node [
    id 1832
    label "Podbeskidzie"
  ]
  node [
    id 1833
    label "Zag&#243;rze"
  ]
  node [
    id 1834
    label "Szkocja"
  ]
  node [
    id 1835
    label "domain"
  ]
  node [
    id 1836
    label "Huculszczyzna"
  ]
  node [
    id 1837
    label "pojazd"
  ]
  node [
    id 1838
    label "budynek"
  ]
  node [
    id 1839
    label "S&#261;decczyzna"
  ]
  node [
    id 1840
    label "Palestyna"
  ]
  node [
    id 1841
    label "Lauda"
  ]
  node [
    id 1842
    label "penetrator"
  ]
  node [
    id 1843
    label "Bojkowszczyzna"
  ]
  node [
    id 1844
    label "ryzosfera"
  ]
  node [
    id 1845
    label "Zamojszczyzna"
  ]
  node [
    id 1846
    label "Walia"
  ]
  node [
    id 1847
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1848
    label "martwica"
  ]
  node [
    id 1849
    label "pr&#243;chnica"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 239
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 888
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 816
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 990
  ]
  edge [
    source 14
    target 991
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 993
  ]
  edge [
    source 14
    target 994
  ]
  edge [
    source 14
    target 995
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 997
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 1007
  ]
  edge [
    source 16
    target 1008
  ]
  edge [
    source 16
    target 1009
  ]
  edge [
    source 16
    target 1010
  ]
  edge [
    source 16
    target 1011
  ]
  edge [
    source 16
    target 1012
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 1013
  ]
  edge [
    source 16
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1120
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 17
    target 1124
  ]
  edge [
    source 17
    target 1125
  ]
  edge [
    source 17
    target 1126
  ]
  edge [
    source 17
    target 1127
  ]
  edge [
    source 17
    target 1128
  ]
  edge [
    source 17
    target 1129
  ]
  edge [
    source 17
    target 1130
  ]
  edge [
    source 17
    target 1131
  ]
  edge [
    source 17
    target 1132
  ]
  edge [
    source 17
    target 1133
  ]
  edge [
    source 17
    target 1134
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 1135
  ]
  edge [
    source 17
    target 1136
  ]
  edge [
    source 17
    target 1137
  ]
  edge [
    source 17
    target 1138
  ]
  edge [
    source 17
    target 1139
  ]
  edge [
    source 17
    target 1140
  ]
  edge [
    source 17
    target 1141
  ]
  edge [
    source 17
    target 1142
  ]
  edge [
    source 17
    target 1143
  ]
  edge [
    source 17
    target 1144
  ]
  edge [
    source 17
    target 1145
  ]
  edge [
    source 17
    target 1146
  ]
  edge [
    source 17
    target 1147
  ]
  edge [
    source 17
    target 1148
  ]
  edge [
    source 17
    target 1149
  ]
  edge [
    source 17
    target 1150
  ]
  edge [
    source 17
    target 1151
  ]
  edge [
    source 17
    target 1152
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 1154
  ]
  edge [
    source 17
    target 1155
  ]
  edge [
    source 17
    target 1156
  ]
  edge [
    source 17
    target 1157
  ]
  edge [
    source 17
    target 1158
  ]
  edge [
    source 17
    target 1159
  ]
  edge [
    source 17
    target 1160
  ]
  edge [
    source 17
    target 1161
  ]
  edge [
    source 17
    target 1162
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 1164
  ]
  edge [
    source 17
    target 1165
  ]
  edge [
    source 17
    target 1166
  ]
  edge [
    source 17
    target 1167
  ]
  edge [
    source 17
    target 1168
  ]
  edge [
    source 17
    target 1169
  ]
  edge [
    source 17
    target 1170
  ]
  edge [
    source 17
    target 1171
  ]
  edge [
    source 17
    target 1172
  ]
  edge [
    source 17
    target 1173
  ]
  edge [
    source 17
    target 1174
  ]
  edge [
    source 17
    target 1175
  ]
  edge [
    source 17
    target 1176
  ]
  edge [
    source 17
    target 1177
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 1179
  ]
  edge [
    source 17
    target 1180
  ]
  edge [
    source 17
    target 1181
  ]
  edge [
    source 17
    target 1182
  ]
  edge [
    source 17
    target 1183
  ]
  edge [
    source 17
    target 1184
  ]
  edge [
    source 17
    target 1185
  ]
  edge [
    source 17
    target 1186
  ]
  edge [
    source 17
    target 1187
  ]
  edge [
    source 17
    target 1188
  ]
  edge [
    source 17
    target 1189
  ]
  edge [
    source 17
    target 1190
  ]
  edge [
    source 17
    target 1191
  ]
  edge [
    source 17
    target 1192
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 1194
  ]
  edge [
    source 17
    target 1195
  ]
  edge [
    source 17
    target 1196
  ]
  edge [
    source 17
    target 1197
  ]
  edge [
    source 17
    target 1198
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 1200
  ]
  edge [
    source 17
    target 1201
  ]
  edge [
    source 17
    target 1202
  ]
  edge [
    source 17
    target 1203
  ]
  edge [
    source 17
    target 1204
  ]
  edge [
    source 17
    target 1205
  ]
  edge [
    source 17
    target 1206
  ]
  edge [
    source 17
    target 1207
  ]
  edge [
    source 17
    target 1208
  ]
  edge [
    source 17
    target 1209
  ]
  edge [
    source 17
    target 1210
  ]
  edge [
    source 17
    target 1211
  ]
  edge [
    source 17
    target 1212
  ]
  edge [
    source 17
    target 1213
  ]
  edge [
    source 17
    target 1214
  ]
  edge [
    source 17
    target 1215
  ]
  edge [
    source 17
    target 1216
  ]
  edge [
    source 17
    target 1217
  ]
  edge [
    source 17
    target 1218
  ]
  edge [
    source 17
    target 1219
  ]
  edge [
    source 17
    target 1220
  ]
  edge [
    source 17
    target 1221
  ]
  edge [
    source 17
    target 1222
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 1223
  ]
  edge [
    source 17
    target 1224
  ]
  edge [
    source 17
    target 1225
  ]
  edge [
    source 17
    target 1226
  ]
  edge [
    source 17
    target 1227
  ]
  edge [
    source 17
    target 1228
  ]
  edge [
    source 17
    target 1229
  ]
  edge [
    source 17
    target 1230
  ]
  edge [
    source 17
    target 1231
  ]
  edge [
    source 17
    target 1232
  ]
  edge [
    source 17
    target 1233
  ]
  edge [
    source 17
    target 1234
  ]
  edge [
    source 17
    target 369
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 1235
  ]
  edge [
    source 17
    target 1236
  ]
  edge [
    source 17
    target 1237
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 1238
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 1239
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 1240
  ]
  edge [
    source 17
    target 1241
  ]
  edge [
    source 17
    target 1242
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 1243
  ]
  edge [
    source 17
    target 1244
  ]
  edge [
    source 17
    target 1245
  ]
  edge [
    source 17
    target 1246
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 54
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 474
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 476
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 17
    target 480
  ]
  edge [
    source 17
    target 481
  ]
  edge [
    source 17
    target 482
  ]
  edge [
    source 17
    target 483
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 1247
  ]
  edge [
    source 17
    target 393
  ]
  edge [
    source 17
    target 1248
  ]
  edge [
    source 17
    target 286
  ]
  edge [
    source 17
    target 499
  ]
  edge [
    source 17
    target 1249
  ]
  edge [
    source 17
    target 1250
  ]
  edge [
    source 17
    target 1251
  ]
  edge [
    source 17
    target 1252
  ]
  edge [
    source 17
    target 1253
  ]
  edge [
    source 17
    target 1254
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 1255
  ]
  edge [
    source 17
    target 1256
  ]
  edge [
    source 17
    target 1257
  ]
  edge [
    source 17
    target 1258
  ]
  edge [
    source 17
    target 1259
  ]
  edge [
    source 17
    target 1260
  ]
  edge [
    source 17
    target 1261
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 1262
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 1263
  ]
  edge [
    source 17
    target 1264
  ]
  edge [
    source 17
    target 1265
  ]
  edge [
    source 17
    target 1266
  ]
  edge [
    source 17
    target 1267
  ]
  edge [
    source 17
    target 1268
  ]
  edge [
    source 17
    target 1269
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1270
  ]
  edge [
    source 17
    target 1271
  ]
  edge [
    source 17
    target 1272
  ]
  edge [
    source 17
    target 1273
  ]
  edge [
    source 17
    target 1274
  ]
  edge [
    source 17
    target 1275
  ]
  edge [
    source 17
    target 1276
  ]
  edge [
    source 17
    target 1277
  ]
  edge [
    source 17
    target 1278
  ]
  edge [
    source 17
    target 1279
  ]
  edge [
    source 17
    target 1280
  ]
  edge [
    source 17
    target 1281
  ]
  edge [
    source 17
    target 1282
  ]
  edge [
    source 17
    target 1283
  ]
  edge [
    source 17
    target 1284
  ]
  edge [
    source 17
    target 1285
  ]
  edge [
    source 17
    target 1286
  ]
  edge [
    source 17
    target 1287
  ]
  edge [
    source 17
    target 1288
  ]
  edge [
    source 17
    target 1289
  ]
  edge [
    source 17
    target 1290
  ]
  edge [
    source 17
    target 1291
  ]
  edge [
    source 17
    target 1292
  ]
  edge [
    source 17
    target 1293
  ]
  edge [
    source 17
    target 1294
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 1295
  ]
  edge [
    source 17
    target 1296
  ]
  edge [
    source 17
    target 1297
  ]
  edge [
    source 17
    target 1298
  ]
  edge [
    source 17
    target 1299
  ]
  edge [
    source 17
    target 1300
  ]
  edge [
    source 17
    target 1301
  ]
  edge [
    source 17
    target 1302
  ]
  edge [
    source 17
    target 1303
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 1304
  ]
  edge [
    source 17
    target 1305
  ]
  edge [
    source 17
    target 1306
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 1307
  ]
  edge [
    source 17
    target 1308
  ]
  edge [
    source 17
    target 1309
  ]
  edge [
    source 17
    target 1310
  ]
  edge [
    source 17
    target 1311
  ]
  edge [
    source 17
    target 1312
  ]
  edge [
    source 17
    target 1313
  ]
  edge [
    source 17
    target 1314
  ]
  edge [
    source 17
    target 1315
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 1316
  ]
  edge [
    source 17
    target 1317
  ]
  edge [
    source 17
    target 1318
  ]
  edge [
    source 17
    target 1319
  ]
  edge [
    source 17
    target 1320
  ]
  edge [
    source 17
    target 1321
  ]
  edge [
    source 17
    target 1322
  ]
  edge [
    source 17
    target 1323
  ]
  edge [
    source 17
    target 1324
  ]
  edge [
    source 17
    target 1325
  ]
  edge [
    source 17
    target 1326
  ]
  edge [
    source 17
    target 1327
  ]
  edge [
    source 17
    target 1328
  ]
  edge [
    source 17
    target 1329
  ]
  edge [
    source 17
    target 1330
  ]
  edge [
    source 17
    target 1331
  ]
  edge [
    source 17
    target 1332
  ]
  edge [
    source 17
    target 1333
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 1334
  ]
  edge [
    source 17
    target 1335
  ]
  edge [
    source 17
    target 1336
  ]
  edge [
    source 17
    target 1337
  ]
  edge [
    source 17
    target 1338
  ]
  edge [
    source 17
    target 1339
  ]
  edge [
    source 17
    target 1340
  ]
  edge [
    source 17
    target 1341
  ]
  edge [
    source 17
    target 1342
  ]
  edge [
    source 17
    target 1343
  ]
  edge [
    source 17
    target 1344
  ]
  edge [
    source 17
    target 1345
  ]
  edge [
    source 17
    target 1346
  ]
  edge [
    source 17
    target 1347
  ]
  edge [
    source 17
    target 1348
  ]
  edge [
    source 17
    target 1349
  ]
  edge [
    source 17
    target 1350
  ]
  edge [
    source 17
    target 1351
  ]
  edge [
    source 17
    target 1352
  ]
  edge [
    source 17
    target 1353
  ]
  edge [
    source 17
    target 1354
  ]
  edge [
    source 17
    target 1355
  ]
  edge [
    source 17
    target 1356
  ]
  edge [
    source 17
    target 1357
  ]
  edge [
    source 17
    target 1358
  ]
  edge [
    source 17
    target 1359
  ]
  edge [
    source 17
    target 1360
  ]
  edge [
    source 17
    target 1361
  ]
  edge [
    source 17
    target 1362
  ]
  edge [
    source 17
    target 1363
  ]
  edge [
    source 17
    target 1364
  ]
  edge [
    source 17
    target 1365
  ]
  edge [
    source 17
    target 1366
  ]
  edge [
    source 17
    target 1367
  ]
  edge [
    source 17
    target 1368
  ]
  edge [
    source 17
    target 1369
  ]
  edge [
    source 17
    target 1370
  ]
  edge [
    source 17
    target 1371
  ]
  edge [
    source 17
    target 1372
  ]
  edge [
    source 17
    target 1373
  ]
  edge [
    source 17
    target 1374
  ]
  edge [
    source 17
    target 1375
  ]
  edge [
    source 17
    target 1376
  ]
  edge [
    source 17
    target 1377
  ]
  edge [
    source 17
    target 1378
  ]
  edge [
    source 17
    target 1379
  ]
  edge [
    source 17
    target 1380
  ]
  edge [
    source 17
    target 1381
  ]
  edge [
    source 17
    target 1382
  ]
  edge [
    source 17
    target 1383
  ]
  edge [
    source 17
    target 1384
  ]
  edge [
    source 17
    target 1385
  ]
  edge [
    source 17
    target 1386
  ]
  edge [
    source 17
    target 1387
  ]
  edge [
    source 17
    target 1388
  ]
  edge [
    source 17
    target 1389
  ]
  edge [
    source 17
    target 1390
  ]
  edge [
    source 17
    target 1391
  ]
  edge [
    source 17
    target 1392
  ]
  edge [
    source 17
    target 1393
  ]
  edge [
    source 17
    target 1394
  ]
  edge [
    source 17
    target 1395
  ]
  edge [
    source 17
    target 1396
  ]
  edge [
    source 17
    target 1397
  ]
  edge [
    source 17
    target 1398
  ]
  edge [
    source 17
    target 1399
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1401
  ]
  edge [
    source 17
    target 1402
  ]
  edge [
    source 17
    target 1403
  ]
  edge [
    source 17
    target 1404
  ]
  edge [
    source 17
    target 1405
  ]
  edge [
    source 17
    target 1406
  ]
  edge [
    source 17
    target 1407
  ]
  edge [
    source 17
    target 1408
  ]
  edge [
    source 17
    target 1409
  ]
  edge [
    source 17
    target 1410
  ]
  edge [
    source 17
    target 1411
  ]
  edge [
    source 17
    target 1412
  ]
  edge [
    source 17
    target 1413
  ]
  edge [
    source 17
    target 1414
  ]
  edge [
    source 17
    target 1415
  ]
  edge [
    source 17
    target 1416
  ]
  edge [
    source 17
    target 1417
  ]
  edge [
    source 17
    target 1418
  ]
  edge [
    source 17
    target 1419
  ]
  edge [
    source 17
    target 1420
  ]
  edge [
    source 17
    target 1421
  ]
  edge [
    source 17
    target 1422
  ]
  edge [
    source 17
    target 1423
  ]
  edge [
    source 17
    target 1424
  ]
  edge [
    source 17
    target 1425
  ]
  edge [
    source 17
    target 1426
  ]
  edge [
    source 17
    target 1427
  ]
  edge [
    source 17
    target 1428
  ]
  edge [
    source 17
    target 1429
  ]
  edge [
    source 17
    target 1430
  ]
  edge [
    source 17
    target 1431
  ]
  edge [
    source 17
    target 1432
  ]
  edge [
    source 17
    target 1433
  ]
  edge [
    source 17
    target 1434
  ]
  edge [
    source 17
    target 1435
  ]
  edge [
    source 17
    target 1436
  ]
  edge [
    source 17
    target 1437
  ]
  edge [
    source 17
    target 1438
  ]
  edge [
    source 17
    target 1439
  ]
  edge [
    source 17
    target 1440
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 1441
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 1442
  ]
  edge [
    source 17
    target 1443
  ]
  edge [
    source 17
    target 1444
  ]
  edge [
    source 17
    target 1445
  ]
  edge [
    source 17
    target 1446
  ]
  edge [
    source 17
    target 1447
  ]
  edge [
    source 17
    target 1448
  ]
  edge [
    source 17
    target 1449
  ]
  edge [
    source 17
    target 1450
  ]
  edge [
    source 17
    target 1451
  ]
  edge [
    source 17
    target 1452
  ]
  edge [
    source 17
    target 1453
  ]
  edge [
    source 17
    target 1454
  ]
  edge [
    source 17
    target 1455
  ]
  edge [
    source 17
    target 1456
  ]
  edge [
    source 17
    target 1457
  ]
  edge [
    source 17
    target 1458
  ]
  edge [
    source 17
    target 1459
  ]
  edge [
    source 17
    target 1460
  ]
  edge [
    source 17
    target 1461
  ]
  edge [
    source 17
    target 1462
  ]
  edge [
    source 17
    target 1463
  ]
  edge [
    source 17
    target 1464
  ]
  edge [
    source 17
    target 1465
  ]
  edge [
    source 17
    target 1466
  ]
  edge [
    source 17
    target 1467
  ]
  edge [
    source 17
    target 1468
  ]
  edge [
    source 17
    target 1469
  ]
  edge [
    source 17
    target 1470
  ]
  edge [
    source 17
    target 1471
  ]
  edge [
    source 17
    target 1472
  ]
  edge [
    source 17
    target 1473
  ]
  edge [
    source 17
    target 1474
  ]
  edge [
    source 17
    target 1475
  ]
  edge [
    source 17
    target 1476
  ]
  edge [
    source 17
    target 1477
  ]
  edge [
    source 17
    target 1478
  ]
  edge [
    source 17
    target 1479
  ]
  edge [
    source 17
    target 1480
  ]
  edge [
    source 17
    target 1481
  ]
  edge [
    source 17
    target 1482
  ]
  edge [
    source 17
    target 1483
  ]
  edge [
    source 17
    target 1484
  ]
  edge [
    source 17
    target 1485
  ]
  edge [
    source 17
    target 1486
  ]
  edge [
    source 17
    target 1487
  ]
  edge [
    source 17
    target 1488
  ]
  edge [
    source 17
    target 1489
  ]
  edge [
    source 17
    target 1490
  ]
  edge [
    source 17
    target 1491
  ]
  edge [
    source 17
    target 1492
  ]
  edge [
    source 17
    target 1493
  ]
  edge [
    source 17
    target 1494
  ]
  edge [
    source 17
    target 1495
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 1496
  ]
  edge [
    source 17
    target 1497
  ]
  edge [
    source 17
    target 1498
  ]
  edge [
    source 17
    target 1499
  ]
  edge [
    source 17
    target 1500
  ]
  edge [
    source 17
    target 1501
  ]
  edge [
    source 17
    target 1502
  ]
  edge [
    source 17
    target 1503
  ]
  edge [
    source 17
    target 1504
  ]
  edge [
    source 17
    target 1505
  ]
  edge [
    source 17
    target 1506
  ]
  edge [
    source 17
    target 1507
  ]
  edge [
    source 17
    target 1508
  ]
  edge [
    source 17
    target 1509
  ]
  edge [
    source 17
    target 1510
  ]
  edge [
    source 17
    target 1511
  ]
  edge [
    source 17
    target 1512
  ]
  edge [
    source 17
    target 1513
  ]
  edge [
    source 17
    target 1514
  ]
  edge [
    source 17
    target 1515
  ]
  edge [
    source 17
    target 1516
  ]
  edge [
    source 17
    target 1517
  ]
  edge [
    source 17
    target 1518
  ]
  edge [
    source 17
    target 1519
  ]
  edge [
    source 17
    target 1520
  ]
  edge [
    source 17
    target 1521
  ]
  edge [
    source 17
    target 1522
  ]
  edge [
    source 17
    target 1523
  ]
  edge [
    source 17
    target 1524
  ]
  edge [
    source 17
    target 1525
  ]
  edge [
    source 17
    target 1526
  ]
  edge [
    source 17
    target 1527
  ]
  edge [
    source 17
    target 1528
  ]
  edge [
    source 17
    target 1529
  ]
  edge [
    source 17
    target 1530
  ]
  edge [
    source 17
    target 1531
  ]
  edge [
    source 17
    target 1532
  ]
  edge [
    source 17
    target 1533
  ]
  edge [
    source 17
    target 1534
  ]
  edge [
    source 17
    target 1535
  ]
  edge [
    source 17
    target 1536
  ]
  edge [
    source 17
    target 1537
  ]
  edge [
    source 17
    target 1538
  ]
  edge [
    source 17
    target 1539
  ]
  edge [
    source 17
    target 1540
  ]
  edge [
    source 17
    target 1541
  ]
  edge [
    source 17
    target 1542
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 1543
  ]
  edge [
    source 17
    target 1544
  ]
  edge [
    source 17
    target 1545
  ]
  edge [
    source 17
    target 1546
  ]
  edge [
    source 17
    target 1547
  ]
  edge [
    source 17
    target 1548
  ]
  edge [
    source 17
    target 1549
  ]
  edge [
    source 17
    target 1550
  ]
  edge [
    source 17
    target 1551
  ]
  edge [
    source 17
    target 1552
  ]
  edge [
    source 17
    target 1553
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 1554
  ]
  edge [
    source 17
    target 1555
  ]
  edge [
    source 17
    target 1556
  ]
  edge [
    source 17
    target 1557
  ]
  edge [
    source 17
    target 1558
  ]
  edge [
    source 17
    target 1559
  ]
  edge [
    source 17
    target 1560
  ]
  edge [
    source 17
    target 1561
  ]
  edge [
    source 17
    target 1562
  ]
  edge [
    source 17
    target 1563
  ]
  edge [
    source 17
    target 1564
  ]
  edge [
    source 17
    target 1565
  ]
  edge [
    source 17
    target 1566
  ]
  edge [
    source 17
    target 1567
  ]
  edge [
    source 17
    target 1568
  ]
  edge [
    source 17
    target 1569
  ]
  edge [
    source 17
    target 1570
  ]
  edge [
    source 17
    target 1571
  ]
  edge [
    source 17
    target 1572
  ]
  edge [
    source 17
    target 1573
  ]
  edge [
    source 17
    target 1574
  ]
  edge [
    source 17
    target 1575
  ]
  edge [
    source 17
    target 1576
  ]
  edge [
    source 17
    target 1577
  ]
  edge [
    source 17
    target 1578
  ]
  edge [
    source 17
    target 1579
  ]
  edge [
    source 17
    target 1580
  ]
  edge [
    source 17
    target 1581
  ]
  edge [
    source 17
    target 1582
  ]
  edge [
    source 17
    target 1583
  ]
  edge [
    source 17
    target 1584
  ]
  edge [
    source 17
    target 1585
  ]
  edge [
    source 17
    target 1586
  ]
  edge [
    source 17
    target 1587
  ]
  edge [
    source 17
    target 1588
  ]
  edge [
    source 17
    target 1589
  ]
  edge [
    source 17
    target 1590
  ]
  edge [
    source 17
    target 1591
  ]
  edge [
    source 17
    target 1592
  ]
  edge [
    source 17
    target 1593
  ]
  edge [
    source 17
    target 1594
  ]
  edge [
    source 17
    target 1595
  ]
  edge [
    source 17
    target 1596
  ]
  edge [
    source 17
    target 1597
  ]
  edge [
    source 17
    target 1598
  ]
  edge [
    source 17
    target 1599
  ]
  edge [
    source 17
    target 1600
  ]
  edge [
    source 17
    target 1601
  ]
  edge [
    source 17
    target 1602
  ]
  edge [
    source 17
    target 1603
  ]
  edge [
    source 17
    target 1604
  ]
  edge [
    source 17
    target 1605
  ]
  edge [
    source 17
    target 1606
  ]
  edge [
    source 17
    target 1607
  ]
  edge [
    source 17
    target 1608
  ]
  edge [
    source 17
    target 1609
  ]
  edge [
    source 17
    target 1610
  ]
  edge [
    source 17
    target 1611
  ]
  edge [
    source 17
    target 1612
  ]
  edge [
    source 17
    target 1613
  ]
  edge [
    source 17
    target 1614
  ]
  edge [
    source 17
    target 1615
  ]
  edge [
    source 17
    target 1616
  ]
  edge [
    source 17
    target 1617
  ]
  edge [
    source 17
    target 1618
  ]
  edge [
    source 17
    target 1619
  ]
  edge [
    source 17
    target 1620
  ]
  edge [
    source 17
    target 1621
  ]
  edge [
    source 17
    target 1622
  ]
  edge [
    source 17
    target 1623
  ]
  edge [
    source 17
    target 1624
  ]
  edge [
    source 17
    target 1625
  ]
  edge [
    source 17
    target 1626
  ]
  edge [
    source 17
    target 1627
  ]
  edge [
    source 17
    target 1628
  ]
  edge [
    source 17
    target 1629
  ]
  edge [
    source 17
    target 1630
  ]
  edge [
    source 17
    target 1631
  ]
  edge [
    source 17
    target 1632
  ]
  edge [
    source 17
    target 1633
  ]
  edge [
    source 17
    target 1634
  ]
  edge [
    source 17
    target 1635
  ]
  edge [
    source 17
    target 1636
  ]
  edge [
    source 17
    target 1637
  ]
  edge [
    source 17
    target 1638
  ]
  edge [
    source 17
    target 1639
  ]
  edge [
    source 17
    target 1640
  ]
  edge [
    source 17
    target 1641
  ]
  edge [
    source 17
    target 1642
  ]
  edge [
    source 17
    target 1643
  ]
  edge [
    source 17
    target 1644
  ]
  edge [
    source 17
    target 1645
  ]
  edge [
    source 17
    target 1646
  ]
  edge [
    source 17
    target 1647
  ]
  edge [
    source 17
    target 1648
  ]
  edge [
    source 17
    target 1649
  ]
  edge [
    source 17
    target 1650
  ]
  edge [
    source 17
    target 1651
  ]
  edge [
    source 17
    target 1652
  ]
  edge [
    source 17
    target 1653
  ]
  edge [
    source 17
    target 1654
  ]
  edge [
    source 17
    target 1655
  ]
  edge [
    source 17
    target 1656
  ]
  edge [
    source 17
    target 1657
  ]
  edge [
    source 17
    target 1658
  ]
  edge [
    source 17
    target 1659
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 1660
  ]
  edge [
    source 17
    target 1661
  ]
  edge [
    source 17
    target 1662
  ]
  edge [
    source 17
    target 1663
  ]
  edge [
    source 17
    target 1664
  ]
  edge [
    source 17
    target 1665
  ]
  edge [
    source 17
    target 1666
  ]
  edge [
    source 17
    target 1667
  ]
  edge [
    source 17
    target 1668
  ]
  edge [
    source 17
    target 1669
  ]
  edge [
    source 17
    target 1670
  ]
  edge [
    source 17
    target 1671
  ]
  edge [
    source 17
    target 1672
  ]
  edge [
    source 17
    target 1673
  ]
  edge [
    source 17
    target 1674
  ]
  edge [
    source 17
    target 1675
  ]
  edge [
    source 17
    target 1676
  ]
  edge [
    source 17
    target 1677
  ]
  edge [
    source 17
    target 1678
  ]
  edge [
    source 17
    target 1679
  ]
  edge [
    source 17
    target 1680
  ]
  edge [
    source 17
    target 1681
  ]
  edge [
    source 17
    target 1682
  ]
  edge [
    source 17
    target 1683
  ]
  edge [
    source 17
    target 1684
  ]
  edge [
    source 17
    target 1685
  ]
  edge [
    source 17
    target 1686
  ]
  edge [
    source 17
    target 1687
  ]
  edge [
    source 17
    target 1688
  ]
  edge [
    source 17
    target 1689
  ]
  edge [
    source 17
    target 1690
  ]
  edge [
    source 17
    target 1691
  ]
  edge [
    source 17
    target 1692
  ]
  edge [
    source 17
    target 1693
  ]
  edge [
    source 17
    target 1694
  ]
  edge [
    source 17
    target 1695
  ]
  edge [
    source 17
    target 1696
  ]
  edge [
    source 17
    target 1697
  ]
  edge [
    source 17
    target 1698
  ]
  edge [
    source 17
    target 1699
  ]
  edge [
    source 17
    target 1700
  ]
  edge [
    source 17
    target 1701
  ]
  edge [
    source 17
    target 1702
  ]
  edge [
    source 17
    target 1703
  ]
  edge [
    source 17
    target 1704
  ]
  edge [
    source 17
    target 1705
  ]
  edge [
    source 17
    target 1706
  ]
  edge [
    source 17
    target 1707
  ]
  edge [
    source 17
    target 1708
  ]
  edge [
    source 17
    target 1709
  ]
  edge [
    source 17
    target 1710
  ]
  edge [
    source 17
    target 1711
  ]
  edge [
    source 17
    target 1712
  ]
  edge [
    source 17
    target 1713
  ]
  edge [
    source 17
    target 1714
  ]
  edge [
    source 17
    target 1715
  ]
  edge [
    source 17
    target 1716
  ]
  edge [
    source 17
    target 1717
  ]
  edge [
    source 17
    target 1718
  ]
  edge [
    source 17
    target 1719
  ]
  edge [
    source 17
    target 1720
  ]
  edge [
    source 17
    target 1721
  ]
  edge [
    source 17
    target 1722
  ]
  edge [
    source 17
    target 1723
  ]
  edge [
    source 17
    target 1724
  ]
  edge [
    source 17
    target 1725
  ]
  edge [
    source 17
    target 1726
  ]
  edge [
    source 17
    target 1727
  ]
  edge [
    source 17
    target 1728
  ]
  edge [
    source 17
    target 1729
  ]
  edge [
    source 17
    target 1730
  ]
  edge [
    source 17
    target 1731
  ]
  edge [
    source 17
    target 1732
  ]
  edge [
    source 17
    target 1733
  ]
  edge [
    source 17
    target 1734
  ]
  edge [
    source 17
    target 1735
  ]
  edge [
    source 17
    target 1736
  ]
  edge [
    source 17
    target 1737
  ]
  edge [
    source 17
    target 1738
  ]
  edge [
    source 17
    target 1739
  ]
  edge [
    source 17
    target 1740
  ]
  edge [
    source 17
    target 1741
  ]
  edge [
    source 17
    target 1742
  ]
  edge [
    source 17
    target 1743
  ]
  edge [
    source 17
    target 1744
  ]
  edge [
    source 17
    target 1745
  ]
  edge [
    source 17
    target 1746
  ]
  edge [
    source 17
    target 1747
  ]
  edge [
    source 17
    target 1748
  ]
  edge [
    source 17
    target 1749
  ]
  edge [
    source 17
    target 1750
  ]
  edge [
    source 17
    target 1751
  ]
  edge [
    source 17
    target 1752
  ]
  edge [
    source 17
    target 1753
  ]
  edge [
    source 17
    target 1754
  ]
  edge [
    source 17
    target 1755
  ]
  edge [
    source 17
    target 1756
  ]
  edge [
    source 17
    target 1757
  ]
  edge [
    source 17
    target 1758
  ]
  edge [
    source 17
    target 1759
  ]
  edge [
    source 17
    target 1760
  ]
  edge [
    source 17
    target 1761
  ]
  edge [
    source 17
    target 1762
  ]
  edge [
    source 17
    target 1763
  ]
  edge [
    source 17
    target 1764
  ]
  edge [
    source 17
    target 1765
  ]
  edge [
    source 17
    target 1766
  ]
  edge [
    source 17
    target 1767
  ]
  edge [
    source 17
    target 1768
  ]
  edge [
    source 17
    target 1769
  ]
  edge [
    source 17
    target 1770
  ]
  edge [
    source 17
    target 1771
  ]
  edge [
    source 17
    target 1772
  ]
  edge [
    source 17
    target 1773
  ]
  edge [
    source 17
    target 1774
  ]
  edge [
    source 17
    target 1775
  ]
  edge [
    source 17
    target 1776
  ]
  edge [
    source 17
    target 1777
  ]
  edge [
    source 17
    target 1778
  ]
  edge [
    source 17
    target 1779
  ]
  edge [
    source 17
    target 1780
  ]
  edge [
    source 17
    target 1781
  ]
  edge [
    source 17
    target 1782
  ]
  edge [
    source 17
    target 1783
  ]
  edge [
    source 17
    target 1784
  ]
  edge [
    source 17
    target 1785
  ]
  edge [
    source 17
    target 1786
  ]
  edge [
    source 17
    target 1787
  ]
  edge [
    source 17
    target 1788
  ]
  edge [
    source 17
    target 1789
  ]
  edge [
    source 17
    target 1790
  ]
  edge [
    source 17
    target 1791
  ]
  edge [
    source 17
    target 1792
  ]
  edge [
    source 17
    target 1793
  ]
  edge [
    source 17
    target 1794
  ]
  edge [
    source 17
    target 1795
  ]
  edge [
    source 17
    target 1796
  ]
  edge [
    source 17
    target 1797
  ]
  edge [
    source 17
    target 1798
  ]
  edge [
    source 17
    target 1799
  ]
  edge [
    source 17
    target 1800
  ]
  edge [
    source 17
    target 1801
  ]
  edge [
    source 17
    target 1802
  ]
  edge [
    source 17
    target 1803
  ]
  edge [
    source 17
    target 1804
  ]
  edge [
    source 17
    target 1805
  ]
  edge [
    source 17
    target 1806
  ]
  edge [
    source 17
    target 1807
  ]
  edge [
    source 17
    target 1808
  ]
  edge [
    source 17
    target 1809
  ]
  edge [
    source 17
    target 1810
  ]
  edge [
    source 17
    target 1811
  ]
  edge [
    source 17
    target 1812
  ]
  edge [
    source 17
    target 1813
  ]
  edge [
    source 17
    target 1814
  ]
  edge [
    source 17
    target 78
  ]
  edge [
    source 17
    target 1815
  ]
  edge [
    source 17
    target 1816
  ]
  edge [
    source 17
    target 1817
  ]
  edge [
    source 17
    target 1818
  ]
  edge [
    source 17
    target 1819
  ]
  edge [
    source 17
    target 1820
  ]
  edge [
    source 17
    target 1821
  ]
  edge [
    source 17
    target 1822
  ]
  edge [
    source 17
    target 1823
  ]
  edge [
    source 17
    target 1824
  ]
  edge [
    source 17
    target 1825
  ]
  edge [
    source 17
    target 1826
  ]
  edge [
    source 17
    target 1827
  ]
  edge [
    source 17
    target 1828
  ]
  edge [
    source 17
    target 1829
  ]
  edge [
    source 17
    target 1830
  ]
  edge [
    source 17
    target 1831
  ]
  edge [
    source 17
    target 1832
  ]
  edge [
    source 17
    target 1833
  ]
  edge [
    source 17
    target 1834
  ]
  edge [
    source 17
    target 1835
  ]
  edge [
    source 17
    target 1836
  ]
  edge [
    source 17
    target 1837
  ]
  edge [
    source 17
    target 1838
  ]
  edge [
    source 17
    target 1839
  ]
  edge [
    source 17
    target 1840
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 1841
  ]
  edge [
    source 17
    target 1842
  ]
  edge [
    source 17
    target 1843
  ]
  edge [
    source 17
    target 1844
  ]
  edge [
    source 17
    target 1845
  ]
  edge [
    source 17
    target 1846
  ]
  edge [
    source 17
    target 1847
  ]
  edge [
    source 17
    target 1848
  ]
  edge [
    source 17
    target 1849
  ]
]
