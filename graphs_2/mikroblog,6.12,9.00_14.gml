graph [
  node [
    id 0
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 1
    label "lodz"
    origin "text"
  ]
  node [
    id 2
    label "czujedobrzeczlowiek"
    origin "text"
  ]
  node [
    id 3
    label "adjustment"
  ]
  node [
    id 4
    label "panowanie"
  ]
  node [
    id 5
    label "przebywanie"
  ]
  node [
    id 6
    label "animation"
  ]
  node [
    id 7
    label "kwadrat"
  ]
  node [
    id 8
    label "stanie"
  ]
  node [
    id 9
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 10
    label "pomieszkanie"
  ]
  node [
    id 11
    label "lokal"
  ]
  node [
    id 12
    label "dom"
  ]
  node [
    id 13
    label "zajmowanie"
  ]
  node [
    id 14
    label "sprawowanie"
  ]
  node [
    id 15
    label "bycie"
  ]
  node [
    id 16
    label "kierowanie"
  ]
  node [
    id 17
    label "w&#322;adca"
  ]
  node [
    id 18
    label "dominowanie"
  ]
  node [
    id 19
    label "przewaga"
  ]
  node [
    id 20
    label "przewa&#380;anie"
  ]
  node [
    id 21
    label "znaczenie"
  ]
  node [
    id 22
    label "laterality"
  ]
  node [
    id 23
    label "control"
  ]
  node [
    id 24
    label "dominance"
  ]
  node [
    id 25
    label "kontrolowanie"
  ]
  node [
    id 26
    label "temper"
  ]
  node [
    id 27
    label "rule"
  ]
  node [
    id 28
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 29
    label "prym"
  ]
  node [
    id 30
    label "w&#322;adza"
  ]
  node [
    id 31
    label "ocieranie_si&#281;"
  ]
  node [
    id 32
    label "otoczenie_si&#281;"
  ]
  node [
    id 33
    label "posiedzenie"
  ]
  node [
    id 34
    label "otarcie_si&#281;"
  ]
  node [
    id 35
    label "atakowanie"
  ]
  node [
    id 36
    label "otaczanie_si&#281;"
  ]
  node [
    id 37
    label "wyj&#347;cie"
  ]
  node [
    id 38
    label "zmierzanie"
  ]
  node [
    id 39
    label "residency"
  ]
  node [
    id 40
    label "sojourn"
  ]
  node [
    id 41
    label "wychodzenie"
  ]
  node [
    id 42
    label "tkwienie"
  ]
  node [
    id 43
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 44
    label "powodowanie"
  ]
  node [
    id 45
    label "lokowanie_si&#281;"
  ]
  node [
    id 46
    label "schorzenie"
  ]
  node [
    id 47
    label "zajmowanie_si&#281;"
  ]
  node [
    id 48
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 49
    label "stosowanie"
  ]
  node [
    id 50
    label "anektowanie"
  ]
  node [
    id 51
    label "ciekawy"
  ]
  node [
    id 52
    label "zabieranie"
  ]
  node [
    id 53
    label "robienie"
  ]
  node [
    id 54
    label "sytuowanie_si&#281;"
  ]
  node [
    id 55
    label "wype&#322;nianie"
  ]
  node [
    id 56
    label "obejmowanie"
  ]
  node [
    id 57
    label "klasyfikacja"
  ]
  node [
    id 58
    label "czynno&#347;&#263;"
  ]
  node [
    id 59
    label "dzianie_si&#281;"
  ]
  node [
    id 60
    label "branie"
  ]
  node [
    id 61
    label "rz&#261;dzenie"
  ]
  node [
    id 62
    label "occupation"
  ]
  node [
    id 63
    label "zadawanie"
  ]
  node [
    id 64
    label "zaj&#281;ty"
  ]
  node [
    id 65
    label "miejsce"
  ]
  node [
    id 66
    label "gastronomia"
  ]
  node [
    id 67
    label "zak&#322;ad"
  ]
  node [
    id 68
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 69
    label "rodzina"
  ]
  node [
    id 70
    label "substancja_mieszkaniowa"
  ]
  node [
    id 71
    label "instytucja"
  ]
  node [
    id 72
    label "siedziba"
  ]
  node [
    id 73
    label "dom_rodzinny"
  ]
  node [
    id 74
    label "budynek"
  ]
  node [
    id 75
    label "grupa"
  ]
  node [
    id 76
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 77
    label "poj&#281;cie"
  ]
  node [
    id 78
    label "stead"
  ]
  node [
    id 79
    label "garderoba"
  ]
  node [
    id 80
    label "wiecha"
  ]
  node [
    id 81
    label "fratria"
  ]
  node [
    id 82
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 83
    label "trwanie"
  ]
  node [
    id 84
    label "ustanie"
  ]
  node [
    id 85
    label "wystanie"
  ]
  node [
    id 86
    label "postanie"
  ]
  node [
    id 87
    label "wystawanie"
  ]
  node [
    id 88
    label "kosztowanie"
  ]
  node [
    id 89
    label "przestanie"
  ]
  node [
    id 90
    label "pot&#281;ga"
  ]
  node [
    id 91
    label "wielok&#261;t_foremny"
  ]
  node [
    id 92
    label "stopie&#324;_pisma"
  ]
  node [
    id 93
    label "prostok&#261;t"
  ]
  node [
    id 94
    label "square"
  ]
  node [
    id 95
    label "romb"
  ]
  node [
    id 96
    label "justunek"
  ]
  node [
    id 97
    label "dzielnica"
  ]
  node [
    id 98
    label "poletko"
  ]
  node [
    id 99
    label "ekologia"
  ]
  node [
    id 100
    label "tango"
  ]
  node [
    id 101
    label "figura_taneczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 1
    target 2
  ]
]
