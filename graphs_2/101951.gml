graph [
  node [
    id 0
    label "art"
    origin "text"
  ]
  node [
    id 1
    label "konstytucja"
    origin "text"
  ]
  node [
    id 2
    label "albowiem"
    origin "text"
  ]
  node [
    id 3
    label "narusza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zasada"
    origin "text"
  ]
  node [
    id 5
    label "zaufanie"
    origin "text"
  ]
  node [
    id 6
    label "obywatel"
    origin "text"
  ]
  node [
    id 7
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 8
    label "stanowi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "prawa"
    origin "text"
  ]
  node [
    id 10
    label "gdy&#380;"
    origin "text"
  ]
  node [
    id 11
    label "kodeks"
    origin "text"
  ]
  node [
    id 12
    label "sp&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 13
    label "handlowy"
    origin "text"
  ]
  node [
    id 14
    label "ogranicza&#263;"
    origin "text"
  ]
  node [
    id 15
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "zaskar&#380;y&#263;"
    origin "text"
  ]
  node [
    id 17
    label "przepis"
    origin "text"
  ]
  node [
    id 18
    label "akcjonariusz"
    origin "text"
  ]
  node [
    id 19
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 20
    label "naby&#263;"
    origin "text"
  ]
  node [
    id 21
    label "akcja"
    origin "text"
  ]
  node [
    id 22
    label "tym"
    origin "text"
  ]
  node [
    id 23
    label "pracownik"
    origin "text"
  ]
  node [
    id 24
    label "prywatyzowa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "przedsi&#281;biorstwo"
    origin "text"
  ]
  node [
    id 26
    label "przed"
    origin "text"
  ]
  node [
    id 27
    label "wej&#347;cie"
    origin "text"
  ]
  node [
    id 28
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 29
    label "droga"
    origin "text"
  ]
  node [
    id 30
    label "odpowiedni"
    origin "text"
  ]
  node [
    id 31
    label "przej&#347;ciowy"
    origin "text"
  ]
  node [
    id 32
    label "struktura"
  ]
  node [
    id 33
    label "zbi&#243;r"
  ]
  node [
    id 34
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 35
    label "akt"
  ]
  node [
    id 36
    label "cezar"
  ]
  node [
    id 37
    label "dokument"
  ]
  node [
    id 38
    label "budowa"
  ]
  node [
    id 39
    label "uchwa&#322;a"
  ]
  node [
    id 40
    label "zapis"
  ]
  node [
    id 41
    label "&#347;wiadectwo"
  ]
  node [
    id 42
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 43
    label "wytw&#243;r"
  ]
  node [
    id 44
    label "parafa"
  ]
  node [
    id 45
    label "plik"
  ]
  node [
    id 46
    label "raport&#243;wka"
  ]
  node [
    id 47
    label "utw&#243;r"
  ]
  node [
    id 48
    label "record"
  ]
  node [
    id 49
    label "fascyku&#322;"
  ]
  node [
    id 50
    label "dokumentacja"
  ]
  node [
    id 51
    label "registratura"
  ]
  node [
    id 52
    label "artyku&#322;"
  ]
  node [
    id 53
    label "writing"
  ]
  node [
    id 54
    label "sygnatariusz"
  ]
  node [
    id 55
    label "podnieci&#263;"
  ]
  node [
    id 56
    label "scena"
  ]
  node [
    id 57
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 58
    label "numer"
  ]
  node [
    id 59
    label "po&#380;ycie"
  ]
  node [
    id 60
    label "poj&#281;cie"
  ]
  node [
    id 61
    label "podniecenie"
  ]
  node [
    id 62
    label "nago&#347;&#263;"
  ]
  node [
    id 63
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 64
    label "seks"
  ]
  node [
    id 65
    label "podniecanie"
  ]
  node [
    id 66
    label "imisja"
  ]
  node [
    id 67
    label "zwyczaj"
  ]
  node [
    id 68
    label "rozmna&#380;anie"
  ]
  node [
    id 69
    label "ruch_frykcyjny"
  ]
  node [
    id 70
    label "ontologia"
  ]
  node [
    id 71
    label "wydarzenie"
  ]
  node [
    id 72
    label "na_pieska"
  ]
  node [
    id 73
    label "pozycja_misjonarska"
  ]
  node [
    id 74
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 75
    label "fragment"
  ]
  node [
    id 76
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 77
    label "z&#322;&#261;czenie"
  ]
  node [
    id 78
    label "czynno&#347;&#263;"
  ]
  node [
    id 79
    label "gra_wst&#281;pna"
  ]
  node [
    id 80
    label "erotyka"
  ]
  node [
    id 81
    label "urzeczywistnienie"
  ]
  node [
    id 82
    label "baraszki"
  ]
  node [
    id 83
    label "certificate"
  ]
  node [
    id 84
    label "po&#380;&#261;danie"
  ]
  node [
    id 85
    label "wzw&#243;d"
  ]
  node [
    id 86
    label "funkcja"
  ]
  node [
    id 87
    label "act"
  ]
  node [
    id 88
    label "arystotelizm"
  ]
  node [
    id 89
    label "podnieca&#263;"
  ]
  node [
    id 90
    label "egzemplarz"
  ]
  node [
    id 91
    label "series"
  ]
  node [
    id 92
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 93
    label "uprawianie"
  ]
  node [
    id 94
    label "praca_rolnicza"
  ]
  node [
    id 95
    label "collection"
  ]
  node [
    id 96
    label "dane"
  ]
  node [
    id 97
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 98
    label "pakiet_klimatyczny"
  ]
  node [
    id 99
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 100
    label "sum"
  ]
  node [
    id 101
    label "gathering"
  ]
  node [
    id 102
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 103
    label "album"
  ]
  node [
    id 104
    label "mechanika"
  ]
  node [
    id 105
    label "figura"
  ]
  node [
    id 106
    label "miejsce_pracy"
  ]
  node [
    id 107
    label "cecha"
  ]
  node [
    id 108
    label "organ"
  ]
  node [
    id 109
    label "kreacja"
  ]
  node [
    id 110
    label "zwierz&#281;"
  ]
  node [
    id 111
    label "r&#243;w"
  ]
  node [
    id 112
    label "posesja"
  ]
  node [
    id 113
    label "konstrukcja"
  ]
  node [
    id 114
    label "wjazd"
  ]
  node [
    id 115
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 116
    label "praca"
  ]
  node [
    id 117
    label "constitution"
  ]
  node [
    id 118
    label "resolution"
  ]
  node [
    id 119
    label "o&#347;"
  ]
  node [
    id 120
    label "usenet"
  ]
  node [
    id 121
    label "rozprz&#261;c"
  ]
  node [
    id 122
    label "zachowanie"
  ]
  node [
    id 123
    label "cybernetyk"
  ]
  node [
    id 124
    label "podsystem"
  ]
  node [
    id 125
    label "system"
  ]
  node [
    id 126
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 127
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 128
    label "sk&#322;ad"
  ]
  node [
    id 129
    label "systemat"
  ]
  node [
    id 130
    label "konstelacja"
  ]
  node [
    id 131
    label "cesarz"
  ]
  node [
    id 132
    label "odejmowa&#263;"
  ]
  node [
    id 133
    label "robi&#263;"
  ]
  node [
    id 134
    label "zaczyna&#263;"
  ]
  node [
    id 135
    label "bankrupt"
  ]
  node [
    id 136
    label "psu&#263;"
  ]
  node [
    id 137
    label "transgress"
  ]
  node [
    id 138
    label "begin"
  ]
  node [
    id 139
    label "zabiera&#263;"
  ]
  node [
    id 140
    label "liczy&#263;"
  ]
  node [
    id 141
    label "reduce"
  ]
  node [
    id 142
    label "take"
  ]
  node [
    id 143
    label "abstract"
  ]
  node [
    id 144
    label "ujemny"
  ]
  node [
    id 145
    label "oddziela&#263;"
  ]
  node [
    id 146
    label "oddala&#263;"
  ]
  node [
    id 147
    label "mie&#263;_miejsce"
  ]
  node [
    id 148
    label "open"
  ]
  node [
    id 149
    label "set_about"
  ]
  node [
    id 150
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 151
    label "post&#281;powa&#263;"
  ]
  node [
    id 152
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 153
    label "demoralizowa&#263;"
  ]
  node [
    id 154
    label "uszkadza&#263;"
  ]
  node [
    id 155
    label "szkodzi&#263;"
  ]
  node [
    id 156
    label "corrupt"
  ]
  node [
    id 157
    label "pamper"
  ]
  node [
    id 158
    label "pierdoli&#263;_si&#281;"
  ]
  node [
    id 159
    label "spoiler"
  ]
  node [
    id 160
    label "pogarsza&#263;"
  ]
  node [
    id 161
    label "organizowa&#263;"
  ]
  node [
    id 162
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 163
    label "czyni&#263;"
  ]
  node [
    id 164
    label "give"
  ]
  node [
    id 165
    label "stylizowa&#263;"
  ]
  node [
    id 166
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 167
    label "falowa&#263;"
  ]
  node [
    id 168
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 169
    label "peddle"
  ]
  node [
    id 170
    label "wydala&#263;"
  ]
  node [
    id 171
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 172
    label "tentegowa&#263;"
  ]
  node [
    id 173
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 174
    label "urz&#261;dza&#263;"
  ]
  node [
    id 175
    label "oszukiwa&#263;"
  ]
  node [
    id 176
    label "work"
  ]
  node [
    id 177
    label "ukazywa&#263;"
  ]
  node [
    id 178
    label "przerabia&#263;"
  ]
  node [
    id 179
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 180
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 181
    label "regu&#322;a_Allena"
  ]
  node [
    id 182
    label "base"
  ]
  node [
    id 183
    label "umowa"
  ]
  node [
    id 184
    label "obserwacja"
  ]
  node [
    id 185
    label "zasada_d'Alemberta"
  ]
  node [
    id 186
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 187
    label "normalizacja"
  ]
  node [
    id 188
    label "moralno&#347;&#263;"
  ]
  node [
    id 189
    label "criterion"
  ]
  node [
    id 190
    label "opis"
  ]
  node [
    id 191
    label "regu&#322;a_Glogera"
  ]
  node [
    id 192
    label "prawo_Mendla"
  ]
  node [
    id 193
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 194
    label "twierdzenie"
  ]
  node [
    id 195
    label "prawo"
  ]
  node [
    id 196
    label "standard"
  ]
  node [
    id 197
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 198
    label "spos&#243;b"
  ]
  node [
    id 199
    label "qualification"
  ]
  node [
    id 200
    label "dominion"
  ]
  node [
    id 201
    label "occupation"
  ]
  node [
    id 202
    label "podstawa"
  ]
  node [
    id 203
    label "substancja"
  ]
  node [
    id 204
    label "prawid&#322;o"
  ]
  node [
    id 205
    label "dobro&#263;"
  ]
  node [
    id 206
    label "aretologia"
  ]
  node [
    id 207
    label "zesp&#243;&#322;"
  ]
  node [
    id 208
    label "morality"
  ]
  node [
    id 209
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 210
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 211
    label "honesty"
  ]
  node [
    id 212
    label "model"
  ]
  node [
    id 213
    label "ordinariness"
  ]
  node [
    id 214
    label "instytucja"
  ]
  node [
    id 215
    label "zorganizowa&#263;"
  ]
  node [
    id 216
    label "taniec_towarzyski"
  ]
  node [
    id 217
    label "organizowanie"
  ]
  node [
    id 218
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 219
    label "zorganizowanie"
  ]
  node [
    id 220
    label "wypowied&#378;"
  ]
  node [
    id 221
    label "exposition"
  ]
  node [
    id 222
    label "obja&#347;nienie"
  ]
  node [
    id 223
    label "zawarcie"
  ]
  node [
    id 224
    label "zawrze&#263;"
  ]
  node [
    id 225
    label "czyn"
  ]
  node [
    id 226
    label "warunek"
  ]
  node [
    id 227
    label "gestia_transportowa"
  ]
  node [
    id 228
    label "contract"
  ]
  node [
    id 229
    label "porozumienie"
  ]
  node [
    id 230
    label "klauzula"
  ]
  node [
    id 231
    label "przenikanie"
  ]
  node [
    id 232
    label "byt"
  ]
  node [
    id 233
    label "materia"
  ]
  node [
    id 234
    label "cz&#261;steczka"
  ]
  node [
    id 235
    label "temperatura_krytyczna"
  ]
  node [
    id 236
    label "przenika&#263;"
  ]
  node [
    id 237
    label "smolisty"
  ]
  node [
    id 238
    label "narz&#281;dzie"
  ]
  node [
    id 239
    label "tryb"
  ]
  node [
    id 240
    label "nature"
  ]
  node [
    id 241
    label "pot&#281;ga"
  ]
  node [
    id 242
    label "documentation"
  ]
  node [
    id 243
    label "przedmiot"
  ]
  node [
    id 244
    label "column"
  ]
  node [
    id 245
    label "zasadzi&#263;"
  ]
  node [
    id 246
    label "za&#322;o&#380;enie"
  ]
  node [
    id 247
    label "punkt_odniesienia"
  ]
  node [
    id 248
    label "zasadzenie"
  ]
  node [
    id 249
    label "bok"
  ]
  node [
    id 250
    label "d&#243;&#322;"
  ]
  node [
    id 251
    label "dzieci&#281;ctwo"
  ]
  node [
    id 252
    label "background"
  ]
  node [
    id 253
    label "podstawowy"
  ]
  node [
    id 254
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 255
    label "strategia"
  ]
  node [
    id 256
    label "pomys&#322;"
  ]
  node [
    id 257
    label "&#347;ciana"
  ]
  node [
    id 258
    label "shoetree"
  ]
  node [
    id 259
    label "badanie"
  ]
  node [
    id 260
    label "proces_my&#347;lowy"
  ]
  node [
    id 261
    label "remark"
  ]
  node [
    id 262
    label "metoda"
  ]
  node [
    id 263
    label "stwierdzenie"
  ]
  node [
    id 264
    label "observation"
  ]
  node [
    id 265
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 266
    label "alternatywa_Fredholma"
  ]
  node [
    id 267
    label "oznajmianie"
  ]
  node [
    id 268
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 269
    label "teoria"
  ]
  node [
    id 270
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 271
    label "paradoks_Leontiefa"
  ]
  node [
    id 272
    label "s&#261;d"
  ]
  node [
    id 273
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 274
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 275
    label "teza"
  ]
  node [
    id 276
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 277
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 278
    label "twierdzenie_Pettisa"
  ]
  node [
    id 279
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 280
    label "twierdzenie_Maya"
  ]
  node [
    id 281
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 282
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 283
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 284
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 285
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 286
    label "zapewnianie"
  ]
  node [
    id 287
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 288
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 289
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 290
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 291
    label "twierdzenie_Stokesa"
  ]
  node [
    id 292
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 293
    label "twierdzenie_Cevy"
  ]
  node [
    id 294
    label "twierdzenie_Pascala"
  ]
  node [
    id 295
    label "proposition"
  ]
  node [
    id 296
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 297
    label "komunikowanie"
  ]
  node [
    id 298
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 299
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 300
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 301
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 302
    label "relacja"
  ]
  node [
    id 303
    label "calibration"
  ]
  node [
    id 304
    label "operacja"
  ]
  node [
    id 305
    label "proces"
  ]
  node [
    id 306
    label "porz&#261;dek"
  ]
  node [
    id 307
    label "dominance"
  ]
  node [
    id 308
    label "zabieg"
  ]
  node [
    id 309
    label "standardization"
  ]
  node [
    id 310
    label "zmiana"
  ]
  node [
    id 311
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 312
    label "umocowa&#263;"
  ]
  node [
    id 313
    label "procesualistyka"
  ]
  node [
    id 314
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 315
    label "kryminalistyka"
  ]
  node [
    id 316
    label "szko&#322;a"
  ]
  node [
    id 317
    label "kierunek"
  ]
  node [
    id 318
    label "normatywizm"
  ]
  node [
    id 319
    label "jurisprudence"
  ]
  node [
    id 320
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 321
    label "kultura_duchowa"
  ]
  node [
    id 322
    label "prawo_karne_procesowe"
  ]
  node [
    id 323
    label "kazuistyka"
  ]
  node [
    id 324
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 325
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 326
    label "kryminologia"
  ]
  node [
    id 327
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 328
    label "prawo_karne"
  ]
  node [
    id 329
    label "legislacyjnie"
  ]
  node [
    id 330
    label "cywilistyka"
  ]
  node [
    id 331
    label "judykatura"
  ]
  node [
    id 332
    label "kanonistyka"
  ]
  node [
    id 333
    label "nauka_prawa"
  ]
  node [
    id 334
    label "podmiot"
  ]
  node [
    id 335
    label "law"
  ]
  node [
    id 336
    label "wykonawczy"
  ]
  node [
    id 337
    label "opoka"
  ]
  node [
    id 338
    label "firma"
  ]
  node [
    id 339
    label "faith"
  ]
  node [
    id 340
    label "zacz&#281;cie"
  ]
  node [
    id 341
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 342
    label "credit"
  ]
  node [
    id 343
    label "postawa"
  ]
  node [
    id 344
    label "zrobienie"
  ]
  node [
    id 345
    label "narobienie"
  ]
  node [
    id 346
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 347
    label "creation"
  ]
  node [
    id 348
    label "porobienie"
  ]
  node [
    id 349
    label "stan"
  ]
  node [
    id 350
    label "nastawienie"
  ]
  node [
    id 351
    label "pozycja"
  ]
  node [
    id 352
    label "attitude"
  ]
  node [
    id 353
    label "discourtesy"
  ]
  node [
    id 354
    label "odj&#281;cie"
  ]
  node [
    id 355
    label "post&#261;pienie"
  ]
  node [
    id 356
    label "opening"
  ]
  node [
    id 357
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 358
    label "wyra&#380;enie"
  ]
  node [
    id 359
    label "materia&#322;_budowlany"
  ]
  node [
    id 360
    label "ska&#322;a"
  ]
  node [
    id 361
    label "ostoja"
  ]
  node [
    id 362
    label "monolit"
  ]
  node [
    id 363
    label "ska&#322;a_osadowa"
  ]
  node [
    id 364
    label "przyjaciel"
  ]
  node [
    id 365
    label "filar"
  ]
  node [
    id 366
    label "Apeks"
  ]
  node [
    id 367
    label "zasoby"
  ]
  node [
    id 368
    label "cz&#322;owiek"
  ]
  node [
    id 369
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 370
    label "Hortex"
  ]
  node [
    id 371
    label "reengineering"
  ]
  node [
    id 372
    label "nazwa_w&#322;asna"
  ]
  node [
    id 373
    label "podmiot_gospodarczy"
  ]
  node [
    id 374
    label "paczkarnia"
  ]
  node [
    id 375
    label "Orlen"
  ]
  node [
    id 376
    label "interes"
  ]
  node [
    id 377
    label "Google"
  ]
  node [
    id 378
    label "Pewex"
  ]
  node [
    id 379
    label "Canon"
  ]
  node [
    id 380
    label "MAN_SE"
  ]
  node [
    id 381
    label "Spo&#322;em"
  ]
  node [
    id 382
    label "klasa"
  ]
  node [
    id 383
    label "networking"
  ]
  node [
    id 384
    label "MAC"
  ]
  node [
    id 385
    label "zasoby_ludzkie"
  ]
  node [
    id 386
    label "Baltona"
  ]
  node [
    id 387
    label "Orbis"
  ]
  node [
    id 388
    label "biurowiec"
  ]
  node [
    id 389
    label "HP"
  ]
  node [
    id 390
    label "siedziba"
  ]
  node [
    id 391
    label "miastowy"
  ]
  node [
    id 392
    label "mieszkaniec"
  ]
  node [
    id 393
    label "przedstawiciel"
  ]
  node [
    id 394
    label "mieszczanin"
  ]
  node [
    id 395
    label "miejski"
  ]
  node [
    id 396
    label "nowoczesny"
  ]
  node [
    id 397
    label "mieszcza&#324;stwo"
  ]
  node [
    id 398
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 399
    label "Katar"
  ]
  node [
    id 400
    label "Libia"
  ]
  node [
    id 401
    label "Gwatemala"
  ]
  node [
    id 402
    label "Ekwador"
  ]
  node [
    id 403
    label "Afganistan"
  ]
  node [
    id 404
    label "Tad&#380;ykistan"
  ]
  node [
    id 405
    label "Bhutan"
  ]
  node [
    id 406
    label "Argentyna"
  ]
  node [
    id 407
    label "D&#380;ibuti"
  ]
  node [
    id 408
    label "Wenezuela"
  ]
  node [
    id 409
    label "Gabon"
  ]
  node [
    id 410
    label "Ukraina"
  ]
  node [
    id 411
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 412
    label "Rwanda"
  ]
  node [
    id 413
    label "Liechtenstein"
  ]
  node [
    id 414
    label "organizacja"
  ]
  node [
    id 415
    label "Sri_Lanka"
  ]
  node [
    id 416
    label "Madagaskar"
  ]
  node [
    id 417
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 418
    label "Kongo"
  ]
  node [
    id 419
    label "Tonga"
  ]
  node [
    id 420
    label "Bangladesz"
  ]
  node [
    id 421
    label "Kanada"
  ]
  node [
    id 422
    label "Wehrlen"
  ]
  node [
    id 423
    label "Algieria"
  ]
  node [
    id 424
    label "Uganda"
  ]
  node [
    id 425
    label "Surinam"
  ]
  node [
    id 426
    label "Sahara_Zachodnia"
  ]
  node [
    id 427
    label "Chile"
  ]
  node [
    id 428
    label "W&#281;gry"
  ]
  node [
    id 429
    label "Birma"
  ]
  node [
    id 430
    label "Kazachstan"
  ]
  node [
    id 431
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 432
    label "Armenia"
  ]
  node [
    id 433
    label "Tuwalu"
  ]
  node [
    id 434
    label "Timor_Wschodni"
  ]
  node [
    id 435
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 436
    label "Izrael"
  ]
  node [
    id 437
    label "Estonia"
  ]
  node [
    id 438
    label "Komory"
  ]
  node [
    id 439
    label "Kamerun"
  ]
  node [
    id 440
    label "Haiti"
  ]
  node [
    id 441
    label "Belize"
  ]
  node [
    id 442
    label "Sierra_Leone"
  ]
  node [
    id 443
    label "Luksemburg"
  ]
  node [
    id 444
    label "USA"
  ]
  node [
    id 445
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 446
    label "Barbados"
  ]
  node [
    id 447
    label "San_Marino"
  ]
  node [
    id 448
    label "Bu&#322;garia"
  ]
  node [
    id 449
    label "Indonezja"
  ]
  node [
    id 450
    label "Wietnam"
  ]
  node [
    id 451
    label "Malawi"
  ]
  node [
    id 452
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 453
    label "Francja"
  ]
  node [
    id 454
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 455
    label "partia"
  ]
  node [
    id 456
    label "Zambia"
  ]
  node [
    id 457
    label "Angola"
  ]
  node [
    id 458
    label "Grenada"
  ]
  node [
    id 459
    label "Nepal"
  ]
  node [
    id 460
    label "Panama"
  ]
  node [
    id 461
    label "Rumunia"
  ]
  node [
    id 462
    label "Czarnog&#243;ra"
  ]
  node [
    id 463
    label "Malediwy"
  ]
  node [
    id 464
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 465
    label "S&#322;owacja"
  ]
  node [
    id 466
    label "para"
  ]
  node [
    id 467
    label "Egipt"
  ]
  node [
    id 468
    label "zwrot"
  ]
  node [
    id 469
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 470
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 471
    label "Mozambik"
  ]
  node [
    id 472
    label "Kolumbia"
  ]
  node [
    id 473
    label "Laos"
  ]
  node [
    id 474
    label "Burundi"
  ]
  node [
    id 475
    label "Suazi"
  ]
  node [
    id 476
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 477
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 478
    label "Czechy"
  ]
  node [
    id 479
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 480
    label "Wyspy_Marshalla"
  ]
  node [
    id 481
    label "Dominika"
  ]
  node [
    id 482
    label "Trynidad_i_Tobago"
  ]
  node [
    id 483
    label "Syria"
  ]
  node [
    id 484
    label "Palau"
  ]
  node [
    id 485
    label "Gwinea_Bissau"
  ]
  node [
    id 486
    label "Liberia"
  ]
  node [
    id 487
    label "Jamajka"
  ]
  node [
    id 488
    label "Zimbabwe"
  ]
  node [
    id 489
    label "Polska"
  ]
  node [
    id 490
    label "Dominikana"
  ]
  node [
    id 491
    label "Senegal"
  ]
  node [
    id 492
    label "Togo"
  ]
  node [
    id 493
    label "Gujana"
  ]
  node [
    id 494
    label "Gruzja"
  ]
  node [
    id 495
    label "Albania"
  ]
  node [
    id 496
    label "Zair"
  ]
  node [
    id 497
    label "Meksyk"
  ]
  node [
    id 498
    label "Macedonia"
  ]
  node [
    id 499
    label "Chorwacja"
  ]
  node [
    id 500
    label "Kambod&#380;a"
  ]
  node [
    id 501
    label "Monako"
  ]
  node [
    id 502
    label "Mauritius"
  ]
  node [
    id 503
    label "Gwinea"
  ]
  node [
    id 504
    label "Mali"
  ]
  node [
    id 505
    label "Nigeria"
  ]
  node [
    id 506
    label "Kostaryka"
  ]
  node [
    id 507
    label "Hanower"
  ]
  node [
    id 508
    label "Paragwaj"
  ]
  node [
    id 509
    label "W&#322;ochy"
  ]
  node [
    id 510
    label "Seszele"
  ]
  node [
    id 511
    label "Wyspy_Salomona"
  ]
  node [
    id 512
    label "Hiszpania"
  ]
  node [
    id 513
    label "Boliwia"
  ]
  node [
    id 514
    label "Kirgistan"
  ]
  node [
    id 515
    label "Irlandia"
  ]
  node [
    id 516
    label "Czad"
  ]
  node [
    id 517
    label "Irak"
  ]
  node [
    id 518
    label "Lesoto"
  ]
  node [
    id 519
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 520
    label "Malta"
  ]
  node [
    id 521
    label "Andora"
  ]
  node [
    id 522
    label "Chiny"
  ]
  node [
    id 523
    label "Filipiny"
  ]
  node [
    id 524
    label "Antarktis"
  ]
  node [
    id 525
    label "Niemcy"
  ]
  node [
    id 526
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 527
    label "Pakistan"
  ]
  node [
    id 528
    label "terytorium"
  ]
  node [
    id 529
    label "Nikaragua"
  ]
  node [
    id 530
    label "Brazylia"
  ]
  node [
    id 531
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 532
    label "Maroko"
  ]
  node [
    id 533
    label "Portugalia"
  ]
  node [
    id 534
    label "Niger"
  ]
  node [
    id 535
    label "Kenia"
  ]
  node [
    id 536
    label "Botswana"
  ]
  node [
    id 537
    label "Fid&#380;i"
  ]
  node [
    id 538
    label "Tunezja"
  ]
  node [
    id 539
    label "Australia"
  ]
  node [
    id 540
    label "Tajlandia"
  ]
  node [
    id 541
    label "Burkina_Faso"
  ]
  node [
    id 542
    label "interior"
  ]
  node [
    id 543
    label "Tanzania"
  ]
  node [
    id 544
    label "Benin"
  ]
  node [
    id 545
    label "Indie"
  ]
  node [
    id 546
    label "&#321;otwa"
  ]
  node [
    id 547
    label "Kiribati"
  ]
  node [
    id 548
    label "Antigua_i_Barbuda"
  ]
  node [
    id 549
    label "Rodezja"
  ]
  node [
    id 550
    label "Cypr"
  ]
  node [
    id 551
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 552
    label "Peru"
  ]
  node [
    id 553
    label "Austria"
  ]
  node [
    id 554
    label "Urugwaj"
  ]
  node [
    id 555
    label "Jordania"
  ]
  node [
    id 556
    label "Grecja"
  ]
  node [
    id 557
    label "Azerbejd&#380;an"
  ]
  node [
    id 558
    label "Turcja"
  ]
  node [
    id 559
    label "Samoa"
  ]
  node [
    id 560
    label "Sudan"
  ]
  node [
    id 561
    label "Oman"
  ]
  node [
    id 562
    label "ziemia"
  ]
  node [
    id 563
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 564
    label "Uzbekistan"
  ]
  node [
    id 565
    label "Portoryko"
  ]
  node [
    id 566
    label "Honduras"
  ]
  node [
    id 567
    label "Mongolia"
  ]
  node [
    id 568
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 569
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 570
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 571
    label "Serbia"
  ]
  node [
    id 572
    label "Tajwan"
  ]
  node [
    id 573
    label "Wielka_Brytania"
  ]
  node [
    id 574
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 575
    label "Liban"
  ]
  node [
    id 576
    label "Japonia"
  ]
  node [
    id 577
    label "Ghana"
  ]
  node [
    id 578
    label "Belgia"
  ]
  node [
    id 579
    label "Bahrajn"
  ]
  node [
    id 580
    label "Mikronezja"
  ]
  node [
    id 581
    label "Etiopia"
  ]
  node [
    id 582
    label "Kuwejt"
  ]
  node [
    id 583
    label "grupa"
  ]
  node [
    id 584
    label "Bahamy"
  ]
  node [
    id 585
    label "Rosja"
  ]
  node [
    id 586
    label "Mo&#322;dawia"
  ]
  node [
    id 587
    label "Litwa"
  ]
  node [
    id 588
    label "S&#322;owenia"
  ]
  node [
    id 589
    label "Szwajcaria"
  ]
  node [
    id 590
    label "Erytrea"
  ]
  node [
    id 591
    label "Arabia_Saudyjska"
  ]
  node [
    id 592
    label "Kuba"
  ]
  node [
    id 593
    label "granica_pa&#324;stwa"
  ]
  node [
    id 594
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 595
    label "Malezja"
  ]
  node [
    id 596
    label "Korea"
  ]
  node [
    id 597
    label "Jemen"
  ]
  node [
    id 598
    label "Nowa_Zelandia"
  ]
  node [
    id 599
    label "Namibia"
  ]
  node [
    id 600
    label "Nauru"
  ]
  node [
    id 601
    label "holoarktyka"
  ]
  node [
    id 602
    label "Brunei"
  ]
  node [
    id 603
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 604
    label "Khitai"
  ]
  node [
    id 605
    label "Mauretania"
  ]
  node [
    id 606
    label "Iran"
  ]
  node [
    id 607
    label "Gambia"
  ]
  node [
    id 608
    label "Somalia"
  ]
  node [
    id 609
    label "Holandia"
  ]
  node [
    id 610
    label "Turkmenistan"
  ]
  node [
    id 611
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 612
    label "Salwador"
  ]
  node [
    id 613
    label "ludno&#347;&#263;"
  ]
  node [
    id 614
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 615
    label "cz&#322;onek"
  ]
  node [
    id 616
    label "przyk&#322;ad"
  ]
  node [
    id 617
    label "substytuowa&#263;"
  ]
  node [
    id 618
    label "substytuowanie"
  ]
  node [
    id 619
    label "zast&#281;pca"
  ]
  node [
    id 620
    label "ludzko&#347;&#263;"
  ]
  node [
    id 621
    label "asymilowanie"
  ]
  node [
    id 622
    label "wapniak"
  ]
  node [
    id 623
    label "asymilowa&#263;"
  ]
  node [
    id 624
    label "os&#322;abia&#263;"
  ]
  node [
    id 625
    label "posta&#263;"
  ]
  node [
    id 626
    label "hominid"
  ]
  node [
    id 627
    label "podw&#322;adny"
  ]
  node [
    id 628
    label "os&#322;abianie"
  ]
  node [
    id 629
    label "g&#322;owa"
  ]
  node [
    id 630
    label "portrecista"
  ]
  node [
    id 631
    label "dwun&#243;g"
  ]
  node [
    id 632
    label "profanum"
  ]
  node [
    id 633
    label "mikrokosmos"
  ]
  node [
    id 634
    label "nasada"
  ]
  node [
    id 635
    label "duch"
  ]
  node [
    id 636
    label "antropochoria"
  ]
  node [
    id 637
    label "osoba"
  ]
  node [
    id 638
    label "wz&#243;r"
  ]
  node [
    id 639
    label "senior"
  ]
  node [
    id 640
    label "oddzia&#322;ywanie"
  ]
  node [
    id 641
    label "Adam"
  ]
  node [
    id 642
    label "homo_sapiens"
  ]
  node [
    id 643
    label "polifag"
  ]
  node [
    id 644
    label "pair"
  ]
  node [
    id 645
    label "odparowywanie"
  ]
  node [
    id 646
    label "gaz_cieplarniany"
  ]
  node [
    id 647
    label "chodzi&#263;"
  ]
  node [
    id 648
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 649
    label "poker"
  ]
  node [
    id 650
    label "moneta"
  ]
  node [
    id 651
    label "parowanie"
  ]
  node [
    id 652
    label "damp"
  ]
  node [
    id 653
    label "nale&#380;e&#263;"
  ]
  node [
    id 654
    label "sztuka"
  ]
  node [
    id 655
    label "odparowanie"
  ]
  node [
    id 656
    label "odparowa&#263;"
  ]
  node [
    id 657
    label "dodatek"
  ]
  node [
    id 658
    label "jednostka_monetarna"
  ]
  node [
    id 659
    label "smoke"
  ]
  node [
    id 660
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 661
    label "odparowywa&#263;"
  ]
  node [
    id 662
    label "uk&#322;ad"
  ]
  node [
    id 663
    label "gaz"
  ]
  node [
    id 664
    label "wyparowanie"
  ]
  node [
    id 665
    label "obszar"
  ]
  node [
    id 666
    label "Wile&#324;szczyzna"
  ]
  node [
    id 667
    label "jednostka_administracyjna"
  ]
  node [
    id 668
    label "Jukon"
  ]
  node [
    id 669
    label "jednostka_organizacyjna"
  ]
  node [
    id 670
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 671
    label "TOPR"
  ]
  node [
    id 672
    label "endecki"
  ]
  node [
    id 673
    label "od&#322;am"
  ]
  node [
    id 674
    label "przedstawicielstwo"
  ]
  node [
    id 675
    label "Cepelia"
  ]
  node [
    id 676
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 677
    label "ZBoWiD"
  ]
  node [
    id 678
    label "organization"
  ]
  node [
    id 679
    label "centrala"
  ]
  node [
    id 680
    label "GOPR"
  ]
  node [
    id 681
    label "ZOMO"
  ]
  node [
    id 682
    label "ZMP"
  ]
  node [
    id 683
    label "komitet_koordynacyjny"
  ]
  node [
    id 684
    label "przybud&#243;wka"
  ]
  node [
    id 685
    label "boj&#243;wka"
  ]
  node [
    id 686
    label "punkt"
  ]
  node [
    id 687
    label "turn"
  ]
  node [
    id 688
    label "turning"
  ]
  node [
    id 689
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 690
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 691
    label "skr&#281;t"
  ]
  node [
    id 692
    label "obr&#243;t"
  ]
  node [
    id 693
    label "fraza_czasownikowa"
  ]
  node [
    id 694
    label "jednostka_leksykalna"
  ]
  node [
    id 695
    label "odm&#322;adzanie"
  ]
  node [
    id 696
    label "liga"
  ]
  node [
    id 697
    label "jednostka_systematyczna"
  ]
  node [
    id 698
    label "gromada"
  ]
  node [
    id 699
    label "Entuzjastki"
  ]
  node [
    id 700
    label "kompozycja"
  ]
  node [
    id 701
    label "Terranie"
  ]
  node [
    id 702
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 703
    label "category"
  ]
  node [
    id 704
    label "oddzia&#322;"
  ]
  node [
    id 705
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 706
    label "stage_set"
  ]
  node [
    id 707
    label "type"
  ]
  node [
    id 708
    label "specgrupa"
  ]
  node [
    id 709
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 710
    label "&#346;wietliki"
  ]
  node [
    id 711
    label "odm&#322;odzenie"
  ]
  node [
    id 712
    label "Eurogrupa"
  ]
  node [
    id 713
    label "odm&#322;adza&#263;"
  ]
  node [
    id 714
    label "formacja_geologiczna"
  ]
  node [
    id 715
    label "harcerze_starsi"
  ]
  node [
    id 716
    label "Bund"
  ]
  node [
    id 717
    label "PPR"
  ]
  node [
    id 718
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 719
    label "wybranek"
  ]
  node [
    id 720
    label "Jakobici"
  ]
  node [
    id 721
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 722
    label "SLD"
  ]
  node [
    id 723
    label "Razem"
  ]
  node [
    id 724
    label "PiS"
  ]
  node [
    id 725
    label "package"
  ]
  node [
    id 726
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 727
    label "Kuomintang"
  ]
  node [
    id 728
    label "ZSL"
  ]
  node [
    id 729
    label "AWS"
  ]
  node [
    id 730
    label "gra"
  ]
  node [
    id 731
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 732
    label "game"
  ]
  node [
    id 733
    label "blok"
  ]
  node [
    id 734
    label "materia&#322;"
  ]
  node [
    id 735
    label "PO"
  ]
  node [
    id 736
    label "si&#322;a"
  ]
  node [
    id 737
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 738
    label "niedoczas"
  ]
  node [
    id 739
    label "Federali&#347;ci"
  ]
  node [
    id 740
    label "PSL"
  ]
  node [
    id 741
    label "Wigowie"
  ]
  node [
    id 742
    label "ZChN"
  ]
  node [
    id 743
    label "egzekutywa"
  ]
  node [
    id 744
    label "aktyw"
  ]
  node [
    id 745
    label "wybranka"
  ]
  node [
    id 746
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 747
    label "unit"
  ]
  node [
    id 748
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 749
    label "biom"
  ]
  node [
    id 750
    label "szata_ro&#347;linna"
  ]
  node [
    id 751
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 752
    label "formacja_ro&#347;linna"
  ]
  node [
    id 753
    label "przyroda"
  ]
  node [
    id 754
    label "zielono&#347;&#263;"
  ]
  node [
    id 755
    label "pi&#281;tro"
  ]
  node [
    id 756
    label "plant"
  ]
  node [
    id 757
    label "ro&#347;lina"
  ]
  node [
    id 758
    label "geosystem"
  ]
  node [
    id 759
    label "teren"
  ]
  node [
    id 760
    label "inti"
  ]
  node [
    id 761
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 762
    label "sol"
  ]
  node [
    id 763
    label "Afryka_Zachodnia"
  ]
  node [
    id 764
    label "baht"
  ]
  node [
    id 765
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 766
    label "boliviano"
  ]
  node [
    id 767
    label "dong"
  ]
  node [
    id 768
    label "Annam"
  ]
  node [
    id 769
    label "Tonkin"
  ]
  node [
    id 770
    label "colon"
  ]
  node [
    id 771
    label "Ameryka_Centralna"
  ]
  node [
    id 772
    label "Piemont"
  ]
  node [
    id 773
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 774
    label "NATO"
  ]
  node [
    id 775
    label "Italia"
  ]
  node [
    id 776
    label "Kalabria"
  ]
  node [
    id 777
    label "Sardynia"
  ]
  node [
    id 778
    label "Apulia"
  ]
  node [
    id 779
    label "strefa_euro"
  ]
  node [
    id 780
    label "Ok&#281;cie"
  ]
  node [
    id 781
    label "Karyntia"
  ]
  node [
    id 782
    label "Umbria"
  ]
  node [
    id 783
    label "Romania"
  ]
  node [
    id 784
    label "Sycylia"
  ]
  node [
    id 785
    label "Warszawa"
  ]
  node [
    id 786
    label "lir"
  ]
  node [
    id 787
    label "Toskania"
  ]
  node [
    id 788
    label "Lombardia"
  ]
  node [
    id 789
    label "Liguria"
  ]
  node [
    id 790
    label "Kampania"
  ]
  node [
    id 791
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 792
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 793
    label "Ad&#380;aria"
  ]
  node [
    id 794
    label "lari"
  ]
  node [
    id 795
    label "Jukatan"
  ]
  node [
    id 796
    label "dolar_Belize"
  ]
  node [
    id 797
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 798
    label "dolar"
  ]
  node [
    id 799
    label "Ohio"
  ]
  node [
    id 800
    label "P&#243;&#322;noc"
  ]
  node [
    id 801
    label "Nowy_York"
  ]
  node [
    id 802
    label "Illinois"
  ]
  node [
    id 803
    label "Po&#322;udnie"
  ]
  node [
    id 804
    label "Kalifornia"
  ]
  node [
    id 805
    label "Wirginia"
  ]
  node [
    id 806
    label "Teksas"
  ]
  node [
    id 807
    label "Waszyngton"
  ]
  node [
    id 808
    label "zielona_karta"
  ]
  node [
    id 809
    label "Massachusetts"
  ]
  node [
    id 810
    label "Alaska"
  ]
  node [
    id 811
    label "Hawaje"
  ]
  node [
    id 812
    label "Maryland"
  ]
  node [
    id 813
    label "Michigan"
  ]
  node [
    id 814
    label "Arizona"
  ]
  node [
    id 815
    label "Georgia"
  ]
  node [
    id 816
    label "stan_wolny"
  ]
  node [
    id 817
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 818
    label "Pensylwania"
  ]
  node [
    id 819
    label "Luizjana"
  ]
  node [
    id 820
    label "Nowy_Meksyk"
  ]
  node [
    id 821
    label "Wuj_Sam"
  ]
  node [
    id 822
    label "Alabama"
  ]
  node [
    id 823
    label "Kansas"
  ]
  node [
    id 824
    label "Oregon"
  ]
  node [
    id 825
    label "Zach&#243;d"
  ]
  node [
    id 826
    label "Floryda"
  ]
  node [
    id 827
    label "Oklahoma"
  ]
  node [
    id 828
    label "Hudson"
  ]
  node [
    id 829
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 830
    label "somoni"
  ]
  node [
    id 831
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 832
    label "perper"
  ]
  node [
    id 833
    label "Sand&#380;ak"
  ]
  node [
    id 834
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 835
    label "euro"
  ]
  node [
    id 836
    label "Bengal"
  ]
  node [
    id 837
    label "taka"
  ]
  node [
    id 838
    label "Karelia"
  ]
  node [
    id 839
    label "Mari_El"
  ]
  node [
    id 840
    label "Inguszetia"
  ]
  node [
    id 841
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 842
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 843
    label "Udmurcja"
  ]
  node [
    id 844
    label "Newa"
  ]
  node [
    id 845
    label "&#321;adoga"
  ]
  node [
    id 846
    label "Czeczenia"
  ]
  node [
    id 847
    label "Anadyr"
  ]
  node [
    id 848
    label "Syberia"
  ]
  node [
    id 849
    label "Tatarstan"
  ]
  node [
    id 850
    label "Wszechrosja"
  ]
  node [
    id 851
    label "Azja"
  ]
  node [
    id 852
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 853
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 854
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 855
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 856
    label "Europa_Wschodnia"
  ]
  node [
    id 857
    label "Witim"
  ]
  node [
    id 858
    label "Kamczatka"
  ]
  node [
    id 859
    label "Jama&#322;"
  ]
  node [
    id 860
    label "Dagestan"
  ]
  node [
    id 861
    label "Baszkiria"
  ]
  node [
    id 862
    label "Tuwa"
  ]
  node [
    id 863
    label "car"
  ]
  node [
    id 864
    label "Komi"
  ]
  node [
    id 865
    label "Czuwaszja"
  ]
  node [
    id 866
    label "Chakasja"
  ]
  node [
    id 867
    label "Perm"
  ]
  node [
    id 868
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 869
    label "Ajon"
  ]
  node [
    id 870
    label "Adygeja"
  ]
  node [
    id 871
    label "Dniepr"
  ]
  node [
    id 872
    label "rubel_rosyjski"
  ]
  node [
    id 873
    label "Don"
  ]
  node [
    id 874
    label "Mordowia"
  ]
  node [
    id 875
    label "s&#322;owianofilstwo"
  ]
  node [
    id 876
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 877
    label "gourde"
  ]
  node [
    id 878
    label "Karaiby"
  ]
  node [
    id 879
    label "escudo_angolskie"
  ]
  node [
    id 880
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 881
    label "kwanza"
  ]
  node [
    id 882
    label "ariary"
  ]
  node [
    id 883
    label "Ocean_Indyjski"
  ]
  node [
    id 884
    label "Afryka_Wschodnia"
  ]
  node [
    id 885
    label "frank_malgaski"
  ]
  node [
    id 886
    label "Unia_Europejska"
  ]
  node [
    id 887
    label "Windawa"
  ]
  node [
    id 888
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 889
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 890
    label "&#379;mud&#378;"
  ]
  node [
    id 891
    label "lit"
  ]
  node [
    id 892
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 893
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 894
    label "Synaj"
  ]
  node [
    id 895
    label "paraszyt"
  ]
  node [
    id 896
    label "funt_egipski"
  ]
  node [
    id 897
    label "Amhara"
  ]
  node [
    id 898
    label "birr"
  ]
  node [
    id 899
    label "Syjon"
  ]
  node [
    id 900
    label "negus"
  ]
  node [
    id 901
    label "peso_kolumbijskie"
  ]
  node [
    id 902
    label "Orinoko"
  ]
  node [
    id 903
    label "rial_katarski"
  ]
  node [
    id 904
    label "dram"
  ]
  node [
    id 905
    label "Limburgia"
  ]
  node [
    id 906
    label "gulden"
  ]
  node [
    id 907
    label "Zelandia"
  ]
  node [
    id 908
    label "Niderlandy"
  ]
  node [
    id 909
    label "Brabancja"
  ]
  node [
    id 910
    label "cedi"
  ]
  node [
    id 911
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 912
    label "milrejs"
  ]
  node [
    id 913
    label "cruzado"
  ]
  node [
    id 914
    label "real"
  ]
  node [
    id 915
    label "frank_monakijski"
  ]
  node [
    id 916
    label "Fryburg"
  ]
  node [
    id 917
    label "Bazylea"
  ]
  node [
    id 918
    label "Alpy"
  ]
  node [
    id 919
    label "frank_szwajcarski"
  ]
  node [
    id 920
    label "Helwecja"
  ]
  node [
    id 921
    label "Europa_Zachodnia"
  ]
  node [
    id 922
    label "Berno"
  ]
  node [
    id 923
    label "Ba&#322;kany"
  ]
  node [
    id 924
    label "lej_mo&#322;dawski"
  ]
  node [
    id 925
    label "Naddniestrze"
  ]
  node [
    id 926
    label "Dniestr"
  ]
  node [
    id 927
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 928
    label "Gagauzja"
  ]
  node [
    id 929
    label "Indie_Zachodnie"
  ]
  node [
    id 930
    label "Sikkim"
  ]
  node [
    id 931
    label "Asam"
  ]
  node [
    id 932
    label "Kaszmir"
  ]
  node [
    id 933
    label "rupia_indyjska"
  ]
  node [
    id 934
    label "Indie_Portugalskie"
  ]
  node [
    id 935
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 936
    label "Indie_Wschodnie"
  ]
  node [
    id 937
    label "Kerala"
  ]
  node [
    id 938
    label "Bollywood"
  ]
  node [
    id 939
    label "Pend&#380;ab"
  ]
  node [
    id 940
    label "boliwar"
  ]
  node [
    id 941
    label "naira"
  ]
  node [
    id 942
    label "frank_gwinejski"
  ]
  node [
    id 943
    label "Karaka&#322;pacja"
  ]
  node [
    id 944
    label "dolar_liberyjski"
  ]
  node [
    id 945
    label "Dacja"
  ]
  node [
    id 946
    label "lej_rumu&#324;ski"
  ]
  node [
    id 947
    label "Siedmiogr&#243;d"
  ]
  node [
    id 948
    label "Dobrud&#380;a"
  ]
  node [
    id 949
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 950
    label "dolar_namibijski"
  ]
  node [
    id 951
    label "kuna"
  ]
  node [
    id 952
    label "Rugia"
  ]
  node [
    id 953
    label "Saksonia"
  ]
  node [
    id 954
    label "Dolna_Saksonia"
  ]
  node [
    id 955
    label "Anglosas"
  ]
  node [
    id 956
    label "Hesja"
  ]
  node [
    id 957
    label "Szlezwik"
  ]
  node [
    id 958
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 959
    label "Wirtembergia"
  ]
  node [
    id 960
    label "Po&#322;abie"
  ]
  node [
    id 961
    label "Germania"
  ]
  node [
    id 962
    label "Frankonia"
  ]
  node [
    id 963
    label "Badenia"
  ]
  node [
    id 964
    label "Holsztyn"
  ]
  node [
    id 965
    label "Bawaria"
  ]
  node [
    id 966
    label "marka"
  ]
  node [
    id 967
    label "Szwabia"
  ]
  node [
    id 968
    label "Brandenburgia"
  ]
  node [
    id 969
    label "Niemcy_Zachodnie"
  ]
  node [
    id 970
    label "Nadrenia"
  ]
  node [
    id 971
    label "Westfalia"
  ]
  node [
    id 972
    label "Turyngia"
  ]
  node [
    id 973
    label "Helgoland"
  ]
  node [
    id 974
    label "Karlsbad"
  ]
  node [
    id 975
    label "Niemcy_Wschodnie"
  ]
  node [
    id 976
    label "korona_w&#281;gierska"
  ]
  node [
    id 977
    label "forint"
  ]
  node [
    id 978
    label "Lipt&#243;w"
  ]
  node [
    id 979
    label "tenge"
  ]
  node [
    id 980
    label "szach"
  ]
  node [
    id 981
    label "Baktria"
  ]
  node [
    id 982
    label "afgani"
  ]
  node [
    id 983
    label "kip"
  ]
  node [
    id 984
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 985
    label "Salzburg"
  ]
  node [
    id 986
    label "Rakuzy"
  ]
  node [
    id 987
    label "Dyja"
  ]
  node [
    id 988
    label "Tyrol"
  ]
  node [
    id 989
    label "konsulent"
  ]
  node [
    id 990
    label "szyling_austryjacki"
  ]
  node [
    id 991
    label "peso_urugwajskie"
  ]
  node [
    id 992
    label "rial_jeme&#324;ski"
  ]
  node [
    id 993
    label "korona_esto&#324;ska"
  ]
  node [
    id 994
    label "Skandynawia"
  ]
  node [
    id 995
    label "Inflanty"
  ]
  node [
    id 996
    label "marka_esto&#324;ska"
  ]
  node [
    id 997
    label "Polinezja"
  ]
  node [
    id 998
    label "tala"
  ]
  node [
    id 999
    label "Oceania"
  ]
  node [
    id 1000
    label "Podole"
  ]
  node [
    id 1001
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1002
    label "Wsch&#243;d"
  ]
  node [
    id 1003
    label "Zakarpacie"
  ]
  node [
    id 1004
    label "Naddnieprze"
  ]
  node [
    id 1005
    label "Ma&#322;orosja"
  ]
  node [
    id 1006
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1007
    label "Nadbu&#380;e"
  ]
  node [
    id 1008
    label "hrywna"
  ]
  node [
    id 1009
    label "Zaporo&#380;e"
  ]
  node [
    id 1010
    label "Krym"
  ]
  node [
    id 1011
    label "Przykarpacie"
  ]
  node [
    id 1012
    label "Kozaczyzna"
  ]
  node [
    id 1013
    label "karbowaniec"
  ]
  node [
    id 1014
    label "riel"
  ]
  node [
    id 1015
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1016
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1017
    label "kyat"
  ]
  node [
    id 1018
    label "Arakan"
  ]
  node [
    id 1019
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1020
    label "funt_liba&#324;ski"
  ]
  node [
    id 1021
    label "Mariany"
  ]
  node [
    id 1022
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1023
    label "Maghreb"
  ]
  node [
    id 1024
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1025
    label "dinar_algierski"
  ]
  node [
    id 1026
    label "Kabylia"
  ]
  node [
    id 1027
    label "ringgit"
  ]
  node [
    id 1028
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1029
    label "Borneo"
  ]
  node [
    id 1030
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1031
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1032
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1033
    label "lira_izraelska"
  ]
  node [
    id 1034
    label "szekel"
  ]
  node [
    id 1035
    label "Galilea"
  ]
  node [
    id 1036
    label "Judea"
  ]
  node [
    id 1037
    label "tolar"
  ]
  node [
    id 1038
    label "frank_luksemburski"
  ]
  node [
    id 1039
    label "lempira"
  ]
  node [
    id 1040
    label "Pozna&#324;"
  ]
  node [
    id 1041
    label "lira_malta&#324;ska"
  ]
  node [
    id 1042
    label "Gozo"
  ]
  node [
    id 1043
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1044
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1045
    label "Paros"
  ]
  node [
    id 1046
    label "Epir"
  ]
  node [
    id 1047
    label "panhellenizm"
  ]
  node [
    id 1048
    label "Eubea"
  ]
  node [
    id 1049
    label "Rodos"
  ]
  node [
    id 1050
    label "Achaja"
  ]
  node [
    id 1051
    label "Termopile"
  ]
  node [
    id 1052
    label "Attyka"
  ]
  node [
    id 1053
    label "Hellada"
  ]
  node [
    id 1054
    label "Etolia"
  ]
  node [
    id 1055
    label "palestra"
  ]
  node [
    id 1056
    label "Kreta"
  ]
  node [
    id 1057
    label "drachma"
  ]
  node [
    id 1058
    label "Olimp"
  ]
  node [
    id 1059
    label "Tesalia"
  ]
  node [
    id 1060
    label "Peloponez"
  ]
  node [
    id 1061
    label "Eolia"
  ]
  node [
    id 1062
    label "Beocja"
  ]
  node [
    id 1063
    label "Parnas"
  ]
  node [
    id 1064
    label "Lesbos"
  ]
  node [
    id 1065
    label "Atlantyk"
  ]
  node [
    id 1066
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1067
    label "Ulster"
  ]
  node [
    id 1068
    label "funt_irlandzki"
  ]
  node [
    id 1069
    label "tugrik"
  ]
  node [
    id 1070
    label "Azja_Wschodnia"
  ]
  node [
    id 1071
    label "Buriaci"
  ]
  node [
    id 1072
    label "ajmak"
  ]
  node [
    id 1073
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1074
    label "Lotaryngia"
  ]
  node [
    id 1075
    label "Bordeaux"
  ]
  node [
    id 1076
    label "Pikardia"
  ]
  node [
    id 1077
    label "Masyw_Centralny"
  ]
  node [
    id 1078
    label "Akwitania"
  ]
  node [
    id 1079
    label "Alzacja"
  ]
  node [
    id 1080
    label "Sekwana"
  ]
  node [
    id 1081
    label "Langwedocja"
  ]
  node [
    id 1082
    label "Armagnac"
  ]
  node [
    id 1083
    label "Martynika"
  ]
  node [
    id 1084
    label "Bretania"
  ]
  node [
    id 1085
    label "Sabaudia"
  ]
  node [
    id 1086
    label "Korsyka"
  ]
  node [
    id 1087
    label "Normandia"
  ]
  node [
    id 1088
    label "Gaskonia"
  ]
  node [
    id 1089
    label "Burgundia"
  ]
  node [
    id 1090
    label "frank_francuski"
  ]
  node [
    id 1091
    label "Wandea"
  ]
  node [
    id 1092
    label "Prowansja"
  ]
  node [
    id 1093
    label "Gwadelupa"
  ]
  node [
    id 1094
    label "lew"
  ]
  node [
    id 1095
    label "c&#243;rdoba"
  ]
  node [
    id 1096
    label "dolar_Zimbabwe"
  ]
  node [
    id 1097
    label "frank_rwandyjski"
  ]
  node [
    id 1098
    label "kwacha_zambijska"
  ]
  node [
    id 1099
    label "&#322;at"
  ]
  node [
    id 1100
    label "Kurlandia"
  ]
  node [
    id 1101
    label "Liwonia"
  ]
  node [
    id 1102
    label "rubel_&#322;otewski"
  ]
  node [
    id 1103
    label "Himalaje"
  ]
  node [
    id 1104
    label "rupia_nepalska"
  ]
  node [
    id 1105
    label "funt_suda&#324;ski"
  ]
  node [
    id 1106
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1107
    label "dolar_bahamski"
  ]
  node [
    id 1108
    label "Wielka_Bahama"
  ]
  node [
    id 1109
    label "Mazowsze"
  ]
  node [
    id 1110
    label "Pa&#322;uki"
  ]
  node [
    id 1111
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1112
    label "Powi&#347;le"
  ]
  node [
    id 1113
    label "Wolin"
  ]
  node [
    id 1114
    label "z&#322;oty"
  ]
  node [
    id 1115
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1116
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1117
    label "So&#322;a"
  ]
  node [
    id 1118
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1119
    label "Opolskie"
  ]
  node [
    id 1120
    label "Suwalszczyzna"
  ]
  node [
    id 1121
    label "Krajna"
  ]
  node [
    id 1122
    label "barwy_polskie"
  ]
  node [
    id 1123
    label "Podlasie"
  ]
  node [
    id 1124
    label "Izera"
  ]
  node [
    id 1125
    label "Ma&#322;opolska"
  ]
  node [
    id 1126
    label "Warmia"
  ]
  node [
    id 1127
    label "Mazury"
  ]
  node [
    id 1128
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1129
    label "Lubelszczyzna"
  ]
  node [
    id 1130
    label "Kaczawa"
  ]
  node [
    id 1131
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1132
    label "Kielecczyzna"
  ]
  node [
    id 1133
    label "Lubuskie"
  ]
  node [
    id 1134
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1135
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1136
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1137
    label "Kujawy"
  ]
  node [
    id 1138
    label "Podkarpacie"
  ]
  node [
    id 1139
    label "Wielkopolska"
  ]
  node [
    id 1140
    label "Wis&#322;a"
  ]
  node [
    id 1141
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1142
    label "Bory_Tucholskie"
  ]
  node [
    id 1143
    label "Antyle"
  ]
  node [
    id 1144
    label "dolar_Tuvalu"
  ]
  node [
    id 1145
    label "dinar_iracki"
  ]
  node [
    id 1146
    label "korona_s&#322;owacka"
  ]
  node [
    id 1147
    label "Turiec"
  ]
  node [
    id 1148
    label "jen"
  ]
  node [
    id 1149
    label "jinja"
  ]
  node [
    id 1150
    label "Okinawa"
  ]
  node [
    id 1151
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1152
    label "Japonica"
  ]
  node [
    id 1153
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1154
    label "szyling_kenijski"
  ]
  node [
    id 1155
    label "peso_chilijskie"
  ]
  node [
    id 1156
    label "Zanzibar"
  ]
  node [
    id 1157
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1158
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1159
    label "Cebu"
  ]
  node [
    id 1160
    label "Sahara"
  ]
  node [
    id 1161
    label "Tasmania"
  ]
  node [
    id 1162
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1163
    label "dolar_australijski"
  ]
  node [
    id 1164
    label "Quebec"
  ]
  node [
    id 1165
    label "dolar_kanadyjski"
  ]
  node [
    id 1166
    label "Nowa_Fundlandia"
  ]
  node [
    id 1167
    label "quetzal"
  ]
  node [
    id 1168
    label "Manica"
  ]
  node [
    id 1169
    label "escudo_mozambickie"
  ]
  node [
    id 1170
    label "Cabo_Delgado"
  ]
  node [
    id 1171
    label "Inhambane"
  ]
  node [
    id 1172
    label "Maputo"
  ]
  node [
    id 1173
    label "Gaza"
  ]
  node [
    id 1174
    label "Niasa"
  ]
  node [
    id 1175
    label "Nampula"
  ]
  node [
    id 1176
    label "metical"
  ]
  node [
    id 1177
    label "frank_tunezyjski"
  ]
  node [
    id 1178
    label "dinar_tunezyjski"
  ]
  node [
    id 1179
    label "lud"
  ]
  node [
    id 1180
    label "frank_kongijski"
  ]
  node [
    id 1181
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1182
    label "dinar_Bahrajnu"
  ]
  node [
    id 1183
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1184
    label "escudo_portugalskie"
  ]
  node [
    id 1185
    label "Melanezja"
  ]
  node [
    id 1186
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1187
    label "d&#380;amahirijja"
  ]
  node [
    id 1188
    label "dinar_libijski"
  ]
  node [
    id 1189
    label "balboa"
  ]
  node [
    id 1190
    label "dolar_surinamski"
  ]
  node [
    id 1191
    label "dolar_Brunei"
  ]
  node [
    id 1192
    label "Estremadura"
  ]
  node [
    id 1193
    label "Andaluzja"
  ]
  node [
    id 1194
    label "Kastylia"
  ]
  node [
    id 1195
    label "Galicja"
  ]
  node [
    id 1196
    label "Rzym_Zachodni"
  ]
  node [
    id 1197
    label "Aragonia"
  ]
  node [
    id 1198
    label "hacjender"
  ]
  node [
    id 1199
    label "Asturia"
  ]
  node [
    id 1200
    label "Baskonia"
  ]
  node [
    id 1201
    label "Majorka"
  ]
  node [
    id 1202
    label "Walencja"
  ]
  node [
    id 1203
    label "peseta"
  ]
  node [
    id 1204
    label "Katalonia"
  ]
  node [
    id 1205
    label "Luksemburgia"
  ]
  node [
    id 1206
    label "frank_belgijski"
  ]
  node [
    id 1207
    label "Walonia"
  ]
  node [
    id 1208
    label "Flandria"
  ]
  node [
    id 1209
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1210
    label "dolar_Barbadosu"
  ]
  node [
    id 1211
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1212
    label "korona_czeska"
  ]
  node [
    id 1213
    label "Lasko"
  ]
  node [
    id 1214
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1215
    label "Wojwodina"
  ]
  node [
    id 1216
    label "dinar_serbski"
  ]
  node [
    id 1217
    label "funt_syryjski"
  ]
  node [
    id 1218
    label "alawizm"
  ]
  node [
    id 1219
    label "Szantung"
  ]
  node [
    id 1220
    label "Chiny_Zachodnie"
  ]
  node [
    id 1221
    label "Kuantung"
  ]
  node [
    id 1222
    label "D&#380;ungaria"
  ]
  node [
    id 1223
    label "yuan"
  ]
  node [
    id 1224
    label "Hongkong"
  ]
  node [
    id 1225
    label "Chiny_Wschodnie"
  ]
  node [
    id 1226
    label "Guangdong"
  ]
  node [
    id 1227
    label "Junnan"
  ]
  node [
    id 1228
    label "Mand&#380;uria"
  ]
  node [
    id 1229
    label "Syczuan"
  ]
  node [
    id 1230
    label "zair"
  ]
  node [
    id 1231
    label "Katanga"
  ]
  node [
    id 1232
    label "ugija"
  ]
  node [
    id 1233
    label "dalasi"
  ]
  node [
    id 1234
    label "funt_cypryjski"
  ]
  node [
    id 1235
    label "Afrodyzje"
  ]
  node [
    id 1236
    label "frank_alba&#324;ski"
  ]
  node [
    id 1237
    label "lek"
  ]
  node [
    id 1238
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1239
    label "kafar"
  ]
  node [
    id 1240
    label "dolar_jamajski"
  ]
  node [
    id 1241
    label "Ocean_Spokojny"
  ]
  node [
    id 1242
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1243
    label "som"
  ]
  node [
    id 1244
    label "guarani"
  ]
  node [
    id 1245
    label "rial_ira&#324;ski"
  ]
  node [
    id 1246
    label "mu&#322;&#322;a"
  ]
  node [
    id 1247
    label "Persja"
  ]
  node [
    id 1248
    label "Jawa"
  ]
  node [
    id 1249
    label "Sumatra"
  ]
  node [
    id 1250
    label "rupia_indonezyjska"
  ]
  node [
    id 1251
    label "Nowa_Gwinea"
  ]
  node [
    id 1252
    label "Moluki"
  ]
  node [
    id 1253
    label "szyling_somalijski"
  ]
  node [
    id 1254
    label "szyling_ugandyjski"
  ]
  node [
    id 1255
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1256
    label "Ujgur"
  ]
  node [
    id 1257
    label "Azja_Mniejsza"
  ]
  node [
    id 1258
    label "lira_turecka"
  ]
  node [
    id 1259
    label "Pireneje"
  ]
  node [
    id 1260
    label "nakfa"
  ]
  node [
    id 1261
    label "won"
  ]
  node [
    id 1262
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1263
    label "&#346;wite&#378;"
  ]
  node [
    id 1264
    label "dinar_kuwejcki"
  ]
  node [
    id 1265
    label "Nachiczewan"
  ]
  node [
    id 1266
    label "manat_azerski"
  ]
  node [
    id 1267
    label "Karabach"
  ]
  node [
    id 1268
    label "dolar_Kiribati"
  ]
  node [
    id 1269
    label "Anglia"
  ]
  node [
    id 1270
    label "Amazonia"
  ]
  node [
    id 1271
    label "plantowa&#263;"
  ]
  node [
    id 1272
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1273
    label "zapadnia"
  ]
  node [
    id 1274
    label "Zamojszczyzna"
  ]
  node [
    id 1275
    label "budynek"
  ]
  node [
    id 1276
    label "skorupa_ziemska"
  ]
  node [
    id 1277
    label "Turkiestan"
  ]
  node [
    id 1278
    label "Noworosja"
  ]
  node [
    id 1279
    label "Mezoameryka"
  ]
  node [
    id 1280
    label "glinowanie"
  ]
  node [
    id 1281
    label "Kurdystan"
  ]
  node [
    id 1282
    label "martwica"
  ]
  node [
    id 1283
    label "Szkocja"
  ]
  node [
    id 1284
    label "litosfera"
  ]
  node [
    id 1285
    label "penetrator"
  ]
  node [
    id 1286
    label "glinowa&#263;"
  ]
  node [
    id 1287
    label "Zabajkale"
  ]
  node [
    id 1288
    label "domain"
  ]
  node [
    id 1289
    label "Bojkowszczyzna"
  ]
  node [
    id 1290
    label "podglebie"
  ]
  node [
    id 1291
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1292
    label "Pamir"
  ]
  node [
    id 1293
    label "Indochiny"
  ]
  node [
    id 1294
    label "miejsce"
  ]
  node [
    id 1295
    label "Kurpie"
  ]
  node [
    id 1296
    label "S&#261;decczyzna"
  ]
  node [
    id 1297
    label "kort"
  ]
  node [
    id 1298
    label "czynnik_produkcji"
  ]
  node [
    id 1299
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1300
    label "Huculszczyzna"
  ]
  node [
    id 1301
    label "pojazd"
  ]
  node [
    id 1302
    label "powierzchnia"
  ]
  node [
    id 1303
    label "Podhale"
  ]
  node [
    id 1304
    label "pr&#243;chnica"
  ]
  node [
    id 1305
    label "Hercegowina"
  ]
  node [
    id 1306
    label "Walia"
  ]
  node [
    id 1307
    label "pomieszczenie"
  ]
  node [
    id 1308
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1309
    label "ryzosfera"
  ]
  node [
    id 1310
    label "Kaukaz"
  ]
  node [
    id 1311
    label "Biskupizna"
  ]
  node [
    id 1312
    label "Bo&#347;nia"
  ]
  node [
    id 1313
    label "p&#322;aszczyzna"
  ]
  node [
    id 1314
    label "dotleni&#263;"
  ]
  node [
    id 1315
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1316
    label "Opolszczyzna"
  ]
  node [
    id 1317
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1318
    label "Podbeskidzie"
  ]
  node [
    id 1319
    label "Kaszuby"
  ]
  node [
    id 1320
    label "Ko&#322;yma"
  ]
  node [
    id 1321
    label "glej"
  ]
  node [
    id 1322
    label "posadzka"
  ]
  node [
    id 1323
    label "Polesie"
  ]
  node [
    id 1324
    label "Palestyna"
  ]
  node [
    id 1325
    label "Lauda"
  ]
  node [
    id 1326
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1327
    label "Laponia"
  ]
  node [
    id 1328
    label "Yorkshire"
  ]
  node [
    id 1329
    label "Zag&#243;rze"
  ]
  node [
    id 1330
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1331
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1332
    label "przestrze&#324;"
  ]
  node [
    id 1333
    label "Oksytania"
  ]
  node [
    id 1334
    label "Kociewie"
  ]
  node [
    id 1335
    label "by&#263;"
  ]
  node [
    id 1336
    label "decide"
  ]
  node [
    id 1337
    label "pies_my&#347;liwski"
  ]
  node [
    id 1338
    label "decydowa&#263;"
  ]
  node [
    id 1339
    label "represent"
  ]
  node [
    id 1340
    label "zatrzymywa&#263;"
  ]
  node [
    id 1341
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1342
    label "typify"
  ]
  node [
    id 1343
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1344
    label "komornik"
  ]
  node [
    id 1345
    label "suspend"
  ]
  node [
    id 1346
    label "zgarnia&#263;"
  ]
  node [
    id 1347
    label "throng"
  ]
  node [
    id 1348
    label "unieruchamia&#263;"
  ]
  node [
    id 1349
    label "przerywa&#263;"
  ]
  node [
    id 1350
    label "przechowywa&#263;"
  ]
  node [
    id 1351
    label "&#322;apa&#263;"
  ]
  node [
    id 1352
    label "przetrzymywa&#263;"
  ]
  node [
    id 1353
    label "allude"
  ]
  node [
    id 1354
    label "zaczepia&#263;"
  ]
  node [
    id 1355
    label "powodowa&#263;"
  ]
  node [
    id 1356
    label "prosecute"
  ]
  node [
    id 1357
    label "hold"
  ]
  node [
    id 1358
    label "zamyka&#263;"
  ]
  node [
    id 1359
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1360
    label "determine"
  ]
  node [
    id 1361
    label "reakcja_chemiczna"
  ]
  node [
    id 1362
    label "klasyfikator"
  ]
  node [
    id 1363
    label "mean"
  ]
  node [
    id 1364
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1365
    label "equal"
  ]
  node [
    id 1366
    label "trwa&#263;"
  ]
  node [
    id 1367
    label "si&#281;ga&#263;"
  ]
  node [
    id 1368
    label "obecno&#347;&#263;"
  ]
  node [
    id 1369
    label "stand"
  ]
  node [
    id 1370
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1371
    label "uczestniczy&#263;"
  ]
  node [
    id 1372
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 1373
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1374
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 1375
    label "relate"
  ]
  node [
    id 1376
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 1377
    label "obwiniony"
  ]
  node [
    id 1378
    label "r&#281;kopis"
  ]
  node [
    id 1379
    label "kodeks_pracy"
  ]
  node [
    id 1380
    label "kodeks_morski"
  ]
  node [
    id 1381
    label "Justynian"
  ]
  node [
    id 1382
    label "code"
  ]
  node [
    id 1383
    label "kodeks_drogowy"
  ]
  node [
    id 1384
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 1385
    label "kodeks_rodzinny"
  ]
  node [
    id 1386
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 1387
    label "kodeks_cywilny"
  ]
  node [
    id 1388
    label "kodeks_karny"
  ]
  node [
    id 1389
    label "cymelium"
  ]
  node [
    id 1390
    label "norma_prawna"
  ]
  node [
    id 1391
    label "przedawnienie_si&#281;"
  ]
  node [
    id 1392
    label "przedawnianie_si&#281;"
  ]
  node [
    id 1393
    label "porada"
  ]
  node [
    id 1394
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 1395
    label "regulation"
  ]
  node [
    id 1396
    label "recepta"
  ]
  node [
    id 1397
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 1398
    label "wsp&#243;lnictwo"
  ]
  node [
    id 1399
    label "whole"
  ]
  node [
    id 1400
    label "skupienie"
  ]
  node [
    id 1401
    label "The_Beatles"
  ]
  node [
    id 1402
    label "zabudowania"
  ]
  node [
    id 1403
    label "group"
  ]
  node [
    id 1404
    label "zespolik"
  ]
  node [
    id 1405
    label "schorzenie"
  ]
  node [
    id 1406
    label "Depeche_Mode"
  ]
  node [
    id 1407
    label "batch"
  ]
  node [
    id 1408
    label "uczestnictwo"
  ]
  node [
    id 1409
    label "handlowo"
  ]
  node [
    id 1410
    label "suppress"
  ]
  node [
    id 1411
    label "wytycza&#263;"
  ]
  node [
    id 1412
    label "zmniejsza&#263;"
  ]
  node [
    id 1413
    label "environment"
  ]
  node [
    id 1414
    label "bound"
  ]
  node [
    id 1415
    label "wi&#281;zienie"
  ]
  node [
    id 1416
    label "zmienia&#263;"
  ]
  node [
    id 1417
    label "control"
  ]
  node [
    id 1418
    label "ustala&#263;"
  ]
  node [
    id 1419
    label "wyznacza&#263;"
  ]
  node [
    id 1420
    label "performance"
  ]
  node [
    id 1421
    label "pierdel"
  ]
  node [
    id 1422
    label "&#321;ubianka"
  ]
  node [
    id 1423
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 1424
    label "ciupa"
  ]
  node [
    id 1425
    label "reedukator"
  ]
  node [
    id 1426
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 1427
    label "Butyrki"
  ]
  node [
    id 1428
    label "imprisonment"
  ]
  node [
    id 1429
    label "miejsce_odosobnienia"
  ]
  node [
    id 1430
    label "uniemo&#380;liwianie"
  ]
  node [
    id 1431
    label "sytuacja"
  ]
  node [
    id 1432
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1433
    label "korzysta&#263;"
  ]
  node [
    id 1434
    label "distribute"
  ]
  node [
    id 1435
    label "bash"
  ]
  node [
    id 1436
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1437
    label "doznawa&#263;"
  ]
  node [
    id 1438
    label "wnie&#347;&#263;"
  ]
  node [
    id 1439
    label "odwo&#322;a&#263;_si&#281;"
  ]
  node [
    id 1440
    label "supply"
  ]
  node [
    id 1441
    label "zanie&#347;&#263;"
  ]
  node [
    id 1442
    label "op&#322;aci&#263;"
  ]
  node [
    id 1443
    label "doda&#263;"
  ]
  node [
    id 1444
    label "przynie&#347;&#263;"
  ]
  node [
    id 1445
    label "wywo&#322;a&#263;"
  ]
  node [
    id 1446
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 1447
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1448
    label "club"
  ]
  node [
    id 1449
    label "przed&#322;o&#380;y&#263;"
  ]
  node [
    id 1450
    label "zobowi&#261;zanie"
  ]
  node [
    id 1451
    label "wskaz&#243;wka"
  ]
  node [
    id 1452
    label "zlecenie"
  ]
  node [
    id 1453
    label "receipt"
  ]
  node [
    id 1454
    label "receptariusz"
  ]
  node [
    id 1455
    label "walne_zgromadzenie"
  ]
  node [
    id 1456
    label "transza_otwarta"
  ]
  node [
    id 1457
    label "akcjonariat"
  ]
  node [
    id 1458
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 1459
    label "gwarectwo"
  ]
  node [
    id 1460
    label "wsp&#243;&#322;w&#322;a&#347;ciciel"
  ]
  node [
    id 1461
    label "mienie"
  ]
  node [
    id 1462
    label "wsp&#243;lnik"
  ]
  node [
    id 1463
    label "wykupienie"
  ]
  node [
    id 1464
    label "bycie_w_posiadaniu"
  ]
  node [
    id 1465
    label "wykupywanie"
  ]
  node [
    id 1466
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1467
    label "wzi&#261;&#263;"
  ]
  node [
    id 1468
    label "catch"
  ]
  node [
    id 1469
    label "odziedziczy&#263;"
  ]
  node [
    id 1470
    label "ruszy&#263;"
  ]
  node [
    id 1471
    label "zaatakowa&#263;"
  ]
  node [
    id 1472
    label "skorzysta&#263;"
  ]
  node [
    id 1473
    label "uciec"
  ]
  node [
    id 1474
    label "receive"
  ]
  node [
    id 1475
    label "nakaza&#263;"
  ]
  node [
    id 1476
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1477
    label "obskoczy&#263;"
  ]
  node [
    id 1478
    label "bra&#263;"
  ]
  node [
    id 1479
    label "u&#380;y&#263;"
  ]
  node [
    id 1480
    label "zrobi&#263;"
  ]
  node [
    id 1481
    label "get"
  ]
  node [
    id 1482
    label "wyrucha&#263;"
  ]
  node [
    id 1483
    label "World_Health_Organization"
  ]
  node [
    id 1484
    label "wyciupcia&#263;"
  ]
  node [
    id 1485
    label "wygra&#263;"
  ]
  node [
    id 1486
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 1487
    label "withdraw"
  ]
  node [
    id 1488
    label "wzi&#281;cie"
  ]
  node [
    id 1489
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 1490
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 1491
    label "poczyta&#263;"
  ]
  node [
    id 1492
    label "obj&#261;&#263;"
  ]
  node [
    id 1493
    label "seize"
  ]
  node [
    id 1494
    label "aim"
  ]
  node [
    id 1495
    label "chwyci&#263;"
  ]
  node [
    id 1496
    label "przyj&#261;&#263;"
  ]
  node [
    id 1497
    label "pokona&#263;"
  ]
  node [
    id 1498
    label "arise"
  ]
  node [
    id 1499
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1500
    label "zacz&#261;&#263;"
  ]
  node [
    id 1501
    label "otrzyma&#263;"
  ]
  node [
    id 1502
    label "wej&#347;&#263;"
  ]
  node [
    id 1503
    label "poruszy&#263;"
  ]
  node [
    id 1504
    label "dosta&#263;"
  ]
  node [
    id 1505
    label "dywidenda"
  ]
  node [
    id 1506
    label "przebieg"
  ]
  node [
    id 1507
    label "zagrywka"
  ]
  node [
    id 1508
    label "udzia&#322;"
  ]
  node [
    id 1509
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1510
    label "commotion"
  ]
  node [
    id 1511
    label "jazda"
  ]
  node [
    id 1512
    label "stock"
  ]
  node [
    id 1513
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 1514
    label "w&#281;ze&#322;"
  ]
  node [
    id 1515
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1516
    label "instrument_strunowy"
  ]
  node [
    id 1517
    label "activity"
  ]
  node [
    id 1518
    label "bezproblemowy"
  ]
  node [
    id 1519
    label "tallness"
  ]
  node [
    id 1520
    label "altitude"
  ]
  node [
    id 1521
    label "rozmiar"
  ]
  node [
    id 1522
    label "degree"
  ]
  node [
    id 1523
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 1524
    label "odcinek"
  ]
  node [
    id 1525
    label "k&#261;t"
  ]
  node [
    id 1526
    label "wielko&#347;&#263;"
  ]
  node [
    id 1527
    label "brzmienie"
  ]
  node [
    id 1528
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1529
    label "gambit"
  ]
  node [
    id 1530
    label "rozgrywka"
  ]
  node [
    id 1531
    label "move"
  ]
  node [
    id 1532
    label "manewr"
  ]
  node [
    id 1533
    label "uderzenie"
  ]
  node [
    id 1534
    label "posuni&#281;cie"
  ]
  node [
    id 1535
    label "myk"
  ]
  node [
    id 1536
    label "gra_w_karty"
  ]
  node [
    id 1537
    label "mecz"
  ]
  node [
    id 1538
    label "travel"
  ]
  node [
    id 1539
    label "przebiec"
  ]
  node [
    id 1540
    label "charakter"
  ]
  node [
    id 1541
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1542
    label "motyw"
  ]
  node [
    id 1543
    label "przebiegni&#281;cie"
  ]
  node [
    id 1544
    label "fabu&#322;a"
  ]
  node [
    id 1545
    label "liczenie"
  ]
  node [
    id 1546
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 1547
    label "supremum"
  ]
  node [
    id 1548
    label "laparotomia"
  ]
  node [
    id 1549
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1550
    label "jednostka"
  ]
  node [
    id 1551
    label "matematyka"
  ]
  node [
    id 1552
    label "rzut"
  ]
  node [
    id 1553
    label "torakotomia"
  ]
  node [
    id 1554
    label "chirurg"
  ]
  node [
    id 1555
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 1556
    label "szew"
  ]
  node [
    id 1557
    label "mathematical_process"
  ]
  node [
    id 1558
    label "infimum"
  ]
  node [
    id 1559
    label "linia"
  ]
  node [
    id 1560
    label "procedura"
  ]
  node [
    id 1561
    label "room"
  ]
  node [
    id 1562
    label "ilo&#347;&#263;"
  ]
  node [
    id 1563
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1564
    label "sequence"
  ]
  node [
    id 1565
    label "cycle"
  ]
  node [
    id 1566
    label "kwota"
  ]
  node [
    id 1567
    label "zmienno&#347;&#263;"
  ]
  node [
    id 1568
    label "play"
  ]
  node [
    id 1569
    label "apparent_motion"
  ]
  node [
    id 1570
    label "contest"
  ]
  node [
    id 1571
    label "komplet"
  ]
  node [
    id 1572
    label "zabawa"
  ]
  node [
    id 1573
    label "rywalizacja"
  ]
  node [
    id 1574
    label "zbijany"
  ]
  node [
    id 1575
    label "post&#281;powanie"
  ]
  node [
    id 1576
    label "odg&#322;os"
  ]
  node [
    id 1577
    label "Pok&#233;mon"
  ]
  node [
    id 1578
    label "synteza"
  ]
  node [
    id 1579
    label "odtworzenie"
  ]
  node [
    id 1580
    label "rekwizyt_do_gry"
  ]
  node [
    id 1581
    label "formacja"
  ]
  node [
    id 1582
    label "szwadron"
  ]
  node [
    id 1583
    label "wykrzyknik"
  ]
  node [
    id 1584
    label "awantura"
  ]
  node [
    id 1585
    label "journey"
  ]
  node [
    id 1586
    label "sport"
  ]
  node [
    id 1587
    label "heca"
  ]
  node [
    id 1588
    label "ruch"
  ]
  node [
    id 1589
    label "cavalry"
  ]
  node [
    id 1590
    label "szale&#324;stwo"
  ]
  node [
    id 1591
    label "chor&#261;giew"
  ]
  node [
    id 1592
    label "doch&#243;d"
  ]
  node [
    id 1593
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 1594
    label "wi&#261;zanie"
  ]
  node [
    id 1595
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 1596
    label "bratnia_dusza"
  ]
  node [
    id 1597
    label "trasa"
  ]
  node [
    id 1598
    label "uczesanie"
  ]
  node [
    id 1599
    label "orbita"
  ]
  node [
    id 1600
    label "kryszta&#322;"
  ]
  node [
    id 1601
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1602
    label "zwi&#261;zanie"
  ]
  node [
    id 1603
    label "graf"
  ]
  node [
    id 1604
    label "hitch"
  ]
  node [
    id 1605
    label "struktura_anatomiczna"
  ]
  node [
    id 1606
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 1607
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1608
    label "o&#347;rodek"
  ]
  node [
    id 1609
    label "marriage"
  ]
  node [
    id 1610
    label "ekliptyka"
  ]
  node [
    id 1611
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 1612
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1613
    label "problem"
  ]
  node [
    id 1614
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1615
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1616
    label "fala_stoj&#261;ca"
  ]
  node [
    id 1617
    label "tying"
  ]
  node [
    id 1618
    label "argument"
  ]
  node [
    id 1619
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1620
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1621
    label "mila_morska"
  ]
  node [
    id 1622
    label "zgrubienie"
  ]
  node [
    id 1623
    label "pismo_klinowe"
  ]
  node [
    id 1624
    label "przeci&#281;cie"
  ]
  node [
    id 1625
    label "band"
  ]
  node [
    id 1626
    label "zwi&#261;zek"
  ]
  node [
    id 1627
    label "salariat"
  ]
  node [
    id 1628
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 1629
    label "delegowanie"
  ]
  node [
    id 1630
    label "pracu&#347;"
  ]
  node [
    id 1631
    label "r&#281;ka"
  ]
  node [
    id 1632
    label "delegowa&#263;"
  ]
  node [
    id 1633
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 1634
    label "warstwa"
  ]
  node [
    id 1635
    label "p&#322;aca"
  ]
  node [
    id 1636
    label "krzy&#380;"
  ]
  node [
    id 1637
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 1638
    label "handwriting"
  ]
  node [
    id 1639
    label "d&#322;o&#324;"
  ]
  node [
    id 1640
    label "gestykulowa&#263;"
  ]
  node [
    id 1641
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 1642
    label "palec"
  ]
  node [
    id 1643
    label "przedrami&#281;"
  ]
  node [
    id 1644
    label "hand"
  ]
  node [
    id 1645
    label "&#322;okie&#263;"
  ]
  node [
    id 1646
    label "hazena"
  ]
  node [
    id 1647
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 1648
    label "bramkarz"
  ]
  node [
    id 1649
    label "nadgarstek"
  ]
  node [
    id 1650
    label "graba"
  ]
  node [
    id 1651
    label "r&#261;czyna"
  ]
  node [
    id 1652
    label "k&#322;&#261;b"
  ]
  node [
    id 1653
    label "pi&#322;ka"
  ]
  node [
    id 1654
    label "chwyta&#263;"
  ]
  node [
    id 1655
    label "cmoknonsens"
  ]
  node [
    id 1656
    label "pomocnik"
  ]
  node [
    id 1657
    label "gestykulowanie"
  ]
  node [
    id 1658
    label "chwytanie"
  ]
  node [
    id 1659
    label "obietnica"
  ]
  node [
    id 1660
    label "kroki"
  ]
  node [
    id 1661
    label "hasta"
  ]
  node [
    id 1662
    label "wykroczenie"
  ]
  node [
    id 1663
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 1664
    label "czerwona_kartka"
  ]
  node [
    id 1665
    label "paw"
  ]
  node [
    id 1666
    label "rami&#281;"
  ]
  node [
    id 1667
    label "zapaleniec"
  ]
  node [
    id 1668
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1669
    label "air"
  ]
  node [
    id 1670
    label "wys&#322;a&#263;"
  ]
  node [
    id 1671
    label "oddelegowa&#263;"
  ]
  node [
    id 1672
    label "oddelegowywa&#263;"
  ]
  node [
    id 1673
    label "wysy&#322;anie"
  ]
  node [
    id 1674
    label "wys&#322;anie"
  ]
  node [
    id 1675
    label "delegacy"
  ]
  node [
    id 1676
    label "oddelegowywanie"
  ]
  node [
    id 1677
    label "oddelegowanie"
  ]
  node [
    id 1678
    label "privatize"
  ]
  node [
    id 1679
    label "przebudowywa&#263;"
  ]
  node [
    id 1680
    label "reconstruct"
  ]
  node [
    id 1681
    label "reform"
  ]
  node [
    id 1682
    label "przeorganizowywa&#263;"
  ]
  node [
    id 1683
    label "zasoby_kopalin"
  ]
  node [
    id 1684
    label "z&#322;o&#380;e"
  ]
  node [
    id 1685
    label "informatyka"
  ]
  node [
    id 1686
    label "ropa_naftowa"
  ]
  node [
    id 1687
    label "paliwo"
  ]
  node [
    id 1688
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 1689
    label "driveway"
  ]
  node [
    id 1690
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 1691
    label "przer&#243;bka"
  ]
  node [
    id 1692
    label "odmienienie"
  ]
  node [
    id 1693
    label "oprogramowanie"
  ]
  node [
    id 1694
    label "sprawa"
  ]
  node [
    id 1695
    label "object"
  ]
  node [
    id 1696
    label "zaleta"
  ]
  node [
    id 1697
    label "dobro"
  ]
  node [
    id 1698
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 1699
    label "penis"
  ]
  node [
    id 1700
    label "wnikni&#281;cie"
  ]
  node [
    id 1701
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 1702
    label "spotkanie"
  ]
  node [
    id 1703
    label "poznanie"
  ]
  node [
    id 1704
    label "pojawienie_si&#281;"
  ]
  node [
    id 1705
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1706
    label "przenikni&#281;cie"
  ]
  node [
    id 1707
    label "wpuszczenie"
  ]
  node [
    id 1708
    label "zaatakowanie"
  ]
  node [
    id 1709
    label "trespass"
  ]
  node [
    id 1710
    label "dost&#281;p"
  ]
  node [
    id 1711
    label "doj&#347;cie"
  ]
  node [
    id 1712
    label "przekroczenie"
  ]
  node [
    id 1713
    label "otw&#243;r"
  ]
  node [
    id 1714
    label "vent"
  ]
  node [
    id 1715
    label "stimulation"
  ]
  node [
    id 1716
    label "dostanie_si&#281;"
  ]
  node [
    id 1717
    label "pocz&#261;tek"
  ]
  node [
    id 1718
    label "approach"
  ]
  node [
    id 1719
    label "release"
  ]
  node [
    id 1720
    label "wnij&#347;cie"
  ]
  node [
    id 1721
    label "bramka"
  ]
  node [
    id 1722
    label "wzniesienie_si&#281;"
  ]
  node [
    id 1723
    label "podw&#243;rze"
  ]
  node [
    id 1724
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1725
    label "dom"
  ]
  node [
    id 1726
    label "wch&#243;d"
  ]
  node [
    id 1727
    label "nast&#261;pienie"
  ]
  node [
    id 1728
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1729
    label "stanie_si&#281;"
  ]
  node [
    id 1730
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 1731
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1732
    label "urz&#261;dzenie"
  ]
  node [
    id 1733
    label "porobienie_si&#281;"
  ]
  node [
    id 1734
    label "naci&#347;ni&#281;cie"
  ]
  node [
    id 1735
    label "chodzenie"
  ]
  node [
    id 1736
    label "event"
  ]
  node [
    id 1737
    label "advent"
  ]
  node [
    id 1738
    label "uzyskanie"
  ]
  node [
    id 1739
    label "dochrapanie_si&#281;"
  ]
  node [
    id 1740
    label "skill"
  ]
  node [
    id 1741
    label "accomplishment"
  ]
  node [
    id 1742
    label "sukces"
  ]
  node [
    id 1743
    label "zaawansowanie"
  ]
  node [
    id 1744
    label "dotarcie"
  ]
  node [
    id 1745
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 1746
    label "doznanie"
  ]
  node [
    id 1747
    label "znajomy"
  ]
  node [
    id 1748
    label "powitanie"
  ]
  node [
    id 1749
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1750
    label "spowodowanie"
  ]
  node [
    id 1751
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 1752
    label "znalezienie"
  ]
  node [
    id 1753
    label "match"
  ]
  node [
    id 1754
    label "employment"
  ]
  node [
    id 1755
    label "po&#380;egnanie"
  ]
  node [
    id 1756
    label "gather"
  ]
  node [
    id 1757
    label "spotykanie"
  ]
  node [
    id 1758
    label "spotkanie_si&#281;"
  ]
  node [
    id 1759
    label "dochodzenie"
  ]
  node [
    id 1760
    label "znajomo&#347;ci"
  ]
  node [
    id 1761
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1762
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1763
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 1764
    label "powi&#261;zanie"
  ]
  node [
    id 1765
    label "entrance"
  ]
  node [
    id 1766
    label "affiliation"
  ]
  node [
    id 1767
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1768
    label "dor&#281;czenie"
  ]
  node [
    id 1769
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 1770
    label "bodziec"
  ]
  node [
    id 1771
    label "informacja"
  ]
  node [
    id 1772
    label "przesy&#322;ka"
  ]
  node [
    id 1773
    label "gotowy"
  ]
  node [
    id 1774
    label "avenue"
  ]
  node [
    id 1775
    label "postrzeganie"
  ]
  node [
    id 1776
    label "dojrza&#322;y"
  ]
  node [
    id 1777
    label "dojechanie"
  ]
  node [
    id 1778
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1779
    label "ingress"
  ]
  node [
    id 1780
    label "strzelenie"
  ]
  node [
    id 1781
    label "orzekni&#281;cie"
  ]
  node [
    id 1782
    label "orgazm"
  ]
  node [
    id 1783
    label "dolecenie"
  ]
  node [
    id 1784
    label "rozpowszechnienie"
  ]
  node [
    id 1785
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 1786
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 1787
    label "dop&#322;ata"
  ]
  node [
    id 1788
    label "konto"
  ]
  node [
    id 1789
    label "has&#322;o"
  ]
  node [
    id 1790
    label "pierworodztwo"
  ]
  node [
    id 1791
    label "faza"
  ]
  node [
    id 1792
    label "upgrade"
  ]
  node [
    id 1793
    label "nast&#281;pstwo"
  ]
  node [
    id 1794
    label "skrytykowanie"
  ]
  node [
    id 1795
    label "time"
  ]
  node [
    id 1796
    label "walka"
  ]
  node [
    id 1797
    label "oddzia&#322;anie"
  ]
  node [
    id 1798
    label "przebycie"
  ]
  node [
    id 1799
    label "upolowanie"
  ]
  node [
    id 1800
    label "wdarcie_si&#281;"
  ]
  node [
    id 1801
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 1802
    label "progress"
  ]
  node [
    id 1803
    label "spr&#243;bowanie"
  ]
  node [
    id 1804
    label "powiedzenie"
  ]
  node [
    id 1805
    label "rozegranie"
  ]
  node [
    id 1806
    label "mini&#281;cie"
  ]
  node [
    id 1807
    label "ograniczenie"
  ]
  node [
    id 1808
    label "przepuszczenie"
  ]
  node [
    id 1809
    label "transgression"
  ]
  node [
    id 1810
    label "transgresja"
  ]
  node [
    id 1811
    label "emergence"
  ]
  node [
    id 1812
    label "offense"
  ]
  node [
    id 1813
    label "kom&#243;rka"
  ]
  node [
    id 1814
    label "furnishing"
  ]
  node [
    id 1815
    label "zabezpieczenie"
  ]
  node [
    id 1816
    label "wyrz&#261;dzenie"
  ]
  node [
    id 1817
    label "zagospodarowanie"
  ]
  node [
    id 1818
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 1819
    label "ig&#322;a"
  ]
  node [
    id 1820
    label "wirnik"
  ]
  node [
    id 1821
    label "aparatura"
  ]
  node [
    id 1822
    label "system_energetyczny"
  ]
  node [
    id 1823
    label "impulsator"
  ]
  node [
    id 1824
    label "mechanizm"
  ]
  node [
    id 1825
    label "sprz&#281;t"
  ]
  node [
    id 1826
    label "blokowanie"
  ]
  node [
    id 1827
    label "set"
  ]
  node [
    id 1828
    label "zablokowanie"
  ]
  node [
    id 1829
    label "przygotowanie"
  ]
  node [
    id 1830
    label "komora"
  ]
  node [
    id 1831
    label "j&#281;zyk"
  ]
  node [
    id 1832
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 1833
    label "wsi&#261;kni&#281;cie"
  ]
  node [
    id 1834
    label "infiltration"
  ]
  node [
    id 1835
    label "penetration"
  ]
  node [
    id 1836
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 1837
    label "wybicie"
  ]
  node [
    id 1838
    label "wyd&#322;ubanie"
  ]
  node [
    id 1839
    label "przerwa"
  ]
  node [
    id 1840
    label "powybijanie"
  ]
  node [
    id 1841
    label "wybijanie"
  ]
  node [
    id 1842
    label "wiercenie"
  ]
  node [
    id 1843
    label "acquaintance"
  ]
  node [
    id 1844
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1845
    label "nauczenie_si&#281;"
  ]
  node [
    id 1846
    label "poczucie"
  ]
  node [
    id 1847
    label "knowing"
  ]
  node [
    id 1848
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1849
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1850
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1851
    label "inclusion"
  ]
  node [
    id 1852
    label "zrozumienie"
  ]
  node [
    id 1853
    label "designation"
  ]
  node [
    id 1854
    label "umo&#380;liwienie"
  ]
  node [
    id 1855
    label "sensing"
  ]
  node [
    id 1856
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 1857
    label "zapoznanie"
  ]
  node [
    id 1858
    label "forma"
  ]
  node [
    id 1859
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 1860
    label "insekt"
  ]
  node [
    id 1861
    label "puszczenie"
  ]
  node [
    id 1862
    label "entree"
  ]
  node [
    id 1863
    label "wprowadzenie"
  ]
  node [
    id 1864
    label "dmuchni&#281;cie"
  ]
  node [
    id 1865
    label "niesienie"
  ]
  node [
    id 1866
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 1867
    label "nakazanie"
  ]
  node [
    id 1868
    label "ruszenie"
  ]
  node [
    id 1869
    label "pokonanie"
  ]
  node [
    id 1870
    label "wywiezienie"
  ]
  node [
    id 1871
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 1872
    label "wymienienie_si&#281;"
  ]
  node [
    id 1873
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 1874
    label "uciekni&#281;cie"
  ]
  node [
    id 1875
    label "pobranie"
  ]
  node [
    id 1876
    label "poczytanie"
  ]
  node [
    id 1877
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 1878
    label "pozabieranie"
  ]
  node [
    id 1879
    label "u&#380;ycie"
  ]
  node [
    id 1880
    label "powodzenie"
  ]
  node [
    id 1881
    label "pickings"
  ]
  node [
    id 1882
    label "przyj&#281;cie"
  ]
  node [
    id 1883
    label "zniesienie"
  ]
  node [
    id 1884
    label "kupienie"
  ]
  node [
    id 1885
    label "bite"
  ]
  node [
    id 1886
    label "dostanie"
  ]
  node [
    id 1887
    label "wyruchanie"
  ]
  node [
    id 1888
    label "odziedziczenie"
  ]
  node [
    id 1889
    label "capture"
  ]
  node [
    id 1890
    label "otrzymanie"
  ]
  node [
    id 1891
    label "branie"
  ]
  node [
    id 1892
    label "wygranie"
  ]
  node [
    id 1893
    label "obj&#281;cie"
  ]
  node [
    id 1894
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1895
    label "udanie_si&#281;"
  ]
  node [
    id 1896
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 1897
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 1898
    label "ptaszek"
  ]
  node [
    id 1899
    label "element_anatomiczny"
  ]
  node [
    id 1900
    label "cia&#322;o"
  ]
  node [
    id 1901
    label "przyrodzenie"
  ]
  node [
    id 1902
    label "fiut"
  ]
  node [
    id 1903
    label "shaft"
  ]
  node [
    id 1904
    label "wchodzenie"
  ]
  node [
    id 1905
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1906
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1907
    label "nas&#261;czenie"
  ]
  node [
    id 1908
    label "strain"
  ]
  node [
    id 1909
    label "przedostanie_si&#281;"
  ]
  node [
    id 1910
    label "nasycenie_si&#281;"
  ]
  node [
    id 1911
    label "permeation"
  ]
  node [
    id 1912
    label "nasilenie_si&#281;"
  ]
  node [
    id 1913
    label "przemokni&#281;cie"
  ]
  node [
    id 1914
    label "przepojenie"
  ]
  node [
    id 1915
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1916
    label "obstawianie"
  ]
  node [
    id 1917
    label "trafienie"
  ]
  node [
    id 1918
    label "obstawienie"
  ]
  node [
    id 1919
    label "przeszkoda"
  ]
  node [
    id 1920
    label "zawiasy"
  ]
  node [
    id 1921
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1922
    label "s&#322;upek"
  ]
  node [
    id 1923
    label "boisko"
  ]
  node [
    id 1924
    label "siatka"
  ]
  node [
    id 1925
    label "obstawia&#263;"
  ]
  node [
    id 1926
    label "ogrodzenie"
  ]
  node [
    id 1927
    label "zamek"
  ]
  node [
    id 1928
    label "goal"
  ]
  node [
    id 1929
    label "poprzeczka"
  ]
  node [
    id 1930
    label "p&#322;ot"
  ]
  node [
    id 1931
    label "obstawi&#263;"
  ]
  node [
    id 1932
    label "brama"
  ]
  node [
    id 1933
    label "plac"
  ]
  node [
    id 1934
    label "przechowalnia"
  ]
  node [
    id 1935
    label "podjazd"
  ]
  node [
    id 1936
    label "ogr&#243;d"
  ]
  node [
    id 1937
    label "rodzina"
  ]
  node [
    id 1938
    label "substancja_mieszkaniowa"
  ]
  node [
    id 1939
    label "dom_rodzinny"
  ]
  node [
    id 1940
    label "stead"
  ]
  node [
    id 1941
    label "garderoba"
  ]
  node [
    id 1942
    label "wiecha"
  ]
  node [
    id 1943
    label "fratria"
  ]
  node [
    id 1944
    label "raj_utracony"
  ]
  node [
    id 1945
    label "umieranie"
  ]
  node [
    id 1946
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1947
    label "prze&#380;ywanie"
  ]
  node [
    id 1948
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1949
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1950
    label "po&#322;&#243;g"
  ]
  node [
    id 1951
    label "umarcie"
  ]
  node [
    id 1952
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1953
    label "subsistence"
  ]
  node [
    id 1954
    label "power"
  ]
  node [
    id 1955
    label "okres_noworodkowy"
  ]
  node [
    id 1956
    label "prze&#380;ycie"
  ]
  node [
    id 1957
    label "wiek_matuzalemowy"
  ]
  node [
    id 1958
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1959
    label "entity"
  ]
  node [
    id 1960
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1961
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1962
    label "do&#380;ywanie"
  ]
  node [
    id 1963
    label "dzieci&#324;stwo"
  ]
  node [
    id 1964
    label "andropauza"
  ]
  node [
    id 1965
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1966
    label "rozw&#243;j"
  ]
  node [
    id 1967
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1968
    label "czas"
  ]
  node [
    id 1969
    label "menopauza"
  ]
  node [
    id 1970
    label "&#347;mier&#263;"
  ]
  node [
    id 1971
    label "koleje_losu"
  ]
  node [
    id 1972
    label "bycie"
  ]
  node [
    id 1973
    label "zegar_biologiczny"
  ]
  node [
    id 1974
    label "szwung"
  ]
  node [
    id 1975
    label "przebywanie"
  ]
  node [
    id 1976
    label "warunki"
  ]
  node [
    id 1977
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1978
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1979
    label "&#380;ywy"
  ]
  node [
    id 1980
    label "life"
  ]
  node [
    id 1981
    label "staro&#347;&#263;"
  ]
  node [
    id 1982
    label "energy"
  ]
  node [
    id 1983
    label "trwanie"
  ]
  node [
    id 1984
    label "wra&#380;enie"
  ]
  node [
    id 1985
    label "przej&#347;cie"
  ]
  node [
    id 1986
    label "poradzenie_sobie"
  ]
  node [
    id 1987
    label "przetrwanie"
  ]
  node [
    id 1988
    label "survival"
  ]
  node [
    id 1989
    label "przechodzenie"
  ]
  node [
    id 1990
    label "wytrzymywanie"
  ]
  node [
    id 1991
    label "zaznawanie"
  ]
  node [
    id 1992
    label "obejrzenie"
  ]
  node [
    id 1993
    label "widzenie"
  ]
  node [
    id 1994
    label "urzeczywistnianie"
  ]
  node [
    id 1995
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1996
    label "produkowanie"
  ]
  node [
    id 1997
    label "przeszkodzenie"
  ]
  node [
    id 1998
    label "being"
  ]
  node [
    id 1999
    label "znikni&#281;cie"
  ]
  node [
    id 2000
    label "robienie"
  ]
  node [
    id 2001
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 2002
    label "przeszkadzanie"
  ]
  node [
    id 2003
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 2004
    label "wyprodukowanie"
  ]
  node [
    id 2005
    label "utrzymywanie"
  ]
  node [
    id 2006
    label "subsystencja"
  ]
  node [
    id 2007
    label "utrzyma&#263;"
  ]
  node [
    id 2008
    label "egzystencja"
  ]
  node [
    id 2009
    label "wy&#380;ywienie"
  ]
  node [
    id 2010
    label "ontologicznie"
  ]
  node [
    id 2011
    label "utrzymanie"
  ]
  node [
    id 2012
    label "potencja"
  ]
  node [
    id 2013
    label "utrzymywa&#263;"
  ]
  node [
    id 2014
    label "status"
  ]
  node [
    id 2015
    label "poprzedzanie"
  ]
  node [
    id 2016
    label "czasoprzestrze&#324;"
  ]
  node [
    id 2017
    label "laba"
  ]
  node [
    id 2018
    label "chronometria"
  ]
  node [
    id 2019
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 2020
    label "rachuba_czasu"
  ]
  node [
    id 2021
    label "przep&#322;ywanie"
  ]
  node [
    id 2022
    label "czasokres"
  ]
  node [
    id 2023
    label "odczyt"
  ]
  node [
    id 2024
    label "chwila"
  ]
  node [
    id 2025
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 2026
    label "dzieje"
  ]
  node [
    id 2027
    label "kategoria_gramatyczna"
  ]
  node [
    id 2028
    label "poprzedzenie"
  ]
  node [
    id 2029
    label "trawienie"
  ]
  node [
    id 2030
    label "pochodzi&#263;"
  ]
  node [
    id 2031
    label "period"
  ]
  node [
    id 2032
    label "okres_czasu"
  ]
  node [
    id 2033
    label "poprzedza&#263;"
  ]
  node [
    id 2034
    label "schy&#322;ek"
  ]
  node [
    id 2035
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 2036
    label "odwlekanie_si&#281;"
  ]
  node [
    id 2037
    label "zegar"
  ]
  node [
    id 2038
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 2039
    label "czwarty_wymiar"
  ]
  node [
    id 2040
    label "pochodzenie"
  ]
  node [
    id 2041
    label "koniugacja"
  ]
  node [
    id 2042
    label "Zeitgeist"
  ]
  node [
    id 2043
    label "trawi&#263;"
  ]
  node [
    id 2044
    label "pogoda"
  ]
  node [
    id 2045
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 2046
    label "poprzedzi&#263;"
  ]
  node [
    id 2047
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 2048
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 2049
    label "time_period"
  ]
  node [
    id 2050
    label "ocieranie_si&#281;"
  ]
  node [
    id 2051
    label "otoczenie_si&#281;"
  ]
  node [
    id 2052
    label "posiedzenie"
  ]
  node [
    id 2053
    label "otarcie_si&#281;"
  ]
  node [
    id 2054
    label "atakowanie"
  ]
  node [
    id 2055
    label "otaczanie_si&#281;"
  ]
  node [
    id 2056
    label "wyj&#347;cie"
  ]
  node [
    id 2057
    label "zmierzanie"
  ]
  node [
    id 2058
    label "residency"
  ]
  node [
    id 2059
    label "sojourn"
  ]
  node [
    id 2060
    label "wychodzenie"
  ]
  node [
    id 2061
    label "tkwienie"
  ]
  node [
    id 2062
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 2063
    label "absolutorium"
  ]
  node [
    id 2064
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 2065
    label "dzia&#322;anie"
  ]
  node [
    id 2066
    label "ton"
  ]
  node [
    id 2067
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 2068
    label "korkowanie"
  ]
  node [
    id 2069
    label "death"
  ]
  node [
    id 2070
    label "zabijanie"
  ]
  node [
    id 2071
    label "martwy"
  ]
  node [
    id 2072
    label "przestawanie"
  ]
  node [
    id 2073
    label "odumieranie"
  ]
  node [
    id 2074
    label "zdychanie"
  ]
  node [
    id 2075
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 2076
    label "zanikanie"
  ]
  node [
    id 2077
    label "ko&#324;czenie"
  ]
  node [
    id 2078
    label "nieuleczalnie_chory"
  ]
  node [
    id 2079
    label "ciekawy"
  ]
  node [
    id 2080
    label "szybki"
  ]
  node [
    id 2081
    label "&#380;ywotny"
  ]
  node [
    id 2082
    label "naturalny"
  ]
  node [
    id 2083
    label "&#380;ywo"
  ]
  node [
    id 2084
    label "o&#380;ywianie"
  ]
  node [
    id 2085
    label "silny"
  ]
  node [
    id 2086
    label "g&#322;&#281;boki"
  ]
  node [
    id 2087
    label "wyra&#378;ny"
  ]
  node [
    id 2088
    label "czynny"
  ]
  node [
    id 2089
    label "aktualny"
  ]
  node [
    id 2090
    label "zgrabny"
  ]
  node [
    id 2091
    label "prawdziwy"
  ]
  node [
    id 2092
    label "realistyczny"
  ]
  node [
    id 2093
    label "energiczny"
  ]
  node [
    id 2094
    label "odumarcie"
  ]
  node [
    id 2095
    label "przestanie"
  ]
  node [
    id 2096
    label "dysponowanie_si&#281;"
  ]
  node [
    id 2097
    label "pomarcie"
  ]
  node [
    id 2098
    label "die"
  ]
  node [
    id 2099
    label "sko&#324;czenie"
  ]
  node [
    id 2100
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 2101
    label "zdechni&#281;cie"
  ]
  node [
    id 2102
    label "zabicie"
  ]
  node [
    id 2103
    label "proces_biologiczny"
  ]
  node [
    id 2104
    label "z&#322;ote_czasy"
  ]
  node [
    id 2105
    label "process"
  ]
  node [
    id 2106
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 2107
    label "adolescence"
  ]
  node [
    id 2108
    label "wiek"
  ]
  node [
    id 2109
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 2110
    label "zielone_lata"
  ]
  node [
    id 2111
    label "rozwi&#261;zanie"
  ]
  node [
    id 2112
    label "zlec"
  ]
  node [
    id 2113
    label "zlegni&#281;cie"
  ]
  node [
    id 2114
    label "defenestracja"
  ]
  node [
    id 2115
    label "agonia"
  ]
  node [
    id 2116
    label "kres"
  ]
  node [
    id 2117
    label "mogi&#322;a"
  ]
  node [
    id 2118
    label "kres_&#380;ycia"
  ]
  node [
    id 2119
    label "upadek"
  ]
  node [
    id 2120
    label "szeol"
  ]
  node [
    id 2121
    label "pogrzebanie"
  ]
  node [
    id 2122
    label "istota_nadprzyrodzona"
  ]
  node [
    id 2123
    label "&#380;a&#322;oba"
  ]
  node [
    id 2124
    label "pogrzeb"
  ]
  node [
    id 2125
    label "majority"
  ]
  node [
    id 2126
    label "osiemnastoletni"
  ]
  node [
    id 2127
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 2128
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 2129
    label "age"
  ]
  node [
    id 2130
    label "kobieta"
  ]
  node [
    id 2131
    label "przekwitanie"
  ]
  node [
    id 2132
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 2133
    label "energia"
  ]
  node [
    id 2134
    label "zapa&#322;"
  ]
  node [
    id 2135
    label "ekskursja"
  ]
  node [
    id 2136
    label "bezsilnikowy"
  ]
  node [
    id 2137
    label "budowla"
  ]
  node [
    id 2138
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 2139
    label "podbieg"
  ]
  node [
    id 2140
    label "turystyka"
  ]
  node [
    id 2141
    label "nawierzchnia"
  ]
  node [
    id 2142
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 2143
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 2144
    label "rajza"
  ]
  node [
    id 2145
    label "korona_drogi"
  ]
  node [
    id 2146
    label "passage"
  ]
  node [
    id 2147
    label "wylot"
  ]
  node [
    id 2148
    label "ekwipunek"
  ]
  node [
    id 2149
    label "zbior&#243;wka"
  ]
  node [
    id 2150
    label "marszrutyzacja"
  ]
  node [
    id 2151
    label "wyb&#243;j"
  ]
  node [
    id 2152
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 2153
    label "drogowskaz"
  ]
  node [
    id 2154
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 2155
    label "pobocze"
  ]
  node [
    id 2156
    label "infrastruktura"
  ]
  node [
    id 2157
    label "obudowanie"
  ]
  node [
    id 2158
    label "obudowywa&#263;"
  ]
  node [
    id 2159
    label "zbudowa&#263;"
  ]
  node [
    id 2160
    label "obudowa&#263;"
  ]
  node [
    id 2161
    label "kolumnada"
  ]
  node [
    id 2162
    label "korpus"
  ]
  node [
    id 2163
    label "Sukiennice"
  ]
  node [
    id 2164
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 2165
    label "fundament"
  ]
  node [
    id 2166
    label "postanie"
  ]
  node [
    id 2167
    label "obudowywanie"
  ]
  node [
    id 2168
    label "zbudowanie"
  ]
  node [
    id 2169
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 2170
    label "stan_surowy"
  ]
  node [
    id 2171
    label "rzecz"
  ]
  node [
    id 2172
    label "ambitus"
  ]
  node [
    id 2173
    label "skala"
  ]
  node [
    id 2174
    label "poruszenie"
  ]
  node [
    id 2175
    label "movement"
  ]
  node [
    id 2176
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 2177
    label "zjawisko"
  ]
  node [
    id 2178
    label "kanciasty"
  ]
  node [
    id 2179
    label "commercial_enterprise"
  ]
  node [
    id 2180
    label "strumie&#324;"
  ]
  node [
    id 2181
    label "aktywno&#347;&#263;"
  ]
  node [
    id 2182
    label "kr&#243;tki"
  ]
  node [
    id 2183
    label "taktyka"
  ]
  node [
    id 2184
    label "apraksja"
  ]
  node [
    id 2185
    label "natural_process"
  ]
  node [
    id 2186
    label "d&#322;ugi"
  ]
  node [
    id 2187
    label "dyssypacja_energii"
  ]
  node [
    id 2188
    label "tumult"
  ]
  node [
    id 2189
    label "stopek"
  ]
  node [
    id 2190
    label "lokomocja"
  ]
  node [
    id 2191
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 2192
    label "komunikacja"
  ]
  node [
    id 2193
    label "drift"
  ]
  node [
    id 2194
    label "r&#281;kaw"
  ]
  node [
    id 2195
    label "kontusz"
  ]
  node [
    id 2196
    label "koniec"
  ]
  node [
    id 2197
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 2198
    label "pokrycie"
  ]
  node [
    id 2199
    label "fingerpost"
  ]
  node [
    id 2200
    label "tablica"
  ]
  node [
    id 2201
    label "przydro&#380;e"
  ]
  node [
    id 2202
    label "autostrada"
  ]
  node [
    id 2203
    label "bieg"
  ]
  node [
    id 2204
    label "podr&#243;&#380;"
  ]
  node [
    id 2205
    label "mieszanie_si&#281;"
  ]
  node [
    id 2206
    label "digress"
  ]
  node [
    id 2207
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 2208
    label "pozostawa&#263;"
  ]
  node [
    id 2209
    label "s&#261;dzi&#263;"
  ]
  node [
    id 2210
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 2211
    label "stray"
  ]
  node [
    id 2212
    label "kocher"
  ]
  node [
    id 2213
    label "wyposa&#380;enie"
  ]
  node [
    id 2214
    label "nie&#347;miertelnik"
  ]
  node [
    id 2215
    label "moderunek"
  ]
  node [
    id 2216
    label "dormitorium"
  ]
  node [
    id 2217
    label "sk&#322;adanka"
  ]
  node [
    id 2218
    label "wyprawa"
  ]
  node [
    id 2219
    label "polowanie"
  ]
  node [
    id 2220
    label "spis"
  ]
  node [
    id 2221
    label "fotografia"
  ]
  node [
    id 2222
    label "beznap&#281;dowy"
  ]
  node [
    id 2223
    label "ukochanie"
  ]
  node [
    id 2224
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 2225
    label "feblik"
  ]
  node [
    id 2226
    label "tendency"
  ]
  node [
    id 2227
    label "afekt"
  ]
  node [
    id 2228
    label "zakochanie"
  ]
  node [
    id 2229
    label "zajawka"
  ]
  node [
    id 2230
    label "love"
  ]
  node [
    id 2231
    label "serce"
  ]
  node [
    id 2232
    label "wi&#281;&#378;"
  ]
  node [
    id 2233
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 2234
    label "emocja"
  ]
  node [
    id 2235
    label "drogi"
  ]
  node [
    id 2236
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 2237
    label "kochanka"
  ]
  node [
    id 2238
    label "kultura_fizyczna"
  ]
  node [
    id 2239
    label "turyzm"
  ]
  node [
    id 2240
    label "zdarzony"
  ]
  node [
    id 2241
    label "odpowiednio"
  ]
  node [
    id 2242
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 2243
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 2244
    label "nale&#380;ny"
  ]
  node [
    id 2245
    label "nale&#380;yty"
  ]
  node [
    id 2246
    label "stosownie"
  ]
  node [
    id 2247
    label "odpowiadanie"
  ]
  node [
    id 2248
    label "specjalny"
  ]
  node [
    id 2249
    label "typowy"
  ]
  node [
    id 2250
    label "uprawniony"
  ]
  node [
    id 2251
    label "zasadniczy"
  ]
  node [
    id 2252
    label "taki"
  ]
  node [
    id 2253
    label "charakterystyczny"
  ]
  node [
    id 2254
    label "ten"
  ]
  node [
    id 2255
    label "dobry"
  ]
  node [
    id 2256
    label "powinny"
  ]
  node [
    id 2257
    label "nale&#380;nie"
  ]
  node [
    id 2258
    label "godny"
  ]
  node [
    id 2259
    label "przynale&#380;ny"
  ]
  node [
    id 2260
    label "zadowalaj&#261;cy"
  ]
  node [
    id 2261
    label "nale&#380;ycie"
  ]
  node [
    id 2262
    label "przystojny"
  ]
  node [
    id 2263
    label "stosowny"
  ]
  node [
    id 2264
    label "intencjonalny"
  ]
  node [
    id 2265
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 2266
    label "niedorozw&#243;j"
  ]
  node [
    id 2267
    label "szczeg&#243;lny"
  ]
  node [
    id 2268
    label "specjalnie"
  ]
  node [
    id 2269
    label "nieetatowy"
  ]
  node [
    id 2270
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 2271
    label "nienormalny"
  ]
  node [
    id 2272
    label "umy&#347;lnie"
  ]
  node [
    id 2273
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 2274
    label "reagowanie"
  ]
  node [
    id 2275
    label "dawanie"
  ]
  node [
    id 2276
    label "powodowanie"
  ]
  node [
    id 2277
    label "pokutowanie"
  ]
  node [
    id 2278
    label "pytanie"
  ]
  node [
    id 2279
    label "odpowiedzialny"
  ]
  node [
    id 2280
    label "winny"
  ]
  node [
    id 2281
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 2282
    label "picie_piwa"
  ]
  node [
    id 2283
    label "parry"
  ]
  node [
    id 2284
    label "fit"
  ]
  node [
    id 2285
    label "dzianie_si&#281;"
  ]
  node [
    id 2286
    label "rendition"
  ]
  node [
    id 2287
    label "ponoszenie"
  ]
  node [
    id 2288
    label "rozmawianie"
  ]
  node [
    id 2289
    label "charakterystycznie"
  ]
  node [
    id 2290
    label "dobrze"
  ]
  node [
    id 2291
    label "prawdziwie"
  ]
  node [
    id 2292
    label "czasowo"
  ]
  node [
    id 2293
    label "przepustka"
  ]
  node [
    id 2294
    label "pobyt"
  ]
  node [
    id 2295
    label "czasowy"
  ]
  node [
    id 2296
    label "zezwolenie"
  ]
  node [
    id 2297
    label "nieobecno&#347;&#263;"
  ]
  node [
    id 2298
    label "temporarily"
  ]
  node [
    id 2299
    label "okr&#281;gowy"
  ]
  node [
    id 2300
    label "wyspa"
  ]
  node [
    id 2301
    label "pozna&#263;"
  ]
  node [
    id 2302
    label "dziennik"
  ]
  node [
    id 2303
    label "u"
  ]
  node [
    id 2304
    label "rzeczpospolita"
  ]
  node [
    id 2305
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 2304
  ]
  edge [
    source 1
    target 2305
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 789
  ]
  edge [
    source 7
    target 790
  ]
  edge [
    source 7
    target 791
  ]
  edge [
    source 7
    target 792
  ]
  edge [
    source 7
    target 793
  ]
  edge [
    source 7
    target 794
  ]
  edge [
    source 7
    target 795
  ]
  edge [
    source 7
    target 796
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 798
  ]
  edge [
    source 7
    target 799
  ]
  edge [
    source 7
    target 800
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 802
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 804
  ]
  edge [
    source 7
    target 805
  ]
  edge [
    source 7
    target 806
  ]
  edge [
    source 7
    target 807
  ]
  edge [
    source 7
    target 808
  ]
  edge [
    source 7
    target 809
  ]
  edge [
    source 7
    target 810
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 7
    target 817
  ]
  edge [
    source 7
    target 818
  ]
  edge [
    source 7
    target 819
  ]
  edge [
    source 7
    target 820
  ]
  edge [
    source 7
    target 821
  ]
  edge [
    source 7
    target 822
  ]
  edge [
    source 7
    target 823
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 7
    target 839
  ]
  edge [
    source 7
    target 840
  ]
  edge [
    source 7
    target 841
  ]
  edge [
    source 7
    target 842
  ]
  edge [
    source 7
    target 843
  ]
  edge [
    source 7
    target 844
  ]
  edge [
    source 7
    target 845
  ]
  edge [
    source 7
    target 846
  ]
  edge [
    source 7
    target 847
  ]
  edge [
    source 7
    target 848
  ]
  edge [
    source 7
    target 849
  ]
  edge [
    source 7
    target 850
  ]
  edge [
    source 7
    target 851
  ]
  edge [
    source 7
    target 852
  ]
  edge [
    source 7
    target 853
  ]
  edge [
    source 7
    target 854
  ]
  edge [
    source 7
    target 855
  ]
  edge [
    source 7
    target 856
  ]
  edge [
    source 7
    target 857
  ]
  edge [
    source 7
    target 858
  ]
  edge [
    source 7
    target 859
  ]
  edge [
    source 7
    target 860
  ]
  edge [
    source 7
    target 861
  ]
  edge [
    source 7
    target 862
  ]
  edge [
    source 7
    target 863
  ]
  edge [
    source 7
    target 864
  ]
  edge [
    source 7
    target 865
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 867
  ]
  edge [
    source 7
    target 868
  ]
  edge [
    source 7
    target 869
  ]
  edge [
    source 7
    target 870
  ]
  edge [
    source 7
    target 871
  ]
  edge [
    source 7
    target 872
  ]
  edge [
    source 7
    target 873
  ]
  edge [
    source 7
    target 874
  ]
  edge [
    source 7
    target 875
  ]
  edge [
    source 7
    target 876
  ]
  edge [
    source 7
    target 877
  ]
  edge [
    source 7
    target 878
  ]
  edge [
    source 7
    target 879
  ]
  edge [
    source 7
    target 880
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 882
  ]
  edge [
    source 7
    target 883
  ]
  edge [
    source 7
    target 884
  ]
  edge [
    source 7
    target 885
  ]
  edge [
    source 7
    target 886
  ]
  edge [
    source 7
    target 887
  ]
  edge [
    source 7
    target 888
  ]
  edge [
    source 7
    target 889
  ]
  edge [
    source 7
    target 890
  ]
  edge [
    source 7
    target 891
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 893
  ]
  edge [
    source 7
    target 894
  ]
  edge [
    source 7
    target 895
  ]
  edge [
    source 7
    target 896
  ]
  edge [
    source 7
    target 897
  ]
  edge [
    source 7
    target 898
  ]
  edge [
    source 7
    target 899
  ]
  edge [
    source 7
    target 900
  ]
  edge [
    source 7
    target 901
  ]
  edge [
    source 7
    target 902
  ]
  edge [
    source 7
    target 903
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 7
    target 907
  ]
  edge [
    source 7
    target 908
  ]
  edge [
    source 7
    target 909
  ]
  edge [
    source 7
    target 910
  ]
  edge [
    source 7
    target 911
  ]
  edge [
    source 7
    target 912
  ]
  edge [
    source 7
    target 913
  ]
  edge [
    source 7
    target 914
  ]
  edge [
    source 7
    target 915
  ]
  edge [
    source 7
    target 916
  ]
  edge [
    source 7
    target 917
  ]
  edge [
    source 7
    target 918
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 921
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 923
  ]
  edge [
    source 7
    target 924
  ]
  edge [
    source 7
    target 925
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 7
    target 928
  ]
  edge [
    source 7
    target 929
  ]
  edge [
    source 7
    target 930
  ]
  edge [
    source 7
    target 931
  ]
  edge [
    source 7
    target 932
  ]
  edge [
    source 7
    target 933
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 7
    target 937
  ]
  edge [
    source 7
    target 938
  ]
  edge [
    source 7
    target 939
  ]
  edge [
    source 7
    target 940
  ]
  edge [
    source 7
    target 941
  ]
  edge [
    source 7
    target 942
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 944
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 974
  ]
  edge [
    source 7
    target 975
  ]
  edge [
    source 7
    target 976
  ]
  edge [
    source 7
    target 977
  ]
  edge [
    source 7
    target 978
  ]
  edge [
    source 7
    target 979
  ]
  edge [
    source 7
    target 980
  ]
  edge [
    source 7
    target 981
  ]
  edge [
    source 7
    target 982
  ]
  edge [
    source 7
    target 983
  ]
  edge [
    source 7
    target 984
  ]
  edge [
    source 7
    target 985
  ]
  edge [
    source 7
    target 986
  ]
  edge [
    source 7
    target 987
  ]
  edge [
    source 7
    target 988
  ]
  edge [
    source 7
    target 989
  ]
  edge [
    source 7
    target 990
  ]
  edge [
    source 7
    target 991
  ]
  edge [
    source 7
    target 992
  ]
  edge [
    source 7
    target 993
  ]
  edge [
    source 7
    target 994
  ]
  edge [
    source 7
    target 995
  ]
  edge [
    source 7
    target 996
  ]
  edge [
    source 7
    target 997
  ]
  edge [
    source 7
    target 998
  ]
  edge [
    source 7
    target 999
  ]
  edge [
    source 7
    target 1000
  ]
  edge [
    source 7
    target 1001
  ]
  edge [
    source 7
    target 1002
  ]
  edge [
    source 7
    target 1003
  ]
  edge [
    source 7
    target 1004
  ]
  edge [
    source 7
    target 1005
  ]
  edge [
    source 7
    target 1006
  ]
  edge [
    source 7
    target 1007
  ]
  edge [
    source 7
    target 1008
  ]
  edge [
    source 7
    target 1009
  ]
  edge [
    source 7
    target 1010
  ]
  edge [
    source 7
    target 1011
  ]
  edge [
    source 7
    target 1012
  ]
  edge [
    source 7
    target 1013
  ]
  edge [
    source 7
    target 1014
  ]
  edge [
    source 7
    target 1015
  ]
  edge [
    source 7
    target 1016
  ]
  edge [
    source 7
    target 1017
  ]
  edge [
    source 7
    target 1018
  ]
  edge [
    source 7
    target 1019
  ]
  edge [
    source 7
    target 1020
  ]
  edge [
    source 7
    target 1021
  ]
  edge [
    source 7
    target 1022
  ]
  edge [
    source 7
    target 1023
  ]
  edge [
    source 7
    target 1024
  ]
  edge [
    source 7
    target 1025
  ]
  edge [
    source 7
    target 1026
  ]
  edge [
    source 7
    target 1027
  ]
  edge [
    source 7
    target 1028
  ]
  edge [
    source 7
    target 1029
  ]
  edge [
    source 7
    target 1030
  ]
  edge [
    source 7
    target 1031
  ]
  edge [
    source 7
    target 1032
  ]
  edge [
    source 7
    target 1033
  ]
  edge [
    source 7
    target 1034
  ]
  edge [
    source 7
    target 1035
  ]
  edge [
    source 7
    target 1036
  ]
  edge [
    source 7
    target 1037
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 7
    target 1039
  ]
  edge [
    source 7
    target 1040
  ]
  edge [
    source 7
    target 1041
  ]
  edge [
    source 7
    target 1042
  ]
  edge [
    source 7
    target 1043
  ]
  edge [
    source 7
    target 1044
  ]
  edge [
    source 7
    target 1045
  ]
  edge [
    source 7
    target 1046
  ]
  edge [
    source 7
    target 1047
  ]
  edge [
    source 7
    target 1048
  ]
  edge [
    source 7
    target 1049
  ]
  edge [
    source 7
    target 1050
  ]
  edge [
    source 7
    target 1051
  ]
  edge [
    source 7
    target 1052
  ]
  edge [
    source 7
    target 1053
  ]
  edge [
    source 7
    target 1054
  ]
  edge [
    source 7
    target 1055
  ]
  edge [
    source 7
    target 1056
  ]
  edge [
    source 7
    target 1057
  ]
  edge [
    source 7
    target 1058
  ]
  edge [
    source 7
    target 1059
  ]
  edge [
    source 7
    target 1060
  ]
  edge [
    source 7
    target 1061
  ]
  edge [
    source 7
    target 1062
  ]
  edge [
    source 7
    target 1063
  ]
  edge [
    source 7
    target 1064
  ]
  edge [
    source 7
    target 1065
  ]
  edge [
    source 7
    target 1066
  ]
  edge [
    source 7
    target 1067
  ]
  edge [
    source 7
    target 1068
  ]
  edge [
    source 7
    target 1069
  ]
  edge [
    source 7
    target 1070
  ]
  edge [
    source 7
    target 1071
  ]
  edge [
    source 7
    target 1072
  ]
  edge [
    source 7
    target 1073
  ]
  edge [
    source 7
    target 1074
  ]
  edge [
    source 7
    target 1075
  ]
  edge [
    source 7
    target 1076
  ]
  edge [
    source 7
    target 1077
  ]
  edge [
    source 7
    target 1078
  ]
  edge [
    source 7
    target 1079
  ]
  edge [
    source 7
    target 1080
  ]
  edge [
    source 7
    target 1081
  ]
  edge [
    source 7
    target 1082
  ]
  edge [
    source 7
    target 1083
  ]
  edge [
    source 7
    target 1084
  ]
  edge [
    source 7
    target 1085
  ]
  edge [
    source 7
    target 1086
  ]
  edge [
    source 7
    target 1087
  ]
  edge [
    source 7
    target 1088
  ]
  edge [
    source 7
    target 1089
  ]
  edge [
    source 7
    target 1090
  ]
  edge [
    source 7
    target 1091
  ]
  edge [
    source 7
    target 1092
  ]
  edge [
    source 7
    target 1093
  ]
  edge [
    source 7
    target 1094
  ]
  edge [
    source 7
    target 1095
  ]
  edge [
    source 7
    target 1096
  ]
  edge [
    source 7
    target 1097
  ]
  edge [
    source 7
    target 1098
  ]
  edge [
    source 7
    target 1099
  ]
  edge [
    source 7
    target 1100
  ]
  edge [
    source 7
    target 1101
  ]
  edge [
    source 7
    target 1102
  ]
  edge [
    source 7
    target 1103
  ]
  edge [
    source 7
    target 1104
  ]
  edge [
    source 7
    target 1105
  ]
  edge [
    source 7
    target 1106
  ]
  edge [
    source 7
    target 1107
  ]
  edge [
    source 7
    target 1108
  ]
  edge [
    source 7
    target 1109
  ]
  edge [
    source 7
    target 1110
  ]
  edge [
    source 7
    target 1111
  ]
  edge [
    source 7
    target 1112
  ]
  edge [
    source 7
    target 1113
  ]
  edge [
    source 7
    target 1114
  ]
  edge [
    source 7
    target 1115
  ]
  edge [
    source 7
    target 1116
  ]
  edge [
    source 7
    target 1117
  ]
  edge [
    source 7
    target 1118
  ]
  edge [
    source 7
    target 1119
  ]
  edge [
    source 7
    target 1120
  ]
  edge [
    source 7
    target 1121
  ]
  edge [
    source 7
    target 1122
  ]
  edge [
    source 7
    target 1123
  ]
  edge [
    source 7
    target 1124
  ]
  edge [
    source 7
    target 1125
  ]
  edge [
    source 7
    target 1126
  ]
  edge [
    source 7
    target 1127
  ]
  edge [
    source 7
    target 1128
  ]
  edge [
    source 7
    target 1129
  ]
  edge [
    source 7
    target 1130
  ]
  edge [
    source 7
    target 1131
  ]
  edge [
    source 7
    target 1132
  ]
  edge [
    source 7
    target 1133
  ]
  edge [
    source 7
    target 1134
  ]
  edge [
    source 7
    target 1135
  ]
  edge [
    source 7
    target 1136
  ]
  edge [
    source 7
    target 1137
  ]
  edge [
    source 7
    target 1138
  ]
  edge [
    source 7
    target 1139
  ]
  edge [
    source 7
    target 1140
  ]
  edge [
    source 7
    target 1141
  ]
  edge [
    source 7
    target 1142
  ]
  edge [
    source 7
    target 1143
  ]
  edge [
    source 7
    target 1144
  ]
  edge [
    source 7
    target 1145
  ]
  edge [
    source 7
    target 1146
  ]
  edge [
    source 7
    target 1147
  ]
  edge [
    source 7
    target 1148
  ]
  edge [
    source 7
    target 1149
  ]
  edge [
    source 7
    target 1150
  ]
  edge [
    source 7
    target 1151
  ]
  edge [
    source 7
    target 1152
  ]
  edge [
    source 7
    target 1153
  ]
  edge [
    source 7
    target 1154
  ]
  edge [
    source 7
    target 1155
  ]
  edge [
    source 7
    target 1156
  ]
  edge [
    source 7
    target 1157
  ]
  edge [
    source 7
    target 1158
  ]
  edge [
    source 7
    target 1159
  ]
  edge [
    source 7
    target 1160
  ]
  edge [
    source 7
    target 1161
  ]
  edge [
    source 7
    target 1162
  ]
  edge [
    source 7
    target 1163
  ]
  edge [
    source 7
    target 1164
  ]
  edge [
    source 7
    target 1165
  ]
  edge [
    source 7
    target 1166
  ]
  edge [
    source 7
    target 1167
  ]
  edge [
    source 7
    target 1168
  ]
  edge [
    source 7
    target 1169
  ]
  edge [
    source 7
    target 1170
  ]
  edge [
    source 7
    target 1171
  ]
  edge [
    source 7
    target 1172
  ]
  edge [
    source 7
    target 1173
  ]
  edge [
    source 7
    target 1174
  ]
  edge [
    source 7
    target 1175
  ]
  edge [
    source 7
    target 1176
  ]
  edge [
    source 7
    target 1177
  ]
  edge [
    source 7
    target 1178
  ]
  edge [
    source 7
    target 1179
  ]
  edge [
    source 7
    target 1180
  ]
  edge [
    source 7
    target 1181
  ]
  edge [
    source 7
    target 1182
  ]
  edge [
    source 7
    target 1183
  ]
  edge [
    source 7
    target 1184
  ]
  edge [
    source 7
    target 1185
  ]
  edge [
    source 7
    target 1186
  ]
  edge [
    source 7
    target 1187
  ]
  edge [
    source 7
    target 1188
  ]
  edge [
    source 7
    target 1189
  ]
  edge [
    source 7
    target 1190
  ]
  edge [
    source 7
    target 1191
  ]
  edge [
    source 7
    target 1192
  ]
  edge [
    source 7
    target 1193
  ]
  edge [
    source 7
    target 1194
  ]
  edge [
    source 7
    target 1195
  ]
  edge [
    source 7
    target 1196
  ]
  edge [
    source 7
    target 1197
  ]
  edge [
    source 7
    target 1198
  ]
  edge [
    source 7
    target 1199
  ]
  edge [
    source 7
    target 1200
  ]
  edge [
    source 7
    target 1201
  ]
  edge [
    source 7
    target 1202
  ]
  edge [
    source 7
    target 1203
  ]
  edge [
    source 7
    target 1204
  ]
  edge [
    source 7
    target 1205
  ]
  edge [
    source 7
    target 1206
  ]
  edge [
    source 7
    target 1207
  ]
  edge [
    source 7
    target 1208
  ]
  edge [
    source 7
    target 1209
  ]
  edge [
    source 7
    target 1210
  ]
  edge [
    source 7
    target 1211
  ]
  edge [
    source 7
    target 1212
  ]
  edge [
    source 7
    target 1213
  ]
  edge [
    source 7
    target 1214
  ]
  edge [
    source 7
    target 1215
  ]
  edge [
    source 7
    target 1216
  ]
  edge [
    source 7
    target 1217
  ]
  edge [
    source 7
    target 1218
  ]
  edge [
    source 7
    target 1219
  ]
  edge [
    source 7
    target 1220
  ]
  edge [
    source 7
    target 1221
  ]
  edge [
    source 7
    target 1222
  ]
  edge [
    source 7
    target 1223
  ]
  edge [
    source 7
    target 1224
  ]
  edge [
    source 7
    target 1225
  ]
  edge [
    source 7
    target 1226
  ]
  edge [
    source 7
    target 1227
  ]
  edge [
    source 7
    target 1228
  ]
  edge [
    source 7
    target 1229
  ]
  edge [
    source 7
    target 1230
  ]
  edge [
    source 7
    target 1231
  ]
  edge [
    source 7
    target 1232
  ]
  edge [
    source 7
    target 1233
  ]
  edge [
    source 7
    target 1234
  ]
  edge [
    source 7
    target 1235
  ]
  edge [
    source 7
    target 1236
  ]
  edge [
    source 7
    target 1237
  ]
  edge [
    source 7
    target 1238
  ]
  edge [
    source 7
    target 1239
  ]
  edge [
    source 7
    target 1240
  ]
  edge [
    source 7
    target 1241
  ]
  edge [
    source 7
    target 1242
  ]
  edge [
    source 7
    target 1243
  ]
  edge [
    source 7
    target 1244
  ]
  edge [
    source 7
    target 1245
  ]
  edge [
    source 7
    target 1246
  ]
  edge [
    source 7
    target 1247
  ]
  edge [
    source 7
    target 1248
  ]
  edge [
    source 7
    target 1249
  ]
  edge [
    source 7
    target 1250
  ]
  edge [
    source 7
    target 1251
  ]
  edge [
    source 7
    target 1252
  ]
  edge [
    source 7
    target 1253
  ]
  edge [
    source 7
    target 1254
  ]
  edge [
    source 7
    target 1255
  ]
  edge [
    source 7
    target 1256
  ]
  edge [
    source 7
    target 1257
  ]
  edge [
    source 7
    target 1258
  ]
  edge [
    source 7
    target 1259
  ]
  edge [
    source 7
    target 1260
  ]
  edge [
    source 7
    target 1261
  ]
  edge [
    source 7
    target 1262
  ]
  edge [
    source 7
    target 1263
  ]
  edge [
    source 7
    target 1264
  ]
  edge [
    source 7
    target 1265
  ]
  edge [
    source 7
    target 1266
  ]
  edge [
    source 7
    target 1267
  ]
  edge [
    source 7
    target 1268
  ]
  edge [
    source 7
    target 1269
  ]
  edge [
    source 7
    target 1270
  ]
  edge [
    source 7
    target 1271
  ]
  edge [
    source 7
    target 1272
  ]
  edge [
    source 7
    target 1273
  ]
  edge [
    source 7
    target 1274
  ]
  edge [
    source 7
    target 1275
  ]
  edge [
    source 7
    target 1276
  ]
  edge [
    source 7
    target 1277
  ]
  edge [
    source 7
    target 1278
  ]
  edge [
    source 7
    target 1279
  ]
  edge [
    source 7
    target 1280
  ]
  edge [
    source 7
    target 1281
  ]
  edge [
    source 7
    target 1282
  ]
  edge [
    source 7
    target 1283
  ]
  edge [
    source 7
    target 1284
  ]
  edge [
    source 7
    target 1285
  ]
  edge [
    source 7
    target 1286
  ]
  edge [
    source 7
    target 1287
  ]
  edge [
    source 7
    target 1288
  ]
  edge [
    source 7
    target 1289
  ]
  edge [
    source 7
    target 1290
  ]
  edge [
    source 7
    target 1291
  ]
  edge [
    source 7
    target 1292
  ]
  edge [
    source 7
    target 1293
  ]
  edge [
    source 7
    target 1294
  ]
  edge [
    source 7
    target 1295
  ]
  edge [
    source 7
    target 1296
  ]
  edge [
    source 7
    target 1297
  ]
  edge [
    source 7
    target 1298
  ]
  edge [
    source 7
    target 1299
  ]
  edge [
    source 7
    target 1300
  ]
  edge [
    source 7
    target 1301
  ]
  edge [
    source 7
    target 1302
  ]
  edge [
    source 7
    target 1303
  ]
  edge [
    source 7
    target 1304
  ]
  edge [
    source 7
    target 1305
  ]
  edge [
    source 7
    target 1306
  ]
  edge [
    source 7
    target 1307
  ]
  edge [
    source 7
    target 1308
  ]
  edge [
    source 7
    target 1309
  ]
  edge [
    source 7
    target 1310
  ]
  edge [
    source 7
    target 1311
  ]
  edge [
    source 7
    target 1312
  ]
  edge [
    source 7
    target 1313
  ]
  edge [
    source 7
    target 1314
  ]
  edge [
    source 7
    target 1315
  ]
  edge [
    source 7
    target 1316
  ]
  edge [
    source 7
    target 1317
  ]
  edge [
    source 7
    target 1318
  ]
  edge [
    source 7
    target 1319
  ]
  edge [
    source 7
    target 1320
  ]
  edge [
    source 7
    target 1321
  ]
  edge [
    source 7
    target 1322
  ]
  edge [
    source 7
    target 1323
  ]
  edge [
    source 7
    target 1324
  ]
  edge [
    source 7
    target 1325
  ]
  edge [
    source 7
    target 1326
  ]
  edge [
    source 7
    target 1327
  ]
  edge [
    source 7
    target 1328
  ]
  edge [
    source 7
    target 1329
  ]
  edge [
    source 7
    target 1330
  ]
  edge [
    source 7
    target 1331
  ]
  edge [
    source 7
    target 1332
  ]
  edge [
    source 7
    target 1333
  ]
  edge [
    source 7
    target 1334
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1335
  ]
  edge [
    source 8
    target 1336
  ]
  edge [
    source 8
    target 1337
  ]
  edge [
    source 8
    target 1338
  ]
  edge [
    source 8
    target 1339
  ]
  edge [
    source 8
    target 1340
  ]
  edge [
    source 8
    target 1341
  ]
  edge [
    source 8
    target 1342
  ]
  edge [
    source 8
    target 1343
  ]
  edge [
    source 8
    target 1344
  ]
  edge [
    source 8
    target 1345
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 1346
  ]
  edge [
    source 8
    target 1347
  ]
  edge [
    source 8
    target 1348
  ]
  edge [
    source 8
    target 1349
  ]
  edge [
    source 8
    target 1350
  ]
  edge [
    source 8
    target 1351
  ]
  edge [
    source 8
    target 1352
  ]
  edge [
    source 8
    target 1353
  ]
  edge [
    source 8
    target 1354
  ]
  edge [
    source 8
    target 1355
  ]
  edge [
    source 8
    target 1356
  ]
  edge [
    source 8
    target 1357
  ]
  edge [
    source 8
    target 1358
  ]
  edge [
    source 8
    target 1359
  ]
  edge [
    source 8
    target 1360
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 1361
  ]
  edge [
    source 8
    target 1362
  ]
  edge [
    source 8
    target 1363
  ]
  edge [
    source 8
    target 1364
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 1365
  ]
  edge [
    source 8
    target 1366
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 1367
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 1368
  ]
  edge [
    source 8
    target 1369
  ]
  edge [
    source 8
    target 1370
  ]
  edge [
    source 8
    target 1371
  ]
  edge [
    source 8
    target 1372
  ]
  edge [
    source 8
    target 1373
  ]
  edge [
    source 8
    target 1374
  ]
  edge [
    source 8
    target 1375
  ]
  edge [
    source 8
    target 1376
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1377
  ]
  edge [
    source 11
    target 1378
  ]
  edge [
    source 11
    target 1379
  ]
  edge [
    source 11
    target 33
  ]
  edge [
    source 11
    target 1380
  ]
  edge [
    source 11
    target 1381
  ]
  edge [
    source 11
    target 1382
  ]
  edge [
    source 11
    target 1383
  ]
  edge [
    source 11
    target 1384
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 1385
  ]
  edge [
    source 11
    target 1386
  ]
  edge [
    source 11
    target 1387
  ]
  edge [
    source 11
    target 1388
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 1389
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 1390
  ]
  edge [
    source 11
    target 1391
  ]
  edge [
    source 11
    target 1392
  ]
  edge [
    source 11
    target 1393
  ]
  edge [
    source 11
    target 1394
  ]
  edge [
    source 11
    target 1395
  ]
  edge [
    source 11
    target 1396
  ]
  edge [
    source 11
    target 1397
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 1398
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 1109
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 1399
  ]
  edge [
    source 12
    target 1400
  ]
  edge [
    source 12
    target 1401
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 1402
  ]
  edge [
    source 12
    target 1403
  ]
  edge [
    source 12
    target 1404
  ]
  edge [
    source 12
    target 1405
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 1406
  ]
  edge [
    source 12
    target 1407
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 1408
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1409
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1410
  ]
  edge [
    source 14
    target 1411
  ]
  edge [
    source 14
    target 1412
  ]
  edge [
    source 14
    target 1343
  ]
  edge [
    source 14
    target 1413
  ]
  edge [
    source 14
    target 1414
  ]
  edge [
    source 14
    target 1415
  ]
  edge [
    source 14
    target 1335
  ]
  edge [
    source 14
    target 1336
  ]
  edge [
    source 14
    target 1337
  ]
  edge [
    source 14
    target 1338
  ]
  edge [
    source 14
    target 1339
  ]
  edge [
    source 14
    target 1340
  ]
  edge [
    source 14
    target 1341
  ]
  edge [
    source 14
    target 1342
  ]
  edge [
    source 14
    target 1416
  ]
  edge [
    source 14
    target 1417
  ]
  edge [
    source 14
    target 1418
  ]
  edge [
    source 14
    target 1419
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 1360
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 1355
  ]
  edge [
    source 14
    target 1361
  ]
  edge [
    source 14
    target 1420
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 1421
  ]
  edge [
    source 14
    target 1422
  ]
  edge [
    source 14
    target 1423
  ]
  edge [
    source 14
    target 1424
  ]
  edge [
    source 14
    target 1425
  ]
  edge [
    source 14
    target 1426
  ]
  edge [
    source 14
    target 1427
  ]
  edge [
    source 14
    target 1428
  ]
  edge [
    source 14
    target 1429
  ]
  edge [
    source 14
    target 1430
  ]
  edge [
    source 14
    target 1431
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1432
  ]
  edge [
    source 15
    target 1433
  ]
  edge [
    source 15
    target 1434
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 1435
  ]
  edge [
    source 15
    target 1436
  ]
  edge [
    source 15
    target 1437
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1438
  ]
  edge [
    source 16
    target 1439
  ]
  edge [
    source 16
    target 1440
  ]
  edge [
    source 16
    target 1441
  ]
  edge [
    source 16
    target 1442
  ]
  edge [
    source 16
    target 1443
  ]
  edge [
    source 16
    target 1444
  ]
  edge [
    source 16
    target 1445
  ]
  edge [
    source 16
    target 1446
  ]
  edge [
    source 16
    target 1447
  ]
  edge [
    source 16
    target 1448
  ]
  edge [
    source 16
    target 1449
  ]
  edge [
    source 16
    target 1450
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 17
    target 1390
  ]
  edge [
    source 17
    target 1391
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 1392
  ]
  edge [
    source 17
    target 1393
  ]
  edge [
    source 17
    target 1394
  ]
  edge [
    source 17
    target 1395
  ]
  edge [
    source 17
    target 1396
  ]
  edge [
    source 17
    target 1397
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 1451
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 1452
  ]
  edge [
    source 17
    target 1453
  ]
  edge [
    source 17
    target 1454
  ]
  edge [
    source 17
    target 1377
  ]
  edge [
    source 17
    target 1378
  ]
  edge [
    source 17
    target 1379
  ]
  edge [
    source 17
    target 1380
  ]
  edge [
    source 17
    target 1381
  ]
  edge [
    source 17
    target 1382
  ]
  edge [
    source 17
    target 1383
  ]
  edge [
    source 17
    target 1384
  ]
  edge [
    source 17
    target 1385
  ]
  edge [
    source 17
    target 1386
  ]
  edge [
    source 17
    target 1387
  ]
  edge [
    source 17
    target 1388
  ]
  edge [
    source 17
    target 311
  ]
  edge [
    source 17
    target 312
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 313
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 314
  ]
  edge [
    source 17
    target 315
  ]
  edge [
    source 17
    target 32
  ]
  edge [
    source 17
    target 316
  ]
  edge [
    source 17
    target 317
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 319
  ]
  edge [
    source 17
    target 320
  ]
  edge [
    source 17
    target 321
  ]
  edge [
    source 17
    target 322
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 323
  ]
  edge [
    source 17
    target 324
  ]
  edge [
    source 17
    target 325
  ]
  edge [
    source 17
    target 326
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 327
  ]
  edge [
    source 17
    target 328
  ]
  edge [
    source 17
    target 329
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 330
  ]
  edge [
    source 17
    target 331
  ]
  edge [
    source 17
    target 332
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 333
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 334
  ]
  edge [
    source 17
    target 335
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 336
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 1455
  ]
  edge [
    source 18
    target 1456
  ]
  edge [
    source 18
    target 1457
  ]
  edge [
    source 18
    target 1458
  ]
  edge [
    source 18
    target 1459
  ]
  edge [
    source 18
    target 1460
  ]
  edge [
    source 18
    target 1461
  ]
  edge [
    source 18
    target 1462
  ]
  edge [
    source 18
    target 334
  ]
  edge [
    source 18
    target 1463
  ]
  edge [
    source 18
    target 1464
  ]
  edge [
    source 18
    target 1465
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1466
  ]
  edge [
    source 20
    target 1467
  ]
  edge [
    source 20
    target 1468
  ]
  edge [
    source 20
    target 1469
  ]
  edge [
    source 20
    target 1470
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 1471
  ]
  edge [
    source 20
    target 1472
  ]
  edge [
    source 20
    target 1473
  ]
  edge [
    source 20
    target 1474
  ]
  edge [
    source 20
    target 1475
  ]
  edge [
    source 20
    target 1476
  ]
  edge [
    source 20
    target 1477
  ]
  edge [
    source 20
    target 1478
  ]
  edge [
    source 20
    target 1479
  ]
  edge [
    source 20
    target 1480
  ]
  edge [
    source 20
    target 1481
  ]
  edge [
    source 20
    target 1482
  ]
  edge [
    source 20
    target 1483
  ]
  edge [
    source 20
    target 1484
  ]
  edge [
    source 20
    target 1485
  ]
  edge [
    source 20
    target 1486
  ]
  edge [
    source 20
    target 1487
  ]
  edge [
    source 20
    target 1488
  ]
  edge [
    source 20
    target 1489
  ]
  edge [
    source 20
    target 1490
  ]
  edge [
    source 20
    target 1491
  ]
  edge [
    source 20
    target 1492
  ]
  edge [
    source 20
    target 1493
  ]
  edge [
    source 20
    target 1494
  ]
  edge [
    source 20
    target 1495
  ]
  edge [
    source 20
    target 1496
  ]
  edge [
    source 20
    target 1497
  ]
  edge [
    source 20
    target 1498
  ]
  edge [
    source 20
    target 1499
  ]
  edge [
    source 20
    target 1500
  ]
  edge [
    source 20
    target 1501
  ]
  edge [
    source 20
    target 1502
  ]
  edge [
    source 20
    target 1503
  ]
  edge [
    source 20
    target 1504
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1505
  ]
  edge [
    source 21
    target 1506
  ]
  edge [
    source 21
    target 304
  ]
  edge [
    source 21
    target 1507
  ]
  edge [
    source 21
    target 71
  ]
  edge [
    source 21
    target 1508
  ]
  edge [
    source 21
    target 1509
  ]
  edge [
    source 21
    target 1510
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 730
  ]
  edge [
    source 21
    target 1511
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 1512
  ]
  edge [
    source 21
    target 1513
  ]
  edge [
    source 21
    target 1514
  ]
  edge [
    source 21
    target 1515
  ]
  edge [
    source 21
    target 78
  ]
  edge [
    source 21
    target 1516
  ]
  edge [
    source 21
    target 1517
  ]
  edge [
    source 21
    target 1518
  ]
  edge [
    source 21
    target 86
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 1519
  ]
  edge [
    source 21
    target 1520
  ]
  edge [
    source 21
    target 1521
  ]
  edge [
    source 21
    target 1522
  ]
  edge [
    source 21
    target 1523
  ]
  edge [
    source 21
    target 1524
  ]
  edge [
    source 21
    target 1525
  ]
  edge [
    source 21
    target 1526
  ]
  edge [
    source 21
    target 1527
  ]
  edge [
    source 21
    target 100
  ]
  edge [
    source 21
    target 1528
  ]
  edge [
    source 21
    target 1529
  ]
  edge [
    source 21
    target 1530
  ]
  edge [
    source 21
    target 1531
  ]
  edge [
    source 21
    target 1532
  ]
  edge [
    source 21
    target 1533
  ]
  edge [
    source 21
    target 1534
  ]
  edge [
    source 21
    target 1535
  ]
  edge [
    source 21
    target 1536
  ]
  edge [
    source 21
    target 1537
  ]
  edge [
    source 21
    target 1538
  ]
  edge [
    source 21
    target 1539
  ]
  edge [
    source 21
    target 1540
  ]
  edge [
    source 21
    target 1541
  ]
  edge [
    source 21
    target 1542
  ]
  edge [
    source 21
    target 1543
  ]
  edge [
    source 21
    target 1544
  ]
  edge [
    source 21
    target 260
  ]
  edge [
    source 21
    target 1545
  ]
  edge [
    source 21
    target 1546
  ]
  edge [
    source 21
    target 1547
  ]
  edge [
    source 21
    target 1548
  ]
  edge [
    source 21
    target 1549
  ]
  edge [
    source 21
    target 1550
  ]
  edge [
    source 21
    target 1551
  ]
  edge [
    source 21
    target 1552
  ]
  edge [
    source 21
    target 140
  ]
  edge [
    source 21
    target 255
  ]
  edge [
    source 21
    target 1553
  ]
  edge [
    source 21
    target 1554
  ]
  edge [
    source 21
    target 1555
  ]
  edge [
    source 21
    target 308
  ]
  edge [
    source 21
    target 1556
  ]
  edge [
    source 21
    target 1557
  ]
  edge [
    source 21
    target 1558
  ]
  edge [
    source 21
    target 1559
  ]
  edge [
    source 21
    target 1560
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 305
  ]
  edge [
    source 21
    target 1561
  ]
  edge [
    source 21
    target 1562
  ]
  edge [
    source 21
    target 1563
  ]
  edge [
    source 21
    target 1564
  ]
  edge [
    source 21
    target 116
  ]
  edge [
    source 21
    target 1565
  ]
  edge [
    source 21
    target 1368
  ]
  edge [
    source 21
    target 1566
  ]
  edge [
    source 21
    target 676
  ]
  edge [
    source 21
    target 1567
  ]
  edge [
    source 21
    target 1568
  ]
  edge [
    source 21
    target 1569
  ]
  edge [
    source 21
    target 1570
  ]
  edge [
    source 21
    target 1571
  ]
  edge [
    source 21
    target 1572
  ]
  edge [
    source 21
    target 1573
  ]
  edge [
    source 21
    target 1574
  ]
  edge [
    source 21
    target 1575
  ]
  edge [
    source 21
    target 732
  ]
  edge [
    source 21
    target 1576
  ]
  edge [
    source 21
    target 1577
  ]
  edge [
    source 21
    target 1578
  ]
  edge [
    source 21
    target 1579
  ]
  edge [
    source 21
    target 1580
  ]
  edge [
    source 21
    target 1581
  ]
  edge [
    source 21
    target 1582
  ]
  edge [
    source 21
    target 1583
  ]
  edge [
    source 21
    target 1584
  ]
  edge [
    source 21
    target 1585
  ]
  edge [
    source 21
    target 1586
  ]
  edge [
    source 21
    target 1587
  ]
  edge [
    source 21
    target 1588
  ]
  edge [
    source 21
    target 1589
  ]
  edge [
    source 21
    target 1590
  ]
  edge [
    source 21
    target 1591
  ]
  edge [
    source 21
    target 1592
  ]
  edge [
    source 21
    target 1593
  ]
  edge [
    source 21
    target 1594
  ]
  edge [
    source 21
    target 1595
  ]
  edge [
    source 21
    target 60
  ]
  edge [
    source 21
    target 1596
  ]
  edge [
    source 21
    target 1597
  ]
  edge [
    source 21
    target 1598
  ]
  edge [
    source 21
    target 1599
  ]
  edge [
    source 21
    target 1600
  ]
  edge [
    source 21
    target 1601
  ]
  edge [
    source 21
    target 1602
  ]
  edge [
    source 21
    target 1603
  ]
  edge [
    source 21
    target 1604
  ]
  edge [
    source 21
    target 1605
  ]
  edge [
    source 21
    target 1606
  ]
  edge [
    source 21
    target 1607
  ]
  edge [
    source 21
    target 1608
  ]
  edge [
    source 21
    target 1609
  ]
  edge [
    source 21
    target 686
  ]
  edge [
    source 21
    target 1610
  ]
  edge [
    source 21
    target 1611
  ]
  edge [
    source 21
    target 1612
  ]
  edge [
    source 21
    target 1613
  ]
  edge [
    source 21
    target 1614
  ]
  edge [
    source 21
    target 1615
  ]
  edge [
    source 21
    target 1616
  ]
  edge [
    source 21
    target 1617
  ]
  edge [
    source 21
    target 1618
  ]
  edge [
    source 21
    target 1619
  ]
  edge [
    source 21
    target 1620
  ]
  edge [
    source 21
    target 1621
  ]
  edge [
    source 21
    target 748
  ]
  edge [
    source 21
    target 1400
  ]
  edge [
    source 21
    target 1622
  ]
  edge [
    source 21
    target 1623
  ]
  edge [
    source 21
    target 1624
  ]
  edge [
    source 21
    target 1625
  ]
  edge [
    source 21
    target 1626
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1627
  ]
  edge [
    source 23
    target 1628
  ]
  edge [
    source 23
    target 368
  ]
  edge [
    source 23
    target 1629
  ]
  edge [
    source 23
    target 1630
  ]
  edge [
    source 23
    target 1631
  ]
  edge [
    source 23
    target 1632
  ]
  edge [
    source 23
    target 1633
  ]
  edge [
    source 23
    target 1634
  ]
  edge [
    source 23
    target 1635
  ]
  edge [
    source 23
    target 620
  ]
  edge [
    source 23
    target 621
  ]
  edge [
    source 23
    target 622
  ]
  edge [
    source 23
    target 623
  ]
  edge [
    source 23
    target 624
  ]
  edge [
    source 23
    target 625
  ]
  edge [
    source 23
    target 626
  ]
  edge [
    source 23
    target 627
  ]
  edge [
    source 23
    target 628
  ]
  edge [
    source 23
    target 629
  ]
  edge [
    source 23
    target 105
  ]
  edge [
    source 23
    target 630
  ]
  edge [
    source 23
    target 631
  ]
  edge [
    source 23
    target 632
  ]
  edge [
    source 23
    target 633
  ]
  edge [
    source 23
    target 634
  ]
  edge [
    source 23
    target 635
  ]
  edge [
    source 23
    target 636
  ]
  edge [
    source 23
    target 637
  ]
  edge [
    source 23
    target 638
  ]
  edge [
    source 23
    target 639
  ]
  edge [
    source 23
    target 640
  ]
  edge [
    source 23
    target 641
  ]
  edge [
    source 23
    target 642
  ]
  edge [
    source 23
    target 643
  ]
  edge [
    source 23
    target 1636
  ]
  edge [
    source 23
    target 1637
  ]
  edge [
    source 23
    target 1638
  ]
  edge [
    source 23
    target 1639
  ]
  edge [
    source 23
    target 1640
  ]
  edge [
    source 23
    target 1641
  ]
  edge [
    source 23
    target 1642
  ]
  edge [
    source 23
    target 1643
  ]
  edge [
    source 23
    target 107
  ]
  edge [
    source 23
    target 1644
  ]
  edge [
    source 23
    target 1645
  ]
  edge [
    source 23
    target 1646
  ]
  edge [
    source 23
    target 1647
  ]
  edge [
    source 23
    target 1648
  ]
  edge [
    source 23
    target 1649
  ]
  edge [
    source 23
    target 1650
  ]
  edge [
    source 23
    target 1651
  ]
  edge [
    source 23
    target 1652
  ]
  edge [
    source 23
    target 1653
  ]
  edge [
    source 23
    target 1654
  ]
  edge [
    source 23
    target 1655
  ]
  edge [
    source 23
    target 1656
  ]
  edge [
    source 23
    target 1657
  ]
  edge [
    source 23
    target 1658
  ]
  edge [
    source 23
    target 1659
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 1507
  ]
  edge [
    source 23
    target 1660
  ]
  edge [
    source 23
    target 1661
  ]
  edge [
    source 23
    target 1662
  ]
  edge [
    source 23
    target 1663
  ]
  edge [
    source 23
    target 1664
  ]
  edge [
    source 23
    target 1665
  ]
  edge [
    source 23
    target 1666
  ]
  edge [
    source 23
    target 1667
  ]
  edge [
    source 23
    target 1668
  ]
  edge [
    source 23
    target 1669
  ]
  edge [
    source 23
    target 1670
  ]
  edge [
    source 23
    target 1671
  ]
  edge [
    source 23
    target 1672
  ]
  edge [
    source 23
    target 1673
  ]
  edge [
    source 23
    target 1674
  ]
  edge [
    source 23
    target 1675
  ]
  edge [
    source 23
    target 1676
  ]
  edge [
    source 23
    target 1677
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1678
  ]
  edge [
    source 24
    target 1679
  ]
  edge [
    source 24
    target 1680
  ]
  edge [
    source 24
    target 1416
  ]
  edge [
    source 24
    target 1681
  ]
  edge [
    source 24
    target 1682
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 389
  ]
  edge [
    source 25
    target 377
  ]
  edge [
    source 25
    target 366
  ]
  edge [
    source 25
    target 383
  ]
  edge [
    source 25
    target 379
  ]
  edge [
    source 25
    target 367
  ]
  edge [
    source 25
    target 378
  ]
  edge [
    source 25
    target 384
  ]
  edge [
    source 25
    target 369
  ]
  edge [
    source 25
    target 376
  ]
  edge [
    source 25
    target 370
  ]
  edge [
    source 25
    target 371
  ]
  edge [
    source 25
    target 380
  ]
  edge [
    source 25
    target 385
  ]
  edge [
    source 25
    target 373
  ]
  edge [
    source 25
    target 375
  ]
  edge [
    source 25
    target 386
  ]
  edge [
    source 25
    target 387
  ]
  edge [
    source 25
    target 381
  ]
  edge [
    source 25
    target 1683
  ]
  edge [
    source 25
    target 102
  ]
  edge [
    source 25
    target 1684
  ]
  edge [
    source 25
    target 1685
  ]
  edge [
    source 25
    target 1686
  ]
  edge [
    source 25
    target 1687
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 25
    target 338
  ]
  edge [
    source 25
    target 1688
  ]
  edge [
    source 25
    target 1689
  ]
  edge [
    source 25
    target 1690
  ]
  edge [
    source 25
    target 676
  ]
  edge [
    source 25
    target 1691
  ]
  edge [
    source 25
    target 1692
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 1693
  ]
  edge [
    source 25
    target 1416
  ]
  edge [
    source 25
    target 1694
  ]
  edge [
    source 25
    target 1695
  ]
  edge [
    source 25
    target 1696
  ]
  edge [
    source 25
    target 1697
  ]
  edge [
    source 25
    target 1698
  ]
  edge [
    source 25
    target 1699
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1700
  ]
  edge [
    source 27
    target 1701
  ]
  edge [
    source 27
    target 1702
  ]
  edge [
    source 27
    target 1703
  ]
  edge [
    source 27
    target 1704
  ]
  edge [
    source 27
    target 1705
  ]
  edge [
    source 27
    target 1706
  ]
  edge [
    source 27
    target 1707
  ]
  edge [
    source 27
    target 1708
  ]
  edge [
    source 27
    target 1709
  ]
  edge [
    source 27
    target 1710
  ]
  edge [
    source 27
    target 1711
  ]
  edge [
    source 27
    target 1712
  ]
  edge [
    source 27
    target 1713
  ]
  edge [
    source 27
    target 1488
  ]
  edge [
    source 27
    target 1714
  ]
  edge [
    source 27
    target 1715
  ]
  edge [
    source 27
    target 1716
  ]
  edge [
    source 27
    target 1717
  ]
  edge [
    source 27
    target 1718
  ]
  edge [
    source 27
    target 1719
  ]
  edge [
    source 27
    target 1720
  ]
  edge [
    source 27
    target 1721
  ]
  edge [
    source 27
    target 1722
  ]
  edge [
    source 27
    target 1723
  ]
  edge [
    source 27
    target 1724
  ]
  edge [
    source 27
    target 1725
  ]
  edge [
    source 27
    target 1726
  ]
  edge [
    source 27
    target 1727
  ]
  edge [
    source 27
    target 1728
  ]
  edge [
    source 27
    target 340
  ]
  edge [
    source 27
    target 615
  ]
  edge [
    source 27
    target 1729
  ]
  edge [
    source 27
    target 1730
  ]
  edge [
    source 27
    target 1731
  ]
  edge [
    source 27
    target 1732
  ]
  edge [
    source 27
    target 353
  ]
  edge [
    source 27
    target 354
  ]
  edge [
    source 27
    target 355
  ]
  edge [
    source 27
    target 356
  ]
  edge [
    source 27
    target 78
  ]
  edge [
    source 27
    target 357
  ]
  edge [
    source 27
    target 358
  ]
  edge [
    source 27
    target 344
  ]
  edge [
    source 27
    target 1733
  ]
  edge [
    source 27
    target 1734
  ]
  edge [
    source 27
    target 1735
  ]
  edge [
    source 27
    target 1736
  ]
  edge [
    source 27
    target 1737
  ]
  edge [
    source 27
    target 1738
  ]
  edge [
    source 27
    target 1739
  ]
  edge [
    source 27
    target 1740
  ]
  edge [
    source 27
    target 1741
  ]
  edge [
    source 27
    target 1742
  ]
  edge [
    source 27
    target 1743
  ]
  edge [
    source 27
    target 1744
  ]
  edge [
    source 27
    target 87
  ]
  edge [
    source 27
    target 1745
  ]
  edge [
    source 27
    target 1746
  ]
  edge [
    source 27
    target 101
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 71
  ]
  edge [
    source 27
    target 1747
  ]
  edge [
    source 27
    target 1748
  ]
  edge [
    source 27
    target 1749
  ]
  edge [
    source 27
    target 1750
  ]
  edge [
    source 27
    target 1751
  ]
  edge [
    source 27
    target 1752
  ]
  edge [
    source 27
    target 1753
  ]
  edge [
    source 27
    target 1754
  ]
  edge [
    source 27
    target 1755
  ]
  edge [
    source 27
    target 1756
  ]
  edge [
    source 27
    target 1757
  ]
  edge [
    source 27
    target 1758
  ]
  edge [
    source 27
    target 1759
  ]
  edge [
    source 27
    target 1760
  ]
  edge [
    source 27
    target 1761
  ]
  edge [
    source 27
    target 1762
  ]
  edge [
    source 27
    target 1763
  ]
  edge [
    source 27
    target 314
  ]
  edge [
    source 27
    target 1764
  ]
  edge [
    source 27
    target 1765
  ]
  edge [
    source 27
    target 1766
  ]
  edge [
    source 27
    target 1767
  ]
  edge [
    source 27
    target 1768
  ]
  edge [
    source 27
    target 1769
  ]
  edge [
    source 27
    target 1770
  ]
  edge [
    source 27
    target 1771
  ]
  edge [
    source 27
    target 1772
  ]
  edge [
    source 27
    target 1773
  ]
  edge [
    source 27
    target 1774
  ]
  edge [
    source 27
    target 1775
  ]
  edge [
    source 27
    target 657
  ]
  edge [
    source 27
    target 1776
  ]
  edge [
    source 27
    target 1777
  ]
  edge [
    source 27
    target 1778
  ]
  edge [
    source 27
    target 1779
  ]
  edge [
    source 27
    target 1780
  ]
  edge [
    source 27
    target 1781
  ]
  edge [
    source 27
    target 1782
  ]
  edge [
    source 27
    target 1783
  ]
  edge [
    source 27
    target 1784
  ]
  edge [
    source 27
    target 1785
  ]
  edge [
    source 27
    target 1786
  ]
  edge [
    source 27
    target 1787
  ]
  edge [
    source 27
    target 1685
  ]
  edge [
    source 27
    target 304
  ]
  edge [
    source 27
    target 1788
  ]
  edge [
    source 27
    target 1294
  ]
  edge [
    source 27
    target 1789
  ]
  edge [
    source 27
    target 1790
  ]
  edge [
    source 27
    target 1791
  ]
  edge [
    source 27
    target 1792
  ]
  edge [
    source 27
    target 1793
  ]
  edge [
    source 27
    target 748
  ]
  edge [
    source 27
    target 1794
  ]
  edge [
    source 27
    target 1795
  ]
  edge [
    source 27
    target 1796
  ]
  edge [
    source 27
    target 1532
  ]
  edge [
    source 27
    target 1797
  ]
  edge [
    source 27
    target 1798
  ]
  edge [
    source 27
    target 1799
  ]
  edge [
    source 27
    target 1800
  ]
  edge [
    source 27
    target 1801
  ]
  edge [
    source 27
    target 1586
  ]
  edge [
    source 27
    target 1802
  ]
  edge [
    source 27
    target 1803
  ]
  edge [
    source 27
    target 1804
  ]
  edge [
    source 27
    target 1805
  ]
  edge [
    source 27
    target 1806
  ]
  edge [
    source 27
    target 1807
  ]
  edge [
    source 27
    target 1808
  ]
  edge [
    source 27
    target 1809
  ]
  edge [
    source 27
    target 1810
  ]
  edge [
    source 27
    target 1811
  ]
  edge [
    source 27
    target 1812
  ]
  edge [
    source 27
    target 243
  ]
  edge [
    source 27
    target 1813
  ]
  edge [
    source 27
    target 1814
  ]
  edge [
    source 27
    target 1815
  ]
  edge [
    source 27
    target 1816
  ]
  edge [
    source 27
    target 1817
  ]
  edge [
    source 27
    target 1818
  ]
  edge [
    source 27
    target 1819
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 1820
  ]
  edge [
    source 27
    target 1821
  ]
  edge [
    source 27
    target 1822
  ]
  edge [
    source 27
    target 1823
  ]
  edge [
    source 27
    target 1824
  ]
  edge [
    source 27
    target 1825
  ]
  edge [
    source 27
    target 1826
  ]
  edge [
    source 27
    target 1827
  ]
  edge [
    source 27
    target 1828
  ]
  edge [
    source 27
    target 1829
  ]
  edge [
    source 27
    target 1830
  ]
  edge [
    source 27
    target 1831
  ]
  edge [
    source 27
    target 1832
  ]
  edge [
    source 27
    target 1833
  ]
  edge [
    source 27
    target 1834
  ]
  edge [
    source 27
    target 1835
  ]
  edge [
    source 27
    target 1332
  ]
  edge [
    source 27
    target 1836
  ]
  edge [
    source 27
    target 1837
  ]
  edge [
    source 27
    target 1838
  ]
  edge [
    source 27
    target 1839
  ]
  edge [
    source 27
    target 1840
  ]
  edge [
    source 27
    target 1841
  ]
  edge [
    source 27
    target 1842
  ]
  edge [
    source 27
    target 1843
  ]
  edge [
    source 27
    target 1844
  ]
  edge [
    source 27
    target 1845
  ]
  edge [
    source 27
    target 1846
  ]
  edge [
    source 27
    target 1847
  ]
  edge [
    source 27
    target 1848
  ]
  edge [
    source 27
    target 1849
  ]
  edge [
    source 27
    target 1850
  ]
  edge [
    source 27
    target 1851
  ]
  edge [
    source 27
    target 1852
  ]
  edge [
    source 27
    target 1853
  ]
  edge [
    source 27
    target 1854
  ]
  edge [
    source 27
    target 1855
  ]
  edge [
    source 27
    target 1856
  ]
  edge [
    source 27
    target 1857
  ]
  edge [
    source 27
    target 1858
  ]
  edge [
    source 27
    target 1859
  ]
  edge [
    source 27
    target 1860
  ]
  edge [
    source 27
    target 1861
  ]
  edge [
    source 27
    target 1862
  ]
  edge [
    source 27
    target 1863
  ]
  edge [
    source 27
    target 1864
  ]
  edge [
    source 27
    target 1865
  ]
  edge [
    source 27
    target 1866
  ]
  edge [
    source 27
    target 1867
  ]
  edge [
    source 27
    target 1868
  ]
  edge [
    source 27
    target 1869
  ]
  edge [
    source 27
    target 142
  ]
  edge [
    source 27
    target 1870
  ]
  edge [
    source 27
    target 1871
  ]
  edge [
    source 27
    target 1872
  ]
  edge [
    source 27
    target 1873
  ]
  edge [
    source 27
    target 1874
  ]
  edge [
    source 27
    target 1875
  ]
  edge [
    source 27
    target 1876
  ]
  edge [
    source 27
    target 1877
  ]
  edge [
    source 27
    target 1878
  ]
  edge [
    source 27
    target 1879
  ]
  edge [
    source 27
    target 1880
  ]
  edge [
    source 27
    target 1881
  ]
  edge [
    source 27
    target 1882
  ]
  edge [
    source 27
    target 1883
  ]
  edge [
    source 27
    target 1884
  ]
  edge [
    source 27
    target 1885
  ]
  edge [
    source 27
    target 1886
  ]
  edge [
    source 27
    target 1887
  ]
  edge [
    source 27
    target 1888
  ]
  edge [
    source 27
    target 1889
  ]
  edge [
    source 27
    target 1890
  ]
  edge [
    source 27
    target 1891
  ]
  edge [
    source 27
    target 1892
  ]
  edge [
    source 27
    target 1467
  ]
  edge [
    source 27
    target 1893
  ]
  edge [
    source 27
    target 1894
  ]
  edge [
    source 27
    target 1895
  ]
  edge [
    source 27
    target 1896
  ]
  edge [
    source 27
    target 334
  ]
  edge [
    source 27
    target 1897
  ]
  edge [
    source 27
    target 368
  ]
  edge [
    source 27
    target 108
  ]
  edge [
    source 27
    target 1898
  ]
  edge [
    source 27
    target 414
  ]
  edge [
    source 27
    target 1899
  ]
  edge [
    source 27
    target 1900
  ]
  edge [
    source 27
    target 1901
  ]
  edge [
    source 27
    target 1902
  ]
  edge [
    source 27
    target 1903
  ]
  edge [
    source 27
    target 1904
  ]
  edge [
    source 27
    target 583
  ]
  edge [
    source 27
    target 393
  ]
  edge [
    source 27
    target 1905
  ]
  edge [
    source 27
    target 1906
  ]
  edge [
    source 27
    target 1907
  ]
  edge [
    source 27
    target 1908
  ]
  edge [
    source 27
    target 1909
  ]
  edge [
    source 27
    target 1417
  ]
  edge [
    source 27
    target 1910
  ]
  edge [
    source 27
    target 1911
  ]
  edge [
    source 27
    target 1912
  ]
  edge [
    source 27
    target 1913
  ]
  edge [
    source 27
    target 1914
  ]
  edge [
    source 27
    target 1915
  ]
  edge [
    source 27
    target 1916
  ]
  edge [
    source 27
    target 1917
  ]
  edge [
    source 27
    target 1918
  ]
  edge [
    source 27
    target 1919
  ]
  edge [
    source 27
    target 1920
  ]
  edge [
    source 27
    target 1921
  ]
  edge [
    source 27
    target 1922
  ]
  edge [
    source 27
    target 1923
  ]
  edge [
    source 27
    target 1924
  ]
  edge [
    source 27
    target 1925
  ]
  edge [
    source 27
    target 1926
  ]
  edge [
    source 27
    target 1927
  ]
  edge [
    source 27
    target 1928
  ]
  edge [
    source 27
    target 1929
  ]
  edge [
    source 27
    target 1930
  ]
  edge [
    source 27
    target 1931
  ]
  edge [
    source 27
    target 1932
  ]
  edge [
    source 27
    target 1933
  ]
  edge [
    source 27
    target 1934
  ]
  edge [
    source 27
    target 1935
  ]
  edge [
    source 27
    target 1936
  ]
  edge [
    source 27
    target 1330
  ]
  edge [
    source 27
    target 1937
  ]
  edge [
    source 27
    target 1938
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 27
    target 390
  ]
  edge [
    source 27
    target 1939
  ]
  edge [
    source 27
    target 1275
  ]
  edge [
    source 27
    target 470
  ]
  edge [
    source 27
    target 60
  ]
  edge [
    source 27
    target 1940
  ]
  edge [
    source 27
    target 1941
  ]
  edge [
    source 27
    target 1942
  ]
  edge [
    source 27
    target 1943
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1944
  ]
  edge [
    source 28
    target 1945
  ]
  edge [
    source 28
    target 1946
  ]
  edge [
    source 28
    target 1947
  ]
  edge [
    source 28
    target 1948
  ]
  edge [
    source 28
    target 1949
  ]
  edge [
    source 28
    target 1950
  ]
  edge [
    source 28
    target 1951
  ]
  edge [
    source 28
    target 1952
  ]
  edge [
    source 28
    target 1953
  ]
  edge [
    source 28
    target 1954
  ]
  edge [
    source 28
    target 1955
  ]
  edge [
    source 28
    target 1956
  ]
  edge [
    source 28
    target 1957
  ]
  edge [
    source 28
    target 1958
  ]
  edge [
    source 28
    target 1959
  ]
  edge [
    source 28
    target 1960
  ]
  edge [
    source 28
    target 1961
  ]
  edge [
    source 28
    target 1962
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 1963
  ]
  edge [
    source 28
    target 1964
  ]
  edge [
    source 28
    target 1965
  ]
  edge [
    source 28
    target 1966
  ]
  edge [
    source 28
    target 1967
  ]
  edge [
    source 28
    target 1968
  ]
  edge [
    source 28
    target 1969
  ]
  edge [
    source 28
    target 1970
  ]
  edge [
    source 28
    target 1971
  ]
  edge [
    source 28
    target 1972
  ]
  edge [
    source 28
    target 1973
  ]
  edge [
    source 28
    target 1974
  ]
  edge [
    source 28
    target 1975
  ]
  edge [
    source 28
    target 1976
  ]
  edge [
    source 28
    target 1977
  ]
  edge [
    source 28
    target 1978
  ]
  edge [
    source 28
    target 1979
  ]
  edge [
    source 28
    target 1980
  ]
  edge [
    source 28
    target 1981
  ]
  edge [
    source 28
    target 1982
  ]
  edge [
    source 28
    target 1983
  ]
  edge [
    source 28
    target 1984
  ]
  edge [
    source 28
    target 1985
  ]
  edge [
    source 28
    target 1746
  ]
  edge [
    source 28
    target 1986
  ]
  edge [
    source 28
    target 1987
  ]
  edge [
    source 28
    target 1988
  ]
  edge [
    source 28
    target 1989
  ]
  edge [
    source 28
    target 1990
  ]
  edge [
    source 28
    target 1991
  ]
  edge [
    source 28
    target 1992
  ]
  edge [
    source 28
    target 1993
  ]
  edge [
    source 28
    target 1994
  ]
  edge [
    source 28
    target 1995
  ]
  edge [
    source 28
    target 1996
  ]
  edge [
    source 28
    target 1997
  ]
  edge [
    source 28
    target 1998
  ]
  edge [
    source 28
    target 1999
  ]
  edge [
    source 28
    target 2000
  ]
  edge [
    source 28
    target 2001
  ]
  edge [
    source 28
    target 2002
  ]
  edge [
    source 28
    target 2003
  ]
  edge [
    source 28
    target 2004
  ]
  edge [
    source 28
    target 2005
  ]
  edge [
    source 28
    target 2006
  ]
  edge [
    source 28
    target 2007
  ]
  edge [
    source 28
    target 2008
  ]
  edge [
    source 28
    target 2009
  ]
  edge [
    source 28
    target 2010
  ]
  edge [
    source 28
    target 2011
  ]
  edge [
    source 28
    target 102
  ]
  edge [
    source 28
    target 2012
  ]
  edge [
    source 28
    target 2013
  ]
  edge [
    source 28
    target 2014
  ]
  edge [
    source 28
    target 1431
  ]
  edge [
    source 28
    target 2015
  ]
  edge [
    source 28
    target 2016
  ]
  edge [
    source 28
    target 2017
  ]
  edge [
    source 28
    target 1762
  ]
  edge [
    source 28
    target 2018
  ]
  edge [
    source 28
    target 2019
  ]
  edge [
    source 28
    target 2020
  ]
  edge [
    source 28
    target 2021
  ]
  edge [
    source 28
    target 1848
  ]
  edge [
    source 28
    target 2022
  ]
  edge [
    source 28
    target 2023
  ]
  edge [
    source 28
    target 2024
  ]
  edge [
    source 28
    target 2025
  ]
  edge [
    source 28
    target 2026
  ]
  edge [
    source 28
    target 2027
  ]
  edge [
    source 28
    target 2028
  ]
  edge [
    source 28
    target 2029
  ]
  edge [
    source 28
    target 2030
  ]
  edge [
    source 28
    target 2031
  ]
  edge [
    source 28
    target 2032
  ]
  edge [
    source 28
    target 2033
  ]
  edge [
    source 28
    target 2034
  ]
  edge [
    source 28
    target 2035
  ]
  edge [
    source 28
    target 2036
  ]
  edge [
    source 28
    target 2037
  ]
  edge [
    source 28
    target 2038
  ]
  edge [
    source 28
    target 2039
  ]
  edge [
    source 28
    target 2040
  ]
  edge [
    source 28
    target 2041
  ]
  edge [
    source 28
    target 2042
  ]
  edge [
    source 28
    target 2043
  ]
  edge [
    source 28
    target 2044
  ]
  edge [
    source 28
    target 2045
  ]
  edge [
    source 28
    target 2046
  ]
  edge [
    source 28
    target 2047
  ]
  edge [
    source 28
    target 2048
  ]
  edge [
    source 28
    target 2049
  ]
  edge [
    source 28
    target 2050
  ]
  edge [
    source 28
    target 2051
  ]
  edge [
    source 28
    target 2052
  ]
  edge [
    source 28
    target 2053
  ]
  edge [
    source 28
    target 2054
  ]
  edge [
    source 28
    target 2055
  ]
  edge [
    source 28
    target 2056
  ]
  edge [
    source 28
    target 2057
  ]
  edge [
    source 28
    target 2058
  ]
  edge [
    source 28
    target 2059
  ]
  edge [
    source 28
    target 2060
  ]
  edge [
    source 28
    target 2061
  ]
  edge [
    source 28
    target 2062
  ]
  edge [
    source 28
    target 2063
  ]
  edge [
    source 28
    target 2064
  ]
  edge [
    source 28
    target 2065
  ]
  edge [
    source 28
    target 1517
  ]
  edge [
    source 28
    target 2066
  ]
  edge [
    source 28
    target 2067
  ]
  edge [
    source 28
    target 107
  ]
  edge [
    source 28
    target 2068
  ]
  edge [
    source 28
    target 2069
  ]
  edge [
    source 28
    target 2070
  ]
  edge [
    source 28
    target 2071
  ]
  edge [
    source 28
    target 2072
  ]
  edge [
    source 28
    target 2073
  ]
  edge [
    source 28
    target 2074
  ]
  edge [
    source 28
    target 349
  ]
  edge [
    source 28
    target 2075
  ]
  edge [
    source 28
    target 2076
  ]
  edge [
    source 28
    target 2077
  ]
  edge [
    source 28
    target 2078
  ]
  edge [
    source 28
    target 2079
  ]
  edge [
    source 28
    target 2080
  ]
  edge [
    source 28
    target 2081
  ]
  edge [
    source 28
    target 2082
  ]
  edge [
    source 28
    target 2083
  ]
  edge [
    source 28
    target 368
  ]
  edge [
    source 28
    target 2084
  ]
  edge [
    source 28
    target 2085
  ]
  edge [
    source 28
    target 2086
  ]
  edge [
    source 28
    target 2087
  ]
  edge [
    source 28
    target 2088
  ]
  edge [
    source 28
    target 2089
  ]
  edge [
    source 28
    target 2090
  ]
  edge [
    source 28
    target 2091
  ]
  edge [
    source 28
    target 2092
  ]
  edge [
    source 28
    target 2093
  ]
  edge [
    source 28
    target 2094
  ]
  edge [
    source 28
    target 2095
  ]
  edge [
    source 28
    target 2096
  ]
  edge [
    source 28
    target 2097
  ]
  edge [
    source 28
    target 2098
  ]
  edge [
    source 28
    target 2099
  ]
  edge [
    source 28
    target 2100
  ]
  edge [
    source 28
    target 2101
  ]
  edge [
    source 28
    target 2102
  ]
  edge [
    source 28
    target 1560
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 2103
  ]
  edge [
    source 28
    target 2104
  ]
  edge [
    source 28
    target 1563
  ]
  edge [
    source 28
    target 2105
  ]
  edge [
    source 28
    target 1565
  ]
  edge [
    source 28
    target 2106
  ]
  edge [
    source 28
    target 2107
  ]
  edge [
    source 28
    target 2108
  ]
  edge [
    source 28
    target 2109
  ]
  edge [
    source 28
    target 2110
  ]
  edge [
    source 28
    target 2111
  ]
  edge [
    source 28
    target 2112
  ]
  edge [
    source 28
    target 2113
  ]
  edge [
    source 28
    target 2114
  ]
  edge [
    source 28
    target 2115
  ]
  edge [
    source 28
    target 2116
  ]
  edge [
    source 28
    target 2117
  ]
  edge [
    source 28
    target 2118
  ]
  edge [
    source 28
    target 2119
  ]
  edge [
    source 28
    target 2120
  ]
  edge [
    source 28
    target 2121
  ]
  edge [
    source 28
    target 2122
  ]
  edge [
    source 28
    target 2123
  ]
  edge [
    source 28
    target 2124
  ]
  edge [
    source 28
    target 2125
  ]
  edge [
    source 28
    target 2126
  ]
  edge [
    source 28
    target 2127
  ]
  edge [
    source 28
    target 2128
  ]
  edge [
    source 28
    target 2129
  ]
  edge [
    source 28
    target 2130
  ]
  edge [
    source 28
    target 2131
  ]
  edge [
    source 28
    target 2132
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 2133
  ]
  edge [
    source 28
    target 2134
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 2135
  ]
  edge [
    source 29
    target 2136
  ]
  edge [
    source 29
    target 2137
  ]
  edge [
    source 29
    target 2138
  ]
  edge [
    source 29
    target 1597
  ]
  edge [
    source 29
    target 2139
  ]
  edge [
    source 29
    target 2140
  ]
  edge [
    source 29
    target 2141
  ]
  edge [
    source 29
    target 2142
  ]
  edge [
    source 29
    target 2143
  ]
  edge [
    source 29
    target 2144
  ]
  edge [
    source 29
    target 1528
  ]
  edge [
    source 29
    target 2145
  ]
  edge [
    source 29
    target 2146
  ]
  edge [
    source 29
    target 2147
  ]
  edge [
    source 29
    target 2148
  ]
  edge [
    source 29
    target 2149
  ]
  edge [
    source 29
    target 2150
  ]
  edge [
    source 29
    target 2151
  ]
  edge [
    source 29
    target 2152
  ]
  edge [
    source 29
    target 2153
  ]
  edge [
    source 29
    target 198
  ]
  edge [
    source 29
    target 2154
  ]
  edge [
    source 29
    target 2155
  ]
  edge [
    source 29
    target 1585
  ]
  edge [
    source 29
    target 1588
  ]
  edge [
    source 29
    target 1506
  ]
  edge [
    source 29
    target 2156
  ]
  edge [
    source 29
    target 1514
  ]
  edge [
    source 29
    target 2157
  ]
  edge [
    source 29
    target 2158
  ]
  edge [
    source 29
    target 2159
  ]
  edge [
    source 29
    target 2160
  ]
  edge [
    source 29
    target 2161
  ]
  edge [
    source 29
    target 2162
  ]
  edge [
    source 29
    target 2163
  ]
  edge [
    source 29
    target 2164
  ]
  edge [
    source 29
    target 2165
  ]
  edge [
    source 29
    target 2166
  ]
  edge [
    source 29
    target 2167
  ]
  edge [
    source 29
    target 2168
  ]
  edge [
    source 29
    target 2169
  ]
  edge [
    source 29
    target 2170
  ]
  edge [
    source 29
    target 113
  ]
  edge [
    source 29
    target 2171
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 238
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 240
  ]
  edge [
    source 29
    target 2066
  ]
  edge [
    source 29
    target 1521
  ]
  edge [
    source 29
    target 1524
  ]
  edge [
    source 29
    target 2172
  ]
  edge [
    source 29
    target 1968
  ]
  edge [
    source 29
    target 2173
  ]
  edge [
    source 29
    target 104
  ]
  edge [
    source 29
    target 2005
  ]
  edge [
    source 29
    target 1531
  ]
  edge [
    source 29
    target 2174
  ]
  edge [
    source 29
    target 2175
  ]
  edge [
    source 29
    target 1535
  ]
  edge [
    source 29
    target 2007
  ]
  edge [
    source 29
    target 2176
  ]
  edge [
    source 29
    target 2177
  ]
  edge [
    source 29
    target 2011
  ]
  edge [
    source 29
    target 1538
  ]
  edge [
    source 29
    target 2178
  ]
  edge [
    source 29
    target 2179
  ]
  edge [
    source 29
    target 2180
  ]
  edge [
    source 29
    target 305
  ]
  edge [
    source 29
    target 2181
  ]
  edge [
    source 29
    target 2182
  ]
  edge [
    source 29
    target 2183
  ]
  edge [
    source 29
    target 1952
  ]
  edge [
    source 29
    target 2184
  ]
  edge [
    source 29
    target 2185
  ]
  edge [
    source 29
    target 2013
  ]
  edge [
    source 29
    target 2186
  ]
  edge [
    source 29
    target 71
  ]
  edge [
    source 29
    target 2187
  ]
  edge [
    source 29
    target 2188
  ]
  edge [
    source 29
    target 2189
  ]
  edge [
    source 29
    target 78
  ]
  edge [
    source 29
    target 310
  ]
  edge [
    source 29
    target 1532
  ]
  edge [
    source 29
    target 2190
  ]
  edge [
    source 29
    target 2191
  ]
  edge [
    source 29
    target 2192
  ]
  edge [
    source 29
    target 2193
  ]
  edge [
    source 29
    target 2194
  ]
  edge [
    source 29
    target 2195
  ]
  edge [
    source 29
    target 2196
  ]
  edge [
    source 29
    target 1713
  ]
  edge [
    source 29
    target 2197
  ]
  edge [
    source 29
    target 2198
  ]
  edge [
    source 29
    target 1634
  ]
  edge [
    source 29
    target 2199
  ]
  edge [
    source 29
    target 2200
  ]
  edge [
    source 29
    target 2201
  ]
  edge [
    source 29
    target 2202
  ]
  edge [
    source 29
    target 2203
  ]
  edge [
    source 29
    target 304
  ]
  edge [
    source 29
    target 2204
  ]
  edge [
    source 29
    target 2205
  ]
  edge [
    source 29
    target 2062
  ]
  edge [
    source 29
    target 1735
  ]
  edge [
    source 29
    target 2206
  ]
  edge [
    source 29
    target 2207
  ]
  edge [
    source 29
    target 2208
  ]
  edge [
    source 29
    target 2209
  ]
  edge [
    source 29
    target 647
  ]
  edge [
    source 29
    target 2210
  ]
  edge [
    source 29
    target 2211
  ]
  edge [
    source 29
    target 2212
  ]
  edge [
    source 29
    target 2213
  ]
  edge [
    source 29
    target 2214
  ]
  edge [
    source 29
    target 2215
  ]
  edge [
    source 29
    target 2216
  ]
  edge [
    source 29
    target 2217
  ]
  edge [
    source 29
    target 2218
  ]
  edge [
    source 29
    target 2219
  ]
  edge [
    source 29
    target 2220
  ]
  edge [
    source 29
    target 1307
  ]
  edge [
    source 29
    target 2221
  ]
  edge [
    source 29
    target 2222
  ]
  edge [
    source 29
    target 368
  ]
  edge [
    source 29
    target 2223
  ]
  edge [
    source 29
    target 2224
  ]
  edge [
    source 29
    target 2225
  ]
  edge [
    source 29
    target 57
  ]
  edge [
    source 29
    target 55
  ]
  edge [
    source 29
    target 58
  ]
  edge [
    source 29
    target 59
  ]
  edge [
    source 29
    target 2226
  ]
  edge [
    source 29
    target 61
  ]
  edge [
    source 29
    target 2227
  ]
  edge [
    source 29
    target 2228
  ]
  edge [
    source 29
    target 2229
  ]
  edge [
    source 29
    target 64
  ]
  edge [
    source 29
    target 65
  ]
  edge [
    source 29
    target 66
  ]
  edge [
    source 29
    target 2230
  ]
  edge [
    source 29
    target 68
  ]
  edge [
    source 29
    target 69
  ]
  edge [
    source 29
    target 72
  ]
  edge [
    source 29
    target 2231
  ]
  edge [
    source 29
    target 73
  ]
  edge [
    source 29
    target 2232
  ]
  edge [
    source 29
    target 76
  ]
  edge [
    source 29
    target 2233
  ]
  edge [
    source 29
    target 77
  ]
  edge [
    source 29
    target 79
  ]
  edge [
    source 29
    target 80
  ]
  edge [
    source 29
    target 2234
  ]
  edge [
    source 29
    target 82
  ]
  edge [
    source 29
    target 2235
  ]
  edge [
    source 29
    target 84
  ]
  edge [
    source 29
    target 85
  ]
  edge [
    source 29
    target 89
  ]
  edge [
    source 29
    target 2236
  ]
  edge [
    source 29
    target 2237
  ]
  edge [
    source 29
    target 2238
  ]
  edge [
    source 29
    target 2239
  ]
  edge [
    source 30
    target 2240
  ]
  edge [
    source 30
    target 2241
  ]
  edge [
    source 30
    target 2242
  ]
  edge [
    source 30
    target 2243
  ]
  edge [
    source 30
    target 2244
  ]
  edge [
    source 30
    target 2245
  ]
  edge [
    source 30
    target 2246
  ]
  edge [
    source 30
    target 2247
  ]
  edge [
    source 30
    target 2248
  ]
  edge [
    source 30
    target 2249
  ]
  edge [
    source 30
    target 2250
  ]
  edge [
    source 30
    target 2251
  ]
  edge [
    source 30
    target 2252
  ]
  edge [
    source 30
    target 2253
  ]
  edge [
    source 30
    target 2091
  ]
  edge [
    source 30
    target 2254
  ]
  edge [
    source 30
    target 2255
  ]
  edge [
    source 30
    target 2256
  ]
  edge [
    source 30
    target 2257
  ]
  edge [
    source 30
    target 2258
  ]
  edge [
    source 30
    target 2259
  ]
  edge [
    source 30
    target 2260
  ]
  edge [
    source 30
    target 2261
  ]
  edge [
    source 30
    target 2262
  ]
  edge [
    source 30
    target 2263
  ]
  edge [
    source 30
    target 2264
  ]
  edge [
    source 30
    target 2265
  ]
  edge [
    source 30
    target 2266
  ]
  edge [
    source 30
    target 2267
  ]
  edge [
    source 30
    target 2268
  ]
  edge [
    source 30
    target 2269
  ]
  edge [
    source 30
    target 2270
  ]
  edge [
    source 30
    target 2271
  ]
  edge [
    source 30
    target 2272
  ]
  edge [
    source 30
    target 2273
  ]
  edge [
    source 30
    target 2274
  ]
  edge [
    source 30
    target 2275
  ]
  edge [
    source 30
    target 2276
  ]
  edge [
    source 30
    target 1972
  ]
  edge [
    source 30
    target 2277
  ]
  edge [
    source 30
    target 2278
  ]
  edge [
    source 30
    target 2279
  ]
  edge [
    source 30
    target 2280
  ]
  edge [
    source 30
    target 2281
  ]
  edge [
    source 30
    target 2282
  ]
  edge [
    source 30
    target 2283
  ]
  edge [
    source 30
    target 2284
  ]
  edge [
    source 30
    target 2285
  ]
  edge [
    source 30
    target 2286
  ]
  edge [
    source 30
    target 2287
  ]
  edge [
    source 30
    target 2288
  ]
  edge [
    source 30
    target 2289
  ]
  edge [
    source 30
    target 2290
  ]
  edge [
    source 30
    target 2291
  ]
  edge [
    source 31
    target 2292
  ]
  edge [
    source 31
    target 2293
  ]
  edge [
    source 31
    target 2294
  ]
  edge [
    source 31
    target 2295
  ]
  edge [
    source 31
    target 2296
  ]
  edge [
    source 31
    target 2297
  ]
  edge [
    source 31
    target 2298
  ]
  edge [
    source 272
    target 2299
  ]
  edge [
    source 272
    target 2300
  ]
  edge [
    source 272
    target 2301
  ]
  edge [
    source 2299
    target 2300
  ]
  edge [
    source 2299
    target 2301
  ]
  edge [
    source 2300
    target 2301
  ]
  edge [
    source 2302
    target 2303
  ]
  edge [
    source 2304
    target 2305
  ]
]
