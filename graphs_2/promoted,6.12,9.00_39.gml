graph [
  node [
    id 0
    label "dominik"
    origin "text"
  ]
  node [
    id 1
    label "olszewski"
    origin "text"
  ]
  node [
    id 2
    label "zalega&#263;"
    origin "text"
  ]
  node [
    id 3
    label "abonament"
    origin "text"
  ]
  node [
    id 4
    label "rtv"
    origin "text"
  ]
  node [
    id 5
    label "cover"
  ]
  node [
    id 6
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 7
    label "pozostawa&#263;"
  ]
  node [
    id 8
    label "screen"
  ]
  node [
    id 9
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 10
    label "z&#322;o&#380;e"
  ]
  node [
    id 11
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 12
    label "wype&#322;nia&#263;"
  ]
  node [
    id 13
    label "front"
  ]
  node [
    id 14
    label "mie&#263;_miejsce"
  ]
  node [
    id 15
    label "odst&#281;powa&#263;"
  ]
  node [
    id 16
    label "perform"
  ]
  node [
    id 17
    label "wychodzi&#263;"
  ]
  node [
    id 18
    label "seclude"
  ]
  node [
    id 19
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 20
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 21
    label "nak&#322;ania&#263;"
  ]
  node [
    id 22
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 23
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 24
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 25
    label "dzia&#322;a&#263;"
  ]
  node [
    id 26
    label "act"
  ]
  node [
    id 27
    label "appear"
  ]
  node [
    id 28
    label "unwrap"
  ]
  node [
    id 29
    label "rezygnowa&#263;"
  ]
  node [
    id 30
    label "overture"
  ]
  node [
    id 31
    label "uczestniczy&#263;"
  ]
  node [
    id 32
    label "robi&#263;"
  ]
  node [
    id 33
    label "zajmowa&#263;"
  ]
  node [
    id 34
    label "istnie&#263;"
  ]
  node [
    id 35
    label "close"
  ]
  node [
    id 36
    label "meet"
  ]
  node [
    id 37
    label "charge"
  ]
  node [
    id 38
    label "powodowa&#263;"
  ]
  node [
    id 39
    label "umieszcza&#263;"
  ]
  node [
    id 40
    label "do"
  ]
  node [
    id 41
    label "blend"
  ]
  node [
    id 42
    label "stop"
  ]
  node [
    id 43
    label "przebywa&#263;"
  ]
  node [
    id 44
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 45
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 46
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 47
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 48
    label "support"
  ]
  node [
    id 49
    label "zaleganie"
  ]
  node [
    id 50
    label "bilansowo&#347;&#263;"
  ]
  node [
    id 51
    label "skupienie"
  ]
  node [
    id 52
    label "zasoby_kopalin"
  ]
  node [
    id 53
    label "wychodnia"
  ]
  node [
    id 54
    label "rokada"
  ]
  node [
    id 55
    label "linia"
  ]
  node [
    id 56
    label "pole_bitwy"
  ]
  node [
    id 57
    label "sfera"
  ]
  node [
    id 58
    label "powietrze"
  ]
  node [
    id 59
    label "zjednoczenie"
  ]
  node [
    id 60
    label "przedpole"
  ]
  node [
    id 61
    label "prz&#243;d"
  ]
  node [
    id 62
    label "budynek"
  ]
  node [
    id 63
    label "szczyt"
  ]
  node [
    id 64
    label "przycz&#243;&#322;ek"
  ]
  node [
    id 65
    label "zjawisko"
  ]
  node [
    id 66
    label "pomieszczenie"
  ]
  node [
    id 67
    label "elewacja"
  ]
  node [
    id 68
    label "stowarzyszenie"
  ]
  node [
    id 69
    label "zapis"
  ]
  node [
    id 70
    label "screenshot"
  ]
  node [
    id 71
    label "kawa&#322;ek"
  ]
  node [
    id 72
    label "aran&#380;acja"
  ]
  node [
    id 73
    label "season"
  ]
  node [
    id 74
    label "bloczek"
  ]
  node [
    id 75
    label "przedp&#322;ata"
  ]
  node [
    id 76
    label "kwota"
  ]
  node [
    id 77
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 78
    label "materia&#322;_budowlany"
  ]
  node [
    id 79
    label "prostopad&#322;o&#347;cian"
  ]
  node [
    id 80
    label "element"
  ]
  node [
    id 81
    label "notatnik"
  ]
  node [
    id 82
    label "pad"
  ]
  node [
    id 83
    label "elektronika"
  ]
  node [
    id 84
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 85
    label "optoelektronika"
  ]
  node [
    id 86
    label "energoelektronika"
  ]
  node [
    id 87
    label "muzyka_rozrywkowa"
  ]
  node [
    id 88
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 89
    label "bioelektronika"
  ]
  node [
    id 90
    label "sprz&#281;t"
  ]
  node [
    id 91
    label "radioelektronika"
  ]
  node [
    id 92
    label "&#322;&#261;cz&#243;wka"
  ]
  node [
    id 93
    label "nauka"
  ]
  node [
    id 94
    label "mikroelektronika"
  ]
  node [
    id 95
    label "elektrotermia"
  ]
  node [
    id 96
    label "Dominik"
  ]
  node [
    id 97
    label "Olszewski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 96
    target 97
  ]
]
