graph [
  node [
    id 0
    label "alc&#225;cer"
    origin "text"
  ]
  node [
    id 1
    label "sala"
    origin "text"
  ]
  node [
    id 2
    label "audience"
  ]
  node [
    id 3
    label "zgromadzenie"
  ]
  node [
    id 4
    label "publiczno&#347;&#263;"
  ]
  node [
    id 5
    label "pomieszczenie"
  ]
  node [
    id 6
    label "odbiorca"
  ]
  node [
    id 7
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 8
    label "publiczka"
  ]
  node [
    id 9
    label "widzownia"
  ]
  node [
    id 10
    label "concourse"
  ]
  node [
    id 11
    label "gathering"
  ]
  node [
    id 12
    label "skupienie"
  ]
  node [
    id 13
    label "wsp&#243;lnota"
  ]
  node [
    id 14
    label "spowodowanie"
  ]
  node [
    id 15
    label "spotkanie"
  ]
  node [
    id 16
    label "organ"
  ]
  node [
    id 17
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 18
    label "grupa"
  ]
  node [
    id 19
    label "gromadzenie"
  ]
  node [
    id 20
    label "templum"
  ]
  node [
    id 21
    label "konwentykiel"
  ]
  node [
    id 22
    label "klasztor"
  ]
  node [
    id 23
    label "caucus"
  ]
  node [
    id 24
    label "czynno&#347;&#263;"
  ]
  node [
    id 25
    label "pozyskanie"
  ]
  node [
    id 26
    label "kongregacja"
  ]
  node [
    id 27
    label "amfilada"
  ]
  node [
    id 28
    label "front"
  ]
  node [
    id 29
    label "apartment"
  ]
  node [
    id 30
    label "udost&#281;pnienie"
  ]
  node [
    id 31
    label "pod&#322;oga"
  ]
  node [
    id 32
    label "miejsce"
  ]
  node [
    id 33
    label "sklepienie"
  ]
  node [
    id 34
    label "sufit"
  ]
  node [
    id 35
    label "umieszczenie"
  ]
  node [
    id 36
    label "zakamarek"
  ]
  node [
    id 37
    label "Alc&#225;cer"
  ]
  node [
    id 38
    label "do"
  ]
  node [
    id 39
    label "Alentejo"
  ]
  node [
    id 40
    label "litoral"
  ]
  node [
    id 41
    label "Santa"
  ]
  node [
    id 42
    label "Maria"
  ]
  node [
    id 43
    label "Castelo"
  ]
  node [
    id 44
    label "Susana"
  ]
  node [
    id 45
    label "S&#227;o"
  ]
  node [
    id 46
    label "Martinho"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 38
    target 42
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 45
    target 46
  ]
]
