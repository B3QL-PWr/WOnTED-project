graph [
  node [
    id 0
    label "organizator"
    origin "text"
  ]
  node [
    id 1
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zale&#378;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 4
    label "pytanie"
    origin "text"
  ]
  node [
    id 5
    label "mo&#380;liwy"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zniesienie"
    origin "text"
  ]
  node [
    id 8
    label "dyskryminacja"
    origin "text"
  ]
  node [
    id 9
    label "kobieta"
    origin "text"
  ]
  node [
    id 10
    label "m&#281;ski"
    origin "text"
  ]
  node [
    id 11
    label "dominacja"
    origin "text"
  ]
  node [
    id 12
    label "bez"
    origin "text"
  ]
  node [
    id 13
    label "inny"
    origin "text"
  ]
  node [
    id 14
    label "forma"
    origin "text"
  ]
  node [
    id 15
    label "opresja"
    origin "text"
  ]
  node [
    id 16
    label "radykalny"
    origin "text"
  ]
  node [
    id 17
    label "ruch"
    origin "text"
  ]
  node [
    id 18
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 19
    label "jak"
    origin "text"
  ]
  node [
    id 20
    label "skrajnie"
    origin "text"
  ]
  node [
    id 21
    label "czy"
    origin "text"
  ]
  node [
    id 22
    label "stan"
    origin "text"
  ]
  node [
    id 23
    label "rozwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 24
    label "pal&#261;ca"
    origin "text"
  ]
  node [
    id 25
    label "problem"
    origin "text"
  ]
  node [
    id 26
    label "tym"
    origin "text"
  ]
  node [
    id 27
    label "spiritus_movens"
  ]
  node [
    id 28
    label "realizator"
  ]
  node [
    id 29
    label "wykonawca"
  ]
  node [
    id 30
    label "czu&#263;"
  ]
  node [
    id 31
    label "desire"
  ]
  node [
    id 32
    label "kcie&#263;"
  ]
  node [
    id 33
    label "postrzega&#263;"
  ]
  node [
    id 34
    label "przewidywa&#263;"
  ]
  node [
    id 35
    label "smell"
  ]
  node [
    id 36
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 37
    label "uczuwa&#263;"
  ]
  node [
    id 38
    label "spirit"
  ]
  node [
    id 39
    label "doznawa&#263;"
  ]
  node [
    id 40
    label "anticipate"
  ]
  node [
    id 41
    label "react"
  ]
  node [
    id 42
    label "replica"
  ]
  node [
    id 43
    label "rozmowa"
  ]
  node [
    id 44
    label "wyj&#347;cie"
  ]
  node [
    id 45
    label "respondent"
  ]
  node [
    id 46
    label "dokument"
  ]
  node [
    id 47
    label "reakcja"
  ]
  node [
    id 48
    label "zachowanie"
  ]
  node [
    id 49
    label "reaction"
  ]
  node [
    id 50
    label "organizm"
  ]
  node [
    id 51
    label "response"
  ]
  node [
    id 52
    label "rezultat"
  ]
  node [
    id 53
    label "zapis"
  ]
  node [
    id 54
    label "&#347;wiadectwo"
  ]
  node [
    id 55
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 56
    label "wytw&#243;r"
  ]
  node [
    id 57
    label "parafa"
  ]
  node [
    id 58
    label "plik"
  ]
  node [
    id 59
    label "raport&#243;wka"
  ]
  node [
    id 60
    label "utw&#243;r"
  ]
  node [
    id 61
    label "record"
  ]
  node [
    id 62
    label "registratura"
  ]
  node [
    id 63
    label "dokumentacja"
  ]
  node [
    id 64
    label "fascyku&#322;"
  ]
  node [
    id 65
    label "artyku&#322;"
  ]
  node [
    id 66
    label "writing"
  ]
  node [
    id 67
    label "sygnatariusz"
  ]
  node [
    id 68
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 69
    label "okazanie_si&#281;"
  ]
  node [
    id 70
    label "ograniczenie"
  ]
  node [
    id 71
    label "uzyskanie"
  ]
  node [
    id 72
    label "ruszenie"
  ]
  node [
    id 73
    label "podzianie_si&#281;"
  ]
  node [
    id 74
    label "spotkanie"
  ]
  node [
    id 75
    label "powychodzenie"
  ]
  node [
    id 76
    label "opuszczenie"
  ]
  node [
    id 77
    label "postrze&#380;enie"
  ]
  node [
    id 78
    label "transgression"
  ]
  node [
    id 79
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 80
    label "wychodzenie"
  ]
  node [
    id 81
    label "uko&#324;czenie"
  ]
  node [
    id 82
    label "miejsce"
  ]
  node [
    id 83
    label "powiedzenie_si&#281;"
  ]
  node [
    id 84
    label "policzenie"
  ]
  node [
    id 85
    label "podziewanie_si&#281;"
  ]
  node [
    id 86
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 87
    label "exit"
  ]
  node [
    id 88
    label "vent"
  ]
  node [
    id 89
    label "uwolnienie_si&#281;"
  ]
  node [
    id 90
    label "deviation"
  ]
  node [
    id 91
    label "release"
  ]
  node [
    id 92
    label "wych&#243;d"
  ]
  node [
    id 93
    label "withdrawal"
  ]
  node [
    id 94
    label "wypadni&#281;cie"
  ]
  node [
    id 95
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 96
    label "kres"
  ]
  node [
    id 97
    label "odch&#243;d"
  ]
  node [
    id 98
    label "przebywanie"
  ]
  node [
    id 99
    label "przedstawienie"
  ]
  node [
    id 100
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 101
    label "zagranie"
  ]
  node [
    id 102
    label "zako&#324;czenie"
  ]
  node [
    id 103
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 104
    label "emergence"
  ]
  node [
    id 105
    label "cisza"
  ]
  node [
    id 106
    label "rozhowor"
  ]
  node [
    id 107
    label "discussion"
  ]
  node [
    id 108
    label "czynno&#347;&#263;"
  ]
  node [
    id 109
    label "badany"
  ]
  node [
    id 110
    label "sprawa"
  ]
  node [
    id 111
    label "wypytanie"
  ]
  node [
    id 112
    label "egzaminowanie"
  ]
  node [
    id 113
    label "zwracanie_si&#281;"
  ]
  node [
    id 114
    label "wywo&#322;ywanie"
  ]
  node [
    id 115
    label "rozpytywanie"
  ]
  node [
    id 116
    label "wypowiedzenie"
  ]
  node [
    id 117
    label "wypowied&#378;"
  ]
  node [
    id 118
    label "problemat"
  ]
  node [
    id 119
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 120
    label "problematyka"
  ]
  node [
    id 121
    label "sprawdzian"
  ]
  node [
    id 122
    label "zadanie"
  ]
  node [
    id 123
    label "odpowiada&#263;"
  ]
  node [
    id 124
    label "przes&#322;uchiwanie"
  ]
  node [
    id 125
    label "question"
  ]
  node [
    id 126
    label "sprawdzanie"
  ]
  node [
    id 127
    label "odpowiadanie"
  ]
  node [
    id 128
    label "survey"
  ]
  node [
    id 129
    label "pos&#322;uchanie"
  ]
  node [
    id 130
    label "s&#261;d"
  ]
  node [
    id 131
    label "sparafrazowanie"
  ]
  node [
    id 132
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 133
    label "strawestowa&#263;"
  ]
  node [
    id 134
    label "sparafrazowa&#263;"
  ]
  node [
    id 135
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 136
    label "trawestowa&#263;"
  ]
  node [
    id 137
    label "sformu&#322;owanie"
  ]
  node [
    id 138
    label "parafrazowanie"
  ]
  node [
    id 139
    label "ozdobnik"
  ]
  node [
    id 140
    label "delimitacja"
  ]
  node [
    id 141
    label "parafrazowa&#263;"
  ]
  node [
    id 142
    label "stylizacja"
  ]
  node [
    id 143
    label "komunikat"
  ]
  node [
    id 144
    label "trawestowanie"
  ]
  node [
    id 145
    label "strawestowanie"
  ]
  node [
    id 146
    label "konwersja"
  ]
  node [
    id 147
    label "notice"
  ]
  node [
    id 148
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 149
    label "przepowiedzenie"
  ]
  node [
    id 150
    label "rozwi&#261;zanie"
  ]
  node [
    id 151
    label "generowa&#263;"
  ]
  node [
    id 152
    label "wydanie"
  ]
  node [
    id 153
    label "message"
  ]
  node [
    id 154
    label "generowanie"
  ]
  node [
    id 155
    label "wydobycie"
  ]
  node [
    id 156
    label "zwerbalizowanie"
  ]
  node [
    id 157
    label "szyk"
  ]
  node [
    id 158
    label "notification"
  ]
  node [
    id 159
    label "powiedzenie"
  ]
  node [
    id 160
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 161
    label "denunciation"
  ]
  node [
    id 162
    label "wyra&#380;enie"
  ]
  node [
    id 163
    label "zaj&#281;cie"
  ]
  node [
    id 164
    label "yield"
  ]
  node [
    id 165
    label "zbi&#243;r"
  ]
  node [
    id 166
    label "zaszkodzenie"
  ]
  node [
    id 167
    label "za&#322;o&#380;enie"
  ]
  node [
    id 168
    label "duty"
  ]
  node [
    id 169
    label "powierzanie"
  ]
  node [
    id 170
    label "work"
  ]
  node [
    id 171
    label "przepisanie"
  ]
  node [
    id 172
    label "nakarmienie"
  ]
  node [
    id 173
    label "przepisa&#263;"
  ]
  node [
    id 174
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 175
    label "zobowi&#261;zanie"
  ]
  node [
    id 176
    label "kognicja"
  ]
  node [
    id 177
    label "object"
  ]
  node [
    id 178
    label "rozprawa"
  ]
  node [
    id 179
    label "temat"
  ]
  node [
    id 180
    label "wydarzenie"
  ]
  node [
    id 181
    label "szczeg&#243;&#322;"
  ]
  node [
    id 182
    label "proposition"
  ]
  node [
    id 183
    label "przes&#322;anka"
  ]
  node [
    id 184
    label "rzecz"
  ]
  node [
    id 185
    label "idea"
  ]
  node [
    id 186
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 187
    label "ustalenie"
  ]
  node [
    id 188
    label "redagowanie"
  ]
  node [
    id 189
    label "ustalanie"
  ]
  node [
    id 190
    label "dociekanie"
  ]
  node [
    id 191
    label "robienie"
  ]
  node [
    id 192
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 193
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 194
    label "investigation"
  ]
  node [
    id 195
    label "macanie"
  ]
  node [
    id 196
    label "usi&#322;owanie"
  ]
  node [
    id 197
    label "penetrowanie"
  ]
  node [
    id 198
    label "przymierzanie"
  ]
  node [
    id 199
    label "przymierzenie"
  ]
  node [
    id 200
    label "examination"
  ]
  node [
    id 201
    label "wypytywanie"
  ]
  node [
    id 202
    label "zbadanie"
  ]
  node [
    id 203
    label "dawa&#263;"
  ]
  node [
    id 204
    label "ponosi&#263;"
  ]
  node [
    id 205
    label "report"
  ]
  node [
    id 206
    label "equate"
  ]
  node [
    id 207
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 208
    label "answer"
  ]
  node [
    id 209
    label "powodowa&#263;"
  ]
  node [
    id 210
    label "tone"
  ]
  node [
    id 211
    label "contend"
  ]
  node [
    id 212
    label "reagowa&#263;"
  ]
  node [
    id 213
    label "impart"
  ]
  node [
    id 214
    label "reagowanie"
  ]
  node [
    id 215
    label "dawanie"
  ]
  node [
    id 216
    label "powodowanie"
  ]
  node [
    id 217
    label "bycie"
  ]
  node [
    id 218
    label "pokutowanie"
  ]
  node [
    id 219
    label "odpowiedzialny"
  ]
  node [
    id 220
    label "winny"
  ]
  node [
    id 221
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 222
    label "picie_piwa"
  ]
  node [
    id 223
    label "odpowiedni"
  ]
  node [
    id 224
    label "parry"
  ]
  node [
    id 225
    label "fit"
  ]
  node [
    id 226
    label "dzianie_si&#281;"
  ]
  node [
    id 227
    label "rendition"
  ]
  node [
    id 228
    label "ponoszenie"
  ]
  node [
    id 229
    label "rozmawianie"
  ]
  node [
    id 230
    label "faza"
  ]
  node [
    id 231
    label "podchodzi&#263;"
  ]
  node [
    id 232
    label "&#263;wiczenie"
  ]
  node [
    id 233
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 234
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 235
    label "praca_pisemna"
  ]
  node [
    id 236
    label "kontrola"
  ]
  node [
    id 237
    label "dydaktyka"
  ]
  node [
    id 238
    label "pr&#243;ba"
  ]
  node [
    id 239
    label "przepytywanie"
  ]
  node [
    id 240
    label "oznajmianie"
  ]
  node [
    id 241
    label "wzywanie"
  ]
  node [
    id 242
    label "development"
  ]
  node [
    id 243
    label "exploitation"
  ]
  node [
    id 244
    label "zdawanie"
  ]
  node [
    id 245
    label "w&#322;&#261;czanie"
  ]
  node [
    id 246
    label "s&#322;uchanie"
  ]
  node [
    id 247
    label "urealnianie"
  ]
  node [
    id 248
    label "mo&#380;ebny"
  ]
  node [
    id 249
    label "umo&#380;liwianie"
  ]
  node [
    id 250
    label "zno&#347;ny"
  ]
  node [
    id 251
    label "umo&#380;liwienie"
  ]
  node [
    id 252
    label "mo&#380;liwie"
  ]
  node [
    id 253
    label "urealnienie"
  ]
  node [
    id 254
    label "dost&#281;pny"
  ]
  node [
    id 255
    label "zno&#347;nie"
  ]
  node [
    id 256
    label "niez&#322;y"
  ]
  node [
    id 257
    label "niedokuczliwy"
  ]
  node [
    id 258
    label "wzgl&#281;dny"
  ]
  node [
    id 259
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 260
    label "odblokowanie_si&#281;"
  ]
  node [
    id 261
    label "zrozumia&#322;y"
  ]
  node [
    id 262
    label "dost&#281;pnie"
  ]
  node [
    id 263
    label "&#322;atwy"
  ]
  node [
    id 264
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 265
    label "przyst&#281;pnie"
  ]
  node [
    id 266
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 267
    label "upowa&#380;nianie"
  ]
  node [
    id 268
    label "spowodowanie"
  ]
  node [
    id 269
    label "upowa&#380;nienie"
  ]
  node [
    id 270
    label "zrobienie"
  ]
  node [
    id 271
    label "akceptowalny"
  ]
  node [
    id 272
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 273
    label "mie&#263;_miejsce"
  ]
  node [
    id 274
    label "equal"
  ]
  node [
    id 275
    label "trwa&#263;"
  ]
  node [
    id 276
    label "chodzi&#263;"
  ]
  node [
    id 277
    label "si&#281;ga&#263;"
  ]
  node [
    id 278
    label "obecno&#347;&#263;"
  ]
  node [
    id 279
    label "stand"
  ]
  node [
    id 280
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 281
    label "uczestniczy&#263;"
  ]
  node [
    id 282
    label "participate"
  ]
  node [
    id 283
    label "robi&#263;"
  ]
  node [
    id 284
    label "istnie&#263;"
  ]
  node [
    id 285
    label "pozostawa&#263;"
  ]
  node [
    id 286
    label "zostawa&#263;"
  ]
  node [
    id 287
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 288
    label "adhere"
  ]
  node [
    id 289
    label "compass"
  ]
  node [
    id 290
    label "korzysta&#263;"
  ]
  node [
    id 291
    label "appreciation"
  ]
  node [
    id 292
    label "osi&#261;ga&#263;"
  ]
  node [
    id 293
    label "dociera&#263;"
  ]
  node [
    id 294
    label "get"
  ]
  node [
    id 295
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 296
    label "mierzy&#263;"
  ]
  node [
    id 297
    label "u&#380;ywa&#263;"
  ]
  node [
    id 298
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 299
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 300
    label "exsert"
  ]
  node [
    id 301
    label "being"
  ]
  node [
    id 302
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 303
    label "cecha"
  ]
  node [
    id 304
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 305
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 306
    label "p&#322;ywa&#263;"
  ]
  node [
    id 307
    label "run"
  ]
  node [
    id 308
    label "bangla&#263;"
  ]
  node [
    id 309
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 310
    label "przebiega&#263;"
  ]
  node [
    id 311
    label "wk&#322;ada&#263;"
  ]
  node [
    id 312
    label "proceed"
  ]
  node [
    id 313
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 314
    label "carry"
  ]
  node [
    id 315
    label "bywa&#263;"
  ]
  node [
    id 316
    label "dziama&#263;"
  ]
  node [
    id 317
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 318
    label "stara&#263;_si&#281;"
  ]
  node [
    id 319
    label "para"
  ]
  node [
    id 320
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 321
    label "str&#243;j"
  ]
  node [
    id 322
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 323
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 324
    label "krok"
  ]
  node [
    id 325
    label "tryb"
  ]
  node [
    id 326
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 327
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 328
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 329
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 330
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 331
    label "continue"
  ]
  node [
    id 332
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 333
    label "Ohio"
  ]
  node [
    id 334
    label "wci&#281;cie"
  ]
  node [
    id 335
    label "Nowy_York"
  ]
  node [
    id 336
    label "warstwa"
  ]
  node [
    id 337
    label "samopoczucie"
  ]
  node [
    id 338
    label "Illinois"
  ]
  node [
    id 339
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 340
    label "state"
  ]
  node [
    id 341
    label "Jukatan"
  ]
  node [
    id 342
    label "Kalifornia"
  ]
  node [
    id 343
    label "Wirginia"
  ]
  node [
    id 344
    label "wektor"
  ]
  node [
    id 345
    label "Teksas"
  ]
  node [
    id 346
    label "Goa"
  ]
  node [
    id 347
    label "Waszyngton"
  ]
  node [
    id 348
    label "Massachusetts"
  ]
  node [
    id 349
    label "Alaska"
  ]
  node [
    id 350
    label "Arakan"
  ]
  node [
    id 351
    label "Hawaje"
  ]
  node [
    id 352
    label "Maryland"
  ]
  node [
    id 353
    label "punkt"
  ]
  node [
    id 354
    label "Michigan"
  ]
  node [
    id 355
    label "Arizona"
  ]
  node [
    id 356
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 357
    label "Georgia"
  ]
  node [
    id 358
    label "poziom"
  ]
  node [
    id 359
    label "Pensylwania"
  ]
  node [
    id 360
    label "shape"
  ]
  node [
    id 361
    label "Luizjana"
  ]
  node [
    id 362
    label "Nowy_Meksyk"
  ]
  node [
    id 363
    label "Alabama"
  ]
  node [
    id 364
    label "ilo&#347;&#263;"
  ]
  node [
    id 365
    label "Kansas"
  ]
  node [
    id 366
    label "Oregon"
  ]
  node [
    id 367
    label "Floryda"
  ]
  node [
    id 368
    label "Oklahoma"
  ]
  node [
    id 369
    label "jednostka_administracyjna"
  ]
  node [
    id 370
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 371
    label "ranny"
  ]
  node [
    id 372
    label "jajko"
  ]
  node [
    id 373
    label "zgromadzenie"
  ]
  node [
    id 374
    label "urodzenie"
  ]
  node [
    id 375
    label "suspension"
  ]
  node [
    id 376
    label "poddanie_si&#281;"
  ]
  node [
    id 377
    label "extinction"
  ]
  node [
    id 378
    label "coitus_interruptus"
  ]
  node [
    id 379
    label "przetrwanie"
  ]
  node [
    id 380
    label "&#347;cierpienie"
  ]
  node [
    id 381
    label "abolicjonista"
  ]
  node [
    id 382
    label "zniszczenie"
  ]
  node [
    id 383
    label "posk&#322;adanie"
  ]
  node [
    id 384
    label "zebranie"
  ]
  node [
    id 385
    label "przeniesienie"
  ]
  node [
    id 386
    label "removal"
  ]
  node [
    id 387
    label "revocation"
  ]
  node [
    id 388
    label "usuni&#281;cie"
  ]
  node [
    id 389
    label "wygranie"
  ]
  node [
    id 390
    label "porwanie"
  ]
  node [
    id 391
    label "uniewa&#380;nienie"
  ]
  node [
    id 392
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 393
    label "beat"
  ]
  node [
    id 394
    label "zwojowanie"
  ]
  node [
    id 395
    label "wr&#243;cenie_z_tarcz&#261;"
  ]
  node [
    id 396
    label "zdarzenie_si&#281;"
  ]
  node [
    id 397
    label "zapanowanie"
  ]
  node [
    id 398
    label "wygrywanie"
  ]
  node [
    id 399
    label "wear"
  ]
  node [
    id 400
    label "destruction"
  ]
  node [
    id 401
    label "zu&#380;ycie"
  ]
  node [
    id 402
    label "attrition"
  ]
  node [
    id 403
    label "os&#322;abienie"
  ]
  node [
    id 404
    label "podpalenie"
  ]
  node [
    id 405
    label "strata"
  ]
  node [
    id 406
    label "kondycja_fizyczna"
  ]
  node [
    id 407
    label "spl&#261;drowanie"
  ]
  node [
    id 408
    label "zdrowie"
  ]
  node [
    id 409
    label "poniszczenie"
  ]
  node [
    id 410
    label "ruin"
  ]
  node [
    id 411
    label "stanie_si&#281;"
  ]
  node [
    id 412
    label "poniszczenie_si&#281;"
  ]
  node [
    id 413
    label "trafienie"
  ]
  node [
    id 414
    label "concourse"
  ]
  node [
    id 415
    label "templum"
  ]
  node [
    id 416
    label "konwentykiel"
  ]
  node [
    id 417
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 418
    label "dostawanie"
  ]
  node [
    id 419
    label "uderzenie"
  ]
  node [
    id 420
    label "pozyskanie"
  ]
  node [
    id 421
    label "wzi&#281;cie"
  ]
  node [
    id 422
    label "sprz&#261;tni&#281;cie"
  ]
  node [
    id 423
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 424
    label "caucus"
  ]
  node [
    id 425
    label "gathering"
  ]
  node [
    id 426
    label "merging"
  ]
  node [
    id 427
    label "skupienie"
  ]
  node [
    id 428
    label "party"
  ]
  node [
    id 429
    label "ocenienie"
  ]
  node [
    id 430
    label "gather"
  ]
  node [
    id 431
    label "status"
  ]
  node [
    id 432
    label "porodzenie"
  ]
  node [
    id 433
    label "narodzenie"
  ]
  node [
    id 434
    label "pocz&#261;tek"
  ]
  node [
    id 435
    label "urodzenie_si&#281;"
  ]
  node [
    id 436
    label "powicie"
  ]
  node [
    id 437
    label "donoszenie"
  ]
  node [
    id 438
    label "zlegni&#281;cie"
  ]
  node [
    id 439
    label "beginning"
  ]
  node [
    id 440
    label "wsp&#243;lnota"
  ]
  node [
    id 441
    label "organ"
  ]
  node [
    id 442
    label "grupa"
  ]
  node [
    id 443
    label "gromadzenie"
  ]
  node [
    id 444
    label "klasztor"
  ]
  node [
    id 445
    label "kongregacja"
  ]
  node [
    id 446
    label "consumption"
  ]
  node [
    id 447
    label "&#380;ycie"
  ]
  node [
    id 448
    label "survival"
  ]
  node [
    id 449
    label "pozostanie"
  ]
  node [
    id 450
    label "experience"
  ]
  node [
    id 451
    label "wyniesienie"
  ]
  node [
    id 452
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 453
    label "odej&#347;cie"
  ]
  node [
    id 454
    label "pozabieranie"
  ]
  node [
    id 455
    label "pozbycie_si&#281;"
  ]
  node [
    id 456
    label "pousuwanie"
  ]
  node [
    id 457
    label "przesuni&#281;cie"
  ]
  node [
    id 458
    label "znikni&#281;cie"
  ]
  node [
    id 459
    label "abstraction"
  ]
  node [
    id 460
    label "wyrugowanie"
  ]
  node [
    id 461
    label "nak&#322;onienie"
  ]
  node [
    id 462
    label "przest&#281;pstwo"
  ]
  node [
    id 463
    label "catch"
  ]
  node [
    id 464
    label "poruszenie"
  ]
  node [
    id 465
    label "z&#322;apanie"
  ]
  node [
    id 466
    label "rozerwanie"
  ]
  node [
    id 467
    label "opanowanie"
  ]
  node [
    id 468
    label "hijack"
  ]
  node [
    id 469
    label "powyrywanie"
  ]
  node [
    id 470
    label "zabranie"
  ]
  node [
    id 471
    label "bust"
  ]
  node [
    id 472
    label "kidnapping"
  ]
  node [
    id 473
    label "dostosowanie"
  ]
  node [
    id 474
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 475
    label "rozpowszechnienie"
  ]
  node [
    id 476
    label "skopiowanie"
  ]
  node [
    id 477
    label "transfer"
  ]
  node [
    id 478
    label "move"
  ]
  node [
    id 479
    label "pocisk"
  ]
  node [
    id 480
    label "assignment"
  ]
  node [
    id 481
    label "przemieszczenie"
  ]
  node [
    id 482
    label "przelecenie"
  ]
  node [
    id 483
    label "mechanizm_obronny"
  ]
  node [
    id 484
    label "zmienienie"
  ]
  node [
    id 485
    label "umieszczenie"
  ]
  node [
    id 486
    label "strzelenie"
  ]
  node [
    id 487
    label "przesadzenie"
  ]
  node [
    id 488
    label "poprzesuwanie"
  ]
  node [
    id 489
    label "retraction"
  ]
  node [
    id 490
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 491
    label "zerwanie"
  ]
  node [
    id 492
    label "konsekwencja"
  ]
  node [
    id 493
    label "znoszenie"
  ]
  node [
    id 494
    label "nap&#322;ywanie"
  ]
  node [
    id 495
    label "communication"
  ]
  node [
    id 496
    label "signal"
  ]
  node [
    id 497
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 498
    label "znie&#347;&#263;"
  ]
  node [
    id 499
    label "znosi&#263;"
  ]
  node [
    id 500
    label "zarys"
  ]
  node [
    id 501
    label "informacja"
  ]
  node [
    id 502
    label "depesza_emska"
  ]
  node [
    id 503
    label "ball"
  ]
  node [
    id 504
    label "kszta&#322;t"
  ]
  node [
    id 505
    label "nabia&#322;"
  ]
  node [
    id 506
    label "pisanka"
  ]
  node [
    id 507
    label "jajo"
  ]
  node [
    id 508
    label "bia&#322;ko"
  ]
  node [
    id 509
    label "owoskop"
  ]
  node [
    id 510
    label "wyt&#322;aczanka"
  ]
  node [
    id 511
    label "rozbijarka"
  ]
  node [
    id 512
    label "ryboflawina"
  ]
  node [
    id 513
    label "produkt"
  ]
  node [
    id 514
    label "skorupka"
  ]
  node [
    id 515
    label "&#380;&#243;&#322;tko"
  ]
  node [
    id 516
    label "porannie"
  ]
  node [
    id 517
    label "zaranny"
  ]
  node [
    id 518
    label "poszkodowany"
  ]
  node [
    id 519
    label "specjalny"
  ]
  node [
    id 520
    label "przeciwnik"
  ]
  node [
    id 521
    label "zwolennik"
  ]
  node [
    id 522
    label "czarnosk&#243;ry"
  ]
  node [
    id 523
    label "z&#322;&#261;czenie"
  ]
  node [
    id 524
    label "wp&#322;acenie"
  ]
  node [
    id 525
    label "u&#322;o&#380;enie"
  ]
  node [
    id 526
    label "nietolerancja"
  ]
  node [
    id 527
    label "apartheid"
  ]
  node [
    id 528
    label "discrimination"
  ]
  node [
    id 529
    label "alergen"
  ]
  node [
    id 530
    label "odczulenie"
  ]
  node [
    id 531
    label "uczulanie"
  ]
  node [
    id 532
    label "rumie&#324;_wielopostaciowy"
  ]
  node [
    id 533
    label "odczula&#263;"
  ]
  node [
    id 534
    label "odczuli&#263;"
  ]
  node [
    id 535
    label "uczulenie"
  ]
  node [
    id 536
    label "choroba_somatyczna"
  ]
  node [
    id 537
    label "uczuli&#263;"
  ]
  node [
    id 538
    label "schorzenie"
  ]
  node [
    id 539
    label "sensybilizacja"
  ]
  node [
    id 540
    label "intolerance"
  ]
  node [
    id 541
    label "krzywda"
  ]
  node [
    id 542
    label "uczula&#263;"
  ]
  node [
    id 543
    label "odczulanie"
  ]
  node [
    id 544
    label "kaszel"
  ]
  node [
    id 545
    label "koncepcja"
  ]
  node [
    id 546
    label "doktryna"
  ]
  node [
    id 547
    label "polityka"
  ]
  node [
    id 548
    label "doros&#322;y"
  ]
  node [
    id 549
    label "&#380;ona"
  ]
  node [
    id 550
    label "cz&#322;owiek"
  ]
  node [
    id 551
    label "samica"
  ]
  node [
    id 552
    label "uleganie"
  ]
  node [
    id 553
    label "ulec"
  ]
  node [
    id 554
    label "m&#281;&#380;yna"
  ]
  node [
    id 555
    label "partnerka"
  ]
  node [
    id 556
    label "ulegni&#281;cie"
  ]
  node [
    id 557
    label "pa&#324;stwo"
  ]
  node [
    id 558
    label "&#322;ono"
  ]
  node [
    id 559
    label "menopauza"
  ]
  node [
    id 560
    label "przekwitanie"
  ]
  node [
    id 561
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 562
    label "babka"
  ]
  node [
    id 563
    label "ulega&#263;"
  ]
  node [
    id 564
    label "ludzko&#347;&#263;"
  ]
  node [
    id 565
    label "asymilowanie"
  ]
  node [
    id 566
    label "wapniak"
  ]
  node [
    id 567
    label "asymilowa&#263;"
  ]
  node [
    id 568
    label "os&#322;abia&#263;"
  ]
  node [
    id 569
    label "posta&#263;"
  ]
  node [
    id 570
    label "hominid"
  ]
  node [
    id 571
    label "podw&#322;adny"
  ]
  node [
    id 572
    label "os&#322;abianie"
  ]
  node [
    id 573
    label "g&#322;owa"
  ]
  node [
    id 574
    label "figura"
  ]
  node [
    id 575
    label "portrecista"
  ]
  node [
    id 576
    label "dwun&#243;g"
  ]
  node [
    id 577
    label "profanum"
  ]
  node [
    id 578
    label "mikrokosmos"
  ]
  node [
    id 579
    label "nasada"
  ]
  node [
    id 580
    label "duch"
  ]
  node [
    id 581
    label "antropochoria"
  ]
  node [
    id 582
    label "osoba"
  ]
  node [
    id 583
    label "wz&#243;r"
  ]
  node [
    id 584
    label "senior"
  ]
  node [
    id 585
    label "oddzia&#322;ywanie"
  ]
  node [
    id 586
    label "Adam"
  ]
  node [
    id 587
    label "homo_sapiens"
  ]
  node [
    id 588
    label "polifag"
  ]
  node [
    id 589
    label "wydoro&#347;lenie"
  ]
  node [
    id 590
    label "du&#380;y"
  ]
  node [
    id 591
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 592
    label "doro&#347;lenie"
  ]
  node [
    id 593
    label "&#378;ra&#322;y"
  ]
  node [
    id 594
    label "doro&#347;le"
  ]
  node [
    id 595
    label "dojrzale"
  ]
  node [
    id 596
    label "dojrza&#322;y"
  ]
  node [
    id 597
    label "m&#261;dry"
  ]
  node [
    id 598
    label "doletni"
  ]
  node [
    id 599
    label "aktorka"
  ]
  node [
    id 600
    label "partner"
  ]
  node [
    id 601
    label "kobita"
  ]
  node [
    id 602
    label "ma&#322;&#380;onek"
  ]
  node [
    id 603
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 604
    label "&#347;lubna"
  ]
  node [
    id 605
    label "panna_m&#322;oda"
  ]
  node [
    id 606
    label "zezwalanie"
  ]
  node [
    id 607
    label "return"
  ]
  node [
    id 608
    label "zaliczanie"
  ]
  node [
    id 609
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 610
    label "poddawanie"
  ]
  node [
    id 611
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 612
    label "burst"
  ]
  node [
    id 613
    label "przywo&#322;anie"
  ]
  node [
    id 614
    label "naginanie_si&#281;"
  ]
  node [
    id 615
    label "poddawanie_si&#281;"
  ]
  node [
    id 616
    label "stawanie_si&#281;"
  ]
  node [
    id 617
    label "przywo&#322;a&#263;"
  ]
  node [
    id 618
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 619
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 620
    label "poddawa&#263;"
  ]
  node [
    id 621
    label "postpone"
  ]
  node [
    id 622
    label "render"
  ]
  node [
    id 623
    label "zezwala&#263;"
  ]
  node [
    id 624
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 625
    label "subject"
  ]
  node [
    id 626
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 627
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 628
    label "poddanie"
  ]
  node [
    id 629
    label "nagi&#281;cie_si&#281;"
  ]
  node [
    id 630
    label "pozwolenie"
  ]
  node [
    id 631
    label "subjugation"
  ]
  node [
    id 632
    label "kwitnienie"
  ]
  node [
    id 633
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 634
    label "przemijanie"
  ]
  node [
    id 635
    label "przestawanie"
  ]
  node [
    id 636
    label "starzenie_si&#281;"
  ]
  node [
    id 637
    label "menopause"
  ]
  node [
    id 638
    label "obumieranie"
  ]
  node [
    id 639
    label "dojrzewanie"
  ]
  node [
    id 640
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 641
    label "sta&#263;_si&#281;"
  ]
  node [
    id 642
    label "fall"
  ]
  node [
    id 643
    label "give"
  ]
  node [
    id 644
    label "pozwoli&#263;"
  ]
  node [
    id 645
    label "podda&#263;"
  ]
  node [
    id 646
    label "put_in"
  ]
  node [
    id 647
    label "podda&#263;_si&#281;"
  ]
  node [
    id 648
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 649
    label "Katar"
  ]
  node [
    id 650
    label "Libia"
  ]
  node [
    id 651
    label "Gwatemala"
  ]
  node [
    id 652
    label "Ekwador"
  ]
  node [
    id 653
    label "Afganistan"
  ]
  node [
    id 654
    label "Tad&#380;ykistan"
  ]
  node [
    id 655
    label "Bhutan"
  ]
  node [
    id 656
    label "Argentyna"
  ]
  node [
    id 657
    label "D&#380;ibuti"
  ]
  node [
    id 658
    label "Wenezuela"
  ]
  node [
    id 659
    label "Gabon"
  ]
  node [
    id 660
    label "Ukraina"
  ]
  node [
    id 661
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 662
    label "Rwanda"
  ]
  node [
    id 663
    label "Liechtenstein"
  ]
  node [
    id 664
    label "organizacja"
  ]
  node [
    id 665
    label "Sri_Lanka"
  ]
  node [
    id 666
    label "Madagaskar"
  ]
  node [
    id 667
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 668
    label "Kongo"
  ]
  node [
    id 669
    label "Tonga"
  ]
  node [
    id 670
    label "Bangladesz"
  ]
  node [
    id 671
    label "Kanada"
  ]
  node [
    id 672
    label "Wehrlen"
  ]
  node [
    id 673
    label "Algieria"
  ]
  node [
    id 674
    label "Uganda"
  ]
  node [
    id 675
    label "Surinam"
  ]
  node [
    id 676
    label "Sahara_Zachodnia"
  ]
  node [
    id 677
    label "Chile"
  ]
  node [
    id 678
    label "W&#281;gry"
  ]
  node [
    id 679
    label "Birma"
  ]
  node [
    id 680
    label "Kazachstan"
  ]
  node [
    id 681
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 682
    label "Armenia"
  ]
  node [
    id 683
    label "Tuwalu"
  ]
  node [
    id 684
    label "Timor_Wschodni"
  ]
  node [
    id 685
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 686
    label "Izrael"
  ]
  node [
    id 687
    label "Estonia"
  ]
  node [
    id 688
    label "Komory"
  ]
  node [
    id 689
    label "Kamerun"
  ]
  node [
    id 690
    label "Haiti"
  ]
  node [
    id 691
    label "Belize"
  ]
  node [
    id 692
    label "Sierra_Leone"
  ]
  node [
    id 693
    label "Luksemburg"
  ]
  node [
    id 694
    label "USA"
  ]
  node [
    id 695
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 696
    label "Barbados"
  ]
  node [
    id 697
    label "San_Marino"
  ]
  node [
    id 698
    label "Bu&#322;garia"
  ]
  node [
    id 699
    label "Indonezja"
  ]
  node [
    id 700
    label "Wietnam"
  ]
  node [
    id 701
    label "Malawi"
  ]
  node [
    id 702
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 703
    label "Francja"
  ]
  node [
    id 704
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 705
    label "partia"
  ]
  node [
    id 706
    label "Zambia"
  ]
  node [
    id 707
    label "Angola"
  ]
  node [
    id 708
    label "Grenada"
  ]
  node [
    id 709
    label "Nepal"
  ]
  node [
    id 710
    label "Panama"
  ]
  node [
    id 711
    label "Rumunia"
  ]
  node [
    id 712
    label "Czarnog&#243;ra"
  ]
  node [
    id 713
    label "Malediwy"
  ]
  node [
    id 714
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 715
    label "S&#322;owacja"
  ]
  node [
    id 716
    label "Egipt"
  ]
  node [
    id 717
    label "zwrot"
  ]
  node [
    id 718
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 719
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 720
    label "Mozambik"
  ]
  node [
    id 721
    label "Kolumbia"
  ]
  node [
    id 722
    label "Laos"
  ]
  node [
    id 723
    label "Burundi"
  ]
  node [
    id 724
    label "Suazi"
  ]
  node [
    id 725
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 726
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 727
    label "Czechy"
  ]
  node [
    id 728
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 729
    label "Wyspy_Marshalla"
  ]
  node [
    id 730
    label "Dominika"
  ]
  node [
    id 731
    label "Trynidad_i_Tobago"
  ]
  node [
    id 732
    label "Syria"
  ]
  node [
    id 733
    label "Palau"
  ]
  node [
    id 734
    label "Gwinea_Bissau"
  ]
  node [
    id 735
    label "Liberia"
  ]
  node [
    id 736
    label "Jamajka"
  ]
  node [
    id 737
    label "Zimbabwe"
  ]
  node [
    id 738
    label "Polska"
  ]
  node [
    id 739
    label "Dominikana"
  ]
  node [
    id 740
    label "Senegal"
  ]
  node [
    id 741
    label "Togo"
  ]
  node [
    id 742
    label "Gujana"
  ]
  node [
    id 743
    label "Gruzja"
  ]
  node [
    id 744
    label "Albania"
  ]
  node [
    id 745
    label "Zair"
  ]
  node [
    id 746
    label "Meksyk"
  ]
  node [
    id 747
    label "Macedonia"
  ]
  node [
    id 748
    label "Chorwacja"
  ]
  node [
    id 749
    label "Kambod&#380;a"
  ]
  node [
    id 750
    label "Monako"
  ]
  node [
    id 751
    label "Mauritius"
  ]
  node [
    id 752
    label "Gwinea"
  ]
  node [
    id 753
    label "Mali"
  ]
  node [
    id 754
    label "Nigeria"
  ]
  node [
    id 755
    label "Kostaryka"
  ]
  node [
    id 756
    label "Hanower"
  ]
  node [
    id 757
    label "Paragwaj"
  ]
  node [
    id 758
    label "W&#322;ochy"
  ]
  node [
    id 759
    label "Seszele"
  ]
  node [
    id 760
    label "Wyspy_Salomona"
  ]
  node [
    id 761
    label "Hiszpania"
  ]
  node [
    id 762
    label "Boliwia"
  ]
  node [
    id 763
    label "Kirgistan"
  ]
  node [
    id 764
    label "Irlandia"
  ]
  node [
    id 765
    label "Czad"
  ]
  node [
    id 766
    label "Irak"
  ]
  node [
    id 767
    label "Lesoto"
  ]
  node [
    id 768
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 769
    label "Malta"
  ]
  node [
    id 770
    label "Andora"
  ]
  node [
    id 771
    label "Chiny"
  ]
  node [
    id 772
    label "Filipiny"
  ]
  node [
    id 773
    label "Antarktis"
  ]
  node [
    id 774
    label "Niemcy"
  ]
  node [
    id 775
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 776
    label "Pakistan"
  ]
  node [
    id 777
    label "terytorium"
  ]
  node [
    id 778
    label "Nikaragua"
  ]
  node [
    id 779
    label "Brazylia"
  ]
  node [
    id 780
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 781
    label "Maroko"
  ]
  node [
    id 782
    label "Portugalia"
  ]
  node [
    id 783
    label "Niger"
  ]
  node [
    id 784
    label "Kenia"
  ]
  node [
    id 785
    label "Botswana"
  ]
  node [
    id 786
    label "Fid&#380;i"
  ]
  node [
    id 787
    label "Tunezja"
  ]
  node [
    id 788
    label "Australia"
  ]
  node [
    id 789
    label "Tajlandia"
  ]
  node [
    id 790
    label "Burkina_Faso"
  ]
  node [
    id 791
    label "interior"
  ]
  node [
    id 792
    label "Tanzania"
  ]
  node [
    id 793
    label "Benin"
  ]
  node [
    id 794
    label "Indie"
  ]
  node [
    id 795
    label "&#321;otwa"
  ]
  node [
    id 796
    label "Kiribati"
  ]
  node [
    id 797
    label "Antigua_i_Barbuda"
  ]
  node [
    id 798
    label "Rodezja"
  ]
  node [
    id 799
    label "Cypr"
  ]
  node [
    id 800
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 801
    label "Peru"
  ]
  node [
    id 802
    label "Austria"
  ]
  node [
    id 803
    label "Urugwaj"
  ]
  node [
    id 804
    label "Jordania"
  ]
  node [
    id 805
    label "Grecja"
  ]
  node [
    id 806
    label "Azerbejd&#380;an"
  ]
  node [
    id 807
    label "Turcja"
  ]
  node [
    id 808
    label "Samoa"
  ]
  node [
    id 809
    label "Sudan"
  ]
  node [
    id 810
    label "Oman"
  ]
  node [
    id 811
    label "ziemia"
  ]
  node [
    id 812
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 813
    label "Uzbekistan"
  ]
  node [
    id 814
    label "Portoryko"
  ]
  node [
    id 815
    label "Honduras"
  ]
  node [
    id 816
    label "Mongolia"
  ]
  node [
    id 817
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 818
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 819
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 820
    label "Serbia"
  ]
  node [
    id 821
    label "Tajwan"
  ]
  node [
    id 822
    label "Wielka_Brytania"
  ]
  node [
    id 823
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 824
    label "Liban"
  ]
  node [
    id 825
    label "Japonia"
  ]
  node [
    id 826
    label "Ghana"
  ]
  node [
    id 827
    label "Belgia"
  ]
  node [
    id 828
    label "Bahrajn"
  ]
  node [
    id 829
    label "Mikronezja"
  ]
  node [
    id 830
    label "Etiopia"
  ]
  node [
    id 831
    label "Kuwejt"
  ]
  node [
    id 832
    label "Bahamy"
  ]
  node [
    id 833
    label "Rosja"
  ]
  node [
    id 834
    label "Mo&#322;dawia"
  ]
  node [
    id 835
    label "Litwa"
  ]
  node [
    id 836
    label "S&#322;owenia"
  ]
  node [
    id 837
    label "Szwajcaria"
  ]
  node [
    id 838
    label "Erytrea"
  ]
  node [
    id 839
    label "Arabia_Saudyjska"
  ]
  node [
    id 840
    label "Kuba"
  ]
  node [
    id 841
    label "granica_pa&#324;stwa"
  ]
  node [
    id 842
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 843
    label "Malezja"
  ]
  node [
    id 844
    label "Korea"
  ]
  node [
    id 845
    label "Jemen"
  ]
  node [
    id 846
    label "Nowa_Zelandia"
  ]
  node [
    id 847
    label "Namibia"
  ]
  node [
    id 848
    label "Nauru"
  ]
  node [
    id 849
    label "holoarktyka"
  ]
  node [
    id 850
    label "Brunei"
  ]
  node [
    id 851
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 852
    label "Khitai"
  ]
  node [
    id 853
    label "Mauretania"
  ]
  node [
    id 854
    label "Iran"
  ]
  node [
    id 855
    label "Gambia"
  ]
  node [
    id 856
    label "Somalia"
  ]
  node [
    id 857
    label "Holandia"
  ]
  node [
    id 858
    label "Turkmenistan"
  ]
  node [
    id 859
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 860
    label "Salwador"
  ]
  node [
    id 861
    label "klatka_piersiowa"
  ]
  node [
    id 862
    label "penis"
  ]
  node [
    id 863
    label "wzg&#243;rek_&#322;onowy"
  ]
  node [
    id 864
    label "brzuch"
  ]
  node [
    id 865
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 866
    label "podbrzusze"
  ]
  node [
    id 867
    label "przyroda"
  ]
  node [
    id 868
    label "l&#281;d&#378;wie"
  ]
  node [
    id 869
    label "wn&#281;trze"
  ]
  node [
    id 870
    label "cia&#322;o"
  ]
  node [
    id 871
    label "dziedzina"
  ]
  node [
    id 872
    label "powierzchnia"
  ]
  node [
    id 873
    label "macica"
  ]
  node [
    id 874
    label "pochwa"
  ]
  node [
    id 875
    label "przodkini"
  ]
  node [
    id 876
    label "baba"
  ]
  node [
    id 877
    label "babulinka"
  ]
  node [
    id 878
    label "ciasto"
  ]
  node [
    id 879
    label "ro&#347;lina_zielna"
  ]
  node [
    id 880
    label "babkowate"
  ]
  node [
    id 881
    label "po&#322;o&#380;na"
  ]
  node [
    id 882
    label "dziadkowie"
  ]
  node [
    id 883
    label "ryba"
  ]
  node [
    id 884
    label "ko&#378;larz_babka"
  ]
  node [
    id 885
    label "moneta"
  ]
  node [
    id 886
    label "plantain"
  ]
  node [
    id 887
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 888
    label "samka"
  ]
  node [
    id 889
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 890
    label "drogi_rodne"
  ]
  node [
    id 891
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 892
    label "zwierz&#281;"
  ]
  node [
    id 893
    label "female"
  ]
  node [
    id 894
    label "odr&#281;bny"
  ]
  node [
    id 895
    label "po_m&#281;sku"
  ]
  node [
    id 896
    label "zdecydowany"
  ]
  node [
    id 897
    label "stosowny"
  ]
  node [
    id 898
    label "toaleta"
  ]
  node [
    id 899
    label "typowy"
  ]
  node [
    id 900
    label "m&#281;sko"
  ]
  node [
    id 901
    label "podobny"
  ]
  node [
    id 902
    label "prawdziwy"
  ]
  node [
    id 903
    label "typowo"
  ]
  node [
    id 904
    label "stosownie"
  ]
  node [
    id 905
    label "zdecydowanie"
  ]
  node [
    id 906
    label "odr&#281;bnie"
  ]
  node [
    id 907
    label "prawdziwie"
  ]
  node [
    id 908
    label "nale&#380;yty"
  ]
  node [
    id 909
    label "ust&#281;p"
  ]
  node [
    id 910
    label "dressing"
  ]
  node [
    id 911
    label "kibel"
  ]
  node [
    id 912
    label "klozetka"
  ]
  node [
    id 913
    label "prewet"
  ]
  node [
    id 914
    label "sypialnia"
  ]
  node [
    id 915
    label "kreacja"
  ]
  node [
    id 916
    label "mycie"
  ]
  node [
    id 917
    label "kosmetyka"
  ]
  node [
    id 918
    label "pomieszczenie"
  ]
  node [
    id 919
    label "podw&#322;o&#347;nik"
  ]
  node [
    id 920
    label "mebel"
  ]
  node [
    id 921
    label "sracz"
  ]
  node [
    id 922
    label "&#380;ywny"
  ]
  node [
    id 923
    label "szczery"
  ]
  node [
    id 924
    label "naturalny"
  ]
  node [
    id 925
    label "naprawd&#281;"
  ]
  node [
    id 926
    label "realnie"
  ]
  node [
    id 927
    label "zgodny"
  ]
  node [
    id 928
    label "pewny"
  ]
  node [
    id 929
    label "zauwa&#380;alny"
  ]
  node [
    id 930
    label "gotowy"
  ]
  node [
    id 931
    label "zwyczajny"
  ]
  node [
    id 932
    label "cz&#281;sty"
  ]
  node [
    id 933
    label "zwyk&#322;y"
  ]
  node [
    id 934
    label "przypominanie"
  ]
  node [
    id 935
    label "podobnie"
  ]
  node [
    id 936
    label "upodabnianie_si&#281;"
  ]
  node [
    id 937
    label "upodobnienie"
  ]
  node [
    id 938
    label "drugi"
  ]
  node [
    id 939
    label "taki"
  ]
  node [
    id 940
    label "charakterystyczny"
  ]
  node [
    id 941
    label "upodobnienie_si&#281;"
  ]
  node [
    id 942
    label "zasymilowanie"
  ]
  node [
    id 943
    label "wyodr&#281;bnienie_si&#281;"
  ]
  node [
    id 944
    label "kolejny"
  ]
  node [
    id 945
    label "wydzielenie"
  ]
  node [
    id 946
    label "osobno"
  ]
  node [
    id 947
    label "inszy"
  ]
  node [
    id 948
    label "wyodr&#281;bnianie"
  ]
  node [
    id 949
    label "wyodr&#281;bnianie_si&#281;"
  ]
  node [
    id 950
    label "osobny"
  ]
  node [
    id 951
    label "przewaga"
  ]
  node [
    id 952
    label "prym"
  ]
  node [
    id 953
    label "znaczenie"
  ]
  node [
    id 954
    label "laterality"
  ]
  node [
    id 955
    label "odk&#322;adanie"
  ]
  node [
    id 956
    label "condition"
  ]
  node [
    id 957
    label "liczenie"
  ]
  node [
    id 958
    label "stawianie"
  ]
  node [
    id 959
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 960
    label "assay"
  ]
  node [
    id 961
    label "wskazywanie"
  ]
  node [
    id 962
    label "wyraz"
  ]
  node [
    id 963
    label "gravity"
  ]
  node [
    id 964
    label "weight"
  ]
  node [
    id 965
    label "command"
  ]
  node [
    id 966
    label "odgrywanie_roli"
  ]
  node [
    id 967
    label "istota"
  ]
  node [
    id 968
    label "okre&#347;lanie"
  ]
  node [
    id 969
    label "kto&#347;"
  ]
  node [
    id 970
    label "r&#243;&#380;nica"
  ]
  node [
    id 971
    label "atakowa&#263;"
  ]
  node [
    id 972
    label "advantage"
  ]
  node [
    id 973
    label "atakowanie"
  ]
  node [
    id 974
    label "przemoc"
  ]
  node [
    id 975
    label "przek&#322;adanie"
  ]
  node [
    id 976
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 977
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 978
    label "predilection"
  ]
  node [
    id 979
    label "przek&#322;ada&#263;"
  ]
  node [
    id 980
    label "preponderencja"
  ]
  node [
    id 981
    label "supremacja"
  ]
  node [
    id 982
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 983
    label "krzew"
  ]
  node [
    id 984
    label "delfinidyna"
  ]
  node [
    id 985
    label "pi&#380;maczkowate"
  ]
  node [
    id 986
    label "ki&#347;&#263;"
  ]
  node [
    id 987
    label "hy&#263;ka"
  ]
  node [
    id 988
    label "pestkowiec"
  ]
  node [
    id 989
    label "kwiat"
  ]
  node [
    id 990
    label "ro&#347;lina"
  ]
  node [
    id 991
    label "owoc"
  ]
  node [
    id 992
    label "oliwkowate"
  ]
  node [
    id 993
    label "lilac"
  ]
  node [
    id 994
    label "flakon"
  ]
  node [
    id 995
    label "przykoronek"
  ]
  node [
    id 996
    label "kielich"
  ]
  node [
    id 997
    label "dno_kwiatowe"
  ]
  node [
    id 998
    label "organ_ro&#347;linny"
  ]
  node [
    id 999
    label "ogon"
  ]
  node [
    id 1000
    label "warga"
  ]
  node [
    id 1001
    label "korona"
  ]
  node [
    id 1002
    label "rurka"
  ]
  node [
    id 1003
    label "ozdoba"
  ]
  node [
    id 1004
    label "kostka"
  ]
  node [
    id 1005
    label "kita"
  ]
  node [
    id 1006
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 1007
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 1008
    label "d&#322;o&#324;"
  ]
  node [
    id 1009
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 1010
    label "powerball"
  ]
  node [
    id 1011
    label "&#380;ubr"
  ]
  node [
    id 1012
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 1013
    label "p&#281;k"
  ]
  node [
    id 1014
    label "r&#281;ka"
  ]
  node [
    id 1015
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 1016
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 1017
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 1018
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 1019
    label "&#322;yko"
  ]
  node [
    id 1020
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 1021
    label "karczowa&#263;"
  ]
  node [
    id 1022
    label "wykarczowanie"
  ]
  node [
    id 1023
    label "skupina"
  ]
  node [
    id 1024
    label "wykarczowa&#263;"
  ]
  node [
    id 1025
    label "karczowanie"
  ]
  node [
    id 1026
    label "fanerofit"
  ]
  node [
    id 1027
    label "zbiorowisko"
  ]
  node [
    id 1028
    label "ro&#347;liny"
  ]
  node [
    id 1029
    label "p&#281;d"
  ]
  node [
    id 1030
    label "wegetowanie"
  ]
  node [
    id 1031
    label "zadziorek"
  ]
  node [
    id 1032
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1033
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1034
    label "do&#322;owa&#263;"
  ]
  node [
    id 1035
    label "wegetacja"
  ]
  node [
    id 1036
    label "strzyc"
  ]
  node [
    id 1037
    label "w&#322;&#243;kno"
  ]
  node [
    id 1038
    label "g&#322;uszenie"
  ]
  node [
    id 1039
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1040
    label "fitotron"
  ]
  node [
    id 1041
    label "bulwka"
  ]
  node [
    id 1042
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1043
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1044
    label "epiderma"
  ]
  node [
    id 1045
    label "gumoza"
  ]
  node [
    id 1046
    label "strzy&#380;enie"
  ]
  node [
    id 1047
    label "wypotnik"
  ]
  node [
    id 1048
    label "flawonoid"
  ]
  node [
    id 1049
    label "wyro&#347;le"
  ]
  node [
    id 1050
    label "do&#322;owanie"
  ]
  node [
    id 1051
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1052
    label "pora&#380;a&#263;"
  ]
  node [
    id 1053
    label "fitocenoza"
  ]
  node [
    id 1054
    label "hodowla"
  ]
  node [
    id 1055
    label "fotoautotrof"
  ]
  node [
    id 1056
    label "nieuleczalnie_chory"
  ]
  node [
    id 1057
    label "wegetowa&#263;"
  ]
  node [
    id 1058
    label "pochewka"
  ]
  node [
    id 1059
    label "sok"
  ]
  node [
    id 1060
    label "system_korzeniowy"
  ]
  node [
    id 1061
    label "zawi&#261;zek"
  ]
  node [
    id 1062
    label "mi&#261;&#380;sz"
  ]
  node [
    id 1063
    label "frukt"
  ]
  node [
    id 1064
    label "drylowanie"
  ]
  node [
    id 1065
    label "owocnia"
  ]
  node [
    id 1066
    label "fruktoza"
  ]
  node [
    id 1067
    label "obiekt"
  ]
  node [
    id 1068
    label "gniazdo_nasienne"
  ]
  node [
    id 1069
    label "glukoza"
  ]
  node [
    id 1070
    label "pestka"
  ]
  node [
    id 1071
    label "antocyjanidyn"
  ]
  node [
    id 1072
    label "szczeciowce"
  ]
  node [
    id 1073
    label "jasnotowce"
  ]
  node [
    id 1074
    label "Oleaceae"
  ]
  node [
    id 1075
    label "wielkopolski"
  ]
  node [
    id 1076
    label "bez_czarny"
  ]
  node [
    id 1077
    label "r&#243;&#380;ny"
  ]
  node [
    id 1078
    label "inaczej"
  ]
  node [
    id 1079
    label "nast&#281;pnie"
  ]
  node [
    id 1080
    label "nastopny"
  ]
  node [
    id 1081
    label "kolejno"
  ]
  node [
    id 1082
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1083
    label "jaki&#347;"
  ]
  node [
    id 1084
    label "r&#243;&#380;nie"
  ]
  node [
    id 1085
    label "niestandardowo"
  ]
  node [
    id 1086
    label "individually"
  ]
  node [
    id 1087
    label "udzielnie"
  ]
  node [
    id 1088
    label "osobnie"
  ]
  node [
    id 1089
    label "jednostka_systematyczna"
  ]
  node [
    id 1090
    label "poznanie"
  ]
  node [
    id 1091
    label "leksem"
  ]
  node [
    id 1092
    label "dzie&#322;o"
  ]
  node [
    id 1093
    label "blaszka"
  ]
  node [
    id 1094
    label "poj&#281;cie"
  ]
  node [
    id 1095
    label "kantyzm"
  ]
  node [
    id 1096
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1097
    label "do&#322;ek"
  ]
  node [
    id 1098
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1099
    label "gwiazda"
  ]
  node [
    id 1100
    label "formality"
  ]
  node [
    id 1101
    label "struktura"
  ]
  node [
    id 1102
    label "wygl&#261;d"
  ]
  node [
    id 1103
    label "mode"
  ]
  node [
    id 1104
    label "morfem"
  ]
  node [
    id 1105
    label "rdze&#324;"
  ]
  node [
    id 1106
    label "ornamentyka"
  ]
  node [
    id 1107
    label "pasmo"
  ]
  node [
    id 1108
    label "zwyczaj"
  ]
  node [
    id 1109
    label "punkt_widzenia"
  ]
  node [
    id 1110
    label "naczynie"
  ]
  node [
    id 1111
    label "p&#322;at"
  ]
  node [
    id 1112
    label "maszyna_drukarska"
  ]
  node [
    id 1113
    label "style"
  ]
  node [
    id 1114
    label "linearno&#347;&#263;"
  ]
  node [
    id 1115
    label "formacja"
  ]
  node [
    id 1116
    label "spirala"
  ]
  node [
    id 1117
    label "dyspozycja"
  ]
  node [
    id 1118
    label "odmiana"
  ]
  node [
    id 1119
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1120
    label "October"
  ]
  node [
    id 1121
    label "creation"
  ]
  node [
    id 1122
    label "p&#281;tla"
  ]
  node [
    id 1123
    label "arystotelizm"
  ]
  node [
    id 1124
    label "szablon"
  ]
  node [
    id 1125
    label "miniatura"
  ]
  node [
    id 1126
    label "acquaintance"
  ]
  node [
    id 1127
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1128
    label "nauczenie_si&#281;"
  ]
  node [
    id 1129
    label "poczucie"
  ]
  node [
    id 1130
    label "knowing"
  ]
  node [
    id 1131
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1132
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1133
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1134
    label "inclusion"
  ]
  node [
    id 1135
    label "zrozumienie"
  ]
  node [
    id 1136
    label "zawarcie"
  ]
  node [
    id 1137
    label "designation"
  ]
  node [
    id 1138
    label "sensing"
  ]
  node [
    id 1139
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 1140
    label "zapoznanie"
  ]
  node [
    id 1141
    label "znajomy"
  ]
  node [
    id 1142
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1143
    label "obrazowanie"
  ]
  node [
    id 1144
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1145
    label "dorobek"
  ]
  node [
    id 1146
    label "tre&#347;&#263;"
  ]
  node [
    id 1147
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 1148
    label "retrospektywa"
  ]
  node [
    id 1149
    label "works"
  ]
  node [
    id 1150
    label "tekst"
  ]
  node [
    id 1151
    label "tetralogia"
  ]
  node [
    id 1152
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 1153
    label "praca"
  ]
  node [
    id 1154
    label "mutant"
  ]
  node [
    id 1155
    label "rewizja"
  ]
  node [
    id 1156
    label "gramatyka"
  ]
  node [
    id 1157
    label "typ"
  ]
  node [
    id 1158
    label "paradygmat"
  ]
  node [
    id 1159
    label "change"
  ]
  node [
    id 1160
    label "podgatunek"
  ]
  node [
    id 1161
    label "ferment"
  ]
  node [
    id 1162
    label "rasa"
  ]
  node [
    id 1163
    label "zjawisko"
  ]
  node [
    id 1164
    label "skumanie"
  ]
  node [
    id 1165
    label "orientacja"
  ]
  node [
    id 1166
    label "zorientowanie"
  ]
  node [
    id 1167
    label "teoria"
  ]
  node [
    id 1168
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1169
    label "clasp"
  ]
  node [
    id 1170
    label "przem&#243;wienie"
  ]
  node [
    id 1171
    label "idealizm"
  ]
  node [
    id 1172
    label "szko&#322;a"
  ]
  node [
    id 1173
    label "imperatyw_kategoryczny"
  ]
  node [
    id 1174
    label "przedmiot"
  ]
  node [
    id 1175
    label "li&#347;&#263;"
  ]
  node [
    id 1176
    label "tw&#243;r"
  ]
  node [
    id 1177
    label "odznaczenie"
  ]
  node [
    id 1178
    label "kapelusz"
  ]
  node [
    id 1179
    label "Arktur"
  ]
  node [
    id 1180
    label "Gwiazda_Polarna"
  ]
  node [
    id 1181
    label "agregatka"
  ]
  node [
    id 1182
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 1183
    label "gromada"
  ]
  node [
    id 1184
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 1185
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1186
    label "Nibiru"
  ]
  node [
    id 1187
    label "konstelacja"
  ]
  node [
    id 1188
    label "ornament"
  ]
  node [
    id 1189
    label "delta_Scuti"
  ]
  node [
    id 1190
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1191
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1192
    label "s&#322;awa"
  ]
  node [
    id 1193
    label "promie&#324;"
  ]
  node [
    id 1194
    label "star"
  ]
  node [
    id 1195
    label "gwiazdosz"
  ]
  node [
    id 1196
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 1197
    label "asocjacja_gwiazd"
  ]
  node [
    id 1198
    label "supergrupa"
  ]
  node [
    id 1199
    label "sid&#322;a"
  ]
  node [
    id 1200
    label "ko&#322;o"
  ]
  node [
    id 1201
    label "p&#281;tlica"
  ]
  node [
    id 1202
    label "hank"
  ]
  node [
    id 1203
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 1204
    label "akrobacja_lotnicza"
  ]
  node [
    id 1205
    label "zawi&#261;zywanie"
  ]
  node [
    id 1206
    label "zawi&#261;zanie"
  ]
  node [
    id 1207
    label "arrest"
  ]
  node [
    id 1208
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1209
    label "koniec"
  ]
  node [
    id 1210
    label "kra&#324;c&#243;wka"
  ]
  node [
    id 1211
    label "roztruchan"
  ]
  node [
    id 1212
    label "dzia&#322;ka"
  ]
  node [
    id 1213
    label "puch_kielichowy"
  ]
  node [
    id 1214
    label "Graal"
  ]
  node [
    id 1215
    label "pryncypa&#322;"
  ]
  node [
    id 1216
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1217
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1218
    label "wiedza"
  ]
  node [
    id 1219
    label "kierowa&#263;"
  ]
  node [
    id 1220
    label "alkohol"
  ]
  node [
    id 1221
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1222
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1223
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1224
    label "sztuka"
  ]
  node [
    id 1225
    label "dekiel"
  ]
  node [
    id 1226
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1227
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1228
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1229
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1230
    label "noosfera"
  ]
  node [
    id 1231
    label "byd&#322;o"
  ]
  node [
    id 1232
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1233
    label "makrocefalia"
  ]
  node [
    id 1234
    label "ucho"
  ]
  node [
    id 1235
    label "g&#243;ra"
  ]
  node [
    id 1236
    label "m&#243;zg"
  ]
  node [
    id 1237
    label "kierownictwo"
  ]
  node [
    id 1238
    label "fryzura"
  ]
  node [
    id 1239
    label "umys&#322;"
  ]
  node [
    id 1240
    label "cz&#322;onek"
  ]
  node [
    id 1241
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1242
    label "czaszka"
  ]
  node [
    id 1243
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1244
    label "whirl"
  ]
  node [
    id 1245
    label "krzywa"
  ]
  node [
    id 1246
    label "spiralny"
  ]
  node [
    id 1247
    label "nagromadzenie"
  ]
  node [
    id 1248
    label "wk&#322;adka"
  ]
  node [
    id 1249
    label "spirograf"
  ]
  node [
    id 1250
    label "spiral"
  ]
  node [
    id 1251
    label "przebieg"
  ]
  node [
    id 1252
    label "pas"
  ]
  node [
    id 1253
    label "swath"
  ]
  node [
    id 1254
    label "streak"
  ]
  node [
    id 1255
    label "kana&#322;"
  ]
  node [
    id 1256
    label "strip"
  ]
  node [
    id 1257
    label "ulica"
  ]
  node [
    id 1258
    label "postarzenie"
  ]
  node [
    id 1259
    label "postarzanie"
  ]
  node [
    id 1260
    label "brzydota"
  ]
  node [
    id 1261
    label "postarza&#263;"
  ]
  node [
    id 1262
    label "nadawanie"
  ]
  node [
    id 1263
    label "postarzy&#263;"
  ]
  node [
    id 1264
    label "widok"
  ]
  node [
    id 1265
    label "prostota"
  ]
  node [
    id 1266
    label "ubarwienie"
  ]
  node [
    id 1267
    label "comeliness"
  ]
  node [
    id 1268
    label "face"
  ]
  node [
    id 1269
    label "charakter"
  ]
  node [
    id 1270
    label "kopia"
  ]
  node [
    id 1271
    label "obraz"
  ]
  node [
    id 1272
    label "ilustracja"
  ]
  node [
    id 1273
    label "miniature"
  ]
  node [
    id 1274
    label "kawa&#322;ek"
  ]
  node [
    id 1275
    label "centrop&#322;at"
  ]
  node [
    id 1276
    label "airfoil"
  ]
  node [
    id 1277
    label "&#347;mig&#322;owiec"
  ]
  node [
    id 1278
    label "samolot"
  ]
  node [
    id 1279
    label "piece"
  ]
  node [
    id 1280
    label "plaster"
  ]
  node [
    id 1281
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 1282
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1283
    label "odmawia&#263;"
  ]
  node [
    id 1284
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 1285
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1286
    label "thank"
  ]
  node [
    id 1287
    label "etykieta"
  ]
  node [
    id 1288
    label "areszt"
  ]
  node [
    id 1289
    label "golf"
  ]
  node [
    id 1290
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 1291
    label "l&#261;d"
  ]
  node [
    id 1292
    label "depressive_disorder"
  ]
  node [
    id 1293
    label "bruzda"
  ]
  node [
    id 1294
    label "obszar"
  ]
  node [
    id 1295
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 1296
    label "Pampa"
  ]
  node [
    id 1297
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1298
    label "odlewnictwo"
  ]
  node [
    id 1299
    label "za&#322;amanie"
  ]
  node [
    id 1300
    label "tomizm"
  ]
  node [
    id 1301
    label "akt"
  ]
  node [
    id 1302
    label "kalokagatia"
  ]
  node [
    id 1303
    label "potencja"
  ]
  node [
    id 1304
    label "wordnet"
  ]
  node [
    id 1305
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 1306
    label "s&#322;ownictwo"
  ]
  node [
    id 1307
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 1308
    label "wykrzyknik"
  ]
  node [
    id 1309
    label "pole_semantyczne"
  ]
  node [
    id 1310
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1311
    label "pisanie_si&#281;"
  ]
  node [
    id 1312
    label "nag&#322;os"
  ]
  node [
    id 1313
    label "wyg&#322;os"
  ]
  node [
    id 1314
    label "jednostka_leksykalna"
  ]
  node [
    id 1315
    label "charakterystyka"
  ]
  node [
    id 1316
    label "zaistnie&#263;"
  ]
  node [
    id 1317
    label "Osjan"
  ]
  node [
    id 1318
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1319
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1320
    label "trim"
  ]
  node [
    id 1321
    label "poby&#263;"
  ]
  node [
    id 1322
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1323
    label "Aspazja"
  ]
  node [
    id 1324
    label "kompleksja"
  ]
  node [
    id 1325
    label "wytrzyma&#263;"
  ]
  node [
    id 1326
    label "budowa"
  ]
  node [
    id 1327
    label "pozosta&#263;"
  ]
  node [
    id 1328
    label "point"
  ]
  node [
    id 1329
    label "go&#347;&#263;"
  ]
  node [
    id 1330
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 1331
    label "vessel"
  ]
  node [
    id 1332
    label "sprz&#281;t"
  ]
  node [
    id 1333
    label "statki"
  ]
  node [
    id 1334
    label "rewaskularyzacja"
  ]
  node [
    id 1335
    label "ceramika"
  ]
  node [
    id 1336
    label "drewno"
  ]
  node [
    id 1337
    label "przew&#243;d"
  ]
  node [
    id 1338
    label "unaczyni&#263;"
  ]
  node [
    id 1339
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 1340
    label "receptacle"
  ]
  node [
    id 1341
    label "co&#347;"
  ]
  node [
    id 1342
    label "budynek"
  ]
  node [
    id 1343
    label "thing"
  ]
  node [
    id 1344
    label "program"
  ]
  node [
    id 1345
    label "strona"
  ]
  node [
    id 1346
    label "posiada&#263;"
  ]
  node [
    id 1347
    label "potencja&#322;"
  ]
  node [
    id 1348
    label "zapomina&#263;"
  ]
  node [
    id 1349
    label "zapomnienie"
  ]
  node [
    id 1350
    label "zapominanie"
  ]
  node [
    id 1351
    label "ability"
  ]
  node [
    id 1352
    label "obliczeniowo"
  ]
  node [
    id 1353
    label "zapomnie&#263;"
  ]
  node [
    id 1354
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 1355
    label "kultura_duchowa"
  ]
  node [
    id 1356
    label "kultura"
  ]
  node [
    id 1357
    label "ceremony"
  ]
  node [
    id 1358
    label "poinformowanie"
  ]
  node [
    id 1359
    label "wording"
  ]
  node [
    id 1360
    label "kompozycja"
  ]
  node [
    id 1361
    label "oznaczenie"
  ]
  node [
    id 1362
    label "znak_j&#281;zykowy"
  ]
  node [
    id 1363
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1364
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 1365
    label "grupa_imienna"
  ]
  node [
    id 1366
    label "term"
  ]
  node [
    id 1367
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1368
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1369
    label "ujawnienie"
  ]
  node [
    id 1370
    label "affirmation"
  ]
  node [
    id 1371
    label "zapisanie"
  ]
  node [
    id 1372
    label "rzucenie"
  ]
  node [
    id 1373
    label "figure"
  ]
  node [
    id 1374
    label "spos&#243;b"
  ]
  node [
    id 1375
    label "mildew"
  ]
  node [
    id 1376
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 1377
    label "ideal"
  ]
  node [
    id 1378
    label "rule"
  ]
  node [
    id 1379
    label "dekal"
  ]
  node [
    id 1380
    label "motyw"
  ]
  node [
    id 1381
    label "projekt"
  ]
  node [
    id 1382
    label "m&#322;ot"
  ]
  node [
    id 1383
    label "znak"
  ]
  node [
    id 1384
    label "drzewo"
  ]
  node [
    id 1385
    label "attribute"
  ]
  node [
    id 1386
    label "marka"
  ]
  node [
    id 1387
    label "mechanika"
  ]
  node [
    id 1388
    label "o&#347;"
  ]
  node [
    id 1389
    label "usenet"
  ]
  node [
    id 1390
    label "rozprz&#261;c"
  ]
  node [
    id 1391
    label "cybernetyk"
  ]
  node [
    id 1392
    label "podsystem"
  ]
  node [
    id 1393
    label "system"
  ]
  node [
    id 1394
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1395
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1396
    label "sk&#322;ad"
  ]
  node [
    id 1397
    label "systemat"
  ]
  node [
    id 1398
    label "konstrukcja"
  ]
  node [
    id 1399
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1400
    label "model"
  ]
  node [
    id 1401
    label "jig"
  ]
  node [
    id 1402
    label "drabina_analgetyczna"
  ]
  node [
    id 1403
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1404
    label "C"
  ]
  node [
    id 1405
    label "D"
  ]
  node [
    id 1406
    label "exemplar"
  ]
  node [
    id 1407
    label "wyraz_pochodny"
  ]
  node [
    id 1408
    label "zboczenie"
  ]
  node [
    id 1409
    label "om&#243;wienie"
  ]
  node [
    id 1410
    label "omawia&#263;"
  ]
  node [
    id 1411
    label "fraza"
  ]
  node [
    id 1412
    label "entity"
  ]
  node [
    id 1413
    label "forum"
  ]
  node [
    id 1414
    label "topik"
  ]
  node [
    id 1415
    label "tematyka"
  ]
  node [
    id 1416
    label "w&#261;tek"
  ]
  node [
    id 1417
    label "zbaczanie"
  ]
  node [
    id 1418
    label "om&#243;wi&#263;"
  ]
  node [
    id 1419
    label "omawianie"
  ]
  node [
    id 1420
    label "melodia"
  ]
  node [
    id 1421
    label "otoczka"
  ]
  node [
    id 1422
    label "zbacza&#263;"
  ]
  node [
    id 1423
    label "zboczy&#263;"
  ]
  node [
    id 1424
    label "morpheme"
  ]
  node [
    id 1425
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 1426
    label "figura_stylistyczna"
  ]
  node [
    id 1427
    label "decoration"
  ]
  node [
    id 1428
    label "dekoracja"
  ]
  node [
    id 1429
    label "reaktor_j&#261;drowy"
  ]
  node [
    id 1430
    label "magnes"
  ]
  node [
    id 1431
    label "spowalniacz"
  ]
  node [
    id 1432
    label "transformator"
  ]
  node [
    id 1433
    label "mi&#281;kisz"
  ]
  node [
    id 1434
    label "marrow"
  ]
  node [
    id 1435
    label "pr&#281;t_kontrolny"
  ]
  node [
    id 1436
    label "procesor"
  ]
  node [
    id 1437
    label "ch&#322;odziwo"
  ]
  node [
    id 1438
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 1439
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1440
    label "surowiak"
  ]
  node [
    id 1441
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 1442
    label "core"
  ]
  node [
    id 1443
    label "plan"
  ]
  node [
    id 1444
    label "kondycja"
  ]
  node [
    id 1445
    label "polecenie"
  ]
  node [
    id 1446
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1447
    label "capability"
  ]
  node [
    id 1448
    label "prawo"
  ]
  node [
    id 1449
    label "Bund"
  ]
  node [
    id 1450
    label "Mazowsze"
  ]
  node [
    id 1451
    label "PPR"
  ]
  node [
    id 1452
    label "Jakobici"
  ]
  node [
    id 1453
    label "zesp&#243;&#322;"
  ]
  node [
    id 1454
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1455
    label "SLD"
  ]
  node [
    id 1456
    label "zespolik"
  ]
  node [
    id 1457
    label "Razem"
  ]
  node [
    id 1458
    label "PiS"
  ]
  node [
    id 1459
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1460
    label "Kuomintang"
  ]
  node [
    id 1461
    label "ZSL"
  ]
  node [
    id 1462
    label "jednostka"
  ]
  node [
    id 1463
    label "proces"
  ]
  node [
    id 1464
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1465
    label "rugby"
  ]
  node [
    id 1466
    label "AWS"
  ]
  node [
    id 1467
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1468
    label "blok"
  ]
  node [
    id 1469
    label "PO"
  ]
  node [
    id 1470
    label "si&#322;a"
  ]
  node [
    id 1471
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1472
    label "Federali&#347;ci"
  ]
  node [
    id 1473
    label "PSL"
  ]
  node [
    id 1474
    label "wojsko"
  ]
  node [
    id 1475
    label "Wigowie"
  ]
  node [
    id 1476
    label "ZChN"
  ]
  node [
    id 1477
    label "egzekutywa"
  ]
  node [
    id 1478
    label "rocznik"
  ]
  node [
    id 1479
    label "The_Beatles"
  ]
  node [
    id 1480
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 1481
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1482
    label "unit"
  ]
  node [
    id 1483
    label "Depeche_Mode"
  ]
  node [
    id 1484
    label "k&#322;opot"
  ]
  node [
    id 1485
    label "subiekcja"
  ]
  node [
    id 1486
    label "konsekwentny"
  ]
  node [
    id 1487
    label "gruntowny"
  ]
  node [
    id 1488
    label "radykalnie"
  ]
  node [
    id 1489
    label "bezkompromisowy"
  ]
  node [
    id 1490
    label "sta&#322;y"
  ]
  node [
    id 1491
    label "wytrwa&#322;y"
  ]
  node [
    id 1492
    label "konsekwentnie"
  ]
  node [
    id 1493
    label "gruntownie"
  ]
  node [
    id 1494
    label "solidny"
  ]
  node [
    id 1495
    label "zupe&#322;ny"
  ]
  node [
    id 1496
    label "generalny"
  ]
  node [
    id 1497
    label "twardy"
  ]
  node [
    id 1498
    label "radykalizowanie"
  ]
  node [
    id 1499
    label "zasadniczy"
  ]
  node [
    id 1500
    label "zradykalizowanie"
  ]
  node [
    id 1501
    label "niepob&#322;a&#380;liwy"
  ]
  node [
    id 1502
    label "bezkompromisowo"
  ]
  node [
    id 1503
    label "utrzymywanie"
  ]
  node [
    id 1504
    label "movement"
  ]
  node [
    id 1505
    label "myk"
  ]
  node [
    id 1506
    label "utrzyma&#263;"
  ]
  node [
    id 1507
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1508
    label "utrzymanie"
  ]
  node [
    id 1509
    label "travel"
  ]
  node [
    id 1510
    label "kanciasty"
  ]
  node [
    id 1511
    label "commercial_enterprise"
  ]
  node [
    id 1512
    label "strumie&#324;"
  ]
  node [
    id 1513
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1514
    label "kr&#243;tki"
  ]
  node [
    id 1515
    label "taktyka"
  ]
  node [
    id 1516
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1517
    label "apraksja"
  ]
  node [
    id 1518
    label "natural_process"
  ]
  node [
    id 1519
    label "utrzymywa&#263;"
  ]
  node [
    id 1520
    label "d&#322;ugi"
  ]
  node [
    id 1521
    label "dyssypacja_energii"
  ]
  node [
    id 1522
    label "tumult"
  ]
  node [
    id 1523
    label "stopek"
  ]
  node [
    id 1524
    label "zmiana"
  ]
  node [
    id 1525
    label "manewr"
  ]
  node [
    id 1526
    label "lokomocja"
  ]
  node [
    id 1527
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1528
    label "komunikacja"
  ]
  node [
    id 1529
    label "drift"
  ]
  node [
    id 1530
    label "legislacyjnie"
  ]
  node [
    id 1531
    label "nast&#281;pstwo"
  ]
  node [
    id 1532
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1533
    label "przebiec"
  ]
  node [
    id 1534
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1535
    label "przebiegni&#281;cie"
  ]
  node [
    id 1536
    label "fabu&#322;a"
  ]
  node [
    id 1537
    label "action"
  ]
  node [
    id 1538
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1539
    label "postawa"
  ]
  node [
    id 1540
    label "posuni&#281;cie"
  ]
  node [
    id 1541
    label "maneuver"
  ]
  node [
    id 1542
    label "absolutorium"
  ]
  node [
    id 1543
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1544
    label "dzia&#322;anie"
  ]
  node [
    id 1545
    label "activity"
  ]
  node [
    id 1546
    label "boski"
  ]
  node [
    id 1547
    label "krajobraz"
  ]
  node [
    id 1548
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1549
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1550
    label "przywidzenie"
  ]
  node [
    id 1551
    label "presence"
  ]
  node [
    id 1552
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1553
    label "transportation_system"
  ]
  node [
    id 1554
    label "explicite"
  ]
  node [
    id 1555
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 1556
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1557
    label "wydeptywanie"
  ]
  node [
    id 1558
    label "wydeptanie"
  ]
  node [
    id 1559
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 1560
    label "implicite"
  ]
  node [
    id 1561
    label "ekspedytor"
  ]
  node [
    id 1562
    label "bezproblemowy"
  ]
  node [
    id 1563
    label "passage"
  ]
  node [
    id 1564
    label "oznaka"
  ]
  node [
    id 1565
    label "komplet"
  ]
  node [
    id 1566
    label "anatomopatolog"
  ]
  node [
    id 1567
    label "zmianka"
  ]
  node [
    id 1568
    label "czas"
  ]
  node [
    id 1569
    label "amendment"
  ]
  node [
    id 1570
    label "odmienianie"
  ]
  node [
    id 1571
    label "tura"
  ]
  node [
    id 1572
    label "woda_powierzchniowa"
  ]
  node [
    id 1573
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1574
    label "ciek_wodny"
  ]
  node [
    id 1575
    label "mn&#243;stwo"
  ]
  node [
    id 1576
    label "Ajgospotamoj"
  ]
  node [
    id 1577
    label "fala"
  ]
  node [
    id 1578
    label "disquiet"
  ]
  node [
    id 1579
    label "ha&#322;as"
  ]
  node [
    id 1580
    label "mechanika_teoretyczna"
  ]
  node [
    id 1581
    label "mechanika_gruntu"
  ]
  node [
    id 1582
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 1583
    label "mechanika_klasyczna"
  ]
  node [
    id 1584
    label "elektromechanika"
  ]
  node [
    id 1585
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 1586
    label "nauka"
  ]
  node [
    id 1587
    label "fizyka"
  ]
  node [
    id 1588
    label "aeromechanika"
  ]
  node [
    id 1589
    label "telemechanika"
  ]
  node [
    id 1590
    label "hydromechanika"
  ]
  node [
    id 1591
    label "daleki"
  ]
  node [
    id 1592
    label "d&#322;ugo"
  ]
  node [
    id 1593
    label "szybki"
  ]
  node [
    id 1594
    label "jednowyrazowy"
  ]
  node [
    id 1595
    label "bliski"
  ]
  node [
    id 1596
    label "s&#322;aby"
  ]
  node [
    id 1597
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1598
    label "kr&#243;tko"
  ]
  node [
    id 1599
    label "drobny"
  ]
  node [
    id 1600
    label "brak"
  ]
  node [
    id 1601
    label "z&#322;y"
  ]
  node [
    id 1602
    label "byt"
  ]
  node [
    id 1603
    label "argue"
  ]
  node [
    id 1604
    label "podtrzymywa&#263;"
  ]
  node [
    id 1605
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1606
    label "twierdzi&#263;"
  ]
  node [
    id 1607
    label "zapewnia&#263;"
  ]
  node [
    id 1608
    label "corroborate"
  ]
  node [
    id 1609
    label "trzyma&#263;"
  ]
  node [
    id 1610
    label "panowa&#263;"
  ]
  node [
    id 1611
    label "defy"
  ]
  node [
    id 1612
    label "cope"
  ]
  node [
    id 1613
    label "broni&#263;"
  ]
  node [
    id 1614
    label "sprawowa&#263;"
  ]
  node [
    id 1615
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 1616
    label "zachowywa&#263;"
  ]
  node [
    id 1617
    label "obroni&#263;"
  ]
  node [
    id 1618
    label "potrzyma&#263;"
  ]
  node [
    id 1619
    label "op&#322;aci&#263;"
  ]
  node [
    id 1620
    label "zdo&#322;a&#263;"
  ]
  node [
    id 1621
    label "podtrzyma&#263;"
  ]
  node [
    id 1622
    label "feed"
  ]
  node [
    id 1623
    label "zrobi&#263;"
  ]
  node [
    id 1624
    label "przetrzyma&#263;"
  ]
  node [
    id 1625
    label "foster"
  ]
  node [
    id 1626
    label "preserve"
  ]
  node [
    id 1627
    label "zapewni&#263;"
  ]
  node [
    id 1628
    label "zachowa&#263;"
  ]
  node [
    id 1629
    label "unie&#347;&#263;"
  ]
  node [
    id 1630
    label "bronienie"
  ]
  node [
    id 1631
    label "trzymanie"
  ]
  node [
    id 1632
    label "podtrzymywanie"
  ]
  node [
    id 1633
    label "potrzymanie"
  ]
  node [
    id 1634
    label "wychowywanie"
  ]
  node [
    id 1635
    label "panowanie"
  ]
  node [
    id 1636
    label "zachowywanie"
  ]
  node [
    id 1637
    label "twierdzenie"
  ]
  node [
    id 1638
    label "preservation"
  ]
  node [
    id 1639
    label "chowanie"
  ]
  node [
    id 1640
    label "retention"
  ]
  node [
    id 1641
    label "op&#322;acanie"
  ]
  node [
    id 1642
    label "zapewnienie"
  ]
  node [
    id 1643
    label "s&#261;dzenie"
  ]
  node [
    id 1644
    label "zapewnianie"
  ]
  node [
    id 1645
    label "obronienie"
  ]
  node [
    id 1646
    label "zap&#322;acenie"
  ]
  node [
    id 1647
    label "przetrzymanie"
  ]
  node [
    id 1648
    label "bearing"
  ]
  node [
    id 1649
    label "zdo&#322;anie"
  ]
  node [
    id 1650
    label "subsystencja"
  ]
  node [
    id 1651
    label "uniesienie"
  ]
  node [
    id 1652
    label "wy&#380;ywienie"
  ]
  node [
    id 1653
    label "podtrzymanie"
  ]
  node [
    id 1654
    label "wychowanie"
  ]
  node [
    id 1655
    label "wzbudzenie"
  ]
  node [
    id 1656
    label "gesture"
  ]
  node [
    id 1657
    label "poruszanie_si&#281;"
  ]
  node [
    id 1658
    label "nietaktowny"
  ]
  node [
    id 1659
    label "kanciasto"
  ]
  node [
    id 1660
    label "niezgrabny"
  ]
  node [
    id 1661
    label "kanciaty"
  ]
  node [
    id 1662
    label "szorstki"
  ]
  node [
    id 1663
    label "niesk&#322;adny"
  ]
  node [
    id 1664
    label "stra&#380;nik"
  ]
  node [
    id 1665
    label "przedszkole"
  ]
  node [
    id 1666
    label "opiekun"
  ]
  node [
    id 1667
    label "prezenter"
  ]
  node [
    id 1668
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1669
    label "motif"
  ]
  node [
    id 1670
    label "pozowanie"
  ]
  node [
    id 1671
    label "matryca"
  ]
  node [
    id 1672
    label "adaptation"
  ]
  node [
    id 1673
    label "pozowa&#263;"
  ]
  node [
    id 1674
    label "imitacja"
  ]
  node [
    id 1675
    label "orygina&#322;"
  ]
  node [
    id 1676
    label "facet"
  ]
  node [
    id 1677
    label "apraxia"
  ]
  node [
    id 1678
    label "zaburzenie"
  ]
  node [
    id 1679
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1680
    label "sport_motorowy"
  ]
  node [
    id 1681
    label "jazda"
  ]
  node [
    id 1682
    label "zwiad"
  ]
  node [
    id 1683
    label "metoda"
  ]
  node [
    id 1684
    label "pocz&#261;tki"
  ]
  node [
    id 1685
    label "wrinkle"
  ]
  node [
    id 1686
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1687
    label "Sierpie&#324;"
  ]
  node [
    id 1688
    label "Michnik"
  ]
  node [
    id 1689
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 1690
    label "solidarno&#347;ciowiec"
  ]
  node [
    id 1691
    label "spo&#322;ecznie"
  ]
  node [
    id 1692
    label "publiczny"
  ]
  node [
    id 1693
    label "niepubliczny"
  ]
  node [
    id 1694
    label "publicznie"
  ]
  node [
    id 1695
    label "upublicznianie"
  ]
  node [
    id 1696
    label "jawny"
  ]
  node [
    id 1697
    label "upublicznienie"
  ]
  node [
    id 1698
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 1699
    label "zobo"
  ]
  node [
    id 1700
    label "yakalo"
  ]
  node [
    id 1701
    label "dzo"
  ]
  node [
    id 1702
    label "kr&#281;torogie"
  ]
  node [
    id 1703
    label "czochrad&#322;o"
  ]
  node [
    id 1704
    label "posp&#243;lstwo"
  ]
  node [
    id 1705
    label "kraal"
  ]
  node [
    id 1706
    label "livestock"
  ]
  node [
    id 1707
    label "prze&#380;uwacz"
  ]
  node [
    id 1708
    label "bizon"
  ]
  node [
    id 1709
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 1710
    label "zebu"
  ]
  node [
    id 1711
    label "byd&#322;o_domowe"
  ]
  node [
    id 1712
    label "skrajny"
  ]
  node [
    id 1713
    label "extremely"
  ]
  node [
    id 1714
    label "okrajkowy"
  ]
  node [
    id 1715
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 1716
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 1717
    label "indentation"
  ]
  node [
    id 1718
    label "zjedzenie"
  ]
  node [
    id 1719
    label "snub"
  ]
  node [
    id 1720
    label "warunek_lokalowy"
  ]
  node [
    id 1721
    label "plac"
  ]
  node [
    id 1722
    label "location"
  ]
  node [
    id 1723
    label "uwaga"
  ]
  node [
    id 1724
    label "przestrze&#324;"
  ]
  node [
    id 1725
    label "chwila"
  ]
  node [
    id 1726
    label "rz&#261;d"
  ]
  node [
    id 1727
    label "sk&#322;adnik"
  ]
  node [
    id 1728
    label "warunki"
  ]
  node [
    id 1729
    label "sytuacja"
  ]
  node [
    id 1730
    label "p&#322;aszczyzna"
  ]
  node [
    id 1731
    label "przek&#322;adaniec"
  ]
  node [
    id 1732
    label "covering"
  ]
  node [
    id 1733
    label "podwarstwa"
  ]
  node [
    id 1734
    label "kierunek"
  ]
  node [
    id 1735
    label "obiekt_matematyczny"
  ]
  node [
    id 1736
    label "zwrot_wektora"
  ]
  node [
    id 1737
    label "vector"
  ]
  node [
    id 1738
    label "parametryzacja"
  ]
  node [
    id 1739
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 1740
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1741
    label "plamka"
  ]
  node [
    id 1742
    label "stopie&#324;_pisma"
  ]
  node [
    id 1743
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1744
    label "mark"
  ]
  node [
    id 1745
    label "prosta"
  ]
  node [
    id 1746
    label "zapunktowa&#263;"
  ]
  node [
    id 1747
    label "podpunkt"
  ]
  node [
    id 1748
    label "pozycja"
  ]
  node [
    id 1749
    label "jako&#347;&#263;"
  ]
  node [
    id 1750
    label "wyk&#322;adnik"
  ]
  node [
    id 1751
    label "szczebel"
  ]
  node [
    id 1752
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1753
    label "ranga"
  ]
  node [
    id 1754
    label "rozmiar"
  ]
  node [
    id 1755
    label "part"
  ]
  node [
    id 1756
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1757
    label "Indie_Portugalskie"
  ]
  node [
    id 1758
    label "Polinezja"
  ]
  node [
    id 1759
    label "Aleuty"
  ]
  node [
    id 1760
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1761
    label "unloose"
  ]
  node [
    id 1762
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1763
    label "bring"
  ]
  node [
    id 1764
    label "urzeczywistni&#263;"
  ]
  node [
    id 1765
    label "undo"
  ]
  node [
    id 1766
    label "usun&#261;&#263;"
  ]
  node [
    id 1767
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1768
    label "przesta&#263;"
  ]
  node [
    id 1769
    label "concoct"
  ]
  node [
    id 1770
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1771
    label "coating"
  ]
  node [
    id 1772
    label "drop"
  ]
  node [
    id 1773
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1774
    label "leave_office"
  ]
  node [
    id 1775
    label "fail"
  ]
  node [
    id 1776
    label "zerwa&#263;"
  ]
  node [
    id 1777
    label "kill"
  ]
  node [
    id 1778
    label "withdraw"
  ]
  node [
    id 1779
    label "motivate"
  ]
  node [
    id 1780
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 1781
    label "wyrugowa&#263;"
  ]
  node [
    id 1782
    label "go"
  ]
  node [
    id 1783
    label "zabi&#263;"
  ]
  node [
    id 1784
    label "spowodowa&#263;"
  ]
  node [
    id 1785
    label "przenie&#347;&#263;"
  ]
  node [
    id 1786
    label "przesun&#261;&#263;"
  ]
  node [
    id 1787
    label "actualize"
  ]
  node [
    id 1788
    label "jajko_Kolumba"
  ]
  node [
    id 1789
    label "obstruction"
  ]
  node [
    id 1790
    label "trudno&#347;&#263;"
  ]
  node [
    id 1791
    label "pierepa&#322;ka"
  ]
  node [
    id 1792
    label "ambaras"
  ]
  node [
    id 1793
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 1794
    label "napotka&#263;"
  ]
  node [
    id 1795
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 1796
    label "k&#322;opotliwy"
  ]
  node [
    id 1797
    label "napotkanie"
  ]
  node [
    id 1798
    label "difficulty"
  ]
  node [
    id 1799
    label "obstacle"
  ]
  node [
    id 1800
    label "Reclaim"
  ]
  node [
    id 1801
    label "the"
  ]
  node [
    id 1802
    label "Streets"
  ]
  node [
    id 1803
    label "odzyskiwa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 894
  ]
  edge [
    source 10
    target 895
  ]
  edge [
    source 10
    target 896
  ]
  edge [
    source 10
    target 897
  ]
  edge [
    source 10
    target 898
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 10
    target 900
  ]
  edge [
    source 10
    target 901
  ]
  edge [
    source 10
    target 902
  ]
  edge [
    source 10
    target 903
  ]
  edge [
    source 10
    target 904
  ]
  edge [
    source 10
    target 905
  ]
  edge [
    source 10
    target 906
  ]
  edge [
    source 10
    target 907
  ]
  edge [
    source 10
    target 908
  ]
  edge [
    source 10
    target 909
  ]
  edge [
    source 10
    target 910
  ]
  edge [
    source 10
    target 911
  ]
  edge [
    source 10
    target 912
  ]
  edge [
    source 10
    target 913
  ]
  edge [
    source 10
    target 914
  ]
  edge [
    source 10
    target 915
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 917
  ]
  edge [
    source 10
    target 918
  ]
  edge [
    source 10
    target 919
  ]
  edge [
    source 10
    target 920
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 921
  ]
  edge [
    source 10
    target 922
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 924
  ]
  edge [
    source 10
    target 925
  ]
  edge [
    source 10
    target 926
  ]
  edge [
    source 10
    target 927
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 928
  ]
  edge [
    source 10
    target 929
  ]
  edge [
    source 10
    target 930
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 931
  ]
  edge [
    source 10
    target 932
  ]
  edge [
    source 10
    target 933
  ]
  edge [
    source 10
    target 934
  ]
  edge [
    source 10
    target 935
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 936
  ]
  edge [
    source 10
    target 937
  ]
  edge [
    source 10
    target 938
  ]
  edge [
    source 10
    target 939
  ]
  edge [
    source 10
    target 940
  ]
  edge [
    source 10
    target 941
  ]
  edge [
    source 10
    target 942
  ]
  edge [
    source 10
    target 943
  ]
  edge [
    source 10
    target 944
  ]
  edge [
    source 10
    target 945
  ]
  edge [
    source 10
    target 946
  ]
  edge [
    source 10
    target 947
  ]
  edge [
    source 10
    target 948
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 949
  ]
  edge [
    source 10
    target 950
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 951
  ]
  edge [
    source 11
    target 952
  ]
  edge [
    source 11
    target 953
  ]
  edge [
    source 11
    target 954
  ]
  edge [
    source 11
    target 955
  ]
  edge [
    source 11
    target 956
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 11
    target 962
  ]
  edge [
    source 11
    target 963
  ]
  edge [
    source 11
    target 964
  ]
  edge [
    source 11
    target 965
  ]
  edge [
    source 11
    target 966
  ]
  edge [
    source 11
    target 967
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 968
  ]
  edge [
    source 11
    target 969
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 970
  ]
  edge [
    source 11
    target 971
  ]
  edge [
    source 11
    target 972
  ]
  edge [
    source 11
    target 973
  ]
  edge [
    source 11
    target 974
  ]
  edge [
    source 11
    target 975
  ]
  edge [
    source 11
    target 976
  ]
  edge [
    source 11
    target 977
  ]
  edge [
    source 11
    target 978
  ]
  edge [
    source 11
    target 979
  ]
  edge [
    source 11
    target 980
  ]
  edge [
    source 11
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 1073
  ]
  edge [
    source 12
    target 1074
  ]
  edge [
    source 12
    target 1075
  ]
  edge [
    source 12
    target 1076
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 1077
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 1078
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 1079
  ]
  edge [
    source 13
    target 1080
  ]
  edge [
    source 13
    target 1081
  ]
  edge [
    source 13
    target 1082
  ]
  edge [
    source 13
    target 1083
  ]
  edge [
    source 13
    target 1084
  ]
  edge [
    source 13
    target 1085
  ]
  edge [
    source 13
    target 1086
  ]
  edge [
    source 13
    target 1087
  ]
  edge [
    source 13
    target 1088
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 304
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 303
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 270
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 14
    target 1148
  ]
  edge [
    source 14
    target 1149
  ]
  edge [
    source 14
    target 1150
  ]
  edge [
    source 14
    target 1151
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 1152
  ]
  edge [
    source 14
    target 1153
  ]
  edge [
    source 14
    target 1154
  ]
  edge [
    source 14
    target 1155
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1157
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 1173
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 1174
  ]
  edge [
    source 14
    target 1175
  ]
  edge [
    source 14
    target 1176
  ]
  edge [
    source 14
    target 1177
  ]
  edge [
    source 14
    target 1178
  ]
  edge [
    source 14
    target 1179
  ]
  edge [
    source 14
    target 1180
  ]
  edge [
    source 14
    target 1181
  ]
  edge [
    source 14
    target 1182
  ]
  edge [
    source 14
    target 1183
  ]
  edge [
    source 14
    target 1184
  ]
  edge [
    source 14
    target 1185
  ]
  edge [
    source 14
    target 1186
  ]
  edge [
    source 14
    target 1187
  ]
  edge [
    source 14
    target 1188
  ]
  edge [
    source 14
    target 1189
  ]
  edge [
    source 14
    target 1190
  ]
  edge [
    source 14
    target 1191
  ]
  edge [
    source 14
    target 1192
  ]
  edge [
    source 14
    target 1193
  ]
  edge [
    source 14
    target 1194
  ]
  edge [
    source 14
    target 1195
  ]
  edge [
    source 14
    target 1196
  ]
  edge [
    source 14
    target 1197
  ]
  edge [
    source 14
    target 1198
  ]
  edge [
    source 14
    target 1199
  ]
  edge [
    source 14
    target 1200
  ]
  edge [
    source 14
    target 1201
  ]
  edge [
    source 14
    target 1202
  ]
  edge [
    source 14
    target 1203
  ]
  edge [
    source 14
    target 1204
  ]
  edge [
    source 14
    target 1205
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 1206
  ]
  edge [
    source 14
    target 1207
  ]
  edge [
    source 14
    target 1208
  ]
  edge [
    source 14
    target 1209
  ]
  edge [
    source 14
    target 1210
  ]
  edge [
    source 14
    target 1211
  ]
  edge [
    source 14
    target 1212
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 1213
  ]
  edge [
    source 14
    target 1214
  ]
  edge [
    source 14
    target 1215
  ]
  edge [
    source 14
    target 1216
  ]
  edge [
    source 14
    target 1217
  ]
  edge [
    source 14
    target 1218
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 1219
  ]
  edge [
    source 14
    target 1220
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 1221
  ]
  edge [
    source 14
    target 1222
  ]
  edge [
    source 14
    target 1223
  ]
  edge [
    source 14
    target 1224
  ]
  edge [
    source 14
    target 1225
  ]
  edge [
    source 14
    target 990
  ]
  edge [
    source 14
    target 1226
  ]
  edge [
    source 14
    target 1227
  ]
  edge [
    source 14
    target 1228
  ]
  edge [
    source 14
    target 1229
  ]
  edge [
    source 14
    target 1230
  ]
  edge [
    source 14
    target 1231
  ]
  edge [
    source 14
    target 1232
  ]
  edge [
    source 14
    target 1233
  ]
  edge [
    source 14
    target 1234
  ]
  edge [
    source 14
    target 1235
  ]
  edge [
    source 14
    target 1236
  ]
  edge [
    source 14
    target 1237
  ]
  edge [
    source 14
    target 1238
  ]
  edge [
    source 14
    target 1239
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 1240
  ]
  edge [
    source 14
    target 1241
  ]
  edge [
    source 14
    target 1242
  ]
  edge [
    source 14
    target 1243
  ]
  edge [
    source 14
    target 1244
  ]
  edge [
    source 14
    target 1245
  ]
  edge [
    source 14
    target 1246
  ]
  edge [
    source 14
    target 1247
  ]
  edge [
    source 14
    target 1248
  ]
  edge [
    source 14
    target 1249
  ]
  edge [
    source 14
    target 1250
  ]
  edge [
    source 14
    target 1251
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 1252
  ]
  edge [
    source 14
    target 1253
  ]
  edge [
    source 14
    target 1254
  ]
  edge [
    source 14
    target 1255
  ]
  edge [
    source 14
    target 1256
  ]
  edge [
    source 14
    target 1257
  ]
  edge [
    source 14
    target 1258
  ]
  edge [
    source 14
    target 1259
  ]
  edge [
    source 14
    target 1260
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 1261
  ]
  edge [
    source 14
    target 1262
  ]
  edge [
    source 14
    target 1263
  ]
  edge [
    source 14
    target 1264
  ]
  edge [
    source 14
    target 1265
  ]
  edge [
    source 14
    target 1266
  ]
  edge [
    source 14
    target 360
  ]
  edge [
    source 14
    target 1267
  ]
  edge [
    source 14
    target 1268
  ]
  edge [
    source 14
    target 1269
  ]
  edge [
    source 14
    target 1270
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 1271
  ]
  edge [
    source 14
    target 1272
  ]
  edge [
    source 14
    target 1273
  ]
  edge [
    source 14
    target 1274
  ]
  edge [
    source 14
    target 1275
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 1276
  ]
  edge [
    source 14
    target 1277
  ]
  edge [
    source 14
    target 1278
  ]
  edge [
    source 14
    target 1279
  ]
  edge [
    source 14
    target 1280
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 1281
  ]
  edge [
    source 14
    target 1282
  ]
  edge [
    source 14
    target 1283
  ]
  edge [
    source 14
    target 1284
  ]
  edge [
    source 14
    target 1285
  ]
  edge [
    source 14
    target 1286
  ]
  edge [
    source 14
    target 1287
  ]
  edge [
    source 14
    target 1288
  ]
  edge [
    source 14
    target 1289
  ]
  edge [
    source 14
    target 230
  ]
  edge [
    source 14
    target 1290
  ]
  edge [
    source 14
    target 1291
  ]
  edge [
    source 14
    target 1292
  ]
  edge [
    source 14
    target 1293
  ]
  edge [
    source 14
    target 1294
  ]
  edge [
    source 14
    target 1295
  ]
  edge [
    source 14
    target 1296
  ]
  edge [
    source 14
    target 1297
  ]
  edge [
    source 14
    target 1298
  ]
  edge [
    source 14
    target 1299
  ]
  edge [
    source 14
    target 1300
  ]
  edge [
    source 14
    target 1301
  ]
  edge [
    source 14
    target 1302
  ]
  edge [
    source 14
    target 1303
  ]
  edge [
    source 14
    target 1304
  ]
  edge [
    source 14
    target 1305
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 1306
  ]
  edge [
    source 14
    target 1307
  ]
  edge [
    source 14
    target 1308
  ]
  edge [
    source 14
    target 1309
  ]
  edge [
    source 14
    target 1310
  ]
  edge [
    source 14
    target 1311
  ]
  edge [
    source 14
    target 1312
  ]
  edge [
    source 14
    target 1313
  ]
  edge [
    source 14
    target 1314
  ]
  edge [
    source 14
    target 1315
  ]
  edge [
    source 14
    target 1316
  ]
  edge [
    source 14
    target 1317
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 1318
  ]
  edge [
    source 14
    target 1319
  ]
  edge [
    source 14
    target 1320
  ]
  edge [
    source 14
    target 1321
  ]
  edge [
    source 14
    target 1322
  ]
  edge [
    source 14
    target 1323
  ]
  edge [
    source 14
    target 1324
  ]
  edge [
    source 14
    target 1325
  ]
  edge [
    source 14
    target 1326
  ]
  edge [
    source 14
    target 1327
  ]
  edge [
    source 14
    target 1328
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 1329
  ]
  edge [
    source 14
    target 364
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 333
  ]
  edge [
    source 14
    target 334
  ]
  edge [
    source 14
    target 335
  ]
  edge [
    source 14
    target 336
  ]
  edge [
    source 14
    target 337
  ]
  edge [
    source 14
    target 338
  ]
  edge [
    source 14
    target 339
  ]
  edge [
    source 14
    target 340
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 342
  ]
  edge [
    source 14
    target 343
  ]
  edge [
    source 14
    target 344
  ]
  edge [
    source 14
    target 345
  ]
  edge [
    source 14
    target 346
  ]
  edge [
    source 14
    target 347
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 348
  ]
  edge [
    source 14
    target 349
  ]
  edge [
    source 14
    target 350
  ]
  edge [
    source 14
    target 351
  ]
  edge [
    source 14
    target 352
  ]
  edge [
    source 14
    target 353
  ]
  edge [
    source 14
    target 354
  ]
  edge [
    source 14
    target 355
  ]
  edge [
    source 14
    target 356
  ]
  edge [
    source 14
    target 357
  ]
  edge [
    source 14
    target 358
  ]
  edge [
    source 14
    target 359
  ]
  edge [
    source 14
    target 361
  ]
  edge [
    source 14
    target 362
  ]
  edge [
    source 14
    target 363
  ]
  edge [
    source 14
    target 365
  ]
  edge [
    source 14
    target 366
  ]
  edge [
    source 14
    target 367
  ]
  edge [
    source 14
    target 368
  ]
  edge [
    source 14
    target 369
  ]
  edge [
    source 14
    target 370
  ]
  edge [
    source 14
    target 1330
  ]
  edge [
    source 14
    target 1331
  ]
  edge [
    source 14
    target 1332
  ]
  edge [
    source 14
    target 1333
  ]
  edge [
    source 14
    target 1334
  ]
  edge [
    source 14
    target 1335
  ]
  edge [
    source 14
    target 1336
  ]
  edge [
    source 14
    target 1337
  ]
  edge [
    source 14
    target 1338
  ]
  edge [
    source 14
    target 1339
  ]
  edge [
    source 14
    target 1340
  ]
  edge [
    source 14
    target 1341
  ]
  edge [
    source 14
    target 1342
  ]
  edge [
    source 14
    target 1343
  ]
  edge [
    source 14
    target 1344
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 1345
  ]
  edge [
    source 14
    target 1346
  ]
  edge [
    source 14
    target 1347
  ]
  edge [
    source 14
    target 1348
  ]
  edge [
    source 14
    target 1349
  ]
  edge [
    source 14
    target 1350
  ]
  edge [
    source 14
    target 1351
  ]
  edge [
    source 14
    target 1352
  ]
  edge [
    source 14
    target 1353
  ]
  edge [
    source 14
    target 1354
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 14
    target 1355
  ]
  edge [
    source 14
    target 1356
  ]
  edge [
    source 14
    target 1357
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 1358
  ]
  edge [
    source 14
    target 1359
  ]
  edge [
    source 14
    target 1360
  ]
  edge [
    source 14
    target 1361
  ]
  edge [
    source 14
    target 1362
  ]
  edge [
    source 14
    target 1363
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 1364
  ]
  edge [
    source 14
    target 1365
  ]
  edge [
    source 14
    target 1366
  ]
  edge [
    source 14
    target 1367
  ]
  edge [
    source 14
    target 1368
  ]
  edge [
    source 14
    target 1369
  ]
  edge [
    source 14
    target 1370
  ]
  edge [
    source 14
    target 1371
  ]
  edge [
    source 14
    target 1372
  ]
  edge [
    source 14
    target 53
  ]
  edge [
    source 14
    target 1373
  ]
  edge [
    source 14
    target 1374
  ]
  edge [
    source 14
    target 1375
  ]
  edge [
    source 14
    target 1376
  ]
  edge [
    source 14
    target 1377
  ]
  edge [
    source 14
    target 1378
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 1379
  ]
  edge [
    source 14
    target 1380
  ]
  edge [
    source 14
    target 1381
  ]
  edge [
    source 14
    target 1382
  ]
  edge [
    source 14
    target 1383
  ]
  edge [
    source 14
    target 1384
  ]
  edge [
    source 14
    target 238
  ]
  edge [
    source 14
    target 1385
  ]
  edge [
    source 14
    target 1386
  ]
  edge [
    source 14
    target 1387
  ]
  edge [
    source 14
    target 1388
  ]
  edge [
    source 14
    target 1389
  ]
  edge [
    source 14
    target 1390
  ]
  edge [
    source 14
    target 1391
  ]
  edge [
    source 14
    target 1392
  ]
  edge [
    source 14
    target 1393
  ]
  edge [
    source 14
    target 1394
  ]
  edge [
    source 14
    target 1395
  ]
  edge [
    source 14
    target 1396
  ]
  edge [
    source 14
    target 1397
  ]
  edge [
    source 14
    target 1398
  ]
  edge [
    source 14
    target 1399
  ]
  edge [
    source 14
    target 1400
  ]
  edge [
    source 14
    target 1401
  ]
  edge [
    source 14
    target 1402
  ]
  edge [
    source 14
    target 1403
  ]
  edge [
    source 14
    target 1404
  ]
  edge [
    source 14
    target 1405
  ]
  edge [
    source 14
    target 1406
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 1407
  ]
  edge [
    source 14
    target 1408
  ]
  edge [
    source 14
    target 1409
  ]
  edge [
    source 14
    target 1410
  ]
  edge [
    source 14
    target 1411
  ]
  edge [
    source 14
    target 1412
  ]
  edge [
    source 14
    target 1413
  ]
  edge [
    source 14
    target 1414
  ]
  edge [
    source 14
    target 1415
  ]
  edge [
    source 14
    target 1416
  ]
  edge [
    source 14
    target 1417
  ]
  edge [
    source 14
    target 1418
  ]
  edge [
    source 14
    target 1419
  ]
  edge [
    source 14
    target 1420
  ]
  edge [
    source 14
    target 1421
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 1422
  ]
  edge [
    source 14
    target 1423
  ]
  edge [
    source 14
    target 1424
  ]
  edge [
    source 14
    target 1425
  ]
  edge [
    source 14
    target 1426
  ]
  edge [
    source 14
    target 1427
  ]
  edge [
    source 14
    target 1428
  ]
  edge [
    source 14
    target 1429
  ]
  edge [
    source 14
    target 1430
  ]
  edge [
    source 14
    target 1431
  ]
  edge [
    source 14
    target 1432
  ]
  edge [
    source 14
    target 1433
  ]
  edge [
    source 14
    target 1434
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 1435
  ]
  edge [
    source 14
    target 1436
  ]
  edge [
    source 14
    target 1437
  ]
  edge [
    source 14
    target 1438
  ]
  edge [
    source 14
    target 1439
  ]
  edge [
    source 14
    target 1440
  ]
  edge [
    source 14
    target 1441
  ]
  edge [
    source 14
    target 1442
  ]
  edge [
    source 14
    target 1443
  ]
  edge [
    source 14
    target 1444
  ]
  edge [
    source 14
    target 1445
  ]
  edge [
    source 14
    target 1446
  ]
  edge [
    source 14
    target 1447
  ]
  edge [
    source 14
    target 1448
  ]
  edge [
    source 14
    target 1449
  ]
  edge [
    source 14
    target 1450
  ]
  edge [
    source 14
    target 1451
  ]
  edge [
    source 14
    target 1452
  ]
  edge [
    source 14
    target 1453
  ]
  edge [
    source 14
    target 1454
  ]
  edge [
    source 14
    target 1455
  ]
  edge [
    source 14
    target 1456
  ]
  edge [
    source 14
    target 1457
  ]
  edge [
    source 14
    target 1458
  ]
  edge [
    source 14
    target 1459
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 1460
  ]
  edge [
    source 14
    target 1461
  ]
  edge [
    source 14
    target 1462
  ]
  edge [
    source 14
    target 1463
  ]
  edge [
    source 14
    target 1464
  ]
  edge [
    source 14
    target 1465
  ]
  edge [
    source 14
    target 1466
  ]
  edge [
    source 14
    target 1467
  ]
  edge [
    source 14
    target 1468
  ]
  edge [
    source 14
    target 1469
  ]
  edge [
    source 14
    target 1470
  ]
  edge [
    source 14
    target 1471
  ]
  edge [
    source 14
    target 1472
  ]
  edge [
    source 14
    target 1473
  ]
  edge [
    source 14
    target 1474
  ]
  edge [
    source 14
    target 1475
  ]
  edge [
    source 14
    target 1476
  ]
  edge [
    source 14
    target 1477
  ]
  edge [
    source 14
    target 1478
  ]
  edge [
    source 14
    target 1479
  ]
  edge [
    source 14
    target 1480
  ]
  edge [
    source 14
    target 1481
  ]
  edge [
    source 14
    target 1482
  ]
  edge [
    source 14
    target 1483
  ]
  edge [
    source 15
    target 1484
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 1485
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1486
  ]
  edge [
    source 16
    target 1487
  ]
  edge [
    source 16
    target 1488
  ]
  edge [
    source 16
    target 1489
  ]
  edge [
    source 16
    target 1490
  ]
  edge [
    source 16
    target 1491
  ]
  edge [
    source 16
    target 1492
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 1493
  ]
  edge [
    source 16
    target 1494
  ]
  edge [
    source 16
    target 1495
  ]
  edge [
    source 16
    target 1496
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 1497
  ]
  edge [
    source 16
    target 1498
  ]
  edge [
    source 16
    target 1499
  ]
  edge [
    source 16
    target 1500
  ]
  edge [
    source 16
    target 1501
  ]
  edge [
    source 16
    target 1502
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1387
  ]
  edge [
    source 17
    target 1503
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 1504
  ]
  edge [
    source 17
    target 1505
  ]
  edge [
    source 17
    target 1506
  ]
  edge [
    source 17
    target 1507
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 1508
  ]
  edge [
    source 17
    target 1509
  ]
  edge [
    source 17
    target 1510
  ]
  edge [
    source 17
    target 1511
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1512
  ]
  edge [
    source 17
    target 1463
  ]
  edge [
    source 17
    target 1513
  ]
  edge [
    source 17
    target 1514
  ]
  edge [
    source 17
    target 1515
  ]
  edge [
    source 17
    target 1516
  ]
  edge [
    source 17
    target 1517
  ]
  edge [
    source 17
    target 1518
  ]
  edge [
    source 17
    target 1519
  ]
  edge [
    source 17
    target 1520
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 1521
  ]
  edge [
    source 17
    target 1522
  ]
  edge [
    source 17
    target 1523
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 1524
  ]
  edge [
    source 17
    target 1525
  ]
  edge [
    source 17
    target 1526
  ]
  edge [
    source 17
    target 1527
  ]
  edge [
    source 17
    target 1528
  ]
  edge [
    source 17
    target 1529
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 1251
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 1530
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 1531
  ]
  edge [
    source 17
    target 1532
  ]
  edge [
    source 17
    target 1533
  ]
  edge [
    source 17
    target 1269
  ]
  edge [
    source 17
    target 1534
  ]
  edge [
    source 17
    target 1380
  ]
  edge [
    source 17
    target 1535
  ]
  edge [
    source 17
    target 1536
  ]
  edge [
    source 17
    target 1537
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 1538
  ]
  edge [
    source 17
    target 1539
  ]
  edge [
    source 17
    target 1540
  ]
  edge [
    source 17
    target 1541
  ]
  edge [
    source 17
    target 1542
  ]
  edge [
    source 17
    target 1543
  ]
  edge [
    source 17
    target 1544
  ]
  edge [
    source 17
    target 1545
  ]
  edge [
    source 17
    target 1546
  ]
  edge [
    source 17
    target 1547
  ]
  edge [
    source 17
    target 1548
  ]
  edge [
    source 17
    target 1549
  ]
  edge [
    source 17
    target 1550
  ]
  edge [
    source 17
    target 1551
  ]
  edge [
    source 17
    target 1552
  ]
  edge [
    source 17
    target 1553
  ]
  edge [
    source 17
    target 1554
  ]
  edge [
    source 17
    target 1555
  ]
  edge [
    source 17
    target 1556
  ]
  edge [
    source 17
    target 1557
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 1558
  ]
  edge [
    source 17
    target 1559
  ]
  edge [
    source 17
    target 1560
  ]
  edge [
    source 17
    target 1561
  ]
  edge [
    source 17
    target 1562
  ]
  edge [
    source 17
    target 1155
  ]
  edge [
    source 17
    target 1563
  ]
  edge [
    source 17
    target 1564
  ]
  edge [
    source 17
    target 1159
  ]
  edge [
    source 17
    target 1161
  ]
  edge [
    source 17
    target 1565
  ]
  edge [
    source 17
    target 1566
  ]
  edge [
    source 17
    target 1567
  ]
  edge [
    source 17
    target 1568
  ]
  edge [
    source 17
    target 1569
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 1570
  ]
  edge [
    source 17
    target 1571
  ]
  edge [
    source 17
    target 1572
  ]
  edge [
    source 17
    target 1573
  ]
  edge [
    source 17
    target 1574
  ]
  edge [
    source 17
    target 1575
  ]
  edge [
    source 17
    target 1576
  ]
  edge [
    source 17
    target 1577
  ]
  edge [
    source 17
    target 1578
  ]
  edge [
    source 17
    target 1579
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1580
  ]
  edge [
    source 17
    target 1581
  ]
  edge [
    source 17
    target 1582
  ]
  edge [
    source 17
    target 1583
  ]
  edge [
    source 17
    target 1584
  ]
  edge [
    source 17
    target 1585
  ]
  edge [
    source 17
    target 1586
  ]
  edge [
    source 17
    target 303
  ]
  edge [
    source 17
    target 1587
  ]
  edge [
    source 17
    target 1588
  ]
  edge [
    source 17
    target 1589
  ]
  edge [
    source 17
    target 1590
  ]
  edge [
    source 17
    target 1591
  ]
  edge [
    source 17
    target 1592
  ]
  edge [
    source 17
    target 1593
  ]
  edge [
    source 17
    target 1594
  ]
  edge [
    source 17
    target 1595
  ]
  edge [
    source 17
    target 1596
  ]
  edge [
    source 17
    target 1597
  ]
  edge [
    source 17
    target 1598
  ]
  edge [
    source 17
    target 1599
  ]
  edge [
    source 17
    target 1600
  ]
  edge [
    source 17
    target 1601
  ]
  edge [
    source 17
    target 1602
  ]
  edge [
    source 17
    target 1603
  ]
  edge [
    source 17
    target 1604
  ]
  edge [
    source 17
    target 1605
  ]
  edge [
    source 17
    target 1606
  ]
  edge [
    source 17
    target 1607
  ]
  edge [
    source 17
    target 1608
  ]
  edge [
    source 17
    target 1609
  ]
  edge [
    source 17
    target 1610
  ]
  edge [
    source 17
    target 1611
  ]
  edge [
    source 17
    target 1612
  ]
  edge [
    source 17
    target 1613
  ]
  edge [
    source 17
    target 1614
  ]
  edge [
    source 17
    target 1615
  ]
  edge [
    source 17
    target 1616
  ]
  edge [
    source 17
    target 1617
  ]
  edge [
    source 17
    target 1618
  ]
  edge [
    source 17
    target 1619
  ]
  edge [
    source 17
    target 1620
  ]
  edge [
    source 17
    target 1621
  ]
  edge [
    source 17
    target 1622
  ]
  edge [
    source 17
    target 1623
  ]
  edge [
    source 17
    target 1624
  ]
  edge [
    source 17
    target 1625
  ]
  edge [
    source 17
    target 1626
  ]
  edge [
    source 17
    target 1627
  ]
  edge [
    source 17
    target 1628
  ]
  edge [
    source 17
    target 1629
  ]
  edge [
    source 17
    target 1630
  ]
  edge [
    source 17
    target 1631
  ]
  edge [
    source 17
    target 1632
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 1633
  ]
  edge [
    source 17
    target 1634
  ]
  edge [
    source 17
    target 1635
  ]
  edge [
    source 17
    target 1636
  ]
  edge [
    source 17
    target 1637
  ]
  edge [
    source 17
    target 1638
  ]
  edge [
    source 17
    target 1639
  ]
  edge [
    source 17
    target 1640
  ]
  edge [
    source 17
    target 1641
  ]
  edge [
    source 17
    target 1642
  ]
  edge [
    source 17
    target 1643
  ]
  edge [
    source 17
    target 1644
  ]
  edge [
    source 17
    target 1645
  ]
  edge [
    source 17
    target 1646
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 1647
  ]
  edge [
    source 17
    target 1648
  ]
  edge [
    source 17
    target 1649
  ]
  edge [
    source 17
    target 1650
  ]
  edge [
    source 17
    target 1651
  ]
  edge [
    source 17
    target 1652
  ]
  edge [
    source 17
    target 1653
  ]
  edge [
    source 17
    target 1654
  ]
  edge [
    source 17
    target 270
  ]
  edge [
    source 17
    target 1655
  ]
  edge [
    source 17
    target 1656
  ]
  edge [
    source 17
    target 268
  ]
  edge [
    source 17
    target 1368
  ]
  edge [
    source 17
    target 1657
  ]
  edge [
    source 17
    target 396
  ]
  edge [
    source 17
    target 1658
  ]
  edge [
    source 17
    target 1659
  ]
  edge [
    source 17
    target 1660
  ]
  edge [
    source 17
    target 1661
  ]
  edge [
    source 17
    target 1662
  ]
  edge [
    source 17
    target 1663
  ]
  edge [
    source 17
    target 1664
  ]
  edge [
    source 17
    target 1172
  ]
  edge [
    source 17
    target 1665
  ]
  edge [
    source 17
    target 1666
  ]
  edge [
    source 17
    target 1374
  ]
  edge [
    source 17
    target 550
  ]
  edge [
    source 17
    target 1667
  ]
  edge [
    source 17
    target 1157
  ]
  edge [
    source 17
    target 1375
  ]
  edge [
    source 17
    target 1668
  ]
  edge [
    source 17
    target 1669
  ]
  edge [
    source 17
    target 1670
  ]
  edge [
    source 17
    target 1377
  ]
  edge [
    source 17
    target 583
  ]
  edge [
    source 17
    target 1671
  ]
  edge [
    source 17
    target 1672
  ]
  edge [
    source 17
    target 1673
  ]
  edge [
    source 17
    target 1674
  ]
  edge [
    source 17
    target 1675
  ]
  edge [
    source 17
    target 1676
  ]
  edge [
    source 17
    target 1125
  ]
  edge [
    source 17
    target 1677
  ]
  edge [
    source 17
    target 1678
  ]
  edge [
    source 17
    target 1679
  ]
  edge [
    source 17
    target 1680
  ]
  edge [
    source 17
    target 1681
  ]
  edge [
    source 17
    target 1682
  ]
  edge [
    source 17
    target 1683
  ]
  edge [
    source 17
    target 1684
  ]
  edge [
    source 17
    target 1685
  ]
  edge [
    source 17
    target 1686
  ]
  edge [
    source 17
    target 1687
  ]
  edge [
    source 17
    target 1688
  ]
  edge [
    source 17
    target 1689
  ]
  edge [
    source 17
    target 1690
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1691
  ]
  edge [
    source 18
    target 1692
  ]
  edge [
    source 18
    target 1693
  ]
  edge [
    source 18
    target 1694
  ]
  edge [
    source 18
    target 1695
  ]
  edge [
    source 18
    target 1696
  ]
  edge [
    source 18
    target 1697
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1698
  ]
  edge [
    source 19
    target 1699
  ]
  edge [
    source 19
    target 1700
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1701
  ]
  edge [
    source 19
    target 1702
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 573
  ]
  edge [
    source 19
    target 1703
  ]
  edge [
    source 19
    target 1704
  ]
  edge [
    source 19
    target 1705
  ]
  edge [
    source 19
    target 1706
  ]
  edge [
    source 19
    target 1707
  ]
  edge [
    source 19
    target 1708
  ]
  edge [
    source 19
    target 1709
  ]
  edge [
    source 19
    target 1710
  ]
  edge [
    source 19
    target 1711
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1712
  ]
  edge [
    source 20
    target 1713
  ]
  edge [
    source 20
    target 1714
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 334
  ]
  edge [
    source 22
    target 335
  ]
  edge [
    source 22
    target 336
  ]
  edge [
    source 22
    target 337
  ]
  edge [
    source 22
    target 338
  ]
  edge [
    source 22
    target 339
  ]
  edge [
    source 22
    target 340
  ]
  edge [
    source 22
    target 341
  ]
  edge [
    source 22
    target 342
  ]
  edge [
    source 22
    target 343
  ]
  edge [
    source 22
    target 344
  ]
  edge [
    source 22
    target 345
  ]
  edge [
    source 22
    target 346
  ]
  edge [
    source 22
    target 347
  ]
  edge [
    source 22
    target 82
  ]
  edge [
    source 22
    target 348
  ]
  edge [
    source 22
    target 349
  ]
  edge [
    source 22
    target 350
  ]
  edge [
    source 22
    target 351
  ]
  edge [
    source 22
    target 352
  ]
  edge [
    source 22
    target 353
  ]
  edge [
    source 22
    target 354
  ]
  edge [
    source 22
    target 355
  ]
  edge [
    source 22
    target 356
  ]
  edge [
    source 22
    target 357
  ]
  edge [
    source 22
    target 358
  ]
  edge [
    source 22
    target 359
  ]
  edge [
    source 22
    target 360
  ]
  edge [
    source 22
    target 361
  ]
  edge [
    source 22
    target 362
  ]
  edge [
    source 22
    target 363
  ]
  edge [
    source 22
    target 364
  ]
  edge [
    source 22
    target 365
  ]
  edge [
    source 22
    target 366
  ]
  edge [
    source 22
    target 367
  ]
  edge [
    source 22
    target 368
  ]
  edge [
    source 22
    target 369
  ]
  edge [
    source 22
    target 370
  ]
  edge [
    source 22
    target 1715
  ]
  edge [
    source 22
    target 1716
  ]
  edge [
    source 22
    target 1717
  ]
  edge [
    source 22
    target 1718
  ]
  edge [
    source 22
    target 1719
  ]
  edge [
    source 22
    target 1720
  ]
  edge [
    source 22
    target 1721
  ]
  edge [
    source 22
    target 1722
  ]
  edge [
    source 22
    target 1723
  ]
  edge [
    source 22
    target 1724
  ]
  edge [
    source 22
    target 431
  ]
  edge [
    source 22
    target 1310
  ]
  edge [
    source 22
    target 1725
  ]
  edge [
    source 22
    target 870
  ]
  edge [
    source 22
    target 303
  ]
  edge [
    source 22
    target 1439
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1726
  ]
  edge [
    source 22
    target 1727
  ]
  edge [
    source 22
    target 1728
  ]
  edge [
    source 22
    target 1729
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 1730
  ]
  edge [
    source 22
    target 1731
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 1732
  ]
  edge [
    source 22
    target 1464
  ]
  edge [
    source 22
    target 1733
  ]
  edge [
    source 22
    target 304
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 1734
  ]
  edge [
    source 22
    target 50
  ]
  edge [
    source 22
    target 1735
  ]
  edge [
    source 22
    target 1573
  ]
  edge [
    source 22
    target 1736
  ]
  edge [
    source 22
    target 1737
  ]
  edge [
    source 22
    target 1738
  ]
  edge [
    source 22
    target 1739
  ]
  edge [
    source 22
    target 1740
  ]
  edge [
    source 22
    target 110
  ]
  edge [
    source 22
    target 909
  ]
  edge [
    source 22
    target 1443
  ]
  edge [
    source 22
    target 118
  ]
  edge [
    source 22
    target 1741
  ]
  edge [
    source 22
    target 1742
  ]
  edge [
    source 22
    target 1462
  ]
  edge [
    source 22
    target 119
  ]
  edge [
    source 22
    target 1743
  ]
  edge [
    source 22
    target 1744
  ]
  edge [
    source 22
    target 1223
  ]
  edge [
    source 22
    target 1745
  ]
  edge [
    source 22
    target 120
  ]
  edge [
    source 22
    target 1067
  ]
  edge [
    source 22
    target 1746
  ]
  edge [
    source 22
    target 1747
  ]
  edge [
    source 22
    target 1474
  ]
  edge [
    source 22
    target 96
  ]
  edge [
    source 22
    target 1328
  ]
  edge [
    source 22
    target 1748
  ]
  edge [
    source 22
    target 1749
  ]
  edge [
    source 22
    target 1109
  ]
  edge [
    source 22
    target 1750
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 1751
  ]
  edge [
    source 22
    target 1342
  ]
  edge [
    source 22
    target 1752
  ]
  edge [
    source 22
    target 1753
  ]
  edge [
    source 22
    target 1399
  ]
  edge [
    source 22
    target 1754
  ]
  edge [
    source 22
    target 1755
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 746
  ]
  edge [
    source 22
    target 1756
  ]
  edge [
    source 22
    target 1757
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 1758
  ]
  edge [
    source 22
    target 1759
  ]
  edge [
    source 22
    target 1760
  ]
  edge [
    source 22
    target 272
  ]
  edge [
    source 22
    target 273
  ]
  edge [
    source 22
    target 274
  ]
  edge [
    source 22
    target 275
  ]
  edge [
    source 22
    target 276
  ]
  edge [
    source 22
    target 277
  ]
  edge [
    source 22
    target 278
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 280
  ]
  edge [
    source 22
    target 281
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1761
  ]
  edge [
    source 23
    target 1762
  ]
  edge [
    source 23
    target 1763
  ]
  edge [
    source 23
    target 1764
  ]
  edge [
    source 23
    target 1765
  ]
  edge [
    source 23
    target 1766
  ]
  edge [
    source 23
    target 1767
  ]
  edge [
    source 23
    target 1768
  ]
  edge [
    source 23
    target 1769
  ]
  edge [
    source 23
    target 641
  ]
  edge [
    source 23
    target 1623
  ]
  edge [
    source 23
    target 1770
  ]
  edge [
    source 23
    target 1771
  ]
  edge [
    source 23
    target 1772
  ]
  edge [
    source 23
    target 1773
  ]
  edge [
    source 23
    target 1774
  ]
  edge [
    source 23
    target 1775
  ]
  edge [
    source 23
    target 1776
  ]
  edge [
    source 23
    target 1777
  ]
  edge [
    source 23
    target 1778
  ]
  edge [
    source 23
    target 1779
  ]
  edge [
    source 23
    target 1780
  ]
  edge [
    source 23
    target 1781
  ]
  edge [
    source 23
    target 1782
  ]
  edge [
    source 23
    target 1783
  ]
  edge [
    source 23
    target 1784
  ]
  edge [
    source 23
    target 1785
  ]
  edge [
    source 23
    target 1786
  ]
  edge [
    source 23
    target 1787
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 110
  ]
  edge [
    source 25
    target 1485
  ]
  edge [
    source 25
    target 118
  ]
  edge [
    source 25
    target 1788
  ]
  edge [
    source 25
    target 1789
  ]
  edge [
    source 25
    target 119
  ]
  edge [
    source 25
    target 120
  ]
  edge [
    source 25
    target 1790
  ]
  edge [
    source 25
    target 1791
  ]
  edge [
    source 25
    target 1792
  ]
  edge [
    source 25
    target 1793
  ]
  edge [
    source 25
    target 1794
  ]
  edge [
    source 25
    target 1795
  ]
  edge [
    source 25
    target 1796
  ]
  edge [
    source 25
    target 1797
  ]
  edge [
    source 25
    target 358
  ]
  edge [
    source 25
    target 1798
  ]
  edge [
    source 25
    target 1799
  ]
  edge [
    source 25
    target 303
  ]
  edge [
    source 25
    target 1729
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 25
    target 185
  ]
  edge [
    source 25
    target 1484
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 1257
    target 1803
  ]
  edge [
    source 1800
    target 1801
  ]
  edge [
    source 1800
    target 1802
  ]
  edge [
    source 1801
    target 1802
  ]
]
