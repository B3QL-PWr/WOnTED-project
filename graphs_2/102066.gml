graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "przyjemno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 4
    label "razem"
    origin "text"
  ]
  node [
    id 5
    label "albert"
    origin "text"
  ]
  node [
    id 6
    label "hup&#261;"
    origin "text"
  ]
  node [
    id 7
    label "isns"
    origin "text"
  ]
  node [
    id 8
    label "klub"
    origin "text"
  ]
  node [
    id 9
    label "dyskusyjny"
    origin "text"
  ]
  node [
    id 10
    label "technologia"
    origin "text"
  ]
  node [
    id 11
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 12
    label "plac"
    origin "text"
  ]
  node [
    id 13
    label "bliski"
    origin "text"
  ]
  node [
    id 14
    label "poniedzia&#322;ek"
    origin "text"
  ]
  node [
    id 15
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 16
    label "rozmowa"
    origin "text"
  ]
  node [
    id 17
    label "wszech&#347;wiat"
    origin "text"
  ]
  node [
    id 18
    label "by&#263;"
    origin "text"
  ]
  node [
    id 19
    label "komputer"
    origin "text"
  ]
  node [
    id 20
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 21
    label "henryk"
    origin "text"
  ]
  node [
    id 22
    label "paprocka"
    origin "text"
  ]
  node [
    id 23
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "koncepcja"
    origin "text"
  ]
  node [
    id 25
    label "niemiecki"
    origin "text"
  ]
  node [
    id 26
    label "pionier"
    origin "text"
  ]
  node [
    id 27
    label "cybernetyka"
    origin "text"
  ]
  node [
    id 28
    label "konrad"
    origin "text"
  ]
  node [
    id 29
    label "zuse"
    origin "text"
  ]
  node [
    id 30
    label "matematyka"
    origin "text"
  ]
  node [
    id 31
    label "informatyka"
    origin "text"
  ]
  node [
    id 32
    label "teologia"
    origin "text"
  ]
  node [
    id 33
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 34
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 35
    label "reszta"
    origin "text"
  ]
  node [
    id 36
    label "zaj&#281;cia"
    origin "text"
  ]
  node [
    id 37
    label "otwarty"
    origin "text"
  ]
  node [
    id 38
    label "dla"
    origin "text"
  ]
  node [
    id 39
    label "wszyscy"
    origin "text"
  ]
  node [
    id 40
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 41
    label "strona"
    origin "text"
  ]
  node [
    id 42
    label "internetowy"
    origin "text"
  ]
  node [
    id 43
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 44
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 45
    label "edytowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "ten"
    origin "text"
  ]
  node [
    id 47
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 48
    label "wsp&#243;lnie"
    origin "text"
  ]
  node [
    id 49
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "decyzja"
    origin "text"
  ]
  node [
    id 51
    label "kolejny"
    origin "text"
  ]
  node [
    id 52
    label "temat"
    origin "text"
  ]
  node [
    id 53
    label "go&#347;cia"
    origin "text"
  ]
  node [
    id 54
    label "znany"
    origin "text"
  ]
  node [
    id 55
    label "przodkini"
  ]
  node [
    id 56
    label "matka_zast&#281;pcza"
  ]
  node [
    id 57
    label "matczysko"
  ]
  node [
    id 58
    label "rodzice"
  ]
  node [
    id 59
    label "stara"
  ]
  node [
    id 60
    label "macierz"
  ]
  node [
    id 61
    label "rodzic"
  ]
  node [
    id 62
    label "Matka_Boska"
  ]
  node [
    id 63
    label "macocha"
  ]
  node [
    id 64
    label "starzy"
  ]
  node [
    id 65
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 66
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 67
    label "pokolenie"
  ]
  node [
    id 68
    label "wapniaki"
  ]
  node [
    id 69
    label "opiekun"
  ]
  node [
    id 70
    label "wapniak"
  ]
  node [
    id 71
    label "rodzic_chrzestny"
  ]
  node [
    id 72
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 73
    label "krewna"
  ]
  node [
    id 74
    label "matka"
  ]
  node [
    id 75
    label "&#380;ona"
  ]
  node [
    id 76
    label "kobieta"
  ]
  node [
    id 77
    label "partnerka"
  ]
  node [
    id 78
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 79
    label "matuszka"
  ]
  node [
    id 80
    label "parametryzacja"
  ]
  node [
    id 81
    label "pa&#324;stwo"
  ]
  node [
    id 82
    label "poj&#281;cie"
  ]
  node [
    id 83
    label "mod"
  ]
  node [
    id 84
    label "patriota"
  ]
  node [
    id 85
    label "m&#281;&#380;atka"
  ]
  node [
    id 86
    label "mutant"
  ]
  node [
    id 87
    label "doznanie"
  ]
  node [
    id 88
    label "dobrostan"
  ]
  node [
    id 89
    label "u&#380;ycie"
  ]
  node [
    id 90
    label "u&#380;y&#263;"
  ]
  node [
    id 91
    label "bawienie"
  ]
  node [
    id 92
    label "lubo&#347;&#263;"
  ]
  node [
    id 93
    label "u&#380;ywa&#263;"
  ]
  node [
    id 94
    label "prze&#380;ycie"
  ]
  node [
    id 95
    label "u&#380;ywanie"
  ]
  node [
    id 96
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 97
    label "wy&#347;wiadczenie"
  ]
  node [
    id 98
    label "zmys&#322;"
  ]
  node [
    id 99
    label "spotkanie"
  ]
  node [
    id 100
    label "czucie"
  ]
  node [
    id 101
    label "przeczulica"
  ]
  node [
    id 102
    label "poczucie"
  ]
  node [
    id 103
    label "wra&#380;enie"
  ]
  node [
    id 104
    label "przej&#347;cie"
  ]
  node [
    id 105
    label "poradzenie_sobie"
  ]
  node [
    id 106
    label "przetrwanie"
  ]
  node [
    id 107
    label "survival"
  ]
  node [
    id 108
    label "utilize"
  ]
  node [
    id 109
    label "seize"
  ]
  node [
    id 110
    label "zrobi&#263;"
  ]
  node [
    id 111
    label "dozna&#263;"
  ]
  node [
    id 112
    label "employment"
  ]
  node [
    id 113
    label "skorzysta&#263;"
  ]
  node [
    id 114
    label "wykorzysta&#263;"
  ]
  node [
    id 115
    label "stosowanie"
  ]
  node [
    id 116
    label "zabawa"
  ]
  node [
    id 117
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 118
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 119
    label "u&#380;yteczny"
  ]
  node [
    id 120
    label "enjoyment"
  ]
  node [
    id 121
    label "use"
  ]
  node [
    id 122
    label "zrobienie"
  ]
  node [
    id 123
    label "przejaskrawianie"
  ]
  node [
    id 124
    label "zniszczenie"
  ]
  node [
    id 125
    label "relish"
  ]
  node [
    id 126
    label "robienie"
  ]
  node [
    id 127
    label "exercise"
  ]
  node [
    id 128
    label "zaznawanie"
  ]
  node [
    id 129
    label "zu&#380;ywanie"
  ]
  node [
    id 130
    label "czynno&#347;&#263;"
  ]
  node [
    id 131
    label "cieszenie"
  ]
  node [
    id 132
    label "roz&#347;mieszenie"
  ]
  node [
    id 133
    label "pobawienie"
  ]
  node [
    id 134
    label "przebywanie"
  ]
  node [
    id 135
    label "&#347;mieszenie"
  ]
  node [
    id 136
    label "zajmowanie"
  ]
  node [
    id 137
    label "korzysta&#263;"
  ]
  node [
    id 138
    label "distribute"
  ]
  node [
    id 139
    label "give"
  ]
  node [
    id 140
    label "bash"
  ]
  node [
    id 141
    label "doznawa&#263;"
  ]
  node [
    id 142
    label "organizm"
  ]
  node [
    id 143
    label "&#380;y&#263;"
  ]
  node [
    id 144
    label "robi&#263;"
  ]
  node [
    id 145
    label "kierowa&#263;"
  ]
  node [
    id 146
    label "g&#243;rowa&#263;"
  ]
  node [
    id 147
    label "tworzy&#263;"
  ]
  node [
    id 148
    label "krzywa"
  ]
  node [
    id 149
    label "linia_melodyczna"
  ]
  node [
    id 150
    label "control"
  ]
  node [
    id 151
    label "string"
  ]
  node [
    id 152
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 153
    label "ukierunkowywa&#263;"
  ]
  node [
    id 154
    label "sterowa&#263;"
  ]
  node [
    id 155
    label "kre&#347;li&#263;"
  ]
  node [
    id 156
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 157
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 158
    label "message"
  ]
  node [
    id 159
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 160
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 161
    label "eksponowa&#263;"
  ]
  node [
    id 162
    label "navigate"
  ]
  node [
    id 163
    label "manipulate"
  ]
  node [
    id 164
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 165
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 166
    label "przesuwa&#263;"
  ]
  node [
    id 167
    label "partner"
  ]
  node [
    id 168
    label "prowadzenie"
  ]
  node [
    id 169
    label "powodowa&#263;"
  ]
  node [
    id 170
    label "mie&#263;_miejsce"
  ]
  node [
    id 171
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 172
    label "motywowa&#263;"
  ]
  node [
    id 173
    label "act"
  ]
  node [
    id 174
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 175
    label "organizowa&#263;"
  ]
  node [
    id 176
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 177
    label "czyni&#263;"
  ]
  node [
    id 178
    label "stylizowa&#263;"
  ]
  node [
    id 179
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 180
    label "falowa&#263;"
  ]
  node [
    id 181
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 182
    label "peddle"
  ]
  node [
    id 183
    label "praca"
  ]
  node [
    id 184
    label "wydala&#263;"
  ]
  node [
    id 185
    label "tentegowa&#263;"
  ]
  node [
    id 186
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 187
    label "urz&#261;dza&#263;"
  ]
  node [
    id 188
    label "oszukiwa&#263;"
  ]
  node [
    id 189
    label "work"
  ]
  node [
    id 190
    label "ukazywa&#263;"
  ]
  node [
    id 191
    label "przerabia&#263;"
  ]
  node [
    id 192
    label "post&#281;powa&#263;"
  ]
  node [
    id 193
    label "draw"
  ]
  node [
    id 194
    label "clear"
  ]
  node [
    id 195
    label "usuwa&#263;"
  ]
  node [
    id 196
    label "report"
  ]
  node [
    id 197
    label "rysowa&#263;"
  ]
  node [
    id 198
    label "describe"
  ]
  node [
    id 199
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 200
    label "przedstawia&#263;"
  ]
  node [
    id 201
    label "delineate"
  ]
  node [
    id 202
    label "pope&#322;nia&#263;"
  ]
  node [
    id 203
    label "wytwarza&#263;"
  ]
  node [
    id 204
    label "get"
  ]
  node [
    id 205
    label "consist"
  ]
  node [
    id 206
    label "stanowi&#263;"
  ]
  node [
    id 207
    label "raise"
  ]
  node [
    id 208
    label "podkre&#347;la&#263;"
  ]
  node [
    id 209
    label "demonstrowa&#263;"
  ]
  node [
    id 210
    label "unwrap"
  ]
  node [
    id 211
    label "napromieniowywa&#263;"
  ]
  node [
    id 212
    label "trzyma&#263;"
  ]
  node [
    id 213
    label "manipulowa&#263;"
  ]
  node [
    id 214
    label "wysy&#322;a&#263;"
  ]
  node [
    id 215
    label "zwierzchnik"
  ]
  node [
    id 216
    label "ustawia&#263;"
  ]
  node [
    id 217
    label "przeznacza&#263;"
  ]
  node [
    id 218
    label "match"
  ]
  node [
    id 219
    label "administrowa&#263;"
  ]
  node [
    id 220
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 221
    label "order"
  ]
  node [
    id 222
    label "indicate"
  ]
  node [
    id 223
    label "undertaking"
  ]
  node [
    id 224
    label "base_on_balls"
  ]
  node [
    id 225
    label "wyprzedza&#263;"
  ]
  node [
    id 226
    label "wygrywa&#263;"
  ]
  node [
    id 227
    label "chop"
  ]
  node [
    id 228
    label "przekracza&#263;"
  ]
  node [
    id 229
    label "treat"
  ]
  node [
    id 230
    label "zaspokaja&#263;"
  ]
  node [
    id 231
    label "suffice"
  ]
  node [
    id 232
    label "zaspakaja&#263;"
  ]
  node [
    id 233
    label "uprawia&#263;_seks"
  ]
  node [
    id 234
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 235
    label "serve"
  ]
  node [
    id 236
    label "dostosowywa&#263;"
  ]
  node [
    id 237
    label "estrange"
  ]
  node [
    id 238
    label "transfer"
  ]
  node [
    id 239
    label "translate"
  ]
  node [
    id 240
    label "go"
  ]
  node [
    id 241
    label "zmienia&#263;"
  ]
  node [
    id 242
    label "postpone"
  ]
  node [
    id 243
    label "przestawia&#263;"
  ]
  node [
    id 244
    label "rusza&#263;"
  ]
  node [
    id 245
    label "przenosi&#263;"
  ]
  node [
    id 246
    label "marshal"
  ]
  node [
    id 247
    label "wyznacza&#263;"
  ]
  node [
    id 248
    label "nadawa&#263;"
  ]
  node [
    id 249
    label "shape"
  ]
  node [
    id 250
    label "istnie&#263;"
  ]
  node [
    id 251
    label "pause"
  ]
  node [
    id 252
    label "stay"
  ]
  node [
    id 253
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 254
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 255
    label "dane"
  ]
  node [
    id 256
    label "klawisz"
  ]
  node [
    id 257
    label "figura_geometryczna"
  ]
  node [
    id 258
    label "linia"
  ]
  node [
    id 259
    label "poprowadzi&#263;"
  ]
  node [
    id 260
    label "curvature"
  ]
  node [
    id 261
    label "curve"
  ]
  node [
    id 262
    label "wystawa&#263;"
  ]
  node [
    id 263
    label "sprout"
  ]
  node [
    id 264
    label "dysponowanie"
  ]
  node [
    id 265
    label "sterowanie"
  ]
  node [
    id 266
    label "powodowanie"
  ]
  node [
    id 267
    label "management"
  ]
  node [
    id 268
    label "kierowanie"
  ]
  node [
    id 269
    label "ukierunkowywanie"
  ]
  node [
    id 270
    label "przywodzenie"
  ]
  node [
    id 271
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 272
    label "doprowadzanie"
  ]
  node [
    id 273
    label "kre&#347;lenie"
  ]
  node [
    id 274
    label "lead"
  ]
  node [
    id 275
    label "eksponowanie"
  ]
  node [
    id 276
    label "prowadzanie"
  ]
  node [
    id 277
    label "wprowadzanie"
  ]
  node [
    id 278
    label "doprowadzenie"
  ]
  node [
    id 279
    label "poprowadzenie"
  ]
  node [
    id 280
    label "kszta&#322;towanie"
  ]
  node [
    id 281
    label "aim"
  ]
  node [
    id 282
    label "zwracanie"
  ]
  node [
    id 283
    label "przecinanie"
  ]
  node [
    id 284
    label "ta&#324;czenie"
  ]
  node [
    id 285
    label "przewy&#380;szanie"
  ]
  node [
    id 286
    label "g&#243;rowanie"
  ]
  node [
    id 287
    label "zaprowadzanie"
  ]
  node [
    id 288
    label "dawanie"
  ]
  node [
    id 289
    label "trzymanie"
  ]
  node [
    id 290
    label "oprowadzanie"
  ]
  node [
    id 291
    label "wprowadzenie"
  ]
  node [
    id 292
    label "drive"
  ]
  node [
    id 293
    label "oprowadzenie"
  ]
  node [
    id 294
    label "przeci&#281;cie"
  ]
  node [
    id 295
    label "przeci&#261;ganie"
  ]
  node [
    id 296
    label "pozarz&#261;dzanie"
  ]
  node [
    id 297
    label "granie"
  ]
  node [
    id 298
    label "pracownik"
  ]
  node [
    id 299
    label "przedsi&#281;biorca"
  ]
  node [
    id 300
    label "cz&#322;owiek"
  ]
  node [
    id 301
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 302
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 303
    label "kolaborator"
  ]
  node [
    id 304
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 305
    label "sp&#243;lnik"
  ]
  node [
    id 306
    label "aktor"
  ]
  node [
    id 307
    label "uczestniczenie"
  ]
  node [
    id 308
    label "doba"
  ]
  node [
    id 309
    label "weekend"
  ]
  node [
    id 310
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 311
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 312
    label "czas"
  ]
  node [
    id 313
    label "miesi&#261;c"
  ]
  node [
    id 314
    label "poprzedzanie"
  ]
  node [
    id 315
    label "czasoprzestrze&#324;"
  ]
  node [
    id 316
    label "laba"
  ]
  node [
    id 317
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 318
    label "chronometria"
  ]
  node [
    id 319
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 320
    label "rachuba_czasu"
  ]
  node [
    id 321
    label "przep&#322;ywanie"
  ]
  node [
    id 322
    label "czasokres"
  ]
  node [
    id 323
    label "odczyt"
  ]
  node [
    id 324
    label "chwila"
  ]
  node [
    id 325
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 326
    label "dzieje"
  ]
  node [
    id 327
    label "kategoria_gramatyczna"
  ]
  node [
    id 328
    label "poprzedzenie"
  ]
  node [
    id 329
    label "trawienie"
  ]
  node [
    id 330
    label "pochodzi&#263;"
  ]
  node [
    id 331
    label "period"
  ]
  node [
    id 332
    label "okres_czasu"
  ]
  node [
    id 333
    label "poprzedza&#263;"
  ]
  node [
    id 334
    label "schy&#322;ek"
  ]
  node [
    id 335
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 336
    label "odwlekanie_si&#281;"
  ]
  node [
    id 337
    label "zegar"
  ]
  node [
    id 338
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 339
    label "czwarty_wymiar"
  ]
  node [
    id 340
    label "pochodzenie"
  ]
  node [
    id 341
    label "koniugacja"
  ]
  node [
    id 342
    label "Zeitgeist"
  ]
  node [
    id 343
    label "trawi&#263;"
  ]
  node [
    id 344
    label "pogoda"
  ]
  node [
    id 345
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 346
    label "poprzedzi&#263;"
  ]
  node [
    id 347
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 348
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 349
    label "time_period"
  ]
  node [
    id 350
    label "noc"
  ]
  node [
    id 351
    label "dzie&#324;"
  ]
  node [
    id 352
    label "godzina"
  ]
  node [
    id 353
    label "long_time"
  ]
  node [
    id 354
    label "jednostka_geologiczna"
  ]
  node [
    id 355
    label "niedziela"
  ]
  node [
    id 356
    label "sobota"
  ]
  node [
    id 357
    label "miech"
  ]
  node [
    id 358
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 359
    label "rok"
  ]
  node [
    id 360
    label "kalendy"
  ]
  node [
    id 361
    label "&#322;&#261;cznie"
  ]
  node [
    id 362
    label "&#322;&#261;czny"
  ]
  node [
    id 363
    label "zbiorczo"
  ]
  node [
    id 364
    label "od&#322;am"
  ]
  node [
    id 365
    label "siedziba"
  ]
  node [
    id 366
    label "society"
  ]
  node [
    id 367
    label "bar"
  ]
  node [
    id 368
    label "jakobini"
  ]
  node [
    id 369
    label "lokal"
  ]
  node [
    id 370
    label "stowarzyszenie"
  ]
  node [
    id 371
    label "klubista"
  ]
  node [
    id 372
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 373
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 374
    label "Chewra_Kadisza"
  ]
  node [
    id 375
    label "organizacja"
  ]
  node [
    id 376
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 377
    label "Rotary_International"
  ]
  node [
    id 378
    label "fabianie"
  ]
  node [
    id 379
    label "Eleusis"
  ]
  node [
    id 380
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 381
    label "Monar"
  ]
  node [
    id 382
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 383
    label "grupa"
  ]
  node [
    id 384
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 385
    label "&#321;ubianka"
  ]
  node [
    id 386
    label "miejsce_pracy"
  ]
  node [
    id 387
    label "dzia&#322;_personalny"
  ]
  node [
    id 388
    label "Kreml"
  ]
  node [
    id 389
    label "Bia&#322;y_Dom"
  ]
  node [
    id 390
    label "budynek"
  ]
  node [
    id 391
    label "miejsce"
  ]
  node [
    id 392
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 393
    label "sadowisko"
  ]
  node [
    id 394
    label "kawa&#322;"
  ]
  node [
    id 395
    label "bry&#322;a"
  ]
  node [
    id 396
    label "fragment"
  ]
  node [
    id 397
    label "struktura_geologiczna"
  ]
  node [
    id 398
    label "dzia&#322;"
  ]
  node [
    id 399
    label "section"
  ]
  node [
    id 400
    label "gastronomia"
  ]
  node [
    id 401
    label "zak&#322;ad"
  ]
  node [
    id 402
    label "cz&#322;onek"
  ]
  node [
    id 403
    label "lada"
  ]
  node [
    id 404
    label "blat"
  ]
  node [
    id 405
    label "berylowiec"
  ]
  node [
    id 406
    label "milibar"
  ]
  node [
    id 407
    label "kawiarnia"
  ]
  node [
    id 408
    label "buffet"
  ]
  node [
    id 409
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 410
    label "mikrobar"
  ]
  node [
    id 411
    label "kontrowersyjnie"
  ]
  node [
    id 412
    label "dyskusyjnie"
  ]
  node [
    id 413
    label "w&#261;tpliwy"
  ]
  node [
    id 414
    label "w&#261;tpliwie"
  ]
  node [
    id 415
    label "pozorny"
  ]
  node [
    id 416
    label "controversially"
  ]
  node [
    id 417
    label "kontrowersyjny"
  ]
  node [
    id 418
    label "technika"
  ]
  node [
    id 419
    label "mikrotechnologia"
  ]
  node [
    id 420
    label "technologia_nieorganiczna"
  ]
  node [
    id 421
    label "engineering"
  ]
  node [
    id 422
    label "biotechnologia"
  ]
  node [
    id 423
    label "model"
  ]
  node [
    id 424
    label "narz&#281;dzie"
  ]
  node [
    id 425
    label "zbi&#243;r"
  ]
  node [
    id 426
    label "tryb"
  ]
  node [
    id 427
    label "nature"
  ]
  node [
    id 428
    label "nauka"
  ]
  node [
    id 429
    label "in&#380;ynieria_genetyczna"
  ]
  node [
    id 430
    label "bioin&#380;ynieria"
  ]
  node [
    id 431
    label "telekomunikacja"
  ]
  node [
    id 432
    label "cywilizacja"
  ]
  node [
    id 433
    label "przedmiot"
  ]
  node [
    id 434
    label "wiedza"
  ]
  node [
    id 435
    label "sprawno&#347;&#263;"
  ]
  node [
    id 436
    label "fotowoltaika"
  ]
  node [
    id 437
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 438
    label "teletechnika"
  ]
  node [
    id 439
    label "mechanika_precyzyjna"
  ]
  node [
    id 440
    label "pole"
  ]
  node [
    id 441
    label "elita"
  ]
  node [
    id 442
    label "status"
  ]
  node [
    id 443
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 444
    label "aspo&#322;eczny"
  ]
  node [
    id 445
    label "ludzie_pracy"
  ]
  node [
    id 446
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 447
    label "pozaklasowy"
  ]
  node [
    id 448
    label "uwarstwienie"
  ]
  node [
    id 449
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 450
    label "community"
  ]
  node [
    id 451
    label "klasa"
  ]
  node [
    id 452
    label "kastowo&#347;&#263;"
  ]
  node [
    id 453
    label "facylitacja"
  ]
  node [
    id 454
    label "elite"
  ]
  node [
    id 455
    label "&#347;rodowisko"
  ]
  node [
    id 456
    label "wagon"
  ]
  node [
    id 457
    label "mecz_mistrzowski"
  ]
  node [
    id 458
    label "arrangement"
  ]
  node [
    id 459
    label "class"
  ]
  node [
    id 460
    label "&#322;awka"
  ]
  node [
    id 461
    label "wykrzyknik"
  ]
  node [
    id 462
    label "zaleta"
  ]
  node [
    id 463
    label "jednostka_systematyczna"
  ]
  node [
    id 464
    label "programowanie_obiektowe"
  ]
  node [
    id 465
    label "tablica"
  ]
  node [
    id 466
    label "warstwa"
  ]
  node [
    id 467
    label "rezerwa"
  ]
  node [
    id 468
    label "gromada"
  ]
  node [
    id 469
    label "Ekwici"
  ]
  node [
    id 470
    label "szko&#322;a"
  ]
  node [
    id 471
    label "sala"
  ]
  node [
    id 472
    label "pomoc"
  ]
  node [
    id 473
    label "form"
  ]
  node [
    id 474
    label "przepisa&#263;"
  ]
  node [
    id 475
    label "jako&#347;&#263;"
  ]
  node [
    id 476
    label "znak_jako&#347;ci"
  ]
  node [
    id 477
    label "poziom"
  ]
  node [
    id 478
    label "type"
  ]
  node [
    id 479
    label "promocja"
  ]
  node [
    id 480
    label "przepisanie"
  ]
  node [
    id 481
    label "kurs"
  ]
  node [
    id 482
    label "obiekt"
  ]
  node [
    id 483
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 484
    label "dziennik_lekcyjny"
  ]
  node [
    id 485
    label "typ"
  ]
  node [
    id 486
    label "fakcja"
  ]
  node [
    id 487
    label "obrona"
  ]
  node [
    id 488
    label "atak"
  ]
  node [
    id 489
    label "botanika"
  ]
  node [
    id 490
    label "uprawienie"
  ]
  node [
    id 491
    label "u&#322;o&#380;enie"
  ]
  node [
    id 492
    label "p&#322;osa"
  ]
  node [
    id 493
    label "ziemia"
  ]
  node [
    id 494
    label "cecha"
  ]
  node [
    id 495
    label "t&#322;o"
  ]
  node [
    id 496
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 497
    label "gospodarstwo"
  ]
  node [
    id 498
    label "uprawi&#263;"
  ]
  node [
    id 499
    label "room"
  ]
  node [
    id 500
    label "dw&#243;r"
  ]
  node [
    id 501
    label "okazja"
  ]
  node [
    id 502
    label "rozmiar"
  ]
  node [
    id 503
    label "irygowanie"
  ]
  node [
    id 504
    label "compass"
  ]
  node [
    id 505
    label "square"
  ]
  node [
    id 506
    label "zmienna"
  ]
  node [
    id 507
    label "irygowa&#263;"
  ]
  node [
    id 508
    label "socjologia"
  ]
  node [
    id 509
    label "boisko"
  ]
  node [
    id 510
    label "dziedzina"
  ]
  node [
    id 511
    label "baza_danych"
  ]
  node [
    id 512
    label "region"
  ]
  node [
    id 513
    label "przestrze&#324;"
  ]
  node [
    id 514
    label "zagon"
  ]
  node [
    id 515
    label "obszar"
  ]
  node [
    id 516
    label "sk&#322;ad"
  ]
  node [
    id 517
    label "powierzchnia"
  ]
  node [
    id 518
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 519
    label "plane"
  ]
  node [
    id 520
    label "radlina"
  ]
  node [
    id 521
    label "Fremeni"
  ]
  node [
    id 522
    label "niskogatunkowy"
  ]
  node [
    id 523
    label "condition"
  ]
  node [
    id 524
    label "awansowa&#263;"
  ]
  node [
    id 525
    label "znaczenie"
  ]
  node [
    id 526
    label "stan"
  ]
  node [
    id 527
    label "awans"
  ]
  node [
    id 528
    label "podmiotowo"
  ]
  node [
    id 529
    label "awansowanie"
  ]
  node [
    id 530
    label "sytuacja"
  ]
  node [
    id 531
    label "niekorzystny"
  ]
  node [
    id 532
    label "aspo&#322;ecznie"
  ]
  node [
    id 533
    label "typowy"
  ]
  node [
    id 534
    label "niech&#281;tny"
  ]
  node [
    id 535
    label "asymilowanie_si&#281;"
  ]
  node [
    id 536
    label "Wsch&#243;d"
  ]
  node [
    id 537
    label "przejmowanie"
  ]
  node [
    id 538
    label "zjawisko"
  ]
  node [
    id 539
    label "rzecz"
  ]
  node [
    id 540
    label "makrokosmos"
  ]
  node [
    id 541
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 542
    label "civilization"
  ]
  node [
    id 543
    label "przejmowa&#263;"
  ]
  node [
    id 544
    label "faza"
  ]
  node [
    id 545
    label "kuchnia"
  ]
  node [
    id 546
    label "rozw&#243;j"
  ]
  node [
    id 547
    label "populace"
  ]
  node [
    id 548
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 549
    label "przej&#281;cie"
  ]
  node [
    id 550
    label "przej&#261;&#263;"
  ]
  node [
    id 551
    label "cywilizowanie"
  ]
  node [
    id 552
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 553
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 554
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 555
    label "struktura"
  ]
  node [
    id 556
    label "stratification"
  ]
  node [
    id 557
    label "lamination"
  ]
  node [
    id 558
    label "podzia&#322;"
  ]
  node [
    id 559
    label "area"
  ]
  node [
    id 560
    label "Majdan"
  ]
  node [
    id 561
    label "pole_bitwy"
  ]
  node [
    id 562
    label "stoisko"
  ]
  node [
    id 563
    label "pierzeja"
  ]
  node [
    id 564
    label "obiekt_handlowy"
  ]
  node [
    id 565
    label "zgromadzenie"
  ]
  node [
    id 566
    label "miasto"
  ]
  node [
    id 567
    label "targowica"
  ]
  node [
    id 568
    label "kram"
  ]
  node [
    id 569
    label "p&#243;&#322;noc"
  ]
  node [
    id 570
    label "Kosowo"
  ]
  node [
    id 571
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 572
    label "Zab&#322;ocie"
  ]
  node [
    id 573
    label "zach&#243;d"
  ]
  node [
    id 574
    label "po&#322;udnie"
  ]
  node [
    id 575
    label "Pow&#261;zki"
  ]
  node [
    id 576
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 577
    label "Piotrowo"
  ]
  node [
    id 578
    label "Olszanica"
  ]
  node [
    id 579
    label "Ruda_Pabianicka"
  ]
  node [
    id 580
    label "holarktyka"
  ]
  node [
    id 581
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 582
    label "Ludwin&#243;w"
  ]
  node [
    id 583
    label "Arktyka"
  ]
  node [
    id 584
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 585
    label "Zabu&#380;e"
  ]
  node [
    id 586
    label "antroposfera"
  ]
  node [
    id 587
    label "Neogea"
  ]
  node [
    id 588
    label "terytorium"
  ]
  node [
    id 589
    label "Syberia_Zachodnia"
  ]
  node [
    id 590
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 591
    label "zakres"
  ]
  node [
    id 592
    label "pas_planetoid"
  ]
  node [
    id 593
    label "Syberia_Wschodnia"
  ]
  node [
    id 594
    label "Antarktyka"
  ]
  node [
    id 595
    label "Rakowice"
  ]
  node [
    id 596
    label "akrecja"
  ]
  node [
    id 597
    label "wymiar"
  ]
  node [
    id 598
    label "&#321;&#281;g"
  ]
  node [
    id 599
    label "Kresy_Zachodnie"
  ]
  node [
    id 600
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 601
    label "wsch&#243;d"
  ]
  node [
    id 602
    label "Notogea"
  ]
  node [
    id 603
    label "rozdzielanie"
  ]
  node [
    id 604
    label "bezbrze&#380;e"
  ]
  node [
    id 605
    label "punkt"
  ]
  node [
    id 606
    label "niezmierzony"
  ]
  node [
    id 607
    label "przedzielenie"
  ]
  node [
    id 608
    label "nielito&#347;ciwy"
  ]
  node [
    id 609
    label "rozdziela&#263;"
  ]
  node [
    id 610
    label "oktant"
  ]
  node [
    id 611
    label "przedzieli&#263;"
  ]
  node [
    id 612
    label "przestw&#243;r"
  ]
  node [
    id 613
    label "concourse"
  ]
  node [
    id 614
    label "gathering"
  ]
  node [
    id 615
    label "skupienie"
  ]
  node [
    id 616
    label "wsp&#243;lnota"
  ]
  node [
    id 617
    label "spowodowanie"
  ]
  node [
    id 618
    label "organ"
  ]
  node [
    id 619
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 620
    label "gromadzenie"
  ]
  node [
    id 621
    label "templum"
  ]
  node [
    id 622
    label "konwentykiel"
  ]
  node [
    id 623
    label "klasztor"
  ]
  node [
    id 624
    label "caucus"
  ]
  node [
    id 625
    label "pozyskanie"
  ]
  node [
    id 626
    label "kongregacja"
  ]
  node [
    id 627
    label "ulica"
  ]
  node [
    id 628
    label "targ"
  ]
  node [
    id 629
    label "szmartuz"
  ]
  node [
    id 630
    label "kramnica"
  ]
  node [
    id 631
    label "Brunszwik"
  ]
  node [
    id 632
    label "Twer"
  ]
  node [
    id 633
    label "Marki"
  ]
  node [
    id 634
    label "Tarnopol"
  ]
  node [
    id 635
    label "Czerkiesk"
  ]
  node [
    id 636
    label "Johannesburg"
  ]
  node [
    id 637
    label "Nowogr&#243;d"
  ]
  node [
    id 638
    label "Heidelberg"
  ]
  node [
    id 639
    label "Korsze"
  ]
  node [
    id 640
    label "Chocim"
  ]
  node [
    id 641
    label "Lenzen"
  ]
  node [
    id 642
    label "Bie&#322;gorod"
  ]
  node [
    id 643
    label "Hebron"
  ]
  node [
    id 644
    label "Korynt"
  ]
  node [
    id 645
    label "Pemba"
  ]
  node [
    id 646
    label "Norfolk"
  ]
  node [
    id 647
    label "Tarragona"
  ]
  node [
    id 648
    label "Loreto"
  ]
  node [
    id 649
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 650
    label "Paczk&#243;w"
  ]
  node [
    id 651
    label "Krasnodar"
  ]
  node [
    id 652
    label "Hadziacz"
  ]
  node [
    id 653
    label "Cymlansk"
  ]
  node [
    id 654
    label "Efez"
  ]
  node [
    id 655
    label "Kandahar"
  ]
  node [
    id 656
    label "&#346;wiebodzice"
  ]
  node [
    id 657
    label "Antwerpia"
  ]
  node [
    id 658
    label "Baltimore"
  ]
  node [
    id 659
    label "Eger"
  ]
  node [
    id 660
    label "Cumana"
  ]
  node [
    id 661
    label "Kanton"
  ]
  node [
    id 662
    label "Sarat&#243;w"
  ]
  node [
    id 663
    label "Siena"
  ]
  node [
    id 664
    label "Dubno"
  ]
  node [
    id 665
    label "Tyl&#380;a"
  ]
  node [
    id 666
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 667
    label "Pi&#324;sk"
  ]
  node [
    id 668
    label "Toledo"
  ]
  node [
    id 669
    label "Piza"
  ]
  node [
    id 670
    label "Triest"
  ]
  node [
    id 671
    label "Struga"
  ]
  node [
    id 672
    label "Gettysburg"
  ]
  node [
    id 673
    label "Sierdobsk"
  ]
  node [
    id 674
    label "Xai-Xai"
  ]
  node [
    id 675
    label "Bristol"
  ]
  node [
    id 676
    label "Katania"
  ]
  node [
    id 677
    label "Parma"
  ]
  node [
    id 678
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 679
    label "Dniepropetrowsk"
  ]
  node [
    id 680
    label "Tours"
  ]
  node [
    id 681
    label "Mohylew"
  ]
  node [
    id 682
    label "Suzdal"
  ]
  node [
    id 683
    label "Samara"
  ]
  node [
    id 684
    label "Akerman"
  ]
  node [
    id 685
    label "Szk&#322;&#243;w"
  ]
  node [
    id 686
    label "Chimoio"
  ]
  node [
    id 687
    label "Perm"
  ]
  node [
    id 688
    label "Murma&#324;sk"
  ]
  node [
    id 689
    label "Z&#322;oczew"
  ]
  node [
    id 690
    label "Reda"
  ]
  node [
    id 691
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 692
    label "Aleksandria"
  ]
  node [
    id 693
    label "Kowel"
  ]
  node [
    id 694
    label "Hamburg"
  ]
  node [
    id 695
    label "Rudki"
  ]
  node [
    id 696
    label "O&#322;omuniec"
  ]
  node [
    id 697
    label "Kowno"
  ]
  node [
    id 698
    label "Luksor"
  ]
  node [
    id 699
    label "Cremona"
  ]
  node [
    id 700
    label "Suczawa"
  ]
  node [
    id 701
    label "M&#252;nster"
  ]
  node [
    id 702
    label "Peszawar"
  ]
  node [
    id 703
    label "Los_Angeles"
  ]
  node [
    id 704
    label "Szawle"
  ]
  node [
    id 705
    label "Winnica"
  ]
  node [
    id 706
    label "I&#322;awka"
  ]
  node [
    id 707
    label "Poniatowa"
  ]
  node [
    id 708
    label "Ko&#322;omyja"
  ]
  node [
    id 709
    label "Asy&#380;"
  ]
  node [
    id 710
    label "Tolkmicko"
  ]
  node [
    id 711
    label "Orlean"
  ]
  node [
    id 712
    label "Koper"
  ]
  node [
    id 713
    label "Le&#324;sk"
  ]
  node [
    id 714
    label "Rostock"
  ]
  node [
    id 715
    label "Mantua"
  ]
  node [
    id 716
    label "Barcelona"
  ]
  node [
    id 717
    label "Mo&#347;ciska"
  ]
  node [
    id 718
    label "Koluszki"
  ]
  node [
    id 719
    label "Stalingrad"
  ]
  node [
    id 720
    label "Fergana"
  ]
  node [
    id 721
    label "A&#322;czewsk"
  ]
  node [
    id 722
    label "Kaszyn"
  ]
  node [
    id 723
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 724
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 725
    label "D&#252;sseldorf"
  ]
  node [
    id 726
    label "Mozyrz"
  ]
  node [
    id 727
    label "Syrakuzy"
  ]
  node [
    id 728
    label "Peszt"
  ]
  node [
    id 729
    label "Lichinga"
  ]
  node [
    id 730
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 731
    label "Choroszcz"
  ]
  node [
    id 732
    label "Po&#322;ock"
  ]
  node [
    id 733
    label "Cherso&#324;"
  ]
  node [
    id 734
    label "Fryburg"
  ]
  node [
    id 735
    label "Izmir"
  ]
  node [
    id 736
    label "Jawor&#243;w"
  ]
  node [
    id 737
    label "Wenecja"
  ]
  node [
    id 738
    label "Kordoba"
  ]
  node [
    id 739
    label "Mrocza"
  ]
  node [
    id 740
    label "Solikamsk"
  ]
  node [
    id 741
    label "Be&#322;z"
  ]
  node [
    id 742
    label "Wo&#322;gograd"
  ]
  node [
    id 743
    label "&#379;ar&#243;w"
  ]
  node [
    id 744
    label "Brugia"
  ]
  node [
    id 745
    label "Radk&#243;w"
  ]
  node [
    id 746
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 747
    label "Harbin"
  ]
  node [
    id 748
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 749
    label "Zaporo&#380;e"
  ]
  node [
    id 750
    label "Smorgonie"
  ]
  node [
    id 751
    label "Nowa_D&#281;ba"
  ]
  node [
    id 752
    label "Aktobe"
  ]
  node [
    id 753
    label "Ussuryjsk"
  ]
  node [
    id 754
    label "Mo&#380;ajsk"
  ]
  node [
    id 755
    label "Tanger"
  ]
  node [
    id 756
    label "Nowogard"
  ]
  node [
    id 757
    label "Utrecht"
  ]
  node [
    id 758
    label "Czerniejewo"
  ]
  node [
    id 759
    label "Bazylea"
  ]
  node [
    id 760
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 761
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 762
    label "Tu&#322;a"
  ]
  node [
    id 763
    label "Al-Kufa"
  ]
  node [
    id 764
    label "Jutrosin"
  ]
  node [
    id 765
    label "Czelabi&#324;sk"
  ]
  node [
    id 766
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 767
    label "Split"
  ]
  node [
    id 768
    label "Czerniowce"
  ]
  node [
    id 769
    label "Majsur"
  ]
  node [
    id 770
    label "Poczdam"
  ]
  node [
    id 771
    label "Troick"
  ]
  node [
    id 772
    label "Minusi&#324;sk"
  ]
  node [
    id 773
    label "Kostroma"
  ]
  node [
    id 774
    label "Barwice"
  ]
  node [
    id 775
    label "U&#322;an_Ude"
  ]
  node [
    id 776
    label "Czeskie_Budziejowice"
  ]
  node [
    id 777
    label "Getynga"
  ]
  node [
    id 778
    label "Kercz"
  ]
  node [
    id 779
    label "B&#322;aszki"
  ]
  node [
    id 780
    label "Lipawa"
  ]
  node [
    id 781
    label "Bujnaksk"
  ]
  node [
    id 782
    label "Wittenberga"
  ]
  node [
    id 783
    label "Gorycja"
  ]
  node [
    id 784
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 785
    label "Swatowe"
  ]
  node [
    id 786
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 787
    label "Magadan"
  ]
  node [
    id 788
    label "Rzg&#243;w"
  ]
  node [
    id 789
    label "Bijsk"
  ]
  node [
    id 790
    label "Norylsk"
  ]
  node [
    id 791
    label "Mesyna"
  ]
  node [
    id 792
    label "Berezyna"
  ]
  node [
    id 793
    label "Stawropol"
  ]
  node [
    id 794
    label "Kircholm"
  ]
  node [
    id 795
    label "Hawana"
  ]
  node [
    id 796
    label "Pardubice"
  ]
  node [
    id 797
    label "Drezno"
  ]
  node [
    id 798
    label "Zaklik&#243;w"
  ]
  node [
    id 799
    label "Kozielsk"
  ]
  node [
    id 800
    label "Paw&#322;owo"
  ]
  node [
    id 801
    label "Kani&#243;w"
  ]
  node [
    id 802
    label "Adana"
  ]
  node [
    id 803
    label "Kleczew"
  ]
  node [
    id 804
    label "Rybi&#324;sk"
  ]
  node [
    id 805
    label "Dayton"
  ]
  node [
    id 806
    label "Nowy_Orlean"
  ]
  node [
    id 807
    label "Perejas&#322;aw"
  ]
  node [
    id 808
    label "Jenisejsk"
  ]
  node [
    id 809
    label "Bolonia"
  ]
  node [
    id 810
    label "Bir&#380;e"
  ]
  node [
    id 811
    label "Marsylia"
  ]
  node [
    id 812
    label "Workuta"
  ]
  node [
    id 813
    label "Sewilla"
  ]
  node [
    id 814
    label "Megara"
  ]
  node [
    id 815
    label "Gotha"
  ]
  node [
    id 816
    label "Kiejdany"
  ]
  node [
    id 817
    label "Zaleszczyki"
  ]
  node [
    id 818
    label "Ja&#322;ta"
  ]
  node [
    id 819
    label "Burgas"
  ]
  node [
    id 820
    label "Essen"
  ]
  node [
    id 821
    label "Czadca"
  ]
  node [
    id 822
    label "Manchester"
  ]
  node [
    id 823
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 824
    label "Schmalkalden"
  ]
  node [
    id 825
    label "Oleszyce"
  ]
  node [
    id 826
    label "Kie&#380;mark"
  ]
  node [
    id 827
    label "Kleck"
  ]
  node [
    id 828
    label "Suez"
  ]
  node [
    id 829
    label "Brack"
  ]
  node [
    id 830
    label "Symferopol"
  ]
  node [
    id 831
    label "Michalovce"
  ]
  node [
    id 832
    label "Tambow"
  ]
  node [
    id 833
    label "Turkmenbaszy"
  ]
  node [
    id 834
    label "Bogumin"
  ]
  node [
    id 835
    label "Sambor"
  ]
  node [
    id 836
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 837
    label "Milan&#243;wek"
  ]
  node [
    id 838
    label "Nachiczewan"
  ]
  node [
    id 839
    label "Cluny"
  ]
  node [
    id 840
    label "Stalinogorsk"
  ]
  node [
    id 841
    label "Lipsk"
  ]
  node [
    id 842
    label "Karlsbad"
  ]
  node [
    id 843
    label "Pietrozawodsk"
  ]
  node [
    id 844
    label "Bar"
  ]
  node [
    id 845
    label "Korfant&#243;w"
  ]
  node [
    id 846
    label "Nieftiegorsk"
  ]
  node [
    id 847
    label "Hanower"
  ]
  node [
    id 848
    label "Windawa"
  ]
  node [
    id 849
    label "&#346;niatyn"
  ]
  node [
    id 850
    label "Dalton"
  ]
  node [
    id 851
    label "tramwaj"
  ]
  node [
    id 852
    label "Kaszgar"
  ]
  node [
    id 853
    label "Berdia&#324;sk"
  ]
  node [
    id 854
    label "Koprzywnica"
  ]
  node [
    id 855
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 856
    label "Brno"
  ]
  node [
    id 857
    label "Wia&#378;ma"
  ]
  node [
    id 858
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 859
    label "Starobielsk"
  ]
  node [
    id 860
    label "Ostr&#243;g"
  ]
  node [
    id 861
    label "Oran"
  ]
  node [
    id 862
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 863
    label "Wyszehrad"
  ]
  node [
    id 864
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 865
    label "Trembowla"
  ]
  node [
    id 866
    label "Tobolsk"
  ]
  node [
    id 867
    label "Liberec"
  ]
  node [
    id 868
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 869
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 870
    label "G&#322;uszyca"
  ]
  node [
    id 871
    label "Akwileja"
  ]
  node [
    id 872
    label "Kar&#322;owice"
  ]
  node [
    id 873
    label "Borys&#243;w"
  ]
  node [
    id 874
    label "Stryj"
  ]
  node [
    id 875
    label "Czeski_Cieszyn"
  ]
  node [
    id 876
    label "Rydu&#322;towy"
  ]
  node [
    id 877
    label "Darmstadt"
  ]
  node [
    id 878
    label "Opawa"
  ]
  node [
    id 879
    label "Jerycho"
  ]
  node [
    id 880
    label "&#321;ohojsk"
  ]
  node [
    id 881
    label "Fatima"
  ]
  node [
    id 882
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 883
    label "Sara&#324;sk"
  ]
  node [
    id 884
    label "Lyon"
  ]
  node [
    id 885
    label "Wormacja"
  ]
  node [
    id 886
    label "Perwomajsk"
  ]
  node [
    id 887
    label "Lubeka"
  ]
  node [
    id 888
    label "Sura&#380;"
  ]
  node [
    id 889
    label "Karaganda"
  ]
  node [
    id 890
    label "Nazaret"
  ]
  node [
    id 891
    label "Poniewie&#380;"
  ]
  node [
    id 892
    label "Siewieromorsk"
  ]
  node [
    id 893
    label "Greifswald"
  ]
  node [
    id 894
    label "Trewir"
  ]
  node [
    id 895
    label "Nitra"
  ]
  node [
    id 896
    label "Karwina"
  ]
  node [
    id 897
    label "Houston"
  ]
  node [
    id 898
    label "Demmin"
  ]
  node [
    id 899
    label "Szamocin"
  ]
  node [
    id 900
    label "Kolkata"
  ]
  node [
    id 901
    label "Brasz&#243;w"
  ]
  node [
    id 902
    label "&#321;uck"
  ]
  node [
    id 903
    label "Peczora"
  ]
  node [
    id 904
    label "S&#322;onim"
  ]
  node [
    id 905
    label "Mekka"
  ]
  node [
    id 906
    label "Rzeczyca"
  ]
  node [
    id 907
    label "Konstancja"
  ]
  node [
    id 908
    label "Orenburg"
  ]
  node [
    id 909
    label "Pittsburgh"
  ]
  node [
    id 910
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 911
    label "Barabi&#324;sk"
  ]
  node [
    id 912
    label "Mory&#324;"
  ]
  node [
    id 913
    label "Hallstatt"
  ]
  node [
    id 914
    label "Mannheim"
  ]
  node [
    id 915
    label "Tarent"
  ]
  node [
    id 916
    label "Dortmund"
  ]
  node [
    id 917
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 918
    label "Dodona"
  ]
  node [
    id 919
    label "Trojan"
  ]
  node [
    id 920
    label "Nankin"
  ]
  node [
    id 921
    label "Weimar"
  ]
  node [
    id 922
    label "Brac&#322;aw"
  ]
  node [
    id 923
    label "Izbica_Kujawska"
  ]
  node [
    id 924
    label "Sankt_Florian"
  ]
  node [
    id 925
    label "Pilzno"
  ]
  node [
    id 926
    label "&#321;uga&#324;sk"
  ]
  node [
    id 927
    label "Sewastopol"
  ]
  node [
    id 928
    label "Poczaj&#243;w"
  ]
  node [
    id 929
    label "Pas&#322;&#281;k"
  ]
  node [
    id 930
    label "Sulech&#243;w"
  ]
  node [
    id 931
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 932
    label "Norak"
  ]
  node [
    id 933
    label "Filadelfia"
  ]
  node [
    id 934
    label "Maribor"
  ]
  node [
    id 935
    label "Detroit"
  ]
  node [
    id 936
    label "Bobolice"
  ]
  node [
    id 937
    label "K&#322;odawa"
  ]
  node [
    id 938
    label "Radziech&#243;w"
  ]
  node [
    id 939
    label "W&#322;odzimierz"
  ]
  node [
    id 940
    label "Tartu"
  ]
  node [
    id 941
    label "Drohobycz"
  ]
  node [
    id 942
    label "Saloniki"
  ]
  node [
    id 943
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 944
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 945
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 946
    label "Buchara"
  ]
  node [
    id 947
    label "P&#322;owdiw"
  ]
  node [
    id 948
    label "Koszyce"
  ]
  node [
    id 949
    label "Brema"
  ]
  node [
    id 950
    label "Wagram"
  ]
  node [
    id 951
    label "Czarnobyl"
  ]
  node [
    id 952
    label "Brze&#347;&#263;"
  ]
  node [
    id 953
    label "S&#232;vres"
  ]
  node [
    id 954
    label "Dubrownik"
  ]
  node [
    id 955
    label "Grenada"
  ]
  node [
    id 956
    label "Jekaterynburg"
  ]
  node [
    id 957
    label "zabudowa"
  ]
  node [
    id 958
    label "Inhambane"
  ]
  node [
    id 959
    label "Konstantyn&#243;wka"
  ]
  node [
    id 960
    label "Krajowa"
  ]
  node [
    id 961
    label "Norymberga"
  ]
  node [
    id 962
    label "Tarnogr&#243;d"
  ]
  node [
    id 963
    label "Beresteczko"
  ]
  node [
    id 964
    label "Chabarowsk"
  ]
  node [
    id 965
    label "Boden"
  ]
  node [
    id 966
    label "Bamberg"
  ]
  node [
    id 967
    label "Podhajce"
  ]
  node [
    id 968
    label "Lhasa"
  ]
  node [
    id 969
    label "Oszmiana"
  ]
  node [
    id 970
    label "Narbona"
  ]
  node [
    id 971
    label "Carrara"
  ]
  node [
    id 972
    label "Soleczniki"
  ]
  node [
    id 973
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 974
    label "Malin"
  ]
  node [
    id 975
    label "Gandawa"
  ]
  node [
    id 976
    label "burmistrz"
  ]
  node [
    id 977
    label "Lancaster"
  ]
  node [
    id 978
    label "S&#322;uck"
  ]
  node [
    id 979
    label "Kronsztad"
  ]
  node [
    id 980
    label "Mosty"
  ]
  node [
    id 981
    label "Budionnowsk"
  ]
  node [
    id 982
    label "Oksford"
  ]
  node [
    id 983
    label "Awinion"
  ]
  node [
    id 984
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 985
    label "Edynburg"
  ]
  node [
    id 986
    label "Zagorsk"
  ]
  node [
    id 987
    label "Kaspijsk"
  ]
  node [
    id 988
    label "Konotop"
  ]
  node [
    id 989
    label "Nantes"
  ]
  node [
    id 990
    label "Sydney"
  ]
  node [
    id 991
    label "Orsza"
  ]
  node [
    id 992
    label "Krzanowice"
  ]
  node [
    id 993
    label "Tiume&#324;"
  ]
  node [
    id 994
    label "Wyborg"
  ]
  node [
    id 995
    label "Nerczy&#324;sk"
  ]
  node [
    id 996
    label "Rost&#243;w"
  ]
  node [
    id 997
    label "Halicz"
  ]
  node [
    id 998
    label "Sumy"
  ]
  node [
    id 999
    label "Locarno"
  ]
  node [
    id 1000
    label "Luboml"
  ]
  node [
    id 1001
    label "Mariupol"
  ]
  node [
    id 1002
    label "Bras&#322;aw"
  ]
  node [
    id 1003
    label "Witnica"
  ]
  node [
    id 1004
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 1005
    label "Orneta"
  ]
  node [
    id 1006
    label "Gr&#243;dek"
  ]
  node [
    id 1007
    label "Go&#347;cino"
  ]
  node [
    id 1008
    label "Cannes"
  ]
  node [
    id 1009
    label "Lw&#243;w"
  ]
  node [
    id 1010
    label "Ulm"
  ]
  node [
    id 1011
    label "Aczy&#324;sk"
  ]
  node [
    id 1012
    label "Stuttgart"
  ]
  node [
    id 1013
    label "weduta"
  ]
  node [
    id 1014
    label "Borowsk"
  ]
  node [
    id 1015
    label "Niko&#322;ajewsk"
  ]
  node [
    id 1016
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 1017
    label "Worone&#380;"
  ]
  node [
    id 1018
    label "Delhi"
  ]
  node [
    id 1019
    label "Adrianopol"
  ]
  node [
    id 1020
    label "Byczyna"
  ]
  node [
    id 1021
    label "Obuch&#243;w"
  ]
  node [
    id 1022
    label "Tyraspol"
  ]
  node [
    id 1023
    label "Modena"
  ]
  node [
    id 1024
    label "Rajgr&#243;d"
  ]
  node [
    id 1025
    label "Wo&#322;kowysk"
  ]
  node [
    id 1026
    label "&#379;ylina"
  ]
  node [
    id 1027
    label "Zurych"
  ]
  node [
    id 1028
    label "Vukovar"
  ]
  node [
    id 1029
    label "Narwa"
  ]
  node [
    id 1030
    label "Neapol"
  ]
  node [
    id 1031
    label "Frydek-Mistek"
  ]
  node [
    id 1032
    label "W&#322;adywostok"
  ]
  node [
    id 1033
    label "Calais"
  ]
  node [
    id 1034
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 1035
    label "Trydent"
  ]
  node [
    id 1036
    label "Magnitogorsk"
  ]
  node [
    id 1037
    label "Padwa"
  ]
  node [
    id 1038
    label "Isfahan"
  ]
  node [
    id 1039
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 1040
    label "Marburg"
  ]
  node [
    id 1041
    label "Homel"
  ]
  node [
    id 1042
    label "Boston"
  ]
  node [
    id 1043
    label "W&#252;rzburg"
  ]
  node [
    id 1044
    label "Antiochia"
  ]
  node [
    id 1045
    label "Wotki&#324;sk"
  ]
  node [
    id 1046
    label "A&#322;apajewsk"
  ]
  node [
    id 1047
    label "Lejda"
  ]
  node [
    id 1048
    label "Nieder_Selters"
  ]
  node [
    id 1049
    label "Nicea"
  ]
  node [
    id 1050
    label "Dmitrow"
  ]
  node [
    id 1051
    label "Taganrog"
  ]
  node [
    id 1052
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 1053
    label "Nowomoskowsk"
  ]
  node [
    id 1054
    label "Koby&#322;ka"
  ]
  node [
    id 1055
    label "Iwano-Frankowsk"
  ]
  node [
    id 1056
    label "Kis&#322;owodzk"
  ]
  node [
    id 1057
    label "Tomsk"
  ]
  node [
    id 1058
    label "Ferrara"
  ]
  node [
    id 1059
    label "Edam"
  ]
  node [
    id 1060
    label "Suworow"
  ]
  node [
    id 1061
    label "Turka"
  ]
  node [
    id 1062
    label "Aralsk"
  ]
  node [
    id 1063
    label "Kobry&#324;"
  ]
  node [
    id 1064
    label "Rotterdam"
  ]
  node [
    id 1065
    label "Bordeaux"
  ]
  node [
    id 1066
    label "L&#252;neburg"
  ]
  node [
    id 1067
    label "Akwizgran"
  ]
  node [
    id 1068
    label "Liverpool"
  ]
  node [
    id 1069
    label "Asuan"
  ]
  node [
    id 1070
    label "Bonn"
  ]
  node [
    id 1071
    label "Teby"
  ]
  node [
    id 1072
    label "Szumsk"
  ]
  node [
    id 1073
    label "Ku&#378;nieck"
  ]
  node [
    id 1074
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 1075
    label "Tyberiada"
  ]
  node [
    id 1076
    label "Turkiestan"
  ]
  node [
    id 1077
    label "Nanning"
  ]
  node [
    id 1078
    label "G&#322;uch&#243;w"
  ]
  node [
    id 1079
    label "Bajonna"
  ]
  node [
    id 1080
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 1081
    label "Orze&#322;"
  ]
  node [
    id 1082
    label "Opalenica"
  ]
  node [
    id 1083
    label "Buczacz"
  ]
  node [
    id 1084
    label "Armenia"
  ]
  node [
    id 1085
    label "Nowoku&#378;nieck"
  ]
  node [
    id 1086
    label "Wuppertal"
  ]
  node [
    id 1087
    label "Wuhan"
  ]
  node [
    id 1088
    label "Betlejem"
  ]
  node [
    id 1089
    label "Wi&#322;komierz"
  ]
  node [
    id 1090
    label "Podiebrady"
  ]
  node [
    id 1091
    label "Rawenna"
  ]
  node [
    id 1092
    label "Haarlem"
  ]
  node [
    id 1093
    label "Woskriesiensk"
  ]
  node [
    id 1094
    label "Pyskowice"
  ]
  node [
    id 1095
    label "Kilonia"
  ]
  node [
    id 1096
    label "Ruciane-Nida"
  ]
  node [
    id 1097
    label "Kursk"
  ]
  node [
    id 1098
    label "Wolgast"
  ]
  node [
    id 1099
    label "Stralsund"
  ]
  node [
    id 1100
    label "Sydon"
  ]
  node [
    id 1101
    label "Natal"
  ]
  node [
    id 1102
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 1103
    label "Baranowicze"
  ]
  node [
    id 1104
    label "Stara_Zagora"
  ]
  node [
    id 1105
    label "Regensburg"
  ]
  node [
    id 1106
    label "Kapsztad"
  ]
  node [
    id 1107
    label "Kemerowo"
  ]
  node [
    id 1108
    label "Mi&#347;nia"
  ]
  node [
    id 1109
    label "Stary_Sambor"
  ]
  node [
    id 1110
    label "Soligorsk"
  ]
  node [
    id 1111
    label "Ostaszk&#243;w"
  ]
  node [
    id 1112
    label "T&#322;uszcz"
  ]
  node [
    id 1113
    label "Uljanowsk"
  ]
  node [
    id 1114
    label "Tuluza"
  ]
  node [
    id 1115
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 1116
    label "Chicago"
  ]
  node [
    id 1117
    label "Kamieniec_Podolski"
  ]
  node [
    id 1118
    label "Dijon"
  ]
  node [
    id 1119
    label "Siedliszcze"
  ]
  node [
    id 1120
    label "Haga"
  ]
  node [
    id 1121
    label "Bobrujsk"
  ]
  node [
    id 1122
    label "Kokand"
  ]
  node [
    id 1123
    label "Windsor"
  ]
  node [
    id 1124
    label "Chmielnicki"
  ]
  node [
    id 1125
    label "Winchester"
  ]
  node [
    id 1126
    label "Bria&#324;sk"
  ]
  node [
    id 1127
    label "Uppsala"
  ]
  node [
    id 1128
    label "Paw&#322;odar"
  ]
  node [
    id 1129
    label "Canterbury"
  ]
  node [
    id 1130
    label "Omsk"
  ]
  node [
    id 1131
    label "Tyr"
  ]
  node [
    id 1132
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 1133
    label "Kolonia"
  ]
  node [
    id 1134
    label "Nowa_Ruda"
  ]
  node [
    id 1135
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 1136
    label "Czerkasy"
  ]
  node [
    id 1137
    label "Budziszyn"
  ]
  node [
    id 1138
    label "Rohatyn"
  ]
  node [
    id 1139
    label "Nowogr&#243;dek"
  ]
  node [
    id 1140
    label "Buda"
  ]
  node [
    id 1141
    label "Zbara&#380;"
  ]
  node [
    id 1142
    label "Korzec"
  ]
  node [
    id 1143
    label "Medyna"
  ]
  node [
    id 1144
    label "Piatigorsk"
  ]
  node [
    id 1145
    label "Monako"
  ]
  node [
    id 1146
    label "Chark&#243;w"
  ]
  node [
    id 1147
    label "Zadar"
  ]
  node [
    id 1148
    label "Brandenburg"
  ]
  node [
    id 1149
    label "&#379;ytawa"
  ]
  node [
    id 1150
    label "Konstantynopol"
  ]
  node [
    id 1151
    label "Wismar"
  ]
  node [
    id 1152
    label "Wielsk"
  ]
  node [
    id 1153
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 1154
    label "Genewa"
  ]
  node [
    id 1155
    label "Merseburg"
  ]
  node [
    id 1156
    label "Lozanna"
  ]
  node [
    id 1157
    label "Azow"
  ]
  node [
    id 1158
    label "K&#322;ajpeda"
  ]
  node [
    id 1159
    label "Angarsk"
  ]
  node [
    id 1160
    label "Ostrawa"
  ]
  node [
    id 1161
    label "Jastarnia"
  ]
  node [
    id 1162
    label "Moguncja"
  ]
  node [
    id 1163
    label "Siewsk"
  ]
  node [
    id 1164
    label "Pasawa"
  ]
  node [
    id 1165
    label "Penza"
  ]
  node [
    id 1166
    label "Borys&#322;aw"
  ]
  node [
    id 1167
    label "Osaka"
  ]
  node [
    id 1168
    label "Eupatoria"
  ]
  node [
    id 1169
    label "Kalmar"
  ]
  node [
    id 1170
    label "Troki"
  ]
  node [
    id 1171
    label "Mosina"
  ]
  node [
    id 1172
    label "Orany"
  ]
  node [
    id 1173
    label "Zas&#322;aw"
  ]
  node [
    id 1174
    label "Dobrodzie&#324;"
  ]
  node [
    id 1175
    label "Kars"
  ]
  node [
    id 1176
    label "Poprad"
  ]
  node [
    id 1177
    label "Sajgon"
  ]
  node [
    id 1178
    label "Tulon"
  ]
  node [
    id 1179
    label "Kro&#347;niewice"
  ]
  node [
    id 1180
    label "Krzywi&#324;"
  ]
  node [
    id 1181
    label "Batumi"
  ]
  node [
    id 1182
    label "Werona"
  ]
  node [
    id 1183
    label "&#379;migr&#243;d"
  ]
  node [
    id 1184
    label "Ka&#322;uga"
  ]
  node [
    id 1185
    label "Rakoniewice"
  ]
  node [
    id 1186
    label "Trabzon"
  ]
  node [
    id 1187
    label "Debreczyn"
  ]
  node [
    id 1188
    label "Jena"
  ]
  node [
    id 1189
    label "Strzelno"
  ]
  node [
    id 1190
    label "Gwardiejsk"
  ]
  node [
    id 1191
    label "Wersal"
  ]
  node [
    id 1192
    label "Bych&#243;w"
  ]
  node [
    id 1193
    label "Ba&#322;tijsk"
  ]
  node [
    id 1194
    label "Trenczyn"
  ]
  node [
    id 1195
    label "Walencja"
  ]
  node [
    id 1196
    label "Warna"
  ]
  node [
    id 1197
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 1198
    label "Huma&#324;"
  ]
  node [
    id 1199
    label "Wilejka"
  ]
  node [
    id 1200
    label "Ochryda"
  ]
  node [
    id 1201
    label "Berdycz&#243;w"
  ]
  node [
    id 1202
    label "Krasnogorsk"
  ]
  node [
    id 1203
    label "Bogus&#322;aw"
  ]
  node [
    id 1204
    label "Trzyniec"
  ]
  node [
    id 1205
    label "urz&#261;d"
  ]
  node [
    id 1206
    label "Mariampol"
  ]
  node [
    id 1207
    label "Ko&#322;omna"
  ]
  node [
    id 1208
    label "Chanty-Mansyjsk"
  ]
  node [
    id 1209
    label "Piast&#243;w"
  ]
  node [
    id 1210
    label "Jastrowie"
  ]
  node [
    id 1211
    label "Nampula"
  ]
  node [
    id 1212
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 1213
    label "Bor"
  ]
  node [
    id 1214
    label "Lengyel"
  ]
  node [
    id 1215
    label "Lubecz"
  ]
  node [
    id 1216
    label "Wierchoja&#324;sk"
  ]
  node [
    id 1217
    label "Barczewo"
  ]
  node [
    id 1218
    label "Madras"
  ]
  node [
    id 1219
    label "zdrada"
  ]
  node [
    id 1220
    label "warunek_lokalowy"
  ]
  node [
    id 1221
    label "location"
  ]
  node [
    id 1222
    label "uwaga"
  ]
  node [
    id 1223
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1224
    label "cia&#322;o"
  ]
  node [
    id 1225
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1226
    label "rz&#261;d"
  ]
  node [
    id 1227
    label "blisko"
  ]
  node [
    id 1228
    label "znajomy"
  ]
  node [
    id 1229
    label "zwi&#261;zany"
  ]
  node [
    id 1230
    label "przesz&#322;y"
  ]
  node [
    id 1231
    label "silny"
  ]
  node [
    id 1232
    label "zbli&#380;enie"
  ]
  node [
    id 1233
    label "kr&#243;tki"
  ]
  node [
    id 1234
    label "oddalony"
  ]
  node [
    id 1235
    label "dok&#322;adny"
  ]
  node [
    id 1236
    label "nieodleg&#322;y"
  ]
  node [
    id 1237
    label "przysz&#322;y"
  ]
  node [
    id 1238
    label "gotowy"
  ]
  node [
    id 1239
    label "ma&#322;y"
  ]
  node [
    id 1240
    label "szybki"
  ]
  node [
    id 1241
    label "jednowyrazowy"
  ]
  node [
    id 1242
    label "s&#322;aby"
  ]
  node [
    id 1243
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1244
    label "kr&#243;tko"
  ]
  node [
    id 1245
    label "drobny"
  ]
  node [
    id 1246
    label "ruch"
  ]
  node [
    id 1247
    label "brak"
  ]
  node [
    id 1248
    label "z&#322;y"
  ]
  node [
    id 1249
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 1250
    label "dok&#322;adnie"
  ]
  node [
    id 1251
    label "silnie"
  ]
  node [
    id 1252
    label "nietrze&#378;wy"
  ]
  node [
    id 1253
    label "czekanie"
  ]
  node [
    id 1254
    label "martwy"
  ]
  node [
    id 1255
    label "m&#243;c"
  ]
  node [
    id 1256
    label "gotowo"
  ]
  node [
    id 1257
    label "przygotowanie"
  ]
  node [
    id 1258
    label "przygotowywanie"
  ]
  node [
    id 1259
    label "dyspozycyjny"
  ]
  node [
    id 1260
    label "zalany"
  ]
  node [
    id 1261
    label "nieuchronny"
  ]
  node [
    id 1262
    label "doj&#347;cie"
  ]
  node [
    id 1263
    label "zetkni&#281;cie"
  ]
  node [
    id 1264
    label "plan"
  ]
  node [
    id 1265
    label "po&#380;ycie"
  ]
  node [
    id 1266
    label "podnieci&#263;"
  ]
  node [
    id 1267
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1268
    label "numer"
  ]
  node [
    id 1269
    label "closeup"
  ]
  node [
    id 1270
    label "podniecenie"
  ]
  node [
    id 1271
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1272
    label "konfidencja"
  ]
  node [
    id 1273
    label "seks"
  ]
  node [
    id 1274
    label "podniecanie"
  ]
  node [
    id 1275
    label "imisja"
  ]
  node [
    id 1276
    label "pobratymstwo"
  ]
  node [
    id 1277
    label "rozmna&#380;anie"
  ]
  node [
    id 1278
    label "proximity"
  ]
  node [
    id 1279
    label "uj&#281;cie"
  ]
  node [
    id 1280
    label "ruch_frykcyjny"
  ]
  node [
    id 1281
    label "na_pieska"
  ]
  node [
    id 1282
    label "pozycja_misjonarska"
  ]
  node [
    id 1283
    label "przemieszczenie"
  ]
  node [
    id 1284
    label "dru&#380;ba"
  ]
  node [
    id 1285
    label "approach"
  ]
  node [
    id 1286
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1287
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1288
    label "gra_wst&#281;pna"
  ]
  node [
    id 1289
    label "znajomo&#347;&#263;"
  ]
  node [
    id 1290
    label "erotyka"
  ]
  node [
    id 1291
    label "baraszki"
  ]
  node [
    id 1292
    label "po&#380;&#261;danie"
  ]
  node [
    id 1293
    label "wzw&#243;d"
  ]
  node [
    id 1294
    label "podnieca&#263;"
  ]
  node [
    id 1295
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 1296
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1297
    label "sprecyzowanie"
  ]
  node [
    id 1298
    label "precyzyjny"
  ]
  node [
    id 1299
    label "miliamperomierz"
  ]
  node [
    id 1300
    label "precyzowanie"
  ]
  node [
    id 1301
    label "rzetelny"
  ]
  node [
    id 1302
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1303
    label "asymilowanie"
  ]
  node [
    id 1304
    label "asymilowa&#263;"
  ]
  node [
    id 1305
    label "os&#322;abia&#263;"
  ]
  node [
    id 1306
    label "posta&#263;"
  ]
  node [
    id 1307
    label "hominid"
  ]
  node [
    id 1308
    label "podw&#322;adny"
  ]
  node [
    id 1309
    label "os&#322;abianie"
  ]
  node [
    id 1310
    label "g&#322;owa"
  ]
  node [
    id 1311
    label "figura"
  ]
  node [
    id 1312
    label "portrecista"
  ]
  node [
    id 1313
    label "dwun&#243;g"
  ]
  node [
    id 1314
    label "profanum"
  ]
  node [
    id 1315
    label "mikrokosmos"
  ]
  node [
    id 1316
    label "nasada"
  ]
  node [
    id 1317
    label "duch"
  ]
  node [
    id 1318
    label "antropochoria"
  ]
  node [
    id 1319
    label "osoba"
  ]
  node [
    id 1320
    label "wz&#243;r"
  ]
  node [
    id 1321
    label "senior"
  ]
  node [
    id 1322
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1323
    label "Adam"
  ]
  node [
    id 1324
    label "homo_sapiens"
  ]
  node [
    id 1325
    label "polifag"
  ]
  node [
    id 1326
    label "zapoznanie"
  ]
  node [
    id 1327
    label "sw&#243;j"
  ]
  node [
    id 1328
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1329
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1330
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1331
    label "znajomek"
  ]
  node [
    id 1332
    label "zapoznawanie"
  ]
  node [
    id 1333
    label "znajomo"
  ]
  node [
    id 1334
    label "pewien"
  ]
  node [
    id 1335
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 1336
    label "przyj&#281;ty"
  ]
  node [
    id 1337
    label "za_pan_brat"
  ]
  node [
    id 1338
    label "nieznaczny"
  ]
  node [
    id 1339
    label "przeci&#281;tny"
  ]
  node [
    id 1340
    label "wstydliwy"
  ]
  node [
    id 1341
    label "niewa&#380;ny"
  ]
  node [
    id 1342
    label "ch&#322;opiec"
  ]
  node [
    id 1343
    label "m&#322;ody"
  ]
  node [
    id 1344
    label "ma&#322;o"
  ]
  node [
    id 1345
    label "marny"
  ]
  node [
    id 1346
    label "nieliczny"
  ]
  node [
    id 1347
    label "n&#281;dznie"
  ]
  node [
    id 1348
    label "oderwany"
  ]
  node [
    id 1349
    label "daleki"
  ]
  node [
    id 1350
    label "daleko"
  ]
  node [
    id 1351
    label "miniony"
  ]
  node [
    id 1352
    label "ostatni"
  ]
  node [
    id 1353
    label "intensywny"
  ]
  node [
    id 1354
    label "krzepienie"
  ]
  node [
    id 1355
    label "&#380;ywotny"
  ]
  node [
    id 1356
    label "mocny"
  ]
  node [
    id 1357
    label "pokrzepienie"
  ]
  node [
    id 1358
    label "zdecydowany"
  ]
  node [
    id 1359
    label "niepodwa&#380;alny"
  ]
  node [
    id 1360
    label "du&#380;y"
  ]
  node [
    id 1361
    label "mocno"
  ]
  node [
    id 1362
    label "przekonuj&#261;cy"
  ]
  node [
    id 1363
    label "wytrzyma&#322;y"
  ]
  node [
    id 1364
    label "konkretny"
  ]
  node [
    id 1365
    label "zdrowy"
  ]
  node [
    id 1366
    label "meflochina"
  ]
  node [
    id 1367
    label "zajebisty"
  ]
  node [
    id 1368
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 1369
    label "dzie&#324;_powszedni"
  ]
  node [
    id 1370
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1371
    label "invite"
  ]
  node [
    id 1372
    label "ask"
  ]
  node [
    id 1373
    label "oferowa&#263;"
  ]
  node [
    id 1374
    label "zach&#281;ca&#263;"
  ]
  node [
    id 1375
    label "volunteer"
  ]
  node [
    id 1376
    label "cisza"
  ]
  node [
    id 1377
    label "odpowied&#378;"
  ]
  node [
    id 1378
    label "rozhowor"
  ]
  node [
    id 1379
    label "discussion"
  ]
  node [
    id 1380
    label "activity"
  ]
  node [
    id 1381
    label "bezproblemowy"
  ]
  node [
    id 1382
    label "wydarzenie"
  ]
  node [
    id 1383
    label "g&#322;adki"
  ]
  node [
    id 1384
    label "cicha_praca"
  ]
  node [
    id 1385
    label "przerwa"
  ]
  node [
    id 1386
    label "cicha_msza"
  ]
  node [
    id 1387
    label "pok&#243;j"
  ]
  node [
    id 1388
    label "motionlessness"
  ]
  node [
    id 1389
    label "spok&#243;j"
  ]
  node [
    id 1390
    label "ci&#261;g"
  ]
  node [
    id 1391
    label "tajemno&#347;&#263;"
  ]
  node [
    id 1392
    label "peace"
  ]
  node [
    id 1393
    label "cicha_modlitwa"
  ]
  node [
    id 1394
    label "react"
  ]
  node [
    id 1395
    label "replica"
  ]
  node [
    id 1396
    label "wyj&#347;cie"
  ]
  node [
    id 1397
    label "respondent"
  ]
  node [
    id 1398
    label "dokument"
  ]
  node [
    id 1399
    label "reakcja"
  ]
  node [
    id 1400
    label "ciemna_materia"
  ]
  node [
    id 1401
    label "planeta"
  ]
  node [
    id 1402
    label "ekosfera"
  ]
  node [
    id 1403
    label "czarna_dziura"
  ]
  node [
    id 1404
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1405
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1406
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 1407
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 1408
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1409
    label "strefa"
  ]
  node [
    id 1410
    label "kosmos"
  ]
  node [
    id 1411
    label "odbicie"
  ]
  node [
    id 1412
    label "atom"
  ]
  node [
    id 1413
    label "przyroda"
  ]
  node [
    id 1414
    label "Ziemia"
  ]
  node [
    id 1415
    label "miniatura"
  ]
  node [
    id 1416
    label "Jowisz"
  ]
  node [
    id 1417
    label "syzygia"
  ]
  node [
    id 1418
    label "atmosfera"
  ]
  node [
    id 1419
    label "Saturn"
  ]
  node [
    id 1420
    label "Uran"
  ]
  node [
    id 1421
    label "aspekt"
  ]
  node [
    id 1422
    label "kultura"
  ]
  node [
    id 1423
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1424
    label "kontekst"
  ]
  node [
    id 1425
    label "&#347;wiat"
  ]
  node [
    id 1426
    label "dar"
  ]
  node [
    id 1427
    label "real"
  ]
  node [
    id 1428
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1429
    label "equal"
  ]
  node [
    id 1430
    label "trwa&#263;"
  ]
  node [
    id 1431
    label "chodzi&#263;"
  ]
  node [
    id 1432
    label "si&#281;ga&#263;"
  ]
  node [
    id 1433
    label "obecno&#347;&#263;"
  ]
  node [
    id 1434
    label "stand"
  ]
  node [
    id 1435
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1436
    label "uczestniczy&#263;"
  ]
  node [
    id 1437
    label "participate"
  ]
  node [
    id 1438
    label "pozostawa&#263;"
  ]
  node [
    id 1439
    label "zostawa&#263;"
  ]
  node [
    id 1440
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1441
    label "adhere"
  ]
  node [
    id 1442
    label "appreciation"
  ]
  node [
    id 1443
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1444
    label "dociera&#263;"
  ]
  node [
    id 1445
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1446
    label "mierzy&#263;"
  ]
  node [
    id 1447
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1448
    label "exsert"
  ]
  node [
    id 1449
    label "being"
  ]
  node [
    id 1450
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1451
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1452
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1453
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1454
    label "run"
  ]
  node [
    id 1455
    label "bangla&#263;"
  ]
  node [
    id 1456
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1457
    label "przebiega&#263;"
  ]
  node [
    id 1458
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1459
    label "proceed"
  ]
  node [
    id 1460
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1461
    label "carry"
  ]
  node [
    id 1462
    label "bywa&#263;"
  ]
  node [
    id 1463
    label "dziama&#263;"
  ]
  node [
    id 1464
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1465
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1466
    label "para"
  ]
  node [
    id 1467
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1468
    label "str&#243;j"
  ]
  node [
    id 1469
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1470
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1471
    label "krok"
  ]
  node [
    id 1472
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1473
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1474
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1475
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1476
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1477
    label "continue"
  ]
  node [
    id 1478
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1479
    label "Ohio"
  ]
  node [
    id 1480
    label "wci&#281;cie"
  ]
  node [
    id 1481
    label "Nowy_York"
  ]
  node [
    id 1482
    label "samopoczucie"
  ]
  node [
    id 1483
    label "Illinois"
  ]
  node [
    id 1484
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1485
    label "state"
  ]
  node [
    id 1486
    label "Jukatan"
  ]
  node [
    id 1487
    label "Kalifornia"
  ]
  node [
    id 1488
    label "Wirginia"
  ]
  node [
    id 1489
    label "wektor"
  ]
  node [
    id 1490
    label "Teksas"
  ]
  node [
    id 1491
    label "Goa"
  ]
  node [
    id 1492
    label "Waszyngton"
  ]
  node [
    id 1493
    label "Massachusetts"
  ]
  node [
    id 1494
    label "Alaska"
  ]
  node [
    id 1495
    label "Arakan"
  ]
  node [
    id 1496
    label "Hawaje"
  ]
  node [
    id 1497
    label "Maryland"
  ]
  node [
    id 1498
    label "Michigan"
  ]
  node [
    id 1499
    label "Arizona"
  ]
  node [
    id 1500
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1501
    label "Georgia"
  ]
  node [
    id 1502
    label "Pensylwania"
  ]
  node [
    id 1503
    label "Luizjana"
  ]
  node [
    id 1504
    label "Nowy_Meksyk"
  ]
  node [
    id 1505
    label "Alabama"
  ]
  node [
    id 1506
    label "ilo&#347;&#263;"
  ]
  node [
    id 1507
    label "Kansas"
  ]
  node [
    id 1508
    label "Oregon"
  ]
  node [
    id 1509
    label "Floryda"
  ]
  node [
    id 1510
    label "Oklahoma"
  ]
  node [
    id 1511
    label "jednostka_administracyjna"
  ]
  node [
    id 1512
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1513
    label "stacja_dysk&#243;w"
  ]
  node [
    id 1514
    label "instalowa&#263;"
  ]
  node [
    id 1515
    label "moc_obliczeniowa"
  ]
  node [
    id 1516
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 1517
    label "pad"
  ]
  node [
    id 1518
    label "modem"
  ]
  node [
    id 1519
    label "pami&#281;&#263;"
  ]
  node [
    id 1520
    label "monitor"
  ]
  node [
    id 1521
    label "zainstalowanie"
  ]
  node [
    id 1522
    label "emulacja"
  ]
  node [
    id 1523
    label "zainstalowa&#263;"
  ]
  node [
    id 1524
    label "procesor"
  ]
  node [
    id 1525
    label "maszyna_Turinga"
  ]
  node [
    id 1526
    label "karta"
  ]
  node [
    id 1527
    label "twardy_dysk"
  ]
  node [
    id 1528
    label "klawiatura"
  ]
  node [
    id 1529
    label "botnet"
  ]
  node [
    id 1530
    label "mysz"
  ]
  node [
    id 1531
    label "instalowanie"
  ]
  node [
    id 1532
    label "radiator"
  ]
  node [
    id 1533
    label "urz&#261;dzenie"
  ]
  node [
    id 1534
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 1535
    label "wirus"
  ]
  node [
    id 1536
    label "kom&#243;rka"
  ]
  node [
    id 1537
    label "furnishing"
  ]
  node [
    id 1538
    label "zabezpieczenie"
  ]
  node [
    id 1539
    label "wyrz&#261;dzenie"
  ]
  node [
    id 1540
    label "zagospodarowanie"
  ]
  node [
    id 1541
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 1542
    label "ig&#322;a"
  ]
  node [
    id 1543
    label "wirnik"
  ]
  node [
    id 1544
    label "aparatura"
  ]
  node [
    id 1545
    label "system_energetyczny"
  ]
  node [
    id 1546
    label "impulsator"
  ]
  node [
    id 1547
    label "mechanizm"
  ]
  node [
    id 1548
    label "sprz&#281;t"
  ]
  node [
    id 1549
    label "blokowanie"
  ]
  node [
    id 1550
    label "set"
  ]
  node [
    id 1551
    label "zablokowanie"
  ]
  node [
    id 1552
    label "komora"
  ]
  node [
    id 1553
    label "j&#281;zyk"
  ]
  node [
    id 1554
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 1555
    label "arytmometr"
  ]
  node [
    id 1556
    label "uk&#322;ad_scalony"
  ]
  node [
    id 1557
    label "pami&#281;&#263;_g&#243;rna"
  ]
  node [
    id 1558
    label "rdze&#324;"
  ]
  node [
    id 1559
    label "cooler"
  ]
  node [
    id 1560
    label "sumator"
  ]
  node [
    id 1561
    label "rejestr"
  ]
  node [
    id 1562
    label "hipokamp"
  ]
  node [
    id 1563
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 1564
    label "wytw&#243;r"
  ]
  node [
    id 1565
    label "memory"
  ]
  node [
    id 1566
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1567
    label "umys&#322;"
  ]
  node [
    id 1568
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 1569
    label "zachowa&#263;"
  ]
  node [
    id 1570
    label "wymazanie"
  ]
  node [
    id 1571
    label "silnik"
  ]
  node [
    id 1572
    label "atrapa"
  ]
  node [
    id 1573
    label "wzmacniacz"
  ]
  node [
    id 1574
    label "regulator"
  ]
  node [
    id 1575
    label "kartka"
  ]
  node [
    id 1576
    label "danie"
  ]
  node [
    id 1577
    label "menu"
  ]
  node [
    id 1578
    label "zezwolenie"
  ]
  node [
    id 1579
    label "restauracja"
  ]
  node [
    id 1580
    label "chart"
  ]
  node [
    id 1581
    label "p&#322;ytka"
  ]
  node [
    id 1582
    label "formularz"
  ]
  node [
    id 1583
    label "ticket"
  ]
  node [
    id 1584
    label "cennik"
  ]
  node [
    id 1585
    label "oferta"
  ]
  node [
    id 1586
    label "charter"
  ]
  node [
    id 1587
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 1588
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 1589
    label "kartonik"
  ]
  node [
    id 1590
    label "circuit_board"
  ]
  node [
    id 1591
    label "supply"
  ]
  node [
    id 1592
    label "accommodate"
  ]
  node [
    id 1593
    label "program"
  ]
  node [
    id 1594
    label "umieszcza&#263;"
  ]
  node [
    id 1595
    label "fit"
  ]
  node [
    id 1596
    label "wystuka&#263;"
  ]
  node [
    id 1597
    label "wystukiwa&#263;"
  ]
  node [
    id 1598
    label "wystukiwanie"
  ]
  node [
    id 1599
    label "palc&#243;wka"
  ]
  node [
    id 1600
    label "wysuwka"
  ]
  node [
    id 1601
    label "Bajca"
  ]
  node [
    id 1602
    label "uk&#322;ad"
  ]
  node [
    id 1603
    label "wystukanie"
  ]
  node [
    id 1604
    label "uz&#281;bienie"
  ]
  node [
    id 1605
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 1606
    label "klawisz_myszki"
  ]
  node [
    id 1607
    label "piszcze&#263;"
  ]
  node [
    id 1608
    label "zapiszcze&#263;"
  ]
  node [
    id 1609
    label "myszy_w&#322;a&#347;ciwe"
  ]
  node [
    id 1610
    label "bary&#322;ka"
  ]
  node [
    id 1611
    label "gryzo&#324;"
  ]
  node [
    id 1612
    label "ekran"
  ]
  node [
    id 1613
    label "okr&#281;t_artyleryjski"
  ]
  node [
    id 1614
    label "okr&#281;t_nawodny"
  ]
  node [
    id 1615
    label "czasopismo"
  ]
  node [
    id 1616
    label "dostosowa&#263;"
  ]
  node [
    id 1617
    label "install"
  ]
  node [
    id 1618
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1619
    label "umieszczanie"
  ]
  node [
    id 1620
    label "installation"
  ]
  node [
    id 1621
    label "collection"
  ]
  node [
    id 1622
    label "wmontowanie"
  ]
  node [
    id 1623
    label "wmontowywanie"
  ]
  node [
    id 1624
    label "fitting"
  ]
  node [
    id 1625
    label "dostosowywanie"
  ]
  node [
    id 1626
    label "dostosowanie"
  ]
  node [
    id 1627
    label "pozak&#322;adanie"
  ]
  node [
    id 1628
    label "proposition"
  ]
  node [
    id 1629
    label "layout"
  ]
  node [
    id 1630
    label "umieszczenie"
  ]
  node [
    id 1631
    label "emulation"
  ]
  node [
    id 1632
    label "proces"
  ]
  node [
    id 1633
    label "kontroler_gier"
  ]
  node [
    id 1634
    label "konsola"
  ]
  node [
    id 1635
    label "ksi&#281;&#380;a"
  ]
  node [
    id 1636
    label "rozgrzeszanie"
  ]
  node [
    id 1637
    label "duszpasterstwo"
  ]
  node [
    id 1638
    label "eklezjasta"
  ]
  node [
    id 1639
    label "duchowny"
  ]
  node [
    id 1640
    label "rozgrzesza&#263;"
  ]
  node [
    id 1641
    label "seminarzysta"
  ]
  node [
    id 1642
    label "klecha"
  ]
  node [
    id 1643
    label "pasterz"
  ]
  node [
    id 1644
    label "kol&#281;da"
  ]
  node [
    id 1645
    label "kap&#322;an"
  ]
  node [
    id 1646
    label "duchowie&#324;stwo"
  ]
  node [
    id 1647
    label "stowarzyszenie_religijne"
  ]
  node [
    id 1648
    label "apostolstwo"
  ]
  node [
    id 1649
    label "kaznodziejstwo"
  ]
  node [
    id 1650
    label "rozsiewca"
  ]
  node [
    id 1651
    label "chor&#261;&#380;y"
  ]
  node [
    id 1652
    label "tuba"
  ]
  node [
    id 1653
    label "zwolennik"
  ]
  node [
    id 1654
    label "popularyzator"
  ]
  node [
    id 1655
    label "wyznawca"
  ]
  node [
    id 1656
    label "Luter"
  ]
  node [
    id 1657
    label "religia"
  ]
  node [
    id 1658
    label "Bayes"
  ]
  node [
    id 1659
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 1660
    label "sekularyzacja"
  ]
  node [
    id 1661
    label "tonsura"
  ]
  node [
    id 1662
    label "Hus"
  ]
  node [
    id 1663
    label "religijny"
  ]
  node [
    id 1664
    label "przedstawiciel"
  ]
  node [
    id 1665
    label "&#347;w"
  ]
  node [
    id 1666
    label "wie&#347;niak"
  ]
  node [
    id 1667
    label "Pan"
  ]
  node [
    id 1668
    label "szpak"
  ]
  node [
    id 1669
    label "hodowca"
  ]
  node [
    id 1670
    label "pracownik_fizyczny"
  ]
  node [
    id 1671
    label "Grek"
  ]
  node [
    id 1672
    label "obywatel"
  ]
  node [
    id 1673
    label "eklezja"
  ]
  node [
    id 1674
    label "kaznodzieja"
  ]
  node [
    id 1675
    label "odwiedziny"
  ]
  node [
    id 1676
    label "pie&#347;&#324;"
  ]
  node [
    id 1677
    label "kol&#281;dnik"
  ]
  node [
    id 1678
    label "dzie&#322;o"
  ]
  node [
    id 1679
    label "zwyczaj_ludowy"
  ]
  node [
    id 1680
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1681
    label "wybacza&#263;"
  ]
  node [
    id 1682
    label "grzesznik"
  ]
  node [
    id 1683
    label "udziela&#263;"
  ]
  node [
    id 1684
    label "wybaczanie"
  ]
  node [
    id 1685
    label "udzielanie"
  ]
  node [
    id 1686
    label "t&#322;umaczenie"
  ]
  node [
    id 1687
    label "spowiadanie"
  ]
  node [
    id 1688
    label "uczestnik"
  ]
  node [
    id 1689
    label "ucze&#324;"
  ]
  node [
    id 1690
    label "student"
  ]
  node [
    id 1691
    label "gaworzy&#263;"
  ]
  node [
    id 1692
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1693
    label "remark"
  ]
  node [
    id 1694
    label "rozmawia&#263;"
  ]
  node [
    id 1695
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1696
    label "umie&#263;"
  ]
  node [
    id 1697
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1698
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1699
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1700
    label "dysfonia"
  ]
  node [
    id 1701
    label "express"
  ]
  node [
    id 1702
    label "talk"
  ]
  node [
    id 1703
    label "prawi&#263;"
  ]
  node [
    id 1704
    label "powiada&#263;"
  ]
  node [
    id 1705
    label "tell"
  ]
  node [
    id 1706
    label "chew_the_fat"
  ]
  node [
    id 1707
    label "say"
  ]
  node [
    id 1708
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1709
    label "informowa&#263;"
  ]
  node [
    id 1710
    label "wydobywa&#263;"
  ]
  node [
    id 1711
    label "okre&#347;la&#263;"
  ]
  node [
    id 1712
    label "decydowa&#263;"
  ]
  node [
    id 1713
    label "signify"
  ]
  node [
    id 1714
    label "style"
  ]
  node [
    id 1715
    label "komunikowa&#263;"
  ]
  node [
    id 1716
    label "inform"
  ]
  node [
    id 1717
    label "znaczy&#263;"
  ]
  node [
    id 1718
    label "give_voice"
  ]
  node [
    id 1719
    label "oznacza&#263;"
  ]
  node [
    id 1720
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 1721
    label "represent"
  ]
  node [
    id 1722
    label "convey"
  ]
  node [
    id 1723
    label "arouse"
  ]
  node [
    id 1724
    label "determine"
  ]
  node [
    id 1725
    label "reakcja_chemiczna"
  ]
  node [
    id 1726
    label "uwydatnia&#263;"
  ]
  node [
    id 1727
    label "eksploatowa&#263;"
  ]
  node [
    id 1728
    label "uzyskiwa&#263;"
  ]
  node [
    id 1729
    label "wydostawa&#263;"
  ]
  node [
    id 1730
    label "wyjmowa&#263;"
  ]
  node [
    id 1731
    label "train"
  ]
  node [
    id 1732
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 1733
    label "wydawa&#263;"
  ]
  node [
    id 1734
    label "dobywa&#263;"
  ]
  node [
    id 1735
    label "ocala&#263;"
  ]
  node [
    id 1736
    label "excavate"
  ]
  node [
    id 1737
    label "g&#243;rnictwo"
  ]
  node [
    id 1738
    label "wiedzie&#263;"
  ]
  node [
    id 1739
    label "can"
  ]
  node [
    id 1740
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1741
    label "rozumie&#263;"
  ]
  node [
    id 1742
    label "szczeka&#263;"
  ]
  node [
    id 1743
    label "funkcjonowa&#263;"
  ]
  node [
    id 1744
    label "mawia&#263;"
  ]
  node [
    id 1745
    label "opowiada&#263;"
  ]
  node [
    id 1746
    label "chatter"
  ]
  node [
    id 1747
    label "niemowl&#281;"
  ]
  node [
    id 1748
    label "kosmetyk"
  ]
  node [
    id 1749
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 1750
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1751
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1752
    label "artykulator"
  ]
  node [
    id 1753
    label "kod"
  ]
  node [
    id 1754
    label "kawa&#322;ek"
  ]
  node [
    id 1755
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1756
    label "gramatyka"
  ]
  node [
    id 1757
    label "stylik"
  ]
  node [
    id 1758
    label "przet&#322;umaczenie"
  ]
  node [
    id 1759
    label "formalizowanie"
  ]
  node [
    id 1760
    label "ssanie"
  ]
  node [
    id 1761
    label "ssa&#263;"
  ]
  node [
    id 1762
    label "language"
  ]
  node [
    id 1763
    label "liza&#263;"
  ]
  node [
    id 1764
    label "napisa&#263;"
  ]
  node [
    id 1765
    label "konsonantyzm"
  ]
  node [
    id 1766
    label "wokalizm"
  ]
  node [
    id 1767
    label "pisa&#263;"
  ]
  node [
    id 1768
    label "fonetyka"
  ]
  node [
    id 1769
    label "jeniec"
  ]
  node [
    id 1770
    label "but"
  ]
  node [
    id 1771
    label "po_koroniarsku"
  ]
  node [
    id 1772
    label "kultura_duchowa"
  ]
  node [
    id 1773
    label "m&#243;wienie"
  ]
  node [
    id 1774
    label "pype&#263;"
  ]
  node [
    id 1775
    label "lizanie"
  ]
  node [
    id 1776
    label "pismo"
  ]
  node [
    id 1777
    label "formalizowa&#263;"
  ]
  node [
    id 1778
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1779
    label "rozumienie"
  ]
  node [
    id 1780
    label "makroglosja"
  ]
  node [
    id 1781
    label "jama_ustna"
  ]
  node [
    id 1782
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1783
    label "formacja_geologiczna"
  ]
  node [
    id 1784
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1785
    label "natural_language"
  ]
  node [
    id 1786
    label "s&#322;ownictwo"
  ]
  node [
    id 1787
    label "dysphonia"
  ]
  node [
    id 1788
    label "dysleksja"
  ]
  node [
    id 1789
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1790
    label "problem"
  ]
  node [
    id 1791
    label "pomys&#322;"
  ]
  node [
    id 1792
    label "zamys&#322;"
  ]
  node [
    id 1793
    label "idea"
  ]
  node [
    id 1794
    label "ideologia"
  ]
  node [
    id 1795
    label "byt"
  ]
  node [
    id 1796
    label "intelekt"
  ]
  node [
    id 1797
    label "Kant"
  ]
  node [
    id 1798
    label "p&#322;&#243;d"
  ]
  node [
    id 1799
    label "cel"
  ]
  node [
    id 1800
    label "istota"
  ]
  node [
    id 1801
    label "ideacja"
  ]
  node [
    id 1802
    label "pos&#322;uchanie"
  ]
  node [
    id 1803
    label "skumanie"
  ]
  node [
    id 1804
    label "orientacja"
  ]
  node [
    id 1805
    label "zorientowanie"
  ]
  node [
    id 1806
    label "teoria"
  ]
  node [
    id 1807
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1808
    label "clasp"
  ]
  node [
    id 1809
    label "forma"
  ]
  node [
    id 1810
    label "przem&#243;wienie"
  ]
  node [
    id 1811
    label "sprawa"
  ]
  node [
    id 1812
    label "subiekcja"
  ]
  node [
    id 1813
    label "problemat"
  ]
  node [
    id 1814
    label "jajko_Kolumba"
  ]
  node [
    id 1815
    label "obstruction"
  ]
  node [
    id 1816
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1817
    label "problematyka"
  ]
  node [
    id 1818
    label "trudno&#347;&#263;"
  ]
  node [
    id 1819
    label "pierepa&#322;ka"
  ]
  node [
    id 1820
    label "ambaras"
  ]
  node [
    id 1821
    label "pochwytanie"
  ]
  node [
    id 1822
    label "wording"
  ]
  node [
    id 1823
    label "wzbudzenie"
  ]
  node [
    id 1824
    label "withdrawal"
  ]
  node [
    id 1825
    label "capture"
  ]
  node [
    id 1826
    label "podniesienie"
  ]
  node [
    id 1827
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1828
    label "film"
  ]
  node [
    id 1829
    label "scena"
  ]
  node [
    id 1830
    label "zapisanie"
  ]
  node [
    id 1831
    label "prezentacja"
  ]
  node [
    id 1832
    label "rzucenie"
  ]
  node [
    id 1833
    label "zamkni&#281;cie"
  ]
  node [
    id 1834
    label "zabranie"
  ]
  node [
    id 1835
    label "poinformowanie"
  ]
  node [
    id 1836
    label "zaaresztowanie"
  ]
  node [
    id 1837
    label "wzi&#281;cie"
  ]
  node [
    id 1838
    label "podwini&#281;cie"
  ]
  node [
    id 1839
    label "zap&#322;acenie"
  ]
  node [
    id 1840
    label "przyodzianie"
  ]
  node [
    id 1841
    label "budowla"
  ]
  node [
    id 1842
    label "pokrycie"
  ]
  node [
    id 1843
    label "rozebranie"
  ]
  node [
    id 1844
    label "zak&#322;adka"
  ]
  node [
    id 1845
    label "poubieranie"
  ]
  node [
    id 1846
    label "infliction"
  ]
  node [
    id 1847
    label "przebranie"
  ]
  node [
    id 1848
    label "przywdzianie"
  ]
  node [
    id 1849
    label "obleczenie_si&#281;"
  ]
  node [
    id 1850
    label "utworzenie"
  ]
  node [
    id 1851
    label "twierdzenie"
  ]
  node [
    id 1852
    label "obleczenie"
  ]
  node [
    id 1853
    label "przymierzenie"
  ]
  node [
    id 1854
    label "wyko&#324;czenie"
  ]
  node [
    id 1855
    label "point"
  ]
  node [
    id 1856
    label "przewidzenie"
  ]
  node [
    id 1857
    label "pocz&#261;tki"
  ]
  node [
    id 1858
    label "ukradzenie"
  ]
  node [
    id 1859
    label "ukra&#347;&#263;"
  ]
  node [
    id 1860
    label "system"
  ]
  node [
    id 1861
    label "po_niemiecku"
  ]
  node [
    id 1862
    label "German"
  ]
  node [
    id 1863
    label "niemiecko"
  ]
  node [
    id 1864
    label "cenar"
  ]
  node [
    id 1865
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 1866
    label "europejski"
  ]
  node [
    id 1867
    label "strudel"
  ]
  node [
    id 1868
    label "niemiec"
  ]
  node [
    id 1869
    label "zachodnioeuropejski"
  ]
  node [
    id 1870
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 1871
    label "junkers"
  ]
  node [
    id 1872
    label "szwabski"
  ]
  node [
    id 1873
    label "szwabsko"
  ]
  node [
    id 1874
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 1875
    label "po_szwabsku"
  ]
  node [
    id 1876
    label "platt"
  ]
  node [
    id 1877
    label "europejsko"
  ]
  node [
    id 1878
    label "&#380;o&#322;nierz"
  ]
  node [
    id 1879
    label "saper"
  ]
  node [
    id 1880
    label "prekursor"
  ]
  node [
    id 1881
    label "osadnik"
  ]
  node [
    id 1882
    label "skaut"
  ]
  node [
    id 1883
    label "g&#322;osiciel"
  ]
  node [
    id 1884
    label "ciasto"
  ]
  node [
    id 1885
    label "taniec_ludowy"
  ]
  node [
    id 1886
    label "melodia"
  ]
  node [
    id 1887
    label "taniec"
  ]
  node [
    id 1888
    label "podgrzewacz"
  ]
  node [
    id 1889
    label "samolot_wojskowy"
  ]
  node [
    id 1890
    label "moreska"
  ]
  node [
    id 1891
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 1892
    label "zachodni"
  ]
  node [
    id 1893
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 1894
    label "langosz"
  ]
  node [
    id 1895
    label "po_europejsku"
  ]
  node [
    id 1896
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1897
    label "European"
  ]
  node [
    id 1898
    label "charakterystyczny"
  ]
  node [
    id 1899
    label "innowator"
  ]
  node [
    id 1900
    label "inicjator"
  ]
  node [
    id 1901
    label "substancja"
  ]
  node [
    id 1902
    label "przybysz"
  ]
  node [
    id 1903
    label "oczyszczanie_si&#281;"
  ]
  node [
    id 1904
    label "oczyszcza&#263;_si&#281;"
  ]
  node [
    id 1905
    label "osadnictwo"
  ]
  node [
    id 1906
    label "planter"
  ]
  node [
    id 1907
    label "zbiornik"
  ]
  node [
    id 1908
    label "skauting"
  ]
  node [
    id 1909
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 1910
    label "harcap"
  ]
  node [
    id 1911
    label "wojsko"
  ]
  node [
    id 1912
    label "elew"
  ]
  node [
    id 1913
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 1914
    label "demobilizowanie"
  ]
  node [
    id 1915
    label "Gurkha"
  ]
  node [
    id 1916
    label "zdemobilizowanie"
  ]
  node [
    id 1917
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 1918
    label "so&#322;dat"
  ]
  node [
    id 1919
    label "demobilizowa&#263;"
  ]
  node [
    id 1920
    label "mundurowy"
  ]
  node [
    id 1921
    label "rota"
  ]
  node [
    id 1922
    label "zdemobilizowa&#263;"
  ]
  node [
    id 1923
    label "walcz&#261;cy"
  ]
  node [
    id 1924
    label "&#380;o&#322;dowy"
  ]
  node [
    id 1925
    label "in&#380;ynier_wojskowy"
  ]
  node [
    id 1926
    label "wojska_in&#380;ynieryjne"
  ]
  node [
    id 1927
    label "&#347;wiadek_Jehowy"
  ]
  node [
    id 1928
    label "system_autonomiczny"
  ]
  node [
    id 1929
    label "biocybernetyka"
  ]
  node [
    id 1930
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1931
    label "neurocybernetyka"
  ]
  node [
    id 1932
    label "rachunek_operatorowy"
  ]
  node [
    id 1933
    label "kryptologia"
  ]
  node [
    id 1934
    label "logicyzm"
  ]
  node [
    id 1935
    label "logika"
  ]
  node [
    id 1936
    label "matematyka_czysta"
  ]
  node [
    id 1937
    label "forsing"
  ]
  node [
    id 1938
    label "supremum"
  ]
  node [
    id 1939
    label "modelowanie_matematyczne"
  ]
  node [
    id 1940
    label "matma"
  ]
  node [
    id 1941
    label "teoria_katastrof"
  ]
  node [
    id 1942
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1943
    label "kierunek"
  ]
  node [
    id 1944
    label "jednostka"
  ]
  node [
    id 1945
    label "fizyka_matematyczna"
  ]
  node [
    id 1946
    label "teoria_graf&#243;w"
  ]
  node [
    id 1947
    label "rzut"
  ]
  node [
    id 1948
    label "rachunki"
  ]
  node [
    id 1949
    label "topologia_algebraiczna"
  ]
  node [
    id 1950
    label "matematyka_stosowana"
  ]
  node [
    id 1951
    label "funkcja"
  ]
  node [
    id 1952
    label "infimum"
  ]
  node [
    id 1953
    label "zboczenie"
  ]
  node [
    id 1954
    label "om&#243;wienie"
  ]
  node [
    id 1955
    label "sponiewieranie"
  ]
  node [
    id 1956
    label "discipline"
  ]
  node [
    id 1957
    label "omawia&#263;"
  ]
  node [
    id 1958
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1959
    label "tre&#347;&#263;"
  ]
  node [
    id 1960
    label "sponiewiera&#263;"
  ]
  node [
    id 1961
    label "element"
  ]
  node [
    id 1962
    label "entity"
  ]
  node [
    id 1963
    label "tematyka"
  ]
  node [
    id 1964
    label "w&#261;tek"
  ]
  node [
    id 1965
    label "charakter"
  ]
  node [
    id 1966
    label "zbaczanie"
  ]
  node [
    id 1967
    label "program_nauczania"
  ]
  node [
    id 1968
    label "om&#243;wi&#263;"
  ]
  node [
    id 1969
    label "omawianie"
  ]
  node [
    id 1970
    label "thing"
  ]
  node [
    id 1971
    label "zbacza&#263;"
  ]
  node [
    id 1972
    label "zboczy&#263;"
  ]
  node [
    id 1973
    label "przebieg"
  ]
  node [
    id 1974
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1975
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1976
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1977
    label "praktyka"
  ]
  node [
    id 1978
    label "przeorientowywanie"
  ]
  node [
    id 1979
    label "studia"
  ]
  node [
    id 1980
    label "bok"
  ]
  node [
    id 1981
    label "skr&#281;canie"
  ]
  node [
    id 1982
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1983
    label "przeorientowywa&#263;"
  ]
  node [
    id 1984
    label "orientowanie"
  ]
  node [
    id 1985
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1986
    label "przeorientowanie"
  ]
  node [
    id 1987
    label "przeorientowa&#263;"
  ]
  node [
    id 1988
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1989
    label "metoda"
  ]
  node [
    id 1990
    label "ty&#322;"
  ]
  node [
    id 1991
    label "zorientowa&#263;"
  ]
  node [
    id 1992
    label "g&#243;ra"
  ]
  node [
    id 1993
    label "orientowa&#263;"
  ]
  node [
    id 1994
    label "prz&#243;d"
  ]
  node [
    id 1995
    label "bearing"
  ]
  node [
    id 1996
    label "skr&#281;cenie"
  ]
  node [
    id 1997
    label "arithmetic"
  ]
  node [
    id 1998
    label "logicism"
  ]
  node [
    id 1999
    label "doktryna_filozoficzna"
  ]
  node [
    id 2000
    label "filozofia_matematyki"
  ]
  node [
    id 2001
    label "ograniczenie"
  ]
  node [
    id 2002
    label "czyn"
  ]
  node [
    id 2003
    label "addytywno&#347;&#263;"
  ]
  node [
    id 2004
    label "function"
  ]
  node [
    id 2005
    label "zastosowanie"
  ]
  node [
    id 2006
    label "funkcjonowanie"
  ]
  node [
    id 2007
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 2008
    label "powierzanie"
  ]
  node [
    id 2009
    label "przeciwdziedzina"
  ]
  node [
    id 2010
    label "stawia&#263;"
  ]
  node [
    id 2011
    label "wakowa&#263;"
  ]
  node [
    id 2012
    label "postawi&#263;"
  ]
  node [
    id 2013
    label "dow&#243;d"
  ]
  node [
    id 2014
    label "armia"
  ]
  node [
    id 2015
    label "nawr&#243;t_choroby"
  ]
  node [
    id 2016
    label "potomstwo"
  ]
  node [
    id 2017
    label "odwzorowanie"
  ]
  node [
    id 2018
    label "rysunek"
  ]
  node [
    id 2019
    label "scene"
  ]
  node [
    id 2020
    label "throw"
  ]
  node [
    id 2021
    label "float"
  ]
  node [
    id 2022
    label "projection"
  ]
  node [
    id 2023
    label "injection"
  ]
  node [
    id 2024
    label "blow"
  ]
  node [
    id 2025
    label "k&#322;ad"
  ]
  node [
    id 2026
    label "mold"
  ]
  node [
    id 2027
    label "przyswoi&#263;"
  ]
  node [
    id 2028
    label "one"
  ]
  node [
    id 2029
    label "ewoluowanie"
  ]
  node [
    id 2030
    label "skala"
  ]
  node [
    id 2031
    label "przyswajanie"
  ]
  node [
    id 2032
    label "wyewoluowanie"
  ]
  node [
    id 2033
    label "przeliczy&#263;"
  ]
  node [
    id 2034
    label "wyewoluowa&#263;"
  ]
  node [
    id 2035
    label "ewoluowa&#263;"
  ]
  node [
    id 2036
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 2037
    label "liczba_naturalna"
  ]
  node [
    id 2038
    label "czynnik_biotyczny"
  ]
  node [
    id 2039
    label "individual"
  ]
  node [
    id 2040
    label "przyswaja&#263;"
  ]
  node [
    id 2041
    label "przyswojenie"
  ]
  node [
    id 2042
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 2043
    label "starzenie_si&#281;"
  ]
  node [
    id 2044
    label "przeliczanie"
  ]
  node [
    id 2045
    label "przelicza&#263;"
  ]
  node [
    id 2046
    label "przeliczenie"
  ]
  node [
    id 2047
    label "izomorfizm"
  ]
  node [
    id 2048
    label "teoremat"
  ]
  node [
    id 2049
    label "logizacja"
  ]
  node [
    id 2050
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 2051
    label "analityka"
  ]
  node [
    id 2052
    label "predykat"
  ]
  node [
    id 2053
    label "rozumno&#347;&#263;"
  ]
  node [
    id 2054
    label "metamatematyka"
  ]
  node [
    id 2055
    label "filozofia"
  ]
  node [
    id 2056
    label "operacja_logiczna"
  ]
  node [
    id 2057
    label "sylogistyka"
  ]
  node [
    id 2058
    label "dialektyka"
  ]
  node [
    id 2059
    label "podzia&#322;_logiczny"
  ]
  node [
    id 2060
    label "logistics"
  ]
  node [
    id 2061
    label "kryptografia"
  ]
  node [
    id 2062
    label "kryptoanaliza"
  ]
  node [
    id 2063
    label "HP"
  ]
  node [
    id 2064
    label "dost&#281;p"
  ]
  node [
    id 2065
    label "infa"
  ]
  node [
    id 2066
    label "przetwarzanie_informacji"
  ]
  node [
    id 2067
    label "sztuczna_inteligencja"
  ]
  node [
    id 2068
    label "gramatyka_formalna"
  ]
  node [
    id 2069
    label "zamek"
  ]
  node [
    id 2070
    label "dziedzina_informatyki"
  ]
  node [
    id 2071
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 2072
    label "artefakt"
  ]
  node [
    id 2073
    label "operacja"
  ]
  node [
    id 2074
    label "konto"
  ]
  node [
    id 2075
    label "has&#322;o"
  ]
  node [
    id 2076
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 2077
    label "oprogramowanie"
  ]
  node [
    id 2078
    label "odinstalowywa&#263;"
  ]
  node [
    id 2079
    label "spis"
  ]
  node [
    id 2080
    label "zaprezentowanie"
  ]
  node [
    id 2081
    label "podprogram"
  ]
  node [
    id 2082
    label "ogranicznik_referencyjny"
  ]
  node [
    id 2083
    label "course_of_study"
  ]
  node [
    id 2084
    label "booklet"
  ]
  node [
    id 2085
    label "odinstalowanie"
  ]
  node [
    id 2086
    label "broszura"
  ]
  node [
    id 2087
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 2088
    label "kana&#322;"
  ]
  node [
    id 2089
    label "teleferie"
  ]
  node [
    id 2090
    label "struktura_organizacyjna"
  ]
  node [
    id 2091
    label "pirat"
  ]
  node [
    id 2092
    label "zaprezentowa&#263;"
  ]
  node [
    id 2093
    label "prezentowanie"
  ]
  node [
    id 2094
    label "prezentowa&#263;"
  ]
  node [
    id 2095
    label "interfejs"
  ]
  node [
    id 2096
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 2097
    label "okno"
  ]
  node [
    id 2098
    label "blok"
  ]
  node [
    id 2099
    label "folder"
  ]
  node [
    id 2100
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 2101
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 2102
    label "ram&#243;wka"
  ]
  node [
    id 2103
    label "emitowa&#263;"
  ]
  node [
    id 2104
    label "emitowanie"
  ]
  node [
    id 2105
    label "odinstalowywanie"
  ]
  node [
    id 2106
    label "instrukcja"
  ]
  node [
    id 2107
    label "deklaracja"
  ]
  node [
    id 2108
    label "sekcja_krytyczna"
  ]
  node [
    id 2109
    label "furkacja"
  ]
  node [
    id 2110
    label "podstawa"
  ]
  node [
    id 2111
    label "odinstalowa&#263;"
  ]
  node [
    id 2112
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 2113
    label "drzwi"
  ]
  node [
    id 2114
    label "blokada"
  ]
  node [
    id 2115
    label "wjazd"
  ]
  node [
    id 2116
    label "bro&#324;_palna"
  ]
  node [
    id 2117
    label "brama"
  ]
  node [
    id 2118
    label "blockage"
  ]
  node [
    id 2119
    label "zapi&#281;cie"
  ]
  node [
    id 2120
    label "tercja"
  ]
  node [
    id 2121
    label "ekspres"
  ]
  node [
    id 2122
    label "komora_zamkowa"
  ]
  node [
    id 2123
    label "stra&#380;nica"
  ]
  node [
    id 2124
    label "fortyfikacja"
  ]
  node [
    id 2125
    label "rezydencja"
  ]
  node [
    id 2126
    label "bramka"
  ]
  node [
    id 2127
    label "iglica"
  ]
  node [
    id 2128
    label "zagrywka"
  ]
  node [
    id 2129
    label "hokej"
  ]
  node [
    id 2130
    label "baszta"
  ]
  node [
    id 2131
    label "fastener"
  ]
  node [
    id 2132
    label "Wawel"
  ]
  node [
    id 2133
    label "ciemno&#347;&#263;"
  ]
  node [
    id 2134
    label "czynnik"
  ]
  node [
    id 2135
    label "radiologia"
  ]
  node [
    id 2136
    label "wada"
  ]
  node [
    id 2137
    label "psychologia"
  ]
  node [
    id 2138
    label "nauka_humanistyczna"
  ]
  node [
    id 2139
    label "liturgika"
  ]
  node [
    id 2140
    label "religiologia"
  ]
  node [
    id 2141
    label "homiletyka"
  ]
  node [
    id 2142
    label "teogonia"
  ]
  node [
    id 2143
    label "teologia_narracyjna"
  ]
  node [
    id 2144
    label "teologia_pastoralna"
  ]
  node [
    id 2145
    label "dogmatyka"
  ]
  node [
    id 2146
    label "misjologia"
  ]
  node [
    id 2147
    label "eschatologia"
  ]
  node [
    id 2148
    label "katechetyka"
  ]
  node [
    id 2149
    label "patrologia"
  ]
  node [
    id 2150
    label "antropologia_religijna"
  ]
  node [
    id 2151
    label "eschatology"
  ]
  node [
    id 2152
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 2153
    label "liturgics"
  ]
  node [
    id 2154
    label "antropologia_misyjna"
  ]
  node [
    id 2155
    label "metodyka"
  ]
  node [
    id 2156
    label "mitologia"
  ]
  node [
    id 2157
    label "charytologia"
  ]
  node [
    id 2158
    label "sakramentologia"
  ]
  node [
    id 2159
    label "mariologia"
  ]
  node [
    id 2160
    label "protologia"
  ]
  node [
    id 2161
    label "religioznawstwo"
  ]
  node [
    id 2162
    label "raj_utracony"
  ]
  node [
    id 2163
    label "umieranie"
  ]
  node [
    id 2164
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 2165
    label "prze&#380;ywanie"
  ]
  node [
    id 2166
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 2167
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 2168
    label "po&#322;&#243;g"
  ]
  node [
    id 2169
    label "umarcie"
  ]
  node [
    id 2170
    label "subsistence"
  ]
  node [
    id 2171
    label "power"
  ]
  node [
    id 2172
    label "okres_noworodkowy"
  ]
  node [
    id 2173
    label "wiek_matuzalemowy"
  ]
  node [
    id 2174
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 2175
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 2176
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 2177
    label "do&#380;ywanie"
  ]
  node [
    id 2178
    label "dzieci&#324;stwo"
  ]
  node [
    id 2179
    label "andropauza"
  ]
  node [
    id 2180
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 2181
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 2182
    label "menopauza"
  ]
  node [
    id 2183
    label "&#347;mier&#263;"
  ]
  node [
    id 2184
    label "koleje_losu"
  ]
  node [
    id 2185
    label "bycie"
  ]
  node [
    id 2186
    label "zegar_biologiczny"
  ]
  node [
    id 2187
    label "szwung"
  ]
  node [
    id 2188
    label "warunki"
  ]
  node [
    id 2189
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 2190
    label "niemowl&#281;ctwo"
  ]
  node [
    id 2191
    label "&#380;ywy"
  ]
  node [
    id 2192
    label "life"
  ]
  node [
    id 2193
    label "staro&#347;&#263;"
  ]
  node [
    id 2194
    label "energy"
  ]
  node [
    id 2195
    label "trwanie"
  ]
  node [
    id 2196
    label "przechodzenie"
  ]
  node [
    id 2197
    label "wytrzymywanie"
  ]
  node [
    id 2198
    label "obejrzenie"
  ]
  node [
    id 2199
    label "widzenie"
  ]
  node [
    id 2200
    label "urzeczywistnianie"
  ]
  node [
    id 2201
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 2202
    label "przeszkodzenie"
  ]
  node [
    id 2203
    label "produkowanie"
  ]
  node [
    id 2204
    label "znikni&#281;cie"
  ]
  node [
    id 2205
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 2206
    label "przeszkadzanie"
  ]
  node [
    id 2207
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 2208
    label "wyprodukowanie"
  ]
  node [
    id 2209
    label "utrzymywanie"
  ]
  node [
    id 2210
    label "subsystencja"
  ]
  node [
    id 2211
    label "utrzyma&#263;"
  ]
  node [
    id 2212
    label "egzystencja"
  ]
  node [
    id 2213
    label "wy&#380;ywienie"
  ]
  node [
    id 2214
    label "ontologicznie"
  ]
  node [
    id 2215
    label "utrzymanie"
  ]
  node [
    id 2216
    label "potencja"
  ]
  node [
    id 2217
    label "utrzymywa&#263;"
  ]
  node [
    id 2218
    label "ocieranie_si&#281;"
  ]
  node [
    id 2219
    label "otoczenie_si&#281;"
  ]
  node [
    id 2220
    label "posiedzenie"
  ]
  node [
    id 2221
    label "otarcie_si&#281;"
  ]
  node [
    id 2222
    label "atakowanie"
  ]
  node [
    id 2223
    label "otaczanie_si&#281;"
  ]
  node [
    id 2224
    label "zmierzanie"
  ]
  node [
    id 2225
    label "residency"
  ]
  node [
    id 2226
    label "sojourn"
  ]
  node [
    id 2227
    label "wychodzenie"
  ]
  node [
    id 2228
    label "tkwienie"
  ]
  node [
    id 2229
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 2230
    label "absolutorium"
  ]
  node [
    id 2231
    label "dzia&#322;anie"
  ]
  node [
    id 2232
    label "ton"
  ]
  node [
    id 2233
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 2234
    label "korkowanie"
  ]
  node [
    id 2235
    label "death"
  ]
  node [
    id 2236
    label "zabijanie"
  ]
  node [
    id 2237
    label "przestawanie"
  ]
  node [
    id 2238
    label "odumieranie"
  ]
  node [
    id 2239
    label "zdychanie"
  ]
  node [
    id 2240
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 2241
    label "zanikanie"
  ]
  node [
    id 2242
    label "ko&#324;czenie"
  ]
  node [
    id 2243
    label "nieuleczalnie_chory"
  ]
  node [
    id 2244
    label "ciekawy"
  ]
  node [
    id 2245
    label "naturalny"
  ]
  node [
    id 2246
    label "&#380;ywo"
  ]
  node [
    id 2247
    label "o&#380;ywianie"
  ]
  node [
    id 2248
    label "g&#322;&#281;boki"
  ]
  node [
    id 2249
    label "wyra&#378;ny"
  ]
  node [
    id 2250
    label "czynny"
  ]
  node [
    id 2251
    label "aktualny"
  ]
  node [
    id 2252
    label "zgrabny"
  ]
  node [
    id 2253
    label "prawdziwy"
  ]
  node [
    id 2254
    label "realistyczny"
  ]
  node [
    id 2255
    label "energiczny"
  ]
  node [
    id 2256
    label "odumarcie"
  ]
  node [
    id 2257
    label "przestanie"
  ]
  node [
    id 2258
    label "dysponowanie_si&#281;"
  ]
  node [
    id 2259
    label "pomarcie"
  ]
  node [
    id 2260
    label "die"
  ]
  node [
    id 2261
    label "sko&#324;czenie"
  ]
  node [
    id 2262
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 2263
    label "zdechni&#281;cie"
  ]
  node [
    id 2264
    label "zabicie"
  ]
  node [
    id 2265
    label "procedura"
  ]
  node [
    id 2266
    label "proces_biologiczny"
  ]
  node [
    id 2267
    label "z&#322;ote_czasy"
  ]
  node [
    id 2268
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 2269
    label "process"
  ]
  node [
    id 2270
    label "cycle"
  ]
  node [
    id 2271
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 2272
    label "adolescence"
  ]
  node [
    id 2273
    label "wiek"
  ]
  node [
    id 2274
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 2275
    label "zielone_lata"
  ]
  node [
    id 2276
    label "rozwi&#261;zanie"
  ]
  node [
    id 2277
    label "zlec"
  ]
  node [
    id 2278
    label "zlegni&#281;cie"
  ]
  node [
    id 2279
    label "defenestracja"
  ]
  node [
    id 2280
    label "agonia"
  ]
  node [
    id 2281
    label "kres"
  ]
  node [
    id 2282
    label "mogi&#322;a"
  ]
  node [
    id 2283
    label "kres_&#380;ycia"
  ]
  node [
    id 2284
    label "upadek"
  ]
  node [
    id 2285
    label "szeol"
  ]
  node [
    id 2286
    label "pogrzebanie"
  ]
  node [
    id 2287
    label "istota_nadprzyrodzona"
  ]
  node [
    id 2288
    label "&#380;a&#322;oba"
  ]
  node [
    id 2289
    label "pogrzeb"
  ]
  node [
    id 2290
    label "majority"
  ]
  node [
    id 2291
    label "osiemnastoletni"
  ]
  node [
    id 2292
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 2293
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 2294
    label "age"
  ]
  node [
    id 2295
    label "przekwitanie"
  ]
  node [
    id 2296
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 2297
    label "dzieci&#281;ctwo"
  ]
  node [
    id 2298
    label "energia"
  ]
  node [
    id 2299
    label "zapa&#322;"
  ]
  node [
    id 2300
    label "jedyny"
  ]
  node [
    id 2301
    label "zdr&#243;w"
  ]
  node [
    id 2302
    label "calu&#347;ko"
  ]
  node [
    id 2303
    label "kompletny"
  ]
  node [
    id 2304
    label "pe&#322;ny"
  ]
  node [
    id 2305
    label "podobny"
  ]
  node [
    id 2306
    label "ca&#322;o"
  ]
  node [
    id 2307
    label "kompletnie"
  ]
  node [
    id 2308
    label "zupe&#322;ny"
  ]
  node [
    id 2309
    label "w_pizdu"
  ]
  node [
    id 2310
    label "przypominanie"
  ]
  node [
    id 2311
    label "podobnie"
  ]
  node [
    id 2312
    label "upodabnianie_si&#281;"
  ]
  node [
    id 2313
    label "upodobnienie"
  ]
  node [
    id 2314
    label "drugi"
  ]
  node [
    id 2315
    label "taki"
  ]
  node [
    id 2316
    label "upodobnienie_si&#281;"
  ]
  node [
    id 2317
    label "zasymilowanie"
  ]
  node [
    id 2318
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 2319
    label "ukochany"
  ]
  node [
    id 2320
    label "najlepszy"
  ]
  node [
    id 2321
    label "optymalnie"
  ]
  node [
    id 2322
    label "doros&#322;y"
  ]
  node [
    id 2323
    label "znaczny"
  ]
  node [
    id 2324
    label "niema&#322;o"
  ]
  node [
    id 2325
    label "wiele"
  ]
  node [
    id 2326
    label "rozwini&#281;ty"
  ]
  node [
    id 2327
    label "dorodny"
  ]
  node [
    id 2328
    label "wa&#380;ny"
  ]
  node [
    id 2329
    label "du&#380;o"
  ]
  node [
    id 2330
    label "nieograniczony"
  ]
  node [
    id 2331
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 2332
    label "satysfakcja"
  ]
  node [
    id 2333
    label "bezwzgl&#281;dny"
  ]
  node [
    id 2334
    label "wype&#322;nienie"
  ]
  node [
    id 2335
    label "pe&#322;no"
  ]
  node [
    id 2336
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 2337
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 2338
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 2339
    label "r&#243;wny"
  ]
  node [
    id 2340
    label "nieuszkodzony"
  ]
  node [
    id 2341
    label "odpowiednio"
  ]
  node [
    id 2342
    label "kwota"
  ]
  node [
    id 2343
    label "wydanie"
  ]
  node [
    id 2344
    label "remainder"
  ]
  node [
    id 2345
    label "pozosta&#322;y"
  ]
  node [
    id 2346
    label "wyda&#263;"
  ]
  node [
    id 2347
    label "Rzym_Zachodni"
  ]
  node [
    id 2348
    label "whole"
  ]
  node [
    id 2349
    label "Rzym_Wschodni"
  ]
  node [
    id 2350
    label "wynie&#347;&#263;"
  ]
  node [
    id 2351
    label "pieni&#261;dze"
  ]
  node [
    id 2352
    label "limit"
  ]
  node [
    id 2353
    label "wynosi&#263;"
  ]
  node [
    id 2354
    label "inny"
  ]
  node [
    id 2355
    label "delivery"
  ]
  node [
    id 2356
    label "zdarzenie_si&#281;"
  ]
  node [
    id 2357
    label "rendition"
  ]
  node [
    id 2358
    label "d&#378;wi&#281;k"
  ]
  node [
    id 2359
    label "egzemplarz"
  ]
  node [
    id 2360
    label "impression"
  ]
  node [
    id 2361
    label "publikacja"
  ]
  node [
    id 2362
    label "zadenuncjowanie"
  ]
  node [
    id 2363
    label "zapach"
  ]
  node [
    id 2364
    label "wytworzenie"
  ]
  node [
    id 2365
    label "issue"
  ]
  node [
    id 2366
    label "podanie"
  ]
  node [
    id 2367
    label "odmiana"
  ]
  node [
    id 2368
    label "ujawnienie"
  ]
  node [
    id 2369
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 2370
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 2371
    label "powierzy&#263;"
  ]
  node [
    id 2372
    label "plon"
  ]
  node [
    id 2373
    label "skojarzy&#263;"
  ]
  node [
    id 2374
    label "zadenuncjowa&#263;"
  ]
  node [
    id 2375
    label "impart"
  ]
  node [
    id 2376
    label "da&#263;"
  ]
  node [
    id 2377
    label "wydawnictwo"
  ]
  node [
    id 2378
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 2379
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 2380
    label "wiano"
  ]
  node [
    id 2381
    label "produkcja"
  ]
  node [
    id 2382
    label "picture"
  ]
  node [
    id 2383
    label "poda&#263;"
  ]
  node [
    id 2384
    label "wprowadzi&#263;"
  ]
  node [
    id 2385
    label "wytworzy&#263;"
  ]
  node [
    id 2386
    label "dress"
  ]
  node [
    id 2387
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 2388
    label "tajemnica"
  ]
  node [
    id 2389
    label "panna_na_wydaniu"
  ]
  node [
    id 2390
    label "ujawni&#263;"
  ]
  node [
    id 2391
    label "surrender"
  ]
  node [
    id 2392
    label "kojarzy&#263;"
  ]
  node [
    id 2393
    label "dawa&#263;"
  ]
  node [
    id 2394
    label "wprowadza&#263;"
  ]
  node [
    id 2395
    label "podawa&#263;"
  ]
  node [
    id 2396
    label "ujawnia&#263;"
  ]
  node [
    id 2397
    label "placard"
  ]
  node [
    id 2398
    label "powierza&#263;"
  ]
  node [
    id 2399
    label "denuncjowa&#263;"
  ]
  node [
    id 2400
    label "pensum"
  ]
  node [
    id 2401
    label "enroll"
  ]
  node [
    id 2402
    label "minimum"
  ]
  node [
    id 2403
    label "granica"
  ]
  node [
    id 2404
    label "otworzysty"
  ]
  node [
    id 2405
    label "aktywny"
  ]
  node [
    id 2406
    label "publiczny"
  ]
  node [
    id 2407
    label "prostoduszny"
  ]
  node [
    id 2408
    label "jawnie"
  ]
  node [
    id 2409
    label "bezpo&#347;redni"
  ]
  node [
    id 2410
    label "kontaktowy"
  ]
  node [
    id 2411
    label "otwarcie"
  ]
  node [
    id 2412
    label "ewidentny"
  ]
  node [
    id 2413
    label "dost&#281;pny"
  ]
  node [
    id 2414
    label "upublicznianie"
  ]
  node [
    id 2415
    label "jawny"
  ]
  node [
    id 2416
    label "upublicznienie"
  ]
  node [
    id 2417
    label "publicznie"
  ]
  node [
    id 2418
    label "oczywisty"
  ]
  node [
    id 2419
    label "pewny"
  ]
  node [
    id 2420
    label "ewidentnie"
  ]
  node [
    id 2421
    label "jednoznaczny"
  ]
  node [
    id 2422
    label "zdecydowanie"
  ]
  node [
    id 2423
    label "zauwa&#380;alny"
  ]
  node [
    id 2424
    label "dowolny"
  ]
  node [
    id 2425
    label "rozleg&#322;y"
  ]
  node [
    id 2426
    label "nieograniczenie"
  ]
  node [
    id 2427
    label "mo&#380;liwy"
  ]
  node [
    id 2428
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 2429
    label "odblokowanie_si&#281;"
  ]
  node [
    id 2430
    label "zrozumia&#322;y"
  ]
  node [
    id 2431
    label "dost&#281;pnie"
  ]
  node [
    id 2432
    label "&#322;atwy"
  ]
  node [
    id 2433
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 2434
    label "przyst&#281;pnie"
  ]
  node [
    id 2435
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 2436
    label "bezpo&#347;rednio"
  ]
  node [
    id 2437
    label "szczery"
  ]
  node [
    id 2438
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 2439
    label "aktualnie"
  ]
  node [
    id 2440
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 2441
    label "aktualizowanie"
  ]
  node [
    id 2442
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 2443
    label "uaktualnienie"
  ]
  node [
    id 2444
    label "realny"
  ]
  node [
    id 2445
    label "zdolny"
  ]
  node [
    id 2446
    label "czynnie"
  ]
  node [
    id 2447
    label "uczynnianie"
  ]
  node [
    id 2448
    label "aktywnie"
  ]
  node [
    id 2449
    label "istotny"
  ]
  node [
    id 2450
    label "faktyczny"
  ]
  node [
    id 2451
    label "uczynnienie"
  ]
  node [
    id 2452
    label "prostodusznie"
  ]
  node [
    id 2453
    label "przyst&#281;pny"
  ]
  node [
    id 2454
    label "kontaktowo"
  ]
  node [
    id 2455
    label "jawno"
  ]
  node [
    id 2456
    label "rozpocz&#281;cie"
  ]
  node [
    id 2457
    label "udost&#281;pnienie"
  ]
  node [
    id 2458
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 2459
    label "opening"
  ]
  node [
    id 2460
    label "gra&#263;"
  ]
  node [
    id 2461
    label "logowanie"
  ]
  node [
    id 2462
    label "plik"
  ]
  node [
    id 2463
    label "s&#261;d"
  ]
  node [
    id 2464
    label "adres_internetowy"
  ]
  node [
    id 2465
    label "serwis_internetowy"
  ]
  node [
    id 2466
    label "pagina"
  ]
  node [
    id 2467
    label "podmiot"
  ]
  node [
    id 2468
    label "voice"
  ]
  node [
    id 2469
    label "internet"
  ]
  node [
    id 2470
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 2471
    label "osobowo&#347;&#263;"
  ]
  node [
    id 2472
    label "prawo"
  ]
  node [
    id 2473
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 2474
    label "nauka_prawa"
  ]
  node [
    id 2475
    label "utw&#243;r"
  ]
  node [
    id 2476
    label "charakterystyka"
  ]
  node [
    id 2477
    label "zaistnie&#263;"
  ]
  node [
    id 2478
    label "Osjan"
  ]
  node [
    id 2479
    label "kto&#347;"
  ]
  node [
    id 2480
    label "wygl&#261;d"
  ]
  node [
    id 2481
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 2482
    label "trim"
  ]
  node [
    id 2483
    label "poby&#263;"
  ]
  node [
    id 2484
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 2485
    label "Aspazja"
  ]
  node [
    id 2486
    label "punkt_widzenia"
  ]
  node [
    id 2487
    label "kompleksja"
  ]
  node [
    id 2488
    label "wytrzyma&#263;"
  ]
  node [
    id 2489
    label "budowa"
  ]
  node [
    id 2490
    label "formacja"
  ]
  node [
    id 2491
    label "pozosta&#263;"
  ]
  node [
    id 2492
    label "przedstawienie"
  ]
  node [
    id 2493
    label "go&#347;&#263;"
  ]
  node [
    id 2494
    label "kszta&#322;t"
  ]
  node [
    id 2495
    label "cord"
  ]
  node [
    id 2496
    label "trasa"
  ]
  node [
    id 2497
    label "tract"
  ]
  node [
    id 2498
    label "materia&#322;_zecerski"
  ]
  node [
    id 2499
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 2500
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 2501
    label "jard"
  ]
  node [
    id 2502
    label "szczep"
  ]
  node [
    id 2503
    label "phreaker"
  ]
  node [
    id 2504
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 2505
    label "grupa_organizm&#243;w"
  ]
  node [
    id 2506
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 2507
    label "access"
  ]
  node [
    id 2508
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 2509
    label "billing"
  ]
  node [
    id 2510
    label "szpaler"
  ]
  node [
    id 2511
    label "sztrych"
  ]
  node [
    id 2512
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 2513
    label "drzewo_genealogiczne"
  ]
  node [
    id 2514
    label "transporter"
  ]
  node [
    id 2515
    label "line"
  ]
  node [
    id 2516
    label "przew&#243;d"
  ]
  node [
    id 2517
    label "granice"
  ]
  node [
    id 2518
    label "kontakt"
  ]
  node [
    id 2519
    label "przewo&#378;nik"
  ]
  node [
    id 2520
    label "przystanek"
  ]
  node [
    id 2521
    label "linijka"
  ]
  node [
    id 2522
    label "uporz&#261;dkowanie"
  ]
  node [
    id 2523
    label "coalescence"
  ]
  node [
    id 2524
    label "Ural"
  ]
  node [
    id 2525
    label "tekst"
  ]
  node [
    id 2526
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 2527
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 2528
    label "koniec"
  ]
  node [
    id 2529
    label "podkatalog"
  ]
  node [
    id 2530
    label "nadpisa&#263;"
  ]
  node [
    id 2531
    label "nadpisanie"
  ]
  node [
    id 2532
    label "bundle"
  ]
  node [
    id 2533
    label "nadpisywanie"
  ]
  node [
    id 2534
    label "paczka"
  ]
  node [
    id 2535
    label "nadpisywa&#263;"
  ]
  node [
    id 2536
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 2537
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 2538
    label "zwierciad&#322;o"
  ]
  node [
    id 2539
    label "capacity"
  ]
  node [
    id 2540
    label "poznanie"
  ]
  node [
    id 2541
    label "leksem"
  ]
  node [
    id 2542
    label "blaszka"
  ]
  node [
    id 2543
    label "kantyzm"
  ]
  node [
    id 2544
    label "zdolno&#347;&#263;"
  ]
  node [
    id 2545
    label "do&#322;ek"
  ]
  node [
    id 2546
    label "zawarto&#347;&#263;"
  ]
  node [
    id 2547
    label "gwiazda"
  ]
  node [
    id 2548
    label "formality"
  ]
  node [
    id 2549
    label "mode"
  ]
  node [
    id 2550
    label "morfem"
  ]
  node [
    id 2551
    label "kielich"
  ]
  node [
    id 2552
    label "ornamentyka"
  ]
  node [
    id 2553
    label "pasmo"
  ]
  node [
    id 2554
    label "zwyczaj"
  ]
  node [
    id 2555
    label "naczynie"
  ]
  node [
    id 2556
    label "p&#322;at"
  ]
  node [
    id 2557
    label "maszyna_drukarska"
  ]
  node [
    id 2558
    label "linearno&#347;&#263;"
  ]
  node [
    id 2559
    label "wyra&#380;enie"
  ]
  node [
    id 2560
    label "spirala"
  ]
  node [
    id 2561
    label "dyspozycja"
  ]
  node [
    id 2562
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 2563
    label "October"
  ]
  node [
    id 2564
    label "creation"
  ]
  node [
    id 2565
    label "p&#281;tla"
  ]
  node [
    id 2566
    label "arystotelizm"
  ]
  node [
    id 2567
    label "szablon"
  ]
  node [
    id 2568
    label "zesp&#243;&#322;"
  ]
  node [
    id 2569
    label "podejrzany"
  ]
  node [
    id 2570
    label "s&#261;downictwo"
  ]
  node [
    id 2571
    label "biuro"
  ]
  node [
    id 2572
    label "court"
  ]
  node [
    id 2573
    label "forum"
  ]
  node [
    id 2574
    label "bronienie"
  ]
  node [
    id 2575
    label "oskar&#380;yciel"
  ]
  node [
    id 2576
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 2577
    label "skazany"
  ]
  node [
    id 2578
    label "post&#281;powanie"
  ]
  node [
    id 2579
    label "broni&#263;"
  ]
  node [
    id 2580
    label "my&#347;l"
  ]
  node [
    id 2581
    label "pods&#261;dny"
  ]
  node [
    id 2582
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 2583
    label "wypowied&#378;"
  ]
  node [
    id 2584
    label "instytucja"
  ]
  node [
    id 2585
    label "antylogizm"
  ]
  node [
    id 2586
    label "konektyw"
  ]
  node [
    id 2587
    label "&#347;wiadek"
  ]
  node [
    id 2588
    label "procesowicz"
  ]
  node [
    id 2589
    label "wyznaczenie"
  ]
  node [
    id 2590
    label "przyczynienie_si&#281;"
  ]
  node [
    id 2591
    label "zwr&#243;cenie"
  ]
  node [
    id 2592
    label "zrozumienie"
  ]
  node [
    id 2593
    label "tu&#322;&#243;w"
  ]
  node [
    id 2594
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 2595
    label "wielok&#261;t"
  ]
  node [
    id 2596
    label "odcinek"
  ]
  node [
    id 2597
    label "strzelba"
  ]
  node [
    id 2598
    label "lufa"
  ]
  node [
    id 2599
    label "&#347;ciana"
  ]
  node [
    id 2600
    label "orient"
  ]
  node [
    id 2601
    label "eastern_hemisphere"
  ]
  node [
    id 2602
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 2603
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 2604
    label "wyznaczy&#263;"
  ]
  node [
    id 2605
    label "wrench"
  ]
  node [
    id 2606
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 2607
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 2608
    label "sple&#347;&#263;"
  ]
  node [
    id 2609
    label "os&#322;abi&#263;"
  ]
  node [
    id 2610
    label "nawin&#261;&#263;"
  ]
  node [
    id 2611
    label "scali&#263;"
  ]
  node [
    id 2612
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 2613
    label "twist"
  ]
  node [
    id 2614
    label "splay"
  ]
  node [
    id 2615
    label "uszkodzi&#263;"
  ]
  node [
    id 2616
    label "break"
  ]
  node [
    id 2617
    label "flex"
  ]
  node [
    id 2618
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 2619
    label "splata&#263;"
  ]
  node [
    id 2620
    label "screw"
  ]
  node [
    id 2621
    label "scala&#263;"
  ]
  node [
    id 2622
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 2623
    label "przelezienie"
  ]
  node [
    id 2624
    label "&#347;piew"
  ]
  node [
    id 2625
    label "Synaj"
  ]
  node [
    id 2626
    label "wysoki"
  ]
  node [
    id 2627
    label "wzniesienie"
  ]
  node [
    id 2628
    label "pi&#281;tro"
  ]
  node [
    id 2629
    label "Ropa"
  ]
  node [
    id 2630
    label "kupa"
  ]
  node [
    id 2631
    label "przele&#378;&#263;"
  ]
  node [
    id 2632
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 2633
    label "karczek"
  ]
  node [
    id 2634
    label "rami&#261;czko"
  ]
  node [
    id 2635
    label "Jaworze"
  ]
  node [
    id 2636
    label "odchylanie_si&#281;"
  ]
  node [
    id 2637
    label "uprz&#281;dzenie"
  ]
  node [
    id 2638
    label "scalanie"
  ]
  node [
    id 2639
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 2640
    label "snucie"
  ]
  node [
    id 2641
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 2642
    label "tortuosity"
  ]
  node [
    id 2643
    label "odbijanie"
  ]
  node [
    id 2644
    label "contortion"
  ]
  node [
    id 2645
    label "splatanie"
  ]
  node [
    id 2646
    label "turn"
  ]
  node [
    id 2647
    label "nawini&#281;cie"
  ]
  node [
    id 2648
    label "os&#322;abienie"
  ]
  node [
    id 2649
    label "uszkodzenie"
  ]
  node [
    id 2650
    label "poskr&#281;canie"
  ]
  node [
    id 2651
    label "uraz"
  ]
  node [
    id 2652
    label "odchylenie_si&#281;"
  ]
  node [
    id 2653
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 2654
    label "splecenie"
  ]
  node [
    id 2655
    label "turning"
  ]
  node [
    id 2656
    label "pomaga&#263;"
  ]
  node [
    id 2657
    label "pomaganie"
  ]
  node [
    id 2658
    label "orientation"
  ]
  node [
    id 2659
    label "przyczynianie_si&#281;"
  ]
  node [
    id 2660
    label "rozeznawanie"
  ]
  node [
    id 2661
    label "oznaczanie"
  ]
  node [
    id 2662
    label "po&#322;o&#380;enie"
  ]
  node [
    id 2663
    label "seksualno&#347;&#263;"
  ]
  node [
    id 2664
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 2665
    label "zorientowanie_si&#281;"
  ]
  node [
    id 2666
    label "pogubienie_si&#281;"
  ]
  node [
    id 2667
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 2668
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 2669
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 2670
    label "gubienie_si&#281;"
  ]
  node [
    id 2671
    label "zaty&#322;"
  ]
  node [
    id 2672
    label "pupa"
  ]
  node [
    id 2673
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 2674
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 2675
    label "uwierzytelnienie"
  ]
  node [
    id 2676
    label "liczba"
  ]
  node [
    id 2677
    label "circumference"
  ]
  node [
    id 2678
    label "cyrkumferencja"
  ]
  node [
    id 2679
    label "provider"
  ]
  node [
    id 2680
    label "hipertekst"
  ]
  node [
    id 2681
    label "cyberprzestrze&#324;"
  ]
  node [
    id 2682
    label "mem"
  ]
  node [
    id 2683
    label "grooming"
  ]
  node [
    id 2684
    label "gra_sieciowa"
  ]
  node [
    id 2685
    label "media"
  ]
  node [
    id 2686
    label "biznes_elektroniczny"
  ]
  node [
    id 2687
    label "sie&#263;_komputerowa"
  ]
  node [
    id 2688
    label "punkt_dost&#281;pu"
  ]
  node [
    id 2689
    label "us&#322;uga_internetowa"
  ]
  node [
    id 2690
    label "netbook"
  ]
  node [
    id 2691
    label "e-hazard"
  ]
  node [
    id 2692
    label "podcast"
  ]
  node [
    id 2693
    label "co&#347;"
  ]
  node [
    id 2694
    label "faul"
  ]
  node [
    id 2695
    label "wk&#322;ad"
  ]
  node [
    id 2696
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 2697
    label "s&#281;dzia"
  ]
  node [
    id 2698
    label "bon"
  ]
  node [
    id 2699
    label "arkusz"
  ]
  node [
    id 2700
    label "kara"
  ]
  node [
    id 2701
    label "pagination"
  ]
  node [
    id 2702
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 2703
    label "elektroniczny"
  ]
  node [
    id 2704
    label "internetowo"
  ]
  node [
    id 2705
    label "nowoczesny"
  ]
  node [
    id 2706
    label "netowy"
  ]
  node [
    id 2707
    label "sieciowo"
  ]
  node [
    id 2708
    label "elektronicznie"
  ]
  node [
    id 2709
    label "siatkowy"
  ]
  node [
    id 2710
    label "sieciowy"
  ]
  node [
    id 2711
    label "nowy"
  ]
  node [
    id 2712
    label "nowo&#380;ytny"
  ]
  node [
    id 2713
    label "nowocze&#347;nie"
  ]
  node [
    id 2714
    label "elektrycznie"
  ]
  node [
    id 2715
    label "jaki&#347;"
  ]
  node [
    id 2716
    label "przyzwoity"
  ]
  node [
    id 2717
    label "jako&#347;"
  ]
  node [
    id 2718
    label "jako_tako"
  ]
  node [
    id 2719
    label "niez&#322;y"
  ]
  node [
    id 2720
    label "dziwny"
  ]
  node [
    id 2721
    label "edit"
  ]
  node [
    id 2722
    label "modyfikowa&#263;"
  ]
  node [
    id 2723
    label "ingerowa&#263;"
  ]
  node [
    id 2724
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 2725
    label "spakowanie"
  ]
  node [
    id 2726
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 2727
    label "pakowa&#263;"
  ]
  node [
    id 2728
    label "rekord"
  ]
  node [
    id 2729
    label "korelator"
  ]
  node [
    id 2730
    label "wyci&#261;ganie"
  ]
  node [
    id 2731
    label "pakowanie"
  ]
  node [
    id 2732
    label "sekwencjonowa&#263;"
  ]
  node [
    id 2733
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 2734
    label "jednostka_informacji"
  ]
  node [
    id 2735
    label "evidence"
  ]
  node [
    id 2736
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 2737
    label "rozpakowywanie"
  ]
  node [
    id 2738
    label "rozpakowanie"
  ]
  node [
    id 2739
    label "informacja"
  ]
  node [
    id 2740
    label "rozpakowywa&#263;"
  ]
  node [
    id 2741
    label "konwersja"
  ]
  node [
    id 2742
    label "nap&#322;ywanie"
  ]
  node [
    id 2743
    label "rozpakowa&#263;"
  ]
  node [
    id 2744
    label "spakowa&#263;"
  ]
  node [
    id 2745
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 2746
    label "edytowanie"
  ]
  node [
    id 2747
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 2748
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 2749
    label "sekwencjonowanie"
  ]
  node [
    id 2750
    label "okre&#347;lony"
  ]
  node [
    id 2751
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 2752
    label "wiadomy"
  ]
  node [
    id 2753
    label "series"
  ]
  node [
    id 2754
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 2755
    label "uprawianie"
  ]
  node [
    id 2756
    label "praca_rolnicza"
  ]
  node [
    id 2757
    label "pakiet_klimatyczny"
  ]
  node [
    id 2758
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 2759
    label "sum"
  ]
  node [
    id 2760
    label "album"
  ]
  node [
    id 2761
    label "&#347;rodek"
  ]
  node [
    id 2762
    label "niezb&#281;dnik"
  ]
  node [
    id 2763
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 2764
    label "tylec"
  ]
  node [
    id 2765
    label "ko&#322;o"
  ]
  node [
    id 2766
    label "modalno&#347;&#263;"
  ]
  node [
    id 2767
    label "z&#261;b"
  ]
  node [
    id 2768
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 2769
    label "prezenter"
  ]
  node [
    id 2770
    label "mildew"
  ]
  node [
    id 2771
    label "zi&#243;&#322;ko"
  ]
  node [
    id 2772
    label "motif"
  ]
  node [
    id 2773
    label "pozowanie"
  ]
  node [
    id 2774
    label "ideal"
  ]
  node [
    id 2775
    label "matryca"
  ]
  node [
    id 2776
    label "adaptation"
  ]
  node [
    id 2777
    label "pozowa&#263;"
  ]
  node [
    id 2778
    label "imitacja"
  ]
  node [
    id 2779
    label "orygina&#322;"
  ]
  node [
    id 2780
    label "facet"
  ]
  node [
    id 2781
    label "sp&#243;lnie"
  ]
  node [
    id 2782
    label "wsp&#243;lny"
  ]
  node [
    id 2783
    label "spolny"
  ]
  node [
    id 2784
    label "sp&#243;lny"
  ]
  node [
    id 2785
    label "jeden"
  ]
  node [
    id 2786
    label "uwsp&#243;lnienie"
  ]
  node [
    id 2787
    label "uwsp&#243;lnianie"
  ]
  node [
    id 2788
    label "podnosi&#263;"
  ]
  node [
    id 2789
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 2790
    label "rise"
  ]
  node [
    id 2791
    label "admit"
  ]
  node [
    id 2792
    label "reagowa&#263;"
  ]
  node [
    id 2793
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 2794
    label "zaczyna&#263;"
  ]
  node [
    id 2795
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 2796
    label "escalate"
  ]
  node [
    id 2797
    label "pia&#263;"
  ]
  node [
    id 2798
    label "przybli&#380;a&#263;"
  ]
  node [
    id 2799
    label "ulepsza&#263;"
  ]
  node [
    id 2800
    label "tire"
  ]
  node [
    id 2801
    label "liczy&#263;"
  ]
  node [
    id 2802
    label "przemieszcza&#263;"
  ]
  node [
    id 2803
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 2804
    label "chwali&#263;"
  ]
  node [
    id 2805
    label "os&#322;awia&#263;"
  ]
  node [
    id 2806
    label "odbudowywa&#263;"
  ]
  node [
    id 2807
    label "enhance"
  ]
  node [
    id 2808
    label "za&#322;apywa&#263;"
  ]
  node [
    id 2809
    label "lift"
  ]
  node [
    id 2810
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 2811
    label "answer"
  ]
  node [
    id 2812
    label "odpowiada&#263;"
  ]
  node [
    id 2813
    label "traci&#263;"
  ]
  node [
    id 2814
    label "alternate"
  ]
  node [
    id 2815
    label "change"
  ]
  node [
    id 2816
    label "reengineering"
  ]
  node [
    id 2817
    label "zast&#281;powa&#263;"
  ]
  node [
    id 2818
    label "sprawia&#263;"
  ]
  node [
    id 2819
    label "zyskiwa&#263;"
  ]
  node [
    id 2820
    label "przechodzi&#263;"
  ]
  node [
    id 2821
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 2822
    label "resolution"
  ]
  node [
    id 2823
    label "zapis"
  ]
  node [
    id 2824
    label "&#347;wiadectwo"
  ]
  node [
    id 2825
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 2826
    label "parafa"
  ]
  node [
    id 2827
    label "raport&#243;wka"
  ]
  node [
    id 2828
    label "record"
  ]
  node [
    id 2829
    label "registratura"
  ]
  node [
    id 2830
    label "dokumentacja"
  ]
  node [
    id 2831
    label "fascyku&#322;"
  ]
  node [
    id 2832
    label "artyku&#322;"
  ]
  node [
    id 2833
    label "writing"
  ]
  node [
    id 2834
    label "sygnatariusz"
  ]
  node [
    id 2835
    label "rezultat"
  ]
  node [
    id 2836
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 2837
    label "pewnie"
  ]
  node [
    id 2838
    label "zauwa&#380;alnie"
  ]
  node [
    id 2839
    label "oddzia&#322;anie"
  ]
  node [
    id 2840
    label "podj&#281;cie"
  ]
  node [
    id 2841
    label "resoluteness"
  ]
  node [
    id 2842
    label "judgment"
  ]
  node [
    id 2843
    label "nast&#281;pnie"
  ]
  node [
    id 2844
    label "nastopny"
  ]
  node [
    id 2845
    label "kolejno"
  ]
  node [
    id 2846
    label "kt&#243;ry&#347;"
  ]
  node [
    id 2847
    label "osobno"
  ]
  node [
    id 2848
    label "r&#243;&#380;ny"
  ]
  node [
    id 2849
    label "inszy"
  ]
  node [
    id 2850
    label "inaczej"
  ]
  node [
    id 2851
    label "wyraz_pochodny"
  ]
  node [
    id 2852
    label "fraza"
  ]
  node [
    id 2853
    label "topik"
  ]
  node [
    id 2854
    label "otoczka"
  ]
  node [
    id 2855
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 2856
    label "wypowiedzenie"
  ]
  node [
    id 2857
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 2858
    label "zdanie"
  ]
  node [
    id 2859
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 2860
    label "motyw"
  ]
  node [
    id 2861
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 2862
    label "zanucenie"
  ]
  node [
    id 2863
    label "nuta"
  ]
  node [
    id 2864
    label "zakosztowa&#263;"
  ]
  node [
    id 2865
    label "zajawka"
  ]
  node [
    id 2866
    label "zanuci&#263;"
  ]
  node [
    id 2867
    label "emocja"
  ]
  node [
    id 2868
    label "oskoma"
  ]
  node [
    id 2869
    label "melika"
  ]
  node [
    id 2870
    label "nucenie"
  ]
  node [
    id 2871
    label "nuci&#263;"
  ]
  node [
    id 2872
    label "brzmienie"
  ]
  node [
    id 2873
    label "taste"
  ]
  node [
    id 2874
    label "muzyka"
  ]
  node [
    id 2875
    label "inclination"
  ]
  node [
    id 2876
    label "m&#322;ot"
  ]
  node [
    id 2877
    label "znak"
  ]
  node [
    id 2878
    label "drzewo"
  ]
  node [
    id 2879
    label "pr&#243;ba"
  ]
  node [
    id 2880
    label "attribute"
  ]
  node [
    id 2881
    label "marka"
  ]
  node [
    id 2882
    label "mentalno&#347;&#263;"
  ]
  node [
    id 2883
    label "superego"
  ]
  node [
    id 2884
    label "psychika"
  ]
  node [
    id 2885
    label "wn&#281;trze"
  ]
  node [
    id 2886
    label "matter"
  ]
  node [
    id 2887
    label "splot"
  ]
  node [
    id 2888
    label "ceg&#322;a"
  ]
  node [
    id 2889
    label "socket"
  ]
  node [
    id 2890
    label "rozmieszczenie"
  ]
  node [
    id 2891
    label "fabu&#322;a"
  ]
  node [
    id 2892
    label "okrywa"
  ]
  node [
    id 2893
    label "object"
  ]
  node [
    id 2894
    label "wpadni&#281;cie"
  ]
  node [
    id 2895
    label "mienie"
  ]
  node [
    id 2896
    label "wpa&#347;&#263;"
  ]
  node [
    id 2897
    label "wpadanie"
  ]
  node [
    id 2898
    label "wpada&#263;"
  ]
  node [
    id 2899
    label "rozpatrywanie"
  ]
  node [
    id 2900
    label "dyskutowanie"
  ]
  node [
    id 2901
    label "omowny"
  ]
  node [
    id 2902
    label "figura_stylistyczna"
  ]
  node [
    id 2903
    label "sformu&#322;owanie"
  ]
  node [
    id 2904
    label "odchodzenie"
  ]
  node [
    id 2905
    label "aberrance"
  ]
  node [
    id 2906
    label "swerve"
  ]
  node [
    id 2907
    label "distract"
  ]
  node [
    id 2908
    label "odej&#347;&#263;"
  ]
  node [
    id 2909
    label "zmieni&#263;"
  ]
  node [
    id 2910
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 2911
    label "przedyskutowa&#263;"
  ]
  node [
    id 2912
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 2913
    label "publicize"
  ]
  node [
    id 2914
    label "digress"
  ]
  node [
    id 2915
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 2916
    label "odchodzi&#263;"
  ]
  node [
    id 2917
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 2918
    label "perversion"
  ]
  node [
    id 2919
    label "odej&#347;cie"
  ]
  node [
    id 2920
    label "k&#261;t"
  ]
  node [
    id 2921
    label "deviation"
  ]
  node [
    id 2922
    label "patologia"
  ]
  node [
    id 2923
    label "dyskutowa&#263;"
  ]
  node [
    id 2924
    label "discourse"
  ]
  node [
    id 2925
    label "kognicja"
  ]
  node [
    id 2926
    label "rozprawa"
  ]
  node [
    id 2927
    label "szczeg&#243;&#322;"
  ]
  node [
    id 2928
    label "przes&#322;anka"
  ]
  node [
    id 2929
    label "paj&#261;k"
  ]
  node [
    id 2930
    label "przewodnik"
  ]
  node [
    id 2931
    label "topikowate"
  ]
  node [
    id 2932
    label "grupa_dyskusyjna"
  ]
  node [
    id 2933
    label "bazylika"
  ]
  node [
    id 2934
    label "portal"
  ]
  node [
    id 2935
    label "konferencja"
  ]
  node [
    id 2936
    label "agora"
  ]
  node [
    id 2937
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 2938
    label "wielki"
  ]
  node [
    id 2939
    label "rozpowszechnianie"
  ]
  node [
    id 2940
    label "wyj&#261;tkowy"
  ]
  node [
    id 2941
    label "nieprzeci&#281;tny"
  ]
  node [
    id 2942
    label "wysoce"
  ]
  node [
    id 2943
    label "wybitny"
  ]
  node [
    id 2944
    label "dupny"
  ]
  node [
    id 2945
    label "dochodzenie"
  ]
  node [
    id 2946
    label "deployment"
  ]
  node [
    id 2947
    label "nuklearyzacja"
  ]
  node [
    id 2948
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 2949
    label "ujawnienie_si&#281;"
  ]
  node [
    id 2950
    label "powstanie"
  ]
  node [
    id 2951
    label "wydostanie_si&#281;"
  ]
  node [
    id 2952
    label "opuszczenie"
  ]
  node [
    id 2953
    label "ukazanie_si&#281;"
  ]
  node [
    id 2954
    label "emergence"
  ]
  node [
    id 2955
    label "wyr&#243;&#380;nienie_si&#281;"
  ]
  node [
    id 2956
    label "zgini&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 1073
  ]
  edge [
    source 12
    target 1074
  ]
  edge [
    source 12
    target 1075
  ]
  edge [
    source 12
    target 1076
  ]
  edge [
    source 12
    target 1077
  ]
  edge [
    source 12
    target 1078
  ]
  edge [
    source 12
    target 1079
  ]
  edge [
    source 12
    target 1080
  ]
  edge [
    source 12
    target 1081
  ]
  edge [
    source 12
    target 1082
  ]
  edge [
    source 12
    target 1083
  ]
  edge [
    source 12
    target 1084
  ]
  edge [
    source 12
    target 1085
  ]
  edge [
    source 12
    target 1086
  ]
  edge [
    source 12
    target 1087
  ]
  edge [
    source 12
    target 1088
  ]
  edge [
    source 12
    target 1089
  ]
  edge [
    source 12
    target 1090
  ]
  edge [
    source 12
    target 1091
  ]
  edge [
    source 12
    target 1092
  ]
  edge [
    source 12
    target 1093
  ]
  edge [
    source 12
    target 1094
  ]
  edge [
    source 12
    target 1095
  ]
  edge [
    source 12
    target 1096
  ]
  edge [
    source 12
    target 1097
  ]
  edge [
    source 12
    target 1098
  ]
  edge [
    source 12
    target 1099
  ]
  edge [
    source 12
    target 1100
  ]
  edge [
    source 12
    target 1101
  ]
  edge [
    source 12
    target 1102
  ]
  edge [
    source 12
    target 1103
  ]
  edge [
    source 12
    target 1104
  ]
  edge [
    source 12
    target 1105
  ]
  edge [
    source 12
    target 1106
  ]
  edge [
    source 12
    target 1107
  ]
  edge [
    source 12
    target 1108
  ]
  edge [
    source 12
    target 1109
  ]
  edge [
    source 12
    target 1110
  ]
  edge [
    source 12
    target 1111
  ]
  edge [
    source 12
    target 1112
  ]
  edge [
    source 12
    target 1113
  ]
  edge [
    source 12
    target 1114
  ]
  edge [
    source 12
    target 1115
  ]
  edge [
    source 12
    target 1116
  ]
  edge [
    source 12
    target 1117
  ]
  edge [
    source 12
    target 1118
  ]
  edge [
    source 12
    target 1119
  ]
  edge [
    source 12
    target 1120
  ]
  edge [
    source 12
    target 1121
  ]
  edge [
    source 12
    target 1122
  ]
  edge [
    source 12
    target 1123
  ]
  edge [
    source 12
    target 1124
  ]
  edge [
    source 12
    target 1125
  ]
  edge [
    source 12
    target 1126
  ]
  edge [
    source 12
    target 1127
  ]
  edge [
    source 12
    target 1128
  ]
  edge [
    source 12
    target 1129
  ]
  edge [
    source 12
    target 1130
  ]
  edge [
    source 12
    target 1131
  ]
  edge [
    source 12
    target 1132
  ]
  edge [
    source 12
    target 1133
  ]
  edge [
    source 12
    target 1134
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 1136
  ]
  edge [
    source 12
    target 1137
  ]
  edge [
    source 12
    target 1138
  ]
  edge [
    source 12
    target 1139
  ]
  edge [
    source 12
    target 1140
  ]
  edge [
    source 12
    target 1141
  ]
  edge [
    source 12
    target 1142
  ]
  edge [
    source 12
    target 1143
  ]
  edge [
    source 12
    target 1144
  ]
  edge [
    source 12
    target 1145
  ]
  edge [
    source 12
    target 1146
  ]
  edge [
    source 12
    target 1147
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1151
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 12
    target 1153
  ]
  edge [
    source 12
    target 1154
  ]
  edge [
    source 12
    target 1155
  ]
  edge [
    source 12
    target 1156
  ]
  edge [
    source 12
    target 1157
  ]
  edge [
    source 12
    target 1158
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1160
  ]
  edge [
    source 12
    target 1161
  ]
  edge [
    source 12
    target 1162
  ]
  edge [
    source 12
    target 1163
  ]
  edge [
    source 12
    target 1164
  ]
  edge [
    source 12
    target 1165
  ]
  edge [
    source 12
    target 1166
  ]
  edge [
    source 12
    target 1167
  ]
  edge [
    source 12
    target 1168
  ]
  edge [
    source 12
    target 1169
  ]
  edge [
    source 12
    target 1170
  ]
  edge [
    source 12
    target 1171
  ]
  edge [
    source 12
    target 1172
  ]
  edge [
    source 12
    target 1173
  ]
  edge [
    source 12
    target 1174
  ]
  edge [
    source 12
    target 1175
  ]
  edge [
    source 12
    target 1176
  ]
  edge [
    source 12
    target 1177
  ]
  edge [
    source 12
    target 1178
  ]
  edge [
    source 12
    target 1179
  ]
  edge [
    source 12
    target 1180
  ]
  edge [
    source 12
    target 1181
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 1183
  ]
  edge [
    source 12
    target 1184
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 1186
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1227
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 1228
  ]
  edge [
    source 13
    target 1229
  ]
  edge [
    source 13
    target 1230
  ]
  edge [
    source 13
    target 1231
  ]
  edge [
    source 13
    target 1232
  ]
  edge [
    source 13
    target 1233
  ]
  edge [
    source 13
    target 1234
  ]
  edge [
    source 13
    target 1235
  ]
  edge [
    source 13
    target 1236
  ]
  edge [
    source 13
    target 1237
  ]
  edge [
    source 13
    target 1238
  ]
  edge [
    source 13
    target 1239
  ]
  edge [
    source 13
    target 1240
  ]
  edge [
    source 13
    target 1241
  ]
  edge [
    source 13
    target 1242
  ]
  edge [
    source 13
    target 1243
  ]
  edge [
    source 13
    target 1244
  ]
  edge [
    source 13
    target 1245
  ]
  edge [
    source 13
    target 1246
  ]
  edge [
    source 13
    target 1247
  ]
  edge [
    source 13
    target 1248
  ]
  edge [
    source 13
    target 1249
  ]
  edge [
    source 13
    target 1250
  ]
  edge [
    source 13
    target 1251
  ]
  edge [
    source 13
    target 1252
  ]
  edge [
    source 13
    target 1253
  ]
  edge [
    source 13
    target 1254
  ]
  edge [
    source 13
    target 1255
  ]
  edge [
    source 13
    target 1256
  ]
  edge [
    source 13
    target 1257
  ]
  edge [
    source 13
    target 1258
  ]
  edge [
    source 13
    target 1259
  ]
  edge [
    source 13
    target 1260
  ]
  edge [
    source 13
    target 1261
  ]
  edge [
    source 13
    target 1262
  ]
  edge [
    source 13
    target 1263
  ]
  edge [
    source 13
    target 1264
  ]
  edge [
    source 13
    target 1265
  ]
  edge [
    source 13
    target 1266
  ]
  edge [
    source 13
    target 1267
  ]
  edge [
    source 13
    target 1268
  ]
  edge [
    source 13
    target 1269
  ]
  edge [
    source 13
    target 1270
  ]
  edge [
    source 13
    target 1271
  ]
  edge [
    source 13
    target 1272
  ]
  edge [
    source 13
    target 1273
  ]
  edge [
    source 13
    target 1274
  ]
  edge [
    source 13
    target 1275
  ]
  edge [
    source 13
    target 1276
  ]
  edge [
    source 13
    target 1277
  ]
  edge [
    source 13
    target 1278
  ]
  edge [
    source 13
    target 1279
  ]
  edge [
    source 13
    target 1280
  ]
  edge [
    source 13
    target 1281
  ]
  edge [
    source 13
    target 1282
  ]
  edge [
    source 13
    target 1283
  ]
  edge [
    source 13
    target 1284
  ]
  edge [
    source 13
    target 1285
  ]
  edge [
    source 13
    target 1286
  ]
  edge [
    source 13
    target 1287
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 1288
  ]
  edge [
    source 13
    target 1289
  ]
  edge [
    source 13
    target 1290
  ]
  edge [
    source 13
    target 1291
  ]
  edge [
    source 13
    target 1292
  ]
  edge [
    source 13
    target 1293
  ]
  edge [
    source 13
    target 1294
  ]
  edge [
    source 13
    target 1295
  ]
  edge [
    source 13
    target 1296
  ]
  edge [
    source 13
    target 1297
  ]
  edge [
    source 13
    target 1298
  ]
  edge [
    source 13
    target 1299
  ]
  edge [
    source 13
    target 1300
  ]
  edge [
    source 13
    target 1301
  ]
  edge [
    source 13
    target 1302
  ]
  edge [
    source 13
    target 1303
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 1304
  ]
  edge [
    source 13
    target 1305
  ]
  edge [
    source 13
    target 1306
  ]
  edge [
    source 13
    target 1307
  ]
  edge [
    source 13
    target 1308
  ]
  edge [
    source 13
    target 1309
  ]
  edge [
    source 13
    target 1310
  ]
  edge [
    source 13
    target 1311
  ]
  edge [
    source 13
    target 1312
  ]
  edge [
    source 13
    target 1313
  ]
  edge [
    source 13
    target 1314
  ]
  edge [
    source 13
    target 1315
  ]
  edge [
    source 13
    target 1316
  ]
  edge [
    source 13
    target 1317
  ]
  edge [
    source 13
    target 1318
  ]
  edge [
    source 13
    target 1319
  ]
  edge [
    source 13
    target 1320
  ]
  edge [
    source 13
    target 1321
  ]
  edge [
    source 13
    target 1322
  ]
  edge [
    source 13
    target 1323
  ]
  edge [
    source 13
    target 1324
  ]
  edge [
    source 13
    target 1325
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 1326
  ]
  edge [
    source 13
    target 1327
  ]
  edge [
    source 13
    target 1328
  ]
  edge [
    source 13
    target 1329
  ]
  edge [
    source 13
    target 1330
  ]
  edge [
    source 13
    target 1331
  ]
  edge [
    source 13
    target 1332
  ]
  edge [
    source 13
    target 1333
  ]
  edge [
    source 13
    target 1334
  ]
  edge [
    source 13
    target 1335
  ]
  edge [
    source 13
    target 1336
  ]
  edge [
    source 13
    target 1337
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 13
    target 1338
  ]
  edge [
    source 13
    target 1339
  ]
  edge [
    source 13
    target 1340
  ]
  edge [
    source 13
    target 1341
  ]
  edge [
    source 13
    target 1342
  ]
  edge [
    source 13
    target 1343
  ]
  edge [
    source 13
    target 1344
  ]
  edge [
    source 13
    target 1345
  ]
  edge [
    source 13
    target 1346
  ]
  edge [
    source 13
    target 1347
  ]
  edge [
    source 13
    target 1348
  ]
  edge [
    source 13
    target 1349
  ]
  edge [
    source 13
    target 1350
  ]
  edge [
    source 13
    target 1351
  ]
  edge [
    source 13
    target 1352
  ]
  edge [
    source 13
    target 1353
  ]
  edge [
    source 13
    target 1354
  ]
  edge [
    source 13
    target 1355
  ]
  edge [
    source 13
    target 1356
  ]
  edge [
    source 13
    target 1357
  ]
  edge [
    source 13
    target 1358
  ]
  edge [
    source 13
    target 1359
  ]
  edge [
    source 13
    target 1360
  ]
  edge [
    source 13
    target 1361
  ]
  edge [
    source 13
    target 1362
  ]
  edge [
    source 13
    target 1363
  ]
  edge [
    source 13
    target 1364
  ]
  edge [
    source 13
    target 1365
  ]
  edge [
    source 13
    target 1366
  ]
  edge [
    source 13
    target 1367
  ]
  edge [
    source 13
    target 37
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1368
  ]
  edge [
    source 14
    target 1369
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 14
    target 310
  ]
  edge [
    source 14
    target 311
  ]
  edge [
    source 14
    target 312
  ]
  edge [
    source 14
    target 313
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1370
  ]
  edge [
    source 15
    target 1371
  ]
  edge [
    source 15
    target 1372
  ]
  edge [
    source 15
    target 1373
  ]
  edge [
    source 15
    target 1374
  ]
  edge [
    source 15
    target 1375
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1376
  ]
  edge [
    source 16
    target 1377
  ]
  edge [
    source 16
    target 1378
  ]
  edge [
    source 16
    target 1379
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 1380
  ]
  edge [
    source 16
    target 1381
  ]
  edge [
    source 16
    target 1382
  ]
  edge [
    source 16
    target 1383
  ]
  edge [
    source 16
    target 1384
  ]
  edge [
    source 16
    target 1385
  ]
  edge [
    source 16
    target 1386
  ]
  edge [
    source 16
    target 1387
  ]
  edge [
    source 16
    target 1388
  ]
  edge [
    source 16
    target 1389
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 494
  ]
  edge [
    source 16
    target 312
  ]
  edge [
    source 16
    target 1390
  ]
  edge [
    source 16
    target 1391
  ]
  edge [
    source 16
    target 1392
  ]
  edge [
    source 16
    target 1393
  ]
  edge [
    source 16
    target 1394
  ]
  edge [
    source 16
    target 1395
  ]
  edge [
    source 16
    target 1396
  ]
  edge [
    source 16
    target 1397
  ]
  edge [
    source 16
    target 1398
  ]
  edge [
    source 16
    target 1399
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1401
  ]
  edge [
    source 17
    target 1315
  ]
  edge [
    source 17
    target 1402
  ]
  edge [
    source 17
    target 513
  ]
  edge [
    source 17
    target 1403
  ]
  edge [
    source 17
    target 1404
  ]
  edge [
    source 17
    target 1405
  ]
  edge [
    source 17
    target 1406
  ]
  edge [
    source 17
    target 1407
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 1408
  ]
  edge [
    source 17
    target 603
  ]
  edge [
    source 17
    target 604
  ]
  edge [
    source 17
    target 605
  ]
  edge [
    source 17
    target 315
  ]
  edge [
    source 17
    target 425
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 610
  ]
  edge [
    source 17
    target 391
  ]
  edge [
    source 17
    target 611
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 17
    target 1409
  ]
  edge [
    source 17
    target 1410
  ]
  edge [
    source 17
    target 300
  ]
  edge [
    source 17
    target 1411
  ]
  edge [
    source 17
    target 1412
  ]
  edge [
    source 17
    target 1413
  ]
  edge [
    source 17
    target 1414
  ]
  edge [
    source 17
    target 1415
  ]
  edge [
    source 17
    target 1416
  ]
  edge [
    source 17
    target 1417
  ]
  edge [
    source 17
    target 1418
  ]
  edge [
    source 17
    target 1419
  ]
  edge [
    source 17
    target 1420
  ]
  edge [
    source 17
    target 1421
  ]
  edge [
    source 17
    target 1422
  ]
  edge [
    source 17
    target 1423
  ]
  edge [
    source 17
    target 1424
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 1425
  ]
  edge [
    source 17
    target 1426
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 494
  ]
  edge [
    source 17
    target 1427
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 18
    target 1428
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 1429
  ]
  edge [
    source 18
    target 1430
  ]
  edge [
    source 18
    target 1431
  ]
  edge [
    source 18
    target 1432
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 1433
  ]
  edge [
    source 18
    target 1434
  ]
  edge [
    source 18
    target 1435
  ]
  edge [
    source 18
    target 1436
  ]
  edge [
    source 18
    target 1437
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 250
  ]
  edge [
    source 18
    target 1438
  ]
  edge [
    source 18
    target 1439
  ]
  edge [
    source 18
    target 1440
  ]
  edge [
    source 18
    target 1441
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 1442
  ]
  edge [
    source 18
    target 1443
  ]
  edge [
    source 18
    target 1444
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 1445
  ]
  edge [
    source 18
    target 1446
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 319
  ]
  edge [
    source 18
    target 1447
  ]
  edge [
    source 18
    target 1448
  ]
  edge [
    source 18
    target 1449
  ]
  edge [
    source 18
    target 1450
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 1451
  ]
  edge [
    source 18
    target 1452
  ]
  edge [
    source 18
    target 1453
  ]
  edge [
    source 18
    target 1454
  ]
  edge [
    source 18
    target 1455
  ]
  edge [
    source 18
    target 1456
  ]
  edge [
    source 18
    target 1457
  ]
  edge [
    source 18
    target 1458
  ]
  edge [
    source 18
    target 1459
  ]
  edge [
    source 18
    target 1460
  ]
  edge [
    source 18
    target 1461
  ]
  edge [
    source 18
    target 1462
  ]
  edge [
    source 18
    target 1463
  ]
  edge [
    source 18
    target 1464
  ]
  edge [
    source 18
    target 1465
  ]
  edge [
    source 18
    target 1466
  ]
  edge [
    source 18
    target 1467
  ]
  edge [
    source 18
    target 1468
  ]
  edge [
    source 18
    target 1469
  ]
  edge [
    source 18
    target 1470
  ]
  edge [
    source 18
    target 1471
  ]
  edge [
    source 18
    target 426
  ]
  edge [
    source 18
    target 1472
  ]
  edge [
    source 18
    target 1473
  ]
  edge [
    source 18
    target 1474
  ]
  edge [
    source 18
    target 1475
  ]
  edge [
    source 18
    target 1476
  ]
  edge [
    source 18
    target 1477
  ]
  edge [
    source 18
    target 1478
  ]
  edge [
    source 18
    target 1479
  ]
  edge [
    source 18
    target 1480
  ]
  edge [
    source 18
    target 1481
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 1482
  ]
  edge [
    source 18
    target 1483
  ]
  edge [
    source 18
    target 1484
  ]
  edge [
    source 18
    target 1485
  ]
  edge [
    source 18
    target 1486
  ]
  edge [
    source 18
    target 1487
  ]
  edge [
    source 18
    target 1488
  ]
  edge [
    source 18
    target 1489
  ]
  edge [
    source 18
    target 1490
  ]
  edge [
    source 18
    target 1491
  ]
  edge [
    source 18
    target 1492
  ]
  edge [
    source 18
    target 391
  ]
  edge [
    source 18
    target 1493
  ]
  edge [
    source 18
    target 1494
  ]
  edge [
    source 18
    target 1495
  ]
  edge [
    source 18
    target 1496
  ]
  edge [
    source 18
    target 1497
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 1498
  ]
  edge [
    source 18
    target 1499
  ]
  edge [
    source 18
    target 1500
  ]
  edge [
    source 18
    target 1501
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 1502
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 1503
  ]
  edge [
    source 18
    target 1504
  ]
  edge [
    source 18
    target 1505
  ]
  edge [
    source 18
    target 1506
  ]
  edge [
    source 18
    target 1507
  ]
  edge [
    source 18
    target 1508
  ]
  edge [
    source 18
    target 1509
  ]
  edge [
    source 18
    target 1510
  ]
  edge [
    source 18
    target 1511
  ]
  edge [
    source 18
    target 1512
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1513
  ]
  edge [
    source 19
    target 1514
  ]
  edge [
    source 19
    target 1515
  ]
  edge [
    source 19
    target 1516
  ]
  edge [
    source 19
    target 1517
  ]
  edge [
    source 19
    target 1518
  ]
  edge [
    source 19
    target 1519
  ]
  edge [
    source 19
    target 1520
  ]
  edge [
    source 19
    target 1521
  ]
  edge [
    source 19
    target 1522
  ]
  edge [
    source 19
    target 1523
  ]
  edge [
    source 19
    target 1524
  ]
  edge [
    source 19
    target 1525
  ]
  edge [
    source 19
    target 1526
  ]
  edge [
    source 19
    target 1527
  ]
  edge [
    source 19
    target 1528
  ]
  edge [
    source 19
    target 1529
  ]
  edge [
    source 19
    target 1530
  ]
  edge [
    source 19
    target 1531
  ]
  edge [
    source 19
    target 1532
  ]
  edge [
    source 19
    target 1533
  ]
  edge [
    source 19
    target 1534
  ]
  edge [
    source 19
    target 1535
  ]
  edge [
    source 19
    target 425
  ]
  edge [
    source 19
    target 433
  ]
  edge [
    source 19
    target 1536
  ]
  edge [
    source 19
    target 1537
  ]
  edge [
    source 19
    target 1538
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 1539
  ]
  edge [
    source 19
    target 1540
  ]
  edge [
    source 19
    target 1541
  ]
  edge [
    source 19
    target 1542
  ]
  edge [
    source 19
    target 424
  ]
  edge [
    source 19
    target 1543
  ]
  edge [
    source 19
    target 1544
  ]
  edge [
    source 19
    target 1545
  ]
  edge [
    source 19
    target 1546
  ]
  edge [
    source 19
    target 1547
  ]
  edge [
    source 19
    target 1548
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 1549
  ]
  edge [
    source 19
    target 1550
  ]
  edge [
    source 19
    target 1551
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1552
  ]
  edge [
    source 19
    target 1553
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1554
  ]
  edge [
    source 19
    target 1555
  ]
  edge [
    source 19
    target 1556
  ]
  edge [
    source 19
    target 1557
  ]
  edge [
    source 19
    target 1558
  ]
  edge [
    source 19
    target 1559
  ]
  edge [
    source 19
    target 1560
  ]
  edge [
    source 19
    target 1561
  ]
  edge [
    source 19
    target 1562
  ]
  edge [
    source 19
    target 1563
  ]
  edge [
    source 19
    target 1564
  ]
  edge [
    source 19
    target 1565
  ]
  edge [
    source 19
    target 1566
  ]
  edge [
    source 19
    target 1567
  ]
  edge [
    source 19
    target 1568
  ]
  edge [
    source 19
    target 1569
  ]
  edge [
    source 19
    target 1570
  ]
  edge [
    source 19
    target 1571
  ]
  edge [
    source 19
    target 1572
  ]
  edge [
    source 19
    target 1573
  ]
  edge [
    source 19
    target 1574
  ]
  edge [
    source 19
    target 1575
  ]
  edge [
    source 19
    target 1576
  ]
  edge [
    source 19
    target 1577
  ]
  edge [
    source 19
    target 1578
  ]
  edge [
    source 19
    target 1579
  ]
  edge [
    source 19
    target 1580
  ]
  edge [
    source 19
    target 1581
  ]
  edge [
    source 19
    target 1582
  ]
  edge [
    source 19
    target 1583
  ]
  edge [
    source 19
    target 1584
  ]
  edge [
    source 19
    target 1585
  ]
  edge [
    source 19
    target 1586
  ]
  edge [
    source 19
    target 1587
  ]
  edge [
    source 19
    target 1588
  ]
  edge [
    source 19
    target 1589
  ]
  edge [
    source 19
    target 1590
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 1591
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 1592
  ]
  edge [
    source 19
    target 1593
  ]
  edge [
    source 19
    target 1594
  ]
  edge [
    source 19
    target 1595
  ]
  edge [
    source 19
    target 1596
  ]
  edge [
    source 19
    target 1597
  ]
  edge [
    source 19
    target 1598
  ]
  edge [
    source 19
    target 1599
  ]
  edge [
    source 19
    target 1600
  ]
  edge [
    source 19
    target 1601
  ]
  edge [
    source 19
    target 1602
  ]
  edge [
    source 19
    target 256
  ]
  edge [
    source 19
    target 1603
  ]
  edge [
    source 19
    target 1604
  ]
  edge [
    source 19
    target 1605
  ]
  edge [
    source 19
    target 1606
  ]
  edge [
    source 19
    target 1607
  ]
  edge [
    source 19
    target 1608
  ]
  edge [
    source 19
    target 1609
  ]
  edge [
    source 19
    target 1610
  ]
  edge [
    source 19
    target 1611
  ]
  edge [
    source 19
    target 1612
  ]
  edge [
    source 19
    target 555
  ]
  edge [
    source 19
    target 1613
  ]
  edge [
    source 19
    target 1614
  ]
  edge [
    source 19
    target 1615
  ]
  edge [
    source 19
    target 1616
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 1617
  ]
  edge [
    source 19
    target 1618
  ]
  edge [
    source 19
    target 1619
  ]
  edge [
    source 19
    target 1620
  ]
  edge [
    source 19
    target 1621
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 1622
  ]
  edge [
    source 19
    target 1623
  ]
  edge [
    source 19
    target 1624
  ]
  edge [
    source 19
    target 1625
  ]
  edge [
    source 19
    target 1626
  ]
  edge [
    source 19
    target 1627
  ]
  edge [
    source 19
    target 1628
  ]
  edge [
    source 19
    target 1629
  ]
  edge [
    source 19
    target 1630
  ]
  edge [
    source 19
    target 1631
  ]
  edge [
    source 19
    target 1632
  ]
  edge [
    source 19
    target 1633
  ]
  edge [
    source 19
    target 1634
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1635
  ]
  edge [
    source 20
    target 1636
  ]
  edge [
    source 20
    target 1637
  ]
  edge [
    source 20
    target 1638
  ]
  edge [
    source 20
    target 1639
  ]
  edge [
    source 20
    target 1640
  ]
  edge [
    source 20
    target 1641
  ]
  edge [
    source 20
    target 1642
  ]
  edge [
    source 20
    target 1643
  ]
  edge [
    source 20
    target 1644
  ]
  edge [
    source 20
    target 1645
  ]
  edge [
    source 20
    target 1646
  ]
  edge [
    source 20
    target 455
  ]
  edge [
    source 20
    target 1647
  ]
  edge [
    source 20
    target 1648
  ]
  edge [
    source 20
    target 1649
  ]
  edge [
    source 20
    target 1650
  ]
  edge [
    source 20
    target 1651
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 1652
  ]
  edge [
    source 20
    target 1653
  ]
  edge [
    source 20
    target 1654
  ]
  edge [
    source 20
    target 1655
  ]
  edge [
    source 20
    target 1656
  ]
  edge [
    source 20
    target 1657
  ]
  edge [
    source 20
    target 1658
  ]
  edge [
    source 20
    target 1659
  ]
  edge [
    source 20
    target 1660
  ]
  edge [
    source 20
    target 1661
  ]
  edge [
    source 20
    target 1662
  ]
  edge [
    source 20
    target 1663
  ]
  edge [
    source 20
    target 1664
  ]
  edge [
    source 20
    target 1665
  ]
  edge [
    source 20
    target 626
  ]
  edge [
    source 20
    target 1666
  ]
  edge [
    source 20
    target 1667
  ]
  edge [
    source 20
    target 1668
  ]
  edge [
    source 20
    target 1669
  ]
  edge [
    source 20
    target 1670
  ]
  edge [
    source 20
    target 1671
  ]
  edge [
    source 20
    target 1672
  ]
  edge [
    source 20
    target 1673
  ]
  edge [
    source 20
    target 1674
  ]
  edge [
    source 20
    target 1675
  ]
  edge [
    source 20
    target 1676
  ]
  edge [
    source 20
    target 1677
  ]
  edge [
    source 20
    target 1678
  ]
  edge [
    source 20
    target 1679
  ]
  edge [
    source 20
    target 1680
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 1681
  ]
  edge [
    source 20
    target 1682
  ]
  edge [
    source 20
    target 1683
  ]
  edge [
    source 20
    target 1684
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 1685
  ]
  edge [
    source 20
    target 1686
  ]
  edge [
    source 20
    target 1687
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 1688
  ]
  edge [
    source 20
    target 1689
  ]
  edge [
    source 20
    target 1690
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1691
  ]
  edge [
    source 23
    target 1692
  ]
  edge [
    source 23
    target 1693
  ]
  edge [
    source 23
    target 1694
  ]
  edge [
    source 23
    target 1695
  ]
  edge [
    source 23
    target 1696
  ]
  edge [
    source 23
    target 1697
  ]
  edge [
    source 23
    target 1463
  ]
  edge [
    source 23
    target 1698
  ]
  edge [
    source 23
    target 1699
  ]
  edge [
    source 23
    target 1700
  ]
  edge [
    source 23
    target 1701
  ]
  edge [
    source 23
    target 1370
  ]
  edge [
    source 23
    target 1702
  ]
  edge [
    source 23
    target 93
  ]
  edge [
    source 23
    target 1703
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 23
    target 1704
  ]
  edge [
    source 23
    target 1705
  ]
  edge [
    source 23
    target 1706
  ]
  edge [
    source 23
    target 1707
  ]
  edge [
    source 23
    target 1553
  ]
  edge [
    source 23
    target 1708
  ]
  edge [
    source 23
    target 1709
  ]
  edge [
    source 23
    target 1710
  ]
  edge [
    source 23
    target 1711
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 1712
  ]
  edge [
    source 23
    target 1713
  ]
  edge [
    source 23
    target 1714
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 1715
  ]
  edge [
    source 23
    target 1716
  ]
  edge [
    source 23
    target 1717
  ]
  edge [
    source 23
    target 1718
  ]
  edge [
    source 23
    target 1719
  ]
  edge [
    source 23
    target 1720
  ]
  edge [
    source 23
    target 1721
  ]
  edge [
    source 23
    target 1722
  ]
  edge [
    source 23
    target 1723
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 1724
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 1725
  ]
  edge [
    source 23
    target 1726
  ]
  edge [
    source 23
    target 1727
  ]
  edge [
    source 23
    target 1728
  ]
  edge [
    source 23
    target 1729
  ]
  edge [
    source 23
    target 1730
  ]
  edge [
    source 23
    target 1731
  ]
  edge [
    source 23
    target 1732
  ]
  edge [
    source 23
    target 1733
  ]
  edge [
    source 23
    target 1734
  ]
  edge [
    source 23
    target 1735
  ]
  edge [
    source 23
    target 1736
  ]
  edge [
    source 23
    target 1737
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 1738
  ]
  edge [
    source 23
    target 1739
  ]
  edge [
    source 23
    target 1255
  ]
  edge [
    source 23
    target 1740
  ]
  edge [
    source 23
    target 1741
  ]
  edge [
    source 23
    target 1742
  ]
  edge [
    source 23
    target 1743
  ]
  edge [
    source 23
    target 1744
  ]
  edge [
    source 23
    target 1745
  ]
  edge [
    source 23
    target 1746
  ]
  edge [
    source 23
    target 1747
  ]
  edge [
    source 23
    target 1748
  ]
  edge [
    source 23
    target 1749
  ]
  edge [
    source 23
    target 1750
  ]
  edge [
    source 23
    target 1751
  ]
  edge [
    source 23
    target 1752
  ]
  edge [
    source 23
    target 1753
  ]
  edge [
    source 23
    target 1754
  ]
  edge [
    source 23
    target 433
  ]
  edge [
    source 23
    target 1755
  ]
  edge [
    source 23
    target 1756
  ]
  edge [
    source 23
    target 1757
  ]
  edge [
    source 23
    target 1758
  ]
  edge [
    source 23
    target 1759
  ]
  edge [
    source 23
    target 1760
  ]
  edge [
    source 23
    target 1761
  ]
  edge [
    source 23
    target 1762
  ]
  edge [
    source 23
    target 1763
  ]
  edge [
    source 23
    target 1764
  ]
  edge [
    source 23
    target 1765
  ]
  edge [
    source 23
    target 1766
  ]
  edge [
    source 23
    target 1767
  ]
  edge [
    source 23
    target 1768
  ]
  edge [
    source 23
    target 1680
  ]
  edge [
    source 23
    target 1769
  ]
  edge [
    source 23
    target 1770
  ]
  edge [
    source 23
    target 1566
  ]
  edge [
    source 23
    target 1771
  ]
  edge [
    source 23
    target 1772
  ]
  edge [
    source 23
    target 1686
  ]
  edge [
    source 23
    target 1773
  ]
  edge [
    source 23
    target 1774
  ]
  edge [
    source 23
    target 1775
  ]
  edge [
    source 23
    target 1776
  ]
  edge [
    source 23
    target 1777
  ]
  edge [
    source 23
    target 618
  ]
  edge [
    source 23
    target 1778
  ]
  edge [
    source 23
    target 1779
  ]
  edge [
    source 23
    target 47
  ]
  edge [
    source 23
    target 1780
  ]
  edge [
    source 23
    target 1781
  ]
  edge [
    source 23
    target 1782
  ]
  edge [
    source 23
    target 1783
  ]
  edge [
    source 23
    target 1784
  ]
  edge [
    source 23
    target 1785
  ]
  edge [
    source 23
    target 1786
  ]
  edge [
    source 23
    target 1533
  ]
  edge [
    source 23
    target 1787
  ]
  edge [
    source 23
    target 1788
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1789
  ]
  edge [
    source 24
    target 1790
  ]
  edge [
    source 24
    target 82
  ]
  edge [
    source 24
    target 1791
  ]
  edge [
    source 24
    target 1792
  ]
  edge [
    source 24
    target 1793
  ]
  edge [
    source 24
    target 1279
  ]
  edge [
    source 24
    target 1794
  ]
  edge [
    source 24
    target 1795
  ]
  edge [
    source 24
    target 1796
  ]
  edge [
    source 24
    target 1797
  ]
  edge [
    source 24
    target 1798
  ]
  edge [
    source 24
    target 1799
  ]
  edge [
    source 24
    target 1800
  ]
  edge [
    source 24
    target 1801
  ]
  edge [
    source 24
    target 1802
  ]
  edge [
    source 24
    target 1803
  ]
  edge [
    source 24
    target 1804
  ]
  edge [
    source 24
    target 1564
  ]
  edge [
    source 24
    target 1805
  ]
  edge [
    source 24
    target 1806
  ]
  edge [
    source 24
    target 1807
  ]
  edge [
    source 24
    target 1808
  ]
  edge [
    source 24
    target 1809
  ]
  edge [
    source 24
    target 1810
  ]
  edge [
    source 24
    target 1811
  ]
  edge [
    source 24
    target 1812
  ]
  edge [
    source 24
    target 1813
  ]
  edge [
    source 24
    target 1814
  ]
  edge [
    source 24
    target 1815
  ]
  edge [
    source 24
    target 1816
  ]
  edge [
    source 24
    target 1817
  ]
  edge [
    source 24
    target 1818
  ]
  edge [
    source 24
    target 1819
  ]
  edge [
    source 24
    target 1820
  ]
  edge [
    source 24
    target 1821
  ]
  edge [
    source 24
    target 1822
  ]
  edge [
    source 24
    target 1823
  ]
  edge [
    source 24
    target 1824
  ]
  edge [
    source 24
    target 1825
  ]
  edge [
    source 24
    target 1826
  ]
  edge [
    source 24
    target 1827
  ]
  edge [
    source 24
    target 1828
  ]
  edge [
    source 24
    target 1829
  ]
  edge [
    source 24
    target 1830
  ]
  edge [
    source 24
    target 1831
  ]
  edge [
    source 24
    target 1832
  ]
  edge [
    source 24
    target 1833
  ]
  edge [
    source 24
    target 1834
  ]
  edge [
    source 24
    target 1835
  ]
  edge [
    source 24
    target 1836
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 1837
  ]
  edge [
    source 24
    target 1838
  ]
  edge [
    source 24
    target 1839
  ]
  edge [
    source 24
    target 1840
  ]
  edge [
    source 24
    target 1841
  ]
  edge [
    source 24
    target 1842
  ]
  edge [
    source 24
    target 1843
  ]
  edge [
    source 24
    target 1844
  ]
  edge [
    source 24
    target 555
  ]
  edge [
    source 24
    target 1845
  ]
  edge [
    source 24
    target 1846
  ]
  edge [
    source 24
    target 617
  ]
  edge [
    source 24
    target 1627
  ]
  edge [
    source 24
    target 1593
  ]
  edge [
    source 24
    target 1847
  ]
  edge [
    source 24
    target 1848
  ]
  edge [
    source 24
    target 1849
  ]
  edge [
    source 24
    target 1850
  ]
  edge [
    source 24
    target 1468
  ]
  edge [
    source 24
    target 1851
  ]
  edge [
    source 24
    target 1852
  ]
  edge [
    source 24
    target 1630
  ]
  edge [
    source 24
    target 130
  ]
  edge [
    source 24
    target 1258
  ]
  edge [
    source 24
    target 1853
  ]
  edge [
    source 24
    target 1854
  ]
  edge [
    source 24
    target 1855
  ]
  edge [
    source 24
    target 1257
  ]
  edge [
    source 24
    target 1628
  ]
  edge [
    source 24
    target 1856
  ]
  edge [
    source 24
    target 122
  ]
  edge [
    source 24
    target 1857
  ]
  edge [
    source 24
    target 1858
  ]
  edge [
    source 24
    target 1859
  ]
  edge [
    source 24
    target 1860
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1861
  ]
  edge [
    source 25
    target 1862
  ]
  edge [
    source 25
    target 1863
  ]
  edge [
    source 25
    target 1864
  ]
  edge [
    source 25
    target 1865
  ]
  edge [
    source 25
    target 1866
  ]
  edge [
    source 25
    target 1867
  ]
  edge [
    source 25
    target 1868
  ]
  edge [
    source 25
    target 1869
  ]
  edge [
    source 25
    target 1553
  ]
  edge [
    source 25
    target 1870
  ]
  edge [
    source 25
    target 1871
  ]
  edge [
    source 25
    target 1872
  ]
  edge [
    source 25
    target 1873
  ]
  edge [
    source 25
    target 1874
  ]
  edge [
    source 25
    target 1875
  ]
  edge [
    source 25
    target 1876
  ]
  edge [
    source 25
    target 1877
  ]
  edge [
    source 25
    target 1878
  ]
  edge [
    source 25
    target 1879
  ]
  edge [
    source 25
    target 1880
  ]
  edge [
    source 25
    target 1881
  ]
  edge [
    source 25
    target 1882
  ]
  edge [
    source 25
    target 1883
  ]
  edge [
    source 25
    target 1884
  ]
  edge [
    source 25
    target 1885
  ]
  edge [
    source 25
    target 1886
  ]
  edge [
    source 25
    target 1887
  ]
  edge [
    source 25
    target 1888
  ]
  edge [
    source 25
    target 1889
  ]
  edge [
    source 25
    target 1890
  ]
  edge [
    source 25
    target 1891
  ]
  edge [
    source 25
    target 1892
  ]
  edge [
    source 25
    target 1893
  ]
  edge [
    source 25
    target 1894
  ]
  edge [
    source 25
    target 1895
  ]
  edge [
    source 25
    target 1896
  ]
  edge [
    source 25
    target 1897
  ]
  edge [
    source 25
    target 533
  ]
  edge [
    source 25
    target 1898
  ]
  edge [
    source 25
    target 1751
  ]
  edge [
    source 25
    target 1752
  ]
  edge [
    source 25
    target 1753
  ]
  edge [
    source 25
    target 1754
  ]
  edge [
    source 25
    target 433
  ]
  edge [
    source 25
    target 1755
  ]
  edge [
    source 25
    target 1756
  ]
  edge [
    source 25
    target 1757
  ]
  edge [
    source 25
    target 1758
  ]
  edge [
    source 25
    target 1759
  ]
  edge [
    source 25
    target 1761
  ]
  edge [
    source 25
    target 1760
  ]
  edge [
    source 25
    target 1762
  ]
  edge [
    source 25
    target 1763
  ]
  edge [
    source 25
    target 1764
  ]
  edge [
    source 25
    target 1765
  ]
  edge [
    source 25
    target 1766
  ]
  edge [
    source 25
    target 1767
  ]
  edge [
    source 25
    target 1768
  ]
  edge [
    source 25
    target 1680
  ]
  edge [
    source 25
    target 1769
  ]
  edge [
    source 25
    target 1770
  ]
  edge [
    source 25
    target 1566
  ]
  edge [
    source 25
    target 1771
  ]
  edge [
    source 25
    target 1772
  ]
  edge [
    source 25
    target 1686
  ]
  edge [
    source 25
    target 1773
  ]
  edge [
    source 25
    target 1774
  ]
  edge [
    source 25
    target 1775
  ]
  edge [
    source 25
    target 1776
  ]
  edge [
    source 25
    target 1777
  ]
  edge [
    source 25
    target 1741
  ]
  edge [
    source 25
    target 618
  ]
  edge [
    source 25
    target 1778
  ]
  edge [
    source 25
    target 1779
  ]
  edge [
    source 25
    target 47
  ]
  edge [
    source 25
    target 1780
  ]
  edge [
    source 25
    target 1781
  ]
  edge [
    source 25
    target 1782
  ]
  edge [
    source 25
    target 1783
  ]
  edge [
    source 25
    target 1784
  ]
  edge [
    source 25
    target 1785
  ]
  edge [
    source 25
    target 1786
  ]
  edge [
    source 25
    target 1533
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1878
  ]
  edge [
    source 26
    target 1879
  ]
  edge [
    source 26
    target 1880
  ]
  edge [
    source 26
    target 1881
  ]
  edge [
    source 26
    target 1882
  ]
  edge [
    source 26
    target 1883
  ]
  edge [
    source 26
    target 1899
  ]
  edge [
    source 26
    target 1900
  ]
  edge [
    source 26
    target 1901
  ]
  edge [
    source 26
    target 1902
  ]
  edge [
    source 26
    target 1841
  ]
  edge [
    source 26
    target 1903
  ]
  edge [
    source 26
    target 1904
  ]
  edge [
    source 26
    target 1905
  ]
  edge [
    source 26
    target 1906
  ]
  edge [
    source 26
    target 1907
  ]
  edge [
    source 26
    target 1908
  ]
  edge [
    source 26
    target 300
  ]
  edge [
    source 26
    target 1909
  ]
  edge [
    source 26
    target 1910
  ]
  edge [
    source 26
    target 1911
  ]
  edge [
    source 26
    target 1912
  ]
  edge [
    source 26
    target 1913
  ]
  edge [
    source 26
    target 1914
  ]
  edge [
    source 26
    target 1915
  ]
  edge [
    source 26
    target 1916
  ]
  edge [
    source 26
    target 1917
  ]
  edge [
    source 26
    target 1918
  ]
  edge [
    source 26
    target 1919
  ]
  edge [
    source 26
    target 1920
  ]
  edge [
    source 26
    target 1921
  ]
  edge [
    source 26
    target 1922
  ]
  edge [
    source 26
    target 1923
  ]
  edge [
    source 26
    target 1924
  ]
  edge [
    source 26
    target 1925
  ]
  edge [
    source 26
    target 1926
  ]
  edge [
    source 26
    target 1650
  ]
  edge [
    source 26
    target 1651
  ]
  edge [
    source 26
    target 1652
  ]
  edge [
    source 26
    target 1653
  ]
  edge [
    source 26
    target 1927
  ]
  edge [
    source 26
    target 1674
  ]
  edge [
    source 26
    target 1654
  ]
  edge [
    source 26
    target 1861
  ]
  edge [
    source 26
    target 1862
  ]
  edge [
    source 26
    target 1863
  ]
  edge [
    source 26
    target 1864
  ]
  edge [
    source 26
    target 1865
  ]
  edge [
    source 26
    target 1866
  ]
  edge [
    source 26
    target 1867
  ]
  edge [
    source 26
    target 1868
  ]
  edge [
    source 26
    target 1869
  ]
  edge [
    source 26
    target 1553
  ]
  edge [
    source 26
    target 1870
  ]
  edge [
    source 26
    target 1871
  ]
  edge [
    source 26
    target 1872
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1928
  ]
  edge [
    source 27
    target 1929
  ]
  edge [
    source 27
    target 1930
  ]
  edge [
    source 27
    target 1931
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1932
  ]
  edge [
    source 30
    target 433
  ]
  edge [
    source 30
    target 1930
  ]
  edge [
    source 30
    target 1933
  ]
  edge [
    source 30
    target 1934
  ]
  edge [
    source 30
    target 1935
  ]
  edge [
    source 30
    target 1936
  ]
  edge [
    source 30
    target 1937
  ]
  edge [
    source 30
    target 1938
  ]
  edge [
    source 30
    target 1939
  ]
  edge [
    source 30
    target 1940
  ]
  edge [
    source 30
    target 1941
  ]
  edge [
    source 30
    target 1942
  ]
  edge [
    source 30
    target 1943
  ]
  edge [
    source 30
    target 1944
  ]
  edge [
    source 30
    target 1945
  ]
  edge [
    source 30
    target 1946
  ]
  edge [
    source 30
    target 1947
  ]
  edge [
    source 30
    target 1948
  ]
  edge [
    source 30
    target 1949
  ]
  edge [
    source 30
    target 1950
  ]
  edge [
    source 30
    target 1951
  ]
  edge [
    source 30
    target 1952
  ]
  edge [
    source 30
    target 1953
  ]
  edge [
    source 30
    target 1954
  ]
  edge [
    source 30
    target 1955
  ]
  edge [
    source 30
    target 1956
  ]
  edge [
    source 30
    target 539
  ]
  edge [
    source 30
    target 1957
  ]
  edge [
    source 30
    target 1958
  ]
  edge [
    source 30
    target 1959
  ]
  edge [
    source 30
    target 126
  ]
  edge [
    source 30
    target 1960
  ]
  edge [
    source 30
    target 1961
  ]
  edge [
    source 30
    target 1962
  ]
  edge [
    source 30
    target 1464
  ]
  edge [
    source 30
    target 1963
  ]
  edge [
    source 30
    target 1964
  ]
  edge [
    source 30
    target 1965
  ]
  edge [
    source 30
    target 1966
  ]
  edge [
    source 30
    target 1967
  ]
  edge [
    source 30
    target 1968
  ]
  edge [
    source 30
    target 1969
  ]
  edge [
    source 30
    target 1970
  ]
  edge [
    source 30
    target 1422
  ]
  edge [
    source 30
    target 1800
  ]
  edge [
    source 30
    target 1971
  ]
  edge [
    source 30
    target 1972
  ]
  edge [
    source 30
    target 1973
  ]
  edge [
    source 30
    target 1974
  ]
  edge [
    source 30
    target 1975
  ]
  edge [
    source 30
    target 1976
  ]
  edge [
    source 30
    target 1977
  ]
  edge [
    source 30
    target 1860
  ]
  edge [
    source 30
    target 1978
  ]
  edge [
    source 30
    target 1979
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 1980
  ]
  edge [
    source 30
    target 1981
  ]
  edge [
    source 30
    target 1982
  ]
  edge [
    source 30
    target 1983
  ]
  edge [
    source 30
    target 1984
  ]
  edge [
    source 30
    target 1985
  ]
  edge [
    source 30
    target 1986
  ]
  edge [
    source 30
    target 1805
  ]
  edge [
    source 30
    target 1987
  ]
  edge [
    source 30
    target 1988
  ]
  edge [
    source 30
    target 1989
  ]
  edge [
    source 30
    target 1990
  ]
  edge [
    source 30
    target 1991
  ]
  edge [
    source 30
    target 1992
  ]
  edge [
    source 30
    target 1993
  ]
  edge [
    source 30
    target 47
  ]
  edge [
    source 30
    target 1794
  ]
  edge [
    source 30
    target 1804
  ]
  edge [
    source 30
    target 1994
  ]
  edge [
    source 30
    target 1995
  ]
  edge [
    source 30
    target 1996
  ]
  edge [
    source 30
    target 1997
  ]
  edge [
    source 30
    target 1998
  ]
  edge [
    source 30
    target 1999
  ]
  edge [
    source 30
    target 2000
  ]
  edge [
    source 30
    target 2001
  ]
  edge [
    source 30
    target 2002
  ]
  edge [
    source 30
    target 2003
  ]
  edge [
    source 30
    target 2004
  ]
  edge [
    source 30
    target 2005
  ]
  edge [
    source 30
    target 2006
  ]
  edge [
    source 30
    target 183
  ]
  edge [
    source 30
    target 2007
  ]
  edge [
    source 30
    target 2008
  ]
  edge [
    source 30
    target 1799
  ]
  edge [
    source 30
    target 510
  ]
  edge [
    source 30
    target 2009
  ]
  edge [
    source 30
    target 524
  ]
  edge [
    source 30
    target 2010
  ]
  edge [
    source 30
    target 2011
  ]
  edge [
    source 30
    target 525
  ]
  edge [
    source 30
    target 2012
  ]
  edge [
    source 30
    target 529
  ]
  edge [
    source 30
    target 2013
  ]
  edge [
    source 30
    target 2014
  ]
  edge [
    source 30
    target 2015
  ]
  edge [
    source 30
    target 2016
  ]
  edge [
    source 30
    target 2017
  ]
  edge [
    source 30
    target 2018
  ]
  edge [
    source 30
    target 2019
  ]
  edge [
    source 30
    target 2020
  ]
  edge [
    source 30
    target 2021
  ]
  edge [
    source 30
    target 605
  ]
  edge [
    source 30
    target 2022
  ]
  edge [
    source 30
    target 2023
  ]
  edge [
    source 30
    target 2024
  ]
  edge [
    source 30
    target 1791
  ]
  edge [
    source 30
    target 1246
  ]
  edge [
    source 30
    target 2025
  ]
  edge [
    source 30
    target 1225
  ]
  edge [
    source 30
    target 2026
  ]
  edge [
    source 30
    target 2027
  ]
  edge [
    source 30
    target 1302
  ]
  edge [
    source 30
    target 2028
  ]
  edge [
    source 30
    target 82
  ]
  edge [
    source 30
    target 2029
  ]
  edge [
    source 30
    target 2030
  ]
  edge [
    source 30
    target 576
  ]
  edge [
    source 30
    target 2031
  ]
  edge [
    source 30
    target 2032
  ]
  edge [
    source 30
    target 1399
  ]
  edge [
    source 30
    target 2033
  ]
  edge [
    source 30
    target 2034
  ]
  edge [
    source 30
    target 2035
  ]
  edge [
    source 30
    target 2036
  ]
  edge [
    source 30
    target 2037
  ]
  edge [
    source 30
    target 2038
  ]
  edge [
    source 30
    target 1310
  ]
  edge [
    source 30
    target 1311
  ]
  edge [
    source 30
    target 2039
  ]
  edge [
    source 30
    target 1312
  ]
  edge [
    source 30
    target 482
  ]
  edge [
    source 30
    target 2040
  ]
  edge [
    source 30
    target 2041
  ]
  edge [
    source 30
    target 2042
  ]
  edge [
    source 30
    target 1314
  ]
  edge [
    source 30
    target 1315
  ]
  edge [
    source 30
    target 2043
  ]
  edge [
    source 30
    target 1317
  ]
  edge [
    source 30
    target 2044
  ]
  edge [
    source 30
    target 1319
  ]
  edge [
    source 30
    target 1322
  ]
  edge [
    source 30
    target 1318
  ]
  edge [
    source 30
    target 1324
  ]
  edge [
    source 30
    target 2045
  ]
  edge [
    source 30
    target 2046
  ]
  edge [
    source 30
    target 2047
  ]
  edge [
    source 30
    target 2048
  ]
  edge [
    source 30
    target 2049
  ]
  edge [
    source 30
    target 2050
  ]
  edge [
    source 30
    target 2051
  ]
  edge [
    source 30
    target 2052
  ]
  edge [
    source 30
    target 2053
  ]
  edge [
    source 30
    target 2054
  ]
  edge [
    source 30
    target 2055
  ]
  edge [
    source 30
    target 2056
  ]
  edge [
    source 30
    target 2057
  ]
  edge [
    source 30
    target 2058
  ]
  edge [
    source 30
    target 2059
  ]
  edge [
    source 30
    target 2060
  ]
  edge [
    source 30
    target 2061
  ]
  edge [
    source 30
    target 428
  ]
  edge [
    source 30
    target 2062
  ]
  edge [
    source 30
    target 49
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 2063
  ]
  edge [
    source 31
    target 2064
  ]
  edge [
    source 31
    target 2065
  ]
  edge [
    source 31
    target 433
  ]
  edge [
    source 31
    target 1943
  ]
  edge [
    source 31
    target 1930
  ]
  edge [
    source 31
    target 1933
  ]
  edge [
    source 31
    target 511
  ]
  edge [
    source 31
    target 2066
  ]
  edge [
    source 31
    target 2067
  ]
  edge [
    source 31
    target 2068
  ]
  edge [
    source 31
    target 1593
  ]
  edge [
    source 31
    target 2069
  ]
  edge [
    source 31
    target 2070
  ]
  edge [
    source 31
    target 2071
  ]
  edge [
    source 31
    target 2072
  ]
  edge [
    source 31
    target 1953
  ]
  edge [
    source 31
    target 1954
  ]
  edge [
    source 31
    target 1955
  ]
  edge [
    source 31
    target 1956
  ]
  edge [
    source 31
    target 539
  ]
  edge [
    source 31
    target 1957
  ]
  edge [
    source 31
    target 1958
  ]
  edge [
    source 31
    target 1959
  ]
  edge [
    source 31
    target 126
  ]
  edge [
    source 31
    target 1960
  ]
  edge [
    source 31
    target 1961
  ]
  edge [
    source 31
    target 1962
  ]
  edge [
    source 31
    target 1464
  ]
  edge [
    source 31
    target 1963
  ]
  edge [
    source 31
    target 1964
  ]
  edge [
    source 31
    target 1965
  ]
  edge [
    source 31
    target 1966
  ]
  edge [
    source 31
    target 1967
  ]
  edge [
    source 31
    target 1968
  ]
  edge [
    source 31
    target 1969
  ]
  edge [
    source 31
    target 1970
  ]
  edge [
    source 31
    target 1422
  ]
  edge [
    source 31
    target 1800
  ]
  edge [
    source 31
    target 1971
  ]
  edge [
    source 31
    target 1972
  ]
  edge [
    source 31
    target 1973
  ]
  edge [
    source 31
    target 1974
  ]
  edge [
    source 31
    target 1975
  ]
  edge [
    source 31
    target 1976
  ]
  edge [
    source 31
    target 1977
  ]
  edge [
    source 31
    target 1860
  ]
  edge [
    source 31
    target 1978
  ]
  edge [
    source 31
    target 1979
  ]
  edge [
    source 31
    target 258
  ]
  edge [
    source 31
    target 1980
  ]
  edge [
    source 31
    target 1981
  ]
  edge [
    source 31
    target 1982
  ]
  edge [
    source 31
    target 1983
  ]
  edge [
    source 31
    target 1984
  ]
  edge [
    source 31
    target 1985
  ]
  edge [
    source 31
    target 1986
  ]
  edge [
    source 31
    target 1805
  ]
  edge [
    source 31
    target 1987
  ]
  edge [
    source 31
    target 1988
  ]
  edge [
    source 31
    target 1989
  ]
  edge [
    source 31
    target 1990
  ]
  edge [
    source 31
    target 1991
  ]
  edge [
    source 31
    target 1992
  ]
  edge [
    source 31
    target 1993
  ]
  edge [
    source 31
    target 47
  ]
  edge [
    source 31
    target 1794
  ]
  edge [
    source 31
    target 1804
  ]
  edge [
    source 31
    target 1994
  ]
  edge [
    source 31
    target 1995
  ]
  edge [
    source 31
    target 1996
  ]
  edge [
    source 31
    target 2061
  ]
  edge [
    source 31
    target 428
  ]
  edge [
    source 31
    target 2062
  ]
  edge [
    source 31
    target 2073
  ]
  edge [
    source 31
    target 2074
  ]
  edge [
    source 31
    target 391
  ]
  edge [
    source 31
    target 2075
  ]
  edge [
    source 31
    target 2076
  ]
  edge [
    source 31
    target 1514
  ]
  edge [
    source 31
    target 2077
  ]
  edge [
    source 31
    target 2078
  ]
  edge [
    source 31
    target 2079
  ]
  edge [
    source 31
    target 2080
  ]
  edge [
    source 31
    target 2081
  ]
  edge [
    source 31
    target 2082
  ]
  edge [
    source 31
    target 2083
  ]
  edge [
    source 31
    target 2084
  ]
  edge [
    source 31
    target 398
  ]
  edge [
    source 31
    target 2085
  ]
  edge [
    source 31
    target 2086
  ]
  edge [
    source 31
    target 1564
  ]
  edge [
    source 31
    target 2087
  ]
  edge [
    source 31
    target 2088
  ]
  edge [
    source 31
    target 2089
  ]
  edge [
    source 31
    target 1521
  ]
  edge [
    source 31
    target 2090
  ]
  edge [
    source 31
    target 2091
  ]
  edge [
    source 31
    target 2092
  ]
  edge [
    source 31
    target 2093
  ]
  edge [
    source 31
    target 2094
  ]
  edge [
    source 31
    target 2095
  ]
  edge [
    source 31
    target 2096
  ]
  edge [
    source 31
    target 2097
  ]
  edge [
    source 31
    target 2098
  ]
  edge [
    source 31
    target 605
  ]
  edge [
    source 31
    target 2099
  ]
  edge [
    source 31
    target 1523
  ]
  edge [
    source 31
    target 1789
  ]
  edge [
    source 31
    target 2100
  ]
  edge [
    source 31
    target 2101
  ]
  edge [
    source 31
    target 2102
  ]
  edge [
    source 31
    target 426
  ]
  edge [
    source 31
    target 2103
  ]
  edge [
    source 31
    target 2104
  ]
  edge [
    source 31
    target 2105
  ]
  edge [
    source 31
    target 2106
  ]
  edge [
    source 31
    target 2107
  ]
  edge [
    source 31
    target 2108
  ]
  edge [
    source 31
    target 1577
  ]
  edge [
    source 31
    target 2109
  ]
  edge [
    source 31
    target 2110
  ]
  edge [
    source 31
    target 1531
  ]
  edge [
    source 31
    target 1585
  ]
  edge [
    source 31
    target 2111
  ]
  edge [
    source 31
    target 2112
  ]
  edge [
    source 31
    target 2113
  ]
  edge [
    source 31
    target 2114
  ]
  edge [
    source 31
    target 1841
  ]
  edge [
    source 31
    target 1833
  ]
  edge [
    source 31
    target 2115
  ]
  edge [
    source 31
    target 2116
  ]
  edge [
    source 31
    target 2117
  ]
  edge [
    source 31
    target 2118
  ]
  edge [
    source 31
    target 2119
  ]
  edge [
    source 31
    target 2120
  ]
  edge [
    source 31
    target 390
  ]
  edge [
    source 31
    target 2121
  ]
  edge [
    source 31
    target 1547
  ]
  edge [
    source 31
    target 2122
  ]
  edge [
    source 31
    target 1123
  ]
  edge [
    source 31
    target 2123
  ]
  edge [
    source 31
    target 2124
  ]
  edge [
    source 31
    target 2125
  ]
  edge [
    source 31
    target 2126
  ]
  edge [
    source 31
    target 2127
  ]
  edge [
    source 31
    target 2128
  ]
  edge [
    source 31
    target 2129
  ]
  edge [
    source 31
    target 2130
  ]
  edge [
    source 31
    target 2131
  ]
  edge [
    source 31
    target 2132
  ]
  edge [
    source 31
    target 2133
  ]
  edge [
    source 31
    target 555
  ]
  edge [
    source 31
    target 2134
  ]
  edge [
    source 31
    target 2135
  ]
  edge [
    source 31
    target 2136
  ]
  edge [
    source 31
    target 2137
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 2138
  ]
  edge [
    source 32
    target 2139
  ]
  edge [
    source 32
    target 2140
  ]
  edge [
    source 32
    target 2141
  ]
  edge [
    source 32
    target 2142
  ]
  edge [
    source 32
    target 2143
  ]
  edge [
    source 32
    target 2144
  ]
  edge [
    source 32
    target 2145
  ]
  edge [
    source 32
    target 2146
  ]
  edge [
    source 32
    target 2147
  ]
  edge [
    source 32
    target 2148
  ]
  edge [
    source 32
    target 2149
  ]
  edge [
    source 32
    target 2150
  ]
  edge [
    source 32
    target 2151
  ]
  edge [
    source 32
    target 2152
  ]
  edge [
    source 32
    target 2153
  ]
  edge [
    source 32
    target 2154
  ]
  edge [
    source 32
    target 2155
  ]
  edge [
    source 32
    target 2156
  ]
  edge [
    source 32
    target 2157
  ]
  edge [
    source 32
    target 2158
  ]
  edge [
    source 32
    target 2159
  ]
  edge [
    source 32
    target 2160
  ]
  edge [
    source 32
    target 2161
  ]
  edge [
    source 32
    target 428
  ]
  edge [
    source 33
    target 2162
  ]
  edge [
    source 33
    target 2163
  ]
  edge [
    source 33
    target 2164
  ]
  edge [
    source 33
    target 2165
  ]
  edge [
    source 33
    target 2166
  ]
  edge [
    source 33
    target 2167
  ]
  edge [
    source 33
    target 2168
  ]
  edge [
    source 33
    target 2169
  ]
  edge [
    source 33
    target 437
  ]
  edge [
    source 33
    target 2170
  ]
  edge [
    source 33
    target 2171
  ]
  edge [
    source 33
    target 2172
  ]
  edge [
    source 33
    target 94
  ]
  edge [
    source 33
    target 2173
  ]
  edge [
    source 33
    target 2174
  ]
  edge [
    source 33
    target 1962
  ]
  edge [
    source 33
    target 2175
  ]
  edge [
    source 33
    target 2176
  ]
  edge [
    source 33
    target 2177
  ]
  edge [
    source 33
    target 1795
  ]
  edge [
    source 33
    target 2178
  ]
  edge [
    source 33
    target 2179
  ]
  edge [
    source 33
    target 2180
  ]
  edge [
    source 33
    target 546
  ]
  edge [
    source 33
    target 2181
  ]
  edge [
    source 33
    target 312
  ]
  edge [
    source 33
    target 2182
  ]
  edge [
    source 33
    target 2183
  ]
  edge [
    source 33
    target 2184
  ]
  edge [
    source 33
    target 2185
  ]
  edge [
    source 33
    target 2186
  ]
  edge [
    source 33
    target 2187
  ]
  edge [
    source 33
    target 134
  ]
  edge [
    source 33
    target 2188
  ]
  edge [
    source 33
    target 2189
  ]
  edge [
    source 33
    target 2190
  ]
  edge [
    source 33
    target 2191
  ]
  edge [
    source 33
    target 2192
  ]
  edge [
    source 33
    target 2193
  ]
  edge [
    source 33
    target 2194
  ]
  edge [
    source 33
    target 2195
  ]
  edge [
    source 33
    target 103
  ]
  edge [
    source 33
    target 104
  ]
  edge [
    source 33
    target 87
  ]
  edge [
    source 33
    target 105
  ]
  edge [
    source 33
    target 106
  ]
  edge [
    source 33
    target 107
  ]
  edge [
    source 33
    target 2196
  ]
  edge [
    source 33
    target 2197
  ]
  edge [
    source 33
    target 128
  ]
  edge [
    source 33
    target 2198
  ]
  edge [
    source 33
    target 2199
  ]
  edge [
    source 33
    target 2200
  ]
  edge [
    source 33
    target 2201
  ]
  edge [
    source 33
    target 2202
  ]
  edge [
    source 33
    target 2203
  ]
  edge [
    source 33
    target 1449
  ]
  edge [
    source 33
    target 2204
  ]
  edge [
    source 33
    target 126
  ]
  edge [
    source 33
    target 2205
  ]
  edge [
    source 33
    target 2206
  ]
  edge [
    source 33
    target 2207
  ]
  edge [
    source 33
    target 2208
  ]
  edge [
    source 33
    target 2209
  ]
  edge [
    source 33
    target 2210
  ]
  edge [
    source 33
    target 2211
  ]
  edge [
    source 33
    target 2212
  ]
  edge [
    source 33
    target 2213
  ]
  edge [
    source 33
    target 2214
  ]
  edge [
    source 33
    target 2215
  ]
  edge [
    source 33
    target 576
  ]
  edge [
    source 33
    target 2216
  ]
  edge [
    source 33
    target 2217
  ]
  edge [
    source 33
    target 442
  ]
  edge [
    source 33
    target 530
  ]
  edge [
    source 33
    target 314
  ]
  edge [
    source 33
    target 315
  ]
  edge [
    source 33
    target 316
  ]
  edge [
    source 33
    target 317
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 321
  ]
  edge [
    source 33
    target 96
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 323
  ]
  edge [
    source 33
    target 324
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 33
    target 329
  ]
  edge [
    source 33
    target 330
  ]
  edge [
    source 33
    target 331
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 33
    target 334
  ]
  edge [
    source 33
    target 335
  ]
  edge [
    source 33
    target 336
  ]
  edge [
    source 33
    target 337
  ]
  edge [
    source 33
    target 338
  ]
  edge [
    source 33
    target 339
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 33
    target 343
  ]
  edge [
    source 33
    target 344
  ]
  edge [
    source 33
    target 345
  ]
  edge [
    source 33
    target 346
  ]
  edge [
    source 33
    target 347
  ]
  edge [
    source 33
    target 348
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 2218
  ]
  edge [
    source 33
    target 2219
  ]
  edge [
    source 33
    target 2220
  ]
  edge [
    source 33
    target 2221
  ]
  edge [
    source 33
    target 2222
  ]
  edge [
    source 33
    target 2223
  ]
  edge [
    source 33
    target 1396
  ]
  edge [
    source 33
    target 2224
  ]
  edge [
    source 33
    target 2225
  ]
  edge [
    source 33
    target 2226
  ]
  edge [
    source 33
    target 2227
  ]
  edge [
    source 33
    target 2228
  ]
  edge [
    source 33
    target 2229
  ]
  edge [
    source 33
    target 2230
  ]
  edge [
    source 33
    target 1423
  ]
  edge [
    source 33
    target 2231
  ]
  edge [
    source 33
    target 1380
  ]
  edge [
    source 33
    target 2232
  ]
  edge [
    source 33
    target 2233
  ]
  edge [
    source 33
    target 494
  ]
  edge [
    source 33
    target 2234
  ]
  edge [
    source 33
    target 2235
  ]
  edge [
    source 33
    target 2236
  ]
  edge [
    source 33
    target 1254
  ]
  edge [
    source 33
    target 2237
  ]
  edge [
    source 33
    target 2238
  ]
  edge [
    source 33
    target 2239
  ]
  edge [
    source 33
    target 526
  ]
  edge [
    source 33
    target 2240
  ]
  edge [
    source 33
    target 2241
  ]
  edge [
    source 33
    target 2242
  ]
  edge [
    source 33
    target 2243
  ]
  edge [
    source 33
    target 2244
  ]
  edge [
    source 33
    target 1240
  ]
  edge [
    source 33
    target 1355
  ]
  edge [
    source 33
    target 2245
  ]
  edge [
    source 33
    target 2246
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 2247
  ]
  edge [
    source 33
    target 1231
  ]
  edge [
    source 33
    target 2248
  ]
  edge [
    source 33
    target 2249
  ]
  edge [
    source 33
    target 2250
  ]
  edge [
    source 33
    target 2251
  ]
  edge [
    source 33
    target 2252
  ]
  edge [
    source 33
    target 2253
  ]
  edge [
    source 33
    target 2254
  ]
  edge [
    source 33
    target 2255
  ]
  edge [
    source 33
    target 2256
  ]
  edge [
    source 33
    target 2257
  ]
  edge [
    source 33
    target 2258
  ]
  edge [
    source 33
    target 2259
  ]
  edge [
    source 33
    target 2260
  ]
  edge [
    source 33
    target 2261
  ]
  edge [
    source 33
    target 2262
  ]
  edge [
    source 33
    target 2263
  ]
  edge [
    source 33
    target 2264
  ]
  edge [
    source 33
    target 2265
  ]
  edge [
    source 33
    target 1632
  ]
  edge [
    source 33
    target 2266
  ]
  edge [
    source 33
    target 2267
  ]
  edge [
    source 33
    target 2268
  ]
  edge [
    source 33
    target 2269
  ]
  edge [
    source 33
    target 2270
  ]
  edge [
    source 33
    target 2271
  ]
  edge [
    source 33
    target 2272
  ]
  edge [
    source 33
    target 2273
  ]
  edge [
    source 33
    target 2274
  ]
  edge [
    source 33
    target 2275
  ]
  edge [
    source 33
    target 2276
  ]
  edge [
    source 33
    target 2277
  ]
  edge [
    source 33
    target 2278
  ]
  edge [
    source 33
    target 2279
  ]
  edge [
    source 33
    target 2280
  ]
  edge [
    source 33
    target 2281
  ]
  edge [
    source 33
    target 2282
  ]
  edge [
    source 33
    target 2283
  ]
  edge [
    source 33
    target 2284
  ]
  edge [
    source 33
    target 2285
  ]
  edge [
    source 33
    target 2286
  ]
  edge [
    source 33
    target 2287
  ]
  edge [
    source 33
    target 2288
  ]
  edge [
    source 33
    target 2289
  ]
  edge [
    source 33
    target 2290
  ]
  edge [
    source 33
    target 2291
  ]
  edge [
    source 33
    target 2292
  ]
  edge [
    source 33
    target 2293
  ]
  edge [
    source 33
    target 2294
  ]
  edge [
    source 33
    target 76
  ]
  edge [
    source 33
    target 2295
  ]
  edge [
    source 33
    target 2296
  ]
  edge [
    source 33
    target 2297
  ]
  edge [
    source 33
    target 2298
  ]
  edge [
    source 33
    target 2299
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 2300
  ]
  edge [
    source 34
    target 1360
  ]
  edge [
    source 34
    target 2301
  ]
  edge [
    source 34
    target 2302
  ]
  edge [
    source 34
    target 2303
  ]
  edge [
    source 34
    target 2191
  ]
  edge [
    source 34
    target 2304
  ]
  edge [
    source 34
    target 2305
  ]
  edge [
    source 34
    target 2306
  ]
  edge [
    source 34
    target 2307
  ]
  edge [
    source 34
    target 2308
  ]
  edge [
    source 34
    target 2309
  ]
  edge [
    source 34
    target 2310
  ]
  edge [
    source 34
    target 2311
  ]
  edge [
    source 34
    target 1303
  ]
  edge [
    source 34
    target 2312
  ]
  edge [
    source 34
    target 2313
  ]
  edge [
    source 34
    target 2314
  ]
  edge [
    source 34
    target 2315
  ]
  edge [
    source 34
    target 1898
  ]
  edge [
    source 34
    target 2316
  ]
  edge [
    source 34
    target 2317
  ]
  edge [
    source 34
    target 2318
  ]
  edge [
    source 34
    target 2319
  ]
  edge [
    source 34
    target 2293
  ]
  edge [
    source 34
    target 2320
  ]
  edge [
    source 34
    target 2321
  ]
  edge [
    source 34
    target 2322
  ]
  edge [
    source 34
    target 2323
  ]
  edge [
    source 34
    target 2324
  ]
  edge [
    source 34
    target 2325
  ]
  edge [
    source 34
    target 2326
  ]
  edge [
    source 34
    target 2327
  ]
  edge [
    source 34
    target 2328
  ]
  edge [
    source 34
    target 2253
  ]
  edge [
    source 34
    target 2329
  ]
  edge [
    source 34
    target 2244
  ]
  edge [
    source 34
    target 1240
  ]
  edge [
    source 34
    target 1355
  ]
  edge [
    source 34
    target 2245
  ]
  edge [
    source 34
    target 2246
  ]
  edge [
    source 34
    target 300
  ]
  edge [
    source 34
    target 2247
  ]
  edge [
    source 34
    target 1231
  ]
  edge [
    source 34
    target 2248
  ]
  edge [
    source 34
    target 2249
  ]
  edge [
    source 34
    target 2250
  ]
  edge [
    source 34
    target 2251
  ]
  edge [
    source 34
    target 2252
  ]
  edge [
    source 34
    target 2254
  ]
  edge [
    source 34
    target 2255
  ]
  edge [
    source 34
    target 1365
  ]
  edge [
    source 34
    target 2330
  ]
  edge [
    source 34
    target 2331
  ]
  edge [
    source 34
    target 2332
  ]
  edge [
    source 34
    target 2333
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 2334
  ]
  edge [
    source 34
    target 576
  ]
  edge [
    source 34
    target 2335
  ]
  edge [
    source 34
    target 2336
  ]
  edge [
    source 34
    target 2337
  ]
  edge [
    source 34
    target 2338
  ]
  edge [
    source 34
    target 2339
  ]
  edge [
    source 34
    target 2340
  ]
  edge [
    source 34
    target 2341
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 2342
  ]
  edge [
    source 35
    target 2343
  ]
  edge [
    source 35
    target 2344
  ]
  edge [
    source 35
    target 2345
  ]
  edge [
    source 35
    target 1733
  ]
  edge [
    source 35
    target 2346
  ]
  edge [
    source 35
    target 1225
  ]
  edge [
    source 35
    target 2347
  ]
  edge [
    source 35
    target 2348
  ]
  edge [
    source 35
    target 1506
  ]
  edge [
    source 35
    target 1961
  ]
  edge [
    source 35
    target 2349
  ]
  edge [
    source 35
    target 1533
  ]
  edge [
    source 35
    target 2350
  ]
  edge [
    source 35
    target 2351
  ]
  edge [
    source 35
    target 2352
  ]
  edge [
    source 35
    target 2353
  ]
  edge [
    source 35
    target 2354
  ]
  edge [
    source 35
    target 2191
  ]
  edge [
    source 35
    target 2355
  ]
  edge [
    source 35
    target 2356
  ]
  edge [
    source 35
    target 2357
  ]
  edge [
    source 35
    target 2358
  ]
  edge [
    source 35
    target 2359
  ]
  edge [
    source 35
    target 2360
  ]
  edge [
    source 35
    target 2361
  ]
  edge [
    source 35
    target 2362
  ]
  edge [
    source 35
    target 2363
  ]
  edge [
    source 35
    target 2364
  ]
  edge [
    source 35
    target 2365
  ]
  edge [
    source 35
    target 1576
  ]
  edge [
    source 35
    target 1615
  ]
  edge [
    source 35
    target 2366
  ]
  edge [
    source 35
    target 291
  ]
  edge [
    source 35
    target 2367
  ]
  edge [
    source 35
    target 2368
  ]
  edge [
    source 35
    target 2369
  ]
  edge [
    source 35
    target 2370
  ]
  edge [
    source 35
    target 122
  ]
  edge [
    source 35
    target 2371
  ]
  edge [
    source 35
    target 2372
  ]
  edge [
    source 35
    target 139
  ]
  edge [
    source 35
    target 2373
  ]
  edge [
    source 35
    target 2374
  ]
  edge [
    source 35
    target 2375
  ]
  edge [
    source 35
    target 2376
  ]
  edge [
    source 35
    target 2377
  ]
  edge [
    source 35
    target 110
  ]
  edge [
    source 35
    target 2378
  ]
  edge [
    source 35
    target 2379
  ]
  edge [
    source 35
    target 2380
  ]
  edge [
    source 35
    target 2381
  ]
  edge [
    source 35
    target 239
  ]
  edge [
    source 35
    target 2382
  ]
  edge [
    source 35
    target 2383
  ]
  edge [
    source 35
    target 2384
  ]
  edge [
    source 35
    target 2385
  ]
  edge [
    source 35
    target 2386
  ]
  edge [
    source 35
    target 2387
  ]
  edge [
    source 35
    target 2388
  ]
  edge [
    source 35
    target 2389
  ]
  edge [
    source 35
    target 1591
  ]
  edge [
    source 35
    target 2390
  ]
  edge [
    source 35
    target 144
  ]
  edge [
    source 35
    target 170
  ]
  edge [
    source 35
    target 2391
  ]
  edge [
    source 35
    target 2392
  ]
  edge [
    source 35
    target 2393
  ]
  edge [
    source 35
    target 2394
  ]
  edge [
    source 35
    target 2395
  ]
  edge [
    source 35
    target 1370
  ]
  edge [
    source 35
    target 2396
  ]
  edge [
    source 35
    target 2397
  ]
  edge [
    source 35
    target 2398
  ]
  edge [
    source 35
    target 2399
  ]
  edge [
    source 35
    target 203
  ]
  edge [
    source 35
    target 1731
  ]
  edge [
    source 36
    target 2400
  ]
  edge [
    source 36
    target 2401
  ]
  edge [
    source 36
    target 2402
  ]
  edge [
    source 36
    target 2403
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 2404
  ]
  edge [
    source 37
    target 2405
  ]
  edge [
    source 37
    target 2330
  ]
  edge [
    source 37
    target 2406
  ]
  edge [
    source 37
    target 1358
  ]
  edge [
    source 37
    target 2407
  ]
  edge [
    source 37
    target 2408
  ]
  edge [
    source 37
    target 2409
  ]
  edge [
    source 37
    target 2251
  ]
  edge [
    source 37
    target 2410
  ]
  edge [
    source 37
    target 2411
  ]
  edge [
    source 37
    target 2412
  ]
  edge [
    source 37
    target 2413
  ]
  edge [
    source 37
    target 1238
  ]
  edge [
    source 37
    target 2414
  ]
  edge [
    source 37
    target 2415
  ]
  edge [
    source 37
    target 2416
  ]
  edge [
    source 37
    target 2417
  ]
  edge [
    source 37
    target 2418
  ]
  edge [
    source 37
    target 2419
  ]
  edge [
    source 37
    target 2249
  ]
  edge [
    source 37
    target 2420
  ]
  edge [
    source 37
    target 2421
  ]
  edge [
    source 37
    target 2422
  ]
  edge [
    source 37
    target 2423
  ]
  edge [
    source 37
    target 2424
  ]
  edge [
    source 37
    target 513
  ]
  edge [
    source 37
    target 2425
  ]
  edge [
    source 37
    target 2426
  ]
  edge [
    source 37
    target 2427
  ]
  edge [
    source 37
    target 2428
  ]
  edge [
    source 37
    target 2429
  ]
  edge [
    source 37
    target 2430
  ]
  edge [
    source 37
    target 2431
  ]
  edge [
    source 37
    target 2432
  ]
  edge [
    source 37
    target 2433
  ]
  edge [
    source 37
    target 2434
  ]
  edge [
    source 37
    target 2435
  ]
  edge [
    source 37
    target 2436
  ]
  edge [
    source 37
    target 2437
  ]
  edge [
    source 37
    target 1252
  ]
  edge [
    source 37
    target 1253
  ]
  edge [
    source 37
    target 1254
  ]
  edge [
    source 37
    target 1255
  ]
  edge [
    source 37
    target 1256
  ]
  edge [
    source 37
    target 1257
  ]
  edge [
    source 37
    target 1258
  ]
  edge [
    source 37
    target 1259
  ]
  edge [
    source 37
    target 1260
  ]
  edge [
    source 37
    target 1261
  ]
  edge [
    source 37
    target 1262
  ]
  edge [
    source 37
    target 2438
  ]
  edge [
    source 37
    target 2439
  ]
  edge [
    source 37
    target 2328
  ]
  edge [
    source 37
    target 2440
  ]
  edge [
    source 37
    target 2441
  ]
  edge [
    source 37
    target 2442
  ]
  edge [
    source 37
    target 2443
  ]
  edge [
    source 37
    target 1353
  ]
  edge [
    source 37
    target 2244
  ]
  edge [
    source 37
    target 2444
  ]
  edge [
    source 37
    target 2231
  ]
  edge [
    source 37
    target 2445
  ]
  edge [
    source 37
    target 2446
  ]
  edge [
    source 37
    target 2250
  ]
  edge [
    source 37
    target 2447
  ]
  edge [
    source 37
    target 2448
  ]
  edge [
    source 37
    target 2449
  ]
  edge [
    source 37
    target 2450
  ]
  edge [
    source 37
    target 2451
  ]
  edge [
    source 37
    target 2452
  ]
  edge [
    source 37
    target 2453
  ]
  edge [
    source 37
    target 2454
  ]
  edge [
    source 37
    target 2455
  ]
  edge [
    source 37
    target 2456
  ]
  edge [
    source 37
    target 2457
  ]
  edge [
    source 37
    target 2458
  ]
  edge [
    source 37
    target 2459
  ]
  edge [
    source 37
    target 2460
  ]
  edge [
    source 37
    target 130
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 54
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1575
  ]
  edge [
    source 41
    target 1451
  ]
  edge [
    source 41
    target 2461
  ]
  edge [
    source 41
    target 2462
  ]
  edge [
    source 41
    target 2463
  ]
  edge [
    source 41
    target 2464
  ]
  edge [
    source 41
    target 258
  ]
  edge [
    source 41
    target 2465
  ]
  edge [
    source 41
    target 1306
  ]
  edge [
    source 41
    target 1980
  ]
  edge [
    source 41
    target 1981
  ]
  edge [
    source 41
    target 1982
  ]
  edge [
    source 41
    target 1984
  ]
  edge [
    source 41
    target 1985
  ]
  edge [
    source 41
    target 1279
  ]
  edge [
    source 41
    target 1805
  ]
  edge [
    source 41
    target 1990
  ]
  edge [
    source 41
    target 1223
  ]
  edge [
    source 41
    target 396
  ]
  edge [
    source 41
    target 1629
  ]
  edge [
    source 41
    target 482
  ]
  edge [
    source 41
    target 1991
  ]
  edge [
    source 41
    target 2466
  ]
  edge [
    source 41
    target 2467
  ]
  edge [
    source 41
    target 1992
  ]
  edge [
    source 41
    target 1993
  ]
  edge [
    source 41
    target 2468
  ]
  edge [
    source 41
    target 1804
  ]
  edge [
    source 41
    target 1994
  ]
  edge [
    source 41
    target 2469
  ]
  edge [
    source 41
    target 517
  ]
  edge [
    source 41
    target 1225
  ]
  edge [
    source 41
    target 1809
  ]
  edge [
    source 41
    target 1996
  ]
  edge [
    source 41
    target 2470
  ]
  edge [
    source 41
    target 1795
  ]
  edge [
    source 41
    target 300
  ]
  edge [
    source 41
    target 2471
  ]
  edge [
    source 41
    target 375
  ]
  edge [
    source 41
    target 2472
  ]
  edge [
    source 41
    target 2473
  ]
  edge [
    source 41
    target 2474
  ]
  edge [
    source 41
    target 2475
  ]
  edge [
    source 41
    target 2476
  ]
  edge [
    source 41
    target 2477
  ]
  edge [
    source 41
    target 494
  ]
  edge [
    source 41
    target 2478
  ]
  edge [
    source 41
    target 2479
  ]
  edge [
    source 41
    target 2480
  ]
  edge [
    source 41
    target 2481
  ]
  edge [
    source 41
    target 1564
  ]
  edge [
    source 41
    target 2482
  ]
  edge [
    source 41
    target 2483
  ]
  edge [
    source 41
    target 2484
  ]
  edge [
    source 41
    target 2485
  ]
  edge [
    source 41
    target 2486
  ]
  edge [
    source 41
    target 2487
  ]
  edge [
    source 41
    target 2488
  ]
  edge [
    source 41
    target 2489
  ]
  edge [
    source 41
    target 2490
  ]
  edge [
    source 41
    target 2491
  ]
  edge [
    source 41
    target 1855
  ]
  edge [
    source 41
    target 2492
  ]
  edge [
    source 41
    target 2493
  ]
  edge [
    source 41
    target 2494
  ]
  edge [
    source 41
    target 1974
  ]
  edge [
    source 41
    target 2014
  ]
  edge [
    source 41
    target 1975
  ]
  edge [
    source 41
    target 259
  ]
  edge [
    source 41
    target 2495
  ]
  edge [
    source 41
    target 1976
  ]
  edge [
    source 41
    target 2496
  ]
  edge [
    source 41
    target 1271
  ]
  edge [
    source 41
    target 2497
  ]
  edge [
    source 41
    target 2498
  ]
  edge [
    source 41
    target 1978
  ]
  edge [
    source 41
    target 2499
  ]
  edge [
    source 41
    target 261
  ]
  edge [
    source 41
    target 257
  ]
  edge [
    source 41
    target 425
  ]
  edge [
    source 41
    target 2500
  ]
  edge [
    source 41
    target 2501
  ]
  edge [
    source 41
    target 2502
  ]
  edge [
    source 41
    target 2503
  ]
  edge [
    source 41
    target 2504
  ]
  edge [
    source 41
    target 2505
  ]
  edge [
    source 41
    target 1983
  ]
  edge [
    source 41
    target 2506
  ]
  edge [
    source 41
    target 2507
  ]
  edge [
    source 41
    target 1986
  ]
  edge [
    source 41
    target 1987
  ]
  edge [
    source 41
    target 2508
  ]
  edge [
    source 41
    target 2509
  ]
  edge [
    source 41
    target 2403
  ]
  edge [
    source 41
    target 2510
  ]
  edge [
    source 41
    target 2511
  ]
  edge [
    source 41
    target 2512
  ]
  edge [
    source 41
    target 1988
  ]
  edge [
    source 41
    target 2513
  ]
  edge [
    source 41
    target 2514
  ]
  edge [
    source 41
    target 2515
  ]
  edge [
    source 41
    target 2516
  ]
  edge [
    source 41
    target 2517
  ]
  edge [
    source 41
    target 2518
  ]
  edge [
    source 41
    target 1226
  ]
  edge [
    source 41
    target 2519
  ]
  edge [
    source 41
    target 2520
  ]
  edge [
    source 41
    target 2521
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 2522
  ]
  edge [
    source 41
    target 2523
  ]
  edge [
    source 41
    target 2524
  ]
  edge [
    source 41
    target 1995
  ]
  edge [
    source 41
    target 168
  ]
  edge [
    source 41
    target 2525
  ]
  edge [
    source 41
    target 2526
  ]
  edge [
    source 41
    target 2527
  ]
  edge [
    source 41
    target 2528
  ]
  edge [
    source 41
    target 2529
  ]
  edge [
    source 41
    target 2530
  ]
  edge [
    source 41
    target 2531
  ]
  edge [
    source 41
    target 2532
  ]
  edge [
    source 41
    target 2099
  ]
  edge [
    source 41
    target 2533
  ]
  edge [
    source 41
    target 2534
  ]
  edge [
    source 41
    target 2535
  ]
  edge [
    source 41
    target 1398
  ]
  edge [
    source 41
    target 576
  ]
  edge [
    source 41
    target 2536
  ]
  edge [
    source 41
    target 2347
  ]
  edge [
    source 41
    target 2348
  ]
  edge [
    source 41
    target 1506
  ]
  edge [
    source 41
    target 1961
  ]
  edge [
    source 41
    target 2349
  ]
  edge [
    source 41
    target 1533
  ]
  edge [
    source 41
    target 502
  ]
  edge [
    source 41
    target 515
  ]
  edge [
    source 41
    target 82
  ]
  edge [
    source 41
    target 2537
  ]
  edge [
    source 41
    target 2538
  ]
  edge [
    source 41
    target 2539
  ]
  edge [
    source 41
    target 519
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 463
  ]
  edge [
    source 41
    target 2540
  ]
  edge [
    source 41
    target 2541
  ]
  edge [
    source 41
    target 1678
  ]
  edge [
    source 41
    target 526
  ]
  edge [
    source 41
    target 2542
  ]
  edge [
    source 41
    target 2543
  ]
  edge [
    source 41
    target 2544
  ]
  edge [
    source 41
    target 2545
  ]
  edge [
    source 41
    target 2546
  ]
  edge [
    source 41
    target 2547
  ]
  edge [
    source 41
    target 2548
  ]
  edge [
    source 41
    target 555
  ]
  edge [
    source 41
    target 2549
  ]
  edge [
    source 41
    target 2550
  ]
  edge [
    source 41
    target 1558
  ]
  edge [
    source 41
    target 2551
  ]
  edge [
    source 41
    target 2552
  ]
  edge [
    source 41
    target 2553
  ]
  edge [
    source 41
    target 2554
  ]
  edge [
    source 41
    target 1310
  ]
  edge [
    source 41
    target 2555
  ]
  edge [
    source 41
    target 2556
  ]
  edge [
    source 41
    target 2557
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 2558
  ]
  edge [
    source 41
    target 2559
  ]
  edge [
    source 41
    target 2560
  ]
  edge [
    source 41
    target 2561
  ]
  edge [
    source 41
    target 2367
  ]
  edge [
    source 41
    target 2562
  ]
  edge [
    source 41
    target 1320
  ]
  edge [
    source 41
    target 2563
  ]
  edge [
    source 41
    target 2564
  ]
  edge [
    source 41
    target 2565
  ]
  edge [
    source 41
    target 2566
  ]
  edge [
    source 41
    target 2567
  ]
  edge [
    source 41
    target 1415
  ]
  edge [
    source 41
    target 2568
  ]
  edge [
    source 41
    target 2569
  ]
  edge [
    source 41
    target 2570
  ]
  edge [
    source 41
    target 1860
  ]
  edge [
    source 41
    target 2571
  ]
  edge [
    source 41
    target 2572
  ]
  edge [
    source 41
    target 2573
  ]
  edge [
    source 41
    target 2574
  ]
  edge [
    source 41
    target 1205
  ]
  edge [
    source 41
    target 1382
  ]
  edge [
    source 41
    target 2575
  ]
  edge [
    source 41
    target 2576
  ]
  edge [
    source 41
    target 2577
  ]
  edge [
    source 41
    target 2578
  ]
  edge [
    source 41
    target 2579
  ]
  edge [
    source 41
    target 2580
  ]
  edge [
    source 41
    target 2581
  ]
  edge [
    source 41
    target 2582
  ]
  edge [
    source 41
    target 487
  ]
  edge [
    source 41
    target 2583
  ]
  edge [
    source 41
    target 2584
  ]
  edge [
    source 41
    target 2585
  ]
  edge [
    source 41
    target 2586
  ]
  edge [
    source 41
    target 2587
  ]
  edge [
    source 41
    target 2588
  ]
  edge [
    source 41
    target 1821
  ]
  edge [
    source 41
    target 1822
  ]
  edge [
    source 41
    target 1823
  ]
  edge [
    source 41
    target 1824
  ]
  edge [
    source 41
    target 1825
  ]
  edge [
    source 41
    target 1826
  ]
  edge [
    source 41
    target 1827
  ]
  edge [
    source 41
    target 1828
  ]
  edge [
    source 41
    target 1829
  ]
  edge [
    source 41
    target 1830
  ]
  edge [
    source 41
    target 1831
  ]
  edge [
    source 41
    target 1832
  ]
  edge [
    source 41
    target 1833
  ]
  edge [
    source 41
    target 1834
  ]
  edge [
    source 41
    target 1835
  ]
  edge [
    source 41
    target 1836
  ]
  edge [
    source 41
    target 1837
  ]
  edge [
    source 41
    target 1943
  ]
  edge [
    source 41
    target 2589
  ]
  edge [
    source 41
    target 2590
  ]
  edge [
    source 41
    target 2591
  ]
  edge [
    source 41
    target 2592
  ]
  edge [
    source 41
    target 2593
  ]
  edge [
    source 41
    target 2594
  ]
  edge [
    source 41
    target 2595
  ]
  edge [
    source 41
    target 2596
  ]
  edge [
    source 41
    target 2597
  ]
  edge [
    source 41
    target 2598
  ]
  edge [
    source 41
    target 2599
  ]
  edge [
    source 41
    target 1550
  ]
  edge [
    source 41
    target 2600
  ]
  edge [
    source 41
    target 2601
  ]
  edge [
    source 41
    target 2602
  ]
  edge [
    source 41
    target 281
  ]
  edge [
    source 41
    target 2603
  ]
  edge [
    source 41
    target 2604
  ]
  edge [
    source 41
    target 2605
  ]
  edge [
    source 41
    target 2606
  ]
  edge [
    source 41
    target 2607
  ]
  edge [
    source 41
    target 2608
  ]
  edge [
    source 41
    target 2609
  ]
  edge [
    source 41
    target 2610
  ]
  edge [
    source 41
    target 2611
  ]
  edge [
    source 41
    target 2612
  ]
  edge [
    source 41
    target 2613
  ]
  edge [
    source 41
    target 2614
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 2615
  ]
  edge [
    source 41
    target 2616
  ]
  edge [
    source 41
    target 2617
  ]
  edge [
    source 41
    target 1460
  ]
  edge [
    source 41
    target 1305
  ]
  edge [
    source 41
    target 165
  ]
  edge [
    source 41
    target 2618
  ]
  edge [
    source 41
    target 2619
  ]
  edge [
    source 41
    target 2020
  ]
  edge [
    source 41
    target 2620
  ]
  edge [
    source 41
    target 319
  ]
  edge [
    source 41
    target 2621
  ]
  edge [
    source 41
    target 2622
  ]
  edge [
    source 41
    target 433
  ]
  edge [
    source 41
    target 2623
  ]
  edge [
    source 41
    target 2624
  ]
  edge [
    source 41
    target 2625
  ]
  edge [
    source 41
    target 388
  ]
  edge [
    source 41
    target 2358
  ]
  edge [
    source 41
    target 2626
  ]
  edge [
    source 41
    target 2627
  ]
  edge [
    source 41
    target 383
  ]
  edge [
    source 41
    target 2628
  ]
  edge [
    source 41
    target 2629
  ]
  edge [
    source 41
    target 2630
  ]
  edge [
    source 41
    target 2631
  ]
  edge [
    source 41
    target 2632
  ]
  edge [
    source 41
    target 2633
  ]
  edge [
    source 41
    target 2634
  ]
  edge [
    source 41
    target 2635
  ]
  edge [
    source 41
    target 2636
  ]
  edge [
    source 41
    target 280
  ]
  edge [
    source 41
    target 1309
  ]
  edge [
    source 41
    target 2637
  ]
  edge [
    source 41
    target 2229
  ]
  edge [
    source 41
    target 2638
  ]
  edge [
    source 41
    target 2639
  ]
  edge [
    source 41
    target 2640
  ]
  edge [
    source 41
    target 2641
  ]
  edge [
    source 41
    target 2642
  ]
  edge [
    source 41
    target 2643
  ]
  edge [
    source 41
    target 2644
  ]
  edge [
    source 41
    target 2645
  ]
  edge [
    source 41
    target 2646
  ]
  edge [
    source 41
    target 2647
  ]
  edge [
    source 41
    target 2648
  ]
  edge [
    source 41
    target 2649
  ]
  edge [
    source 41
    target 1411
  ]
  edge [
    source 41
    target 2650
  ]
  edge [
    source 41
    target 2651
  ]
  edge [
    source 41
    target 2652
  ]
  edge [
    source 41
    target 2653
  ]
  edge [
    source 41
    target 1287
  ]
  edge [
    source 41
    target 2654
  ]
  edge [
    source 41
    target 2655
  ]
  edge [
    source 41
    target 145
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 246
  ]
  edge [
    source 41
    target 171
  ]
  edge [
    source 41
    target 247
  ]
  edge [
    source 41
    target 2656
  ]
  edge [
    source 41
    target 2657
  ]
  edge [
    source 41
    target 2658
  ]
  edge [
    source 41
    target 2659
  ]
  edge [
    source 41
    target 282
  ]
  edge [
    source 41
    target 2660
  ]
  edge [
    source 41
    target 2661
  ]
  edge [
    source 41
    target 513
  ]
  edge [
    source 41
    target 1224
  ]
  edge [
    source 41
    target 2662
  ]
  edge [
    source 41
    target 2663
  ]
  edge [
    source 41
    target 434
  ]
  edge [
    source 41
    target 2664
  ]
  edge [
    source 41
    target 2665
  ]
  edge [
    source 41
    target 2666
  ]
  edge [
    source 41
    target 2667
  ]
  edge [
    source 41
    target 2668
  ]
  edge [
    source 41
    target 2669
  ]
  edge [
    source 41
    target 2670
  ]
  edge [
    source 41
    target 2671
  ]
  edge [
    source 41
    target 2672
  ]
  edge [
    source 41
    target 1311
  ]
  edge [
    source 41
    target 2673
  ]
  edge [
    source 41
    target 2674
  ]
  edge [
    source 41
    target 2675
  ]
  edge [
    source 41
    target 2676
  ]
  edge [
    source 41
    target 2677
  ]
  edge [
    source 41
    target 2678
  ]
  edge [
    source 41
    target 391
  ]
  edge [
    source 41
    target 2679
  ]
  edge [
    source 41
    target 2680
  ]
  edge [
    source 41
    target 2681
  ]
  edge [
    source 41
    target 2682
  ]
  edge [
    source 41
    target 2683
  ]
  edge [
    source 41
    target 2684
  ]
  edge [
    source 41
    target 2685
  ]
  edge [
    source 41
    target 2686
  ]
  edge [
    source 41
    target 2687
  ]
  edge [
    source 41
    target 2688
  ]
  edge [
    source 41
    target 2689
  ]
  edge [
    source 41
    target 2690
  ]
  edge [
    source 41
    target 2691
  ]
  edge [
    source 41
    target 2692
  ]
  edge [
    source 41
    target 2693
  ]
  edge [
    source 41
    target 390
  ]
  edge [
    source 41
    target 1970
  ]
  edge [
    source 41
    target 1593
  ]
  edge [
    source 41
    target 539
  ]
  edge [
    source 41
    target 2694
  ]
  edge [
    source 41
    target 2695
  ]
  edge [
    source 41
    target 2696
  ]
  edge [
    source 41
    target 2697
  ]
  edge [
    source 41
    target 2698
  ]
  edge [
    source 41
    target 1583
  ]
  edge [
    source 41
    target 2699
  ]
  edge [
    source 41
    target 1589
  ]
  edge [
    source 41
    target 2700
  ]
  edge [
    source 41
    target 2701
  ]
  edge [
    source 41
    target 2702
  ]
  edge [
    source 41
    target 1268
  ]
  edge [
    source 42
    target 2703
  ]
  edge [
    source 42
    target 2704
  ]
  edge [
    source 42
    target 2705
  ]
  edge [
    source 42
    target 2706
  ]
  edge [
    source 42
    target 2707
  ]
  edge [
    source 42
    target 2708
  ]
  edge [
    source 42
    target 2709
  ]
  edge [
    source 42
    target 2710
  ]
  edge [
    source 42
    target 2711
  ]
  edge [
    source 42
    target 2712
  ]
  edge [
    source 42
    target 2713
  ]
  edge [
    source 42
    target 2714
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2715
  ]
  edge [
    source 43
    target 2716
  ]
  edge [
    source 43
    target 2244
  ]
  edge [
    source 43
    target 2717
  ]
  edge [
    source 43
    target 2718
  ]
  edge [
    source 43
    target 2719
  ]
  edge [
    source 43
    target 2720
  ]
  edge [
    source 43
    target 1898
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 2721
  ]
  edge [
    source 45
    target 255
  ]
  edge [
    source 45
    target 2722
  ]
  edge [
    source 45
    target 2723
  ]
  edge [
    source 45
    target 241
  ]
  edge [
    source 45
    target 2724
  ]
  edge [
    source 45
    target 2725
  ]
  edge [
    source 45
    target 2726
  ]
  edge [
    source 45
    target 2727
  ]
  edge [
    source 45
    target 2728
  ]
  edge [
    source 45
    target 2729
  ]
  edge [
    source 45
    target 2730
  ]
  edge [
    source 45
    target 2731
  ]
  edge [
    source 45
    target 2732
  ]
  edge [
    source 45
    target 2733
  ]
  edge [
    source 45
    target 2734
  ]
  edge [
    source 45
    target 425
  ]
  edge [
    source 45
    target 2735
  ]
  edge [
    source 45
    target 2736
  ]
  edge [
    source 45
    target 2737
  ]
  edge [
    source 45
    target 1445
  ]
  edge [
    source 45
    target 2738
  ]
  edge [
    source 45
    target 2739
  ]
  edge [
    source 45
    target 2740
  ]
  edge [
    source 45
    target 2741
  ]
  edge [
    source 45
    target 2742
  ]
  edge [
    source 45
    target 2743
  ]
  edge [
    source 45
    target 2744
  ]
  edge [
    source 45
    target 2745
  ]
  edge [
    source 45
    target 2746
  ]
  edge [
    source 45
    target 2747
  ]
  edge [
    source 45
    target 2748
  ]
  edge [
    source 45
    target 2749
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2750
  ]
  edge [
    source 46
    target 2751
  ]
  edge [
    source 46
    target 2752
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 423
  ]
  edge [
    source 47
    target 424
  ]
  edge [
    source 47
    target 425
  ]
  edge [
    source 47
    target 426
  ]
  edge [
    source 47
    target 427
  ]
  edge [
    source 47
    target 2359
  ]
  edge [
    source 47
    target 2753
  ]
  edge [
    source 47
    target 2754
  ]
  edge [
    source 47
    target 2755
  ]
  edge [
    source 47
    target 2756
  ]
  edge [
    source 47
    target 1621
  ]
  edge [
    source 47
    target 255
  ]
  edge [
    source 47
    target 2702
  ]
  edge [
    source 47
    target 2757
  ]
  edge [
    source 47
    target 82
  ]
  edge [
    source 47
    target 2758
  ]
  edge [
    source 47
    target 2759
  ]
  edge [
    source 47
    target 614
  ]
  edge [
    source 47
    target 576
  ]
  edge [
    source 47
    target 2760
  ]
  edge [
    source 47
    target 2761
  ]
  edge [
    source 47
    target 2762
  ]
  edge [
    source 47
    target 433
  ]
  edge [
    source 47
    target 300
  ]
  edge [
    source 47
    target 2763
  ]
  edge [
    source 47
    target 2764
  ]
  edge [
    source 47
    target 1533
  ]
  edge [
    source 47
    target 2765
  ]
  edge [
    source 47
    target 2766
  ]
  edge [
    source 47
    target 2767
  ]
  edge [
    source 47
    target 494
  ]
  edge [
    source 47
    target 327
  ]
  edge [
    source 47
    target 2030
  ]
  edge [
    source 47
    target 1743
  ]
  edge [
    source 47
    target 2768
  ]
  edge [
    source 47
    target 341
  ]
  edge [
    source 47
    target 2769
  ]
  edge [
    source 47
    target 485
  ]
  edge [
    source 47
    target 2770
  ]
  edge [
    source 47
    target 2771
  ]
  edge [
    source 47
    target 2772
  ]
  edge [
    source 47
    target 2773
  ]
  edge [
    source 47
    target 2774
  ]
  edge [
    source 47
    target 1320
  ]
  edge [
    source 47
    target 2775
  ]
  edge [
    source 47
    target 2776
  ]
  edge [
    source 47
    target 1246
  ]
  edge [
    source 47
    target 2777
  ]
  edge [
    source 47
    target 2778
  ]
  edge [
    source 47
    target 2779
  ]
  edge [
    source 47
    target 2780
  ]
  edge [
    source 47
    target 1415
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 2781
  ]
  edge [
    source 48
    target 2782
  ]
  edge [
    source 48
    target 2783
  ]
  edge [
    source 48
    target 2784
  ]
  edge [
    source 48
    target 2785
  ]
  edge [
    source 48
    target 2786
  ]
  edge [
    source 48
    target 2787
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 2788
  ]
  edge [
    source 49
    target 144
  ]
  edge [
    source 49
    target 193
  ]
  edge [
    source 49
    target 292
  ]
  edge [
    source 49
    target 241
  ]
  edge [
    source 49
    target 2789
  ]
  edge [
    source 49
    target 2790
  ]
  edge [
    source 49
    target 2791
  ]
  edge [
    source 49
    target 2792
  ]
  edge [
    source 49
    target 2793
  ]
  edge [
    source 49
    target 2794
  ]
  edge [
    source 49
    target 2795
  ]
  edge [
    source 49
    target 2796
  ]
  edge [
    source 49
    target 2797
  ]
  edge [
    source 49
    target 207
  ]
  edge [
    source 49
    target 2798
  ]
  edge [
    source 49
    target 2799
  ]
  edge [
    source 49
    target 2800
  ]
  edge [
    source 49
    target 2656
  ]
  edge [
    source 49
    target 2801
  ]
  edge [
    source 49
    target 1701
  ]
  edge [
    source 49
    target 2802
  ]
  edge [
    source 49
    target 2803
  ]
  edge [
    source 49
    target 2804
  ]
  edge [
    source 49
    target 2805
  ]
  edge [
    source 49
    target 2806
  ]
  edge [
    source 49
    target 2807
  ]
  edge [
    source 49
    target 2808
  ]
  edge [
    source 49
    target 2809
  ]
  edge [
    source 49
    target 2810
  ]
  edge [
    source 49
    target 1394
  ]
  edge [
    source 49
    target 2811
  ]
  edge [
    source 49
    target 2812
  ]
  edge [
    source 49
    target 1436
  ]
  edge [
    source 49
    target 175
  ]
  edge [
    source 49
    target 176
  ]
  edge [
    source 49
    target 177
  ]
  edge [
    source 49
    target 139
  ]
  edge [
    source 49
    target 178
  ]
  edge [
    source 49
    target 179
  ]
  edge [
    source 49
    target 180
  ]
  edge [
    source 49
    target 181
  ]
  edge [
    source 49
    target 182
  ]
  edge [
    source 49
    target 183
  ]
  edge [
    source 49
    target 184
  ]
  edge [
    source 49
    target 157
  ]
  edge [
    source 49
    target 185
  ]
  edge [
    source 49
    target 186
  ]
  edge [
    source 49
    target 187
  ]
  edge [
    source 49
    target 188
  ]
  edge [
    source 49
    target 189
  ]
  edge [
    source 49
    target 190
  ]
  edge [
    source 49
    target 191
  ]
  edge [
    source 49
    target 173
  ]
  edge [
    source 49
    target 192
  ]
  edge [
    source 49
    target 2813
  ]
  edge [
    source 49
    target 2814
  ]
  edge [
    source 49
    target 2815
  ]
  edge [
    source 49
    target 2816
  ]
  edge [
    source 49
    target 2817
  ]
  edge [
    source 49
    target 2818
  ]
  edge [
    source 49
    target 2819
  ]
  edge [
    source 49
    target 2820
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 2821
  ]
  edge [
    source 50
    target 267
  ]
  edge [
    source 50
    target 2822
  ]
  edge [
    source 50
    target 1564
  ]
  edge [
    source 50
    target 2422
  ]
  edge [
    source 50
    target 1398
  ]
  edge [
    source 50
    target 2823
  ]
  edge [
    source 50
    target 2824
  ]
  edge [
    source 50
    target 2825
  ]
  edge [
    source 50
    target 2826
  ]
  edge [
    source 50
    target 2462
  ]
  edge [
    source 50
    target 2827
  ]
  edge [
    source 50
    target 2475
  ]
  edge [
    source 50
    target 2828
  ]
  edge [
    source 50
    target 2829
  ]
  edge [
    source 50
    target 2830
  ]
  edge [
    source 50
    target 2831
  ]
  edge [
    source 50
    target 2832
  ]
  edge [
    source 50
    target 2833
  ]
  edge [
    source 50
    target 2834
  ]
  edge [
    source 50
    target 433
  ]
  edge [
    source 50
    target 1798
  ]
  edge [
    source 50
    target 189
  ]
  edge [
    source 50
    target 2835
  ]
  edge [
    source 50
    target 2568
  ]
  edge [
    source 50
    target 2836
  ]
  edge [
    source 50
    target 2837
  ]
  edge [
    source 50
    target 1358
  ]
  edge [
    source 50
    target 2838
  ]
  edge [
    source 50
    target 2839
  ]
  edge [
    source 50
    target 2840
  ]
  edge [
    source 50
    target 494
  ]
  edge [
    source 50
    target 2841
  ]
  edge [
    source 50
    target 2842
  ]
  edge [
    source 50
    target 122
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 2843
  ]
  edge [
    source 51
    target 2354
  ]
  edge [
    source 51
    target 2844
  ]
  edge [
    source 51
    target 2845
  ]
  edge [
    source 51
    target 2846
  ]
  edge [
    source 51
    target 2847
  ]
  edge [
    source 51
    target 2848
  ]
  edge [
    source 51
    target 2849
  ]
  edge [
    source 51
    target 2850
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1811
  ]
  edge [
    source 52
    target 2851
  ]
  edge [
    source 52
    target 1953
  ]
  edge [
    source 52
    target 1954
  ]
  edge [
    source 52
    target 494
  ]
  edge [
    source 52
    target 539
  ]
  edge [
    source 52
    target 1957
  ]
  edge [
    source 52
    target 2852
  ]
  edge [
    source 52
    target 1959
  ]
  edge [
    source 52
    target 1962
  ]
  edge [
    source 52
    target 2573
  ]
  edge [
    source 52
    target 2853
  ]
  edge [
    source 52
    target 1963
  ]
  edge [
    source 52
    target 1964
  ]
  edge [
    source 52
    target 1966
  ]
  edge [
    source 52
    target 1809
  ]
  edge [
    source 52
    target 1968
  ]
  edge [
    source 52
    target 1969
  ]
  edge [
    source 52
    target 1886
  ]
  edge [
    source 52
    target 2854
  ]
  edge [
    source 52
    target 1800
  ]
  edge [
    source 52
    target 1971
  ]
  edge [
    source 52
    target 1972
  ]
  edge [
    source 52
    target 2855
  ]
  edge [
    source 52
    target 425
  ]
  edge [
    source 52
    target 2856
  ]
  edge [
    source 52
    target 2857
  ]
  edge [
    source 52
    target 2858
  ]
  edge [
    source 52
    target 2859
  ]
  edge [
    source 52
    target 2860
  ]
  edge [
    source 52
    target 576
  ]
  edge [
    source 52
    target 2861
  ]
  edge [
    source 52
    target 2739
  ]
  edge [
    source 52
    target 2546
  ]
  edge [
    source 52
    target 2494
  ]
  edge [
    source 52
    target 1451
  ]
  edge [
    source 52
    target 463
  ]
  edge [
    source 52
    target 2540
  ]
  edge [
    source 52
    target 2541
  ]
  edge [
    source 52
    target 1678
  ]
  edge [
    source 52
    target 526
  ]
  edge [
    source 52
    target 2542
  ]
  edge [
    source 52
    target 82
  ]
  edge [
    source 52
    target 2543
  ]
  edge [
    source 52
    target 2544
  ]
  edge [
    source 52
    target 2545
  ]
  edge [
    source 52
    target 2547
  ]
  edge [
    source 52
    target 2548
  ]
  edge [
    source 52
    target 555
  ]
  edge [
    source 52
    target 2480
  ]
  edge [
    source 52
    target 2549
  ]
  edge [
    source 52
    target 2550
  ]
  edge [
    source 52
    target 1558
  ]
  edge [
    source 52
    target 1306
  ]
  edge [
    source 52
    target 2551
  ]
  edge [
    source 52
    target 2552
  ]
  edge [
    source 52
    target 2553
  ]
  edge [
    source 52
    target 2554
  ]
  edge [
    source 52
    target 2486
  ]
  edge [
    source 52
    target 1310
  ]
  edge [
    source 52
    target 2555
  ]
  edge [
    source 52
    target 2556
  ]
  edge [
    source 52
    target 2557
  ]
  edge [
    source 52
    target 482
  ]
  edge [
    source 52
    target 1714
  ]
  edge [
    source 52
    target 2558
  ]
  edge [
    source 52
    target 2559
  ]
  edge [
    source 52
    target 2490
  ]
  edge [
    source 52
    target 2560
  ]
  edge [
    source 52
    target 2561
  ]
  edge [
    source 52
    target 2367
  ]
  edge [
    source 52
    target 2562
  ]
  edge [
    source 52
    target 1320
  ]
  edge [
    source 52
    target 2563
  ]
  edge [
    source 52
    target 2564
  ]
  edge [
    source 52
    target 2565
  ]
  edge [
    source 52
    target 2566
  ]
  edge [
    source 52
    target 2567
  ]
  edge [
    source 52
    target 1415
  ]
  edge [
    source 52
    target 2862
  ]
  edge [
    source 52
    target 2863
  ]
  edge [
    source 52
    target 2864
  ]
  edge [
    source 52
    target 2865
  ]
  edge [
    source 52
    target 2866
  ]
  edge [
    source 52
    target 2867
  ]
  edge [
    source 52
    target 2868
  ]
  edge [
    source 52
    target 2869
  ]
  edge [
    source 52
    target 2870
  ]
  edge [
    source 52
    target 2871
  ]
  edge [
    source 52
    target 2872
  ]
  edge [
    source 52
    target 538
  ]
  edge [
    source 52
    target 2873
  ]
  edge [
    source 52
    target 2874
  ]
  edge [
    source 52
    target 2875
  ]
  edge [
    source 52
    target 2476
  ]
  edge [
    source 52
    target 2876
  ]
  edge [
    source 52
    target 2877
  ]
  edge [
    source 52
    target 2878
  ]
  edge [
    source 52
    target 2879
  ]
  edge [
    source 52
    target 2880
  ]
  edge [
    source 52
    target 2881
  ]
  edge [
    source 52
    target 2882
  ]
  edge [
    source 52
    target 2883
  ]
  edge [
    source 52
    target 2884
  ]
  edge [
    source 52
    target 525
  ]
  edge [
    source 52
    target 2885
  ]
  edge [
    source 52
    target 1965
  ]
  edge [
    source 52
    target 2886
  ]
  edge [
    source 52
    target 2887
  ]
  edge [
    source 52
    target 1564
  ]
  edge [
    source 52
    target 2888
  ]
  edge [
    source 52
    target 2889
  ]
  edge [
    source 52
    target 2890
  ]
  edge [
    source 52
    target 2891
  ]
  edge [
    source 52
    target 2892
  ]
  edge [
    source 52
    target 1424
  ]
  edge [
    source 52
    target 2893
  ]
  edge [
    source 52
    target 433
  ]
  edge [
    source 52
    target 2894
  ]
  edge [
    source 52
    target 2895
  ]
  edge [
    source 52
    target 1413
  ]
  edge [
    source 52
    target 1422
  ]
  edge [
    source 52
    target 2896
  ]
  edge [
    source 52
    target 2897
  ]
  edge [
    source 52
    target 2898
  ]
  edge [
    source 52
    target 1379
  ]
  edge [
    source 52
    target 2899
  ]
  edge [
    source 52
    target 2900
  ]
  edge [
    source 52
    target 2901
  ]
  edge [
    source 52
    target 2902
  ]
  edge [
    source 52
    target 2903
  ]
  edge [
    source 52
    target 2229
  ]
  edge [
    source 52
    target 2904
  ]
  edge [
    source 52
    target 2905
  ]
  edge [
    source 52
    target 2906
  ]
  edge [
    source 52
    target 1943
  ]
  edge [
    source 52
    target 2606
  ]
  edge [
    source 52
    target 2907
  ]
  edge [
    source 52
    target 2607
  ]
  edge [
    source 52
    target 2908
  ]
  edge [
    source 52
    target 2612
  ]
  edge [
    source 52
    target 2613
  ]
  edge [
    source 52
    target 2909
  ]
  edge [
    source 52
    target 338
  ]
  edge [
    source 52
    target 2910
  ]
  edge [
    source 52
    target 2911
  ]
  edge [
    source 52
    target 2912
  ]
  edge [
    source 52
    target 2913
  ]
  edge [
    source 52
    target 2914
  ]
  edge [
    source 52
    target 1460
  ]
  edge [
    source 52
    target 2618
  ]
  edge [
    source 52
    target 2915
  ]
  edge [
    source 52
    target 2916
  ]
  edge [
    source 52
    target 319
  ]
  edge [
    source 52
    target 2917
  ]
  edge [
    source 52
    target 2622
  ]
  edge [
    source 52
    target 2918
  ]
  edge [
    source 52
    target 2235
  ]
  edge [
    source 52
    target 2919
  ]
  edge [
    source 52
    target 2646
  ]
  edge [
    source 52
    target 2920
  ]
  edge [
    source 52
    target 2653
  ]
  edge [
    source 52
    target 2652
  ]
  edge [
    source 52
    target 2921
  ]
  edge [
    source 52
    target 2922
  ]
  edge [
    source 52
    target 2923
  ]
  edge [
    source 52
    target 1699
  ]
  edge [
    source 52
    target 2924
  ]
  edge [
    source 52
    target 2925
  ]
  edge [
    source 52
    target 2926
  ]
  edge [
    source 52
    target 1382
  ]
  edge [
    source 52
    target 2927
  ]
  edge [
    source 52
    target 1628
  ]
  edge [
    source 52
    target 2928
  ]
  edge [
    source 52
    target 1793
  ]
  edge [
    source 52
    target 2929
  ]
  edge [
    source 52
    target 2930
  ]
  edge [
    source 52
    target 2596
  ]
  edge [
    source 52
    target 2931
  ]
  edge [
    source 52
    target 2932
  ]
  edge [
    source 52
    target 2463
  ]
  edge [
    source 52
    target 2933
  ]
  edge [
    source 52
    target 513
  ]
  edge [
    source 52
    target 391
  ]
  edge [
    source 52
    target 2934
  ]
  edge [
    source 52
    target 2935
  ]
  edge [
    source 52
    target 2936
  ]
  edge [
    source 52
    target 383
  ]
  edge [
    source 53
    target 53
  ]
  edge [
    source 54
    target 2937
  ]
  edge [
    source 54
    target 2938
  ]
  edge [
    source 54
    target 2939
  ]
  edge [
    source 54
    target 2323
  ]
  edge [
    source 54
    target 2940
  ]
  edge [
    source 54
    target 2941
  ]
  edge [
    source 54
    target 2942
  ]
  edge [
    source 54
    target 2328
  ]
  edge [
    source 54
    target 2253
  ]
  edge [
    source 54
    target 2943
  ]
  edge [
    source 54
    target 2944
  ]
  edge [
    source 54
    target 2945
  ]
  edge [
    source 54
    target 266
  ]
  edge [
    source 54
    target 2946
  ]
  edge [
    source 54
    target 126
  ]
  edge [
    source 54
    target 2947
  ]
  edge [
    source 54
    target 2948
  ]
  edge [
    source 54
    target 130
  ]
  edge [
    source 54
    target 2949
  ]
  edge [
    source 54
    target 2950
  ]
  edge [
    source 54
    target 2951
  ]
  edge [
    source 54
    target 2952
  ]
  edge [
    source 54
    target 2953
  ]
  edge [
    source 54
    target 2954
  ]
  edge [
    source 54
    target 2955
  ]
  edge [
    source 54
    target 2956
  ]
]
