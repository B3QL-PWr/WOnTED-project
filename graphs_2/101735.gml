graph [
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "diariusz"
  ]
  node [
    id 4
    label "pami&#281;tnik"
  ]
  node [
    id 5
    label "journal"
  ]
  node [
    id 6
    label "gazeta"
  ]
  node [
    id 7
    label "spis"
  ]
  node [
    id 8
    label "ksi&#281;ga"
  ]
  node [
    id 9
    label "program_informacyjny"
  ]
  node [
    id 10
    label "sheet"
  ]
  node [
    id 11
    label "redakcja"
  ]
  node [
    id 12
    label "prasa"
  ]
  node [
    id 13
    label "tytu&#322;"
  ]
  node [
    id 14
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 15
    label "czasopismo"
  ]
  node [
    id 16
    label "Ewangelia"
  ]
  node [
    id 17
    label "dokument"
  ]
  node [
    id 18
    label "tome"
  ]
  node [
    id 19
    label "book"
  ]
  node [
    id 20
    label "rozdzia&#322;"
  ]
  node [
    id 21
    label "pismo"
  ]
  node [
    id 22
    label "notes"
  ]
  node [
    id 23
    label "utw&#243;r_epicki"
  ]
  node [
    id 24
    label "zapiski"
  ]
  node [
    id 25
    label "album"
  ]
  node [
    id 26
    label "pami&#261;tka"
  ]
  node [
    id 27
    label "raptularz"
  ]
  node [
    id 28
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 29
    label "catalog"
  ]
  node [
    id 30
    label "figurowa&#263;"
  ]
  node [
    id 31
    label "tekst"
  ]
  node [
    id 32
    label "pozycja"
  ]
  node [
    id 33
    label "zbi&#243;r"
  ]
  node [
    id 34
    label "akt"
  ]
  node [
    id 35
    label "wyliczanka"
  ]
  node [
    id 36
    label "sumariusz"
  ]
  node [
    id 37
    label "stock"
  ]
  node [
    id 38
    label "czynno&#347;&#263;"
  ]
  node [
    id 39
    label "marc&#243;wka"
  ]
  node [
    id 40
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 41
    label "charter"
  ]
  node [
    id 42
    label "Karta_Nauczyciela"
  ]
  node [
    id 43
    label "przej&#347;&#263;"
  ]
  node [
    id 44
    label "przej&#347;cie"
  ]
  node [
    id 45
    label "poj&#281;cie"
  ]
  node [
    id 46
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 47
    label "erotyka"
  ]
  node [
    id 48
    label "fragment"
  ]
  node [
    id 49
    label "podniecanie"
  ]
  node [
    id 50
    label "po&#380;ycie"
  ]
  node [
    id 51
    label "baraszki"
  ]
  node [
    id 52
    label "numer"
  ]
  node [
    id 53
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 54
    label "certificate"
  ]
  node [
    id 55
    label "ruch_frykcyjny"
  ]
  node [
    id 56
    label "wydarzenie"
  ]
  node [
    id 57
    label "ontologia"
  ]
  node [
    id 58
    label "wzw&#243;d"
  ]
  node [
    id 59
    label "scena"
  ]
  node [
    id 60
    label "seks"
  ]
  node [
    id 61
    label "pozycja_misjonarska"
  ]
  node [
    id 62
    label "rozmna&#380;anie"
  ]
  node [
    id 63
    label "arystotelizm"
  ]
  node [
    id 64
    label "zwyczaj"
  ]
  node [
    id 65
    label "urzeczywistnienie"
  ]
  node [
    id 66
    label "z&#322;&#261;czenie"
  ]
  node [
    id 67
    label "funkcja"
  ]
  node [
    id 68
    label "act"
  ]
  node [
    id 69
    label "imisja"
  ]
  node [
    id 70
    label "podniecenie"
  ]
  node [
    id 71
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 72
    label "podnieca&#263;"
  ]
  node [
    id 73
    label "fascyku&#322;"
  ]
  node [
    id 74
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 75
    label "nago&#347;&#263;"
  ]
  node [
    id 76
    label "gra_wst&#281;pna"
  ]
  node [
    id 77
    label "po&#380;&#261;danie"
  ]
  node [
    id 78
    label "podnieci&#263;"
  ]
  node [
    id 79
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 80
    label "na_pieska"
  ]
  node [
    id 81
    label "rozwi&#261;zanie"
  ]
  node [
    id 82
    label "zabory"
  ]
  node [
    id 83
    label "ci&#281;&#380;arna"
  ]
  node [
    id 84
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 85
    label "zmieni&#263;"
  ]
  node [
    id 86
    label "absorb"
  ]
  node [
    id 87
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 88
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 89
    label "przesta&#263;"
  ]
  node [
    id 90
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 91
    label "podlec"
  ]
  node [
    id 92
    label "die"
  ]
  node [
    id 93
    label "pique"
  ]
  node [
    id 94
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 95
    label "zacz&#261;&#263;"
  ]
  node [
    id 96
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 97
    label "przeby&#263;"
  ]
  node [
    id 98
    label "happen"
  ]
  node [
    id 99
    label "zaliczy&#263;"
  ]
  node [
    id 100
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 101
    label "pass"
  ]
  node [
    id 102
    label "dozna&#263;"
  ]
  node [
    id 103
    label "przerobi&#263;"
  ]
  node [
    id 104
    label "mienie"
  ]
  node [
    id 105
    label "min&#261;&#263;"
  ]
  node [
    id 106
    label "beat"
  ]
  node [
    id 107
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 108
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 109
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 110
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 111
    label "przewy&#380;szenie"
  ]
  node [
    id 112
    label "experience"
  ]
  node [
    id 113
    label "przemokni&#281;cie"
  ]
  node [
    id 114
    label "prze&#380;ycie"
  ]
  node [
    id 115
    label "wydeptywanie"
  ]
  node [
    id 116
    label "offense"
  ]
  node [
    id 117
    label "traversal"
  ]
  node [
    id 118
    label "trwanie"
  ]
  node [
    id 119
    label "przepojenie"
  ]
  node [
    id 120
    label "przedostanie_si&#281;"
  ]
  node [
    id 121
    label "mini&#281;cie"
  ]
  node [
    id 122
    label "przestanie"
  ]
  node [
    id 123
    label "stanie_si&#281;"
  ]
  node [
    id 124
    label "miejsce"
  ]
  node [
    id 125
    label "nas&#261;czenie"
  ]
  node [
    id 126
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 127
    label "przebycie"
  ]
  node [
    id 128
    label "wymienienie"
  ]
  node [
    id 129
    label "nasycenie_si&#281;"
  ]
  node [
    id 130
    label "strain"
  ]
  node [
    id 131
    label "wytyczenie"
  ]
  node [
    id 132
    label "przerobienie"
  ]
  node [
    id 133
    label "zdarzenie_si&#281;"
  ]
  node [
    id 134
    label "uznanie"
  ]
  node [
    id 135
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 136
    label "przepuszczenie"
  ]
  node [
    id 137
    label "dostanie_si&#281;"
  ]
  node [
    id 138
    label "nale&#380;enie"
  ]
  node [
    id 139
    label "odmienienie"
  ]
  node [
    id 140
    label "wydeptanie"
  ]
  node [
    id 141
    label "doznanie"
  ]
  node [
    id 142
    label "zaliczenie"
  ]
  node [
    id 143
    label "wstawka"
  ]
  node [
    id 144
    label "faza"
  ]
  node [
    id 145
    label "crack"
  ]
  node [
    id 146
    label "zacz&#281;cie"
  ]
  node [
    id 147
    label "odnaj&#281;cie"
  ]
  node [
    id 148
    label "naj&#281;cie"
  ]
  node [
    id 149
    label "Barb&#243;rka"
  ]
  node [
    id 150
    label "miesi&#261;c"
  ]
  node [
    id 151
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 152
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 153
    label "Sylwester"
  ]
  node [
    id 154
    label "tydzie&#324;"
  ]
  node [
    id 155
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 156
    label "rok"
  ]
  node [
    id 157
    label "miech"
  ]
  node [
    id 158
    label "kalendy"
  ]
  node [
    id 159
    label "czas"
  ]
  node [
    id 160
    label "g&#243;rnik"
  ]
  node [
    id 161
    label "comber"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
]
