graph [
  node [
    id 0
    label "momus"
    origin "text"
  ]
  node [
    id 1
    label "dodawa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "liczba"
    origin "text"
  ]
  node [
    id 3
    label "siebie"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 5
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "anglia"
    origin "text"
  ]
  node [
    id 7
    label "t&#322;umaczenie"
    origin "text"
  ]
  node [
    id 8
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 12
    label "angielski"
    origin "text"
  ]
  node [
    id 13
    label "wszyscy"
    origin "text"
  ]
  node [
    id 14
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 15
    label "poszczeg&#243;lny"
    origin "text"
  ]
  node [
    id 16
    label "kraj"
    origin "text"
  ]
  node [
    id 17
    label "bywa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "nawet"
    origin "text"
  ]
  node [
    id 19
    label "wysoki"
    origin "text"
  ]
  node [
    id 20
    label "dawa&#263;"
  ]
  node [
    id 21
    label "liczy&#263;"
  ]
  node [
    id 22
    label "bind"
  ]
  node [
    id 23
    label "suma"
  ]
  node [
    id 24
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 25
    label "nadawa&#263;"
  ]
  node [
    id 26
    label "assign"
  ]
  node [
    id 27
    label "gada&#263;"
  ]
  node [
    id 28
    label "give"
  ]
  node [
    id 29
    label "donosi&#263;"
  ]
  node [
    id 30
    label "rekomendowa&#263;"
  ]
  node [
    id 31
    label "za&#322;atwia&#263;"
  ]
  node [
    id 32
    label "obgadywa&#263;"
  ]
  node [
    id 33
    label "sprawia&#263;"
  ]
  node [
    id 34
    label "przesy&#322;a&#263;"
  ]
  node [
    id 35
    label "report"
  ]
  node [
    id 36
    label "dyskalkulia"
  ]
  node [
    id 37
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 38
    label "wynagrodzenie"
  ]
  node [
    id 39
    label "osi&#261;ga&#263;"
  ]
  node [
    id 40
    label "wymienia&#263;"
  ]
  node [
    id 41
    label "posiada&#263;"
  ]
  node [
    id 42
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 43
    label "wycenia&#263;"
  ]
  node [
    id 44
    label "bra&#263;"
  ]
  node [
    id 45
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 46
    label "mierzy&#263;"
  ]
  node [
    id 47
    label "rachowa&#263;"
  ]
  node [
    id 48
    label "count"
  ]
  node [
    id 49
    label "tell"
  ]
  node [
    id 50
    label "odlicza&#263;"
  ]
  node [
    id 51
    label "wyznacza&#263;"
  ]
  node [
    id 52
    label "admit"
  ]
  node [
    id 53
    label "policza&#263;"
  ]
  node [
    id 54
    label "okre&#347;la&#263;"
  ]
  node [
    id 55
    label "stara&#263;_si&#281;"
  ]
  node [
    id 56
    label "robi&#263;"
  ]
  node [
    id 57
    label "perform"
  ]
  node [
    id 58
    label "amend"
  ]
  node [
    id 59
    label "repair"
  ]
  node [
    id 60
    label "przekazywa&#263;"
  ]
  node [
    id 61
    label "dostarcza&#263;"
  ]
  node [
    id 62
    label "mie&#263;_miejsce"
  ]
  node [
    id 63
    label "&#322;adowa&#263;"
  ]
  node [
    id 64
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 65
    label "przeznacza&#263;"
  ]
  node [
    id 66
    label "surrender"
  ]
  node [
    id 67
    label "traktowa&#263;"
  ]
  node [
    id 68
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 69
    label "obiecywa&#263;"
  ]
  node [
    id 70
    label "odst&#281;powa&#263;"
  ]
  node [
    id 71
    label "tender"
  ]
  node [
    id 72
    label "rap"
  ]
  node [
    id 73
    label "umieszcza&#263;"
  ]
  node [
    id 74
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 75
    label "t&#322;uc"
  ]
  node [
    id 76
    label "powierza&#263;"
  ]
  node [
    id 77
    label "render"
  ]
  node [
    id 78
    label "wpiernicza&#263;"
  ]
  node [
    id 79
    label "exsert"
  ]
  node [
    id 80
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 81
    label "train"
  ]
  node [
    id 82
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 83
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 84
    label "p&#322;aci&#263;"
  ]
  node [
    id 85
    label "hold_out"
  ]
  node [
    id 86
    label "nalewa&#263;"
  ]
  node [
    id 87
    label "zezwala&#263;"
  ]
  node [
    id 88
    label "hold"
  ]
  node [
    id 89
    label "assembly"
  ]
  node [
    id 90
    label "pieni&#261;dze"
  ]
  node [
    id 91
    label "wynie&#347;&#263;"
  ]
  node [
    id 92
    label "ilo&#347;&#263;"
  ]
  node [
    id 93
    label "msza"
  ]
  node [
    id 94
    label "wynosi&#263;"
  ]
  node [
    id 95
    label "addytywny"
  ]
  node [
    id 96
    label "quota"
  ]
  node [
    id 97
    label "dodawanie"
  ]
  node [
    id 98
    label "sk&#322;adnik"
  ]
  node [
    id 99
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 100
    label "wynik"
  ]
  node [
    id 101
    label "kategoria"
  ]
  node [
    id 102
    label "pierwiastek"
  ]
  node [
    id 103
    label "rozmiar"
  ]
  node [
    id 104
    label "wyra&#380;enie"
  ]
  node [
    id 105
    label "poj&#281;cie"
  ]
  node [
    id 106
    label "number"
  ]
  node [
    id 107
    label "cecha"
  ]
  node [
    id 108
    label "kategoria_gramatyczna"
  ]
  node [
    id 109
    label "grupa"
  ]
  node [
    id 110
    label "kwadrat_magiczny"
  ]
  node [
    id 111
    label "koniugacja"
  ]
  node [
    id 112
    label "odm&#322;adzanie"
  ]
  node [
    id 113
    label "liga"
  ]
  node [
    id 114
    label "jednostka_systematyczna"
  ]
  node [
    id 115
    label "asymilowanie"
  ]
  node [
    id 116
    label "gromada"
  ]
  node [
    id 117
    label "asymilowa&#263;"
  ]
  node [
    id 118
    label "egzemplarz"
  ]
  node [
    id 119
    label "Entuzjastki"
  ]
  node [
    id 120
    label "zbi&#243;r"
  ]
  node [
    id 121
    label "kompozycja"
  ]
  node [
    id 122
    label "Terranie"
  ]
  node [
    id 123
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 124
    label "category"
  ]
  node [
    id 125
    label "pakiet_klimatyczny"
  ]
  node [
    id 126
    label "oddzia&#322;"
  ]
  node [
    id 127
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 128
    label "cz&#261;steczka"
  ]
  node [
    id 129
    label "stage_set"
  ]
  node [
    id 130
    label "type"
  ]
  node [
    id 131
    label "specgrupa"
  ]
  node [
    id 132
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 133
    label "&#346;wietliki"
  ]
  node [
    id 134
    label "odm&#322;odzenie"
  ]
  node [
    id 135
    label "Eurogrupa"
  ]
  node [
    id 136
    label "odm&#322;adza&#263;"
  ]
  node [
    id 137
    label "formacja_geologiczna"
  ]
  node [
    id 138
    label "harcerze_starsi"
  ]
  node [
    id 139
    label "wytw&#243;r"
  ]
  node [
    id 140
    label "teoria"
  ]
  node [
    id 141
    label "forma"
  ]
  node [
    id 142
    label "klasa"
  ]
  node [
    id 143
    label "charakterystyka"
  ]
  node [
    id 144
    label "m&#322;ot"
  ]
  node [
    id 145
    label "znak"
  ]
  node [
    id 146
    label "drzewo"
  ]
  node [
    id 147
    label "pr&#243;ba"
  ]
  node [
    id 148
    label "attribute"
  ]
  node [
    id 149
    label "marka"
  ]
  node [
    id 150
    label "pos&#322;uchanie"
  ]
  node [
    id 151
    label "skumanie"
  ]
  node [
    id 152
    label "orientacja"
  ]
  node [
    id 153
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 154
    label "clasp"
  ]
  node [
    id 155
    label "przem&#243;wienie"
  ]
  node [
    id 156
    label "zorientowanie"
  ]
  node [
    id 157
    label "warunek_lokalowy"
  ]
  node [
    id 158
    label "circumference"
  ]
  node [
    id 159
    label "odzie&#380;"
  ]
  node [
    id 160
    label "znaczenie"
  ]
  node [
    id 161
    label "dymensja"
  ]
  node [
    id 162
    label "fleksja"
  ]
  node [
    id 163
    label "coupling"
  ]
  node [
    id 164
    label "osoba"
  ]
  node [
    id 165
    label "tryb"
  ]
  node [
    id 166
    label "czas"
  ]
  node [
    id 167
    label "czasownik"
  ]
  node [
    id 168
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 169
    label "orz&#281;sek"
  ]
  node [
    id 170
    label "leksem"
  ]
  node [
    id 171
    label "sformu&#322;owanie"
  ]
  node [
    id 172
    label "zdarzenie_si&#281;"
  ]
  node [
    id 173
    label "poinformowanie"
  ]
  node [
    id 174
    label "wording"
  ]
  node [
    id 175
    label "oznaczenie"
  ]
  node [
    id 176
    label "znak_j&#281;zykowy"
  ]
  node [
    id 177
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 178
    label "ozdobnik"
  ]
  node [
    id 179
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 180
    label "grupa_imienna"
  ]
  node [
    id 181
    label "jednostka_leksykalna"
  ]
  node [
    id 182
    label "term"
  ]
  node [
    id 183
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 184
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 185
    label "ujawnienie"
  ]
  node [
    id 186
    label "affirmation"
  ]
  node [
    id 187
    label "zapisanie"
  ]
  node [
    id 188
    label "rzucenie"
  ]
  node [
    id 189
    label "substancja_chemiczna"
  ]
  node [
    id 190
    label "morfem"
  ]
  node [
    id 191
    label "root"
  ]
  node [
    id 192
    label "plon"
  ]
  node [
    id 193
    label "kojarzy&#263;"
  ]
  node [
    id 194
    label "d&#378;wi&#281;k"
  ]
  node [
    id 195
    label "impart"
  ]
  node [
    id 196
    label "reszta"
  ]
  node [
    id 197
    label "zapach"
  ]
  node [
    id 198
    label "wydawnictwo"
  ]
  node [
    id 199
    label "wiano"
  ]
  node [
    id 200
    label "produkcja"
  ]
  node [
    id 201
    label "wprowadza&#263;"
  ]
  node [
    id 202
    label "podawa&#263;"
  ]
  node [
    id 203
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 204
    label "ujawnia&#263;"
  ]
  node [
    id 205
    label "placard"
  ]
  node [
    id 206
    label "denuncjowa&#263;"
  ]
  node [
    id 207
    label "tajemnica"
  ]
  node [
    id 208
    label "panna_na_wydaniu"
  ]
  node [
    id 209
    label "wytwarza&#263;"
  ]
  node [
    id 210
    label "organizowa&#263;"
  ]
  node [
    id 211
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 212
    label "czyni&#263;"
  ]
  node [
    id 213
    label "stylizowa&#263;"
  ]
  node [
    id 214
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 215
    label "falowa&#263;"
  ]
  node [
    id 216
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 217
    label "peddle"
  ]
  node [
    id 218
    label "praca"
  ]
  node [
    id 219
    label "wydala&#263;"
  ]
  node [
    id 220
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 221
    label "tentegowa&#263;"
  ]
  node [
    id 222
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 223
    label "urz&#261;dza&#263;"
  ]
  node [
    id 224
    label "oszukiwa&#263;"
  ]
  node [
    id 225
    label "work"
  ]
  node [
    id 226
    label "ukazywa&#263;"
  ]
  node [
    id 227
    label "przerabia&#263;"
  ]
  node [
    id 228
    label "act"
  ]
  node [
    id 229
    label "post&#281;powa&#263;"
  ]
  node [
    id 230
    label "rynek"
  ]
  node [
    id 231
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 232
    label "wprawia&#263;"
  ]
  node [
    id 233
    label "zaczyna&#263;"
  ]
  node [
    id 234
    label "wpisywa&#263;"
  ]
  node [
    id 235
    label "wchodzi&#263;"
  ]
  node [
    id 236
    label "take"
  ]
  node [
    id 237
    label "zapoznawa&#263;"
  ]
  node [
    id 238
    label "powodowa&#263;"
  ]
  node [
    id 239
    label "inflict"
  ]
  node [
    id 240
    label "schodzi&#263;"
  ]
  node [
    id 241
    label "induct"
  ]
  node [
    id 242
    label "begin"
  ]
  node [
    id 243
    label "doprowadza&#263;"
  ]
  node [
    id 244
    label "create"
  ]
  node [
    id 245
    label "demaskator"
  ]
  node [
    id 246
    label "dostrzega&#263;"
  ]
  node [
    id 247
    label "objawia&#263;"
  ]
  node [
    id 248
    label "unwrap"
  ]
  node [
    id 249
    label "informowa&#263;"
  ]
  node [
    id 250
    label "indicate"
  ]
  node [
    id 251
    label "inform"
  ]
  node [
    id 252
    label "zaskakiwa&#263;"
  ]
  node [
    id 253
    label "cover"
  ]
  node [
    id 254
    label "rozumie&#263;"
  ]
  node [
    id 255
    label "swat"
  ]
  node [
    id 256
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 257
    label "relate"
  ]
  node [
    id 258
    label "wyznawa&#263;"
  ]
  node [
    id 259
    label "oddawa&#263;"
  ]
  node [
    id 260
    label "confide"
  ]
  node [
    id 261
    label "zleca&#263;"
  ]
  node [
    id 262
    label "ufa&#263;"
  ]
  node [
    id 263
    label "command"
  ]
  node [
    id 264
    label "grant"
  ]
  node [
    id 265
    label "tenis"
  ]
  node [
    id 266
    label "deal"
  ]
  node [
    id 267
    label "stawia&#263;"
  ]
  node [
    id 268
    label "rozgrywa&#263;"
  ]
  node [
    id 269
    label "kelner"
  ]
  node [
    id 270
    label "siatk&#243;wka"
  ]
  node [
    id 271
    label "jedzenie"
  ]
  node [
    id 272
    label "faszerowa&#263;"
  ]
  node [
    id 273
    label "introduce"
  ]
  node [
    id 274
    label "serwowa&#263;"
  ]
  node [
    id 275
    label "kwota"
  ]
  node [
    id 276
    label "wydanie"
  ]
  node [
    id 277
    label "remainder"
  ]
  node [
    id 278
    label "pozosta&#322;y"
  ]
  node [
    id 279
    label "wyda&#263;"
  ]
  node [
    id 280
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 281
    label "impreza"
  ]
  node [
    id 282
    label "realizacja"
  ]
  node [
    id 283
    label "tingel-tangel"
  ]
  node [
    id 284
    label "numer"
  ]
  node [
    id 285
    label "monta&#380;"
  ]
  node [
    id 286
    label "postprodukcja"
  ]
  node [
    id 287
    label "performance"
  ]
  node [
    id 288
    label "fabrication"
  ]
  node [
    id 289
    label "product"
  ]
  node [
    id 290
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 291
    label "uzysk"
  ]
  node [
    id 292
    label "rozw&#243;j"
  ]
  node [
    id 293
    label "odtworzenie"
  ]
  node [
    id 294
    label "dorobek"
  ]
  node [
    id 295
    label "kreacja"
  ]
  node [
    id 296
    label "trema"
  ]
  node [
    id 297
    label "creation"
  ]
  node [
    id 298
    label "kooperowa&#263;"
  ]
  node [
    id 299
    label "return"
  ]
  node [
    id 300
    label "metr"
  ]
  node [
    id 301
    label "rezultat"
  ]
  node [
    id 302
    label "naturalia"
  ]
  node [
    id 303
    label "wypaplanie"
  ]
  node [
    id 304
    label "enigmat"
  ]
  node [
    id 305
    label "wiedza"
  ]
  node [
    id 306
    label "spos&#243;b"
  ]
  node [
    id 307
    label "zachowanie"
  ]
  node [
    id 308
    label "zachowywanie"
  ]
  node [
    id 309
    label "secret"
  ]
  node [
    id 310
    label "obowi&#261;zek"
  ]
  node [
    id 311
    label "dyskrecja"
  ]
  node [
    id 312
    label "informacja"
  ]
  node [
    id 313
    label "rzecz"
  ]
  node [
    id 314
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 315
    label "taj&#324;"
  ]
  node [
    id 316
    label "zachowa&#263;"
  ]
  node [
    id 317
    label "zachowywa&#263;"
  ]
  node [
    id 318
    label "posa&#380;ek"
  ]
  node [
    id 319
    label "mienie"
  ]
  node [
    id 320
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 321
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 322
    label "debit"
  ]
  node [
    id 323
    label "redaktor"
  ]
  node [
    id 324
    label "druk"
  ]
  node [
    id 325
    label "publikacja"
  ]
  node [
    id 326
    label "redakcja"
  ]
  node [
    id 327
    label "szata_graficzna"
  ]
  node [
    id 328
    label "firma"
  ]
  node [
    id 329
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 330
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 331
    label "poster"
  ]
  node [
    id 332
    label "phone"
  ]
  node [
    id 333
    label "wpadni&#281;cie"
  ]
  node [
    id 334
    label "zjawisko"
  ]
  node [
    id 335
    label "intonacja"
  ]
  node [
    id 336
    label "wpa&#347;&#263;"
  ]
  node [
    id 337
    label "note"
  ]
  node [
    id 338
    label "onomatopeja"
  ]
  node [
    id 339
    label "modalizm"
  ]
  node [
    id 340
    label "nadlecenie"
  ]
  node [
    id 341
    label "sound"
  ]
  node [
    id 342
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 343
    label "wpada&#263;"
  ]
  node [
    id 344
    label "solmizacja"
  ]
  node [
    id 345
    label "seria"
  ]
  node [
    id 346
    label "dobiec"
  ]
  node [
    id 347
    label "transmiter"
  ]
  node [
    id 348
    label "heksachord"
  ]
  node [
    id 349
    label "akcent"
  ]
  node [
    id 350
    label "repetycja"
  ]
  node [
    id 351
    label "brzmienie"
  ]
  node [
    id 352
    label "wpadanie"
  ]
  node [
    id 353
    label "liczba_kwantowa"
  ]
  node [
    id 354
    label "kosmetyk"
  ]
  node [
    id 355
    label "ciasto"
  ]
  node [
    id 356
    label "aromat"
  ]
  node [
    id 357
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 358
    label "puff"
  ]
  node [
    id 359
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 360
    label "przyprawa"
  ]
  node [
    id 361
    label "upojno&#347;&#263;"
  ]
  node [
    id 362
    label "owiewanie"
  ]
  node [
    id 363
    label "smak"
  ]
  node [
    id 364
    label "explanation"
  ]
  node [
    id 365
    label "bronienie"
  ]
  node [
    id 366
    label "remark"
  ]
  node [
    id 367
    label "przek&#322;adanie"
  ]
  node [
    id 368
    label "zrozumia&#322;y"
  ]
  node [
    id 369
    label "robienie"
  ]
  node [
    id 370
    label "przekonywanie"
  ]
  node [
    id 371
    label "uzasadnianie"
  ]
  node [
    id 372
    label "rozwianie"
  ]
  node [
    id 373
    label "rozwiewanie"
  ]
  node [
    id 374
    label "tekst"
  ]
  node [
    id 375
    label "gossip"
  ]
  node [
    id 376
    label "przedstawianie"
  ]
  node [
    id 377
    label "rendition"
  ]
  node [
    id 378
    label "kr&#281;ty"
  ]
  node [
    id 379
    label "przedmiot"
  ]
  node [
    id 380
    label "bycie"
  ]
  node [
    id 381
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 382
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 383
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 384
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 385
    label "porobienie"
  ]
  node [
    id 386
    label "czynno&#347;&#263;"
  ]
  node [
    id 387
    label "tentegowanie"
  ]
  node [
    id 388
    label "s&#261;d"
  ]
  node [
    id 389
    label "niedost&#281;pny"
  ]
  node [
    id 390
    label "obstawanie"
  ]
  node [
    id 391
    label "adwokatowanie"
  ]
  node [
    id 392
    label "zdawanie"
  ]
  node [
    id 393
    label "walczenie"
  ]
  node [
    id 394
    label "zabezpieczenie"
  ]
  node [
    id 395
    label "parry"
  ]
  node [
    id 396
    label "guard_duty"
  ]
  node [
    id 397
    label "or&#281;dowanie"
  ]
  node [
    id 398
    label "granie"
  ]
  node [
    id 399
    label "oddzia&#322;ywanie"
  ]
  node [
    id 400
    label "sk&#322;anianie"
  ]
  node [
    id 401
    label "przekonywanie_si&#281;"
  ]
  node [
    id 402
    label "persuasion"
  ]
  node [
    id 403
    label "&#347;wiadczenie"
  ]
  node [
    id 404
    label "transformation"
  ]
  node [
    id 405
    label "preferowanie"
  ]
  node [
    id 406
    label "zmienianie"
  ]
  node [
    id 407
    label "ekranizowanie"
  ]
  node [
    id 408
    label "przesuwanie_si&#281;"
  ]
  node [
    id 409
    label "k&#322;adzenie"
  ]
  node [
    id 410
    label "przenoszenie"
  ]
  node [
    id 411
    label "prym"
  ]
  node [
    id 412
    label "translation"
  ]
  node [
    id 413
    label "wk&#322;adanie"
  ]
  node [
    id 414
    label "ekscerpcja"
  ]
  node [
    id 415
    label "j&#281;zykowo"
  ]
  node [
    id 416
    label "wypowied&#378;"
  ]
  node [
    id 417
    label "pomini&#281;cie"
  ]
  node [
    id 418
    label "dzie&#322;o"
  ]
  node [
    id 419
    label "preparacja"
  ]
  node [
    id 420
    label "odmianka"
  ]
  node [
    id 421
    label "opu&#347;ci&#263;"
  ]
  node [
    id 422
    label "koniektura"
  ]
  node [
    id 423
    label "pisa&#263;"
  ]
  node [
    id 424
    label "obelga"
  ]
  node [
    id 425
    label "dyskutowanie"
  ]
  node [
    id 426
    label "motivation"
  ]
  node [
    id 427
    label "teatr"
  ]
  node [
    id 428
    label "opisywanie"
  ]
  node [
    id 429
    label "representation"
  ]
  node [
    id 430
    label "obgadywanie"
  ]
  node [
    id 431
    label "zapoznawanie"
  ]
  node [
    id 432
    label "wyst&#281;powanie"
  ]
  node [
    id 433
    label "ukazywanie"
  ]
  node [
    id 434
    label "pokazywanie"
  ]
  node [
    id 435
    label "przedstawienie"
  ]
  node [
    id 436
    label "display"
  ]
  node [
    id 437
    label "podawanie"
  ]
  node [
    id 438
    label "demonstrowanie"
  ]
  node [
    id 439
    label "presentation"
  ]
  node [
    id 440
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 441
    label "artykulator"
  ]
  node [
    id 442
    label "kod"
  ]
  node [
    id 443
    label "kawa&#322;ek"
  ]
  node [
    id 444
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 445
    label "gramatyka"
  ]
  node [
    id 446
    label "stylik"
  ]
  node [
    id 447
    label "przet&#322;umaczenie"
  ]
  node [
    id 448
    label "formalizowanie"
  ]
  node [
    id 449
    label "ssa&#263;"
  ]
  node [
    id 450
    label "ssanie"
  ]
  node [
    id 451
    label "language"
  ]
  node [
    id 452
    label "liza&#263;"
  ]
  node [
    id 453
    label "napisa&#263;"
  ]
  node [
    id 454
    label "konsonantyzm"
  ]
  node [
    id 455
    label "wokalizm"
  ]
  node [
    id 456
    label "fonetyka"
  ]
  node [
    id 457
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 458
    label "jeniec"
  ]
  node [
    id 459
    label "but"
  ]
  node [
    id 460
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 461
    label "po_koroniarsku"
  ]
  node [
    id 462
    label "kultura_duchowa"
  ]
  node [
    id 463
    label "m&#243;wienie"
  ]
  node [
    id 464
    label "pype&#263;"
  ]
  node [
    id 465
    label "lizanie"
  ]
  node [
    id 466
    label "pismo"
  ]
  node [
    id 467
    label "formalizowa&#263;"
  ]
  node [
    id 468
    label "organ"
  ]
  node [
    id 469
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 470
    label "rozumienie"
  ]
  node [
    id 471
    label "makroglosja"
  ]
  node [
    id 472
    label "m&#243;wi&#263;"
  ]
  node [
    id 473
    label "jama_ustna"
  ]
  node [
    id 474
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 475
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 476
    label "natural_language"
  ]
  node [
    id 477
    label "s&#322;ownictwo"
  ]
  node [
    id 478
    label "urz&#261;dzenie"
  ]
  node [
    id 479
    label "prosty"
  ]
  node [
    id 480
    label "pojmowalny"
  ]
  node [
    id 481
    label "uzasadniony"
  ]
  node [
    id 482
    label "wyja&#347;nienie"
  ]
  node [
    id 483
    label "wyja&#347;nianie_si&#281;"
  ]
  node [
    id 484
    label "rozja&#347;nienie"
  ]
  node [
    id 485
    label "przeja&#347;nienie_si&#281;"
  ]
  node [
    id 486
    label "zrozumiale"
  ]
  node [
    id 487
    label "przeja&#347;nianie_si&#281;"
  ]
  node [
    id 488
    label "sensowny"
  ]
  node [
    id 489
    label "rozja&#347;nianie"
  ]
  node [
    id 490
    label "wyja&#347;nienie_si&#281;"
  ]
  node [
    id 491
    label "zakrzywiony"
  ]
  node [
    id 492
    label "niezrozumia&#322;y"
  ]
  node [
    id 493
    label "kr&#281;to"
  ]
  node [
    id 494
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 495
    label "rozrzucanie"
  ]
  node [
    id 496
    label "occupation"
  ]
  node [
    id 497
    label "mierzwienie"
  ]
  node [
    id 498
    label "usuwanie"
  ]
  node [
    id 499
    label "rozwiewanie_si&#281;"
  ]
  node [
    id 500
    label "usuni&#281;cie"
  ]
  node [
    id 501
    label "rozproszenie_si&#281;"
  ]
  node [
    id 502
    label "rozrzucenie"
  ]
  node [
    id 503
    label "rozwianie_si&#281;"
  ]
  node [
    id 504
    label "waste"
  ]
  node [
    id 505
    label "zmierzwienie"
  ]
  node [
    id 506
    label "&#347;wiatowo"
  ]
  node [
    id 507
    label "kulturalny"
  ]
  node [
    id 508
    label "generalny"
  ]
  node [
    id 509
    label "og&#243;lnie"
  ]
  node [
    id 510
    label "zwierzchni"
  ]
  node [
    id 511
    label "porz&#261;dny"
  ]
  node [
    id 512
    label "nadrz&#281;dny"
  ]
  node [
    id 513
    label "podstawowy"
  ]
  node [
    id 514
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 515
    label "zasadniczy"
  ]
  node [
    id 516
    label "generalnie"
  ]
  node [
    id 517
    label "wykszta&#322;cony"
  ]
  node [
    id 518
    label "stosowny"
  ]
  node [
    id 519
    label "elegancki"
  ]
  node [
    id 520
    label "kulturalnie"
  ]
  node [
    id 521
    label "dobrze_wychowany"
  ]
  node [
    id 522
    label "kulturny"
  ]
  node [
    id 523
    label "internationally"
  ]
  node [
    id 524
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 525
    label "equal"
  ]
  node [
    id 526
    label "trwa&#263;"
  ]
  node [
    id 527
    label "chodzi&#263;"
  ]
  node [
    id 528
    label "si&#281;ga&#263;"
  ]
  node [
    id 529
    label "stan"
  ]
  node [
    id 530
    label "obecno&#347;&#263;"
  ]
  node [
    id 531
    label "stand"
  ]
  node [
    id 532
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 533
    label "uczestniczy&#263;"
  ]
  node [
    id 534
    label "participate"
  ]
  node [
    id 535
    label "istnie&#263;"
  ]
  node [
    id 536
    label "pozostawa&#263;"
  ]
  node [
    id 537
    label "zostawa&#263;"
  ]
  node [
    id 538
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 539
    label "adhere"
  ]
  node [
    id 540
    label "compass"
  ]
  node [
    id 541
    label "korzysta&#263;"
  ]
  node [
    id 542
    label "appreciation"
  ]
  node [
    id 543
    label "dociera&#263;"
  ]
  node [
    id 544
    label "get"
  ]
  node [
    id 545
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 546
    label "u&#380;ywa&#263;"
  ]
  node [
    id 547
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 548
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 549
    label "being"
  ]
  node [
    id 550
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 551
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 552
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 553
    label "p&#322;ywa&#263;"
  ]
  node [
    id 554
    label "run"
  ]
  node [
    id 555
    label "bangla&#263;"
  ]
  node [
    id 556
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 557
    label "przebiega&#263;"
  ]
  node [
    id 558
    label "wk&#322;ada&#263;"
  ]
  node [
    id 559
    label "proceed"
  ]
  node [
    id 560
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 561
    label "carry"
  ]
  node [
    id 562
    label "dziama&#263;"
  ]
  node [
    id 563
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 564
    label "para"
  ]
  node [
    id 565
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 566
    label "str&#243;j"
  ]
  node [
    id 567
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 568
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 569
    label "krok"
  ]
  node [
    id 570
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 571
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 572
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 573
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 574
    label "continue"
  ]
  node [
    id 575
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 576
    label "Ohio"
  ]
  node [
    id 577
    label "wci&#281;cie"
  ]
  node [
    id 578
    label "Nowy_York"
  ]
  node [
    id 579
    label "warstwa"
  ]
  node [
    id 580
    label "samopoczucie"
  ]
  node [
    id 581
    label "Illinois"
  ]
  node [
    id 582
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 583
    label "state"
  ]
  node [
    id 584
    label "Jukatan"
  ]
  node [
    id 585
    label "Kalifornia"
  ]
  node [
    id 586
    label "Wirginia"
  ]
  node [
    id 587
    label "wektor"
  ]
  node [
    id 588
    label "Teksas"
  ]
  node [
    id 589
    label "Goa"
  ]
  node [
    id 590
    label "Waszyngton"
  ]
  node [
    id 591
    label "miejsce"
  ]
  node [
    id 592
    label "Massachusetts"
  ]
  node [
    id 593
    label "Alaska"
  ]
  node [
    id 594
    label "Arakan"
  ]
  node [
    id 595
    label "Hawaje"
  ]
  node [
    id 596
    label "Maryland"
  ]
  node [
    id 597
    label "punkt"
  ]
  node [
    id 598
    label "Michigan"
  ]
  node [
    id 599
    label "Arizona"
  ]
  node [
    id 600
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 601
    label "Georgia"
  ]
  node [
    id 602
    label "poziom"
  ]
  node [
    id 603
    label "Pensylwania"
  ]
  node [
    id 604
    label "shape"
  ]
  node [
    id 605
    label "Luizjana"
  ]
  node [
    id 606
    label "Nowy_Meksyk"
  ]
  node [
    id 607
    label "Alabama"
  ]
  node [
    id 608
    label "Kansas"
  ]
  node [
    id 609
    label "Oregon"
  ]
  node [
    id 610
    label "Floryda"
  ]
  node [
    id 611
    label "Oklahoma"
  ]
  node [
    id 612
    label "jednostka_administracyjna"
  ]
  node [
    id 613
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 614
    label "make"
  ]
  node [
    id 615
    label "determine"
  ]
  node [
    id 616
    label "przestawa&#263;"
  ]
  node [
    id 617
    label "&#380;y&#263;"
  ]
  node [
    id 618
    label "coating"
  ]
  node [
    id 619
    label "przebywa&#263;"
  ]
  node [
    id 620
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 621
    label "ko&#324;czy&#263;"
  ]
  node [
    id 622
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 623
    label "finish_up"
  ]
  node [
    id 624
    label "kawa&#322;"
  ]
  node [
    id 625
    label "plot"
  ]
  node [
    id 626
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 627
    label "utw&#243;r"
  ]
  node [
    id 628
    label "piece"
  ]
  node [
    id 629
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 630
    label "podp&#322;ywanie"
  ]
  node [
    id 631
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 632
    label "model"
  ]
  node [
    id 633
    label "narz&#281;dzie"
  ]
  node [
    id 634
    label "nature"
  ]
  node [
    id 635
    label "struktura"
  ]
  node [
    id 636
    label "code"
  ]
  node [
    id 637
    label "szyfrowanie"
  ]
  node [
    id 638
    label "ci&#261;g"
  ]
  node [
    id 639
    label "szablon"
  ]
  node [
    id 640
    label "&#380;o&#322;nierz"
  ]
  node [
    id 641
    label "internowanie"
  ]
  node [
    id 642
    label "ojczyc"
  ]
  node [
    id 643
    label "pojmaniec"
  ]
  node [
    id 644
    label "niewolnik"
  ]
  node [
    id 645
    label "internowa&#263;"
  ]
  node [
    id 646
    label "tkanka"
  ]
  node [
    id 647
    label "jednostka_organizacyjna"
  ]
  node [
    id 648
    label "budowa"
  ]
  node [
    id 649
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 650
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 651
    label "tw&#243;r"
  ]
  node [
    id 652
    label "organogeneza"
  ]
  node [
    id 653
    label "zesp&#243;&#322;"
  ]
  node [
    id 654
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 655
    label "struktura_anatomiczna"
  ]
  node [
    id 656
    label "uk&#322;ad"
  ]
  node [
    id 657
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 658
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 659
    label "Izba_Konsyliarska"
  ]
  node [
    id 660
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 661
    label "stomia"
  ]
  node [
    id 662
    label "dekortykacja"
  ]
  node [
    id 663
    label "okolica"
  ]
  node [
    id 664
    label "Komitet_Region&#243;w"
  ]
  node [
    id 665
    label "aparat_artykulacyjny"
  ]
  node [
    id 666
    label "zboczenie"
  ]
  node [
    id 667
    label "om&#243;wienie"
  ]
  node [
    id 668
    label "sponiewieranie"
  ]
  node [
    id 669
    label "discipline"
  ]
  node [
    id 670
    label "omawia&#263;"
  ]
  node [
    id 671
    label "kr&#261;&#380;enie"
  ]
  node [
    id 672
    label "tre&#347;&#263;"
  ]
  node [
    id 673
    label "sponiewiera&#263;"
  ]
  node [
    id 674
    label "element"
  ]
  node [
    id 675
    label "entity"
  ]
  node [
    id 676
    label "tematyka"
  ]
  node [
    id 677
    label "w&#261;tek"
  ]
  node [
    id 678
    label "charakter"
  ]
  node [
    id 679
    label "zbaczanie"
  ]
  node [
    id 680
    label "program_nauczania"
  ]
  node [
    id 681
    label "om&#243;wi&#263;"
  ]
  node [
    id 682
    label "omawianie"
  ]
  node [
    id 683
    label "thing"
  ]
  node [
    id 684
    label "kultura"
  ]
  node [
    id 685
    label "istota"
  ]
  node [
    id 686
    label "zbacza&#263;"
  ]
  node [
    id 687
    label "zboczy&#263;"
  ]
  node [
    id 688
    label "zapi&#281;tek"
  ]
  node [
    id 689
    label "sznurowad&#322;o"
  ]
  node [
    id 690
    label "rozbijarka"
  ]
  node [
    id 691
    label "podeszwa"
  ]
  node [
    id 692
    label "obcas"
  ]
  node [
    id 693
    label "wzuwanie"
  ]
  node [
    id 694
    label "wzu&#263;"
  ]
  node [
    id 695
    label "przyszwa"
  ]
  node [
    id 696
    label "raki"
  ]
  node [
    id 697
    label "cholewa"
  ]
  node [
    id 698
    label "cholewka"
  ]
  node [
    id 699
    label "zel&#243;wka"
  ]
  node [
    id 700
    label "obuwie"
  ]
  node [
    id 701
    label "napi&#281;tek"
  ]
  node [
    id 702
    label "wzucie"
  ]
  node [
    id 703
    label "kom&#243;rka"
  ]
  node [
    id 704
    label "furnishing"
  ]
  node [
    id 705
    label "zrobienie"
  ]
  node [
    id 706
    label "wyrz&#261;dzenie"
  ]
  node [
    id 707
    label "zagospodarowanie"
  ]
  node [
    id 708
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 709
    label "ig&#322;a"
  ]
  node [
    id 710
    label "wirnik"
  ]
  node [
    id 711
    label "aparatura"
  ]
  node [
    id 712
    label "system_energetyczny"
  ]
  node [
    id 713
    label "impulsator"
  ]
  node [
    id 714
    label "mechanizm"
  ]
  node [
    id 715
    label "sprz&#281;t"
  ]
  node [
    id 716
    label "blokowanie"
  ]
  node [
    id 717
    label "set"
  ]
  node [
    id 718
    label "zablokowanie"
  ]
  node [
    id 719
    label "przygotowanie"
  ]
  node [
    id 720
    label "komora"
  ]
  node [
    id 721
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 722
    label "public_speaking"
  ]
  node [
    id 723
    label "powiadanie"
  ]
  node [
    id 724
    label "przepowiadanie"
  ]
  node [
    id 725
    label "wyznawanie"
  ]
  node [
    id 726
    label "wypowiadanie"
  ]
  node [
    id 727
    label "wydobywanie"
  ]
  node [
    id 728
    label "gaworzenie"
  ]
  node [
    id 729
    label "stosowanie"
  ]
  node [
    id 730
    label "wyra&#380;anie"
  ]
  node [
    id 731
    label "formu&#322;owanie"
  ]
  node [
    id 732
    label "dowalenie"
  ]
  node [
    id 733
    label "przerywanie"
  ]
  node [
    id 734
    label "wydawanie"
  ]
  node [
    id 735
    label "dogadywanie_si&#281;"
  ]
  node [
    id 736
    label "prawienie"
  ]
  node [
    id 737
    label "opowiadanie"
  ]
  node [
    id 738
    label "ozywanie_si&#281;"
  ]
  node [
    id 739
    label "zapeszanie"
  ]
  node [
    id 740
    label "zwracanie_si&#281;"
  ]
  node [
    id 741
    label "dysfonia"
  ]
  node [
    id 742
    label "speaking"
  ]
  node [
    id 743
    label "zauwa&#380;enie"
  ]
  node [
    id 744
    label "mawianie"
  ]
  node [
    id 745
    label "opowiedzenie"
  ]
  node [
    id 746
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 747
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 748
    label "informowanie"
  ]
  node [
    id 749
    label "dogadanie_si&#281;"
  ]
  node [
    id 750
    label "wygadanie"
  ]
  node [
    id 751
    label "terminology"
  ]
  node [
    id 752
    label "termin"
  ]
  node [
    id 753
    label "g&#322;osownia"
  ]
  node [
    id 754
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 755
    label "zasymilowa&#263;"
  ]
  node [
    id 756
    label "phonetics"
  ]
  node [
    id 757
    label "palatogram"
  ]
  node [
    id 758
    label "transkrypcja"
  ]
  node [
    id 759
    label "zasymilowanie"
  ]
  node [
    id 760
    label "psychotest"
  ]
  node [
    id 761
    label "wk&#322;ad"
  ]
  node [
    id 762
    label "handwriting"
  ]
  node [
    id 763
    label "przekaz"
  ]
  node [
    id 764
    label "paleograf"
  ]
  node [
    id 765
    label "interpunkcja"
  ]
  node [
    id 766
    label "dzia&#322;"
  ]
  node [
    id 767
    label "grafia"
  ]
  node [
    id 768
    label "communication"
  ]
  node [
    id 769
    label "script"
  ]
  node [
    id 770
    label "zajawka"
  ]
  node [
    id 771
    label "list"
  ]
  node [
    id 772
    label "adres"
  ]
  node [
    id 773
    label "Zwrotnica"
  ]
  node [
    id 774
    label "czasopismo"
  ]
  node [
    id 775
    label "ok&#322;adka"
  ]
  node [
    id 776
    label "ortografia"
  ]
  node [
    id 777
    label "letter"
  ]
  node [
    id 778
    label "komunikacja"
  ]
  node [
    id 779
    label "paleografia"
  ]
  node [
    id 780
    label "dokument"
  ]
  node [
    id 781
    label "prasa"
  ]
  node [
    id 782
    label "sk&#322;adnia"
  ]
  node [
    id 783
    label "morfologia"
  ]
  node [
    id 784
    label "styl"
  ]
  node [
    id 785
    label "stworzy&#263;"
  ]
  node [
    id 786
    label "read"
  ]
  node [
    id 787
    label "postawi&#263;"
  ]
  node [
    id 788
    label "write"
  ]
  node [
    id 789
    label "donie&#347;&#263;"
  ]
  node [
    id 790
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 791
    label "formu&#322;owa&#263;"
  ]
  node [
    id 792
    label "ozdabia&#263;"
  ]
  node [
    id 793
    label "spell"
  ]
  node [
    id 794
    label "skryba"
  ]
  node [
    id 795
    label "dysgrafia"
  ]
  node [
    id 796
    label "dysortografia"
  ]
  node [
    id 797
    label "tworzy&#263;"
  ]
  node [
    id 798
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 799
    label "poja&#347;nia&#263;"
  ]
  node [
    id 800
    label "u&#322;atwia&#263;"
  ]
  node [
    id 801
    label "elaborate"
  ]
  node [
    id 802
    label "suplikowa&#263;"
  ]
  node [
    id 803
    label "przek&#322;ada&#263;"
  ]
  node [
    id 804
    label "przekonywa&#263;"
  ]
  node [
    id 805
    label "interpretowa&#263;"
  ]
  node [
    id 806
    label "broni&#263;"
  ]
  node [
    id 807
    label "explain"
  ]
  node [
    id 808
    label "przedstawia&#263;"
  ]
  node [
    id 809
    label "sprawowa&#263;"
  ]
  node [
    id 810
    label "uzasadnia&#263;"
  ]
  node [
    id 811
    label "zinterpretowa&#263;"
  ]
  node [
    id 812
    label "put"
  ]
  node [
    id 813
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 814
    label "zrobi&#263;"
  ]
  node [
    id 815
    label "przekona&#263;"
  ]
  node [
    id 816
    label "frame"
  ]
  node [
    id 817
    label "wiedzie&#263;"
  ]
  node [
    id 818
    label "kuma&#263;"
  ]
  node [
    id 819
    label "czu&#263;"
  ]
  node [
    id 820
    label "match"
  ]
  node [
    id 821
    label "empatia"
  ]
  node [
    id 822
    label "odbiera&#263;"
  ]
  node [
    id 823
    label "see"
  ]
  node [
    id 824
    label "zna&#263;"
  ]
  node [
    id 825
    label "gaworzy&#263;"
  ]
  node [
    id 826
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 827
    label "rozmawia&#263;"
  ]
  node [
    id 828
    label "wyra&#380;a&#263;"
  ]
  node [
    id 829
    label "umie&#263;"
  ]
  node [
    id 830
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 831
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 832
    label "express"
  ]
  node [
    id 833
    label "talk"
  ]
  node [
    id 834
    label "prawi&#263;"
  ]
  node [
    id 835
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 836
    label "powiada&#263;"
  ]
  node [
    id 837
    label "chew_the_fat"
  ]
  node [
    id 838
    label "say"
  ]
  node [
    id 839
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 840
    label "wydobywa&#263;"
  ]
  node [
    id 841
    label "hermeneutyka"
  ]
  node [
    id 842
    label "kontekst"
  ]
  node [
    id 843
    label "apprehension"
  ]
  node [
    id 844
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 845
    label "interpretation"
  ]
  node [
    id 846
    label "obja&#347;nienie"
  ]
  node [
    id 847
    label "czucie"
  ]
  node [
    id 848
    label "realization"
  ]
  node [
    id 849
    label "kumanie"
  ]
  node [
    id 850
    label "wnioskowanie"
  ]
  node [
    id 851
    label "nadawanie"
  ]
  node [
    id 852
    label "precyzowanie"
  ]
  node [
    id 853
    label "formalny"
  ]
  node [
    id 854
    label "validate"
  ]
  node [
    id 855
    label "precyzowa&#263;"
  ]
  node [
    id 856
    label "salt_lick"
  ]
  node [
    id 857
    label "dotyka&#263;"
  ]
  node [
    id 858
    label "muska&#263;"
  ]
  node [
    id 859
    label "wada_wrodzona"
  ]
  node [
    id 860
    label "dotykanie"
  ]
  node [
    id 861
    label "przesuwanie"
  ]
  node [
    id 862
    label "zlizanie"
  ]
  node [
    id 863
    label "g&#322;askanie"
  ]
  node [
    id 864
    label "wylizywanie"
  ]
  node [
    id 865
    label "zlizywanie"
  ]
  node [
    id 866
    label "wylizanie"
  ]
  node [
    id 867
    label "usta"
  ]
  node [
    id 868
    label "&#347;lina"
  ]
  node [
    id 869
    label "pi&#263;"
  ]
  node [
    id 870
    label "sponge"
  ]
  node [
    id 871
    label "mleko"
  ]
  node [
    id 872
    label "rozpuszcza&#263;"
  ]
  node [
    id 873
    label "wci&#261;ga&#263;"
  ]
  node [
    id 874
    label "rusza&#263;"
  ]
  node [
    id 875
    label "sucking"
  ]
  node [
    id 876
    label "smoczek"
  ]
  node [
    id 877
    label "znami&#281;"
  ]
  node [
    id 878
    label "krosta"
  ]
  node [
    id 879
    label "spot"
  ]
  node [
    id 880
    label "schorzenie"
  ]
  node [
    id 881
    label "brodawka"
  ]
  node [
    id 882
    label "pip"
  ]
  node [
    id 883
    label "picie"
  ]
  node [
    id 884
    label "ruszanie"
  ]
  node [
    id 885
    label "consumption"
  ]
  node [
    id 886
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 887
    label "rozpuszczanie"
  ]
  node [
    id 888
    label "aspiration"
  ]
  node [
    id 889
    label "wci&#261;ganie"
  ]
  node [
    id 890
    label "odci&#261;ganie"
  ]
  node [
    id 891
    label "wessanie"
  ]
  node [
    id 892
    label "ga&#378;nik"
  ]
  node [
    id 893
    label "wysysanie"
  ]
  node [
    id 894
    label "wyssanie"
  ]
  node [
    id 895
    label "angol"
  ]
  node [
    id 896
    label "po_angielsku"
  ]
  node [
    id 897
    label "English"
  ]
  node [
    id 898
    label "anglicki"
  ]
  node [
    id 899
    label "angielsko"
  ]
  node [
    id 900
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 901
    label "brytyjski"
  ]
  node [
    id 902
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 903
    label "europejsko"
  ]
  node [
    id 904
    label "morris"
  ]
  node [
    id 905
    label "j&#281;zyk_angielski"
  ]
  node [
    id 906
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 907
    label "anglosaski"
  ]
  node [
    id 908
    label "brytyjsko"
  ]
  node [
    id 909
    label "europejski"
  ]
  node [
    id 910
    label "zachodnioeuropejski"
  ]
  node [
    id 911
    label "po_brytyjsku"
  ]
  node [
    id 912
    label "j&#281;zyk_martwy"
  ]
  node [
    id 913
    label "Stary_&#346;wiat"
  ]
  node [
    id 914
    label "asymilowanie_si&#281;"
  ]
  node [
    id 915
    label "p&#243;&#322;noc"
  ]
  node [
    id 916
    label "Wsch&#243;d"
  ]
  node [
    id 917
    label "class"
  ]
  node [
    id 918
    label "geosfera"
  ]
  node [
    id 919
    label "obiekt_naturalny"
  ]
  node [
    id 920
    label "przejmowanie"
  ]
  node [
    id 921
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 922
    label "przyroda"
  ]
  node [
    id 923
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 924
    label "po&#322;udnie"
  ]
  node [
    id 925
    label "makrokosmos"
  ]
  node [
    id 926
    label "huczek"
  ]
  node [
    id 927
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 928
    label "environment"
  ]
  node [
    id 929
    label "morze"
  ]
  node [
    id 930
    label "rze&#378;ba"
  ]
  node [
    id 931
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 932
    label "przejmowa&#263;"
  ]
  node [
    id 933
    label "hydrosfera"
  ]
  node [
    id 934
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 935
    label "ciemna_materia"
  ]
  node [
    id 936
    label "ekosystem"
  ]
  node [
    id 937
    label "biota"
  ]
  node [
    id 938
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 939
    label "planeta"
  ]
  node [
    id 940
    label "geotermia"
  ]
  node [
    id 941
    label "ekosfera"
  ]
  node [
    id 942
    label "ozonosfera"
  ]
  node [
    id 943
    label "wszechstworzenie"
  ]
  node [
    id 944
    label "woda"
  ]
  node [
    id 945
    label "kuchnia"
  ]
  node [
    id 946
    label "biosfera"
  ]
  node [
    id 947
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 948
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 949
    label "populace"
  ]
  node [
    id 950
    label "magnetosfera"
  ]
  node [
    id 951
    label "Nowy_&#346;wiat"
  ]
  node [
    id 952
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 953
    label "universe"
  ]
  node [
    id 954
    label "biegun"
  ]
  node [
    id 955
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 956
    label "litosfera"
  ]
  node [
    id 957
    label "teren"
  ]
  node [
    id 958
    label "mikrokosmos"
  ]
  node [
    id 959
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 960
    label "przestrze&#324;"
  ]
  node [
    id 961
    label "stw&#243;r"
  ]
  node [
    id 962
    label "p&#243;&#322;kula"
  ]
  node [
    id 963
    label "przej&#281;cie"
  ]
  node [
    id 964
    label "barysfera"
  ]
  node [
    id 965
    label "obszar"
  ]
  node [
    id 966
    label "czarna_dziura"
  ]
  node [
    id 967
    label "atmosfera"
  ]
  node [
    id 968
    label "przej&#261;&#263;"
  ]
  node [
    id 969
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 970
    label "Ziemia"
  ]
  node [
    id 971
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 972
    label "geoida"
  ]
  node [
    id 973
    label "zagranica"
  ]
  node [
    id 974
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 975
    label "fauna"
  ]
  node [
    id 976
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 977
    label "Kosowo"
  ]
  node [
    id 978
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 979
    label "Zab&#322;ocie"
  ]
  node [
    id 980
    label "zach&#243;d"
  ]
  node [
    id 981
    label "Pow&#261;zki"
  ]
  node [
    id 982
    label "Piotrowo"
  ]
  node [
    id 983
    label "Olszanica"
  ]
  node [
    id 984
    label "holarktyka"
  ]
  node [
    id 985
    label "Ruda_Pabianicka"
  ]
  node [
    id 986
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 987
    label "Ludwin&#243;w"
  ]
  node [
    id 988
    label "Arktyka"
  ]
  node [
    id 989
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 990
    label "Zabu&#380;e"
  ]
  node [
    id 991
    label "antroposfera"
  ]
  node [
    id 992
    label "terytorium"
  ]
  node [
    id 993
    label "Neogea"
  ]
  node [
    id 994
    label "Syberia_Zachodnia"
  ]
  node [
    id 995
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 996
    label "zakres"
  ]
  node [
    id 997
    label "pas_planetoid"
  ]
  node [
    id 998
    label "Syberia_Wschodnia"
  ]
  node [
    id 999
    label "Antarktyka"
  ]
  node [
    id 1000
    label "Rakowice"
  ]
  node [
    id 1001
    label "akrecja"
  ]
  node [
    id 1002
    label "wymiar"
  ]
  node [
    id 1003
    label "&#321;&#281;g"
  ]
  node [
    id 1004
    label "Kresy_Zachodnie"
  ]
  node [
    id 1005
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1006
    label "wsch&#243;d"
  ]
  node [
    id 1007
    label "Notogea"
  ]
  node [
    id 1008
    label "integer"
  ]
  node [
    id 1009
    label "zlewanie_si&#281;"
  ]
  node [
    id 1010
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1011
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1012
    label "pe&#322;ny"
  ]
  node [
    id 1013
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1014
    label "proces"
  ]
  node [
    id 1015
    label "boski"
  ]
  node [
    id 1016
    label "krajobraz"
  ]
  node [
    id 1017
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1018
    label "przywidzenie"
  ]
  node [
    id 1019
    label "presence"
  ]
  node [
    id 1020
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1021
    label "rozdzielanie"
  ]
  node [
    id 1022
    label "bezbrze&#380;e"
  ]
  node [
    id 1023
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1024
    label "niezmierzony"
  ]
  node [
    id 1025
    label "przedzielenie"
  ]
  node [
    id 1026
    label "nielito&#347;ciwy"
  ]
  node [
    id 1027
    label "rozdziela&#263;"
  ]
  node [
    id 1028
    label "oktant"
  ]
  node [
    id 1029
    label "przedzieli&#263;"
  ]
  node [
    id 1030
    label "przestw&#243;r"
  ]
  node [
    id 1031
    label "&#347;rodowisko"
  ]
  node [
    id 1032
    label "rura"
  ]
  node [
    id 1033
    label "grzebiuszka"
  ]
  node [
    id 1034
    label "cz&#322;owiek"
  ]
  node [
    id 1035
    label "odbicie"
  ]
  node [
    id 1036
    label "atom"
  ]
  node [
    id 1037
    label "kosmos"
  ]
  node [
    id 1038
    label "miniatura"
  ]
  node [
    id 1039
    label "smok_wawelski"
  ]
  node [
    id 1040
    label "niecz&#322;owiek"
  ]
  node [
    id 1041
    label "monster"
  ]
  node [
    id 1042
    label "istota_&#380;ywa"
  ]
  node [
    id 1043
    label "potw&#243;r"
  ]
  node [
    id 1044
    label "istota_fantastyczna"
  ]
  node [
    id 1045
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 1046
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1047
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 1048
    label "aspekt"
  ]
  node [
    id 1049
    label "troposfera"
  ]
  node [
    id 1050
    label "klimat"
  ]
  node [
    id 1051
    label "metasfera"
  ]
  node [
    id 1052
    label "atmosferyki"
  ]
  node [
    id 1053
    label "homosfera"
  ]
  node [
    id 1054
    label "powietrznia"
  ]
  node [
    id 1055
    label "jonosfera"
  ]
  node [
    id 1056
    label "termosfera"
  ]
  node [
    id 1057
    label "egzosfera"
  ]
  node [
    id 1058
    label "heterosfera"
  ]
  node [
    id 1059
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 1060
    label "tropopauza"
  ]
  node [
    id 1061
    label "kwas"
  ]
  node [
    id 1062
    label "powietrze"
  ]
  node [
    id 1063
    label "stratosfera"
  ]
  node [
    id 1064
    label "pow&#322;oka"
  ]
  node [
    id 1065
    label "mezosfera"
  ]
  node [
    id 1066
    label "mezopauza"
  ]
  node [
    id 1067
    label "atmosphere"
  ]
  node [
    id 1068
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1069
    label "ciep&#322;o"
  ]
  node [
    id 1070
    label "energia_termiczna"
  ]
  node [
    id 1071
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 1072
    label "sferoida"
  ]
  node [
    id 1073
    label "object"
  ]
  node [
    id 1074
    label "temat"
  ]
  node [
    id 1075
    label "obiekt"
  ]
  node [
    id 1076
    label "wra&#380;enie"
  ]
  node [
    id 1077
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 1078
    label "interception"
  ]
  node [
    id 1079
    label "wzbudzenie"
  ]
  node [
    id 1080
    label "emotion"
  ]
  node [
    id 1081
    label "movement"
  ]
  node [
    id 1082
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1083
    label "wzi&#281;cie"
  ]
  node [
    id 1084
    label "bang"
  ]
  node [
    id 1085
    label "wzi&#261;&#263;"
  ]
  node [
    id 1086
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1087
    label "stimulate"
  ]
  node [
    id 1088
    label "ogarn&#261;&#263;"
  ]
  node [
    id 1089
    label "wzbudzi&#263;"
  ]
  node [
    id 1090
    label "thrill"
  ]
  node [
    id 1091
    label "treat"
  ]
  node [
    id 1092
    label "czerpa&#263;"
  ]
  node [
    id 1093
    label "go"
  ]
  node [
    id 1094
    label "handle"
  ]
  node [
    id 1095
    label "wzbudza&#263;"
  ]
  node [
    id 1096
    label "ogarnia&#263;"
  ]
  node [
    id 1097
    label "czerpanie"
  ]
  node [
    id 1098
    label "acquisition"
  ]
  node [
    id 1099
    label "branie"
  ]
  node [
    id 1100
    label "caparison"
  ]
  node [
    id 1101
    label "wzbudzanie"
  ]
  node [
    id 1102
    label "ogarnianie"
  ]
  node [
    id 1103
    label "sztuka"
  ]
  node [
    id 1104
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1105
    label "Boreasz"
  ]
  node [
    id 1106
    label "noc"
  ]
  node [
    id 1107
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1108
    label "strona_&#347;wiata"
  ]
  node [
    id 1109
    label "godzina"
  ]
  node [
    id 1110
    label "&#347;rodek"
  ]
  node [
    id 1111
    label "dzie&#324;"
  ]
  node [
    id 1112
    label "dwunasta"
  ]
  node [
    id 1113
    label "pora"
  ]
  node [
    id 1114
    label "brzeg"
  ]
  node [
    id 1115
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 1116
    label "p&#322;oza"
  ]
  node [
    id 1117
    label "zawiasy"
  ]
  node [
    id 1118
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1119
    label "element_anatomiczny"
  ]
  node [
    id 1120
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 1121
    label "reda"
  ]
  node [
    id 1122
    label "zbiornik_wodny"
  ]
  node [
    id 1123
    label "przymorze"
  ]
  node [
    id 1124
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 1125
    label "bezmiar"
  ]
  node [
    id 1126
    label "pe&#322;ne_morze"
  ]
  node [
    id 1127
    label "latarnia_morska"
  ]
  node [
    id 1128
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1129
    label "nereida"
  ]
  node [
    id 1130
    label "okeanida"
  ]
  node [
    id 1131
    label "marina"
  ]
  node [
    id 1132
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 1133
    label "Morze_Czerwone"
  ]
  node [
    id 1134
    label "talasoterapia"
  ]
  node [
    id 1135
    label "Morze_Bia&#322;e"
  ]
  node [
    id 1136
    label "paliszcze"
  ]
  node [
    id 1137
    label "Neptun"
  ]
  node [
    id 1138
    label "Morze_Czarne"
  ]
  node [
    id 1139
    label "laguna"
  ]
  node [
    id 1140
    label "Morze_Egejskie"
  ]
  node [
    id 1141
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 1142
    label "Morze_Adriatyckie"
  ]
  node [
    id 1143
    label "rze&#378;biarstwo"
  ]
  node [
    id 1144
    label "planacja"
  ]
  node [
    id 1145
    label "relief"
  ]
  node [
    id 1146
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1147
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1148
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 1149
    label "bozzetto"
  ]
  node [
    id 1150
    label "plastyka"
  ]
  node [
    id 1151
    label "sfera"
  ]
  node [
    id 1152
    label "gleba"
  ]
  node [
    id 1153
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 1154
    label "sialma"
  ]
  node [
    id 1155
    label "skorupa_ziemska"
  ]
  node [
    id 1156
    label "warstwa_perydotytowa"
  ]
  node [
    id 1157
    label "warstwa_granitowa"
  ]
  node [
    id 1158
    label "kriosfera"
  ]
  node [
    id 1159
    label "j&#261;dro"
  ]
  node [
    id 1160
    label "lej_polarny"
  ]
  node [
    id 1161
    label "kula"
  ]
  node [
    id 1162
    label "kresom&#243;zgowie"
  ]
  node [
    id 1163
    label "ozon"
  ]
  node [
    id 1164
    label "przyra"
  ]
  node [
    id 1165
    label "miejsce_pracy"
  ]
  node [
    id 1166
    label "nation"
  ]
  node [
    id 1167
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1168
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1169
    label "w&#322;adza"
  ]
  node [
    id 1170
    label "iglak"
  ]
  node [
    id 1171
    label "cyprysowate"
  ]
  node [
    id 1172
    label "biom"
  ]
  node [
    id 1173
    label "szata_ro&#347;linna"
  ]
  node [
    id 1174
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1175
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1176
    label "zielono&#347;&#263;"
  ]
  node [
    id 1177
    label "pi&#281;tro"
  ]
  node [
    id 1178
    label "plant"
  ]
  node [
    id 1179
    label "ro&#347;lina"
  ]
  node [
    id 1180
    label "geosystem"
  ]
  node [
    id 1181
    label "dotleni&#263;"
  ]
  node [
    id 1182
    label "spi&#281;trza&#263;"
  ]
  node [
    id 1183
    label "spi&#281;trzenie"
  ]
  node [
    id 1184
    label "utylizator"
  ]
  node [
    id 1185
    label "p&#322;ycizna"
  ]
  node [
    id 1186
    label "nabranie"
  ]
  node [
    id 1187
    label "Waruna"
  ]
  node [
    id 1188
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 1189
    label "przybieranie"
  ]
  node [
    id 1190
    label "uci&#261;g"
  ]
  node [
    id 1191
    label "bombast"
  ]
  node [
    id 1192
    label "fala"
  ]
  node [
    id 1193
    label "kryptodepresja"
  ]
  node [
    id 1194
    label "water"
  ]
  node [
    id 1195
    label "wysi&#281;k"
  ]
  node [
    id 1196
    label "pustka"
  ]
  node [
    id 1197
    label "ciecz"
  ]
  node [
    id 1198
    label "przybrze&#380;e"
  ]
  node [
    id 1199
    label "nap&#243;j"
  ]
  node [
    id 1200
    label "spi&#281;trzanie"
  ]
  node [
    id 1201
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1202
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 1203
    label "bicie"
  ]
  node [
    id 1204
    label "klarownik"
  ]
  node [
    id 1205
    label "chlastanie"
  ]
  node [
    id 1206
    label "woda_s&#322;odka"
  ]
  node [
    id 1207
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1208
    label "nabra&#263;"
  ]
  node [
    id 1209
    label "chlasta&#263;"
  ]
  node [
    id 1210
    label "uj&#281;cie_wody"
  ]
  node [
    id 1211
    label "zrzut"
  ]
  node [
    id 1212
    label "wodnik"
  ]
  node [
    id 1213
    label "pojazd"
  ]
  node [
    id 1214
    label "l&#243;d"
  ]
  node [
    id 1215
    label "wybrze&#380;e"
  ]
  node [
    id 1216
    label "deklamacja"
  ]
  node [
    id 1217
    label "tlenek"
  ]
  node [
    id 1218
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1219
    label "biotop"
  ]
  node [
    id 1220
    label "biocenoza"
  ]
  node [
    id 1221
    label "awifauna"
  ]
  node [
    id 1222
    label "ichtiofauna"
  ]
  node [
    id 1223
    label "zaj&#281;cie"
  ]
  node [
    id 1224
    label "instytucja"
  ]
  node [
    id 1225
    label "tajniki"
  ]
  node [
    id 1226
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 1227
    label "zaplecze"
  ]
  node [
    id 1228
    label "pomieszczenie"
  ]
  node [
    id 1229
    label "zlewozmywak"
  ]
  node [
    id 1230
    label "gotowa&#263;"
  ]
  node [
    id 1231
    label "Jowisz"
  ]
  node [
    id 1232
    label "syzygia"
  ]
  node [
    id 1233
    label "Saturn"
  ]
  node [
    id 1234
    label "Uran"
  ]
  node [
    id 1235
    label "strefa"
  ]
  node [
    id 1236
    label "message"
  ]
  node [
    id 1237
    label "dar"
  ]
  node [
    id 1238
    label "real"
  ]
  node [
    id 1239
    label "Ukraina"
  ]
  node [
    id 1240
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1241
    label "blok_wschodni"
  ]
  node [
    id 1242
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1243
    label "Europa_Wschodnia"
  ]
  node [
    id 1244
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 1245
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1246
    label "poszczeg&#243;lnie"
  ]
  node [
    id 1247
    label "pojedynczy"
  ]
  node [
    id 1248
    label "jednodzielny"
  ]
  node [
    id 1249
    label "rzadki"
  ]
  node [
    id 1250
    label "pojedynczo"
  ]
  node [
    id 1251
    label "Katar"
  ]
  node [
    id 1252
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1253
    label "Mazowsze"
  ]
  node [
    id 1254
    label "Libia"
  ]
  node [
    id 1255
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1256
    label "Gwatemala"
  ]
  node [
    id 1257
    label "Anglia"
  ]
  node [
    id 1258
    label "Amazonia"
  ]
  node [
    id 1259
    label "Afganistan"
  ]
  node [
    id 1260
    label "Ekwador"
  ]
  node [
    id 1261
    label "Bordeaux"
  ]
  node [
    id 1262
    label "Tad&#380;ykistan"
  ]
  node [
    id 1263
    label "Bhutan"
  ]
  node [
    id 1264
    label "Argentyna"
  ]
  node [
    id 1265
    label "D&#380;ibuti"
  ]
  node [
    id 1266
    label "Wenezuela"
  ]
  node [
    id 1267
    label "Gabon"
  ]
  node [
    id 1268
    label "Naddniestrze"
  ]
  node [
    id 1269
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1270
    label "Europa_Zachodnia"
  ]
  node [
    id 1271
    label "Armagnac"
  ]
  node [
    id 1272
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1273
    label "Rwanda"
  ]
  node [
    id 1274
    label "Liechtenstein"
  ]
  node [
    id 1275
    label "Amhara"
  ]
  node [
    id 1276
    label "organizacja"
  ]
  node [
    id 1277
    label "Sri_Lanka"
  ]
  node [
    id 1278
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1279
    label "Zamojszczyzna"
  ]
  node [
    id 1280
    label "Madagaskar"
  ]
  node [
    id 1281
    label "Tonga"
  ]
  node [
    id 1282
    label "Kongo"
  ]
  node [
    id 1283
    label "Bangladesz"
  ]
  node [
    id 1284
    label "Kanada"
  ]
  node [
    id 1285
    label "Ma&#322;opolska"
  ]
  node [
    id 1286
    label "Wehrlen"
  ]
  node [
    id 1287
    label "Turkiestan"
  ]
  node [
    id 1288
    label "Algieria"
  ]
  node [
    id 1289
    label "Noworosja"
  ]
  node [
    id 1290
    label "Surinam"
  ]
  node [
    id 1291
    label "Chile"
  ]
  node [
    id 1292
    label "Sahara_Zachodnia"
  ]
  node [
    id 1293
    label "Uganda"
  ]
  node [
    id 1294
    label "Lubelszczyzna"
  ]
  node [
    id 1295
    label "W&#281;gry"
  ]
  node [
    id 1296
    label "Mezoameryka"
  ]
  node [
    id 1297
    label "Birma"
  ]
  node [
    id 1298
    label "Ba&#322;kany"
  ]
  node [
    id 1299
    label "Kurdystan"
  ]
  node [
    id 1300
    label "Kazachstan"
  ]
  node [
    id 1301
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1302
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1303
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1304
    label "Armenia"
  ]
  node [
    id 1305
    label "Tuwalu"
  ]
  node [
    id 1306
    label "Timor_Wschodni"
  ]
  node [
    id 1307
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1308
    label "Szkocja"
  ]
  node [
    id 1309
    label "Baszkiria"
  ]
  node [
    id 1310
    label "Tonkin"
  ]
  node [
    id 1311
    label "Maghreb"
  ]
  node [
    id 1312
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1313
    label "Izrael"
  ]
  node [
    id 1314
    label "Nadrenia"
  ]
  node [
    id 1315
    label "Estonia"
  ]
  node [
    id 1316
    label "Komory"
  ]
  node [
    id 1317
    label "Podhale"
  ]
  node [
    id 1318
    label "Wielkopolska"
  ]
  node [
    id 1319
    label "Zabajkale"
  ]
  node [
    id 1320
    label "Kamerun"
  ]
  node [
    id 1321
    label "Haiti"
  ]
  node [
    id 1322
    label "Belize"
  ]
  node [
    id 1323
    label "Sierra_Leone"
  ]
  node [
    id 1324
    label "Apulia"
  ]
  node [
    id 1325
    label "Luksemburg"
  ]
  node [
    id 1326
    label "USA"
  ]
  node [
    id 1327
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1328
    label "Barbados"
  ]
  node [
    id 1329
    label "San_Marino"
  ]
  node [
    id 1330
    label "Bu&#322;garia"
  ]
  node [
    id 1331
    label "Wietnam"
  ]
  node [
    id 1332
    label "Indonezja"
  ]
  node [
    id 1333
    label "Bojkowszczyzna"
  ]
  node [
    id 1334
    label "Malawi"
  ]
  node [
    id 1335
    label "Francja"
  ]
  node [
    id 1336
    label "Zambia"
  ]
  node [
    id 1337
    label "Kujawy"
  ]
  node [
    id 1338
    label "Angola"
  ]
  node [
    id 1339
    label "Liguria"
  ]
  node [
    id 1340
    label "Grenada"
  ]
  node [
    id 1341
    label "Pamir"
  ]
  node [
    id 1342
    label "Nepal"
  ]
  node [
    id 1343
    label "Panama"
  ]
  node [
    id 1344
    label "Rumunia"
  ]
  node [
    id 1345
    label "Indochiny"
  ]
  node [
    id 1346
    label "Podlasie"
  ]
  node [
    id 1347
    label "Polinezja"
  ]
  node [
    id 1348
    label "Kurpie"
  ]
  node [
    id 1349
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1350
    label "S&#261;decczyzna"
  ]
  node [
    id 1351
    label "Umbria"
  ]
  node [
    id 1352
    label "Czarnog&#243;ra"
  ]
  node [
    id 1353
    label "Malediwy"
  ]
  node [
    id 1354
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1355
    label "S&#322;owacja"
  ]
  node [
    id 1356
    label "Karaiby"
  ]
  node [
    id 1357
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1358
    label "Kielecczyzna"
  ]
  node [
    id 1359
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1360
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1361
    label "Egipt"
  ]
  node [
    id 1362
    label "Kolumbia"
  ]
  node [
    id 1363
    label "Mozambik"
  ]
  node [
    id 1364
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1365
    label "Laos"
  ]
  node [
    id 1366
    label "Burundi"
  ]
  node [
    id 1367
    label "Suazi"
  ]
  node [
    id 1368
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1369
    label "Czechy"
  ]
  node [
    id 1370
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1371
    label "Wyspy_Marshalla"
  ]
  node [
    id 1372
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1373
    label "Dominika"
  ]
  node [
    id 1374
    label "Palau"
  ]
  node [
    id 1375
    label "Syria"
  ]
  node [
    id 1376
    label "Skandynawia"
  ]
  node [
    id 1377
    label "Gwinea_Bissau"
  ]
  node [
    id 1378
    label "Liberia"
  ]
  node [
    id 1379
    label "Zimbabwe"
  ]
  node [
    id 1380
    label "Polska"
  ]
  node [
    id 1381
    label "Jamajka"
  ]
  node [
    id 1382
    label "Tyrol"
  ]
  node [
    id 1383
    label "Huculszczyzna"
  ]
  node [
    id 1384
    label "Bory_Tucholskie"
  ]
  node [
    id 1385
    label "Turyngia"
  ]
  node [
    id 1386
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1387
    label "Dominikana"
  ]
  node [
    id 1388
    label "Senegal"
  ]
  node [
    id 1389
    label "Gruzja"
  ]
  node [
    id 1390
    label "Chorwacja"
  ]
  node [
    id 1391
    label "Togo"
  ]
  node [
    id 1392
    label "Meksyk"
  ]
  node [
    id 1393
    label "Macedonia"
  ]
  node [
    id 1394
    label "Gujana"
  ]
  node [
    id 1395
    label "Zair"
  ]
  node [
    id 1396
    label "Kambod&#380;a"
  ]
  node [
    id 1397
    label "Albania"
  ]
  node [
    id 1398
    label "Mauritius"
  ]
  node [
    id 1399
    label "Monako"
  ]
  node [
    id 1400
    label "Gwinea"
  ]
  node [
    id 1401
    label "Mali"
  ]
  node [
    id 1402
    label "Nigeria"
  ]
  node [
    id 1403
    label "Kalabria"
  ]
  node [
    id 1404
    label "Hercegowina"
  ]
  node [
    id 1405
    label "Kostaryka"
  ]
  node [
    id 1406
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1407
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1408
    label "Lotaryngia"
  ]
  node [
    id 1409
    label "Hanower"
  ]
  node [
    id 1410
    label "Paragwaj"
  ]
  node [
    id 1411
    label "W&#322;ochy"
  ]
  node [
    id 1412
    label "Wyspy_Salomona"
  ]
  node [
    id 1413
    label "Seszele"
  ]
  node [
    id 1414
    label "Hiszpania"
  ]
  node [
    id 1415
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1416
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1417
    label "Walia"
  ]
  node [
    id 1418
    label "Boliwia"
  ]
  node [
    id 1419
    label "Opolskie"
  ]
  node [
    id 1420
    label "Kirgistan"
  ]
  node [
    id 1421
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1422
    label "Irlandia"
  ]
  node [
    id 1423
    label "Kampania"
  ]
  node [
    id 1424
    label "Czad"
  ]
  node [
    id 1425
    label "Irak"
  ]
  node [
    id 1426
    label "Lesoto"
  ]
  node [
    id 1427
    label "Malta"
  ]
  node [
    id 1428
    label "Andora"
  ]
  node [
    id 1429
    label "Sand&#380;ak"
  ]
  node [
    id 1430
    label "Chiny"
  ]
  node [
    id 1431
    label "Filipiny"
  ]
  node [
    id 1432
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1433
    label "Syjon"
  ]
  node [
    id 1434
    label "Niemcy"
  ]
  node [
    id 1435
    label "Kabylia"
  ]
  node [
    id 1436
    label "Lombardia"
  ]
  node [
    id 1437
    label "Warmia"
  ]
  node [
    id 1438
    label "Brazylia"
  ]
  node [
    id 1439
    label "Nikaragua"
  ]
  node [
    id 1440
    label "Pakistan"
  ]
  node [
    id 1441
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1442
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1443
    label "Kaszmir"
  ]
  node [
    id 1444
    label "Kenia"
  ]
  node [
    id 1445
    label "Niger"
  ]
  node [
    id 1446
    label "Tunezja"
  ]
  node [
    id 1447
    label "Portugalia"
  ]
  node [
    id 1448
    label "Fid&#380;i"
  ]
  node [
    id 1449
    label "Maroko"
  ]
  node [
    id 1450
    label "Botswana"
  ]
  node [
    id 1451
    label "Tajlandia"
  ]
  node [
    id 1452
    label "Australia"
  ]
  node [
    id 1453
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1454
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1455
    label "Burkina_Faso"
  ]
  node [
    id 1456
    label "Benin"
  ]
  node [
    id 1457
    label "Tanzania"
  ]
  node [
    id 1458
    label "interior"
  ]
  node [
    id 1459
    label "Indie"
  ]
  node [
    id 1460
    label "&#321;otwa"
  ]
  node [
    id 1461
    label "Biskupizna"
  ]
  node [
    id 1462
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1463
    label "Kiribati"
  ]
  node [
    id 1464
    label "Kaukaz"
  ]
  node [
    id 1465
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1466
    label "Rodezja"
  ]
  node [
    id 1467
    label "Afryka_Wschodnia"
  ]
  node [
    id 1468
    label "Cypr"
  ]
  node [
    id 1469
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1470
    label "Podkarpacie"
  ]
  node [
    id 1471
    label "Peru"
  ]
  node [
    id 1472
    label "Toskania"
  ]
  node [
    id 1473
    label "Afryka_Zachodnia"
  ]
  node [
    id 1474
    label "Austria"
  ]
  node [
    id 1475
    label "Podbeskidzie"
  ]
  node [
    id 1476
    label "Urugwaj"
  ]
  node [
    id 1477
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1478
    label "Jordania"
  ]
  node [
    id 1479
    label "Bo&#347;nia"
  ]
  node [
    id 1480
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1481
    label "Grecja"
  ]
  node [
    id 1482
    label "Azerbejd&#380;an"
  ]
  node [
    id 1483
    label "Oceania"
  ]
  node [
    id 1484
    label "Turcja"
  ]
  node [
    id 1485
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1486
    label "Samoa"
  ]
  node [
    id 1487
    label "Powi&#347;le"
  ]
  node [
    id 1488
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1489
    label "ziemia"
  ]
  node [
    id 1490
    label "Oman"
  ]
  node [
    id 1491
    label "Sudan"
  ]
  node [
    id 1492
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1493
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1494
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1495
    label "Uzbekistan"
  ]
  node [
    id 1496
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1497
    label "Honduras"
  ]
  node [
    id 1498
    label "Mongolia"
  ]
  node [
    id 1499
    label "Portoryko"
  ]
  node [
    id 1500
    label "Kaszuby"
  ]
  node [
    id 1501
    label "Ko&#322;yma"
  ]
  node [
    id 1502
    label "Szlezwik"
  ]
  node [
    id 1503
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1504
    label "Serbia"
  ]
  node [
    id 1505
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1506
    label "Tajwan"
  ]
  node [
    id 1507
    label "Wielka_Brytania"
  ]
  node [
    id 1508
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1509
    label "Liban"
  ]
  node [
    id 1510
    label "Japonia"
  ]
  node [
    id 1511
    label "Ghana"
  ]
  node [
    id 1512
    label "Bahrajn"
  ]
  node [
    id 1513
    label "Belgia"
  ]
  node [
    id 1514
    label "Etiopia"
  ]
  node [
    id 1515
    label "Mikronezja"
  ]
  node [
    id 1516
    label "Polesie"
  ]
  node [
    id 1517
    label "Kuwejt"
  ]
  node [
    id 1518
    label "Kerala"
  ]
  node [
    id 1519
    label "Mazury"
  ]
  node [
    id 1520
    label "Bahamy"
  ]
  node [
    id 1521
    label "Rosja"
  ]
  node [
    id 1522
    label "Mo&#322;dawia"
  ]
  node [
    id 1523
    label "Palestyna"
  ]
  node [
    id 1524
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1525
    label "Lauda"
  ]
  node [
    id 1526
    label "Azja_Wschodnia"
  ]
  node [
    id 1527
    label "Litwa"
  ]
  node [
    id 1528
    label "S&#322;owenia"
  ]
  node [
    id 1529
    label "Szwajcaria"
  ]
  node [
    id 1530
    label "Erytrea"
  ]
  node [
    id 1531
    label "Lubuskie"
  ]
  node [
    id 1532
    label "Kuba"
  ]
  node [
    id 1533
    label "Arabia_Saudyjska"
  ]
  node [
    id 1534
    label "Galicja"
  ]
  node [
    id 1535
    label "Zakarpacie"
  ]
  node [
    id 1536
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1537
    label "Laponia"
  ]
  node [
    id 1538
    label "Malezja"
  ]
  node [
    id 1539
    label "Korea"
  ]
  node [
    id 1540
    label "Yorkshire"
  ]
  node [
    id 1541
    label "Bawaria"
  ]
  node [
    id 1542
    label "Zag&#243;rze"
  ]
  node [
    id 1543
    label "Jemen"
  ]
  node [
    id 1544
    label "Nowa_Zelandia"
  ]
  node [
    id 1545
    label "Andaluzja"
  ]
  node [
    id 1546
    label "Namibia"
  ]
  node [
    id 1547
    label "Nauru"
  ]
  node [
    id 1548
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1549
    label "Brunei"
  ]
  node [
    id 1550
    label "Oksytania"
  ]
  node [
    id 1551
    label "Opolszczyzna"
  ]
  node [
    id 1552
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1553
    label "Kociewie"
  ]
  node [
    id 1554
    label "Khitai"
  ]
  node [
    id 1555
    label "Mauretania"
  ]
  node [
    id 1556
    label "Iran"
  ]
  node [
    id 1557
    label "Gambia"
  ]
  node [
    id 1558
    label "Somalia"
  ]
  node [
    id 1559
    label "Holandia"
  ]
  node [
    id 1560
    label "Lasko"
  ]
  node [
    id 1561
    label "Turkmenistan"
  ]
  node [
    id 1562
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1563
    label "Salwador"
  ]
  node [
    id 1564
    label "linia"
  ]
  node [
    id 1565
    label "ekoton"
  ]
  node [
    id 1566
    label "str&#261;d"
  ]
  node [
    id 1567
    label "koniec"
  ]
  node [
    id 1568
    label "plantowa&#263;"
  ]
  node [
    id 1569
    label "zapadnia"
  ]
  node [
    id 1570
    label "budynek"
  ]
  node [
    id 1571
    label "glinowanie"
  ]
  node [
    id 1572
    label "martwica"
  ]
  node [
    id 1573
    label "penetrator"
  ]
  node [
    id 1574
    label "glinowa&#263;"
  ]
  node [
    id 1575
    label "domain"
  ]
  node [
    id 1576
    label "podglebie"
  ]
  node [
    id 1577
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1578
    label "kort"
  ]
  node [
    id 1579
    label "czynnik_produkcji"
  ]
  node [
    id 1580
    label "powierzchnia"
  ]
  node [
    id 1581
    label "pr&#243;chnica"
  ]
  node [
    id 1582
    label "ryzosfera"
  ]
  node [
    id 1583
    label "p&#322;aszczyzna"
  ]
  node [
    id 1584
    label "glej"
  ]
  node [
    id 1585
    label "pa&#324;stwo"
  ]
  node [
    id 1586
    label "posadzka"
  ]
  node [
    id 1587
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1588
    label "podmiot"
  ]
  node [
    id 1589
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1590
    label "TOPR"
  ]
  node [
    id 1591
    label "endecki"
  ]
  node [
    id 1592
    label "przedstawicielstwo"
  ]
  node [
    id 1593
    label "od&#322;am"
  ]
  node [
    id 1594
    label "Cepelia"
  ]
  node [
    id 1595
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1596
    label "ZBoWiD"
  ]
  node [
    id 1597
    label "organization"
  ]
  node [
    id 1598
    label "centrala"
  ]
  node [
    id 1599
    label "GOPR"
  ]
  node [
    id 1600
    label "ZOMO"
  ]
  node [
    id 1601
    label "ZMP"
  ]
  node [
    id 1602
    label "komitet_koordynacyjny"
  ]
  node [
    id 1603
    label "przybud&#243;wka"
  ]
  node [
    id 1604
    label "boj&#243;wka"
  ]
  node [
    id 1605
    label "inti"
  ]
  node [
    id 1606
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1607
    label "sol"
  ]
  node [
    id 1608
    label "baht"
  ]
  node [
    id 1609
    label "boliviano"
  ]
  node [
    id 1610
    label "dong"
  ]
  node [
    id 1611
    label "Annam"
  ]
  node [
    id 1612
    label "colon"
  ]
  node [
    id 1613
    label "Ameryka_Centralna"
  ]
  node [
    id 1614
    label "Piemont"
  ]
  node [
    id 1615
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1616
    label "NATO"
  ]
  node [
    id 1617
    label "Sardynia"
  ]
  node [
    id 1618
    label "Italia"
  ]
  node [
    id 1619
    label "strefa_euro"
  ]
  node [
    id 1620
    label "Ok&#281;cie"
  ]
  node [
    id 1621
    label "Karyntia"
  ]
  node [
    id 1622
    label "Romania"
  ]
  node [
    id 1623
    label "Warszawa"
  ]
  node [
    id 1624
    label "lir"
  ]
  node [
    id 1625
    label "Sycylia"
  ]
  node [
    id 1626
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1627
    label "Ad&#380;aria"
  ]
  node [
    id 1628
    label "lari"
  ]
  node [
    id 1629
    label "dolar_Belize"
  ]
  node [
    id 1630
    label "dolar"
  ]
  node [
    id 1631
    label "P&#243;&#322;noc"
  ]
  node [
    id 1632
    label "Po&#322;udnie"
  ]
  node [
    id 1633
    label "zielona_karta"
  ]
  node [
    id 1634
    label "stan_wolny"
  ]
  node [
    id 1635
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1636
    label "Wuj_Sam"
  ]
  node [
    id 1637
    label "Zach&#243;d"
  ]
  node [
    id 1638
    label "Hudson"
  ]
  node [
    id 1639
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1640
    label "somoni"
  ]
  node [
    id 1641
    label "euro"
  ]
  node [
    id 1642
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1643
    label "perper"
  ]
  node [
    id 1644
    label "Bengal"
  ]
  node [
    id 1645
    label "taka"
  ]
  node [
    id 1646
    label "Karelia"
  ]
  node [
    id 1647
    label "Mari_El"
  ]
  node [
    id 1648
    label "Inguszetia"
  ]
  node [
    id 1649
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1650
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1651
    label "Udmurcja"
  ]
  node [
    id 1652
    label "Newa"
  ]
  node [
    id 1653
    label "&#321;adoga"
  ]
  node [
    id 1654
    label "Czeczenia"
  ]
  node [
    id 1655
    label "Anadyr"
  ]
  node [
    id 1656
    label "Syberia"
  ]
  node [
    id 1657
    label "Tatarstan"
  ]
  node [
    id 1658
    label "Wszechrosja"
  ]
  node [
    id 1659
    label "Azja"
  ]
  node [
    id 1660
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1661
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1662
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1663
    label "Witim"
  ]
  node [
    id 1664
    label "Kamczatka"
  ]
  node [
    id 1665
    label "Jama&#322;"
  ]
  node [
    id 1666
    label "Dagestan"
  ]
  node [
    id 1667
    label "Tuwa"
  ]
  node [
    id 1668
    label "car"
  ]
  node [
    id 1669
    label "Komi"
  ]
  node [
    id 1670
    label "Czuwaszja"
  ]
  node [
    id 1671
    label "Chakasja"
  ]
  node [
    id 1672
    label "Perm"
  ]
  node [
    id 1673
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1674
    label "Ajon"
  ]
  node [
    id 1675
    label "Adygeja"
  ]
  node [
    id 1676
    label "Dniepr"
  ]
  node [
    id 1677
    label "rubel_rosyjski"
  ]
  node [
    id 1678
    label "Don"
  ]
  node [
    id 1679
    label "Mordowia"
  ]
  node [
    id 1680
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1681
    label "gourde"
  ]
  node [
    id 1682
    label "escudo_angolskie"
  ]
  node [
    id 1683
    label "kwanza"
  ]
  node [
    id 1684
    label "ariary"
  ]
  node [
    id 1685
    label "Ocean_Indyjski"
  ]
  node [
    id 1686
    label "frank_malgaski"
  ]
  node [
    id 1687
    label "Unia_Europejska"
  ]
  node [
    id 1688
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1689
    label "Windawa"
  ]
  node [
    id 1690
    label "&#379;mud&#378;"
  ]
  node [
    id 1691
    label "lit"
  ]
  node [
    id 1692
    label "Synaj"
  ]
  node [
    id 1693
    label "paraszyt"
  ]
  node [
    id 1694
    label "funt_egipski"
  ]
  node [
    id 1695
    label "birr"
  ]
  node [
    id 1696
    label "negus"
  ]
  node [
    id 1697
    label "peso_kolumbijskie"
  ]
  node [
    id 1698
    label "Orinoko"
  ]
  node [
    id 1699
    label "rial_katarski"
  ]
  node [
    id 1700
    label "dram"
  ]
  node [
    id 1701
    label "Limburgia"
  ]
  node [
    id 1702
    label "gulden"
  ]
  node [
    id 1703
    label "Zelandia"
  ]
  node [
    id 1704
    label "Niderlandy"
  ]
  node [
    id 1705
    label "Brabancja"
  ]
  node [
    id 1706
    label "cedi"
  ]
  node [
    id 1707
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1708
    label "milrejs"
  ]
  node [
    id 1709
    label "cruzado"
  ]
  node [
    id 1710
    label "frank_monakijski"
  ]
  node [
    id 1711
    label "Fryburg"
  ]
  node [
    id 1712
    label "Bazylea"
  ]
  node [
    id 1713
    label "Alpy"
  ]
  node [
    id 1714
    label "frank_szwajcarski"
  ]
  node [
    id 1715
    label "Helwecja"
  ]
  node [
    id 1716
    label "Berno"
  ]
  node [
    id 1717
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1718
    label "Dniestr"
  ]
  node [
    id 1719
    label "Gagauzja"
  ]
  node [
    id 1720
    label "Indie_Zachodnie"
  ]
  node [
    id 1721
    label "Sikkim"
  ]
  node [
    id 1722
    label "Asam"
  ]
  node [
    id 1723
    label "rupia_indyjska"
  ]
  node [
    id 1724
    label "Indie_Portugalskie"
  ]
  node [
    id 1725
    label "Indie_Wschodnie"
  ]
  node [
    id 1726
    label "Bollywood"
  ]
  node [
    id 1727
    label "Pend&#380;ab"
  ]
  node [
    id 1728
    label "boliwar"
  ]
  node [
    id 1729
    label "naira"
  ]
  node [
    id 1730
    label "frank_gwinejski"
  ]
  node [
    id 1731
    label "sum"
  ]
  node [
    id 1732
    label "Karaka&#322;pacja"
  ]
  node [
    id 1733
    label "dolar_liberyjski"
  ]
  node [
    id 1734
    label "Dacja"
  ]
  node [
    id 1735
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1736
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1737
    label "Dobrud&#380;a"
  ]
  node [
    id 1738
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1739
    label "dolar_namibijski"
  ]
  node [
    id 1740
    label "kuna"
  ]
  node [
    id 1741
    label "Rugia"
  ]
  node [
    id 1742
    label "Saksonia"
  ]
  node [
    id 1743
    label "Dolna_Saksonia"
  ]
  node [
    id 1744
    label "Anglosas"
  ]
  node [
    id 1745
    label "Hesja"
  ]
  node [
    id 1746
    label "Wirtembergia"
  ]
  node [
    id 1747
    label "Po&#322;abie"
  ]
  node [
    id 1748
    label "Germania"
  ]
  node [
    id 1749
    label "Frankonia"
  ]
  node [
    id 1750
    label "Badenia"
  ]
  node [
    id 1751
    label "Holsztyn"
  ]
  node [
    id 1752
    label "Szwabia"
  ]
  node [
    id 1753
    label "Brandenburgia"
  ]
  node [
    id 1754
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1755
    label "Westfalia"
  ]
  node [
    id 1756
    label "Helgoland"
  ]
  node [
    id 1757
    label "Karlsbad"
  ]
  node [
    id 1758
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1759
    label "korona_w&#281;gierska"
  ]
  node [
    id 1760
    label "forint"
  ]
  node [
    id 1761
    label "Lipt&#243;w"
  ]
  node [
    id 1762
    label "tenge"
  ]
  node [
    id 1763
    label "szach"
  ]
  node [
    id 1764
    label "Baktria"
  ]
  node [
    id 1765
    label "afgani"
  ]
  node [
    id 1766
    label "kip"
  ]
  node [
    id 1767
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1768
    label "Salzburg"
  ]
  node [
    id 1769
    label "Rakuzy"
  ]
  node [
    id 1770
    label "Dyja"
  ]
  node [
    id 1771
    label "konsulent"
  ]
  node [
    id 1772
    label "szyling_austryjacki"
  ]
  node [
    id 1773
    label "peso_urugwajskie"
  ]
  node [
    id 1774
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1775
    label "korona_esto&#324;ska"
  ]
  node [
    id 1776
    label "Inflanty"
  ]
  node [
    id 1777
    label "marka_esto&#324;ska"
  ]
  node [
    id 1778
    label "tala"
  ]
  node [
    id 1779
    label "Podole"
  ]
  node [
    id 1780
    label "Naddnieprze"
  ]
  node [
    id 1781
    label "Ma&#322;orosja"
  ]
  node [
    id 1782
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1783
    label "Nadbu&#380;e"
  ]
  node [
    id 1784
    label "hrywna"
  ]
  node [
    id 1785
    label "Zaporo&#380;e"
  ]
  node [
    id 1786
    label "Krym"
  ]
  node [
    id 1787
    label "Przykarpacie"
  ]
  node [
    id 1788
    label "Kozaczyzna"
  ]
  node [
    id 1789
    label "karbowaniec"
  ]
  node [
    id 1790
    label "riel"
  ]
  node [
    id 1791
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1792
    label "kyat"
  ]
  node [
    id 1793
    label "funt_liba&#324;ski"
  ]
  node [
    id 1794
    label "Mariany"
  ]
  node [
    id 1795
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1796
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1797
    label "dinar_algierski"
  ]
  node [
    id 1798
    label "ringgit"
  ]
  node [
    id 1799
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1800
    label "Borneo"
  ]
  node [
    id 1801
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1802
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1803
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1804
    label "lira_izraelska"
  ]
  node [
    id 1805
    label "szekel"
  ]
  node [
    id 1806
    label "Galilea"
  ]
  node [
    id 1807
    label "Judea"
  ]
  node [
    id 1808
    label "tolar"
  ]
  node [
    id 1809
    label "frank_luksemburski"
  ]
  node [
    id 1810
    label "lempira"
  ]
  node [
    id 1811
    label "Pozna&#324;"
  ]
  node [
    id 1812
    label "lira_malta&#324;ska"
  ]
  node [
    id 1813
    label "Gozo"
  ]
  node [
    id 1814
    label "Paros"
  ]
  node [
    id 1815
    label "Epir"
  ]
  node [
    id 1816
    label "panhellenizm"
  ]
  node [
    id 1817
    label "Eubea"
  ]
  node [
    id 1818
    label "Rodos"
  ]
  node [
    id 1819
    label "Achaja"
  ]
  node [
    id 1820
    label "Termopile"
  ]
  node [
    id 1821
    label "Attyka"
  ]
  node [
    id 1822
    label "Hellada"
  ]
  node [
    id 1823
    label "Etolia"
  ]
  node [
    id 1824
    label "palestra"
  ]
  node [
    id 1825
    label "Kreta"
  ]
  node [
    id 1826
    label "drachma"
  ]
  node [
    id 1827
    label "Olimp"
  ]
  node [
    id 1828
    label "Tesalia"
  ]
  node [
    id 1829
    label "Peloponez"
  ]
  node [
    id 1830
    label "Eolia"
  ]
  node [
    id 1831
    label "Beocja"
  ]
  node [
    id 1832
    label "Parnas"
  ]
  node [
    id 1833
    label "Lesbos"
  ]
  node [
    id 1834
    label "Atlantyk"
  ]
  node [
    id 1835
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1836
    label "Ulster"
  ]
  node [
    id 1837
    label "funt_irlandzki"
  ]
  node [
    id 1838
    label "tugrik"
  ]
  node [
    id 1839
    label "Buriaci"
  ]
  node [
    id 1840
    label "ajmak"
  ]
  node [
    id 1841
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1842
    label "Pikardia"
  ]
  node [
    id 1843
    label "Masyw_Centralny"
  ]
  node [
    id 1844
    label "Akwitania"
  ]
  node [
    id 1845
    label "Alzacja"
  ]
  node [
    id 1846
    label "Sekwana"
  ]
  node [
    id 1847
    label "Langwedocja"
  ]
  node [
    id 1848
    label "Martynika"
  ]
  node [
    id 1849
    label "Bretania"
  ]
  node [
    id 1850
    label "Sabaudia"
  ]
  node [
    id 1851
    label "Korsyka"
  ]
  node [
    id 1852
    label "Normandia"
  ]
  node [
    id 1853
    label "Gaskonia"
  ]
  node [
    id 1854
    label "Burgundia"
  ]
  node [
    id 1855
    label "frank_francuski"
  ]
  node [
    id 1856
    label "Wandea"
  ]
  node [
    id 1857
    label "Prowansja"
  ]
  node [
    id 1858
    label "Gwadelupa"
  ]
  node [
    id 1859
    label "lew"
  ]
  node [
    id 1860
    label "c&#243;rdoba"
  ]
  node [
    id 1861
    label "dolar_Zimbabwe"
  ]
  node [
    id 1862
    label "frank_rwandyjski"
  ]
  node [
    id 1863
    label "kwacha_zambijska"
  ]
  node [
    id 1864
    label "&#322;at"
  ]
  node [
    id 1865
    label "Kurlandia"
  ]
  node [
    id 1866
    label "Liwonia"
  ]
  node [
    id 1867
    label "rubel_&#322;otewski"
  ]
  node [
    id 1868
    label "Himalaje"
  ]
  node [
    id 1869
    label "rupia_nepalska"
  ]
  node [
    id 1870
    label "funt_suda&#324;ski"
  ]
  node [
    id 1871
    label "dolar_bahamski"
  ]
  node [
    id 1872
    label "Wielka_Bahama"
  ]
  node [
    id 1873
    label "Pa&#322;uki"
  ]
  node [
    id 1874
    label "Wolin"
  ]
  node [
    id 1875
    label "z&#322;oty"
  ]
  node [
    id 1876
    label "So&#322;a"
  ]
  node [
    id 1877
    label "Suwalszczyzna"
  ]
  node [
    id 1878
    label "Krajna"
  ]
  node [
    id 1879
    label "barwy_polskie"
  ]
  node [
    id 1880
    label "Izera"
  ]
  node [
    id 1881
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1882
    label "Kaczawa"
  ]
  node [
    id 1883
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1884
    label "Wis&#322;a"
  ]
  node [
    id 1885
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1886
    label "Antyle"
  ]
  node [
    id 1887
    label "dolar_Tuvalu"
  ]
  node [
    id 1888
    label "dinar_iracki"
  ]
  node [
    id 1889
    label "korona_s&#322;owacka"
  ]
  node [
    id 1890
    label "Turiec"
  ]
  node [
    id 1891
    label "jen"
  ]
  node [
    id 1892
    label "jinja"
  ]
  node [
    id 1893
    label "Okinawa"
  ]
  node [
    id 1894
    label "Japonica"
  ]
  node [
    id 1895
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1896
    label "szyling_kenijski"
  ]
  node [
    id 1897
    label "peso_chilijskie"
  ]
  node [
    id 1898
    label "Zanzibar"
  ]
  node [
    id 1899
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1900
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1901
    label "Cebu"
  ]
  node [
    id 1902
    label "Sahara"
  ]
  node [
    id 1903
    label "Tasmania"
  ]
  node [
    id 1904
    label "dolar_australijski"
  ]
  node [
    id 1905
    label "Quebec"
  ]
  node [
    id 1906
    label "dolar_kanadyjski"
  ]
  node [
    id 1907
    label "Nowa_Fundlandia"
  ]
  node [
    id 1908
    label "quetzal"
  ]
  node [
    id 1909
    label "Manica"
  ]
  node [
    id 1910
    label "escudo_mozambickie"
  ]
  node [
    id 1911
    label "Cabo_Delgado"
  ]
  node [
    id 1912
    label "Inhambane"
  ]
  node [
    id 1913
    label "Maputo"
  ]
  node [
    id 1914
    label "Gaza"
  ]
  node [
    id 1915
    label "Niasa"
  ]
  node [
    id 1916
    label "Nampula"
  ]
  node [
    id 1917
    label "metical"
  ]
  node [
    id 1918
    label "frank_tunezyjski"
  ]
  node [
    id 1919
    label "dinar_tunezyjski"
  ]
  node [
    id 1920
    label "lud"
  ]
  node [
    id 1921
    label "frank_kongijski"
  ]
  node [
    id 1922
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1923
    label "dinar_Bahrajnu"
  ]
  node [
    id 1924
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1925
    label "escudo_portugalskie"
  ]
  node [
    id 1926
    label "Melanezja"
  ]
  node [
    id 1927
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1928
    label "d&#380;amahirijja"
  ]
  node [
    id 1929
    label "dinar_libijski"
  ]
  node [
    id 1930
    label "balboa"
  ]
  node [
    id 1931
    label "dolar_surinamski"
  ]
  node [
    id 1932
    label "dolar_Brunei"
  ]
  node [
    id 1933
    label "Estremadura"
  ]
  node [
    id 1934
    label "Kastylia"
  ]
  node [
    id 1935
    label "Rzym_Zachodni"
  ]
  node [
    id 1936
    label "Aragonia"
  ]
  node [
    id 1937
    label "hacjender"
  ]
  node [
    id 1938
    label "Asturia"
  ]
  node [
    id 1939
    label "Baskonia"
  ]
  node [
    id 1940
    label "Majorka"
  ]
  node [
    id 1941
    label "Walencja"
  ]
  node [
    id 1942
    label "peseta"
  ]
  node [
    id 1943
    label "Katalonia"
  ]
  node [
    id 1944
    label "Luksemburgia"
  ]
  node [
    id 1945
    label "frank_belgijski"
  ]
  node [
    id 1946
    label "Walonia"
  ]
  node [
    id 1947
    label "Flandria"
  ]
  node [
    id 1948
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1949
    label "dolar_Barbadosu"
  ]
  node [
    id 1950
    label "korona_czeska"
  ]
  node [
    id 1951
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1952
    label "Wojwodina"
  ]
  node [
    id 1953
    label "dinar_serbski"
  ]
  node [
    id 1954
    label "funt_syryjski"
  ]
  node [
    id 1955
    label "alawizm"
  ]
  node [
    id 1956
    label "Szantung"
  ]
  node [
    id 1957
    label "Chiny_Zachodnie"
  ]
  node [
    id 1958
    label "Kuantung"
  ]
  node [
    id 1959
    label "D&#380;ungaria"
  ]
  node [
    id 1960
    label "yuan"
  ]
  node [
    id 1961
    label "Hongkong"
  ]
  node [
    id 1962
    label "Chiny_Wschodnie"
  ]
  node [
    id 1963
    label "Guangdong"
  ]
  node [
    id 1964
    label "Junnan"
  ]
  node [
    id 1965
    label "Mand&#380;uria"
  ]
  node [
    id 1966
    label "Syczuan"
  ]
  node [
    id 1967
    label "zair"
  ]
  node [
    id 1968
    label "Katanga"
  ]
  node [
    id 1969
    label "ugija"
  ]
  node [
    id 1970
    label "dalasi"
  ]
  node [
    id 1971
    label "funt_cypryjski"
  ]
  node [
    id 1972
    label "Afrodyzje"
  ]
  node [
    id 1973
    label "frank_alba&#324;ski"
  ]
  node [
    id 1974
    label "lek"
  ]
  node [
    id 1975
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1976
    label "kafar"
  ]
  node [
    id 1977
    label "dolar_jamajski"
  ]
  node [
    id 1978
    label "Ocean_Spokojny"
  ]
  node [
    id 1979
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1980
    label "som"
  ]
  node [
    id 1981
    label "guarani"
  ]
  node [
    id 1982
    label "rial_ira&#324;ski"
  ]
  node [
    id 1983
    label "mu&#322;&#322;a"
  ]
  node [
    id 1984
    label "Persja"
  ]
  node [
    id 1985
    label "Jawa"
  ]
  node [
    id 1986
    label "Sumatra"
  ]
  node [
    id 1987
    label "rupia_indonezyjska"
  ]
  node [
    id 1988
    label "Nowa_Gwinea"
  ]
  node [
    id 1989
    label "Moluki"
  ]
  node [
    id 1990
    label "szyling_somalijski"
  ]
  node [
    id 1991
    label "szyling_ugandyjski"
  ]
  node [
    id 1992
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1993
    label "Ujgur"
  ]
  node [
    id 1994
    label "Azja_Mniejsza"
  ]
  node [
    id 1995
    label "lira_turecka"
  ]
  node [
    id 1996
    label "Pireneje"
  ]
  node [
    id 1997
    label "nakfa"
  ]
  node [
    id 1998
    label "won"
  ]
  node [
    id 1999
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 2000
    label "&#346;wite&#378;"
  ]
  node [
    id 2001
    label "dinar_kuwejcki"
  ]
  node [
    id 2002
    label "Nachiczewan"
  ]
  node [
    id 2003
    label "manat_azerski"
  ]
  node [
    id 2004
    label "Karabach"
  ]
  node [
    id 2005
    label "dolar_Kiribati"
  ]
  node [
    id 2006
    label "moszaw"
  ]
  node [
    id 2007
    label "Kanaan"
  ]
  node [
    id 2008
    label "Aruba"
  ]
  node [
    id 2009
    label "Kajmany"
  ]
  node [
    id 2010
    label "Anguilla"
  ]
  node [
    id 2011
    label "Mogielnica"
  ]
  node [
    id 2012
    label "jezioro"
  ]
  node [
    id 2013
    label "Rumelia"
  ]
  node [
    id 2014
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 2015
    label "Poprad"
  ]
  node [
    id 2016
    label "Tatry"
  ]
  node [
    id 2017
    label "Podtatrze"
  ]
  node [
    id 2018
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 2019
    label "Austro-W&#281;gry"
  ]
  node [
    id 2020
    label "Biskupice"
  ]
  node [
    id 2021
    label "Iwanowice"
  ]
  node [
    id 2022
    label "Ziemia_Sandomierska"
  ]
  node [
    id 2023
    label "Rogo&#378;nik"
  ]
  node [
    id 2024
    label "Ropa"
  ]
  node [
    id 2025
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 2026
    label "Karpaty"
  ]
  node [
    id 2027
    label "Beskidy_Zachodnie"
  ]
  node [
    id 2028
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 2029
    label "Beskid_Niski"
  ]
  node [
    id 2030
    label "Etruria"
  ]
  node [
    id 2031
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 2032
    label "Bojanowo"
  ]
  node [
    id 2033
    label "Obra"
  ]
  node [
    id 2034
    label "Wilkowo_Polskie"
  ]
  node [
    id 2035
    label "Dobra"
  ]
  node [
    id 2036
    label "Buriacja"
  ]
  node [
    id 2037
    label "Rozewie"
  ]
  node [
    id 2038
    label "&#346;l&#261;sk"
  ]
  node [
    id 2039
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 2040
    label "Norwegia"
  ]
  node [
    id 2041
    label "Szwecja"
  ]
  node [
    id 2042
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 2043
    label "Finlandia"
  ]
  node [
    id 2044
    label "Wiktoria"
  ]
  node [
    id 2045
    label "Guernsey"
  ]
  node [
    id 2046
    label "Conrad"
  ]
  node [
    id 2047
    label "funt_szterling"
  ]
  node [
    id 2048
    label "Portland"
  ]
  node [
    id 2049
    label "El&#380;bieta_I"
  ]
  node [
    id 2050
    label "Kornwalia"
  ]
  node [
    id 2051
    label "Amazonka"
  ]
  node [
    id 2052
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 2053
    label "Imperium_Rosyjskie"
  ]
  node [
    id 2054
    label "Moza"
  ]
  node [
    id 2055
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 2056
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 2057
    label "Paj&#281;czno"
  ]
  node [
    id 2058
    label "Tar&#322;&#243;w"
  ]
  node [
    id 2059
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 2060
    label "Gop&#322;o"
  ]
  node [
    id 2061
    label "Jerozolima"
  ]
  node [
    id 2062
    label "Dolna_Frankonia"
  ]
  node [
    id 2063
    label "funt_szkocki"
  ]
  node [
    id 2064
    label "Kaledonia"
  ]
  node [
    id 2065
    label "Abchazja"
  ]
  node [
    id 2066
    label "Sarmata"
  ]
  node [
    id 2067
    label "Eurazja"
  ]
  node [
    id 2068
    label "Mariensztat"
  ]
  node [
    id 2069
    label "&#380;egna&#263;"
  ]
  node [
    id 2070
    label "pozosta&#263;"
  ]
  node [
    id 2071
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 2072
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 2073
    label "rozstawa&#263;_si&#281;"
  ]
  node [
    id 2074
    label "pozdrawia&#263;"
  ]
  node [
    id 2075
    label "b&#322;ogos&#322;awi&#263;"
  ]
  node [
    id 2076
    label "bless"
  ]
  node [
    id 2077
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 2078
    label "osta&#263;_si&#281;"
  ]
  node [
    id 2079
    label "catch"
  ]
  node [
    id 2080
    label "support"
  ]
  node [
    id 2081
    label "prze&#380;y&#263;"
  ]
  node [
    id 2082
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 2083
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 2084
    label "wyrafinowany"
  ]
  node [
    id 2085
    label "niepo&#347;ledni"
  ]
  node [
    id 2086
    label "du&#380;y"
  ]
  node [
    id 2087
    label "chwalebny"
  ]
  node [
    id 2088
    label "z_wysoka"
  ]
  node [
    id 2089
    label "wznios&#322;y"
  ]
  node [
    id 2090
    label "daleki"
  ]
  node [
    id 2091
    label "wysoce"
  ]
  node [
    id 2092
    label "szczytnie"
  ]
  node [
    id 2093
    label "znaczny"
  ]
  node [
    id 2094
    label "warto&#347;ciowy"
  ]
  node [
    id 2095
    label "wysoko"
  ]
  node [
    id 2096
    label "uprzywilejowany"
  ]
  node [
    id 2097
    label "doros&#322;y"
  ]
  node [
    id 2098
    label "niema&#322;o"
  ]
  node [
    id 2099
    label "wiele"
  ]
  node [
    id 2100
    label "rozwini&#281;ty"
  ]
  node [
    id 2101
    label "dorodny"
  ]
  node [
    id 2102
    label "wa&#380;ny"
  ]
  node [
    id 2103
    label "prawdziwy"
  ]
  node [
    id 2104
    label "du&#380;o"
  ]
  node [
    id 2105
    label "znacznie"
  ]
  node [
    id 2106
    label "zauwa&#380;alny"
  ]
  node [
    id 2107
    label "szczeg&#243;lny"
  ]
  node [
    id 2108
    label "lekki"
  ]
  node [
    id 2109
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 2110
    label "niez&#322;y"
  ]
  node [
    id 2111
    label "niepo&#347;lednio"
  ]
  node [
    id 2112
    label "wyj&#261;tkowy"
  ]
  node [
    id 2113
    label "szlachetny"
  ]
  node [
    id 2114
    label "powa&#380;ny"
  ]
  node [
    id 2115
    label "podnios&#322;y"
  ]
  node [
    id 2116
    label "wznio&#347;le"
  ]
  node [
    id 2117
    label "oderwany"
  ]
  node [
    id 2118
    label "pi&#281;kny"
  ]
  node [
    id 2119
    label "pochwalny"
  ]
  node [
    id 2120
    label "wspania&#322;y"
  ]
  node [
    id 2121
    label "chwalebnie"
  ]
  node [
    id 2122
    label "obyty"
  ]
  node [
    id 2123
    label "wykwintny"
  ]
  node [
    id 2124
    label "wyrafinowanie"
  ]
  node [
    id 2125
    label "wymy&#347;lny"
  ]
  node [
    id 2126
    label "rewaluowanie"
  ]
  node [
    id 2127
    label "warto&#347;ciowo"
  ]
  node [
    id 2128
    label "drogi"
  ]
  node [
    id 2129
    label "u&#380;yteczny"
  ]
  node [
    id 2130
    label "zrewaluowanie"
  ]
  node [
    id 2131
    label "dobry"
  ]
  node [
    id 2132
    label "dawny"
  ]
  node [
    id 2133
    label "ogl&#281;dny"
  ]
  node [
    id 2134
    label "d&#322;ugi"
  ]
  node [
    id 2135
    label "daleko"
  ]
  node [
    id 2136
    label "odleg&#322;y"
  ]
  node [
    id 2137
    label "zwi&#261;zany"
  ]
  node [
    id 2138
    label "r&#243;&#380;ny"
  ]
  node [
    id 2139
    label "s&#322;aby"
  ]
  node [
    id 2140
    label "odlegle"
  ]
  node [
    id 2141
    label "oddalony"
  ]
  node [
    id 2142
    label "g&#322;&#281;boki"
  ]
  node [
    id 2143
    label "obcy"
  ]
  node [
    id 2144
    label "nieobecny"
  ]
  node [
    id 2145
    label "przysz&#322;y"
  ]
  node [
    id 2146
    label "g&#243;rno"
  ]
  node [
    id 2147
    label "szczytny"
  ]
  node [
    id 2148
    label "intensywnie"
  ]
  node [
    id 2149
    label "wielki"
  ]
  node [
    id 2150
    label "niezmiernie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 7
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 12
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 334
  ]
  edge [
    source 14
    target 313
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 990
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 991
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 993
  ]
  edge [
    source 14
    target 994
  ]
  edge [
    source 14
    target 995
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 997
  ]
  edge [
    source 14
    target 998
  ]
  edge [
    source 14
    target 999
  ]
  edge [
    source 14
    target 1000
  ]
  edge [
    source 14
    target 1001
  ]
  edge [
    source 14
    target 1002
  ]
  edge [
    source 14
    target 1003
  ]
  edge [
    source 14
    target 1004
  ]
  edge [
    source 14
    target 1005
  ]
  edge [
    source 14
    target 1006
  ]
  edge [
    source 14
    target 1007
  ]
  edge [
    source 14
    target 1008
  ]
  edge [
    source 14
    target 1009
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 1010
  ]
  edge [
    source 14
    target 1011
  ]
  edge [
    source 14
    target 1012
  ]
  edge [
    source 14
    target 1013
  ]
  edge [
    source 14
    target 1014
  ]
  edge [
    source 14
    target 1015
  ]
  edge [
    source 14
    target 1016
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 1018
  ]
  edge [
    source 14
    target 1019
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 1020
  ]
  edge [
    source 14
    target 1021
  ]
  edge [
    source 14
    target 1022
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 1023
  ]
  edge [
    source 14
    target 1024
  ]
  edge [
    source 14
    target 1025
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1028
  ]
  edge [
    source 14
    target 1029
  ]
  edge [
    source 14
    target 1030
  ]
  edge [
    source 14
    target 1031
  ]
  edge [
    source 14
    target 1032
  ]
  edge [
    source 14
    target 1033
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 14
    target 1046
  ]
  edge [
    source 14
    target 1047
  ]
  edge [
    source 14
    target 1048
  ]
  edge [
    source 14
    target 1049
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 333
  ]
  edge [
    source 14
    target 319
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 336
  ]
  edge [
    source 14
    target 352
  ]
  edge [
    source 14
    target 343
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 386
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 369
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 280
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 14
    target 1148
  ]
  edge [
    source 14
    target 1149
  ]
  edge [
    source 14
    target 1150
  ]
  edge [
    source 14
    target 1151
  ]
  edge [
    source 14
    target 1152
  ]
  edge [
    source 14
    target 1153
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 1154
  ]
  edge [
    source 14
    target 1155
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1157
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 842
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 1173
  ]
  edge [
    source 14
    target 1174
  ]
  edge [
    source 14
    target 1175
  ]
  edge [
    source 14
    target 1176
  ]
  edge [
    source 14
    target 1177
  ]
  edge [
    source 14
    target 1178
  ]
  edge [
    source 14
    target 1179
  ]
  edge [
    source 14
    target 1180
  ]
  edge [
    source 14
    target 1181
  ]
  edge [
    source 14
    target 1182
  ]
  edge [
    source 14
    target 1183
  ]
  edge [
    source 14
    target 1184
  ]
  edge [
    source 14
    target 1185
  ]
  edge [
    source 14
    target 1186
  ]
  edge [
    source 14
    target 1187
  ]
  edge [
    source 14
    target 1188
  ]
  edge [
    source 14
    target 1189
  ]
  edge [
    source 14
    target 1190
  ]
  edge [
    source 14
    target 1191
  ]
  edge [
    source 14
    target 1192
  ]
  edge [
    source 14
    target 1193
  ]
  edge [
    source 14
    target 1194
  ]
  edge [
    source 14
    target 1195
  ]
  edge [
    source 14
    target 1196
  ]
  edge [
    source 14
    target 1197
  ]
  edge [
    source 14
    target 1198
  ]
  edge [
    source 14
    target 1199
  ]
  edge [
    source 14
    target 1200
  ]
  edge [
    source 14
    target 1201
  ]
  edge [
    source 14
    target 1202
  ]
  edge [
    source 14
    target 1203
  ]
  edge [
    source 14
    target 1204
  ]
  edge [
    source 14
    target 1205
  ]
  edge [
    source 14
    target 1206
  ]
  edge [
    source 14
    target 1207
  ]
  edge [
    source 14
    target 1208
  ]
  edge [
    source 14
    target 1209
  ]
  edge [
    source 14
    target 1210
  ]
  edge [
    source 14
    target 1211
  ]
  edge [
    source 14
    target 416
  ]
  edge [
    source 14
    target 1212
  ]
  edge [
    source 14
    target 1213
  ]
  edge [
    source 14
    target 1214
  ]
  edge [
    source 14
    target 1215
  ]
  edge [
    source 14
    target 1216
  ]
  edge [
    source 14
    target 1217
  ]
  edge [
    source 14
    target 1218
  ]
  edge [
    source 14
    target 1219
  ]
  edge [
    source 14
    target 1220
  ]
  edge [
    source 14
    target 1221
  ]
  edge [
    source 14
    target 1222
  ]
  edge [
    source 14
    target 1223
  ]
  edge [
    source 14
    target 1224
  ]
  edge [
    source 14
    target 1225
  ]
  edge [
    source 14
    target 1226
  ]
  edge [
    source 14
    target 271
  ]
  edge [
    source 14
    target 1227
  ]
  edge [
    source 14
    target 1228
  ]
  edge [
    source 14
    target 1229
  ]
  edge [
    source 14
    target 1230
  ]
  edge [
    source 14
    target 1231
  ]
  edge [
    source 14
    target 1232
  ]
  edge [
    source 14
    target 1233
  ]
  edge [
    source 14
    target 1234
  ]
  edge [
    source 14
    target 1235
  ]
  edge [
    source 14
    target 1236
  ]
  edge [
    source 14
    target 1237
  ]
  edge [
    source 14
    target 1238
  ]
  edge [
    source 14
    target 1239
  ]
  edge [
    source 14
    target 1240
  ]
  edge [
    source 14
    target 1241
  ]
  edge [
    source 14
    target 1242
  ]
  edge [
    source 14
    target 1243
  ]
  edge [
    source 14
    target 1244
  ]
  edge [
    source 14
    target 1245
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1246
  ]
  edge [
    source 15
    target 1247
  ]
  edge [
    source 15
    target 1248
  ]
  edge [
    source 15
    target 1249
  ]
  edge [
    source 15
    target 1250
  ]
  edge [
    source 16
    target 1251
  ]
  edge [
    source 16
    target 1252
  ]
  edge [
    source 16
    target 1253
  ]
  edge [
    source 16
    target 1254
  ]
  edge [
    source 16
    target 1255
  ]
  edge [
    source 16
    target 1256
  ]
  edge [
    source 16
    target 1257
  ]
  edge [
    source 16
    target 1258
  ]
  edge [
    source 16
    target 1259
  ]
  edge [
    source 16
    target 1260
  ]
  edge [
    source 16
    target 1261
  ]
  edge [
    source 16
    target 1262
  ]
  edge [
    source 16
    target 1263
  ]
  edge [
    source 16
    target 1264
  ]
  edge [
    source 16
    target 1265
  ]
  edge [
    source 16
    target 1266
  ]
  edge [
    source 16
    target 1239
  ]
  edge [
    source 16
    target 1267
  ]
  edge [
    source 16
    target 1268
  ]
  edge [
    source 16
    target 1269
  ]
  edge [
    source 16
    target 1270
  ]
  edge [
    source 16
    target 1271
  ]
  edge [
    source 16
    target 1272
  ]
  edge [
    source 16
    target 1273
  ]
  edge [
    source 16
    target 1274
  ]
  edge [
    source 16
    target 1275
  ]
  edge [
    source 16
    target 1276
  ]
  edge [
    source 16
    target 1277
  ]
  edge [
    source 16
    target 1278
  ]
  edge [
    source 16
    target 1279
  ]
  edge [
    source 16
    target 1280
  ]
  edge [
    source 16
    target 1281
  ]
  edge [
    source 16
    target 1282
  ]
  edge [
    source 16
    target 1283
  ]
  edge [
    source 16
    target 1284
  ]
  edge [
    source 16
    target 1285
  ]
  edge [
    source 16
    target 1286
  ]
  edge [
    source 16
    target 1287
  ]
  edge [
    source 16
    target 1288
  ]
  edge [
    source 16
    target 1289
  ]
  edge [
    source 16
    target 1290
  ]
  edge [
    source 16
    target 1291
  ]
  edge [
    source 16
    target 1292
  ]
  edge [
    source 16
    target 1293
  ]
  edge [
    source 16
    target 1294
  ]
  edge [
    source 16
    target 1295
  ]
  edge [
    source 16
    target 1296
  ]
  edge [
    source 16
    target 1297
  ]
  edge [
    source 16
    target 1298
  ]
  edge [
    source 16
    target 1299
  ]
  edge [
    source 16
    target 1300
  ]
  edge [
    source 16
    target 1301
  ]
  edge [
    source 16
    target 1302
  ]
  edge [
    source 16
    target 1303
  ]
  edge [
    source 16
    target 1304
  ]
  edge [
    source 16
    target 1305
  ]
  edge [
    source 16
    target 1306
  ]
  edge [
    source 16
    target 1307
  ]
  edge [
    source 16
    target 1308
  ]
  edge [
    source 16
    target 1309
  ]
  edge [
    source 16
    target 1310
  ]
  edge [
    source 16
    target 1311
  ]
  edge [
    source 16
    target 1312
  ]
  edge [
    source 16
    target 1313
  ]
  edge [
    source 16
    target 1240
  ]
  edge [
    source 16
    target 1314
  ]
  edge [
    source 16
    target 1315
  ]
  edge [
    source 16
    target 1316
  ]
  edge [
    source 16
    target 1317
  ]
  edge [
    source 16
    target 1318
  ]
  edge [
    source 16
    target 1319
  ]
  edge [
    source 16
    target 1320
  ]
  edge [
    source 16
    target 1321
  ]
  edge [
    source 16
    target 1322
  ]
  edge [
    source 16
    target 1323
  ]
  edge [
    source 16
    target 1324
  ]
  edge [
    source 16
    target 1325
  ]
  edge [
    source 16
    target 1114
  ]
  edge [
    source 16
    target 1326
  ]
  edge [
    source 16
    target 1327
  ]
  edge [
    source 16
    target 1328
  ]
  edge [
    source 16
    target 1329
  ]
  edge [
    source 16
    target 1330
  ]
  edge [
    source 16
    target 1331
  ]
  edge [
    source 16
    target 1332
  ]
  edge [
    source 16
    target 1333
  ]
  edge [
    source 16
    target 1334
  ]
  edge [
    source 16
    target 1335
  ]
  edge [
    source 16
    target 1336
  ]
  edge [
    source 16
    target 1337
  ]
  edge [
    source 16
    target 1338
  ]
  edge [
    source 16
    target 1339
  ]
  edge [
    source 16
    target 1340
  ]
  edge [
    source 16
    target 1341
  ]
  edge [
    source 16
    target 1342
  ]
  edge [
    source 16
    target 1343
  ]
  edge [
    source 16
    target 1344
  ]
  edge [
    source 16
    target 1345
  ]
  edge [
    source 16
    target 1346
  ]
  edge [
    source 16
    target 1347
  ]
  edge [
    source 16
    target 1348
  ]
  edge [
    source 16
    target 1349
  ]
  edge [
    source 16
    target 1350
  ]
  edge [
    source 16
    target 1351
  ]
  edge [
    source 16
    target 1352
  ]
  edge [
    source 16
    target 1353
  ]
  edge [
    source 16
    target 1354
  ]
  edge [
    source 16
    target 1355
  ]
  edge [
    source 16
    target 1356
  ]
  edge [
    source 16
    target 1357
  ]
  edge [
    source 16
    target 1358
  ]
  edge [
    source 16
    target 1359
  ]
  edge [
    source 16
    target 1360
  ]
  edge [
    source 16
    target 1361
  ]
  edge [
    source 16
    target 1362
  ]
  edge [
    source 16
    target 1363
  ]
  edge [
    source 16
    target 1364
  ]
  edge [
    source 16
    target 1365
  ]
  edge [
    source 16
    target 1366
  ]
  edge [
    source 16
    target 1367
  ]
  edge [
    source 16
    target 1368
  ]
  edge [
    source 16
    target 1369
  ]
  edge [
    source 16
    target 1370
  ]
  edge [
    source 16
    target 1371
  ]
  edge [
    source 16
    target 1372
  ]
  edge [
    source 16
    target 1373
  ]
  edge [
    source 16
    target 1374
  ]
  edge [
    source 16
    target 1375
  ]
  edge [
    source 16
    target 1376
  ]
  edge [
    source 16
    target 1377
  ]
  edge [
    source 16
    target 1378
  ]
  edge [
    source 16
    target 1379
  ]
  edge [
    source 16
    target 1380
  ]
  edge [
    source 16
    target 1381
  ]
  edge [
    source 16
    target 1382
  ]
  edge [
    source 16
    target 1383
  ]
  edge [
    source 16
    target 1384
  ]
  edge [
    source 16
    target 1385
  ]
  edge [
    source 16
    target 1386
  ]
  edge [
    source 16
    target 1387
  ]
  edge [
    source 16
    target 1388
  ]
  edge [
    source 16
    target 1389
  ]
  edge [
    source 16
    target 1390
  ]
  edge [
    source 16
    target 1391
  ]
  edge [
    source 16
    target 1392
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 1393
  ]
  edge [
    source 16
    target 1394
  ]
  edge [
    source 16
    target 1395
  ]
  edge [
    source 16
    target 1396
  ]
  edge [
    source 16
    target 1397
  ]
  edge [
    source 16
    target 1398
  ]
  edge [
    source 16
    target 1399
  ]
  edge [
    source 16
    target 1400
  ]
  edge [
    source 16
    target 1401
  ]
  edge [
    source 16
    target 1402
  ]
  edge [
    source 16
    target 1403
  ]
  edge [
    source 16
    target 1404
  ]
  edge [
    source 16
    target 1405
  ]
  edge [
    source 16
    target 1406
  ]
  edge [
    source 16
    target 1407
  ]
  edge [
    source 16
    target 1408
  ]
  edge [
    source 16
    target 1409
  ]
  edge [
    source 16
    target 1410
  ]
  edge [
    source 16
    target 1411
  ]
  edge [
    source 16
    target 1412
  ]
  edge [
    source 16
    target 1413
  ]
  edge [
    source 16
    target 1414
  ]
  edge [
    source 16
    target 1415
  ]
  edge [
    source 16
    target 1416
  ]
  edge [
    source 16
    target 1417
  ]
  edge [
    source 16
    target 1418
  ]
  edge [
    source 16
    target 1245
  ]
  edge [
    source 16
    target 1419
  ]
  edge [
    source 16
    target 1420
  ]
  edge [
    source 16
    target 1421
  ]
  edge [
    source 16
    target 1422
  ]
  edge [
    source 16
    target 1423
  ]
  edge [
    source 16
    target 1424
  ]
  edge [
    source 16
    target 1425
  ]
  edge [
    source 16
    target 1426
  ]
  edge [
    source 16
    target 1427
  ]
  edge [
    source 16
    target 1428
  ]
  edge [
    source 16
    target 1429
  ]
  edge [
    source 16
    target 1430
  ]
  edge [
    source 16
    target 1431
  ]
  edge [
    source 16
    target 1432
  ]
  edge [
    source 16
    target 1433
  ]
  edge [
    source 16
    target 1434
  ]
  edge [
    source 16
    target 1435
  ]
  edge [
    source 16
    target 1436
  ]
  edge [
    source 16
    target 1437
  ]
  edge [
    source 16
    target 1438
  ]
  edge [
    source 16
    target 1439
  ]
  edge [
    source 16
    target 1440
  ]
  edge [
    source 16
    target 1441
  ]
  edge [
    source 16
    target 1442
  ]
  edge [
    source 16
    target 1443
  ]
  edge [
    source 16
    target 1444
  ]
  edge [
    source 16
    target 1445
  ]
  edge [
    source 16
    target 1446
  ]
  edge [
    source 16
    target 1447
  ]
  edge [
    source 16
    target 1448
  ]
  edge [
    source 16
    target 1449
  ]
  edge [
    source 16
    target 1450
  ]
  edge [
    source 16
    target 1451
  ]
  edge [
    source 16
    target 1452
  ]
  edge [
    source 16
    target 1453
  ]
  edge [
    source 16
    target 1243
  ]
  edge [
    source 16
    target 1454
  ]
  edge [
    source 16
    target 1455
  ]
  edge [
    source 16
    target 1456
  ]
  edge [
    source 16
    target 1457
  ]
  edge [
    source 16
    target 1458
  ]
  edge [
    source 16
    target 1459
  ]
  edge [
    source 16
    target 1460
  ]
  edge [
    source 16
    target 1461
  ]
  edge [
    source 16
    target 1462
  ]
  edge [
    source 16
    target 1463
  ]
  edge [
    source 16
    target 1464
  ]
  edge [
    source 16
    target 1465
  ]
  edge [
    source 16
    target 1466
  ]
  edge [
    source 16
    target 1467
  ]
  edge [
    source 16
    target 1468
  ]
  edge [
    source 16
    target 1242
  ]
  edge [
    source 16
    target 1469
  ]
  edge [
    source 16
    target 1470
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 1471
  ]
  edge [
    source 16
    target 1472
  ]
  edge [
    source 16
    target 1473
  ]
  edge [
    source 16
    target 1474
  ]
  edge [
    source 16
    target 1475
  ]
  edge [
    source 16
    target 1476
  ]
  edge [
    source 16
    target 1477
  ]
  edge [
    source 16
    target 1478
  ]
  edge [
    source 16
    target 1479
  ]
  edge [
    source 16
    target 1480
  ]
  edge [
    source 16
    target 1481
  ]
  edge [
    source 16
    target 1482
  ]
  edge [
    source 16
    target 1483
  ]
  edge [
    source 16
    target 1484
  ]
  edge [
    source 16
    target 1485
  ]
  edge [
    source 16
    target 1486
  ]
  edge [
    source 16
    target 1487
  ]
  edge [
    source 16
    target 1488
  ]
  edge [
    source 16
    target 1489
  ]
  edge [
    source 16
    target 1490
  ]
  edge [
    source 16
    target 1491
  ]
  edge [
    source 16
    target 1492
  ]
  edge [
    source 16
    target 1493
  ]
  edge [
    source 16
    target 1494
  ]
  edge [
    source 16
    target 1495
  ]
  edge [
    source 16
    target 1496
  ]
  edge [
    source 16
    target 1497
  ]
  edge [
    source 16
    target 1498
  ]
  edge [
    source 16
    target 1499
  ]
  edge [
    source 16
    target 1500
  ]
  edge [
    source 16
    target 1501
  ]
  edge [
    source 16
    target 1502
  ]
  edge [
    source 16
    target 1503
  ]
  edge [
    source 16
    target 1504
  ]
  edge [
    source 16
    target 1505
  ]
  edge [
    source 16
    target 1506
  ]
  edge [
    source 16
    target 1507
  ]
  edge [
    source 16
    target 1508
  ]
  edge [
    source 16
    target 1509
  ]
  edge [
    source 16
    target 1510
  ]
  edge [
    source 16
    target 1511
  ]
  edge [
    source 16
    target 1512
  ]
  edge [
    source 16
    target 1513
  ]
  edge [
    source 16
    target 1514
  ]
  edge [
    source 16
    target 1515
  ]
  edge [
    source 16
    target 1516
  ]
  edge [
    source 16
    target 1517
  ]
  edge [
    source 16
    target 1518
  ]
  edge [
    source 16
    target 1519
  ]
  edge [
    source 16
    target 1520
  ]
  edge [
    source 16
    target 1521
  ]
  edge [
    source 16
    target 1522
  ]
  edge [
    source 16
    target 1523
  ]
  edge [
    source 16
    target 1524
  ]
  edge [
    source 16
    target 1525
  ]
  edge [
    source 16
    target 1526
  ]
  edge [
    source 16
    target 1527
  ]
  edge [
    source 16
    target 1528
  ]
  edge [
    source 16
    target 1529
  ]
  edge [
    source 16
    target 1530
  ]
  edge [
    source 16
    target 1531
  ]
  edge [
    source 16
    target 1532
  ]
  edge [
    source 16
    target 1533
  ]
  edge [
    source 16
    target 1534
  ]
  edge [
    source 16
    target 1535
  ]
  edge [
    source 16
    target 1536
  ]
  edge [
    source 16
    target 1537
  ]
  edge [
    source 16
    target 1104
  ]
  edge [
    source 16
    target 1538
  ]
  edge [
    source 16
    target 1539
  ]
  edge [
    source 16
    target 1540
  ]
  edge [
    source 16
    target 1541
  ]
  edge [
    source 16
    target 1542
  ]
  edge [
    source 16
    target 1543
  ]
  edge [
    source 16
    target 1544
  ]
  edge [
    source 16
    target 1545
  ]
  edge [
    source 16
    target 1546
  ]
  edge [
    source 16
    target 1547
  ]
  edge [
    source 16
    target 1548
  ]
  edge [
    source 16
    target 1549
  ]
  edge [
    source 16
    target 1550
  ]
  edge [
    source 16
    target 1551
  ]
  edge [
    source 16
    target 1552
  ]
  edge [
    source 16
    target 1553
  ]
  edge [
    source 16
    target 1554
  ]
  edge [
    source 16
    target 1555
  ]
  edge [
    source 16
    target 1556
  ]
  edge [
    source 16
    target 1557
  ]
  edge [
    source 16
    target 1558
  ]
  edge [
    source 16
    target 1559
  ]
  edge [
    source 16
    target 1560
  ]
  edge [
    source 16
    target 1561
  ]
  edge [
    source 16
    target 1562
  ]
  edge [
    source 16
    target 1563
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 1564
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 1565
  ]
  edge [
    source 16
    target 1566
  ]
  edge [
    source 16
    target 1567
  ]
  edge [
    source 16
    target 1568
  ]
  edge [
    source 16
    target 1569
  ]
  edge [
    source 16
    target 1570
  ]
  edge [
    source 16
    target 1155
  ]
  edge [
    source 16
    target 1571
  ]
  edge [
    source 16
    target 1572
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 1573
  ]
  edge [
    source 16
    target 1574
  ]
  edge [
    source 16
    target 1575
  ]
  edge [
    source 16
    target 1576
  ]
  edge [
    source 16
    target 1577
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 1578
  ]
  edge [
    source 16
    target 1579
  ]
  edge [
    source 16
    target 1213
  ]
  edge [
    source 16
    target 1580
  ]
  edge [
    source 16
    target 1581
  ]
  edge [
    source 16
    target 1228
  ]
  edge [
    source 16
    target 1582
  ]
  edge [
    source 16
    target 1583
  ]
  edge [
    source 16
    target 1181
  ]
  edge [
    source 16
    target 1584
  ]
  edge [
    source 16
    target 1585
  ]
  edge [
    source 16
    target 1586
  ]
  edge [
    source 16
    target 1180
  ]
  edge [
    source 16
    target 1587
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 1588
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 635
  ]
  edge [
    source 16
    target 1589
  ]
  edge [
    source 16
    target 1590
  ]
  edge [
    source 16
    target 1591
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 1592
  ]
  edge [
    source 16
    target 1593
  ]
  edge [
    source 16
    target 1594
  ]
  edge [
    source 16
    target 1595
  ]
  edge [
    source 16
    target 1596
  ]
  edge [
    source 16
    target 1597
  ]
  edge [
    source 16
    target 1598
  ]
  edge [
    source 16
    target 1599
  ]
  edge [
    source 16
    target 1600
  ]
  edge [
    source 16
    target 1601
  ]
  edge [
    source 16
    target 1602
  ]
  edge [
    source 16
    target 1603
  ]
  edge [
    source 16
    target 1604
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 983
  ]
  edge [
    source 16
    target 984
  ]
  edge [
    source 16
    target 985
  ]
  edge [
    source 16
    target 986
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 988
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 990
  ]
  edge [
    source 16
    target 991
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 993
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 995
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 16
    target 997
  ]
  edge [
    source 16
    target 998
  ]
  edge [
    source 16
    target 999
  ]
  edge [
    source 16
    target 1000
  ]
  edge [
    source 16
    target 1001
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 1007
  ]
  edge [
    source 16
    target 1605
  ]
  edge [
    source 16
    target 1606
  ]
  edge [
    source 16
    target 1607
  ]
  edge [
    source 16
    target 1608
  ]
  edge [
    source 16
    target 1609
  ]
  edge [
    source 16
    target 1610
  ]
  edge [
    source 16
    target 1611
  ]
  edge [
    source 16
    target 1612
  ]
  edge [
    source 16
    target 1613
  ]
  edge [
    source 16
    target 1614
  ]
  edge [
    source 16
    target 1615
  ]
  edge [
    source 16
    target 1616
  ]
  edge [
    source 16
    target 1617
  ]
  edge [
    source 16
    target 1618
  ]
  edge [
    source 16
    target 1619
  ]
  edge [
    source 16
    target 1620
  ]
  edge [
    source 16
    target 1621
  ]
  edge [
    source 16
    target 1622
  ]
  edge [
    source 16
    target 1623
  ]
  edge [
    source 16
    target 1624
  ]
  edge [
    source 16
    target 1625
  ]
  edge [
    source 16
    target 1626
  ]
  edge [
    source 16
    target 1627
  ]
  edge [
    source 16
    target 1628
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 1629
  ]
  edge [
    source 16
    target 1630
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 1631
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 1632
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 1633
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 1634
  ]
  edge [
    source 16
    target 1635
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 16
    target 1636
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 609
  ]
  edge [
    source 16
    target 1637
  ]
  edge [
    source 16
    target 611
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 1638
  ]
  edge [
    source 16
    target 1639
  ]
  edge [
    source 16
    target 1640
  ]
  edge [
    source 16
    target 1641
  ]
  edge [
    source 16
    target 1642
  ]
  edge [
    source 16
    target 1643
  ]
  edge [
    source 16
    target 1644
  ]
  edge [
    source 16
    target 1645
  ]
  edge [
    source 16
    target 1646
  ]
  edge [
    source 16
    target 1647
  ]
  edge [
    source 16
    target 1648
  ]
  edge [
    source 16
    target 1649
  ]
  edge [
    source 16
    target 1650
  ]
  edge [
    source 16
    target 1651
  ]
  edge [
    source 16
    target 1652
  ]
  edge [
    source 16
    target 1653
  ]
  edge [
    source 16
    target 1654
  ]
  edge [
    source 16
    target 1655
  ]
  edge [
    source 16
    target 1656
  ]
  edge [
    source 16
    target 1657
  ]
  edge [
    source 16
    target 1658
  ]
  edge [
    source 16
    target 1659
  ]
  edge [
    source 16
    target 1660
  ]
  edge [
    source 16
    target 1661
  ]
  edge [
    source 16
    target 1662
  ]
  edge [
    source 16
    target 1663
  ]
  edge [
    source 16
    target 1664
  ]
  edge [
    source 16
    target 1665
  ]
  edge [
    source 16
    target 1666
  ]
  edge [
    source 16
    target 1667
  ]
  edge [
    source 16
    target 1668
  ]
  edge [
    source 16
    target 1669
  ]
  edge [
    source 16
    target 1670
  ]
  edge [
    source 16
    target 1671
  ]
  edge [
    source 16
    target 1672
  ]
  edge [
    source 16
    target 1673
  ]
  edge [
    source 16
    target 1674
  ]
  edge [
    source 16
    target 1675
  ]
  edge [
    source 16
    target 1676
  ]
  edge [
    source 16
    target 1677
  ]
  edge [
    source 16
    target 1678
  ]
  edge [
    source 16
    target 1679
  ]
  edge [
    source 16
    target 1680
  ]
  edge [
    source 16
    target 1681
  ]
  edge [
    source 16
    target 1682
  ]
  edge [
    source 16
    target 1683
  ]
  edge [
    source 16
    target 1684
  ]
  edge [
    source 16
    target 1685
  ]
  edge [
    source 16
    target 1686
  ]
  edge [
    source 16
    target 1687
  ]
  edge [
    source 16
    target 1688
  ]
  edge [
    source 16
    target 1689
  ]
  edge [
    source 16
    target 1690
  ]
  edge [
    source 16
    target 1691
  ]
  edge [
    source 16
    target 1692
  ]
  edge [
    source 16
    target 1693
  ]
  edge [
    source 16
    target 1694
  ]
  edge [
    source 16
    target 1695
  ]
  edge [
    source 16
    target 1696
  ]
  edge [
    source 16
    target 1697
  ]
  edge [
    source 16
    target 1698
  ]
  edge [
    source 16
    target 1699
  ]
  edge [
    source 16
    target 1700
  ]
  edge [
    source 16
    target 1701
  ]
  edge [
    source 16
    target 1702
  ]
  edge [
    source 16
    target 1703
  ]
  edge [
    source 16
    target 1704
  ]
  edge [
    source 16
    target 1705
  ]
  edge [
    source 16
    target 1706
  ]
  edge [
    source 16
    target 1707
  ]
  edge [
    source 16
    target 1708
  ]
  edge [
    source 16
    target 1709
  ]
  edge [
    source 16
    target 1238
  ]
  edge [
    source 16
    target 1710
  ]
  edge [
    source 16
    target 1711
  ]
  edge [
    source 16
    target 1712
  ]
  edge [
    source 16
    target 1713
  ]
  edge [
    source 16
    target 1714
  ]
  edge [
    source 16
    target 1715
  ]
  edge [
    source 16
    target 1716
  ]
  edge [
    source 16
    target 1717
  ]
  edge [
    source 16
    target 1718
  ]
  edge [
    source 16
    target 1719
  ]
  edge [
    source 16
    target 1720
  ]
  edge [
    source 16
    target 1721
  ]
  edge [
    source 16
    target 1722
  ]
  edge [
    source 16
    target 1723
  ]
  edge [
    source 16
    target 1724
  ]
  edge [
    source 16
    target 1725
  ]
  edge [
    source 16
    target 1726
  ]
  edge [
    source 16
    target 1727
  ]
  edge [
    source 16
    target 1728
  ]
  edge [
    source 16
    target 1729
  ]
  edge [
    source 16
    target 1730
  ]
  edge [
    source 16
    target 1731
  ]
  edge [
    source 16
    target 1732
  ]
  edge [
    source 16
    target 1733
  ]
  edge [
    source 16
    target 1734
  ]
  edge [
    source 16
    target 1735
  ]
  edge [
    source 16
    target 1736
  ]
  edge [
    source 16
    target 1737
  ]
  edge [
    source 16
    target 1738
  ]
  edge [
    source 16
    target 1739
  ]
  edge [
    source 16
    target 1740
  ]
  edge [
    source 16
    target 1741
  ]
  edge [
    source 16
    target 1742
  ]
  edge [
    source 16
    target 1743
  ]
  edge [
    source 16
    target 1744
  ]
  edge [
    source 16
    target 1745
  ]
  edge [
    source 16
    target 1746
  ]
  edge [
    source 16
    target 1747
  ]
  edge [
    source 16
    target 1748
  ]
  edge [
    source 16
    target 1749
  ]
  edge [
    source 16
    target 1750
  ]
  edge [
    source 16
    target 1751
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 1752
  ]
  edge [
    source 16
    target 1753
  ]
  edge [
    source 16
    target 1754
  ]
  edge [
    source 16
    target 1755
  ]
  edge [
    source 16
    target 1756
  ]
  edge [
    source 16
    target 1757
  ]
  edge [
    source 16
    target 1758
  ]
  edge [
    source 16
    target 1759
  ]
  edge [
    source 16
    target 1760
  ]
  edge [
    source 16
    target 1761
  ]
  edge [
    source 16
    target 1762
  ]
  edge [
    source 16
    target 1763
  ]
  edge [
    source 16
    target 1764
  ]
  edge [
    source 16
    target 1765
  ]
  edge [
    source 16
    target 1766
  ]
  edge [
    source 16
    target 1767
  ]
  edge [
    source 16
    target 1768
  ]
  edge [
    source 16
    target 1769
  ]
  edge [
    source 16
    target 1770
  ]
  edge [
    source 16
    target 1771
  ]
  edge [
    source 16
    target 1772
  ]
  edge [
    source 16
    target 1773
  ]
  edge [
    source 16
    target 1774
  ]
  edge [
    source 16
    target 1775
  ]
  edge [
    source 16
    target 1776
  ]
  edge [
    source 16
    target 1777
  ]
  edge [
    source 16
    target 1778
  ]
  edge [
    source 16
    target 1779
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 1780
  ]
  edge [
    source 16
    target 1781
  ]
  edge [
    source 16
    target 1782
  ]
  edge [
    source 16
    target 1783
  ]
  edge [
    source 16
    target 1784
  ]
  edge [
    source 16
    target 1785
  ]
  edge [
    source 16
    target 1786
  ]
  edge [
    source 16
    target 1787
  ]
  edge [
    source 16
    target 1788
  ]
  edge [
    source 16
    target 1789
  ]
  edge [
    source 16
    target 1790
  ]
  edge [
    source 16
    target 1791
  ]
  edge [
    source 16
    target 1792
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 1793
  ]
  edge [
    source 16
    target 1794
  ]
  edge [
    source 16
    target 1795
  ]
  edge [
    source 16
    target 1796
  ]
  edge [
    source 16
    target 1797
  ]
  edge [
    source 16
    target 1798
  ]
  edge [
    source 16
    target 1799
  ]
  edge [
    source 16
    target 1800
  ]
  edge [
    source 16
    target 1801
  ]
  edge [
    source 16
    target 1802
  ]
  edge [
    source 16
    target 1803
  ]
  edge [
    source 16
    target 1804
  ]
  edge [
    source 16
    target 1805
  ]
  edge [
    source 16
    target 1806
  ]
  edge [
    source 16
    target 1807
  ]
  edge [
    source 16
    target 1808
  ]
  edge [
    source 16
    target 1809
  ]
  edge [
    source 16
    target 1810
  ]
  edge [
    source 16
    target 1811
  ]
  edge [
    source 16
    target 1812
  ]
  edge [
    source 16
    target 1813
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 1814
  ]
  edge [
    source 16
    target 1815
  ]
  edge [
    source 16
    target 1816
  ]
  edge [
    source 16
    target 1817
  ]
  edge [
    source 16
    target 1818
  ]
  edge [
    source 16
    target 1819
  ]
  edge [
    source 16
    target 1820
  ]
  edge [
    source 16
    target 1821
  ]
  edge [
    source 16
    target 1822
  ]
  edge [
    source 16
    target 1823
  ]
  edge [
    source 16
    target 1824
  ]
  edge [
    source 16
    target 1825
  ]
  edge [
    source 16
    target 1826
  ]
  edge [
    source 16
    target 1827
  ]
  edge [
    source 16
    target 1828
  ]
  edge [
    source 16
    target 1829
  ]
  edge [
    source 16
    target 1830
  ]
  edge [
    source 16
    target 1831
  ]
  edge [
    source 16
    target 1832
  ]
  edge [
    source 16
    target 1833
  ]
  edge [
    source 16
    target 1834
  ]
  edge [
    source 16
    target 1835
  ]
  edge [
    source 16
    target 1836
  ]
  edge [
    source 16
    target 1837
  ]
  edge [
    source 16
    target 1838
  ]
  edge [
    source 16
    target 1839
  ]
  edge [
    source 16
    target 1840
  ]
  edge [
    source 16
    target 1841
  ]
  edge [
    source 16
    target 1842
  ]
  edge [
    source 16
    target 1843
  ]
  edge [
    source 16
    target 1844
  ]
  edge [
    source 16
    target 1845
  ]
  edge [
    source 16
    target 1846
  ]
  edge [
    source 16
    target 1847
  ]
  edge [
    source 16
    target 1848
  ]
  edge [
    source 16
    target 1849
  ]
  edge [
    source 16
    target 1850
  ]
  edge [
    source 16
    target 1851
  ]
  edge [
    source 16
    target 1852
  ]
  edge [
    source 16
    target 1853
  ]
  edge [
    source 16
    target 1854
  ]
  edge [
    source 16
    target 1855
  ]
  edge [
    source 16
    target 1856
  ]
  edge [
    source 16
    target 1857
  ]
  edge [
    source 16
    target 1858
  ]
  edge [
    source 16
    target 1859
  ]
  edge [
    source 16
    target 1860
  ]
  edge [
    source 16
    target 1861
  ]
  edge [
    source 16
    target 1862
  ]
  edge [
    source 16
    target 1863
  ]
  edge [
    source 16
    target 1864
  ]
  edge [
    source 16
    target 1865
  ]
  edge [
    source 16
    target 1866
  ]
  edge [
    source 16
    target 1867
  ]
  edge [
    source 16
    target 1868
  ]
  edge [
    source 16
    target 1869
  ]
  edge [
    source 16
    target 1870
  ]
  edge [
    source 16
    target 1871
  ]
  edge [
    source 16
    target 1872
  ]
  edge [
    source 16
    target 1873
  ]
  edge [
    source 16
    target 1874
  ]
  edge [
    source 16
    target 1875
  ]
  edge [
    source 16
    target 1876
  ]
  edge [
    source 16
    target 1877
  ]
  edge [
    source 16
    target 1878
  ]
  edge [
    source 16
    target 1879
  ]
  edge [
    source 16
    target 1880
  ]
  edge [
    source 16
    target 1881
  ]
  edge [
    source 16
    target 1882
  ]
  edge [
    source 16
    target 1883
  ]
  edge [
    source 16
    target 1884
  ]
  edge [
    source 16
    target 1885
  ]
  edge [
    source 16
    target 1886
  ]
  edge [
    source 16
    target 1887
  ]
  edge [
    source 16
    target 1888
  ]
  edge [
    source 16
    target 1889
  ]
  edge [
    source 16
    target 1890
  ]
  edge [
    source 16
    target 1891
  ]
  edge [
    source 16
    target 1892
  ]
  edge [
    source 16
    target 1893
  ]
  edge [
    source 16
    target 1894
  ]
  edge [
    source 16
    target 1895
  ]
  edge [
    source 16
    target 1896
  ]
  edge [
    source 16
    target 1897
  ]
  edge [
    source 16
    target 1898
  ]
  edge [
    source 16
    target 1899
  ]
  edge [
    source 16
    target 1900
  ]
  edge [
    source 16
    target 1901
  ]
  edge [
    source 16
    target 1902
  ]
  edge [
    source 16
    target 1903
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 1904
  ]
  edge [
    source 16
    target 1905
  ]
  edge [
    source 16
    target 1906
  ]
  edge [
    source 16
    target 1907
  ]
  edge [
    source 16
    target 1908
  ]
  edge [
    source 16
    target 1909
  ]
  edge [
    source 16
    target 1910
  ]
  edge [
    source 16
    target 1911
  ]
  edge [
    source 16
    target 1912
  ]
  edge [
    source 16
    target 1913
  ]
  edge [
    source 16
    target 1914
  ]
  edge [
    source 16
    target 1915
  ]
  edge [
    source 16
    target 1916
  ]
  edge [
    source 16
    target 1917
  ]
  edge [
    source 16
    target 1918
  ]
  edge [
    source 16
    target 1919
  ]
  edge [
    source 16
    target 1920
  ]
  edge [
    source 16
    target 1921
  ]
  edge [
    source 16
    target 1922
  ]
  edge [
    source 16
    target 1923
  ]
  edge [
    source 16
    target 1924
  ]
  edge [
    source 16
    target 1925
  ]
  edge [
    source 16
    target 1926
  ]
  edge [
    source 16
    target 1927
  ]
  edge [
    source 16
    target 1928
  ]
  edge [
    source 16
    target 1929
  ]
  edge [
    source 16
    target 1930
  ]
  edge [
    source 16
    target 1931
  ]
  edge [
    source 16
    target 1932
  ]
  edge [
    source 16
    target 1933
  ]
  edge [
    source 16
    target 1934
  ]
  edge [
    source 16
    target 1935
  ]
  edge [
    source 16
    target 1936
  ]
  edge [
    source 16
    target 1937
  ]
  edge [
    source 16
    target 1938
  ]
  edge [
    source 16
    target 1939
  ]
  edge [
    source 16
    target 1940
  ]
  edge [
    source 16
    target 1941
  ]
  edge [
    source 16
    target 1942
  ]
  edge [
    source 16
    target 1943
  ]
  edge [
    source 16
    target 1944
  ]
  edge [
    source 16
    target 1945
  ]
  edge [
    source 16
    target 1946
  ]
  edge [
    source 16
    target 1947
  ]
  edge [
    source 16
    target 1948
  ]
  edge [
    source 16
    target 1949
  ]
  edge [
    source 16
    target 1950
  ]
  edge [
    source 16
    target 1951
  ]
  edge [
    source 16
    target 1952
  ]
  edge [
    source 16
    target 1953
  ]
  edge [
    source 16
    target 1954
  ]
  edge [
    source 16
    target 1955
  ]
  edge [
    source 16
    target 1956
  ]
  edge [
    source 16
    target 1957
  ]
  edge [
    source 16
    target 1958
  ]
  edge [
    source 16
    target 1959
  ]
  edge [
    source 16
    target 1960
  ]
  edge [
    source 16
    target 1961
  ]
  edge [
    source 16
    target 1962
  ]
  edge [
    source 16
    target 1963
  ]
  edge [
    source 16
    target 1964
  ]
  edge [
    source 16
    target 1965
  ]
  edge [
    source 16
    target 1966
  ]
  edge [
    source 16
    target 1967
  ]
  edge [
    source 16
    target 1968
  ]
  edge [
    source 16
    target 1969
  ]
  edge [
    source 16
    target 1970
  ]
  edge [
    source 16
    target 1971
  ]
  edge [
    source 16
    target 1972
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 1973
  ]
  edge [
    source 16
    target 1974
  ]
  edge [
    source 16
    target 1975
  ]
  edge [
    source 16
    target 1976
  ]
  edge [
    source 16
    target 1977
  ]
  edge [
    source 16
    target 1978
  ]
  edge [
    source 16
    target 1979
  ]
  edge [
    source 16
    target 1980
  ]
  edge [
    source 16
    target 1981
  ]
  edge [
    source 16
    target 1982
  ]
  edge [
    source 16
    target 1983
  ]
  edge [
    source 16
    target 1984
  ]
  edge [
    source 16
    target 1985
  ]
  edge [
    source 16
    target 1986
  ]
  edge [
    source 16
    target 1987
  ]
  edge [
    source 16
    target 1988
  ]
  edge [
    source 16
    target 1989
  ]
  edge [
    source 16
    target 1990
  ]
  edge [
    source 16
    target 1991
  ]
  edge [
    source 16
    target 1992
  ]
  edge [
    source 16
    target 1993
  ]
  edge [
    source 16
    target 1994
  ]
  edge [
    source 16
    target 1995
  ]
  edge [
    source 16
    target 1996
  ]
  edge [
    source 16
    target 1997
  ]
  edge [
    source 16
    target 1998
  ]
  edge [
    source 16
    target 1999
  ]
  edge [
    source 16
    target 2000
  ]
  edge [
    source 16
    target 2001
  ]
  edge [
    source 16
    target 2002
  ]
  edge [
    source 16
    target 2003
  ]
  edge [
    source 16
    target 2004
  ]
  edge [
    source 16
    target 2005
  ]
  edge [
    source 16
    target 2006
  ]
  edge [
    source 16
    target 2007
  ]
  edge [
    source 16
    target 2008
  ]
  edge [
    source 16
    target 2009
  ]
  edge [
    source 16
    target 2010
  ]
  edge [
    source 16
    target 2011
  ]
  edge [
    source 16
    target 2012
  ]
  edge [
    source 16
    target 2013
  ]
  edge [
    source 16
    target 2014
  ]
  edge [
    source 16
    target 2015
  ]
  edge [
    source 16
    target 2016
  ]
  edge [
    source 16
    target 2017
  ]
  edge [
    source 16
    target 2018
  ]
  edge [
    source 16
    target 2019
  ]
  edge [
    source 16
    target 2020
  ]
  edge [
    source 16
    target 2021
  ]
  edge [
    source 16
    target 2022
  ]
  edge [
    source 16
    target 2023
  ]
  edge [
    source 16
    target 2024
  ]
  edge [
    source 16
    target 2025
  ]
  edge [
    source 16
    target 2026
  ]
  edge [
    source 16
    target 2027
  ]
  edge [
    source 16
    target 2028
  ]
  edge [
    source 16
    target 2029
  ]
  edge [
    source 16
    target 2030
  ]
  edge [
    source 16
    target 2031
  ]
  edge [
    source 16
    target 2032
  ]
  edge [
    source 16
    target 2033
  ]
  edge [
    source 16
    target 2034
  ]
  edge [
    source 16
    target 2035
  ]
  edge [
    source 16
    target 2036
  ]
  edge [
    source 16
    target 2037
  ]
  edge [
    source 16
    target 2038
  ]
  edge [
    source 16
    target 2039
  ]
  edge [
    source 16
    target 2040
  ]
  edge [
    source 16
    target 2041
  ]
  edge [
    source 16
    target 2042
  ]
  edge [
    source 16
    target 2043
  ]
  edge [
    source 16
    target 2044
  ]
  edge [
    source 16
    target 2045
  ]
  edge [
    source 16
    target 2046
  ]
  edge [
    source 16
    target 2047
  ]
  edge [
    source 16
    target 2048
  ]
  edge [
    source 16
    target 2049
  ]
  edge [
    source 16
    target 2050
  ]
  edge [
    source 16
    target 2051
  ]
  edge [
    source 16
    target 2052
  ]
  edge [
    source 16
    target 2053
  ]
  edge [
    source 16
    target 2054
  ]
  edge [
    source 16
    target 2055
  ]
  edge [
    source 16
    target 2056
  ]
  edge [
    source 16
    target 2057
  ]
  edge [
    source 16
    target 2058
  ]
  edge [
    source 16
    target 2059
  ]
  edge [
    source 16
    target 2060
  ]
  edge [
    source 16
    target 2061
  ]
  edge [
    source 16
    target 2062
  ]
  edge [
    source 16
    target 2063
  ]
  edge [
    source 16
    target 2064
  ]
  edge [
    source 16
    target 2065
  ]
  edge [
    source 16
    target 2066
  ]
  edge [
    source 16
    target 2067
  ]
  edge [
    source 16
    target 2068
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 2069
  ]
  edge [
    source 17
    target 2070
  ]
  edge [
    source 17
    target 2071
  ]
  edge [
    source 17
    target 2072
  ]
  edge [
    source 17
    target 559
  ]
  edge [
    source 17
    target 2073
  ]
  edge [
    source 17
    target 2074
  ]
  edge [
    source 17
    target 2075
  ]
  edge [
    source 17
    target 2076
  ]
  edge [
    source 17
    target 2077
  ]
  edge [
    source 17
    target 2078
  ]
  edge [
    source 17
    target 2079
  ]
  edge [
    source 17
    target 2080
  ]
  edge [
    source 17
    target 2081
  ]
  edge [
    source 17
    target 2082
  ]
  edge [
    source 17
    target 2083
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 2084
  ]
  edge [
    source 19
    target 2085
  ]
  edge [
    source 19
    target 2086
  ]
  edge [
    source 19
    target 2087
  ]
  edge [
    source 19
    target 2088
  ]
  edge [
    source 19
    target 2089
  ]
  edge [
    source 19
    target 2090
  ]
  edge [
    source 19
    target 2091
  ]
  edge [
    source 19
    target 2092
  ]
  edge [
    source 19
    target 2093
  ]
  edge [
    source 19
    target 2094
  ]
  edge [
    source 19
    target 2095
  ]
  edge [
    source 19
    target 2096
  ]
  edge [
    source 19
    target 2097
  ]
  edge [
    source 19
    target 2098
  ]
  edge [
    source 19
    target 2099
  ]
  edge [
    source 19
    target 2100
  ]
  edge [
    source 19
    target 2101
  ]
  edge [
    source 19
    target 2102
  ]
  edge [
    source 19
    target 2103
  ]
  edge [
    source 19
    target 2104
  ]
  edge [
    source 19
    target 2105
  ]
  edge [
    source 19
    target 2106
  ]
  edge [
    source 19
    target 2107
  ]
  edge [
    source 19
    target 2108
  ]
  edge [
    source 19
    target 2109
  ]
  edge [
    source 19
    target 2110
  ]
  edge [
    source 19
    target 2111
  ]
  edge [
    source 19
    target 2112
  ]
  edge [
    source 19
    target 2113
  ]
  edge [
    source 19
    target 2114
  ]
  edge [
    source 19
    target 2115
  ]
  edge [
    source 19
    target 2116
  ]
  edge [
    source 19
    target 2117
  ]
  edge [
    source 19
    target 2118
  ]
  edge [
    source 19
    target 2119
  ]
  edge [
    source 19
    target 2120
  ]
  edge [
    source 19
    target 2121
  ]
  edge [
    source 19
    target 2122
  ]
  edge [
    source 19
    target 2123
  ]
  edge [
    source 19
    target 2124
  ]
  edge [
    source 19
    target 2125
  ]
  edge [
    source 19
    target 2126
  ]
  edge [
    source 19
    target 2127
  ]
  edge [
    source 19
    target 2128
  ]
  edge [
    source 19
    target 2129
  ]
  edge [
    source 19
    target 2130
  ]
  edge [
    source 19
    target 2131
  ]
  edge [
    source 19
    target 2132
  ]
  edge [
    source 19
    target 2133
  ]
  edge [
    source 19
    target 2134
  ]
  edge [
    source 19
    target 2135
  ]
  edge [
    source 19
    target 2136
  ]
  edge [
    source 19
    target 2137
  ]
  edge [
    source 19
    target 2138
  ]
  edge [
    source 19
    target 2139
  ]
  edge [
    source 19
    target 2140
  ]
  edge [
    source 19
    target 2141
  ]
  edge [
    source 19
    target 2142
  ]
  edge [
    source 19
    target 2143
  ]
  edge [
    source 19
    target 2144
  ]
  edge [
    source 19
    target 2145
  ]
  edge [
    source 19
    target 2146
  ]
  edge [
    source 19
    target 2147
  ]
  edge [
    source 19
    target 2148
  ]
  edge [
    source 19
    target 2149
  ]
  edge [
    source 19
    target 2150
  ]
]
