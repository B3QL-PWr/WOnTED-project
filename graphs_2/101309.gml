graph [
  node [
    id 0
    label "historia"
    origin "text"
  ]
  node [
    id 1
    label "historiografia"
  ]
  node [
    id 2
    label "nauka_humanistyczna"
  ]
  node [
    id 3
    label "nautologia"
  ]
  node [
    id 4
    label "przedmiot"
  ]
  node [
    id 5
    label "epigrafika"
  ]
  node [
    id 6
    label "muzealnictwo"
  ]
  node [
    id 7
    label "report"
  ]
  node [
    id 8
    label "hista"
  ]
  node [
    id 9
    label "przebiec"
  ]
  node [
    id 10
    label "zabytkoznawstwo"
  ]
  node [
    id 11
    label "historia_gospodarcza"
  ]
  node [
    id 12
    label "motyw"
  ]
  node [
    id 13
    label "kierunek"
  ]
  node [
    id 14
    label "varsavianistyka"
  ]
  node [
    id 15
    label "filigranistyka"
  ]
  node [
    id 16
    label "neografia"
  ]
  node [
    id 17
    label "prezentyzm"
  ]
  node [
    id 18
    label "genealogia"
  ]
  node [
    id 19
    label "ikonografia"
  ]
  node [
    id 20
    label "bizantynistyka"
  ]
  node [
    id 21
    label "epoka"
  ]
  node [
    id 22
    label "historia_sztuki"
  ]
  node [
    id 23
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 24
    label "ruralistyka"
  ]
  node [
    id 25
    label "annalistyka"
  ]
  node [
    id 26
    label "charakter"
  ]
  node [
    id 27
    label "papirologia"
  ]
  node [
    id 28
    label "heraldyka"
  ]
  node [
    id 29
    label "archiwistyka"
  ]
  node [
    id 30
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 31
    label "dyplomatyka"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "numizmatyka"
  ]
  node [
    id 34
    label "chronologia"
  ]
  node [
    id 35
    label "wypowied&#378;"
  ]
  node [
    id 36
    label "historyka"
  ]
  node [
    id 37
    label "prozopografia"
  ]
  node [
    id 38
    label "sfragistyka"
  ]
  node [
    id 39
    label "weksylologia"
  ]
  node [
    id 40
    label "paleografia"
  ]
  node [
    id 41
    label "mediewistyka"
  ]
  node [
    id 42
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 43
    label "przebiegni&#281;cie"
  ]
  node [
    id 44
    label "fabu&#322;a"
  ]
  node [
    id 45
    label "koleje_losu"
  ]
  node [
    id 46
    label "&#380;ycie"
  ]
  node [
    id 47
    label "czas"
  ]
  node [
    id 48
    label "zboczenie"
  ]
  node [
    id 49
    label "om&#243;wienie"
  ]
  node [
    id 50
    label "sponiewieranie"
  ]
  node [
    id 51
    label "discipline"
  ]
  node [
    id 52
    label "rzecz"
  ]
  node [
    id 53
    label "omawia&#263;"
  ]
  node [
    id 54
    label "kr&#261;&#380;enie"
  ]
  node [
    id 55
    label "tre&#347;&#263;"
  ]
  node [
    id 56
    label "robienie"
  ]
  node [
    id 57
    label "sponiewiera&#263;"
  ]
  node [
    id 58
    label "element"
  ]
  node [
    id 59
    label "entity"
  ]
  node [
    id 60
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 61
    label "tematyka"
  ]
  node [
    id 62
    label "w&#261;tek"
  ]
  node [
    id 63
    label "zbaczanie"
  ]
  node [
    id 64
    label "program_nauczania"
  ]
  node [
    id 65
    label "om&#243;wi&#263;"
  ]
  node [
    id 66
    label "omawianie"
  ]
  node [
    id 67
    label "thing"
  ]
  node [
    id 68
    label "kultura"
  ]
  node [
    id 69
    label "istota"
  ]
  node [
    id 70
    label "zbacza&#263;"
  ]
  node [
    id 71
    label "zboczy&#263;"
  ]
  node [
    id 72
    label "pos&#322;uchanie"
  ]
  node [
    id 73
    label "s&#261;d"
  ]
  node [
    id 74
    label "sparafrazowanie"
  ]
  node [
    id 75
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 76
    label "strawestowa&#263;"
  ]
  node [
    id 77
    label "sparafrazowa&#263;"
  ]
  node [
    id 78
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 79
    label "trawestowa&#263;"
  ]
  node [
    id 80
    label "sformu&#322;owanie"
  ]
  node [
    id 81
    label "parafrazowanie"
  ]
  node [
    id 82
    label "ozdobnik"
  ]
  node [
    id 83
    label "delimitacja"
  ]
  node [
    id 84
    label "parafrazowa&#263;"
  ]
  node [
    id 85
    label "stylizacja"
  ]
  node [
    id 86
    label "komunikat"
  ]
  node [
    id 87
    label "trawestowanie"
  ]
  node [
    id 88
    label "strawestowanie"
  ]
  node [
    id 89
    label "rezultat"
  ]
  node [
    id 90
    label "przebieg"
  ]
  node [
    id 91
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 92
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 93
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 94
    label "praktyka"
  ]
  node [
    id 95
    label "system"
  ]
  node [
    id 96
    label "przeorientowywanie"
  ]
  node [
    id 97
    label "studia"
  ]
  node [
    id 98
    label "linia"
  ]
  node [
    id 99
    label "bok"
  ]
  node [
    id 100
    label "skr&#281;canie"
  ]
  node [
    id 101
    label "skr&#281;ca&#263;"
  ]
  node [
    id 102
    label "przeorientowywa&#263;"
  ]
  node [
    id 103
    label "orientowanie"
  ]
  node [
    id 104
    label "skr&#281;ci&#263;"
  ]
  node [
    id 105
    label "przeorientowanie"
  ]
  node [
    id 106
    label "zorientowanie"
  ]
  node [
    id 107
    label "przeorientowa&#263;"
  ]
  node [
    id 108
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 109
    label "metoda"
  ]
  node [
    id 110
    label "ty&#322;"
  ]
  node [
    id 111
    label "zorientowa&#263;"
  ]
  node [
    id 112
    label "g&#243;ra"
  ]
  node [
    id 113
    label "orientowa&#263;"
  ]
  node [
    id 114
    label "spos&#243;b"
  ]
  node [
    id 115
    label "ideologia"
  ]
  node [
    id 116
    label "orientacja"
  ]
  node [
    id 117
    label "prz&#243;d"
  ]
  node [
    id 118
    label "bearing"
  ]
  node [
    id 119
    label "skr&#281;cenie"
  ]
  node [
    id 120
    label "aalen"
  ]
  node [
    id 121
    label "jura_wczesna"
  ]
  node [
    id 122
    label "holocen"
  ]
  node [
    id 123
    label "pliocen"
  ]
  node [
    id 124
    label "plejstocen"
  ]
  node [
    id 125
    label "paleocen"
  ]
  node [
    id 126
    label "dzieje"
  ]
  node [
    id 127
    label "bajos"
  ]
  node [
    id 128
    label "kelowej"
  ]
  node [
    id 129
    label "eocen"
  ]
  node [
    id 130
    label "jednostka_geologiczna"
  ]
  node [
    id 131
    label "okres"
  ]
  node [
    id 132
    label "schy&#322;ek"
  ]
  node [
    id 133
    label "miocen"
  ]
  node [
    id 134
    label "&#347;rodkowy_trias"
  ]
  node [
    id 135
    label "term"
  ]
  node [
    id 136
    label "Zeitgeist"
  ]
  node [
    id 137
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 138
    label "wczesny_trias"
  ]
  node [
    id 139
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 140
    label "jura_&#347;rodkowa"
  ]
  node [
    id 141
    label "oligocen"
  ]
  node [
    id 142
    label "w&#281;ze&#322;"
  ]
  node [
    id 143
    label "perypetia"
  ]
  node [
    id 144
    label "opowiadanie"
  ]
  node [
    id 145
    label "datacja"
  ]
  node [
    id 146
    label "dendrochronologia"
  ]
  node [
    id 147
    label "kolejno&#347;&#263;"
  ]
  node [
    id 148
    label "plastyka"
  ]
  node [
    id 149
    label "&#347;redniowiecze"
  ]
  node [
    id 150
    label "descendencja"
  ]
  node [
    id 151
    label "drzewo_genealogiczne"
  ]
  node [
    id 152
    label "procedencja"
  ]
  node [
    id 153
    label "pochodzenie"
  ]
  node [
    id 154
    label "medal"
  ]
  node [
    id 155
    label "kolekcjonerstwo"
  ]
  node [
    id 156
    label "numismatics"
  ]
  node [
    id 157
    label "archeologia"
  ]
  node [
    id 158
    label "archiwoznawstwo"
  ]
  node [
    id 159
    label "Byzantine_Empire"
  ]
  node [
    id 160
    label "pismo"
  ]
  node [
    id 161
    label "brachygrafia"
  ]
  node [
    id 162
    label "architektura"
  ]
  node [
    id 163
    label "nauka"
  ]
  node [
    id 164
    label "oksza"
  ]
  node [
    id 165
    label "pas"
  ]
  node [
    id 166
    label "s&#322;up"
  ]
  node [
    id 167
    label "barwa_heraldyczna"
  ]
  node [
    id 168
    label "herb"
  ]
  node [
    id 169
    label "or&#281;&#380;"
  ]
  node [
    id 170
    label "museum"
  ]
  node [
    id 171
    label "bibliologia"
  ]
  node [
    id 172
    label "historiography"
  ]
  node [
    id 173
    label "pi&#347;miennictwo"
  ]
  node [
    id 174
    label "metodologia"
  ]
  node [
    id 175
    label "fraza"
  ]
  node [
    id 176
    label "temat"
  ]
  node [
    id 177
    label "wydarzenie"
  ]
  node [
    id 178
    label "melodia"
  ]
  node [
    id 179
    label "cecha"
  ]
  node [
    id 180
    label "przyczyna"
  ]
  node [
    id 181
    label "sytuacja"
  ]
  node [
    id 182
    label "ozdoba"
  ]
  node [
    id 183
    label "umowa"
  ]
  node [
    id 184
    label "cover"
  ]
  node [
    id 185
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 186
    label "zbi&#243;r"
  ]
  node [
    id 187
    label "cz&#322;owiek"
  ]
  node [
    id 188
    label "osobowo&#347;&#263;"
  ]
  node [
    id 189
    label "psychika"
  ]
  node [
    id 190
    label "posta&#263;"
  ]
  node [
    id 191
    label "kompleksja"
  ]
  node [
    id 192
    label "fizjonomia"
  ]
  node [
    id 193
    label "zjawisko"
  ]
  node [
    id 194
    label "activity"
  ]
  node [
    id 195
    label "bezproblemowy"
  ]
  node [
    id 196
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 197
    label "przeby&#263;"
  ]
  node [
    id 198
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 199
    label "run"
  ]
  node [
    id 200
    label "proceed"
  ]
  node [
    id 201
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 202
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 203
    label "przemierzy&#263;"
  ]
  node [
    id 204
    label "fly"
  ]
  node [
    id 205
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 206
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 207
    label "przesun&#261;&#263;"
  ]
  node [
    id 208
    label "przemkni&#281;cie"
  ]
  node [
    id 209
    label "zabrzmienie"
  ]
  node [
    id 210
    label "przebycie"
  ]
  node [
    id 211
    label "zdarzenie_si&#281;"
  ]
  node [
    id 212
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 213
    label "w&#322;oski"
  ]
  node [
    id 214
    label "republika"
  ]
  node [
    id 215
    label "socjalny"
  ]
  node [
    id 216
    label "Maria"
  ]
  node [
    id 217
    label "Carlonim"
  ]
  node [
    id 218
    label "4"
  ]
  node [
    id 219
    label "dywizja"
  ]
  node [
    id 220
    label "bersalier"
  ]
  node [
    id 221
    label "Italia"
  ]
  node [
    id 222
    label "m&#281;ski"
  ]
  node [
    id 223
    label "Carloni"
  ]
  node [
    id 224
    label "alpejski"
  ]
  node [
    id 225
    label "Monterosa"
  ]
  node [
    id 226
    label "1"
  ]
  node [
    id 227
    label "Guido"
  ]
  node [
    id 228
    label "Manardi"
  ]
  node [
    id 229
    label "got&#243;w"
  ]
  node [
    id 230
    label "ii"
  ]
  node [
    id 231
    label "batalion"
  ]
  node [
    id 232
    label "3"
  ]
  node [
    id 233
    label "pu&#322;k"
  ]
  node [
    id 234
    label "i"
  ]
  node [
    id 235
    label "artyleria"
  ]
  node [
    id 236
    label "Kampfgruppe"
  ]
  node [
    id 237
    label "Ferrario"
  ]
  node [
    id 238
    label "grupa"
  ]
  node [
    id 239
    label "artyleryjski"
  ]
  node [
    id 240
    label "Goffredo"
  ]
  node [
    id 241
    label "Mameli"
  ]
  node [
    id 242
    label "Bergamo"
  ]
  node [
    id 243
    label "2"
  ]
  node [
    id 244
    label "Cacciatori"
  ]
  node [
    id 245
    label "degli"
  ]
  node [
    id 246
    label "Appenninini"
  ]
  node [
    id 247
    label "148"
  ]
  node [
    id 248
    label "piechota"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 98
    target 229
  ]
  edge [
    source 213
    target 214
  ]
  edge [
    source 213
    target 215
  ]
  edge [
    source 214
    target 215
  ]
  edge [
    source 216
    target 217
  ]
  edge [
    source 218
    target 219
  ]
  edge [
    source 218
    target 220
  ]
  edge [
    source 218
    target 221
  ]
  edge [
    source 218
    target 224
  ]
  edge [
    source 218
    target 225
  ]
  edge [
    source 218
    target 233
  ]
  edge [
    source 218
    target 235
  ]
  edge [
    source 219
    target 220
  ]
  edge [
    source 219
    target 221
  ]
  edge [
    source 219
    target 224
  ]
  edge [
    source 219
    target 225
  ]
  edge [
    source 219
    target 226
  ]
  edge [
    source 219
    target 247
  ]
  edge [
    source 219
    target 248
  ]
  edge [
    source 220
    target 221
  ]
  edge [
    source 220
    target 226
  ]
  edge [
    source 220
    target 232
  ]
  edge [
    source 220
    target 233
  ]
  edge [
    source 221
    target 226
  ]
  edge [
    source 222
    target 223
  ]
  edge [
    source 224
    target 225
  ]
  edge [
    source 224
    target 238
  ]
  edge [
    source 224
    target 239
  ]
  edge [
    source 224
    target 242
  ]
  edge [
    source 227
    target 228
  ]
  edge [
    source 230
    target 231
  ]
  edge [
    source 230
    target 238
  ]
  edge [
    source 230
    target 239
  ]
  edge [
    source 231
    target 234
  ]
  edge [
    source 231
    target 240
  ]
  edge [
    source 231
    target 241
  ]
  edge [
    source 232
    target 233
  ]
  edge [
    source 233
    target 235
  ]
  edge [
    source 233
    target 243
  ]
  edge [
    source 233
    target 244
  ]
  edge [
    source 233
    target 245
  ]
  edge [
    source 233
    target 246
  ]
  edge [
    source 236
    target 237
  ]
  edge [
    source 238
    target 239
  ]
  edge [
    source 238
    target 242
  ]
  edge [
    source 239
    target 242
  ]
  edge [
    source 240
    target 241
  ]
  edge [
    source 243
    target 244
  ]
  edge [
    source 243
    target 245
  ]
  edge [
    source 243
    target 246
  ]
  edge [
    source 244
    target 245
  ]
  edge [
    source 244
    target 246
  ]
  edge [
    source 245
    target 246
  ]
  edge [
    source 247
    target 248
  ]
]
