graph [
  node [
    id 0
    label "kole&#380;anka"
    origin "text"
  ]
  node [
    id 1
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "udost&#281;pnienie"
    origin "text"
  ]
  node [
    id 3
    label "kuma"
  ]
  node [
    id 4
    label "kumostwo"
  ]
  node [
    id 5
    label "invite"
  ]
  node [
    id 6
    label "poleca&#263;"
  ]
  node [
    id 7
    label "trwa&#263;"
  ]
  node [
    id 8
    label "zaprasza&#263;"
  ]
  node [
    id 9
    label "zach&#281;ca&#263;"
  ]
  node [
    id 10
    label "suffice"
  ]
  node [
    id 11
    label "preach"
  ]
  node [
    id 12
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 14
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 15
    label "pies"
  ]
  node [
    id 16
    label "zezwala&#263;"
  ]
  node [
    id 17
    label "ask"
  ]
  node [
    id 18
    label "oferowa&#263;"
  ]
  node [
    id 19
    label "istnie&#263;"
  ]
  node [
    id 20
    label "pozostawa&#263;"
  ]
  node [
    id 21
    label "zostawa&#263;"
  ]
  node [
    id 22
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 23
    label "stand"
  ]
  node [
    id 24
    label "adhere"
  ]
  node [
    id 25
    label "ordynowa&#263;"
  ]
  node [
    id 26
    label "doradza&#263;"
  ]
  node [
    id 27
    label "wydawa&#263;"
  ]
  node [
    id 28
    label "m&#243;wi&#263;"
  ]
  node [
    id 29
    label "control"
  ]
  node [
    id 30
    label "charge"
  ]
  node [
    id 31
    label "placard"
  ]
  node [
    id 32
    label "powierza&#263;"
  ]
  node [
    id 33
    label "zadawa&#263;"
  ]
  node [
    id 34
    label "pozyskiwa&#263;"
  ]
  node [
    id 35
    label "act"
  ]
  node [
    id 36
    label "uznawa&#263;"
  ]
  node [
    id 37
    label "authorize"
  ]
  node [
    id 38
    label "piese&#322;"
  ]
  node [
    id 39
    label "cz&#322;owiek"
  ]
  node [
    id 40
    label "Cerber"
  ]
  node [
    id 41
    label "szczeka&#263;"
  ]
  node [
    id 42
    label "&#322;ajdak"
  ]
  node [
    id 43
    label "kabanos"
  ]
  node [
    id 44
    label "wyzwisko"
  ]
  node [
    id 45
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 46
    label "samiec"
  ]
  node [
    id 47
    label "spragniony"
  ]
  node [
    id 48
    label "policjant"
  ]
  node [
    id 49
    label "rakarz"
  ]
  node [
    id 50
    label "szczu&#263;"
  ]
  node [
    id 51
    label "wycie"
  ]
  node [
    id 52
    label "istota_&#380;ywa"
  ]
  node [
    id 53
    label "trufla"
  ]
  node [
    id 54
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 55
    label "zawy&#263;"
  ]
  node [
    id 56
    label "sobaka"
  ]
  node [
    id 57
    label "dogoterapia"
  ]
  node [
    id 58
    label "s&#322;u&#380;enie"
  ]
  node [
    id 59
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 60
    label "psowate"
  ]
  node [
    id 61
    label "wy&#263;"
  ]
  node [
    id 62
    label "szczucie"
  ]
  node [
    id 63
    label "czworon&#243;g"
  ]
  node [
    id 64
    label "umo&#380;liwienie"
  ]
  node [
    id 65
    label "mo&#380;liwy"
  ]
  node [
    id 66
    label "spowodowanie"
  ]
  node [
    id 67
    label "upowa&#380;nienie"
  ]
  node [
    id 68
    label "zrobienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
]
