graph [
  node [
    id 0
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "godzina"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "spacex"
    origin "text"
  ]
  node [
    id 5
    label "zaplanowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "misja"
    origin "text"
  ]
  node [
    id 8
    label "sso"
    origin "text"
  ]
  node [
    id 9
    label "slc"
    origin "text"
  ]
  node [
    id 10
    label "Barb&#243;rka"
  ]
  node [
    id 11
    label "miesi&#261;c"
  ]
  node [
    id 12
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 13
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 14
    label "Sylwester"
  ]
  node [
    id 15
    label "tydzie&#324;"
  ]
  node [
    id 16
    label "miech"
  ]
  node [
    id 17
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 18
    label "rok"
  ]
  node [
    id 19
    label "kalendy"
  ]
  node [
    id 20
    label "g&#243;rnik"
  ]
  node [
    id 21
    label "comber"
  ]
  node [
    id 22
    label "time"
  ]
  node [
    id 23
    label "doba"
  ]
  node [
    id 24
    label "p&#243;&#322;godzina"
  ]
  node [
    id 25
    label "jednostka_czasu"
  ]
  node [
    id 26
    label "minuta"
  ]
  node [
    id 27
    label "kwadrans"
  ]
  node [
    id 28
    label "poprzedzanie"
  ]
  node [
    id 29
    label "czasoprzestrze&#324;"
  ]
  node [
    id 30
    label "laba"
  ]
  node [
    id 31
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 32
    label "chronometria"
  ]
  node [
    id 33
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 34
    label "rachuba_czasu"
  ]
  node [
    id 35
    label "przep&#322;ywanie"
  ]
  node [
    id 36
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 37
    label "czasokres"
  ]
  node [
    id 38
    label "odczyt"
  ]
  node [
    id 39
    label "chwila"
  ]
  node [
    id 40
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 41
    label "dzieje"
  ]
  node [
    id 42
    label "kategoria_gramatyczna"
  ]
  node [
    id 43
    label "poprzedzenie"
  ]
  node [
    id 44
    label "trawienie"
  ]
  node [
    id 45
    label "pochodzi&#263;"
  ]
  node [
    id 46
    label "period"
  ]
  node [
    id 47
    label "okres_czasu"
  ]
  node [
    id 48
    label "poprzedza&#263;"
  ]
  node [
    id 49
    label "schy&#322;ek"
  ]
  node [
    id 50
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 51
    label "odwlekanie_si&#281;"
  ]
  node [
    id 52
    label "zegar"
  ]
  node [
    id 53
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 54
    label "czwarty_wymiar"
  ]
  node [
    id 55
    label "pochodzenie"
  ]
  node [
    id 56
    label "koniugacja"
  ]
  node [
    id 57
    label "Zeitgeist"
  ]
  node [
    id 58
    label "trawi&#263;"
  ]
  node [
    id 59
    label "pogoda"
  ]
  node [
    id 60
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 61
    label "poprzedzi&#263;"
  ]
  node [
    id 62
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 63
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 64
    label "time_period"
  ]
  node [
    id 65
    label "zapis"
  ]
  node [
    id 66
    label "sekunda"
  ]
  node [
    id 67
    label "jednostka"
  ]
  node [
    id 68
    label "stopie&#324;"
  ]
  node [
    id 69
    label "design"
  ]
  node [
    id 70
    label "noc"
  ]
  node [
    id 71
    label "dzie&#324;"
  ]
  node [
    id 72
    label "long_time"
  ]
  node [
    id 73
    label "jednostka_geologiczna"
  ]
  node [
    id 74
    label "blok"
  ]
  node [
    id 75
    label "handout"
  ]
  node [
    id 76
    label "pomiar"
  ]
  node [
    id 77
    label "lecture"
  ]
  node [
    id 78
    label "reading"
  ]
  node [
    id 79
    label "podawanie"
  ]
  node [
    id 80
    label "wyk&#322;ad"
  ]
  node [
    id 81
    label "potrzyma&#263;"
  ]
  node [
    id 82
    label "warunki"
  ]
  node [
    id 83
    label "pok&#243;j"
  ]
  node [
    id 84
    label "atak"
  ]
  node [
    id 85
    label "program"
  ]
  node [
    id 86
    label "zjawisko"
  ]
  node [
    id 87
    label "meteorology"
  ]
  node [
    id 88
    label "weather"
  ]
  node [
    id 89
    label "prognoza_meteorologiczna"
  ]
  node [
    id 90
    label "czas_wolny"
  ]
  node [
    id 91
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 92
    label "metrologia"
  ]
  node [
    id 93
    label "godzinnik"
  ]
  node [
    id 94
    label "bicie"
  ]
  node [
    id 95
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 96
    label "wahad&#322;o"
  ]
  node [
    id 97
    label "kurant"
  ]
  node [
    id 98
    label "cyferblat"
  ]
  node [
    id 99
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 100
    label "nabicie"
  ]
  node [
    id 101
    label "werk"
  ]
  node [
    id 102
    label "czasomierz"
  ]
  node [
    id 103
    label "tyka&#263;"
  ]
  node [
    id 104
    label "tykn&#261;&#263;"
  ]
  node [
    id 105
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 106
    label "urz&#261;dzenie"
  ]
  node [
    id 107
    label "kotwica"
  ]
  node [
    id 108
    label "fleksja"
  ]
  node [
    id 109
    label "liczba"
  ]
  node [
    id 110
    label "coupling"
  ]
  node [
    id 111
    label "osoba"
  ]
  node [
    id 112
    label "tryb"
  ]
  node [
    id 113
    label "czasownik"
  ]
  node [
    id 114
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 115
    label "orz&#281;sek"
  ]
  node [
    id 116
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 117
    label "zaczynanie_si&#281;"
  ]
  node [
    id 118
    label "str&#243;j"
  ]
  node [
    id 119
    label "wynikanie"
  ]
  node [
    id 120
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 121
    label "origin"
  ]
  node [
    id 122
    label "background"
  ]
  node [
    id 123
    label "geneza"
  ]
  node [
    id 124
    label "beginning"
  ]
  node [
    id 125
    label "digestion"
  ]
  node [
    id 126
    label "unicestwianie"
  ]
  node [
    id 127
    label "sp&#281;dzanie"
  ]
  node [
    id 128
    label "contemplation"
  ]
  node [
    id 129
    label "rozk&#322;adanie"
  ]
  node [
    id 130
    label "marnowanie"
  ]
  node [
    id 131
    label "proces_fizjologiczny"
  ]
  node [
    id 132
    label "przetrawianie"
  ]
  node [
    id 133
    label "perystaltyka"
  ]
  node [
    id 134
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 135
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 136
    label "przebywa&#263;"
  ]
  node [
    id 137
    label "pour"
  ]
  node [
    id 138
    label "carry"
  ]
  node [
    id 139
    label "sail"
  ]
  node [
    id 140
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 141
    label "go&#347;ci&#263;"
  ]
  node [
    id 142
    label "mija&#263;"
  ]
  node [
    id 143
    label "proceed"
  ]
  node [
    id 144
    label "odej&#347;cie"
  ]
  node [
    id 145
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 146
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 147
    label "zanikni&#281;cie"
  ]
  node [
    id 148
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 149
    label "ciecz"
  ]
  node [
    id 150
    label "opuszczenie"
  ]
  node [
    id 151
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 152
    label "departure"
  ]
  node [
    id 153
    label "oddalenie_si&#281;"
  ]
  node [
    id 154
    label "przeby&#263;"
  ]
  node [
    id 155
    label "min&#261;&#263;"
  ]
  node [
    id 156
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 157
    label "swimming"
  ]
  node [
    id 158
    label "zago&#347;ci&#263;"
  ]
  node [
    id 159
    label "cross"
  ]
  node [
    id 160
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 161
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 162
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 163
    label "zrobi&#263;"
  ]
  node [
    id 164
    label "opatrzy&#263;"
  ]
  node [
    id 165
    label "overwhelm"
  ]
  node [
    id 166
    label "opatrywa&#263;"
  ]
  node [
    id 167
    label "date"
  ]
  node [
    id 168
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 169
    label "wynika&#263;"
  ]
  node [
    id 170
    label "fall"
  ]
  node [
    id 171
    label "poby&#263;"
  ]
  node [
    id 172
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 173
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 174
    label "bolt"
  ]
  node [
    id 175
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 176
    label "spowodowa&#263;"
  ]
  node [
    id 177
    label "uda&#263;_si&#281;"
  ]
  node [
    id 178
    label "opatrzenie"
  ]
  node [
    id 179
    label "zdarzenie_si&#281;"
  ]
  node [
    id 180
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 181
    label "progress"
  ]
  node [
    id 182
    label "opatrywanie"
  ]
  node [
    id 183
    label "mini&#281;cie"
  ]
  node [
    id 184
    label "doznanie"
  ]
  node [
    id 185
    label "zaistnienie"
  ]
  node [
    id 186
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 187
    label "przebycie"
  ]
  node [
    id 188
    label "cruise"
  ]
  node [
    id 189
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 190
    label "usuwa&#263;"
  ]
  node [
    id 191
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 192
    label "lutowa&#263;"
  ]
  node [
    id 193
    label "marnowa&#263;"
  ]
  node [
    id 194
    label "przetrawia&#263;"
  ]
  node [
    id 195
    label "poch&#322;ania&#263;"
  ]
  node [
    id 196
    label "digest"
  ]
  node [
    id 197
    label "metal"
  ]
  node [
    id 198
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 199
    label "sp&#281;dza&#263;"
  ]
  node [
    id 200
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 201
    label "zjawianie_si&#281;"
  ]
  node [
    id 202
    label "przebywanie"
  ]
  node [
    id 203
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 204
    label "mijanie"
  ]
  node [
    id 205
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 206
    label "zaznawanie"
  ]
  node [
    id 207
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 208
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 209
    label "flux"
  ]
  node [
    id 210
    label "epoka"
  ]
  node [
    id 211
    label "charakter"
  ]
  node [
    id 212
    label "flow"
  ]
  node [
    id 213
    label "choroba_przyrodzona"
  ]
  node [
    id 214
    label "ciota"
  ]
  node [
    id 215
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 216
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 217
    label "kres"
  ]
  node [
    id 218
    label "przestrze&#324;"
  ]
  node [
    id 219
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 220
    label "przedmiot"
  ]
  node [
    id 221
    label "Polish"
  ]
  node [
    id 222
    label "goniony"
  ]
  node [
    id 223
    label "oberek"
  ]
  node [
    id 224
    label "ryba_po_grecku"
  ]
  node [
    id 225
    label "sztajer"
  ]
  node [
    id 226
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 227
    label "krakowiak"
  ]
  node [
    id 228
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 229
    label "pierogi_ruskie"
  ]
  node [
    id 230
    label "lacki"
  ]
  node [
    id 231
    label "polak"
  ]
  node [
    id 232
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 233
    label "chodzony"
  ]
  node [
    id 234
    label "po_polsku"
  ]
  node [
    id 235
    label "mazur"
  ]
  node [
    id 236
    label "polsko"
  ]
  node [
    id 237
    label "skoczny"
  ]
  node [
    id 238
    label "drabant"
  ]
  node [
    id 239
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 240
    label "j&#281;zyk"
  ]
  node [
    id 241
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 242
    label "artykulator"
  ]
  node [
    id 243
    label "kod"
  ]
  node [
    id 244
    label "kawa&#322;ek"
  ]
  node [
    id 245
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 246
    label "gramatyka"
  ]
  node [
    id 247
    label "stylik"
  ]
  node [
    id 248
    label "przet&#322;umaczenie"
  ]
  node [
    id 249
    label "formalizowanie"
  ]
  node [
    id 250
    label "ssanie"
  ]
  node [
    id 251
    label "ssa&#263;"
  ]
  node [
    id 252
    label "language"
  ]
  node [
    id 253
    label "liza&#263;"
  ]
  node [
    id 254
    label "napisa&#263;"
  ]
  node [
    id 255
    label "konsonantyzm"
  ]
  node [
    id 256
    label "wokalizm"
  ]
  node [
    id 257
    label "pisa&#263;"
  ]
  node [
    id 258
    label "fonetyka"
  ]
  node [
    id 259
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 260
    label "jeniec"
  ]
  node [
    id 261
    label "but"
  ]
  node [
    id 262
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 263
    label "po_koroniarsku"
  ]
  node [
    id 264
    label "kultura_duchowa"
  ]
  node [
    id 265
    label "t&#322;umaczenie"
  ]
  node [
    id 266
    label "m&#243;wienie"
  ]
  node [
    id 267
    label "pype&#263;"
  ]
  node [
    id 268
    label "lizanie"
  ]
  node [
    id 269
    label "pismo"
  ]
  node [
    id 270
    label "formalizowa&#263;"
  ]
  node [
    id 271
    label "rozumie&#263;"
  ]
  node [
    id 272
    label "organ"
  ]
  node [
    id 273
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 274
    label "rozumienie"
  ]
  node [
    id 275
    label "spos&#243;b"
  ]
  node [
    id 276
    label "makroglosja"
  ]
  node [
    id 277
    label "m&#243;wi&#263;"
  ]
  node [
    id 278
    label "jama_ustna"
  ]
  node [
    id 279
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 280
    label "formacja_geologiczna"
  ]
  node [
    id 281
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 282
    label "natural_language"
  ]
  node [
    id 283
    label "s&#322;ownictwo"
  ]
  node [
    id 284
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 285
    label "wschodnioeuropejski"
  ]
  node [
    id 286
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 287
    label "poga&#324;ski"
  ]
  node [
    id 288
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 289
    label "topielec"
  ]
  node [
    id 290
    label "europejski"
  ]
  node [
    id 291
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 292
    label "langosz"
  ]
  node [
    id 293
    label "zboczenie"
  ]
  node [
    id 294
    label "om&#243;wienie"
  ]
  node [
    id 295
    label "sponiewieranie"
  ]
  node [
    id 296
    label "discipline"
  ]
  node [
    id 297
    label "rzecz"
  ]
  node [
    id 298
    label "omawia&#263;"
  ]
  node [
    id 299
    label "kr&#261;&#380;enie"
  ]
  node [
    id 300
    label "tre&#347;&#263;"
  ]
  node [
    id 301
    label "robienie"
  ]
  node [
    id 302
    label "sponiewiera&#263;"
  ]
  node [
    id 303
    label "element"
  ]
  node [
    id 304
    label "entity"
  ]
  node [
    id 305
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 306
    label "tematyka"
  ]
  node [
    id 307
    label "w&#261;tek"
  ]
  node [
    id 308
    label "zbaczanie"
  ]
  node [
    id 309
    label "program_nauczania"
  ]
  node [
    id 310
    label "om&#243;wi&#263;"
  ]
  node [
    id 311
    label "omawianie"
  ]
  node [
    id 312
    label "thing"
  ]
  node [
    id 313
    label "kultura"
  ]
  node [
    id 314
    label "istota"
  ]
  node [
    id 315
    label "zbacza&#263;"
  ]
  node [
    id 316
    label "zboczy&#263;"
  ]
  node [
    id 317
    label "gwardzista"
  ]
  node [
    id 318
    label "melodia"
  ]
  node [
    id 319
    label "taniec"
  ]
  node [
    id 320
    label "taniec_ludowy"
  ]
  node [
    id 321
    label "&#347;redniowieczny"
  ]
  node [
    id 322
    label "specjalny"
  ]
  node [
    id 323
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 324
    label "weso&#322;y"
  ]
  node [
    id 325
    label "sprawny"
  ]
  node [
    id 326
    label "rytmiczny"
  ]
  node [
    id 327
    label "skocznie"
  ]
  node [
    id 328
    label "energiczny"
  ]
  node [
    id 329
    label "lendler"
  ]
  node [
    id 330
    label "austriacki"
  ]
  node [
    id 331
    label "polka"
  ]
  node [
    id 332
    label "europejsko"
  ]
  node [
    id 333
    label "przytup"
  ]
  node [
    id 334
    label "ho&#322;ubiec"
  ]
  node [
    id 335
    label "wodzi&#263;"
  ]
  node [
    id 336
    label "ludowy"
  ]
  node [
    id 337
    label "pie&#347;&#324;"
  ]
  node [
    id 338
    label "mieszkaniec"
  ]
  node [
    id 339
    label "centu&#347;"
  ]
  node [
    id 340
    label "lalka"
  ]
  node [
    id 341
    label "Ma&#322;opolanin"
  ]
  node [
    id 342
    label "krakauer"
  ]
  node [
    id 343
    label "przemy&#347;le&#263;"
  ]
  node [
    id 344
    label "line_up"
  ]
  node [
    id 345
    label "opracowa&#263;"
  ]
  node [
    id 346
    label "map"
  ]
  node [
    id 347
    label "pomy&#347;le&#263;"
  ]
  node [
    id 348
    label "post&#261;pi&#263;"
  ]
  node [
    id 349
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 350
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 351
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 352
    label "zorganizowa&#263;"
  ]
  node [
    id 353
    label "appoint"
  ]
  node [
    id 354
    label "wystylizowa&#263;"
  ]
  node [
    id 355
    label "cause"
  ]
  node [
    id 356
    label "przerobi&#263;"
  ]
  node [
    id 357
    label "nabra&#263;"
  ]
  node [
    id 358
    label "make"
  ]
  node [
    id 359
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 360
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 361
    label "wydali&#263;"
  ]
  node [
    id 362
    label "reconsideration"
  ]
  node [
    id 363
    label "rozwa&#380;y&#263;"
  ]
  node [
    id 364
    label "oceni&#263;"
  ]
  node [
    id 365
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 366
    label "uzna&#263;"
  ]
  node [
    id 367
    label "porobi&#263;"
  ]
  node [
    id 368
    label "wymy&#347;li&#263;"
  ]
  node [
    id 369
    label "think"
  ]
  node [
    id 370
    label "invent"
  ]
  node [
    id 371
    label "przygotowa&#263;"
  ]
  node [
    id 372
    label "wykona&#263;"
  ]
  node [
    id 373
    label "zbudowa&#263;"
  ]
  node [
    id 374
    label "draw"
  ]
  node [
    id 375
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 376
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 377
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 378
    label "leave"
  ]
  node [
    id 379
    label "przewie&#347;&#263;"
  ]
  node [
    id 380
    label "pom&#243;c"
  ]
  node [
    id 381
    label "profit"
  ]
  node [
    id 382
    label "score"
  ]
  node [
    id 383
    label "dotrze&#263;"
  ]
  node [
    id 384
    label "uzyska&#263;"
  ]
  node [
    id 385
    label "wytworzy&#263;"
  ]
  node [
    id 386
    label "picture"
  ]
  node [
    id 387
    label "manufacture"
  ]
  node [
    id 388
    label "go"
  ]
  node [
    id 389
    label "travel"
  ]
  node [
    id 390
    label "stworzy&#263;"
  ]
  node [
    id 391
    label "budowla"
  ]
  node [
    id 392
    label "establish"
  ]
  node [
    id 393
    label "evolve"
  ]
  node [
    id 394
    label "wear"
  ]
  node [
    id 395
    label "return"
  ]
  node [
    id 396
    label "plant"
  ]
  node [
    id 397
    label "pozostawi&#263;"
  ]
  node [
    id 398
    label "pokry&#263;"
  ]
  node [
    id 399
    label "znak"
  ]
  node [
    id 400
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 401
    label "stagger"
  ]
  node [
    id 402
    label "zepsu&#263;"
  ]
  node [
    id 403
    label "zmieni&#263;"
  ]
  node [
    id 404
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 405
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 406
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 407
    label "umie&#347;ci&#263;"
  ]
  node [
    id 408
    label "zacz&#261;&#263;"
  ]
  node [
    id 409
    label "raise"
  ]
  node [
    id 410
    label "wygra&#263;"
  ]
  node [
    id 411
    label "aid"
  ]
  node [
    id 412
    label "concur"
  ]
  node [
    id 413
    label "help"
  ]
  node [
    id 414
    label "u&#322;atwi&#263;"
  ]
  node [
    id 415
    label "zaskutkowa&#263;"
  ]
  node [
    id 416
    label "obowi&#261;zek"
  ]
  node [
    id 417
    label "plac&#243;wka"
  ]
  node [
    id 418
    label "zadanie"
  ]
  node [
    id 419
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 420
    label "reprezentacja"
  ]
  node [
    id 421
    label "misje"
  ]
  node [
    id 422
    label "absolutorium"
  ]
  node [
    id 423
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 424
    label "dzia&#322;anie"
  ]
  node [
    id 425
    label "activity"
  ]
  node [
    id 426
    label "zaj&#281;cie"
  ]
  node [
    id 427
    label "yield"
  ]
  node [
    id 428
    label "zbi&#243;r"
  ]
  node [
    id 429
    label "zaszkodzenie"
  ]
  node [
    id 430
    label "za&#322;o&#380;enie"
  ]
  node [
    id 431
    label "duty"
  ]
  node [
    id 432
    label "powierzanie"
  ]
  node [
    id 433
    label "work"
  ]
  node [
    id 434
    label "problem"
  ]
  node [
    id 435
    label "przepisanie"
  ]
  node [
    id 436
    label "nakarmienie"
  ]
  node [
    id 437
    label "przepisa&#263;"
  ]
  node [
    id 438
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 439
    label "czynno&#347;&#263;"
  ]
  node [
    id 440
    label "zobowi&#261;zanie"
  ]
  node [
    id 441
    label "wym&#243;g"
  ]
  node [
    id 442
    label "obarczy&#263;"
  ]
  node [
    id 443
    label "powinno&#347;&#263;"
  ]
  node [
    id 444
    label "zesp&#243;&#322;"
  ]
  node [
    id 445
    label "dru&#380;yna"
  ]
  node [
    id 446
    label "emblemat"
  ]
  node [
    id 447
    label "deputation"
  ]
  node [
    id 448
    label "agencja"
  ]
  node [
    id 449
    label "siedziba"
  ]
  node [
    id 450
    label "sie&#263;"
  ]
  node [
    id 451
    label "rekolekcje"
  ]
  node [
    id 452
    label "PW"
  ]
  node [
    id 453
    label "Sat2"
  ]
  node [
    id 454
    label "Mister"
  ]
  node [
    id 455
    label "Steven"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 452
    target 453
  ]
  edge [
    source 454
    target 455
  ]
]
