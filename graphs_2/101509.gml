graph [
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "profesor"
    origin "text"
  ]
  node [
    id 2
    label "albert"
    origin "text"
  ]
  node [
    id 3
    label "bandura"
    origin "text"
  ]
  node [
    id 4
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 5
    label "stanfordzkiego"
    origin "text"
  ]
  node [
    id 6
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "badan"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 10
    label "znana"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "jako"
    origin "text"
  ]
  node [
    id 13
    label "lalka"
    origin "text"
  ]
  node [
    id 14
    label "bobo"
    origin "text"
  ]
  node [
    id 15
    label "experiment"
    origin "text"
  ]
  node [
    id 16
    label "pora_roku"
  ]
  node [
    id 17
    label "nauczyciel"
  ]
  node [
    id 18
    label "stopie&#324;_naukowy"
  ]
  node [
    id 19
    label "nauczyciel_akademicki"
  ]
  node [
    id 20
    label "tytu&#322;"
  ]
  node [
    id 21
    label "profesura"
  ]
  node [
    id 22
    label "konsulent"
  ]
  node [
    id 23
    label "wirtuoz"
  ]
  node [
    id 24
    label "stan"
  ]
  node [
    id 25
    label "stanowisko"
  ]
  node [
    id 26
    label "professorship"
  ]
  node [
    id 27
    label "debit"
  ]
  node [
    id 28
    label "redaktor"
  ]
  node [
    id 29
    label "druk"
  ]
  node [
    id 30
    label "publikacja"
  ]
  node [
    id 31
    label "nadtytu&#322;"
  ]
  node [
    id 32
    label "szata_graficzna"
  ]
  node [
    id 33
    label "tytulatura"
  ]
  node [
    id 34
    label "wydawa&#263;"
  ]
  node [
    id 35
    label "elevation"
  ]
  node [
    id 36
    label "wyda&#263;"
  ]
  node [
    id 37
    label "mianowaniec"
  ]
  node [
    id 38
    label "poster"
  ]
  node [
    id 39
    label "nazwa"
  ]
  node [
    id 40
    label "podtytu&#322;"
  ]
  node [
    id 41
    label "wymiatacz"
  ]
  node [
    id 42
    label "gigant"
  ]
  node [
    id 43
    label "maestro"
  ]
  node [
    id 44
    label "mistrz"
  ]
  node [
    id 45
    label "instrumentalista"
  ]
  node [
    id 46
    label "ekspert"
  ]
  node [
    id 47
    label "kraj_zwi&#261;zkowy"
  ]
  node [
    id 48
    label "Austria"
  ]
  node [
    id 49
    label "doradca"
  ]
  node [
    id 50
    label "belfer"
  ]
  node [
    id 51
    label "kszta&#322;ciciel"
  ]
  node [
    id 52
    label "preceptor"
  ]
  node [
    id 53
    label "pedagog"
  ]
  node [
    id 54
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 55
    label "szkolnik"
  ]
  node [
    id 56
    label "popularyzator"
  ]
  node [
    id 57
    label "chordofon_szarpany"
  ]
  node [
    id 58
    label "Stanford"
  ]
  node [
    id 59
    label "academy"
  ]
  node [
    id 60
    label "Harvard"
  ]
  node [
    id 61
    label "uczelnia"
  ]
  node [
    id 62
    label "wydzia&#322;"
  ]
  node [
    id 63
    label "Sorbona"
  ]
  node [
    id 64
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 65
    label "Princeton"
  ]
  node [
    id 66
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 67
    label "ku&#378;nia"
  ]
  node [
    id 68
    label "warsztat"
  ]
  node [
    id 69
    label "instytucja"
  ]
  node [
    id 70
    label "fabryka"
  ]
  node [
    id 71
    label "kanclerz"
  ]
  node [
    id 72
    label "szko&#322;a"
  ]
  node [
    id 73
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 74
    label "podkanclerz"
  ]
  node [
    id 75
    label "miasteczko_studenckie"
  ]
  node [
    id 76
    label "kwestura"
  ]
  node [
    id 77
    label "wyk&#322;adanie"
  ]
  node [
    id 78
    label "rektorat"
  ]
  node [
    id 79
    label "school"
  ]
  node [
    id 80
    label "senat"
  ]
  node [
    id 81
    label "promotorstwo"
  ]
  node [
    id 82
    label "jednostka_organizacyjna"
  ]
  node [
    id 83
    label "relation"
  ]
  node [
    id 84
    label "urz&#261;d"
  ]
  node [
    id 85
    label "whole"
  ]
  node [
    id 86
    label "miejsce_pracy"
  ]
  node [
    id 87
    label "podsekcja"
  ]
  node [
    id 88
    label "insourcing"
  ]
  node [
    id 89
    label "politechnika"
  ]
  node [
    id 90
    label "katedra"
  ]
  node [
    id 91
    label "ministerstwo"
  ]
  node [
    id 92
    label "dzia&#322;"
  ]
  node [
    id 93
    label "paryski"
  ]
  node [
    id 94
    label "ameryka&#324;ski"
  ]
  node [
    id 95
    label "wykona&#263;"
  ]
  node [
    id 96
    label "zbudowa&#263;"
  ]
  node [
    id 97
    label "draw"
  ]
  node [
    id 98
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 99
    label "carry"
  ]
  node [
    id 100
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 101
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 102
    label "leave"
  ]
  node [
    id 103
    label "przewie&#347;&#263;"
  ]
  node [
    id 104
    label "pom&#243;c"
  ]
  node [
    id 105
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 106
    label "profit"
  ]
  node [
    id 107
    label "score"
  ]
  node [
    id 108
    label "make"
  ]
  node [
    id 109
    label "dotrze&#263;"
  ]
  node [
    id 110
    label "uzyska&#263;"
  ]
  node [
    id 111
    label "wytworzy&#263;"
  ]
  node [
    id 112
    label "picture"
  ]
  node [
    id 113
    label "manufacture"
  ]
  node [
    id 114
    label "zrobi&#263;"
  ]
  node [
    id 115
    label "go"
  ]
  node [
    id 116
    label "spowodowa&#263;"
  ]
  node [
    id 117
    label "travel"
  ]
  node [
    id 118
    label "stworzy&#263;"
  ]
  node [
    id 119
    label "budowla"
  ]
  node [
    id 120
    label "establish"
  ]
  node [
    id 121
    label "evolve"
  ]
  node [
    id 122
    label "zaplanowa&#263;"
  ]
  node [
    id 123
    label "wear"
  ]
  node [
    id 124
    label "return"
  ]
  node [
    id 125
    label "plant"
  ]
  node [
    id 126
    label "pozostawi&#263;"
  ]
  node [
    id 127
    label "pokry&#263;"
  ]
  node [
    id 128
    label "znak"
  ]
  node [
    id 129
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 130
    label "przygotowa&#263;"
  ]
  node [
    id 131
    label "stagger"
  ]
  node [
    id 132
    label "zepsu&#263;"
  ]
  node [
    id 133
    label "zmieni&#263;"
  ]
  node [
    id 134
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 135
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 136
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 137
    label "umie&#347;ci&#263;"
  ]
  node [
    id 138
    label "zacz&#261;&#263;"
  ]
  node [
    id 139
    label "raise"
  ]
  node [
    id 140
    label "wygra&#263;"
  ]
  node [
    id 141
    label "aid"
  ]
  node [
    id 142
    label "concur"
  ]
  node [
    id 143
    label "help"
  ]
  node [
    id 144
    label "u&#322;atwi&#263;"
  ]
  node [
    id 145
    label "zaskutkowa&#263;"
  ]
  node [
    id 146
    label "doba"
  ]
  node [
    id 147
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 148
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 149
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 150
    label "dzi&#347;"
  ]
  node [
    id 151
    label "teraz"
  ]
  node [
    id 152
    label "czas"
  ]
  node [
    id 153
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 154
    label "jednocze&#347;nie"
  ]
  node [
    id 155
    label "tydzie&#324;"
  ]
  node [
    id 156
    label "noc"
  ]
  node [
    id 157
    label "dzie&#324;"
  ]
  node [
    id 158
    label "godzina"
  ]
  node [
    id 159
    label "long_time"
  ]
  node [
    id 160
    label "jednostka_geologiczna"
  ]
  node [
    id 161
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 162
    label "mie&#263;_miejsce"
  ]
  node [
    id 163
    label "equal"
  ]
  node [
    id 164
    label "trwa&#263;"
  ]
  node [
    id 165
    label "chodzi&#263;"
  ]
  node [
    id 166
    label "si&#281;ga&#263;"
  ]
  node [
    id 167
    label "obecno&#347;&#263;"
  ]
  node [
    id 168
    label "stand"
  ]
  node [
    id 169
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 170
    label "uczestniczy&#263;"
  ]
  node [
    id 171
    label "participate"
  ]
  node [
    id 172
    label "robi&#263;"
  ]
  node [
    id 173
    label "istnie&#263;"
  ]
  node [
    id 174
    label "pozostawa&#263;"
  ]
  node [
    id 175
    label "zostawa&#263;"
  ]
  node [
    id 176
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 177
    label "adhere"
  ]
  node [
    id 178
    label "compass"
  ]
  node [
    id 179
    label "korzysta&#263;"
  ]
  node [
    id 180
    label "appreciation"
  ]
  node [
    id 181
    label "osi&#261;ga&#263;"
  ]
  node [
    id 182
    label "dociera&#263;"
  ]
  node [
    id 183
    label "get"
  ]
  node [
    id 184
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 185
    label "mierzy&#263;"
  ]
  node [
    id 186
    label "u&#380;ywa&#263;"
  ]
  node [
    id 187
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 188
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 189
    label "exsert"
  ]
  node [
    id 190
    label "being"
  ]
  node [
    id 191
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 192
    label "cecha"
  ]
  node [
    id 193
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 194
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 195
    label "p&#322;ywa&#263;"
  ]
  node [
    id 196
    label "run"
  ]
  node [
    id 197
    label "bangla&#263;"
  ]
  node [
    id 198
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 199
    label "przebiega&#263;"
  ]
  node [
    id 200
    label "wk&#322;ada&#263;"
  ]
  node [
    id 201
    label "proceed"
  ]
  node [
    id 202
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 203
    label "bywa&#263;"
  ]
  node [
    id 204
    label "dziama&#263;"
  ]
  node [
    id 205
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 206
    label "stara&#263;_si&#281;"
  ]
  node [
    id 207
    label "para"
  ]
  node [
    id 208
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 209
    label "str&#243;j"
  ]
  node [
    id 210
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 211
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 212
    label "krok"
  ]
  node [
    id 213
    label "tryb"
  ]
  node [
    id 214
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 215
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 216
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 217
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 218
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 219
    label "continue"
  ]
  node [
    id 220
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 221
    label "Ohio"
  ]
  node [
    id 222
    label "wci&#281;cie"
  ]
  node [
    id 223
    label "Nowy_York"
  ]
  node [
    id 224
    label "warstwa"
  ]
  node [
    id 225
    label "samopoczucie"
  ]
  node [
    id 226
    label "Illinois"
  ]
  node [
    id 227
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 228
    label "state"
  ]
  node [
    id 229
    label "Jukatan"
  ]
  node [
    id 230
    label "Kalifornia"
  ]
  node [
    id 231
    label "Wirginia"
  ]
  node [
    id 232
    label "wektor"
  ]
  node [
    id 233
    label "Teksas"
  ]
  node [
    id 234
    label "Goa"
  ]
  node [
    id 235
    label "Waszyngton"
  ]
  node [
    id 236
    label "miejsce"
  ]
  node [
    id 237
    label "Massachusetts"
  ]
  node [
    id 238
    label "Alaska"
  ]
  node [
    id 239
    label "Arakan"
  ]
  node [
    id 240
    label "Hawaje"
  ]
  node [
    id 241
    label "Maryland"
  ]
  node [
    id 242
    label "punkt"
  ]
  node [
    id 243
    label "Michigan"
  ]
  node [
    id 244
    label "Arizona"
  ]
  node [
    id 245
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 246
    label "Georgia"
  ]
  node [
    id 247
    label "poziom"
  ]
  node [
    id 248
    label "Pensylwania"
  ]
  node [
    id 249
    label "shape"
  ]
  node [
    id 250
    label "Luizjana"
  ]
  node [
    id 251
    label "Nowy_Meksyk"
  ]
  node [
    id 252
    label "Alabama"
  ]
  node [
    id 253
    label "ilo&#347;&#263;"
  ]
  node [
    id 254
    label "Kansas"
  ]
  node [
    id 255
    label "Oregon"
  ]
  node [
    id 256
    label "Floryda"
  ]
  node [
    id 257
    label "Oklahoma"
  ]
  node [
    id 258
    label "jednostka_administracyjna"
  ]
  node [
    id 259
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 260
    label "zabawka"
  ]
  node [
    id 261
    label "dummy"
  ]
  node [
    id 262
    label "statuetka"
  ]
  node [
    id 263
    label "lalkarstwo"
  ]
  node [
    id 264
    label "matrioszka"
  ]
  node [
    id 265
    label "narz&#281;dzie"
  ]
  node [
    id 266
    label "przedmiot"
  ]
  node [
    id 267
    label "bawid&#322;o"
  ]
  node [
    id 268
    label "frisbee"
  ]
  node [
    id 269
    label "smoczek"
  ]
  node [
    id 270
    label "ceramika"
  ]
  node [
    id 271
    label "oskar"
  ]
  node [
    id 272
    label "figurine"
  ]
  node [
    id 273
    label "figura"
  ]
  node [
    id 274
    label "teatr"
  ]
  node [
    id 275
    label "rzemios&#322;o"
  ]
  node [
    id 276
    label "bobok"
  ]
  node [
    id 277
    label "dziecko"
  ]
  node [
    id 278
    label "duch"
  ]
  node [
    id 279
    label "niemowl&#281;"
  ]
  node [
    id 280
    label "utulenie"
  ]
  node [
    id 281
    label "pediatra"
  ]
  node [
    id 282
    label "dzieciak"
  ]
  node [
    id 283
    label "utulanie"
  ]
  node [
    id 284
    label "dzieciarnia"
  ]
  node [
    id 285
    label "cz&#322;owiek"
  ]
  node [
    id 286
    label "niepe&#322;noletni"
  ]
  node [
    id 287
    label "organizm"
  ]
  node [
    id 288
    label "utula&#263;"
  ]
  node [
    id 289
    label "cz&#322;owieczek"
  ]
  node [
    id 290
    label "fledgling"
  ]
  node [
    id 291
    label "zwierz&#281;"
  ]
  node [
    id 292
    label "utuli&#263;"
  ]
  node [
    id 293
    label "m&#322;odzik"
  ]
  node [
    id 294
    label "pedofil"
  ]
  node [
    id 295
    label "m&#322;odziak"
  ]
  node [
    id 296
    label "potomek"
  ]
  node [
    id 297
    label "entliczek-pentliczek"
  ]
  node [
    id 298
    label "potomstwo"
  ]
  node [
    id 299
    label "sraluch"
  ]
  node [
    id 300
    label "piek&#322;o"
  ]
  node [
    id 301
    label "human_body"
  ]
  node [
    id 302
    label "ofiarowywanie"
  ]
  node [
    id 303
    label "sfera_afektywna"
  ]
  node [
    id 304
    label "nekromancja"
  ]
  node [
    id 305
    label "Po&#347;wist"
  ]
  node [
    id 306
    label "podekscytowanie"
  ]
  node [
    id 307
    label "deformowanie"
  ]
  node [
    id 308
    label "sumienie"
  ]
  node [
    id 309
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 310
    label "deformowa&#263;"
  ]
  node [
    id 311
    label "osobowo&#347;&#263;"
  ]
  node [
    id 312
    label "psychika"
  ]
  node [
    id 313
    label "zjawa"
  ]
  node [
    id 314
    label "zmar&#322;y"
  ]
  node [
    id 315
    label "istota_nadprzyrodzona"
  ]
  node [
    id 316
    label "power"
  ]
  node [
    id 317
    label "entity"
  ]
  node [
    id 318
    label "ofiarowywa&#263;"
  ]
  node [
    id 319
    label "oddech"
  ]
  node [
    id 320
    label "seksualno&#347;&#263;"
  ]
  node [
    id 321
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 322
    label "byt"
  ]
  node [
    id 323
    label "si&#322;a"
  ]
  node [
    id 324
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 325
    label "ego"
  ]
  node [
    id 326
    label "ofiarowanie"
  ]
  node [
    id 327
    label "kompleksja"
  ]
  node [
    id 328
    label "charakter"
  ]
  node [
    id 329
    label "fizjonomia"
  ]
  node [
    id 330
    label "kompleks"
  ]
  node [
    id 331
    label "zapalno&#347;&#263;"
  ]
  node [
    id 332
    label "mikrokosmos"
  ]
  node [
    id 333
    label "T&#281;sknica"
  ]
  node [
    id 334
    label "ofiarowa&#263;"
  ]
  node [
    id 335
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 336
    label "osoba"
  ]
  node [
    id 337
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 338
    label "passion"
  ]
  node [
    id 339
    label "wyprawka"
  ]
  node [
    id 340
    label "butelka"
  ]
  node [
    id 341
    label "gaworzy&#263;"
  ]
  node [
    id 342
    label "&#347;piochy"
  ]
  node [
    id 343
    label "koszulka"
  ]
  node [
    id 344
    label "dzidziu&#347;"
  ]
  node [
    id 345
    label "gaworzenie"
  ]
  node [
    id 346
    label "niunia"
  ]
  node [
    id 347
    label "Stanfordzkiego"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 213
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 222
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 269
  ]
  edge [
    source 13
    target 270
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 14
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 276
  ]
  edge [
    source 14
    target 277
  ]
  edge [
    source 14
    target 278
  ]
  edge [
    source 14
    target 279
  ]
  edge [
    source 14
    target 280
  ]
  edge [
    source 14
    target 281
  ]
  edge [
    source 14
    target 282
  ]
  edge [
    source 14
    target 283
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 14
    target 286
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 288
  ]
  edge [
    source 14
    target 289
  ]
  edge [
    source 14
    target 290
  ]
  edge [
    source 14
    target 291
  ]
  edge [
    source 14
    target 292
  ]
  edge [
    source 14
    target 293
  ]
  edge [
    source 14
    target 294
  ]
  edge [
    source 14
    target 295
  ]
  edge [
    source 14
    target 296
  ]
  edge [
    source 14
    target 297
  ]
  edge [
    source 14
    target 298
  ]
  edge [
    source 14
    target 299
  ]
  edge [
    source 14
    target 300
  ]
  edge [
    source 14
    target 301
  ]
  edge [
    source 14
    target 302
  ]
  edge [
    source 14
    target 303
  ]
  edge [
    source 14
    target 304
  ]
  edge [
    source 14
    target 305
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 306
  ]
  edge [
    source 14
    target 307
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 14
    target 310
  ]
  edge [
    source 14
    target 311
  ]
  edge [
    source 14
    target 312
  ]
  edge [
    source 14
    target 313
  ]
  edge [
    source 14
    target 314
  ]
  edge [
    source 14
    target 315
  ]
  edge [
    source 14
    target 316
  ]
  edge [
    source 14
    target 317
  ]
  edge [
    source 14
    target 318
  ]
  edge [
    source 14
    target 319
  ]
  edge [
    source 14
    target 320
  ]
  edge [
    source 14
    target 321
  ]
  edge [
    source 14
    target 322
  ]
  edge [
    source 14
    target 323
  ]
  edge [
    source 14
    target 324
  ]
  edge [
    source 14
    target 325
  ]
  edge [
    source 14
    target 326
  ]
  edge [
    source 14
    target 327
  ]
  edge [
    source 14
    target 328
  ]
  edge [
    source 14
    target 329
  ]
  edge [
    source 14
    target 330
  ]
  edge [
    source 14
    target 249
  ]
  edge [
    source 14
    target 331
  ]
  edge [
    source 14
    target 332
  ]
  edge [
    source 14
    target 333
  ]
  edge [
    source 14
    target 334
  ]
  edge [
    source 14
    target 335
  ]
  edge [
    source 14
    target 336
  ]
  edge [
    source 14
    target 337
  ]
  edge [
    source 14
    target 338
  ]
  edge [
    source 14
    target 339
  ]
  edge [
    source 14
    target 340
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 342
  ]
  edge [
    source 14
    target 343
  ]
  edge [
    source 14
    target 344
  ]
  edge [
    source 14
    target 345
  ]
  edge [
    source 14
    target 346
  ]
  edge [
    source 14
    target 269
  ]
]
