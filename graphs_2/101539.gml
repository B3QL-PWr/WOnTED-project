graph [
  node [
    id 0
    label "czas"
    origin "text"
  ]
  node [
    id 1
    label "wakacje"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dobre"
    origin "text"
  ]
  node [
    id 4
    label "moment"
    origin "text"
  ]
  node [
    id 5
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 6
    label "uaktualnie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "opracowanie"
    origin "text"
  ]
  node [
    id 8
    label "nowa"
    origin "text"
  ]
  node [
    id 9
    label "funkcjonalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "uczelniany"
    origin "text"
  ]
  node [
    id 11
    label "platforma"
    origin "text"
  ]
  node [
    id 12
    label "learningowej"
    origin "text"
  ]
  node [
    id 13
    label "jeden"
    origin "text"
  ]
  node [
    id 14
    label "dostosowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "moodle"
    origin "text"
  ]
  node [
    id 16
    label "dla"
    origin "text"
  ]
  node [
    id 17
    label "osoba"
    origin "text"
  ]
  node [
    id 18
    label "s&#322;abowidz&#261;ca"
    origin "text"
  ]
  node [
    id 19
    label "inny"
    origin "text"
  ]
  node [
    id 20
    label "wkr&#243;tce"
    origin "text"
  ]
  node [
    id 21
    label "dostosowywa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "potrzeb"
    origin "text"
  ]
  node [
    id 23
    label "przyj&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 25
    label "za&#322;o&#380;enia"
    origin "text"
  ]
  node [
    id 26
    label "chronometria"
  ]
  node [
    id 27
    label "odczyt"
  ]
  node [
    id 28
    label "laba"
  ]
  node [
    id 29
    label "czasoprzestrze&#324;"
  ]
  node [
    id 30
    label "time_period"
  ]
  node [
    id 31
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 32
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 33
    label "Zeitgeist"
  ]
  node [
    id 34
    label "pochodzenie"
  ]
  node [
    id 35
    label "przep&#322;ywanie"
  ]
  node [
    id 36
    label "schy&#322;ek"
  ]
  node [
    id 37
    label "czwarty_wymiar"
  ]
  node [
    id 38
    label "kategoria_gramatyczna"
  ]
  node [
    id 39
    label "poprzedzi&#263;"
  ]
  node [
    id 40
    label "pogoda"
  ]
  node [
    id 41
    label "czasokres"
  ]
  node [
    id 42
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 43
    label "poprzedzenie"
  ]
  node [
    id 44
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 45
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 46
    label "dzieje"
  ]
  node [
    id 47
    label "zegar"
  ]
  node [
    id 48
    label "koniugacja"
  ]
  node [
    id 49
    label "trawi&#263;"
  ]
  node [
    id 50
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 51
    label "poprzedza&#263;"
  ]
  node [
    id 52
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 53
    label "trawienie"
  ]
  node [
    id 54
    label "chwila"
  ]
  node [
    id 55
    label "rachuba_czasu"
  ]
  node [
    id 56
    label "poprzedzanie"
  ]
  node [
    id 57
    label "okres_czasu"
  ]
  node [
    id 58
    label "period"
  ]
  node [
    id 59
    label "odwlekanie_si&#281;"
  ]
  node [
    id 60
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 61
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 62
    label "pochodzi&#263;"
  ]
  node [
    id 63
    label "time"
  ]
  node [
    id 64
    label "blok"
  ]
  node [
    id 65
    label "reading"
  ]
  node [
    id 66
    label "handout"
  ]
  node [
    id 67
    label "podawanie"
  ]
  node [
    id 68
    label "wyk&#322;ad"
  ]
  node [
    id 69
    label "lecture"
  ]
  node [
    id 70
    label "pomiar"
  ]
  node [
    id 71
    label "meteorology"
  ]
  node [
    id 72
    label "warunki"
  ]
  node [
    id 73
    label "weather"
  ]
  node [
    id 74
    label "zjawisko"
  ]
  node [
    id 75
    label "pok&#243;j"
  ]
  node [
    id 76
    label "atak"
  ]
  node [
    id 77
    label "prognoza_meteorologiczna"
  ]
  node [
    id 78
    label "potrzyma&#263;"
  ]
  node [
    id 79
    label "program"
  ]
  node [
    id 80
    label "czas_wolny"
  ]
  node [
    id 81
    label "metrologia"
  ]
  node [
    id 82
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 83
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 84
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 85
    label "czasomierz"
  ]
  node [
    id 86
    label "tyka&#263;"
  ]
  node [
    id 87
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 88
    label "tykn&#261;&#263;"
  ]
  node [
    id 89
    label "nabicie"
  ]
  node [
    id 90
    label "bicie"
  ]
  node [
    id 91
    label "kotwica"
  ]
  node [
    id 92
    label "godzinnik"
  ]
  node [
    id 93
    label "werk"
  ]
  node [
    id 94
    label "urz&#261;dzenie"
  ]
  node [
    id 95
    label "wahad&#322;o"
  ]
  node [
    id 96
    label "kurant"
  ]
  node [
    id 97
    label "cyferblat"
  ]
  node [
    id 98
    label "liczba"
  ]
  node [
    id 99
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 100
    label "czasownik"
  ]
  node [
    id 101
    label "tryb"
  ]
  node [
    id 102
    label "coupling"
  ]
  node [
    id 103
    label "fleksja"
  ]
  node [
    id 104
    label "orz&#281;sek"
  ]
  node [
    id 105
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 106
    label "background"
  ]
  node [
    id 107
    label "str&#243;j"
  ]
  node [
    id 108
    label "wynikanie"
  ]
  node [
    id 109
    label "origin"
  ]
  node [
    id 110
    label "zaczynanie_si&#281;"
  ]
  node [
    id 111
    label "beginning"
  ]
  node [
    id 112
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 113
    label "geneza"
  ]
  node [
    id 114
    label "marnowanie"
  ]
  node [
    id 115
    label "unicestwianie"
  ]
  node [
    id 116
    label "sp&#281;dzanie"
  ]
  node [
    id 117
    label "digestion"
  ]
  node [
    id 118
    label "perystaltyka"
  ]
  node [
    id 119
    label "proces_fizjologiczny"
  ]
  node [
    id 120
    label "rozk&#322;adanie"
  ]
  node [
    id 121
    label "przetrawianie"
  ]
  node [
    id 122
    label "contemplation"
  ]
  node [
    id 123
    label "proceed"
  ]
  node [
    id 124
    label "pour"
  ]
  node [
    id 125
    label "mija&#263;"
  ]
  node [
    id 126
    label "sail"
  ]
  node [
    id 127
    label "przebywa&#263;"
  ]
  node [
    id 128
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 129
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 130
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 131
    label "carry"
  ]
  node [
    id 132
    label "go&#347;ci&#263;"
  ]
  node [
    id 133
    label "zanikni&#281;cie"
  ]
  node [
    id 134
    label "departure"
  ]
  node [
    id 135
    label "odej&#347;cie"
  ]
  node [
    id 136
    label "opuszczenie"
  ]
  node [
    id 137
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 138
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 139
    label "ciecz"
  ]
  node [
    id 140
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 141
    label "oddalenie_si&#281;"
  ]
  node [
    id 142
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 143
    label "cross"
  ]
  node [
    id 144
    label "swimming"
  ]
  node [
    id 145
    label "min&#261;&#263;"
  ]
  node [
    id 146
    label "przeby&#263;"
  ]
  node [
    id 147
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 148
    label "zago&#347;ci&#263;"
  ]
  node [
    id 149
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 150
    label "overwhelm"
  ]
  node [
    id 151
    label "zrobi&#263;"
  ]
  node [
    id 152
    label "opatrzy&#263;"
  ]
  node [
    id 153
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 154
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 155
    label "opatrywa&#263;"
  ]
  node [
    id 156
    label "poby&#263;"
  ]
  node [
    id 157
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 158
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 159
    label "bolt"
  ]
  node [
    id 160
    label "uda&#263;_si&#281;"
  ]
  node [
    id 161
    label "date"
  ]
  node [
    id 162
    label "fall"
  ]
  node [
    id 163
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 164
    label "spowodowa&#263;"
  ]
  node [
    id 165
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 166
    label "wynika&#263;"
  ]
  node [
    id 167
    label "zdarzenie_si&#281;"
  ]
  node [
    id 168
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 169
    label "progress"
  ]
  node [
    id 170
    label "opatrzenie"
  ]
  node [
    id 171
    label "opatrywanie"
  ]
  node [
    id 172
    label "przebycie"
  ]
  node [
    id 173
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 174
    label "mini&#281;cie"
  ]
  node [
    id 175
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 176
    label "zaistnienie"
  ]
  node [
    id 177
    label "doznanie"
  ]
  node [
    id 178
    label "cruise"
  ]
  node [
    id 179
    label "lutowa&#263;"
  ]
  node [
    id 180
    label "metal"
  ]
  node [
    id 181
    label "przetrawia&#263;"
  ]
  node [
    id 182
    label "poch&#322;ania&#263;"
  ]
  node [
    id 183
    label "marnowa&#263;"
  ]
  node [
    id 184
    label "digest"
  ]
  node [
    id 185
    label "usuwa&#263;"
  ]
  node [
    id 186
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 187
    label "sp&#281;dza&#263;"
  ]
  node [
    id 188
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 189
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 190
    label "zjawianie_si&#281;"
  ]
  node [
    id 191
    label "mijanie"
  ]
  node [
    id 192
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 193
    label "przebywanie"
  ]
  node [
    id 194
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 195
    label "flux"
  ]
  node [
    id 196
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 197
    label "zaznawanie"
  ]
  node [
    id 198
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 199
    label "charakter"
  ]
  node [
    id 200
    label "epoka"
  ]
  node [
    id 201
    label "ciota"
  ]
  node [
    id 202
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 203
    label "flow"
  ]
  node [
    id 204
    label "choroba_przyrodzona"
  ]
  node [
    id 205
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 206
    label "kres"
  ]
  node [
    id 207
    label "przestrze&#324;"
  ]
  node [
    id 208
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 209
    label "rok_szkolny"
  ]
  node [
    id 210
    label "rok_akademicki"
  ]
  node [
    id 211
    label "urlop"
  ]
  node [
    id 212
    label "wycieczka"
  ]
  node [
    id 213
    label "wolne"
  ]
  node [
    id 214
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 215
    label "stan"
  ]
  node [
    id 216
    label "stand"
  ]
  node [
    id 217
    label "trwa&#263;"
  ]
  node [
    id 218
    label "equal"
  ]
  node [
    id 219
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 220
    label "chodzi&#263;"
  ]
  node [
    id 221
    label "uczestniczy&#263;"
  ]
  node [
    id 222
    label "obecno&#347;&#263;"
  ]
  node [
    id 223
    label "si&#281;ga&#263;"
  ]
  node [
    id 224
    label "mie&#263;_miejsce"
  ]
  node [
    id 225
    label "robi&#263;"
  ]
  node [
    id 226
    label "participate"
  ]
  node [
    id 227
    label "adhere"
  ]
  node [
    id 228
    label "pozostawa&#263;"
  ]
  node [
    id 229
    label "zostawa&#263;"
  ]
  node [
    id 230
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 231
    label "istnie&#263;"
  ]
  node [
    id 232
    label "compass"
  ]
  node [
    id 233
    label "exsert"
  ]
  node [
    id 234
    label "get"
  ]
  node [
    id 235
    label "u&#380;ywa&#263;"
  ]
  node [
    id 236
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 237
    label "osi&#261;ga&#263;"
  ]
  node [
    id 238
    label "korzysta&#263;"
  ]
  node [
    id 239
    label "appreciation"
  ]
  node [
    id 240
    label "dociera&#263;"
  ]
  node [
    id 241
    label "mierzy&#263;"
  ]
  node [
    id 242
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 243
    label "being"
  ]
  node [
    id 244
    label "cecha"
  ]
  node [
    id 245
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 246
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 247
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 248
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 249
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 250
    label "para"
  ]
  node [
    id 251
    label "krok"
  ]
  node [
    id 252
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 253
    label "przebiega&#263;"
  ]
  node [
    id 254
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 255
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 256
    label "continue"
  ]
  node [
    id 257
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 258
    label "wk&#322;ada&#263;"
  ]
  node [
    id 259
    label "p&#322;ywa&#263;"
  ]
  node [
    id 260
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 261
    label "bangla&#263;"
  ]
  node [
    id 262
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 263
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 264
    label "bywa&#263;"
  ]
  node [
    id 265
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 266
    label "dziama&#263;"
  ]
  node [
    id 267
    label "run"
  ]
  node [
    id 268
    label "stara&#263;_si&#281;"
  ]
  node [
    id 269
    label "Arakan"
  ]
  node [
    id 270
    label "Teksas"
  ]
  node [
    id 271
    label "Georgia"
  ]
  node [
    id 272
    label "Maryland"
  ]
  node [
    id 273
    label "warstwa"
  ]
  node [
    id 274
    label "Michigan"
  ]
  node [
    id 275
    label "Massachusetts"
  ]
  node [
    id 276
    label "Luizjana"
  ]
  node [
    id 277
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 278
    label "samopoczucie"
  ]
  node [
    id 279
    label "Floryda"
  ]
  node [
    id 280
    label "Ohio"
  ]
  node [
    id 281
    label "Alaska"
  ]
  node [
    id 282
    label "Nowy_Meksyk"
  ]
  node [
    id 283
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 284
    label "wci&#281;cie"
  ]
  node [
    id 285
    label "Kansas"
  ]
  node [
    id 286
    label "Alabama"
  ]
  node [
    id 287
    label "miejsce"
  ]
  node [
    id 288
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 289
    label "Kalifornia"
  ]
  node [
    id 290
    label "Wirginia"
  ]
  node [
    id 291
    label "punkt"
  ]
  node [
    id 292
    label "Nowy_York"
  ]
  node [
    id 293
    label "Waszyngton"
  ]
  node [
    id 294
    label "Pensylwania"
  ]
  node [
    id 295
    label "wektor"
  ]
  node [
    id 296
    label "Hawaje"
  ]
  node [
    id 297
    label "state"
  ]
  node [
    id 298
    label "poziom"
  ]
  node [
    id 299
    label "jednostka_administracyjna"
  ]
  node [
    id 300
    label "Illinois"
  ]
  node [
    id 301
    label "Oklahoma"
  ]
  node [
    id 302
    label "Oregon"
  ]
  node [
    id 303
    label "Arizona"
  ]
  node [
    id 304
    label "ilo&#347;&#263;"
  ]
  node [
    id 305
    label "Jukatan"
  ]
  node [
    id 306
    label "shape"
  ]
  node [
    id 307
    label "Goa"
  ]
  node [
    id 308
    label "fragment"
  ]
  node [
    id 309
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 310
    label "chron"
  ]
  node [
    id 311
    label "minute"
  ]
  node [
    id 312
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 313
    label "jednostka_geologiczna"
  ]
  node [
    id 314
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 315
    label "utw&#243;r"
  ]
  node [
    id 316
    label "wiek"
  ]
  node [
    id 317
    label "umo&#380;liwienie"
  ]
  node [
    id 318
    label "spowodowanie"
  ]
  node [
    id 319
    label "wej&#347;cie"
  ]
  node [
    id 320
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 321
    label "evocation"
  ]
  node [
    id 322
    label "doprowadzenie"
  ]
  node [
    id 323
    label "nuklearyzacja"
  ]
  node [
    id 324
    label "w&#322;&#261;czenie"
  ]
  node [
    id 325
    label "zacz&#281;cie"
  ]
  node [
    id 326
    label "zapoznanie"
  ]
  node [
    id 327
    label "czynno&#347;&#263;"
  ]
  node [
    id 328
    label "podstawy"
  ]
  node [
    id 329
    label "wst&#281;p"
  ]
  node [
    id 330
    label "entrance"
  ]
  node [
    id 331
    label "wpisanie"
  ]
  node [
    id 332
    label "deduction"
  ]
  node [
    id 333
    label "umieszczenie"
  ]
  node [
    id 334
    label "issue"
  ]
  node [
    id 335
    label "zrobienie"
  ]
  node [
    id 336
    label "rynek"
  ]
  node [
    id 337
    label "przewietrzenie"
  ]
  node [
    id 338
    label "upowa&#380;nienie"
  ]
  node [
    id 339
    label "mo&#380;liwy"
  ]
  node [
    id 340
    label "obejrzenie"
  ]
  node [
    id 341
    label "za&#347;wiecenie"
  ]
  node [
    id 342
    label "pos&#322;uchanie"
  ]
  node [
    id 343
    label "involvement"
  ]
  node [
    id 344
    label "nastawienie"
  ]
  node [
    id 345
    label "funkcjonowanie"
  ]
  node [
    id 346
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 347
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 348
    label "uruchomienie"
  ]
  node [
    id 349
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 350
    label "narobienie"
  ]
  node [
    id 351
    label "porobienie"
  ]
  node [
    id 352
    label "creation"
  ]
  node [
    id 353
    label "pojawienie_si&#281;"
  ]
  node [
    id 354
    label "wnij&#347;cie"
  ]
  node [
    id 355
    label "podw&#243;rze"
  ]
  node [
    id 356
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 357
    label "wzi&#281;cie"
  ]
  node [
    id 358
    label "stimulation"
  ]
  node [
    id 359
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 360
    label "approach"
  ]
  node [
    id 361
    label "dom"
  ]
  node [
    id 362
    label "wnikni&#281;cie"
  ]
  node [
    id 363
    label "release"
  ]
  node [
    id 364
    label "trespass"
  ]
  node [
    id 365
    label "spotkanie"
  ]
  node [
    id 366
    label "poznanie"
  ]
  node [
    id 367
    label "wzniesienie_si&#281;"
  ]
  node [
    id 368
    label "pocz&#261;tek"
  ]
  node [
    id 369
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 370
    label "wch&#243;d"
  ]
  node [
    id 371
    label "stanie_si&#281;"
  ]
  node [
    id 372
    label "bramka"
  ]
  node [
    id 373
    label "vent"
  ]
  node [
    id 374
    label "zaatakowanie"
  ]
  node [
    id 375
    label "przenikni&#281;cie"
  ]
  node [
    id 376
    label "cz&#322;onek"
  ]
  node [
    id 377
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 378
    label "doj&#347;cie"
  ]
  node [
    id 379
    label "otw&#243;r"
  ]
  node [
    id 380
    label "dostanie_si&#281;"
  ]
  node [
    id 381
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 382
    label "nast&#261;pienie"
  ]
  node [
    id 383
    label "dost&#281;p"
  ]
  node [
    id 384
    label "wpuszczenie"
  ]
  node [
    id 385
    label "przekroczenie"
  ]
  node [
    id 386
    label "campaign"
  ]
  node [
    id 387
    label "causing"
  ]
  node [
    id 388
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 389
    label "naruszenie"
  ]
  node [
    id 390
    label "sygna&#322;"
  ]
  node [
    id 391
    label "hindrance"
  ]
  node [
    id 392
    label "przeszkoda"
  ]
  node [
    id 393
    label "disorder"
  ]
  node [
    id 394
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 395
    label "sytuacja"
  ]
  node [
    id 396
    label "aberration"
  ]
  node [
    id 397
    label "perturbation"
  ]
  node [
    id 398
    label "opening"
  ]
  node [
    id 399
    label "odj&#281;cie"
  ]
  node [
    id 400
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 401
    label "wyra&#380;enie"
  ]
  node [
    id 402
    label "discourtesy"
  ]
  node [
    id 403
    label "post&#261;pienie"
  ]
  node [
    id 404
    label "record"
  ]
  node [
    id 405
    label "napisanie"
  ]
  node [
    id 406
    label "inscription"
  ]
  node [
    id 407
    label "wype&#322;nienie"
  ]
  node [
    id 408
    label "detail"
  ]
  node [
    id 409
    label "wiedza"
  ]
  node [
    id 410
    label "bezproblemowy"
  ]
  node [
    id 411
    label "wydarzenie"
  ]
  node [
    id 412
    label "activity"
  ]
  node [
    id 413
    label "tekst"
  ]
  node [
    id 414
    label "g&#322;oska"
  ]
  node [
    id 415
    label "zapowied&#378;"
  ]
  node [
    id 416
    label "wymowa"
  ]
  node [
    id 417
    label "lead"
  ]
  node [
    id 418
    label "sp&#281;dzenie"
  ]
  node [
    id 419
    label "wzbudzenie"
  ]
  node [
    id 420
    label "zainstalowanie"
  ]
  node [
    id 421
    label "spe&#322;nienie"
  ]
  node [
    id 422
    label "znalezienie_si&#281;"
  ]
  node [
    id 423
    label "introduction"
  ]
  node [
    id 424
    label "pos&#322;anie"
  ]
  node [
    id 425
    label "layout"
  ]
  node [
    id 426
    label "poumieszczanie"
  ]
  node [
    id 427
    label "ulokowanie_si&#281;"
  ]
  node [
    id 428
    label "siedzenie"
  ]
  node [
    id 429
    label "uplasowanie"
  ]
  node [
    id 430
    label "zakrycie"
  ]
  node [
    id 431
    label "ustalenie"
  ]
  node [
    id 432
    label "pomieszczenie"
  ]
  node [
    id 433
    label "prze&#322;adowanie"
  ]
  node [
    id 434
    label "zawarcie"
  ]
  node [
    id 435
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 436
    label "knowing"
  ]
  node [
    id 437
    label "poinformowanie"
  ]
  node [
    id 438
    label "obznajomienie"
  ]
  node [
    id 439
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 440
    label "gathering"
  ]
  node [
    id 441
    label "representation"
  ]
  node [
    id 442
    label "znajomy"
  ]
  node [
    id 443
    label "wymienienie"
  ]
  node [
    id 444
    label "refresher_course"
  ]
  node [
    id 445
    label "potraktowanie"
  ]
  node [
    id 446
    label "przewietrzenie_si&#281;"
  ]
  node [
    id 447
    label "oczyszczenie"
  ]
  node [
    id 448
    label "vaporization"
  ]
  node [
    id 449
    label "powietrze"
  ]
  node [
    id 450
    label "ventilation"
  ]
  node [
    id 451
    label "rozpowszechnianie"
  ]
  node [
    id 452
    label "proces"
  ]
  node [
    id 453
    label "rynek_wt&#243;rny"
  ]
  node [
    id 454
    label "wprowadzanie"
  ]
  node [
    id 455
    label "gospodarka"
  ]
  node [
    id 456
    label "segment_rynku"
  ]
  node [
    id 457
    label "targowica"
  ]
  node [
    id 458
    label "obiekt_handlowy"
  ]
  node [
    id 459
    label "konsument"
  ]
  node [
    id 460
    label "rynek_podstawowy"
  ]
  node [
    id 461
    label "emitowa&#263;"
  ]
  node [
    id 462
    label "wprowadzi&#263;"
  ]
  node [
    id 463
    label "wytw&#243;rca"
  ]
  node [
    id 464
    label "emitowanie"
  ]
  node [
    id 465
    label "plac"
  ]
  node [
    id 466
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 467
    label "biznes"
  ]
  node [
    id 468
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 469
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 470
    label "stoisko"
  ]
  node [
    id 471
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 472
    label "wprowadza&#263;"
  ]
  node [
    id 473
    label "kram"
  ]
  node [
    id 474
    label "rozprawa"
  ]
  node [
    id 475
    label "paper"
  ]
  node [
    id 476
    label "przygotowanie"
  ]
  node [
    id 477
    label "pisa&#263;"
  ]
  node [
    id 478
    label "redakcja"
  ]
  node [
    id 479
    label "j&#281;zykowo"
  ]
  node [
    id 480
    label "preparacja"
  ]
  node [
    id 481
    label "dzie&#322;o"
  ]
  node [
    id 482
    label "wypowied&#378;"
  ]
  node [
    id 483
    label "obelga"
  ]
  node [
    id 484
    label "wytw&#243;r"
  ]
  node [
    id 485
    label "odmianka"
  ]
  node [
    id 486
    label "opu&#347;ci&#263;"
  ]
  node [
    id 487
    label "pomini&#281;cie"
  ]
  node [
    id 488
    label "koniektura"
  ]
  node [
    id 489
    label "ekscerpcja"
  ]
  node [
    id 490
    label "zorganizowanie"
  ]
  node [
    id 491
    label "przekwalifikowanie"
  ]
  node [
    id 492
    label "zaplanowanie"
  ]
  node [
    id 493
    label "preparation"
  ]
  node [
    id 494
    label "wykonanie"
  ]
  node [
    id 495
    label "gotowy"
  ]
  node [
    id 496
    label "nauczenie"
  ]
  node [
    id 497
    label "cytat"
  ]
  node [
    id 498
    label "s&#261;d"
  ]
  node [
    id 499
    label "obja&#347;nienie"
  ]
  node [
    id 500
    label "s&#261;dzenie"
  ]
  node [
    id 501
    label "obrady"
  ]
  node [
    id 502
    label "rozumowanie"
  ]
  node [
    id 503
    label "gwiazda"
  ]
  node [
    id 504
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 505
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 506
    label "Nibiru"
  ]
  node [
    id 507
    label "supergrupa"
  ]
  node [
    id 508
    label "obiekt"
  ]
  node [
    id 509
    label "konstelacja"
  ]
  node [
    id 510
    label "gromada"
  ]
  node [
    id 511
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 512
    label "ornament"
  ]
  node [
    id 513
    label "promie&#324;"
  ]
  node [
    id 514
    label "agregatka"
  ]
  node [
    id 515
    label "Gwiazda_Polarna"
  ]
  node [
    id 516
    label "Arktur"
  ]
  node [
    id 517
    label "delta_Scuti"
  ]
  node [
    id 518
    label "s&#322;awa"
  ]
  node [
    id 519
    label "S&#322;o&#324;ce"
  ]
  node [
    id 520
    label "gwiazdosz"
  ]
  node [
    id 521
    label "asocjacja_gwiazd"
  ]
  node [
    id 522
    label "star"
  ]
  node [
    id 523
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 524
    label "kszta&#322;t"
  ]
  node [
    id 525
    label "&#347;wiat&#322;o"
  ]
  node [
    id 526
    label "praktyczno&#347;&#263;"
  ]
  node [
    id 527
    label "functionality"
  ]
  node [
    id 528
    label "u&#380;ytkowo&#347;&#263;"
  ]
  node [
    id 529
    label "practicality"
  ]
  node [
    id 530
    label "rzeczowo&#347;&#263;"
  ]
  node [
    id 531
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 532
    label "u&#380;yteczno&#347;&#263;"
  ]
  node [
    id 533
    label "zmy&#347;lno&#347;&#263;"
  ]
  node [
    id 534
    label "akademicki"
  ]
  node [
    id 535
    label "intelektualny"
  ]
  node [
    id 536
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 537
    label "prawid&#322;owy"
  ]
  node [
    id 538
    label "naukowy"
  ]
  node [
    id 539
    label "akademiczny"
  ]
  node [
    id 540
    label "studencki"
  ]
  node [
    id 541
    label "po_akademicku"
  ]
  node [
    id 542
    label "teoretyczny"
  ]
  node [
    id 543
    label "akademicko"
  ]
  node [
    id 544
    label "odpowiedni"
  ]
  node [
    id 545
    label "tradycyjny"
  ]
  node [
    id 546
    label "szkolny"
  ]
  node [
    id 547
    label "struktura"
  ]
  node [
    id 548
    label "nadwozie"
  ]
  node [
    id 549
    label "skorupa_ziemska"
  ]
  node [
    id 550
    label "podeszwa"
  ]
  node [
    id 551
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 552
    label "p&#322;aszczyzna"
  ]
  node [
    id 553
    label "koturn"
  ]
  node [
    id 554
    label "but"
  ]
  node [
    id 555
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 556
    label "sfera"
  ]
  node [
    id 557
    label "p&#322;aszczak"
  ]
  node [
    id 558
    label "zakres"
  ]
  node [
    id 559
    label "&#347;ciana"
  ]
  node [
    id 560
    label "cia&#322;o"
  ]
  node [
    id 561
    label "ukszta&#322;towanie"
  ]
  node [
    id 562
    label "kwadrant"
  ]
  node [
    id 563
    label "degree"
  ]
  node [
    id 564
    label "wymiar"
  ]
  node [
    id 565
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 566
    label "powierzchnia"
  ]
  node [
    id 567
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 568
    label "surface"
  ]
  node [
    id 569
    label "o&#347;"
  ]
  node [
    id 570
    label "zachowanie"
  ]
  node [
    id 571
    label "podsystem"
  ]
  node [
    id 572
    label "systemat"
  ]
  node [
    id 573
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 574
    label "system"
  ]
  node [
    id 575
    label "rozprz&#261;c"
  ]
  node [
    id 576
    label "konstrukcja"
  ]
  node [
    id 577
    label "cybernetyk"
  ]
  node [
    id 578
    label "mechanika"
  ]
  node [
    id 579
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 580
    label "usenet"
  ]
  node [
    id 581
    label "sk&#322;ad"
  ]
  node [
    id 582
    label "class"
  ]
  node [
    id 583
    label "sector"
  ]
  node [
    id 584
    label "kula"
  ]
  node [
    id 585
    label "p&#243;&#322;kula"
  ]
  node [
    id 586
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 587
    label "strefa"
  ]
  node [
    id 588
    label "kolur"
  ]
  node [
    id 589
    label "huczek"
  ]
  node [
    id 590
    label "grupa"
  ]
  node [
    id 591
    label "p&#243;&#322;sfera"
  ]
  node [
    id 592
    label "stopa"
  ]
  node [
    id 593
    label "reflektor"
  ]
  node [
    id 594
    label "karoseria"
  ]
  node [
    id 595
    label "obudowa"
  ]
  node [
    id 596
    label "pr&#243;g"
  ]
  node [
    id 597
    label "zderzak"
  ]
  node [
    id 598
    label "dach"
  ]
  node [
    id 599
    label "b&#322;otnik"
  ]
  node [
    id 600
    label "pojazd"
  ]
  node [
    id 601
    label "spoiler"
  ]
  node [
    id 602
    label "buda"
  ]
  node [
    id 603
    label "rozbijarka"
  ]
  node [
    id 604
    label "j&#281;zyk"
  ]
  node [
    id 605
    label "cholewa"
  ]
  node [
    id 606
    label "raki"
  ]
  node [
    id 607
    label "wzuwanie"
  ]
  node [
    id 608
    label "sznurowad&#322;o"
  ]
  node [
    id 609
    label "obuwie"
  ]
  node [
    id 610
    label "zapi&#281;tek"
  ]
  node [
    id 611
    label "wzu&#263;"
  ]
  node [
    id 612
    label "cholewka"
  ]
  node [
    id 613
    label "zel&#243;wka"
  ]
  node [
    id 614
    label "wzucie"
  ]
  node [
    id 615
    label "napi&#281;tek"
  ]
  node [
    id 616
    label "obcas"
  ]
  node [
    id 617
    label "przyszwa"
  ]
  node [
    id 618
    label "podnios&#322;o&#347;&#263;"
  ]
  node [
    id 619
    label "pathos"
  ]
  node [
    id 620
    label "styl"
  ]
  node [
    id 621
    label "kieliszek"
  ]
  node [
    id 622
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 623
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 624
    label "w&#243;dka"
  ]
  node [
    id 625
    label "ujednolicenie"
  ]
  node [
    id 626
    label "ten"
  ]
  node [
    id 627
    label "jaki&#347;"
  ]
  node [
    id 628
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 629
    label "jednakowy"
  ]
  node [
    id 630
    label "jednolicie"
  ]
  node [
    id 631
    label "shot"
  ]
  node [
    id 632
    label "naczynie"
  ]
  node [
    id 633
    label "zawarto&#347;&#263;"
  ]
  node [
    id 634
    label "szk&#322;o"
  ]
  node [
    id 635
    label "mohorycz"
  ]
  node [
    id 636
    label "gorza&#322;ka"
  ]
  node [
    id 637
    label "alkohol"
  ]
  node [
    id 638
    label "sznaps"
  ]
  node [
    id 639
    label "nap&#243;j"
  ]
  node [
    id 640
    label "taki&#380;"
  ]
  node [
    id 641
    label "identyczny"
  ]
  node [
    id 642
    label "zr&#243;wnanie"
  ]
  node [
    id 643
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 644
    label "zr&#243;wnywanie"
  ]
  node [
    id 645
    label "mundurowanie"
  ]
  node [
    id 646
    label "mundurowa&#263;"
  ]
  node [
    id 647
    label "jednakowo"
  ]
  node [
    id 648
    label "okre&#347;lony"
  ]
  node [
    id 649
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 650
    label "z&#322;o&#380;ony"
  ]
  node [
    id 651
    label "jako&#347;"
  ]
  node [
    id 652
    label "niez&#322;y"
  ]
  node [
    id 653
    label "charakterystyczny"
  ]
  node [
    id 654
    label "jako_tako"
  ]
  node [
    id 655
    label "ciekawy"
  ]
  node [
    id 656
    label "dziwny"
  ]
  node [
    id 657
    label "przyzwoity"
  ]
  node [
    id 658
    label "g&#322;&#281;bszy"
  ]
  node [
    id 659
    label "drink"
  ]
  node [
    id 660
    label "jednolity"
  ]
  node [
    id 661
    label "upodobnienie"
  ]
  node [
    id 662
    label "calibration"
  ]
  node [
    id 663
    label "adjust"
  ]
  node [
    id 664
    label "zmieni&#263;"
  ]
  node [
    id 665
    label "come_up"
  ]
  node [
    id 666
    label "sprawi&#263;"
  ]
  node [
    id 667
    label "zyska&#263;"
  ]
  node [
    id 668
    label "change"
  ]
  node [
    id 669
    label "straci&#263;"
  ]
  node [
    id 670
    label "zast&#261;pi&#263;"
  ]
  node [
    id 671
    label "przej&#347;&#263;"
  ]
  node [
    id 672
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 673
    label "Gargantua"
  ]
  node [
    id 674
    label "Chocho&#322;"
  ]
  node [
    id 675
    label "Hamlet"
  ]
  node [
    id 676
    label "profanum"
  ]
  node [
    id 677
    label "Wallenrod"
  ]
  node [
    id 678
    label "Quasimodo"
  ]
  node [
    id 679
    label "homo_sapiens"
  ]
  node [
    id 680
    label "parali&#380;owa&#263;"
  ]
  node [
    id 681
    label "Plastu&#347;"
  ]
  node [
    id 682
    label "ludzko&#347;&#263;"
  ]
  node [
    id 683
    label "posta&#263;"
  ]
  node [
    id 684
    label "portrecista"
  ]
  node [
    id 685
    label "istota"
  ]
  node [
    id 686
    label "Casanova"
  ]
  node [
    id 687
    label "Szwejk"
  ]
  node [
    id 688
    label "Edyp"
  ]
  node [
    id 689
    label "Don_Juan"
  ]
  node [
    id 690
    label "Werter"
  ]
  node [
    id 691
    label "duch"
  ]
  node [
    id 692
    label "person"
  ]
  node [
    id 693
    label "Harry_Potter"
  ]
  node [
    id 694
    label "Sherlock_Holmes"
  ]
  node [
    id 695
    label "antropochoria"
  ]
  node [
    id 696
    label "figura"
  ]
  node [
    id 697
    label "Dwukwiat"
  ]
  node [
    id 698
    label "g&#322;owa"
  ]
  node [
    id 699
    label "mikrokosmos"
  ]
  node [
    id 700
    label "Winnetou"
  ]
  node [
    id 701
    label "oddzia&#322;ywanie"
  ]
  node [
    id 702
    label "Don_Kiszot"
  ]
  node [
    id 703
    label "Herkules_Poirot"
  ]
  node [
    id 704
    label "Faust"
  ]
  node [
    id 705
    label "Zgredek"
  ]
  node [
    id 706
    label "Dulcynea"
  ]
  node [
    id 707
    label "superego"
  ]
  node [
    id 708
    label "mentalno&#347;&#263;"
  ]
  node [
    id 709
    label "znaczenie"
  ]
  node [
    id 710
    label "wn&#281;trze"
  ]
  node [
    id 711
    label "psychika"
  ]
  node [
    id 712
    label "wytrzyma&#263;"
  ]
  node [
    id 713
    label "trim"
  ]
  node [
    id 714
    label "Osjan"
  ]
  node [
    id 715
    label "formacja"
  ]
  node [
    id 716
    label "point"
  ]
  node [
    id 717
    label "kto&#347;"
  ]
  node [
    id 718
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 719
    label "pozosta&#263;"
  ]
  node [
    id 720
    label "cz&#322;owiek"
  ]
  node [
    id 721
    label "przedstawienie"
  ]
  node [
    id 722
    label "Aspazja"
  ]
  node [
    id 723
    label "go&#347;&#263;"
  ]
  node [
    id 724
    label "budowa"
  ]
  node [
    id 725
    label "osobowo&#347;&#263;"
  ]
  node [
    id 726
    label "charakterystyka"
  ]
  node [
    id 727
    label "kompleksja"
  ]
  node [
    id 728
    label "wygl&#261;d"
  ]
  node [
    id 729
    label "punkt_widzenia"
  ]
  node [
    id 730
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 731
    label "zaistnie&#263;"
  ]
  node [
    id 732
    label "hamper"
  ]
  node [
    id 733
    label "pora&#380;a&#263;"
  ]
  node [
    id 734
    label "mrozi&#263;"
  ]
  node [
    id 735
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 736
    label "spasm"
  ]
  node [
    id 737
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 738
    label "zdolno&#347;&#263;"
  ]
  node [
    id 739
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 740
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 741
    label "umys&#322;"
  ]
  node [
    id 742
    label "kierowa&#263;"
  ]
  node [
    id 743
    label "sztuka"
  ]
  node [
    id 744
    label "czaszka"
  ]
  node [
    id 745
    label "g&#243;ra"
  ]
  node [
    id 746
    label "fryzura"
  ]
  node [
    id 747
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 748
    label "pryncypa&#322;"
  ]
  node [
    id 749
    label "ro&#347;lina"
  ]
  node [
    id 750
    label "ucho"
  ]
  node [
    id 751
    label "byd&#322;o"
  ]
  node [
    id 752
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 753
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 754
    label "kierownictwo"
  ]
  node [
    id 755
    label "&#347;ci&#281;cie"
  ]
  node [
    id 756
    label "makrocefalia"
  ]
  node [
    id 757
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 758
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 759
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 760
    label "&#380;ycie"
  ]
  node [
    id 761
    label "dekiel"
  ]
  node [
    id 762
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 763
    label "m&#243;zg"
  ]
  node [
    id 764
    label "&#347;ci&#281;gno"
  ]
  node [
    id 765
    label "noosfera"
  ]
  node [
    id 766
    label "dziedzina"
  ]
  node [
    id 767
    label "hipnotyzowanie"
  ]
  node [
    id 768
    label "powodowanie"
  ]
  node [
    id 769
    label "act"
  ]
  node [
    id 770
    label "&#347;lad"
  ]
  node [
    id 771
    label "rezultat"
  ]
  node [
    id 772
    label "reakcja_chemiczna"
  ]
  node [
    id 773
    label "docieranie"
  ]
  node [
    id 774
    label "lobbysta"
  ]
  node [
    id 775
    label "natural_process"
  ]
  node [
    id 776
    label "wdzieranie_si&#281;"
  ]
  node [
    id 777
    label "allochoria"
  ]
  node [
    id 778
    label "malarz"
  ]
  node [
    id 779
    label "artysta"
  ]
  node [
    id 780
    label "fotograf"
  ]
  node [
    id 781
    label "obiekt_matematyczny"
  ]
  node [
    id 782
    label "gestaltyzm"
  ]
  node [
    id 783
    label "d&#378;wi&#281;k"
  ]
  node [
    id 784
    label "ornamentyka"
  ]
  node [
    id 785
    label "stylistyka"
  ]
  node [
    id 786
    label "podzbi&#243;r"
  ]
  node [
    id 787
    label "antycypacja"
  ]
  node [
    id 788
    label "przedmiot"
  ]
  node [
    id 789
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 790
    label "wiersz"
  ]
  node [
    id 791
    label "facet"
  ]
  node [
    id 792
    label "popis"
  ]
  node [
    id 793
    label "obraz"
  ]
  node [
    id 794
    label "informacja"
  ]
  node [
    id 795
    label "symetria"
  ]
  node [
    id 796
    label "figure"
  ]
  node [
    id 797
    label "rzecz"
  ]
  node [
    id 798
    label "perspektywa"
  ]
  node [
    id 799
    label "lingwistyka_kognitywna"
  ]
  node [
    id 800
    label "character"
  ]
  node [
    id 801
    label "rze&#378;ba"
  ]
  node [
    id 802
    label "bierka_szachowa"
  ]
  node [
    id 803
    label "karta"
  ]
  node [
    id 804
    label "Szekspir"
  ]
  node [
    id 805
    label "Mickiewicz"
  ]
  node [
    id 806
    label "cierpienie"
  ]
  node [
    id 807
    label "deformowa&#263;"
  ]
  node [
    id 808
    label "deformowanie"
  ]
  node [
    id 809
    label "sfera_afektywna"
  ]
  node [
    id 810
    label "sumienie"
  ]
  node [
    id 811
    label "entity"
  ]
  node [
    id 812
    label "istota_nadprzyrodzona"
  ]
  node [
    id 813
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 814
    label "fizjonomia"
  ]
  node [
    id 815
    label "power"
  ]
  node [
    id 816
    label "byt"
  ]
  node [
    id 817
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 818
    label "human_body"
  ]
  node [
    id 819
    label "podekscytowanie"
  ]
  node [
    id 820
    label "kompleks"
  ]
  node [
    id 821
    label "piek&#322;o"
  ]
  node [
    id 822
    label "oddech"
  ]
  node [
    id 823
    label "ofiarowywa&#263;"
  ]
  node [
    id 824
    label "nekromancja"
  ]
  node [
    id 825
    label "si&#322;a"
  ]
  node [
    id 826
    label "seksualno&#347;&#263;"
  ]
  node [
    id 827
    label "zjawa"
  ]
  node [
    id 828
    label "zapalno&#347;&#263;"
  ]
  node [
    id 829
    label "ego"
  ]
  node [
    id 830
    label "ofiarowa&#263;"
  ]
  node [
    id 831
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 832
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 833
    label "Po&#347;wist"
  ]
  node [
    id 834
    label "passion"
  ]
  node [
    id 835
    label "zmar&#322;y"
  ]
  node [
    id 836
    label "ofiarowanie"
  ]
  node [
    id 837
    label "ofiarowywanie"
  ]
  node [
    id 838
    label "T&#281;sknica"
  ]
  node [
    id 839
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 840
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 841
    label "miniatura"
  ]
  node [
    id 842
    label "przyroda"
  ]
  node [
    id 843
    label "odbicie"
  ]
  node [
    id 844
    label "atom"
  ]
  node [
    id 845
    label "kosmos"
  ]
  node [
    id 846
    label "Ziemia"
  ]
  node [
    id 847
    label "inszy"
  ]
  node [
    id 848
    label "inaczej"
  ]
  node [
    id 849
    label "osobno"
  ]
  node [
    id 850
    label "r&#243;&#380;ny"
  ]
  node [
    id 851
    label "kolejny"
  ]
  node [
    id 852
    label "odr&#281;bny"
  ]
  node [
    id 853
    label "nast&#281;pnie"
  ]
  node [
    id 854
    label "kolejno"
  ]
  node [
    id 855
    label "kt&#243;ry&#347;"
  ]
  node [
    id 856
    label "nastopny"
  ]
  node [
    id 857
    label "r&#243;&#380;nie"
  ]
  node [
    id 858
    label "niestandardowo"
  ]
  node [
    id 859
    label "osobny"
  ]
  node [
    id 860
    label "osobnie"
  ]
  node [
    id 861
    label "odr&#281;bnie"
  ]
  node [
    id 862
    label "udzielnie"
  ]
  node [
    id 863
    label "individually"
  ]
  node [
    id 864
    label "wpr&#281;dce"
  ]
  node [
    id 865
    label "nied&#322;ugi"
  ]
  node [
    id 866
    label "blisko"
  ]
  node [
    id 867
    label "dok&#322;adnie"
  ]
  node [
    id 868
    label "bliski"
  ]
  node [
    id 869
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 870
    label "silnie"
  ]
  node [
    id 871
    label "nied&#322;ugo"
  ]
  node [
    id 872
    label "od_nied&#322;uga"
  ]
  node [
    id 873
    label "jednowyrazowy"
  ]
  node [
    id 874
    label "kr&#243;tko"
  ]
  node [
    id 875
    label "szybki"
  ]
  node [
    id 876
    label "zmienia&#263;"
  ]
  node [
    id 877
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 878
    label "powodowa&#263;"
  ]
  node [
    id 879
    label "determine"
  ]
  node [
    id 880
    label "work"
  ]
  node [
    id 881
    label "zyskiwa&#263;"
  ]
  node [
    id 882
    label "alternate"
  ]
  node [
    id 883
    label "traci&#263;"
  ]
  node [
    id 884
    label "zast&#281;powa&#263;"
  ]
  node [
    id 885
    label "przechodzi&#263;"
  ]
  node [
    id 886
    label "sprawia&#263;"
  ]
  node [
    id 887
    label "reengineering"
  ]
  node [
    id 888
    label "wiadomy"
  ]
  node [
    id 889
    label "e"
  ]
  node [
    id 890
    label "Learningowej"
  ]
  node [
    id 891
    label "XHTML"
  ]
  node [
    id 892
    label "1"
  ]
  node [
    id 893
    label "0"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 889
  ]
  edge [
    source 10
    target 890
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 38
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 703
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 706
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 508
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 409
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 376
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 560
  ]
  edge [
    source 17
    target 524
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 74
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 287
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 552
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 306
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 627
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 864
  ]
  edge [
    source 20
    target 865
  ]
  edge [
    source 20
    target 866
  ]
  edge [
    source 20
    target 867
  ]
  edge [
    source 20
    target 868
  ]
  edge [
    source 20
    target 869
  ]
  edge [
    source 20
    target 870
  ]
  edge [
    source 20
    target 871
  ]
  edge [
    source 20
    target 872
  ]
  edge [
    source 20
    target 873
  ]
  edge [
    source 20
    target 874
  ]
  edge [
    source 20
    target 875
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 877
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 772
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 668
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 648
  ]
  edge [
    source 24
    target 649
  ]
  edge [
    source 24
    target 888
  ]
  edge [
    source 24
    target 626
  ]
  edge [
    source 889
    target 890
  ]
  edge [
    source 891
    target 892
  ]
  edge [
    source 891
    target 893
  ]
  edge [
    source 892
    target 893
  ]
]
