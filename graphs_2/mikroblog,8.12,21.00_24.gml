graph [
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "&#322;aciata"
    origin "text"
  ]
  node [
    id 2
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "supermarket"
    origin "text"
  ]
  node [
    id 4
    label "guangzhou"
    origin "text"
  ]
  node [
    id 5
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "matiwojwchinach"
    origin "text"
  ]
  node [
    id 7
    label "przedmiot"
  ]
  node [
    id 8
    label "Polish"
  ]
  node [
    id 9
    label "goniony"
  ]
  node [
    id 10
    label "oberek"
  ]
  node [
    id 11
    label "ryba_po_grecku"
  ]
  node [
    id 12
    label "sztajer"
  ]
  node [
    id 13
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 14
    label "krakowiak"
  ]
  node [
    id 15
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 16
    label "pierogi_ruskie"
  ]
  node [
    id 17
    label "lacki"
  ]
  node [
    id 18
    label "polak"
  ]
  node [
    id 19
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 20
    label "chodzony"
  ]
  node [
    id 21
    label "po_polsku"
  ]
  node [
    id 22
    label "mazur"
  ]
  node [
    id 23
    label "polsko"
  ]
  node [
    id 24
    label "skoczny"
  ]
  node [
    id 25
    label "drabant"
  ]
  node [
    id 26
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 27
    label "j&#281;zyk"
  ]
  node [
    id 28
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 29
    label "artykulator"
  ]
  node [
    id 30
    label "kod"
  ]
  node [
    id 31
    label "kawa&#322;ek"
  ]
  node [
    id 32
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 33
    label "gramatyka"
  ]
  node [
    id 34
    label "stylik"
  ]
  node [
    id 35
    label "przet&#322;umaczenie"
  ]
  node [
    id 36
    label "formalizowanie"
  ]
  node [
    id 37
    label "ssa&#263;"
  ]
  node [
    id 38
    label "ssanie"
  ]
  node [
    id 39
    label "language"
  ]
  node [
    id 40
    label "liza&#263;"
  ]
  node [
    id 41
    label "napisa&#263;"
  ]
  node [
    id 42
    label "konsonantyzm"
  ]
  node [
    id 43
    label "wokalizm"
  ]
  node [
    id 44
    label "pisa&#263;"
  ]
  node [
    id 45
    label "fonetyka"
  ]
  node [
    id 46
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 47
    label "jeniec"
  ]
  node [
    id 48
    label "but"
  ]
  node [
    id 49
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 50
    label "po_koroniarsku"
  ]
  node [
    id 51
    label "kultura_duchowa"
  ]
  node [
    id 52
    label "t&#322;umaczenie"
  ]
  node [
    id 53
    label "m&#243;wienie"
  ]
  node [
    id 54
    label "pype&#263;"
  ]
  node [
    id 55
    label "lizanie"
  ]
  node [
    id 56
    label "pismo"
  ]
  node [
    id 57
    label "formalizowa&#263;"
  ]
  node [
    id 58
    label "rozumie&#263;"
  ]
  node [
    id 59
    label "organ"
  ]
  node [
    id 60
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 61
    label "rozumienie"
  ]
  node [
    id 62
    label "spos&#243;b"
  ]
  node [
    id 63
    label "makroglosja"
  ]
  node [
    id 64
    label "m&#243;wi&#263;"
  ]
  node [
    id 65
    label "jama_ustna"
  ]
  node [
    id 66
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 67
    label "formacja_geologiczna"
  ]
  node [
    id 68
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 69
    label "natural_language"
  ]
  node [
    id 70
    label "s&#322;ownictwo"
  ]
  node [
    id 71
    label "urz&#261;dzenie"
  ]
  node [
    id 72
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 73
    label "wschodnioeuropejski"
  ]
  node [
    id 74
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 75
    label "poga&#324;ski"
  ]
  node [
    id 76
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 77
    label "topielec"
  ]
  node [
    id 78
    label "europejski"
  ]
  node [
    id 79
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 80
    label "langosz"
  ]
  node [
    id 81
    label "zboczenie"
  ]
  node [
    id 82
    label "om&#243;wienie"
  ]
  node [
    id 83
    label "sponiewieranie"
  ]
  node [
    id 84
    label "discipline"
  ]
  node [
    id 85
    label "rzecz"
  ]
  node [
    id 86
    label "omawia&#263;"
  ]
  node [
    id 87
    label "kr&#261;&#380;enie"
  ]
  node [
    id 88
    label "tre&#347;&#263;"
  ]
  node [
    id 89
    label "robienie"
  ]
  node [
    id 90
    label "sponiewiera&#263;"
  ]
  node [
    id 91
    label "element"
  ]
  node [
    id 92
    label "entity"
  ]
  node [
    id 93
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 94
    label "tematyka"
  ]
  node [
    id 95
    label "w&#261;tek"
  ]
  node [
    id 96
    label "charakter"
  ]
  node [
    id 97
    label "zbaczanie"
  ]
  node [
    id 98
    label "program_nauczania"
  ]
  node [
    id 99
    label "om&#243;wi&#263;"
  ]
  node [
    id 100
    label "omawianie"
  ]
  node [
    id 101
    label "thing"
  ]
  node [
    id 102
    label "kultura"
  ]
  node [
    id 103
    label "istota"
  ]
  node [
    id 104
    label "zbacza&#263;"
  ]
  node [
    id 105
    label "zboczy&#263;"
  ]
  node [
    id 106
    label "gwardzista"
  ]
  node [
    id 107
    label "melodia"
  ]
  node [
    id 108
    label "taniec"
  ]
  node [
    id 109
    label "taniec_ludowy"
  ]
  node [
    id 110
    label "&#347;redniowieczny"
  ]
  node [
    id 111
    label "europejsko"
  ]
  node [
    id 112
    label "specjalny"
  ]
  node [
    id 113
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 114
    label "weso&#322;y"
  ]
  node [
    id 115
    label "sprawny"
  ]
  node [
    id 116
    label "rytmiczny"
  ]
  node [
    id 117
    label "skocznie"
  ]
  node [
    id 118
    label "energiczny"
  ]
  node [
    id 119
    label "przytup"
  ]
  node [
    id 120
    label "ho&#322;ubiec"
  ]
  node [
    id 121
    label "wodzi&#263;"
  ]
  node [
    id 122
    label "lendler"
  ]
  node [
    id 123
    label "austriacki"
  ]
  node [
    id 124
    label "polka"
  ]
  node [
    id 125
    label "ludowy"
  ]
  node [
    id 126
    label "pie&#347;&#324;"
  ]
  node [
    id 127
    label "mieszkaniec"
  ]
  node [
    id 128
    label "centu&#347;"
  ]
  node [
    id 129
    label "lalka"
  ]
  node [
    id 130
    label "Ma&#322;opolanin"
  ]
  node [
    id 131
    label "krakauer"
  ]
  node [
    id 132
    label "chi&#324;sko"
  ]
  node [
    id 133
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 134
    label "po_chi&#324;sku"
  ]
  node [
    id 135
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 136
    label "kitajski"
  ]
  node [
    id 137
    label "lipny"
  ]
  node [
    id 138
    label "go"
  ]
  node [
    id 139
    label "niedrogi"
  ]
  node [
    id 140
    label "makroj&#281;zyk"
  ]
  node [
    id 141
    label "dziwaczny"
  ]
  node [
    id 142
    label "azjatycki"
  ]
  node [
    id 143
    label "tandetny"
  ]
  node [
    id 144
    label "dalekowschodni"
  ]
  node [
    id 145
    label "niedrogo"
  ]
  node [
    id 146
    label "kiczowaty"
  ]
  node [
    id 147
    label "banalny"
  ]
  node [
    id 148
    label "nieelegancki"
  ]
  node [
    id 149
    label "&#380;a&#322;osny"
  ]
  node [
    id 150
    label "tani"
  ]
  node [
    id 151
    label "kiepski"
  ]
  node [
    id 152
    label "tandetnie"
  ]
  node [
    id 153
    label "odra&#380;aj&#261;cy"
  ]
  node [
    id 154
    label "nikczemny"
  ]
  node [
    id 155
    label "lipnie"
  ]
  node [
    id 156
    label "siajowy"
  ]
  node [
    id 157
    label "nieprawdziwy"
  ]
  node [
    id 158
    label "z&#322;y"
  ]
  node [
    id 159
    label "po_dalekowschodniemu"
  ]
  node [
    id 160
    label "j&#281;zyk_naturalny"
  ]
  node [
    id 161
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 162
    label "kampong"
  ]
  node [
    id 163
    label "typowy"
  ]
  node [
    id 164
    label "ghaty"
  ]
  node [
    id 165
    label "charakterystyczny"
  ]
  node [
    id 166
    label "balut"
  ]
  node [
    id 167
    label "ka&#322;mucki"
  ]
  node [
    id 168
    label "azjatycko"
  ]
  node [
    id 169
    label "dziwny"
  ]
  node [
    id 170
    label "dziwotworny"
  ]
  node [
    id 171
    label "dziwacznie"
  ]
  node [
    id 172
    label "goban"
  ]
  node [
    id 173
    label "gra_planszowa"
  ]
  node [
    id 174
    label "sport_umys&#322;owy"
  ]
  node [
    id 175
    label "market"
  ]
  node [
    id 176
    label "sklep"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
]
