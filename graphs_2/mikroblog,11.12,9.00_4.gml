graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "pytanie"
    origin "text"
  ]
  node [
    id 2
    label "zwiazki"
    origin "text"
  ]
  node [
    id 3
    label "logikarozowychpaskow"
    origin "text"
  ]
  node [
    id 4
    label "przodkini"
  ]
  node [
    id 5
    label "matka_zast&#281;pcza"
  ]
  node [
    id 6
    label "matczysko"
  ]
  node [
    id 7
    label "rodzice"
  ]
  node [
    id 8
    label "stara"
  ]
  node [
    id 9
    label "macierz"
  ]
  node [
    id 10
    label "rodzic"
  ]
  node [
    id 11
    label "Matka_Boska"
  ]
  node [
    id 12
    label "macocha"
  ]
  node [
    id 13
    label "starzy"
  ]
  node [
    id 14
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 15
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 16
    label "pokolenie"
  ]
  node [
    id 17
    label "wapniaki"
  ]
  node [
    id 18
    label "krewna"
  ]
  node [
    id 19
    label "opiekun"
  ]
  node [
    id 20
    label "wapniak"
  ]
  node [
    id 21
    label "rodzic_chrzestny"
  ]
  node [
    id 22
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 23
    label "matka"
  ]
  node [
    id 24
    label "&#380;ona"
  ]
  node [
    id 25
    label "kobieta"
  ]
  node [
    id 26
    label "partnerka"
  ]
  node [
    id 27
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 28
    label "matuszka"
  ]
  node [
    id 29
    label "parametryzacja"
  ]
  node [
    id 30
    label "pa&#324;stwo"
  ]
  node [
    id 31
    label "poj&#281;cie"
  ]
  node [
    id 32
    label "mod"
  ]
  node [
    id 33
    label "patriota"
  ]
  node [
    id 34
    label "m&#281;&#380;atka"
  ]
  node [
    id 35
    label "sprawa"
  ]
  node [
    id 36
    label "wypytanie"
  ]
  node [
    id 37
    label "egzaminowanie"
  ]
  node [
    id 38
    label "zwracanie_si&#281;"
  ]
  node [
    id 39
    label "wywo&#322;ywanie"
  ]
  node [
    id 40
    label "rozpytywanie"
  ]
  node [
    id 41
    label "wypowiedzenie"
  ]
  node [
    id 42
    label "wypowied&#378;"
  ]
  node [
    id 43
    label "problemat"
  ]
  node [
    id 44
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 45
    label "problematyka"
  ]
  node [
    id 46
    label "sprawdzian"
  ]
  node [
    id 47
    label "zadanie"
  ]
  node [
    id 48
    label "odpowiada&#263;"
  ]
  node [
    id 49
    label "przes&#322;uchiwanie"
  ]
  node [
    id 50
    label "question"
  ]
  node [
    id 51
    label "sprawdzanie"
  ]
  node [
    id 52
    label "odpowiadanie"
  ]
  node [
    id 53
    label "survey"
  ]
  node [
    id 54
    label "konwersja"
  ]
  node [
    id 55
    label "notice"
  ]
  node [
    id 56
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 57
    label "przepowiedzenie"
  ]
  node [
    id 58
    label "rozwi&#261;zanie"
  ]
  node [
    id 59
    label "generowa&#263;"
  ]
  node [
    id 60
    label "wydanie"
  ]
  node [
    id 61
    label "message"
  ]
  node [
    id 62
    label "generowanie"
  ]
  node [
    id 63
    label "wydobycie"
  ]
  node [
    id 64
    label "zwerbalizowanie"
  ]
  node [
    id 65
    label "szyk"
  ]
  node [
    id 66
    label "notification"
  ]
  node [
    id 67
    label "powiedzenie"
  ]
  node [
    id 68
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 69
    label "denunciation"
  ]
  node [
    id 70
    label "wyra&#380;enie"
  ]
  node [
    id 71
    label "pos&#322;uchanie"
  ]
  node [
    id 72
    label "s&#261;d"
  ]
  node [
    id 73
    label "sparafrazowanie"
  ]
  node [
    id 74
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 75
    label "strawestowa&#263;"
  ]
  node [
    id 76
    label "sparafrazowa&#263;"
  ]
  node [
    id 77
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 78
    label "trawestowa&#263;"
  ]
  node [
    id 79
    label "sformu&#322;owanie"
  ]
  node [
    id 80
    label "parafrazowanie"
  ]
  node [
    id 81
    label "ozdobnik"
  ]
  node [
    id 82
    label "delimitacja"
  ]
  node [
    id 83
    label "parafrazowa&#263;"
  ]
  node [
    id 84
    label "stylizacja"
  ]
  node [
    id 85
    label "komunikat"
  ]
  node [
    id 86
    label "trawestowanie"
  ]
  node [
    id 87
    label "strawestowanie"
  ]
  node [
    id 88
    label "rezultat"
  ]
  node [
    id 89
    label "zaj&#281;cie"
  ]
  node [
    id 90
    label "yield"
  ]
  node [
    id 91
    label "zbi&#243;r"
  ]
  node [
    id 92
    label "zaszkodzenie"
  ]
  node [
    id 93
    label "za&#322;o&#380;enie"
  ]
  node [
    id 94
    label "duty"
  ]
  node [
    id 95
    label "powierzanie"
  ]
  node [
    id 96
    label "work"
  ]
  node [
    id 97
    label "problem"
  ]
  node [
    id 98
    label "przepisanie"
  ]
  node [
    id 99
    label "nakarmienie"
  ]
  node [
    id 100
    label "przepisa&#263;"
  ]
  node [
    id 101
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 102
    label "czynno&#347;&#263;"
  ]
  node [
    id 103
    label "zobowi&#261;zanie"
  ]
  node [
    id 104
    label "kognicja"
  ]
  node [
    id 105
    label "object"
  ]
  node [
    id 106
    label "rozprawa"
  ]
  node [
    id 107
    label "temat"
  ]
  node [
    id 108
    label "wydarzenie"
  ]
  node [
    id 109
    label "szczeg&#243;&#322;"
  ]
  node [
    id 110
    label "proposition"
  ]
  node [
    id 111
    label "przes&#322;anka"
  ]
  node [
    id 112
    label "rzecz"
  ]
  node [
    id 113
    label "idea"
  ]
  node [
    id 114
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 115
    label "ustalenie"
  ]
  node [
    id 116
    label "redagowanie"
  ]
  node [
    id 117
    label "ustalanie"
  ]
  node [
    id 118
    label "dociekanie"
  ]
  node [
    id 119
    label "robienie"
  ]
  node [
    id 120
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 121
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 122
    label "investigation"
  ]
  node [
    id 123
    label "macanie"
  ]
  node [
    id 124
    label "usi&#322;owanie"
  ]
  node [
    id 125
    label "penetrowanie"
  ]
  node [
    id 126
    label "przymierzanie"
  ]
  node [
    id 127
    label "przymierzenie"
  ]
  node [
    id 128
    label "examination"
  ]
  node [
    id 129
    label "zbadanie"
  ]
  node [
    id 130
    label "wypytywanie"
  ]
  node [
    id 131
    label "react"
  ]
  node [
    id 132
    label "dawa&#263;"
  ]
  node [
    id 133
    label "by&#263;"
  ]
  node [
    id 134
    label "ponosi&#263;"
  ]
  node [
    id 135
    label "report"
  ]
  node [
    id 136
    label "equate"
  ]
  node [
    id 137
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 138
    label "answer"
  ]
  node [
    id 139
    label "powodowa&#263;"
  ]
  node [
    id 140
    label "tone"
  ]
  node [
    id 141
    label "contend"
  ]
  node [
    id 142
    label "reagowa&#263;"
  ]
  node [
    id 143
    label "impart"
  ]
  node [
    id 144
    label "reagowanie"
  ]
  node [
    id 145
    label "dawanie"
  ]
  node [
    id 146
    label "powodowanie"
  ]
  node [
    id 147
    label "bycie"
  ]
  node [
    id 148
    label "pokutowanie"
  ]
  node [
    id 149
    label "odpowiedzialny"
  ]
  node [
    id 150
    label "winny"
  ]
  node [
    id 151
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 152
    label "picie_piwa"
  ]
  node [
    id 153
    label "odpowiedni"
  ]
  node [
    id 154
    label "parry"
  ]
  node [
    id 155
    label "fit"
  ]
  node [
    id 156
    label "dzianie_si&#281;"
  ]
  node [
    id 157
    label "rendition"
  ]
  node [
    id 158
    label "ponoszenie"
  ]
  node [
    id 159
    label "rozmawianie"
  ]
  node [
    id 160
    label "faza"
  ]
  node [
    id 161
    label "podchodzi&#263;"
  ]
  node [
    id 162
    label "&#263;wiczenie"
  ]
  node [
    id 163
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 164
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 165
    label "praca_pisemna"
  ]
  node [
    id 166
    label "kontrola"
  ]
  node [
    id 167
    label "dydaktyka"
  ]
  node [
    id 168
    label "pr&#243;ba"
  ]
  node [
    id 169
    label "przepytywanie"
  ]
  node [
    id 170
    label "zdawanie"
  ]
  node [
    id 171
    label "oznajmianie"
  ]
  node [
    id 172
    label "wzywanie"
  ]
  node [
    id 173
    label "development"
  ]
  node [
    id 174
    label "exploitation"
  ]
  node [
    id 175
    label "w&#322;&#261;czanie"
  ]
  node [
    id 176
    label "s&#322;uchanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 2
    target 3
  ]
]
