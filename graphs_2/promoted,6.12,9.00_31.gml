graph [
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "noc"
    origin "text"
  ]
  node [
    id 2
    label "p&#243;&#322;noc"
  ]
  node [
    id 3
    label "doba"
  ]
  node [
    id 4
    label "night"
  ]
  node [
    id 5
    label "nokturn"
  ]
  node [
    id 6
    label "czas"
  ]
  node [
    id 7
    label "zjawisko"
  ]
  node [
    id 8
    label "proces"
  ]
  node [
    id 9
    label "boski"
  ]
  node [
    id 10
    label "krajobraz"
  ]
  node [
    id 11
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 12
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 13
    label "przywidzenie"
  ]
  node [
    id 14
    label "presence"
  ]
  node [
    id 15
    label "charakter"
  ]
  node [
    id 16
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 17
    label "poprzedzanie"
  ]
  node [
    id 18
    label "czasoprzestrze&#324;"
  ]
  node [
    id 19
    label "laba"
  ]
  node [
    id 20
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 21
    label "chronometria"
  ]
  node [
    id 22
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 23
    label "rachuba_czasu"
  ]
  node [
    id 24
    label "przep&#322;ywanie"
  ]
  node [
    id 25
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 26
    label "czasokres"
  ]
  node [
    id 27
    label "odczyt"
  ]
  node [
    id 28
    label "chwila"
  ]
  node [
    id 29
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 30
    label "dzieje"
  ]
  node [
    id 31
    label "kategoria_gramatyczna"
  ]
  node [
    id 32
    label "poprzedzenie"
  ]
  node [
    id 33
    label "trawienie"
  ]
  node [
    id 34
    label "pochodzi&#263;"
  ]
  node [
    id 35
    label "period"
  ]
  node [
    id 36
    label "okres_czasu"
  ]
  node [
    id 37
    label "poprzedza&#263;"
  ]
  node [
    id 38
    label "schy&#322;ek"
  ]
  node [
    id 39
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 40
    label "odwlekanie_si&#281;"
  ]
  node [
    id 41
    label "zegar"
  ]
  node [
    id 42
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 43
    label "czwarty_wymiar"
  ]
  node [
    id 44
    label "pochodzenie"
  ]
  node [
    id 45
    label "koniugacja"
  ]
  node [
    id 46
    label "Zeitgeist"
  ]
  node [
    id 47
    label "trawi&#263;"
  ]
  node [
    id 48
    label "pogoda"
  ]
  node [
    id 49
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 50
    label "poprzedzi&#263;"
  ]
  node [
    id 51
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 52
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 53
    label "time_period"
  ]
  node [
    id 54
    label "Boreasz"
  ]
  node [
    id 55
    label "&#347;wiat"
  ]
  node [
    id 56
    label "obszar"
  ]
  node [
    id 57
    label "p&#243;&#322;nocek"
  ]
  node [
    id 58
    label "Ziemia"
  ]
  node [
    id 59
    label "strona_&#347;wiata"
  ]
  node [
    id 60
    label "godzina"
  ]
  node [
    id 61
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 62
    label "tydzie&#324;"
  ]
  node [
    id 63
    label "dzie&#324;"
  ]
  node [
    id 64
    label "long_time"
  ]
  node [
    id 65
    label "jednostka_geologiczna"
  ]
  node [
    id 66
    label "liryczny"
  ]
  node [
    id 67
    label "nocturne"
  ]
  node [
    id 68
    label "dzie&#322;o"
  ]
  node [
    id 69
    label "marzenie_senne"
  ]
  node [
    id 70
    label "wiersz"
  ]
  node [
    id 71
    label "utw&#243;r"
  ]
  node [
    id 72
    label "pejza&#380;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
]
