graph [
  node [
    id 0
    label "taki"
    origin "text"
  ]
  node [
    id 1
    label "chop"
    origin "text"
  ]
  node [
    id 2
    label "klep"
    origin "text"
  ]
  node [
    id 3
    label "twoja"
    origin "text"
  ]
  node [
    id 4
    label "dziewczyne"
    origin "text"
  ]
  node [
    id 5
    label "ty&#322;ek"
    origin "text"
  ]
  node [
    id 6
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "okre&#347;lony"
  ]
  node [
    id 8
    label "jaki&#347;"
  ]
  node [
    id 9
    label "przyzwoity"
  ]
  node [
    id 10
    label "ciekawy"
  ]
  node [
    id 11
    label "jako&#347;"
  ]
  node [
    id 12
    label "jako_tako"
  ]
  node [
    id 13
    label "niez&#322;y"
  ]
  node [
    id 14
    label "dziwny"
  ]
  node [
    id 15
    label "charakterystyczny"
  ]
  node [
    id 16
    label "wiadomy"
  ]
  node [
    id 17
    label "sempiterna"
  ]
  node [
    id 18
    label "ty&#322;"
  ]
  node [
    id 19
    label "dupa"
  ]
  node [
    id 20
    label "tu&#322;&#243;w"
  ]
  node [
    id 21
    label "kierunek"
  ]
  node [
    id 22
    label "przestrze&#324;"
  ]
  node [
    id 23
    label "zaty&#322;"
  ]
  node [
    id 24
    label "pupa"
  ]
  node [
    id 25
    label "cia&#322;o"
  ]
  node [
    id 26
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 27
    label "strona"
  ]
  node [
    id 28
    label "oferma"
  ]
  node [
    id 29
    label "srom"
  ]
  node [
    id 30
    label "kobieta"
  ]
  node [
    id 31
    label "sk&#243;ra"
  ]
  node [
    id 32
    label "dusza_wo&#322;owa"
  ]
  node [
    id 33
    label "kochanka"
  ]
  node [
    id 34
    label "krocze"
  ]
  node [
    id 35
    label "klatka_piersiowa"
  ]
  node [
    id 36
    label "biodro"
  ]
  node [
    id 37
    label "pier&#347;"
  ]
  node [
    id 38
    label "pachwina"
  ]
  node [
    id 39
    label "pacha"
  ]
  node [
    id 40
    label "brzuch"
  ]
  node [
    id 41
    label "struktura_anatomiczna"
  ]
  node [
    id 42
    label "bok"
  ]
  node [
    id 43
    label "body"
  ]
  node [
    id 44
    label "plecy"
  ]
  node [
    id 45
    label "stawon&#243;g"
  ]
  node [
    id 46
    label "dekolt"
  ]
  node [
    id 47
    label "zad"
  ]
  node [
    id 48
    label "organizowa&#263;"
  ]
  node [
    id 49
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 50
    label "czyni&#263;"
  ]
  node [
    id 51
    label "give"
  ]
  node [
    id 52
    label "stylizowa&#263;"
  ]
  node [
    id 53
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 54
    label "falowa&#263;"
  ]
  node [
    id 55
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 56
    label "peddle"
  ]
  node [
    id 57
    label "praca"
  ]
  node [
    id 58
    label "wydala&#263;"
  ]
  node [
    id 59
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 60
    label "tentegowa&#263;"
  ]
  node [
    id 61
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 62
    label "urz&#261;dza&#263;"
  ]
  node [
    id 63
    label "oszukiwa&#263;"
  ]
  node [
    id 64
    label "work"
  ]
  node [
    id 65
    label "ukazywa&#263;"
  ]
  node [
    id 66
    label "przerabia&#263;"
  ]
  node [
    id 67
    label "act"
  ]
  node [
    id 68
    label "post&#281;powa&#263;"
  ]
  node [
    id 69
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 70
    label "billow"
  ]
  node [
    id 71
    label "clutter"
  ]
  node [
    id 72
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 73
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 74
    label "beckon"
  ]
  node [
    id 75
    label "powiewa&#263;"
  ]
  node [
    id 76
    label "planowa&#263;"
  ]
  node [
    id 77
    label "dostosowywa&#263;"
  ]
  node [
    id 78
    label "treat"
  ]
  node [
    id 79
    label "pozyskiwa&#263;"
  ]
  node [
    id 80
    label "ensnare"
  ]
  node [
    id 81
    label "skupia&#263;"
  ]
  node [
    id 82
    label "create"
  ]
  node [
    id 83
    label "przygotowywa&#263;"
  ]
  node [
    id 84
    label "tworzy&#263;"
  ]
  node [
    id 85
    label "standard"
  ]
  node [
    id 86
    label "wprowadza&#263;"
  ]
  node [
    id 87
    label "kopiowa&#263;"
  ]
  node [
    id 88
    label "czerpa&#263;"
  ]
  node [
    id 89
    label "dally"
  ]
  node [
    id 90
    label "mock"
  ]
  node [
    id 91
    label "sprawia&#263;"
  ]
  node [
    id 92
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 93
    label "decydowa&#263;"
  ]
  node [
    id 94
    label "cast"
  ]
  node [
    id 95
    label "podbija&#263;"
  ]
  node [
    id 96
    label "przechodzi&#263;"
  ]
  node [
    id 97
    label "wytwarza&#263;"
  ]
  node [
    id 98
    label "amend"
  ]
  node [
    id 99
    label "zalicza&#263;"
  ]
  node [
    id 100
    label "overwork"
  ]
  node [
    id 101
    label "convert"
  ]
  node [
    id 102
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 103
    label "zamienia&#263;"
  ]
  node [
    id 104
    label "zmienia&#263;"
  ]
  node [
    id 105
    label "modyfikowa&#263;"
  ]
  node [
    id 106
    label "radzi&#263;_sobie"
  ]
  node [
    id 107
    label "pracowa&#263;"
  ]
  node [
    id 108
    label "przetwarza&#263;"
  ]
  node [
    id 109
    label "sp&#281;dza&#263;"
  ]
  node [
    id 110
    label "stylize"
  ]
  node [
    id 111
    label "upodabnia&#263;"
  ]
  node [
    id 112
    label "nadawa&#263;"
  ]
  node [
    id 113
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 114
    label "go"
  ]
  node [
    id 115
    label "przybiera&#263;"
  ]
  node [
    id 116
    label "i&#347;&#263;"
  ]
  node [
    id 117
    label "use"
  ]
  node [
    id 118
    label "blurt_out"
  ]
  node [
    id 119
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 120
    label "usuwa&#263;"
  ]
  node [
    id 121
    label "unwrap"
  ]
  node [
    id 122
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 123
    label "pokazywa&#263;"
  ]
  node [
    id 124
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 125
    label "orzyna&#263;"
  ]
  node [
    id 126
    label "oszwabia&#263;"
  ]
  node [
    id 127
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 128
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 129
    label "cheat"
  ]
  node [
    id 130
    label "dispose"
  ]
  node [
    id 131
    label "aran&#380;owa&#263;"
  ]
  node [
    id 132
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 133
    label "odpowiada&#263;"
  ]
  node [
    id 134
    label "zabezpiecza&#263;"
  ]
  node [
    id 135
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 136
    label "doprowadza&#263;"
  ]
  node [
    id 137
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 138
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 139
    label "najem"
  ]
  node [
    id 140
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 141
    label "zak&#322;ad"
  ]
  node [
    id 142
    label "stosunek_pracy"
  ]
  node [
    id 143
    label "benedykty&#324;ski"
  ]
  node [
    id 144
    label "poda&#380;_pracy"
  ]
  node [
    id 145
    label "pracowanie"
  ]
  node [
    id 146
    label "tyrka"
  ]
  node [
    id 147
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 148
    label "wytw&#243;r"
  ]
  node [
    id 149
    label "miejsce"
  ]
  node [
    id 150
    label "zaw&#243;d"
  ]
  node [
    id 151
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 152
    label "tynkarski"
  ]
  node [
    id 153
    label "czynno&#347;&#263;"
  ]
  node [
    id 154
    label "zmiana"
  ]
  node [
    id 155
    label "czynnik_produkcji"
  ]
  node [
    id 156
    label "zobowi&#261;zanie"
  ]
  node [
    id 157
    label "kierownictwo"
  ]
  node [
    id 158
    label "siedziba"
  ]
  node [
    id 159
    label "zmianowo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
]
