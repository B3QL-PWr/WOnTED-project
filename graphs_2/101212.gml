graph [
  node [
    id 0
    label "herb"
    origin "text"
  ]
  node [
    id 1
    label "gmin"
    origin "text"
  ]
  node [
    id 2
    label "jastk&#243;w"
    origin "text"
  ]
  node [
    id 3
    label "klejnot_herbowy"
  ]
  node [
    id 4
    label "barwy"
  ]
  node [
    id 5
    label "znak"
  ]
  node [
    id 6
    label "blazonowa&#263;"
  ]
  node [
    id 7
    label "symbol"
  ]
  node [
    id 8
    label "blazonowanie"
  ]
  node [
    id 9
    label "korona_rangowa"
  ]
  node [
    id 10
    label "heraldyka"
  ]
  node [
    id 11
    label "tarcza_herbowa"
  ]
  node [
    id 12
    label "trzymacz"
  ]
  node [
    id 13
    label "znak_pisarski"
  ]
  node [
    id 14
    label "notacja"
  ]
  node [
    id 15
    label "wcielenie"
  ]
  node [
    id 16
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 17
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 18
    label "character"
  ]
  node [
    id 19
    label "symbolizowanie"
  ]
  node [
    id 20
    label "baretka"
  ]
  node [
    id 21
    label "dow&#243;d"
  ]
  node [
    id 22
    label "oznakowanie"
  ]
  node [
    id 23
    label "fakt"
  ]
  node [
    id 24
    label "stawia&#263;"
  ]
  node [
    id 25
    label "wytw&#243;r"
  ]
  node [
    id 26
    label "point"
  ]
  node [
    id 27
    label "kodzik"
  ]
  node [
    id 28
    label "postawi&#263;"
  ]
  node [
    id 29
    label "mark"
  ]
  node [
    id 30
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 31
    label "attribute"
  ]
  node [
    id 32
    label "implikowa&#263;"
  ]
  node [
    id 33
    label "coat_of_arms"
  ]
  node [
    id 34
    label "opisywanie"
  ]
  node [
    id 35
    label "opisywa&#263;"
  ]
  node [
    id 36
    label "oksza"
  ]
  node [
    id 37
    label "pas"
  ]
  node [
    id 38
    label "s&#322;up"
  ]
  node [
    id 39
    label "historia"
  ]
  node [
    id 40
    label "barwa_heraldyczna"
  ]
  node [
    id 41
    label "or&#281;&#380;"
  ]
  node [
    id 42
    label "stan"
  ]
  node [
    id 43
    label "stan_trzeci"
  ]
  node [
    id 44
    label "gminno&#347;&#263;"
  ]
  node [
    id 45
    label "Ohio"
  ]
  node [
    id 46
    label "wci&#281;cie"
  ]
  node [
    id 47
    label "Nowy_York"
  ]
  node [
    id 48
    label "warstwa"
  ]
  node [
    id 49
    label "samopoczucie"
  ]
  node [
    id 50
    label "Illinois"
  ]
  node [
    id 51
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 52
    label "state"
  ]
  node [
    id 53
    label "Jukatan"
  ]
  node [
    id 54
    label "Kalifornia"
  ]
  node [
    id 55
    label "Wirginia"
  ]
  node [
    id 56
    label "wektor"
  ]
  node [
    id 57
    label "by&#263;"
  ]
  node [
    id 58
    label "Teksas"
  ]
  node [
    id 59
    label "Goa"
  ]
  node [
    id 60
    label "Waszyngton"
  ]
  node [
    id 61
    label "miejsce"
  ]
  node [
    id 62
    label "Massachusetts"
  ]
  node [
    id 63
    label "Alaska"
  ]
  node [
    id 64
    label "Arakan"
  ]
  node [
    id 65
    label "Hawaje"
  ]
  node [
    id 66
    label "Maryland"
  ]
  node [
    id 67
    label "punkt"
  ]
  node [
    id 68
    label "Michigan"
  ]
  node [
    id 69
    label "Arizona"
  ]
  node [
    id 70
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 71
    label "Georgia"
  ]
  node [
    id 72
    label "poziom"
  ]
  node [
    id 73
    label "Pensylwania"
  ]
  node [
    id 74
    label "shape"
  ]
  node [
    id 75
    label "Luizjana"
  ]
  node [
    id 76
    label "Nowy_Meksyk"
  ]
  node [
    id 77
    label "Alabama"
  ]
  node [
    id 78
    label "ilo&#347;&#263;"
  ]
  node [
    id 79
    label "Kansas"
  ]
  node [
    id 80
    label "Oregon"
  ]
  node [
    id 81
    label "Floryda"
  ]
  node [
    id 82
    label "Oklahoma"
  ]
  node [
    id 83
    label "jednostka_administracyjna"
  ]
  node [
    id 84
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 85
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 86
    label "chamstwo"
  ]
  node [
    id 87
    label "pochodzenie"
  ]
  node [
    id 88
    label "ii"
  ]
  node [
    id 89
    label "RP"
  ]
  node [
    id 90
    label "8"
  ]
  node [
    id 91
    label "pu&#322;k"
  ]
  node [
    id 92
    label "piechota"
  ]
  node [
    id 93
    label "legion"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 90
    target 93
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 92
    target 93
  ]
]
