graph [
  node [
    id 0
    label "pracbaza"
    origin "text"
  ]
  node [
    id 1
    label "patologiazewsi"
    origin "text"
  ]
  node [
    id 2
    label "zalesie"
    origin "text"
  ]
  node [
    id 3
    label "wasz"
    origin "text"
  ]
  node [
    id 4
    label "te&#380;"
    origin "text"
  ]
  node [
    id 5
    label "wkurwia&#263;"
    origin "text"
  ]
  node [
    id 6
    label "co&#347;"
    origin "text"
  ]
  node [
    id 7
    label "taki"
    origin "text"
  ]
  node [
    id 8
    label "praca"
    origin "text"
  ]
  node [
    id 9
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 10
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "teraz"
    origin "text"
  ]
  node [
    id 12
    label "czyj&#347;"
  ]
  node [
    id 13
    label "prywatny"
  ]
  node [
    id 14
    label "dzia&#322;a&#263;_na_nerwy"
  ]
  node [
    id 15
    label "thing"
  ]
  node [
    id 16
    label "cosik"
  ]
  node [
    id 17
    label "jaki&#347;"
  ]
  node [
    id 18
    label "okre&#347;lony"
  ]
  node [
    id 19
    label "jako&#347;"
  ]
  node [
    id 20
    label "niez&#322;y"
  ]
  node [
    id 21
    label "charakterystyczny"
  ]
  node [
    id 22
    label "jako_tako"
  ]
  node [
    id 23
    label "ciekawy"
  ]
  node [
    id 24
    label "dziwny"
  ]
  node [
    id 25
    label "przyzwoity"
  ]
  node [
    id 26
    label "wiadomy"
  ]
  node [
    id 27
    label "zaw&#243;d"
  ]
  node [
    id 28
    label "zmiana"
  ]
  node [
    id 29
    label "pracowanie"
  ]
  node [
    id 30
    label "pracowa&#263;"
  ]
  node [
    id 31
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 32
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 33
    label "czynnik_produkcji"
  ]
  node [
    id 34
    label "miejsce"
  ]
  node [
    id 35
    label "stosunek_pracy"
  ]
  node [
    id 36
    label "kierownictwo"
  ]
  node [
    id 37
    label "najem"
  ]
  node [
    id 38
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 39
    label "czynno&#347;&#263;"
  ]
  node [
    id 40
    label "siedziba"
  ]
  node [
    id 41
    label "zak&#322;ad"
  ]
  node [
    id 42
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 43
    label "tynkarski"
  ]
  node [
    id 44
    label "tyrka"
  ]
  node [
    id 45
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 46
    label "benedykty&#324;ski"
  ]
  node [
    id 47
    label "poda&#380;_pracy"
  ]
  node [
    id 48
    label "wytw&#243;r"
  ]
  node [
    id 49
    label "zobowi&#261;zanie"
  ]
  node [
    id 50
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 51
    label "przedmiot"
  ]
  node [
    id 52
    label "rezultat"
  ]
  node [
    id 53
    label "p&#322;&#243;d"
  ]
  node [
    id 54
    label "work"
  ]
  node [
    id 55
    label "bezproblemowy"
  ]
  node [
    id 56
    label "wydarzenie"
  ]
  node [
    id 57
    label "activity"
  ]
  node [
    id 58
    label "przestrze&#324;"
  ]
  node [
    id 59
    label "rz&#261;d"
  ]
  node [
    id 60
    label "uwaga"
  ]
  node [
    id 61
    label "cecha"
  ]
  node [
    id 62
    label "plac"
  ]
  node [
    id 63
    label "location"
  ]
  node [
    id 64
    label "warunek_lokalowy"
  ]
  node [
    id 65
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 66
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 67
    label "cia&#322;o"
  ]
  node [
    id 68
    label "status"
  ]
  node [
    id 69
    label "chwila"
  ]
  node [
    id 70
    label "stosunek_prawny"
  ]
  node [
    id 71
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 72
    label "zapewnienie"
  ]
  node [
    id 73
    label "uregulowa&#263;"
  ]
  node [
    id 74
    label "oblig"
  ]
  node [
    id 75
    label "oddzia&#322;anie"
  ]
  node [
    id 76
    label "obowi&#261;zek"
  ]
  node [
    id 77
    label "zapowied&#378;"
  ]
  node [
    id 78
    label "statement"
  ]
  node [
    id 79
    label "duty"
  ]
  node [
    id 80
    label "occupation"
  ]
  node [
    id 81
    label "miejsce_pracy"
  ]
  node [
    id 82
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 83
    label "budynek"
  ]
  node [
    id 84
    label "&#321;ubianka"
  ]
  node [
    id 85
    label "Bia&#322;y_Dom"
  ]
  node [
    id 86
    label "dzia&#322;_personalny"
  ]
  node [
    id 87
    label "Kreml"
  ]
  node [
    id 88
    label "sadowisko"
  ]
  node [
    id 89
    label "czyn"
  ]
  node [
    id 90
    label "wyko&#324;czenie"
  ]
  node [
    id 91
    label "umowa"
  ]
  node [
    id 92
    label "instytut"
  ]
  node [
    id 93
    label "jednostka_organizacyjna"
  ]
  node [
    id 94
    label "instytucja"
  ]
  node [
    id 95
    label "zak&#322;adka"
  ]
  node [
    id 96
    label "firma"
  ]
  node [
    id 97
    label "company"
  ]
  node [
    id 98
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 99
    label "wytrwa&#322;y"
  ]
  node [
    id 100
    label "cierpliwy"
  ]
  node [
    id 101
    label "benedykty&#324;sko"
  ]
  node [
    id 102
    label "typowy"
  ]
  node [
    id 103
    label "mozolny"
  ]
  node [
    id 104
    label "po_benedykty&#324;sku"
  ]
  node [
    id 105
    label "oznaka"
  ]
  node [
    id 106
    label "odmienianie"
  ]
  node [
    id 107
    label "zmianka"
  ]
  node [
    id 108
    label "amendment"
  ]
  node [
    id 109
    label "passage"
  ]
  node [
    id 110
    label "rewizja"
  ]
  node [
    id 111
    label "zjawisko"
  ]
  node [
    id 112
    label "komplet"
  ]
  node [
    id 113
    label "tura"
  ]
  node [
    id 114
    label "change"
  ]
  node [
    id 115
    label "ferment"
  ]
  node [
    id 116
    label "czas"
  ]
  node [
    id 117
    label "anatomopatolog"
  ]
  node [
    id 118
    label "nakr&#281;canie"
  ]
  node [
    id 119
    label "nakr&#281;cenie"
  ]
  node [
    id 120
    label "zarz&#261;dzanie"
  ]
  node [
    id 121
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 122
    label "skakanie"
  ]
  node [
    id 123
    label "d&#261;&#380;enie"
  ]
  node [
    id 124
    label "zatrzymanie"
  ]
  node [
    id 125
    label "postaranie_si&#281;"
  ]
  node [
    id 126
    label "dzianie_si&#281;"
  ]
  node [
    id 127
    label "przepracowanie"
  ]
  node [
    id 128
    label "przepracowanie_si&#281;"
  ]
  node [
    id 129
    label "podlizanie_si&#281;"
  ]
  node [
    id 130
    label "podlizywanie_si&#281;"
  ]
  node [
    id 131
    label "w&#322;&#261;czanie"
  ]
  node [
    id 132
    label "przepracowywanie"
  ]
  node [
    id 133
    label "w&#322;&#261;czenie"
  ]
  node [
    id 134
    label "awansowanie"
  ]
  node [
    id 135
    label "dzia&#322;anie"
  ]
  node [
    id 136
    label "uruchomienie"
  ]
  node [
    id 137
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 138
    label "odpocz&#281;cie"
  ]
  node [
    id 139
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 140
    label "impact"
  ]
  node [
    id 141
    label "podtrzymywanie"
  ]
  node [
    id 142
    label "tr&#243;jstronny"
  ]
  node [
    id 143
    label "courtship"
  ]
  node [
    id 144
    label "funkcja"
  ]
  node [
    id 145
    label "dopracowanie"
  ]
  node [
    id 146
    label "wyrabianie"
  ]
  node [
    id 147
    label "uruchamianie"
  ]
  node [
    id 148
    label "zapracowanie"
  ]
  node [
    id 149
    label "maszyna"
  ]
  node [
    id 150
    label "wyrobienie"
  ]
  node [
    id 151
    label "spracowanie_si&#281;"
  ]
  node [
    id 152
    label "poruszanie_si&#281;"
  ]
  node [
    id 153
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 154
    label "podejmowanie"
  ]
  node [
    id 155
    label "funkcjonowanie"
  ]
  node [
    id 156
    label "use"
  ]
  node [
    id 157
    label "zaprz&#281;ganie"
  ]
  node [
    id 158
    label "craft"
  ]
  node [
    id 159
    label "emocja"
  ]
  node [
    id 160
    label "zawodoznawstwo"
  ]
  node [
    id 161
    label "office"
  ]
  node [
    id 162
    label "kwalifikacje"
  ]
  node [
    id 163
    label "transakcja"
  ]
  node [
    id 164
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 165
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 166
    label "dzia&#322;a&#263;"
  ]
  node [
    id 167
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 168
    label "tryb"
  ]
  node [
    id 169
    label "endeavor"
  ]
  node [
    id 170
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 171
    label "funkcjonowa&#263;"
  ]
  node [
    id 172
    label "do"
  ]
  node [
    id 173
    label "dziama&#263;"
  ]
  node [
    id 174
    label "bangla&#263;"
  ]
  node [
    id 175
    label "mie&#263;_miejsce"
  ]
  node [
    id 176
    label "podejmowa&#263;"
  ]
  node [
    id 177
    label "lead"
  ]
  node [
    id 178
    label "w&#322;adza"
  ]
  node [
    id 179
    label "biuro"
  ]
  node [
    id 180
    label "zesp&#243;&#322;"
  ]
  node [
    id 181
    label "bankrupt"
  ]
  node [
    id 182
    label "open"
  ]
  node [
    id 183
    label "post&#281;powa&#263;"
  ]
  node [
    id 184
    label "odejmowa&#263;"
  ]
  node [
    id 185
    label "set_about"
  ]
  node [
    id 186
    label "begin"
  ]
  node [
    id 187
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 188
    label "take"
  ]
  node [
    id 189
    label "ujemny"
  ]
  node [
    id 190
    label "abstract"
  ]
  node [
    id 191
    label "liczy&#263;"
  ]
  node [
    id 192
    label "zabiera&#263;"
  ]
  node [
    id 193
    label "oddziela&#263;"
  ]
  node [
    id 194
    label "oddala&#263;"
  ]
  node [
    id 195
    label "reduce"
  ]
  node [
    id 196
    label "robi&#263;"
  ]
  node [
    id 197
    label "przybiera&#263;"
  ]
  node [
    id 198
    label "act"
  ]
  node [
    id 199
    label "i&#347;&#263;"
  ]
  node [
    id 200
    label "go"
  ]
  node [
    id 201
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 202
    label "przestawa&#263;"
  ]
  node [
    id 203
    label "stanowi&#263;"
  ]
  node [
    id 204
    label "zako&#324;cza&#263;"
  ]
  node [
    id 205
    label "satisfy"
  ]
  node [
    id 206
    label "determine"
  ]
  node [
    id 207
    label "close"
  ]
  node [
    id 208
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 209
    label "oszukiwa&#263;"
  ]
  node [
    id 210
    label "tentegowa&#263;"
  ]
  node [
    id 211
    label "urz&#261;dza&#263;"
  ]
  node [
    id 212
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 213
    label "czyni&#263;"
  ]
  node [
    id 214
    label "przerabia&#263;"
  ]
  node [
    id 215
    label "give"
  ]
  node [
    id 216
    label "peddle"
  ]
  node [
    id 217
    label "organizowa&#263;"
  ]
  node [
    id 218
    label "falowa&#263;"
  ]
  node [
    id 219
    label "stylizowa&#263;"
  ]
  node [
    id 220
    label "wydala&#263;"
  ]
  node [
    id 221
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 222
    label "ukazywa&#263;"
  ]
  node [
    id 223
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 224
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 225
    label "pies_my&#347;liwski"
  ]
  node [
    id 226
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 227
    label "decydowa&#263;"
  ]
  node [
    id 228
    label "decide"
  ]
  node [
    id 229
    label "by&#263;"
  ]
  node [
    id 230
    label "typify"
  ]
  node [
    id 231
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 232
    label "represent"
  ]
  node [
    id 233
    label "zatrzymywa&#263;"
  ]
  node [
    id 234
    label "nadawa&#263;"
  ]
  node [
    id 235
    label "finish_up"
  ]
  node [
    id 236
    label "dopracowywa&#263;"
  ]
  node [
    id 237
    label "rezygnowa&#263;"
  ]
  node [
    id 238
    label "elaborate"
  ]
  node [
    id 239
    label "&#380;y&#263;"
  ]
  node [
    id 240
    label "coating"
  ]
  node [
    id 241
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 242
    label "przebywa&#263;"
  ]
  node [
    id 243
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 244
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 245
    label "time"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 116
  ]
]
