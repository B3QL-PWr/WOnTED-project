graph [
  node [
    id 0
    label "gal"
    origin "text"
  ]
  node [
    id 1
    label "jednostka_przy&#347;pieszenia"
  ]
  node [
    id 2
    label "metal"
  ]
  node [
    id 3
    label "borowiec"
  ]
  node [
    id 4
    label "borowce"
  ]
  node [
    id 5
    label "ochroniarz"
  ]
  node [
    id 6
    label "duch"
  ]
  node [
    id 7
    label "borowik"
  ]
  node [
    id 8
    label "nietoperz"
  ]
  node [
    id 9
    label "kozio&#322;ek"
  ]
  node [
    id 10
    label "owado&#380;erca"
  ]
  node [
    id 11
    label "funkcjonariusz"
  ]
  node [
    id 12
    label "BOR"
  ]
  node [
    id 13
    label "mroczkowate"
  ]
  node [
    id 14
    label "ochrona"
  ]
  node [
    id 15
    label "pierwiastek"
  ]
  node [
    id 16
    label "pi&#243;ra"
  ]
  node [
    id 17
    label "odlewalnia"
  ]
  node [
    id 18
    label "naszywka"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "pieszczocha"
  ]
  node [
    id 21
    label "sk&#243;ra"
  ]
  node [
    id 22
    label "pogo"
  ]
  node [
    id 23
    label "fan"
  ]
  node [
    id 24
    label "przedstawiciel"
  ]
  node [
    id 25
    label "przebijarka"
  ]
  node [
    id 26
    label "wytrawialnia"
  ]
  node [
    id 27
    label "orygina&#322;"
  ]
  node [
    id 28
    label "pogowa&#263;"
  ]
  node [
    id 29
    label "metallic_element"
  ]
  node [
    id 30
    label "kuc"
  ]
  node [
    id 31
    label "ku&#263;"
  ]
  node [
    id 32
    label "rock"
  ]
  node [
    id 33
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 34
    label "kucie"
  ]
  node [
    id 35
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 36
    label "wytrawia&#263;"
  ]
  node [
    id 37
    label "topialnia"
  ]
  node [
    id 38
    label "34D"
  ]
  node [
    id 39
    label "ga&#322;a"
  ]
  node [
    id 40
    label "Walter"
  ]
  node [
    id 41
    label "Frederick"
  ]
  node [
    id 42
    label "dzienny"
  ]
  node [
    id 43
    label "gwiazda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
]
