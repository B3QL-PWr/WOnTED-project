graph [
  node [
    id 0
    label "lmde"
    origin "text"
  ]
  node [
    id 1
    label "kry&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "pod"
    origin "text"
  ]
  node [
    id 4
    label "system"
    origin "text"
  ]
  node [
    id 5
    label "debian"
    origin "text"
  ]
  node [
    id 6
    label "testing"
    origin "text"
  ]
  node [
    id 7
    label "kilka"
    origin "text"
  ]
  node [
    id 8
    label "program"
    origin "text"
  ]
  node [
    id 9
    label "gui"
    origin "text"
  ]
  node [
    id 10
    label "maska"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "czysty"
    origin "text"
  ]
  node [
    id 13
    label "ubuntu"
    origin "text"
  ]
  node [
    id 14
    label "pokrywa&#263;"
  ]
  node [
    id 15
    label "r&#243;wna&#263;"
  ]
  node [
    id 16
    label "przeciwdzia&#322;a&#263;"
  ]
  node [
    id 17
    label "zawiera&#263;"
  ]
  node [
    id 18
    label "rozwija&#263;"
  ]
  node [
    id 19
    label "umieszcza&#263;"
  ]
  node [
    id 20
    label "cover"
  ]
  node [
    id 21
    label "zas&#322;ania&#263;"
  ]
  node [
    id 22
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 24
    label "zataja&#263;"
  ]
  node [
    id 25
    label "pilnowa&#263;"
  ]
  node [
    id 26
    label "hide"
  ]
  node [
    id 27
    label "meliniarz"
  ]
  node [
    id 28
    label "cache"
  ]
  node [
    id 29
    label "os&#322;ania&#263;"
  ]
  node [
    id 30
    label "robi&#263;"
  ]
  node [
    id 31
    label "ukrywa&#263;"
  ]
  node [
    id 32
    label "report"
  ]
  node [
    id 33
    label "farba"
  ]
  node [
    id 34
    label "chowany"
  ]
  node [
    id 35
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 36
    label "zachowywa&#263;"
  ]
  node [
    id 37
    label "continue"
  ]
  node [
    id 38
    label "zapobiega&#263;"
  ]
  node [
    id 39
    label "react"
  ]
  node [
    id 40
    label "nape&#322;nia&#263;"
  ]
  node [
    id 41
    label "inspirowa&#263;"
  ]
  node [
    id 42
    label "create"
  ]
  node [
    id 43
    label "fold"
  ]
  node [
    id 44
    label "zamyka&#263;"
  ]
  node [
    id 45
    label "obejmowa&#263;"
  ]
  node [
    id 46
    label "ustala&#263;"
  ]
  node [
    id 47
    label "mie&#263;"
  ]
  node [
    id 48
    label "make"
  ]
  node [
    id 49
    label "lock"
  ]
  node [
    id 50
    label "poznawa&#263;"
  ]
  node [
    id 51
    label "zmienia&#263;"
  ]
  node [
    id 52
    label "okre&#347;la&#263;"
  ]
  node [
    id 53
    label "plasowa&#263;"
  ]
  node [
    id 54
    label "wpiernicza&#263;"
  ]
  node [
    id 55
    label "powodowa&#263;"
  ]
  node [
    id 56
    label "pomieszcza&#263;"
  ]
  node [
    id 57
    label "umie&#347;ci&#263;"
  ]
  node [
    id 58
    label "accommodate"
  ]
  node [
    id 59
    label "venture"
  ]
  node [
    id 60
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 61
    label "smother"
  ]
  node [
    id 62
    label "przykrywa&#263;"
  ]
  node [
    id 63
    label "defray"
  ]
  node [
    id 64
    label "supernatural"
  ]
  node [
    id 65
    label "p&#322;aci&#263;"
  ]
  node [
    id 66
    label "zaspokaja&#263;"
  ]
  node [
    id 67
    label "maskowa&#263;"
  ]
  node [
    id 68
    label "chroni&#263;"
  ]
  node [
    id 69
    label "fight"
  ]
  node [
    id 70
    label "broni&#263;"
  ]
  node [
    id 71
    label "champion"
  ]
  node [
    id 72
    label "odgradza&#263;"
  ]
  node [
    id 73
    label "puszcza&#263;"
  ]
  node [
    id 74
    label "rozstawia&#263;"
  ]
  node [
    id 75
    label "dopowiada&#263;"
  ]
  node [
    id 76
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 77
    label "inflate"
  ]
  node [
    id 78
    label "omawia&#263;"
  ]
  node [
    id 79
    label "rozpakowywa&#263;"
  ]
  node [
    id 80
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 81
    label "stawia&#263;"
  ]
  node [
    id 82
    label "train"
  ]
  node [
    id 83
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 84
    label "level"
  ]
  node [
    id 85
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 86
    label "suppress"
  ]
  node [
    id 87
    label "oszukiwa&#263;"
  ]
  node [
    id 88
    label "tentegowa&#263;"
  ]
  node [
    id 89
    label "urz&#261;dza&#263;"
  ]
  node [
    id 90
    label "praca"
  ]
  node [
    id 91
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 92
    label "czyni&#263;"
  ]
  node [
    id 93
    label "work"
  ]
  node [
    id 94
    label "przerabia&#263;"
  ]
  node [
    id 95
    label "act"
  ]
  node [
    id 96
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 97
    label "give"
  ]
  node [
    id 98
    label "post&#281;powa&#263;"
  ]
  node [
    id 99
    label "peddle"
  ]
  node [
    id 100
    label "organizowa&#263;"
  ]
  node [
    id 101
    label "falowa&#263;"
  ]
  node [
    id 102
    label "stylizowa&#263;"
  ]
  node [
    id 103
    label "wydala&#263;"
  ]
  node [
    id 104
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 105
    label "ukazywa&#263;"
  ]
  node [
    id 106
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 107
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 108
    label "umowa"
  ]
  node [
    id 109
    label "aran&#380;acja"
  ]
  node [
    id 110
    label "kawa&#322;ek"
  ]
  node [
    id 111
    label "pami&#281;&#263;"
  ]
  node [
    id 112
    label "wypunktowa&#263;"
  ]
  node [
    id 113
    label "krycie"
  ]
  node [
    id 114
    label "podk&#322;ad"
  ]
  node [
    id 115
    label "zwierz&#281;"
  ]
  node [
    id 116
    label "pr&#243;szy&#263;"
  ]
  node [
    id 117
    label "pr&#243;szenie"
  ]
  node [
    id 118
    label "kolor"
  ]
  node [
    id 119
    label "krew"
  ]
  node [
    id 120
    label "blik"
  ]
  node [
    id 121
    label "punktowa&#263;"
  ]
  node [
    id 122
    label "substancja"
  ]
  node [
    id 123
    label "bimbrownik"
  ]
  node [
    id 124
    label "przest&#281;pca"
  ]
  node [
    id 125
    label "p&#281;dzi&#263;"
  ]
  node [
    id 126
    label "zabawa"
  ]
  node [
    id 127
    label "poj&#281;cie"
  ]
  node [
    id 128
    label "model"
  ]
  node [
    id 129
    label "systemik"
  ]
  node [
    id 130
    label "Android"
  ]
  node [
    id 131
    label "podsystem"
  ]
  node [
    id 132
    label "systemat"
  ]
  node [
    id 133
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 134
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 135
    label "p&#322;&#243;d"
  ]
  node [
    id 136
    label "konstelacja"
  ]
  node [
    id 137
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 138
    label "oprogramowanie"
  ]
  node [
    id 139
    label "j&#261;dro"
  ]
  node [
    id 140
    label "zbi&#243;r"
  ]
  node [
    id 141
    label "rozprz&#261;c"
  ]
  node [
    id 142
    label "usenet"
  ]
  node [
    id 143
    label "jednostka_geologiczna"
  ]
  node [
    id 144
    label "ryba"
  ]
  node [
    id 145
    label "oddzia&#322;"
  ]
  node [
    id 146
    label "net"
  ]
  node [
    id 147
    label "podstawa"
  ]
  node [
    id 148
    label "metoda"
  ]
  node [
    id 149
    label "method"
  ]
  node [
    id 150
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 151
    label "porz&#261;dek"
  ]
  node [
    id 152
    label "struktura"
  ]
  node [
    id 153
    label "spos&#243;b"
  ]
  node [
    id 154
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 155
    label "w&#281;dkarstwo"
  ]
  node [
    id 156
    label "doktryna"
  ]
  node [
    id 157
    label "Leopard"
  ]
  node [
    id 158
    label "zachowanie"
  ]
  node [
    id 159
    label "o&#347;"
  ]
  node [
    id 160
    label "sk&#322;ad"
  ]
  node [
    id 161
    label "pulpit"
  ]
  node [
    id 162
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 163
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 164
    label "cybernetyk"
  ]
  node [
    id 165
    label "przyn&#281;ta"
  ]
  node [
    id 166
    label "s&#261;d"
  ]
  node [
    id 167
    label "eratem"
  ]
  node [
    id 168
    label "za&#322;o&#380;enie"
  ]
  node [
    id 169
    label "strategia"
  ]
  node [
    id 170
    label "background"
  ]
  node [
    id 171
    label "przedmiot"
  ]
  node [
    id 172
    label "punkt_odniesienia"
  ]
  node [
    id 173
    label "zasadzenie"
  ]
  node [
    id 174
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 175
    label "&#347;ciana"
  ]
  node [
    id 176
    label "podstawowy"
  ]
  node [
    id 177
    label "dzieci&#281;ctwo"
  ]
  node [
    id 178
    label "d&#243;&#322;"
  ]
  node [
    id 179
    label "documentation"
  ]
  node [
    id 180
    label "bok"
  ]
  node [
    id 181
    label "pomys&#322;"
  ]
  node [
    id 182
    label "zasadzi&#263;"
  ]
  node [
    id 183
    label "column"
  ]
  node [
    id 184
    label "pot&#281;ga"
  ]
  node [
    id 185
    label "narz&#281;dzie"
  ]
  node [
    id 186
    label "nature"
  ]
  node [
    id 187
    label "tryb"
  ]
  node [
    id 188
    label "stan"
  ]
  node [
    id 189
    label "uk&#322;ad"
  ]
  node [
    id 190
    label "normalizacja"
  ]
  node [
    id 191
    label "cecha"
  ]
  node [
    id 192
    label "styl_architektoniczny"
  ]
  node [
    id 193
    label "relacja"
  ]
  node [
    id 194
    label "zasada"
  ]
  node [
    id 195
    label "pakiet_klimatyczny"
  ]
  node [
    id 196
    label "uprawianie"
  ]
  node [
    id 197
    label "collection"
  ]
  node [
    id 198
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 199
    label "gathering"
  ]
  node [
    id 200
    label "album"
  ]
  node [
    id 201
    label "praca_rolnicza"
  ]
  node [
    id 202
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 203
    label "sum"
  ]
  node [
    id 204
    label "egzemplarz"
  ]
  node [
    id 205
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 206
    label "series"
  ]
  node [
    id 207
    label "dane"
  ]
  node [
    id 208
    label "orientacja"
  ]
  node [
    id 209
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 210
    label "skumanie"
  ]
  node [
    id 211
    label "pos&#322;uchanie"
  ]
  node [
    id 212
    label "wytw&#243;r"
  ]
  node [
    id 213
    label "teoria"
  ]
  node [
    id 214
    label "forma"
  ]
  node [
    id 215
    label "zorientowanie"
  ]
  node [
    id 216
    label "clasp"
  ]
  node [
    id 217
    label "przem&#243;wienie"
  ]
  node [
    id 218
    label "konstrukcja"
  ]
  node [
    id 219
    label "mechanika"
  ]
  node [
    id 220
    label "system_komputerowy"
  ]
  node [
    id 221
    label "sprz&#281;t"
  ]
  node [
    id 222
    label "embryo"
  ]
  node [
    id 223
    label "moczownik"
  ]
  node [
    id 224
    label "latawiec"
  ]
  node [
    id 225
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 226
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 227
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 228
    label "zarodek"
  ]
  node [
    id 229
    label "reengineering"
  ]
  node [
    id 230
    label "liczba"
  ]
  node [
    id 231
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 232
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 233
    label "integer"
  ]
  node [
    id 234
    label "zlewanie_si&#281;"
  ]
  node [
    id 235
    label "ilo&#347;&#263;"
  ]
  node [
    id 236
    label "pe&#322;ny"
  ]
  node [
    id 237
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 238
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 239
    label "grupa_dyskusyjna"
  ]
  node [
    id 240
    label "doctrine"
  ]
  node [
    id 241
    label "tar&#322;o"
  ]
  node [
    id 242
    label "rakowato&#347;&#263;"
  ]
  node [
    id 243
    label "szczelina_skrzelowa"
  ]
  node [
    id 244
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 245
    label "doniczkowiec"
  ]
  node [
    id 246
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 247
    label "cz&#322;owiek"
  ]
  node [
    id 248
    label "mi&#281;so"
  ]
  node [
    id 249
    label "fish"
  ]
  node [
    id 250
    label "patroszy&#263;"
  ]
  node [
    id 251
    label "linia_boczna"
  ]
  node [
    id 252
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 253
    label "pokrywa_skrzelowa"
  ]
  node [
    id 254
    label "kr&#281;gowiec"
  ]
  node [
    id 255
    label "ryby"
  ]
  node [
    id 256
    label "m&#281;tnooki"
  ]
  node [
    id 257
    label "ikra"
  ]
  node [
    id 258
    label "wyrostek_filtracyjny"
  ]
  node [
    id 259
    label "sport"
  ]
  node [
    id 260
    label "urozmaicenie"
  ]
  node [
    id 261
    label "pu&#322;apka"
  ]
  node [
    id 262
    label "wabik"
  ]
  node [
    id 263
    label "pon&#281;ta"
  ]
  node [
    id 264
    label "blat"
  ]
  node [
    id 265
    label "obszar"
  ]
  node [
    id 266
    label "okno"
  ]
  node [
    id 267
    label "mebel"
  ]
  node [
    id 268
    label "system_operacyjny"
  ]
  node [
    id 269
    label "interfejs"
  ]
  node [
    id 270
    label "ikona"
  ]
  node [
    id 271
    label "zdolno&#347;&#263;"
  ]
  node [
    id 272
    label "oswobodzi&#263;"
  ]
  node [
    id 273
    label "disengage"
  ]
  node [
    id 274
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 275
    label "zdezorganizowa&#263;"
  ]
  node [
    id 276
    label "os&#322;abi&#263;"
  ]
  node [
    id 277
    label "post"
  ]
  node [
    id 278
    label "etolog"
  ]
  node [
    id 279
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 280
    label "dieta"
  ]
  node [
    id 281
    label "zdyscyplinowanie"
  ]
  node [
    id 282
    label "bearing"
  ]
  node [
    id 283
    label "wydarzenie"
  ]
  node [
    id 284
    label "observation"
  ]
  node [
    id 285
    label "behawior"
  ]
  node [
    id 286
    label "reakcja"
  ]
  node [
    id 287
    label "zrobienie"
  ]
  node [
    id 288
    label "tajemnica"
  ]
  node [
    id 289
    label "przechowanie"
  ]
  node [
    id 290
    label "pochowanie"
  ]
  node [
    id 291
    label "podtrzymanie"
  ]
  node [
    id 292
    label "post&#261;pienie"
  ]
  node [
    id 293
    label "oswobodzenie"
  ]
  node [
    id 294
    label "zdezorganizowanie"
  ]
  node [
    id 295
    label "relaxation"
  ]
  node [
    id 296
    label "os&#322;abienie"
  ]
  node [
    id 297
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 298
    label "naukowiec"
  ]
  node [
    id 299
    label "provider"
  ]
  node [
    id 300
    label "b&#322;&#261;d"
  ]
  node [
    id 301
    label "podcast"
  ]
  node [
    id 302
    label "mem"
  ]
  node [
    id 303
    label "cyberprzestrze&#324;"
  ]
  node [
    id 304
    label "punkt_dost&#281;pu"
  ]
  node [
    id 305
    label "sie&#263;_komputerowa"
  ]
  node [
    id 306
    label "biznes_elektroniczny"
  ]
  node [
    id 307
    label "media"
  ]
  node [
    id 308
    label "gra_sieciowa"
  ]
  node [
    id 309
    label "hipertekst"
  ]
  node [
    id 310
    label "netbook"
  ]
  node [
    id 311
    label "strona"
  ]
  node [
    id 312
    label "e-hazard"
  ]
  node [
    id 313
    label "us&#322;uga_internetowa"
  ]
  node [
    id 314
    label "grooming"
  ]
  node [
    id 315
    label "matryca"
  ]
  node [
    id 316
    label "facet"
  ]
  node [
    id 317
    label "zi&#243;&#322;ko"
  ]
  node [
    id 318
    label "mildew"
  ]
  node [
    id 319
    label "miniatura"
  ]
  node [
    id 320
    label "ideal"
  ]
  node [
    id 321
    label "adaptation"
  ]
  node [
    id 322
    label "typ"
  ]
  node [
    id 323
    label "ruch"
  ]
  node [
    id 324
    label "imitacja"
  ]
  node [
    id 325
    label "pozowa&#263;"
  ]
  node [
    id 326
    label "orygina&#322;"
  ]
  node [
    id 327
    label "wz&#243;r"
  ]
  node [
    id 328
    label "motif"
  ]
  node [
    id 329
    label "prezenter"
  ]
  node [
    id 330
    label "pozowanie"
  ]
  node [
    id 331
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 332
    label "forum"
  ]
  node [
    id 333
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 334
    label "s&#261;downictwo"
  ]
  node [
    id 335
    label "podejrzany"
  ]
  node [
    id 336
    label "&#347;wiadek"
  ]
  node [
    id 337
    label "instytucja"
  ]
  node [
    id 338
    label "biuro"
  ]
  node [
    id 339
    label "post&#281;powanie"
  ]
  node [
    id 340
    label "court"
  ]
  node [
    id 341
    label "my&#347;l"
  ]
  node [
    id 342
    label "obrona"
  ]
  node [
    id 343
    label "antylogizm"
  ]
  node [
    id 344
    label "oskar&#380;yciel"
  ]
  node [
    id 345
    label "urz&#261;d"
  ]
  node [
    id 346
    label "skazany"
  ]
  node [
    id 347
    label "konektyw"
  ]
  node [
    id 348
    label "wypowied&#378;"
  ]
  node [
    id 349
    label "bronienie"
  ]
  node [
    id 350
    label "pods&#261;dny"
  ]
  node [
    id 351
    label "zesp&#243;&#322;"
  ]
  node [
    id 352
    label "procesowicz"
  ]
  node [
    id 353
    label "dzia&#322;"
  ]
  node [
    id 354
    label "filia"
  ]
  node [
    id 355
    label "bank"
  ]
  node [
    id 356
    label "formacja"
  ]
  node [
    id 357
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 358
    label "klasa"
  ]
  node [
    id 359
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 360
    label "agencja"
  ]
  node [
    id 361
    label "whole"
  ]
  node [
    id 362
    label "malm"
  ]
  node [
    id 363
    label "promocja"
  ]
  node [
    id 364
    label "szpital"
  ]
  node [
    id 365
    label "siedziba"
  ]
  node [
    id 366
    label "dogger"
  ]
  node [
    id 367
    label "ajencja"
  ]
  node [
    id 368
    label "poziom"
  ]
  node [
    id 369
    label "jednostka"
  ]
  node [
    id 370
    label "lias"
  ]
  node [
    id 371
    label "wojsko"
  ]
  node [
    id 372
    label "kurs"
  ]
  node [
    id 373
    label "pi&#281;tro"
  ]
  node [
    id 374
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 375
    label "algebra_liniowa"
  ]
  node [
    id 376
    label "chromosom"
  ]
  node [
    id 377
    label "&#347;rodek"
  ]
  node [
    id 378
    label "nasieniak"
  ]
  node [
    id 379
    label "nukleon"
  ]
  node [
    id 380
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 381
    label "ziarno"
  ]
  node [
    id 382
    label "znaczenie"
  ]
  node [
    id 383
    label "j&#261;derko"
  ]
  node [
    id 384
    label "macierz_j&#261;drowa"
  ]
  node [
    id 385
    label "anorchizm"
  ]
  node [
    id 386
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 387
    label "wn&#281;trostwo"
  ]
  node [
    id 388
    label "kariokineza"
  ]
  node [
    id 389
    label "nukleosynteza"
  ]
  node [
    id 390
    label "organellum"
  ]
  node [
    id 391
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 392
    label "chemia_j&#261;drowa"
  ]
  node [
    id 393
    label "atom"
  ]
  node [
    id 394
    label "przeciwobraz"
  ]
  node [
    id 395
    label "jajo"
  ]
  node [
    id 396
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 397
    label "core"
  ]
  node [
    id 398
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 399
    label "protoplazma"
  ]
  node [
    id 400
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 401
    label "moszna"
  ]
  node [
    id 402
    label "subsystem"
  ]
  node [
    id 403
    label "suport"
  ]
  node [
    id 404
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 405
    label "granica"
  ]
  node [
    id 406
    label "o&#347;rodek"
  ]
  node [
    id 407
    label "prosta"
  ]
  node [
    id 408
    label "ko&#322;o"
  ]
  node [
    id 409
    label "eonotem"
  ]
  node [
    id 410
    label "constellation"
  ]
  node [
    id 411
    label "W&#261;&#380;"
  ]
  node [
    id 412
    label "Panna"
  ]
  node [
    id 413
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 414
    label "W&#281;&#380;ownik"
  ]
  node [
    id 415
    label "Ptak_Rajski"
  ]
  node [
    id 416
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 417
    label "fabryka"
  ]
  node [
    id 418
    label "pole"
  ]
  node [
    id 419
    label "tekst"
  ]
  node [
    id 420
    label "pas"
  ]
  node [
    id 421
    label "blokada"
  ]
  node [
    id 422
    label "miejsce"
  ]
  node [
    id 423
    label "tabulacja"
  ]
  node [
    id 424
    label "hurtownia"
  ]
  node [
    id 425
    label "basic"
  ]
  node [
    id 426
    label "obr&#243;bka"
  ]
  node [
    id 427
    label "rank_and_file"
  ]
  node [
    id 428
    label "pomieszczenie"
  ]
  node [
    id 429
    label "syf"
  ]
  node [
    id 430
    label "sk&#322;adnik"
  ]
  node [
    id 431
    label "constitution"
  ]
  node [
    id 432
    label "sklep"
  ]
  node [
    id 433
    label "set"
  ]
  node [
    id 434
    label "&#347;wiat&#322;o"
  ]
  node [
    id 435
    label "&#347;ledziowate"
  ]
  node [
    id 436
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 437
    label "odinstalowa&#263;"
  ]
  node [
    id 438
    label "spis"
  ]
  node [
    id 439
    label "broszura"
  ]
  node [
    id 440
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 441
    label "informatyka"
  ]
  node [
    id 442
    label "odinstalowywa&#263;"
  ]
  node [
    id 443
    label "furkacja"
  ]
  node [
    id 444
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 445
    label "ogranicznik_referencyjny"
  ]
  node [
    id 446
    label "blok"
  ]
  node [
    id 447
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 448
    label "prezentowa&#263;"
  ]
  node [
    id 449
    label "emitowa&#263;"
  ]
  node [
    id 450
    label "kana&#322;"
  ]
  node [
    id 451
    label "sekcja_krytyczna"
  ]
  node [
    id 452
    label "pirat"
  ]
  node [
    id 453
    label "folder"
  ]
  node [
    id 454
    label "zaprezentowa&#263;"
  ]
  node [
    id 455
    label "course_of_study"
  ]
  node [
    id 456
    label "punkt"
  ]
  node [
    id 457
    label "zainstalowa&#263;"
  ]
  node [
    id 458
    label "emitowanie"
  ]
  node [
    id 459
    label "teleferie"
  ]
  node [
    id 460
    label "deklaracja"
  ]
  node [
    id 461
    label "instrukcja"
  ]
  node [
    id 462
    label "zainstalowanie"
  ]
  node [
    id 463
    label "zaprezentowanie"
  ]
  node [
    id 464
    label "instalowa&#263;"
  ]
  node [
    id 465
    label "oferta"
  ]
  node [
    id 466
    label "odinstalowanie"
  ]
  node [
    id 467
    label "odinstalowywanie"
  ]
  node [
    id 468
    label "ram&#243;wka"
  ]
  node [
    id 469
    label "menu"
  ]
  node [
    id 470
    label "podprogram"
  ]
  node [
    id 471
    label "instalowanie"
  ]
  node [
    id 472
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 473
    label "booklet"
  ]
  node [
    id 474
    label "struktura_organizacyjna"
  ]
  node [
    id 475
    label "prezentowanie"
  ]
  node [
    id 476
    label "wydawnictwo"
  ]
  node [
    id 477
    label "druk_ulotny"
  ]
  node [
    id 478
    label "zasi&#261;g"
  ]
  node [
    id 479
    label "distribution"
  ]
  node [
    id 480
    label "zakres"
  ]
  node [
    id 481
    label "rozmiar"
  ]
  node [
    id 482
    label "bridge"
  ]
  node [
    id 483
    label "izochronizm"
  ]
  node [
    id 484
    label "rezultat"
  ]
  node [
    id 485
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 486
    label "modalno&#347;&#263;"
  ]
  node [
    id 487
    label "z&#261;b"
  ]
  node [
    id 488
    label "koniugacja"
  ]
  node [
    id 489
    label "kategoria_gramatyczna"
  ]
  node [
    id 490
    label "funkcjonowa&#263;"
  ]
  node [
    id 491
    label "skala"
  ]
  node [
    id 492
    label "propozycja"
  ]
  node [
    id 493
    label "offer"
  ]
  node [
    id 494
    label "formularz"
  ]
  node [
    id 495
    label "announcement"
  ]
  node [
    id 496
    label "o&#347;wiadczenie"
  ]
  node [
    id 497
    label "akt"
  ]
  node [
    id 498
    label "digest"
  ]
  node [
    id 499
    label "obietnica"
  ]
  node [
    id 500
    label "dokument"
  ]
  node [
    id 501
    label "o&#347;wiadczyny"
  ]
  node [
    id 502
    label "statement"
  ]
  node [
    id 503
    label "struktura_anatomiczna"
  ]
  node [
    id 504
    label "gara&#380;"
  ]
  node [
    id 505
    label "syfon"
  ]
  node [
    id 506
    label "przew&#243;d"
  ]
  node [
    id 507
    label "chody"
  ]
  node [
    id 508
    label "urz&#261;dzenie"
  ]
  node [
    id 509
    label "ciek"
  ]
  node [
    id 510
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 511
    label "grodzisko"
  ]
  node [
    id 512
    label "szaniec"
  ]
  node [
    id 513
    label "warsztat"
  ]
  node [
    id 514
    label "zrzutowy"
  ]
  node [
    id 515
    label "kanalizacja"
  ]
  node [
    id 516
    label "budowa"
  ]
  node [
    id 517
    label "teatr"
  ]
  node [
    id 518
    label "klarownia"
  ]
  node [
    id 519
    label "pit"
  ]
  node [
    id 520
    label "piaskownik"
  ]
  node [
    id 521
    label "bystrza"
  ]
  node [
    id 522
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 523
    label "topologia_magistrali"
  ]
  node [
    id 524
    label "tarapaty"
  ]
  node [
    id 525
    label "odwa&#322;"
  ]
  node [
    id 526
    label "odk&#322;ad"
  ]
  node [
    id 527
    label "catalog"
  ]
  node [
    id 528
    label "figurowa&#263;"
  ]
  node [
    id 529
    label "pozycja"
  ]
  node [
    id 530
    label "wyliczanka"
  ]
  node [
    id 531
    label "sumariusz"
  ]
  node [
    id 532
    label "stock"
  ]
  node [
    id 533
    label "book"
  ]
  node [
    id 534
    label "czynno&#347;&#263;"
  ]
  node [
    id 535
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 536
    label "usuwanie"
  ]
  node [
    id 537
    label "usuni&#281;cie"
  ]
  node [
    id 538
    label "zamek"
  ]
  node [
    id 539
    label "infa"
  ]
  node [
    id 540
    label "gramatyka_formalna"
  ]
  node [
    id 541
    label "baza_danych"
  ]
  node [
    id 542
    label "HP"
  ]
  node [
    id 543
    label "kryptologia"
  ]
  node [
    id 544
    label "przetwarzanie_informacji"
  ]
  node [
    id 545
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 546
    label "dost&#281;p"
  ]
  node [
    id 547
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 548
    label "dziedzina_informatyki"
  ]
  node [
    id 549
    label "kierunek"
  ]
  node [
    id 550
    label "sztuczna_inteligencja"
  ]
  node [
    id 551
    label "artefakt"
  ]
  node [
    id 552
    label "komputer"
  ]
  node [
    id 553
    label "zrobi&#263;"
  ]
  node [
    id 554
    label "install"
  ]
  node [
    id 555
    label "dostosowa&#263;"
  ]
  node [
    id 556
    label "dostosowywa&#263;"
  ]
  node [
    id 557
    label "supply"
  ]
  node [
    id 558
    label "fit"
  ]
  node [
    id 559
    label "usuwa&#263;"
  ]
  node [
    id 560
    label "usun&#261;&#263;"
  ]
  node [
    id 561
    label "layout"
  ]
  node [
    id 562
    label "installation"
  ]
  node [
    id 563
    label "proposition"
  ]
  node [
    id 564
    label "pozak&#322;adanie"
  ]
  node [
    id 565
    label "dostosowanie"
  ]
  node [
    id 566
    label "umieszczenie"
  ]
  node [
    id 567
    label "parapet"
  ]
  node [
    id 568
    label "okiennica"
  ]
  node [
    id 569
    label "lufcik"
  ]
  node [
    id 570
    label "futryna"
  ]
  node [
    id 571
    label "prze&#347;wit"
  ]
  node [
    id 572
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 573
    label "inspekt"
  ]
  node [
    id 574
    label "szyba"
  ]
  node [
    id 575
    label "nora"
  ]
  node [
    id 576
    label "nadokiennik"
  ]
  node [
    id 577
    label "skrzyd&#322;o"
  ]
  node [
    id 578
    label "transenna"
  ]
  node [
    id 579
    label "kwatera_okienna"
  ]
  node [
    id 580
    label "otw&#243;r"
  ]
  node [
    id 581
    label "menad&#380;er_okien"
  ]
  node [
    id 582
    label "casement"
  ]
  node [
    id 583
    label "wmontowanie"
  ]
  node [
    id 584
    label "wmontowywanie"
  ]
  node [
    id 585
    label "dostosowywanie"
  ]
  node [
    id 586
    label "umieszczanie"
  ]
  node [
    id 587
    label "robienie"
  ]
  node [
    id 588
    label "fitting"
  ]
  node [
    id 589
    label "uprzedzi&#263;"
  ]
  node [
    id 590
    label "testify"
  ]
  node [
    id 591
    label "pokaza&#263;"
  ]
  node [
    id 592
    label "zapozna&#263;"
  ]
  node [
    id 593
    label "attest"
  ]
  node [
    id 594
    label "typify"
  ]
  node [
    id 595
    label "represent"
  ]
  node [
    id 596
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 597
    label "przedstawi&#263;"
  ]
  node [
    id 598
    label "present"
  ]
  node [
    id 599
    label "wyra&#380;anie"
  ]
  node [
    id 600
    label "presentation"
  ]
  node [
    id 601
    label "granie"
  ]
  node [
    id 602
    label "zapoznawanie"
  ]
  node [
    id 603
    label "demonstrowanie"
  ]
  node [
    id 604
    label "display"
  ]
  node [
    id 605
    label "representation"
  ]
  node [
    id 606
    label "uprzedzanie"
  ]
  node [
    id 607
    label "przedstawianie"
  ]
  node [
    id 608
    label "rozb&#243;jnik"
  ]
  node [
    id 609
    label "kopiowa&#263;"
  ]
  node [
    id 610
    label "podr&#243;bka"
  ]
  node [
    id 611
    label "&#380;agl&#243;wka"
  ]
  node [
    id 612
    label "rum"
  ]
  node [
    id 613
    label "postrzeleniec"
  ]
  node [
    id 614
    label "kieruj&#261;cy"
  ]
  node [
    id 615
    label "uprzedzenie"
  ]
  node [
    id 616
    label "przedstawienie"
  ]
  node [
    id 617
    label "exhibit"
  ]
  node [
    id 618
    label "pokazanie"
  ]
  node [
    id 619
    label "zapoznanie_si&#281;"
  ]
  node [
    id 620
    label "wyst&#261;pienie"
  ]
  node [
    id 621
    label "zapoznanie"
  ]
  node [
    id 622
    label "uprzedza&#263;"
  ]
  node [
    id 623
    label "gra&#263;"
  ]
  node [
    id 624
    label "przedstawia&#263;"
  ]
  node [
    id 625
    label "wyra&#380;a&#263;"
  ]
  node [
    id 626
    label "zapoznawa&#263;"
  ]
  node [
    id 627
    label "tembr"
  ]
  node [
    id 628
    label "wprowadzanie"
  ]
  node [
    id 629
    label "wydzielanie"
  ]
  node [
    id 630
    label "wydzielenie"
  ]
  node [
    id 631
    label "wysy&#322;anie"
  ]
  node [
    id 632
    label "wprowadzenie"
  ]
  node [
    id 633
    label "energia"
  ]
  node [
    id 634
    label "wydobywanie"
  ]
  node [
    id 635
    label "issue"
  ]
  node [
    id 636
    label "nadanie"
  ]
  node [
    id 637
    label "wydobycie"
  ]
  node [
    id 638
    label "emission"
  ]
  node [
    id 639
    label "wys&#322;anie"
  ]
  node [
    id 640
    label "rynek"
  ]
  node [
    id 641
    label "nadawanie"
  ]
  node [
    id 642
    label "wys&#322;a&#263;"
  ]
  node [
    id 643
    label "emit"
  ]
  node [
    id 644
    label "nadawa&#263;"
  ]
  node [
    id 645
    label "air"
  ]
  node [
    id 646
    label "wydoby&#263;"
  ]
  node [
    id 647
    label "nada&#263;"
  ]
  node [
    id 648
    label "wprowadzi&#263;"
  ]
  node [
    id 649
    label "wysy&#322;a&#263;"
  ]
  node [
    id 650
    label "wprowadza&#263;"
  ]
  node [
    id 651
    label "wydobywa&#263;"
  ]
  node [
    id 652
    label "wydziela&#263;"
  ]
  node [
    id 653
    label "wydzieli&#263;"
  ]
  node [
    id 654
    label "instruktarz"
  ]
  node [
    id 655
    label "wskaz&#243;wka"
  ]
  node [
    id 656
    label "ulotka"
  ]
  node [
    id 657
    label "routine"
  ]
  node [
    id 658
    label "proceduralnie"
  ]
  node [
    id 659
    label "danie"
  ]
  node [
    id 660
    label "cennik"
  ]
  node [
    id 661
    label "chart"
  ]
  node [
    id 662
    label "restauracja"
  ]
  node [
    id 663
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 664
    label "zestaw"
  ]
  node [
    id 665
    label "karta"
  ]
  node [
    id 666
    label "skorupa_ziemska"
  ]
  node [
    id 667
    label "budynek"
  ]
  node [
    id 668
    label "przeszkoda"
  ]
  node [
    id 669
    label "bry&#322;a"
  ]
  node [
    id 670
    label "j&#261;kanie"
  ]
  node [
    id 671
    label "square"
  ]
  node [
    id 672
    label "bloking"
  ]
  node [
    id 673
    label "kontynent"
  ]
  node [
    id 674
    label "ok&#322;adka"
  ]
  node [
    id 675
    label "kr&#261;g"
  ]
  node [
    id 676
    label "start"
  ]
  node [
    id 677
    label "blockage"
  ]
  node [
    id 678
    label "blokowisko"
  ]
  node [
    id 679
    label "artyku&#322;"
  ]
  node [
    id 680
    label "stok_kontynentalny"
  ]
  node [
    id 681
    label "bajt"
  ]
  node [
    id 682
    label "barak"
  ]
  node [
    id 683
    label "referat"
  ]
  node [
    id 684
    label "nastawnia"
  ]
  node [
    id 685
    label "organizacja"
  ]
  node [
    id 686
    label "grupa"
  ]
  node [
    id 687
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 688
    label "dom_wielorodzinny"
  ]
  node [
    id 689
    label "zeszyt"
  ]
  node [
    id 690
    label "siatk&#243;wka"
  ]
  node [
    id 691
    label "block"
  ]
  node [
    id 692
    label "bie&#380;nia"
  ]
  node [
    id 693
    label "miejsce_pracy"
  ]
  node [
    id 694
    label "poddzia&#322;"
  ]
  node [
    id 695
    label "bezdro&#380;e"
  ]
  node [
    id 696
    label "insourcing"
  ]
  node [
    id 697
    label "stopie&#324;"
  ]
  node [
    id 698
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 699
    label "competence"
  ]
  node [
    id 700
    label "sfera"
  ]
  node [
    id 701
    label "jednostka_organizacyjna"
  ]
  node [
    id 702
    label "infliction"
  ]
  node [
    id 703
    label "spowodowanie"
  ]
  node [
    id 704
    label "przygotowanie"
  ]
  node [
    id 705
    label "point"
  ]
  node [
    id 706
    label "poubieranie"
  ]
  node [
    id 707
    label "rozebranie"
  ]
  node [
    id 708
    label "str&#243;j"
  ]
  node [
    id 709
    label "budowla"
  ]
  node [
    id 710
    label "przewidzenie"
  ]
  node [
    id 711
    label "zak&#322;adka"
  ]
  node [
    id 712
    label "twierdzenie"
  ]
  node [
    id 713
    label "przygotowywanie"
  ]
  node [
    id 714
    label "podwini&#281;cie"
  ]
  node [
    id 715
    label "zap&#322;acenie"
  ]
  node [
    id 716
    label "wyko&#324;czenie"
  ]
  node [
    id 717
    label "utworzenie"
  ]
  node [
    id 718
    label "przebranie"
  ]
  node [
    id 719
    label "obleczenie"
  ]
  node [
    id 720
    label "przymierzenie"
  ]
  node [
    id 721
    label "obleczenie_si&#281;"
  ]
  node [
    id 722
    label "przywdzianie"
  ]
  node [
    id 723
    label "przyodzianie"
  ]
  node [
    id 724
    label "pokrycie"
  ]
  node [
    id 725
    label "obiekt_matematyczny"
  ]
  node [
    id 726
    label "stopie&#324;_pisma"
  ]
  node [
    id 727
    label "problemat"
  ]
  node [
    id 728
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 729
    label "obiekt"
  ]
  node [
    id 730
    label "plamka"
  ]
  node [
    id 731
    label "przestrze&#324;"
  ]
  node [
    id 732
    label "mark"
  ]
  node [
    id 733
    label "ust&#281;p"
  ]
  node [
    id 734
    label "po&#322;o&#380;enie"
  ]
  node [
    id 735
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 736
    label "kres"
  ]
  node [
    id 737
    label "plan"
  ]
  node [
    id 738
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 739
    label "chwila"
  ]
  node [
    id 740
    label "podpunkt"
  ]
  node [
    id 741
    label "sprawa"
  ]
  node [
    id 742
    label "problematyka"
  ]
  node [
    id 743
    label "zapunktowa&#263;"
  ]
  node [
    id 744
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 745
    label "okienko"
  ]
  node [
    id 746
    label "scheduling"
  ]
  node [
    id 747
    label "odlew"
  ]
  node [
    id 748
    label "kosmetyk"
  ]
  node [
    id 749
    label "karoseria"
  ]
  node [
    id 750
    label "zas&#322;ona"
  ]
  node [
    id 751
    label "pokrywa"
  ]
  node [
    id 752
    label "przebieraniec"
  ]
  node [
    id 753
    label "hood"
  ]
  node [
    id 754
    label "przesada"
  ]
  node [
    id 755
    label "gra"
  ]
  node [
    id 756
    label "os&#322;ona"
  ]
  node [
    id 757
    label "mask"
  ]
  node [
    id 758
    label "visor"
  ]
  node [
    id 759
    label "nag&#322;owie"
  ]
  node [
    id 760
    label "mode"
  ]
  node [
    id 761
    label "aparat_tlenowy"
  ]
  node [
    id 762
    label "twarz"
  ]
  node [
    id 763
    label "wyraz_twarzy"
  ]
  node [
    id 764
    label "p&#243;&#322;profil"
  ]
  node [
    id 765
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 766
    label "rys"
  ]
  node [
    id 767
    label "brew"
  ]
  node [
    id 768
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 769
    label "micha"
  ]
  node [
    id 770
    label "ucho"
  ]
  node [
    id 771
    label "profil"
  ]
  node [
    id 772
    label "podbr&#243;dek"
  ]
  node [
    id 773
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 774
    label "policzek"
  ]
  node [
    id 775
    label "posta&#263;"
  ]
  node [
    id 776
    label "cera"
  ]
  node [
    id 777
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 778
    label "czo&#322;o"
  ]
  node [
    id 779
    label "oko"
  ]
  node [
    id 780
    label "uj&#281;cie"
  ]
  node [
    id 781
    label "dzi&#243;b"
  ]
  node [
    id 782
    label "maskowato&#347;&#263;"
  ]
  node [
    id 783
    label "powieka"
  ]
  node [
    id 784
    label "nos"
  ]
  node [
    id 785
    label "wielko&#347;&#263;"
  ]
  node [
    id 786
    label "prz&#243;d"
  ]
  node [
    id 787
    label "twarzyczka"
  ]
  node [
    id 788
    label "pysk"
  ]
  node [
    id 789
    label "reputacja"
  ]
  node [
    id 790
    label "liczko"
  ]
  node [
    id 791
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 792
    label "usta"
  ]
  node [
    id 793
    label "przedstawiciel"
  ]
  node [
    id 794
    label "p&#322;e&#263;"
  ]
  node [
    id 795
    label "cast"
  ]
  node [
    id 796
    label "ochrona"
  ]
  node [
    id 797
    label "przegroda"
  ]
  node [
    id 798
    label "obronienie"
  ]
  node [
    id 799
    label "przy&#322;bica"
  ]
  node [
    id 800
    label "dekoracja_okna"
  ]
  node [
    id 801
    label "nadmiar"
  ]
  node [
    id 802
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 803
    label "game"
  ]
  node [
    id 804
    label "zbijany"
  ]
  node [
    id 805
    label "rekwizyt_do_gry"
  ]
  node [
    id 806
    label "odg&#322;os"
  ]
  node [
    id 807
    label "Pok&#233;mon"
  ]
  node [
    id 808
    label "komplet"
  ]
  node [
    id 809
    label "apparent_motion"
  ]
  node [
    id 810
    label "contest"
  ]
  node [
    id 811
    label "akcja"
  ]
  node [
    id 812
    label "rozgrywka"
  ]
  node [
    id 813
    label "rywalizacja"
  ]
  node [
    id 814
    label "synteza"
  ]
  node [
    id 815
    label "play"
  ]
  node [
    id 816
    label "odtworzenie"
  ]
  node [
    id 817
    label "zmienno&#347;&#263;"
  ]
  node [
    id 818
    label "niezb&#281;dnik"
  ]
  node [
    id 819
    label "tylec"
  ]
  node [
    id 820
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 821
    label "owad"
  ]
  node [
    id 822
    label "layer"
  ]
  node [
    id 823
    label "przykrywad&#322;o"
  ]
  node [
    id 824
    label "warstwa"
  ]
  node [
    id 825
    label "pi&#243;ro"
  ]
  node [
    id 826
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 827
    label "preparat"
  ]
  node [
    id 828
    label "olejek"
  ]
  node [
    id 829
    label "cosmetic"
  ]
  node [
    id 830
    label "przebiera&#263;_si&#281;"
  ]
  node [
    id 831
    label "crossdresser"
  ]
  node [
    id 832
    label "trans"
  ]
  node [
    id 833
    label "oszust"
  ]
  node [
    id 834
    label "dewiant"
  ]
  node [
    id 835
    label "transwestyta"
  ]
  node [
    id 836
    label "dziwak"
  ]
  node [
    id 837
    label "oszukaniec"
  ]
  node [
    id 838
    label "farmaceutyk"
  ]
  node [
    id 839
    label "operacja"
  ]
  node [
    id 840
    label "blacharka"
  ]
  node [
    id 841
    label "obcierka"
  ]
  node [
    id 842
    label "nadwozie"
  ]
  node [
    id 843
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 844
    label "stand"
  ]
  node [
    id 845
    label "trwa&#263;"
  ]
  node [
    id 846
    label "equal"
  ]
  node [
    id 847
    label "chodzi&#263;"
  ]
  node [
    id 848
    label "uczestniczy&#263;"
  ]
  node [
    id 849
    label "obecno&#347;&#263;"
  ]
  node [
    id 850
    label "si&#281;ga&#263;"
  ]
  node [
    id 851
    label "mie&#263;_miejsce"
  ]
  node [
    id 852
    label "participate"
  ]
  node [
    id 853
    label "adhere"
  ]
  node [
    id 854
    label "pozostawa&#263;"
  ]
  node [
    id 855
    label "zostawa&#263;"
  ]
  node [
    id 856
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 857
    label "istnie&#263;"
  ]
  node [
    id 858
    label "compass"
  ]
  node [
    id 859
    label "exsert"
  ]
  node [
    id 860
    label "get"
  ]
  node [
    id 861
    label "u&#380;ywa&#263;"
  ]
  node [
    id 862
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 863
    label "osi&#261;ga&#263;"
  ]
  node [
    id 864
    label "korzysta&#263;"
  ]
  node [
    id 865
    label "appreciation"
  ]
  node [
    id 866
    label "dociera&#263;"
  ]
  node [
    id 867
    label "mierzy&#263;"
  ]
  node [
    id 868
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 869
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 870
    label "being"
  ]
  node [
    id 871
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 872
    label "proceed"
  ]
  node [
    id 873
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 874
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 875
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 876
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 877
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 878
    label "para"
  ]
  node [
    id 879
    label "krok"
  ]
  node [
    id 880
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 881
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 882
    label "przebiega&#263;"
  ]
  node [
    id 883
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 884
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 885
    label "carry"
  ]
  node [
    id 886
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 887
    label "wk&#322;ada&#263;"
  ]
  node [
    id 888
    label "p&#322;ywa&#263;"
  ]
  node [
    id 889
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 890
    label "bangla&#263;"
  ]
  node [
    id 891
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 892
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 893
    label "bywa&#263;"
  ]
  node [
    id 894
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 895
    label "dziama&#263;"
  ]
  node [
    id 896
    label "run"
  ]
  node [
    id 897
    label "stara&#263;_si&#281;"
  ]
  node [
    id 898
    label "Arakan"
  ]
  node [
    id 899
    label "Teksas"
  ]
  node [
    id 900
    label "Georgia"
  ]
  node [
    id 901
    label "Maryland"
  ]
  node [
    id 902
    label "Michigan"
  ]
  node [
    id 903
    label "Massachusetts"
  ]
  node [
    id 904
    label "Luizjana"
  ]
  node [
    id 905
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 906
    label "samopoczucie"
  ]
  node [
    id 907
    label "Floryda"
  ]
  node [
    id 908
    label "Ohio"
  ]
  node [
    id 909
    label "Alaska"
  ]
  node [
    id 910
    label "Nowy_Meksyk"
  ]
  node [
    id 911
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 912
    label "wci&#281;cie"
  ]
  node [
    id 913
    label "Kansas"
  ]
  node [
    id 914
    label "Alabama"
  ]
  node [
    id 915
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 916
    label "Kalifornia"
  ]
  node [
    id 917
    label "Wirginia"
  ]
  node [
    id 918
    label "Nowy_York"
  ]
  node [
    id 919
    label "Waszyngton"
  ]
  node [
    id 920
    label "Pensylwania"
  ]
  node [
    id 921
    label "wektor"
  ]
  node [
    id 922
    label "Hawaje"
  ]
  node [
    id 923
    label "state"
  ]
  node [
    id 924
    label "jednostka_administracyjna"
  ]
  node [
    id 925
    label "Illinois"
  ]
  node [
    id 926
    label "Oklahoma"
  ]
  node [
    id 927
    label "Oregon"
  ]
  node [
    id 928
    label "Arizona"
  ]
  node [
    id 929
    label "Jukatan"
  ]
  node [
    id 930
    label "shape"
  ]
  node [
    id 931
    label "Goa"
  ]
  node [
    id 932
    label "pewny"
  ]
  node [
    id 933
    label "wolny"
  ]
  node [
    id 934
    label "kompletny"
  ]
  node [
    id 935
    label "czysto"
  ]
  node [
    id 936
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 937
    label "zdrowy"
  ]
  node [
    id 938
    label "klarowanie"
  ]
  node [
    id 939
    label "prze&#378;roczy"
  ]
  node [
    id 940
    label "ostry"
  ]
  node [
    id 941
    label "bezpieczny"
  ]
  node [
    id 942
    label "sklarowanie"
  ]
  node [
    id 943
    label "porz&#261;dny"
  ]
  node [
    id 944
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 945
    label "mycie"
  ]
  node [
    id 946
    label "prawdziwy"
  ]
  node [
    id 947
    label "uczciwy"
  ]
  node [
    id 948
    label "ekologiczny"
  ]
  node [
    id 949
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 950
    label "umycie"
  ]
  node [
    id 951
    label "bezchmurny"
  ]
  node [
    id 952
    label "przyjemny"
  ]
  node [
    id 953
    label "nieodrodny"
  ]
  node [
    id 954
    label "do_czysta"
  ]
  node [
    id 955
    label "schludny"
  ]
  node [
    id 956
    label "czyszczenie_si&#281;"
  ]
  node [
    id 957
    label "ca&#322;y"
  ]
  node [
    id 958
    label "legalny"
  ]
  node [
    id 959
    label "cnotliwy"
  ]
  node [
    id 960
    label "udany"
  ]
  node [
    id 961
    label "dobry"
  ]
  node [
    id 962
    label "jednolity"
  ]
  node [
    id 963
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 964
    label "nieemisyjny"
  ]
  node [
    id 965
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 966
    label "ewidentny"
  ]
  node [
    id 967
    label "doskona&#322;y"
  ]
  node [
    id 968
    label "dopuszczalny"
  ]
  node [
    id 969
    label "klarowny"
  ]
  node [
    id 970
    label "przezroczy&#347;cie"
  ]
  node [
    id 971
    label "wspinaczka"
  ]
  node [
    id 972
    label "klarowanie_si&#281;"
  ]
  node [
    id 973
    label "schludnie"
  ]
  node [
    id 974
    label "dba&#322;y"
  ]
  node [
    id 975
    label "&#322;adny"
  ]
  node [
    id 976
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 977
    label "skuteczny"
  ]
  node [
    id 978
    label "czw&#243;rka"
  ]
  node [
    id 979
    label "spokojny"
  ]
  node [
    id 980
    label "pos&#322;uszny"
  ]
  node [
    id 981
    label "korzystny"
  ]
  node [
    id 982
    label "drogi"
  ]
  node [
    id 983
    label "pozytywny"
  ]
  node [
    id 984
    label "moralny"
  ]
  node [
    id 985
    label "pomy&#347;lny"
  ]
  node [
    id 986
    label "powitanie"
  ]
  node [
    id 987
    label "grzeczny"
  ]
  node [
    id 988
    label "&#347;mieszny"
  ]
  node [
    id 989
    label "odpowiedni"
  ]
  node [
    id 990
    label "zwrot"
  ]
  node [
    id 991
    label "dobrze"
  ]
  node [
    id 992
    label "dobroczynny"
  ]
  node [
    id 993
    label "mi&#322;y"
  ]
  node [
    id 994
    label "mo&#380;liwy"
  ]
  node [
    id 995
    label "dopuszczalnie"
  ]
  node [
    id 996
    label "gajny"
  ]
  node [
    id 997
    label "legalnie"
  ]
  node [
    id 998
    label "upewnienie_si&#281;"
  ]
  node [
    id 999
    label "wiarygodny"
  ]
  node [
    id 1000
    label "ufanie"
  ]
  node [
    id 1001
    label "wierzenie"
  ]
  node [
    id 1002
    label "upewnianie_si&#281;"
  ]
  node [
    id 1003
    label "pewnie"
  ]
  node [
    id 1004
    label "strza&#322;"
  ]
  node [
    id 1005
    label "rozwodnienie"
  ]
  node [
    id 1006
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1007
    label "wakowa&#263;"
  ]
  node [
    id 1008
    label "rozrzedzenie"
  ]
  node [
    id 1009
    label "wolnie"
  ]
  node [
    id 1010
    label "rozrzedzanie"
  ]
  node [
    id 1011
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1012
    label "rozwadnianie"
  ]
  node [
    id 1013
    label "lu&#378;no"
  ]
  node [
    id 1014
    label "niespieszny"
  ]
  node [
    id 1015
    label "niezale&#380;ny"
  ]
  node [
    id 1016
    label "swobodnie"
  ]
  node [
    id 1017
    label "zrzedni&#281;cie"
  ]
  node [
    id 1018
    label "wolno"
  ]
  node [
    id 1019
    label "rzedni&#281;cie"
  ]
  node [
    id 1020
    label "pogodny"
  ]
  node [
    id 1021
    label "bezchmurnie"
  ]
  node [
    id 1022
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 1023
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 1024
    label "ujednolicenie"
  ]
  node [
    id 1025
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 1026
    label "jednakowy"
  ]
  node [
    id 1027
    label "jednolicie"
  ]
  node [
    id 1028
    label "jednostajny"
  ]
  node [
    id 1029
    label "bezpiecznie"
  ]
  node [
    id 1030
    label "schronienie"
  ]
  node [
    id 1031
    label "&#322;atwy"
  ]
  node [
    id 1032
    label "solidny"
  ]
  node [
    id 1033
    label "zgodny"
  ]
  node [
    id 1034
    label "rzetelny"
  ]
  node [
    id 1035
    label "s&#322;uszny"
  ]
  node [
    id 1036
    label "uczciwie"
  ]
  node [
    id 1037
    label "s&#322;usznie"
  ]
  node [
    id 1038
    label "nale&#380;yty"
  ]
  node [
    id 1039
    label "porz&#261;dnie"
  ]
  node [
    id 1040
    label "intensywny"
  ]
  node [
    id 1041
    label "szczery"
  ]
  node [
    id 1042
    label "w_pizdu"
  ]
  node [
    id 1043
    label "zupe&#322;ny"
  ]
  node [
    id 1044
    label "kompletnie"
  ]
  node [
    id 1045
    label "ch&#281;dogi"
  ]
  node [
    id 1046
    label "przyzwoity"
  ]
  node [
    id 1047
    label "przyjemnie"
  ]
  node [
    id 1048
    label "prostolinijny"
  ]
  node [
    id 1049
    label "skromny"
  ]
  node [
    id 1050
    label "szlachetny"
  ]
  node [
    id 1051
    label "cny"
  ]
  node [
    id 1052
    label "cnotliwie"
  ]
  node [
    id 1053
    label "niewinny"
  ]
  node [
    id 1054
    label "prawdziwie"
  ]
  node [
    id 1055
    label "podobny"
  ]
  node [
    id 1056
    label "m&#261;dry"
  ]
  node [
    id 1057
    label "naprawd&#281;"
  ]
  node [
    id 1058
    label "naturalny"
  ]
  node [
    id 1059
    label "&#380;ywny"
  ]
  node [
    id 1060
    label "realnie"
  ]
  node [
    id 1061
    label "ekologicznie"
  ]
  node [
    id 1062
    label "przyjazny"
  ]
  node [
    id 1063
    label "fajny"
  ]
  node [
    id 1064
    label "udanie"
  ]
  node [
    id 1065
    label "kapitalny"
  ]
  node [
    id 1066
    label "prawid&#322;owy"
  ]
  node [
    id 1067
    label "bezb&#322;&#281;dnie"
  ]
  node [
    id 1068
    label "silny"
  ]
  node [
    id 1069
    label "powa&#380;ny"
  ]
  node [
    id 1070
    label "jednoznaczny"
  ]
  node [
    id 1071
    label "wyra&#378;ny"
  ]
  node [
    id 1072
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1073
    label "ci&#281;&#380;ki"
  ]
  node [
    id 1074
    label "raptowny"
  ]
  node [
    id 1075
    label "gryz&#261;cy"
  ]
  node [
    id 1076
    label "gro&#378;ny"
  ]
  node [
    id 1077
    label "podniecaj&#261;cy"
  ]
  node [
    id 1078
    label "nieobyczajny"
  ]
  node [
    id 1079
    label "surowy"
  ]
  node [
    id 1080
    label "trudny"
  ]
  node [
    id 1081
    label "kategoryczny"
  ]
  node [
    id 1082
    label "porywczy"
  ]
  node [
    id 1083
    label "ostrzenie"
  ]
  node [
    id 1084
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 1085
    label "nieneutralny"
  ]
  node [
    id 1086
    label "dramatyczny"
  ]
  node [
    id 1087
    label "za&#380;arcie"
  ]
  node [
    id 1088
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1089
    label "naostrzenie"
  ]
  node [
    id 1090
    label "dziki"
  ]
  node [
    id 1091
    label "dokuczliwy"
  ]
  node [
    id 1092
    label "bystro"
  ]
  node [
    id 1093
    label "dotkliwy"
  ]
  node [
    id 1094
    label "szorstki"
  ]
  node [
    id 1095
    label "widoczny"
  ]
  node [
    id 1096
    label "energiczny"
  ]
  node [
    id 1097
    label "nieprzyjazny"
  ]
  node [
    id 1098
    label "ostro"
  ]
  node [
    id 1099
    label "agresywny"
  ]
  node [
    id 1100
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 1101
    label "mocny"
  ]
  node [
    id 1102
    label "zdecydowany"
  ]
  node [
    id 1103
    label "osch&#322;y"
  ]
  node [
    id 1104
    label "dynamiczny"
  ]
  node [
    id 1105
    label "niebezpieczny"
  ]
  node [
    id 1106
    label "nieoboj&#281;tny"
  ]
  node [
    id 1107
    label "&#347;wietny"
  ]
  node [
    id 1108
    label "naj"
  ]
  node [
    id 1109
    label "doskonale"
  ]
  node [
    id 1110
    label "wspania&#322;y"
  ]
  node [
    id 1111
    label "oczywisty"
  ]
  node [
    id 1112
    label "ewidentnie"
  ]
  node [
    id 1113
    label "zdrowo"
  ]
  node [
    id 1114
    label "uzdrawianie"
  ]
  node [
    id 1115
    label "wyleczenie_si&#281;"
  ]
  node [
    id 1116
    label "uzdrowienie"
  ]
  node [
    id 1117
    label "normalny"
  ]
  node [
    id 1118
    label "rozs&#261;dny"
  ]
  node [
    id 1119
    label "zdrowienie"
  ]
  node [
    id 1120
    label "wyzdrowienie"
  ]
  node [
    id 1121
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 1122
    label "zrozumia&#322;y"
  ]
  node [
    id 1123
    label "klarownie"
  ]
  node [
    id 1124
    label "jasno"
  ]
  node [
    id 1125
    label "zdr&#243;w"
  ]
  node [
    id 1126
    label "ca&#322;o"
  ]
  node [
    id 1127
    label "du&#380;y"
  ]
  node [
    id 1128
    label "calu&#347;ko"
  ]
  node [
    id 1129
    label "&#380;ywy"
  ]
  node [
    id 1130
    label "jedyny"
  ]
  node [
    id 1131
    label "przezroczo"
  ]
  node [
    id 1132
    label "przezroczysty"
  ]
  node [
    id 1133
    label "cleanly"
  ]
  node [
    id 1134
    label "transparently"
  ]
  node [
    id 1135
    label "niedobry"
  ]
  node [
    id 1136
    label "o&#380;ywczy"
  ]
  node [
    id 1137
    label "stymuluj&#261;cy"
  ]
  node [
    id 1138
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 1139
    label "o&#380;ywczo"
  ]
  node [
    id 1140
    label "kolucho"
  ]
  node [
    id 1141
    label "climb"
  ]
  node [
    id 1142
    label "ekspozycja"
  ]
  node [
    id 1143
    label "nieczysty"
  ]
  node [
    id 1144
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1145
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 1146
    label "&#347;ciana_wspinaczkowa"
  ]
  node [
    id 1147
    label "oczyszczenie"
  ]
  node [
    id 1148
    label "jawnie"
  ]
  node [
    id 1149
    label "blado"
  ]
  node [
    id 1150
    label "purge"
  ]
  node [
    id 1151
    label "rendition"
  ]
  node [
    id 1152
    label "rozwianie"
  ]
  node [
    id 1153
    label "kr&#281;ty"
  ]
  node [
    id 1154
    label "rozwiewanie"
  ]
  node [
    id 1155
    label "czyszczenie"
  ]
  node [
    id 1156
    label "wymycie"
  ]
  node [
    id 1157
    label "ablucja"
  ]
  node [
    id 1158
    label "przemycie"
  ]
  node [
    id 1159
    label "wymywanie"
  ]
  node [
    id 1160
    label "wash"
  ]
  node [
    id 1161
    label "przemywanie"
  ]
  node [
    id 1162
    label "toaleta"
  ]
  node [
    id 1163
    label "ablution"
  ]
  node [
    id 1164
    label "Debian"
  ]
  node [
    id 1165
    label "Testing"
  ]
  node [
    id 1166
    label "Minta"
  ]
  node [
    id 1167
    label "10"
  ]
  node [
    id 1168
    label "Linux"
  ]
  node [
    id 1169
    label "Mint"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 11
    target 927
  ]
  edge [
    source 11
    target 928
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 929
  ]
  edge [
    source 11
    target 930
  ]
  edge [
    source 11
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 1073
  ]
  edge [
    source 12
    target 1074
  ]
  edge [
    source 12
    target 1075
  ]
  edge [
    source 12
    target 1076
  ]
  edge [
    source 12
    target 1077
  ]
  edge [
    source 12
    target 1078
  ]
  edge [
    source 12
    target 1079
  ]
  edge [
    source 12
    target 1080
  ]
  edge [
    source 12
    target 1081
  ]
  edge [
    source 12
    target 1082
  ]
  edge [
    source 12
    target 1083
  ]
  edge [
    source 12
    target 1084
  ]
  edge [
    source 12
    target 1085
  ]
  edge [
    source 12
    target 1086
  ]
  edge [
    source 12
    target 1087
  ]
  edge [
    source 12
    target 1088
  ]
  edge [
    source 12
    target 1089
  ]
  edge [
    source 12
    target 1090
  ]
  edge [
    source 12
    target 1091
  ]
  edge [
    source 12
    target 1092
  ]
  edge [
    source 12
    target 1093
  ]
  edge [
    source 12
    target 1094
  ]
  edge [
    source 12
    target 1095
  ]
  edge [
    source 12
    target 1096
  ]
  edge [
    source 12
    target 1097
  ]
  edge [
    source 12
    target 1098
  ]
  edge [
    source 12
    target 1099
  ]
  edge [
    source 12
    target 1100
  ]
  edge [
    source 12
    target 1101
  ]
  edge [
    source 12
    target 1102
  ]
  edge [
    source 12
    target 1103
  ]
  edge [
    source 12
    target 1104
  ]
  edge [
    source 12
    target 1105
  ]
  edge [
    source 12
    target 1106
  ]
  edge [
    source 12
    target 1107
  ]
  edge [
    source 12
    target 1108
  ]
  edge [
    source 12
    target 1109
  ]
  edge [
    source 12
    target 1110
  ]
  edge [
    source 12
    target 1111
  ]
  edge [
    source 12
    target 1112
  ]
  edge [
    source 12
    target 1113
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 1114
  ]
  edge [
    source 12
    target 1115
  ]
  edge [
    source 12
    target 1116
  ]
  edge [
    source 12
    target 1117
  ]
  edge [
    source 12
    target 1118
  ]
  edge [
    source 12
    target 1119
  ]
  edge [
    source 12
    target 1120
  ]
  edge [
    source 12
    target 1121
  ]
  edge [
    source 12
    target 1122
  ]
  edge [
    source 12
    target 1123
  ]
  edge [
    source 12
    target 1124
  ]
  edge [
    source 12
    target 1125
  ]
  edge [
    source 12
    target 1126
  ]
  edge [
    source 12
    target 1127
  ]
  edge [
    source 12
    target 1128
  ]
  edge [
    source 12
    target 1129
  ]
  edge [
    source 12
    target 1130
  ]
  edge [
    source 12
    target 1131
  ]
  edge [
    source 12
    target 1132
  ]
  edge [
    source 12
    target 1133
  ]
  edge [
    source 12
    target 1134
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 1136
  ]
  edge [
    source 12
    target 1137
  ]
  edge [
    source 12
    target 1138
  ]
  edge [
    source 12
    target 1139
  ]
  edge [
    source 12
    target 1140
  ]
  edge [
    source 12
    target 1141
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 1142
  ]
  edge [
    source 12
    target 1143
  ]
  edge [
    source 12
    target 1144
  ]
  edge [
    source 12
    target 1145
  ]
  edge [
    source 12
    target 1146
  ]
  edge [
    source 12
    target 1147
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1151
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 12
    target 1153
  ]
  edge [
    source 12
    target 1154
  ]
  edge [
    source 12
    target 1155
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 1156
  ]
  edge [
    source 12
    target 1157
  ]
  edge [
    source 12
    target 1158
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1160
  ]
  edge [
    source 12
    target 1161
  ]
  edge [
    source 12
    target 1162
  ]
  edge [
    source 12
    target 1163
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 1164
    target 1165
  ]
  edge [
    source 1166
    target 1167
  ]
  edge [
    source 1168
    target 1169
  ]
]
