graph [
  node [
    id 0
    label "licklider"
    origin "text"
  ]
  node [
    id 1
    label "taylor"
    origin "text"
  ]
  node [
    id 2
    label "nasta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "powszechny"
    origin "text"
  ]
  node [
    id 4
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 5
    label "robot"
    origin "text"
  ]
  node [
    id 6
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 7
    label "supervene"
  ]
  node [
    id 8
    label "znany"
  ]
  node [
    id 9
    label "zbiorowy"
  ]
  node [
    id 10
    label "cz&#281;sty"
  ]
  node [
    id 11
    label "powszechnie"
  ]
  node [
    id 12
    label "generalny"
  ]
  node [
    id 13
    label "wsp&#243;lny"
  ]
  node [
    id 14
    label "zbiorowo"
  ]
  node [
    id 15
    label "og&#243;lnie"
  ]
  node [
    id 16
    label "zwierzchni"
  ]
  node [
    id 17
    label "porz&#261;dny"
  ]
  node [
    id 18
    label "nadrz&#281;dny"
  ]
  node [
    id 19
    label "podstawowy"
  ]
  node [
    id 20
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 21
    label "zasadniczy"
  ]
  node [
    id 22
    label "generalnie"
  ]
  node [
    id 23
    label "cz&#281;sto"
  ]
  node [
    id 24
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 25
    label "wielki"
  ]
  node [
    id 26
    label "rozpowszechnianie"
  ]
  node [
    id 27
    label "og&#243;lny"
  ]
  node [
    id 28
    label "elektroniczny"
  ]
  node [
    id 29
    label "cyfrowo"
  ]
  node [
    id 30
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 31
    label "cyfryzacja"
  ]
  node [
    id 32
    label "modernizacja"
  ]
  node [
    id 33
    label "elektrycznie"
  ]
  node [
    id 34
    label "elektronicznie"
  ]
  node [
    id 35
    label "sprz&#281;t_AGD"
  ]
  node [
    id 36
    label "maszyna"
  ]
  node [
    id 37
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 38
    label "automat"
  ]
  node [
    id 39
    label "cz&#322;owiek"
  ]
  node [
    id 40
    label "automatyczna_skrzynia_bieg&#243;w"
  ]
  node [
    id 41
    label "wrzutnik_monet"
  ]
  node [
    id 42
    label "samoch&#243;d"
  ]
  node [
    id 43
    label "telefon"
  ]
  node [
    id 44
    label "dehumanizacja"
  ]
  node [
    id 45
    label "pistolet"
  ]
  node [
    id 46
    label "bro&#324;_samoczynno-samopowtarzalna"
  ]
  node [
    id 47
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 48
    label "pralka"
  ]
  node [
    id 49
    label "urz&#261;dzenie"
  ]
  node [
    id 50
    label "lody_w&#322;oskie"
  ]
  node [
    id 51
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 52
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 53
    label "tuleja"
  ]
  node [
    id 54
    label "pracowanie"
  ]
  node [
    id 55
    label "kad&#322;ub"
  ]
  node [
    id 56
    label "n&#243;&#380;"
  ]
  node [
    id 57
    label "b&#281;benek"
  ]
  node [
    id 58
    label "wa&#322;"
  ]
  node [
    id 59
    label "maszyneria"
  ]
  node [
    id 60
    label "prototypownia"
  ]
  node [
    id 61
    label "trawers"
  ]
  node [
    id 62
    label "deflektor"
  ]
  node [
    id 63
    label "kolumna"
  ]
  node [
    id 64
    label "mechanizm"
  ]
  node [
    id 65
    label "wa&#322;ek"
  ]
  node [
    id 66
    label "pracowa&#263;"
  ]
  node [
    id 67
    label "b&#281;ben"
  ]
  node [
    id 68
    label "rz&#281;zi&#263;"
  ]
  node [
    id 69
    label "przyk&#322;adka"
  ]
  node [
    id 70
    label "t&#322;ok"
  ]
  node [
    id 71
    label "rami&#281;"
  ]
  node [
    id 72
    label "rz&#281;&#380;enie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
]
