graph [
  node [
    id 0
    label "pis"
    origin "text"
  ]
  node [
    id 1
    label "swoje"
    origin "text"
  ]
  node [
    id 2
    label "wobec"
    origin "text"
  ]
  node [
    id 3
    label "farma"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;oneczny"
    origin "text"
  ]
  node [
    id 5
    label "wiatrowy"
    origin "text"
  ]
  node [
    id 6
    label "inny"
    origin "text"
  ]
  node [
    id 7
    label "oze"
    origin "text"
  ]
  node [
    id 8
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wygini&#281;cie"
    origin "text"
  ]
  node [
    id 10
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 11
    label "gospodarstwo_rolne"
  ]
  node [
    id 12
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 13
    label "s&#322;onecznie"
  ]
  node [
    id 14
    label "letni"
  ]
  node [
    id 15
    label "weso&#322;y"
  ]
  node [
    id 16
    label "bezdeszczowy"
  ]
  node [
    id 17
    label "ciep&#322;y"
  ]
  node [
    id 18
    label "bezchmurny"
  ]
  node [
    id 19
    label "pogodny"
  ]
  node [
    id 20
    label "fotowoltaiczny"
  ]
  node [
    id 21
    label "jasny"
  ]
  node [
    id 22
    label "o&#347;wietlenie"
  ]
  node [
    id 23
    label "szczery"
  ]
  node [
    id 24
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 25
    label "jasno"
  ]
  node [
    id 26
    label "o&#347;wietlanie"
  ]
  node [
    id 27
    label "przytomny"
  ]
  node [
    id 28
    label "zrozumia&#322;y"
  ]
  node [
    id 29
    label "niezm&#261;cony"
  ]
  node [
    id 30
    label "bia&#322;y"
  ]
  node [
    id 31
    label "jednoznaczny"
  ]
  node [
    id 32
    label "klarowny"
  ]
  node [
    id 33
    label "dobry"
  ]
  node [
    id 34
    label "mi&#322;y"
  ]
  node [
    id 35
    label "ocieplanie_si&#281;"
  ]
  node [
    id 36
    label "ocieplanie"
  ]
  node [
    id 37
    label "grzanie"
  ]
  node [
    id 38
    label "ocieplenie_si&#281;"
  ]
  node [
    id 39
    label "zagrzanie"
  ]
  node [
    id 40
    label "ocieplenie"
  ]
  node [
    id 41
    label "korzystny"
  ]
  node [
    id 42
    label "przyjemny"
  ]
  node [
    id 43
    label "ciep&#322;o"
  ]
  node [
    id 44
    label "spokojny"
  ]
  node [
    id 45
    label "&#322;adny"
  ]
  node [
    id 46
    label "udany"
  ]
  node [
    id 47
    label "pozytywny"
  ]
  node [
    id 48
    label "pogodnie"
  ]
  node [
    id 49
    label "pijany"
  ]
  node [
    id 50
    label "weso&#322;o"
  ]
  node [
    id 51
    label "beztroski"
  ]
  node [
    id 52
    label "bezopadowy"
  ]
  node [
    id 53
    label "bezdeszczowo"
  ]
  node [
    id 54
    label "bezchmurnie"
  ]
  node [
    id 55
    label "wolny"
  ]
  node [
    id 56
    label "ekologiczny"
  ]
  node [
    id 57
    label "latowy"
  ]
  node [
    id 58
    label "typowy"
  ]
  node [
    id 59
    label "sezonowy"
  ]
  node [
    id 60
    label "letnio"
  ]
  node [
    id 61
    label "oboj&#281;tny"
  ]
  node [
    id 62
    label "nijaki"
  ]
  node [
    id 63
    label "kolejny"
  ]
  node [
    id 64
    label "osobno"
  ]
  node [
    id 65
    label "r&#243;&#380;ny"
  ]
  node [
    id 66
    label "inszy"
  ]
  node [
    id 67
    label "inaczej"
  ]
  node [
    id 68
    label "odr&#281;bny"
  ]
  node [
    id 69
    label "nast&#281;pnie"
  ]
  node [
    id 70
    label "nastopny"
  ]
  node [
    id 71
    label "kolejno"
  ]
  node [
    id 72
    label "kt&#243;ry&#347;"
  ]
  node [
    id 73
    label "jaki&#347;"
  ]
  node [
    id 74
    label "r&#243;&#380;nie"
  ]
  node [
    id 75
    label "niestandardowo"
  ]
  node [
    id 76
    label "individually"
  ]
  node [
    id 77
    label "udzielnie"
  ]
  node [
    id 78
    label "osobnie"
  ]
  node [
    id 79
    label "odr&#281;bnie"
  ]
  node [
    id 80
    label "osobny"
  ]
  node [
    id 81
    label "set"
  ]
  node [
    id 82
    label "wykona&#263;"
  ]
  node [
    id 83
    label "pos&#322;a&#263;"
  ]
  node [
    id 84
    label "carry"
  ]
  node [
    id 85
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 86
    label "poprowadzi&#263;"
  ]
  node [
    id 87
    label "take"
  ]
  node [
    id 88
    label "spowodowa&#263;"
  ]
  node [
    id 89
    label "wprowadzi&#263;"
  ]
  node [
    id 90
    label "wzbudzi&#263;"
  ]
  node [
    id 91
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 92
    label "act"
  ]
  node [
    id 93
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 94
    label "zbudowa&#263;"
  ]
  node [
    id 95
    label "krzywa"
  ]
  node [
    id 96
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 97
    label "control"
  ]
  node [
    id 98
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 99
    label "leave"
  ]
  node [
    id 100
    label "nakre&#347;li&#263;"
  ]
  node [
    id 101
    label "moderate"
  ]
  node [
    id 102
    label "guidebook"
  ]
  node [
    id 103
    label "wytworzy&#263;"
  ]
  node [
    id 104
    label "picture"
  ]
  node [
    id 105
    label "manufacture"
  ]
  node [
    id 106
    label "zrobi&#263;"
  ]
  node [
    id 107
    label "nakaza&#263;"
  ]
  node [
    id 108
    label "pod&#322;o&#380;y&#263;"
  ]
  node [
    id 109
    label "przekaza&#263;"
  ]
  node [
    id 110
    label "dispatch"
  ]
  node [
    id 111
    label "report"
  ]
  node [
    id 112
    label "ship"
  ]
  node [
    id 113
    label "wys&#322;a&#263;"
  ]
  node [
    id 114
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 115
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 116
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 117
    label "post"
  ]
  node [
    id 118
    label "convey"
  ]
  node [
    id 119
    label "wywo&#322;a&#263;"
  ]
  node [
    id 120
    label "arouse"
  ]
  node [
    id 121
    label "gem"
  ]
  node [
    id 122
    label "kompozycja"
  ]
  node [
    id 123
    label "runda"
  ]
  node [
    id 124
    label "muzyka"
  ]
  node [
    id 125
    label "zestaw"
  ]
  node [
    id 126
    label "rynek"
  ]
  node [
    id 127
    label "testify"
  ]
  node [
    id 128
    label "insert"
  ]
  node [
    id 129
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 130
    label "wpisa&#263;"
  ]
  node [
    id 131
    label "zapozna&#263;"
  ]
  node [
    id 132
    label "wej&#347;&#263;"
  ]
  node [
    id 133
    label "zej&#347;&#263;"
  ]
  node [
    id 134
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 135
    label "umie&#347;ci&#263;"
  ]
  node [
    id 136
    label "zacz&#261;&#263;"
  ]
  node [
    id 137
    label "indicate"
  ]
  node [
    id 138
    label "extinction"
  ]
  node [
    id 139
    label "pomarcie"
  ]
  node [
    id 140
    label "Irokezi"
  ]
  node [
    id 141
    label "ludno&#347;&#263;"
  ]
  node [
    id 142
    label "Apacze"
  ]
  node [
    id 143
    label "Syngalezi"
  ]
  node [
    id 144
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 145
    label "t&#322;um"
  ]
  node [
    id 146
    label "Mohikanie"
  ]
  node [
    id 147
    label "Komancze"
  ]
  node [
    id 148
    label "lud"
  ]
  node [
    id 149
    label "Siuksowie"
  ]
  node [
    id 150
    label "Buriaci"
  ]
  node [
    id 151
    label "Samojedzi"
  ]
  node [
    id 152
    label "Baszkirzy"
  ]
  node [
    id 153
    label "Wotiacy"
  ]
  node [
    id 154
    label "Aztekowie"
  ]
  node [
    id 155
    label "nacja"
  ]
  node [
    id 156
    label "Czejenowie"
  ]
  node [
    id 157
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 158
    label "innowierstwo"
  ]
  node [
    id 159
    label "ch&#322;opstwo"
  ]
  node [
    id 160
    label "grupa"
  ]
  node [
    id 161
    label "najazd"
  ]
  node [
    id 162
    label "demofobia"
  ]
  node [
    id 163
    label "Tagalowie"
  ]
  node [
    id 164
    label "Ugrowie"
  ]
  node [
    id 165
    label "Retowie"
  ]
  node [
    id 166
    label "Negryci"
  ]
  node [
    id 167
    label "Ladynowie"
  ]
  node [
    id 168
    label "Wizygoci"
  ]
  node [
    id 169
    label "Dogonowie"
  ]
  node [
    id 170
    label "chamstwo"
  ]
  node [
    id 171
    label "Do&#322;ganie"
  ]
  node [
    id 172
    label "Indoira&#324;czycy"
  ]
  node [
    id 173
    label "gmin"
  ]
  node [
    id 174
    label "Kozacy"
  ]
  node [
    id 175
    label "Indoariowie"
  ]
  node [
    id 176
    label "Maroni"
  ]
  node [
    id 177
    label "Po&#322;owcy"
  ]
  node [
    id 178
    label "Kumbrowie"
  ]
  node [
    id 179
    label "Nogajowie"
  ]
  node [
    id 180
    label "Nawahowie"
  ]
  node [
    id 181
    label "Wenedowie"
  ]
  node [
    id 182
    label "Majowie"
  ]
  node [
    id 183
    label "Kipczacy"
  ]
  node [
    id 184
    label "Frygijczycy"
  ]
  node [
    id 185
    label "Paleoazjaci"
  ]
  node [
    id 186
    label "Tocharowie"
  ]
  node [
    id 187
    label "etnogeneza"
  ]
  node [
    id 188
    label "nationality"
  ]
  node [
    id 189
    label "Mongolia"
  ]
  node [
    id 190
    label "Sri_Lanka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
]
