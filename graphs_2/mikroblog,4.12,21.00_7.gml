graph [
  node [
    id 0
    label "szkola"
    origin "text"
  ]
  node [
    id 1
    label "sekretariat"
    origin "text"
  ]
  node [
    id 2
    label "dziecko"
    origin "text"
  ]
  node [
    id 3
    label "pracbaza"
    origin "text"
  ]
  node [
    id 4
    label "biuro"
  ]
  node [
    id 5
    label "s&#261;d"
  ]
  node [
    id 6
    label "biurko"
  ]
  node [
    id 7
    label "boks"
  ]
  node [
    id 8
    label "instytucja"
  ]
  node [
    id 9
    label "palestra"
  ]
  node [
    id 10
    label "Biuro_Lustracyjne"
  ]
  node [
    id 11
    label "agency"
  ]
  node [
    id 12
    label "board"
  ]
  node [
    id 13
    label "dzia&#322;"
  ]
  node [
    id 14
    label "pomieszczenie"
  ]
  node [
    id 15
    label "utulenie"
  ]
  node [
    id 16
    label "pediatra"
  ]
  node [
    id 17
    label "dzieciak"
  ]
  node [
    id 18
    label "utulanie"
  ]
  node [
    id 19
    label "dzieciarnia"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "niepe&#322;noletni"
  ]
  node [
    id 22
    label "organizm"
  ]
  node [
    id 23
    label "utula&#263;"
  ]
  node [
    id 24
    label "cz&#322;owieczek"
  ]
  node [
    id 25
    label "fledgling"
  ]
  node [
    id 26
    label "zwierz&#281;"
  ]
  node [
    id 27
    label "utuli&#263;"
  ]
  node [
    id 28
    label "m&#322;odzik"
  ]
  node [
    id 29
    label "pedofil"
  ]
  node [
    id 30
    label "m&#322;odziak"
  ]
  node [
    id 31
    label "potomek"
  ]
  node [
    id 32
    label "entliczek-pentliczek"
  ]
  node [
    id 33
    label "potomstwo"
  ]
  node [
    id 34
    label "sraluch"
  ]
  node [
    id 35
    label "zbi&#243;r"
  ]
  node [
    id 36
    label "czeladka"
  ]
  node [
    id 37
    label "dzietno&#347;&#263;"
  ]
  node [
    id 38
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 39
    label "bawienie_si&#281;"
  ]
  node [
    id 40
    label "pomiot"
  ]
  node [
    id 41
    label "grupa"
  ]
  node [
    id 42
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 43
    label "kinderbal"
  ]
  node [
    id 44
    label "krewny"
  ]
  node [
    id 45
    label "ludzko&#347;&#263;"
  ]
  node [
    id 46
    label "asymilowanie"
  ]
  node [
    id 47
    label "wapniak"
  ]
  node [
    id 48
    label "asymilowa&#263;"
  ]
  node [
    id 49
    label "os&#322;abia&#263;"
  ]
  node [
    id 50
    label "posta&#263;"
  ]
  node [
    id 51
    label "hominid"
  ]
  node [
    id 52
    label "podw&#322;adny"
  ]
  node [
    id 53
    label "os&#322;abianie"
  ]
  node [
    id 54
    label "g&#322;owa"
  ]
  node [
    id 55
    label "figura"
  ]
  node [
    id 56
    label "portrecista"
  ]
  node [
    id 57
    label "dwun&#243;g"
  ]
  node [
    id 58
    label "profanum"
  ]
  node [
    id 59
    label "mikrokosmos"
  ]
  node [
    id 60
    label "nasada"
  ]
  node [
    id 61
    label "duch"
  ]
  node [
    id 62
    label "antropochoria"
  ]
  node [
    id 63
    label "osoba"
  ]
  node [
    id 64
    label "wz&#243;r"
  ]
  node [
    id 65
    label "senior"
  ]
  node [
    id 66
    label "oddzia&#322;ywanie"
  ]
  node [
    id 67
    label "Adam"
  ]
  node [
    id 68
    label "homo_sapiens"
  ]
  node [
    id 69
    label "polifag"
  ]
  node [
    id 70
    label "ma&#322;oletny"
  ]
  node [
    id 71
    label "m&#322;ody"
  ]
  node [
    id 72
    label "degenerat"
  ]
  node [
    id 73
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 74
    label "zwyrol"
  ]
  node [
    id 75
    label "czerniak"
  ]
  node [
    id 76
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 77
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 78
    label "paszcza"
  ]
  node [
    id 79
    label "popapraniec"
  ]
  node [
    id 80
    label "skuba&#263;"
  ]
  node [
    id 81
    label "skubanie"
  ]
  node [
    id 82
    label "agresja"
  ]
  node [
    id 83
    label "skubni&#281;cie"
  ]
  node [
    id 84
    label "zwierz&#281;ta"
  ]
  node [
    id 85
    label "fukni&#281;cie"
  ]
  node [
    id 86
    label "farba"
  ]
  node [
    id 87
    label "fukanie"
  ]
  node [
    id 88
    label "istota_&#380;ywa"
  ]
  node [
    id 89
    label "gad"
  ]
  node [
    id 90
    label "tresowa&#263;"
  ]
  node [
    id 91
    label "siedzie&#263;"
  ]
  node [
    id 92
    label "oswaja&#263;"
  ]
  node [
    id 93
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 94
    label "poligamia"
  ]
  node [
    id 95
    label "oz&#243;r"
  ]
  node [
    id 96
    label "skubn&#261;&#263;"
  ]
  node [
    id 97
    label "wios&#322;owa&#263;"
  ]
  node [
    id 98
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 99
    label "le&#380;enie"
  ]
  node [
    id 100
    label "niecz&#322;owiek"
  ]
  node [
    id 101
    label "wios&#322;owanie"
  ]
  node [
    id 102
    label "napasienie_si&#281;"
  ]
  node [
    id 103
    label "wiwarium"
  ]
  node [
    id 104
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 105
    label "animalista"
  ]
  node [
    id 106
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 107
    label "budowa"
  ]
  node [
    id 108
    label "hodowla"
  ]
  node [
    id 109
    label "pasienie_si&#281;"
  ]
  node [
    id 110
    label "sodomita"
  ]
  node [
    id 111
    label "monogamia"
  ]
  node [
    id 112
    label "przyssawka"
  ]
  node [
    id 113
    label "zachowanie"
  ]
  node [
    id 114
    label "budowa_cia&#322;a"
  ]
  node [
    id 115
    label "okrutnik"
  ]
  node [
    id 116
    label "grzbiet"
  ]
  node [
    id 117
    label "weterynarz"
  ]
  node [
    id 118
    label "&#322;eb"
  ]
  node [
    id 119
    label "wylinka"
  ]
  node [
    id 120
    label "bestia"
  ]
  node [
    id 121
    label "poskramia&#263;"
  ]
  node [
    id 122
    label "fauna"
  ]
  node [
    id 123
    label "treser"
  ]
  node [
    id 124
    label "siedzenie"
  ]
  node [
    id 125
    label "le&#380;e&#263;"
  ]
  node [
    id 126
    label "p&#322;aszczyzna"
  ]
  node [
    id 127
    label "odwadnia&#263;"
  ]
  node [
    id 128
    label "przyswoi&#263;"
  ]
  node [
    id 129
    label "sk&#243;ra"
  ]
  node [
    id 130
    label "odwodni&#263;"
  ]
  node [
    id 131
    label "ewoluowanie"
  ]
  node [
    id 132
    label "staw"
  ]
  node [
    id 133
    label "ow&#322;osienie"
  ]
  node [
    id 134
    label "unerwienie"
  ]
  node [
    id 135
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 136
    label "reakcja"
  ]
  node [
    id 137
    label "wyewoluowanie"
  ]
  node [
    id 138
    label "przyswajanie"
  ]
  node [
    id 139
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 140
    label "wyewoluowa&#263;"
  ]
  node [
    id 141
    label "miejsce"
  ]
  node [
    id 142
    label "biorytm"
  ]
  node [
    id 143
    label "ewoluowa&#263;"
  ]
  node [
    id 144
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 145
    label "otworzy&#263;"
  ]
  node [
    id 146
    label "otwiera&#263;"
  ]
  node [
    id 147
    label "czynnik_biotyczny"
  ]
  node [
    id 148
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 149
    label "otworzenie"
  ]
  node [
    id 150
    label "otwieranie"
  ]
  node [
    id 151
    label "individual"
  ]
  node [
    id 152
    label "szkielet"
  ]
  node [
    id 153
    label "ty&#322;"
  ]
  node [
    id 154
    label "obiekt"
  ]
  node [
    id 155
    label "przyswaja&#263;"
  ]
  node [
    id 156
    label "przyswojenie"
  ]
  node [
    id 157
    label "odwadnianie"
  ]
  node [
    id 158
    label "odwodnienie"
  ]
  node [
    id 159
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 160
    label "starzenie_si&#281;"
  ]
  node [
    id 161
    label "prz&#243;d"
  ]
  node [
    id 162
    label "uk&#322;ad"
  ]
  node [
    id 163
    label "temperatura"
  ]
  node [
    id 164
    label "l&#281;d&#378;wie"
  ]
  node [
    id 165
    label "cia&#322;o"
  ]
  node [
    id 166
    label "cz&#322;onek"
  ]
  node [
    id 167
    label "utulanie_si&#281;"
  ]
  node [
    id 168
    label "usypianie"
  ]
  node [
    id 169
    label "pocieszanie"
  ]
  node [
    id 170
    label "uspokajanie"
  ]
  node [
    id 171
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 172
    label "uspokoi&#263;"
  ]
  node [
    id 173
    label "uspokojenie"
  ]
  node [
    id 174
    label "utulenie_si&#281;"
  ]
  node [
    id 175
    label "u&#347;pienie"
  ]
  node [
    id 176
    label "usypia&#263;"
  ]
  node [
    id 177
    label "uspokaja&#263;"
  ]
  node [
    id 178
    label "dewiant"
  ]
  node [
    id 179
    label "specjalista"
  ]
  node [
    id 180
    label "wyliczanka"
  ]
  node [
    id 181
    label "harcerz"
  ]
  node [
    id 182
    label "ch&#322;opta&#347;"
  ]
  node [
    id 183
    label "zawodnik"
  ]
  node [
    id 184
    label "go&#322;ow&#261;s"
  ]
  node [
    id 185
    label "m&#322;ode"
  ]
  node [
    id 186
    label "stopie&#324;_harcerski"
  ]
  node [
    id 187
    label "g&#243;wniarz"
  ]
  node [
    id 188
    label "beniaminek"
  ]
  node [
    id 189
    label "istotka"
  ]
  node [
    id 190
    label "bech"
  ]
  node [
    id 191
    label "dziecinny"
  ]
  node [
    id 192
    label "naiwniak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
]
