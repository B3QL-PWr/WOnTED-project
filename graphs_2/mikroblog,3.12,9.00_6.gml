graph [
  node [
    id 0
    label "wirus"
    origin "text"
  ]
  node [
    id 1
    label "cz&#261;steczka"
  ]
  node [
    id 2
    label "trojan"
  ]
  node [
    id 3
    label "botnet"
  ]
  node [
    id 4
    label "prokariont"
  ]
  node [
    id 5
    label "program"
  ]
  node [
    id 6
    label "kr&#243;lik"
  ]
  node [
    id 7
    label "wirusy"
  ]
  node [
    id 8
    label "instalowa&#263;"
  ]
  node [
    id 9
    label "oprogramowanie"
  ]
  node [
    id 10
    label "odinstalowywa&#263;"
  ]
  node [
    id 11
    label "spis"
  ]
  node [
    id 12
    label "zaprezentowanie"
  ]
  node [
    id 13
    label "podprogram"
  ]
  node [
    id 14
    label "ogranicznik_referencyjny"
  ]
  node [
    id 15
    label "course_of_study"
  ]
  node [
    id 16
    label "booklet"
  ]
  node [
    id 17
    label "dzia&#322;"
  ]
  node [
    id 18
    label "odinstalowanie"
  ]
  node [
    id 19
    label "broszura"
  ]
  node [
    id 20
    label "wytw&#243;r"
  ]
  node [
    id 21
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 22
    label "kana&#322;"
  ]
  node [
    id 23
    label "teleferie"
  ]
  node [
    id 24
    label "zainstalowanie"
  ]
  node [
    id 25
    label "struktura_organizacyjna"
  ]
  node [
    id 26
    label "pirat"
  ]
  node [
    id 27
    label "zaprezentowa&#263;"
  ]
  node [
    id 28
    label "prezentowanie"
  ]
  node [
    id 29
    label "prezentowa&#263;"
  ]
  node [
    id 30
    label "interfejs"
  ]
  node [
    id 31
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 32
    label "okno"
  ]
  node [
    id 33
    label "blok"
  ]
  node [
    id 34
    label "punkt"
  ]
  node [
    id 35
    label "folder"
  ]
  node [
    id 36
    label "zainstalowa&#263;"
  ]
  node [
    id 37
    label "za&#322;o&#380;enie"
  ]
  node [
    id 38
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 39
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 40
    label "ram&#243;wka"
  ]
  node [
    id 41
    label "tryb"
  ]
  node [
    id 42
    label "emitowa&#263;"
  ]
  node [
    id 43
    label "emitowanie"
  ]
  node [
    id 44
    label "odinstalowywanie"
  ]
  node [
    id 45
    label "instrukcja"
  ]
  node [
    id 46
    label "informatyka"
  ]
  node [
    id 47
    label "deklaracja"
  ]
  node [
    id 48
    label "sekcja_krytyczna"
  ]
  node [
    id 49
    label "menu"
  ]
  node [
    id 50
    label "furkacja"
  ]
  node [
    id 51
    label "podstawa"
  ]
  node [
    id 52
    label "instalowanie"
  ]
  node [
    id 53
    label "oferta"
  ]
  node [
    id 54
    label "odinstalowa&#263;"
  ]
  node [
    id 55
    label "mikroorganizm"
  ]
  node [
    id 56
    label "prokarioty"
  ]
  node [
    id 57
    label "konfiguracja"
  ]
  node [
    id 58
    label "cz&#261;stka"
  ]
  node [
    id 59
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 60
    label "diadochia"
  ]
  node [
    id 61
    label "substancja"
  ]
  node [
    id 62
    label "grupa_funkcyjna"
  ]
  node [
    id 63
    label "mysikr&#243;lik"
  ]
  node [
    id 64
    label "futro"
  ]
  node [
    id 65
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 66
    label "tuszka"
  ]
  node [
    id 67
    label "zaj&#261;cowate"
  ]
  node [
    id 68
    label "kicaj"
  ]
  node [
    id 69
    label "comber"
  ]
  node [
    id 70
    label "omyk"
  ]
  node [
    id 71
    label "trusia"
  ]
  node [
    id 72
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 73
    label "mi&#281;so"
  ]
  node [
    id 74
    label "turzyca"
  ]
  node [
    id 75
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 76
    label "zbi&#243;r"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
]
