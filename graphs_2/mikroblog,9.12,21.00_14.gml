graph [
  node [
    id 0
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 1
    label "olx"
    origin "text"
  ]
  node [
    id 2
    label "wyrabia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nawet"
    origin "text"
  ]
  node [
    id 4
    label "ludzko&#347;&#263;"
  ]
  node [
    id 5
    label "asymilowanie"
  ]
  node [
    id 6
    label "wapniak"
  ]
  node [
    id 7
    label "asymilowa&#263;"
  ]
  node [
    id 8
    label "os&#322;abia&#263;"
  ]
  node [
    id 9
    label "posta&#263;"
  ]
  node [
    id 10
    label "hominid"
  ]
  node [
    id 11
    label "podw&#322;adny"
  ]
  node [
    id 12
    label "os&#322;abianie"
  ]
  node [
    id 13
    label "g&#322;owa"
  ]
  node [
    id 14
    label "figura"
  ]
  node [
    id 15
    label "portrecista"
  ]
  node [
    id 16
    label "dwun&#243;g"
  ]
  node [
    id 17
    label "profanum"
  ]
  node [
    id 18
    label "mikrokosmos"
  ]
  node [
    id 19
    label "nasada"
  ]
  node [
    id 20
    label "duch"
  ]
  node [
    id 21
    label "antropochoria"
  ]
  node [
    id 22
    label "osoba"
  ]
  node [
    id 23
    label "wz&#243;r"
  ]
  node [
    id 24
    label "senior"
  ]
  node [
    id 25
    label "oddzia&#322;ywanie"
  ]
  node [
    id 26
    label "Adam"
  ]
  node [
    id 27
    label "homo_sapiens"
  ]
  node [
    id 28
    label "polifag"
  ]
  node [
    id 29
    label "konsument"
  ]
  node [
    id 30
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 31
    label "cz&#322;owiekowate"
  ]
  node [
    id 32
    label "istota_&#380;ywa"
  ]
  node [
    id 33
    label "pracownik"
  ]
  node [
    id 34
    label "Chocho&#322;"
  ]
  node [
    id 35
    label "Herkules_Poirot"
  ]
  node [
    id 36
    label "Edyp"
  ]
  node [
    id 37
    label "parali&#380;owa&#263;"
  ]
  node [
    id 38
    label "Harry_Potter"
  ]
  node [
    id 39
    label "Casanova"
  ]
  node [
    id 40
    label "Zgredek"
  ]
  node [
    id 41
    label "Gargantua"
  ]
  node [
    id 42
    label "Winnetou"
  ]
  node [
    id 43
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 44
    label "Dulcynea"
  ]
  node [
    id 45
    label "kategoria_gramatyczna"
  ]
  node [
    id 46
    label "person"
  ]
  node [
    id 47
    label "Plastu&#347;"
  ]
  node [
    id 48
    label "Quasimodo"
  ]
  node [
    id 49
    label "Sherlock_Holmes"
  ]
  node [
    id 50
    label "Faust"
  ]
  node [
    id 51
    label "Wallenrod"
  ]
  node [
    id 52
    label "Dwukwiat"
  ]
  node [
    id 53
    label "Don_Juan"
  ]
  node [
    id 54
    label "koniugacja"
  ]
  node [
    id 55
    label "Don_Kiszot"
  ]
  node [
    id 56
    label "Hamlet"
  ]
  node [
    id 57
    label "Werter"
  ]
  node [
    id 58
    label "istota"
  ]
  node [
    id 59
    label "Szwejk"
  ]
  node [
    id 60
    label "doros&#322;y"
  ]
  node [
    id 61
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 62
    label "jajko"
  ]
  node [
    id 63
    label "rodzic"
  ]
  node [
    id 64
    label "wapniaki"
  ]
  node [
    id 65
    label "zwierzchnik"
  ]
  node [
    id 66
    label "feuda&#322;"
  ]
  node [
    id 67
    label "starzec"
  ]
  node [
    id 68
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 69
    label "zawodnik"
  ]
  node [
    id 70
    label "komendancja"
  ]
  node [
    id 71
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 72
    label "asymilowanie_si&#281;"
  ]
  node [
    id 73
    label "absorption"
  ]
  node [
    id 74
    label "pobieranie"
  ]
  node [
    id 75
    label "czerpanie"
  ]
  node [
    id 76
    label "acquisition"
  ]
  node [
    id 77
    label "zmienianie"
  ]
  node [
    id 78
    label "organizm"
  ]
  node [
    id 79
    label "assimilation"
  ]
  node [
    id 80
    label "upodabnianie"
  ]
  node [
    id 81
    label "g&#322;oska"
  ]
  node [
    id 82
    label "kultura"
  ]
  node [
    id 83
    label "podobny"
  ]
  node [
    id 84
    label "grupa"
  ]
  node [
    id 85
    label "fonetyka"
  ]
  node [
    id 86
    label "suppress"
  ]
  node [
    id 87
    label "robi&#263;"
  ]
  node [
    id 88
    label "os&#322;abienie"
  ]
  node [
    id 89
    label "kondycja_fizyczna"
  ]
  node [
    id 90
    label "os&#322;abi&#263;"
  ]
  node [
    id 91
    label "zdrowie"
  ]
  node [
    id 92
    label "powodowa&#263;"
  ]
  node [
    id 93
    label "zmniejsza&#263;"
  ]
  node [
    id 94
    label "bate"
  ]
  node [
    id 95
    label "de-escalation"
  ]
  node [
    id 96
    label "powodowanie"
  ]
  node [
    id 97
    label "debilitation"
  ]
  node [
    id 98
    label "zmniejszanie"
  ]
  node [
    id 99
    label "s&#322;abszy"
  ]
  node [
    id 100
    label "pogarszanie"
  ]
  node [
    id 101
    label "assimilate"
  ]
  node [
    id 102
    label "dostosowywa&#263;"
  ]
  node [
    id 103
    label "dostosowa&#263;"
  ]
  node [
    id 104
    label "przejmowa&#263;"
  ]
  node [
    id 105
    label "upodobni&#263;"
  ]
  node [
    id 106
    label "przej&#261;&#263;"
  ]
  node [
    id 107
    label "upodabnia&#263;"
  ]
  node [
    id 108
    label "pobiera&#263;"
  ]
  node [
    id 109
    label "pobra&#263;"
  ]
  node [
    id 110
    label "zapis"
  ]
  node [
    id 111
    label "figure"
  ]
  node [
    id 112
    label "typ"
  ]
  node [
    id 113
    label "spos&#243;b"
  ]
  node [
    id 114
    label "mildew"
  ]
  node [
    id 115
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 116
    label "ideal"
  ]
  node [
    id 117
    label "rule"
  ]
  node [
    id 118
    label "ruch"
  ]
  node [
    id 119
    label "dekal"
  ]
  node [
    id 120
    label "motyw"
  ]
  node [
    id 121
    label "projekt"
  ]
  node [
    id 122
    label "charakterystyka"
  ]
  node [
    id 123
    label "zaistnie&#263;"
  ]
  node [
    id 124
    label "cecha"
  ]
  node [
    id 125
    label "Osjan"
  ]
  node [
    id 126
    label "kto&#347;"
  ]
  node [
    id 127
    label "wygl&#261;d"
  ]
  node [
    id 128
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 129
    label "osobowo&#347;&#263;"
  ]
  node [
    id 130
    label "wytw&#243;r"
  ]
  node [
    id 131
    label "trim"
  ]
  node [
    id 132
    label "poby&#263;"
  ]
  node [
    id 133
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 134
    label "Aspazja"
  ]
  node [
    id 135
    label "punkt_widzenia"
  ]
  node [
    id 136
    label "kompleksja"
  ]
  node [
    id 137
    label "wytrzyma&#263;"
  ]
  node [
    id 138
    label "budowa"
  ]
  node [
    id 139
    label "formacja"
  ]
  node [
    id 140
    label "pozosta&#263;"
  ]
  node [
    id 141
    label "point"
  ]
  node [
    id 142
    label "przedstawienie"
  ]
  node [
    id 143
    label "go&#347;&#263;"
  ]
  node [
    id 144
    label "fotograf"
  ]
  node [
    id 145
    label "malarz"
  ]
  node [
    id 146
    label "artysta"
  ]
  node [
    id 147
    label "hipnotyzowanie"
  ]
  node [
    id 148
    label "&#347;lad"
  ]
  node [
    id 149
    label "docieranie"
  ]
  node [
    id 150
    label "natural_process"
  ]
  node [
    id 151
    label "reakcja_chemiczna"
  ]
  node [
    id 152
    label "wdzieranie_si&#281;"
  ]
  node [
    id 153
    label "zjawisko"
  ]
  node [
    id 154
    label "act"
  ]
  node [
    id 155
    label "rezultat"
  ]
  node [
    id 156
    label "lobbysta"
  ]
  node [
    id 157
    label "pryncypa&#322;"
  ]
  node [
    id 158
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 159
    label "kszta&#322;t"
  ]
  node [
    id 160
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 161
    label "wiedza"
  ]
  node [
    id 162
    label "kierowa&#263;"
  ]
  node [
    id 163
    label "alkohol"
  ]
  node [
    id 164
    label "zdolno&#347;&#263;"
  ]
  node [
    id 165
    label "&#380;ycie"
  ]
  node [
    id 166
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 167
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 168
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 169
    label "sztuka"
  ]
  node [
    id 170
    label "dekiel"
  ]
  node [
    id 171
    label "ro&#347;lina"
  ]
  node [
    id 172
    label "&#347;ci&#281;cie"
  ]
  node [
    id 173
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 174
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 175
    label "&#347;ci&#281;gno"
  ]
  node [
    id 176
    label "noosfera"
  ]
  node [
    id 177
    label "byd&#322;o"
  ]
  node [
    id 178
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 179
    label "makrocefalia"
  ]
  node [
    id 180
    label "obiekt"
  ]
  node [
    id 181
    label "ucho"
  ]
  node [
    id 182
    label "g&#243;ra"
  ]
  node [
    id 183
    label "m&#243;zg"
  ]
  node [
    id 184
    label "kierownictwo"
  ]
  node [
    id 185
    label "fryzura"
  ]
  node [
    id 186
    label "umys&#322;"
  ]
  node [
    id 187
    label "cia&#322;o"
  ]
  node [
    id 188
    label "cz&#322;onek"
  ]
  node [
    id 189
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 190
    label "czaszka"
  ]
  node [
    id 191
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 192
    label "allochoria"
  ]
  node [
    id 193
    label "p&#322;aszczyzna"
  ]
  node [
    id 194
    label "przedmiot"
  ]
  node [
    id 195
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 196
    label "bierka_szachowa"
  ]
  node [
    id 197
    label "obiekt_matematyczny"
  ]
  node [
    id 198
    label "gestaltyzm"
  ]
  node [
    id 199
    label "styl"
  ]
  node [
    id 200
    label "obraz"
  ]
  node [
    id 201
    label "rzecz"
  ]
  node [
    id 202
    label "d&#378;wi&#281;k"
  ]
  node [
    id 203
    label "character"
  ]
  node [
    id 204
    label "rze&#378;ba"
  ]
  node [
    id 205
    label "stylistyka"
  ]
  node [
    id 206
    label "miejsce"
  ]
  node [
    id 207
    label "antycypacja"
  ]
  node [
    id 208
    label "ornamentyka"
  ]
  node [
    id 209
    label "informacja"
  ]
  node [
    id 210
    label "facet"
  ]
  node [
    id 211
    label "popis"
  ]
  node [
    id 212
    label "wiersz"
  ]
  node [
    id 213
    label "symetria"
  ]
  node [
    id 214
    label "lingwistyka_kognitywna"
  ]
  node [
    id 215
    label "karta"
  ]
  node [
    id 216
    label "shape"
  ]
  node [
    id 217
    label "podzbi&#243;r"
  ]
  node [
    id 218
    label "perspektywa"
  ]
  node [
    id 219
    label "dziedzina"
  ]
  node [
    id 220
    label "nak&#322;adka"
  ]
  node [
    id 221
    label "li&#347;&#263;"
  ]
  node [
    id 222
    label "jama_gard&#322;owa"
  ]
  node [
    id 223
    label "rezonator"
  ]
  node [
    id 224
    label "podstawa"
  ]
  node [
    id 225
    label "base"
  ]
  node [
    id 226
    label "piek&#322;o"
  ]
  node [
    id 227
    label "human_body"
  ]
  node [
    id 228
    label "ofiarowywanie"
  ]
  node [
    id 229
    label "sfera_afektywna"
  ]
  node [
    id 230
    label "nekromancja"
  ]
  node [
    id 231
    label "Po&#347;wist"
  ]
  node [
    id 232
    label "podekscytowanie"
  ]
  node [
    id 233
    label "deformowanie"
  ]
  node [
    id 234
    label "sumienie"
  ]
  node [
    id 235
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 236
    label "deformowa&#263;"
  ]
  node [
    id 237
    label "psychika"
  ]
  node [
    id 238
    label "zjawa"
  ]
  node [
    id 239
    label "zmar&#322;y"
  ]
  node [
    id 240
    label "istota_nadprzyrodzona"
  ]
  node [
    id 241
    label "power"
  ]
  node [
    id 242
    label "entity"
  ]
  node [
    id 243
    label "ofiarowywa&#263;"
  ]
  node [
    id 244
    label "oddech"
  ]
  node [
    id 245
    label "seksualno&#347;&#263;"
  ]
  node [
    id 246
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 247
    label "byt"
  ]
  node [
    id 248
    label "si&#322;a"
  ]
  node [
    id 249
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 250
    label "ego"
  ]
  node [
    id 251
    label "ofiarowanie"
  ]
  node [
    id 252
    label "charakter"
  ]
  node [
    id 253
    label "fizjonomia"
  ]
  node [
    id 254
    label "kompleks"
  ]
  node [
    id 255
    label "zapalno&#347;&#263;"
  ]
  node [
    id 256
    label "T&#281;sknica"
  ]
  node [
    id 257
    label "ofiarowa&#263;"
  ]
  node [
    id 258
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 259
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 260
    label "passion"
  ]
  node [
    id 261
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 262
    label "atom"
  ]
  node [
    id 263
    label "odbicie"
  ]
  node [
    id 264
    label "przyroda"
  ]
  node [
    id 265
    label "Ziemia"
  ]
  node [
    id 266
    label "kosmos"
  ]
  node [
    id 267
    label "miniatura"
  ]
  node [
    id 268
    label "ugniata&#263;"
  ]
  node [
    id 269
    label "wywi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 270
    label "rozwija&#263;"
  ]
  node [
    id 271
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 272
    label "wytrzymywa&#263;"
  ]
  node [
    id 273
    label "spe&#322;nia&#263;"
  ]
  node [
    id 274
    label "train"
  ]
  node [
    id 275
    label "produkowa&#263;"
  ]
  node [
    id 276
    label "doskonali&#263;"
  ]
  node [
    id 277
    label "wykonywa&#263;"
  ]
  node [
    id 278
    label "osi&#261;ga&#263;"
  ]
  node [
    id 279
    label "get"
  ]
  node [
    id 280
    label "zdobywa&#263;"
  ]
  node [
    id 281
    label "przepracowywa&#263;"
  ]
  node [
    id 282
    label "wypracowywa&#263;"
  ]
  node [
    id 283
    label "organizowa&#263;"
  ]
  node [
    id 284
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 285
    label "czyni&#263;"
  ]
  node [
    id 286
    label "give"
  ]
  node [
    id 287
    label "stylizowa&#263;"
  ]
  node [
    id 288
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 289
    label "falowa&#263;"
  ]
  node [
    id 290
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 291
    label "peddle"
  ]
  node [
    id 292
    label "praca"
  ]
  node [
    id 293
    label "wydala&#263;"
  ]
  node [
    id 294
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 295
    label "tentegowa&#263;"
  ]
  node [
    id 296
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 297
    label "urz&#261;dza&#263;"
  ]
  node [
    id 298
    label "oszukiwa&#263;"
  ]
  node [
    id 299
    label "work"
  ]
  node [
    id 300
    label "ukazywa&#263;"
  ]
  node [
    id 301
    label "przerabia&#263;"
  ]
  node [
    id 302
    label "post&#281;powa&#263;"
  ]
  node [
    id 303
    label "create"
  ]
  node [
    id 304
    label "dostarcza&#263;"
  ]
  node [
    id 305
    label "tworzy&#263;"
  ]
  node [
    id 306
    label "wytwarza&#263;"
  ]
  node [
    id 307
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 308
    label "ubija&#263;"
  ]
  node [
    id 309
    label "press"
  ]
  node [
    id 310
    label "muzyka"
  ]
  node [
    id 311
    label "rola"
  ]
  node [
    id 312
    label "niewoli&#263;"
  ]
  node [
    id 313
    label "uzyskiwa&#263;"
  ]
  node [
    id 314
    label "tease"
  ]
  node [
    id 315
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 316
    label "dostawa&#263;"
  ]
  node [
    id 317
    label "have"
  ]
  node [
    id 318
    label "raise"
  ]
  node [
    id 319
    label "pozostawa&#263;"
  ]
  node [
    id 320
    label "digest"
  ]
  node [
    id 321
    label "zmusza&#263;"
  ]
  node [
    id 322
    label "stay"
  ]
  node [
    id 323
    label "elaborate"
  ]
  node [
    id 324
    label "use"
  ]
  node [
    id 325
    label "overwork"
  ]
  node [
    id 326
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 327
    label "sp&#281;dza&#263;"
  ]
  node [
    id 328
    label "pracowa&#263;"
  ]
  node [
    id 329
    label "radzi&#263;_sobie"
  ]
  node [
    id 330
    label "urzeczywistnia&#263;"
  ]
  node [
    id 331
    label "close"
  ]
  node [
    id 332
    label "perform"
  ]
  node [
    id 333
    label "omawia&#263;"
  ]
  node [
    id 334
    label "puszcza&#263;"
  ]
  node [
    id 335
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 336
    label "stawia&#263;"
  ]
  node [
    id 337
    label "rozpakowywa&#263;"
  ]
  node [
    id 338
    label "rozstawia&#263;"
  ]
  node [
    id 339
    label "dopowiada&#263;"
  ]
  node [
    id 340
    label "inflate"
  ]
  node [
    id 341
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 342
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 343
    label "hone"
  ]
  node [
    id 344
    label "ulepsza&#263;"
  ]
  node [
    id 345
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 346
    label "dociera&#263;"
  ]
  node [
    id 347
    label "mark"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
]
