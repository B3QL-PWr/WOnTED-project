graph [
  node [
    id 0
    label "okres"
    origin "text"
  ]
  node [
    id 1
    label "powstanie"
    origin "text"
  ]
  node [
    id 2
    label "cmentarz"
    origin "text"
  ]
  node [
    id 3
    label "koniec"
    origin "text"
  ]
  node [
    id 4
    label "wojna"
    origin "text"
  ]
  node [
    id 5
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 6
    label "pochowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "nim"
    origin "text"
  ]
  node [
    id 8
    label "&#380;o&#322;nierz"
    origin "text"
  ]
  node [
    id 9
    label "polski"
    origin "text"
  ]
  node [
    id 10
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 11
    label "podziemny"
    origin "text"
  ]
  node [
    id 12
    label "niemcy"
    origin "text"
  ]
  node [
    id 13
    label "nigdy"
    origin "text"
  ]
  node [
    id 14
    label "sprofanowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "le&#347;ny"
    origin "text"
  ]
  node [
    id 16
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 17
    label "pewno"
    origin "text"
  ]
  node [
    id 18
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 19
    label "by&#263;"
    origin "text"
  ]
  node [
    id 20
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 21
    label "obawia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "si&#281;"
    origin "text"
  ]
  node [
    id 23
    label "partyzant"
    origin "text"
  ]
  node [
    id 24
    label "uszanowa&#263;by"
    origin "text"
  ]
  node [
    id 25
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 26
    label "pobliski"
    origin "text"
  ]
  node [
    id 27
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 28
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 29
    label "rocznik"
    origin "text"
  ]
  node [
    id 30
    label "bitwa"
    origin "text"
  ]
  node [
    id 31
    label "pod"
    origin "text"
  ]
  node [
    id 32
    label "barak"
    origin "text"
  ]
  node [
    id 33
    label "swoje"
    origin "text"
  ]
  node [
    id 34
    label "okres_amazo&#324;ski"
  ]
  node [
    id 35
    label "stater"
  ]
  node [
    id 36
    label "flow"
  ]
  node [
    id 37
    label "choroba_przyrodzona"
  ]
  node [
    id 38
    label "postglacja&#322;"
  ]
  node [
    id 39
    label "sylur"
  ]
  node [
    id 40
    label "kreda"
  ]
  node [
    id 41
    label "ordowik"
  ]
  node [
    id 42
    label "okres_hesperyjski"
  ]
  node [
    id 43
    label "paleogen"
  ]
  node [
    id 44
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 45
    label "okres_halsztacki"
  ]
  node [
    id 46
    label "riak"
  ]
  node [
    id 47
    label "czwartorz&#281;d"
  ]
  node [
    id 48
    label "podokres"
  ]
  node [
    id 49
    label "trzeciorz&#281;d"
  ]
  node [
    id 50
    label "kalim"
  ]
  node [
    id 51
    label "fala"
  ]
  node [
    id 52
    label "perm"
  ]
  node [
    id 53
    label "retoryka"
  ]
  node [
    id 54
    label "prekambr"
  ]
  node [
    id 55
    label "faza"
  ]
  node [
    id 56
    label "neogen"
  ]
  node [
    id 57
    label "pulsacja"
  ]
  node [
    id 58
    label "proces_fizjologiczny"
  ]
  node [
    id 59
    label "kambr"
  ]
  node [
    id 60
    label "dzieje"
  ]
  node [
    id 61
    label "kriogen"
  ]
  node [
    id 62
    label "jednostka_geologiczna"
  ]
  node [
    id 63
    label "time_period"
  ]
  node [
    id 64
    label "period"
  ]
  node [
    id 65
    label "ton"
  ]
  node [
    id 66
    label "orosir"
  ]
  node [
    id 67
    label "okres_czasu"
  ]
  node [
    id 68
    label "poprzednik"
  ]
  node [
    id 69
    label "spell"
  ]
  node [
    id 70
    label "interstadia&#322;"
  ]
  node [
    id 71
    label "ektas"
  ]
  node [
    id 72
    label "sider"
  ]
  node [
    id 73
    label "epoka"
  ]
  node [
    id 74
    label "rok_akademicki"
  ]
  node [
    id 75
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 76
    label "schy&#322;ek"
  ]
  node [
    id 77
    label "cykl"
  ]
  node [
    id 78
    label "ciota"
  ]
  node [
    id 79
    label "pierwszorz&#281;d"
  ]
  node [
    id 80
    label "okres_noachijski"
  ]
  node [
    id 81
    label "czas"
  ]
  node [
    id 82
    label "ediakar"
  ]
  node [
    id 83
    label "zdanie"
  ]
  node [
    id 84
    label "nast&#281;pnik"
  ]
  node [
    id 85
    label "condition"
  ]
  node [
    id 86
    label "jura"
  ]
  node [
    id 87
    label "glacja&#322;"
  ]
  node [
    id 88
    label "sten"
  ]
  node [
    id 89
    label "Zeitgeist"
  ]
  node [
    id 90
    label "era"
  ]
  node [
    id 91
    label "trias"
  ]
  node [
    id 92
    label "p&#243;&#322;okres"
  ]
  node [
    id 93
    label "rok_szkolny"
  ]
  node [
    id 94
    label "dewon"
  ]
  node [
    id 95
    label "karbon"
  ]
  node [
    id 96
    label "izochronizm"
  ]
  node [
    id 97
    label "preglacja&#322;"
  ]
  node [
    id 98
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 99
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 100
    label "drugorz&#281;d"
  ]
  node [
    id 101
    label "semester"
  ]
  node [
    id 102
    label "zniewie&#347;cialec"
  ]
  node [
    id 103
    label "miesi&#261;czka"
  ]
  node [
    id 104
    label "oferma"
  ]
  node [
    id 105
    label "gej"
  ]
  node [
    id 106
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 107
    label "pedalstwo"
  ]
  node [
    id 108
    label "mazgaj"
  ]
  node [
    id 109
    label "poprzedzanie"
  ]
  node [
    id 110
    label "czasoprzestrze&#324;"
  ]
  node [
    id 111
    label "laba"
  ]
  node [
    id 112
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 113
    label "chronometria"
  ]
  node [
    id 114
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 115
    label "rachuba_czasu"
  ]
  node [
    id 116
    label "przep&#322;ywanie"
  ]
  node [
    id 117
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 118
    label "czasokres"
  ]
  node [
    id 119
    label "odczyt"
  ]
  node [
    id 120
    label "chwila"
  ]
  node [
    id 121
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 122
    label "kategoria_gramatyczna"
  ]
  node [
    id 123
    label "poprzedzenie"
  ]
  node [
    id 124
    label "trawienie"
  ]
  node [
    id 125
    label "pochodzi&#263;"
  ]
  node [
    id 126
    label "poprzedza&#263;"
  ]
  node [
    id 127
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 128
    label "odwlekanie_si&#281;"
  ]
  node [
    id 129
    label "zegar"
  ]
  node [
    id 130
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 131
    label "czwarty_wymiar"
  ]
  node [
    id 132
    label "pochodzenie"
  ]
  node [
    id 133
    label "koniugacja"
  ]
  node [
    id 134
    label "trawi&#263;"
  ]
  node [
    id 135
    label "pogoda"
  ]
  node [
    id 136
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 137
    label "poprzedzi&#263;"
  ]
  node [
    id 138
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 139
    label "szko&#322;a"
  ]
  node [
    id 140
    label "fraza"
  ]
  node [
    id 141
    label "przekazanie"
  ]
  node [
    id 142
    label "stanowisko"
  ]
  node [
    id 143
    label "wypowiedzenie"
  ]
  node [
    id 144
    label "prison_term"
  ]
  node [
    id 145
    label "system"
  ]
  node [
    id 146
    label "przedstawienie"
  ]
  node [
    id 147
    label "wyra&#380;enie"
  ]
  node [
    id 148
    label "zaliczenie"
  ]
  node [
    id 149
    label "antylogizm"
  ]
  node [
    id 150
    label "zmuszenie"
  ]
  node [
    id 151
    label "konektyw"
  ]
  node [
    id 152
    label "attitude"
  ]
  node [
    id 153
    label "powierzenie"
  ]
  node [
    id 154
    label "adjudication"
  ]
  node [
    id 155
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 156
    label "pass"
  ]
  node [
    id 157
    label "kres"
  ]
  node [
    id 158
    label "aalen"
  ]
  node [
    id 159
    label "jura_wczesna"
  ]
  node [
    id 160
    label "holocen"
  ]
  node [
    id 161
    label "pliocen"
  ]
  node [
    id 162
    label "plejstocen"
  ]
  node [
    id 163
    label "paleocen"
  ]
  node [
    id 164
    label "bajos"
  ]
  node [
    id 165
    label "kelowej"
  ]
  node [
    id 166
    label "eocen"
  ]
  node [
    id 167
    label "miocen"
  ]
  node [
    id 168
    label "&#347;rodkowy_trias"
  ]
  node [
    id 169
    label "term"
  ]
  node [
    id 170
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 171
    label "wczesny_trias"
  ]
  node [
    id 172
    label "jura_&#347;rodkowa"
  ]
  node [
    id 173
    label "oligocen"
  ]
  node [
    id 174
    label "implikacja"
  ]
  node [
    id 175
    label "rzecz"
  ]
  node [
    id 176
    label "cz&#322;owiek"
  ]
  node [
    id 177
    label "argument"
  ]
  node [
    id 178
    label "kszta&#322;t"
  ]
  node [
    id 179
    label "pasemko"
  ]
  node [
    id 180
    label "znak_diakrytyczny"
  ]
  node [
    id 181
    label "zjawisko"
  ]
  node [
    id 182
    label "zafalowanie"
  ]
  node [
    id 183
    label "kot"
  ]
  node [
    id 184
    label "przemoc"
  ]
  node [
    id 185
    label "reakcja"
  ]
  node [
    id 186
    label "strumie&#324;"
  ]
  node [
    id 187
    label "karb"
  ]
  node [
    id 188
    label "mn&#243;stwo"
  ]
  node [
    id 189
    label "fit"
  ]
  node [
    id 190
    label "grzywa_fali"
  ]
  node [
    id 191
    label "woda"
  ]
  node [
    id 192
    label "efekt_Dopplera"
  ]
  node [
    id 193
    label "obcinka"
  ]
  node [
    id 194
    label "t&#322;um"
  ]
  node [
    id 195
    label "stream"
  ]
  node [
    id 196
    label "zafalowa&#263;"
  ]
  node [
    id 197
    label "rozbicie_si&#281;"
  ]
  node [
    id 198
    label "wojsko"
  ]
  node [
    id 199
    label "clutter"
  ]
  node [
    id 200
    label "rozbijanie_si&#281;"
  ]
  node [
    id 201
    label "czo&#322;o_fali"
  ]
  node [
    id 202
    label "cykl_astronomiczny"
  ]
  node [
    id 203
    label "coil"
  ]
  node [
    id 204
    label "fotoelement"
  ]
  node [
    id 205
    label "komutowanie"
  ]
  node [
    id 206
    label "stan_skupienia"
  ]
  node [
    id 207
    label "nastr&#243;j"
  ]
  node [
    id 208
    label "przerywacz"
  ]
  node [
    id 209
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 210
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 211
    label "kraw&#281;d&#378;"
  ]
  node [
    id 212
    label "obsesja"
  ]
  node [
    id 213
    label "dw&#243;jnik"
  ]
  node [
    id 214
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 215
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 216
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 217
    label "przew&#243;d"
  ]
  node [
    id 218
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 219
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 220
    label "obw&#243;d"
  ]
  node [
    id 221
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 222
    label "degree"
  ]
  node [
    id 223
    label "komutowa&#263;"
  ]
  node [
    id 224
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 225
    label "cecha"
  ]
  node [
    id 226
    label "serce"
  ]
  node [
    id 227
    label "ripple"
  ]
  node [
    id 228
    label "pracowanie"
  ]
  node [
    id 229
    label "zabicie"
  ]
  node [
    id 230
    label "set"
  ]
  node [
    id 231
    label "przebieg"
  ]
  node [
    id 232
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 233
    label "owulacja"
  ]
  node [
    id 234
    label "sekwencja"
  ]
  node [
    id 235
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 236
    label "edycja"
  ]
  node [
    id 237
    label "cycle"
  ]
  node [
    id 238
    label "charakter"
  ]
  node [
    id 239
    label "nauka_humanistyczna"
  ]
  node [
    id 240
    label "erystyka"
  ]
  node [
    id 241
    label "chironomia"
  ]
  node [
    id 242
    label "elokwencja"
  ]
  node [
    id 243
    label "sztuka"
  ]
  node [
    id 244
    label "elokucja"
  ]
  node [
    id 245
    label "tropika"
  ]
  node [
    id 246
    label "formacja_geologiczna"
  ]
  node [
    id 247
    label "era_paleozoiczna"
  ]
  node [
    id 248
    label "ludlow"
  ]
  node [
    id 249
    label "moneta"
  ]
  node [
    id 250
    label "paleoproterozoik"
  ]
  node [
    id 251
    label "zlodowacenie"
  ]
  node [
    id 252
    label "asteroksylon"
  ]
  node [
    id 253
    label "pluwia&#322;"
  ]
  node [
    id 254
    label "mezoproterozoik"
  ]
  node [
    id 255
    label "era_kenozoiczna"
  ]
  node [
    id 256
    label "p&#243;&#378;ny_trias"
  ]
  node [
    id 257
    label "ret"
  ]
  node [
    id 258
    label "era_mezozoiczna"
  ]
  node [
    id 259
    label "konodont"
  ]
  node [
    id 260
    label "kajper"
  ]
  node [
    id 261
    label "neoproterozoik"
  ]
  node [
    id 262
    label "chalk"
  ]
  node [
    id 263
    label "narz&#281;dzie"
  ]
  node [
    id 264
    label "santon"
  ]
  node [
    id 265
    label "cenoman"
  ]
  node [
    id 266
    label "neokom"
  ]
  node [
    id 267
    label "apt"
  ]
  node [
    id 268
    label "pobia&#322;ka"
  ]
  node [
    id 269
    label "ska&#322;a_organogeniczna"
  ]
  node [
    id 270
    label "alb"
  ]
  node [
    id 271
    label "pastel"
  ]
  node [
    id 272
    label "turon"
  ]
  node [
    id 273
    label "pteranodon"
  ]
  node [
    id 274
    label "wieloton"
  ]
  node [
    id 275
    label "tu&#324;czyk"
  ]
  node [
    id 276
    label "d&#378;wi&#281;k"
  ]
  node [
    id 277
    label "zabarwienie"
  ]
  node [
    id 278
    label "interwa&#322;"
  ]
  node [
    id 279
    label "modalizm"
  ]
  node [
    id 280
    label "ubarwienie"
  ]
  node [
    id 281
    label "note"
  ]
  node [
    id 282
    label "formality"
  ]
  node [
    id 283
    label "glinka"
  ]
  node [
    id 284
    label "jednostka"
  ]
  node [
    id 285
    label "sound"
  ]
  node [
    id 286
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 287
    label "zwyczaj"
  ]
  node [
    id 288
    label "solmizacja"
  ]
  node [
    id 289
    label "seria"
  ]
  node [
    id 290
    label "tone"
  ]
  node [
    id 291
    label "kolorystyka"
  ]
  node [
    id 292
    label "r&#243;&#380;nica"
  ]
  node [
    id 293
    label "akcent"
  ]
  node [
    id 294
    label "repetycja"
  ]
  node [
    id 295
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 296
    label "heksachord"
  ]
  node [
    id 297
    label "rejestr"
  ]
  node [
    id 298
    label "pistolet_maszynowy"
  ]
  node [
    id 299
    label "jednostka_si&#322;y"
  ]
  node [
    id 300
    label "pozauk&#322;adowa_jednostka_miary"
  ]
  node [
    id 301
    label "wapie&#324;_pi&#324;czowski"
  ]
  node [
    id 302
    label "pensylwan"
  ]
  node [
    id 303
    label "tworzywo"
  ]
  node [
    id 304
    label "mezozaur"
  ]
  node [
    id 305
    label "pikaia"
  ]
  node [
    id 306
    label "era_eozoiczna"
  ]
  node [
    id 307
    label "era_archaiczna"
  ]
  node [
    id 308
    label "rand"
  ]
  node [
    id 309
    label "huron"
  ]
  node [
    id 310
    label "Permian"
  ]
  node [
    id 311
    label "blokada"
  ]
  node [
    id 312
    label "cechsztyn"
  ]
  node [
    id 313
    label "dogger"
  ]
  node [
    id 314
    label "plezjozaur"
  ]
  node [
    id 315
    label "euoplocefal"
  ]
  node [
    id 316
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 317
    label "eon"
  ]
  node [
    id 318
    label "stworzenie"
  ]
  node [
    id 319
    label "walka"
  ]
  node [
    id 320
    label "koliszczyzna"
  ]
  node [
    id 321
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 322
    label "Ko&#347;ciuszko"
  ]
  node [
    id 323
    label "powstanie_tambowskie"
  ]
  node [
    id 324
    label "odbudowanie_si&#281;"
  ]
  node [
    id 325
    label "powstanie_listopadowe"
  ]
  node [
    id 326
    label "origin"
  ]
  node [
    id 327
    label "&#380;akieria"
  ]
  node [
    id 328
    label "zaistnienie"
  ]
  node [
    id 329
    label "potworzenie_si&#281;"
  ]
  node [
    id 330
    label "geneza"
  ]
  node [
    id 331
    label "orgy"
  ]
  node [
    id 332
    label "beginning"
  ]
  node [
    id 333
    label "utworzenie"
  ]
  node [
    id 334
    label "le&#380;enie"
  ]
  node [
    id 335
    label "kl&#281;czenie"
  ]
  node [
    id 336
    label "chmielnicczyzna"
  ]
  node [
    id 337
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 338
    label "pierwocina"
  ]
  node [
    id 339
    label "powstanie_warszawskie"
  ]
  node [
    id 340
    label "uniesienie_si&#281;"
  ]
  node [
    id 341
    label "siedzenie"
  ]
  node [
    id 342
    label "wydarzenie"
  ]
  node [
    id 343
    label "obrona"
  ]
  node [
    id 344
    label "zaatakowanie"
  ]
  node [
    id 345
    label "konfrontacyjny"
  ]
  node [
    id 346
    label "contest"
  ]
  node [
    id 347
    label "action"
  ]
  node [
    id 348
    label "sambo"
  ]
  node [
    id 349
    label "czyn"
  ]
  node [
    id 350
    label "rywalizacja"
  ]
  node [
    id 351
    label "trudno&#347;&#263;"
  ]
  node [
    id 352
    label "sp&#243;r"
  ]
  node [
    id 353
    label "wrestle"
  ]
  node [
    id 354
    label "military_action"
  ]
  node [
    id 355
    label "wojna_stuletnia"
  ]
  node [
    id 356
    label "trwanie"
  ]
  node [
    id 357
    label "wstanie"
  ]
  node [
    id 358
    label "po&#322;o&#380;enie"
  ]
  node [
    id 359
    label "bycie"
  ]
  node [
    id 360
    label "pobyczenie_si&#281;"
  ]
  node [
    id 361
    label "tarzanie_si&#281;"
  ]
  node [
    id 362
    label "zwierz&#281;"
  ]
  node [
    id 363
    label "przele&#380;enie"
  ]
  node [
    id 364
    label "odpowiedni"
  ]
  node [
    id 365
    label "zlegni&#281;cie"
  ]
  node [
    id 366
    label "spoczywanie"
  ]
  node [
    id 367
    label "pole&#380;enie"
  ]
  node [
    id 368
    label "odsiedzenie"
  ]
  node [
    id 369
    label "wysiadywanie"
  ]
  node [
    id 370
    label "przedmiot"
  ]
  node [
    id 371
    label "odsiadywanie"
  ]
  node [
    id 372
    label "otoczenie_si&#281;"
  ]
  node [
    id 373
    label "posiedzenie"
  ]
  node [
    id 374
    label "wysiedzenie"
  ]
  node [
    id 375
    label "posadzenie"
  ]
  node [
    id 376
    label "wychodzenie"
  ]
  node [
    id 377
    label "zajmowanie_si&#281;"
  ]
  node [
    id 378
    label "tkwienie"
  ]
  node [
    id 379
    label "sadzanie"
  ]
  node [
    id 380
    label "trybuna"
  ]
  node [
    id 381
    label "ocieranie_si&#281;"
  ]
  node [
    id 382
    label "room"
  ]
  node [
    id 383
    label "jadalnia"
  ]
  node [
    id 384
    label "miejsce"
  ]
  node [
    id 385
    label "residency"
  ]
  node [
    id 386
    label "pupa"
  ]
  node [
    id 387
    label "samolot"
  ]
  node [
    id 388
    label "touch"
  ]
  node [
    id 389
    label "otarcie_si&#281;"
  ]
  node [
    id 390
    label "position"
  ]
  node [
    id 391
    label "otaczanie_si&#281;"
  ]
  node [
    id 392
    label "wyj&#347;cie"
  ]
  node [
    id 393
    label "przedzia&#322;"
  ]
  node [
    id 394
    label "umieszczenie"
  ]
  node [
    id 395
    label "mieszkanie"
  ]
  node [
    id 396
    label "przebywanie"
  ]
  node [
    id 397
    label "ujmowanie"
  ]
  node [
    id 398
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 399
    label "autobus"
  ]
  node [
    id 400
    label "foundation"
  ]
  node [
    id 401
    label "potworzenie"
  ]
  node [
    id 402
    label "erecting"
  ]
  node [
    id 403
    label "ukszta&#322;towanie"
  ]
  node [
    id 404
    label "stanie_si&#281;"
  ]
  node [
    id 405
    label "zorganizowanie"
  ]
  node [
    id 406
    label "zrobienie"
  ]
  node [
    id 407
    label "obiekt_naturalny"
  ]
  node [
    id 408
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 409
    label "environment"
  ]
  node [
    id 410
    label "ekosystem"
  ]
  node [
    id 411
    label "organizm"
  ]
  node [
    id 412
    label "pope&#322;nienie"
  ]
  node [
    id 413
    label "wizerunek"
  ]
  node [
    id 414
    label "wszechstworzenie"
  ]
  node [
    id 415
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 416
    label "czynno&#347;&#263;"
  ]
  node [
    id 417
    label "teren"
  ]
  node [
    id 418
    label "mikrokosmos"
  ]
  node [
    id 419
    label "stw&#243;r"
  ]
  node [
    id 420
    label "work"
  ]
  node [
    id 421
    label "Ziemia"
  ]
  node [
    id 422
    label "cia&#322;o"
  ]
  node [
    id 423
    label "istota"
  ]
  node [
    id 424
    label "fauna"
  ]
  node [
    id 425
    label "biota"
  ]
  node [
    id 426
    label "czynnik"
  ]
  node [
    id 427
    label "proces"
  ]
  node [
    id 428
    label "zesp&#243;&#322;"
  ]
  node [
    id 429
    label "rodny"
  ]
  node [
    id 430
    label "monogeneza"
  ]
  node [
    id 431
    label "give"
  ]
  node [
    id 432
    label "pocz&#261;tek"
  ]
  node [
    id 433
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 434
    label "przyczyna"
  ]
  node [
    id 435
    label "apparition"
  ]
  node [
    id 436
    label "nap&#281;dzenie"
  ]
  node [
    id 437
    label "debiut"
  ]
  node [
    id 438
    label "&#380;alnik"
  ]
  node [
    id 439
    label "cinerarium"
  ]
  node [
    id 440
    label "park_sztywnych"
  ]
  node [
    id 441
    label "sm&#281;tarz"
  ]
  node [
    id 442
    label "pogrobowisko"
  ]
  node [
    id 443
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 444
    label "smentarz"
  ]
  node [
    id 445
    label "ostatnie_podrygi"
  ]
  node [
    id 446
    label "visitation"
  ]
  node [
    id 447
    label "agonia"
  ]
  node [
    id 448
    label "defenestracja"
  ]
  node [
    id 449
    label "punkt"
  ]
  node [
    id 450
    label "dzia&#322;anie"
  ]
  node [
    id 451
    label "mogi&#322;a"
  ]
  node [
    id 452
    label "kres_&#380;ycia"
  ]
  node [
    id 453
    label "szereg"
  ]
  node [
    id 454
    label "szeol"
  ]
  node [
    id 455
    label "pogrzebanie"
  ]
  node [
    id 456
    label "&#380;a&#322;oba"
  ]
  node [
    id 457
    label "przebiec"
  ]
  node [
    id 458
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 459
    label "motyw"
  ]
  node [
    id 460
    label "przebiegni&#281;cie"
  ]
  node [
    id 461
    label "fabu&#322;a"
  ]
  node [
    id 462
    label "Rzym_Zachodni"
  ]
  node [
    id 463
    label "whole"
  ]
  node [
    id 464
    label "ilo&#347;&#263;"
  ]
  node [
    id 465
    label "element"
  ]
  node [
    id 466
    label "Rzym_Wschodni"
  ]
  node [
    id 467
    label "urz&#261;dzenie"
  ]
  node [
    id 468
    label "warunek_lokalowy"
  ]
  node [
    id 469
    label "plac"
  ]
  node [
    id 470
    label "location"
  ]
  node [
    id 471
    label "uwaga"
  ]
  node [
    id 472
    label "przestrze&#324;"
  ]
  node [
    id 473
    label "status"
  ]
  node [
    id 474
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 475
    label "praca"
  ]
  node [
    id 476
    label "rz&#261;d"
  ]
  node [
    id 477
    label "time"
  ]
  node [
    id 478
    label "&#347;mier&#263;"
  ]
  node [
    id 479
    label "death"
  ]
  node [
    id 480
    label "upadek"
  ]
  node [
    id 481
    label "zmierzch"
  ]
  node [
    id 482
    label "stan"
  ]
  node [
    id 483
    label "nieuleczalnie_chory"
  ]
  node [
    id 484
    label "spocz&#261;&#263;"
  ]
  node [
    id 485
    label "spocz&#281;cie"
  ]
  node [
    id 486
    label "pochowanie"
  ]
  node [
    id 487
    label "spoczywa&#263;"
  ]
  node [
    id 488
    label "chowanie"
  ]
  node [
    id 489
    label "pomnik"
  ]
  node [
    id 490
    label "nagrobek"
  ]
  node [
    id 491
    label "prochowisko"
  ]
  node [
    id 492
    label "za&#347;wiaty"
  ]
  node [
    id 493
    label "piek&#322;o"
  ]
  node [
    id 494
    label "judaizm"
  ]
  node [
    id 495
    label "destruction"
  ]
  node [
    id 496
    label "zabrzmienie"
  ]
  node [
    id 497
    label "skrzywdzenie"
  ]
  node [
    id 498
    label "pozabijanie"
  ]
  node [
    id 499
    label "zniszczenie"
  ]
  node [
    id 500
    label "zaszkodzenie"
  ]
  node [
    id 501
    label "usuni&#281;cie"
  ]
  node [
    id 502
    label "spowodowanie"
  ]
  node [
    id 503
    label "killing"
  ]
  node [
    id 504
    label "zdarzenie_si&#281;"
  ]
  node [
    id 505
    label "umarcie"
  ]
  node [
    id 506
    label "granie"
  ]
  node [
    id 507
    label "zamkni&#281;cie"
  ]
  node [
    id 508
    label "compaction"
  ]
  node [
    id 509
    label "&#380;al"
  ]
  node [
    id 510
    label "paznokie&#263;"
  ]
  node [
    id 511
    label "symbol"
  ]
  node [
    id 512
    label "kir"
  ]
  node [
    id 513
    label "brud"
  ]
  node [
    id 514
    label "wyrzucenie"
  ]
  node [
    id 515
    label "defenestration"
  ]
  node [
    id 516
    label "zaj&#347;cie"
  ]
  node [
    id 517
    label "burying"
  ]
  node [
    id 518
    label "zasypanie"
  ]
  node [
    id 519
    label "zw&#322;oki"
  ]
  node [
    id 520
    label "burial"
  ]
  node [
    id 521
    label "w&#322;o&#380;enie"
  ]
  node [
    id 522
    label "porobienie"
  ]
  node [
    id 523
    label "gr&#243;b"
  ]
  node [
    id 524
    label "uniemo&#380;liwienie"
  ]
  node [
    id 525
    label "sprawa"
  ]
  node [
    id 526
    label "ust&#281;p"
  ]
  node [
    id 527
    label "plan"
  ]
  node [
    id 528
    label "obiekt_matematyczny"
  ]
  node [
    id 529
    label "problemat"
  ]
  node [
    id 530
    label "plamka"
  ]
  node [
    id 531
    label "stopie&#324;_pisma"
  ]
  node [
    id 532
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 533
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 534
    label "mark"
  ]
  node [
    id 535
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 536
    label "prosta"
  ]
  node [
    id 537
    label "problematyka"
  ]
  node [
    id 538
    label "obiekt"
  ]
  node [
    id 539
    label "zapunktowa&#263;"
  ]
  node [
    id 540
    label "podpunkt"
  ]
  node [
    id 541
    label "point"
  ]
  node [
    id 542
    label "pozycja"
  ]
  node [
    id 543
    label "szpaler"
  ]
  node [
    id 544
    label "zbi&#243;r"
  ]
  node [
    id 545
    label "column"
  ]
  node [
    id 546
    label "uporz&#261;dkowanie"
  ]
  node [
    id 547
    label "unit"
  ]
  node [
    id 548
    label "rozmieszczenie"
  ]
  node [
    id 549
    label "tract"
  ]
  node [
    id 550
    label "infimum"
  ]
  node [
    id 551
    label "powodowanie"
  ]
  node [
    id 552
    label "liczenie"
  ]
  node [
    id 553
    label "skutek"
  ]
  node [
    id 554
    label "podzia&#322;anie"
  ]
  node [
    id 555
    label "supremum"
  ]
  node [
    id 556
    label "kampania"
  ]
  node [
    id 557
    label "uruchamianie"
  ]
  node [
    id 558
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 559
    label "operacja"
  ]
  node [
    id 560
    label "hipnotyzowanie"
  ]
  node [
    id 561
    label "robienie"
  ]
  node [
    id 562
    label "uruchomienie"
  ]
  node [
    id 563
    label "nakr&#281;canie"
  ]
  node [
    id 564
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 565
    label "matematyka"
  ]
  node [
    id 566
    label "reakcja_chemiczna"
  ]
  node [
    id 567
    label "tr&#243;jstronny"
  ]
  node [
    id 568
    label "natural_process"
  ]
  node [
    id 569
    label "nakr&#281;cenie"
  ]
  node [
    id 570
    label "zatrzymanie"
  ]
  node [
    id 571
    label "wp&#322;yw"
  ]
  node [
    id 572
    label "rzut"
  ]
  node [
    id 573
    label "podtrzymywanie"
  ]
  node [
    id 574
    label "w&#322;&#261;czanie"
  ]
  node [
    id 575
    label "liczy&#263;"
  ]
  node [
    id 576
    label "operation"
  ]
  node [
    id 577
    label "rezultat"
  ]
  node [
    id 578
    label "dzianie_si&#281;"
  ]
  node [
    id 579
    label "zadzia&#322;anie"
  ]
  node [
    id 580
    label "priorytet"
  ]
  node [
    id 581
    label "rozpocz&#281;cie"
  ]
  node [
    id 582
    label "docieranie"
  ]
  node [
    id 583
    label "funkcja"
  ]
  node [
    id 584
    label "czynny"
  ]
  node [
    id 585
    label "impact"
  ]
  node [
    id 586
    label "oferta"
  ]
  node [
    id 587
    label "zako&#324;czenie"
  ]
  node [
    id 588
    label "act"
  ]
  node [
    id 589
    label "wdzieranie_si&#281;"
  ]
  node [
    id 590
    label "w&#322;&#261;czenie"
  ]
  node [
    id 591
    label "war"
  ]
  node [
    id 592
    label "angaria"
  ]
  node [
    id 593
    label "zimna_wojna"
  ]
  node [
    id 594
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 595
    label "konflikt"
  ]
  node [
    id 596
    label "wr&#243;g"
  ]
  node [
    id 597
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 598
    label "gra_w_karty"
  ]
  node [
    id 599
    label "burza"
  ]
  node [
    id 600
    label "zbrodnia_wojenna"
  ]
  node [
    id 601
    label "clash"
  ]
  node [
    id 602
    label "wsp&#243;r"
  ]
  node [
    id 603
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 604
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 605
    label "prawo"
  ]
  node [
    id 606
    label "grzmienie"
  ]
  node [
    id 607
    label "pogrzmot"
  ]
  node [
    id 608
    label "nieporz&#261;dek"
  ]
  node [
    id 609
    label "rioting"
  ]
  node [
    id 610
    label "scene"
  ]
  node [
    id 611
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 612
    label "zagrzmie&#263;"
  ]
  node [
    id 613
    label "grzmie&#263;"
  ]
  node [
    id 614
    label "burza_piaskowa"
  ]
  node [
    id 615
    label "deszcz"
  ]
  node [
    id 616
    label "piorun"
  ]
  node [
    id 617
    label "chmura"
  ]
  node [
    id 618
    label "nawa&#322;"
  ]
  node [
    id 619
    label "zagrzmienie"
  ]
  node [
    id 620
    label "fire"
  ]
  node [
    id 621
    label "przeciwnik"
  ]
  node [
    id 622
    label "posta&#263;"
  ]
  node [
    id 623
    label "wrz&#261;tek"
  ]
  node [
    id 624
    label "ciep&#322;o"
  ]
  node [
    id 625
    label "gor&#261;co"
  ]
  node [
    id 626
    label "&#347;wiatowo"
  ]
  node [
    id 627
    label "kulturalny"
  ]
  node [
    id 628
    label "generalny"
  ]
  node [
    id 629
    label "og&#243;lnie"
  ]
  node [
    id 630
    label "zwierzchni"
  ]
  node [
    id 631
    label "porz&#261;dny"
  ]
  node [
    id 632
    label "nadrz&#281;dny"
  ]
  node [
    id 633
    label "podstawowy"
  ]
  node [
    id 634
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 635
    label "zasadniczy"
  ]
  node [
    id 636
    label "generalnie"
  ]
  node [
    id 637
    label "wykszta&#322;cony"
  ]
  node [
    id 638
    label "stosowny"
  ]
  node [
    id 639
    label "elegancki"
  ]
  node [
    id 640
    label "kulturalnie"
  ]
  node [
    id 641
    label "dobrze_wychowany"
  ]
  node [
    id 642
    label "kulturny"
  ]
  node [
    id 643
    label "internationally"
  ]
  node [
    id 644
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 645
    label "poumieszcza&#263;"
  ]
  node [
    id 646
    label "hide"
  ]
  node [
    id 647
    label "znie&#347;&#263;"
  ]
  node [
    id 648
    label "straci&#263;"
  ]
  node [
    id 649
    label "powk&#322;ada&#263;"
  ]
  node [
    id 650
    label "bury"
  ]
  node [
    id 651
    label "stracenie"
  ]
  node [
    id 652
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 653
    label "leave_office"
  ]
  node [
    id 654
    label "zabi&#263;"
  ]
  node [
    id 655
    label "forfeit"
  ]
  node [
    id 656
    label "wytraci&#263;"
  ]
  node [
    id 657
    label "waste"
  ]
  node [
    id 658
    label "spowodowa&#263;"
  ]
  node [
    id 659
    label "przegra&#263;"
  ]
  node [
    id 660
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 661
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 662
    label "execute"
  ]
  node [
    id 663
    label "omin&#261;&#263;"
  ]
  node [
    id 664
    label "ubra&#263;"
  ]
  node [
    id 665
    label "oblec_si&#281;"
  ]
  node [
    id 666
    label "przekaza&#263;"
  ]
  node [
    id 667
    label "str&#243;j"
  ]
  node [
    id 668
    label "insert"
  ]
  node [
    id 669
    label "wpoi&#263;"
  ]
  node [
    id 670
    label "pour"
  ]
  node [
    id 671
    label "przyodzia&#263;"
  ]
  node [
    id 672
    label "natchn&#261;&#263;"
  ]
  node [
    id 673
    label "load"
  ]
  node [
    id 674
    label "wzbudzi&#263;"
  ]
  node [
    id 675
    label "deposit"
  ]
  node [
    id 676
    label "umie&#347;ci&#263;"
  ]
  node [
    id 677
    label "oblec"
  ]
  node [
    id 678
    label "zgromadzi&#263;"
  ]
  node [
    id 679
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 680
    label "float"
  ]
  node [
    id 681
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 682
    label "revoke"
  ]
  node [
    id 683
    label "ranny"
  ]
  node [
    id 684
    label "usun&#261;&#263;"
  ]
  node [
    id 685
    label "zebra&#263;"
  ]
  node [
    id 686
    label "wytrzyma&#263;"
  ]
  node [
    id 687
    label "digest"
  ]
  node [
    id 688
    label "lift"
  ]
  node [
    id 689
    label "podda&#263;_si&#281;"
  ]
  node [
    id 690
    label "przenie&#347;&#263;"
  ]
  node [
    id 691
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 692
    label "&#347;cierpie&#263;"
  ]
  node [
    id 693
    label "porwa&#263;"
  ]
  node [
    id 694
    label "wygra&#263;"
  ]
  node [
    id 695
    label "abolicjonista"
  ]
  node [
    id 696
    label "raise"
  ]
  node [
    id 697
    label "zniszczy&#263;"
  ]
  node [
    id 698
    label "ekshumowanie"
  ]
  node [
    id 699
    label "zabalsamowanie"
  ]
  node [
    id 700
    label "kremacja"
  ]
  node [
    id 701
    label "pijany"
  ]
  node [
    id 702
    label "zm&#281;czony"
  ]
  node [
    id 703
    label "nieumar&#322;y"
  ]
  node [
    id 704
    label "balsamowa&#263;"
  ]
  node [
    id 705
    label "tanatoplastyka"
  ]
  node [
    id 706
    label "ekshumowa&#263;"
  ]
  node [
    id 707
    label "tanatoplastyk"
  ]
  node [
    id 708
    label "sekcja"
  ]
  node [
    id 709
    label "balsamowanie"
  ]
  node [
    id 710
    label "zabalsamowa&#263;"
  ]
  node [
    id 711
    label "pogrzeb"
  ]
  node [
    id 712
    label "brunatny"
  ]
  node [
    id 713
    label "przygn&#281;biaj&#261;cy"
  ]
  node [
    id 714
    label "ciemnoszary"
  ]
  node [
    id 715
    label "brudnoszary"
  ]
  node [
    id 716
    label "buro"
  ]
  node [
    id 717
    label "cover"
  ]
  node [
    id 718
    label "gra_planszowa"
  ]
  node [
    id 719
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 720
    label "harcap"
  ]
  node [
    id 721
    label "elew"
  ]
  node [
    id 722
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 723
    label "demobilizowanie"
  ]
  node [
    id 724
    label "demobilizowa&#263;"
  ]
  node [
    id 725
    label "zdemobilizowanie"
  ]
  node [
    id 726
    label "Gurkha"
  ]
  node [
    id 727
    label "so&#322;dat"
  ]
  node [
    id 728
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 729
    label "mundurowy"
  ]
  node [
    id 730
    label "rota"
  ]
  node [
    id 731
    label "zdemobilizowa&#263;"
  ]
  node [
    id 732
    label "walcz&#261;cy"
  ]
  node [
    id 733
    label "&#380;o&#322;dowy"
  ]
  node [
    id 734
    label "piecz&#261;tka"
  ]
  node [
    id 735
    label "s&#261;d_ko&#347;cielny"
  ]
  node [
    id 736
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 737
    label "&#322;ama&#263;"
  ]
  node [
    id 738
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 739
    label "tortury"
  ]
  node [
    id 740
    label "papie&#380;"
  ]
  node [
    id 741
    label "chordofon_szarpany"
  ]
  node [
    id 742
    label "przysi&#281;ga"
  ]
  node [
    id 743
    label "&#322;amanie"
  ]
  node [
    id 744
    label "szyk"
  ]
  node [
    id 745
    label "s&#261;d_apelacyjny"
  ]
  node [
    id 746
    label "whip"
  ]
  node [
    id 747
    label "Rota"
  ]
  node [
    id 748
    label "instrument_strunowy"
  ]
  node [
    id 749
    label "formu&#322;a"
  ]
  node [
    id 750
    label "zrejterowanie"
  ]
  node [
    id 751
    label "zmobilizowa&#263;"
  ]
  node [
    id 752
    label "dezerter"
  ]
  node [
    id 753
    label "oddzia&#322;_karny"
  ]
  node [
    id 754
    label "rezerwa"
  ]
  node [
    id 755
    label "tabor"
  ]
  node [
    id 756
    label "wermacht"
  ]
  node [
    id 757
    label "cofni&#281;cie"
  ]
  node [
    id 758
    label "potencja"
  ]
  node [
    id 759
    label "struktura"
  ]
  node [
    id 760
    label "korpus"
  ]
  node [
    id 761
    label "soldateska"
  ]
  node [
    id 762
    label "ods&#322;ugiwanie"
  ]
  node [
    id 763
    label "werbowanie_si&#281;"
  ]
  node [
    id 764
    label "oddzia&#322;"
  ]
  node [
    id 765
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 766
    label "s&#322;u&#380;ba"
  ]
  node [
    id 767
    label "or&#281;&#380;"
  ]
  node [
    id 768
    label "Legia_Cudzoziemska"
  ]
  node [
    id 769
    label "Armia_Czerwona"
  ]
  node [
    id 770
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 771
    label "rejterowanie"
  ]
  node [
    id 772
    label "Czerwona_Gwardia"
  ]
  node [
    id 773
    label "si&#322;a"
  ]
  node [
    id 774
    label "zrejterowa&#263;"
  ]
  node [
    id 775
    label "sztabslekarz"
  ]
  node [
    id 776
    label "zmobilizowanie"
  ]
  node [
    id 777
    label "wojo"
  ]
  node [
    id 778
    label "pospolite_ruszenie"
  ]
  node [
    id 779
    label "Eurokorpus"
  ]
  node [
    id 780
    label "mobilizowanie"
  ]
  node [
    id 781
    label "rejterowa&#263;"
  ]
  node [
    id 782
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 783
    label "mobilizowa&#263;"
  ]
  node [
    id 784
    label "Armia_Krajowa"
  ]
  node [
    id 785
    label "dryl"
  ]
  node [
    id 786
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 787
    label "petarda"
  ]
  node [
    id 788
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 789
    label "grupa"
  ]
  node [
    id 790
    label "zaw&#243;d"
  ]
  node [
    id 791
    label "ludzko&#347;&#263;"
  ]
  node [
    id 792
    label "asymilowanie"
  ]
  node [
    id 793
    label "wapniak"
  ]
  node [
    id 794
    label "asymilowa&#263;"
  ]
  node [
    id 795
    label "os&#322;abia&#263;"
  ]
  node [
    id 796
    label "hominid"
  ]
  node [
    id 797
    label "podw&#322;adny"
  ]
  node [
    id 798
    label "os&#322;abianie"
  ]
  node [
    id 799
    label "g&#322;owa"
  ]
  node [
    id 800
    label "figura"
  ]
  node [
    id 801
    label "portrecista"
  ]
  node [
    id 802
    label "dwun&#243;g"
  ]
  node [
    id 803
    label "profanum"
  ]
  node [
    id 804
    label "nasada"
  ]
  node [
    id 805
    label "duch"
  ]
  node [
    id 806
    label "antropochoria"
  ]
  node [
    id 807
    label "osoba"
  ]
  node [
    id 808
    label "wz&#243;r"
  ]
  node [
    id 809
    label "senior"
  ]
  node [
    id 810
    label "oddzia&#322;ywanie"
  ]
  node [
    id 811
    label "Adam"
  ]
  node [
    id 812
    label "homo_sapiens"
  ]
  node [
    id 813
    label "polifag"
  ]
  node [
    id 814
    label "ucze&#324;"
  ]
  node [
    id 815
    label "odstr&#281;czenie"
  ]
  node [
    id 816
    label "zreorganizowanie"
  ]
  node [
    id 817
    label "odprawienie"
  ]
  node [
    id 818
    label "zniech&#281;cenie_si&#281;"
  ]
  node [
    id 819
    label "zniech&#281;ca&#263;"
  ]
  node [
    id 820
    label "zwalnia&#263;"
  ]
  node [
    id 821
    label "przebudowywa&#263;"
  ]
  node [
    id 822
    label "Nepal"
  ]
  node [
    id 823
    label "peruka"
  ]
  node [
    id 824
    label "warkocz"
  ]
  node [
    id 825
    label "zwalnianie"
  ]
  node [
    id 826
    label "zniech&#281;canie"
  ]
  node [
    id 827
    label "przebudowywanie"
  ]
  node [
    id 828
    label "zniech&#281;canie_si&#281;"
  ]
  node [
    id 829
    label "funkcjonariusz"
  ]
  node [
    id 830
    label "nosiciel"
  ]
  node [
    id 831
    label "wojownik"
  ]
  node [
    id 832
    label "zniech&#281;ci&#263;"
  ]
  node [
    id 833
    label "zreorganizowa&#263;"
  ]
  node [
    id 834
    label "odprawi&#263;"
  ]
  node [
    id 835
    label "Polish"
  ]
  node [
    id 836
    label "goniony"
  ]
  node [
    id 837
    label "oberek"
  ]
  node [
    id 838
    label "ryba_po_grecku"
  ]
  node [
    id 839
    label "sztajer"
  ]
  node [
    id 840
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 841
    label "krakowiak"
  ]
  node [
    id 842
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 843
    label "pierogi_ruskie"
  ]
  node [
    id 844
    label "lacki"
  ]
  node [
    id 845
    label "polak"
  ]
  node [
    id 846
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 847
    label "chodzony"
  ]
  node [
    id 848
    label "po_polsku"
  ]
  node [
    id 849
    label "mazur"
  ]
  node [
    id 850
    label "polsko"
  ]
  node [
    id 851
    label "skoczny"
  ]
  node [
    id 852
    label "drabant"
  ]
  node [
    id 853
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 854
    label "j&#281;zyk"
  ]
  node [
    id 855
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 856
    label "artykulator"
  ]
  node [
    id 857
    label "kod"
  ]
  node [
    id 858
    label "kawa&#322;ek"
  ]
  node [
    id 859
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 860
    label "gramatyka"
  ]
  node [
    id 861
    label "stylik"
  ]
  node [
    id 862
    label "przet&#322;umaczenie"
  ]
  node [
    id 863
    label "formalizowanie"
  ]
  node [
    id 864
    label "ssanie"
  ]
  node [
    id 865
    label "ssa&#263;"
  ]
  node [
    id 866
    label "language"
  ]
  node [
    id 867
    label "liza&#263;"
  ]
  node [
    id 868
    label "napisa&#263;"
  ]
  node [
    id 869
    label "konsonantyzm"
  ]
  node [
    id 870
    label "wokalizm"
  ]
  node [
    id 871
    label "pisa&#263;"
  ]
  node [
    id 872
    label "fonetyka"
  ]
  node [
    id 873
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 874
    label "jeniec"
  ]
  node [
    id 875
    label "but"
  ]
  node [
    id 876
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 877
    label "po_koroniarsku"
  ]
  node [
    id 878
    label "kultura_duchowa"
  ]
  node [
    id 879
    label "t&#322;umaczenie"
  ]
  node [
    id 880
    label "m&#243;wienie"
  ]
  node [
    id 881
    label "pype&#263;"
  ]
  node [
    id 882
    label "lizanie"
  ]
  node [
    id 883
    label "pismo"
  ]
  node [
    id 884
    label "formalizowa&#263;"
  ]
  node [
    id 885
    label "rozumie&#263;"
  ]
  node [
    id 886
    label "organ"
  ]
  node [
    id 887
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 888
    label "rozumienie"
  ]
  node [
    id 889
    label "spos&#243;b"
  ]
  node [
    id 890
    label "makroglosja"
  ]
  node [
    id 891
    label "m&#243;wi&#263;"
  ]
  node [
    id 892
    label "jama_ustna"
  ]
  node [
    id 893
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 894
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 895
    label "natural_language"
  ]
  node [
    id 896
    label "s&#322;ownictwo"
  ]
  node [
    id 897
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 898
    label "wschodnioeuropejski"
  ]
  node [
    id 899
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 900
    label "poga&#324;ski"
  ]
  node [
    id 901
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 902
    label "topielec"
  ]
  node [
    id 903
    label "europejski"
  ]
  node [
    id 904
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 905
    label "langosz"
  ]
  node [
    id 906
    label "zboczenie"
  ]
  node [
    id 907
    label "om&#243;wienie"
  ]
  node [
    id 908
    label "sponiewieranie"
  ]
  node [
    id 909
    label "discipline"
  ]
  node [
    id 910
    label "omawia&#263;"
  ]
  node [
    id 911
    label "kr&#261;&#380;enie"
  ]
  node [
    id 912
    label "tre&#347;&#263;"
  ]
  node [
    id 913
    label "sponiewiera&#263;"
  ]
  node [
    id 914
    label "entity"
  ]
  node [
    id 915
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 916
    label "tematyka"
  ]
  node [
    id 917
    label "w&#261;tek"
  ]
  node [
    id 918
    label "zbaczanie"
  ]
  node [
    id 919
    label "program_nauczania"
  ]
  node [
    id 920
    label "om&#243;wi&#263;"
  ]
  node [
    id 921
    label "omawianie"
  ]
  node [
    id 922
    label "thing"
  ]
  node [
    id 923
    label "kultura"
  ]
  node [
    id 924
    label "zbacza&#263;"
  ]
  node [
    id 925
    label "zboczy&#263;"
  ]
  node [
    id 926
    label "gwardzista"
  ]
  node [
    id 927
    label "melodia"
  ]
  node [
    id 928
    label "taniec"
  ]
  node [
    id 929
    label "taniec_ludowy"
  ]
  node [
    id 930
    label "&#347;redniowieczny"
  ]
  node [
    id 931
    label "europejsko"
  ]
  node [
    id 932
    label "specjalny"
  ]
  node [
    id 933
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 934
    label "weso&#322;y"
  ]
  node [
    id 935
    label "sprawny"
  ]
  node [
    id 936
    label "rytmiczny"
  ]
  node [
    id 937
    label "skocznie"
  ]
  node [
    id 938
    label "energiczny"
  ]
  node [
    id 939
    label "przytup"
  ]
  node [
    id 940
    label "ho&#322;ubiec"
  ]
  node [
    id 941
    label "wodzi&#263;"
  ]
  node [
    id 942
    label "lendler"
  ]
  node [
    id 943
    label "austriacki"
  ]
  node [
    id 944
    label "polka"
  ]
  node [
    id 945
    label "ludowy"
  ]
  node [
    id 946
    label "pie&#347;&#324;"
  ]
  node [
    id 947
    label "mieszkaniec"
  ]
  node [
    id 948
    label "centu&#347;"
  ]
  node [
    id 949
    label "lalka"
  ]
  node [
    id 950
    label "Ma&#322;opolanin"
  ]
  node [
    id 951
    label "krakauer"
  ]
  node [
    id 952
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 953
    label "Katar"
  ]
  node [
    id 954
    label "Libia"
  ]
  node [
    id 955
    label "Gwatemala"
  ]
  node [
    id 956
    label "Ekwador"
  ]
  node [
    id 957
    label "Afganistan"
  ]
  node [
    id 958
    label "Tad&#380;ykistan"
  ]
  node [
    id 959
    label "Bhutan"
  ]
  node [
    id 960
    label "Argentyna"
  ]
  node [
    id 961
    label "D&#380;ibuti"
  ]
  node [
    id 962
    label "Wenezuela"
  ]
  node [
    id 963
    label "Gabon"
  ]
  node [
    id 964
    label "Ukraina"
  ]
  node [
    id 965
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 966
    label "Rwanda"
  ]
  node [
    id 967
    label "Liechtenstein"
  ]
  node [
    id 968
    label "organizacja"
  ]
  node [
    id 969
    label "Sri_Lanka"
  ]
  node [
    id 970
    label "Madagaskar"
  ]
  node [
    id 971
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 972
    label "Kongo"
  ]
  node [
    id 973
    label "Tonga"
  ]
  node [
    id 974
    label "Bangladesz"
  ]
  node [
    id 975
    label "Kanada"
  ]
  node [
    id 976
    label "Wehrlen"
  ]
  node [
    id 977
    label "Algieria"
  ]
  node [
    id 978
    label "Uganda"
  ]
  node [
    id 979
    label "Surinam"
  ]
  node [
    id 980
    label "Sahara_Zachodnia"
  ]
  node [
    id 981
    label "Chile"
  ]
  node [
    id 982
    label "W&#281;gry"
  ]
  node [
    id 983
    label "Birma"
  ]
  node [
    id 984
    label "Kazachstan"
  ]
  node [
    id 985
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 986
    label "Armenia"
  ]
  node [
    id 987
    label "Tuwalu"
  ]
  node [
    id 988
    label "Timor_Wschodni"
  ]
  node [
    id 989
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 990
    label "Izrael"
  ]
  node [
    id 991
    label "Estonia"
  ]
  node [
    id 992
    label "Komory"
  ]
  node [
    id 993
    label "Kamerun"
  ]
  node [
    id 994
    label "Haiti"
  ]
  node [
    id 995
    label "Belize"
  ]
  node [
    id 996
    label "Sierra_Leone"
  ]
  node [
    id 997
    label "Luksemburg"
  ]
  node [
    id 998
    label "USA"
  ]
  node [
    id 999
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1000
    label "Barbados"
  ]
  node [
    id 1001
    label "San_Marino"
  ]
  node [
    id 1002
    label "Bu&#322;garia"
  ]
  node [
    id 1003
    label "Indonezja"
  ]
  node [
    id 1004
    label "Wietnam"
  ]
  node [
    id 1005
    label "Malawi"
  ]
  node [
    id 1006
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1007
    label "Francja"
  ]
  node [
    id 1008
    label "partia"
  ]
  node [
    id 1009
    label "Zambia"
  ]
  node [
    id 1010
    label "Angola"
  ]
  node [
    id 1011
    label "Grenada"
  ]
  node [
    id 1012
    label "Panama"
  ]
  node [
    id 1013
    label "Rumunia"
  ]
  node [
    id 1014
    label "Czarnog&#243;ra"
  ]
  node [
    id 1015
    label "Malediwy"
  ]
  node [
    id 1016
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1017
    label "S&#322;owacja"
  ]
  node [
    id 1018
    label "para"
  ]
  node [
    id 1019
    label "Egipt"
  ]
  node [
    id 1020
    label "zwrot"
  ]
  node [
    id 1021
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1022
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1023
    label "Mozambik"
  ]
  node [
    id 1024
    label "Kolumbia"
  ]
  node [
    id 1025
    label "Laos"
  ]
  node [
    id 1026
    label "Burundi"
  ]
  node [
    id 1027
    label "Suazi"
  ]
  node [
    id 1028
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1029
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1030
    label "Czechy"
  ]
  node [
    id 1031
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1032
    label "Wyspy_Marshalla"
  ]
  node [
    id 1033
    label "Dominika"
  ]
  node [
    id 1034
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1035
    label "Syria"
  ]
  node [
    id 1036
    label "Palau"
  ]
  node [
    id 1037
    label "Gwinea_Bissau"
  ]
  node [
    id 1038
    label "Liberia"
  ]
  node [
    id 1039
    label "Jamajka"
  ]
  node [
    id 1040
    label "Zimbabwe"
  ]
  node [
    id 1041
    label "Polska"
  ]
  node [
    id 1042
    label "Dominikana"
  ]
  node [
    id 1043
    label "Senegal"
  ]
  node [
    id 1044
    label "Togo"
  ]
  node [
    id 1045
    label "Gujana"
  ]
  node [
    id 1046
    label "Gruzja"
  ]
  node [
    id 1047
    label "Albania"
  ]
  node [
    id 1048
    label "Zair"
  ]
  node [
    id 1049
    label "Meksyk"
  ]
  node [
    id 1050
    label "Macedonia"
  ]
  node [
    id 1051
    label "Chorwacja"
  ]
  node [
    id 1052
    label "Kambod&#380;a"
  ]
  node [
    id 1053
    label "Monako"
  ]
  node [
    id 1054
    label "Mauritius"
  ]
  node [
    id 1055
    label "Gwinea"
  ]
  node [
    id 1056
    label "Mali"
  ]
  node [
    id 1057
    label "Nigeria"
  ]
  node [
    id 1058
    label "Kostaryka"
  ]
  node [
    id 1059
    label "Hanower"
  ]
  node [
    id 1060
    label "Paragwaj"
  ]
  node [
    id 1061
    label "W&#322;ochy"
  ]
  node [
    id 1062
    label "Seszele"
  ]
  node [
    id 1063
    label "Wyspy_Salomona"
  ]
  node [
    id 1064
    label "Hiszpania"
  ]
  node [
    id 1065
    label "Boliwia"
  ]
  node [
    id 1066
    label "Kirgistan"
  ]
  node [
    id 1067
    label "Irlandia"
  ]
  node [
    id 1068
    label "Czad"
  ]
  node [
    id 1069
    label "Irak"
  ]
  node [
    id 1070
    label "Lesoto"
  ]
  node [
    id 1071
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1072
    label "Malta"
  ]
  node [
    id 1073
    label "Andora"
  ]
  node [
    id 1074
    label "Chiny"
  ]
  node [
    id 1075
    label "Filipiny"
  ]
  node [
    id 1076
    label "Antarktis"
  ]
  node [
    id 1077
    label "Niemcy"
  ]
  node [
    id 1078
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1079
    label "Pakistan"
  ]
  node [
    id 1080
    label "terytorium"
  ]
  node [
    id 1081
    label "Nikaragua"
  ]
  node [
    id 1082
    label "Brazylia"
  ]
  node [
    id 1083
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1084
    label "Maroko"
  ]
  node [
    id 1085
    label "Portugalia"
  ]
  node [
    id 1086
    label "Niger"
  ]
  node [
    id 1087
    label "Kenia"
  ]
  node [
    id 1088
    label "Botswana"
  ]
  node [
    id 1089
    label "Fid&#380;i"
  ]
  node [
    id 1090
    label "Tunezja"
  ]
  node [
    id 1091
    label "Australia"
  ]
  node [
    id 1092
    label "Tajlandia"
  ]
  node [
    id 1093
    label "Burkina_Faso"
  ]
  node [
    id 1094
    label "interior"
  ]
  node [
    id 1095
    label "Tanzania"
  ]
  node [
    id 1096
    label "Benin"
  ]
  node [
    id 1097
    label "Indie"
  ]
  node [
    id 1098
    label "&#321;otwa"
  ]
  node [
    id 1099
    label "Kiribati"
  ]
  node [
    id 1100
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1101
    label "Rodezja"
  ]
  node [
    id 1102
    label "Cypr"
  ]
  node [
    id 1103
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1104
    label "Peru"
  ]
  node [
    id 1105
    label "Austria"
  ]
  node [
    id 1106
    label "Urugwaj"
  ]
  node [
    id 1107
    label "Jordania"
  ]
  node [
    id 1108
    label "Grecja"
  ]
  node [
    id 1109
    label "Azerbejd&#380;an"
  ]
  node [
    id 1110
    label "Turcja"
  ]
  node [
    id 1111
    label "Samoa"
  ]
  node [
    id 1112
    label "Sudan"
  ]
  node [
    id 1113
    label "Oman"
  ]
  node [
    id 1114
    label "ziemia"
  ]
  node [
    id 1115
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1116
    label "Uzbekistan"
  ]
  node [
    id 1117
    label "Portoryko"
  ]
  node [
    id 1118
    label "Honduras"
  ]
  node [
    id 1119
    label "Mongolia"
  ]
  node [
    id 1120
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1121
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1122
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1123
    label "Serbia"
  ]
  node [
    id 1124
    label "Tajwan"
  ]
  node [
    id 1125
    label "Wielka_Brytania"
  ]
  node [
    id 1126
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1127
    label "Liban"
  ]
  node [
    id 1128
    label "Japonia"
  ]
  node [
    id 1129
    label "Ghana"
  ]
  node [
    id 1130
    label "Belgia"
  ]
  node [
    id 1131
    label "Bahrajn"
  ]
  node [
    id 1132
    label "Mikronezja"
  ]
  node [
    id 1133
    label "Etiopia"
  ]
  node [
    id 1134
    label "Kuwejt"
  ]
  node [
    id 1135
    label "Bahamy"
  ]
  node [
    id 1136
    label "Rosja"
  ]
  node [
    id 1137
    label "Mo&#322;dawia"
  ]
  node [
    id 1138
    label "Litwa"
  ]
  node [
    id 1139
    label "S&#322;owenia"
  ]
  node [
    id 1140
    label "Szwajcaria"
  ]
  node [
    id 1141
    label "Erytrea"
  ]
  node [
    id 1142
    label "Arabia_Saudyjska"
  ]
  node [
    id 1143
    label "Kuba"
  ]
  node [
    id 1144
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1145
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1146
    label "Malezja"
  ]
  node [
    id 1147
    label "Korea"
  ]
  node [
    id 1148
    label "Jemen"
  ]
  node [
    id 1149
    label "Nowa_Zelandia"
  ]
  node [
    id 1150
    label "Namibia"
  ]
  node [
    id 1151
    label "Nauru"
  ]
  node [
    id 1152
    label "holoarktyka"
  ]
  node [
    id 1153
    label "Brunei"
  ]
  node [
    id 1154
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1155
    label "Khitai"
  ]
  node [
    id 1156
    label "Mauretania"
  ]
  node [
    id 1157
    label "Iran"
  ]
  node [
    id 1158
    label "Gambia"
  ]
  node [
    id 1159
    label "Somalia"
  ]
  node [
    id 1160
    label "Holandia"
  ]
  node [
    id 1161
    label "Turkmenistan"
  ]
  node [
    id 1162
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1163
    label "Salwador"
  ]
  node [
    id 1164
    label "pair"
  ]
  node [
    id 1165
    label "odparowywanie"
  ]
  node [
    id 1166
    label "gaz_cieplarniany"
  ]
  node [
    id 1167
    label "chodzi&#263;"
  ]
  node [
    id 1168
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1169
    label "poker"
  ]
  node [
    id 1170
    label "parowanie"
  ]
  node [
    id 1171
    label "damp"
  ]
  node [
    id 1172
    label "nale&#380;e&#263;"
  ]
  node [
    id 1173
    label "odparowanie"
  ]
  node [
    id 1174
    label "odparowa&#263;"
  ]
  node [
    id 1175
    label "dodatek"
  ]
  node [
    id 1176
    label "jednostka_monetarna"
  ]
  node [
    id 1177
    label "smoke"
  ]
  node [
    id 1178
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1179
    label "odparowywa&#263;"
  ]
  node [
    id 1180
    label "uk&#322;ad"
  ]
  node [
    id 1181
    label "gaz"
  ]
  node [
    id 1182
    label "wyparowanie"
  ]
  node [
    id 1183
    label "obszar"
  ]
  node [
    id 1184
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1185
    label "jednostka_administracyjna"
  ]
  node [
    id 1186
    label "Jukon"
  ]
  node [
    id 1187
    label "podmiot"
  ]
  node [
    id 1188
    label "jednostka_organizacyjna"
  ]
  node [
    id 1189
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1190
    label "TOPR"
  ]
  node [
    id 1191
    label "endecki"
  ]
  node [
    id 1192
    label "przedstawicielstwo"
  ]
  node [
    id 1193
    label "od&#322;am"
  ]
  node [
    id 1194
    label "Cepelia"
  ]
  node [
    id 1195
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1196
    label "ZBoWiD"
  ]
  node [
    id 1197
    label "organization"
  ]
  node [
    id 1198
    label "centrala"
  ]
  node [
    id 1199
    label "GOPR"
  ]
  node [
    id 1200
    label "ZOMO"
  ]
  node [
    id 1201
    label "ZMP"
  ]
  node [
    id 1202
    label "komitet_koordynacyjny"
  ]
  node [
    id 1203
    label "przybud&#243;wka"
  ]
  node [
    id 1204
    label "boj&#243;wka"
  ]
  node [
    id 1205
    label "turn"
  ]
  node [
    id 1206
    label "turning"
  ]
  node [
    id 1207
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1208
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1209
    label "skr&#281;t"
  ]
  node [
    id 1210
    label "obr&#243;t"
  ]
  node [
    id 1211
    label "fraza_czasownikowa"
  ]
  node [
    id 1212
    label "jednostka_leksykalna"
  ]
  node [
    id 1213
    label "zmiana"
  ]
  node [
    id 1214
    label "odm&#322;adzanie"
  ]
  node [
    id 1215
    label "liga"
  ]
  node [
    id 1216
    label "jednostka_systematyczna"
  ]
  node [
    id 1217
    label "gromada"
  ]
  node [
    id 1218
    label "egzemplarz"
  ]
  node [
    id 1219
    label "Entuzjastki"
  ]
  node [
    id 1220
    label "kompozycja"
  ]
  node [
    id 1221
    label "Terranie"
  ]
  node [
    id 1222
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1223
    label "category"
  ]
  node [
    id 1224
    label "pakiet_klimatyczny"
  ]
  node [
    id 1225
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1226
    label "cz&#261;steczka"
  ]
  node [
    id 1227
    label "stage_set"
  ]
  node [
    id 1228
    label "type"
  ]
  node [
    id 1229
    label "specgrupa"
  ]
  node [
    id 1230
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1231
    label "&#346;wietliki"
  ]
  node [
    id 1232
    label "odm&#322;odzenie"
  ]
  node [
    id 1233
    label "Eurogrupa"
  ]
  node [
    id 1234
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1235
    label "harcerze_starsi"
  ]
  node [
    id 1236
    label "Bund"
  ]
  node [
    id 1237
    label "PPR"
  ]
  node [
    id 1238
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1239
    label "wybranek"
  ]
  node [
    id 1240
    label "Jakobici"
  ]
  node [
    id 1241
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1242
    label "SLD"
  ]
  node [
    id 1243
    label "Razem"
  ]
  node [
    id 1244
    label "PiS"
  ]
  node [
    id 1245
    label "package"
  ]
  node [
    id 1246
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1247
    label "Kuomintang"
  ]
  node [
    id 1248
    label "ZSL"
  ]
  node [
    id 1249
    label "AWS"
  ]
  node [
    id 1250
    label "gra"
  ]
  node [
    id 1251
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1252
    label "game"
  ]
  node [
    id 1253
    label "blok"
  ]
  node [
    id 1254
    label "materia&#322;"
  ]
  node [
    id 1255
    label "PO"
  ]
  node [
    id 1256
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1257
    label "niedoczas"
  ]
  node [
    id 1258
    label "Federali&#347;ci"
  ]
  node [
    id 1259
    label "PSL"
  ]
  node [
    id 1260
    label "Wigowie"
  ]
  node [
    id 1261
    label "ZChN"
  ]
  node [
    id 1262
    label "egzekutywa"
  ]
  node [
    id 1263
    label "aktyw"
  ]
  node [
    id 1264
    label "wybranka"
  ]
  node [
    id 1265
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1266
    label "biom"
  ]
  node [
    id 1267
    label "szata_ro&#347;linna"
  ]
  node [
    id 1268
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1269
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1270
    label "przyroda"
  ]
  node [
    id 1271
    label "zielono&#347;&#263;"
  ]
  node [
    id 1272
    label "pi&#281;tro"
  ]
  node [
    id 1273
    label "plant"
  ]
  node [
    id 1274
    label "ro&#347;lina"
  ]
  node [
    id 1275
    label "geosystem"
  ]
  node [
    id 1276
    label "Kaszmir"
  ]
  node [
    id 1277
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1278
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1279
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1280
    label "Pend&#380;ab"
  ]
  node [
    id 1281
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1282
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1283
    label "funt_liba&#324;ski"
  ]
  node [
    id 1284
    label "strefa_euro"
  ]
  node [
    id 1285
    label "Pozna&#324;"
  ]
  node [
    id 1286
    label "lira_malta&#324;ska"
  ]
  node [
    id 1287
    label "Gozo"
  ]
  node [
    id 1288
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1289
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1290
    label "Afryka_Zachodnia"
  ]
  node [
    id 1291
    label "Afryka_Wschodnia"
  ]
  node [
    id 1292
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1293
    label "dolar_namibijski"
  ]
  node [
    id 1294
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1295
    label "milrejs"
  ]
  node [
    id 1296
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1297
    label "NATO"
  ]
  node [
    id 1298
    label "escudo_portugalskie"
  ]
  node [
    id 1299
    label "dolar_bahamski"
  ]
  node [
    id 1300
    label "Wielka_Bahama"
  ]
  node [
    id 1301
    label "Karaiby"
  ]
  node [
    id 1302
    label "dolar_liberyjski"
  ]
  node [
    id 1303
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1304
    label "riel"
  ]
  node [
    id 1305
    label "Karelia"
  ]
  node [
    id 1306
    label "Mari_El"
  ]
  node [
    id 1307
    label "Inguszetia"
  ]
  node [
    id 1308
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1309
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1310
    label "Udmurcja"
  ]
  node [
    id 1311
    label "Newa"
  ]
  node [
    id 1312
    label "&#321;adoga"
  ]
  node [
    id 1313
    label "Czeczenia"
  ]
  node [
    id 1314
    label "Anadyr"
  ]
  node [
    id 1315
    label "Syberia"
  ]
  node [
    id 1316
    label "Tatarstan"
  ]
  node [
    id 1317
    label "Wszechrosja"
  ]
  node [
    id 1318
    label "Azja"
  ]
  node [
    id 1319
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1320
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1321
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1322
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1323
    label "Europa_Wschodnia"
  ]
  node [
    id 1324
    label "Witim"
  ]
  node [
    id 1325
    label "Kamczatka"
  ]
  node [
    id 1326
    label "Jama&#322;"
  ]
  node [
    id 1327
    label "Dagestan"
  ]
  node [
    id 1328
    label "Baszkiria"
  ]
  node [
    id 1329
    label "Tuwa"
  ]
  node [
    id 1330
    label "car"
  ]
  node [
    id 1331
    label "Komi"
  ]
  node [
    id 1332
    label "Czuwaszja"
  ]
  node [
    id 1333
    label "Chakasja"
  ]
  node [
    id 1334
    label "Perm"
  ]
  node [
    id 1335
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1336
    label "Ajon"
  ]
  node [
    id 1337
    label "Adygeja"
  ]
  node [
    id 1338
    label "Dniepr"
  ]
  node [
    id 1339
    label "rubel_rosyjski"
  ]
  node [
    id 1340
    label "Don"
  ]
  node [
    id 1341
    label "Mordowia"
  ]
  node [
    id 1342
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1343
    label "lew"
  ]
  node [
    id 1344
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1345
    label "Dobrud&#380;a"
  ]
  node [
    id 1346
    label "Unia_Europejska"
  ]
  node [
    id 1347
    label "lira_izraelska"
  ]
  node [
    id 1348
    label "szekel"
  ]
  node [
    id 1349
    label "Galilea"
  ]
  node [
    id 1350
    label "Judea"
  ]
  node [
    id 1351
    label "Luksemburgia"
  ]
  node [
    id 1352
    label "frank_belgijski"
  ]
  node [
    id 1353
    label "Limburgia"
  ]
  node [
    id 1354
    label "Brabancja"
  ]
  node [
    id 1355
    label "Walonia"
  ]
  node [
    id 1356
    label "Flandria"
  ]
  node [
    id 1357
    label "Niderlandy"
  ]
  node [
    id 1358
    label "dinar_iracki"
  ]
  node [
    id 1359
    label "Maghreb"
  ]
  node [
    id 1360
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1361
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1362
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1363
    label "szyling_ugandyjski"
  ]
  node [
    id 1364
    label "dolar_jamajski"
  ]
  node [
    id 1365
    label "kafar"
  ]
  node [
    id 1366
    label "ringgit"
  ]
  node [
    id 1367
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1368
    label "Borneo"
  ]
  node [
    id 1369
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1370
    label "dolar_surinamski"
  ]
  node [
    id 1371
    label "funt_suda&#324;ski"
  ]
  node [
    id 1372
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1373
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1374
    label "Manica"
  ]
  node [
    id 1375
    label "escudo_mozambickie"
  ]
  node [
    id 1376
    label "Cabo_Delgado"
  ]
  node [
    id 1377
    label "Inhambane"
  ]
  node [
    id 1378
    label "Maputo"
  ]
  node [
    id 1379
    label "Gaza"
  ]
  node [
    id 1380
    label "Niasa"
  ]
  node [
    id 1381
    label "Nampula"
  ]
  node [
    id 1382
    label "metical"
  ]
  node [
    id 1383
    label "Sahara"
  ]
  node [
    id 1384
    label "inti"
  ]
  node [
    id 1385
    label "sol"
  ]
  node [
    id 1386
    label "kip"
  ]
  node [
    id 1387
    label "Pireneje"
  ]
  node [
    id 1388
    label "euro"
  ]
  node [
    id 1389
    label "kwacha_zambijska"
  ]
  node [
    id 1390
    label "tugrik"
  ]
  node [
    id 1391
    label "Azja_Wschodnia"
  ]
  node [
    id 1392
    label "Buriaci"
  ]
  node [
    id 1393
    label "ajmak"
  ]
  node [
    id 1394
    label "balboa"
  ]
  node [
    id 1395
    label "Ameryka_Centralna"
  ]
  node [
    id 1396
    label "dolar"
  ]
  node [
    id 1397
    label "gulden"
  ]
  node [
    id 1398
    label "Zelandia"
  ]
  node [
    id 1399
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1400
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1401
    label "Polinezja"
  ]
  node [
    id 1402
    label "dolar_Tuvalu"
  ]
  node [
    id 1403
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1404
    label "zair"
  ]
  node [
    id 1405
    label "Katanga"
  ]
  node [
    id 1406
    label "Europa_Zachodnia"
  ]
  node [
    id 1407
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1408
    label "frank_szwajcarski"
  ]
  node [
    id 1409
    label "Jukatan"
  ]
  node [
    id 1410
    label "dolar_Belize"
  ]
  node [
    id 1411
    label "colon"
  ]
  node [
    id 1412
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1413
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1414
    label "Dyja"
  ]
  node [
    id 1415
    label "korona_czeska"
  ]
  node [
    id 1416
    label "Izera"
  ]
  node [
    id 1417
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1418
    label "Lasko"
  ]
  node [
    id 1419
    label "ugija"
  ]
  node [
    id 1420
    label "szyling_kenijski"
  ]
  node [
    id 1421
    label "Nachiczewan"
  ]
  node [
    id 1422
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1423
    label "manat_azerski"
  ]
  node [
    id 1424
    label "Karabach"
  ]
  node [
    id 1425
    label "Bengal"
  ]
  node [
    id 1426
    label "taka"
  ]
  node [
    id 1427
    label "Ocean_Spokojny"
  ]
  node [
    id 1428
    label "dolar_Kiribati"
  ]
  node [
    id 1429
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1430
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1431
    label "Cebu"
  ]
  node [
    id 1432
    label "Atlantyk"
  ]
  node [
    id 1433
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1434
    label "Ulster"
  ]
  node [
    id 1435
    label "funt_irlandzki"
  ]
  node [
    id 1436
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1437
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1438
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1439
    label "cedi"
  ]
  node [
    id 1440
    label "ariary"
  ]
  node [
    id 1441
    label "Ocean_Indyjski"
  ]
  node [
    id 1442
    label "frank_malgaski"
  ]
  node [
    id 1443
    label "Estremadura"
  ]
  node [
    id 1444
    label "Andaluzja"
  ]
  node [
    id 1445
    label "Kastylia"
  ]
  node [
    id 1446
    label "Galicja"
  ]
  node [
    id 1447
    label "Aragonia"
  ]
  node [
    id 1448
    label "hacjender"
  ]
  node [
    id 1449
    label "Asturia"
  ]
  node [
    id 1450
    label "Baskonia"
  ]
  node [
    id 1451
    label "Majorka"
  ]
  node [
    id 1452
    label "Walencja"
  ]
  node [
    id 1453
    label "peseta"
  ]
  node [
    id 1454
    label "Katalonia"
  ]
  node [
    id 1455
    label "peso_chilijskie"
  ]
  node [
    id 1456
    label "Indie_Zachodnie"
  ]
  node [
    id 1457
    label "Sikkim"
  ]
  node [
    id 1458
    label "Asam"
  ]
  node [
    id 1459
    label "rupia_indyjska"
  ]
  node [
    id 1460
    label "Indie_Portugalskie"
  ]
  node [
    id 1461
    label "Indie_Wschodnie"
  ]
  node [
    id 1462
    label "Kerala"
  ]
  node [
    id 1463
    label "Bollywood"
  ]
  node [
    id 1464
    label "jen"
  ]
  node [
    id 1465
    label "jinja"
  ]
  node [
    id 1466
    label "Okinawa"
  ]
  node [
    id 1467
    label "Japonica"
  ]
  node [
    id 1468
    label "Rugia"
  ]
  node [
    id 1469
    label "Saksonia"
  ]
  node [
    id 1470
    label "Dolna_Saksonia"
  ]
  node [
    id 1471
    label "Anglosas"
  ]
  node [
    id 1472
    label "Hesja"
  ]
  node [
    id 1473
    label "Szlezwik"
  ]
  node [
    id 1474
    label "Wirtembergia"
  ]
  node [
    id 1475
    label "Po&#322;abie"
  ]
  node [
    id 1476
    label "Germania"
  ]
  node [
    id 1477
    label "Frankonia"
  ]
  node [
    id 1478
    label "Badenia"
  ]
  node [
    id 1479
    label "Holsztyn"
  ]
  node [
    id 1480
    label "Bawaria"
  ]
  node [
    id 1481
    label "marka"
  ]
  node [
    id 1482
    label "Brandenburgia"
  ]
  node [
    id 1483
    label "Szwabia"
  ]
  node [
    id 1484
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1485
    label "Nadrenia"
  ]
  node [
    id 1486
    label "Westfalia"
  ]
  node [
    id 1487
    label "Turyngia"
  ]
  node [
    id 1488
    label "Helgoland"
  ]
  node [
    id 1489
    label "Karlsbad"
  ]
  node [
    id 1490
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1491
    label "Piemont"
  ]
  node [
    id 1492
    label "Lombardia"
  ]
  node [
    id 1493
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1494
    label "Kalabria"
  ]
  node [
    id 1495
    label "Sardynia"
  ]
  node [
    id 1496
    label "Italia"
  ]
  node [
    id 1497
    label "Ok&#281;cie"
  ]
  node [
    id 1498
    label "Kampania"
  ]
  node [
    id 1499
    label "Karyntia"
  ]
  node [
    id 1500
    label "Umbria"
  ]
  node [
    id 1501
    label "Romania"
  ]
  node [
    id 1502
    label "Warszawa"
  ]
  node [
    id 1503
    label "lir"
  ]
  node [
    id 1504
    label "Toskania"
  ]
  node [
    id 1505
    label "Apulia"
  ]
  node [
    id 1506
    label "Liguria"
  ]
  node [
    id 1507
    label "Sycylia"
  ]
  node [
    id 1508
    label "Dacja"
  ]
  node [
    id 1509
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1510
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1511
    label "Ba&#322;kany"
  ]
  node [
    id 1512
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1513
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1514
    label "funt_syryjski"
  ]
  node [
    id 1515
    label "alawizm"
  ]
  node [
    id 1516
    label "frank_rwandyjski"
  ]
  node [
    id 1517
    label "dinar_Bahrajnu"
  ]
  node [
    id 1518
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1519
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1520
    label "frank_luksemburski"
  ]
  node [
    id 1521
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1522
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1523
    label "frank_monakijski"
  ]
  node [
    id 1524
    label "dinar_algierski"
  ]
  node [
    id 1525
    label "Kabylia"
  ]
  node [
    id 1526
    label "Oceania"
  ]
  node [
    id 1527
    label "Wojwodina"
  ]
  node [
    id 1528
    label "Sand&#380;ak"
  ]
  node [
    id 1529
    label "dinar_serbski"
  ]
  node [
    id 1530
    label "Orinoko"
  ]
  node [
    id 1531
    label "boliwar"
  ]
  node [
    id 1532
    label "tenge"
  ]
  node [
    id 1533
    label "frank_alba&#324;ski"
  ]
  node [
    id 1534
    label "lek"
  ]
  node [
    id 1535
    label "dolar_Barbadosu"
  ]
  node [
    id 1536
    label "Antyle"
  ]
  node [
    id 1537
    label "kyat"
  ]
  node [
    id 1538
    label "Arakan"
  ]
  node [
    id 1539
    label "c&#243;rdoba"
  ]
  node [
    id 1540
    label "Paros"
  ]
  node [
    id 1541
    label "Epir"
  ]
  node [
    id 1542
    label "panhellenizm"
  ]
  node [
    id 1543
    label "Eubea"
  ]
  node [
    id 1544
    label "Rodos"
  ]
  node [
    id 1545
    label "Achaja"
  ]
  node [
    id 1546
    label "Termopile"
  ]
  node [
    id 1547
    label "Attyka"
  ]
  node [
    id 1548
    label "Hellada"
  ]
  node [
    id 1549
    label "Etolia"
  ]
  node [
    id 1550
    label "palestra"
  ]
  node [
    id 1551
    label "Kreta"
  ]
  node [
    id 1552
    label "drachma"
  ]
  node [
    id 1553
    label "Olimp"
  ]
  node [
    id 1554
    label "Tesalia"
  ]
  node [
    id 1555
    label "Peloponez"
  ]
  node [
    id 1556
    label "Eolia"
  ]
  node [
    id 1557
    label "Beocja"
  ]
  node [
    id 1558
    label "Parnas"
  ]
  node [
    id 1559
    label "Lesbos"
  ]
  node [
    id 1560
    label "Mariany"
  ]
  node [
    id 1561
    label "Salzburg"
  ]
  node [
    id 1562
    label "Rakuzy"
  ]
  node [
    id 1563
    label "Tyrol"
  ]
  node [
    id 1564
    label "konsulent"
  ]
  node [
    id 1565
    label "szyling_austryjacki"
  ]
  node [
    id 1566
    label "Amhara"
  ]
  node [
    id 1567
    label "birr"
  ]
  node [
    id 1568
    label "Syjon"
  ]
  node [
    id 1569
    label "negus"
  ]
  node [
    id 1570
    label "Jawa"
  ]
  node [
    id 1571
    label "Sumatra"
  ]
  node [
    id 1572
    label "rupia_indonezyjska"
  ]
  node [
    id 1573
    label "Nowa_Gwinea"
  ]
  node [
    id 1574
    label "Moluki"
  ]
  node [
    id 1575
    label "boliviano"
  ]
  node [
    id 1576
    label "Lotaryngia"
  ]
  node [
    id 1577
    label "Bordeaux"
  ]
  node [
    id 1578
    label "Pikardia"
  ]
  node [
    id 1579
    label "Alzacja"
  ]
  node [
    id 1580
    label "Masyw_Centralny"
  ]
  node [
    id 1581
    label "Akwitania"
  ]
  node [
    id 1582
    label "Sekwana"
  ]
  node [
    id 1583
    label "Langwedocja"
  ]
  node [
    id 1584
    label "Armagnac"
  ]
  node [
    id 1585
    label "Martynika"
  ]
  node [
    id 1586
    label "Bretania"
  ]
  node [
    id 1587
    label "Sabaudia"
  ]
  node [
    id 1588
    label "Korsyka"
  ]
  node [
    id 1589
    label "Normandia"
  ]
  node [
    id 1590
    label "Gaskonia"
  ]
  node [
    id 1591
    label "Burgundia"
  ]
  node [
    id 1592
    label "frank_francuski"
  ]
  node [
    id 1593
    label "Wandea"
  ]
  node [
    id 1594
    label "Prowansja"
  ]
  node [
    id 1595
    label "Gwadelupa"
  ]
  node [
    id 1596
    label "somoni"
  ]
  node [
    id 1597
    label "Melanezja"
  ]
  node [
    id 1598
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1599
    label "funt_cypryjski"
  ]
  node [
    id 1600
    label "Afrodyzje"
  ]
  node [
    id 1601
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1602
    label "Fryburg"
  ]
  node [
    id 1603
    label "Bazylea"
  ]
  node [
    id 1604
    label "Alpy"
  ]
  node [
    id 1605
    label "Helwecja"
  ]
  node [
    id 1606
    label "Berno"
  ]
  node [
    id 1607
    label "sum"
  ]
  node [
    id 1608
    label "Karaka&#322;pacja"
  ]
  node [
    id 1609
    label "Kurlandia"
  ]
  node [
    id 1610
    label "Windawa"
  ]
  node [
    id 1611
    label "&#322;at"
  ]
  node [
    id 1612
    label "Liwonia"
  ]
  node [
    id 1613
    label "rubel_&#322;otewski"
  ]
  node [
    id 1614
    label "Inflanty"
  ]
  node [
    id 1615
    label "&#379;mud&#378;"
  ]
  node [
    id 1616
    label "lit"
  ]
  node [
    id 1617
    label "frank_tunezyjski"
  ]
  node [
    id 1618
    label "dinar_tunezyjski"
  ]
  node [
    id 1619
    label "lempira"
  ]
  node [
    id 1620
    label "korona_w&#281;gierska"
  ]
  node [
    id 1621
    label "forint"
  ]
  node [
    id 1622
    label "Lipt&#243;w"
  ]
  node [
    id 1623
    label "dong"
  ]
  node [
    id 1624
    label "Annam"
  ]
  node [
    id 1625
    label "Tonkin"
  ]
  node [
    id 1626
    label "lud"
  ]
  node [
    id 1627
    label "frank_kongijski"
  ]
  node [
    id 1628
    label "szyling_somalijski"
  ]
  node [
    id 1629
    label "cruzado"
  ]
  node [
    id 1630
    label "real"
  ]
  node [
    id 1631
    label "Podole"
  ]
  node [
    id 1632
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1633
    label "Wsch&#243;d"
  ]
  node [
    id 1634
    label "Zakarpacie"
  ]
  node [
    id 1635
    label "Naddnieprze"
  ]
  node [
    id 1636
    label "Ma&#322;orosja"
  ]
  node [
    id 1637
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1638
    label "Nadbu&#380;e"
  ]
  node [
    id 1639
    label "hrywna"
  ]
  node [
    id 1640
    label "Zaporo&#380;e"
  ]
  node [
    id 1641
    label "Krym"
  ]
  node [
    id 1642
    label "Dniestr"
  ]
  node [
    id 1643
    label "Przykarpacie"
  ]
  node [
    id 1644
    label "Kozaczyzna"
  ]
  node [
    id 1645
    label "karbowaniec"
  ]
  node [
    id 1646
    label "Tasmania"
  ]
  node [
    id 1647
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1648
    label "dolar_australijski"
  ]
  node [
    id 1649
    label "gourde"
  ]
  node [
    id 1650
    label "escudo_angolskie"
  ]
  node [
    id 1651
    label "kwanza"
  ]
  node [
    id 1652
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1653
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1654
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1655
    label "Ad&#380;aria"
  ]
  node [
    id 1656
    label "lari"
  ]
  node [
    id 1657
    label "naira"
  ]
  node [
    id 1658
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1659
    label "Ohio"
  ]
  node [
    id 1660
    label "P&#243;&#322;noc"
  ]
  node [
    id 1661
    label "Nowy_York"
  ]
  node [
    id 1662
    label "Illinois"
  ]
  node [
    id 1663
    label "Po&#322;udnie"
  ]
  node [
    id 1664
    label "Kalifornia"
  ]
  node [
    id 1665
    label "Wirginia"
  ]
  node [
    id 1666
    label "Teksas"
  ]
  node [
    id 1667
    label "Waszyngton"
  ]
  node [
    id 1668
    label "zielona_karta"
  ]
  node [
    id 1669
    label "Alaska"
  ]
  node [
    id 1670
    label "Massachusetts"
  ]
  node [
    id 1671
    label "Hawaje"
  ]
  node [
    id 1672
    label "Maryland"
  ]
  node [
    id 1673
    label "Michigan"
  ]
  node [
    id 1674
    label "Arizona"
  ]
  node [
    id 1675
    label "Georgia"
  ]
  node [
    id 1676
    label "stan_wolny"
  ]
  node [
    id 1677
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1678
    label "Pensylwania"
  ]
  node [
    id 1679
    label "Luizjana"
  ]
  node [
    id 1680
    label "Nowy_Meksyk"
  ]
  node [
    id 1681
    label "Wuj_Sam"
  ]
  node [
    id 1682
    label "Alabama"
  ]
  node [
    id 1683
    label "Kansas"
  ]
  node [
    id 1684
    label "Oregon"
  ]
  node [
    id 1685
    label "Zach&#243;d"
  ]
  node [
    id 1686
    label "Oklahoma"
  ]
  node [
    id 1687
    label "Floryda"
  ]
  node [
    id 1688
    label "Hudson"
  ]
  node [
    id 1689
    label "som"
  ]
  node [
    id 1690
    label "peso_urugwajskie"
  ]
  node [
    id 1691
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1692
    label "dolar_Brunei"
  ]
  node [
    id 1693
    label "rial_ira&#324;ski"
  ]
  node [
    id 1694
    label "mu&#322;&#322;a"
  ]
  node [
    id 1695
    label "Persja"
  ]
  node [
    id 1696
    label "d&#380;amahirijja"
  ]
  node [
    id 1697
    label "dinar_libijski"
  ]
  node [
    id 1698
    label "nakfa"
  ]
  node [
    id 1699
    label "rial_katarski"
  ]
  node [
    id 1700
    label "quetzal"
  ]
  node [
    id 1701
    label "won"
  ]
  node [
    id 1702
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1703
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1704
    label "guarani"
  ]
  node [
    id 1705
    label "perper"
  ]
  node [
    id 1706
    label "dinar_kuwejcki"
  ]
  node [
    id 1707
    label "dalasi"
  ]
  node [
    id 1708
    label "dolar_Zimbabwe"
  ]
  node [
    id 1709
    label "Szantung"
  ]
  node [
    id 1710
    label "Chiny_Zachodnie"
  ]
  node [
    id 1711
    label "Kuantung"
  ]
  node [
    id 1712
    label "D&#380;ungaria"
  ]
  node [
    id 1713
    label "yuan"
  ]
  node [
    id 1714
    label "Hongkong"
  ]
  node [
    id 1715
    label "Chiny_Wschodnie"
  ]
  node [
    id 1716
    label "Guangdong"
  ]
  node [
    id 1717
    label "Junnan"
  ]
  node [
    id 1718
    label "Mand&#380;uria"
  ]
  node [
    id 1719
    label "Syczuan"
  ]
  node [
    id 1720
    label "Mazowsze"
  ]
  node [
    id 1721
    label "Pa&#322;uki"
  ]
  node [
    id 1722
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1723
    label "Powi&#347;le"
  ]
  node [
    id 1724
    label "Wolin"
  ]
  node [
    id 1725
    label "z&#322;oty"
  ]
  node [
    id 1726
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1727
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1728
    label "So&#322;a"
  ]
  node [
    id 1729
    label "Krajna"
  ]
  node [
    id 1730
    label "Opolskie"
  ]
  node [
    id 1731
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1732
    label "Suwalszczyzna"
  ]
  node [
    id 1733
    label "barwy_polskie"
  ]
  node [
    id 1734
    label "Podlasie"
  ]
  node [
    id 1735
    label "Ma&#322;opolska"
  ]
  node [
    id 1736
    label "Warmia"
  ]
  node [
    id 1737
    label "Mazury"
  ]
  node [
    id 1738
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1739
    label "Kaczawa"
  ]
  node [
    id 1740
    label "Lubelszczyzna"
  ]
  node [
    id 1741
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1742
    label "Kielecczyzna"
  ]
  node [
    id 1743
    label "Lubuskie"
  ]
  node [
    id 1744
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1745
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1746
    label "Kujawy"
  ]
  node [
    id 1747
    label "Podkarpacie"
  ]
  node [
    id 1748
    label "Wielkopolska"
  ]
  node [
    id 1749
    label "Wis&#322;a"
  ]
  node [
    id 1750
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1751
    label "Bory_Tucholskie"
  ]
  node [
    id 1752
    label "lira_turecka"
  ]
  node [
    id 1753
    label "Azja_Mniejsza"
  ]
  node [
    id 1754
    label "Ujgur"
  ]
  node [
    id 1755
    label "kuna"
  ]
  node [
    id 1756
    label "dram"
  ]
  node [
    id 1757
    label "tala"
  ]
  node [
    id 1758
    label "korona_s&#322;owacka"
  ]
  node [
    id 1759
    label "Turiec"
  ]
  node [
    id 1760
    label "Himalaje"
  ]
  node [
    id 1761
    label "rupia_nepalska"
  ]
  node [
    id 1762
    label "frank_gwinejski"
  ]
  node [
    id 1763
    label "korona_esto&#324;ska"
  ]
  node [
    id 1764
    label "Skandynawia"
  ]
  node [
    id 1765
    label "marka_esto&#324;ska"
  ]
  node [
    id 1766
    label "Quebec"
  ]
  node [
    id 1767
    label "dolar_kanadyjski"
  ]
  node [
    id 1768
    label "Nowa_Fundlandia"
  ]
  node [
    id 1769
    label "Zanzibar"
  ]
  node [
    id 1770
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1771
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1772
    label "&#346;wite&#378;"
  ]
  node [
    id 1773
    label "peso_kolumbijskie"
  ]
  node [
    id 1774
    label "Synaj"
  ]
  node [
    id 1775
    label "paraszyt"
  ]
  node [
    id 1776
    label "funt_egipski"
  ]
  node [
    id 1777
    label "szach"
  ]
  node [
    id 1778
    label "Baktria"
  ]
  node [
    id 1779
    label "afgani"
  ]
  node [
    id 1780
    label "baht"
  ]
  node [
    id 1781
    label "tolar"
  ]
  node [
    id 1782
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1783
    label "Naddniestrze"
  ]
  node [
    id 1784
    label "Gagauzja"
  ]
  node [
    id 1785
    label "Anglia"
  ]
  node [
    id 1786
    label "Amazonia"
  ]
  node [
    id 1787
    label "plantowa&#263;"
  ]
  node [
    id 1788
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1789
    label "zapadnia"
  ]
  node [
    id 1790
    label "Zamojszczyzna"
  ]
  node [
    id 1791
    label "budynek"
  ]
  node [
    id 1792
    label "skorupa_ziemska"
  ]
  node [
    id 1793
    label "Turkiestan"
  ]
  node [
    id 1794
    label "Noworosja"
  ]
  node [
    id 1795
    label "Mezoameryka"
  ]
  node [
    id 1796
    label "glinowanie"
  ]
  node [
    id 1797
    label "Kurdystan"
  ]
  node [
    id 1798
    label "martwica"
  ]
  node [
    id 1799
    label "Szkocja"
  ]
  node [
    id 1800
    label "litosfera"
  ]
  node [
    id 1801
    label "penetrator"
  ]
  node [
    id 1802
    label "glinowa&#263;"
  ]
  node [
    id 1803
    label "Zabajkale"
  ]
  node [
    id 1804
    label "domain"
  ]
  node [
    id 1805
    label "Bojkowszczyzna"
  ]
  node [
    id 1806
    label "podglebie"
  ]
  node [
    id 1807
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1808
    label "Pamir"
  ]
  node [
    id 1809
    label "Indochiny"
  ]
  node [
    id 1810
    label "Kurpie"
  ]
  node [
    id 1811
    label "S&#261;decczyzna"
  ]
  node [
    id 1812
    label "kort"
  ]
  node [
    id 1813
    label "czynnik_produkcji"
  ]
  node [
    id 1814
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1815
    label "Huculszczyzna"
  ]
  node [
    id 1816
    label "pojazd"
  ]
  node [
    id 1817
    label "powierzchnia"
  ]
  node [
    id 1818
    label "Podhale"
  ]
  node [
    id 1819
    label "pr&#243;chnica"
  ]
  node [
    id 1820
    label "Hercegowina"
  ]
  node [
    id 1821
    label "Walia"
  ]
  node [
    id 1822
    label "pomieszczenie"
  ]
  node [
    id 1823
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1824
    label "ryzosfera"
  ]
  node [
    id 1825
    label "Kaukaz"
  ]
  node [
    id 1826
    label "Biskupizna"
  ]
  node [
    id 1827
    label "Bo&#347;nia"
  ]
  node [
    id 1828
    label "p&#322;aszczyzna"
  ]
  node [
    id 1829
    label "dotleni&#263;"
  ]
  node [
    id 1830
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1831
    label "Opolszczyzna"
  ]
  node [
    id 1832
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1833
    label "Podbeskidzie"
  ]
  node [
    id 1834
    label "Kaszuby"
  ]
  node [
    id 1835
    label "Ko&#322;yma"
  ]
  node [
    id 1836
    label "glej"
  ]
  node [
    id 1837
    label "posadzka"
  ]
  node [
    id 1838
    label "Polesie"
  ]
  node [
    id 1839
    label "Palestyna"
  ]
  node [
    id 1840
    label "Lauda"
  ]
  node [
    id 1841
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1842
    label "Laponia"
  ]
  node [
    id 1843
    label "Yorkshire"
  ]
  node [
    id 1844
    label "Zag&#243;rze"
  ]
  node [
    id 1845
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1846
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1847
    label "Oksytania"
  ]
  node [
    id 1848
    label "Kociewie"
  ]
  node [
    id 1849
    label "podziemnie"
  ]
  node [
    id 1850
    label "konspiracyjnie"
  ]
  node [
    id 1851
    label "tajny"
  ]
  node [
    id 1852
    label "konspiracyjny"
  ]
  node [
    id 1853
    label "tajnie"
  ]
  node [
    id 1854
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1855
    label "tajemniczo"
  ]
  node [
    id 1856
    label "konspiratorski"
  ]
  node [
    id 1857
    label "niejawny"
  ]
  node [
    id 1858
    label "sekretny"
  ]
  node [
    id 1859
    label "nieoficjalny"
  ]
  node [
    id 1860
    label "intymny"
  ]
  node [
    id 1861
    label "utajnianie"
  ]
  node [
    id 1862
    label "tajno"
  ]
  node [
    id 1863
    label "utajnienie"
  ]
  node [
    id 1864
    label "kompletnie"
  ]
  node [
    id 1865
    label "kompletny"
  ]
  node [
    id 1866
    label "zupe&#322;nie"
  ]
  node [
    id 1867
    label "skali&#263;"
  ]
  node [
    id 1868
    label "defile"
  ]
  node [
    id 1869
    label "zniewa&#380;y&#263;"
  ]
  node [
    id 1870
    label "obrazi&#263;"
  ]
  node [
    id 1871
    label "take_down"
  ]
  node [
    id 1872
    label "zabrudzi&#263;"
  ]
  node [
    id 1873
    label "zbruka&#263;"
  ]
  node [
    id 1874
    label "zanieczy&#347;ci&#263;"
  ]
  node [
    id 1875
    label "poziomka"
  ]
  node [
    id 1876
    label "zielony"
  ]
  node [
    id 1877
    label "owocowy"
  ]
  node [
    id 1878
    label "je&#380;yna"
  ]
  node [
    id 1879
    label "drzewny"
  ]
  node [
    id 1880
    label "bor&#243;wka"
  ]
  node [
    id 1881
    label "&#347;wie&#380;y"
  ]
  node [
    id 1882
    label "nowy"
  ]
  node [
    id 1883
    label "jasny"
  ]
  node [
    id 1884
    label "&#347;wie&#380;o"
  ]
  node [
    id 1885
    label "dobry"
  ]
  node [
    id 1886
    label "surowy"
  ]
  node [
    id 1887
    label "orze&#378;wienie"
  ]
  node [
    id 1888
    label "orze&#378;wianie"
  ]
  node [
    id 1889
    label "rze&#347;ki"
  ]
  node [
    id 1890
    label "zdrowy"
  ]
  node [
    id 1891
    label "czysty"
  ]
  node [
    id 1892
    label "oryginalnie"
  ]
  node [
    id 1893
    label "przyjemny"
  ]
  node [
    id 1894
    label "o&#380;ywczy"
  ]
  node [
    id 1895
    label "m&#322;ody"
  ]
  node [
    id 1896
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 1897
    label "inny"
  ]
  node [
    id 1898
    label "&#380;ywy"
  ]
  node [
    id 1899
    label "soczysty"
  ]
  node [
    id 1900
    label "nowotny"
  ]
  node [
    id 1901
    label "drewniany"
  ]
  node [
    id 1902
    label "drewny"
  ]
  node [
    id 1903
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 1904
    label "majny"
  ]
  node [
    id 1905
    label "Bonaire"
  ]
  node [
    id 1906
    label "Sint_Eustatius"
  ]
  node [
    id 1907
    label "zzielenienie"
  ]
  node [
    id 1908
    label "zielono"
  ]
  node [
    id 1909
    label "dzia&#322;acz"
  ]
  node [
    id 1910
    label "zazielenianie"
  ]
  node [
    id 1911
    label "zazielenienie"
  ]
  node [
    id 1912
    label "niedojrza&#322;y"
  ]
  node [
    id 1913
    label "pokryty"
  ]
  node [
    id 1914
    label "socjalista"
  ]
  node [
    id 1915
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 1916
    label "Saba"
  ]
  node [
    id 1917
    label "zwolennik"
  ]
  node [
    id 1918
    label "polityk"
  ]
  node [
    id 1919
    label "zieloni"
  ]
  node [
    id 1920
    label "naturalny"
  ]
  node [
    id 1921
    label "blady"
  ]
  node [
    id 1922
    label "zielenienie"
  ]
  node [
    id 1923
    label "ch&#322;odny"
  ]
  node [
    id 1924
    label "ro&#347;linny"
  ]
  node [
    id 1925
    label "u&#380;ytkowy"
  ]
  node [
    id 1926
    label "przypominaj&#261;cy"
  ]
  node [
    id 1927
    label "s&#322;odki"
  ]
  node [
    id 1928
    label "owocowo"
  ]
  node [
    id 1929
    label "bylina"
  ]
  node [
    id 1930
    label "owoc"
  ]
  node [
    id 1931
    label "wieloorzeszkowiec"
  ]
  node [
    id 1932
    label "r&#243;&#380;owate"
  ]
  node [
    id 1933
    label "wielopestkowiec"
  ]
  node [
    id 1934
    label "o&#380;yna"
  ]
  node [
    id 1935
    label "sweetbrier"
  ]
  node [
    id 1936
    label "krzew_owocowy"
  ]
  node [
    id 1937
    label "ro&#347;lina_oligotroficzna"
  ]
  node [
    id 1938
    label "wrzosowate"
  ]
  node [
    id 1939
    label "krzewinka"
  ]
  node [
    id 1940
    label "jagoda"
  ]
  node [
    id 1941
    label "cognizance"
  ]
  node [
    id 1942
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1943
    label "mie&#263;_miejsce"
  ]
  node [
    id 1944
    label "equal"
  ]
  node [
    id 1945
    label "trwa&#263;"
  ]
  node [
    id 1946
    label "si&#281;ga&#263;"
  ]
  node [
    id 1947
    label "obecno&#347;&#263;"
  ]
  node [
    id 1948
    label "stand"
  ]
  node [
    id 1949
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1950
    label "uczestniczy&#263;"
  ]
  node [
    id 1951
    label "participate"
  ]
  node [
    id 1952
    label "robi&#263;"
  ]
  node [
    id 1953
    label "istnie&#263;"
  ]
  node [
    id 1954
    label "pozostawa&#263;"
  ]
  node [
    id 1955
    label "zostawa&#263;"
  ]
  node [
    id 1956
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1957
    label "adhere"
  ]
  node [
    id 1958
    label "compass"
  ]
  node [
    id 1959
    label "korzysta&#263;"
  ]
  node [
    id 1960
    label "appreciation"
  ]
  node [
    id 1961
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1962
    label "dociera&#263;"
  ]
  node [
    id 1963
    label "get"
  ]
  node [
    id 1964
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1965
    label "mierzy&#263;"
  ]
  node [
    id 1966
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1967
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1968
    label "exsert"
  ]
  node [
    id 1969
    label "being"
  ]
  node [
    id 1970
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1971
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1972
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1973
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1974
    label "run"
  ]
  node [
    id 1975
    label "bangla&#263;"
  ]
  node [
    id 1976
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1977
    label "przebiega&#263;"
  ]
  node [
    id 1978
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1979
    label "proceed"
  ]
  node [
    id 1980
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1981
    label "carry"
  ]
  node [
    id 1982
    label "bywa&#263;"
  ]
  node [
    id 1983
    label "dziama&#263;"
  ]
  node [
    id 1984
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1985
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1986
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1987
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1988
    label "krok"
  ]
  node [
    id 1989
    label "tryb"
  ]
  node [
    id 1990
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1991
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1992
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1993
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1994
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1995
    label "continue"
  ]
  node [
    id 1996
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1997
    label "wci&#281;cie"
  ]
  node [
    id 1998
    label "warstwa"
  ]
  node [
    id 1999
    label "samopoczucie"
  ]
  node [
    id 2000
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 2001
    label "state"
  ]
  node [
    id 2002
    label "wektor"
  ]
  node [
    id 2003
    label "Goa"
  ]
  node [
    id 2004
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 2005
    label "poziom"
  ]
  node [
    id 2006
    label "shape"
  ]
  node [
    id 2007
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 2008
    label "partyzantka"
  ]
  node [
    id 2009
    label "bojownik"
  ]
  node [
    id 2010
    label "stronnik"
  ]
  node [
    id 2011
    label "wojna_podjazdowa"
  ]
  node [
    id 2012
    label "s&#322;onki"
  ]
  node [
    id 2013
    label "ptak_w&#281;drowny"
  ]
  node [
    id 2014
    label "ryba"
  ]
  node [
    id 2015
    label "walczenie"
  ]
  node [
    id 2016
    label "guramiowate"
  ]
  node [
    id 2017
    label "ptak_wodno-b&#322;otny"
  ]
  node [
    id 2018
    label "pobratymiec"
  ]
  node [
    id 2019
    label "guerrilla"
  ]
  node [
    id 2020
    label "woluntaryzm"
  ]
  node [
    id 2021
    label "bliski"
  ]
  node [
    id 2022
    label "poblisko"
  ]
  node [
    id 2023
    label "blisko"
  ]
  node [
    id 2024
    label "znajomy"
  ]
  node [
    id 2025
    label "zwi&#261;zany"
  ]
  node [
    id 2026
    label "przesz&#322;y"
  ]
  node [
    id 2027
    label "silny"
  ]
  node [
    id 2028
    label "zbli&#380;enie"
  ]
  node [
    id 2029
    label "kr&#243;tki"
  ]
  node [
    id 2030
    label "oddalony"
  ]
  node [
    id 2031
    label "dok&#322;adny"
  ]
  node [
    id 2032
    label "nieodleg&#322;y"
  ]
  node [
    id 2033
    label "przysz&#322;y"
  ]
  node [
    id 2034
    label "gotowy"
  ]
  node [
    id 2035
    label "ma&#322;y"
  ]
  node [
    id 2036
    label "miesi&#261;c"
  ]
  node [
    id 2037
    label "tydzie&#324;"
  ]
  node [
    id 2038
    label "miech"
  ]
  node [
    id 2039
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 2040
    label "rok"
  ]
  node [
    id 2041
    label "kalendy"
  ]
  node [
    id 2042
    label "formacja"
  ]
  node [
    id 2043
    label "yearbook"
  ]
  node [
    id 2044
    label "czasopismo"
  ]
  node [
    id 2045
    label "kronika"
  ]
  node [
    id 2046
    label "leksem"
  ]
  node [
    id 2047
    label "zespolik"
  ]
  node [
    id 2048
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 2049
    label "rugby"
  ]
  node [
    id 2050
    label "The_Beatles"
  ]
  node [
    id 2051
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 2052
    label "Depeche_Mode"
  ]
  node [
    id 2053
    label "forma"
  ]
  node [
    id 2054
    label "zapis"
  ]
  node [
    id 2055
    label "chronograf"
  ]
  node [
    id 2056
    label "latopis"
  ]
  node [
    id 2057
    label "ksi&#281;ga"
  ]
  node [
    id 2058
    label "psychotest"
  ]
  node [
    id 2059
    label "communication"
  ]
  node [
    id 2060
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 2061
    label "wk&#322;ad"
  ]
  node [
    id 2062
    label "zajawka"
  ]
  node [
    id 2063
    label "ok&#322;adka"
  ]
  node [
    id 2064
    label "Zwrotnica"
  ]
  node [
    id 2065
    label "dzia&#322;"
  ]
  node [
    id 2066
    label "prasa"
  ]
  node [
    id 2067
    label "batalista"
  ]
  node [
    id 2068
    label "bitwa_pod_Pi&#322;awcami"
  ]
  node [
    id 2069
    label "ploy"
  ]
  node [
    id 2070
    label "doj&#347;cie"
  ]
  node [
    id 2071
    label "skrycie_si&#281;"
  ]
  node [
    id 2072
    label "odwiedzenie"
  ]
  node [
    id 2073
    label "zakrycie"
  ]
  node [
    id 2074
    label "happening"
  ]
  node [
    id 2075
    label "porobienie_si&#281;"
  ]
  node [
    id 2076
    label "krajobraz"
  ]
  node [
    id 2077
    label "zaniesienie"
  ]
  node [
    id 2078
    label "przyobleczenie_si&#281;"
  ]
  node [
    id 2079
    label "event"
  ]
  node [
    id 2080
    label "entrance"
  ]
  node [
    id 2081
    label "podej&#347;cie"
  ]
  node [
    id 2082
    label "przestanie"
  ]
  node [
    id 2083
    label "artysta"
  ]
  node [
    id 2084
    label "say_farewell"
  ]
  node [
    id 2085
    label "balkon"
  ]
  node [
    id 2086
    label "budowla"
  ]
  node [
    id 2087
    label "pod&#322;oga"
  ]
  node [
    id 2088
    label "kondygnacja"
  ]
  node [
    id 2089
    label "skrzyd&#322;o"
  ]
  node [
    id 2090
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 2091
    label "dach"
  ]
  node [
    id 2092
    label "strop"
  ]
  node [
    id 2093
    label "klatka_schodowa"
  ]
  node [
    id 2094
    label "przedpro&#380;e"
  ]
  node [
    id 2095
    label "Pentagon"
  ]
  node [
    id 2096
    label "alkierz"
  ]
  node [
    id 2097
    label "front"
  ]
  node [
    id 2098
    label "ii"
  ]
  node [
    id 2099
    label "polskie"
  ]
  node [
    id 2100
    label "regionalny"
  ]
  node [
    id 2101
    label "dyrekcja"
  ]
  node [
    id 2102
    label "las"
  ]
  node [
    id 2103
    label "pa&#324;stwowy"
  ]
  node [
    id 2104
    label "drogi"
  ]
  node [
    id 2105
    label "krajowy"
  ]
  node [
    id 2106
    label "nr"
  ]
  node [
    id 2107
    label "7"
  ]
  node [
    id 2108
    label "Virtuti"
  ]
  node [
    id 2109
    label "Militari"
  ]
  node [
    id 2110
    label "urz&#261;d"
  ]
  node [
    id 2111
    label "wojew&#243;dzki"
  ]
  node [
    id 2112
    label "miasto"
  ]
  node [
    id 2113
    label "skar&#380;yski"
  ]
  node [
    id 2114
    label "kamienny"
  ]
  node [
    id 2115
    label "Ewa"
  ]
  node [
    id 2116
    label "Kierzkowska"
  ]
  node [
    id 2117
    label "Krzysztofa"
  ]
  node [
    id 2118
    label "Jurgiel"
  ]
  node [
    id 2119
    label "i"
  ]
  node [
    id 2120
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 2121
    label "Sawicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 2098
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 2098
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 894
  ]
  edge [
    source 9
    target 895
  ]
  edge [
    source 9
    target 896
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 897
  ]
  edge [
    source 9
    target 898
  ]
  edge [
    source 9
    target 899
  ]
  edge [
    source 9
    target 900
  ]
  edge [
    source 9
    target 901
  ]
  edge [
    source 9
    target 902
  ]
  edge [
    source 9
    target 903
  ]
  edge [
    source 9
    target 904
  ]
  edge [
    source 9
    target 905
  ]
  edge [
    source 9
    target 906
  ]
  edge [
    source 9
    target 907
  ]
  edge [
    source 9
    target 908
  ]
  edge [
    source 9
    target 909
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 910
  ]
  edge [
    source 9
    target 911
  ]
  edge [
    source 9
    target 912
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 913
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 914
  ]
  edge [
    source 9
    target 915
  ]
  edge [
    source 9
    target 916
  ]
  edge [
    source 9
    target 917
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 918
  ]
  edge [
    source 9
    target 919
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 921
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 923
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 924
  ]
  edge [
    source 9
    target 925
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 933
  ]
  edge [
    source 9
    target 934
  ]
  edge [
    source 9
    target 935
  ]
  edge [
    source 9
    target 936
  ]
  edge [
    source 9
    target 937
  ]
  edge [
    source 9
    target 938
  ]
  edge [
    source 9
    target 939
  ]
  edge [
    source 9
    target 940
  ]
  edge [
    source 9
    target 941
  ]
  edge [
    source 9
    target 942
  ]
  edge [
    source 9
    target 943
  ]
  edge [
    source 9
    target 944
  ]
  edge [
    source 9
    target 945
  ]
  edge [
    source 9
    target 946
  ]
  edge [
    source 9
    target 947
  ]
  edge [
    source 9
    target 948
  ]
  edge [
    source 9
    target 949
  ]
  edge [
    source 9
    target 950
  ]
  edge [
    source 9
    target 951
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 952
  ]
  edge [
    source 10
    target 953
  ]
  edge [
    source 10
    target 954
  ]
  edge [
    source 10
    target 955
  ]
  edge [
    source 10
    target 956
  ]
  edge [
    source 10
    target 957
  ]
  edge [
    source 10
    target 958
  ]
  edge [
    source 10
    target 959
  ]
  edge [
    source 10
    target 960
  ]
  edge [
    source 10
    target 961
  ]
  edge [
    source 10
    target 962
  ]
  edge [
    source 10
    target 963
  ]
  edge [
    source 10
    target 964
  ]
  edge [
    source 10
    target 965
  ]
  edge [
    source 10
    target 966
  ]
  edge [
    source 10
    target 967
  ]
  edge [
    source 10
    target 968
  ]
  edge [
    source 10
    target 969
  ]
  edge [
    source 10
    target 970
  ]
  edge [
    source 10
    target 971
  ]
  edge [
    source 10
    target 972
  ]
  edge [
    source 10
    target 973
  ]
  edge [
    source 10
    target 974
  ]
  edge [
    source 10
    target 975
  ]
  edge [
    source 10
    target 976
  ]
  edge [
    source 10
    target 977
  ]
  edge [
    source 10
    target 978
  ]
  edge [
    source 10
    target 979
  ]
  edge [
    source 10
    target 980
  ]
  edge [
    source 10
    target 981
  ]
  edge [
    source 10
    target 982
  ]
  edge [
    source 10
    target 983
  ]
  edge [
    source 10
    target 984
  ]
  edge [
    source 10
    target 985
  ]
  edge [
    source 10
    target 986
  ]
  edge [
    source 10
    target 987
  ]
  edge [
    source 10
    target 988
  ]
  edge [
    source 10
    target 989
  ]
  edge [
    source 10
    target 990
  ]
  edge [
    source 10
    target 991
  ]
  edge [
    source 10
    target 992
  ]
  edge [
    source 10
    target 993
  ]
  edge [
    source 10
    target 994
  ]
  edge [
    source 10
    target 995
  ]
  edge [
    source 10
    target 996
  ]
  edge [
    source 10
    target 997
  ]
  edge [
    source 10
    target 998
  ]
  edge [
    source 10
    target 999
  ]
  edge [
    source 10
    target 1000
  ]
  edge [
    source 10
    target 1001
  ]
  edge [
    source 10
    target 1002
  ]
  edge [
    source 10
    target 1003
  ]
  edge [
    source 10
    target 1004
  ]
  edge [
    source 10
    target 1005
  ]
  edge [
    source 10
    target 1006
  ]
  edge [
    source 10
    target 1007
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 1008
  ]
  edge [
    source 10
    target 1009
  ]
  edge [
    source 10
    target 1010
  ]
  edge [
    source 10
    target 1011
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 1012
  ]
  edge [
    source 10
    target 1013
  ]
  edge [
    source 10
    target 1014
  ]
  edge [
    source 10
    target 1015
  ]
  edge [
    source 10
    target 1016
  ]
  edge [
    source 10
    target 1017
  ]
  edge [
    source 10
    target 1018
  ]
  edge [
    source 10
    target 1019
  ]
  edge [
    source 10
    target 1020
  ]
  edge [
    source 10
    target 1021
  ]
  edge [
    source 10
    target 1022
  ]
  edge [
    source 10
    target 1023
  ]
  edge [
    source 10
    target 1024
  ]
  edge [
    source 10
    target 1025
  ]
  edge [
    source 10
    target 1026
  ]
  edge [
    source 10
    target 1027
  ]
  edge [
    source 10
    target 1028
  ]
  edge [
    source 10
    target 1029
  ]
  edge [
    source 10
    target 1030
  ]
  edge [
    source 10
    target 1031
  ]
  edge [
    source 10
    target 1032
  ]
  edge [
    source 10
    target 1033
  ]
  edge [
    source 10
    target 1034
  ]
  edge [
    source 10
    target 1035
  ]
  edge [
    source 10
    target 1036
  ]
  edge [
    source 10
    target 1037
  ]
  edge [
    source 10
    target 1038
  ]
  edge [
    source 10
    target 1039
  ]
  edge [
    source 10
    target 1040
  ]
  edge [
    source 10
    target 1041
  ]
  edge [
    source 10
    target 1042
  ]
  edge [
    source 10
    target 1043
  ]
  edge [
    source 10
    target 1044
  ]
  edge [
    source 10
    target 1045
  ]
  edge [
    source 10
    target 1046
  ]
  edge [
    source 10
    target 1047
  ]
  edge [
    source 10
    target 1048
  ]
  edge [
    source 10
    target 1049
  ]
  edge [
    source 10
    target 1050
  ]
  edge [
    source 10
    target 1051
  ]
  edge [
    source 10
    target 1052
  ]
  edge [
    source 10
    target 1053
  ]
  edge [
    source 10
    target 1054
  ]
  edge [
    source 10
    target 1055
  ]
  edge [
    source 10
    target 1056
  ]
  edge [
    source 10
    target 1057
  ]
  edge [
    source 10
    target 1058
  ]
  edge [
    source 10
    target 1059
  ]
  edge [
    source 10
    target 1060
  ]
  edge [
    source 10
    target 1061
  ]
  edge [
    source 10
    target 1062
  ]
  edge [
    source 10
    target 1063
  ]
  edge [
    source 10
    target 1064
  ]
  edge [
    source 10
    target 1065
  ]
  edge [
    source 10
    target 1066
  ]
  edge [
    source 10
    target 1067
  ]
  edge [
    source 10
    target 1068
  ]
  edge [
    source 10
    target 1069
  ]
  edge [
    source 10
    target 1070
  ]
  edge [
    source 10
    target 1071
  ]
  edge [
    source 10
    target 1072
  ]
  edge [
    source 10
    target 1073
  ]
  edge [
    source 10
    target 1074
  ]
  edge [
    source 10
    target 1075
  ]
  edge [
    source 10
    target 1076
  ]
  edge [
    source 10
    target 1077
  ]
  edge [
    source 10
    target 1078
  ]
  edge [
    source 10
    target 1079
  ]
  edge [
    source 10
    target 1080
  ]
  edge [
    source 10
    target 1081
  ]
  edge [
    source 10
    target 1082
  ]
  edge [
    source 10
    target 1083
  ]
  edge [
    source 10
    target 1084
  ]
  edge [
    source 10
    target 1085
  ]
  edge [
    source 10
    target 1086
  ]
  edge [
    source 10
    target 1087
  ]
  edge [
    source 10
    target 1088
  ]
  edge [
    source 10
    target 1089
  ]
  edge [
    source 10
    target 1090
  ]
  edge [
    source 10
    target 1091
  ]
  edge [
    source 10
    target 1092
  ]
  edge [
    source 10
    target 1093
  ]
  edge [
    source 10
    target 1094
  ]
  edge [
    source 10
    target 1095
  ]
  edge [
    source 10
    target 1096
  ]
  edge [
    source 10
    target 1097
  ]
  edge [
    source 10
    target 1098
  ]
  edge [
    source 10
    target 1099
  ]
  edge [
    source 10
    target 1100
  ]
  edge [
    source 10
    target 1101
  ]
  edge [
    source 10
    target 1102
  ]
  edge [
    source 10
    target 1103
  ]
  edge [
    source 10
    target 1104
  ]
  edge [
    source 10
    target 1105
  ]
  edge [
    source 10
    target 1106
  ]
  edge [
    source 10
    target 1107
  ]
  edge [
    source 10
    target 1108
  ]
  edge [
    source 10
    target 1109
  ]
  edge [
    source 10
    target 1110
  ]
  edge [
    source 10
    target 1111
  ]
  edge [
    source 10
    target 1112
  ]
  edge [
    source 10
    target 1113
  ]
  edge [
    source 10
    target 1114
  ]
  edge [
    source 10
    target 1115
  ]
  edge [
    source 10
    target 1116
  ]
  edge [
    source 10
    target 1117
  ]
  edge [
    source 10
    target 1118
  ]
  edge [
    source 10
    target 1119
  ]
  edge [
    source 10
    target 1120
  ]
  edge [
    source 10
    target 1121
  ]
  edge [
    source 10
    target 1122
  ]
  edge [
    source 10
    target 1123
  ]
  edge [
    source 10
    target 1124
  ]
  edge [
    source 10
    target 1125
  ]
  edge [
    source 10
    target 1126
  ]
  edge [
    source 10
    target 1127
  ]
  edge [
    source 10
    target 1128
  ]
  edge [
    source 10
    target 1129
  ]
  edge [
    source 10
    target 1130
  ]
  edge [
    source 10
    target 1131
  ]
  edge [
    source 10
    target 1132
  ]
  edge [
    source 10
    target 1133
  ]
  edge [
    source 10
    target 1134
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 1135
  ]
  edge [
    source 10
    target 1136
  ]
  edge [
    source 10
    target 1137
  ]
  edge [
    source 10
    target 1138
  ]
  edge [
    source 10
    target 1139
  ]
  edge [
    source 10
    target 1140
  ]
  edge [
    source 10
    target 1141
  ]
  edge [
    source 10
    target 1142
  ]
  edge [
    source 10
    target 1143
  ]
  edge [
    source 10
    target 1144
  ]
  edge [
    source 10
    target 1145
  ]
  edge [
    source 10
    target 1146
  ]
  edge [
    source 10
    target 1147
  ]
  edge [
    source 10
    target 1148
  ]
  edge [
    source 10
    target 1149
  ]
  edge [
    source 10
    target 1150
  ]
  edge [
    source 10
    target 1151
  ]
  edge [
    source 10
    target 1152
  ]
  edge [
    source 10
    target 1153
  ]
  edge [
    source 10
    target 1154
  ]
  edge [
    source 10
    target 1155
  ]
  edge [
    source 10
    target 1156
  ]
  edge [
    source 10
    target 1157
  ]
  edge [
    source 10
    target 1158
  ]
  edge [
    source 10
    target 1159
  ]
  edge [
    source 10
    target 1160
  ]
  edge [
    source 10
    target 1161
  ]
  edge [
    source 10
    target 1162
  ]
  edge [
    source 10
    target 1163
  ]
  edge [
    source 10
    target 1164
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 1165
  ]
  edge [
    source 10
    target 1166
  ]
  edge [
    source 10
    target 1167
  ]
  edge [
    source 10
    target 1168
  ]
  edge [
    source 10
    target 1169
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 1170
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 1171
  ]
  edge [
    source 10
    target 1172
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 1173
  ]
  edge [
    source 10
    target 1174
  ]
  edge [
    source 10
    target 1175
  ]
  edge [
    source 10
    target 1176
  ]
  edge [
    source 10
    target 1177
  ]
  edge [
    source 10
    target 1178
  ]
  edge [
    source 10
    target 1179
  ]
  edge [
    source 10
    target 1180
  ]
  edge [
    source 10
    target 1181
  ]
  edge [
    source 10
    target 1182
  ]
  edge [
    source 10
    target 1183
  ]
  edge [
    source 10
    target 1184
  ]
  edge [
    source 10
    target 1185
  ]
  edge [
    source 10
    target 1186
  ]
  edge [
    source 10
    target 1187
  ]
  edge [
    source 10
    target 1188
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 1189
  ]
  edge [
    source 10
    target 1190
  ]
  edge [
    source 10
    target 1191
  ]
  edge [
    source 10
    target 1192
  ]
  edge [
    source 10
    target 1193
  ]
  edge [
    source 10
    target 1194
  ]
  edge [
    source 10
    target 1195
  ]
  edge [
    source 10
    target 1196
  ]
  edge [
    source 10
    target 1197
  ]
  edge [
    source 10
    target 1198
  ]
  edge [
    source 10
    target 1199
  ]
  edge [
    source 10
    target 1200
  ]
  edge [
    source 10
    target 1201
  ]
  edge [
    source 10
    target 1202
  ]
  edge [
    source 10
    target 1203
  ]
  edge [
    source 10
    target 1204
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 1205
  ]
  edge [
    source 10
    target 1206
  ]
  edge [
    source 10
    target 1207
  ]
  edge [
    source 10
    target 1208
  ]
  edge [
    source 10
    target 1209
  ]
  edge [
    source 10
    target 1210
  ]
  edge [
    source 10
    target 1211
  ]
  edge [
    source 10
    target 1212
  ]
  edge [
    source 10
    target 1213
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 1214
  ]
  edge [
    source 10
    target 1215
  ]
  edge [
    source 10
    target 1216
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 1217
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 1218
  ]
  edge [
    source 10
    target 1219
  ]
  edge [
    source 10
    target 1220
  ]
  edge [
    source 10
    target 1221
  ]
  edge [
    source 10
    target 1222
  ]
  edge [
    source 10
    target 1223
  ]
  edge [
    source 10
    target 1224
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 1225
  ]
  edge [
    source 10
    target 1226
  ]
  edge [
    source 10
    target 1227
  ]
  edge [
    source 10
    target 1228
  ]
  edge [
    source 10
    target 1229
  ]
  edge [
    source 10
    target 1230
  ]
  edge [
    source 10
    target 1231
  ]
  edge [
    source 10
    target 1232
  ]
  edge [
    source 10
    target 1233
  ]
  edge [
    source 10
    target 1234
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 1235
  ]
  edge [
    source 10
    target 1236
  ]
  edge [
    source 10
    target 1237
  ]
  edge [
    source 10
    target 1238
  ]
  edge [
    source 10
    target 1239
  ]
  edge [
    source 10
    target 1240
  ]
  edge [
    source 10
    target 1241
  ]
  edge [
    source 10
    target 1242
  ]
  edge [
    source 10
    target 1243
  ]
  edge [
    source 10
    target 1244
  ]
  edge [
    source 10
    target 1245
  ]
  edge [
    source 10
    target 1246
  ]
  edge [
    source 10
    target 1247
  ]
  edge [
    source 10
    target 1248
  ]
  edge [
    source 10
    target 1249
  ]
  edge [
    source 10
    target 1250
  ]
  edge [
    source 10
    target 1251
  ]
  edge [
    source 10
    target 1252
  ]
  edge [
    source 10
    target 1253
  ]
  edge [
    source 10
    target 1254
  ]
  edge [
    source 10
    target 1255
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 1256
  ]
  edge [
    source 10
    target 1257
  ]
  edge [
    source 10
    target 1258
  ]
  edge [
    source 10
    target 1259
  ]
  edge [
    source 10
    target 1260
  ]
  edge [
    source 10
    target 1261
  ]
  edge [
    source 10
    target 1262
  ]
  edge [
    source 10
    target 1263
  ]
  edge [
    source 10
    target 1264
  ]
  edge [
    source 10
    target 1265
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 1266
  ]
  edge [
    source 10
    target 1267
  ]
  edge [
    source 10
    target 1268
  ]
  edge [
    source 10
    target 1269
  ]
  edge [
    source 10
    target 1270
  ]
  edge [
    source 10
    target 1271
  ]
  edge [
    source 10
    target 1272
  ]
  edge [
    source 10
    target 1273
  ]
  edge [
    source 10
    target 1274
  ]
  edge [
    source 10
    target 1275
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 1276
  ]
  edge [
    source 10
    target 1277
  ]
  edge [
    source 10
    target 1278
  ]
  edge [
    source 10
    target 1279
  ]
  edge [
    source 10
    target 1280
  ]
  edge [
    source 10
    target 1281
  ]
  edge [
    source 10
    target 1282
  ]
  edge [
    source 10
    target 1283
  ]
  edge [
    source 10
    target 1284
  ]
  edge [
    source 10
    target 1285
  ]
  edge [
    source 10
    target 1286
  ]
  edge [
    source 10
    target 1287
  ]
  edge [
    source 10
    target 1288
  ]
  edge [
    source 10
    target 1289
  ]
  edge [
    source 10
    target 1290
  ]
  edge [
    source 10
    target 1291
  ]
  edge [
    source 10
    target 1292
  ]
  edge [
    source 10
    target 1293
  ]
  edge [
    source 10
    target 1294
  ]
  edge [
    source 10
    target 1295
  ]
  edge [
    source 10
    target 1296
  ]
  edge [
    source 10
    target 1297
  ]
  edge [
    source 10
    target 1298
  ]
  edge [
    source 10
    target 1299
  ]
  edge [
    source 10
    target 1300
  ]
  edge [
    source 10
    target 1301
  ]
  edge [
    source 10
    target 1302
  ]
  edge [
    source 10
    target 1303
  ]
  edge [
    source 10
    target 1304
  ]
  edge [
    source 10
    target 1305
  ]
  edge [
    source 10
    target 1306
  ]
  edge [
    source 10
    target 1307
  ]
  edge [
    source 10
    target 1308
  ]
  edge [
    source 10
    target 1309
  ]
  edge [
    source 10
    target 1310
  ]
  edge [
    source 10
    target 1311
  ]
  edge [
    source 10
    target 1312
  ]
  edge [
    source 10
    target 1313
  ]
  edge [
    source 10
    target 1314
  ]
  edge [
    source 10
    target 1315
  ]
  edge [
    source 10
    target 1316
  ]
  edge [
    source 10
    target 1317
  ]
  edge [
    source 10
    target 1318
  ]
  edge [
    source 10
    target 1319
  ]
  edge [
    source 10
    target 1320
  ]
  edge [
    source 10
    target 1321
  ]
  edge [
    source 10
    target 1322
  ]
  edge [
    source 10
    target 1323
  ]
  edge [
    source 10
    target 1324
  ]
  edge [
    source 10
    target 1325
  ]
  edge [
    source 10
    target 1326
  ]
  edge [
    source 10
    target 1327
  ]
  edge [
    source 10
    target 1328
  ]
  edge [
    source 10
    target 1329
  ]
  edge [
    source 10
    target 1330
  ]
  edge [
    source 10
    target 1331
  ]
  edge [
    source 10
    target 1332
  ]
  edge [
    source 10
    target 1333
  ]
  edge [
    source 10
    target 1334
  ]
  edge [
    source 10
    target 1335
  ]
  edge [
    source 10
    target 1336
  ]
  edge [
    source 10
    target 1337
  ]
  edge [
    source 10
    target 1338
  ]
  edge [
    source 10
    target 1339
  ]
  edge [
    source 10
    target 1340
  ]
  edge [
    source 10
    target 1341
  ]
  edge [
    source 10
    target 1342
  ]
  edge [
    source 10
    target 1343
  ]
  edge [
    source 10
    target 1344
  ]
  edge [
    source 10
    target 1345
  ]
  edge [
    source 10
    target 1346
  ]
  edge [
    source 10
    target 1347
  ]
  edge [
    source 10
    target 1348
  ]
  edge [
    source 10
    target 1349
  ]
  edge [
    source 10
    target 1350
  ]
  edge [
    source 10
    target 1351
  ]
  edge [
    source 10
    target 1352
  ]
  edge [
    source 10
    target 1353
  ]
  edge [
    source 10
    target 1354
  ]
  edge [
    source 10
    target 1355
  ]
  edge [
    source 10
    target 1356
  ]
  edge [
    source 10
    target 1357
  ]
  edge [
    source 10
    target 1358
  ]
  edge [
    source 10
    target 1359
  ]
  edge [
    source 10
    target 1360
  ]
  edge [
    source 10
    target 1361
  ]
  edge [
    source 10
    target 1362
  ]
  edge [
    source 10
    target 1363
  ]
  edge [
    source 10
    target 1364
  ]
  edge [
    source 10
    target 1365
  ]
  edge [
    source 10
    target 1366
  ]
  edge [
    source 10
    target 1367
  ]
  edge [
    source 10
    target 1368
  ]
  edge [
    source 10
    target 1369
  ]
  edge [
    source 10
    target 1370
  ]
  edge [
    source 10
    target 1371
  ]
  edge [
    source 10
    target 1372
  ]
  edge [
    source 10
    target 1373
  ]
  edge [
    source 10
    target 1374
  ]
  edge [
    source 10
    target 1375
  ]
  edge [
    source 10
    target 1376
  ]
  edge [
    source 10
    target 1377
  ]
  edge [
    source 10
    target 1378
  ]
  edge [
    source 10
    target 1379
  ]
  edge [
    source 10
    target 1380
  ]
  edge [
    source 10
    target 1381
  ]
  edge [
    source 10
    target 1382
  ]
  edge [
    source 10
    target 1383
  ]
  edge [
    source 10
    target 1384
  ]
  edge [
    source 10
    target 1385
  ]
  edge [
    source 10
    target 1386
  ]
  edge [
    source 10
    target 1387
  ]
  edge [
    source 10
    target 1388
  ]
  edge [
    source 10
    target 1389
  ]
  edge [
    source 10
    target 1390
  ]
  edge [
    source 10
    target 1391
  ]
  edge [
    source 10
    target 1392
  ]
  edge [
    source 10
    target 1393
  ]
  edge [
    source 10
    target 1394
  ]
  edge [
    source 10
    target 1395
  ]
  edge [
    source 10
    target 1396
  ]
  edge [
    source 10
    target 1397
  ]
  edge [
    source 10
    target 1398
  ]
  edge [
    source 10
    target 1399
  ]
  edge [
    source 10
    target 1400
  ]
  edge [
    source 10
    target 1401
  ]
  edge [
    source 10
    target 1402
  ]
  edge [
    source 10
    target 1403
  ]
  edge [
    source 10
    target 1404
  ]
  edge [
    source 10
    target 1405
  ]
  edge [
    source 10
    target 1406
  ]
  edge [
    source 10
    target 1407
  ]
  edge [
    source 10
    target 1408
  ]
  edge [
    source 10
    target 1409
  ]
  edge [
    source 10
    target 1410
  ]
  edge [
    source 10
    target 1411
  ]
  edge [
    source 10
    target 1412
  ]
  edge [
    source 10
    target 1413
  ]
  edge [
    source 10
    target 1414
  ]
  edge [
    source 10
    target 1415
  ]
  edge [
    source 10
    target 1416
  ]
  edge [
    source 10
    target 1417
  ]
  edge [
    source 10
    target 1418
  ]
  edge [
    source 10
    target 1419
  ]
  edge [
    source 10
    target 1420
  ]
  edge [
    source 10
    target 1421
  ]
  edge [
    source 10
    target 1422
  ]
  edge [
    source 10
    target 1423
  ]
  edge [
    source 10
    target 1424
  ]
  edge [
    source 10
    target 1425
  ]
  edge [
    source 10
    target 1426
  ]
  edge [
    source 10
    target 1427
  ]
  edge [
    source 10
    target 1428
  ]
  edge [
    source 10
    target 1429
  ]
  edge [
    source 10
    target 1430
  ]
  edge [
    source 10
    target 1431
  ]
  edge [
    source 10
    target 1432
  ]
  edge [
    source 10
    target 1433
  ]
  edge [
    source 10
    target 1434
  ]
  edge [
    source 10
    target 1435
  ]
  edge [
    source 10
    target 1436
  ]
  edge [
    source 10
    target 1437
  ]
  edge [
    source 10
    target 1438
  ]
  edge [
    source 10
    target 1439
  ]
  edge [
    source 10
    target 1440
  ]
  edge [
    source 10
    target 1441
  ]
  edge [
    source 10
    target 1442
  ]
  edge [
    source 10
    target 1443
  ]
  edge [
    source 10
    target 1444
  ]
  edge [
    source 10
    target 1445
  ]
  edge [
    source 10
    target 1446
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 1447
  ]
  edge [
    source 10
    target 1448
  ]
  edge [
    source 10
    target 1449
  ]
  edge [
    source 10
    target 1450
  ]
  edge [
    source 10
    target 1451
  ]
  edge [
    source 10
    target 1452
  ]
  edge [
    source 10
    target 1453
  ]
  edge [
    source 10
    target 1454
  ]
  edge [
    source 10
    target 1455
  ]
  edge [
    source 10
    target 1456
  ]
  edge [
    source 10
    target 1457
  ]
  edge [
    source 10
    target 1458
  ]
  edge [
    source 10
    target 1459
  ]
  edge [
    source 10
    target 1460
  ]
  edge [
    source 10
    target 1461
  ]
  edge [
    source 10
    target 1462
  ]
  edge [
    source 10
    target 1463
  ]
  edge [
    source 10
    target 1464
  ]
  edge [
    source 10
    target 1465
  ]
  edge [
    source 10
    target 1466
  ]
  edge [
    source 10
    target 1467
  ]
  edge [
    source 10
    target 1468
  ]
  edge [
    source 10
    target 1469
  ]
  edge [
    source 10
    target 1470
  ]
  edge [
    source 10
    target 1471
  ]
  edge [
    source 10
    target 1472
  ]
  edge [
    source 10
    target 1473
  ]
  edge [
    source 10
    target 1474
  ]
  edge [
    source 10
    target 1475
  ]
  edge [
    source 10
    target 1476
  ]
  edge [
    source 10
    target 1477
  ]
  edge [
    source 10
    target 1478
  ]
  edge [
    source 10
    target 1479
  ]
  edge [
    source 10
    target 1480
  ]
  edge [
    source 10
    target 1481
  ]
  edge [
    source 10
    target 1482
  ]
  edge [
    source 10
    target 1483
  ]
  edge [
    source 10
    target 1484
  ]
  edge [
    source 10
    target 1485
  ]
  edge [
    source 10
    target 1486
  ]
  edge [
    source 10
    target 1487
  ]
  edge [
    source 10
    target 1488
  ]
  edge [
    source 10
    target 1489
  ]
  edge [
    source 10
    target 1490
  ]
  edge [
    source 10
    target 1491
  ]
  edge [
    source 10
    target 1492
  ]
  edge [
    source 10
    target 1493
  ]
  edge [
    source 10
    target 1494
  ]
  edge [
    source 10
    target 1495
  ]
  edge [
    source 10
    target 1496
  ]
  edge [
    source 10
    target 1497
  ]
  edge [
    source 10
    target 1498
  ]
  edge [
    source 10
    target 1499
  ]
  edge [
    source 10
    target 1500
  ]
  edge [
    source 10
    target 1501
  ]
  edge [
    source 10
    target 1502
  ]
  edge [
    source 10
    target 1503
  ]
  edge [
    source 10
    target 1504
  ]
  edge [
    source 10
    target 1505
  ]
  edge [
    source 10
    target 1506
  ]
  edge [
    source 10
    target 1507
  ]
  edge [
    source 10
    target 1508
  ]
  edge [
    source 10
    target 1509
  ]
  edge [
    source 10
    target 1510
  ]
  edge [
    source 10
    target 1511
  ]
  edge [
    source 10
    target 1512
  ]
  edge [
    source 10
    target 1513
  ]
  edge [
    source 10
    target 1514
  ]
  edge [
    source 10
    target 1515
  ]
  edge [
    source 10
    target 1516
  ]
  edge [
    source 10
    target 1517
  ]
  edge [
    source 10
    target 1518
  ]
  edge [
    source 10
    target 1519
  ]
  edge [
    source 10
    target 1520
  ]
  edge [
    source 10
    target 1521
  ]
  edge [
    source 10
    target 1522
  ]
  edge [
    source 10
    target 1523
  ]
  edge [
    source 10
    target 1524
  ]
  edge [
    source 10
    target 1525
  ]
  edge [
    source 10
    target 1526
  ]
  edge [
    source 10
    target 1527
  ]
  edge [
    source 10
    target 1528
  ]
  edge [
    source 10
    target 1529
  ]
  edge [
    source 10
    target 1530
  ]
  edge [
    source 10
    target 1531
  ]
  edge [
    source 10
    target 1532
  ]
  edge [
    source 10
    target 1533
  ]
  edge [
    source 10
    target 1534
  ]
  edge [
    source 10
    target 1535
  ]
  edge [
    source 10
    target 1536
  ]
  edge [
    source 10
    target 1537
  ]
  edge [
    source 10
    target 1538
  ]
  edge [
    source 10
    target 1539
  ]
  edge [
    source 10
    target 1540
  ]
  edge [
    source 10
    target 1541
  ]
  edge [
    source 10
    target 1542
  ]
  edge [
    source 10
    target 1543
  ]
  edge [
    source 10
    target 1544
  ]
  edge [
    source 10
    target 1545
  ]
  edge [
    source 10
    target 1546
  ]
  edge [
    source 10
    target 1547
  ]
  edge [
    source 10
    target 1548
  ]
  edge [
    source 10
    target 1549
  ]
  edge [
    source 10
    target 1550
  ]
  edge [
    source 10
    target 1551
  ]
  edge [
    source 10
    target 1552
  ]
  edge [
    source 10
    target 1553
  ]
  edge [
    source 10
    target 1554
  ]
  edge [
    source 10
    target 1555
  ]
  edge [
    source 10
    target 1556
  ]
  edge [
    source 10
    target 1557
  ]
  edge [
    source 10
    target 1558
  ]
  edge [
    source 10
    target 1559
  ]
  edge [
    source 10
    target 1560
  ]
  edge [
    source 10
    target 1561
  ]
  edge [
    source 10
    target 1562
  ]
  edge [
    source 10
    target 1563
  ]
  edge [
    source 10
    target 1564
  ]
  edge [
    source 10
    target 1565
  ]
  edge [
    source 10
    target 1566
  ]
  edge [
    source 10
    target 1567
  ]
  edge [
    source 10
    target 1568
  ]
  edge [
    source 10
    target 1569
  ]
  edge [
    source 10
    target 1570
  ]
  edge [
    source 10
    target 1571
  ]
  edge [
    source 10
    target 1572
  ]
  edge [
    source 10
    target 1573
  ]
  edge [
    source 10
    target 1574
  ]
  edge [
    source 10
    target 1575
  ]
  edge [
    source 10
    target 1576
  ]
  edge [
    source 10
    target 1577
  ]
  edge [
    source 10
    target 1578
  ]
  edge [
    source 10
    target 1579
  ]
  edge [
    source 10
    target 1580
  ]
  edge [
    source 10
    target 1581
  ]
  edge [
    source 10
    target 1582
  ]
  edge [
    source 10
    target 1583
  ]
  edge [
    source 10
    target 1584
  ]
  edge [
    source 10
    target 1585
  ]
  edge [
    source 10
    target 1586
  ]
  edge [
    source 10
    target 1587
  ]
  edge [
    source 10
    target 1588
  ]
  edge [
    source 10
    target 1589
  ]
  edge [
    source 10
    target 1590
  ]
  edge [
    source 10
    target 1591
  ]
  edge [
    source 10
    target 1592
  ]
  edge [
    source 10
    target 1593
  ]
  edge [
    source 10
    target 1594
  ]
  edge [
    source 10
    target 1595
  ]
  edge [
    source 10
    target 1596
  ]
  edge [
    source 10
    target 1597
  ]
  edge [
    source 10
    target 1598
  ]
  edge [
    source 10
    target 1599
  ]
  edge [
    source 10
    target 1600
  ]
  edge [
    source 10
    target 1601
  ]
  edge [
    source 10
    target 1602
  ]
  edge [
    source 10
    target 1603
  ]
  edge [
    source 10
    target 1604
  ]
  edge [
    source 10
    target 1605
  ]
  edge [
    source 10
    target 1606
  ]
  edge [
    source 10
    target 1607
  ]
  edge [
    source 10
    target 1608
  ]
  edge [
    source 10
    target 1609
  ]
  edge [
    source 10
    target 1610
  ]
  edge [
    source 10
    target 1611
  ]
  edge [
    source 10
    target 1612
  ]
  edge [
    source 10
    target 1613
  ]
  edge [
    source 10
    target 1614
  ]
  edge [
    source 10
    target 1615
  ]
  edge [
    source 10
    target 1616
  ]
  edge [
    source 10
    target 1617
  ]
  edge [
    source 10
    target 1618
  ]
  edge [
    source 10
    target 1619
  ]
  edge [
    source 10
    target 1620
  ]
  edge [
    source 10
    target 1621
  ]
  edge [
    source 10
    target 1622
  ]
  edge [
    source 10
    target 1623
  ]
  edge [
    source 10
    target 1624
  ]
  edge [
    source 10
    target 1625
  ]
  edge [
    source 10
    target 1626
  ]
  edge [
    source 10
    target 1627
  ]
  edge [
    source 10
    target 1628
  ]
  edge [
    source 10
    target 1629
  ]
  edge [
    source 10
    target 1630
  ]
  edge [
    source 10
    target 1631
  ]
  edge [
    source 10
    target 1632
  ]
  edge [
    source 10
    target 1633
  ]
  edge [
    source 10
    target 1634
  ]
  edge [
    source 10
    target 1635
  ]
  edge [
    source 10
    target 1636
  ]
  edge [
    source 10
    target 1637
  ]
  edge [
    source 10
    target 1638
  ]
  edge [
    source 10
    target 1639
  ]
  edge [
    source 10
    target 1640
  ]
  edge [
    source 10
    target 1641
  ]
  edge [
    source 10
    target 1642
  ]
  edge [
    source 10
    target 1643
  ]
  edge [
    source 10
    target 1644
  ]
  edge [
    source 10
    target 1645
  ]
  edge [
    source 10
    target 1646
  ]
  edge [
    source 10
    target 1647
  ]
  edge [
    source 10
    target 1648
  ]
  edge [
    source 10
    target 1649
  ]
  edge [
    source 10
    target 1650
  ]
  edge [
    source 10
    target 1651
  ]
  edge [
    source 10
    target 1652
  ]
  edge [
    source 10
    target 1653
  ]
  edge [
    source 10
    target 1654
  ]
  edge [
    source 10
    target 1655
  ]
  edge [
    source 10
    target 1656
  ]
  edge [
    source 10
    target 1657
  ]
  edge [
    source 10
    target 1658
  ]
  edge [
    source 10
    target 1659
  ]
  edge [
    source 10
    target 1660
  ]
  edge [
    source 10
    target 1661
  ]
  edge [
    source 10
    target 1662
  ]
  edge [
    source 10
    target 1663
  ]
  edge [
    source 10
    target 1664
  ]
  edge [
    source 10
    target 1665
  ]
  edge [
    source 10
    target 1666
  ]
  edge [
    source 10
    target 1667
  ]
  edge [
    source 10
    target 1668
  ]
  edge [
    source 10
    target 1669
  ]
  edge [
    source 10
    target 1670
  ]
  edge [
    source 10
    target 1671
  ]
  edge [
    source 10
    target 1672
  ]
  edge [
    source 10
    target 1673
  ]
  edge [
    source 10
    target 1674
  ]
  edge [
    source 10
    target 1675
  ]
  edge [
    source 10
    target 1676
  ]
  edge [
    source 10
    target 1677
  ]
  edge [
    source 10
    target 1678
  ]
  edge [
    source 10
    target 1679
  ]
  edge [
    source 10
    target 1680
  ]
  edge [
    source 10
    target 1681
  ]
  edge [
    source 10
    target 1682
  ]
  edge [
    source 10
    target 1683
  ]
  edge [
    source 10
    target 1684
  ]
  edge [
    source 10
    target 1685
  ]
  edge [
    source 10
    target 1686
  ]
  edge [
    source 10
    target 1687
  ]
  edge [
    source 10
    target 1688
  ]
  edge [
    source 10
    target 1689
  ]
  edge [
    source 10
    target 1690
  ]
  edge [
    source 10
    target 1691
  ]
  edge [
    source 10
    target 1692
  ]
  edge [
    source 10
    target 1693
  ]
  edge [
    source 10
    target 1694
  ]
  edge [
    source 10
    target 1695
  ]
  edge [
    source 10
    target 1696
  ]
  edge [
    source 10
    target 1697
  ]
  edge [
    source 10
    target 1698
  ]
  edge [
    source 10
    target 1699
  ]
  edge [
    source 10
    target 1700
  ]
  edge [
    source 10
    target 1701
  ]
  edge [
    source 10
    target 1702
  ]
  edge [
    source 10
    target 1703
  ]
  edge [
    source 10
    target 1704
  ]
  edge [
    source 10
    target 1705
  ]
  edge [
    source 10
    target 1706
  ]
  edge [
    source 10
    target 1707
  ]
  edge [
    source 10
    target 1708
  ]
  edge [
    source 10
    target 1709
  ]
  edge [
    source 10
    target 1710
  ]
  edge [
    source 10
    target 1711
  ]
  edge [
    source 10
    target 1712
  ]
  edge [
    source 10
    target 1713
  ]
  edge [
    source 10
    target 1714
  ]
  edge [
    source 10
    target 1715
  ]
  edge [
    source 10
    target 1716
  ]
  edge [
    source 10
    target 1717
  ]
  edge [
    source 10
    target 1718
  ]
  edge [
    source 10
    target 1719
  ]
  edge [
    source 10
    target 1720
  ]
  edge [
    source 10
    target 1721
  ]
  edge [
    source 10
    target 1722
  ]
  edge [
    source 10
    target 1723
  ]
  edge [
    source 10
    target 1724
  ]
  edge [
    source 10
    target 1725
  ]
  edge [
    source 10
    target 1726
  ]
  edge [
    source 10
    target 1727
  ]
  edge [
    source 10
    target 1728
  ]
  edge [
    source 10
    target 1729
  ]
  edge [
    source 10
    target 1730
  ]
  edge [
    source 10
    target 1731
  ]
  edge [
    source 10
    target 1732
  ]
  edge [
    source 10
    target 1733
  ]
  edge [
    source 10
    target 1734
  ]
  edge [
    source 10
    target 1735
  ]
  edge [
    source 10
    target 1736
  ]
  edge [
    source 10
    target 1737
  ]
  edge [
    source 10
    target 1738
  ]
  edge [
    source 10
    target 1739
  ]
  edge [
    source 10
    target 1740
  ]
  edge [
    source 10
    target 1741
  ]
  edge [
    source 10
    target 1742
  ]
  edge [
    source 10
    target 1743
  ]
  edge [
    source 10
    target 1744
  ]
  edge [
    source 10
    target 1745
  ]
  edge [
    source 10
    target 1746
  ]
  edge [
    source 10
    target 1747
  ]
  edge [
    source 10
    target 1748
  ]
  edge [
    source 10
    target 1749
  ]
  edge [
    source 10
    target 1750
  ]
  edge [
    source 10
    target 1751
  ]
  edge [
    source 10
    target 1752
  ]
  edge [
    source 10
    target 1753
  ]
  edge [
    source 10
    target 1754
  ]
  edge [
    source 10
    target 1755
  ]
  edge [
    source 10
    target 1756
  ]
  edge [
    source 10
    target 1757
  ]
  edge [
    source 10
    target 1758
  ]
  edge [
    source 10
    target 1759
  ]
  edge [
    source 10
    target 1760
  ]
  edge [
    source 10
    target 1761
  ]
  edge [
    source 10
    target 1762
  ]
  edge [
    source 10
    target 1763
  ]
  edge [
    source 10
    target 1764
  ]
  edge [
    source 10
    target 1765
  ]
  edge [
    source 10
    target 1766
  ]
  edge [
    source 10
    target 1767
  ]
  edge [
    source 10
    target 1768
  ]
  edge [
    source 10
    target 1769
  ]
  edge [
    source 10
    target 1770
  ]
  edge [
    source 10
    target 1771
  ]
  edge [
    source 10
    target 1772
  ]
  edge [
    source 10
    target 1773
  ]
  edge [
    source 10
    target 1774
  ]
  edge [
    source 10
    target 1775
  ]
  edge [
    source 10
    target 1776
  ]
  edge [
    source 10
    target 1777
  ]
  edge [
    source 10
    target 1778
  ]
  edge [
    source 10
    target 1779
  ]
  edge [
    source 10
    target 1780
  ]
  edge [
    source 10
    target 1781
  ]
  edge [
    source 10
    target 1782
  ]
  edge [
    source 10
    target 1783
  ]
  edge [
    source 10
    target 1784
  ]
  edge [
    source 10
    target 1785
  ]
  edge [
    source 10
    target 1786
  ]
  edge [
    source 10
    target 1787
  ]
  edge [
    source 10
    target 1788
  ]
  edge [
    source 10
    target 1789
  ]
  edge [
    source 10
    target 1790
  ]
  edge [
    source 10
    target 1791
  ]
  edge [
    source 10
    target 1792
  ]
  edge [
    source 10
    target 1793
  ]
  edge [
    source 10
    target 1794
  ]
  edge [
    source 10
    target 1795
  ]
  edge [
    source 10
    target 1796
  ]
  edge [
    source 10
    target 1797
  ]
  edge [
    source 10
    target 1798
  ]
  edge [
    source 10
    target 1799
  ]
  edge [
    source 10
    target 1800
  ]
  edge [
    source 10
    target 1801
  ]
  edge [
    source 10
    target 1802
  ]
  edge [
    source 10
    target 1803
  ]
  edge [
    source 10
    target 1804
  ]
  edge [
    source 10
    target 1805
  ]
  edge [
    source 10
    target 1806
  ]
  edge [
    source 10
    target 1807
  ]
  edge [
    source 10
    target 1808
  ]
  edge [
    source 10
    target 1809
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 1810
  ]
  edge [
    source 10
    target 1811
  ]
  edge [
    source 10
    target 1812
  ]
  edge [
    source 10
    target 1813
  ]
  edge [
    source 10
    target 1814
  ]
  edge [
    source 10
    target 1815
  ]
  edge [
    source 10
    target 1816
  ]
  edge [
    source 10
    target 1817
  ]
  edge [
    source 10
    target 1818
  ]
  edge [
    source 10
    target 1819
  ]
  edge [
    source 10
    target 1820
  ]
  edge [
    source 10
    target 1821
  ]
  edge [
    source 10
    target 1822
  ]
  edge [
    source 10
    target 1823
  ]
  edge [
    source 10
    target 1824
  ]
  edge [
    source 10
    target 1825
  ]
  edge [
    source 10
    target 1826
  ]
  edge [
    source 10
    target 1827
  ]
  edge [
    source 10
    target 1828
  ]
  edge [
    source 10
    target 1829
  ]
  edge [
    source 10
    target 1830
  ]
  edge [
    source 10
    target 1831
  ]
  edge [
    source 10
    target 1832
  ]
  edge [
    source 10
    target 1833
  ]
  edge [
    source 10
    target 1834
  ]
  edge [
    source 10
    target 1835
  ]
  edge [
    source 10
    target 1836
  ]
  edge [
    source 10
    target 1837
  ]
  edge [
    source 10
    target 1838
  ]
  edge [
    source 10
    target 1839
  ]
  edge [
    source 10
    target 1840
  ]
  edge [
    source 10
    target 1841
  ]
  edge [
    source 10
    target 1842
  ]
  edge [
    source 10
    target 1843
  ]
  edge [
    source 10
    target 1844
  ]
  edge [
    source 10
    target 1845
  ]
  edge [
    source 10
    target 1846
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 1847
  ]
  edge [
    source 10
    target 1848
  ]
  edge [
    source 10
    target 2099
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1849
  ]
  edge [
    source 11
    target 1850
  ]
  edge [
    source 11
    target 1851
  ]
  edge [
    source 11
    target 1852
  ]
  edge [
    source 11
    target 1853
  ]
  edge [
    source 11
    target 1854
  ]
  edge [
    source 11
    target 1855
  ]
  edge [
    source 11
    target 1856
  ]
  edge [
    source 11
    target 1857
  ]
  edge [
    source 11
    target 1858
  ]
  edge [
    source 11
    target 1859
  ]
  edge [
    source 11
    target 1860
  ]
  edge [
    source 11
    target 1861
  ]
  edge [
    source 11
    target 1862
  ]
  edge [
    source 11
    target 1863
  ]
  edge [
    source 11
    target 2099
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1864
  ]
  edge [
    source 13
    target 1865
  ]
  edge [
    source 13
    target 1866
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1867
  ]
  edge [
    source 14
    target 1868
  ]
  edge [
    source 14
    target 1869
  ]
  edge [
    source 14
    target 1870
  ]
  edge [
    source 14
    target 1871
  ]
  edge [
    source 14
    target 1872
  ]
  edge [
    source 14
    target 1873
  ]
  edge [
    source 14
    target 1874
  ]
  edge [
    source 15
    target 1875
  ]
  edge [
    source 15
    target 1876
  ]
  edge [
    source 15
    target 1877
  ]
  edge [
    source 15
    target 1878
  ]
  edge [
    source 15
    target 1879
  ]
  edge [
    source 15
    target 1880
  ]
  edge [
    source 15
    target 1881
  ]
  edge [
    source 15
    target 1882
  ]
  edge [
    source 15
    target 1883
  ]
  edge [
    source 15
    target 1884
  ]
  edge [
    source 15
    target 1885
  ]
  edge [
    source 15
    target 1886
  ]
  edge [
    source 15
    target 1887
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 1888
  ]
  edge [
    source 15
    target 1889
  ]
  edge [
    source 15
    target 1890
  ]
  edge [
    source 15
    target 1891
  ]
  edge [
    source 15
    target 1892
  ]
  edge [
    source 15
    target 1893
  ]
  edge [
    source 15
    target 1894
  ]
  edge [
    source 15
    target 1895
  ]
  edge [
    source 15
    target 1896
  ]
  edge [
    source 15
    target 1897
  ]
  edge [
    source 15
    target 1898
  ]
  edge [
    source 15
    target 1899
  ]
  edge [
    source 15
    target 1900
  ]
  edge [
    source 15
    target 1901
  ]
  edge [
    source 15
    target 1902
  ]
  edge [
    source 15
    target 1903
  ]
  edge [
    source 15
    target 1396
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 1904
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 1905
  ]
  edge [
    source 15
    target 1906
  ]
  edge [
    source 15
    target 1907
  ]
  edge [
    source 15
    target 1908
  ]
  edge [
    source 15
    target 1117
  ]
  edge [
    source 15
    target 1909
  ]
  edge [
    source 15
    target 1910
  ]
  edge [
    source 15
    target 1012
  ]
  edge [
    source 15
    target 1132
  ]
  edge [
    source 15
    target 1911
  ]
  edge [
    source 15
    target 1912
  ]
  edge [
    source 15
    target 1913
  ]
  edge [
    source 15
    target 1914
  ]
  edge [
    source 15
    target 1915
  ]
  edge [
    source 15
    target 1916
  ]
  edge [
    source 15
    target 1917
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 1918
  ]
  edge [
    source 15
    target 1919
  ]
  edge [
    source 15
    target 1032
  ]
  edge [
    source 15
    target 1920
  ]
  edge [
    source 15
    target 1036
  ]
  edge [
    source 15
    target 1040
  ]
  edge [
    source 15
    target 1921
  ]
  edge [
    source 15
    target 1922
  ]
  edge [
    source 15
    target 1923
  ]
  edge [
    source 15
    target 1163
  ]
  edge [
    source 15
    target 1924
  ]
  edge [
    source 15
    target 1925
  ]
  edge [
    source 15
    target 1926
  ]
  edge [
    source 15
    target 1927
  ]
  edge [
    source 15
    target 1928
  ]
  edge [
    source 15
    target 1929
  ]
  edge [
    source 15
    target 1930
  ]
  edge [
    source 15
    target 1931
  ]
  edge [
    source 15
    target 1932
  ]
  edge [
    source 15
    target 1933
  ]
  edge [
    source 15
    target 1934
  ]
  edge [
    source 15
    target 1935
  ]
  edge [
    source 15
    target 1936
  ]
  edge [
    source 15
    target 1937
  ]
  edge [
    source 15
    target 1938
  ]
  edge [
    source 15
    target 1939
  ]
  edge [
    source 15
    target 1940
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1941
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1942
  ]
  edge [
    source 19
    target 1943
  ]
  edge [
    source 19
    target 1944
  ]
  edge [
    source 19
    target 1945
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1946
  ]
  edge [
    source 19
    target 482
  ]
  edge [
    source 19
    target 1947
  ]
  edge [
    source 19
    target 1948
  ]
  edge [
    source 19
    target 1949
  ]
  edge [
    source 19
    target 1950
  ]
  edge [
    source 19
    target 1951
  ]
  edge [
    source 19
    target 1952
  ]
  edge [
    source 19
    target 1953
  ]
  edge [
    source 19
    target 1954
  ]
  edge [
    source 19
    target 1955
  ]
  edge [
    source 19
    target 1956
  ]
  edge [
    source 19
    target 1957
  ]
  edge [
    source 19
    target 1958
  ]
  edge [
    source 19
    target 1959
  ]
  edge [
    source 19
    target 1960
  ]
  edge [
    source 19
    target 1961
  ]
  edge [
    source 19
    target 1962
  ]
  edge [
    source 19
    target 1963
  ]
  edge [
    source 19
    target 1964
  ]
  edge [
    source 19
    target 1965
  ]
  edge [
    source 19
    target 1966
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 1967
  ]
  edge [
    source 19
    target 1968
  ]
  edge [
    source 19
    target 1969
  ]
  edge [
    source 19
    target 1970
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 1971
  ]
  edge [
    source 19
    target 1972
  ]
  edge [
    source 19
    target 1973
  ]
  edge [
    source 19
    target 1974
  ]
  edge [
    source 19
    target 1975
  ]
  edge [
    source 19
    target 1976
  ]
  edge [
    source 19
    target 1977
  ]
  edge [
    source 19
    target 1978
  ]
  edge [
    source 19
    target 1979
  ]
  edge [
    source 19
    target 1980
  ]
  edge [
    source 19
    target 1981
  ]
  edge [
    source 19
    target 1982
  ]
  edge [
    source 19
    target 1983
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 1984
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1985
  ]
  edge [
    source 19
    target 667
  ]
  edge [
    source 19
    target 1986
  ]
  edge [
    source 19
    target 1987
  ]
  edge [
    source 19
    target 1988
  ]
  edge [
    source 19
    target 1989
  ]
  edge [
    source 19
    target 1990
  ]
  edge [
    source 19
    target 1991
  ]
  edge [
    source 19
    target 1992
  ]
  edge [
    source 19
    target 1993
  ]
  edge [
    source 19
    target 1994
  ]
  edge [
    source 19
    target 1995
  ]
  edge [
    source 19
    target 1996
  ]
  edge [
    source 19
    target 1659
  ]
  edge [
    source 19
    target 1997
  ]
  edge [
    source 19
    target 1661
  ]
  edge [
    source 19
    target 1998
  ]
  edge [
    source 19
    target 1999
  ]
  edge [
    source 19
    target 1662
  ]
  edge [
    source 19
    target 2000
  ]
  edge [
    source 19
    target 2001
  ]
  edge [
    source 19
    target 1409
  ]
  edge [
    source 19
    target 1664
  ]
  edge [
    source 19
    target 1665
  ]
  edge [
    source 19
    target 2002
  ]
  edge [
    source 19
    target 1666
  ]
  edge [
    source 19
    target 2003
  ]
  edge [
    source 19
    target 1667
  ]
  edge [
    source 19
    target 384
  ]
  edge [
    source 19
    target 1670
  ]
  edge [
    source 19
    target 1669
  ]
  edge [
    source 19
    target 1538
  ]
  edge [
    source 19
    target 1671
  ]
  edge [
    source 19
    target 1672
  ]
  edge [
    source 19
    target 449
  ]
  edge [
    source 19
    target 1673
  ]
  edge [
    source 19
    target 1674
  ]
  edge [
    source 19
    target 2004
  ]
  edge [
    source 19
    target 1675
  ]
  edge [
    source 19
    target 2005
  ]
  edge [
    source 19
    target 1678
  ]
  edge [
    source 19
    target 2006
  ]
  edge [
    source 19
    target 1679
  ]
  edge [
    source 19
    target 1680
  ]
  edge [
    source 19
    target 1682
  ]
  edge [
    source 19
    target 464
  ]
  edge [
    source 19
    target 1683
  ]
  edge [
    source 19
    target 1684
  ]
  edge [
    source 19
    target 1687
  ]
  edge [
    source 19
    target 1686
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 2007
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 2008
  ]
  edge [
    source 23
    target 2009
  ]
  edge [
    source 23
    target 2010
  ]
  edge [
    source 23
    target 2011
  ]
  edge [
    source 23
    target 719
  ]
  edge [
    source 23
    target 720
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 721
  ]
  edge [
    source 23
    target 722
  ]
  edge [
    source 23
    target 723
  ]
  edge [
    source 23
    target 724
  ]
  edge [
    source 23
    target 725
  ]
  edge [
    source 23
    target 726
  ]
  edge [
    source 23
    target 727
  ]
  edge [
    source 23
    target 728
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 2012
  ]
  edge [
    source 23
    target 2013
  ]
  edge [
    source 23
    target 2014
  ]
  edge [
    source 23
    target 2015
  ]
  edge [
    source 23
    target 2016
  ]
  edge [
    source 23
    target 1909
  ]
  edge [
    source 23
    target 2017
  ]
  edge [
    source 23
    target 1917
  ]
  edge [
    source 23
    target 2018
  ]
  edge [
    source 23
    target 2019
  ]
  edge [
    source 23
    target 319
  ]
  edge [
    source 23
    target 2020
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 2021
  ]
  edge [
    source 26
    target 2022
  ]
  edge [
    source 26
    target 2023
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 2024
  ]
  edge [
    source 26
    target 2025
  ]
  edge [
    source 26
    target 2026
  ]
  edge [
    source 26
    target 2027
  ]
  edge [
    source 26
    target 2028
  ]
  edge [
    source 26
    target 2029
  ]
  edge [
    source 26
    target 2030
  ]
  edge [
    source 26
    target 2031
  ]
  edge [
    source 26
    target 2032
  ]
  edge [
    source 26
    target 2033
  ]
  edge [
    source 26
    target 2034
  ]
  edge [
    source 26
    target 2035
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 2036
  ]
  edge [
    source 28
    target 2037
  ]
  edge [
    source 28
    target 2038
  ]
  edge [
    source 28
    target 2039
  ]
  edge [
    source 28
    target 81
  ]
  edge [
    source 28
    target 2040
  ]
  edge [
    source 28
    target 2041
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 2042
  ]
  edge [
    source 29
    target 2043
  ]
  edge [
    source 29
    target 2044
  ]
  edge [
    source 29
    target 2045
  ]
  edge [
    source 29
    target 1236
  ]
  edge [
    source 29
    target 1720
  ]
  edge [
    source 29
    target 1237
  ]
  edge [
    source 29
    target 1240
  ]
  edge [
    source 29
    target 428
  ]
  edge [
    source 29
    target 1241
  ]
  edge [
    source 29
    target 2046
  ]
  edge [
    source 29
    target 1242
  ]
  edge [
    source 29
    target 2047
  ]
  edge [
    source 29
    target 1243
  ]
  edge [
    source 29
    target 1244
  ]
  edge [
    source 29
    target 181
  ]
  edge [
    source 29
    target 1246
  ]
  edge [
    source 29
    target 1008
  ]
  edge [
    source 29
    target 1247
  ]
  edge [
    source 29
    target 1248
  ]
  edge [
    source 29
    target 139
  ]
  edge [
    source 29
    target 284
  ]
  edge [
    source 29
    target 427
  ]
  edge [
    source 29
    target 968
  ]
  edge [
    source 29
    target 2048
  ]
  edge [
    source 29
    target 2049
  ]
  edge [
    source 29
    target 1249
  ]
  edge [
    source 29
    target 622
  ]
  edge [
    source 29
    target 1251
  ]
  edge [
    source 29
    target 1253
  ]
  edge [
    source 29
    target 1255
  ]
  edge [
    source 29
    target 773
  ]
  edge [
    source 29
    target 1256
  ]
  edge [
    source 29
    target 1258
  ]
  edge [
    source 29
    target 1259
  ]
  edge [
    source 29
    target 416
  ]
  edge [
    source 29
    target 198
  ]
  edge [
    source 29
    target 1260
  ]
  edge [
    source 29
    target 1261
  ]
  edge [
    source 29
    target 1262
  ]
  edge [
    source 29
    target 2050
  ]
  edge [
    source 29
    target 2051
  ]
  edge [
    source 29
    target 1265
  ]
  edge [
    source 29
    target 547
  ]
  edge [
    source 29
    target 2052
  ]
  edge [
    source 29
    target 2053
  ]
  edge [
    source 29
    target 2054
  ]
  edge [
    source 29
    target 2055
  ]
  edge [
    source 29
    target 2056
  ]
  edge [
    source 29
    target 2057
  ]
  edge [
    source 29
    target 1218
  ]
  edge [
    source 29
    target 2058
  ]
  edge [
    source 29
    target 883
  ]
  edge [
    source 29
    target 2059
  ]
  edge [
    source 29
    target 2060
  ]
  edge [
    source 29
    target 2061
  ]
  edge [
    source 29
    target 2062
  ]
  edge [
    source 29
    target 2063
  ]
  edge [
    source 29
    target 2064
  ]
  edge [
    source 29
    target 2065
  ]
  edge [
    source 29
    target 2066
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 319
  ]
  edge [
    source 30
    target 347
  ]
  edge [
    source 30
    target 2067
  ]
  edge [
    source 30
    target 2068
  ]
  edge [
    source 30
    target 516
  ]
  edge [
    source 30
    target 342
  ]
  edge [
    source 30
    target 343
  ]
  edge [
    source 30
    target 344
  ]
  edge [
    source 30
    target 345
  ]
  edge [
    source 30
    target 346
  ]
  edge [
    source 30
    target 348
  ]
  edge [
    source 30
    target 349
  ]
  edge [
    source 30
    target 350
  ]
  edge [
    source 30
    target 351
  ]
  edge [
    source 30
    target 352
  ]
  edge [
    source 30
    target 353
  ]
  edge [
    source 30
    target 354
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 2069
  ]
  edge [
    source 30
    target 2070
  ]
  edge [
    source 30
    target 2071
  ]
  edge [
    source 30
    target 2072
  ]
  edge [
    source 30
    target 2073
  ]
  edge [
    source 30
    target 2074
  ]
  edge [
    source 30
    target 2075
  ]
  edge [
    source 30
    target 2076
  ]
  edge [
    source 30
    target 2077
  ]
  edge [
    source 30
    target 2078
  ]
  edge [
    source 30
    target 404
  ]
  edge [
    source 30
    target 2079
  ]
  edge [
    source 30
    target 2080
  ]
  edge [
    source 30
    target 2081
  ]
  edge [
    source 30
    target 2082
  ]
  edge [
    source 30
    target 2083
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 2084
  ]
  edge [
    source 32
    target 1791
  ]
  edge [
    source 32
    target 2085
  ]
  edge [
    source 32
    target 2086
  ]
  edge [
    source 32
    target 2087
  ]
  edge [
    source 32
    target 2088
  ]
  edge [
    source 32
    target 2089
  ]
  edge [
    source 32
    target 2090
  ]
  edge [
    source 32
    target 2091
  ]
  edge [
    source 32
    target 2092
  ]
  edge [
    source 32
    target 2093
  ]
  edge [
    source 32
    target 2094
  ]
  edge [
    source 32
    target 2095
  ]
  edge [
    source 32
    target 2096
  ]
  edge [
    source 32
    target 2097
  ]
  edge [
    source 605
    target 2119
  ]
  edge [
    source 605
    target 2120
  ]
  edge [
    source 1481
    target 2121
  ]
  edge [
    source 2100
    target 2101
  ]
  edge [
    source 2100
    target 2102
  ]
  edge [
    source 2100
    target 2103
  ]
  edge [
    source 2101
    target 2102
  ]
  edge [
    source 2101
    target 2103
  ]
  edge [
    source 2102
    target 2103
  ]
  edge [
    source 2104
    target 2105
  ]
  edge [
    source 2104
    target 2106
  ]
  edge [
    source 2104
    target 2107
  ]
  edge [
    source 2105
    target 2106
  ]
  edge [
    source 2105
    target 2107
  ]
  edge [
    source 2106
    target 2107
  ]
  edge [
    source 2108
    target 2109
  ]
  edge [
    source 2110
    target 2111
  ]
  edge [
    source 2110
    target 2112
  ]
  edge [
    source 2110
    target 2113
  ]
  edge [
    source 2110
    target 2114
  ]
  edge [
    source 2112
    target 2113
  ]
  edge [
    source 2112
    target 2114
  ]
  edge [
    source 2113
    target 2114
  ]
  edge [
    source 2115
    target 2116
  ]
  edge [
    source 2117
    target 2118
  ]
  edge [
    source 2119
    target 2120
  ]
]
