graph [
  node [
    id 0
    label "siema"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "czyj&#347;"
  ]
  node [
    id 3
    label "m&#261;&#380;"
  ]
  node [
    id 4
    label "prywatny"
  ]
  node [
    id 5
    label "ma&#322;&#380;onek"
  ]
  node [
    id 6
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 7
    label "ch&#322;op"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "pan_m&#322;ody"
  ]
  node [
    id 10
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 11
    label "&#347;lubny"
  ]
  node [
    id 12
    label "pan_domu"
  ]
  node [
    id 13
    label "pan_i_w&#322;adca"
  ]
  node [
    id 14
    label "stary"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
]
