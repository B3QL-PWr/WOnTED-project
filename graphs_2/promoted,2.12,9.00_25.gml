graph [
  node [
    id 0
    label "gdy"
    origin "text"
  ]
  node [
    id 1
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "rutyna"
    origin "text"
  ]
  node [
    id 4
    label "brawura"
    origin "text"
  ]
  node [
    id 5
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 6
    label "insert"
  ]
  node [
    id 7
    label "visualize"
  ]
  node [
    id 8
    label "pozna&#263;"
  ]
  node [
    id 9
    label "befall"
  ]
  node [
    id 10
    label "spowodowa&#263;"
  ]
  node [
    id 11
    label "go_steady"
  ]
  node [
    id 12
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 13
    label "znale&#378;&#263;"
  ]
  node [
    id 14
    label "act"
  ]
  node [
    id 15
    label "pozyska&#263;"
  ]
  node [
    id 16
    label "oceni&#263;"
  ]
  node [
    id 17
    label "devise"
  ]
  node [
    id 18
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 19
    label "dozna&#263;"
  ]
  node [
    id 20
    label "wykry&#263;"
  ]
  node [
    id 21
    label "odzyska&#263;"
  ]
  node [
    id 22
    label "znaj&#347;&#263;"
  ]
  node [
    id 23
    label "invent"
  ]
  node [
    id 24
    label "wymy&#347;li&#263;"
  ]
  node [
    id 25
    label "zrozumie&#263;"
  ]
  node [
    id 26
    label "feel"
  ]
  node [
    id 27
    label "topographic_point"
  ]
  node [
    id 28
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 29
    label "przyswoi&#263;"
  ]
  node [
    id 30
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 31
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 32
    label "teach"
  ]
  node [
    id 33
    label "experience"
  ]
  node [
    id 34
    label "wiedza"
  ]
  node [
    id 35
    label "metyl"
  ]
  node [
    id 36
    label "flawonoid"
  ]
  node [
    id 37
    label "skill"
  ]
  node [
    id 38
    label "znawstwo"
  ]
  node [
    id 39
    label "nawyk"
  ]
  node [
    id 40
    label "przeciwutleniacz"
  ]
  node [
    id 41
    label "monotonia"
  ]
  node [
    id 42
    label "witamina"
  ]
  node [
    id 43
    label "eksperiencja"
  ]
  node [
    id 44
    label "glikozyd"
  ]
  node [
    id 45
    label "kwercetyna"
  ]
  node [
    id 46
    label "adeptness"
  ]
  node [
    id 47
    label "glukoza"
  ]
  node [
    id 48
    label "kisi&#263;_si&#281;_we_w&#322;asnym_sosie"
  ]
  node [
    id 49
    label "sameness"
  ]
  node [
    id 50
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 51
    label "kiszenie_si&#281;_we_w&#322;asnym_sosie"
  ]
  node [
    id 52
    label "nawyknienie"
  ]
  node [
    id 53
    label "zwyczaj"
  ]
  node [
    id 54
    label "sk&#322;adnik_pokarmowy"
  ]
  node [
    id 55
    label "&#380;ycian"
  ]
  node [
    id 56
    label "vitamin"
  ]
  node [
    id 57
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 58
    label "aglikon"
  ]
  node [
    id 59
    label "glycoside"
  ]
  node [
    id 60
    label "flavonoid"
  ]
  node [
    id 61
    label "karbonyl"
  ]
  node [
    id 62
    label "ro&#347;lina"
  ]
  node [
    id 63
    label "insektycyd"
  ]
  node [
    id 64
    label "fungicyd"
  ]
  node [
    id 65
    label "barwnik_naturalny"
  ]
  node [
    id 66
    label "substancja"
  ]
  node [
    id 67
    label "antioxidant"
  ]
  node [
    id 68
    label "cognition"
  ]
  node [
    id 69
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 70
    label "intelekt"
  ]
  node [
    id 71
    label "pozwolenie"
  ]
  node [
    id 72
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 73
    label "zaawansowanie"
  ]
  node [
    id 74
    label "wykszta&#322;cenie"
  ]
  node [
    id 75
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 76
    label "odczynnik"
  ]
  node [
    id 77
    label "flawonol"
  ]
  node [
    id 78
    label "hiperglikemia"
  ]
  node [
    id 79
    label "aldoheksoza"
  ]
  node [
    id 80
    label "hipoglikemia"
  ]
  node [
    id 81
    label "grupa_alkilowa"
  ]
  node [
    id 82
    label "methyl"
  ]
  node [
    id 83
    label "praktyka"
  ]
  node [
    id 84
    label "znajomo&#347;&#263;"
  ]
  node [
    id 85
    label "information"
  ]
  node [
    id 86
    label "kiperstwo"
  ]
  node [
    id 87
    label "efektowno&#347;&#263;"
  ]
  node [
    id 88
    label "nieostro&#380;no&#347;&#263;"
  ]
  node [
    id 89
    label "odwaga"
  ]
  node [
    id 90
    label "wymy&#347;lno&#347;&#263;"
  ]
  node [
    id 91
    label "czyn"
  ]
  node [
    id 92
    label "cecha"
  ]
  node [
    id 93
    label "dusza"
  ]
  node [
    id 94
    label "courage"
  ]
  node [
    id 95
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 96
    label "oryginalno&#347;&#263;"
  ]
  node [
    id 97
    label "atrakcyjno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
]
