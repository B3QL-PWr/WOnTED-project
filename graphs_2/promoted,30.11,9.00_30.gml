graph [
  node [
    id 0
    label "bestialski"
    origin "text"
  ]
  node [
    id 1
    label "zachowanie"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;a&#347;cicielka"
    origin "text"
  ]
  node [
    id 3
    label "pies"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "dla"
    origin "text"
  ]
  node [
    id 6
    label "swoje"
    origin "text"
  ]
  node [
    id 7
    label "czworon&#243;g"
    origin "text"
  ]
  node [
    id 8
    label "okaza&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "kat"
    origin "text"
  ]
  node [
    id 11
    label "zwierz&#281;cy"
  ]
  node [
    id 12
    label "bestialsko"
  ]
  node [
    id 13
    label "okrutny"
  ]
  node [
    id 14
    label "z&#322;y"
  ]
  node [
    id 15
    label "nieludzki"
  ]
  node [
    id 16
    label "straszny"
  ]
  node [
    id 17
    label "mocny"
  ]
  node [
    id 18
    label "pod&#322;y"
  ]
  node [
    id 19
    label "bezlito&#347;ny"
  ]
  node [
    id 20
    label "gro&#378;ny"
  ]
  node [
    id 21
    label "okrutnie"
  ]
  node [
    id 22
    label "zwierz&#281;co"
  ]
  node [
    id 23
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 24
    label "wzorzysty"
  ]
  node [
    id 25
    label "organiczny"
  ]
  node [
    id 26
    label "pierwotny"
  ]
  node [
    id 27
    label "aintelektualny"
  ]
  node [
    id 28
    label "pieski"
  ]
  node [
    id 29
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 30
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 31
    label "niekorzystny"
  ]
  node [
    id 32
    label "z&#322;oszczenie"
  ]
  node [
    id 33
    label "sierdzisty"
  ]
  node [
    id 34
    label "niegrzeczny"
  ]
  node [
    id 35
    label "zez&#322;oszczenie"
  ]
  node [
    id 36
    label "zdenerwowany"
  ]
  node [
    id 37
    label "negatywny"
  ]
  node [
    id 38
    label "rozgniewanie"
  ]
  node [
    id 39
    label "gniewanie"
  ]
  node [
    id 40
    label "niemoralny"
  ]
  node [
    id 41
    label "&#378;le"
  ]
  node [
    id 42
    label "niepomy&#347;lny"
  ]
  node [
    id 43
    label "syf"
  ]
  node [
    id 44
    label "beastly"
  ]
  node [
    id 45
    label "reakcja"
  ]
  node [
    id 46
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 47
    label "tajemnica"
  ]
  node [
    id 48
    label "struktura"
  ]
  node [
    id 49
    label "spos&#243;b"
  ]
  node [
    id 50
    label "wydarzenie"
  ]
  node [
    id 51
    label "pochowanie"
  ]
  node [
    id 52
    label "zdyscyplinowanie"
  ]
  node [
    id 53
    label "post&#261;pienie"
  ]
  node [
    id 54
    label "post"
  ]
  node [
    id 55
    label "bearing"
  ]
  node [
    id 56
    label "zwierz&#281;"
  ]
  node [
    id 57
    label "behawior"
  ]
  node [
    id 58
    label "observation"
  ]
  node [
    id 59
    label "dieta"
  ]
  node [
    id 60
    label "podtrzymanie"
  ]
  node [
    id 61
    label "etolog"
  ]
  node [
    id 62
    label "przechowanie"
  ]
  node [
    id 63
    label "zrobienie"
  ]
  node [
    id 64
    label "model"
  ]
  node [
    id 65
    label "narz&#281;dzie"
  ]
  node [
    id 66
    label "zbi&#243;r"
  ]
  node [
    id 67
    label "tryb"
  ]
  node [
    id 68
    label "nature"
  ]
  node [
    id 69
    label "p&#243;j&#347;cie"
  ]
  node [
    id 70
    label "behavior"
  ]
  node [
    id 71
    label "comfort"
  ]
  node [
    id 72
    label "pocieszenie"
  ]
  node [
    id 73
    label "uniesienie"
  ]
  node [
    id 74
    label "sustainability"
  ]
  node [
    id 75
    label "support"
  ]
  node [
    id 76
    label "utrzymanie"
  ]
  node [
    id 77
    label "czynno&#347;&#263;"
  ]
  node [
    id 78
    label "narobienie"
  ]
  node [
    id 79
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 80
    label "creation"
  ]
  node [
    id 81
    label "porobienie"
  ]
  node [
    id 82
    label "ukrycie"
  ]
  node [
    id 83
    label "retention"
  ]
  node [
    id 84
    label "preserve"
  ]
  node [
    id 85
    label "zmagazynowanie"
  ]
  node [
    id 86
    label "uchronienie"
  ]
  node [
    id 87
    label "przebiec"
  ]
  node [
    id 88
    label "charakter"
  ]
  node [
    id 89
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 90
    label "motyw"
  ]
  node [
    id 91
    label "przebiegni&#281;cie"
  ]
  node [
    id 92
    label "fabu&#322;a"
  ]
  node [
    id 93
    label "react"
  ]
  node [
    id 94
    label "reaction"
  ]
  node [
    id 95
    label "organizm"
  ]
  node [
    id 96
    label "rozmowa"
  ]
  node [
    id 97
    label "response"
  ]
  node [
    id 98
    label "rezultat"
  ]
  node [
    id 99
    label "respondent"
  ]
  node [
    id 100
    label "degenerat"
  ]
  node [
    id 101
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 102
    label "cz&#322;owiek"
  ]
  node [
    id 103
    label "zwyrol"
  ]
  node [
    id 104
    label "czerniak"
  ]
  node [
    id 105
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 106
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 107
    label "paszcza"
  ]
  node [
    id 108
    label "popapraniec"
  ]
  node [
    id 109
    label "skuba&#263;"
  ]
  node [
    id 110
    label "skubanie"
  ]
  node [
    id 111
    label "agresja"
  ]
  node [
    id 112
    label "skubni&#281;cie"
  ]
  node [
    id 113
    label "zwierz&#281;ta"
  ]
  node [
    id 114
    label "fukni&#281;cie"
  ]
  node [
    id 115
    label "farba"
  ]
  node [
    id 116
    label "fukanie"
  ]
  node [
    id 117
    label "istota_&#380;ywa"
  ]
  node [
    id 118
    label "gad"
  ]
  node [
    id 119
    label "tresowa&#263;"
  ]
  node [
    id 120
    label "siedzie&#263;"
  ]
  node [
    id 121
    label "oswaja&#263;"
  ]
  node [
    id 122
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 123
    label "poligamia"
  ]
  node [
    id 124
    label "oz&#243;r"
  ]
  node [
    id 125
    label "skubn&#261;&#263;"
  ]
  node [
    id 126
    label "wios&#322;owa&#263;"
  ]
  node [
    id 127
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 128
    label "le&#380;enie"
  ]
  node [
    id 129
    label "niecz&#322;owiek"
  ]
  node [
    id 130
    label "wios&#322;owanie"
  ]
  node [
    id 131
    label "napasienie_si&#281;"
  ]
  node [
    id 132
    label "wiwarium"
  ]
  node [
    id 133
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 134
    label "animalista"
  ]
  node [
    id 135
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 136
    label "budowa"
  ]
  node [
    id 137
    label "hodowla"
  ]
  node [
    id 138
    label "pasienie_si&#281;"
  ]
  node [
    id 139
    label "sodomita"
  ]
  node [
    id 140
    label "monogamia"
  ]
  node [
    id 141
    label "przyssawka"
  ]
  node [
    id 142
    label "budowa_cia&#322;a"
  ]
  node [
    id 143
    label "okrutnik"
  ]
  node [
    id 144
    label "grzbiet"
  ]
  node [
    id 145
    label "weterynarz"
  ]
  node [
    id 146
    label "&#322;eb"
  ]
  node [
    id 147
    label "wylinka"
  ]
  node [
    id 148
    label "bestia"
  ]
  node [
    id 149
    label "poskramia&#263;"
  ]
  node [
    id 150
    label "fauna"
  ]
  node [
    id 151
    label "treser"
  ]
  node [
    id 152
    label "siedzenie"
  ]
  node [
    id 153
    label "le&#380;e&#263;"
  ]
  node [
    id 154
    label "zoopsycholog"
  ]
  node [
    id 155
    label "zoolog"
  ]
  node [
    id 156
    label "wypaplanie"
  ]
  node [
    id 157
    label "enigmat"
  ]
  node [
    id 158
    label "wiedza"
  ]
  node [
    id 159
    label "zachowywanie"
  ]
  node [
    id 160
    label "secret"
  ]
  node [
    id 161
    label "wydawa&#263;"
  ]
  node [
    id 162
    label "obowi&#261;zek"
  ]
  node [
    id 163
    label "dyskrecja"
  ]
  node [
    id 164
    label "informacja"
  ]
  node [
    id 165
    label "wyda&#263;"
  ]
  node [
    id 166
    label "rzecz"
  ]
  node [
    id 167
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 168
    label "taj&#324;"
  ]
  node [
    id 169
    label "zachowa&#263;"
  ]
  node [
    id 170
    label "zachowywa&#263;"
  ]
  node [
    id 171
    label "podporz&#261;dkowanie"
  ]
  node [
    id 172
    label "porz&#261;dek"
  ]
  node [
    id 173
    label "mores"
  ]
  node [
    id 174
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 175
    label "nauczenie"
  ]
  node [
    id 176
    label "chart"
  ]
  node [
    id 177
    label "wynagrodzenie"
  ]
  node [
    id 178
    label "regimen"
  ]
  node [
    id 179
    label "r&#243;wnowarto&#347;&#263;"
  ]
  node [
    id 180
    label "terapia"
  ]
  node [
    id 181
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 182
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 183
    label "rok_ko&#347;cielny"
  ]
  node [
    id 184
    label "tekst"
  ]
  node [
    id 185
    label "czas"
  ]
  node [
    id 186
    label "praktyka"
  ]
  node [
    id 187
    label "mechanika"
  ]
  node [
    id 188
    label "o&#347;"
  ]
  node [
    id 189
    label "usenet"
  ]
  node [
    id 190
    label "rozprz&#261;c"
  ]
  node [
    id 191
    label "cybernetyk"
  ]
  node [
    id 192
    label "podsystem"
  ]
  node [
    id 193
    label "system"
  ]
  node [
    id 194
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 195
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 196
    label "sk&#322;ad"
  ]
  node [
    id 197
    label "systemat"
  ]
  node [
    id 198
    label "cecha"
  ]
  node [
    id 199
    label "konstrukcja"
  ]
  node [
    id 200
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 201
    label "konstelacja"
  ]
  node [
    id 202
    label "poumieszczanie"
  ]
  node [
    id 203
    label "burying"
  ]
  node [
    id 204
    label "powk&#322;adanie"
  ]
  node [
    id 205
    label "zw&#322;oki"
  ]
  node [
    id 206
    label "burial"
  ]
  node [
    id 207
    label "w&#322;o&#380;enie"
  ]
  node [
    id 208
    label "gr&#243;b"
  ]
  node [
    id 209
    label "spocz&#281;cie"
  ]
  node [
    id 210
    label "piese&#322;"
  ]
  node [
    id 211
    label "Cerber"
  ]
  node [
    id 212
    label "szczeka&#263;"
  ]
  node [
    id 213
    label "&#322;ajdak"
  ]
  node [
    id 214
    label "kabanos"
  ]
  node [
    id 215
    label "wyzwisko"
  ]
  node [
    id 216
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 217
    label "samiec"
  ]
  node [
    id 218
    label "spragniony"
  ]
  node [
    id 219
    label "policjant"
  ]
  node [
    id 220
    label "rakarz"
  ]
  node [
    id 221
    label "szczu&#263;"
  ]
  node [
    id 222
    label "wycie"
  ]
  node [
    id 223
    label "trufla"
  ]
  node [
    id 224
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 225
    label "zawy&#263;"
  ]
  node [
    id 226
    label "sobaka"
  ]
  node [
    id 227
    label "dogoterapia"
  ]
  node [
    id 228
    label "s&#322;u&#380;enie"
  ]
  node [
    id 229
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 230
    label "psowate"
  ]
  node [
    id 231
    label "wy&#263;"
  ]
  node [
    id 232
    label "szczucie"
  ]
  node [
    id 233
    label "sympatyk"
  ]
  node [
    id 234
    label "entuzjasta"
  ]
  node [
    id 235
    label "critter"
  ]
  node [
    id 236
    label "zwierz&#281;_domowe"
  ]
  node [
    id 237
    label "kr&#281;gowiec"
  ]
  node [
    id 238
    label "tetrapody"
  ]
  node [
    id 239
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 240
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 241
    label "ludzko&#347;&#263;"
  ]
  node [
    id 242
    label "asymilowanie"
  ]
  node [
    id 243
    label "wapniak"
  ]
  node [
    id 244
    label "asymilowa&#263;"
  ]
  node [
    id 245
    label "os&#322;abia&#263;"
  ]
  node [
    id 246
    label "posta&#263;"
  ]
  node [
    id 247
    label "hominid"
  ]
  node [
    id 248
    label "podw&#322;adny"
  ]
  node [
    id 249
    label "os&#322;abianie"
  ]
  node [
    id 250
    label "g&#322;owa"
  ]
  node [
    id 251
    label "figura"
  ]
  node [
    id 252
    label "portrecista"
  ]
  node [
    id 253
    label "dwun&#243;g"
  ]
  node [
    id 254
    label "profanum"
  ]
  node [
    id 255
    label "mikrokosmos"
  ]
  node [
    id 256
    label "nasada"
  ]
  node [
    id 257
    label "duch"
  ]
  node [
    id 258
    label "antropochoria"
  ]
  node [
    id 259
    label "osoba"
  ]
  node [
    id 260
    label "wz&#243;r"
  ]
  node [
    id 261
    label "senior"
  ]
  node [
    id 262
    label "oddzia&#322;ywanie"
  ]
  node [
    id 263
    label "Adam"
  ]
  node [
    id 264
    label "homo_sapiens"
  ]
  node [
    id 265
    label "polifag"
  ]
  node [
    id 266
    label "palconogie"
  ]
  node [
    id 267
    label "stra&#380;nik"
  ]
  node [
    id 268
    label "wielog&#322;owy"
  ]
  node [
    id 269
    label "przek&#261;ska"
  ]
  node [
    id 270
    label "w&#281;dzi&#263;"
  ]
  node [
    id 271
    label "przysmak"
  ]
  node [
    id 272
    label "kie&#322;basa"
  ]
  node [
    id 273
    label "cygaro"
  ]
  node [
    id 274
    label "kot"
  ]
  node [
    id 275
    label "zooterapia"
  ]
  node [
    id 276
    label "&#380;o&#322;nierz"
  ]
  node [
    id 277
    label "robi&#263;"
  ]
  node [
    id 278
    label "by&#263;"
  ]
  node [
    id 279
    label "trwa&#263;"
  ]
  node [
    id 280
    label "use"
  ]
  node [
    id 281
    label "suffice"
  ]
  node [
    id 282
    label "cel"
  ]
  node [
    id 283
    label "pracowa&#263;"
  ]
  node [
    id 284
    label "match"
  ]
  node [
    id 285
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 286
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 287
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 288
    label "wait"
  ]
  node [
    id 289
    label "pomaga&#263;"
  ]
  node [
    id 290
    label "czekoladka"
  ]
  node [
    id 291
    label "afrodyzjak"
  ]
  node [
    id 292
    label "workowiec"
  ]
  node [
    id 293
    label "nos"
  ]
  node [
    id 294
    label "grzyb_owocnikowy"
  ]
  node [
    id 295
    label "truflowate"
  ]
  node [
    id 296
    label "grzyb_mikoryzowy"
  ]
  node [
    id 297
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 298
    label "powodowanie"
  ]
  node [
    id 299
    label "pod&#380;eganie"
  ]
  node [
    id 300
    label "atakowanie"
  ]
  node [
    id 301
    label "fomentation"
  ]
  node [
    id 302
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 303
    label "bark"
  ]
  node [
    id 304
    label "m&#243;wi&#263;"
  ]
  node [
    id 305
    label "hum"
  ]
  node [
    id 306
    label "obgadywa&#263;"
  ]
  node [
    id 307
    label "kozio&#322;"
  ]
  node [
    id 308
    label "szcz&#281;ka&#263;"
  ]
  node [
    id 309
    label "karabin"
  ]
  node [
    id 310
    label "wymy&#347;la&#263;"
  ]
  node [
    id 311
    label "wilk"
  ]
  node [
    id 312
    label "ha&#322;asowa&#263;"
  ]
  node [
    id 313
    label "fa&#322;szowa&#263;"
  ]
  node [
    id 314
    label "p&#322;aka&#263;"
  ]
  node [
    id 315
    label "snivel"
  ]
  node [
    id 316
    label "yip"
  ]
  node [
    id 317
    label "pracownik_komunalny"
  ]
  node [
    id 318
    label "uczynny"
  ]
  node [
    id 319
    label "s&#322;ugiwanie"
  ]
  node [
    id 320
    label "pomaganie"
  ]
  node [
    id 321
    label "bycie"
  ]
  node [
    id 322
    label "wys&#322;u&#380;enie_si&#281;"
  ]
  node [
    id 323
    label "request"
  ]
  node [
    id 324
    label "trwanie"
  ]
  node [
    id 325
    label "robienie"
  ]
  node [
    id 326
    label "service"
  ]
  node [
    id 327
    label "przydawanie_si&#281;"
  ]
  node [
    id 328
    label "pracowanie"
  ]
  node [
    id 329
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 330
    label "wydoby&#263;"
  ]
  node [
    id 331
    label "rant"
  ]
  node [
    id 332
    label "rave"
  ]
  node [
    id 333
    label "zabrzmie&#263;"
  ]
  node [
    id 334
    label "tease"
  ]
  node [
    id 335
    label "pod&#380;ega&#263;"
  ]
  node [
    id 336
    label "podjudza&#263;"
  ]
  node [
    id 337
    label "wo&#322;anie"
  ]
  node [
    id 338
    label "wydobywanie"
  ]
  node [
    id 339
    label "brzmienie"
  ]
  node [
    id 340
    label "wydawanie"
  ]
  node [
    id 341
    label "d&#378;wi&#281;k"
  ]
  node [
    id 342
    label "whimper"
  ]
  node [
    id 343
    label "cholera"
  ]
  node [
    id 344
    label "wypowied&#378;"
  ]
  node [
    id 345
    label "chuj"
  ]
  node [
    id 346
    label "bluzg"
  ]
  node [
    id 347
    label "chujowy"
  ]
  node [
    id 348
    label "obelga"
  ]
  node [
    id 349
    label "szmata"
  ]
  node [
    id 350
    label "ch&#281;tny"
  ]
  node [
    id 351
    label "z&#322;akniony"
  ]
  node [
    id 352
    label "upodlenie_si&#281;"
  ]
  node [
    id 353
    label "skurwysyn"
  ]
  node [
    id 354
    label "upadlanie_si&#281;"
  ]
  node [
    id 355
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 356
    label "psubrat"
  ]
  node [
    id 357
    label "policja"
  ]
  node [
    id 358
    label "blacharz"
  ]
  node [
    id 359
    label "str&#243;&#380;"
  ]
  node [
    id 360
    label "pa&#322;a"
  ]
  node [
    id 361
    label "mundurowy"
  ]
  node [
    id 362
    label "glina"
  ]
  node [
    id 363
    label "kr&#281;gowce"
  ]
  node [
    id 364
    label "czaszkowiec"
  ]
  node [
    id 365
    label "okazale"
  ]
  node [
    id 366
    label "imponuj&#261;cy"
  ]
  node [
    id 367
    label "bogaty"
  ]
  node [
    id 368
    label "poka&#378;ny"
  ]
  node [
    id 369
    label "&#322;adnie"
  ]
  node [
    id 370
    label "spory"
  ]
  node [
    id 371
    label "obficie"
  ]
  node [
    id 372
    label "niez&#322;y"
  ]
  node [
    id 373
    label "imponuj&#261;co"
  ]
  node [
    id 374
    label "efektowny"
  ]
  node [
    id 375
    label "wspania&#322;y"
  ]
  node [
    id 376
    label "obfituj&#261;cy"
  ]
  node [
    id 377
    label "nabab"
  ]
  node [
    id 378
    label "r&#243;&#380;norodny"
  ]
  node [
    id 379
    label "spania&#322;y"
  ]
  node [
    id 380
    label "sytuowany"
  ]
  node [
    id 381
    label "och&#281;do&#380;ny"
  ]
  node [
    id 382
    label "forsiasty"
  ]
  node [
    id 383
    label "zapa&#347;ny"
  ]
  node [
    id 384
    label "bogato"
  ]
  node [
    id 385
    label "ciernikowate"
  ]
  node [
    id 386
    label "kara_&#347;mierci"
  ]
  node [
    id 387
    label "morderca"
  ]
  node [
    id 388
    label "ciemi&#281;zca"
  ]
  node [
    id 389
    label "prze&#347;ladowca"
  ]
  node [
    id 390
    label "ryba"
  ]
  node [
    id 391
    label "funkcjonariusz"
  ]
  node [
    id 392
    label "zwyrodnialec"
  ]
  node [
    id 393
    label "wr&#243;g"
  ]
  node [
    id 394
    label "krzywdziciel"
  ]
  node [
    id 395
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 396
    label "systemik"
  ]
  node [
    id 397
    label "doniczkowiec"
  ]
  node [
    id 398
    label "mi&#281;so"
  ]
  node [
    id 399
    label "patroszy&#263;"
  ]
  node [
    id 400
    label "rakowato&#347;&#263;"
  ]
  node [
    id 401
    label "w&#281;dkarstwo"
  ]
  node [
    id 402
    label "ryby"
  ]
  node [
    id 403
    label "fish"
  ]
  node [
    id 404
    label "linia_boczna"
  ]
  node [
    id 405
    label "tar&#322;o"
  ]
  node [
    id 406
    label "wyrostek_filtracyjny"
  ]
  node [
    id 407
    label "m&#281;tnooki"
  ]
  node [
    id 408
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 409
    label "pokrywa_skrzelowa"
  ]
  node [
    id 410
    label "ikra"
  ]
  node [
    id 411
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 412
    label "szczelina_skrzelowa"
  ]
  node [
    id 413
    label "pracownik"
  ]
  node [
    id 414
    label "&#380;y&#322;a"
  ]
  node [
    id 415
    label "zab&#243;jca"
  ]
  node [
    id 416
    label "m&#281;&#380;ob&#243;jca"
  ]
  node [
    id 417
    label "ciernikokszta&#322;tne"
  ]
  node [
    id 418
    label "koluszki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
]
