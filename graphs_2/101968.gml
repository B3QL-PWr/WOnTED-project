graph [
  node [
    id 0
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 1
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "rocznik"
    origin "text"
  ]
  node [
    id 3
    label "si&#243;dma"
    origin "text"
  ]
  node [
    id 4
    label "rocznica"
    origin "text"
  ]
  node [
    id 5
    label "przyznanie"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "ukraina"
    origin "text"
  ]
  node [
    id 8
    label "organizacja"
    origin "text"
  ]
  node [
    id 9
    label "euro"
    origin "text"
  ]
  node [
    id 10
    label "prezydent"
    origin "text"
  ]
  node [
    id 11
    label "rafa&#322;"
    origin "text"
  ]
  node [
    id 12
    label "dutkiewicz"
    origin "text"
  ]
  node [
    id 13
    label "wmurowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kamie&#324;"
    origin "text"
  ]
  node [
    id 15
    label "w&#281;gielny"
    origin "text"
  ]
  node [
    id 16
    label "pod"
    origin "text"
  ]
  node [
    id 17
    label "budowa"
    origin "text"
  ]
  node [
    id 18
    label "nowy"
    origin "text"
  ]
  node [
    id 19
    label "lotnisko"
    origin "text"
  ]
  node [
    id 20
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 21
    label "uroczysto&#347;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "czysto"
    origin "text"
  ]
  node [
    id 23
    label "symboliczny"
    origin "text"
  ]
  node [
    id 24
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 25
    label "robotnica"
    origin "text"
  ]
  node [
    id 26
    label "dawny"
    origin "text"
  ]
  node [
    id 27
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 28
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "betonowy"
    origin "text"
  ]
  node [
    id 30
    label "s&#322;up"
    origin "text"
  ]
  node [
    id 31
    label "tora"
    origin "text"
  ]
  node [
    id 32
    label "kolej"
    origin "text"
  ]
  node [
    id 33
    label "magnetyczny"
    origin "text"
  ]
  node [
    id 34
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 35
    label "metro"
    origin "text"
  ]
  node [
    id 36
    label "te&#380;"
    origin "text"
  ]
  node [
    id 37
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 38
    label "nowa"
    origin "text"
  ]
  node [
    id 39
    label "pewno"
    origin "text"
  ]
  node [
    id 40
    label "by&#263;"
    origin "text"
  ]
  node [
    id 41
    label "argument"
    origin "text"
  ]
  node [
    id 42
    label "walc"
    origin "text"
  ]
  node [
    id 43
    label "lekki"
    origin "text"
  ]
  node [
    id 44
    label "atletyka"
    origin "text"
  ]
  node [
    id 45
    label "kongres"
    origin "text"
  ]
  node [
    id 46
    label "futurologiczny"
    origin "text"
  ]
  node [
    id 47
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 48
    label "tym"
    origin "text"
  ]
  node [
    id 49
    label "ostatni"
    origin "text"
  ]
  node [
    id 50
    label "miejsce"
    origin "text"
  ]
  node [
    id 51
    label "galeriowiec"
    origin "text"
  ]
  node [
    id 52
    label "powstawa&#263;"
    origin "text"
  ]
  node [
    id 53
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 54
    label "budynek"
    origin "text"
  ]
  node [
    id 55
    label "europa"
    origin "text"
  ]
  node [
    id 56
    label "water"
    origin "text"
  ]
  node [
    id 57
    label "tower"
    origin "text"
  ]
  node [
    id 58
    label "miesi&#261;c"
  ]
  node [
    id 59
    label "tydzie&#324;"
  ]
  node [
    id 60
    label "miech"
  ]
  node [
    id 61
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 62
    label "czas"
  ]
  node [
    id 63
    label "rok"
  ]
  node [
    id 64
    label "kalendy"
  ]
  node [
    id 65
    label "formacja"
  ]
  node [
    id 66
    label "yearbook"
  ]
  node [
    id 67
    label "czasopismo"
  ]
  node [
    id 68
    label "kronika"
  ]
  node [
    id 69
    label "Bund"
  ]
  node [
    id 70
    label "Mazowsze"
  ]
  node [
    id 71
    label "PPR"
  ]
  node [
    id 72
    label "Jakobici"
  ]
  node [
    id 73
    label "zesp&#243;&#322;"
  ]
  node [
    id 74
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 75
    label "leksem"
  ]
  node [
    id 76
    label "SLD"
  ]
  node [
    id 77
    label "zespolik"
  ]
  node [
    id 78
    label "Razem"
  ]
  node [
    id 79
    label "PiS"
  ]
  node [
    id 80
    label "zjawisko"
  ]
  node [
    id 81
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 82
    label "partia"
  ]
  node [
    id 83
    label "Kuomintang"
  ]
  node [
    id 84
    label "ZSL"
  ]
  node [
    id 85
    label "szko&#322;a"
  ]
  node [
    id 86
    label "jednostka"
  ]
  node [
    id 87
    label "proces"
  ]
  node [
    id 88
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 89
    label "rugby"
  ]
  node [
    id 90
    label "AWS"
  ]
  node [
    id 91
    label "posta&#263;"
  ]
  node [
    id 92
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 93
    label "blok"
  ]
  node [
    id 94
    label "PO"
  ]
  node [
    id 95
    label "si&#322;a"
  ]
  node [
    id 96
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 97
    label "Federali&#347;ci"
  ]
  node [
    id 98
    label "PSL"
  ]
  node [
    id 99
    label "czynno&#347;&#263;"
  ]
  node [
    id 100
    label "wojsko"
  ]
  node [
    id 101
    label "Wigowie"
  ]
  node [
    id 102
    label "ZChN"
  ]
  node [
    id 103
    label "egzekutywa"
  ]
  node [
    id 104
    label "The_Beatles"
  ]
  node [
    id 105
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 106
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 107
    label "unit"
  ]
  node [
    id 108
    label "Depeche_Mode"
  ]
  node [
    id 109
    label "forma"
  ]
  node [
    id 110
    label "zapis"
  ]
  node [
    id 111
    label "chronograf"
  ]
  node [
    id 112
    label "latopis"
  ]
  node [
    id 113
    label "ksi&#281;ga"
  ]
  node [
    id 114
    label "egzemplarz"
  ]
  node [
    id 115
    label "psychotest"
  ]
  node [
    id 116
    label "pismo"
  ]
  node [
    id 117
    label "communication"
  ]
  node [
    id 118
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 119
    label "wk&#322;ad"
  ]
  node [
    id 120
    label "zajawka"
  ]
  node [
    id 121
    label "ok&#322;adka"
  ]
  node [
    id 122
    label "Zwrotnica"
  ]
  node [
    id 123
    label "dzia&#322;"
  ]
  node [
    id 124
    label "prasa"
  ]
  node [
    id 125
    label "godzina"
  ]
  node [
    id 126
    label "time"
  ]
  node [
    id 127
    label "doba"
  ]
  node [
    id 128
    label "p&#243;&#322;godzina"
  ]
  node [
    id 129
    label "jednostka_czasu"
  ]
  node [
    id 130
    label "minuta"
  ]
  node [
    id 131
    label "kwadrans"
  ]
  node [
    id 132
    label "termin"
  ]
  node [
    id 133
    label "obchody"
  ]
  node [
    id 134
    label "nazewnictwo"
  ]
  node [
    id 135
    label "term"
  ]
  node [
    id 136
    label "przypadni&#281;cie"
  ]
  node [
    id 137
    label "ekspiracja"
  ]
  node [
    id 138
    label "przypa&#347;&#263;"
  ]
  node [
    id 139
    label "chronogram"
  ]
  node [
    id 140
    label "praktyka"
  ]
  node [
    id 141
    label "nazwa"
  ]
  node [
    id 142
    label "fest"
  ]
  node [
    id 143
    label "celebration"
  ]
  node [
    id 144
    label "danie"
  ]
  node [
    id 145
    label "confession"
  ]
  node [
    id 146
    label "stwierdzenie"
  ]
  node [
    id 147
    label "recognition"
  ]
  node [
    id 148
    label "oznajmienie"
  ]
  node [
    id 149
    label "obiecanie"
  ]
  node [
    id 150
    label "zap&#322;acenie"
  ]
  node [
    id 151
    label "cios"
  ]
  node [
    id 152
    label "give"
  ]
  node [
    id 153
    label "udost&#281;pnienie"
  ]
  node [
    id 154
    label "rendition"
  ]
  node [
    id 155
    label "wymienienie_si&#281;"
  ]
  node [
    id 156
    label "eating"
  ]
  node [
    id 157
    label "coup"
  ]
  node [
    id 158
    label "hand"
  ]
  node [
    id 159
    label "uprawianie_seksu"
  ]
  node [
    id 160
    label "allow"
  ]
  node [
    id 161
    label "dostarczenie"
  ]
  node [
    id 162
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 163
    label "uderzenie"
  ]
  node [
    id 164
    label "zadanie"
  ]
  node [
    id 165
    label "powierzenie"
  ]
  node [
    id 166
    label "przeznaczenie"
  ]
  node [
    id 167
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 168
    label "przekazanie"
  ]
  node [
    id 169
    label "odst&#261;pienie"
  ]
  node [
    id 170
    label "dodanie"
  ]
  node [
    id 171
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 172
    label "wyposa&#380;enie"
  ]
  node [
    id 173
    label "dostanie"
  ]
  node [
    id 174
    label "karta"
  ]
  node [
    id 175
    label "potrawa"
  ]
  node [
    id 176
    label "pass"
  ]
  node [
    id 177
    label "menu"
  ]
  node [
    id 178
    label "uderzanie"
  ]
  node [
    id 179
    label "wyst&#261;pienie"
  ]
  node [
    id 180
    label "jedzenie"
  ]
  node [
    id 181
    label "wyposa&#380;anie"
  ]
  node [
    id 182
    label "pobicie"
  ]
  node [
    id 183
    label "posi&#322;ek"
  ]
  node [
    id 184
    label "urz&#261;dzenie"
  ]
  node [
    id 185
    label "zrobienie"
  ]
  node [
    id 186
    label "wypowied&#378;"
  ]
  node [
    id 187
    label "ustalenie"
  ]
  node [
    id 188
    label "claim"
  ]
  node [
    id 189
    label "statement"
  ]
  node [
    id 190
    label "wypowiedzenie"
  ]
  node [
    id 191
    label "manifesto"
  ]
  node [
    id 192
    label "zwiastowanie"
  ]
  node [
    id 193
    label "announcement"
  ]
  node [
    id 194
    label "apel"
  ]
  node [
    id 195
    label "Manifest_lipcowy"
  ]
  node [
    id 196
    label "poinformowanie"
  ]
  node [
    id 197
    label "podmiot"
  ]
  node [
    id 198
    label "jednostka_organizacyjna"
  ]
  node [
    id 199
    label "struktura"
  ]
  node [
    id 200
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 201
    label "TOPR"
  ]
  node [
    id 202
    label "endecki"
  ]
  node [
    id 203
    label "przedstawicielstwo"
  ]
  node [
    id 204
    label "od&#322;am"
  ]
  node [
    id 205
    label "Cepelia"
  ]
  node [
    id 206
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 207
    label "ZBoWiD"
  ]
  node [
    id 208
    label "organization"
  ]
  node [
    id 209
    label "centrala"
  ]
  node [
    id 210
    label "GOPR"
  ]
  node [
    id 211
    label "ZOMO"
  ]
  node [
    id 212
    label "ZMP"
  ]
  node [
    id 213
    label "komitet_koordynacyjny"
  ]
  node [
    id 214
    label "przybud&#243;wka"
  ]
  node [
    id 215
    label "boj&#243;wka"
  ]
  node [
    id 216
    label "mechanika"
  ]
  node [
    id 217
    label "o&#347;"
  ]
  node [
    id 218
    label "usenet"
  ]
  node [
    id 219
    label "rozprz&#261;c"
  ]
  node [
    id 220
    label "zachowanie"
  ]
  node [
    id 221
    label "cybernetyk"
  ]
  node [
    id 222
    label "podsystem"
  ]
  node [
    id 223
    label "system"
  ]
  node [
    id 224
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 225
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 226
    label "sk&#322;ad"
  ]
  node [
    id 227
    label "systemat"
  ]
  node [
    id 228
    label "cecha"
  ]
  node [
    id 229
    label "konstrukcja"
  ]
  node [
    id 230
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 231
    label "konstelacja"
  ]
  node [
    id 232
    label "odm&#322;adzanie"
  ]
  node [
    id 233
    label "&#346;wietliki"
  ]
  node [
    id 234
    label "zbi&#243;r"
  ]
  node [
    id 235
    label "whole"
  ]
  node [
    id 236
    label "skupienie"
  ]
  node [
    id 237
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 238
    label "odm&#322;adza&#263;"
  ]
  node [
    id 239
    label "zabudowania"
  ]
  node [
    id 240
    label "group"
  ]
  node [
    id 241
    label "schorzenie"
  ]
  node [
    id 242
    label "ro&#347;lina"
  ]
  node [
    id 243
    label "grupa"
  ]
  node [
    id 244
    label "batch"
  ]
  node [
    id 245
    label "odm&#322;odzenie"
  ]
  node [
    id 246
    label "ajencja"
  ]
  node [
    id 247
    label "siedziba"
  ]
  node [
    id 248
    label "agencja"
  ]
  node [
    id 249
    label "bank"
  ]
  node [
    id 250
    label "filia"
  ]
  node [
    id 251
    label "kawa&#322;"
  ]
  node [
    id 252
    label "bry&#322;a"
  ]
  node [
    id 253
    label "fragment"
  ]
  node [
    id 254
    label "struktura_geologiczna"
  ]
  node [
    id 255
    label "section"
  ]
  node [
    id 256
    label "b&#281;ben_wielki"
  ]
  node [
    id 257
    label "Bruksela"
  ]
  node [
    id 258
    label "administration"
  ]
  node [
    id 259
    label "zarz&#261;d"
  ]
  node [
    id 260
    label "stopa"
  ]
  node [
    id 261
    label "o&#347;rodek"
  ]
  node [
    id 262
    label "w&#322;adza"
  ]
  node [
    id 263
    label "ratownictwo"
  ]
  node [
    id 264
    label "milicja_obywatelska"
  ]
  node [
    id 265
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 266
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 267
    label "byt"
  ]
  node [
    id 268
    label "cz&#322;owiek"
  ]
  node [
    id 269
    label "osobowo&#347;&#263;"
  ]
  node [
    id 270
    label "prawo"
  ]
  node [
    id 271
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 272
    label "nauka_prawa"
  ]
  node [
    id 273
    label "jednostka_monetarna"
  ]
  node [
    id 274
    label "Kosowo"
  ]
  node [
    id 275
    label "Monako"
  ]
  node [
    id 276
    label "Watykan"
  ]
  node [
    id 277
    label "Andora"
  ]
  node [
    id 278
    label "Czarnog&#243;ra"
  ]
  node [
    id 279
    label "San_Marino"
  ]
  node [
    id 280
    label "cent"
  ]
  node [
    id 281
    label "moneta"
  ]
  node [
    id 282
    label "awers"
  ]
  node [
    id 283
    label "legenda"
  ]
  node [
    id 284
    label "liga"
  ]
  node [
    id 285
    label "rewers"
  ]
  node [
    id 286
    label "egzerga"
  ]
  node [
    id 287
    label "pieni&#261;dz"
  ]
  node [
    id 288
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 289
    label "otok"
  ]
  node [
    id 290
    label "balansjerka"
  ]
  node [
    id 291
    label "papie&#380;"
  ]
  node [
    id 292
    label "Rzym"
  ]
  node [
    id 293
    label "Sand&#380;ak"
  ]
  node [
    id 294
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 295
    label "perper"
  ]
  node [
    id 296
    label "Serbia"
  ]
  node [
    id 297
    label "Pireneje"
  ]
  node [
    id 298
    label "frank_monakijski"
  ]
  node [
    id 299
    label "gruba_ryba"
  ]
  node [
    id 300
    label "Gorbaczow"
  ]
  node [
    id 301
    label "zwierzchnik"
  ]
  node [
    id 302
    label "Putin"
  ]
  node [
    id 303
    label "Tito"
  ]
  node [
    id 304
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 305
    label "Naser"
  ]
  node [
    id 306
    label "de_Gaulle"
  ]
  node [
    id 307
    label "Nixon"
  ]
  node [
    id 308
    label "Kemal"
  ]
  node [
    id 309
    label "Clinton"
  ]
  node [
    id 310
    label "Bierut"
  ]
  node [
    id 311
    label "Roosevelt"
  ]
  node [
    id 312
    label "samorz&#261;dowiec"
  ]
  node [
    id 313
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 314
    label "Jelcyn"
  ]
  node [
    id 315
    label "dostojnik"
  ]
  node [
    id 316
    label "pryncypa&#322;"
  ]
  node [
    id 317
    label "kierowa&#263;"
  ]
  node [
    id 318
    label "kierownictwo"
  ]
  node [
    id 319
    label "urz&#281;dnik"
  ]
  node [
    id 320
    label "notabl"
  ]
  node [
    id 321
    label "oficja&#322;"
  ]
  node [
    id 322
    label "samorz&#261;d"
  ]
  node [
    id 323
    label "polityk"
  ]
  node [
    id 324
    label "komuna"
  ]
  node [
    id 325
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 326
    label "wprawi&#263;"
  ]
  node [
    id 327
    label "wykszta&#322;ci&#263;"
  ]
  node [
    id 328
    label "train"
  ]
  node [
    id 329
    label "wywo&#322;a&#263;"
  ]
  node [
    id 330
    label "udoskonali&#263;"
  ]
  node [
    id 331
    label "plant"
  ]
  node [
    id 332
    label "jell"
  ]
  node [
    id 333
    label "umie&#347;ci&#263;"
  ]
  node [
    id 334
    label "Had&#380;ar"
  ]
  node [
    id 335
    label "kamienienie"
  ]
  node [
    id 336
    label "oczko"
  ]
  node [
    id 337
    label "ska&#322;a"
  ]
  node [
    id 338
    label "osad"
  ]
  node [
    id 339
    label "ci&#281;&#380;ar"
  ]
  node [
    id 340
    label "p&#322;ytka"
  ]
  node [
    id 341
    label "skamienienie"
  ]
  node [
    id 342
    label "cube"
  ]
  node [
    id 343
    label "funt"
  ]
  node [
    id 344
    label "mad&#380;ong"
  ]
  node [
    id 345
    label "tworzywo"
  ]
  node [
    id 346
    label "jednostka_avoirdupois"
  ]
  node [
    id 347
    label "domino"
  ]
  node [
    id 348
    label "rock"
  ]
  node [
    id 349
    label "z&#322;&#243;g"
  ]
  node [
    id 350
    label "lapidarium"
  ]
  node [
    id 351
    label "autografia"
  ]
  node [
    id 352
    label "rekwizyt_do_gry"
  ]
  node [
    id 353
    label "minera&#322;_barwny"
  ]
  node [
    id 354
    label "sedymentacja"
  ]
  node [
    id 355
    label "kompakcja"
  ]
  node [
    id 356
    label "terygeniczny"
  ]
  node [
    id 357
    label "wspomnienie"
  ]
  node [
    id 358
    label "warstwa"
  ]
  node [
    id 359
    label "kolmatacja"
  ]
  node [
    id 360
    label "deposit"
  ]
  node [
    id 361
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 362
    label "uskoczenie"
  ]
  node [
    id 363
    label "mieszanina"
  ]
  node [
    id 364
    label "zmetamorfizowanie"
  ]
  node [
    id 365
    label "soczewa"
  ]
  node [
    id 366
    label "opoka"
  ]
  node [
    id 367
    label "uskakiwa&#263;"
  ]
  node [
    id 368
    label "sklerometr"
  ]
  node [
    id 369
    label "&#322;upkowato&#347;&#263;"
  ]
  node [
    id 370
    label "uskakiwanie"
  ]
  node [
    id 371
    label "uskoczy&#263;"
  ]
  node [
    id 372
    label "obiekt"
  ]
  node [
    id 373
    label "porwak"
  ]
  node [
    id 374
    label "bloczno&#347;&#263;"
  ]
  node [
    id 375
    label "zmetamorfizowa&#263;"
  ]
  node [
    id 376
    label "lepiszcze_skalne"
  ]
  node [
    id 377
    label "rygiel"
  ]
  node [
    id 378
    label "lamina"
  ]
  node [
    id 379
    label "wyg&#322;ad_lodowcowy"
  ]
  node [
    id 380
    label "blaszka"
  ]
  node [
    id 381
    label "plate"
  ]
  node [
    id 382
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 383
    label "p&#322;yta"
  ]
  node [
    id 384
    label "dysk_optyczny"
  ]
  node [
    id 385
    label "zmiana"
  ]
  node [
    id 386
    label "warto&#347;&#263;"
  ]
  node [
    id 387
    label "przedmiot"
  ]
  node [
    id 388
    label "przeszkoda"
  ]
  node [
    id 389
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 390
    label "hantla"
  ]
  node [
    id 391
    label "hazard"
  ]
  node [
    id 392
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 393
    label "zawa&#380;y&#263;"
  ]
  node [
    id 394
    label "wym&#243;g"
  ]
  node [
    id 395
    label "obarczy&#263;"
  ]
  node [
    id 396
    label "zawa&#380;enie"
  ]
  node [
    id 397
    label "weight"
  ]
  node [
    id 398
    label "powinno&#347;&#263;"
  ]
  node [
    id 399
    label "load"
  ]
  node [
    id 400
    label "substancja"
  ]
  node [
    id 401
    label "pens_brytyjski"
  ]
  node [
    id 402
    label "Falklandy"
  ]
  node [
    id 403
    label "Wielka_Brytania"
  ]
  node [
    id 404
    label "Wyspa_Man"
  ]
  node [
    id 405
    label "uncja"
  ]
  node [
    id 406
    label "cetnar"
  ]
  node [
    id 407
    label "Wyspa_&#346;wi&#281;tej_Heleny"
  ]
  node [
    id 408
    label "Guernsey"
  ]
  node [
    id 409
    label "marka"
  ]
  node [
    id 410
    label "haczyk"
  ]
  node [
    id 411
    label "&#347;cieg"
  ]
  node [
    id 412
    label "ekoton"
  ]
  node [
    id 413
    label "staw"
  ]
  node [
    id 414
    label "rzecz"
  ]
  node [
    id 415
    label "ozdoba"
  ]
  node [
    id 416
    label "ladder"
  ]
  node [
    id 417
    label "szczep"
  ]
  node [
    id 418
    label "stawon&#243;g"
  ]
  node [
    id 419
    label "punkt"
  ]
  node [
    id 420
    label "p&#281;telka"
  ]
  node [
    id 421
    label "organ"
  ]
  node [
    id 422
    label "eye"
  ]
  node [
    id 423
    label "czcionka"
  ]
  node [
    id 424
    label "k&#243;&#322;ko"
  ]
  node [
    id 425
    label "pier&#347;cionek"
  ]
  node [
    id 426
    label "p&#261;k"
  ]
  node [
    id 427
    label "ros&#243;&#322;"
  ]
  node [
    id 428
    label "kostka"
  ]
  node [
    id 429
    label "uk&#322;ad"
  ]
  node [
    id 430
    label "oko"
  ]
  node [
    id 431
    label "ziemniak"
  ]
  node [
    id 432
    label "ogr&#243;d_wodny"
  ]
  node [
    id 433
    label "gra_w_karty"
  ]
  node [
    id 434
    label "muzeum"
  ]
  node [
    id 435
    label "gra_towarzyska"
  ]
  node [
    id 436
    label "gra"
  ]
  node [
    id 437
    label "kaptur"
  ]
  node [
    id 438
    label "p&#322;aszcz"
  ]
  node [
    id 439
    label "kostium"
  ]
  node [
    id 440
    label "technika_litograficzna"
  ]
  node [
    id 441
    label "napis"
  ]
  node [
    id 442
    label "reprodukcja"
  ]
  node [
    id 443
    label "przenoszenie"
  ]
  node [
    id 444
    label "oboj&#281;tnienie"
  ]
  node [
    id 445
    label "nieruchomienie"
  ]
  node [
    id 446
    label "twardnienie"
  ]
  node [
    id 447
    label "petrifaction"
  ]
  node [
    id 448
    label "stawanie_si&#281;"
  ]
  node [
    id 449
    label "zoboj&#281;tnienie"
  ]
  node [
    id 450
    label "stwardnienie"
  ]
  node [
    id 451
    label "stanie_si&#281;"
  ]
  node [
    id 452
    label "znieruchomienie"
  ]
  node [
    id 453
    label "riff"
  ]
  node [
    id 454
    label "muzyka_rozrywkowa"
  ]
  node [
    id 455
    label "w&#281;glowy"
  ]
  node [
    id 456
    label "w&#281;glisty"
  ]
  node [
    id 457
    label "figura"
  ]
  node [
    id 458
    label "miejsce_pracy"
  ]
  node [
    id 459
    label "kreacja"
  ]
  node [
    id 460
    label "zwierz&#281;"
  ]
  node [
    id 461
    label "r&#243;w"
  ]
  node [
    id 462
    label "posesja"
  ]
  node [
    id 463
    label "wjazd"
  ]
  node [
    id 464
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 465
    label "praca"
  ]
  node [
    id 466
    label "constitution"
  ]
  node [
    id 467
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 468
    label "najem"
  ]
  node [
    id 469
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 470
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 471
    label "zak&#322;ad"
  ]
  node [
    id 472
    label "stosunek_pracy"
  ]
  node [
    id 473
    label "benedykty&#324;ski"
  ]
  node [
    id 474
    label "poda&#380;_pracy"
  ]
  node [
    id 475
    label "pracowanie"
  ]
  node [
    id 476
    label "tyrka"
  ]
  node [
    id 477
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 478
    label "wytw&#243;r"
  ]
  node [
    id 479
    label "zaw&#243;d"
  ]
  node [
    id 480
    label "tynkarski"
  ]
  node [
    id 481
    label "czynnik_produkcji"
  ]
  node [
    id 482
    label "zobowi&#261;zanie"
  ]
  node [
    id 483
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 484
    label "charakterystyka"
  ]
  node [
    id 485
    label "m&#322;ot"
  ]
  node [
    id 486
    label "znak"
  ]
  node [
    id 487
    label "drzewo"
  ]
  node [
    id 488
    label "pr&#243;ba"
  ]
  node [
    id 489
    label "attribute"
  ]
  node [
    id 490
    label "plisa"
  ]
  node [
    id 491
    label "ustawienie"
  ]
  node [
    id 492
    label "function"
  ]
  node [
    id 493
    label "tren"
  ]
  node [
    id 494
    label "zreinterpretowa&#263;"
  ]
  node [
    id 495
    label "element"
  ]
  node [
    id 496
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 497
    label "production"
  ]
  node [
    id 498
    label "reinterpretowa&#263;"
  ]
  node [
    id 499
    label "str&#243;j"
  ]
  node [
    id 500
    label "ustawi&#263;"
  ]
  node [
    id 501
    label "zreinterpretowanie"
  ]
  node [
    id 502
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 503
    label "gra&#263;"
  ]
  node [
    id 504
    label "aktorstwo"
  ]
  node [
    id 505
    label "toaleta"
  ]
  node [
    id 506
    label "zagra&#263;"
  ]
  node [
    id 507
    label "reinterpretowanie"
  ]
  node [
    id 508
    label "zagranie"
  ]
  node [
    id 509
    label "granie"
  ]
  node [
    id 510
    label "obszar"
  ]
  node [
    id 511
    label "degenerat"
  ]
  node [
    id 512
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 513
    label "zwyrol"
  ]
  node [
    id 514
    label "czerniak"
  ]
  node [
    id 515
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 516
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 517
    label "paszcza"
  ]
  node [
    id 518
    label "popapraniec"
  ]
  node [
    id 519
    label "skuba&#263;"
  ]
  node [
    id 520
    label "skubanie"
  ]
  node [
    id 521
    label "agresja"
  ]
  node [
    id 522
    label "skubni&#281;cie"
  ]
  node [
    id 523
    label "zwierz&#281;ta"
  ]
  node [
    id 524
    label "fukni&#281;cie"
  ]
  node [
    id 525
    label "farba"
  ]
  node [
    id 526
    label "fukanie"
  ]
  node [
    id 527
    label "istota_&#380;ywa"
  ]
  node [
    id 528
    label "gad"
  ]
  node [
    id 529
    label "tresowa&#263;"
  ]
  node [
    id 530
    label "siedzie&#263;"
  ]
  node [
    id 531
    label "oswaja&#263;"
  ]
  node [
    id 532
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 533
    label "poligamia"
  ]
  node [
    id 534
    label "oz&#243;r"
  ]
  node [
    id 535
    label "skubn&#261;&#263;"
  ]
  node [
    id 536
    label "wios&#322;owa&#263;"
  ]
  node [
    id 537
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 538
    label "le&#380;enie"
  ]
  node [
    id 539
    label "niecz&#322;owiek"
  ]
  node [
    id 540
    label "wios&#322;owanie"
  ]
  node [
    id 541
    label "napasienie_si&#281;"
  ]
  node [
    id 542
    label "wiwarium"
  ]
  node [
    id 543
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 544
    label "animalista"
  ]
  node [
    id 545
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 546
    label "hodowla"
  ]
  node [
    id 547
    label "pasienie_si&#281;"
  ]
  node [
    id 548
    label "sodomita"
  ]
  node [
    id 549
    label "monogamia"
  ]
  node [
    id 550
    label "przyssawka"
  ]
  node [
    id 551
    label "budowa_cia&#322;a"
  ]
  node [
    id 552
    label "okrutnik"
  ]
  node [
    id 553
    label "grzbiet"
  ]
  node [
    id 554
    label "weterynarz"
  ]
  node [
    id 555
    label "&#322;eb"
  ]
  node [
    id 556
    label "wylinka"
  ]
  node [
    id 557
    label "bestia"
  ]
  node [
    id 558
    label "poskramia&#263;"
  ]
  node [
    id 559
    label "fauna"
  ]
  node [
    id 560
    label "treser"
  ]
  node [
    id 561
    label "siedzenie"
  ]
  node [
    id 562
    label "le&#380;e&#263;"
  ]
  node [
    id 563
    label "tkanka"
  ]
  node [
    id 564
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 565
    label "tw&#243;r"
  ]
  node [
    id 566
    label "organogeneza"
  ]
  node [
    id 567
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 568
    label "struktura_anatomiczna"
  ]
  node [
    id 569
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 570
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 571
    label "Izba_Konsyliarska"
  ]
  node [
    id 572
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 573
    label "stomia"
  ]
  node [
    id 574
    label "dekortykacja"
  ]
  node [
    id 575
    label "okolica"
  ]
  node [
    id 576
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 577
    label "Komitet_Region&#243;w"
  ]
  node [
    id 578
    label "p&#322;aszczyzna"
  ]
  node [
    id 579
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 580
    label "bierka_szachowa"
  ]
  node [
    id 581
    label "obiekt_matematyczny"
  ]
  node [
    id 582
    label "gestaltyzm"
  ]
  node [
    id 583
    label "styl"
  ]
  node [
    id 584
    label "obraz"
  ]
  node [
    id 585
    label "Osjan"
  ]
  node [
    id 586
    label "d&#378;wi&#281;k"
  ]
  node [
    id 587
    label "character"
  ]
  node [
    id 588
    label "kto&#347;"
  ]
  node [
    id 589
    label "rze&#378;ba"
  ]
  node [
    id 590
    label "stylistyka"
  ]
  node [
    id 591
    label "figure"
  ]
  node [
    id 592
    label "wygl&#261;d"
  ]
  node [
    id 593
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 594
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 595
    label "antycypacja"
  ]
  node [
    id 596
    label "ornamentyka"
  ]
  node [
    id 597
    label "sztuka"
  ]
  node [
    id 598
    label "informacja"
  ]
  node [
    id 599
    label "Aspazja"
  ]
  node [
    id 600
    label "facet"
  ]
  node [
    id 601
    label "popis"
  ]
  node [
    id 602
    label "wiersz"
  ]
  node [
    id 603
    label "kompleksja"
  ]
  node [
    id 604
    label "symetria"
  ]
  node [
    id 605
    label "lingwistyka_kognitywna"
  ]
  node [
    id 606
    label "shape"
  ]
  node [
    id 607
    label "podzbi&#243;r"
  ]
  node [
    id 608
    label "przedstawienie"
  ]
  node [
    id 609
    label "point"
  ]
  node [
    id 610
    label "perspektywa"
  ]
  node [
    id 611
    label "practice"
  ]
  node [
    id 612
    label "wykre&#347;lanie"
  ]
  node [
    id 613
    label "element_konstrukcyjny"
  ]
  node [
    id 614
    label "mechanika_teoretyczna"
  ]
  node [
    id 615
    label "mechanika_gruntu"
  ]
  node [
    id 616
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 617
    label "mechanika_klasyczna"
  ]
  node [
    id 618
    label "elektromechanika"
  ]
  node [
    id 619
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 620
    label "ruch"
  ]
  node [
    id 621
    label "nauka"
  ]
  node [
    id 622
    label "fizyka"
  ]
  node [
    id 623
    label "aeromechanika"
  ]
  node [
    id 624
    label "telemechanika"
  ]
  node [
    id 625
    label "hydromechanika"
  ]
  node [
    id 626
    label "droga"
  ]
  node [
    id 627
    label "wydarzenie"
  ]
  node [
    id 628
    label "zawiasy"
  ]
  node [
    id 629
    label "antaba"
  ]
  node [
    id 630
    label "ogrodzenie"
  ]
  node [
    id 631
    label "zamek"
  ]
  node [
    id 632
    label "wrzeci&#261;dz"
  ]
  node [
    id 633
    label "dost&#281;p"
  ]
  node [
    id 634
    label "wej&#347;cie"
  ]
  node [
    id 635
    label "zrzutowy"
  ]
  node [
    id 636
    label "odk&#322;ad"
  ]
  node [
    id 637
    label "chody"
  ]
  node [
    id 638
    label "szaniec"
  ]
  node [
    id 639
    label "budowla"
  ]
  node [
    id 640
    label "fortyfikacja"
  ]
  node [
    id 641
    label "obni&#380;enie"
  ]
  node [
    id 642
    label "przedpiersie"
  ]
  node [
    id 643
    label "formacja_geologiczna"
  ]
  node [
    id 644
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 645
    label "odwa&#322;"
  ]
  node [
    id 646
    label "grodzisko"
  ]
  node [
    id 647
    label "blinda&#380;"
  ]
  node [
    id 648
    label "kolejny"
  ]
  node [
    id 649
    label "nowo"
  ]
  node [
    id 650
    label "bie&#380;&#261;cy"
  ]
  node [
    id 651
    label "drugi"
  ]
  node [
    id 652
    label "narybek"
  ]
  node [
    id 653
    label "obcy"
  ]
  node [
    id 654
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 655
    label "nowotny"
  ]
  node [
    id 656
    label "nadprzyrodzony"
  ]
  node [
    id 657
    label "nieznany"
  ]
  node [
    id 658
    label "pozaludzki"
  ]
  node [
    id 659
    label "obco"
  ]
  node [
    id 660
    label "tameczny"
  ]
  node [
    id 661
    label "osoba"
  ]
  node [
    id 662
    label "nieznajomo"
  ]
  node [
    id 663
    label "inny"
  ]
  node [
    id 664
    label "cudzy"
  ]
  node [
    id 665
    label "zaziemsko"
  ]
  node [
    id 666
    label "jednoczesny"
  ]
  node [
    id 667
    label "unowocze&#347;nianie"
  ]
  node [
    id 668
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 669
    label "tera&#378;niejszy"
  ]
  node [
    id 670
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 671
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 672
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 673
    label "nast&#281;pnie"
  ]
  node [
    id 674
    label "nastopny"
  ]
  node [
    id 675
    label "kolejno"
  ]
  node [
    id 676
    label "kt&#243;ry&#347;"
  ]
  node [
    id 677
    label "sw&#243;j"
  ]
  node [
    id 678
    label "przeciwny"
  ]
  node [
    id 679
    label "wt&#243;ry"
  ]
  node [
    id 680
    label "dzie&#324;"
  ]
  node [
    id 681
    label "odwrotnie"
  ]
  node [
    id 682
    label "podobny"
  ]
  node [
    id 683
    label "bie&#380;&#261;co"
  ]
  node [
    id 684
    label "ci&#261;g&#322;y"
  ]
  node [
    id 685
    label "aktualny"
  ]
  node [
    id 686
    label "ludzko&#347;&#263;"
  ]
  node [
    id 687
    label "asymilowanie"
  ]
  node [
    id 688
    label "wapniak"
  ]
  node [
    id 689
    label "asymilowa&#263;"
  ]
  node [
    id 690
    label "os&#322;abia&#263;"
  ]
  node [
    id 691
    label "hominid"
  ]
  node [
    id 692
    label "podw&#322;adny"
  ]
  node [
    id 693
    label "os&#322;abianie"
  ]
  node [
    id 694
    label "g&#322;owa"
  ]
  node [
    id 695
    label "portrecista"
  ]
  node [
    id 696
    label "dwun&#243;g"
  ]
  node [
    id 697
    label "profanum"
  ]
  node [
    id 698
    label "mikrokosmos"
  ]
  node [
    id 699
    label "nasada"
  ]
  node [
    id 700
    label "duch"
  ]
  node [
    id 701
    label "antropochoria"
  ]
  node [
    id 702
    label "wz&#243;r"
  ]
  node [
    id 703
    label "senior"
  ]
  node [
    id 704
    label "oddzia&#322;ywanie"
  ]
  node [
    id 705
    label "Adam"
  ]
  node [
    id 706
    label "homo_sapiens"
  ]
  node [
    id 707
    label "polifag"
  ]
  node [
    id 708
    label "dopiero_co"
  ]
  node [
    id 709
    label "potomstwo"
  ]
  node [
    id 710
    label "terminal"
  ]
  node [
    id 711
    label "droga_ko&#322;owania"
  ]
  node [
    id 712
    label "p&#322;yta_postojowa"
  ]
  node [
    id 713
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 714
    label "aerodrom"
  ]
  node [
    id 715
    label "pas_startowy"
  ]
  node [
    id 716
    label "baza"
  ]
  node [
    id 717
    label "hala"
  ]
  node [
    id 718
    label "betonka"
  ]
  node [
    id 719
    label "rekord"
  ]
  node [
    id 720
    label "poj&#281;cie"
  ]
  node [
    id 721
    label "base"
  ]
  node [
    id 722
    label "stacjonowanie"
  ]
  node [
    id 723
    label "documentation"
  ]
  node [
    id 724
    label "pole"
  ]
  node [
    id 725
    label "zasadzenie"
  ]
  node [
    id 726
    label "zasadzi&#263;"
  ]
  node [
    id 727
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 728
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 729
    label "podstawowy"
  ]
  node [
    id 730
    label "baseball"
  ]
  node [
    id 731
    label "kolumna"
  ]
  node [
    id 732
    label "kosmetyk"
  ]
  node [
    id 733
    label "za&#322;o&#380;enie"
  ]
  node [
    id 734
    label "punkt_odniesienia"
  ]
  node [
    id 735
    label "boisko"
  ]
  node [
    id 736
    label "system_bazy_danych"
  ]
  node [
    id 737
    label "informatyka"
  ]
  node [
    id 738
    label "podstawa"
  ]
  node [
    id 739
    label "obudowanie"
  ]
  node [
    id 740
    label "obudowywa&#263;"
  ]
  node [
    id 741
    label "zbudowa&#263;"
  ]
  node [
    id 742
    label "obudowa&#263;"
  ]
  node [
    id 743
    label "kolumnada"
  ]
  node [
    id 744
    label "korpus"
  ]
  node [
    id 745
    label "Sukiennice"
  ]
  node [
    id 746
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 747
    label "fundament"
  ]
  node [
    id 748
    label "postanie"
  ]
  node [
    id 749
    label "obudowywanie"
  ]
  node [
    id 750
    label "zbudowanie"
  ]
  node [
    id 751
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 752
    label "stan_surowy"
  ]
  node [
    id 753
    label "pod&#322;oga"
  ]
  node [
    id 754
    label "port"
  ]
  node [
    id 755
    label "dworzec"
  ]
  node [
    id 756
    label "oczyszczalnia"
  ]
  node [
    id 757
    label "huta"
  ]
  node [
    id 758
    label "kopalnia"
  ]
  node [
    id 759
    label "pomieszczenie"
  ]
  node [
    id 760
    label "pastwisko"
  ]
  node [
    id 761
    label "pi&#281;tro"
  ]
  node [
    id 762
    label "halizna"
  ]
  node [
    id 763
    label "fabryka"
  ]
  node [
    id 764
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 765
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 766
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 767
    label "transgraniczny"
  ]
  node [
    id 768
    label "zbiorowo"
  ]
  node [
    id 769
    label "internationalization"
  ]
  node [
    id 770
    label "uwsp&#243;lnienie"
  ]
  node [
    id 771
    label "udost&#281;pnianie"
  ]
  node [
    id 772
    label "przyrzeczenie_harcerskie"
  ]
  node [
    id 773
    label "przysi&#281;ga_wojskowa"
  ]
  node [
    id 774
    label "patos"
  ]
  node [
    id 775
    label "egzaltacja"
  ]
  node [
    id 776
    label "atmosfera"
  ]
  node [
    id 777
    label "troposfera"
  ]
  node [
    id 778
    label "klimat"
  ]
  node [
    id 779
    label "obiekt_naturalny"
  ]
  node [
    id 780
    label "metasfera"
  ]
  node [
    id 781
    label "atmosferyki"
  ]
  node [
    id 782
    label "homosfera"
  ]
  node [
    id 783
    label "powietrznia"
  ]
  node [
    id 784
    label "jonosfera"
  ]
  node [
    id 785
    label "planeta"
  ]
  node [
    id 786
    label "termosfera"
  ]
  node [
    id 787
    label "egzosfera"
  ]
  node [
    id 788
    label "heterosfera"
  ]
  node [
    id 789
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 790
    label "tropopauza"
  ]
  node [
    id 791
    label "kwas"
  ]
  node [
    id 792
    label "powietrze"
  ]
  node [
    id 793
    label "stratosfera"
  ]
  node [
    id 794
    label "pow&#322;oka"
  ]
  node [
    id 795
    label "charakter"
  ]
  node [
    id 796
    label "mezosfera"
  ]
  node [
    id 797
    label "Ziemia"
  ]
  node [
    id 798
    label "mezopauza"
  ]
  node [
    id 799
    label "atmosphere"
  ]
  node [
    id 800
    label "przebiec"
  ]
  node [
    id 801
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 802
    label "motyw"
  ]
  node [
    id 803
    label "przebiegni&#281;cie"
  ]
  node [
    id 804
    label "fabu&#322;a"
  ]
  node [
    id 805
    label "pompatyczno&#347;&#263;"
  ]
  node [
    id 806
    label "pathos"
  ]
  node [
    id 807
    label "podnios&#322;o&#347;&#263;"
  ]
  node [
    id 808
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 809
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 810
    label "deklamowa&#263;"
  ]
  node [
    id 811
    label "deklamowanie"
  ]
  node [
    id 812
    label "przyjemnie"
  ]
  node [
    id 813
    label "porz&#261;dnie"
  ]
  node [
    id 814
    label "zdrowo"
  ]
  node [
    id 815
    label "cleanly"
  ]
  node [
    id 816
    label "bezpiecznie"
  ]
  node [
    id 817
    label "bezb&#322;&#281;dnie"
  ]
  node [
    id 818
    label "doskonale"
  ]
  node [
    id 819
    label "klarownie"
  ]
  node [
    id 820
    label "uczciwie"
  ]
  node [
    id 821
    label "ewidentnie"
  ]
  node [
    id 822
    label "transparently"
  ]
  node [
    id 823
    label "bezchmurnie"
  ]
  node [
    id 824
    label "przezroczysty"
  ]
  node [
    id 825
    label "kompletnie"
  ]
  node [
    id 826
    label "czysty"
  ]
  node [
    id 827
    label "ostro"
  ]
  node [
    id 828
    label "ekologicznie"
  ]
  node [
    id 829
    label "udanie"
  ]
  node [
    id 830
    label "cnotliwie"
  ]
  node [
    id 831
    label "przezroczo"
  ]
  node [
    id 832
    label "prawdziwie"
  ]
  node [
    id 833
    label "moralnie"
  ]
  node [
    id 834
    label "morally"
  ]
  node [
    id 835
    label "cnotliwy"
  ]
  node [
    id 836
    label "szczero"
  ]
  node [
    id 837
    label "podobnie"
  ]
  node [
    id 838
    label "zgodnie"
  ]
  node [
    id 839
    label "naprawd&#281;"
  ]
  node [
    id 840
    label "szczerze"
  ]
  node [
    id 841
    label "truly"
  ]
  node [
    id 842
    label "prawdziwy"
  ]
  node [
    id 843
    label "rzeczywisty"
  ]
  node [
    id 844
    label "pogodnie"
  ]
  node [
    id 845
    label "bezchmurny"
  ]
  node [
    id 846
    label "bezpieczny"
  ]
  node [
    id 847
    label "&#322;atwo"
  ]
  node [
    id 848
    label "bezpieczno"
  ]
  node [
    id 849
    label "kompletny"
  ]
  node [
    id 850
    label "zupe&#322;nie"
  ]
  node [
    id 851
    label "zachowanie_si&#281;"
  ]
  node [
    id 852
    label "dobrze"
  ]
  node [
    id 853
    label "udany"
  ]
  node [
    id 854
    label "maneuver"
  ]
  node [
    id 855
    label "excellently"
  ]
  node [
    id 856
    label "kapitalnie"
  ]
  node [
    id 857
    label "prawid&#322;owo"
  ]
  node [
    id 858
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 859
    label "odpowiednio"
  ]
  node [
    id 860
    label "solidnie"
  ]
  node [
    id 861
    label "korzystnie"
  ]
  node [
    id 862
    label "normalnie"
  ]
  node [
    id 863
    label "rozs&#261;dnie"
  ]
  node [
    id 864
    label "zdrowy"
  ]
  node [
    id 865
    label "sprawnie"
  ]
  node [
    id 866
    label "porz&#261;dny"
  ]
  node [
    id 867
    label "ch&#281;dogo"
  ]
  node [
    id 868
    label "intensywnie"
  ]
  node [
    id 869
    label "schludnie"
  ]
  node [
    id 870
    label "pleasantly"
  ]
  node [
    id 871
    label "deliciously"
  ]
  node [
    id 872
    label "przyjemny"
  ]
  node [
    id 873
    label "gratifyingly"
  ]
  node [
    id 874
    label "przyja&#378;nie"
  ]
  node [
    id 875
    label "ekologiczny"
  ]
  node [
    id 876
    label "environmentally"
  ]
  node [
    id 877
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 878
    label "doskona&#322;y"
  ]
  node [
    id 879
    label "wspaniale"
  ]
  node [
    id 880
    label "&#347;wietnie"
  ]
  node [
    id 881
    label "jednoznacznie"
  ]
  node [
    id 882
    label "gryz&#261;co"
  ]
  node [
    id 883
    label "energicznie"
  ]
  node [
    id 884
    label "nieneutralnie"
  ]
  node [
    id 885
    label "dziko"
  ]
  node [
    id 886
    label "widocznie"
  ]
  node [
    id 887
    label "wyra&#378;nie"
  ]
  node [
    id 888
    label "szybko"
  ]
  node [
    id 889
    label "ci&#281;&#380;ko"
  ]
  node [
    id 890
    label "podniecaj&#261;co"
  ]
  node [
    id 891
    label "zdecydowanie"
  ]
  node [
    id 892
    label "ostry"
  ]
  node [
    id 893
    label "niemile"
  ]
  node [
    id 894
    label "raptownie"
  ]
  node [
    id 895
    label "s&#322;uszny"
  ]
  node [
    id 896
    label "s&#322;usznie"
  ]
  node [
    id 897
    label "uczciwy"
  ]
  node [
    id 898
    label "rz&#261;dnie"
  ]
  node [
    id 899
    label "nale&#380;ycie"
  ]
  node [
    id 900
    label "fair"
  ]
  node [
    id 901
    label "rzetelnie"
  ]
  node [
    id 902
    label "zrozumiale"
  ]
  node [
    id 903
    label "klarowny"
  ]
  node [
    id 904
    label "jasny"
  ]
  node [
    id 905
    label "pewnie"
  ]
  node [
    id 906
    label "obviously"
  ]
  node [
    id 907
    label "ewidentny"
  ]
  node [
    id 908
    label "pewny"
  ]
  node [
    id 909
    label "przezroczy&#347;cie"
  ]
  node [
    id 910
    label "nieemisyjny"
  ]
  node [
    id 911
    label "umycie"
  ]
  node [
    id 912
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 913
    label "dobry"
  ]
  node [
    id 914
    label "dopuszczalny"
  ]
  node [
    id 915
    label "ca&#322;y"
  ]
  node [
    id 916
    label "mycie"
  ]
  node [
    id 917
    label "jednolity"
  ]
  node [
    id 918
    label "klarowanie"
  ]
  node [
    id 919
    label "legalny"
  ]
  node [
    id 920
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 921
    label "prze&#378;roczy"
  ]
  node [
    id 922
    label "wolny"
  ]
  node [
    id 923
    label "czyszczenie_si&#281;"
  ]
  node [
    id 924
    label "do_czysta"
  ]
  node [
    id 925
    label "klarowanie_si&#281;"
  ]
  node [
    id 926
    label "sklarowanie"
  ]
  node [
    id 927
    label "wspinaczka"
  ]
  node [
    id 928
    label "schludny"
  ]
  node [
    id 929
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 930
    label "nieodrodny"
  ]
  node [
    id 931
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 932
    label "blady"
  ]
  node [
    id 933
    label "przezroczysto"
  ]
  node [
    id 934
    label "symbolicznie"
  ]
  node [
    id 935
    label "nieprawdziwy"
  ]
  node [
    id 936
    label "ma&#322;y"
  ]
  node [
    id 937
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 938
    label "ma&#322;o"
  ]
  node [
    id 939
    label "szybki"
  ]
  node [
    id 940
    label "nieznaczny"
  ]
  node [
    id 941
    label "przeci&#281;tny"
  ]
  node [
    id 942
    label "wstydliwy"
  ]
  node [
    id 943
    label "s&#322;aby"
  ]
  node [
    id 944
    label "niewa&#380;ny"
  ]
  node [
    id 945
    label "ch&#322;opiec"
  ]
  node [
    id 946
    label "m&#322;ody"
  ]
  node [
    id 947
    label "marny"
  ]
  node [
    id 948
    label "nieliczny"
  ]
  node [
    id 949
    label "n&#281;dznie"
  ]
  node [
    id 950
    label "nieprawdziwie"
  ]
  node [
    id 951
    label "niezgodny"
  ]
  node [
    id 952
    label "udawany"
  ]
  node [
    id 953
    label "prawda"
  ]
  node [
    id 954
    label "nieszczery"
  ]
  node [
    id 955
    label "niehistoryczny"
  ]
  node [
    id 956
    label "chi&#324;sko"
  ]
  node [
    id 957
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 958
    label "po_chi&#324;sku"
  ]
  node [
    id 959
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 960
    label "kitajski"
  ]
  node [
    id 961
    label "lipny"
  ]
  node [
    id 962
    label "go"
  ]
  node [
    id 963
    label "niedrogi"
  ]
  node [
    id 964
    label "makroj&#281;zyk"
  ]
  node [
    id 965
    label "dziwaczny"
  ]
  node [
    id 966
    label "azjatycki"
  ]
  node [
    id 967
    label "j&#281;zyk"
  ]
  node [
    id 968
    label "tandetny"
  ]
  node [
    id 969
    label "dalekowschodni"
  ]
  node [
    id 970
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 971
    label "artykulator"
  ]
  node [
    id 972
    label "kod"
  ]
  node [
    id 973
    label "kawa&#322;ek"
  ]
  node [
    id 974
    label "gramatyka"
  ]
  node [
    id 975
    label "stylik"
  ]
  node [
    id 976
    label "przet&#322;umaczenie"
  ]
  node [
    id 977
    label "formalizowanie"
  ]
  node [
    id 978
    label "ssa&#263;"
  ]
  node [
    id 979
    label "ssanie"
  ]
  node [
    id 980
    label "language"
  ]
  node [
    id 981
    label "liza&#263;"
  ]
  node [
    id 982
    label "napisa&#263;"
  ]
  node [
    id 983
    label "konsonantyzm"
  ]
  node [
    id 984
    label "wokalizm"
  ]
  node [
    id 985
    label "pisa&#263;"
  ]
  node [
    id 986
    label "fonetyka"
  ]
  node [
    id 987
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 988
    label "jeniec"
  ]
  node [
    id 989
    label "but"
  ]
  node [
    id 990
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 991
    label "po_koroniarsku"
  ]
  node [
    id 992
    label "kultura_duchowa"
  ]
  node [
    id 993
    label "t&#322;umaczenie"
  ]
  node [
    id 994
    label "m&#243;wienie"
  ]
  node [
    id 995
    label "pype&#263;"
  ]
  node [
    id 996
    label "lizanie"
  ]
  node [
    id 997
    label "formalizowa&#263;"
  ]
  node [
    id 998
    label "rozumie&#263;"
  ]
  node [
    id 999
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1000
    label "rozumienie"
  ]
  node [
    id 1001
    label "spos&#243;b"
  ]
  node [
    id 1002
    label "makroglosja"
  ]
  node [
    id 1003
    label "m&#243;wi&#263;"
  ]
  node [
    id 1004
    label "jama_ustna"
  ]
  node [
    id 1005
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1006
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1007
    label "natural_language"
  ]
  node [
    id 1008
    label "s&#322;ownictwo"
  ]
  node [
    id 1009
    label "niedrogo"
  ]
  node [
    id 1010
    label "kiczowaty"
  ]
  node [
    id 1011
    label "banalny"
  ]
  node [
    id 1012
    label "nieelegancki"
  ]
  node [
    id 1013
    label "&#380;a&#322;osny"
  ]
  node [
    id 1014
    label "tani"
  ]
  node [
    id 1015
    label "kiepski"
  ]
  node [
    id 1016
    label "tandetnie"
  ]
  node [
    id 1017
    label "odra&#380;aj&#261;cy"
  ]
  node [
    id 1018
    label "nikczemny"
  ]
  node [
    id 1019
    label "lipnie"
  ]
  node [
    id 1020
    label "siajowy"
  ]
  node [
    id 1021
    label "z&#322;y"
  ]
  node [
    id 1022
    label "po_dalekowschodniemu"
  ]
  node [
    id 1023
    label "j&#281;zyk_naturalny"
  ]
  node [
    id 1024
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1025
    label "kampong"
  ]
  node [
    id 1026
    label "typowy"
  ]
  node [
    id 1027
    label "ghaty"
  ]
  node [
    id 1028
    label "charakterystyczny"
  ]
  node [
    id 1029
    label "balut"
  ]
  node [
    id 1030
    label "ka&#322;mucki"
  ]
  node [
    id 1031
    label "azjatycko"
  ]
  node [
    id 1032
    label "dziwny"
  ]
  node [
    id 1033
    label "dziwotworny"
  ]
  node [
    id 1034
    label "dziwacznie"
  ]
  node [
    id 1035
    label "goban"
  ]
  node [
    id 1036
    label "gra_planszowa"
  ]
  node [
    id 1037
    label "sport_umys&#322;owy"
  ]
  node [
    id 1038
    label "owad"
  ]
  node [
    id 1039
    label "samica"
  ]
  node [
    id 1040
    label "samka"
  ]
  node [
    id 1041
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 1042
    label "drogi_rodne"
  ]
  node [
    id 1043
    label "kobieta"
  ]
  node [
    id 1044
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 1045
    label "female"
  ]
  node [
    id 1046
    label "zabrz&#281;czenie"
  ]
  node [
    id 1047
    label "bzyka&#263;"
  ]
  node [
    id 1048
    label "hukni&#281;cie"
  ]
  node [
    id 1049
    label "owady"
  ]
  node [
    id 1050
    label "parabioza"
  ]
  node [
    id 1051
    label "prostoskrzyd&#322;y"
  ]
  node [
    id 1052
    label "skrzyd&#322;o"
  ]
  node [
    id 1053
    label "cierkanie"
  ]
  node [
    id 1054
    label "bzykni&#281;cie"
  ]
  node [
    id 1055
    label "brz&#281;czenie"
  ]
  node [
    id 1056
    label "r&#243;&#380;noskrzyd&#322;y"
  ]
  node [
    id 1057
    label "bzykn&#261;&#263;"
  ]
  node [
    id 1058
    label "aparat_g&#281;bowy"
  ]
  node [
    id 1059
    label "entomofauna"
  ]
  node [
    id 1060
    label "bzykanie"
  ]
  node [
    id 1061
    label "przestarza&#322;y"
  ]
  node [
    id 1062
    label "odleg&#322;y"
  ]
  node [
    id 1063
    label "przesz&#322;y"
  ]
  node [
    id 1064
    label "od_dawna"
  ]
  node [
    id 1065
    label "poprzedni"
  ]
  node [
    id 1066
    label "dawno"
  ]
  node [
    id 1067
    label "d&#322;ugoletni"
  ]
  node [
    id 1068
    label "anachroniczny"
  ]
  node [
    id 1069
    label "dawniej"
  ]
  node [
    id 1070
    label "niegdysiejszy"
  ]
  node [
    id 1071
    label "wcze&#347;niejszy"
  ]
  node [
    id 1072
    label "kombatant"
  ]
  node [
    id 1073
    label "stary"
  ]
  node [
    id 1074
    label "poprzednio"
  ]
  node [
    id 1075
    label "anachronicznie"
  ]
  node [
    id 1076
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 1077
    label "zestarzenie_si&#281;"
  ]
  node [
    id 1078
    label "starzenie_si&#281;"
  ]
  node [
    id 1079
    label "archaicznie"
  ]
  node [
    id 1080
    label "zgrzybienie"
  ]
  node [
    id 1081
    label "niedzisiejszy"
  ]
  node [
    id 1082
    label "staro&#347;wiecki"
  ]
  node [
    id 1083
    label "przestarzale"
  ]
  node [
    id 1084
    label "ongi&#347;"
  ]
  node [
    id 1085
    label "odlegle"
  ]
  node [
    id 1086
    label "delikatny"
  ]
  node [
    id 1087
    label "r&#243;&#380;ny"
  ]
  node [
    id 1088
    label "daleko"
  ]
  node [
    id 1089
    label "daleki"
  ]
  node [
    id 1090
    label "oddalony"
  ]
  node [
    id 1091
    label "nieobecny"
  ]
  node [
    id 1092
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 1093
    label "ojciec"
  ]
  node [
    id 1094
    label "nienowoczesny"
  ]
  node [
    id 1095
    label "staro"
  ]
  node [
    id 1096
    label "m&#261;&#380;"
  ]
  node [
    id 1097
    label "starzy"
  ]
  node [
    id 1098
    label "dotychczasowy"
  ]
  node [
    id 1099
    label "p&#243;&#378;ny"
  ]
  node [
    id 1100
    label "brat"
  ]
  node [
    id 1101
    label "po_staro&#347;wiecku"
  ]
  node [
    id 1102
    label "znajomy"
  ]
  node [
    id 1103
    label "starczo"
  ]
  node [
    id 1104
    label "dojrza&#322;y"
  ]
  node [
    id 1105
    label "wcze&#347;niej"
  ]
  node [
    id 1106
    label "miniony"
  ]
  node [
    id 1107
    label "d&#322;ugi"
  ]
  node [
    id 1108
    label "wieloletni"
  ]
  node [
    id 1109
    label "kiedy&#347;"
  ]
  node [
    id 1110
    label "d&#322;ugotrwale"
  ]
  node [
    id 1111
    label "dawnie"
  ]
  node [
    id 1112
    label "wyjadacz"
  ]
  node [
    id 1113
    label "weteran"
  ]
  node [
    id 1114
    label "endeavor"
  ]
  node [
    id 1115
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1116
    label "mie&#263;_miejsce"
  ]
  node [
    id 1117
    label "podejmowa&#263;"
  ]
  node [
    id 1118
    label "dziama&#263;"
  ]
  node [
    id 1119
    label "do"
  ]
  node [
    id 1120
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 1121
    label "bangla&#263;"
  ]
  node [
    id 1122
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 1123
    label "work"
  ]
  node [
    id 1124
    label "maszyna"
  ]
  node [
    id 1125
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1126
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1127
    label "tryb"
  ]
  node [
    id 1128
    label "funkcjonowa&#263;"
  ]
  node [
    id 1129
    label "podnosi&#263;"
  ]
  node [
    id 1130
    label "robi&#263;"
  ]
  node [
    id 1131
    label "draw"
  ]
  node [
    id 1132
    label "drive"
  ]
  node [
    id 1133
    label "zmienia&#263;"
  ]
  node [
    id 1134
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1135
    label "rise"
  ]
  node [
    id 1136
    label "admit"
  ]
  node [
    id 1137
    label "reagowa&#263;"
  ]
  node [
    id 1138
    label "try"
  ]
  node [
    id 1139
    label "post&#281;powa&#263;"
  ]
  node [
    id 1140
    label "istnie&#263;"
  ]
  node [
    id 1141
    label "determine"
  ]
  node [
    id 1142
    label "powodowa&#263;"
  ]
  node [
    id 1143
    label "reakcja_chemiczna"
  ]
  node [
    id 1144
    label "commit"
  ]
  node [
    id 1145
    label "his"
  ]
  node [
    id 1146
    label "ut"
  ]
  node [
    id 1147
    label "C"
  ]
  node [
    id 1148
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 1149
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 1150
    label "tuleja"
  ]
  node [
    id 1151
    label "kad&#322;ub"
  ]
  node [
    id 1152
    label "n&#243;&#380;"
  ]
  node [
    id 1153
    label "b&#281;benek"
  ]
  node [
    id 1154
    label "wa&#322;"
  ]
  node [
    id 1155
    label "maszyneria"
  ]
  node [
    id 1156
    label "prototypownia"
  ]
  node [
    id 1157
    label "trawers"
  ]
  node [
    id 1158
    label "deflektor"
  ]
  node [
    id 1159
    label "mechanizm"
  ]
  node [
    id 1160
    label "wa&#322;ek"
  ]
  node [
    id 1161
    label "b&#281;ben"
  ]
  node [
    id 1162
    label "rz&#281;zi&#263;"
  ]
  node [
    id 1163
    label "przyk&#322;adka"
  ]
  node [
    id 1164
    label "t&#322;ok"
  ]
  node [
    id 1165
    label "dehumanizacja"
  ]
  node [
    id 1166
    label "rami&#281;"
  ]
  node [
    id 1167
    label "rz&#281;&#380;enie"
  ]
  node [
    id 1168
    label "ko&#322;o"
  ]
  node [
    id 1169
    label "modalno&#347;&#263;"
  ]
  node [
    id 1170
    label "z&#261;b"
  ]
  node [
    id 1171
    label "kategoria_gramatyczna"
  ]
  node [
    id 1172
    label "skala"
  ]
  node [
    id 1173
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1174
    label "koniugacja"
  ]
  node [
    id 1175
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1176
    label "szczeka&#263;"
  ]
  node [
    id 1177
    label "rozmawia&#263;"
  ]
  node [
    id 1178
    label "t&#281;py"
  ]
  node [
    id 1179
    label "uparty"
  ]
  node [
    id 1180
    label "oddany"
  ]
  node [
    id 1181
    label "t&#281;po"
  ]
  node [
    id 1182
    label "jednostajny"
  ]
  node [
    id 1183
    label "ograniczony"
  ]
  node [
    id 1184
    label "st&#281;pianie"
  ]
  node [
    id 1185
    label "st&#281;pienie"
  ]
  node [
    id 1186
    label "uporczywy"
  ]
  node [
    id 1187
    label "t&#281;pienie"
  ]
  node [
    id 1188
    label "oboj&#281;tny"
  ]
  node [
    id 1189
    label "g&#322;upi"
  ]
  node [
    id 1190
    label "apatyczny"
  ]
  node [
    id 1191
    label "t&#281;pienie_si&#281;"
  ]
  node [
    id 1192
    label "przyt&#281;pienie"
  ]
  node [
    id 1193
    label "nieostry"
  ]
  node [
    id 1194
    label "st&#281;pienie_si&#281;"
  ]
  node [
    id 1195
    label "uporny"
  ]
  node [
    id 1196
    label "wytrwa&#322;y"
  ]
  node [
    id 1197
    label "uparcie"
  ]
  node [
    id 1198
    label "trudny"
  ]
  node [
    id 1199
    label "konsekwentny"
  ]
  node [
    id 1200
    label "wierny"
  ]
  node [
    id 1201
    label "ofiarny"
  ]
  node [
    id 1202
    label "picket"
  ]
  node [
    id 1203
    label "chmura"
  ]
  node [
    id 1204
    label "wska&#378;nik"
  ]
  node [
    id 1205
    label "pas"
  ]
  node [
    id 1206
    label "figura_heraldyczna"
  ]
  node [
    id 1207
    label "oszustwo"
  ]
  node [
    id 1208
    label "plume"
  ]
  node [
    id 1209
    label "heraldyka"
  ]
  node [
    id 1210
    label "tarcza_herbowa"
  ]
  node [
    id 1211
    label "upright"
  ]
  node [
    id 1212
    label "osoba_fizyczna"
  ]
  node [
    id 1213
    label "dodatek"
  ]
  node [
    id 1214
    label "linia"
  ]
  node [
    id 1215
    label "licytacja"
  ]
  node [
    id 1216
    label "wci&#281;cie"
  ]
  node [
    id 1217
    label "bielizna"
  ]
  node [
    id 1218
    label "odznaka"
  ]
  node [
    id 1219
    label "nap&#281;d"
  ]
  node [
    id 1220
    label "zboczenie"
  ]
  node [
    id 1221
    label "om&#243;wienie"
  ]
  node [
    id 1222
    label "sponiewieranie"
  ]
  node [
    id 1223
    label "discipline"
  ]
  node [
    id 1224
    label "omawia&#263;"
  ]
  node [
    id 1225
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1226
    label "tre&#347;&#263;"
  ]
  node [
    id 1227
    label "robienie"
  ]
  node [
    id 1228
    label "sponiewiera&#263;"
  ]
  node [
    id 1229
    label "entity"
  ]
  node [
    id 1230
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1231
    label "tematyka"
  ]
  node [
    id 1232
    label "w&#261;tek"
  ]
  node [
    id 1233
    label "zbaczanie"
  ]
  node [
    id 1234
    label "program_nauczania"
  ]
  node [
    id 1235
    label "om&#243;wi&#263;"
  ]
  node [
    id 1236
    label "omawianie"
  ]
  node [
    id 1237
    label "thing"
  ]
  node [
    id 1238
    label "kultura"
  ]
  node [
    id 1239
    label "istota"
  ]
  node [
    id 1240
    label "zbacza&#263;"
  ]
  node [
    id 1241
    label "zboczy&#263;"
  ]
  node [
    id 1242
    label "gauge"
  ]
  node [
    id 1243
    label "liczba"
  ]
  node [
    id 1244
    label "ufno&#347;&#263;_konsumencka"
  ]
  node [
    id 1245
    label "oznaka"
  ]
  node [
    id 1246
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 1247
    label "marker"
  ]
  node [
    id 1248
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1249
    label "cloud"
  ]
  node [
    id 1250
    label "kszta&#322;t"
  ]
  node [
    id 1251
    label "ob&#322;ok_Oorta"
  ]
  node [
    id 1252
    label "oberwanie_si&#281;"
  ]
  node [
    id 1253
    label "oberwa&#263;_si&#281;"
  ]
  node [
    id 1254
    label "burza"
  ]
  node [
    id 1255
    label "oksza"
  ]
  node [
    id 1256
    label "historia"
  ]
  node [
    id 1257
    label "barwa_heraldyczna"
  ]
  node [
    id 1258
    label "herb"
  ]
  node [
    id 1259
    label "or&#281;&#380;"
  ]
  node [
    id 1260
    label "ba&#322;amutnia"
  ]
  node [
    id 1261
    label "trickery"
  ]
  node [
    id 1262
    label "&#347;ciema"
  ]
  node [
    id 1263
    label "czyn"
  ]
  node [
    id 1264
    label "Tora"
  ]
  node [
    id 1265
    label "zw&#243;j"
  ]
  node [
    id 1266
    label "wrench"
  ]
  node [
    id 1267
    label "m&#243;zg"
  ]
  node [
    id 1268
    label "kink"
  ]
  node [
    id 1269
    label "plik"
  ]
  node [
    id 1270
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1271
    label "manuskrypt"
  ]
  node [
    id 1272
    label "rolka"
  ]
  node [
    id 1273
    label "Parasza"
  ]
  node [
    id 1274
    label "Stary_Testament"
  ]
  node [
    id 1275
    label "wagon"
  ]
  node [
    id 1276
    label "lokomotywa"
  ]
  node [
    id 1277
    label "trakcja"
  ]
  node [
    id 1278
    label "run"
  ]
  node [
    id 1279
    label "blokada"
  ]
  node [
    id 1280
    label "kolejno&#347;&#263;"
  ]
  node [
    id 1281
    label "tor"
  ]
  node [
    id 1282
    label "pojazd_kolejowy"
  ]
  node [
    id 1283
    label "tender"
  ]
  node [
    id 1284
    label "cug"
  ]
  node [
    id 1285
    label "pocz&#261;tek"
  ]
  node [
    id 1286
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1287
    label "poci&#261;g"
  ]
  node [
    id 1288
    label "cedu&#322;a"
  ]
  node [
    id 1289
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 1290
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 1291
    label "nast&#281;pstwo"
  ]
  node [
    id 1292
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1293
    label "armia"
  ]
  node [
    id 1294
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1295
    label "poprowadzi&#263;"
  ]
  node [
    id 1296
    label "cord"
  ]
  node [
    id 1297
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1298
    label "trasa"
  ]
  node [
    id 1299
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1300
    label "tract"
  ]
  node [
    id 1301
    label "materia&#322;_zecerski"
  ]
  node [
    id 1302
    label "przeorientowywanie"
  ]
  node [
    id 1303
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1304
    label "curve"
  ]
  node [
    id 1305
    label "figura_geometryczna"
  ]
  node [
    id 1306
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 1307
    label "jard"
  ]
  node [
    id 1308
    label "phreaker"
  ]
  node [
    id 1309
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 1310
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1311
    label "prowadzi&#263;"
  ]
  node [
    id 1312
    label "przeorientowywa&#263;"
  ]
  node [
    id 1313
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1314
    label "access"
  ]
  node [
    id 1315
    label "przeorientowanie"
  ]
  node [
    id 1316
    label "przeorientowa&#263;"
  ]
  node [
    id 1317
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 1318
    label "billing"
  ]
  node [
    id 1319
    label "granica"
  ]
  node [
    id 1320
    label "szpaler"
  ]
  node [
    id 1321
    label "sztrych"
  ]
  node [
    id 1322
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1323
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1324
    label "drzewo_genealogiczne"
  ]
  node [
    id 1325
    label "transporter"
  ]
  node [
    id 1326
    label "line"
  ]
  node [
    id 1327
    label "przew&#243;d"
  ]
  node [
    id 1328
    label "granice"
  ]
  node [
    id 1329
    label "kontakt"
  ]
  node [
    id 1330
    label "rz&#261;d"
  ]
  node [
    id 1331
    label "przewo&#378;nik"
  ]
  node [
    id 1332
    label "przystanek"
  ]
  node [
    id 1333
    label "linijka"
  ]
  node [
    id 1334
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1335
    label "coalescence"
  ]
  node [
    id 1336
    label "Ural"
  ]
  node [
    id 1337
    label "bearing"
  ]
  node [
    id 1338
    label "prowadzenie"
  ]
  node [
    id 1339
    label "tekst"
  ]
  node [
    id 1340
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1341
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1342
    label "koniec"
  ]
  node [
    id 1343
    label "poprzedzanie"
  ]
  node [
    id 1344
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1345
    label "laba"
  ]
  node [
    id 1346
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1347
    label "chronometria"
  ]
  node [
    id 1348
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1349
    label "rachuba_czasu"
  ]
  node [
    id 1350
    label "przep&#322;ywanie"
  ]
  node [
    id 1351
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1352
    label "czasokres"
  ]
  node [
    id 1353
    label "odczyt"
  ]
  node [
    id 1354
    label "chwila"
  ]
  node [
    id 1355
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1356
    label "dzieje"
  ]
  node [
    id 1357
    label "poprzedzenie"
  ]
  node [
    id 1358
    label "trawienie"
  ]
  node [
    id 1359
    label "pochodzi&#263;"
  ]
  node [
    id 1360
    label "period"
  ]
  node [
    id 1361
    label "okres_czasu"
  ]
  node [
    id 1362
    label "poprzedza&#263;"
  ]
  node [
    id 1363
    label "schy&#322;ek"
  ]
  node [
    id 1364
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1365
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1366
    label "zegar"
  ]
  node [
    id 1367
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1368
    label "czwarty_wymiar"
  ]
  node [
    id 1369
    label "pochodzenie"
  ]
  node [
    id 1370
    label "Zeitgeist"
  ]
  node [
    id 1371
    label "trawi&#263;"
  ]
  node [
    id 1372
    label "pogoda"
  ]
  node [
    id 1373
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1374
    label "poprzedzi&#263;"
  ]
  node [
    id 1375
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1376
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1377
    label "time_period"
  ]
  node [
    id 1378
    label "ekskursja"
  ]
  node [
    id 1379
    label "bezsilnikowy"
  ]
  node [
    id 1380
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 1381
    label "podbieg"
  ]
  node [
    id 1382
    label "turystyka"
  ]
  node [
    id 1383
    label "nawierzchnia"
  ]
  node [
    id 1384
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 1385
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 1386
    label "rajza"
  ]
  node [
    id 1387
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1388
    label "korona_drogi"
  ]
  node [
    id 1389
    label "passage"
  ]
  node [
    id 1390
    label "wylot"
  ]
  node [
    id 1391
    label "ekwipunek"
  ]
  node [
    id 1392
    label "zbior&#243;wka"
  ]
  node [
    id 1393
    label "marszrutyzacja"
  ]
  node [
    id 1394
    label "wyb&#243;j"
  ]
  node [
    id 1395
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 1396
    label "drogowskaz"
  ]
  node [
    id 1397
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1398
    label "pobocze"
  ]
  node [
    id 1399
    label "journey"
  ]
  node [
    id 1400
    label "popyt"
  ]
  node [
    id 1401
    label "podbijarka_torowa"
  ]
  node [
    id 1402
    label "rozstaw_tr&#243;jstopowy"
  ]
  node [
    id 1403
    label "torowisko"
  ]
  node [
    id 1404
    label "szyna"
  ]
  node [
    id 1405
    label "rozstaw_przyl&#261;dkowy"
  ]
  node [
    id 1406
    label "linia_kolejowa"
  ]
  node [
    id 1407
    label "lane"
  ]
  node [
    id 1408
    label "nawierzchnia_kolejowa"
  ]
  node [
    id 1409
    label "podk&#322;ad"
  ]
  node [
    id 1410
    label "aktynowiec"
  ]
  node [
    id 1411
    label "balastowanie"
  ]
  node [
    id 1412
    label "rozstaw_bo&#347;niacki"
  ]
  node [
    id 1413
    label "pierworodztwo"
  ]
  node [
    id 1414
    label "faza"
  ]
  node [
    id 1415
    label "upgrade"
  ]
  node [
    id 1416
    label "statek"
  ]
  node [
    id 1417
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 1418
    label "okr&#281;t"
  ]
  node [
    id 1419
    label "karton"
  ]
  node [
    id 1420
    label "czo&#322;ownica"
  ]
  node [
    id 1421
    label "harmonijka"
  ]
  node [
    id 1422
    label "tramwaj"
  ]
  node [
    id 1423
    label "klasa"
  ]
  node [
    id 1424
    label "ciuchcia"
  ]
  node [
    id 1425
    label "pojazd_trakcyjny"
  ]
  node [
    id 1426
    label "raport"
  ]
  node [
    id 1427
    label "transport"
  ]
  node [
    id 1428
    label "kurs"
  ]
  node [
    id 1429
    label "spis"
  ]
  node [
    id 1430
    label "bilet"
  ]
  node [
    id 1431
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1432
    label "bloking"
  ]
  node [
    id 1433
    label "znieczulenie"
  ]
  node [
    id 1434
    label "block"
  ]
  node [
    id 1435
    label "utrudnienie"
  ]
  node [
    id 1436
    label "arrest"
  ]
  node [
    id 1437
    label "anestezja"
  ]
  node [
    id 1438
    label "ochrona"
  ]
  node [
    id 1439
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 1440
    label "izolacja"
  ]
  node [
    id 1441
    label "zwrotnica"
  ]
  node [
    id 1442
    label "siatk&#243;wka"
  ]
  node [
    id 1443
    label "sankcja"
  ]
  node [
    id 1444
    label "semafor"
  ]
  node [
    id 1445
    label "obrona"
  ]
  node [
    id 1446
    label "deadlock"
  ]
  node [
    id 1447
    label "lock"
  ]
  node [
    id 1448
    label "niemo&#380;no&#347;&#263;"
  ]
  node [
    id 1449
    label "kognicja"
  ]
  node [
    id 1450
    label "przebieg"
  ]
  node [
    id 1451
    label "rozprawa"
  ]
  node [
    id 1452
    label "legislacyjnie"
  ]
  node [
    id 1453
    label "przes&#322;anka"
  ]
  node [
    id 1454
    label "odczuwa&#263;"
  ]
  node [
    id 1455
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 1456
    label "wydziedziczy&#263;"
  ]
  node [
    id 1457
    label "skrupienie_si&#281;"
  ]
  node [
    id 1458
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 1459
    label "wydziedziczenie"
  ]
  node [
    id 1460
    label "odczucie"
  ]
  node [
    id 1461
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 1462
    label "koszula_Dejaniry"
  ]
  node [
    id 1463
    label "odczuwanie"
  ]
  node [
    id 1464
    label "event"
  ]
  node [
    id 1465
    label "rezultat"
  ]
  node [
    id 1466
    label "skrupianie_si&#281;"
  ]
  node [
    id 1467
    label "odczu&#263;"
  ]
  node [
    id 1468
    label "para"
  ]
  node [
    id 1469
    label "pr&#261;d"
  ]
  node [
    id 1470
    label "draft"
  ]
  node [
    id 1471
    label "stan"
  ]
  node [
    id 1472
    label "&#347;l&#261;ski"
  ]
  node [
    id 1473
    label "ci&#261;g"
  ]
  node [
    id 1474
    label "zaprz&#281;g"
  ]
  node [
    id 1475
    label "zniewalaj&#261;cy"
  ]
  node [
    id 1476
    label "magnetycznie"
  ]
  node [
    id 1477
    label "zniewalaj&#261;co"
  ]
  node [
    id 1478
    label "niewol&#261;cy"
  ]
  node [
    id 1479
    label "wystarczy&#263;"
  ]
  node [
    id 1480
    label "trwa&#263;"
  ]
  node [
    id 1481
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 1482
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1483
    label "przebywa&#263;"
  ]
  node [
    id 1484
    label "pozostawa&#263;"
  ]
  node [
    id 1485
    label "kosztowa&#263;"
  ]
  node [
    id 1486
    label "undertaking"
  ]
  node [
    id 1487
    label "digest"
  ]
  node [
    id 1488
    label "wystawa&#263;"
  ]
  node [
    id 1489
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1490
    label "wystarcza&#263;"
  ]
  node [
    id 1491
    label "mieszka&#263;"
  ]
  node [
    id 1492
    label "stand"
  ]
  node [
    id 1493
    label "sprawowa&#263;"
  ]
  node [
    id 1494
    label "czeka&#263;"
  ]
  node [
    id 1495
    label "dobywa&#263;_si&#281;"
  ]
  node [
    id 1496
    label "zostawa&#263;"
  ]
  node [
    id 1497
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1498
    label "adhere"
  ]
  node [
    id 1499
    label "bind"
  ]
  node [
    id 1500
    label "panowa&#263;"
  ]
  node [
    id 1501
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 1502
    label "zjednywa&#263;"
  ]
  node [
    id 1503
    label "tkwi&#263;"
  ]
  node [
    id 1504
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1505
    label "pause"
  ]
  node [
    id 1506
    label "przestawa&#263;"
  ]
  node [
    id 1507
    label "hesitate"
  ]
  node [
    id 1508
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1509
    label "equal"
  ]
  node [
    id 1510
    label "chodzi&#263;"
  ]
  node [
    id 1511
    label "si&#281;ga&#263;"
  ]
  node [
    id 1512
    label "obecno&#347;&#263;"
  ]
  node [
    id 1513
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1514
    label "uczestniczy&#263;"
  ]
  node [
    id 1515
    label "savor"
  ]
  node [
    id 1516
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 1517
    label "cena"
  ]
  node [
    id 1518
    label "doznawa&#263;"
  ]
  node [
    id 1519
    label "essay"
  ]
  node [
    id 1520
    label "zaspokaja&#263;"
  ]
  node [
    id 1521
    label "suffice"
  ]
  node [
    id 1522
    label "dostawa&#263;"
  ]
  node [
    id 1523
    label "stawa&#263;"
  ]
  node [
    id 1524
    label "spowodowa&#263;"
  ]
  node [
    id 1525
    label "stan&#261;&#263;"
  ]
  node [
    id 1526
    label "zaspokoi&#263;"
  ]
  node [
    id 1527
    label "dosta&#263;"
  ]
  node [
    id 1528
    label "pauzowa&#263;"
  ]
  node [
    id 1529
    label "oczekiwa&#263;"
  ]
  node [
    id 1530
    label "decydowa&#263;"
  ]
  node [
    id 1531
    label "sp&#281;dza&#263;"
  ]
  node [
    id 1532
    label "look"
  ]
  node [
    id 1533
    label "hold"
  ]
  node [
    id 1534
    label "anticipate"
  ]
  node [
    id 1535
    label "blend"
  ]
  node [
    id 1536
    label "stop"
  ]
  node [
    id 1537
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 1538
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 1539
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1540
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 1541
    label "support"
  ]
  node [
    id 1542
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 1543
    label "prosecute"
  ]
  node [
    id 1544
    label "zajmowa&#263;"
  ]
  node [
    id 1545
    label "room"
  ]
  node [
    id 1546
    label "fall"
  ]
  node [
    id 1547
    label "set"
  ]
  node [
    id 1548
    label "wykona&#263;"
  ]
  node [
    id 1549
    label "pos&#322;a&#263;"
  ]
  node [
    id 1550
    label "carry"
  ]
  node [
    id 1551
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 1552
    label "take"
  ]
  node [
    id 1553
    label "wprowadzi&#263;"
  ]
  node [
    id 1554
    label "wzbudzi&#263;"
  ]
  node [
    id 1555
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1556
    label "act"
  ]
  node [
    id 1557
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1558
    label "krzywa"
  ]
  node [
    id 1559
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1560
    label "control"
  ]
  node [
    id 1561
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1562
    label "leave"
  ]
  node [
    id 1563
    label "nakre&#347;li&#263;"
  ]
  node [
    id 1564
    label "moderate"
  ]
  node [
    id 1565
    label "guidebook"
  ]
  node [
    id 1566
    label "wytworzy&#263;"
  ]
  node [
    id 1567
    label "picture"
  ]
  node [
    id 1568
    label "manufacture"
  ]
  node [
    id 1569
    label "zrobi&#263;"
  ]
  node [
    id 1570
    label "nakaza&#263;"
  ]
  node [
    id 1571
    label "pod&#322;o&#380;y&#263;"
  ]
  node [
    id 1572
    label "przekaza&#263;"
  ]
  node [
    id 1573
    label "dispatch"
  ]
  node [
    id 1574
    label "report"
  ]
  node [
    id 1575
    label "ship"
  ]
  node [
    id 1576
    label "wys&#322;a&#263;"
  ]
  node [
    id 1577
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 1578
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 1579
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 1580
    label "post"
  ]
  node [
    id 1581
    label "convey"
  ]
  node [
    id 1582
    label "arouse"
  ]
  node [
    id 1583
    label "gem"
  ]
  node [
    id 1584
    label "kompozycja"
  ]
  node [
    id 1585
    label "runda"
  ]
  node [
    id 1586
    label "muzyka"
  ]
  node [
    id 1587
    label "zestaw"
  ]
  node [
    id 1588
    label "rynek"
  ]
  node [
    id 1589
    label "testify"
  ]
  node [
    id 1590
    label "insert"
  ]
  node [
    id 1591
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1592
    label "wpisa&#263;"
  ]
  node [
    id 1593
    label "zapozna&#263;"
  ]
  node [
    id 1594
    label "wej&#347;&#263;"
  ]
  node [
    id 1595
    label "zej&#347;&#263;"
  ]
  node [
    id 1596
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 1597
    label "zacz&#261;&#263;"
  ]
  node [
    id 1598
    label "indicate"
  ]
  node [
    id 1599
    label "gwiazda"
  ]
  node [
    id 1600
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 1601
    label "Arktur"
  ]
  node [
    id 1602
    label "Gwiazda_Polarna"
  ]
  node [
    id 1603
    label "agregatka"
  ]
  node [
    id 1604
    label "gromada"
  ]
  node [
    id 1605
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 1606
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1607
    label "Nibiru"
  ]
  node [
    id 1608
    label "ornament"
  ]
  node [
    id 1609
    label "delta_Scuti"
  ]
  node [
    id 1610
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1611
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1612
    label "s&#322;awa"
  ]
  node [
    id 1613
    label "promie&#324;"
  ]
  node [
    id 1614
    label "star"
  ]
  node [
    id 1615
    label "gwiazdosz"
  ]
  node [
    id 1616
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 1617
    label "asocjacja_gwiazd"
  ]
  node [
    id 1618
    label "supergrupa"
  ]
  node [
    id 1619
    label "participate"
  ]
  node [
    id 1620
    label "compass"
  ]
  node [
    id 1621
    label "korzysta&#263;"
  ]
  node [
    id 1622
    label "appreciation"
  ]
  node [
    id 1623
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1624
    label "dociera&#263;"
  ]
  node [
    id 1625
    label "get"
  ]
  node [
    id 1626
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1627
    label "mierzy&#263;"
  ]
  node [
    id 1628
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1629
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1630
    label "exsert"
  ]
  node [
    id 1631
    label "being"
  ]
  node [
    id 1632
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1633
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1634
    label "przebiega&#263;"
  ]
  node [
    id 1635
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1636
    label "proceed"
  ]
  node [
    id 1637
    label "bywa&#263;"
  ]
  node [
    id 1638
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1639
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1640
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1641
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1642
    label "krok"
  ]
  node [
    id 1643
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1644
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1645
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1646
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1647
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1648
    label "continue"
  ]
  node [
    id 1649
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1650
    label "Ohio"
  ]
  node [
    id 1651
    label "Nowy_York"
  ]
  node [
    id 1652
    label "samopoczucie"
  ]
  node [
    id 1653
    label "Illinois"
  ]
  node [
    id 1654
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1655
    label "state"
  ]
  node [
    id 1656
    label "Jukatan"
  ]
  node [
    id 1657
    label "Kalifornia"
  ]
  node [
    id 1658
    label "Wirginia"
  ]
  node [
    id 1659
    label "wektor"
  ]
  node [
    id 1660
    label "Goa"
  ]
  node [
    id 1661
    label "Teksas"
  ]
  node [
    id 1662
    label "Waszyngton"
  ]
  node [
    id 1663
    label "Massachusetts"
  ]
  node [
    id 1664
    label "Alaska"
  ]
  node [
    id 1665
    label "Arakan"
  ]
  node [
    id 1666
    label "Hawaje"
  ]
  node [
    id 1667
    label "Maryland"
  ]
  node [
    id 1668
    label "Michigan"
  ]
  node [
    id 1669
    label "Arizona"
  ]
  node [
    id 1670
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1671
    label "Georgia"
  ]
  node [
    id 1672
    label "poziom"
  ]
  node [
    id 1673
    label "Pensylwania"
  ]
  node [
    id 1674
    label "Luizjana"
  ]
  node [
    id 1675
    label "Nowy_Meksyk"
  ]
  node [
    id 1676
    label "Alabama"
  ]
  node [
    id 1677
    label "ilo&#347;&#263;"
  ]
  node [
    id 1678
    label "Kansas"
  ]
  node [
    id 1679
    label "Oregon"
  ]
  node [
    id 1680
    label "Oklahoma"
  ]
  node [
    id 1681
    label "Floryda"
  ]
  node [
    id 1682
    label "jednostka_administracyjna"
  ]
  node [
    id 1683
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1684
    label "parametr"
  ]
  node [
    id 1685
    label "s&#261;d"
  ]
  node [
    id 1686
    label "operand"
  ]
  node [
    id 1687
    label "dow&#243;d"
  ]
  node [
    id 1688
    label "zmienna"
  ]
  node [
    id 1689
    label "argumentacja"
  ]
  node [
    id 1690
    label "wymiar"
  ]
  node [
    id 1691
    label "wielko&#347;&#263;"
  ]
  node [
    id 1692
    label "object"
  ]
  node [
    id 1693
    label "temat"
  ]
  node [
    id 1694
    label "wpadni&#281;cie"
  ]
  node [
    id 1695
    label "mienie"
  ]
  node [
    id 1696
    label "przyroda"
  ]
  node [
    id 1697
    label "wpa&#347;&#263;"
  ]
  node [
    id 1698
    label "wpadanie"
  ]
  node [
    id 1699
    label "wpada&#263;"
  ]
  node [
    id 1700
    label "podejrzany"
  ]
  node [
    id 1701
    label "s&#261;downictwo"
  ]
  node [
    id 1702
    label "biuro"
  ]
  node [
    id 1703
    label "court"
  ]
  node [
    id 1704
    label "forum"
  ]
  node [
    id 1705
    label "bronienie"
  ]
  node [
    id 1706
    label "urz&#261;d"
  ]
  node [
    id 1707
    label "oskar&#380;yciel"
  ]
  node [
    id 1708
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1709
    label "skazany"
  ]
  node [
    id 1710
    label "post&#281;powanie"
  ]
  node [
    id 1711
    label "broni&#263;"
  ]
  node [
    id 1712
    label "pods&#261;dny"
  ]
  node [
    id 1713
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1714
    label "instytucja"
  ]
  node [
    id 1715
    label "antylogizm"
  ]
  node [
    id 1716
    label "konektyw"
  ]
  node [
    id 1717
    label "&#347;wiadek"
  ]
  node [
    id 1718
    label "procesowicz"
  ]
  node [
    id 1719
    label "strona"
  ]
  node [
    id 1720
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1721
    label "variable"
  ]
  node [
    id 1722
    label "komunikacja"
  ]
  node [
    id 1723
    label "uzasadnienie"
  ]
  node [
    id 1724
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1725
    label "transformata"
  ]
  node [
    id 1726
    label "&#347;rodek"
  ]
  node [
    id 1727
    label "rewizja"
  ]
  node [
    id 1728
    label "certificate"
  ]
  node [
    id 1729
    label "forsing"
  ]
  node [
    id 1730
    label "dokument"
  ]
  node [
    id 1731
    label "taniec_towarzyski"
  ]
  node [
    id 1732
    label "utw&#243;r"
  ]
  node [
    id 1733
    label "melodia"
  ]
  node [
    id 1734
    label "taniec"
  ]
  node [
    id 1735
    label "obrazowanie"
  ]
  node [
    id 1736
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1737
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 1738
    label "part"
  ]
  node [
    id 1739
    label "element_anatomiczny"
  ]
  node [
    id 1740
    label "komunikat"
  ]
  node [
    id 1741
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 1742
    label "karnet"
  ]
  node [
    id 1743
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 1744
    label "parkiet"
  ]
  node [
    id 1745
    label "choreologia"
  ]
  node [
    id 1746
    label "krok_taneczny"
  ]
  node [
    id 1747
    label "zanucenie"
  ]
  node [
    id 1748
    label "nuta"
  ]
  node [
    id 1749
    label "zakosztowa&#263;"
  ]
  node [
    id 1750
    label "zanuci&#263;"
  ]
  node [
    id 1751
    label "emocja"
  ]
  node [
    id 1752
    label "oskoma"
  ]
  node [
    id 1753
    label "melika"
  ]
  node [
    id 1754
    label "nucenie"
  ]
  node [
    id 1755
    label "nuci&#263;"
  ]
  node [
    id 1756
    label "brzmienie"
  ]
  node [
    id 1757
    label "taste"
  ]
  node [
    id 1758
    label "inclination"
  ]
  node [
    id 1759
    label "przewiewny"
  ]
  node [
    id 1760
    label "nieg&#322;&#281;boko"
  ]
  node [
    id 1761
    label "subtelny"
  ]
  node [
    id 1762
    label "lekko"
  ]
  node [
    id 1763
    label "polotny"
  ]
  node [
    id 1764
    label "przyswajalny"
  ]
  node [
    id 1765
    label "beztrosko"
  ]
  node [
    id 1766
    label "&#322;atwy"
  ]
  node [
    id 1767
    label "piaszczysty"
  ]
  node [
    id 1768
    label "suchy"
  ]
  node [
    id 1769
    label "letki"
  ]
  node [
    id 1770
    label "p&#322;ynny"
  ]
  node [
    id 1771
    label "dietetyczny"
  ]
  node [
    id 1772
    label "lekkozbrojny"
  ]
  node [
    id 1773
    label "delikatnie"
  ]
  node [
    id 1774
    label "&#322;acny"
  ]
  node [
    id 1775
    label "snadny"
  ]
  node [
    id 1776
    label "nierozwa&#380;ny"
  ]
  node [
    id 1777
    label "g&#322;adko"
  ]
  node [
    id 1778
    label "zgrabny"
  ]
  node [
    id 1779
    label "ubogi"
  ]
  node [
    id 1780
    label "zwinnie"
  ]
  node [
    id 1781
    label "beztroskliwy"
  ]
  node [
    id 1782
    label "zr&#281;czny"
  ]
  node [
    id 1783
    label "prosty"
  ]
  node [
    id 1784
    label "cienki"
  ]
  node [
    id 1785
    label "nieznacznie"
  ]
  node [
    id 1786
    label "&#322;agodny"
  ]
  node [
    id 1787
    label "wydelikacanie"
  ]
  node [
    id 1788
    label "delikatnienie"
  ]
  node [
    id 1789
    label "nieszkodliwy"
  ]
  node [
    id 1790
    label "k&#322;opotliwy"
  ]
  node [
    id 1791
    label "zdelikatnienie"
  ]
  node [
    id 1792
    label "dra&#380;liwy"
  ]
  node [
    id 1793
    label "ostro&#380;ny"
  ]
  node [
    id 1794
    label "wra&#380;liwy"
  ]
  node [
    id 1795
    label "&#322;agodnie"
  ]
  node [
    id 1796
    label "wydelikacenie"
  ]
  node [
    id 1797
    label "taktowny"
  ]
  node [
    id 1798
    label "rozlewny"
  ]
  node [
    id 1799
    label "cytozol"
  ]
  node [
    id 1800
    label "up&#322;ynnianie"
  ]
  node [
    id 1801
    label "roztapianie_si&#281;"
  ]
  node [
    id 1802
    label "roztopienie_si&#281;"
  ]
  node [
    id 1803
    label "p&#322;ynnie"
  ]
  node [
    id 1804
    label "bieg&#322;y"
  ]
  node [
    id 1805
    label "nieokre&#347;lony"
  ]
  node [
    id 1806
    label "up&#322;ynnienie"
  ]
  node [
    id 1807
    label "skuteczny"
  ]
  node [
    id 1808
    label "zwinny"
  ]
  node [
    id 1809
    label "zgrabnie"
  ]
  node [
    id 1810
    label "sprytny"
  ]
  node [
    id 1811
    label "sprawny"
  ]
  node [
    id 1812
    label "gracki"
  ]
  node [
    id 1813
    label "kszta&#322;tny"
  ]
  node [
    id 1814
    label "harmonijny"
  ]
  node [
    id 1815
    label "filigranowo"
  ]
  node [
    id 1816
    label "drobny"
  ]
  node [
    id 1817
    label "elegancki"
  ]
  node [
    id 1818
    label "subtelnie"
  ]
  node [
    id 1819
    label "wnikliwy"
  ]
  node [
    id 1820
    label "nie&#347;mieszny"
  ]
  node [
    id 1821
    label "niesympatyczny"
  ]
  node [
    id 1822
    label "twardy"
  ]
  node [
    id 1823
    label "sucho"
  ]
  node [
    id 1824
    label "wysuszenie_si&#281;"
  ]
  node [
    id 1825
    label "czczy"
  ]
  node [
    id 1826
    label "sam"
  ]
  node [
    id 1827
    label "do_sucha"
  ]
  node [
    id 1828
    label "suszenie"
  ]
  node [
    id 1829
    label "wysuszanie"
  ]
  node [
    id 1830
    label "matowy"
  ]
  node [
    id 1831
    label "wysuszenie"
  ]
  node [
    id 1832
    label "cichy"
  ]
  node [
    id 1833
    label "chudy"
  ]
  node [
    id 1834
    label "ch&#322;odno"
  ]
  node [
    id 1835
    label "bankrutowanie"
  ]
  node [
    id 1836
    label "ubo&#380;enie"
  ]
  node [
    id 1837
    label "go&#322;odupiec"
  ]
  node [
    id 1838
    label "biedny"
  ]
  node [
    id 1839
    label "zubo&#380;enie"
  ]
  node [
    id 1840
    label "raw_material"
  ]
  node [
    id 1841
    label "zubo&#380;anie"
  ]
  node [
    id 1842
    label "ho&#322;ysz"
  ]
  node [
    id 1843
    label "zbiednienie"
  ]
  node [
    id 1844
    label "proletariusz"
  ]
  node [
    id 1845
    label "sytuowany"
  ]
  node [
    id 1846
    label "biedota"
  ]
  node [
    id 1847
    label "ubogo"
  ]
  node [
    id 1848
    label "biednie"
  ]
  node [
    id 1849
    label "piaszczysto"
  ]
  node [
    id 1850
    label "kruchy"
  ]
  node [
    id 1851
    label "dietetycznie"
  ]
  node [
    id 1852
    label "mo&#380;liwy"
  ]
  node [
    id 1853
    label "nietrwa&#322;y"
  ]
  node [
    id 1854
    label "mizerny"
  ]
  node [
    id 1855
    label "marnie"
  ]
  node [
    id 1856
    label "po&#347;ledni"
  ]
  node [
    id 1857
    label "niezdrowy"
  ]
  node [
    id 1858
    label "nieumiej&#281;tny"
  ]
  node [
    id 1859
    label "s&#322;abo"
  ]
  node [
    id 1860
    label "lura"
  ]
  node [
    id 1861
    label "nieudany"
  ]
  node [
    id 1862
    label "s&#322;abowity"
  ]
  node [
    id 1863
    label "zawodny"
  ]
  node [
    id 1864
    label "md&#322;y"
  ]
  node [
    id 1865
    label "niedoskona&#322;y"
  ]
  node [
    id 1866
    label "przemijaj&#261;cy"
  ]
  node [
    id 1867
    label "niemocny"
  ]
  node [
    id 1868
    label "niefajny"
  ]
  node [
    id 1869
    label "kiepsko"
  ]
  node [
    id 1870
    label "wysoki"
  ]
  node [
    id 1871
    label "ulotny"
  ]
  node [
    id 1872
    label "nieuchwytny"
  ]
  node [
    id 1873
    label "w&#261;ski"
  ]
  node [
    id 1874
    label "w&#261;t&#322;y"
  ]
  node [
    id 1875
    label "cienko"
  ]
  node [
    id 1876
    label "do_cienka"
  ]
  node [
    id 1877
    label "niem&#261;dry"
  ]
  node [
    id 1878
    label "nierozwa&#380;nie"
  ]
  node [
    id 1879
    label "skromny"
  ]
  node [
    id 1880
    label "po_prostu"
  ]
  node [
    id 1881
    label "naturalny"
  ]
  node [
    id 1882
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 1883
    label "rozprostowanie"
  ]
  node [
    id 1884
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 1885
    label "prosto"
  ]
  node [
    id 1886
    label "prostowanie_si&#281;"
  ]
  node [
    id 1887
    label "niepozorny"
  ]
  node [
    id 1888
    label "prostoduszny"
  ]
  node [
    id 1889
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 1890
    label "naiwny"
  ]
  node [
    id 1891
    label "prostowanie"
  ]
  node [
    id 1892
    label "zwyk&#322;y"
  ]
  node [
    id 1893
    label "zbrojny"
  ]
  node [
    id 1894
    label "wojownik"
  ]
  node [
    id 1895
    label "schronienie"
  ]
  node [
    id 1896
    label "polotnie"
  ]
  node [
    id 1897
    label "mi&#281;kko"
  ]
  node [
    id 1898
    label "mo&#380;liwie"
  ]
  node [
    id 1899
    label "g&#322;adki"
  ]
  node [
    id 1900
    label "snadnie"
  ]
  node [
    id 1901
    label "&#322;atwie"
  ]
  node [
    id 1902
    label "&#322;acno"
  ]
  node [
    id 1903
    label "nieg&#322;&#281;boki"
  ]
  node [
    id 1904
    label "mi&#281;ciuchno"
  ]
  node [
    id 1905
    label "beztroski"
  ]
  node [
    id 1906
    label "p&#322;ytki"
  ]
  node [
    id 1907
    label "shallowly"
  ]
  node [
    id 1908
    label "przewiewnie"
  ]
  node [
    id 1909
    label "przestronny"
  ]
  node [
    id 1910
    label "przepuszczalny"
  ]
  node [
    id 1911
    label "&#380;yzny"
  ]
  node [
    id 1912
    label "nie&#347;cis&#322;y"
  ]
  node [
    id 1913
    label "nieistotnie"
  ]
  node [
    id 1914
    label "ostro&#380;nie"
  ]
  node [
    id 1915
    label "grzecznie"
  ]
  node [
    id 1916
    label "jednobarwnie"
  ]
  node [
    id 1917
    label "bezproblemowo"
  ]
  node [
    id 1918
    label "nieruchomo"
  ]
  node [
    id 1919
    label "elegancko"
  ]
  node [
    id 1920
    label "okr&#261;g&#322;o"
  ]
  node [
    id 1921
    label "r&#243;wno"
  ]
  node [
    id 1922
    label "og&#243;lnikowo"
  ]
  node [
    id 1923
    label "lotny"
  ]
  node [
    id 1924
    label "b&#322;yskotliwy"
  ]
  node [
    id 1925
    label "r&#261;czy"
  ]
  node [
    id 1926
    label "sport"
  ]
  node [
    id 1927
    label "zgrupowanie"
  ]
  node [
    id 1928
    label "kultura_fizyczna"
  ]
  node [
    id 1929
    label "usportowienie"
  ]
  node [
    id 1930
    label "atakowa&#263;"
  ]
  node [
    id 1931
    label "zaatakowanie"
  ]
  node [
    id 1932
    label "atakowanie"
  ]
  node [
    id 1933
    label "zaatakowa&#263;"
  ]
  node [
    id 1934
    label "usportowi&#263;"
  ]
  node [
    id 1935
    label "sokolstwo"
  ]
  node [
    id 1936
    label "kongres_wiede&#324;ski"
  ]
  node [
    id 1937
    label "convention"
  ]
  node [
    id 1938
    label "konferencja"
  ]
  node [
    id 1939
    label "Ja&#322;ta"
  ]
  node [
    id 1940
    label "spotkanie"
  ]
  node [
    id 1941
    label "konferencyjka"
  ]
  node [
    id 1942
    label "conference"
  ]
  node [
    id 1943
    label "grusza_pospolita"
  ]
  node [
    id 1944
    label "Poczdam"
  ]
  node [
    id 1945
    label "p&#322;&#243;d"
  ]
  node [
    id 1946
    label "thinking"
  ]
  node [
    id 1947
    label "umys&#322;"
  ]
  node [
    id 1948
    label "political_orientation"
  ]
  node [
    id 1949
    label "pomys&#322;"
  ]
  node [
    id 1950
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1951
    label "idea"
  ]
  node [
    id 1952
    label "fantomatyka"
  ]
  node [
    id 1953
    label "pami&#281;&#263;"
  ]
  node [
    id 1954
    label "intelekt"
  ]
  node [
    id 1955
    label "pomieszanie_si&#281;"
  ]
  node [
    id 1956
    label "wn&#281;trze"
  ]
  node [
    id 1957
    label "wyobra&#378;nia"
  ]
  node [
    id 1958
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1959
    label "superego"
  ]
  node [
    id 1960
    label "psychika"
  ]
  node [
    id 1961
    label "znaczenie"
  ]
  node [
    id 1962
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1963
    label "moczownik"
  ]
  node [
    id 1964
    label "embryo"
  ]
  node [
    id 1965
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1966
    label "zarodek"
  ]
  node [
    id 1967
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1968
    label "latawiec"
  ]
  node [
    id 1969
    label "j&#261;dro"
  ]
  node [
    id 1970
    label "systemik"
  ]
  node [
    id 1971
    label "oprogramowanie"
  ]
  node [
    id 1972
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1973
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1974
    label "model"
  ]
  node [
    id 1975
    label "porz&#261;dek"
  ]
  node [
    id 1976
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1977
    label "przyn&#281;ta"
  ]
  node [
    id 1978
    label "net"
  ]
  node [
    id 1979
    label "w&#281;dkarstwo"
  ]
  node [
    id 1980
    label "eratem"
  ]
  node [
    id 1981
    label "oddzia&#322;"
  ]
  node [
    id 1982
    label "doktryna"
  ]
  node [
    id 1983
    label "pulpit"
  ]
  node [
    id 1984
    label "jednostka_geologiczna"
  ]
  node [
    id 1985
    label "metoda"
  ]
  node [
    id 1986
    label "ryba"
  ]
  node [
    id 1987
    label "Leopard"
  ]
  node [
    id 1988
    label "Android"
  ]
  node [
    id 1989
    label "method"
  ]
  node [
    id 1990
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1991
    label "technika"
  ]
  node [
    id 1992
    label "pocz&#261;tki"
  ]
  node [
    id 1993
    label "ukra&#347;&#263;"
  ]
  node [
    id 1994
    label "ukradzenie"
  ]
  node [
    id 1995
    label "do&#347;wiadczenie"
  ]
  node [
    id 1996
    label "teren_szko&#322;y"
  ]
  node [
    id 1997
    label "wiedza"
  ]
  node [
    id 1998
    label "Mickiewicz"
  ]
  node [
    id 1999
    label "kwalifikacje"
  ]
  node [
    id 2000
    label "podr&#281;cznik"
  ]
  node [
    id 2001
    label "absolwent"
  ]
  node [
    id 2002
    label "school"
  ]
  node [
    id 2003
    label "zda&#263;"
  ]
  node [
    id 2004
    label "gabinet"
  ]
  node [
    id 2005
    label "urszulanki"
  ]
  node [
    id 2006
    label "sztuba"
  ]
  node [
    id 2007
    label "&#322;awa_szkolna"
  ]
  node [
    id 2008
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 2009
    label "przepisa&#263;"
  ]
  node [
    id 2010
    label "form"
  ]
  node [
    id 2011
    label "lekcja"
  ]
  node [
    id 2012
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 2013
    label "przepisanie"
  ]
  node [
    id 2014
    label "skolaryzacja"
  ]
  node [
    id 2015
    label "zdanie"
  ]
  node [
    id 2016
    label "stopek"
  ]
  node [
    id 2017
    label "sekretariat"
  ]
  node [
    id 2018
    label "ideologia"
  ]
  node [
    id 2019
    label "lesson"
  ]
  node [
    id 2020
    label "niepokalanki"
  ]
  node [
    id 2021
    label "szkolenie"
  ]
  node [
    id 2022
    label "kara"
  ]
  node [
    id 2023
    label "tablica"
  ]
  node [
    id 2024
    label "Kant"
  ]
  node [
    id 2025
    label "cel"
  ]
  node [
    id 2026
    label "ideacja"
  ]
  node [
    id 2027
    label "niedawno"
  ]
  node [
    id 2028
    label "pozosta&#322;y"
  ]
  node [
    id 2029
    label "ostatnio"
  ]
  node [
    id 2030
    label "sko&#324;czony"
  ]
  node [
    id 2031
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 2032
    label "najgorszy"
  ]
  node [
    id 2033
    label "w&#261;tpliwy"
  ]
  node [
    id 2034
    label "w&#261;tpliwie"
  ]
  node [
    id 2035
    label "pozorny"
  ]
  node [
    id 2036
    label "&#380;ywy"
  ]
  node [
    id 2037
    label "ostateczny"
  ]
  node [
    id 2038
    label "wa&#380;ny"
  ]
  node [
    id 2039
    label "wykszta&#322;cony"
  ]
  node [
    id 2040
    label "dyplomowany"
  ]
  node [
    id 2041
    label "wykwalifikowany"
  ]
  node [
    id 2042
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 2043
    label "sko&#324;czenie"
  ]
  node [
    id 2044
    label "okre&#347;lony"
  ]
  node [
    id 2045
    label "wielki"
  ]
  node [
    id 2046
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 2047
    label "aktualnie"
  ]
  node [
    id 2048
    label "aktualizowanie"
  ]
  node [
    id 2049
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 2050
    label "uaktualnienie"
  ]
  node [
    id 2051
    label "warunek_lokalowy"
  ]
  node [
    id 2052
    label "plac"
  ]
  node [
    id 2053
    label "location"
  ]
  node [
    id 2054
    label "uwaga"
  ]
  node [
    id 2055
    label "przestrze&#324;"
  ]
  node [
    id 2056
    label "status"
  ]
  node [
    id 2057
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 2058
    label "cia&#322;o"
  ]
  node [
    id 2059
    label "Rzym_Zachodni"
  ]
  node [
    id 2060
    label "Rzym_Wschodni"
  ]
  node [
    id 2061
    label "nagana"
  ]
  node [
    id 2062
    label "upomnienie"
  ]
  node [
    id 2063
    label "dzienniczek"
  ]
  node [
    id 2064
    label "wzgl&#261;d"
  ]
  node [
    id 2065
    label "gossip"
  ]
  node [
    id 2066
    label "rozdzielanie"
  ]
  node [
    id 2067
    label "bezbrze&#380;e"
  ]
  node [
    id 2068
    label "niezmierzony"
  ]
  node [
    id 2069
    label "przedzielenie"
  ]
  node [
    id 2070
    label "nielito&#347;ciwy"
  ]
  node [
    id 2071
    label "rozdziela&#263;"
  ]
  node [
    id 2072
    label "oktant"
  ]
  node [
    id 2073
    label "przedzieli&#263;"
  ]
  node [
    id 2074
    label "przestw&#243;r"
  ]
  node [
    id 2075
    label "condition"
  ]
  node [
    id 2076
    label "awansowa&#263;"
  ]
  node [
    id 2077
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 2078
    label "awans"
  ]
  node [
    id 2079
    label "podmiotowo"
  ]
  node [
    id 2080
    label "awansowanie"
  ]
  node [
    id 2081
    label "sytuacja"
  ]
  node [
    id 2082
    label "rozmiar"
  ]
  node [
    id 2083
    label "circumference"
  ]
  node [
    id 2084
    label "cyrkumferencja"
  ]
  node [
    id 2085
    label "ekshumowanie"
  ]
  node [
    id 2086
    label "odwadnia&#263;"
  ]
  node [
    id 2087
    label "zabalsamowanie"
  ]
  node [
    id 2088
    label "odwodni&#263;"
  ]
  node [
    id 2089
    label "sk&#243;ra"
  ]
  node [
    id 2090
    label "ow&#322;osienie"
  ]
  node [
    id 2091
    label "mi&#281;so"
  ]
  node [
    id 2092
    label "zabalsamowa&#263;"
  ]
  node [
    id 2093
    label "unerwienie"
  ]
  node [
    id 2094
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 2095
    label "kremacja"
  ]
  node [
    id 2096
    label "biorytm"
  ]
  node [
    id 2097
    label "sekcja"
  ]
  node [
    id 2098
    label "otworzy&#263;"
  ]
  node [
    id 2099
    label "otwiera&#263;"
  ]
  node [
    id 2100
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 2101
    label "otworzenie"
  ]
  node [
    id 2102
    label "materia"
  ]
  node [
    id 2103
    label "pochowanie"
  ]
  node [
    id 2104
    label "otwieranie"
  ]
  node [
    id 2105
    label "ty&#322;"
  ]
  node [
    id 2106
    label "szkielet"
  ]
  node [
    id 2107
    label "tanatoplastyk"
  ]
  node [
    id 2108
    label "odwadnianie"
  ]
  node [
    id 2109
    label "odwodnienie"
  ]
  node [
    id 2110
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 2111
    label "nieumar&#322;y"
  ]
  node [
    id 2112
    label "pochowa&#263;"
  ]
  node [
    id 2113
    label "balsamowa&#263;"
  ]
  node [
    id 2114
    label "tanatoplastyka"
  ]
  node [
    id 2115
    label "temperatura"
  ]
  node [
    id 2116
    label "ekshumowa&#263;"
  ]
  node [
    id 2117
    label "balsamowanie"
  ]
  node [
    id 2118
    label "prz&#243;d"
  ]
  node [
    id 2119
    label "l&#281;d&#378;wie"
  ]
  node [
    id 2120
    label "cz&#322;onek"
  ]
  node [
    id 2121
    label "pogrzeb"
  ]
  node [
    id 2122
    label "&#321;ubianka"
  ]
  node [
    id 2123
    label "area"
  ]
  node [
    id 2124
    label "Majdan"
  ]
  node [
    id 2125
    label "pole_bitwy"
  ]
  node [
    id 2126
    label "stoisko"
  ]
  node [
    id 2127
    label "pierzeja"
  ]
  node [
    id 2128
    label "obiekt_handlowy"
  ]
  node [
    id 2129
    label "zgromadzenie"
  ]
  node [
    id 2130
    label "miasto"
  ]
  node [
    id 2131
    label "targowica"
  ]
  node [
    id 2132
    label "kram"
  ]
  node [
    id 2133
    label "przybli&#380;enie"
  ]
  node [
    id 2134
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 2135
    label "kategoria"
  ]
  node [
    id 2136
    label "lon&#380;a"
  ]
  node [
    id 2137
    label "jednostka_systematyczna"
  ]
  node [
    id 2138
    label "premier"
  ]
  node [
    id 2139
    label "Londyn"
  ]
  node [
    id 2140
    label "gabinet_cieni"
  ]
  node [
    id 2141
    label "number"
  ]
  node [
    id 2142
    label "Konsulat"
  ]
  node [
    id 2143
    label "bajt"
  ]
  node [
    id 2144
    label "j&#261;kanie"
  ]
  node [
    id 2145
    label "kontynent"
  ]
  node [
    id 2146
    label "nastawnia"
  ]
  node [
    id 2147
    label "blockage"
  ]
  node [
    id 2148
    label "start"
  ]
  node [
    id 2149
    label "skorupa_ziemska"
  ]
  node [
    id 2150
    label "program"
  ]
  node [
    id 2151
    label "zeszyt"
  ]
  node [
    id 2152
    label "blokowisko"
  ]
  node [
    id 2153
    label "artyku&#322;"
  ]
  node [
    id 2154
    label "barak"
  ]
  node [
    id 2155
    label "stok_kontynentalny"
  ]
  node [
    id 2156
    label "square"
  ]
  node [
    id 2157
    label "kr&#261;g"
  ]
  node [
    id 2158
    label "ram&#243;wka"
  ]
  node [
    id 2159
    label "bie&#380;nia"
  ]
  node [
    id 2160
    label "referat"
  ]
  node [
    id 2161
    label "dom_wielorodzinny"
  ]
  node [
    id 2162
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 2163
    label "publish"
  ]
  node [
    id 2164
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 2165
    label "spring"
  ]
  node [
    id 2166
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 2167
    label "plot"
  ]
  node [
    id 2168
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 2169
    label "buntowa&#263;_si&#281;"
  ]
  node [
    id 2170
    label "pull"
  ]
  node [
    id 2171
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 2172
    label "przybywa&#263;"
  ]
  node [
    id 2173
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 2174
    label "narracja"
  ]
  node [
    id 2175
    label "balkon"
  ]
  node [
    id 2176
    label "kondygnacja"
  ]
  node [
    id 2177
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 2178
    label "dach"
  ]
  node [
    id 2179
    label "strop"
  ]
  node [
    id 2180
    label "klatka_schodowa"
  ]
  node [
    id 2181
    label "przedpro&#380;e"
  ]
  node [
    id 2182
    label "Pentagon"
  ]
  node [
    id 2183
    label "alkierz"
  ]
  node [
    id 2184
    label "front"
  ]
  node [
    id 2185
    label "rokada"
  ]
  node [
    id 2186
    label "sfera"
  ]
  node [
    id 2187
    label "zaleganie"
  ]
  node [
    id 2188
    label "zjednoczenie"
  ]
  node [
    id 2189
    label "przedpole"
  ]
  node [
    id 2190
    label "szczyt"
  ]
  node [
    id 2191
    label "zalega&#263;"
  ]
  node [
    id 2192
    label "przycz&#243;&#322;ek"
  ]
  node [
    id 2193
    label "elewacja"
  ]
  node [
    id 2194
    label "stowarzyszenie"
  ]
  node [
    id 2195
    label "szybowiec"
  ]
  node [
    id 2196
    label "wo&#322;owina"
  ]
  node [
    id 2197
    label "dywizjon_lotniczy"
  ]
  node [
    id 2198
    label "drzwi"
  ]
  node [
    id 2199
    label "strz&#281;pina"
  ]
  node [
    id 2200
    label "&#380;y&#322;kowanie"
  ]
  node [
    id 2201
    label "winglet"
  ]
  node [
    id 2202
    label "lotka"
  ]
  node [
    id 2203
    label "brama"
  ]
  node [
    id 2204
    label "zbroja"
  ]
  node [
    id 2205
    label "wing"
  ]
  node [
    id 2206
    label "skrzele"
  ]
  node [
    id 2207
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 2208
    label "wirolot"
  ]
  node [
    id 2209
    label "samolot"
  ]
  node [
    id 2210
    label "okno"
  ]
  node [
    id 2211
    label "o&#322;tarz"
  ]
  node [
    id 2212
    label "p&#243;&#322;tusza"
  ]
  node [
    id 2213
    label "tuszka"
  ]
  node [
    id 2214
    label "klapa"
  ]
  node [
    id 2215
    label "szyk"
  ]
  node [
    id 2216
    label "dr&#243;b"
  ]
  node [
    id 2217
    label "narz&#261;d_ruchu"
  ]
  node [
    id 2218
    label "husarz"
  ]
  node [
    id 2219
    label "skrzyd&#322;owiec"
  ]
  node [
    id 2220
    label "dr&#243;bka"
  ]
  node [
    id 2221
    label "sterolotka"
  ]
  node [
    id 2222
    label "keson"
  ]
  node [
    id 2223
    label "husaria"
  ]
  node [
    id 2224
    label "ugrupowanie"
  ]
  node [
    id 2225
    label "si&#322;y_powietrzne"
  ]
  node [
    id 2226
    label "izba"
  ]
  node [
    id 2227
    label "belka_stropowa"
  ]
  node [
    id 2228
    label "przegroda"
  ]
  node [
    id 2229
    label "jaskinia"
  ]
  node [
    id 2230
    label "wyrobisko"
  ]
  node [
    id 2231
    label "kaseton"
  ]
  node [
    id 2232
    label "pok&#322;ad"
  ]
  node [
    id 2233
    label "sufit"
  ]
  node [
    id 2234
    label "pu&#322;ap"
  ]
  node [
    id 2235
    label "dziedzina"
  ]
  node [
    id 2236
    label "&#347;lemi&#281;"
  ]
  node [
    id 2237
    label "pokrycie_dachowe"
  ]
  node [
    id 2238
    label "okap"
  ]
  node [
    id 2239
    label "po&#322;a&#263;_dachowa"
  ]
  node [
    id 2240
    label "podsufitka"
  ]
  node [
    id 2241
    label "wi&#281;&#378;ba"
  ]
  node [
    id 2242
    label "nadwozie"
  ]
  node [
    id 2243
    label "obr&#243;bka_blacharska"
  ]
  node [
    id 2244
    label "garderoba"
  ]
  node [
    id 2245
    label "dom"
  ]
  node [
    id 2246
    label "widownia"
  ]
  node [
    id 2247
    label "balustrada"
  ]
  node [
    id 2248
    label "zapadnia"
  ]
  node [
    id 2249
    label "pojazd"
  ]
  node [
    id 2250
    label "posadzka"
  ]
  node [
    id 2251
    label "2012"
  ]
  node [
    id 2252
    label "Rafa&#322;"
  ]
  node [
    id 2253
    label "Dutkiewicz"
  ]
  node [
    id 2254
    label "Water"
  ]
  node [
    id 2255
    label "Tower"
  ]
  node [
    id 2256
    label "promocja"
  ]
  node [
    id 2257
    label "Wroc&#322;aw"
  ]
  node [
    id 2258
    label "pawe&#322;"
  ]
  node [
    id 2259
    label "Romaszkan"
  ]
  node [
    id 2260
    label "mistrz"
  ]
  node [
    id 2261
    label "Final"
  ]
  node [
    id 2262
    label "Four"
  ]
  node [
    id 2263
    label "euroliga"
  ]
  node [
    id 2264
    label "europejski"
  ]
  node [
    id 2265
    label "instytut"
  ]
  node [
    id 2266
    label "technologiczny"
  ]
  node [
    id 2267
    label "stadion"
  ]
  node [
    id 2268
    label "olimpijski"
  ]
  node [
    id 2269
    label "Toyotaka"
  ]
  node [
    id 2270
    label "Ota"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 2251
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 334
  ]
  edge [
    source 14
    target 335
  ]
  edge [
    source 14
    target 336
  ]
  edge [
    source 14
    target 337
  ]
  edge [
    source 14
    target 338
  ]
  edge [
    source 14
    target 339
  ]
  edge [
    source 14
    target 340
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 342
  ]
  edge [
    source 14
    target 343
  ]
  edge [
    source 14
    target 344
  ]
  edge [
    source 14
    target 345
  ]
  edge [
    source 14
    target 346
  ]
  edge [
    source 14
    target 347
  ]
  edge [
    source 14
    target 348
  ]
  edge [
    source 14
    target 349
  ]
  edge [
    source 14
    target 350
  ]
  edge [
    source 14
    target 351
  ]
  edge [
    source 14
    target 352
  ]
  edge [
    source 14
    target 353
  ]
  edge [
    source 14
    target 354
  ]
  edge [
    source 14
    target 355
  ]
  edge [
    source 14
    target 356
  ]
  edge [
    source 14
    target 357
  ]
  edge [
    source 14
    target 358
  ]
  edge [
    source 14
    target 359
  ]
  edge [
    source 14
    target 360
  ]
  edge [
    source 14
    target 361
  ]
  edge [
    source 14
    target 362
  ]
  edge [
    source 14
    target 363
  ]
  edge [
    source 14
    target 364
  ]
  edge [
    source 14
    target 365
  ]
  edge [
    source 14
    target 366
  ]
  edge [
    source 14
    target 367
  ]
  edge [
    source 14
    target 368
  ]
  edge [
    source 14
    target 369
  ]
  edge [
    source 14
    target 370
  ]
  edge [
    source 14
    target 371
  ]
  edge [
    source 14
    target 372
  ]
  edge [
    source 14
    target 373
  ]
  edge [
    source 14
    target 374
  ]
  edge [
    source 14
    target 375
  ]
  edge [
    source 14
    target 376
  ]
  edge [
    source 14
    target 377
  ]
  edge [
    source 14
    target 378
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 380
  ]
  edge [
    source 14
    target 381
  ]
  edge [
    source 14
    target 382
  ]
  edge [
    source 14
    target 383
  ]
  edge [
    source 14
    target 384
  ]
  edge [
    source 14
    target 385
  ]
  edge [
    source 14
    target 386
  ]
  edge [
    source 14
    target 387
  ]
  edge [
    source 14
    target 388
  ]
  edge [
    source 14
    target 389
  ]
  edge [
    source 14
    target 390
  ]
  edge [
    source 14
    target 391
  ]
  edge [
    source 14
    target 392
  ]
  edge [
    source 14
    target 393
  ]
  edge [
    source 14
    target 394
  ]
  edge [
    source 14
    target 395
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 397
  ]
  edge [
    source 14
    target 398
  ]
  edge [
    source 14
    target 399
  ]
  edge [
    source 14
    target 400
  ]
  edge [
    source 14
    target 273
  ]
  edge [
    source 14
    target 401
  ]
  edge [
    source 14
    target 402
  ]
  edge [
    source 14
    target 403
  ]
  edge [
    source 14
    target 404
  ]
  edge [
    source 14
    target 405
  ]
  edge [
    source 14
    target 406
  ]
  edge [
    source 14
    target 407
  ]
  edge [
    source 14
    target 408
  ]
  edge [
    source 14
    target 409
  ]
  edge [
    source 14
    target 410
  ]
  edge [
    source 14
    target 411
  ]
  edge [
    source 14
    target 268
  ]
  edge [
    source 14
    target 412
  ]
  edge [
    source 14
    target 413
  ]
  edge [
    source 14
    target 414
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 416
  ]
  edge [
    source 14
    target 417
  ]
  edge [
    source 14
    target 418
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 420
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 455
  ]
  edge [
    source 15
    target 456
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 421
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 474
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 476
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 17
    target 392
  ]
  edge [
    source 17
    target 480
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 385
  ]
  edge [
    source 17
    target 481
  ]
  edge [
    source 17
    target 482
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 483
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 485
  ]
  edge [
    source 17
    target 486
  ]
  edge [
    source 17
    target 487
  ]
  edge [
    source 17
    target 488
  ]
  edge [
    source 17
    target 489
  ]
  edge [
    source 17
    target 409
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 490
  ]
  edge [
    source 17
    target 491
  ]
  edge [
    source 17
    target 492
  ]
  edge [
    source 17
    target 493
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 494
  ]
  edge [
    source 17
    target 495
  ]
  edge [
    source 17
    target 496
  ]
  edge [
    source 17
    target 497
  ]
  edge [
    source 17
    target 498
  ]
  edge [
    source 17
    target 499
  ]
  edge [
    source 17
    target 500
  ]
  edge [
    source 17
    target 501
  ]
  edge [
    source 17
    target 502
  ]
  edge [
    source 17
    target 503
  ]
  edge [
    source 17
    target 504
  ]
  edge [
    source 17
    target 439
  ]
  edge [
    source 17
    target 505
  ]
  edge [
    source 17
    target 506
  ]
  edge [
    source 17
    target 507
  ]
  edge [
    source 17
    target 508
  ]
  edge [
    source 17
    target 509
  ]
  edge [
    source 17
    target 510
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 511
  ]
  edge [
    source 17
    target 512
  ]
  edge [
    source 17
    target 268
  ]
  edge [
    source 17
    target 513
  ]
  edge [
    source 17
    target 514
  ]
  edge [
    source 17
    target 515
  ]
  edge [
    source 17
    target 516
  ]
  edge [
    source 17
    target 517
  ]
  edge [
    source 17
    target 518
  ]
  edge [
    source 17
    target 519
  ]
  edge [
    source 17
    target 520
  ]
  edge [
    source 17
    target 521
  ]
  edge [
    source 17
    target 522
  ]
  edge [
    source 17
    target 523
  ]
  edge [
    source 17
    target 524
  ]
  edge [
    source 17
    target 525
  ]
  edge [
    source 17
    target 526
  ]
  edge [
    source 17
    target 527
  ]
  edge [
    source 17
    target 528
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 530
  ]
  edge [
    source 17
    target 531
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 534
  ]
  edge [
    source 17
    target 535
  ]
  edge [
    source 17
    target 536
  ]
  edge [
    source 17
    target 537
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 539
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 541
  ]
  edge [
    source 17
    target 542
  ]
  edge [
    source 17
    target 543
  ]
  edge [
    source 17
    target 544
  ]
  edge [
    source 17
    target 545
  ]
  edge [
    source 17
    target 546
  ]
  edge [
    source 17
    target 547
  ]
  edge [
    source 17
    target 548
  ]
  edge [
    source 17
    target 549
  ]
  edge [
    source 17
    target 550
  ]
  edge [
    source 17
    target 551
  ]
  edge [
    source 17
    target 552
  ]
  edge [
    source 17
    target 553
  ]
  edge [
    source 17
    target 554
  ]
  edge [
    source 17
    target 555
  ]
  edge [
    source 17
    target 556
  ]
  edge [
    source 17
    target 557
  ]
  edge [
    source 17
    target 558
  ]
  edge [
    source 17
    target 559
  ]
  edge [
    source 17
    target 560
  ]
  edge [
    source 17
    target 561
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 17
    target 563
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 565
  ]
  edge [
    source 17
    target 566
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 567
  ]
  edge [
    source 17
    target 568
  ]
  edge [
    source 17
    target 429
  ]
  edge [
    source 17
    target 569
  ]
  edge [
    source 17
    target 570
  ]
  edge [
    source 17
    target 571
  ]
  edge [
    source 17
    target 572
  ]
  edge [
    source 17
    target 573
  ]
  edge [
    source 17
    target 574
  ]
  edge [
    source 17
    target 575
  ]
  edge [
    source 17
    target 576
  ]
  edge [
    source 17
    target 577
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 579
  ]
  edge [
    source 17
    target 580
  ]
  edge [
    source 17
    target 581
  ]
  edge [
    source 17
    target 582
  ]
  edge [
    source 17
    target 583
  ]
  edge [
    source 17
    target 584
  ]
  edge [
    source 17
    target 585
  ]
  edge [
    source 17
    target 414
  ]
  edge [
    source 17
    target 586
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 588
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 591
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 593
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 595
  ]
  edge [
    source 17
    target 596
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 598
  ]
  edge [
    source 17
    target 599
  ]
  edge [
    source 17
    target 600
  ]
  edge [
    source 17
    target 601
  ]
  edge [
    source 17
    target 602
  ]
  edge [
    source 17
    target 603
  ]
  edge [
    source 17
    target 604
  ]
  edge [
    source 17
    target 605
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 610
  ]
  edge [
    source 17
    target 611
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 614
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 622
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 625
  ]
  edge [
    source 17
    target 626
  ]
  edge [
    source 17
    target 627
  ]
  edge [
    source 17
    target 628
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 632
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 635
  ]
  edge [
    source 17
    target 636
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 638
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 32
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 268
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 65
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 38
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 639
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 712
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 714
  ]
  edge [
    source 19
    target 715
  ]
  edge [
    source 19
    target 716
  ]
  edge [
    source 19
    target 717
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 719
  ]
  edge [
    source 19
    target 720
  ]
  edge [
    source 19
    target 721
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 725
  ]
  edge [
    source 19
    target 726
  ]
  edge [
    source 19
    target 727
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 728
  ]
  edge [
    source 19
    target 729
  ]
  edge [
    source 19
    target 730
  ]
  edge [
    source 19
    target 731
  ]
  edge [
    source 19
    target 732
  ]
  edge [
    source 19
    target 733
  ]
  edge [
    source 19
    target 734
  ]
  edge [
    source 19
    target 735
  ]
  edge [
    source 19
    target 736
  ]
  edge [
    source 19
    target 737
  ]
  edge [
    source 19
    target 738
  ]
  edge [
    source 19
    target 739
  ]
  edge [
    source 19
    target 740
  ]
  edge [
    source 19
    target 741
  ]
  edge [
    source 19
    target 742
  ]
  edge [
    source 19
    target 743
  ]
  edge [
    source 19
    target 744
  ]
  edge [
    source 19
    target 745
  ]
  edge [
    source 19
    target 746
  ]
  edge [
    source 19
    target 747
  ]
  edge [
    source 19
    target 748
  ]
  edge [
    source 19
    target 749
  ]
  edge [
    source 19
    target 750
  ]
  edge [
    source 19
    target 751
  ]
  edge [
    source 19
    target 752
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 414
  ]
  edge [
    source 19
    target 753
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 754
  ]
  edge [
    source 19
    target 755
  ]
  edge [
    source 19
    target 756
  ]
  edge [
    source 19
    target 757
  ]
  edge [
    source 19
    target 54
  ]
  edge [
    source 19
    target 758
  ]
  edge [
    source 19
    target 759
  ]
  edge [
    source 19
    target 760
  ]
  edge [
    source 19
    target 761
  ]
  edge [
    source 19
    target 762
  ]
  edge [
    source 19
    target 763
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 764
  ]
  edge [
    source 20
    target 765
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 772
  ]
  edge [
    source 21
    target 627
  ]
  edge [
    source 21
    target 773
  ]
  edge [
    source 21
    target 774
  ]
  edge [
    source 21
    target 775
  ]
  edge [
    source 21
    target 776
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 777
  ]
  edge [
    source 21
    target 778
  ]
  edge [
    source 21
    target 779
  ]
  edge [
    source 21
    target 780
  ]
  edge [
    source 21
    target 781
  ]
  edge [
    source 21
    target 782
  ]
  edge [
    source 21
    target 783
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 786
  ]
  edge [
    source 21
    target 787
  ]
  edge [
    source 21
    target 788
  ]
  edge [
    source 21
    target 789
  ]
  edge [
    source 21
    target 790
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 794
  ]
  edge [
    source 21
    target 795
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 797
  ]
  edge [
    source 21
    target 798
  ]
  edge [
    source 21
    target 799
  ]
  edge [
    source 21
    target 484
  ]
  edge [
    source 21
    target 485
  ]
  edge [
    source 21
    target 486
  ]
  edge [
    source 21
    target 487
  ]
  edge [
    source 21
    target 488
  ]
  edge [
    source 21
    target 489
  ]
  edge [
    source 21
    target 409
  ]
  edge [
    source 21
    target 800
  ]
  edge [
    source 21
    target 99
  ]
  edge [
    source 21
    target 801
  ]
  edge [
    source 21
    target 802
  ]
  edge [
    source 21
    target 803
  ]
  edge [
    source 21
    target 804
  ]
  edge [
    source 21
    target 805
  ]
  edge [
    source 21
    target 806
  ]
  edge [
    source 21
    target 807
  ]
  edge [
    source 21
    target 808
  ]
  edge [
    source 21
    target 583
  ]
  edge [
    source 21
    target 809
  ]
  edge [
    source 21
    target 810
  ]
  edge [
    source 21
    target 811
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 812
  ]
  edge [
    source 22
    target 813
  ]
  edge [
    source 22
    target 814
  ]
  edge [
    source 22
    target 815
  ]
  edge [
    source 22
    target 816
  ]
  edge [
    source 22
    target 817
  ]
  edge [
    source 22
    target 818
  ]
  edge [
    source 22
    target 819
  ]
  edge [
    source 22
    target 820
  ]
  edge [
    source 22
    target 821
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 22
    target 823
  ]
  edge [
    source 22
    target 824
  ]
  edge [
    source 22
    target 825
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 22
    target 827
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 829
  ]
  edge [
    source 22
    target 830
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 22
    target 833
  ]
  edge [
    source 22
    target 834
  ]
  edge [
    source 22
    target 835
  ]
  edge [
    source 22
    target 836
  ]
  edge [
    source 22
    target 837
  ]
  edge [
    source 22
    target 838
  ]
  edge [
    source 22
    target 839
  ]
  edge [
    source 22
    target 840
  ]
  edge [
    source 22
    target 841
  ]
  edge [
    source 22
    target 842
  ]
  edge [
    source 22
    target 843
  ]
  edge [
    source 22
    target 844
  ]
  edge [
    source 22
    target 845
  ]
  edge [
    source 22
    target 846
  ]
  edge [
    source 22
    target 847
  ]
  edge [
    source 22
    target 848
  ]
  edge [
    source 22
    target 849
  ]
  edge [
    source 22
    target 850
  ]
  edge [
    source 22
    target 851
  ]
  edge [
    source 22
    target 852
  ]
  edge [
    source 22
    target 853
  ]
  edge [
    source 22
    target 854
  ]
  edge [
    source 22
    target 855
  ]
  edge [
    source 22
    target 856
  ]
  edge [
    source 22
    target 857
  ]
  edge [
    source 22
    target 858
  ]
  edge [
    source 22
    target 859
  ]
  edge [
    source 22
    target 860
  ]
  edge [
    source 22
    target 861
  ]
  edge [
    source 22
    target 862
  ]
  edge [
    source 22
    target 863
  ]
  edge [
    source 22
    target 864
  ]
  edge [
    source 22
    target 865
  ]
  edge [
    source 22
    target 866
  ]
  edge [
    source 22
    target 867
  ]
  edge [
    source 22
    target 868
  ]
  edge [
    source 22
    target 869
  ]
  edge [
    source 22
    target 870
  ]
  edge [
    source 22
    target 871
  ]
  edge [
    source 22
    target 872
  ]
  edge [
    source 22
    target 873
  ]
  edge [
    source 22
    target 874
  ]
  edge [
    source 22
    target 875
  ]
  edge [
    source 22
    target 876
  ]
  edge [
    source 22
    target 877
  ]
  edge [
    source 22
    target 878
  ]
  edge [
    source 22
    target 879
  ]
  edge [
    source 22
    target 880
  ]
  edge [
    source 22
    target 881
  ]
  edge [
    source 22
    target 882
  ]
  edge [
    source 22
    target 883
  ]
  edge [
    source 22
    target 884
  ]
  edge [
    source 22
    target 885
  ]
  edge [
    source 22
    target 886
  ]
  edge [
    source 22
    target 887
  ]
  edge [
    source 22
    target 888
  ]
  edge [
    source 22
    target 889
  ]
  edge [
    source 22
    target 890
  ]
  edge [
    source 22
    target 891
  ]
  edge [
    source 22
    target 892
  ]
  edge [
    source 22
    target 893
  ]
  edge [
    source 22
    target 894
  ]
  edge [
    source 22
    target 895
  ]
  edge [
    source 22
    target 896
  ]
  edge [
    source 22
    target 897
  ]
  edge [
    source 22
    target 898
  ]
  edge [
    source 22
    target 899
  ]
  edge [
    source 22
    target 900
  ]
  edge [
    source 22
    target 901
  ]
  edge [
    source 22
    target 902
  ]
  edge [
    source 22
    target 903
  ]
  edge [
    source 22
    target 904
  ]
  edge [
    source 22
    target 905
  ]
  edge [
    source 22
    target 906
  ]
  edge [
    source 22
    target 907
  ]
  edge [
    source 22
    target 908
  ]
  edge [
    source 22
    target 909
  ]
  edge [
    source 22
    target 910
  ]
  edge [
    source 22
    target 911
  ]
  edge [
    source 22
    target 912
  ]
  edge [
    source 22
    target 913
  ]
  edge [
    source 22
    target 914
  ]
  edge [
    source 22
    target 915
  ]
  edge [
    source 22
    target 916
  ]
  edge [
    source 22
    target 917
  ]
  edge [
    source 22
    target 918
  ]
  edge [
    source 22
    target 919
  ]
  edge [
    source 22
    target 920
  ]
  edge [
    source 22
    target 921
  ]
  edge [
    source 22
    target 922
  ]
  edge [
    source 22
    target 923
  ]
  edge [
    source 22
    target 924
  ]
  edge [
    source 22
    target 925
  ]
  edge [
    source 22
    target 926
  ]
  edge [
    source 22
    target 927
  ]
  edge [
    source 22
    target 928
  ]
  edge [
    source 22
    target 929
  ]
  edge [
    source 22
    target 930
  ]
  edge [
    source 22
    target 931
  ]
  edge [
    source 22
    target 932
  ]
  edge [
    source 22
    target 933
  ]
  edge [
    source 22
    target 53
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 934
  ]
  edge [
    source 23
    target 935
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 937
  ]
  edge [
    source 23
    target 938
  ]
  edge [
    source 23
    target 939
  ]
  edge [
    source 23
    target 940
  ]
  edge [
    source 23
    target 941
  ]
  edge [
    source 23
    target 942
  ]
  edge [
    source 23
    target 943
  ]
  edge [
    source 23
    target 944
  ]
  edge [
    source 23
    target 945
  ]
  edge [
    source 23
    target 946
  ]
  edge [
    source 23
    target 947
  ]
  edge [
    source 23
    target 948
  ]
  edge [
    source 23
    target 949
  ]
  edge [
    source 23
    target 950
  ]
  edge [
    source 23
    target 951
  ]
  edge [
    source 23
    target 952
  ]
  edge [
    source 23
    target 953
  ]
  edge [
    source 23
    target 954
  ]
  edge [
    source 23
    target 955
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 956
  ]
  edge [
    source 24
    target 957
  ]
  edge [
    source 24
    target 958
  ]
  edge [
    source 24
    target 959
  ]
  edge [
    source 24
    target 960
  ]
  edge [
    source 24
    target 961
  ]
  edge [
    source 24
    target 962
  ]
  edge [
    source 24
    target 963
  ]
  edge [
    source 24
    target 964
  ]
  edge [
    source 24
    target 965
  ]
  edge [
    source 24
    target 966
  ]
  edge [
    source 24
    target 967
  ]
  edge [
    source 24
    target 968
  ]
  edge [
    source 24
    target 969
  ]
  edge [
    source 24
    target 970
  ]
  edge [
    source 24
    target 971
  ]
  edge [
    source 24
    target 972
  ]
  edge [
    source 24
    target 973
  ]
  edge [
    source 24
    target 387
  ]
  edge [
    source 24
    target 579
  ]
  edge [
    source 24
    target 974
  ]
  edge [
    source 24
    target 975
  ]
  edge [
    source 24
    target 976
  ]
  edge [
    source 24
    target 977
  ]
  edge [
    source 24
    target 978
  ]
  edge [
    source 24
    target 979
  ]
  edge [
    source 24
    target 980
  ]
  edge [
    source 24
    target 981
  ]
  edge [
    source 24
    target 982
  ]
  edge [
    source 24
    target 983
  ]
  edge [
    source 24
    target 984
  ]
  edge [
    source 24
    target 985
  ]
  edge [
    source 24
    target 986
  ]
  edge [
    source 24
    target 987
  ]
  edge [
    source 24
    target 988
  ]
  edge [
    source 24
    target 989
  ]
  edge [
    source 24
    target 990
  ]
  edge [
    source 24
    target 991
  ]
  edge [
    source 24
    target 992
  ]
  edge [
    source 24
    target 993
  ]
  edge [
    source 24
    target 994
  ]
  edge [
    source 24
    target 995
  ]
  edge [
    source 24
    target 996
  ]
  edge [
    source 24
    target 116
  ]
  edge [
    source 24
    target 997
  ]
  edge [
    source 24
    target 998
  ]
  edge [
    source 24
    target 421
  ]
  edge [
    source 24
    target 999
  ]
  edge [
    source 24
    target 1000
  ]
  edge [
    source 24
    target 1001
  ]
  edge [
    source 24
    target 1002
  ]
  edge [
    source 24
    target 1003
  ]
  edge [
    source 24
    target 1004
  ]
  edge [
    source 24
    target 1005
  ]
  edge [
    source 24
    target 643
  ]
  edge [
    source 24
    target 1006
  ]
  edge [
    source 24
    target 1007
  ]
  edge [
    source 24
    target 1008
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 1009
  ]
  edge [
    source 24
    target 1010
  ]
  edge [
    source 24
    target 1011
  ]
  edge [
    source 24
    target 1012
  ]
  edge [
    source 24
    target 1013
  ]
  edge [
    source 24
    target 1014
  ]
  edge [
    source 24
    target 1015
  ]
  edge [
    source 24
    target 1016
  ]
  edge [
    source 24
    target 1017
  ]
  edge [
    source 24
    target 1018
  ]
  edge [
    source 24
    target 1019
  ]
  edge [
    source 24
    target 1020
  ]
  edge [
    source 24
    target 935
  ]
  edge [
    source 24
    target 1021
  ]
  edge [
    source 24
    target 1022
  ]
  edge [
    source 24
    target 1023
  ]
  edge [
    source 24
    target 1024
  ]
  edge [
    source 24
    target 1025
  ]
  edge [
    source 24
    target 1026
  ]
  edge [
    source 24
    target 1027
  ]
  edge [
    source 24
    target 1028
  ]
  edge [
    source 24
    target 1029
  ]
  edge [
    source 24
    target 1030
  ]
  edge [
    source 24
    target 1031
  ]
  edge [
    source 24
    target 1032
  ]
  edge [
    source 24
    target 1033
  ]
  edge [
    source 24
    target 1034
  ]
  edge [
    source 24
    target 1035
  ]
  edge [
    source 24
    target 1036
  ]
  edge [
    source 24
    target 1037
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1038
  ]
  edge [
    source 25
    target 1039
  ]
  edge [
    source 25
    target 1040
  ]
  edge [
    source 25
    target 1041
  ]
  edge [
    source 25
    target 1042
  ]
  edge [
    source 25
    target 1043
  ]
  edge [
    source 25
    target 1044
  ]
  edge [
    source 25
    target 460
  ]
  edge [
    source 25
    target 1045
  ]
  edge [
    source 25
    target 1046
  ]
  edge [
    source 25
    target 1047
  ]
  edge [
    source 25
    target 1048
  ]
  edge [
    source 25
    target 1049
  ]
  edge [
    source 25
    target 1050
  ]
  edge [
    source 25
    target 1051
  ]
  edge [
    source 25
    target 1052
  ]
  edge [
    source 25
    target 1053
  ]
  edge [
    source 25
    target 418
  ]
  edge [
    source 25
    target 1054
  ]
  edge [
    source 25
    target 1055
  ]
  edge [
    source 25
    target 1056
  ]
  edge [
    source 25
    target 1057
  ]
  edge [
    source 25
    target 1058
  ]
  edge [
    source 25
    target 1059
  ]
  edge [
    source 25
    target 1060
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1061
  ]
  edge [
    source 26
    target 1062
  ]
  edge [
    source 26
    target 1063
  ]
  edge [
    source 26
    target 1064
  ]
  edge [
    source 26
    target 1065
  ]
  edge [
    source 26
    target 1066
  ]
  edge [
    source 26
    target 1067
  ]
  edge [
    source 26
    target 1068
  ]
  edge [
    source 26
    target 1069
  ]
  edge [
    source 26
    target 1070
  ]
  edge [
    source 26
    target 1071
  ]
  edge [
    source 26
    target 1072
  ]
  edge [
    source 26
    target 1073
  ]
  edge [
    source 26
    target 1074
  ]
  edge [
    source 26
    target 1075
  ]
  edge [
    source 26
    target 951
  ]
  edge [
    source 26
    target 1076
  ]
  edge [
    source 26
    target 1077
  ]
  edge [
    source 26
    target 1078
  ]
  edge [
    source 26
    target 1079
  ]
  edge [
    source 26
    target 1080
  ]
  edge [
    source 26
    target 1081
  ]
  edge [
    source 26
    target 1082
  ]
  edge [
    source 26
    target 1083
  ]
  edge [
    source 26
    target 1084
  ]
  edge [
    source 26
    target 1085
  ]
  edge [
    source 26
    target 1086
  ]
  edge [
    source 26
    target 1087
  ]
  edge [
    source 26
    target 1088
  ]
  edge [
    source 26
    target 943
  ]
  edge [
    source 26
    target 1089
  ]
  edge [
    source 26
    target 1090
  ]
  edge [
    source 26
    target 653
  ]
  edge [
    source 26
    target 1091
  ]
  edge [
    source 26
    target 1092
  ]
  edge [
    source 26
    target 1093
  ]
  edge [
    source 26
    target 1094
  ]
  edge [
    source 26
    target 299
  ]
  edge [
    source 26
    target 1095
  ]
  edge [
    source 26
    target 1096
  ]
  edge [
    source 26
    target 1097
  ]
  edge [
    source 26
    target 1098
  ]
  edge [
    source 26
    target 1099
  ]
  edge [
    source 26
    target 1028
  ]
  edge [
    source 26
    target 1100
  ]
  edge [
    source 26
    target 1101
  ]
  edge [
    source 26
    target 301
  ]
  edge [
    source 26
    target 1102
  ]
  edge [
    source 26
    target 1103
  ]
  edge [
    source 26
    target 1104
  ]
  edge [
    source 26
    target 1105
  ]
  edge [
    source 26
    target 1106
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 1107
  ]
  edge [
    source 26
    target 1108
  ]
  edge [
    source 26
    target 1109
  ]
  edge [
    source 26
    target 1110
  ]
  edge [
    source 26
    target 1111
  ]
  edge [
    source 26
    target 1112
  ]
  edge [
    source 26
    target 1113
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1114
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 1116
  ]
  edge [
    source 28
    target 1117
  ]
  edge [
    source 28
    target 1118
  ]
  edge [
    source 28
    target 1119
  ]
  edge [
    source 28
    target 1120
  ]
  edge [
    source 28
    target 1121
  ]
  edge [
    source 28
    target 1122
  ]
  edge [
    source 28
    target 1123
  ]
  edge [
    source 28
    target 1124
  ]
  edge [
    source 28
    target 1125
  ]
  edge [
    source 28
    target 1126
  ]
  edge [
    source 28
    target 1127
  ]
  edge [
    source 28
    target 1128
  ]
  edge [
    source 28
    target 465
  ]
  edge [
    source 28
    target 1129
  ]
  edge [
    source 28
    target 1130
  ]
  edge [
    source 28
    target 1131
  ]
  edge [
    source 28
    target 1132
  ]
  edge [
    source 28
    target 1133
  ]
  edge [
    source 28
    target 1134
  ]
  edge [
    source 28
    target 1135
  ]
  edge [
    source 28
    target 1136
  ]
  edge [
    source 28
    target 1137
  ]
  edge [
    source 28
    target 1138
  ]
  edge [
    source 28
    target 1139
  ]
  edge [
    source 28
    target 1140
  ]
  edge [
    source 28
    target 492
  ]
  edge [
    source 28
    target 1141
  ]
  edge [
    source 28
    target 1142
  ]
  edge [
    source 28
    target 1143
  ]
  edge [
    source 28
    target 1144
  ]
  edge [
    source 28
    target 1145
  ]
  edge [
    source 28
    target 586
  ]
  edge [
    source 28
    target 1146
  ]
  edge [
    source 28
    target 1147
  ]
  edge [
    source 28
    target 1148
  ]
  edge [
    source 28
    target 268
  ]
  edge [
    source 28
    target 1149
  ]
  edge [
    source 28
    target 1150
  ]
  edge [
    source 28
    target 475
  ]
  edge [
    source 28
    target 1151
  ]
  edge [
    source 28
    target 1152
  ]
  edge [
    source 28
    target 1153
  ]
  edge [
    source 28
    target 1154
  ]
  edge [
    source 28
    target 1155
  ]
  edge [
    source 28
    target 1156
  ]
  edge [
    source 28
    target 1157
  ]
  edge [
    source 28
    target 1158
  ]
  edge [
    source 28
    target 1159
  ]
  edge [
    source 28
    target 731
  ]
  edge [
    source 28
    target 1160
  ]
  edge [
    source 28
    target 1161
  ]
  edge [
    source 28
    target 1162
  ]
  edge [
    source 28
    target 1163
  ]
  edge [
    source 28
    target 1164
  ]
  edge [
    source 28
    target 1165
  ]
  edge [
    source 28
    target 1166
  ]
  edge [
    source 28
    target 1167
  ]
  edge [
    source 28
    target 184
  ]
  edge [
    source 28
    target 467
  ]
  edge [
    source 28
    target 469
  ]
  edge [
    source 28
    target 468
  ]
  edge [
    source 28
    target 470
  ]
  edge [
    source 28
    target 471
  ]
  edge [
    source 28
    target 472
  ]
  edge [
    source 28
    target 473
  ]
  edge [
    source 28
    target 474
  ]
  edge [
    source 28
    target 476
  ]
  edge [
    source 28
    target 477
  ]
  edge [
    source 28
    target 478
  ]
  edge [
    source 28
    target 50
  ]
  edge [
    source 28
    target 479
  ]
  edge [
    source 28
    target 392
  ]
  edge [
    source 28
    target 480
  ]
  edge [
    source 28
    target 99
  ]
  edge [
    source 28
    target 385
  ]
  edge [
    source 28
    target 481
  ]
  edge [
    source 28
    target 482
  ]
  edge [
    source 28
    target 318
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 483
  ]
  edge [
    source 28
    target 1168
  ]
  edge [
    source 28
    target 1001
  ]
  edge [
    source 28
    target 1169
  ]
  edge [
    source 28
    target 1170
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 1171
  ]
  edge [
    source 28
    target 1172
  ]
  edge [
    source 28
    target 1173
  ]
  edge [
    source 28
    target 1174
  ]
  edge [
    source 28
    target 1175
  ]
  edge [
    source 28
    target 998
  ]
  edge [
    source 28
    target 1176
  ]
  edge [
    source 28
    target 1177
  ]
  edge [
    source 28
    target 1003
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1178
  ]
  edge [
    source 29
    target 1179
  ]
  edge [
    source 29
    target 1180
  ]
  edge [
    source 29
    target 1181
  ]
  edge [
    source 29
    target 1182
  ]
  edge [
    source 29
    target 1183
  ]
  edge [
    source 29
    target 1184
  ]
  edge [
    source 29
    target 1185
  ]
  edge [
    source 29
    target 943
  ]
  edge [
    source 29
    target 1186
  ]
  edge [
    source 29
    target 1187
  ]
  edge [
    source 29
    target 1188
  ]
  edge [
    source 29
    target 1189
  ]
  edge [
    source 29
    target 1190
  ]
  edge [
    source 29
    target 1191
  ]
  edge [
    source 29
    target 1192
  ]
  edge [
    source 29
    target 1193
  ]
  edge [
    source 29
    target 1194
  ]
  edge [
    source 29
    target 1195
  ]
  edge [
    source 29
    target 1196
  ]
  edge [
    source 29
    target 1197
  ]
  edge [
    source 29
    target 1198
  ]
  edge [
    source 29
    target 1199
  ]
  edge [
    source 29
    target 1200
  ]
  edge [
    source 29
    target 1201
  ]
  edge [
    source 30
    target 1202
  ]
  edge [
    source 30
    target 1203
  ]
  edge [
    source 30
    target 387
  ]
  edge [
    source 30
    target 1204
  ]
  edge [
    source 30
    target 1205
  ]
  edge [
    source 30
    target 1206
  ]
  edge [
    source 30
    target 1207
  ]
  edge [
    source 30
    target 643
  ]
  edge [
    source 30
    target 1208
  ]
  edge [
    source 30
    target 1209
  ]
  edge [
    source 30
    target 1210
  ]
  edge [
    source 30
    target 1211
  ]
  edge [
    source 30
    target 1212
  ]
  edge [
    source 30
    target 1213
  ]
  edge [
    source 30
    target 1214
  ]
  edge [
    source 30
    target 1215
  ]
  edge [
    source 30
    target 973
  ]
  edge [
    source 30
    target 1216
  ]
  edge [
    source 30
    target 510
  ]
  edge [
    source 30
    target 1217
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 372
  ]
  edge [
    source 30
    target 508
  ]
  edge [
    source 30
    target 1218
  ]
  edge [
    source 30
    target 576
  ]
  edge [
    source 30
    target 1219
  ]
  edge [
    source 30
    target 1220
  ]
  edge [
    source 30
    target 1221
  ]
  edge [
    source 30
    target 1222
  ]
  edge [
    source 30
    target 1223
  ]
  edge [
    source 30
    target 414
  ]
  edge [
    source 30
    target 1224
  ]
  edge [
    source 30
    target 1225
  ]
  edge [
    source 30
    target 1226
  ]
  edge [
    source 30
    target 1227
  ]
  edge [
    source 30
    target 1228
  ]
  edge [
    source 30
    target 495
  ]
  edge [
    source 30
    target 1229
  ]
  edge [
    source 30
    target 1230
  ]
  edge [
    source 30
    target 1231
  ]
  edge [
    source 30
    target 1232
  ]
  edge [
    source 30
    target 795
  ]
  edge [
    source 30
    target 1233
  ]
  edge [
    source 30
    target 1234
  ]
  edge [
    source 30
    target 1235
  ]
  edge [
    source 30
    target 1236
  ]
  edge [
    source 30
    target 1237
  ]
  edge [
    source 30
    target 1238
  ]
  edge [
    source 30
    target 1239
  ]
  edge [
    source 30
    target 1240
  ]
  edge [
    source 30
    target 1241
  ]
  edge [
    source 30
    target 1242
  ]
  edge [
    source 30
    target 1243
  ]
  edge [
    source 30
    target 1244
  ]
  edge [
    source 30
    target 1245
  ]
  edge [
    source 30
    target 1246
  ]
  edge [
    source 30
    target 1247
  ]
  edge [
    source 30
    target 400
  ]
  edge [
    source 30
    target 1248
  ]
  edge [
    source 30
    target 1249
  ]
  edge [
    source 30
    target 1250
  ]
  edge [
    source 30
    target 1251
  ]
  edge [
    source 30
    target 80
  ]
  edge [
    source 30
    target 1252
  ]
  edge [
    source 30
    target 1253
  ]
  edge [
    source 30
    target 1254
  ]
  edge [
    source 30
    target 1255
  ]
  edge [
    source 30
    target 1256
  ]
  edge [
    source 30
    target 1257
  ]
  edge [
    source 30
    target 1258
  ]
  edge [
    source 30
    target 1259
  ]
  edge [
    source 30
    target 1260
  ]
  edge [
    source 30
    target 1261
  ]
  edge [
    source 30
    target 1262
  ]
  edge [
    source 30
    target 1263
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1264
  ]
  edge [
    source 31
    target 1265
  ]
  edge [
    source 31
    target 114
  ]
  edge [
    source 31
    target 1250
  ]
  edge [
    source 31
    target 1266
  ]
  edge [
    source 31
    target 1267
  ]
  edge [
    source 31
    target 1268
  ]
  edge [
    source 31
    target 1269
  ]
  edge [
    source 31
    target 1270
  ]
  edge [
    source 31
    target 1271
  ]
  edge [
    source 31
    target 1272
  ]
  edge [
    source 31
    target 1273
  ]
  edge [
    source 31
    target 1274
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 626
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 32
    target 1276
  ]
  edge [
    source 32
    target 1277
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1279
  ]
  edge [
    source 32
    target 1280
  ]
  edge [
    source 32
    target 1281
  ]
  edge [
    source 32
    target 1282
  ]
  edge [
    source 32
    target 1214
  ]
  edge [
    source 32
    target 1283
  ]
  edge [
    source 32
    target 87
  ]
  edge [
    source 32
    target 1284
  ]
  edge [
    source 32
    target 1285
  ]
  edge [
    source 32
    target 62
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1288
  ]
  edge [
    source 32
    target 1289
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1291
  ]
  edge [
    source 32
    target 1250
  ]
  edge [
    source 32
    target 1292
  ]
  edge [
    source 32
    target 1293
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 1294
  ]
  edge [
    source 32
    target 1295
  ]
  edge [
    source 32
    target 1296
  ]
  edge [
    source 32
    target 1297
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 32
    target 1298
  ]
  edge [
    source 32
    target 1299
  ]
  edge [
    source 32
    target 1300
  ]
  edge [
    source 32
    target 1301
  ]
  edge [
    source 32
    target 1302
  ]
  edge [
    source 32
    target 1303
  ]
  edge [
    source 32
    target 1304
  ]
  edge [
    source 32
    target 1305
  ]
  edge [
    source 32
    target 592
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 1306
  ]
  edge [
    source 32
    target 1307
  ]
  edge [
    source 32
    target 417
  ]
  edge [
    source 32
    target 1308
  ]
  edge [
    source 32
    target 1309
  ]
  edge [
    source 32
    target 1310
  ]
  edge [
    source 32
    target 1311
  ]
  edge [
    source 32
    target 1312
  ]
  edge [
    source 32
    target 1313
  ]
  edge [
    source 32
    target 1314
  ]
  edge [
    source 32
    target 1315
  ]
  edge [
    source 32
    target 1316
  ]
  edge [
    source 32
    target 1317
  ]
  edge [
    source 32
    target 1318
  ]
  edge [
    source 32
    target 1319
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 1321
  ]
  edge [
    source 32
    target 1322
  ]
  edge [
    source 32
    target 1323
  ]
  edge [
    source 32
    target 1324
  ]
  edge [
    source 32
    target 1325
  ]
  edge [
    source 32
    target 1326
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 32
    target 603
  ]
  edge [
    source 32
    target 1327
  ]
  edge [
    source 32
    target 1328
  ]
  edge [
    source 32
    target 1329
  ]
  edge [
    source 32
    target 1330
  ]
  edge [
    source 32
    target 1331
  ]
  edge [
    source 32
    target 1332
  ]
  edge [
    source 32
    target 1333
  ]
  edge [
    source 32
    target 1001
  ]
  edge [
    source 32
    target 1334
  ]
  edge [
    source 32
    target 1335
  ]
  edge [
    source 32
    target 1336
  ]
  edge [
    source 32
    target 609
  ]
  edge [
    source 32
    target 1337
  ]
  edge [
    source 32
    target 1338
  ]
  edge [
    source 32
    target 1339
  ]
  edge [
    source 32
    target 1340
  ]
  edge [
    source 32
    target 1341
  ]
  edge [
    source 32
    target 1342
  ]
  edge [
    source 32
    target 576
  ]
  edge [
    source 32
    target 1343
  ]
  edge [
    source 32
    target 1344
  ]
  edge [
    source 32
    target 1345
  ]
  edge [
    source 32
    target 1346
  ]
  edge [
    source 32
    target 1347
  ]
  edge [
    source 32
    target 1348
  ]
  edge [
    source 32
    target 1349
  ]
  edge [
    source 32
    target 1350
  ]
  edge [
    source 32
    target 1351
  ]
  edge [
    source 32
    target 1352
  ]
  edge [
    source 32
    target 1353
  ]
  edge [
    source 32
    target 1354
  ]
  edge [
    source 32
    target 1355
  ]
  edge [
    source 32
    target 1356
  ]
  edge [
    source 32
    target 1171
  ]
  edge [
    source 32
    target 1357
  ]
  edge [
    source 32
    target 1358
  ]
  edge [
    source 32
    target 1359
  ]
  edge [
    source 32
    target 1360
  ]
  edge [
    source 32
    target 1361
  ]
  edge [
    source 32
    target 1362
  ]
  edge [
    source 32
    target 1363
  ]
  edge [
    source 32
    target 1364
  ]
  edge [
    source 32
    target 1365
  ]
  edge [
    source 32
    target 1366
  ]
  edge [
    source 32
    target 1367
  ]
  edge [
    source 32
    target 1368
  ]
  edge [
    source 32
    target 1369
  ]
  edge [
    source 32
    target 1174
  ]
  edge [
    source 32
    target 1370
  ]
  edge [
    source 32
    target 1371
  ]
  edge [
    source 32
    target 1372
  ]
  edge [
    source 32
    target 1373
  ]
  edge [
    source 32
    target 1374
  ]
  edge [
    source 32
    target 1375
  ]
  edge [
    source 32
    target 1376
  ]
  edge [
    source 32
    target 1377
  ]
  edge [
    source 32
    target 1378
  ]
  edge [
    source 32
    target 1379
  ]
  edge [
    source 32
    target 639
  ]
  edge [
    source 32
    target 1380
  ]
  edge [
    source 32
    target 1381
  ]
  edge [
    source 32
    target 1382
  ]
  edge [
    source 32
    target 1383
  ]
  edge [
    source 32
    target 1384
  ]
  edge [
    source 32
    target 1385
  ]
  edge [
    source 32
    target 1386
  ]
  edge [
    source 32
    target 1387
  ]
  edge [
    source 32
    target 1388
  ]
  edge [
    source 32
    target 1389
  ]
  edge [
    source 32
    target 1390
  ]
  edge [
    source 32
    target 1391
  ]
  edge [
    source 32
    target 1392
  ]
  edge [
    source 32
    target 1393
  ]
  edge [
    source 32
    target 1394
  ]
  edge [
    source 32
    target 1395
  ]
  edge [
    source 32
    target 1396
  ]
  edge [
    source 32
    target 1397
  ]
  edge [
    source 32
    target 1398
  ]
  edge [
    source 32
    target 1399
  ]
  edge [
    source 32
    target 620
  ]
  edge [
    source 32
    target 429
  ]
  edge [
    source 32
    target 1400
  ]
  edge [
    source 32
    target 1401
  ]
  edge [
    source 32
    target 1402
  ]
  edge [
    source 32
    target 1403
  ]
  edge [
    source 32
    target 1404
  ]
  edge [
    source 32
    target 50
  ]
  edge [
    source 32
    target 789
  ]
  edge [
    source 32
    target 1405
  ]
  edge [
    source 32
    target 1406
  ]
  edge [
    source 32
    target 1407
  ]
  edge [
    source 32
    target 1408
  ]
  edge [
    source 32
    target 1409
  ]
  edge [
    source 32
    target 1410
  ]
  edge [
    source 32
    target 1411
  ]
  edge [
    source 32
    target 1412
  ]
  edge [
    source 32
    target 1413
  ]
  edge [
    source 32
    target 1414
  ]
  edge [
    source 32
    target 1415
  ]
  edge [
    source 32
    target 1416
  ]
  edge [
    source 32
    target 1417
  ]
  edge [
    source 32
    target 1418
  ]
  edge [
    source 32
    target 1419
  ]
  edge [
    source 32
    target 1420
  ]
  edge [
    source 32
    target 1421
  ]
  edge [
    source 32
    target 1422
  ]
  edge [
    source 32
    target 1423
  ]
  edge [
    source 32
    target 1424
  ]
  edge [
    source 32
    target 1425
  ]
  edge [
    source 32
    target 1426
  ]
  edge [
    source 32
    target 1427
  ]
  edge [
    source 32
    target 1428
  ]
  edge [
    source 32
    target 1429
  ]
  edge [
    source 32
    target 1430
  ]
  edge [
    source 32
    target 1431
  ]
  edge [
    source 32
    target 1432
  ]
  edge [
    source 32
    target 1433
  ]
  edge [
    source 32
    target 1434
  ]
  edge [
    source 32
    target 1435
  ]
  edge [
    source 32
    target 1436
  ]
  edge [
    source 32
    target 1437
  ]
  edge [
    source 32
    target 1438
  ]
  edge [
    source 32
    target 1439
  ]
  edge [
    source 32
    target 1440
  ]
  edge [
    source 32
    target 93
  ]
  edge [
    source 32
    target 1441
  ]
  edge [
    source 32
    target 1442
  ]
  edge [
    source 32
    target 1443
  ]
  edge [
    source 32
    target 1444
  ]
  edge [
    source 32
    target 1445
  ]
  edge [
    source 32
    target 1446
  ]
  edge [
    source 32
    target 1447
  ]
  edge [
    source 32
    target 1448
  ]
  edge [
    source 32
    target 184
  ]
  edge [
    source 32
    target 1449
  ]
  edge [
    source 32
    target 1450
  ]
  edge [
    source 32
    target 1451
  ]
  edge [
    source 32
    target 627
  ]
  edge [
    source 32
    target 1452
  ]
  edge [
    source 32
    target 1453
  ]
  edge [
    source 32
    target 80
  ]
  edge [
    source 32
    target 1454
  ]
  edge [
    source 32
    target 1455
  ]
  edge [
    source 32
    target 1456
  ]
  edge [
    source 32
    target 1457
  ]
  edge [
    source 32
    target 1458
  ]
  edge [
    source 32
    target 1459
  ]
  edge [
    source 32
    target 1460
  ]
  edge [
    source 32
    target 1461
  ]
  edge [
    source 32
    target 1462
  ]
  edge [
    source 32
    target 1463
  ]
  edge [
    source 32
    target 1464
  ]
  edge [
    source 32
    target 1465
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 1466
  ]
  edge [
    source 32
    target 1467
  ]
  edge [
    source 32
    target 1468
  ]
  edge [
    source 32
    target 1469
  ]
  edge [
    source 32
    target 1470
  ]
  edge [
    source 32
    target 1471
  ]
  edge [
    source 32
    target 1472
  ]
  edge [
    source 32
    target 1473
  ]
  edge [
    source 32
    target 1474
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 33
    target 1475
  ]
  edge [
    source 33
    target 1476
  ]
  edge [
    source 33
    target 1477
  ]
  edge [
    source 33
    target 1478
  ]
  edge [
    source 33
    target 44
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1479
  ]
  edge [
    source 34
    target 1480
  ]
  edge [
    source 34
    target 1481
  ]
  edge [
    source 34
    target 1482
  ]
  edge [
    source 34
    target 1483
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 34
    target 1484
  ]
  edge [
    source 34
    target 1485
  ]
  edge [
    source 34
    target 1486
  ]
  edge [
    source 34
    target 1487
  ]
  edge [
    source 34
    target 1488
  ]
  edge [
    source 34
    target 1489
  ]
  edge [
    source 34
    target 1490
  ]
  edge [
    source 34
    target 721
  ]
  edge [
    source 34
    target 1491
  ]
  edge [
    source 34
    target 1492
  ]
  edge [
    source 34
    target 1493
  ]
  edge [
    source 34
    target 1494
  ]
  edge [
    source 34
    target 1495
  ]
  edge [
    source 34
    target 1140
  ]
  edge [
    source 34
    target 1496
  ]
  edge [
    source 34
    target 1497
  ]
  edge [
    source 34
    target 1498
  ]
  edge [
    source 34
    target 492
  ]
  edge [
    source 34
    target 1499
  ]
  edge [
    source 34
    target 1500
  ]
  edge [
    source 34
    target 1501
  ]
  edge [
    source 34
    target 1502
  ]
  edge [
    source 34
    target 1503
  ]
  edge [
    source 34
    target 1504
  ]
  edge [
    source 34
    target 1505
  ]
  edge [
    source 34
    target 1506
  ]
  edge [
    source 34
    target 1507
  ]
  edge [
    source 34
    target 1508
  ]
  edge [
    source 34
    target 1116
  ]
  edge [
    source 34
    target 1509
  ]
  edge [
    source 34
    target 1510
  ]
  edge [
    source 34
    target 1511
  ]
  edge [
    source 34
    target 1471
  ]
  edge [
    source 34
    target 1512
  ]
  edge [
    source 34
    target 1513
  ]
  edge [
    source 34
    target 1514
  ]
  edge [
    source 34
    target 1138
  ]
  edge [
    source 34
    target 1515
  ]
  edge [
    source 34
    target 1516
  ]
  edge [
    source 34
    target 1517
  ]
  edge [
    source 34
    target 1518
  ]
  edge [
    source 34
    target 1519
  ]
  edge [
    source 34
    target 1520
  ]
  edge [
    source 34
    target 1521
  ]
  edge [
    source 34
    target 1522
  ]
  edge [
    source 34
    target 1523
  ]
  edge [
    source 34
    target 1524
  ]
  edge [
    source 34
    target 1525
  ]
  edge [
    source 34
    target 1526
  ]
  edge [
    source 34
    target 1527
  ]
  edge [
    source 34
    target 1528
  ]
  edge [
    source 34
    target 1529
  ]
  edge [
    source 34
    target 1530
  ]
  edge [
    source 34
    target 1531
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 1535
  ]
  edge [
    source 34
    target 1536
  ]
  edge [
    source 34
    target 1537
  ]
  edge [
    source 34
    target 1538
  ]
  edge [
    source 34
    target 1539
  ]
  edge [
    source 34
    target 1540
  ]
  edge [
    source 34
    target 1541
  ]
  edge [
    source 34
    target 1542
  ]
  edge [
    source 34
    target 1543
  ]
  edge [
    source 34
    target 1544
  ]
  edge [
    source 34
    target 1545
  ]
  edge [
    source 34
    target 1546
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1287
  ]
  edge [
    source 35
    target 1282
  ]
  edge [
    source 35
    target 1275
  ]
  edge [
    source 35
    target 1284
  ]
  edge [
    source 35
    target 1431
  ]
  edge [
    source 35
    target 1276
  ]
  edge [
    source 35
    target 1283
  ]
  edge [
    source 35
    target 1289
  ]
  edge [
    source 35
    target 1290
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1547
  ]
  edge [
    source 37
    target 1548
  ]
  edge [
    source 37
    target 1549
  ]
  edge [
    source 37
    target 1550
  ]
  edge [
    source 37
    target 1551
  ]
  edge [
    source 37
    target 1295
  ]
  edge [
    source 37
    target 1552
  ]
  edge [
    source 37
    target 1524
  ]
  edge [
    source 37
    target 1553
  ]
  edge [
    source 37
    target 1554
  ]
  edge [
    source 37
    target 1555
  ]
  edge [
    source 37
    target 1556
  ]
  edge [
    source 37
    target 1557
  ]
  edge [
    source 37
    target 741
  ]
  edge [
    source 37
    target 1558
  ]
  edge [
    source 37
    target 1559
  ]
  edge [
    source 37
    target 1560
  ]
  edge [
    source 37
    target 1561
  ]
  edge [
    source 37
    target 1562
  ]
  edge [
    source 37
    target 1563
  ]
  edge [
    source 37
    target 1564
  ]
  edge [
    source 37
    target 1565
  ]
  edge [
    source 37
    target 1566
  ]
  edge [
    source 37
    target 1567
  ]
  edge [
    source 37
    target 1568
  ]
  edge [
    source 37
    target 1569
  ]
  edge [
    source 37
    target 1570
  ]
  edge [
    source 37
    target 1571
  ]
  edge [
    source 37
    target 1572
  ]
  edge [
    source 37
    target 1573
  ]
  edge [
    source 37
    target 1574
  ]
  edge [
    source 37
    target 1575
  ]
  edge [
    source 37
    target 1576
  ]
  edge [
    source 37
    target 1577
  ]
  edge [
    source 37
    target 1578
  ]
  edge [
    source 37
    target 1579
  ]
  edge [
    source 37
    target 1580
  ]
  edge [
    source 37
    target 1581
  ]
  edge [
    source 37
    target 329
  ]
  edge [
    source 37
    target 1582
  ]
  edge [
    source 37
    target 1583
  ]
  edge [
    source 37
    target 1584
  ]
  edge [
    source 37
    target 1585
  ]
  edge [
    source 37
    target 1586
  ]
  edge [
    source 37
    target 1587
  ]
  edge [
    source 37
    target 1588
  ]
  edge [
    source 37
    target 1589
  ]
  edge [
    source 37
    target 1590
  ]
  edge [
    source 37
    target 1591
  ]
  edge [
    source 37
    target 1592
  ]
  edge [
    source 37
    target 1593
  ]
  edge [
    source 37
    target 1594
  ]
  edge [
    source 37
    target 1595
  ]
  edge [
    source 37
    target 1596
  ]
  edge [
    source 37
    target 333
  ]
  edge [
    source 37
    target 1597
  ]
  edge [
    source 37
    target 1598
  ]
  edge [
    source 38
    target 1599
  ]
  edge [
    source 38
    target 1600
  ]
  edge [
    source 38
    target 1601
  ]
  edge [
    source 38
    target 1250
  ]
  edge [
    source 38
    target 1602
  ]
  edge [
    source 38
    target 1603
  ]
  edge [
    source 38
    target 1604
  ]
  edge [
    source 38
    target 1605
  ]
  edge [
    source 38
    target 1606
  ]
  edge [
    source 38
    target 1607
  ]
  edge [
    source 38
    target 231
  ]
  edge [
    source 38
    target 1608
  ]
  edge [
    source 38
    target 1609
  ]
  edge [
    source 38
    target 1610
  ]
  edge [
    source 38
    target 1611
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 1612
  ]
  edge [
    source 38
    target 1613
  ]
  edge [
    source 38
    target 1614
  ]
  edge [
    source 38
    target 1615
  ]
  edge [
    source 38
    target 1616
  ]
  edge [
    source 38
    target 1617
  ]
  edge [
    source 38
    target 1618
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1508
  ]
  edge [
    source 40
    target 1116
  ]
  edge [
    source 40
    target 1509
  ]
  edge [
    source 40
    target 1480
  ]
  edge [
    source 40
    target 1510
  ]
  edge [
    source 40
    target 1511
  ]
  edge [
    source 40
    target 1471
  ]
  edge [
    source 40
    target 1512
  ]
  edge [
    source 40
    target 1492
  ]
  edge [
    source 40
    target 1513
  ]
  edge [
    source 40
    target 1514
  ]
  edge [
    source 40
    target 1619
  ]
  edge [
    source 40
    target 1130
  ]
  edge [
    source 40
    target 1140
  ]
  edge [
    source 40
    target 1484
  ]
  edge [
    source 40
    target 1496
  ]
  edge [
    source 40
    target 1497
  ]
  edge [
    source 40
    target 1498
  ]
  edge [
    source 40
    target 1620
  ]
  edge [
    source 40
    target 1621
  ]
  edge [
    source 40
    target 1622
  ]
  edge [
    source 40
    target 1623
  ]
  edge [
    source 40
    target 1624
  ]
  edge [
    source 40
    target 1625
  ]
  edge [
    source 40
    target 1626
  ]
  edge [
    source 40
    target 1627
  ]
  edge [
    source 40
    target 1628
  ]
  edge [
    source 40
    target 1348
  ]
  edge [
    source 40
    target 1629
  ]
  edge [
    source 40
    target 1630
  ]
  edge [
    source 40
    target 1631
  ]
  edge [
    source 40
    target 1489
  ]
  edge [
    source 40
    target 228
  ]
  edge [
    source 40
    target 1482
  ]
  edge [
    source 40
    target 1632
  ]
  edge [
    source 40
    target 1633
  ]
  edge [
    source 40
    target 1278
  ]
  edge [
    source 40
    target 1121
  ]
  edge [
    source 40
    target 1126
  ]
  edge [
    source 40
    target 1634
  ]
  edge [
    source 40
    target 1635
  ]
  edge [
    source 40
    target 1636
  ]
  edge [
    source 40
    target 1504
  ]
  edge [
    source 40
    target 1550
  ]
  edge [
    source 40
    target 1637
  ]
  edge [
    source 40
    target 1118
  ]
  edge [
    source 40
    target 1230
  ]
  edge [
    source 40
    target 1638
  ]
  edge [
    source 40
    target 1468
  ]
  edge [
    source 40
    target 1639
  ]
  edge [
    source 40
    target 499
  ]
  edge [
    source 40
    target 1640
  ]
  edge [
    source 40
    target 1641
  ]
  edge [
    source 40
    target 1642
  ]
  edge [
    source 40
    target 1127
  ]
  edge [
    source 40
    target 1643
  ]
  edge [
    source 40
    target 1644
  ]
  edge [
    source 40
    target 1645
  ]
  edge [
    source 40
    target 1646
  ]
  edge [
    source 40
    target 1647
  ]
  edge [
    source 40
    target 1648
  ]
  edge [
    source 40
    target 1649
  ]
  edge [
    source 40
    target 1650
  ]
  edge [
    source 40
    target 1216
  ]
  edge [
    source 40
    target 1651
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 40
    target 1652
  ]
  edge [
    source 40
    target 1653
  ]
  edge [
    source 40
    target 1654
  ]
  edge [
    source 40
    target 1655
  ]
  edge [
    source 40
    target 1656
  ]
  edge [
    source 40
    target 1657
  ]
  edge [
    source 40
    target 1658
  ]
  edge [
    source 40
    target 1659
  ]
  edge [
    source 40
    target 1660
  ]
  edge [
    source 40
    target 1661
  ]
  edge [
    source 40
    target 1662
  ]
  edge [
    source 40
    target 50
  ]
  edge [
    source 40
    target 1663
  ]
  edge [
    source 40
    target 1664
  ]
  edge [
    source 40
    target 1665
  ]
  edge [
    source 40
    target 1666
  ]
  edge [
    source 40
    target 1667
  ]
  edge [
    source 40
    target 419
  ]
  edge [
    source 40
    target 1668
  ]
  edge [
    source 40
    target 1669
  ]
  edge [
    source 40
    target 1670
  ]
  edge [
    source 40
    target 1671
  ]
  edge [
    source 40
    target 1672
  ]
  edge [
    source 40
    target 1673
  ]
  edge [
    source 40
    target 606
  ]
  edge [
    source 40
    target 1674
  ]
  edge [
    source 40
    target 1675
  ]
  edge [
    source 40
    target 1676
  ]
  edge [
    source 40
    target 1677
  ]
  edge [
    source 40
    target 1678
  ]
  edge [
    source 40
    target 1679
  ]
  edge [
    source 40
    target 1680
  ]
  edge [
    source 40
    target 1681
  ]
  edge [
    source 40
    target 1682
  ]
  edge [
    source 40
    target 1683
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1684
  ]
  edge [
    source 41
    target 1685
  ]
  edge [
    source 41
    target 1686
  ]
  edge [
    source 41
    target 1687
  ]
  edge [
    source 41
    target 1688
  ]
  edge [
    source 41
    target 1689
  ]
  edge [
    source 41
    target 414
  ]
  edge [
    source 41
    target 1690
  ]
  edge [
    source 41
    target 484
  ]
  edge [
    source 41
    target 1691
  ]
  edge [
    source 41
    target 1692
  ]
  edge [
    source 41
    target 387
  ]
  edge [
    source 41
    target 1693
  ]
  edge [
    source 41
    target 1694
  ]
  edge [
    source 41
    target 1695
  ]
  edge [
    source 41
    target 1696
  ]
  edge [
    source 41
    target 1239
  ]
  edge [
    source 41
    target 372
  ]
  edge [
    source 41
    target 1238
  ]
  edge [
    source 41
    target 1697
  ]
  edge [
    source 41
    target 1698
  ]
  edge [
    source 41
    target 1699
  ]
  edge [
    source 41
    target 73
  ]
  edge [
    source 41
    target 1700
  ]
  edge [
    source 41
    target 1701
  ]
  edge [
    source 41
    target 223
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 478
  ]
  edge [
    source 41
    target 1703
  ]
  edge [
    source 41
    target 1704
  ]
  edge [
    source 41
    target 1705
  ]
  edge [
    source 41
    target 1706
  ]
  edge [
    source 41
    target 627
  ]
  edge [
    source 41
    target 1707
  ]
  edge [
    source 41
    target 1708
  ]
  edge [
    source 41
    target 1709
  ]
  edge [
    source 41
    target 1710
  ]
  edge [
    source 41
    target 1711
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 1712
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 1445
  ]
  edge [
    source 41
    target 186
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1719
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 386
  ]
  edge [
    source 41
    target 1721
  ]
  edge [
    source 41
    target 1722
  ]
  edge [
    source 41
    target 1723
  ]
  edge [
    source 41
    target 1724
  ]
  edge [
    source 41
    target 1725
  ]
  edge [
    source 41
    target 1726
  ]
  edge [
    source 41
    target 1727
  ]
  edge [
    source 41
    target 1728
  ]
  edge [
    source 41
    target 1556
  ]
  edge [
    source 41
    target 1729
  ]
  edge [
    source 41
    target 1730
  ]
  edge [
    source 42
    target 1731
  ]
  edge [
    source 42
    target 1732
  ]
  edge [
    source 42
    target 1733
  ]
  edge [
    source 42
    target 1734
  ]
  edge [
    source 42
    target 1735
  ]
  edge [
    source 42
    target 1736
  ]
  edge [
    source 42
    target 421
  ]
  edge [
    source 42
    target 1226
  ]
  edge [
    source 42
    target 1737
  ]
  edge [
    source 42
    target 1738
  ]
  edge [
    source 42
    target 1739
  ]
  edge [
    source 42
    target 1339
  ]
  edge [
    source 42
    target 1740
  ]
  edge [
    source 42
    target 1741
  ]
  edge [
    source 42
    target 234
  ]
  edge [
    source 42
    target 1742
  ]
  edge [
    source 42
    target 1743
  ]
  edge [
    source 42
    target 620
  ]
  edge [
    source 42
    target 1744
  ]
  edge [
    source 42
    target 1745
  ]
  edge [
    source 42
    target 99
  ]
  edge [
    source 42
    target 1746
  ]
  edge [
    source 42
    target 1747
  ]
  edge [
    source 42
    target 1748
  ]
  edge [
    source 42
    target 1749
  ]
  edge [
    source 42
    target 120
  ]
  edge [
    source 42
    target 1750
  ]
  edge [
    source 42
    target 1751
  ]
  edge [
    source 42
    target 1752
  ]
  edge [
    source 42
    target 1753
  ]
  edge [
    source 42
    target 1754
  ]
  edge [
    source 42
    target 1755
  ]
  edge [
    source 42
    target 1239
  ]
  edge [
    source 42
    target 1756
  ]
  edge [
    source 42
    target 80
  ]
  edge [
    source 42
    target 1757
  ]
  edge [
    source 42
    target 1586
  ]
  edge [
    source 42
    target 1758
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 847
  ]
  edge [
    source 43
    target 1086
  ]
  edge [
    source 43
    target 1759
  ]
  edge [
    source 43
    target 1760
  ]
  edge [
    source 43
    target 1761
  ]
  edge [
    source 43
    target 846
  ]
  edge [
    source 43
    target 1762
  ]
  edge [
    source 43
    target 1763
  ]
  edge [
    source 43
    target 1764
  ]
  edge [
    source 43
    target 1765
  ]
  edge [
    source 43
    target 1766
  ]
  edge [
    source 43
    target 1767
  ]
  edge [
    source 43
    target 1768
  ]
  edge [
    source 43
    target 1769
  ]
  edge [
    source 43
    target 1770
  ]
  edge [
    source 43
    target 1771
  ]
  edge [
    source 43
    target 1772
  ]
  edge [
    source 43
    target 1773
  ]
  edge [
    source 43
    target 943
  ]
  edge [
    source 43
    target 1774
  ]
  edge [
    source 43
    target 1775
  ]
  edge [
    source 43
    target 1776
  ]
  edge [
    source 43
    target 1777
  ]
  edge [
    source 43
    target 1778
  ]
  edge [
    source 43
    target 872
  ]
  edge [
    source 43
    target 1779
  ]
  edge [
    source 43
    target 1780
  ]
  edge [
    source 43
    target 1781
  ]
  edge [
    source 43
    target 1782
  ]
  edge [
    source 43
    target 1783
  ]
  edge [
    source 43
    target 1784
  ]
  edge [
    source 43
    target 1785
  ]
  edge [
    source 43
    target 1786
  ]
  edge [
    source 43
    target 1787
  ]
  edge [
    source 43
    target 1788
  ]
  edge [
    source 43
    target 1789
  ]
  edge [
    source 43
    target 1790
  ]
  edge [
    source 43
    target 1791
  ]
  edge [
    source 43
    target 1792
  ]
  edge [
    source 43
    target 1793
  ]
  edge [
    source 43
    target 1794
  ]
  edge [
    source 43
    target 1795
  ]
  edge [
    source 43
    target 1796
  ]
  edge [
    source 43
    target 1797
  ]
  edge [
    source 43
    target 1798
  ]
  edge [
    source 43
    target 1799
  ]
  edge [
    source 43
    target 1800
  ]
  edge [
    source 43
    target 1801
  ]
  edge [
    source 43
    target 1802
  ]
  edge [
    source 43
    target 1803
  ]
  edge [
    source 43
    target 1804
  ]
  edge [
    source 43
    target 1805
  ]
  edge [
    source 43
    target 1806
  ]
  edge [
    source 43
    target 1807
  ]
  edge [
    source 43
    target 1808
  ]
  edge [
    source 43
    target 1809
  ]
  edge [
    source 43
    target 1810
  ]
  edge [
    source 43
    target 1811
  ]
  edge [
    source 43
    target 1812
  ]
  edge [
    source 43
    target 1813
  ]
  edge [
    source 43
    target 1814
  ]
  edge [
    source 43
    target 1815
  ]
  edge [
    source 43
    target 1816
  ]
  edge [
    source 43
    target 1817
  ]
  edge [
    source 43
    target 1818
  ]
  edge [
    source 43
    target 1819
  ]
  edge [
    source 43
    target 1820
  ]
  edge [
    source 43
    target 1821
  ]
  edge [
    source 43
    target 1822
  ]
  edge [
    source 43
    target 1823
  ]
  edge [
    source 43
    target 1824
  ]
  edge [
    source 43
    target 1825
  ]
  edge [
    source 43
    target 1826
  ]
  edge [
    source 43
    target 1827
  ]
  edge [
    source 43
    target 1828
  ]
  edge [
    source 43
    target 1829
  ]
  edge [
    source 43
    target 1830
  ]
  edge [
    source 43
    target 1831
  ]
  edge [
    source 43
    target 1832
  ]
  edge [
    source 43
    target 1833
  ]
  edge [
    source 43
    target 1834
  ]
  edge [
    source 43
    target 1835
  ]
  edge [
    source 43
    target 1836
  ]
  edge [
    source 43
    target 268
  ]
  edge [
    source 43
    target 1837
  ]
  edge [
    source 43
    target 1838
  ]
  edge [
    source 43
    target 1839
  ]
  edge [
    source 43
    target 1840
  ]
  edge [
    source 43
    target 1841
  ]
  edge [
    source 43
    target 1842
  ]
  edge [
    source 43
    target 1843
  ]
  edge [
    source 43
    target 1844
  ]
  edge [
    source 43
    target 1845
  ]
  edge [
    source 43
    target 1846
  ]
  edge [
    source 43
    target 1847
  ]
  edge [
    source 43
    target 1848
  ]
  edge [
    source 43
    target 1849
  ]
  edge [
    source 43
    target 1850
  ]
  edge [
    source 43
    target 1851
  ]
  edge [
    source 43
    target 864
  ]
  edge [
    source 43
    target 1852
  ]
  edge [
    source 43
    target 1853
  ]
  edge [
    source 43
    target 1854
  ]
  edge [
    source 43
    target 1855
  ]
  edge [
    source 43
    target 1856
  ]
  edge [
    source 43
    target 1857
  ]
  edge [
    source 43
    target 1021
  ]
  edge [
    source 43
    target 1858
  ]
  edge [
    source 43
    target 1859
  ]
  edge [
    source 43
    target 940
  ]
  edge [
    source 43
    target 1860
  ]
  edge [
    source 43
    target 1861
  ]
  edge [
    source 43
    target 1862
  ]
  edge [
    source 43
    target 1863
  ]
  edge [
    source 43
    target 1864
  ]
  edge [
    source 43
    target 1865
  ]
  edge [
    source 43
    target 1866
  ]
  edge [
    source 43
    target 1867
  ]
  edge [
    source 43
    target 1868
  ]
  edge [
    source 43
    target 1869
  ]
  edge [
    source 43
    target 1198
  ]
  edge [
    source 43
    target 1870
  ]
  edge [
    source 43
    target 1015
  ]
  edge [
    source 43
    target 1871
  ]
  edge [
    source 43
    target 1872
  ]
  edge [
    source 43
    target 1873
  ]
  edge [
    source 43
    target 1874
  ]
  edge [
    source 43
    target 1875
  ]
  edge [
    source 43
    target 1876
  ]
  edge [
    source 43
    target 1193
  ]
  edge [
    source 43
    target 1877
  ]
  edge [
    source 43
    target 1878
  ]
  edge [
    source 43
    target 812
  ]
  edge [
    source 43
    target 913
  ]
  edge [
    source 43
    target 1879
  ]
  edge [
    source 43
    target 1880
  ]
  edge [
    source 43
    target 1881
  ]
  edge [
    source 43
    target 1882
  ]
  edge [
    source 43
    target 1883
  ]
  edge [
    source 43
    target 1884
  ]
  edge [
    source 43
    target 1885
  ]
  edge [
    source 43
    target 1886
  ]
  edge [
    source 43
    target 1887
  ]
  edge [
    source 43
    target 151
  ]
  edge [
    source 43
    target 1888
  ]
  edge [
    source 43
    target 1889
  ]
  edge [
    source 43
    target 1890
  ]
  edge [
    source 43
    target 1891
  ]
  edge [
    source 43
    target 1892
  ]
  edge [
    source 43
    target 1893
  ]
  edge [
    source 43
    target 1894
  ]
  edge [
    source 43
    target 1895
  ]
  edge [
    source 43
    target 816
  ]
  edge [
    source 43
    target 1896
  ]
  edge [
    source 43
    target 1897
  ]
  edge [
    source 43
    target 905
  ]
  edge [
    source 43
    target 1898
  ]
  edge [
    source 43
    target 1899
  ]
  edge [
    source 43
    target 865
  ]
  edge [
    source 43
    target 1900
  ]
  edge [
    source 43
    target 1901
  ]
  edge [
    source 43
    target 1902
  ]
  edge [
    source 43
    target 1903
  ]
  edge [
    source 43
    target 1904
  ]
  edge [
    source 43
    target 1905
  ]
  edge [
    source 43
    target 844
  ]
  edge [
    source 43
    target 1906
  ]
  edge [
    source 43
    target 1907
  ]
  edge [
    source 43
    target 1908
  ]
  edge [
    source 43
    target 1909
  ]
  edge [
    source 43
    target 1910
  ]
  edge [
    source 43
    target 1911
  ]
  edge [
    source 43
    target 1912
  ]
  edge [
    source 43
    target 888
  ]
  edge [
    source 43
    target 1913
  ]
  edge [
    source 43
    target 1914
  ]
  edge [
    source 43
    target 1915
  ]
  edge [
    source 43
    target 1916
  ]
  edge [
    source 43
    target 1917
  ]
  edge [
    source 43
    target 1918
  ]
  edge [
    source 43
    target 1919
  ]
  edge [
    source 43
    target 1920
  ]
  edge [
    source 43
    target 1921
  ]
  edge [
    source 43
    target 1922
  ]
  edge [
    source 43
    target 939
  ]
  edge [
    source 43
    target 1923
  ]
  edge [
    source 43
    target 1924
  ]
  edge [
    source 43
    target 1925
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1926
  ]
  edge [
    source 44
    target 1927
  ]
  edge [
    source 44
    target 1928
  ]
  edge [
    source 44
    target 1929
  ]
  edge [
    source 44
    target 1930
  ]
  edge [
    source 44
    target 1931
  ]
  edge [
    source 44
    target 1932
  ]
  edge [
    source 44
    target 496
  ]
  edge [
    source 44
    target 1933
  ]
  edge [
    source 44
    target 1934
  ]
  edge [
    source 44
    target 1935
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1936
  ]
  edge [
    source 45
    target 1937
  ]
  edge [
    source 45
    target 1938
  ]
  edge [
    source 45
    target 1939
  ]
  edge [
    source 45
    target 1940
  ]
  edge [
    source 45
    target 1941
  ]
  edge [
    source 45
    target 1942
  ]
  edge [
    source 45
    target 1943
  ]
  edge [
    source 45
    target 1944
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1685
  ]
  edge [
    source 47
    target 85
  ]
  edge [
    source 47
    target 478
  ]
  edge [
    source 47
    target 1945
  ]
  edge [
    source 47
    target 1946
  ]
  edge [
    source 47
    target 1947
  ]
  edge [
    source 47
    target 1948
  ]
  edge [
    source 47
    target 1239
  ]
  edge [
    source 47
    target 1949
  ]
  edge [
    source 47
    target 1950
  ]
  edge [
    source 47
    target 1951
  ]
  edge [
    source 47
    target 223
  ]
  edge [
    source 47
    target 1952
  ]
  edge [
    source 47
    target 1953
  ]
  edge [
    source 47
    target 268
  ]
  edge [
    source 47
    target 1954
  ]
  edge [
    source 47
    target 1955
  ]
  edge [
    source 47
    target 1956
  ]
  edge [
    source 47
    target 1957
  ]
  edge [
    source 47
    target 387
  ]
  edge [
    source 47
    target 1123
  ]
  edge [
    source 47
    target 1465
  ]
  edge [
    source 47
    target 1958
  ]
  edge [
    source 47
    target 1959
  ]
  edge [
    source 47
    target 1960
  ]
  edge [
    source 47
    target 1961
  ]
  edge [
    source 47
    target 795
  ]
  edge [
    source 47
    target 228
  ]
  edge [
    source 47
    target 1962
  ]
  edge [
    source 47
    target 1963
  ]
  edge [
    source 47
    target 1964
  ]
  edge [
    source 47
    target 1965
  ]
  edge [
    source 47
    target 1966
  ]
  edge [
    source 47
    target 1967
  ]
  edge [
    source 47
    target 1968
  ]
  edge [
    source 47
    target 1969
  ]
  edge [
    source 47
    target 1970
  ]
  edge [
    source 47
    target 219
  ]
  edge [
    source 47
    target 1971
  ]
  edge [
    source 47
    target 720
  ]
  edge [
    source 47
    target 227
  ]
  edge [
    source 47
    target 1972
  ]
  edge [
    source 47
    target 230
  ]
  edge [
    source 47
    target 1973
  ]
  edge [
    source 47
    target 1974
  ]
  edge [
    source 47
    target 199
  ]
  edge [
    source 47
    target 218
  ]
  edge [
    source 47
    target 234
  ]
  edge [
    source 47
    target 1975
  ]
  edge [
    source 47
    target 1976
  ]
  edge [
    source 47
    target 1977
  ]
  edge [
    source 47
    target 1978
  ]
  edge [
    source 47
    target 1979
  ]
  edge [
    source 47
    target 1980
  ]
  edge [
    source 47
    target 1981
  ]
  edge [
    source 47
    target 1982
  ]
  edge [
    source 47
    target 1983
  ]
  edge [
    source 47
    target 231
  ]
  edge [
    source 47
    target 1984
  ]
  edge [
    source 47
    target 217
  ]
  edge [
    source 47
    target 222
  ]
  edge [
    source 47
    target 1985
  ]
  edge [
    source 47
    target 1986
  ]
  edge [
    source 47
    target 1987
  ]
  edge [
    source 47
    target 1001
  ]
  edge [
    source 47
    target 1988
  ]
  edge [
    source 47
    target 220
  ]
  edge [
    source 47
    target 221
  ]
  edge [
    source 47
    target 224
  ]
  edge [
    source 47
    target 225
  ]
  edge [
    source 47
    target 1989
  ]
  edge [
    source 47
    target 226
  ]
  edge [
    source 47
    target 738
  ]
  edge [
    source 47
    target 1990
  ]
  edge [
    source 47
    target 73
  ]
  edge [
    source 47
    target 1700
  ]
  edge [
    source 47
    target 1701
  ]
  edge [
    source 47
    target 1702
  ]
  edge [
    source 47
    target 1703
  ]
  edge [
    source 47
    target 1704
  ]
  edge [
    source 47
    target 1705
  ]
  edge [
    source 47
    target 1706
  ]
  edge [
    source 47
    target 627
  ]
  edge [
    source 47
    target 1707
  ]
  edge [
    source 47
    target 1708
  ]
  edge [
    source 47
    target 1709
  ]
  edge [
    source 47
    target 1710
  ]
  edge [
    source 47
    target 1711
  ]
  edge [
    source 47
    target 1712
  ]
  edge [
    source 47
    target 1713
  ]
  edge [
    source 47
    target 1445
  ]
  edge [
    source 47
    target 186
  ]
  edge [
    source 47
    target 1714
  ]
  edge [
    source 47
    target 1715
  ]
  edge [
    source 47
    target 1716
  ]
  edge [
    source 47
    target 1717
  ]
  edge [
    source 47
    target 1718
  ]
  edge [
    source 47
    target 1719
  ]
  edge [
    source 47
    target 1991
  ]
  edge [
    source 47
    target 1992
  ]
  edge [
    source 47
    target 1993
  ]
  edge [
    source 47
    target 1994
  ]
  edge [
    source 47
    target 1995
  ]
  edge [
    source 47
    target 1996
  ]
  edge [
    source 47
    target 1997
  ]
  edge [
    source 47
    target 1998
  ]
  edge [
    source 47
    target 1999
  ]
  edge [
    source 47
    target 2000
  ]
  edge [
    source 47
    target 2001
  ]
  edge [
    source 47
    target 140
  ]
  edge [
    source 47
    target 2002
  ]
  edge [
    source 47
    target 2003
  ]
  edge [
    source 47
    target 2004
  ]
  edge [
    source 47
    target 2005
  ]
  edge [
    source 47
    target 2006
  ]
  edge [
    source 47
    target 2007
  ]
  edge [
    source 47
    target 621
  ]
  edge [
    source 47
    target 2008
  ]
  edge [
    source 47
    target 2009
  ]
  edge [
    source 47
    target 1586
  ]
  edge [
    source 47
    target 243
  ]
  edge [
    source 47
    target 2010
  ]
  edge [
    source 47
    target 1423
  ]
  edge [
    source 47
    target 2011
  ]
  edge [
    source 47
    target 2012
  ]
  edge [
    source 47
    target 2013
  ]
  edge [
    source 47
    target 62
  ]
  edge [
    source 47
    target 2014
  ]
  edge [
    source 47
    target 2015
  ]
  edge [
    source 47
    target 2016
  ]
  edge [
    source 47
    target 2017
  ]
  edge [
    source 47
    target 2018
  ]
  edge [
    source 47
    target 2019
  ]
  edge [
    source 47
    target 2020
  ]
  edge [
    source 47
    target 247
  ]
  edge [
    source 47
    target 2021
  ]
  edge [
    source 47
    target 2022
  ]
  edge [
    source 47
    target 2023
  ]
  edge [
    source 47
    target 267
  ]
  edge [
    source 47
    target 2024
  ]
  edge [
    source 47
    target 2025
  ]
  edge [
    source 47
    target 2026
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 648
  ]
  edge [
    source 49
    target 268
  ]
  edge [
    source 49
    target 2027
  ]
  edge [
    source 49
    target 1065
  ]
  edge [
    source 49
    target 2028
  ]
  edge [
    source 49
    target 2029
  ]
  edge [
    source 49
    target 2030
  ]
  edge [
    source 49
    target 2031
  ]
  edge [
    source 49
    target 685
  ]
  edge [
    source 49
    target 2032
  ]
  edge [
    source 49
    target 527
  ]
  edge [
    source 49
    target 2033
  ]
  edge [
    source 49
    target 673
  ]
  edge [
    source 49
    target 663
  ]
  edge [
    source 49
    target 674
  ]
  edge [
    source 49
    target 675
  ]
  edge [
    source 49
    target 676
  ]
  edge [
    source 49
    target 1063
  ]
  edge [
    source 49
    target 1071
  ]
  edge [
    source 49
    target 1074
  ]
  edge [
    source 49
    target 2034
  ]
  edge [
    source 49
    target 2035
  ]
  edge [
    source 49
    target 2036
  ]
  edge [
    source 49
    target 2037
  ]
  edge [
    source 49
    target 2038
  ]
  edge [
    source 49
    target 686
  ]
  edge [
    source 49
    target 687
  ]
  edge [
    source 49
    target 688
  ]
  edge [
    source 49
    target 689
  ]
  edge [
    source 49
    target 690
  ]
  edge [
    source 49
    target 91
  ]
  edge [
    source 49
    target 691
  ]
  edge [
    source 49
    target 692
  ]
  edge [
    source 49
    target 693
  ]
  edge [
    source 49
    target 694
  ]
  edge [
    source 49
    target 457
  ]
  edge [
    source 49
    target 695
  ]
  edge [
    source 49
    target 696
  ]
  edge [
    source 49
    target 697
  ]
  edge [
    source 49
    target 698
  ]
  edge [
    source 49
    target 699
  ]
  edge [
    source 49
    target 700
  ]
  edge [
    source 49
    target 701
  ]
  edge [
    source 49
    target 661
  ]
  edge [
    source 49
    target 702
  ]
  edge [
    source 49
    target 703
  ]
  edge [
    source 49
    target 704
  ]
  edge [
    source 49
    target 705
  ]
  edge [
    source 49
    target 706
  ]
  edge [
    source 49
    target 707
  ]
  edge [
    source 49
    target 2039
  ]
  edge [
    source 49
    target 2040
  ]
  edge [
    source 49
    target 2041
  ]
  edge [
    source 49
    target 2042
  ]
  edge [
    source 49
    target 849
  ]
  edge [
    source 49
    target 2043
  ]
  edge [
    source 49
    target 2044
  ]
  edge [
    source 49
    target 2045
  ]
  edge [
    source 49
    target 2046
  ]
  edge [
    source 49
    target 2047
  ]
  edge [
    source 49
    target 654
  ]
  edge [
    source 49
    target 2048
  ]
  edge [
    source 49
    target 2049
  ]
  edge [
    source 49
    target 2050
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 2051
  ]
  edge [
    source 50
    target 2052
  ]
  edge [
    source 50
    target 2053
  ]
  edge [
    source 50
    target 2054
  ]
  edge [
    source 50
    target 2055
  ]
  edge [
    source 50
    target 2056
  ]
  edge [
    source 50
    target 2057
  ]
  edge [
    source 50
    target 1354
  ]
  edge [
    source 50
    target 2058
  ]
  edge [
    source 50
    target 228
  ]
  edge [
    source 50
    target 576
  ]
  edge [
    source 50
    target 465
  ]
  edge [
    source 50
    target 1330
  ]
  edge [
    source 50
    target 484
  ]
  edge [
    source 50
    target 485
  ]
  edge [
    source 50
    target 486
  ]
  edge [
    source 50
    target 487
  ]
  edge [
    source 50
    target 488
  ]
  edge [
    source 50
    target 489
  ]
  edge [
    source 50
    target 409
  ]
  edge [
    source 50
    target 2059
  ]
  edge [
    source 50
    target 235
  ]
  edge [
    source 50
    target 1677
  ]
  edge [
    source 50
    target 495
  ]
  edge [
    source 50
    target 2060
  ]
  edge [
    source 50
    target 184
  ]
  edge [
    source 50
    target 186
  ]
  edge [
    source 50
    target 990
  ]
  edge [
    source 50
    target 1471
  ]
  edge [
    source 50
    target 2061
  ]
  edge [
    source 50
    target 1339
  ]
  edge [
    source 50
    target 2062
  ]
  edge [
    source 50
    target 2063
  ]
  edge [
    source 50
    target 2064
  ]
  edge [
    source 50
    target 2065
  ]
  edge [
    source 50
    target 467
  ]
  edge [
    source 50
    target 469
  ]
  edge [
    source 50
    target 468
  ]
  edge [
    source 50
    target 470
  ]
  edge [
    source 50
    target 471
  ]
  edge [
    source 50
    target 472
  ]
  edge [
    source 50
    target 473
  ]
  edge [
    source 50
    target 474
  ]
  edge [
    source 50
    target 475
  ]
  edge [
    source 50
    target 476
  ]
  edge [
    source 50
    target 477
  ]
  edge [
    source 50
    target 478
  ]
  edge [
    source 50
    target 479
  ]
  edge [
    source 50
    target 392
  ]
  edge [
    source 50
    target 480
  ]
  edge [
    source 50
    target 99
  ]
  edge [
    source 50
    target 385
  ]
  edge [
    source 50
    target 481
  ]
  edge [
    source 50
    target 482
  ]
  edge [
    source 50
    target 318
  ]
  edge [
    source 50
    target 247
  ]
  edge [
    source 50
    target 483
  ]
  edge [
    source 50
    target 2066
  ]
  edge [
    source 50
    target 2067
  ]
  edge [
    source 50
    target 419
  ]
  edge [
    source 50
    target 1344
  ]
  edge [
    source 50
    target 234
  ]
  edge [
    source 50
    target 2068
  ]
  edge [
    source 50
    target 2069
  ]
  edge [
    source 50
    target 2070
  ]
  edge [
    source 50
    target 2071
  ]
  edge [
    source 50
    target 2072
  ]
  edge [
    source 50
    target 2073
  ]
  edge [
    source 50
    target 2074
  ]
  edge [
    source 50
    target 2075
  ]
  edge [
    source 50
    target 2076
  ]
  edge [
    source 50
    target 2077
  ]
  edge [
    source 50
    target 1961
  ]
  edge [
    source 50
    target 2078
  ]
  edge [
    source 50
    target 2079
  ]
  edge [
    source 50
    target 2080
  ]
  edge [
    source 50
    target 2081
  ]
  edge [
    source 50
    target 126
  ]
  edge [
    source 50
    target 62
  ]
  edge [
    source 50
    target 2082
  ]
  edge [
    source 50
    target 1243
  ]
  edge [
    source 50
    target 2083
  ]
  edge [
    source 50
    target 75
  ]
  edge [
    source 50
    target 2084
  ]
  edge [
    source 50
    target 1719
  ]
  edge [
    source 50
    target 2085
  ]
  edge [
    source 50
    target 198
  ]
  edge [
    source 50
    target 578
  ]
  edge [
    source 50
    target 2086
  ]
  edge [
    source 50
    target 2087
  ]
  edge [
    source 50
    target 73
  ]
  edge [
    source 50
    target 570
  ]
  edge [
    source 50
    target 2088
  ]
  edge [
    source 50
    target 2089
  ]
  edge [
    source 50
    target 572
  ]
  edge [
    source 50
    target 413
  ]
  edge [
    source 50
    target 2090
  ]
  edge [
    source 50
    target 2091
  ]
  edge [
    source 50
    target 2092
  ]
  edge [
    source 50
    target 571
  ]
  edge [
    source 50
    target 2093
  ]
  edge [
    source 50
    target 2094
  ]
  edge [
    source 50
    target 2095
  ]
  edge [
    source 50
    target 2096
  ]
  edge [
    source 50
    target 2097
  ]
  edge [
    source 50
    target 527
  ]
  edge [
    source 50
    target 2098
  ]
  edge [
    source 50
    target 2099
  ]
  edge [
    source 50
    target 2100
  ]
  edge [
    source 50
    target 2101
  ]
  edge [
    source 50
    target 2102
  ]
  edge [
    source 50
    target 2103
  ]
  edge [
    source 50
    target 2104
  ]
  edge [
    source 50
    target 2105
  ]
  edge [
    source 50
    target 2106
  ]
  edge [
    source 50
    target 2107
  ]
  edge [
    source 50
    target 2108
  ]
  edge [
    source 50
    target 577
  ]
  edge [
    source 50
    target 2109
  ]
  edge [
    source 50
    target 564
  ]
  edge [
    source 50
    target 2110
  ]
  edge [
    source 50
    target 2111
  ]
  edge [
    source 50
    target 2112
  ]
  edge [
    source 50
    target 2113
  ]
  edge [
    source 50
    target 2114
  ]
  edge [
    source 50
    target 2115
  ]
  edge [
    source 50
    target 2116
  ]
  edge [
    source 50
    target 2117
  ]
  edge [
    source 50
    target 429
  ]
  edge [
    source 50
    target 2118
  ]
  edge [
    source 50
    target 2119
  ]
  edge [
    source 50
    target 2120
  ]
  edge [
    source 50
    target 2121
  ]
  edge [
    source 50
    target 2122
  ]
  edge [
    source 50
    target 2123
  ]
  edge [
    source 50
    target 2124
  ]
  edge [
    source 50
    target 2125
  ]
  edge [
    source 50
    target 2126
  ]
  edge [
    source 50
    target 510
  ]
  edge [
    source 50
    target 2127
  ]
  edge [
    source 50
    target 2128
  ]
  edge [
    source 50
    target 2129
  ]
  edge [
    source 50
    target 2130
  ]
  edge [
    source 50
    target 2131
  ]
  edge [
    source 50
    target 2132
  ]
  edge [
    source 50
    target 2133
  ]
  edge [
    source 50
    target 2134
  ]
  edge [
    source 50
    target 2135
  ]
  edge [
    source 50
    target 1320
  ]
  edge [
    source 50
    target 2136
  ]
  edge [
    source 50
    target 1334
  ]
  edge [
    source 50
    target 1714
  ]
  edge [
    source 50
    target 2137
  ]
  edge [
    source 50
    target 103
  ]
  edge [
    source 50
    target 2138
  ]
  edge [
    source 50
    target 2139
  ]
  edge [
    source 50
    target 2140
  ]
  edge [
    source 50
    target 1604
  ]
  edge [
    source 50
    target 2141
  ]
  edge [
    source 50
    target 2142
  ]
  edge [
    source 50
    target 1300
  ]
  edge [
    source 50
    target 1423
  ]
  edge [
    source 50
    target 262
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 93
  ]
  edge [
    source 51
    target 2143
  ]
  edge [
    source 51
    target 1432
  ]
  edge [
    source 51
    target 2144
  ]
  edge [
    source 51
    target 388
  ]
  edge [
    source 51
    target 73
  ]
  edge [
    source 51
    target 1279
  ]
  edge [
    source 51
    target 252
  ]
  edge [
    source 51
    target 123
  ]
  edge [
    source 51
    target 2145
  ]
  edge [
    source 51
    target 2146
  ]
  edge [
    source 51
    target 1303
  ]
  edge [
    source 51
    target 2147
  ]
  edge [
    source 51
    target 234
  ]
  edge [
    source 51
    target 1434
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 2148
  ]
  edge [
    source 51
    target 2149
  ]
  edge [
    source 51
    target 2150
  ]
  edge [
    source 51
    target 2151
  ]
  edge [
    source 51
    target 243
  ]
  edge [
    source 51
    target 2152
  ]
  edge [
    source 51
    target 2153
  ]
  edge [
    source 51
    target 2154
  ]
  edge [
    source 51
    target 2155
  ]
  edge [
    source 51
    target 235
  ]
  edge [
    source 51
    target 2156
  ]
  edge [
    source 51
    target 1442
  ]
  edge [
    source 51
    target 2157
  ]
  edge [
    source 51
    target 2158
  ]
  edge [
    source 51
    target 631
  ]
  edge [
    source 51
    target 1445
  ]
  edge [
    source 51
    target 121
  ]
  edge [
    source 51
    target 2159
  ]
  edge [
    source 51
    target 2160
  ]
  edge [
    source 51
    target 2161
  ]
  edge [
    source 51
    target 2162
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 2163
  ]
  edge [
    source 52
    target 2164
  ]
  edge [
    source 52
    target 2165
  ]
  edge [
    source 52
    target 1646
  ]
  edge [
    source 52
    target 2166
  ]
  edge [
    source 52
    target 2167
  ]
  edge [
    source 52
    target 2168
  ]
  edge [
    source 52
    target 1523
  ]
  edge [
    source 52
    target 1135
  ]
  edge [
    source 52
    target 2169
  ]
  edge [
    source 52
    target 2170
  ]
  edge [
    source 52
    target 1536
  ]
  edge [
    source 52
    target 1496
  ]
  edge [
    source 52
    target 809
  ]
  edge [
    source 52
    target 1506
  ]
  edge [
    source 52
    target 2171
  ]
  edge [
    source 52
    target 2172
  ]
  edge [
    source 52
    target 1490
  ]
  edge [
    source 52
    target 2173
  ]
  edge [
    source 52
    target 1584
  ]
  edge [
    source 52
    target 2174
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 2175
  ]
  edge [
    source 54
    target 639
  ]
  edge [
    source 54
    target 753
  ]
  edge [
    source 54
    target 2176
  ]
  edge [
    source 54
    target 1052
  ]
  edge [
    source 54
    target 2177
  ]
  edge [
    source 54
    target 2178
  ]
  edge [
    source 54
    target 2179
  ]
  edge [
    source 54
    target 2180
  ]
  edge [
    source 54
    target 2181
  ]
  edge [
    source 54
    target 2182
  ]
  edge [
    source 54
    target 2183
  ]
  edge [
    source 54
    target 2184
  ]
  edge [
    source 54
    target 739
  ]
  edge [
    source 54
    target 740
  ]
  edge [
    source 54
    target 741
  ]
  edge [
    source 54
    target 742
  ]
  edge [
    source 54
    target 743
  ]
  edge [
    source 54
    target 744
  ]
  edge [
    source 54
    target 745
  ]
  edge [
    source 54
    target 746
  ]
  edge [
    source 54
    target 747
  ]
  edge [
    source 54
    target 749
  ]
  edge [
    source 54
    target 748
  ]
  edge [
    source 54
    target 750
  ]
  edge [
    source 54
    target 751
  ]
  edge [
    source 54
    target 752
  ]
  edge [
    source 54
    target 229
  ]
  edge [
    source 54
    target 414
  ]
  edge [
    source 54
    target 2185
  ]
  edge [
    source 54
    target 1214
  ]
  edge [
    source 54
    target 2125
  ]
  edge [
    source 54
    target 2186
  ]
  edge [
    source 54
    target 2187
  ]
  edge [
    source 54
    target 792
  ]
  edge [
    source 54
    target 2188
  ]
  edge [
    source 54
    target 2189
  ]
  edge [
    source 54
    target 2118
  ]
  edge [
    source 54
    target 2190
  ]
  edge [
    source 54
    target 2191
  ]
  edge [
    source 54
    target 2192
  ]
  edge [
    source 54
    target 80
  ]
  edge [
    source 54
    target 759
  ]
  edge [
    source 54
    target 2193
  ]
  edge [
    source 54
    target 2194
  ]
  edge [
    source 54
    target 2195
  ]
  edge [
    source 54
    target 2196
  ]
  edge [
    source 54
    target 2197
  ]
  edge [
    source 54
    target 2198
  ]
  edge [
    source 54
    target 2199
  ]
  edge [
    source 54
    target 2200
  ]
  edge [
    source 54
    target 2091
  ]
  edge [
    source 54
    target 2201
  ]
  edge [
    source 54
    target 2202
  ]
  edge [
    source 54
    target 2203
  ]
  edge [
    source 54
    target 2204
  ]
  edge [
    source 54
    target 2205
  ]
  edge [
    source 54
    target 2206
  ]
  edge [
    source 54
    target 2207
  ]
  edge [
    source 54
    target 2208
  ]
  edge [
    source 54
    target 495
  ]
  edge [
    source 54
    target 2209
  ]
  edge [
    source 54
    target 1981
  ]
  edge [
    source 54
    target 243
  ]
  edge [
    source 54
    target 2210
  ]
  edge [
    source 54
    target 2211
  ]
  edge [
    source 54
    target 2212
  ]
  edge [
    source 54
    target 2213
  ]
  edge [
    source 54
    target 2214
  ]
  edge [
    source 54
    target 2215
  ]
  edge [
    source 54
    target 735
  ]
  edge [
    source 54
    target 2216
  ]
  edge [
    source 54
    target 2217
  ]
  edge [
    source 54
    target 2218
  ]
  edge [
    source 54
    target 2219
  ]
  edge [
    source 54
    target 2220
  ]
  edge [
    source 54
    target 2221
  ]
  edge [
    source 54
    target 2222
  ]
  edge [
    source 54
    target 2223
  ]
  edge [
    source 54
    target 2224
  ]
  edge [
    source 54
    target 2225
  ]
  edge [
    source 54
    target 2226
  ]
  edge [
    source 54
    target 613
  ]
  edge [
    source 54
    target 2227
  ]
  edge [
    source 54
    target 2228
  ]
  edge [
    source 54
    target 2229
  ]
  edge [
    source 54
    target 2230
  ]
  edge [
    source 54
    target 2231
  ]
  edge [
    source 54
    target 2232
  ]
  edge [
    source 54
    target 2233
  ]
  edge [
    source 54
    target 2234
  ]
  edge [
    source 54
    target 578
  ]
  edge [
    source 54
    target 575
  ]
  edge [
    source 54
    target 2235
  ]
  edge [
    source 54
    target 2236
  ]
  edge [
    source 54
    target 2237
  ]
  edge [
    source 54
    target 2238
  ]
  edge [
    source 54
    target 2239
  ]
  edge [
    source 54
    target 2240
  ]
  edge [
    source 54
    target 2241
  ]
  edge [
    source 54
    target 247
  ]
  edge [
    source 54
    target 2242
  ]
  edge [
    source 54
    target 2243
  ]
  edge [
    source 54
    target 2244
  ]
  edge [
    source 54
    target 2245
  ]
  edge [
    source 54
    target 2246
  ]
  edge [
    source 54
    target 2247
  ]
  edge [
    source 54
    target 2248
  ]
  edge [
    source 54
    target 2249
  ]
  edge [
    source 54
    target 2250
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 284
    target 2260
  ]
  edge [
    source 1702
    target 2256
  ]
  edge [
    source 1702
    target 2257
  ]
  edge [
    source 2252
    target 2253
  ]
  edge [
    source 2254
    target 2255
  ]
  edge [
    source 2256
    target 2257
  ]
  edge [
    source 2258
    target 2259
  ]
  edge [
    source 2261
    target 2262
  ]
  edge [
    source 2261
    target 2263
  ]
  edge [
    source 2262
    target 2263
  ]
  edge [
    source 2264
    target 2265
  ]
  edge [
    source 2264
    target 2266
  ]
  edge [
    source 2265
    target 2266
  ]
  edge [
    source 2267
    target 2268
  ]
  edge [
    source 2269
    target 2270
  ]
]
