graph [
  node [
    id 0
    label "ludwik"
    origin "text"
  ]
  node [
    id 1
    label "antoni"
    origin "text"
  ]
  node [
    id 2
    label "paszkiewicz"
    origin "text"
  ]
  node [
    id 3
    label "moneta"
  ]
  node [
    id 4
    label "antyk"
  ]
  node [
    id 5
    label "staro&#380;ytno&#347;&#263;"
  ]
  node [
    id 6
    label "styl_dorycki"
  ]
  node [
    id 7
    label "styl_koryncki"
  ]
  node [
    id 8
    label "aretalogia"
  ]
  node [
    id 9
    label "okres_klasyczny"
  ]
  node [
    id 10
    label "epoka"
  ]
  node [
    id 11
    label "styl_kompozytowy"
  ]
  node [
    id 12
    label "staro&#263;"
  ]
  node [
    id 13
    label "klasycyzm"
  ]
  node [
    id 14
    label "styl_jo&#324;ski"
  ]
  node [
    id 15
    label "awers"
  ]
  node [
    id 16
    label "legenda"
  ]
  node [
    id 17
    label "liga"
  ]
  node [
    id 18
    label "rewers"
  ]
  node [
    id 19
    label "egzerga"
  ]
  node [
    id 20
    label "pieni&#261;dz"
  ]
  node [
    id 21
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 22
    label "otok"
  ]
  node [
    id 23
    label "balansjerka"
  ]
  node [
    id 24
    label "Antoni"
  ]
  node [
    id 25
    label "Paszkiewicz"
  ]
  node [
    id 26
    label "akademia"
  ]
  node [
    id 27
    label "medyczny"
  ]
  node [
    id 28
    label "pa&#324;stwowy"
  ]
  node [
    id 29
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 30
    label "nauka"
  ]
  node [
    id 31
    label "polskie"
  ]
  node [
    id 32
    label "towarzystwo"
  ]
  node [
    id 33
    label "anatomopatologiczny"
  ]
  node [
    id 34
    label "zak&#322;ad"
  ]
  node [
    id 35
    label "patologia"
  ]
  node [
    id 36
    label "do&#347;wiadczalny"
  ]
  node [
    id 37
    label "tygodnik"
  ]
  node [
    id 38
    label "lekarski"
  ]
  node [
    id 39
    label "anatomia"
  ]
  node [
    id 40
    label "patologiczny"
  ]
  node [
    id 41
    label "wyspa"
  ]
  node [
    id 42
    label "sprawa"
  ]
  node [
    id 43
    label "do&#347;wiadczy&#263;"
  ]
  node [
    id 44
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 45
    label "nowotw&#243;r"
  ]
  node [
    id 46
    label "przez"
  ]
  node [
    id 47
    label "dra&#380;ni&#263;"
  ]
  node [
    id 48
    label "smo&#322;a"
  ]
  node [
    id 49
    label "pogazowy"
  ]
  node [
    id 50
    label "ojciec"
  ]
  node [
    id 51
    label "utrwala&#263;"
  ]
  node [
    id 52
    label "i"
  ]
  node [
    id 53
    label "przechowywa&#263;"
  ]
  node [
    id 54
    label "preparat"
  ]
  node [
    id 55
    label "anatomiczny"
  ]
  node [
    id 56
    label "zeszyt"
  ]
  node [
    id 57
    label "utrzyma&#263;"
  ]
  node [
    id 58
    label "on"
  ]
  node [
    id 59
    label "barwa"
  ]
  node [
    id 60
    label "naturalny"
  ]
  node [
    id 61
    label "technika"
  ]
  node [
    id 62
    label "sekcja"
  ]
  node [
    id 63
    label "zw&#322;oki"
  ]
  node [
    id 64
    label "297"
  ]
  node [
    id 65
    label "rysunki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 43
    target 48
  ]
  edge [
    source 43
    target 49
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 49
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 50
    target 56
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 58
  ]
  edge [
    source 50
    target 59
  ]
  edge [
    source 50
    target 60
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 51
    target 58
  ]
  edge [
    source 51
    target 59
  ]
  edge [
    source 51
    target 60
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 52
    target 58
  ]
  edge [
    source 52
    target 59
  ]
  edge [
    source 52
    target 60
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 53
    target 59
  ]
  edge [
    source 53
    target 60
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 59
  ]
  edge [
    source 54
    target 60
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 55
    target 59
  ]
  edge [
    source 55
    target 60
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 56
    target 61
  ]
  edge [
    source 56
    target 62
  ]
  edge [
    source 56
    target 63
  ]
  edge [
    source 56
    target 64
  ]
  edge [
    source 56
    target 65
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 61
    target 64
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 64
    target 65
  ]
]
