graph [
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "urz&#261;d"
    origin "text"
  ]
  node [
    id 3
    label "ochrona"
    origin "text"
  ]
  node [
    id 4
    label "konkurencja"
    origin "text"
  ]
  node [
    id 5
    label "konsument"
    origin "text"
  ]
  node [
    id 6
    label "marco"
    origin "text"
  ]
  node [
    id 7
    label "poz"
    origin "text"
  ]
  node [
    id 8
    label "program_informacyjny"
  ]
  node [
    id 9
    label "journal"
  ]
  node [
    id 10
    label "diariusz"
  ]
  node [
    id 11
    label "spis"
  ]
  node [
    id 12
    label "ksi&#281;ga"
  ]
  node [
    id 13
    label "sheet"
  ]
  node [
    id 14
    label "pami&#281;tnik"
  ]
  node [
    id 15
    label "gazeta"
  ]
  node [
    id 16
    label "tytu&#322;"
  ]
  node [
    id 17
    label "redakcja"
  ]
  node [
    id 18
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 19
    label "czasopismo"
  ]
  node [
    id 20
    label "prasa"
  ]
  node [
    id 21
    label "rozdzia&#322;"
  ]
  node [
    id 22
    label "pismo"
  ]
  node [
    id 23
    label "Ewangelia"
  ]
  node [
    id 24
    label "book"
  ]
  node [
    id 25
    label "dokument"
  ]
  node [
    id 26
    label "tome"
  ]
  node [
    id 27
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 28
    label "pami&#261;tka"
  ]
  node [
    id 29
    label "notes"
  ]
  node [
    id 30
    label "zapiski"
  ]
  node [
    id 31
    label "raptularz"
  ]
  node [
    id 32
    label "album"
  ]
  node [
    id 33
    label "utw&#243;r_epicki"
  ]
  node [
    id 34
    label "zbi&#243;r"
  ]
  node [
    id 35
    label "catalog"
  ]
  node [
    id 36
    label "pozycja"
  ]
  node [
    id 37
    label "akt"
  ]
  node [
    id 38
    label "tekst"
  ]
  node [
    id 39
    label "sumariusz"
  ]
  node [
    id 40
    label "stock"
  ]
  node [
    id 41
    label "figurowa&#263;"
  ]
  node [
    id 42
    label "czynno&#347;&#263;"
  ]
  node [
    id 43
    label "wyliczanka"
  ]
  node [
    id 44
    label "oficjalny"
  ]
  node [
    id 45
    label "urz&#281;dowo"
  ]
  node [
    id 46
    label "formalny"
  ]
  node [
    id 47
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 48
    label "formalizowanie"
  ]
  node [
    id 49
    label "formalnie"
  ]
  node [
    id 50
    label "oficjalnie"
  ]
  node [
    id 51
    label "jawny"
  ]
  node [
    id 52
    label "legalny"
  ]
  node [
    id 53
    label "sformalizowanie"
  ]
  node [
    id 54
    label "pozorny"
  ]
  node [
    id 55
    label "kompletny"
  ]
  node [
    id 56
    label "prawdziwy"
  ]
  node [
    id 57
    label "prawomocny"
  ]
  node [
    id 58
    label "stanowisko"
  ]
  node [
    id 59
    label "position"
  ]
  node [
    id 60
    label "instytucja"
  ]
  node [
    id 61
    label "siedziba"
  ]
  node [
    id 62
    label "organ"
  ]
  node [
    id 63
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 64
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 65
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 66
    label "mianowaniec"
  ]
  node [
    id 67
    label "dzia&#322;"
  ]
  node [
    id 68
    label "okienko"
  ]
  node [
    id 69
    label "w&#322;adza"
  ]
  node [
    id 70
    label "&#321;ubianka"
  ]
  node [
    id 71
    label "miejsce_pracy"
  ]
  node [
    id 72
    label "dzia&#322;_personalny"
  ]
  node [
    id 73
    label "Kreml"
  ]
  node [
    id 74
    label "Bia&#322;y_Dom"
  ]
  node [
    id 75
    label "budynek"
  ]
  node [
    id 76
    label "miejsce"
  ]
  node [
    id 77
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 78
    label "sadowisko"
  ]
  node [
    id 79
    label "struktura"
  ]
  node [
    id 80
    label "prawo"
  ]
  node [
    id 81
    label "cz&#322;owiek"
  ]
  node [
    id 82
    label "rz&#261;dzenie"
  ]
  node [
    id 83
    label "panowanie"
  ]
  node [
    id 84
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 85
    label "wydolno&#347;&#263;"
  ]
  node [
    id 86
    label "grupa"
  ]
  node [
    id 87
    label "rz&#261;d"
  ]
  node [
    id 88
    label "po&#322;o&#380;enie"
  ]
  node [
    id 89
    label "punkt"
  ]
  node [
    id 90
    label "pogl&#261;d"
  ]
  node [
    id 91
    label "wojsko"
  ]
  node [
    id 92
    label "awansowa&#263;"
  ]
  node [
    id 93
    label "stawia&#263;"
  ]
  node [
    id 94
    label "uprawianie"
  ]
  node [
    id 95
    label "wakowa&#263;"
  ]
  node [
    id 96
    label "powierzanie"
  ]
  node [
    id 97
    label "postawi&#263;"
  ]
  node [
    id 98
    label "awansowanie"
  ]
  node [
    id 99
    label "praca"
  ]
  node [
    id 100
    label "tkanka"
  ]
  node [
    id 101
    label "jednostka_organizacyjna"
  ]
  node [
    id 102
    label "budowa"
  ]
  node [
    id 103
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 104
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 105
    label "tw&#243;r"
  ]
  node [
    id 106
    label "organogeneza"
  ]
  node [
    id 107
    label "zesp&#243;&#322;"
  ]
  node [
    id 108
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 109
    label "struktura_anatomiczna"
  ]
  node [
    id 110
    label "uk&#322;ad"
  ]
  node [
    id 111
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 112
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 113
    label "Izba_Konsyliarska"
  ]
  node [
    id 114
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 115
    label "stomia"
  ]
  node [
    id 116
    label "dekortykacja"
  ]
  node [
    id 117
    label "okolica"
  ]
  node [
    id 118
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 119
    label "Komitet_Region&#243;w"
  ]
  node [
    id 120
    label "ekran"
  ]
  node [
    id 121
    label "interfejs"
  ]
  node [
    id 122
    label "okno"
  ]
  node [
    id 123
    label "tabela"
  ]
  node [
    id 124
    label "poczta"
  ]
  node [
    id 125
    label "rubryka"
  ]
  node [
    id 126
    label "ram&#243;wka"
  ]
  node [
    id 127
    label "czas_wolny"
  ]
  node [
    id 128
    label "wype&#322;nianie"
  ]
  node [
    id 129
    label "program"
  ]
  node [
    id 130
    label "wype&#322;nienie"
  ]
  node [
    id 131
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 132
    label "pulpit"
  ]
  node [
    id 133
    label "menad&#380;er_okien"
  ]
  node [
    id 134
    label "otw&#243;r"
  ]
  node [
    id 135
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 136
    label "sfera"
  ]
  node [
    id 137
    label "zakres"
  ]
  node [
    id 138
    label "insourcing"
  ]
  node [
    id 139
    label "whole"
  ]
  node [
    id 140
    label "wytw&#243;r"
  ]
  node [
    id 141
    label "column"
  ]
  node [
    id 142
    label "distribution"
  ]
  node [
    id 143
    label "stopie&#324;"
  ]
  node [
    id 144
    label "competence"
  ]
  node [
    id 145
    label "bezdro&#380;e"
  ]
  node [
    id 146
    label "poddzia&#322;"
  ]
  node [
    id 147
    label "mandatariusz"
  ]
  node [
    id 148
    label "osoba_prawna"
  ]
  node [
    id 149
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 150
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 151
    label "poj&#281;cie"
  ]
  node [
    id 152
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 153
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 154
    label "biuro"
  ]
  node [
    id 155
    label "organizacja"
  ]
  node [
    id 156
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 157
    label "Fundusze_Unijne"
  ]
  node [
    id 158
    label "zamyka&#263;"
  ]
  node [
    id 159
    label "establishment"
  ]
  node [
    id 160
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 161
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 162
    label "afiliowa&#263;"
  ]
  node [
    id 163
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 164
    label "standard"
  ]
  node [
    id 165
    label "zamykanie"
  ]
  node [
    id 166
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 167
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 168
    label "formacja"
  ]
  node [
    id 169
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 170
    label "obstawianie"
  ]
  node [
    id 171
    label "obstawienie"
  ]
  node [
    id 172
    label "tarcza"
  ]
  node [
    id 173
    label "ubezpieczenie"
  ]
  node [
    id 174
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 175
    label "transportacja"
  ]
  node [
    id 176
    label "obstawia&#263;"
  ]
  node [
    id 177
    label "obiekt"
  ]
  node [
    id 178
    label "borowiec"
  ]
  node [
    id 179
    label "chemical_bond"
  ]
  node [
    id 180
    label "co&#347;"
  ]
  node [
    id 181
    label "thing"
  ]
  node [
    id 182
    label "rzecz"
  ]
  node [
    id 183
    label "strona"
  ]
  node [
    id 184
    label "Bund"
  ]
  node [
    id 185
    label "Mazowsze"
  ]
  node [
    id 186
    label "PPR"
  ]
  node [
    id 187
    label "Jakobici"
  ]
  node [
    id 188
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 189
    label "leksem"
  ]
  node [
    id 190
    label "SLD"
  ]
  node [
    id 191
    label "zespolik"
  ]
  node [
    id 192
    label "Razem"
  ]
  node [
    id 193
    label "PiS"
  ]
  node [
    id 194
    label "zjawisko"
  ]
  node [
    id 195
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 196
    label "partia"
  ]
  node [
    id 197
    label "Kuomintang"
  ]
  node [
    id 198
    label "ZSL"
  ]
  node [
    id 199
    label "szko&#322;a"
  ]
  node [
    id 200
    label "jednostka"
  ]
  node [
    id 201
    label "proces"
  ]
  node [
    id 202
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 203
    label "rugby"
  ]
  node [
    id 204
    label "AWS"
  ]
  node [
    id 205
    label "posta&#263;"
  ]
  node [
    id 206
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 207
    label "blok"
  ]
  node [
    id 208
    label "PO"
  ]
  node [
    id 209
    label "si&#322;a"
  ]
  node [
    id 210
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 211
    label "Federali&#347;ci"
  ]
  node [
    id 212
    label "PSL"
  ]
  node [
    id 213
    label "Wigowie"
  ]
  node [
    id 214
    label "ZChN"
  ]
  node [
    id 215
    label "egzekutywa"
  ]
  node [
    id 216
    label "rocznik"
  ]
  node [
    id 217
    label "The_Beatles"
  ]
  node [
    id 218
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 219
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 220
    label "unit"
  ]
  node [
    id 221
    label "Depeche_Mode"
  ]
  node [
    id 222
    label "forma"
  ]
  node [
    id 223
    label "naszywka"
  ]
  node [
    id 224
    label "przedmiot"
  ]
  node [
    id 225
    label "kszta&#322;t"
  ]
  node [
    id 226
    label "wskaz&#243;wka"
  ]
  node [
    id 227
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 228
    label "obro&#324;ca"
  ]
  node [
    id 229
    label "bro&#324;_ochronna"
  ]
  node [
    id 230
    label "odznaka"
  ]
  node [
    id 231
    label "bro&#324;"
  ]
  node [
    id 232
    label "denture"
  ]
  node [
    id 233
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 234
    label "telefon"
  ]
  node [
    id 235
    label "or&#281;&#380;"
  ]
  node [
    id 236
    label "target"
  ]
  node [
    id 237
    label "cel"
  ]
  node [
    id 238
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 239
    label "maszyna"
  ]
  node [
    id 240
    label "obszar"
  ]
  node [
    id 241
    label "ucze&#324;"
  ]
  node [
    id 242
    label "powierzchnia"
  ]
  node [
    id 243
    label "tablica"
  ]
  node [
    id 244
    label "op&#322;ata"
  ]
  node [
    id 245
    label "ubezpieczalnia"
  ]
  node [
    id 246
    label "insurance"
  ]
  node [
    id 247
    label "cover"
  ]
  node [
    id 248
    label "suma_ubezpieczenia"
  ]
  node [
    id 249
    label "screen"
  ]
  node [
    id 250
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 251
    label "franszyza"
  ]
  node [
    id 252
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 253
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 254
    label "oddzia&#322;"
  ]
  node [
    id 255
    label "zapewnienie"
  ]
  node [
    id 256
    label "przyznanie"
  ]
  node [
    id 257
    label "umowa"
  ]
  node [
    id 258
    label "uchronienie"
  ]
  node [
    id 259
    label "transport"
  ]
  node [
    id 260
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 261
    label "Irish_pound"
  ]
  node [
    id 262
    label "otoczenie"
  ]
  node [
    id 263
    label "bramka"
  ]
  node [
    id 264
    label "wytypowanie"
  ]
  node [
    id 265
    label "za&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 266
    label "otaczanie"
  ]
  node [
    id 267
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 268
    label "os&#322;anianie"
  ]
  node [
    id 269
    label "typowanie"
  ]
  node [
    id 270
    label "ubezpieczanie"
  ]
  node [
    id 271
    label "ubezpiecza&#263;"
  ]
  node [
    id 272
    label "venture"
  ]
  node [
    id 273
    label "przewidywa&#263;"
  ]
  node [
    id 274
    label "zapewnia&#263;"
  ]
  node [
    id 275
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 276
    label "typowa&#263;"
  ]
  node [
    id 277
    label "zastawia&#263;"
  ]
  node [
    id 278
    label "budowa&#263;"
  ]
  node [
    id 279
    label "zajmowa&#263;"
  ]
  node [
    id 280
    label "obejmowa&#263;"
  ]
  node [
    id 281
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 282
    label "os&#322;ania&#263;"
  ]
  node [
    id 283
    label "otacza&#263;"
  ]
  node [
    id 284
    label "broni&#263;"
  ]
  node [
    id 285
    label "powierza&#263;"
  ]
  node [
    id 286
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 287
    label "frame"
  ]
  node [
    id 288
    label "wysy&#322;a&#263;"
  ]
  node [
    id 289
    label "typ"
  ]
  node [
    id 290
    label "borowce"
  ]
  node [
    id 291
    label "ochroniarz"
  ]
  node [
    id 292
    label "duch"
  ]
  node [
    id 293
    label "borowik"
  ]
  node [
    id 294
    label "nietoperz"
  ]
  node [
    id 295
    label "kozio&#322;ek"
  ]
  node [
    id 296
    label "owado&#380;erca"
  ]
  node [
    id 297
    label "funkcjonariusz"
  ]
  node [
    id 298
    label "BOR"
  ]
  node [
    id 299
    label "mroczkowate"
  ]
  node [
    id 300
    label "pierwiastek"
  ]
  node [
    id 301
    label "wydarzenie"
  ]
  node [
    id 302
    label "interakcja"
  ]
  node [
    id 303
    label "firma"
  ]
  node [
    id 304
    label "uczestnik"
  ]
  node [
    id 305
    label "contest"
  ]
  node [
    id 306
    label "dyscyplina_sportowa"
  ]
  node [
    id 307
    label "rywalizacja"
  ]
  node [
    id 308
    label "dob&#243;r_naturalny"
  ]
  node [
    id 309
    label "interaction"
  ]
  node [
    id 310
    label "wp&#322;yw"
  ]
  node [
    id 311
    label "Apeks"
  ]
  node [
    id 312
    label "zasoby"
  ]
  node [
    id 313
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 314
    label "zaufanie"
  ]
  node [
    id 315
    label "Hortex"
  ]
  node [
    id 316
    label "reengineering"
  ]
  node [
    id 317
    label "nazwa_w&#322;asna"
  ]
  node [
    id 318
    label "podmiot_gospodarczy"
  ]
  node [
    id 319
    label "paczkarnia"
  ]
  node [
    id 320
    label "Orlen"
  ]
  node [
    id 321
    label "interes"
  ]
  node [
    id 322
    label "Google"
  ]
  node [
    id 323
    label "Pewex"
  ]
  node [
    id 324
    label "Canon"
  ]
  node [
    id 325
    label "MAN_SE"
  ]
  node [
    id 326
    label "Spo&#322;em"
  ]
  node [
    id 327
    label "klasa"
  ]
  node [
    id 328
    label "networking"
  ]
  node [
    id 329
    label "MAC"
  ]
  node [
    id 330
    label "zasoby_ludzkie"
  ]
  node [
    id 331
    label "Baltona"
  ]
  node [
    id 332
    label "Orbis"
  ]
  node [
    id 333
    label "biurowiec"
  ]
  node [
    id 334
    label "HP"
  ]
  node [
    id 335
    label "przebiec"
  ]
  node [
    id 336
    label "charakter"
  ]
  node [
    id 337
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 338
    label "motyw"
  ]
  node [
    id 339
    label "przebiegni&#281;cie"
  ]
  node [
    id 340
    label "fabu&#322;a"
  ]
  node [
    id 341
    label "rynek"
  ]
  node [
    id 342
    label "u&#380;ytkownik"
  ]
  node [
    id 343
    label "odbiorca"
  ]
  node [
    id 344
    label "klient"
  ]
  node [
    id 345
    label "restauracja"
  ]
  node [
    id 346
    label "zjadacz"
  ]
  node [
    id 347
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 348
    label "heterotrof"
  ]
  node [
    id 349
    label "go&#347;&#263;"
  ]
  node [
    id 350
    label "recipient_role"
  ]
  node [
    id 351
    label "otrzymanie"
  ]
  node [
    id 352
    label "otrzymywanie"
  ]
  node [
    id 353
    label "agent_rozliczeniowy"
  ]
  node [
    id 354
    label "komputer_cyfrowy"
  ]
  node [
    id 355
    label "us&#322;ugobiorca"
  ]
  node [
    id 356
    label "Rzymianin"
  ]
  node [
    id 357
    label "szlachcic"
  ]
  node [
    id 358
    label "obywatel"
  ]
  node [
    id 359
    label "klientela"
  ]
  node [
    id 360
    label "bratek"
  ]
  node [
    id 361
    label "podmiot"
  ]
  node [
    id 362
    label "j&#281;zykowo"
  ]
  node [
    id 363
    label "odwiedziny"
  ]
  node [
    id 364
    label "przybysz"
  ]
  node [
    id 365
    label "hotel"
  ]
  node [
    id 366
    label "sztuka"
  ]
  node [
    id 367
    label "facet"
  ]
  node [
    id 368
    label "organizm"
  ]
  node [
    id 369
    label "heterotroph"
  ]
  node [
    id 370
    label "cudzo&#380;ywny"
  ]
  node [
    id 371
    label "stoisko"
  ]
  node [
    id 372
    label "rynek_podstawowy"
  ]
  node [
    id 373
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 374
    label "pojawienie_si&#281;"
  ]
  node [
    id 375
    label "obiekt_handlowy"
  ]
  node [
    id 376
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 377
    label "wytw&#243;rca"
  ]
  node [
    id 378
    label "rynek_wt&#243;rny"
  ]
  node [
    id 379
    label "wprowadzanie"
  ]
  node [
    id 380
    label "wprowadza&#263;"
  ]
  node [
    id 381
    label "kram"
  ]
  node [
    id 382
    label "plac"
  ]
  node [
    id 383
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 384
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 385
    label "emitowa&#263;"
  ]
  node [
    id 386
    label "wprowadzi&#263;"
  ]
  node [
    id 387
    label "emitowanie"
  ]
  node [
    id 388
    label "gospodarka"
  ]
  node [
    id 389
    label "biznes"
  ]
  node [
    id 390
    label "segment_rynku"
  ]
  node [
    id 391
    label "wprowadzenie"
  ]
  node [
    id 392
    label "targowica"
  ]
  node [
    id 393
    label "gastronomia"
  ]
  node [
    id 394
    label "zak&#322;ad"
  ]
  node [
    id 395
    label "naprawa"
  ]
  node [
    id 396
    label "powr&#243;t"
  ]
  node [
    id 397
    label "pikolak"
  ]
  node [
    id 398
    label "karta"
  ]
  node [
    id 399
    label "i"
  ]
  node [
    id 400
    label "polski"
  ]
  node [
    id 401
    label "telefonia"
  ]
  node [
    id 402
    label "cyfrowy"
  ]
  node [
    id 403
    label "sp&#243;&#322;ka"
  ]
  node [
    id 404
    label "zeszyt"
  ]
  node [
    id 405
    label "ojciec"
  ]
  node [
    id 406
    label "prezes"
  ]
  node [
    id 407
    label "UOKiK"
  ]
  node [
    id 408
    label "s&#261;d"
  ]
  node [
    id 409
    label "okr&#281;gowy"
  ]
  node [
    id 410
    label "wyspa"
  ]
  node [
    id 411
    label "warszawa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 399
    target 406
  ]
  edge [
    source 399
    target 408
  ]
  edge [
    source 400
    target 401
  ]
  edge [
    source 400
    target 402
  ]
  edge [
    source 400
    target 403
  ]
  edge [
    source 400
    target 404
  ]
  edge [
    source 400
    target 405
  ]
  edge [
    source 401
    target 402
  ]
  edge [
    source 401
    target 403
  ]
  edge [
    source 401
    target 404
  ]
  edge [
    source 401
    target 405
  ]
  edge [
    source 402
    target 403
  ]
  edge [
    source 402
    target 404
  ]
  edge [
    source 402
    target 405
  ]
  edge [
    source 403
    target 404
  ]
  edge [
    source 403
    target 405
  ]
  edge [
    source 404
    target 405
  ]
  edge [
    source 405
    target 405
  ]
  edge [
    source 406
    target 407
  ]
  edge [
    source 408
    target 409
  ]
  edge [
    source 408
    target 410
  ]
  edge [
    source 408
    target 411
  ]
  edge [
    source 409
    target 410
  ]
  edge [
    source 409
    target 411
  ]
  edge [
    source 410
    target 411
  ]
]
