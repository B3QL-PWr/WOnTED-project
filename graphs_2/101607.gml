graph [
  node [
    id 0
    label "pami&#281;tnik"
    origin "text"
  ]
  node [
    id 1
    label "wi&#281;zie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "xiii"
    origin "text"
  ]
  node [
    id 3
    label "fort"
    origin "text"
  ]
  node [
    id 4
    label "twierdza"
    origin "text"
  ]
  node [
    id 5
    label "przemy&#347;l"
    origin "text"
  ]
  node [
    id 6
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 7
    label "pami&#261;tka"
  ]
  node [
    id 8
    label "notes"
  ]
  node [
    id 9
    label "zapiski"
  ]
  node [
    id 10
    label "raptularz"
  ]
  node [
    id 11
    label "album"
  ]
  node [
    id 12
    label "utw&#243;r_epicki"
  ]
  node [
    id 13
    label "przedmiot"
  ]
  node [
    id 14
    label "&#347;wiadectwo"
  ]
  node [
    id 15
    label "zapis"
  ]
  node [
    id 16
    label "notatnik"
  ]
  node [
    id 17
    label "kajecik"
  ]
  node [
    id 18
    label "blok"
  ]
  node [
    id 19
    label "egzemplarz"
  ]
  node [
    id 20
    label "indeks"
  ]
  node [
    id 21
    label "sketchbook"
  ]
  node [
    id 22
    label "kolekcja"
  ]
  node [
    id 23
    label "etui"
  ]
  node [
    id 24
    label "wydawnictwo"
  ]
  node [
    id 25
    label "szkic"
  ]
  node [
    id 26
    label "stamp_album"
  ]
  node [
    id 27
    label "studiowa&#263;"
  ]
  node [
    id 28
    label "ksi&#281;ga"
  ]
  node [
    id 29
    label "p&#322;yta"
  ]
  node [
    id 30
    label "literatura_faktu"
  ]
  node [
    id 31
    label "pisarstwo"
  ]
  node [
    id 32
    label "pierdel"
  ]
  node [
    id 33
    label "&#321;ubianka"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 36
    label "kiciarz"
  ]
  node [
    id 37
    label "ciupa"
  ]
  node [
    id 38
    label "reedukator"
  ]
  node [
    id 39
    label "pasiak"
  ]
  node [
    id 40
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 41
    label "Butyrki"
  ]
  node [
    id 42
    label "miejsce_odosobnienia"
  ]
  node [
    id 43
    label "ludzko&#347;&#263;"
  ]
  node [
    id 44
    label "asymilowanie"
  ]
  node [
    id 45
    label "wapniak"
  ]
  node [
    id 46
    label "asymilowa&#263;"
  ]
  node [
    id 47
    label "os&#322;abia&#263;"
  ]
  node [
    id 48
    label "posta&#263;"
  ]
  node [
    id 49
    label "hominid"
  ]
  node [
    id 50
    label "podw&#322;adny"
  ]
  node [
    id 51
    label "os&#322;abianie"
  ]
  node [
    id 52
    label "g&#322;owa"
  ]
  node [
    id 53
    label "figura"
  ]
  node [
    id 54
    label "portrecista"
  ]
  node [
    id 55
    label "dwun&#243;g"
  ]
  node [
    id 56
    label "profanum"
  ]
  node [
    id 57
    label "mikrokosmos"
  ]
  node [
    id 58
    label "nasada"
  ]
  node [
    id 59
    label "duch"
  ]
  node [
    id 60
    label "antropochoria"
  ]
  node [
    id 61
    label "osoba"
  ]
  node [
    id 62
    label "wz&#243;r"
  ]
  node [
    id 63
    label "senior"
  ]
  node [
    id 64
    label "oddzia&#322;ywanie"
  ]
  node [
    id 65
    label "Adam"
  ]
  node [
    id 66
    label "homo_sapiens"
  ]
  node [
    id 67
    label "polifag"
  ]
  node [
    id 68
    label "alfabet_wi&#281;zienny"
  ]
  node [
    id 69
    label "odsiadka"
  ]
  node [
    id 70
    label "wi&#281;zienie"
  ]
  node [
    id 71
    label "tkanina"
  ]
  node [
    id 72
    label "pasiasty"
  ]
  node [
    id 73
    label "uniform"
  ]
  node [
    id 74
    label "nauczyciel"
  ]
  node [
    id 75
    label "pracownik"
  ]
  node [
    id 76
    label "dom_poprawczy"
  ]
  node [
    id 77
    label "Monar"
  ]
  node [
    id 78
    label "rehabilitant"
  ]
  node [
    id 79
    label "gra_MMORPG"
  ]
  node [
    id 80
    label "budowla"
  ]
  node [
    id 81
    label "fortyfikacja"
  ]
  node [
    id 82
    label "fosa"
  ]
  node [
    id 83
    label "szaniec"
  ]
  node [
    id 84
    label "machiku&#322;"
  ]
  node [
    id 85
    label "kazamata"
  ]
  node [
    id 86
    label "bastion"
  ]
  node [
    id 87
    label "kurtyna"
  ]
  node [
    id 88
    label "barykada"
  ]
  node [
    id 89
    label "przedbramie"
  ]
  node [
    id 90
    label "baszta"
  ]
  node [
    id 91
    label "transzeja"
  ]
  node [
    id 92
    label "zamek"
  ]
  node [
    id 93
    label "palisada"
  ]
  node [
    id 94
    label "in&#380;ynieria"
  ]
  node [
    id 95
    label "okop"
  ]
  node [
    id 96
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 97
    label "&#347;r&#243;dszaniec"
  ]
  node [
    id 98
    label "obudowanie"
  ]
  node [
    id 99
    label "obudowywa&#263;"
  ]
  node [
    id 100
    label "zbudowa&#263;"
  ]
  node [
    id 101
    label "obudowa&#263;"
  ]
  node [
    id 102
    label "kolumnada"
  ]
  node [
    id 103
    label "korpus"
  ]
  node [
    id 104
    label "Sukiennice"
  ]
  node [
    id 105
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 106
    label "fundament"
  ]
  node [
    id 107
    label "postanie"
  ]
  node [
    id 108
    label "obudowywanie"
  ]
  node [
    id 109
    label "zbudowanie"
  ]
  node [
    id 110
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 111
    label "stan_surowy"
  ]
  node [
    id 112
    label "konstrukcja"
  ]
  node [
    id 113
    label "rzecz"
  ]
  node [
    id 114
    label "Dyjament"
  ]
  node [
    id 115
    label "Brenna"
  ]
  node [
    id 116
    label "flanka"
  ]
  node [
    id 117
    label "schronienie"
  ]
  node [
    id 118
    label "Szlisselburg"
  ]
  node [
    id 119
    label "bezpieczny"
  ]
  node [
    id 120
    label "ukryty"
  ]
  node [
    id 121
    label "cover"
  ]
  node [
    id 122
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 123
    label "ukrycie"
  ]
  node [
    id 124
    label "miejsce"
  ]
  node [
    id 125
    label "basteja"
  ]
  node [
    id 126
    label "ostoja"
  ]
  node [
    id 127
    label "dzie&#322;o_koronowe"
  ]
  node [
    id 128
    label "narys_bastionowy"
  ]
  node [
    id 129
    label "boisko"
  ]
  node [
    id 130
    label "ugrupowanie"
  ]
  node [
    id 131
    label "bok"
  ]
  node [
    id 132
    label "Brandenburg"
  ]
  node [
    id 133
    label "Ryga"
  ]
  node [
    id 134
    label "przemy&#347;le&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
]
