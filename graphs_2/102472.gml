graph [
  node [
    id 0
    label "ozogalerii"
    origin "text"
  ]
  node [
    id 1
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 2
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 4
    label "wygrany"
    origin "text"
  ]
  node [
    id 5
    label "mecz"
    origin "text"
  ]
  node [
    id 6
    label "mks"
    origin "text"
  ]
  node [
    id 7
    label "bzura"
    origin "text"
  ]
  node [
    id 8
    label "sms"
    origin "text"
  ]
  node [
    id 9
    label "pzps"
    origin "text"
  ]
  node [
    id 10
    label "spa&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "dwa"
    origin "text"
  ]
  node [
    id 15
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 16
    label "temu"
    origin "text"
  ]
  node [
    id 17
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 18
    label "free"
  ]
  node [
    id 19
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 20
    label "spoziera&#263;"
  ]
  node [
    id 21
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 22
    label "peek"
  ]
  node [
    id 23
    label "postrzec"
  ]
  node [
    id 24
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 25
    label "cognizance"
  ]
  node [
    id 26
    label "popatrze&#263;"
  ]
  node [
    id 27
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 28
    label "pojrze&#263;"
  ]
  node [
    id 29
    label "dostrzec"
  ]
  node [
    id 30
    label "spot"
  ]
  node [
    id 31
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 32
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 33
    label "go_steady"
  ]
  node [
    id 34
    label "zinterpretowa&#263;"
  ]
  node [
    id 35
    label "spotka&#263;"
  ]
  node [
    id 36
    label "obejrze&#263;"
  ]
  node [
    id 37
    label "znale&#378;&#263;"
  ]
  node [
    id 38
    label "see"
  ]
  node [
    id 39
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 40
    label "oceni&#263;"
  ]
  node [
    id 41
    label "zagra&#263;"
  ]
  node [
    id 42
    label "illustrate"
  ]
  node [
    id 43
    label "zanalizowa&#263;"
  ]
  node [
    id 44
    label "read"
  ]
  node [
    id 45
    label "think"
  ]
  node [
    id 46
    label "visualize"
  ]
  node [
    id 47
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 48
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 49
    label "sta&#263;_si&#281;"
  ]
  node [
    id 50
    label "zrobi&#263;"
  ]
  node [
    id 51
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 52
    label "notice"
  ]
  node [
    id 53
    label "respect"
  ]
  node [
    id 54
    label "insert"
  ]
  node [
    id 55
    label "pozna&#263;"
  ]
  node [
    id 56
    label "befall"
  ]
  node [
    id 57
    label "spowodowa&#263;"
  ]
  node [
    id 58
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 59
    label "discover"
  ]
  node [
    id 60
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 61
    label "pozyska&#263;"
  ]
  node [
    id 62
    label "devise"
  ]
  node [
    id 63
    label "dozna&#263;"
  ]
  node [
    id 64
    label "wykry&#263;"
  ]
  node [
    id 65
    label "odzyska&#263;"
  ]
  node [
    id 66
    label "znaj&#347;&#263;"
  ]
  node [
    id 67
    label "invent"
  ]
  node [
    id 68
    label "wymy&#347;li&#263;"
  ]
  node [
    id 69
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 70
    label "radio"
  ]
  node [
    id 71
    label "lampa"
  ]
  node [
    id 72
    label "film"
  ]
  node [
    id 73
    label "pomiar"
  ]
  node [
    id 74
    label "booklet"
  ]
  node [
    id 75
    label "transakcja"
  ]
  node [
    id 76
    label "ekspozycja"
  ]
  node [
    id 77
    label "reklama"
  ]
  node [
    id 78
    label "fotografia"
  ]
  node [
    id 79
    label "spojrze&#263;"
  ]
  node [
    id 80
    label "spogl&#261;da&#263;"
  ]
  node [
    id 81
    label "zwyci&#281;ski"
  ]
  node [
    id 82
    label "zwyci&#281;sko"
  ]
  node [
    id 83
    label "najlepszy"
  ]
  node [
    id 84
    label "tryumfalny"
  ]
  node [
    id 85
    label "udany"
  ]
  node [
    id 86
    label "pe&#322;ny"
  ]
  node [
    id 87
    label "triumfalny"
  ]
  node [
    id 88
    label "obrona"
  ]
  node [
    id 89
    label "gra"
  ]
  node [
    id 90
    label "game"
  ]
  node [
    id 91
    label "serw"
  ]
  node [
    id 92
    label "dwumecz"
  ]
  node [
    id 93
    label "zmienno&#347;&#263;"
  ]
  node [
    id 94
    label "play"
  ]
  node [
    id 95
    label "rozgrywka"
  ]
  node [
    id 96
    label "apparent_motion"
  ]
  node [
    id 97
    label "wydarzenie"
  ]
  node [
    id 98
    label "contest"
  ]
  node [
    id 99
    label "akcja"
  ]
  node [
    id 100
    label "komplet"
  ]
  node [
    id 101
    label "zabawa"
  ]
  node [
    id 102
    label "zasada"
  ]
  node [
    id 103
    label "rywalizacja"
  ]
  node [
    id 104
    label "zbijany"
  ]
  node [
    id 105
    label "post&#281;powanie"
  ]
  node [
    id 106
    label "odg&#322;os"
  ]
  node [
    id 107
    label "Pok&#233;mon"
  ]
  node [
    id 108
    label "czynno&#347;&#263;"
  ]
  node [
    id 109
    label "synteza"
  ]
  node [
    id 110
    label "odtworzenie"
  ]
  node [
    id 111
    label "rekwizyt_do_gry"
  ]
  node [
    id 112
    label "egzamin"
  ]
  node [
    id 113
    label "walka"
  ]
  node [
    id 114
    label "liga"
  ]
  node [
    id 115
    label "gracz"
  ]
  node [
    id 116
    label "poj&#281;cie"
  ]
  node [
    id 117
    label "protection"
  ]
  node [
    id 118
    label "poparcie"
  ]
  node [
    id 119
    label "reakcja"
  ]
  node [
    id 120
    label "defense"
  ]
  node [
    id 121
    label "s&#261;d"
  ]
  node [
    id 122
    label "auspices"
  ]
  node [
    id 123
    label "ochrona"
  ]
  node [
    id 124
    label "sp&#243;r"
  ]
  node [
    id 125
    label "wojsko"
  ]
  node [
    id 126
    label "manewr"
  ]
  node [
    id 127
    label "defensive_structure"
  ]
  node [
    id 128
    label "guard_duty"
  ]
  node [
    id 129
    label "strona"
  ]
  node [
    id 130
    label "uderzenie"
  ]
  node [
    id 131
    label "SMS"
  ]
  node [
    id 132
    label "us&#322;uga"
  ]
  node [
    id 133
    label "wiadomo&#347;&#263;_tekstowa"
  ]
  node [
    id 134
    label "pie&#324;"
  ]
  node [
    id 135
    label "odziomek"
  ]
  node [
    id 136
    label "m&#243;zg"
  ]
  node [
    id 137
    label "morfem"
  ]
  node [
    id 138
    label "s&#322;&#243;j"
  ]
  node [
    id 139
    label "plombowanie"
  ]
  node [
    id 140
    label "drzewo"
  ]
  node [
    id 141
    label "organ_ro&#347;linny"
  ]
  node [
    id 142
    label "element_anatomiczny"
  ]
  node [
    id 143
    label "plombowa&#263;"
  ]
  node [
    id 144
    label "pniak"
  ]
  node [
    id 145
    label "zaplombowa&#263;"
  ]
  node [
    id 146
    label "zaplombowanie"
  ]
  node [
    id 147
    label "reserve"
  ]
  node [
    id 148
    label "przej&#347;&#263;"
  ]
  node [
    id 149
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 150
    label "ustawa"
  ]
  node [
    id 151
    label "podlec"
  ]
  node [
    id 152
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 153
    label "min&#261;&#263;"
  ]
  node [
    id 154
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 155
    label "zaliczy&#263;"
  ]
  node [
    id 156
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 157
    label "zmieni&#263;"
  ]
  node [
    id 158
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 159
    label "przeby&#263;"
  ]
  node [
    id 160
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 161
    label "die"
  ]
  node [
    id 162
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 163
    label "zacz&#261;&#263;"
  ]
  node [
    id 164
    label "happen"
  ]
  node [
    id 165
    label "pass"
  ]
  node [
    id 166
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 167
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 168
    label "beat"
  ]
  node [
    id 169
    label "mienie"
  ]
  node [
    id 170
    label "absorb"
  ]
  node [
    id 171
    label "przerobi&#263;"
  ]
  node [
    id 172
    label "pique"
  ]
  node [
    id 173
    label "przesta&#263;"
  ]
  node [
    id 174
    label "doba"
  ]
  node [
    id 175
    label "weekend"
  ]
  node [
    id 176
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 177
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 178
    label "czas"
  ]
  node [
    id 179
    label "miesi&#261;c"
  ]
  node [
    id 180
    label "poprzedzanie"
  ]
  node [
    id 181
    label "czasoprzestrze&#324;"
  ]
  node [
    id 182
    label "laba"
  ]
  node [
    id 183
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 184
    label "chronometria"
  ]
  node [
    id 185
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 186
    label "rachuba_czasu"
  ]
  node [
    id 187
    label "przep&#322;ywanie"
  ]
  node [
    id 188
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 189
    label "czasokres"
  ]
  node [
    id 190
    label "odczyt"
  ]
  node [
    id 191
    label "chwila"
  ]
  node [
    id 192
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 193
    label "dzieje"
  ]
  node [
    id 194
    label "kategoria_gramatyczna"
  ]
  node [
    id 195
    label "poprzedzenie"
  ]
  node [
    id 196
    label "trawienie"
  ]
  node [
    id 197
    label "pochodzi&#263;"
  ]
  node [
    id 198
    label "period"
  ]
  node [
    id 199
    label "okres_czasu"
  ]
  node [
    id 200
    label "poprzedza&#263;"
  ]
  node [
    id 201
    label "schy&#322;ek"
  ]
  node [
    id 202
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 203
    label "odwlekanie_si&#281;"
  ]
  node [
    id 204
    label "zegar"
  ]
  node [
    id 205
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 206
    label "czwarty_wymiar"
  ]
  node [
    id 207
    label "pochodzenie"
  ]
  node [
    id 208
    label "koniugacja"
  ]
  node [
    id 209
    label "Zeitgeist"
  ]
  node [
    id 210
    label "trawi&#263;"
  ]
  node [
    id 211
    label "pogoda"
  ]
  node [
    id 212
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 213
    label "poprzedzi&#263;"
  ]
  node [
    id 214
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 215
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 216
    label "time_period"
  ]
  node [
    id 217
    label "noc"
  ]
  node [
    id 218
    label "dzie&#324;"
  ]
  node [
    id 219
    label "godzina"
  ]
  node [
    id 220
    label "long_time"
  ]
  node [
    id 221
    label "jednostka_geologiczna"
  ]
  node [
    id 222
    label "niedziela"
  ]
  node [
    id 223
    label "sobota"
  ]
  node [
    id 224
    label "miech"
  ]
  node [
    id 225
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 226
    label "rok"
  ]
  node [
    id 227
    label "kalendy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 223
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 16
    target 17
  ]
]
