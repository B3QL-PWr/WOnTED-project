graph [
  node [
    id 0
    label "os&#243;wiec"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "kujawsko"
    origin "text"
  ]
  node [
    id 3
    label "pomorski"
    origin "text"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "mikroregion"
  ]
  node [
    id 6
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 7
    label "pa&#324;stwo"
  ]
  node [
    id 8
    label "makroregion"
  ]
  node [
    id 9
    label "powiat"
  ]
  node [
    id 10
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 11
    label "region"
  ]
  node [
    id 12
    label "gmina"
  ]
  node [
    id 13
    label "Japonia"
  ]
  node [
    id 14
    label "Zair"
  ]
  node [
    id 15
    label "Belize"
  ]
  node [
    id 16
    label "San_Marino"
  ]
  node [
    id 17
    label "Tanzania"
  ]
  node [
    id 18
    label "Antigua_i_Barbuda"
  ]
  node [
    id 19
    label "granica_pa&#324;stwa"
  ]
  node [
    id 20
    label "Senegal"
  ]
  node [
    id 21
    label "Seszele"
  ]
  node [
    id 22
    label "Mauretania"
  ]
  node [
    id 23
    label "Indie"
  ]
  node [
    id 24
    label "Filipiny"
  ]
  node [
    id 25
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 26
    label "Zimbabwe"
  ]
  node [
    id 27
    label "Malezja"
  ]
  node [
    id 28
    label "Rumunia"
  ]
  node [
    id 29
    label "Surinam"
  ]
  node [
    id 30
    label "Ukraina"
  ]
  node [
    id 31
    label "Syria"
  ]
  node [
    id 32
    label "Wyspy_Marshalla"
  ]
  node [
    id 33
    label "Burkina_Faso"
  ]
  node [
    id 34
    label "Grecja"
  ]
  node [
    id 35
    label "Polska"
  ]
  node [
    id 36
    label "Wenezuela"
  ]
  node [
    id 37
    label "Suazi"
  ]
  node [
    id 38
    label "Nepal"
  ]
  node [
    id 39
    label "S&#322;owacja"
  ]
  node [
    id 40
    label "Algieria"
  ]
  node [
    id 41
    label "Chiny"
  ]
  node [
    id 42
    label "Grenada"
  ]
  node [
    id 43
    label "Barbados"
  ]
  node [
    id 44
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 45
    label "Pakistan"
  ]
  node [
    id 46
    label "Niemcy"
  ]
  node [
    id 47
    label "Bahrajn"
  ]
  node [
    id 48
    label "Komory"
  ]
  node [
    id 49
    label "Australia"
  ]
  node [
    id 50
    label "Rodezja"
  ]
  node [
    id 51
    label "Malawi"
  ]
  node [
    id 52
    label "Gwinea"
  ]
  node [
    id 53
    label "Wehrlen"
  ]
  node [
    id 54
    label "Meksyk"
  ]
  node [
    id 55
    label "Liechtenstein"
  ]
  node [
    id 56
    label "Czarnog&#243;ra"
  ]
  node [
    id 57
    label "Wielka_Brytania"
  ]
  node [
    id 58
    label "Kuwejt"
  ]
  node [
    id 59
    label "Monako"
  ]
  node [
    id 60
    label "Angola"
  ]
  node [
    id 61
    label "Jemen"
  ]
  node [
    id 62
    label "Etiopia"
  ]
  node [
    id 63
    label "Madagaskar"
  ]
  node [
    id 64
    label "terytorium"
  ]
  node [
    id 65
    label "Kolumbia"
  ]
  node [
    id 66
    label "Portoryko"
  ]
  node [
    id 67
    label "Mauritius"
  ]
  node [
    id 68
    label "Kostaryka"
  ]
  node [
    id 69
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 70
    label "Tajlandia"
  ]
  node [
    id 71
    label "Argentyna"
  ]
  node [
    id 72
    label "Zambia"
  ]
  node [
    id 73
    label "Sri_Lanka"
  ]
  node [
    id 74
    label "Gwatemala"
  ]
  node [
    id 75
    label "Kirgistan"
  ]
  node [
    id 76
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 77
    label "Hiszpania"
  ]
  node [
    id 78
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 79
    label "Salwador"
  ]
  node [
    id 80
    label "Korea"
  ]
  node [
    id 81
    label "Macedonia"
  ]
  node [
    id 82
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 83
    label "Brunei"
  ]
  node [
    id 84
    label "Mozambik"
  ]
  node [
    id 85
    label "Turcja"
  ]
  node [
    id 86
    label "Kambod&#380;a"
  ]
  node [
    id 87
    label "Benin"
  ]
  node [
    id 88
    label "Bhutan"
  ]
  node [
    id 89
    label "Tunezja"
  ]
  node [
    id 90
    label "Austria"
  ]
  node [
    id 91
    label "Izrael"
  ]
  node [
    id 92
    label "Sierra_Leone"
  ]
  node [
    id 93
    label "Jamajka"
  ]
  node [
    id 94
    label "Rosja"
  ]
  node [
    id 95
    label "Rwanda"
  ]
  node [
    id 96
    label "holoarktyka"
  ]
  node [
    id 97
    label "Nigeria"
  ]
  node [
    id 98
    label "USA"
  ]
  node [
    id 99
    label "Oman"
  ]
  node [
    id 100
    label "Luksemburg"
  ]
  node [
    id 101
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 102
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 103
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 104
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 105
    label "Dominikana"
  ]
  node [
    id 106
    label "Irlandia"
  ]
  node [
    id 107
    label "Liban"
  ]
  node [
    id 108
    label "Hanower"
  ]
  node [
    id 109
    label "Estonia"
  ]
  node [
    id 110
    label "Iran"
  ]
  node [
    id 111
    label "Nowa_Zelandia"
  ]
  node [
    id 112
    label "Gabon"
  ]
  node [
    id 113
    label "Samoa"
  ]
  node [
    id 114
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 115
    label "S&#322;owenia"
  ]
  node [
    id 116
    label "Kiribati"
  ]
  node [
    id 117
    label "Egipt"
  ]
  node [
    id 118
    label "Togo"
  ]
  node [
    id 119
    label "Mongolia"
  ]
  node [
    id 120
    label "Sudan"
  ]
  node [
    id 121
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 122
    label "Bahamy"
  ]
  node [
    id 123
    label "Bangladesz"
  ]
  node [
    id 124
    label "partia"
  ]
  node [
    id 125
    label "Serbia"
  ]
  node [
    id 126
    label "Czechy"
  ]
  node [
    id 127
    label "Holandia"
  ]
  node [
    id 128
    label "Birma"
  ]
  node [
    id 129
    label "Albania"
  ]
  node [
    id 130
    label "Mikronezja"
  ]
  node [
    id 131
    label "Gambia"
  ]
  node [
    id 132
    label "Kazachstan"
  ]
  node [
    id 133
    label "interior"
  ]
  node [
    id 134
    label "Uzbekistan"
  ]
  node [
    id 135
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 136
    label "Malta"
  ]
  node [
    id 137
    label "Lesoto"
  ]
  node [
    id 138
    label "para"
  ]
  node [
    id 139
    label "Antarktis"
  ]
  node [
    id 140
    label "Andora"
  ]
  node [
    id 141
    label "Nauru"
  ]
  node [
    id 142
    label "Kuba"
  ]
  node [
    id 143
    label "Wietnam"
  ]
  node [
    id 144
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 145
    label "ziemia"
  ]
  node [
    id 146
    label "Kamerun"
  ]
  node [
    id 147
    label "Chorwacja"
  ]
  node [
    id 148
    label "Urugwaj"
  ]
  node [
    id 149
    label "Niger"
  ]
  node [
    id 150
    label "Turkmenistan"
  ]
  node [
    id 151
    label "Szwajcaria"
  ]
  node [
    id 152
    label "zwrot"
  ]
  node [
    id 153
    label "organizacja"
  ]
  node [
    id 154
    label "grupa"
  ]
  node [
    id 155
    label "Palau"
  ]
  node [
    id 156
    label "Litwa"
  ]
  node [
    id 157
    label "Gruzja"
  ]
  node [
    id 158
    label "Tajwan"
  ]
  node [
    id 159
    label "Kongo"
  ]
  node [
    id 160
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 161
    label "Honduras"
  ]
  node [
    id 162
    label "Boliwia"
  ]
  node [
    id 163
    label "Uganda"
  ]
  node [
    id 164
    label "Namibia"
  ]
  node [
    id 165
    label "Azerbejd&#380;an"
  ]
  node [
    id 166
    label "Erytrea"
  ]
  node [
    id 167
    label "Gujana"
  ]
  node [
    id 168
    label "Panama"
  ]
  node [
    id 169
    label "Somalia"
  ]
  node [
    id 170
    label "Burundi"
  ]
  node [
    id 171
    label "Tuwalu"
  ]
  node [
    id 172
    label "Libia"
  ]
  node [
    id 173
    label "Katar"
  ]
  node [
    id 174
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 175
    label "Sahara_Zachodnia"
  ]
  node [
    id 176
    label "Trynidad_i_Tobago"
  ]
  node [
    id 177
    label "Gwinea_Bissau"
  ]
  node [
    id 178
    label "Bu&#322;garia"
  ]
  node [
    id 179
    label "Fid&#380;i"
  ]
  node [
    id 180
    label "Nikaragua"
  ]
  node [
    id 181
    label "Tonga"
  ]
  node [
    id 182
    label "Timor_Wschodni"
  ]
  node [
    id 183
    label "Laos"
  ]
  node [
    id 184
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 185
    label "Ghana"
  ]
  node [
    id 186
    label "Brazylia"
  ]
  node [
    id 187
    label "Belgia"
  ]
  node [
    id 188
    label "Irak"
  ]
  node [
    id 189
    label "Peru"
  ]
  node [
    id 190
    label "Arabia_Saudyjska"
  ]
  node [
    id 191
    label "Indonezja"
  ]
  node [
    id 192
    label "Malediwy"
  ]
  node [
    id 193
    label "Afganistan"
  ]
  node [
    id 194
    label "Jordania"
  ]
  node [
    id 195
    label "Kenia"
  ]
  node [
    id 196
    label "Czad"
  ]
  node [
    id 197
    label "Liberia"
  ]
  node [
    id 198
    label "W&#281;gry"
  ]
  node [
    id 199
    label "Chile"
  ]
  node [
    id 200
    label "Mali"
  ]
  node [
    id 201
    label "Armenia"
  ]
  node [
    id 202
    label "Kanada"
  ]
  node [
    id 203
    label "Cypr"
  ]
  node [
    id 204
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 205
    label "Ekwador"
  ]
  node [
    id 206
    label "Mo&#322;dawia"
  ]
  node [
    id 207
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 208
    label "W&#322;ochy"
  ]
  node [
    id 209
    label "Wyspy_Salomona"
  ]
  node [
    id 210
    label "&#321;otwa"
  ]
  node [
    id 211
    label "D&#380;ibuti"
  ]
  node [
    id 212
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 213
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 214
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 215
    label "Portugalia"
  ]
  node [
    id 216
    label "Botswana"
  ]
  node [
    id 217
    label "Maroko"
  ]
  node [
    id 218
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 219
    label "Francja"
  ]
  node [
    id 220
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 221
    label "Dominika"
  ]
  node [
    id 222
    label "Paragwaj"
  ]
  node [
    id 223
    label "Tad&#380;ykistan"
  ]
  node [
    id 224
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 225
    label "Haiti"
  ]
  node [
    id 226
    label "Khitai"
  ]
  node [
    id 227
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 228
    label "mezoregion"
  ]
  node [
    id 229
    label "Jura"
  ]
  node [
    id 230
    label "Beskidy_Zachodnie"
  ]
  node [
    id 231
    label "polski"
  ]
  node [
    id 232
    label "etnolekt"
  ]
  node [
    id 233
    label "po_pomorsku"
  ]
  node [
    id 234
    label "regionalny"
  ]
  node [
    id 235
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 236
    label "pierogi_ruskie"
  ]
  node [
    id 237
    label "oberek"
  ]
  node [
    id 238
    label "po_polsku"
  ]
  node [
    id 239
    label "goniony"
  ]
  node [
    id 240
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 241
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 242
    label "przedmiot"
  ]
  node [
    id 243
    label "polsko"
  ]
  node [
    id 244
    label "Polish"
  ]
  node [
    id 245
    label "mazur"
  ]
  node [
    id 246
    label "skoczny"
  ]
  node [
    id 247
    label "j&#281;zyk"
  ]
  node [
    id 248
    label "chodzony"
  ]
  node [
    id 249
    label "krakowiak"
  ]
  node [
    id 250
    label "ryba_po_grecku"
  ]
  node [
    id 251
    label "polak"
  ]
  node [
    id 252
    label "lacki"
  ]
  node [
    id 253
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 254
    label "sztajer"
  ]
  node [
    id 255
    label "drabant"
  ]
  node [
    id 256
    label "typowy"
  ]
  node [
    id 257
    label "lokalny"
  ]
  node [
    id 258
    label "tradycyjny"
  ]
  node [
    id 259
    label "regionalnie"
  ]
  node [
    id 260
    label "j&#281;zyk_naturalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
]
