graph [
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "temu"
    origin "text"
  ]
  node [
    id 4
    label "kserowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "stara&#263;by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "chyba"
    origin "text"
  ]
  node [
    id 8
    label "skutecznie"
    origin "text"
  ]
  node [
    id 9
    label "konieczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "zrozumienie"
    origin "text"
  ]
  node [
    id 11
    label "coraz"
    origin "text"
  ]
  node [
    id 12
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 13
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 14
    label "komercyjny"
    origin "text"
  ]
  node [
    id 15
    label "wsp&#243;&#322;istnie&#263;"
    origin "text"
  ]
  node [
    id 16
    label "wi&#281;c"
    origin "text"
  ]
  node [
    id 17
    label "amatorski"
    origin "text"
  ]
  node [
    id 18
    label "mama"
    origin "text"
  ]
  node [
    id 19
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 20
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 21
    label "aparat"
    origin "text"
  ]
  node [
    id 22
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "r&#243;wnie"
    origin "text"
  ]
  node [
    id 25
    label "przydatny"
    origin "text"
  ]
  node [
    id 26
    label "jak"
    origin "text"
  ]
  node [
    id 27
    label "p&#322;atny"
    origin "text"
  ]
  node [
    id 28
    label "ksero"
    origin "text"
  ]
  node [
    id 29
    label "pirat"
    origin "text"
  ]
  node [
    id 30
    label "darmo"
    origin "text"
  ]
  node [
    id 31
    label "udost&#281;pnia&#263;"
    origin "text"
  ]
  node [
    id 32
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 33
    label "sklep"
    origin "text"
  ]
  node [
    id 34
    label "trzeba"
    origin "text"
  ]
  node [
    id 35
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "formu&#322;owa&#263;"
  ]
  node [
    id 37
    label "ozdabia&#263;"
  ]
  node [
    id 38
    label "stawia&#263;"
  ]
  node [
    id 39
    label "spell"
  ]
  node [
    id 40
    label "styl"
  ]
  node [
    id 41
    label "skryba"
  ]
  node [
    id 42
    label "read"
  ]
  node [
    id 43
    label "donosi&#263;"
  ]
  node [
    id 44
    label "code"
  ]
  node [
    id 45
    label "tekst"
  ]
  node [
    id 46
    label "dysgrafia"
  ]
  node [
    id 47
    label "dysortografia"
  ]
  node [
    id 48
    label "tworzy&#263;"
  ]
  node [
    id 49
    label "prasa"
  ]
  node [
    id 50
    label "robi&#263;"
  ]
  node [
    id 51
    label "pope&#322;nia&#263;"
  ]
  node [
    id 52
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 53
    label "wytwarza&#263;"
  ]
  node [
    id 54
    label "get"
  ]
  node [
    id 55
    label "consist"
  ]
  node [
    id 56
    label "stanowi&#263;"
  ]
  node [
    id 57
    label "raise"
  ]
  node [
    id 58
    label "spill_the_beans"
  ]
  node [
    id 59
    label "przeby&#263;"
  ]
  node [
    id 60
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 61
    label "zanosi&#263;"
  ]
  node [
    id 62
    label "inform"
  ]
  node [
    id 63
    label "give"
  ]
  node [
    id 64
    label "zu&#380;y&#263;"
  ]
  node [
    id 65
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 66
    label "introduce"
  ]
  node [
    id 67
    label "render"
  ]
  node [
    id 68
    label "ci&#261;&#380;a"
  ]
  node [
    id 69
    label "informowa&#263;"
  ]
  node [
    id 70
    label "komunikowa&#263;"
  ]
  node [
    id 71
    label "convey"
  ]
  node [
    id 72
    label "pozostawia&#263;"
  ]
  node [
    id 73
    label "czyni&#263;"
  ]
  node [
    id 74
    label "wydawa&#263;"
  ]
  node [
    id 75
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 76
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 77
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 78
    label "przewidywa&#263;"
  ]
  node [
    id 79
    label "przyznawa&#263;"
  ]
  node [
    id 80
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 81
    label "go"
  ]
  node [
    id 82
    label "obstawia&#263;"
  ]
  node [
    id 83
    label "umieszcza&#263;"
  ]
  node [
    id 84
    label "ocenia&#263;"
  ]
  node [
    id 85
    label "zastawia&#263;"
  ]
  node [
    id 86
    label "stanowisko"
  ]
  node [
    id 87
    label "znak"
  ]
  node [
    id 88
    label "wskazywa&#263;"
  ]
  node [
    id 89
    label "uruchamia&#263;"
  ]
  node [
    id 90
    label "fundowa&#263;"
  ]
  node [
    id 91
    label "zmienia&#263;"
  ]
  node [
    id 92
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 93
    label "deliver"
  ]
  node [
    id 94
    label "powodowa&#263;"
  ]
  node [
    id 95
    label "wyznacza&#263;"
  ]
  node [
    id 96
    label "przedstawia&#263;"
  ]
  node [
    id 97
    label "wydobywa&#263;"
  ]
  node [
    id 98
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 99
    label "trim"
  ]
  node [
    id 100
    label "gryzipi&#243;rek"
  ]
  node [
    id 101
    label "pisarz"
  ]
  node [
    id 102
    label "ekscerpcja"
  ]
  node [
    id 103
    label "j&#281;zykowo"
  ]
  node [
    id 104
    label "wypowied&#378;"
  ]
  node [
    id 105
    label "redakcja"
  ]
  node [
    id 106
    label "wytw&#243;r"
  ]
  node [
    id 107
    label "pomini&#281;cie"
  ]
  node [
    id 108
    label "dzie&#322;o"
  ]
  node [
    id 109
    label "preparacja"
  ]
  node [
    id 110
    label "odmianka"
  ]
  node [
    id 111
    label "opu&#347;ci&#263;"
  ]
  node [
    id 112
    label "koniektura"
  ]
  node [
    id 113
    label "obelga"
  ]
  node [
    id 114
    label "zesp&#243;&#322;"
  ]
  node [
    id 115
    label "t&#322;oczysko"
  ]
  node [
    id 116
    label "depesza"
  ]
  node [
    id 117
    label "maszyna"
  ]
  node [
    id 118
    label "media"
  ]
  node [
    id 119
    label "napisa&#263;"
  ]
  node [
    id 120
    label "czasopismo"
  ]
  node [
    id 121
    label "dziennikarz_prasowy"
  ]
  node [
    id 122
    label "kiosk"
  ]
  node [
    id 123
    label "maszyna_rolnicza"
  ]
  node [
    id 124
    label "gazeta"
  ]
  node [
    id 125
    label "dysleksja"
  ]
  node [
    id 126
    label "pisanie"
  ]
  node [
    id 127
    label "trzonek"
  ]
  node [
    id 128
    label "reakcja"
  ]
  node [
    id 129
    label "narz&#281;dzie"
  ]
  node [
    id 130
    label "spos&#243;b"
  ]
  node [
    id 131
    label "zbi&#243;r"
  ]
  node [
    id 132
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 133
    label "zachowanie"
  ]
  node [
    id 134
    label "stylik"
  ]
  node [
    id 135
    label "dyscyplina_sportowa"
  ]
  node [
    id 136
    label "handle"
  ]
  node [
    id 137
    label "stroke"
  ]
  node [
    id 138
    label "line"
  ]
  node [
    id 139
    label "charakter"
  ]
  node [
    id 140
    label "natural_language"
  ]
  node [
    id 141
    label "kanon"
  ]
  node [
    id 142
    label "behawior"
  ]
  node [
    id 143
    label "dysgraphia"
  ]
  node [
    id 144
    label "przyzwoity"
  ]
  node [
    id 145
    label "ciekawy"
  ]
  node [
    id 146
    label "jako&#347;"
  ]
  node [
    id 147
    label "jako_tako"
  ]
  node [
    id 148
    label "niez&#322;y"
  ]
  node [
    id 149
    label "dziwny"
  ]
  node [
    id 150
    label "charakterystyczny"
  ]
  node [
    id 151
    label "intensywny"
  ]
  node [
    id 152
    label "udolny"
  ]
  node [
    id 153
    label "skuteczny"
  ]
  node [
    id 154
    label "&#347;mieszny"
  ]
  node [
    id 155
    label "niczegowaty"
  ]
  node [
    id 156
    label "dobrze"
  ]
  node [
    id 157
    label "nieszpetny"
  ]
  node [
    id 158
    label "spory"
  ]
  node [
    id 159
    label "pozytywny"
  ]
  node [
    id 160
    label "korzystny"
  ]
  node [
    id 161
    label "nie&#378;le"
  ]
  node [
    id 162
    label "skromny"
  ]
  node [
    id 163
    label "kulturalny"
  ]
  node [
    id 164
    label "grzeczny"
  ]
  node [
    id 165
    label "stosowny"
  ]
  node [
    id 166
    label "przystojny"
  ]
  node [
    id 167
    label "nale&#380;yty"
  ]
  node [
    id 168
    label "moralny"
  ]
  node [
    id 169
    label "przyzwoicie"
  ]
  node [
    id 170
    label "wystarczaj&#261;cy"
  ]
  node [
    id 171
    label "nietuzinkowy"
  ]
  node [
    id 172
    label "intryguj&#261;cy"
  ]
  node [
    id 173
    label "ch&#281;tny"
  ]
  node [
    id 174
    label "swoisty"
  ]
  node [
    id 175
    label "interesowanie"
  ]
  node [
    id 176
    label "interesuj&#261;cy"
  ]
  node [
    id 177
    label "ciekawie"
  ]
  node [
    id 178
    label "indagator"
  ]
  node [
    id 179
    label "charakterystycznie"
  ]
  node [
    id 180
    label "szczeg&#243;lny"
  ]
  node [
    id 181
    label "wyj&#261;tkowy"
  ]
  node [
    id 182
    label "typowy"
  ]
  node [
    id 183
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 184
    label "podobny"
  ]
  node [
    id 185
    label "dziwnie"
  ]
  node [
    id 186
    label "dziwy"
  ]
  node [
    id 187
    label "inny"
  ]
  node [
    id 188
    label "w_miar&#281;"
  ]
  node [
    id 189
    label "jako_taki"
  ]
  node [
    id 190
    label "poprzedzanie"
  ]
  node [
    id 191
    label "czasoprzestrze&#324;"
  ]
  node [
    id 192
    label "laba"
  ]
  node [
    id 193
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 194
    label "chronometria"
  ]
  node [
    id 195
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 196
    label "rachuba_czasu"
  ]
  node [
    id 197
    label "przep&#322;ywanie"
  ]
  node [
    id 198
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 199
    label "czasokres"
  ]
  node [
    id 200
    label "odczyt"
  ]
  node [
    id 201
    label "chwila"
  ]
  node [
    id 202
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 203
    label "dzieje"
  ]
  node [
    id 204
    label "kategoria_gramatyczna"
  ]
  node [
    id 205
    label "poprzedzenie"
  ]
  node [
    id 206
    label "trawienie"
  ]
  node [
    id 207
    label "pochodzi&#263;"
  ]
  node [
    id 208
    label "period"
  ]
  node [
    id 209
    label "okres_czasu"
  ]
  node [
    id 210
    label "poprzedza&#263;"
  ]
  node [
    id 211
    label "schy&#322;ek"
  ]
  node [
    id 212
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 213
    label "odwlekanie_si&#281;"
  ]
  node [
    id 214
    label "zegar"
  ]
  node [
    id 215
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 216
    label "czwarty_wymiar"
  ]
  node [
    id 217
    label "pochodzenie"
  ]
  node [
    id 218
    label "koniugacja"
  ]
  node [
    id 219
    label "Zeitgeist"
  ]
  node [
    id 220
    label "trawi&#263;"
  ]
  node [
    id 221
    label "pogoda"
  ]
  node [
    id 222
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 223
    label "poprzedzi&#263;"
  ]
  node [
    id 224
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 225
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 226
    label "time_period"
  ]
  node [
    id 227
    label "time"
  ]
  node [
    id 228
    label "blok"
  ]
  node [
    id 229
    label "handout"
  ]
  node [
    id 230
    label "pomiar"
  ]
  node [
    id 231
    label "lecture"
  ]
  node [
    id 232
    label "reading"
  ]
  node [
    id 233
    label "podawanie"
  ]
  node [
    id 234
    label "wyk&#322;ad"
  ]
  node [
    id 235
    label "potrzyma&#263;"
  ]
  node [
    id 236
    label "warunki"
  ]
  node [
    id 237
    label "pok&#243;j"
  ]
  node [
    id 238
    label "atak"
  ]
  node [
    id 239
    label "program"
  ]
  node [
    id 240
    label "zjawisko"
  ]
  node [
    id 241
    label "meteorology"
  ]
  node [
    id 242
    label "weather"
  ]
  node [
    id 243
    label "prognoza_meteorologiczna"
  ]
  node [
    id 244
    label "czas_wolny"
  ]
  node [
    id 245
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 246
    label "metrologia"
  ]
  node [
    id 247
    label "godzinnik"
  ]
  node [
    id 248
    label "bicie"
  ]
  node [
    id 249
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 250
    label "wahad&#322;o"
  ]
  node [
    id 251
    label "kurant"
  ]
  node [
    id 252
    label "cyferblat"
  ]
  node [
    id 253
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 254
    label "nabicie"
  ]
  node [
    id 255
    label "werk"
  ]
  node [
    id 256
    label "czasomierz"
  ]
  node [
    id 257
    label "tyka&#263;"
  ]
  node [
    id 258
    label "tykn&#261;&#263;"
  ]
  node [
    id 259
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 260
    label "urz&#261;dzenie"
  ]
  node [
    id 261
    label "kotwica"
  ]
  node [
    id 262
    label "fleksja"
  ]
  node [
    id 263
    label "liczba"
  ]
  node [
    id 264
    label "coupling"
  ]
  node [
    id 265
    label "osoba"
  ]
  node [
    id 266
    label "tryb"
  ]
  node [
    id 267
    label "czasownik"
  ]
  node [
    id 268
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 269
    label "orz&#281;sek"
  ]
  node [
    id 270
    label "usuwa&#263;"
  ]
  node [
    id 271
    label "lutowa&#263;"
  ]
  node [
    id 272
    label "marnowa&#263;"
  ]
  node [
    id 273
    label "przetrawia&#263;"
  ]
  node [
    id 274
    label "poch&#322;ania&#263;"
  ]
  node [
    id 275
    label "digest"
  ]
  node [
    id 276
    label "metal"
  ]
  node [
    id 277
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 278
    label "sp&#281;dza&#263;"
  ]
  node [
    id 279
    label "digestion"
  ]
  node [
    id 280
    label "unicestwianie"
  ]
  node [
    id 281
    label "sp&#281;dzanie"
  ]
  node [
    id 282
    label "contemplation"
  ]
  node [
    id 283
    label "rozk&#322;adanie"
  ]
  node [
    id 284
    label "marnowanie"
  ]
  node [
    id 285
    label "proces_fizjologiczny"
  ]
  node [
    id 286
    label "przetrawianie"
  ]
  node [
    id 287
    label "perystaltyka"
  ]
  node [
    id 288
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 289
    label "zaczynanie_si&#281;"
  ]
  node [
    id 290
    label "str&#243;j"
  ]
  node [
    id 291
    label "wynikanie"
  ]
  node [
    id 292
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 293
    label "origin"
  ]
  node [
    id 294
    label "background"
  ]
  node [
    id 295
    label "geneza"
  ]
  node [
    id 296
    label "beginning"
  ]
  node [
    id 297
    label "min&#261;&#263;"
  ]
  node [
    id 298
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 299
    label "swimming"
  ]
  node [
    id 300
    label "zago&#347;ci&#263;"
  ]
  node [
    id 301
    label "cross"
  ]
  node [
    id 302
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 303
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 304
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 305
    label "przebywa&#263;"
  ]
  node [
    id 306
    label "pour"
  ]
  node [
    id 307
    label "carry"
  ]
  node [
    id 308
    label "sail"
  ]
  node [
    id 309
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 310
    label "go&#347;ci&#263;"
  ]
  node [
    id 311
    label "mija&#263;"
  ]
  node [
    id 312
    label "proceed"
  ]
  node [
    id 313
    label "mini&#281;cie"
  ]
  node [
    id 314
    label "doznanie"
  ]
  node [
    id 315
    label "zaistnienie"
  ]
  node [
    id 316
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 317
    label "przebycie"
  ]
  node [
    id 318
    label "cruise"
  ]
  node [
    id 319
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 320
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 321
    label "zjawianie_si&#281;"
  ]
  node [
    id 322
    label "przebywanie"
  ]
  node [
    id 323
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 324
    label "mijanie"
  ]
  node [
    id 325
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 326
    label "zaznawanie"
  ]
  node [
    id 327
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 328
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 329
    label "flux"
  ]
  node [
    id 330
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 331
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 332
    label "zrobi&#263;"
  ]
  node [
    id 333
    label "opatrzy&#263;"
  ]
  node [
    id 334
    label "overwhelm"
  ]
  node [
    id 335
    label "opatrywanie"
  ]
  node [
    id 336
    label "odej&#347;cie"
  ]
  node [
    id 337
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 338
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 339
    label "zanikni&#281;cie"
  ]
  node [
    id 340
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 341
    label "ciecz"
  ]
  node [
    id 342
    label "opuszczenie"
  ]
  node [
    id 343
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 344
    label "departure"
  ]
  node [
    id 345
    label "oddalenie_si&#281;"
  ]
  node [
    id 346
    label "date"
  ]
  node [
    id 347
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 348
    label "wynika&#263;"
  ]
  node [
    id 349
    label "fall"
  ]
  node [
    id 350
    label "poby&#263;"
  ]
  node [
    id 351
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 352
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 353
    label "bolt"
  ]
  node [
    id 354
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 355
    label "spowodowa&#263;"
  ]
  node [
    id 356
    label "uda&#263;_si&#281;"
  ]
  node [
    id 357
    label "opatrzenie"
  ]
  node [
    id 358
    label "zdarzenie_si&#281;"
  ]
  node [
    id 359
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 360
    label "progress"
  ]
  node [
    id 361
    label "opatrywa&#263;"
  ]
  node [
    id 362
    label "epoka"
  ]
  node [
    id 363
    label "flow"
  ]
  node [
    id 364
    label "choroba_przyrodzona"
  ]
  node [
    id 365
    label "ciota"
  ]
  node [
    id 366
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 367
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 368
    label "kres"
  ]
  node [
    id 369
    label "przestrze&#324;"
  ]
  node [
    id 370
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 371
    label "odbija&#263;"
  ]
  node [
    id 372
    label "transcript"
  ]
  node [
    id 373
    label "poprawia&#263;_si&#281;"
  ]
  node [
    id 374
    label "pull"
  ]
  node [
    id 375
    label "zwierciad&#322;o"
  ]
  node [
    id 376
    label "powiela&#263;"
  ]
  node [
    id 377
    label "nachodzi&#263;"
  ]
  node [
    id 378
    label "odrabia&#263;"
  ]
  node [
    id 379
    label "pokonywa&#263;"
  ]
  node [
    id 380
    label "od&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 381
    label "odzwierciedla&#263;"
  ]
  node [
    id 382
    label "pilnowa&#263;"
  ]
  node [
    id 383
    label "uszkadza&#263;"
  ]
  node [
    id 384
    label "return"
  ]
  node [
    id 385
    label "odskakiwa&#263;"
  ]
  node [
    id 386
    label "odchodzi&#263;"
  ]
  node [
    id 387
    label "otwiera&#263;"
  ]
  node [
    id 388
    label "r&#243;&#380;ni&#263;_si&#281;"
  ]
  node [
    id 389
    label "rusza&#263;"
  ]
  node [
    id 390
    label "zabiera&#263;"
  ]
  node [
    id 391
    label "utr&#261;ca&#263;"
  ]
  node [
    id 392
    label "zaprasza&#263;"
  ]
  node [
    id 393
    label "zostawia&#263;"
  ]
  node [
    id 394
    label "odgrywa&#263;_si&#281;"
  ]
  node [
    id 395
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 396
    label "u&#380;ywa&#263;"
  ]
  node [
    id 397
    label "rozbija&#263;"
  ]
  node [
    id 398
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 399
    label "odciska&#263;"
  ]
  node [
    id 400
    label "wynagradza&#263;"
  ]
  node [
    id 401
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 402
    label "odpiera&#263;"
  ]
  node [
    id 403
    label "&#347;miga&#263;"
  ]
  node [
    id 404
    label "przybija&#263;"
  ]
  node [
    id 405
    label "zbacza&#263;"
  ]
  node [
    id 406
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 407
    label "odpowiednio"
  ]
  node [
    id 408
    label "dobroczynnie"
  ]
  node [
    id 409
    label "moralnie"
  ]
  node [
    id 410
    label "korzystnie"
  ]
  node [
    id 411
    label "pozytywnie"
  ]
  node [
    id 412
    label "lepiej"
  ]
  node [
    id 413
    label "wiele"
  ]
  node [
    id 414
    label "pomy&#347;lnie"
  ]
  node [
    id 415
    label "dobry"
  ]
  node [
    id 416
    label "poskutkowanie"
  ]
  node [
    id 417
    label "sprawny"
  ]
  node [
    id 418
    label "skutkowanie"
  ]
  node [
    id 419
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 420
    label "przymus"
  ]
  node [
    id 421
    label "wym&#243;g"
  ]
  node [
    id 422
    label "obligatoryjno&#347;&#263;"
  ]
  node [
    id 423
    label "operator_modalny"
  ]
  node [
    id 424
    label "condition"
  ]
  node [
    id 425
    label "stan"
  ]
  node [
    id 426
    label "potrzeba"
  ]
  node [
    id 427
    label "need"
  ]
  node [
    id 428
    label "umowa"
  ]
  node [
    id 429
    label "presja"
  ]
  node [
    id 430
    label "obowi&#261;zkowo&#347;&#263;"
  ]
  node [
    id 431
    label "sk&#322;adnik"
  ]
  node [
    id 432
    label "sytuacja"
  ]
  node [
    id 433
    label "wydarzenie"
  ]
  node [
    id 434
    label "pos&#322;uchanie"
  ]
  node [
    id 435
    label "skumanie"
  ]
  node [
    id 436
    label "appreciation"
  ]
  node [
    id 437
    label "creation"
  ]
  node [
    id 438
    label "zorientowanie"
  ]
  node [
    id 439
    label "ocenienie"
  ]
  node [
    id 440
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 441
    label "clasp"
  ]
  node [
    id 442
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 443
    label "poczucie"
  ]
  node [
    id 444
    label "sympathy"
  ]
  node [
    id 445
    label "przem&#243;wienie"
  ]
  node [
    id 446
    label "follow-up"
  ]
  node [
    id 447
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 448
    label "appraisal"
  ]
  node [
    id 449
    label "potraktowanie"
  ]
  node [
    id 450
    label "przyznanie"
  ]
  node [
    id 451
    label "dostanie"
  ]
  node [
    id 452
    label "wywy&#380;szenie"
  ]
  node [
    id 453
    label "przewidzenie"
  ]
  node [
    id 454
    label "favor"
  ]
  node [
    id 455
    label "dobro&#263;"
  ]
  node [
    id 456
    label "nastawienie"
  ]
  node [
    id 457
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 458
    label "wola"
  ]
  node [
    id 459
    label "ekstraspekcja"
  ]
  node [
    id 460
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 461
    label "feeling"
  ]
  node [
    id 462
    label "wiedza"
  ]
  node [
    id 463
    label "smell"
  ]
  node [
    id 464
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 465
    label "opanowanie"
  ]
  node [
    id 466
    label "os&#322;upienie"
  ]
  node [
    id 467
    label "zareagowanie"
  ]
  node [
    id 468
    label "intuition"
  ]
  node [
    id 469
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 470
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 471
    label "spotkanie"
  ]
  node [
    id 472
    label "wys&#322;uchanie"
  ]
  node [
    id 473
    label "porobienie"
  ]
  node [
    id 474
    label "audience"
  ]
  node [
    id 475
    label "w&#322;&#261;czenie"
  ]
  node [
    id 476
    label "zrobienie"
  ]
  node [
    id 477
    label "obronienie"
  ]
  node [
    id 478
    label "wydanie"
  ]
  node [
    id 479
    label "wyg&#322;oszenie"
  ]
  node [
    id 480
    label "oddzia&#322;anie"
  ]
  node [
    id 481
    label "address"
  ]
  node [
    id 482
    label "wydobycie"
  ]
  node [
    id 483
    label "wyst&#261;pienie"
  ]
  node [
    id 484
    label "talk"
  ]
  node [
    id 485
    label "odzyskanie"
  ]
  node [
    id 486
    label "sermon"
  ]
  node [
    id 487
    label "kierunek"
  ]
  node [
    id 488
    label "wyznaczenie"
  ]
  node [
    id 489
    label "przyczynienie_si&#281;"
  ]
  node [
    id 490
    label "zwr&#243;cenie"
  ]
  node [
    id 491
    label "cz&#281;sty"
  ]
  node [
    id 492
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 493
    label "wielokrotnie"
  ]
  node [
    id 494
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 495
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 496
    label "strategia"
  ]
  node [
    id 497
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 498
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 499
    label "plan"
  ]
  node [
    id 500
    label "operacja"
  ]
  node [
    id 501
    label "metoda"
  ]
  node [
    id 502
    label "gra"
  ]
  node [
    id 503
    label "pocz&#261;tki"
  ]
  node [
    id 504
    label "wzorzec_projektowy"
  ]
  node [
    id 505
    label "dziedzina"
  ]
  node [
    id 506
    label "doktryna"
  ]
  node [
    id 507
    label "wrinkle"
  ]
  node [
    id 508
    label "dokument"
  ]
  node [
    id 509
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 510
    label "absolutorium"
  ]
  node [
    id 511
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 512
    label "dzia&#322;anie"
  ]
  node [
    id 513
    label "activity"
  ]
  node [
    id 514
    label "skomercjalizowanie"
  ]
  node [
    id 515
    label "komercjalizowanie"
  ]
  node [
    id 516
    label "rynkowy"
  ]
  node [
    id 517
    label "masowy"
  ]
  node [
    id 518
    label "komercyjnie"
  ]
  node [
    id 519
    label "popularny"
  ]
  node [
    id 520
    label "niski"
  ]
  node [
    id 521
    label "seryjny"
  ]
  node [
    id 522
    label "masowo"
  ]
  node [
    id 523
    label "urynkawianie"
  ]
  node [
    id 524
    label "urynkowienie"
  ]
  node [
    id 525
    label "rynkowo"
  ]
  node [
    id 526
    label "sprzedawanie"
  ]
  node [
    id 527
    label "sprzedanie"
  ]
  node [
    id 528
    label "popularnie"
  ]
  node [
    id 529
    label "wsp&#243;&#322;wyst&#281;powa&#263;"
  ]
  node [
    id 530
    label "niezawodowy"
  ]
  node [
    id 531
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 532
    label "po_laicku"
  ]
  node [
    id 533
    label "s&#322;aby"
  ]
  node [
    id 534
    label "po_amatorsku"
  ]
  node [
    id 535
    label "niezawodowo"
  ]
  node [
    id 536
    label "nierzetelny"
  ]
  node [
    id 537
    label "amatorsko"
  ]
  node [
    id 538
    label "nieprzekonuj&#261;cy"
  ]
  node [
    id 539
    label "nieporz&#261;dny"
  ]
  node [
    id 540
    label "niewiarygodny"
  ]
  node [
    id 541
    label "niedok&#322;adny"
  ]
  node [
    id 542
    label "nierzetelnie"
  ]
  node [
    id 543
    label "nietrwa&#322;y"
  ]
  node [
    id 544
    label "mizerny"
  ]
  node [
    id 545
    label "marnie"
  ]
  node [
    id 546
    label "delikatny"
  ]
  node [
    id 547
    label "po&#347;ledni"
  ]
  node [
    id 548
    label "niezdrowy"
  ]
  node [
    id 549
    label "z&#322;y"
  ]
  node [
    id 550
    label "nieumiej&#281;tny"
  ]
  node [
    id 551
    label "s&#322;abo"
  ]
  node [
    id 552
    label "nieznaczny"
  ]
  node [
    id 553
    label "lura"
  ]
  node [
    id 554
    label "nieudany"
  ]
  node [
    id 555
    label "s&#322;abowity"
  ]
  node [
    id 556
    label "zawodny"
  ]
  node [
    id 557
    label "&#322;agodny"
  ]
  node [
    id 558
    label "md&#322;y"
  ]
  node [
    id 559
    label "niedoskona&#322;y"
  ]
  node [
    id 560
    label "przemijaj&#261;cy"
  ]
  node [
    id 561
    label "niemocny"
  ]
  node [
    id 562
    label "niefajny"
  ]
  node [
    id 563
    label "kiepsko"
  ]
  node [
    id 564
    label "nale&#380;ny"
  ]
  node [
    id 565
    label "uprawniony"
  ]
  node [
    id 566
    label "zasadniczy"
  ]
  node [
    id 567
    label "stosownie"
  ]
  node [
    id 568
    label "taki"
  ]
  node [
    id 569
    label "prawdziwy"
  ]
  node [
    id 570
    label "ten"
  ]
  node [
    id 571
    label "artlessly"
  ]
  node [
    id 572
    label "przodkini"
  ]
  node [
    id 573
    label "matka_zast&#281;pcza"
  ]
  node [
    id 574
    label "matczysko"
  ]
  node [
    id 575
    label "rodzice"
  ]
  node [
    id 576
    label "stara"
  ]
  node [
    id 577
    label "macierz"
  ]
  node [
    id 578
    label "rodzic"
  ]
  node [
    id 579
    label "Matka_Boska"
  ]
  node [
    id 580
    label "macocha"
  ]
  node [
    id 581
    label "starzy"
  ]
  node [
    id 582
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 583
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 584
    label "pokolenie"
  ]
  node [
    id 585
    label "wapniaki"
  ]
  node [
    id 586
    label "opiekun"
  ]
  node [
    id 587
    label "wapniak"
  ]
  node [
    id 588
    label "rodzic_chrzestny"
  ]
  node [
    id 589
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 590
    label "krewna"
  ]
  node [
    id 591
    label "matka"
  ]
  node [
    id 592
    label "&#380;ona"
  ]
  node [
    id 593
    label "kobieta"
  ]
  node [
    id 594
    label "partnerka"
  ]
  node [
    id 595
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 596
    label "matuszka"
  ]
  node [
    id 597
    label "parametryzacja"
  ]
  node [
    id 598
    label "pa&#324;stwo"
  ]
  node [
    id 599
    label "poj&#281;cie"
  ]
  node [
    id 600
    label "mod"
  ]
  node [
    id 601
    label "patriota"
  ]
  node [
    id 602
    label "m&#281;&#380;atka"
  ]
  node [
    id 603
    label "doba"
  ]
  node [
    id 604
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 605
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 606
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 607
    label "teraz"
  ]
  node [
    id 608
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 609
    label "jednocze&#347;nie"
  ]
  node [
    id 610
    label "tydzie&#324;"
  ]
  node [
    id 611
    label "noc"
  ]
  node [
    id 612
    label "dzie&#324;"
  ]
  node [
    id 613
    label "godzina"
  ]
  node [
    id 614
    label "long_time"
  ]
  node [
    id 615
    label "jednostka_geologiczna"
  ]
  node [
    id 616
    label "ludzko&#347;&#263;"
  ]
  node [
    id 617
    label "asymilowanie"
  ]
  node [
    id 618
    label "asymilowa&#263;"
  ]
  node [
    id 619
    label "os&#322;abia&#263;"
  ]
  node [
    id 620
    label "posta&#263;"
  ]
  node [
    id 621
    label "hominid"
  ]
  node [
    id 622
    label "podw&#322;adny"
  ]
  node [
    id 623
    label "os&#322;abianie"
  ]
  node [
    id 624
    label "g&#322;owa"
  ]
  node [
    id 625
    label "figura"
  ]
  node [
    id 626
    label "portrecista"
  ]
  node [
    id 627
    label "dwun&#243;g"
  ]
  node [
    id 628
    label "profanum"
  ]
  node [
    id 629
    label "mikrokosmos"
  ]
  node [
    id 630
    label "nasada"
  ]
  node [
    id 631
    label "duch"
  ]
  node [
    id 632
    label "antropochoria"
  ]
  node [
    id 633
    label "wz&#243;r"
  ]
  node [
    id 634
    label "senior"
  ]
  node [
    id 635
    label "oddzia&#322;ywanie"
  ]
  node [
    id 636
    label "Adam"
  ]
  node [
    id 637
    label "homo_sapiens"
  ]
  node [
    id 638
    label "polifag"
  ]
  node [
    id 639
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 640
    label "cz&#322;owiekowate"
  ]
  node [
    id 641
    label "konsument"
  ]
  node [
    id 642
    label "istota_&#380;ywa"
  ]
  node [
    id 643
    label "pracownik"
  ]
  node [
    id 644
    label "Chocho&#322;"
  ]
  node [
    id 645
    label "Herkules_Poirot"
  ]
  node [
    id 646
    label "Edyp"
  ]
  node [
    id 647
    label "parali&#380;owa&#263;"
  ]
  node [
    id 648
    label "Harry_Potter"
  ]
  node [
    id 649
    label "Casanova"
  ]
  node [
    id 650
    label "Zgredek"
  ]
  node [
    id 651
    label "Gargantua"
  ]
  node [
    id 652
    label "Winnetou"
  ]
  node [
    id 653
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 654
    label "Dulcynea"
  ]
  node [
    id 655
    label "person"
  ]
  node [
    id 656
    label "Plastu&#347;"
  ]
  node [
    id 657
    label "Quasimodo"
  ]
  node [
    id 658
    label "Sherlock_Holmes"
  ]
  node [
    id 659
    label "Faust"
  ]
  node [
    id 660
    label "Wallenrod"
  ]
  node [
    id 661
    label "Dwukwiat"
  ]
  node [
    id 662
    label "Don_Juan"
  ]
  node [
    id 663
    label "Don_Kiszot"
  ]
  node [
    id 664
    label "Hamlet"
  ]
  node [
    id 665
    label "Werter"
  ]
  node [
    id 666
    label "istota"
  ]
  node [
    id 667
    label "Szwejk"
  ]
  node [
    id 668
    label "doros&#322;y"
  ]
  node [
    id 669
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 670
    label "jajko"
  ]
  node [
    id 671
    label "zwierzchnik"
  ]
  node [
    id 672
    label "feuda&#322;"
  ]
  node [
    id 673
    label "starzec"
  ]
  node [
    id 674
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 675
    label "zawodnik"
  ]
  node [
    id 676
    label "komendancja"
  ]
  node [
    id 677
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 678
    label "de-escalation"
  ]
  node [
    id 679
    label "powodowanie"
  ]
  node [
    id 680
    label "os&#322;abienie"
  ]
  node [
    id 681
    label "kondycja_fizyczna"
  ]
  node [
    id 682
    label "os&#322;abi&#263;"
  ]
  node [
    id 683
    label "debilitation"
  ]
  node [
    id 684
    label "zdrowie"
  ]
  node [
    id 685
    label "zmniejszanie"
  ]
  node [
    id 686
    label "s&#322;abszy"
  ]
  node [
    id 687
    label "pogarszanie"
  ]
  node [
    id 688
    label "suppress"
  ]
  node [
    id 689
    label "zmniejsza&#263;"
  ]
  node [
    id 690
    label "bate"
  ]
  node [
    id 691
    label "asymilowanie_si&#281;"
  ]
  node [
    id 692
    label "absorption"
  ]
  node [
    id 693
    label "pobieranie"
  ]
  node [
    id 694
    label "czerpanie"
  ]
  node [
    id 695
    label "acquisition"
  ]
  node [
    id 696
    label "zmienianie"
  ]
  node [
    id 697
    label "organizm"
  ]
  node [
    id 698
    label "assimilation"
  ]
  node [
    id 699
    label "upodabnianie"
  ]
  node [
    id 700
    label "g&#322;oska"
  ]
  node [
    id 701
    label "kultura"
  ]
  node [
    id 702
    label "grupa"
  ]
  node [
    id 703
    label "fonetyka"
  ]
  node [
    id 704
    label "assimilate"
  ]
  node [
    id 705
    label "dostosowywa&#263;"
  ]
  node [
    id 706
    label "dostosowa&#263;"
  ]
  node [
    id 707
    label "przejmowa&#263;"
  ]
  node [
    id 708
    label "upodobni&#263;"
  ]
  node [
    id 709
    label "przej&#261;&#263;"
  ]
  node [
    id 710
    label "upodabnia&#263;"
  ]
  node [
    id 711
    label "pobiera&#263;"
  ]
  node [
    id 712
    label "pobra&#263;"
  ]
  node [
    id 713
    label "charakterystyka"
  ]
  node [
    id 714
    label "zaistnie&#263;"
  ]
  node [
    id 715
    label "cecha"
  ]
  node [
    id 716
    label "Osjan"
  ]
  node [
    id 717
    label "kto&#347;"
  ]
  node [
    id 718
    label "wygl&#261;d"
  ]
  node [
    id 719
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 720
    label "osobowo&#347;&#263;"
  ]
  node [
    id 721
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 722
    label "Aspazja"
  ]
  node [
    id 723
    label "punkt_widzenia"
  ]
  node [
    id 724
    label "kompleksja"
  ]
  node [
    id 725
    label "wytrzyma&#263;"
  ]
  node [
    id 726
    label "budowa"
  ]
  node [
    id 727
    label "formacja"
  ]
  node [
    id 728
    label "pozosta&#263;"
  ]
  node [
    id 729
    label "point"
  ]
  node [
    id 730
    label "przedstawienie"
  ]
  node [
    id 731
    label "go&#347;&#263;"
  ]
  node [
    id 732
    label "zapis"
  ]
  node [
    id 733
    label "figure"
  ]
  node [
    id 734
    label "typ"
  ]
  node [
    id 735
    label "mildew"
  ]
  node [
    id 736
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 737
    label "ideal"
  ]
  node [
    id 738
    label "rule"
  ]
  node [
    id 739
    label "ruch"
  ]
  node [
    id 740
    label "dekal"
  ]
  node [
    id 741
    label "motyw"
  ]
  node [
    id 742
    label "projekt"
  ]
  node [
    id 743
    label "pryncypa&#322;"
  ]
  node [
    id 744
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 745
    label "kszta&#322;t"
  ]
  node [
    id 746
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 747
    label "kierowa&#263;"
  ]
  node [
    id 748
    label "alkohol"
  ]
  node [
    id 749
    label "zdolno&#347;&#263;"
  ]
  node [
    id 750
    label "&#380;ycie"
  ]
  node [
    id 751
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 752
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 753
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 754
    label "sztuka"
  ]
  node [
    id 755
    label "dekiel"
  ]
  node [
    id 756
    label "ro&#347;lina"
  ]
  node [
    id 757
    label "&#347;ci&#281;cie"
  ]
  node [
    id 758
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 759
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 760
    label "&#347;ci&#281;gno"
  ]
  node [
    id 761
    label "noosfera"
  ]
  node [
    id 762
    label "byd&#322;o"
  ]
  node [
    id 763
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 764
    label "makrocefalia"
  ]
  node [
    id 765
    label "obiekt"
  ]
  node [
    id 766
    label "ucho"
  ]
  node [
    id 767
    label "g&#243;ra"
  ]
  node [
    id 768
    label "m&#243;zg"
  ]
  node [
    id 769
    label "kierownictwo"
  ]
  node [
    id 770
    label "fryzura"
  ]
  node [
    id 771
    label "umys&#322;"
  ]
  node [
    id 772
    label "cia&#322;o"
  ]
  node [
    id 773
    label "cz&#322;onek"
  ]
  node [
    id 774
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 775
    label "czaszka"
  ]
  node [
    id 776
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 777
    label "hipnotyzowanie"
  ]
  node [
    id 778
    label "&#347;lad"
  ]
  node [
    id 779
    label "docieranie"
  ]
  node [
    id 780
    label "natural_process"
  ]
  node [
    id 781
    label "reakcja_chemiczna"
  ]
  node [
    id 782
    label "wdzieranie_si&#281;"
  ]
  node [
    id 783
    label "act"
  ]
  node [
    id 784
    label "rezultat"
  ]
  node [
    id 785
    label "lobbysta"
  ]
  node [
    id 786
    label "allochoria"
  ]
  node [
    id 787
    label "fotograf"
  ]
  node [
    id 788
    label "malarz"
  ]
  node [
    id 789
    label "artysta"
  ]
  node [
    id 790
    label "p&#322;aszczyzna"
  ]
  node [
    id 791
    label "przedmiot"
  ]
  node [
    id 792
    label "bierka_szachowa"
  ]
  node [
    id 793
    label "obiekt_matematyczny"
  ]
  node [
    id 794
    label "gestaltyzm"
  ]
  node [
    id 795
    label "obraz"
  ]
  node [
    id 796
    label "rzecz"
  ]
  node [
    id 797
    label "d&#378;wi&#281;k"
  ]
  node [
    id 798
    label "character"
  ]
  node [
    id 799
    label "rze&#378;ba"
  ]
  node [
    id 800
    label "stylistyka"
  ]
  node [
    id 801
    label "miejsce"
  ]
  node [
    id 802
    label "antycypacja"
  ]
  node [
    id 803
    label "ornamentyka"
  ]
  node [
    id 804
    label "informacja"
  ]
  node [
    id 805
    label "facet"
  ]
  node [
    id 806
    label "popis"
  ]
  node [
    id 807
    label "wiersz"
  ]
  node [
    id 808
    label "symetria"
  ]
  node [
    id 809
    label "lingwistyka_kognitywna"
  ]
  node [
    id 810
    label "karta"
  ]
  node [
    id 811
    label "shape"
  ]
  node [
    id 812
    label "podzbi&#243;r"
  ]
  node [
    id 813
    label "perspektywa"
  ]
  node [
    id 814
    label "nak&#322;adka"
  ]
  node [
    id 815
    label "li&#347;&#263;"
  ]
  node [
    id 816
    label "jama_gard&#322;owa"
  ]
  node [
    id 817
    label "rezonator"
  ]
  node [
    id 818
    label "podstawa"
  ]
  node [
    id 819
    label "base"
  ]
  node [
    id 820
    label "piek&#322;o"
  ]
  node [
    id 821
    label "human_body"
  ]
  node [
    id 822
    label "ofiarowywanie"
  ]
  node [
    id 823
    label "sfera_afektywna"
  ]
  node [
    id 824
    label "nekromancja"
  ]
  node [
    id 825
    label "Po&#347;wist"
  ]
  node [
    id 826
    label "podekscytowanie"
  ]
  node [
    id 827
    label "deformowanie"
  ]
  node [
    id 828
    label "sumienie"
  ]
  node [
    id 829
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 830
    label "deformowa&#263;"
  ]
  node [
    id 831
    label "psychika"
  ]
  node [
    id 832
    label "zjawa"
  ]
  node [
    id 833
    label "zmar&#322;y"
  ]
  node [
    id 834
    label "istota_nadprzyrodzona"
  ]
  node [
    id 835
    label "power"
  ]
  node [
    id 836
    label "entity"
  ]
  node [
    id 837
    label "ofiarowywa&#263;"
  ]
  node [
    id 838
    label "oddech"
  ]
  node [
    id 839
    label "seksualno&#347;&#263;"
  ]
  node [
    id 840
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 841
    label "byt"
  ]
  node [
    id 842
    label "si&#322;a"
  ]
  node [
    id 843
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 844
    label "ego"
  ]
  node [
    id 845
    label "ofiarowanie"
  ]
  node [
    id 846
    label "fizjonomia"
  ]
  node [
    id 847
    label "kompleks"
  ]
  node [
    id 848
    label "zapalno&#347;&#263;"
  ]
  node [
    id 849
    label "T&#281;sknica"
  ]
  node [
    id 850
    label "ofiarowa&#263;"
  ]
  node [
    id 851
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 852
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 853
    label "passion"
  ]
  node [
    id 854
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 855
    label "atom"
  ]
  node [
    id 856
    label "odbicie"
  ]
  node [
    id 857
    label "przyroda"
  ]
  node [
    id 858
    label "Ziemia"
  ]
  node [
    id 859
    label "kosmos"
  ]
  node [
    id 860
    label "miniatura"
  ]
  node [
    id 861
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 862
    label "zadzwoni&#263;"
  ]
  node [
    id 863
    label "zi&#243;&#322;ko"
  ]
  node [
    id 864
    label "mat&#243;wka"
  ]
  node [
    id 865
    label "celownik"
  ]
  node [
    id 866
    label "przyrz&#261;d"
  ]
  node [
    id 867
    label "obiektyw"
  ]
  node [
    id 868
    label "odleg&#322;o&#347;&#263;_hiperfokalna"
  ]
  node [
    id 869
    label "lampa_b&#322;yskowa"
  ]
  node [
    id 870
    label "mikrotelefon"
  ]
  node [
    id 871
    label "orygina&#322;"
  ]
  node [
    id 872
    label "w&#322;adza"
  ]
  node [
    id 873
    label "miech"
  ]
  node [
    id 874
    label "dzwoni&#263;"
  ]
  node [
    id 875
    label "ciemnia_optyczna"
  ]
  node [
    id 876
    label "spust"
  ]
  node [
    id 877
    label "wyzwalacz"
  ]
  node [
    id 878
    label "dekielek"
  ]
  node [
    id 879
    label "wy&#347;wietlacz"
  ]
  node [
    id 880
    label "device"
  ]
  node [
    id 881
    label "dzwonienie"
  ]
  node [
    id 882
    label "migawka"
  ]
  node [
    id 883
    label "aparatownia"
  ]
  node [
    id 884
    label "Mazowsze"
  ]
  node [
    id 885
    label "odm&#322;adzanie"
  ]
  node [
    id 886
    label "&#346;wietliki"
  ]
  node [
    id 887
    label "whole"
  ]
  node [
    id 888
    label "skupienie"
  ]
  node [
    id 889
    label "The_Beatles"
  ]
  node [
    id 890
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 891
    label "odm&#322;adza&#263;"
  ]
  node [
    id 892
    label "zabudowania"
  ]
  node [
    id 893
    label "group"
  ]
  node [
    id 894
    label "zespolik"
  ]
  node [
    id 895
    label "schorzenie"
  ]
  node [
    id 896
    label "Depeche_Mode"
  ]
  node [
    id 897
    label "batch"
  ]
  node [
    id 898
    label "odm&#322;odzenie"
  ]
  node [
    id 899
    label "utensylia"
  ]
  node [
    id 900
    label "kom&#243;rka"
  ]
  node [
    id 901
    label "furnishing"
  ]
  node [
    id 902
    label "zabezpieczenie"
  ]
  node [
    id 903
    label "wyrz&#261;dzenie"
  ]
  node [
    id 904
    label "zagospodarowanie"
  ]
  node [
    id 905
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 906
    label "ig&#322;a"
  ]
  node [
    id 907
    label "wirnik"
  ]
  node [
    id 908
    label "aparatura"
  ]
  node [
    id 909
    label "system_energetyczny"
  ]
  node [
    id 910
    label "impulsator"
  ]
  node [
    id 911
    label "mechanizm"
  ]
  node [
    id 912
    label "sprz&#281;t"
  ]
  node [
    id 913
    label "czynno&#347;&#263;"
  ]
  node [
    id 914
    label "blokowanie"
  ]
  node [
    id 915
    label "set"
  ]
  node [
    id 916
    label "zablokowanie"
  ]
  node [
    id 917
    label "przygotowanie"
  ]
  node [
    id 918
    label "komora"
  ]
  node [
    id 919
    label "j&#281;zyk"
  ]
  node [
    id 920
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 921
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 922
    label "model"
  ]
  node [
    id 923
    label "bratek"
  ]
  node [
    id 924
    label "struktura"
  ]
  node [
    id 925
    label "prawo"
  ]
  node [
    id 926
    label "rz&#261;dzenie"
  ]
  node [
    id 927
    label "panowanie"
  ]
  node [
    id 928
    label "Kreml"
  ]
  node [
    id 929
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 930
    label "wydolno&#347;&#263;"
  ]
  node [
    id 931
    label "rz&#261;d"
  ]
  node [
    id 932
    label "aparat_fotograficzny"
  ]
  node [
    id 933
    label "spadochron"
  ]
  node [
    id 934
    label "snapshot"
  ]
  node [
    id 935
    label "&#322;&#243;dzki"
  ]
  node [
    id 936
    label "film"
  ]
  node [
    id 937
    label "bilet_komunikacji_miejskiej"
  ]
  node [
    id 938
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 939
    label "beczkowa&#263;"
  ]
  node [
    id 940
    label "soczewka"
  ]
  node [
    id 941
    label "przys&#322;ona"
  ]
  node [
    id 942
    label "decentracja"
  ]
  node [
    id 943
    label "filtr_fotograficzny"
  ]
  node [
    id 944
    label "przeziernik"
  ]
  node [
    id 945
    label "geodezja"
  ]
  node [
    id 946
    label "przypadek"
  ]
  node [
    id 947
    label "meta"
  ]
  node [
    id 948
    label "wizjer"
  ]
  node [
    id 949
    label "wy&#347;cig"
  ]
  node [
    id 950
    label "bro&#324;_palna"
  ]
  node [
    id 951
    label "szczerbina"
  ]
  node [
    id 952
    label "czapka"
  ]
  node [
    id 953
    label "piasta"
  ]
  node [
    id 954
    label "ko&#322;pak"
  ]
  node [
    id 955
    label "obiektyw_fotograficzny"
  ]
  node [
    id 956
    label "os&#322;ona"
  ]
  node [
    id 957
    label "k&#243;&#322;ko"
  ]
  node [
    id 958
    label "os&#322;onka"
  ]
  node [
    id 959
    label "szybka"
  ]
  node [
    id 960
    label "d&#378;wignia"
  ]
  node [
    id 961
    label "wylot"
  ]
  node [
    id 962
    label "przy&#347;piesznik"
  ]
  node [
    id 963
    label "bag"
  ]
  node [
    id 964
    label "sakwa"
  ]
  node [
    id 965
    label "torba"
  ]
  node [
    id 966
    label "w&#243;r"
  ]
  node [
    id 967
    label "miesi&#261;c"
  ]
  node [
    id 968
    label "ekran"
  ]
  node [
    id 969
    label "telefon"
  ]
  node [
    id 970
    label "handset"
  ]
  node [
    id 971
    label "call"
  ]
  node [
    id 972
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 973
    label "dzwonek"
  ]
  node [
    id 974
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 975
    label "sound"
  ]
  node [
    id 976
    label "bi&#263;"
  ]
  node [
    id 977
    label "brzmie&#263;"
  ]
  node [
    id 978
    label "drynda&#263;"
  ]
  node [
    id 979
    label "brz&#281;cze&#263;"
  ]
  node [
    id 980
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 981
    label "jingle"
  ]
  node [
    id 982
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 983
    label "zabi&#263;"
  ]
  node [
    id 984
    label "zadrynda&#263;"
  ]
  node [
    id 985
    label "zabrzmie&#263;"
  ]
  node [
    id 986
    label "nacisn&#261;&#263;"
  ]
  node [
    id 987
    label "dodzwanianie_si&#281;"
  ]
  node [
    id 988
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 989
    label "wydzwanianie"
  ]
  node [
    id 990
    label "naciskanie"
  ]
  node [
    id 991
    label "brzmienie"
  ]
  node [
    id 992
    label "wybijanie"
  ]
  node [
    id 993
    label "dryndanie"
  ]
  node [
    id 994
    label "dodzwonienie_si&#281;"
  ]
  node [
    id 995
    label "wydzwonienie"
  ]
  node [
    id 996
    label "pomieszczenie"
  ]
  node [
    id 997
    label "nicpo&#324;"
  ]
  node [
    id 998
    label "agent"
  ]
  node [
    id 999
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1000
    label "mie&#263;_miejsce"
  ]
  node [
    id 1001
    label "equal"
  ]
  node [
    id 1002
    label "trwa&#263;"
  ]
  node [
    id 1003
    label "chodzi&#263;"
  ]
  node [
    id 1004
    label "si&#281;ga&#263;"
  ]
  node [
    id 1005
    label "obecno&#347;&#263;"
  ]
  node [
    id 1006
    label "stand"
  ]
  node [
    id 1007
    label "uczestniczy&#263;"
  ]
  node [
    id 1008
    label "participate"
  ]
  node [
    id 1009
    label "istnie&#263;"
  ]
  node [
    id 1010
    label "pozostawa&#263;"
  ]
  node [
    id 1011
    label "zostawa&#263;"
  ]
  node [
    id 1012
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1013
    label "adhere"
  ]
  node [
    id 1014
    label "compass"
  ]
  node [
    id 1015
    label "korzysta&#263;"
  ]
  node [
    id 1016
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1017
    label "dociera&#263;"
  ]
  node [
    id 1018
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1019
    label "mierzy&#263;"
  ]
  node [
    id 1020
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1021
    label "exsert"
  ]
  node [
    id 1022
    label "being"
  ]
  node [
    id 1023
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1024
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1025
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1026
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1027
    label "run"
  ]
  node [
    id 1028
    label "bangla&#263;"
  ]
  node [
    id 1029
    label "przebiega&#263;"
  ]
  node [
    id 1030
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1031
    label "bywa&#263;"
  ]
  node [
    id 1032
    label "dziama&#263;"
  ]
  node [
    id 1033
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1034
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1035
    label "para"
  ]
  node [
    id 1036
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1037
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1038
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1039
    label "krok"
  ]
  node [
    id 1040
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1041
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1042
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1043
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1044
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1045
    label "continue"
  ]
  node [
    id 1046
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1047
    label "Ohio"
  ]
  node [
    id 1048
    label "wci&#281;cie"
  ]
  node [
    id 1049
    label "Nowy_York"
  ]
  node [
    id 1050
    label "warstwa"
  ]
  node [
    id 1051
    label "samopoczucie"
  ]
  node [
    id 1052
    label "Illinois"
  ]
  node [
    id 1053
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1054
    label "state"
  ]
  node [
    id 1055
    label "Jukatan"
  ]
  node [
    id 1056
    label "Kalifornia"
  ]
  node [
    id 1057
    label "Wirginia"
  ]
  node [
    id 1058
    label "wektor"
  ]
  node [
    id 1059
    label "Teksas"
  ]
  node [
    id 1060
    label "Goa"
  ]
  node [
    id 1061
    label "Waszyngton"
  ]
  node [
    id 1062
    label "Massachusetts"
  ]
  node [
    id 1063
    label "Alaska"
  ]
  node [
    id 1064
    label "Arakan"
  ]
  node [
    id 1065
    label "Hawaje"
  ]
  node [
    id 1066
    label "Maryland"
  ]
  node [
    id 1067
    label "punkt"
  ]
  node [
    id 1068
    label "Michigan"
  ]
  node [
    id 1069
    label "Arizona"
  ]
  node [
    id 1070
    label "Georgia"
  ]
  node [
    id 1071
    label "poziom"
  ]
  node [
    id 1072
    label "Pensylwania"
  ]
  node [
    id 1073
    label "Luizjana"
  ]
  node [
    id 1074
    label "Nowy_Meksyk"
  ]
  node [
    id 1075
    label "Alabama"
  ]
  node [
    id 1076
    label "ilo&#347;&#263;"
  ]
  node [
    id 1077
    label "Kansas"
  ]
  node [
    id 1078
    label "Oregon"
  ]
  node [
    id 1079
    label "Floryda"
  ]
  node [
    id 1080
    label "Oklahoma"
  ]
  node [
    id 1081
    label "jednostka_administracyjna"
  ]
  node [
    id 1082
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1083
    label "dok&#322;adnie"
  ]
  node [
    id 1084
    label "r&#243;wny"
  ]
  node [
    id 1085
    label "punctiliously"
  ]
  node [
    id 1086
    label "meticulously"
  ]
  node [
    id 1087
    label "precyzyjnie"
  ]
  node [
    id 1088
    label "dok&#322;adny"
  ]
  node [
    id 1089
    label "rzetelnie"
  ]
  node [
    id 1090
    label "mundurowanie"
  ]
  node [
    id 1091
    label "klawy"
  ]
  node [
    id 1092
    label "dor&#243;wnywanie"
  ]
  node [
    id 1093
    label "zr&#243;wnanie_si&#281;"
  ]
  node [
    id 1094
    label "jednotonny"
  ]
  node [
    id 1095
    label "taki&#380;"
  ]
  node [
    id 1096
    label "ca&#322;y"
  ]
  node [
    id 1097
    label "jednolity"
  ]
  node [
    id 1098
    label "mundurowa&#263;"
  ]
  node [
    id 1099
    label "r&#243;wnanie"
  ]
  node [
    id 1100
    label "jednoczesny"
  ]
  node [
    id 1101
    label "zr&#243;wnanie"
  ]
  node [
    id 1102
    label "miarowo"
  ]
  node [
    id 1103
    label "r&#243;wno"
  ]
  node [
    id 1104
    label "jednakowo"
  ]
  node [
    id 1105
    label "zr&#243;wnywanie"
  ]
  node [
    id 1106
    label "identyczny"
  ]
  node [
    id 1107
    label "regularny"
  ]
  node [
    id 1108
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 1109
    label "prosty"
  ]
  node [
    id 1110
    label "stabilny"
  ]
  node [
    id 1111
    label "potrzebny"
  ]
  node [
    id 1112
    label "po&#380;&#261;dany"
  ]
  node [
    id 1113
    label "przydatnie"
  ]
  node [
    id 1114
    label "po&#380;yteczno"
  ]
  node [
    id 1115
    label "potrzebnie"
  ]
  node [
    id 1116
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 1117
    label "zobo"
  ]
  node [
    id 1118
    label "yakalo"
  ]
  node [
    id 1119
    label "dzo"
  ]
  node [
    id 1120
    label "kr&#281;torogie"
  ]
  node [
    id 1121
    label "czochrad&#322;o"
  ]
  node [
    id 1122
    label "posp&#243;lstwo"
  ]
  node [
    id 1123
    label "kraal"
  ]
  node [
    id 1124
    label "livestock"
  ]
  node [
    id 1125
    label "prze&#380;uwacz"
  ]
  node [
    id 1126
    label "bizon"
  ]
  node [
    id 1127
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 1128
    label "zebu"
  ]
  node [
    id 1129
    label "byd&#322;o_domowe"
  ]
  node [
    id 1130
    label "najemny"
  ]
  node [
    id 1131
    label "p&#322;atnie"
  ]
  node [
    id 1132
    label "przekupny"
  ]
  node [
    id 1133
    label "najemnie"
  ]
  node [
    id 1134
    label "udzielny"
  ]
  node [
    id 1135
    label "xerox"
  ]
  node [
    id 1136
    label "zak&#322;ad"
  ]
  node [
    id 1137
    label "toner"
  ]
  node [
    id 1138
    label "odbitka"
  ]
  node [
    id 1139
    label "kopiarka"
  ]
  node [
    id 1140
    label "dupleks"
  ]
  node [
    id 1141
    label "sylaba"
  ]
  node [
    id 1142
    label "nuta"
  ]
  node [
    id 1143
    label "kopia"
  ]
  node [
    id 1144
    label "wers"
  ]
  node [
    id 1145
    label "print"
  ]
  node [
    id 1146
    label "zak&#322;adka"
  ]
  node [
    id 1147
    label "jednostka_organizacyjna"
  ]
  node [
    id 1148
    label "miejsce_pracy"
  ]
  node [
    id 1149
    label "instytucja"
  ]
  node [
    id 1150
    label "wyko&#324;czenie"
  ]
  node [
    id 1151
    label "firma"
  ]
  node [
    id 1152
    label "czyn"
  ]
  node [
    id 1153
    label "company"
  ]
  node [
    id 1154
    label "instytut"
  ]
  node [
    id 1155
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1156
    label "sprawa"
  ]
  node [
    id 1157
    label "ust&#281;p"
  ]
  node [
    id 1158
    label "problemat"
  ]
  node [
    id 1159
    label "plamka"
  ]
  node [
    id 1160
    label "stopie&#324;_pisma"
  ]
  node [
    id 1161
    label "jednostka"
  ]
  node [
    id 1162
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1163
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1164
    label "mark"
  ]
  node [
    id 1165
    label "prosta"
  ]
  node [
    id 1166
    label "problematyka"
  ]
  node [
    id 1167
    label "zapunktowa&#263;"
  ]
  node [
    id 1168
    label "podpunkt"
  ]
  node [
    id 1169
    label "wojsko"
  ]
  node [
    id 1170
    label "pozycja"
  ]
  node [
    id 1171
    label "kserokopiarka"
  ]
  node [
    id 1172
    label "karton"
  ]
  node [
    id 1173
    label "system_&#322;&#261;czno&#347;ci"
  ]
  node [
    id 1174
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 1175
    label "drukarka"
  ]
  node [
    id 1176
    label "wk&#322;ad"
  ]
  node [
    id 1177
    label "drukarka_laserowa"
  ]
  node [
    id 1178
    label "proszek"
  ]
  node [
    id 1179
    label "przest&#281;pca"
  ]
  node [
    id 1180
    label "kopiowa&#263;"
  ]
  node [
    id 1181
    label "podr&#243;bka"
  ]
  node [
    id 1182
    label "kieruj&#261;cy"
  ]
  node [
    id 1183
    label "&#380;agl&#243;wka"
  ]
  node [
    id 1184
    label "rum"
  ]
  node [
    id 1185
    label "rozb&#243;jnik"
  ]
  node [
    id 1186
    label "postrzeleniec"
  ]
  node [
    id 1187
    label "krzy&#380;ak"
  ]
  node [
    id 1188
    label "&#380;aglowiec"
  ]
  node [
    id 1189
    label "miecz"
  ]
  node [
    id 1190
    label "wios&#322;o"
  ]
  node [
    id 1191
    label "bom"
  ]
  node [
    id 1192
    label "pok&#322;ad"
  ]
  node [
    id 1193
    label "ster"
  ]
  node [
    id 1194
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 1195
    label "&#380;agiel"
  ]
  node [
    id 1196
    label "imitation"
  ]
  node [
    id 1197
    label "oszuka&#324;stwo"
  ]
  node [
    id 1198
    label "brygant"
  ]
  node [
    id 1199
    label "bandyta"
  ]
  node [
    id 1200
    label "pogwa&#322;ciciel"
  ]
  node [
    id 1201
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 1202
    label "kierowca"
  ]
  node [
    id 1203
    label "szalona_g&#322;owa"
  ]
  node [
    id 1204
    label "instalowa&#263;"
  ]
  node [
    id 1205
    label "oprogramowanie"
  ]
  node [
    id 1206
    label "odinstalowywa&#263;"
  ]
  node [
    id 1207
    label "spis"
  ]
  node [
    id 1208
    label "zaprezentowanie"
  ]
  node [
    id 1209
    label "podprogram"
  ]
  node [
    id 1210
    label "ogranicznik_referencyjny"
  ]
  node [
    id 1211
    label "course_of_study"
  ]
  node [
    id 1212
    label "booklet"
  ]
  node [
    id 1213
    label "dzia&#322;"
  ]
  node [
    id 1214
    label "odinstalowanie"
  ]
  node [
    id 1215
    label "broszura"
  ]
  node [
    id 1216
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 1217
    label "kana&#322;"
  ]
  node [
    id 1218
    label "teleferie"
  ]
  node [
    id 1219
    label "zainstalowanie"
  ]
  node [
    id 1220
    label "struktura_organizacyjna"
  ]
  node [
    id 1221
    label "zaprezentowa&#263;"
  ]
  node [
    id 1222
    label "prezentowanie"
  ]
  node [
    id 1223
    label "prezentowa&#263;"
  ]
  node [
    id 1224
    label "interfejs"
  ]
  node [
    id 1225
    label "okno"
  ]
  node [
    id 1226
    label "folder"
  ]
  node [
    id 1227
    label "zainstalowa&#263;"
  ]
  node [
    id 1228
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1229
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1230
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1231
    label "ram&#243;wka"
  ]
  node [
    id 1232
    label "emitowa&#263;"
  ]
  node [
    id 1233
    label "emitowanie"
  ]
  node [
    id 1234
    label "odinstalowywanie"
  ]
  node [
    id 1235
    label "instrukcja"
  ]
  node [
    id 1236
    label "informatyka"
  ]
  node [
    id 1237
    label "deklaracja"
  ]
  node [
    id 1238
    label "menu"
  ]
  node [
    id 1239
    label "sekcja_krytyczna"
  ]
  node [
    id 1240
    label "furkacja"
  ]
  node [
    id 1241
    label "instalowanie"
  ]
  node [
    id 1242
    label "oferta"
  ]
  node [
    id 1243
    label "odinstalowa&#263;"
  ]
  node [
    id 1244
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1245
    label "transcribe"
  ]
  node [
    id 1246
    label "mock"
  ]
  node [
    id 1247
    label "bezcelowy"
  ]
  node [
    id 1248
    label "zb&#281;dnie"
  ]
  node [
    id 1249
    label "darmowy"
  ]
  node [
    id 1250
    label "bezskutecznie"
  ]
  node [
    id 1251
    label "&#378;le"
  ]
  node [
    id 1252
    label "nieskuteczny"
  ]
  node [
    id 1253
    label "superfluously"
  ]
  node [
    id 1254
    label "uselessly"
  ]
  node [
    id 1255
    label "nadmiarowo"
  ]
  node [
    id 1256
    label "zb&#281;dny"
  ]
  node [
    id 1257
    label "p&#322;onny"
  ]
  node [
    id 1258
    label "niekonstruktywny"
  ]
  node [
    id 1259
    label "bezcelowo"
  ]
  node [
    id 1260
    label "darmowo"
  ]
  node [
    id 1261
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1262
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1263
    label "temat"
  ]
  node [
    id 1264
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1265
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1266
    label "wn&#281;trze"
  ]
  node [
    id 1267
    label "publikacja"
  ]
  node [
    id 1268
    label "doj&#347;cie"
  ]
  node [
    id 1269
    label "obiega&#263;"
  ]
  node [
    id 1270
    label "powzi&#281;cie"
  ]
  node [
    id 1271
    label "dane"
  ]
  node [
    id 1272
    label "obiegni&#281;cie"
  ]
  node [
    id 1273
    label "sygna&#322;"
  ]
  node [
    id 1274
    label "obieganie"
  ]
  node [
    id 1275
    label "powzi&#261;&#263;"
  ]
  node [
    id 1276
    label "obiec"
  ]
  node [
    id 1277
    label "doj&#347;&#263;"
  ]
  node [
    id 1278
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1279
    label "superego"
  ]
  node [
    id 1280
    label "znaczenie"
  ]
  node [
    id 1281
    label "wyraz_pochodny"
  ]
  node [
    id 1282
    label "zboczenie"
  ]
  node [
    id 1283
    label "om&#243;wienie"
  ]
  node [
    id 1284
    label "omawia&#263;"
  ]
  node [
    id 1285
    label "fraza"
  ]
  node [
    id 1286
    label "forum"
  ]
  node [
    id 1287
    label "topik"
  ]
  node [
    id 1288
    label "tematyka"
  ]
  node [
    id 1289
    label "w&#261;tek"
  ]
  node [
    id 1290
    label "zbaczanie"
  ]
  node [
    id 1291
    label "forma"
  ]
  node [
    id 1292
    label "om&#243;wi&#263;"
  ]
  node [
    id 1293
    label "omawianie"
  ]
  node [
    id 1294
    label "melodia"
  ]
  node [
    id 1295
    label "otoczka"
  ]
  node [
    id 1296
    label "zboczy&#263;"
  ]
  node [
    id 1297
    label "p&#243;&#322;ka"
  ]
  node [
    id 1298
    label "stoisko"
  ]
  node [
    id 1299
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 1300
    label "sk&#322;ad"
  ]
  node [
    id 1301
    label "obiekt_handlowy"
  ]
  node [
    id 1302
    label "zaplecze"
  ]
  node [
    id 1303
    label "witryna"
  ]
  node [
    id 1304
    label "Apeks"
  ]
  node [
    id 1305
    label "zasoby"
  ]
  node [
    id 1306
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 1307
    label "zaufanie"
  ]
  node [
    id 1308
    label "Hortex"
  ]
  node [
    id 1309
    label "reengineering"
  ]
  node [
    id 1310
    label "nazwa_w&#322;asna"
  ]
  node [
    id 1311
    label "podmiot_gospodarczy"
  ]
  node [
    id 1312
    label "paczkarnia"
  ]
  node [
    id 1313
    label "Orlen"
  ]
  node [
    id 1314
    label "interes"
  ]
  node [
    id 1315
    label "Google"
  ]
  node [
    id 1316
    label "Canon"
  ]
  node [
    id 1317
    label "Pewex"
  ]
  node [
    id 1318
    label "MAN_SE"
  ]
  node [
    id 1319
    label "Spo&#322;em"
  ]
  node [
    id 1320
    label "klasa"
  ]
  node [
    id 1321
    label "networking"
  ]
  node [
    id 1322
    label "MAC"
  ]
  node [
    id 1323
    label "zasoby_ludzkie"
  ]
  node [
    id 1324
    label "Baltona"
  ]
  node [
    id 1325
    label "Orbis"
  ]
  node [
    id 1326
    label "biurowiec"
  ]
  node [
    id 1327
    label "HP"
  ]
  node [
    id 1328
    label "siedziba"
  ]
  node [
    id 1329
    label "stela&#380;"
  ]
  node [
    id 1330
    label "szafa"
  ]
  node [
    id 1331
    label "mebel"
  ]
  node [
    id 1332
    label "meblo&#347;cianka"
  ]
  node [
    id 1333
    label "infrastruktura"
  ]
  node [
    id 1334
    label "wyposa&#380;enie"
  ]
  node [
    id 1335
    label "szyba"
  ]
  node [
    id 1336
    label "YouTube"
  ]
  node [
    id 1337
    label "gablota"
  ]
  node [
    id 1338
    label "strona"
  ]
  node [
    id 1339
    label "blokada"
  ]
  node [
    id 1340
    label "hurtownia"
  ]
  node [
    id 1341
    label "pole"
  ]
  node [
    id 1342
    label "pas"
  ]
  node [
    id 1343
    label "basic"
  ]
  node [
    id 1344
    label "obr&#243;bka"
  ]
  node [
    id 1345
    label "constitution"
  ]
  node [
    id 1346
    label "fabryka"
  ]
  node [
    id 1347
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1348
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1349
    label "syf"
  ]
  node [
    id 1350
    label "rank_and_file"
  ]
  node [
    id 1351
    label "tabulacja"
  ]
  node [
    id 1352
    label "necessity"
  ]
  node [
    id 1353
    label "trza"
  ]
  node [
    id 1354
    label "pozyska&#263;"
  ]
  node [
    id 1355
    label "ustawi&#263;"
  ]
  node [
    id 1356
    label "wzi&#261;&#263;"
  ]
  node [
    id 1357
    label "uwierzy&#263;"
  ]
  node [
    id 1358
    label "catch"
  ]
  node [
    id 1359
    label "zagra&#263;"
  ]
  node [
    id 1360
    label "beget"
  ]
  node [
    id 1361
    label "uzna&#263;"
  ]
  node [
    id 1362
    label "przyj&#261;&#263;"
  ]
  node [
    id 1363
    label "play"
  ]
  node [
    id 1364
    label "leave"
  ]
  node [
    id 1365
    label "instrument_muzyczny"
  ]
  node [
    id 1366
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 1367
    label "flare"
  ]
  node [
    id 1368
    label "rozegra&#263;"
  ]
  node [
    id 1369
    label "zaszczeka&#263;"
  ]
  node [
    id 1370
    label "represent"
  ]
  node [
    id 1371
    label "wykorzysta&#263;"
  ]
  node [
    id 1372
    label "zatokowa&#263;"
  ]
  node [
    id 1373
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 1374
    label "zacz&#261;&#263;"
  ]
  node [
    id 1375
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1376
    label "wykona&#263;"
  ]
  node [
    id 1377
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 1378
    label "typify"
  ]
  node [
    id 1379
    label "rola"
  ]
  node [
    id 1380
    label "poprawi&#263;"
  ]
  node [
    id 1381
    label "nada&#263;"
  ]
  node [
    id 1382
    label "peddle"
  ]
  node [
    id 1383
    label "marshal"
  ]
  node [
    id 1384
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 1385
    label "wyznaczy&#263;"
  ]
  node [
    id 1386
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 1387
    label "zabezpieczy&#263;"
  ]
  node [
    id 1388
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1389
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 1390
    label "zinterpretowa&#263;"
  ]
  node [
    id 1391
    label "wskaza&#263;"
  ]
  node [
    id 1392
    label "przyzna&#263;"
  ]
  node [
    id 1393
    label "sk&#322;oni&#263;"
  ]
  node [
    id 1394
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 1395
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 1396
    label "zdecydowa&#263;"
  ]
  node [
    id 1397
    label "accommodate"
  ]
  node [
    id 1398
    label "ustali&#263;"
  ]
  node [
    id 1399
    label "situate"
  ]
  node [
    id 1400
    label "odziedziczy&#263;"
  ]
  node [
    id 1401
    label "ruszy&#263;"
  ]
  node [
    id 1402
    label "take"
  ]
  node [
    id 1403
    label "zaatakowa&#263;"
  ]
  node [
    id 1404
    label "skorzysta&#263;"
  ]
  node [
    id 1405
    label "uciec"
  ]
  node [
    id 1406
    label "receive"
  ]
  node [
    id 1407
    label "nakaza&#263;"
  ]
  node [
    id 1408
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1409
    label "obskoczy&#263;"
  ]
  node [
    id 1410
    label "bra&#263;"
  ]
  node [
    id 1411
    label "u&#380;y&#263;"
  ]
  node [
    id 1412
    label "wyrucha&#263;"
  ]
  node [
    id 1413
    label "World_Health_Organization"
  ]
  node [
    id 1414
    label "wyciupcia&#263;"
  ]
  node [
    id 1415
    label "wygra&#263;"
  ]
  node [
    id 1416
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 1417
    label "withdraw"
  ]
  node [
    id 1418
    label "wzi&#281;cie"
  ]
  node [
    id 1419
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 1420
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 1421
    label "poczyta&#263;"
  ]
  node [
    id 1422
    label "obj&#261;&#263;"
  ]
  node [
    id 1423
    label "seize"
  ]
  node [
    id 1424
    label "aim"
  ]
  node [
    id 1425
    label "chwyci&#263;"
  ]
  node [
    id 1426
    label "pokona&#263;"
  ]
  node [
    id 1427
    label "arise"
  ]
  node [
    id 1428
    label "otrzyma&#263;"
  ]
  node [
    id 1429
    label "wej&#347;&#263;"
  ]
  node [
    id 1430
    label "poruszy&#263;"
  ]
  node [
    id 1431
    label "dosta&#263;"
  ]
  node [
    id 1432
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1433
    label "stage"
  ]
  node [
    id 1434
    label "uzyska&#263;"
  ]
  node [
    id 1435
    label "wytworzy&#263;"
  ]
  node [
    id 1436
    label "give_birth"
  ]
  node [
    id 1437
    label "przybra&#263;"
  ]
  node [
    id 1438
    label "strike"
  ]
  node [
    id 1439
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1440
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 1441
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 1442
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 1443
    label "obra&#263;"
  ]
  node [
    id 1444
    label "draw"
  ]
  node [
    id 1445
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1446
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 1447
    label "przyj&#281;cie"
  ]
  node [
    id 1448
    label "swallow"
  ]
  node [
    id 1449
    label "odebra&#263;"
  ]
  node [
    id 1450
    label "dostarczy&#263;"
  ]
  node [
    id 1451
    label "absorb"
  ]
  node [
    id 1452
    label "undertake"
  ]
  node [
    id 1453
    label "oceni&#263;"
  ]
  node [
    id 1454
    label "stwierdzi&#263;"
  ]
  node [
    id 1455
    label "assent"
  ]
  node [
    id 1456
    label "rede"
  ]
  node [
    id 1457
    label "see"
  ]
  node [
    id 1458
    label "trust"
  ]
  node [
    id 1459
    label "Andrew"
  ]
  node [
    id 1460
    label "Keen"
  ]
  node [
    id 1461
    label "Craig"
  ]
  node [
    id 1462
    label "Newmark"
  ]
  node [
    id 1463
    label "Second"
  ]
  node [
    id 1464
    label "Life"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 239
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 529
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 530
  ]
  edge [
    source 17
    target 531
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 534
  ]
  edge [
    source 17
    target 535
  ]
  edge [
    source 17
    target 536
  ]
  edge [
    source 17
    target 537
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 539
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 541
  ]
  edge [
    source 17
    target 542
  ]
  edge [
    source 17
    target 543
  ]
  edge [
    source 17
    target 544
  ]
  edge [
    source 17
    target 545
  ]
  edge [
    source 17
    target 546
  ]
  edge [
    source 17
    target 547
  ]
  edge [
    source 17
    target 548
  ]
  edge [
    source 17
    target 549
  ]
  edge [
    source 17
    target 550
  ]
  edge [
    source 17
    target 551
  ]
  edge [
    source 17
    target 552
  ]
  edge [
    source 17
    target 553
  ]
  edge [
    source 17
    target 554
  ]
  edge [
    source 17
    target 555
  ]
  edge [
    source 17
    target 556
  ]
  edge [
    source 17
    target 557
  ]
  edge [
    source 17
    target 558
  ]
  edge [
    source 17
    target 559
  ]
  edge [
    source 17
    target 560
  ]
  edge [
    source 17
    target 561
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 17
    target 563
  ]
  edge [
    source 17
    target 406
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 565
  ]
  edge [
    source 17
    target 566
  ]
  edge [
    source 17
    target 567
  ]
  edge [
    source 17
    target 568
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 569
  ]
  edge [
    source 17
    target 570
  ]
  edge [
    source 17
    target 415
  ]
  edge [
    source 17
    target 571
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 603
  ]
  edge [
    source 19
    target 604
  ]
  edge [
    source 19
    target 605
  ]
  edge [
    source 19
    target 606
  ]
  edge [
    source 19
    target 607
  ]
  edge [
    source 19
    target 608
  ]
  edge [
    source 19
    target 609
  ]
  edge [
    source 19
    target 610
  ]
  edge [
    source 19
    target 611
  ]
  edge [
    source 19
    target 612
  ]
  edge [
    source 19
    target 613
  ]
  edge [
    source 19
    target 614
  ]
  edge [
    source 19
    target 615
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 616
  ]
  edge [
    source 20
    target 617
  ]
  edge [
    source 20
    target 587
  ]
  edge [
    source 20
    target 618
  ]
  edge [
    source 20
    target 619
  ]
  edge [
    source 20
    target 620
  ]
  edge [
    source 20
    target 621
  ]
  edge [
    source 20
    target 622
  ]
  edge [
    source 20
    target 623
  ]
  edge [
    source 20
    target 624
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 626
  ]
  edge [
    source 20
    target 627
  ]
  edge [
    source 20
    target 628
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 630
  ]
  edge [
    source 20
    target 631
  ]
  edge [
    source 20
    target 632
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 633
  ]
  edge [
    source 20
    target 634
  ]
  edge [
    source 20
    target 635
  ]
  edge [
    source 20
    target 636
  ]
  edge [
    source 20
    target 637
  ]
  edge [
    source 20
    target 638
  ]
  edge [
    source 20
    target 639
  ]
  edge [
    source 20
    target 640
  ]
  edge [
    source 20
    target 641
  ]
  edge [
    source 20
    target 642
  ]
  edge [
    source 20
    target 643
  ]
  edge [
    source 20
    target 644
  ]
  edge [
    source 20
    target 645
  ]
  edge [
    source 20
    target 646
  ]
  edge [
    source 20
    target 647
  ]
  edge [
    source 20
    target 648
  ]
  edge [
    source 20
    target 649
  ]
  edge [
    source 20
    target 650
  ]
  edge [
    source 20
    target 651
  ]
  edge [
    source 20
    target 652
  ]
  edge [
    source 20
    target 653
  ]
  edge [
    source 20
    target 654
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 655
  ]
  edge [
    source 20
    target 656
  ]
  edge [
    source 20
    target 657
  ]
  edge [
    source 20
    target 658
  ]
  edge [
    source 20
    target 659
  ]
  edge [
    source 20
    target 660
  ]
  edge [
    source 20
    target 661
  ]
  edge [
    source 20
    target 662
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 663
  ]
  edge [
    source 20
    target 664
  ]
  edge [
    source 20
    target 665
  ]
  edge [
    source 20
    target 666
  ]
  edge [
    source 20
    target 667
  ]
  edge [
    source 20
    target 668
  ]
  edge [
    source 20
    target 669
  ]
  edge [
    source 20
    target 670
  ]
  edge [
    source 20
    target 578
  ]
  edge [
    source 20
    target 585
  ]
  edge [
    source 20
    target 671
  ]
  edge [
    source 20
    target 672
  ]
  edge [
    source 20
    target 673
  ]
  edge [
    source 20
    target 674
  ]
  edge [
    source 20
    target 675
  ]
  edge [
    source 20
    target 676
  ]
  edge [
    source 20
    target 677
  ]
  edge [
    source 20
    target 678
  ]
  edge [
    source 20
    target 679
  ]
  edge [
    source 20
    target 680
  ]
  edge [
    source 20
    target 681
  ]
  edge [
    source 20
    target 682
  ]
  edge [
    source 20
    target 683
  ]
  edge [
    source 20
    target 684
  ]
  edge [
    source 20
    target 685
  ]
  edge [
    source 20
    target 686
  ]
  edge [
    source 20
    target 687
  ]
  edge [
    source 20
    target 688
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 689
  ]
  edge [
    source 20
    target 690
  ]
  edge [
    source 20
    target 691
  ]
  edge [
    source 20
    target 692
  ]
  edge [
    source 20
    target 693
  ]
  edge [
    source 20
    target 694
  ]
  edge [
    source 20
    target 695
  ]
  edge [
    source 20
    target 696
  ]
  edge [
    source 20
    target 697
  ]
  edge [
    source 20
    target 698
  ]
  edge [
    source 20
    target 699
  ]
  edge [
    source 20
    target 700
  ]
  edge [
    source 20
    target 701
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 702
  ]
  edge [
    source 20
    target 703
  ]
  edge [
    source 20
    target 704
  ]
  edge [
    source 20
    target 705
  ]
  edge [
    source 20
    target 706
  ]
  edge [
    source 20
    target 707
  ]
  edge [
    source 20
    target 708
  ]
  edge [
    source 20
    target 709
  ]
  edge [
    source 20
    target 710
  ]
  edge [
    source 20
    target 711
  ]
  edge [
    source 20
    target 712
  ]
  edge [
    source 20
    target 713
  ]
  edge [
    source 20
    target 714
  ]
  edge [
    source 20
    target 715
  ]
  edge [
    source 20
    target 716
  ]
  edge [
    source 20
    target 717
  ]
  edge [
    source 20
    target 718
  ]
  edge [
    source 20
    target 719
  ]
  edge [
    source 20
    target 720
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 20
    target 99
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 20
    target 721
  ]
  edge [
    source 20
    target 722
  ]
  edge [
    source 20
    target 723
  ]
  edge [
    source 20
    target 724
  ]
  edge [
    source 20
    target 725
  ]
  edge [
    source 20
    target 726
  ]
  edge [
    source 20
    target 727
  ]
  edge [
    source 20
    target 728
  ]
  edge [
    source 20
    target 729
  ]
  edge [
    source 20
    target 730
  ]
  edge [
    source 20
    target 731
  ]
  edge [
    source 20
    target 732
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 734
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 736
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 745
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 462
  ]
  edge [
    source 20
    target 747
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 761
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 20
    target 763
  ]
  edge [
    source 20
    target 764
  ]
  edge [
    source 20
    target 765
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 772
  ]
  edge [
    source 20
    target 773
  ]
  edge [
    source 20
    target 774
  ]
  edge [
    source 20
    target 775
  ]
  edge [
    source 20
    target 776
  ]
  edge [
    source 20
    target 505
  ]
  edge [
    source 20
    target 777
  ]
  edge [
    source 20
    target 778
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 780
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 240
  ]
  edge [
    source 20
    target 783
  ]
  edge [
    source 20
    target 784
  ]
  edge [
    source 20
    target 785
  ]
  edge [
    source 20
    target 786
  ]
  edge [
    source 20
    target 787
  ]
  edge [
    source 20
    target 788
  ]
  edge [
    source 20
    target 789
  ]
  edge [
    source 20
    target 790
  ]
  edge [
    source 20
    target 791
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 792
  ]
  edge [
    source 20
    target 793
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 795
  ]
  edge [
    source 20
    target 796
  ]
  edge [
    source 20
    target 797
  ]
  edge [
    source 20
    target 798
  ]
  edge [
    source 20
    target 799
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 801
  ]
  edge [
    source 20
    target 802
  ]
  edge [
    source 20
    target 803
  ]
  edge [
    source 20
    target 804
  ]
  edge [
    source 20
    target 805
  ]
  edge [
    source 20
    target 806
  ]
  edge [
    source 20
    target 807
  ]
  edge [
    source 20
    target 808
  ]
  edge [
    source 20
    target 809
  ]
  edge [
    source 20
    target 810
  ]
  edge [
    source 20
    target 811
  ]
  edge [
    source 20
    target 812
  ]
  edge [
    source 20
    target 813
  ]
  edge [
    source 20
    target 814
  ]
  edge [
    source 20
    target 815
  ]
  edge [
    source 20
    target 816
  ]
  edge [
    source 20
    target 817
  ]
  edge [
    source 20
    target 818
  ]
  edge [
    source 20
    target 819
  ]
  edge [
    source 20
    target 820
  ]
  edge [
    source 20
    target 821
  ]
  edge [
    source 20
    target 822
  ]
  edge [
    source 20
    target 823
  ]
  edge [
    source 20
    target 824
  ]
  edge [
    source 20
    target 825
  ]
  edge [
    source 20
    target 826
  ]
  edge [
    source 20
    target 827
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 830
  ]
  edge [
    source 20
    target 831
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 833
  ]
  edge [
    source 20
    target 834
  ]
  edge [
    source 20
    target 835
  ]
  edge [
    source 20
    target 836
  ]
  edge [
    source 20
    target 837
  ]
  edge [
    source 20
    target 838
  ]
  edge [
    source 20
    target 839
  ]
  edge [
    source 20
    target 840
  ]
  edge [
    source 20
    target 841
  ]
  edge [
    source 20
    target 842
  ]
  edge [
    source 20
    target 843
  ]
  edge [
    source 20
    target 844
  ]
  edge [
    source 20
    target 845
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 846
  ]
  edge [
    source 20
    target 847
  ]
  edge [
    source 20
    target 848
  ]
  edge [
    source 20
    target 849
  ]
  edge [
    source 20
    target 850
  ]
  edge [
    source 20
    target 851
  ]
  edge [
    source 20
    target 852
  ]
  edge [
    source 20
    target 853
  ]
  edge [
    source 20
    target 854
  ]
  edge [
    source 20
    target 855
  ]
  edge [
    source 20
    target 856
  ]
  edge [
    source 20
    target 857
  ]
  edge [
    source 20
    target 858
  ]
  edge [
    source 20
    target 859
  ]
  edge [
    source 20
    target 860
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 861
  ]
  edge [
    source 21
    target 862
  ]
  edge [
    source 21
    target 863
  ]
  edge [
    source 21
    target 114
  ]
  edge [
    source 21
    target 864
  ]
  edge [
    source 21
    target 865
  ]
  edge [
    source 21
    target 866
  ]
  edge [
    source 21
    target 867
  ]
  edge [
    source 21
    target 868
  ]
  edge [
    source 21
    target 869
  ]
  edge [
    source 21
    target 870
  ]
  edge [
    source 21
    target 871
  ]
  edge [
    source 21
    target 805
  ]
  edge [
    source 21
    target 872
  ]
  edge [
    source 21
    target 873
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 875
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 877
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 260
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 131
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 891
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 756
  ]
  edge [
    source 21
    target 702
  ]
  edge [
    source 21
    target 896
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 476
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 993
  ]
  edge [
    source 21
    target 994
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 21
    target 996
  ]
  edge [
    source 21
    target 997
  ]
  edge [
    source 21
    target 998
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1000
  ]
  edge [
    source 23
    target 1001
  ]
  edge [
    source 23
    target 1002
  ]
  edge [
    source 23
    target 1003
  ]
  edge [
    source 23
    target 1004
  ]
  edge [
    source 23
    target 425
  ]
  edge [
    source 23
    target 1005
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 398
  ]
  edge [
    source 23
    target 1007
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 50
  ]
  edge [
    source 23
    target 1009
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1011
  ]
  edge [
    source 23
    target 1012
  ]
  edge [
    source 23
    target 1013
  ]
  edge [
    source 23
    target 1014
  ]
  edge [
    source 23
    target 1015
  ]
  edge [
    source 23
    target 436
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 54
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 396
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 352
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 312
  ]
  edge [
    source 23
    target 303
  ]
  edge [
    source 23
    target 307
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 290
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 801
  ]
  edge [
    source 23
    target 1062
  ]
  edge [
    source 23
    target 1063
  ]
  edge [
    source 23
    target 1064
  ]
  edge [
    source 23
    target 1065
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 1067
  ]
  edge [
    source 23
    target 1068
  ]
  edge [
    source 23
    target 1069
  ]
  edge [
    source 23
    target 419
  ]
  edge [
    source 23
    target 1070
  ]
  edge [
    source 23
    target 1071
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 811
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 23
    target 1074
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1083
  ]
  edge [
    source 24
    target 1084
  ]
  edge [
    source 24
    target 1085
  ]
  edge [
    source 24
    target 1086
  ]
  edge [
    source 24
    target 1087
  ]
  edge [
    source 24
    target 1088
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 24
    target 1090
  ]
  edge [
    source 24
    target 1091
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 1093
  ]
  edge [
    source 24
    target 1094
  ]
  edge [
    source 24
    target 1095
  ]
  edge [
    source 24
    target 415
  ]
  edge [
    source 24
    target 1096
  ]
  edge [
    source 24
    target 1097
  ]
  edge [
    source 24
    target 1098
  ]
  edge [
    source 24
    target 1099
  ]
  edge [
    source 24
    target 1100
  ]
  edge [
    source 24
    target 1101
  ]
  edge [
    source 24
    target 1102
  ]
  edge [
    source 24
    target 1103
  ]
  edge [
    source 24
    target 1104
  ]
  edge [
    source 24
    target 1105
  ]
  edge [
    source 24
    target 1106
  ]
  edge [
    source 24
    target 1107
  ]
  edge [
    source 24
    target 1108
  ]
  edge [
    source 24
    target 1109
  ]
  edge [
    source 24
    target 1110
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1111
  ]
  edge [
    source 25
    target 1112
  ]
  edge [
    source 25
    target 1113
  ]
  edge [
    source 25
    target 415
  ]
  edge [
    source 25
    target 156
  ]
  edge [
    source 25
    target 1114
  ]
  edge [
    source 25
    target 1115
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1116
  ]
  edge [
    source 26
    target 1117
  ]
  edge [
    source 26
    target 1118
  ]
  edge [
    source 26
    target 762
  ]
  edge [
    source 26
    target 1119
  ]
  edge [
    source 26
    target 1120
  ]
  edge [
    source 26
    target 131
  ]
  edge [
    source 26
    target 624
  ]
  edge [
    source 26
    target 1121
  ]
  edge [
    source 26
    target 1122
  ]
  edge [
    source 26
    target 1123
  ]
  edge [
    source 26
    target 1124
  ]
  edge [
    source 26
    target 1125
  ]
  edge [
    source 26
    target 1126
  ]
  edge [
    source 26
    target 1127
  ]
  edge [
    source 26
    target 1128
  ]
  edge [
    source 26
    target 1129
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1130
  ]
  edge [
    source 27
    target 1131
  ]
  edge [
    source 27
    target 1132
  ]
  edge [
    source 27
    target 1133
  ]
  edge [
    source 27
    target 1134
  ]
  edge [
    source 28
    target 1135
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1136
  ]
  edge [
    source 28
    target 1137
  ]
  edge [
    source 28
    target 1138
  ]
  edge [
    source 28
    target 1139
  ]
  edge [
    source 28
    target 1140
  ]
  edge [
    source 28
    target 1141
  ]
  edge [
    source 28
    target 1142
  ]
  edge [
    source 28
    target 1143
  ]
  edge [
    source 28
    target 1144
  ]
  edge [
    source 28
    target 1145
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 1146
  ]
  edge [
    source 28
    target 1147
  ]
  edge [
    source 28
    target 1148
  ]
  edge [
    source 28
    target 1149
  ]
  edge [
    source 28
    target 1150
  ]
  edge [
    source 28
    target 1151
  ]
  edge [
    source 28
    target 1152
  ]
  edge [
    source 28
    target 1153
  ]
  edge [
    source 28
    target 1154
  ]
  edge [
    source 28
    target 428
  ]
  edge [
    source 28
    target 1155
  ]
  edge [
    source 28
    target 1156
  ]
  edge [
    source 28
    target 1157
  ]
  edge [
    source 28
    target 499
  ]
  edge [
    source 28
    target 793
  ]
  edge [
    source 28
    target 1158
  ]
  edge [
    source 28
    target 1159
  ]
  edge [
    source 28
    target 1160
  ]
  edge [
    source 28
    target 1161
  ]
  edge [
    source 28
    target 1162
  ]
  edge [
    source 28
    target 801
  ]
  edge [
    source 28
    target 1163
  ]
  edge [
    source 28
    target 1164
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 753
  ]
  edge [
    source 28
    target 1165
  ]
  edge [
    source 28
    target 1166
  ]
  edge [
    source 28
    target 765
  ]
  edge [
    source 28
    target 1167
  ]
  edge [
    source 28
    target 1168
  ]
  edge [
    source 28
    target 1169
  ]
  edge [
    source 28
    target 368
  ]
  edge [
    source 28
    target 369
  ]
  edge [
    source 28
    target 729
  ]
  edge [
    source 28
    target 1170
  ]
  edge [
    source 28
    target 1171
  ]
  edge [
    source 28
    target 1172
  ]
  edge [
    source 28
    target 1173
  ]
  edge [
    source 28
    target 1174
  ]
  edge [
    source 28
    target 1175
  ]
  edge [
    source 28
    target 1176
  ]
  edge [
    source 28
    target 1177
  ]
  edge [
    source 28
    target 1178
  ]
  edge [
    source 29
    target 1179
  ]
  edge [
    source 29
    target 1180
  ]
  edge [
    source 29
    target 1181
  ]
  edge [
    source 29
    target 1182
  ]
  edge [
    source 29
    target 1183
  ]
  edge [
    source 29
    target 1184
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 1185
  ]
  edge [
    source 29
    target 1186
  ]
  edge [
    source 29
    target 1187
  ]
  edge [
    source 29
    target 1188
  ]
  edge [
    source 29
    target 1189
  ]
  edge [
    source 29
    target 1190
  ]
  edge [
    source 29
    target 1191
  ]
  edge [
    source 29
    target 1192
  ]
  edge [
    source 29
    target 1193
  ]
  edge [
    source 29
    target 1194
  ]
  edge [
    source 29
    target 1195
  ]
  edge [
    source 29
    target 1196
  ]
  edge [
    source 29
    target 1197
  ]
  edge [
    source 29
    target 1143
  ]
  edge [
    source 29
    target 1198
  ]
  edge [
    source 29
    target 1199
  ]
  edge [
    source 29
    target 1200
  ]
  edge [
    source 29
    target 1201
  ]
  edge [
    source 29
    target 1202
  ]
  edge [
    source 29
    target 1203
  ]
  edge [
    source 29
    target 748
  ]
  edge [
    source 29
    target 1204
  ]
  edge [
    source 29
    target 1205
  ]
  edge [
    source 29
    target 1206
  ]
  edge [
    source 29
    target 1207
  ]
  edge [
    source 29
    target 1208
  ]
  edge [
    source 29
    target 1209
  ]
  edge [
    source 29
    target 1210
  ]
  edge [
    source 29
    target 1211
  ]
  edge [
    source 29
    target 1212
  ]
  edge [
    source 29
    target 1213
  ]
  edge [
    source 29
    target 1214
  ]
  edge [
    source 29
    target 1215
  ]
  edge [
    source 29
    target 106
  ]
  edge [
    source 29
    target 1216
  ]
  edge [
    source 29
    target 1217
  ]
  edge [
    source 29
    target 1218
  ]
  edge [
    source 29
    target 1219
  ]
  edge [
    source 29
    target 1220
  ]
  edge [
    source 29
    target 1221
  ]
  edge [
    source 29
    target 1222
  ]
  edge [
    source 29
    target 1223
  ]
  edge [
    source 29
    target 1224
  ]
  edge [
    source 29
    target 498
  ]
  edge [
    source 29
    target 1225
  ]
  edge [
    source 29
    target 228
  ]
  edge [
    source 29
    target 1067
  ]
  edge [
    source 29
    target 1226
  ]
  edge [
    source 29
    target 1227
  ]
  edge [
    source 29
    target 1228
  ]
  edge [
    source 29
    target 1229
  ]
  edge [
    source 29
    target 1230
  ]
  edge [
    source 29
    target 1231
  ]
  edge [
    source 29
    target 266
  ]
  edge [
    source 29
    target 1232
  ]
  edge [
    source 29
    target 1233
  ]
  edge [
    source 29
    target 1234
  ]
  edge [
    source 29
    target 1235
  ]
  edge [
    source 29
    target 1236
  ]
  edge [
    source 29
    target 1237
  ]
  edge [
    source 29
    target 1238
  ]
  edge [
    source 29
    target 1239
  ]
  edge [
    source 29
    target 1240
  ]
  edge [
    source 29
    target 818
  ]
  edge [
    source 29
    target 1241
  ]
  edge [
    source 29
    target 1242
  ]
  edge [
    source 29
    target 1243
  ]
  edge [
    source 29
    target 50
  ]
  edge [
    source 29
    target 1244
  ]
  edge [
    source 29
    target 53
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 29
    target 1245
  ]
  edge [
    source 29
    target 1246
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1247
  ]
  edge [
    source 30
    target 1248
  ]
  edge [
    source 30
    target 1249
  ]
  edge [
    source 30
    target 1250
  ]
  edge [
    source 30
    target 1251
  ]
  edge [
    source 30
    target 1252
  ]
  edge [
    source 30
    target 1253
  ]
  edge [
    source 30
    target 1254
  ]
  edge [
    source 30
    target 1255
  ]
  edge [
    source 30
    target 1256
  ]
  edge [
    source 30
    target 1257
  ]
  edge [
    source 30
    target 1258
  ]
  edge [
    source 30
    target 1259
  ]
  edge [
    source 30
    target 1260
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1261
  ]
  edge [
    source 31
    target 94
  ]
  edge [
    source 32
    target 1262
  ]
  edge [
    source 32
    target 1263
  ]
  edge [
    source 32
    target 666
  ]
  edge [
    source 32
    target 804
  ]
  edge [
    source 32
    target 1264
  ]
  edge [
    source 32
    target 1076
  ]
  edge [
    source 32
    target 1265
  ]
  edge [
    source 32
    target 1266
  ]
  edge [
    source 32
    target 1067
  ]
  edge [
    source 32
    target 1267
  ]
  edge [
    source 32
    target 462
  ]
  edge [
    source 32
    target 1268
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1270
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1272
  ]
  edge [
    source 32
    target 1273
  ]
  edge [
    source 32
    target 1274
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 32
    target 1276
  ]
  edge [
    source 32
    target 1277
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1279
  ]
  edge [
    source 32
    target 831
  ]
  edge [
    source 32
    target 1280
  ]
  edge [
    source 32
    target 139
  ]
  edge [
    source 32
    target 715
  ]
  edge [
    source 32
    target 1156
  ]
  edge [
    source 32
    target 1281
  ]
  edge [
    source 32
    target 1282
  ]
  edge [
    source 32
    target 1283
  ]
  edge [
    source 32
    target 796
  ]
  edge [
    source 32
    target 1284
  ]
  edge [
    source 32
    target 1285
  ]
  edge [
    source 32
    target 836
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1288
  ]
  edge [
    source 32
    target 1289
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1291
  ]
  edge [
    source 32
    target 1292
  ]
  edge [
    source 32
    target 1293
  ]
  edge [
    source 32
    target 1294
  ]
  edge [
    source 32
    target 1295
  ]
  edge [
    source 32
    target 405
  ]
  edge [
    source 32
    target 1296
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1297
  ]
  edge [
    source 33
    target 1151
  ]
  edge [
    source 33
    target 1298
  ]
  edge [
    source 33
    target 1299
  ]
  edge [
    source 33
    target 1300
  ]
  edge [
    source 33
    target 1301
  ]
  edge [
    source 33
    target 1302
  ]
  edge [
    source 33
    target 1303
  ]
  edge [
    source 33
    target 1304
  ]
  edge [
    source 33
    target 1305
  ]
  edge [
    source 33
    target 1148
  ]
  edge [
    source 33
    target 1306
  ]
  edge [
    source 33
    target 1307
  ]
  edge [
    source 33
    target 1308
  ]
  edge [
    source 33
    target 1309
  ]
  edge [
    source 33
    target 1310
  ]
  edge [
    source 33
    target 1311
  ]
  edge [
    source 33
    target 1312
  ]
  edge [
    source 33
    target 1313
  ]
  edge [
    source 33
    target 1314
  ]
  edge [
    source 33
    target 1315
  ]
  edge [
    source 33
    target 1316
  ]
  edge [
    source 33
    target 1317
  ]
  edge [
    source 33
    target 1318
  ]
  edge [
    source 33
    target 1319
  ]
  edge [
    source 33
    target 1320
  ]
  edge [
    source 33
    target 1321
  ]
  edge [
    source 33
    target 1322
  ]
  edge [
    source 33
    target 1323
  ]
  edge [
    source 33
    target 1324
  ]
  edge [
    source 33
    target 1325
  ]
  edge [
    source 33
    target 1326
  ]
  edge [
    source 33
    target 1327
  ]
  edge [
    source 33
    target 1328
  ]
  edge [
    source 33
    target 1329
  ]
  edge [
    source 33
    target 1330
  ]
  edge [
    source 33
    target 1331
  ]
  edge [
    source 33
    target 1332
  ]
  edge [
    source 33
    target 1333
  ]
  edge [
    source 33
    target 1334
  ]
  edge [
    source 33
    target 996
  ]
  edge [
    source 33
    target 1335
  ]
  edge [
    source 33
    target 1225
  ]
  edge [
    source 33
    target 1336
  ]
  edge [
    source 33
    target 106
  ]
  edge [
    source 33
    target 1337
  ]
  edge [
    source 33
    target 1338
  ]
  edge [
    source 33
    target 114
  ]
  edge [
    source 33
    target 1339
  ]
  edge [
    source 33
    target 1340
  ]
  edge [
    source 33
    target 924
  ]
  edge [
    source 33
    target 1341
  ]
  edge [
    source 33
    target 1342
  ]
  edge [
    source 33
    target 801
  ]
  edge [
    source 33
    target 1343
  ]
  edge [
    source 33
    target 431
  ]
  edge [
    source 33
    target 1344
  ]
  edge [
    source 33
    target 1345
  ]
  edge [
    source 33
    target 1346
  ]
  edge [
    source 33
    target 1347
  ]
  edge [
    source 33
    target 1348
  ]
  edge [
    source 33
    target 1349
  ]
  edge [
    source 33
    target 1350
  ]
  edge [
    source 33
    target 915
  ]
  edge [
    source 33
    target 1351
  ]
  edge [
    source 33
    target 45
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1352
  ]
  edge [
    source 34
    target 1353
  ]
  edge [
    source 35
    target 1354
  ]
  edge [
    source 35
    target 1355
  ]
  edge [
    source 35
    target 1356
  ]
  edge [
    source 35
    target 1357
  ]
  edge [
    source 35
    target 1358
  ]
  edge [
    source 35
    target 1359
  ]
  edge [
    source 35
    target 54
  ]
  edge [
    source 35
    target 1360
  ]
  edge [
    source 35
    target 1361
  ]
  edge [
    source 35
    target 1362
  ]
  edge [
    source 35
    target 1363
  ]
  edge [
    source 35
    target 985
  ]
  edge [
    source 35
    target 1364
  ]
  edge [
    source 35
    target 1365
  ]
  edge [
    source 35
    target 1366
  ]
  edge [
    source 35
    target 1367
  ]
  edge [
    source 35
    target 1368
  ]
  edge [
    source 35
    target 332
  ]
  edge [
    source 35
    target 1369
  ]
  edge [
    source 35
    target 975
  ]
  edge [
    source 35
    target 1370
  ]
  edge [
    source 35
    target 1371
  ]
  edge [
    source 35
    target 1372
  ]
  edge [
    source 35
    target 1373
  ]
  edge [
    source 35
    target 356
  ]
  edge [
    source 35
    target 1374
  ]
  edge [
    source 35
    target 1375
  ]
  edge [
    source 35
    target 1376
  ]
  edge [
    source 35
    target 1377
  ]
  edge [
    source 35
    target 1378
  ]
  edge [
    source 35
    target 1379
  ]
  edge [
    source 35
    target 1380
  ]
  edge [
    source 35
    target 1381
  ]
  edge [
    source 35
    target 1382
  ]
  edge [
    source 35
    target 1383
  ]
  edge [
    source 35
    target 1384
  ]
  edge [
    source 35
    target 1385
  ]
  edge [
    source 35
    target 86
  ]
  edge [
    source 35
    target 1386
  ]
  edge [
    source 35
    target 355
  ]
  edge [
    source 35
    target 1387
  ]
  edge [
    source 35
    target 1388
  ]
  edge [
    source 35
    target 1389
  ]
  edge [
    source 35
    target 1390
  ]
  edge [
    source 35
    target 1391
  ]
  edge [
    source 35
    target 915
  ]
  edge [
    source 35
    target 1392
  ]
  edge [
    source 35
    target 1393
  ]
  edge [
    source 35
    target 1394
  ]
  edge [
    source 35
    target 1395
  ]
  edge [
    source 35
    target 1396
  ]
  edge [
    source 35
    target 1397
  ]
  edge [
    source 35
    target 1398
  ]
  edge [
    source 35
    target 1399
  ]
  edge [
    source 35
    target 1400
  ]
  edge [
    source 35
    target 1401
  ]
  edge [
    source 35
    target 1402
  ]
  edge [
    source 35
    target 1403
  ]
  edge [
    source 35
    target 1404
  ]
  edge [
    source 35
    target 1405
  ]
  edge [
    source 35
    target 1406
  ]
  edge [
    source 35
    target 1407
  ]
  edge [
    source 35
    target 1408
  ]
  edge [
    source 35
    target 1409
  ]
  edge [
    source 35
    target 1410
  ]
  edge [
    source 35
    target 1411
  ]
  edge [
    source 35
    target 1412
  ]
  edge [
    source 35
    target 1413
  ]
  edge [
    source 35
    target 1414
  ]
  edge [
    source 35
    target 1415
  ]
  edge [
    source 35
    target 1416
  ]
  edge [
    source 35
    target 1417
  ]
  edge [
    source 35
    target 1418
  ]
  edge [
    source 35
    target 1419
  ]
  edge [
    source 35
    target 1420
  ]
  edge [
    source 35
    target 1421
  ]
  edge [
    source 35
    target 1422
  ]
  edge [
    source 35
    target 1423
  ]
  edge [
    source 35
    target 1424
  ]
  edge [
    source 35
    target 1425
  ]
  edge [
    source 35
    target 1426
  ]
  edge [
    source 35
    target 1427
  ]
  edge [
    source 35
    target 1428
  ]
  edge [
    source 35
    target 1429
  ]
  edge [
    source 35
    target 1430
  ]
  edge [
    source 35
    target 1431
  ]
  edge [
    source 35
    target 1432
  ]
  edge [
    source 35
    target 1433
  ]
  edge [
    source 35
    target 1434
  ]
  edge [
    source 35
    target 1435
  ]
  edge [
    source 35
    target 1436
  ]
  edge [
    source 35
    target 1437
  ]
  edge [
    source 35
    target 1438
  ]
  edge [
    source 35
    target 1439
  ]
  edge [
    source 35
    target 1440
  ]
  edge [
    source 35
    target 1441
  ]
  edge [
    source 35
    target 1442
  ]
  edge [
    source 35
    target 1443
  ]
  edge [
    source 35
    target 1444
  ]
  edge [
    source 35
    target 1445
  ]
  edge [
    source 35
    target 1446
  ]
  edge [
    source 35
    target 1447
  ]
  edge [
    source 35
    target 349
  ]
  edge [
    source 35
    target 1448
  ]
  edge [
    source 35
    target 1449
  ]
  edge [
    source 35
    target 1450
  ]
  edge [
    source 35
    target 1451
  ]
  edge [
    source 35
    target 1452
  ]
  edge [
    source 35
    target 1453
  ]
  edge [
    source 35
    target 1454
  ]
  edge [
    source 35
    target 1455
  ]
  edge [
    source 35
    target 1456
  ]
  edge [
    source 35
    target 1457
  ]
  edge [
    source 35
    target 1458
  ]
  edge [
    source 1459
    target 1460
  ]
  edge [
    source 1461
    target 1462
  ]
  edge [
    source 1463
    target 1464
  ]
]
