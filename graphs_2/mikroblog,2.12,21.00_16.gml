graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zadanie"
    origin "text"
  ]
  node [
    id 3
    label "domowy"
    origin "text"
  ]
  node [
    id 4
    label "nikt"
    origin "text"
  ]
  node [
    id 5
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 7
    label "przodkini"
  ]
  node [
    id 8
    label "matka_zast&#281;pcza"
  ]
  node [
    id 9
    label "matczysko"
  ]
  node [
    id 10
    label "rodzice"
  ]
  node [
    id 11
    label "stara"
  ]
  node [
    id 12
    label "macierz"
  ]
  node [
    id 13
    label "rodzic"
  ]
  node [
    id 14
    label "Matka_Boska"
  ]
  node [
    id 15
    label "macocha"
  ]
  node [
    id 16
    label "starzy"
  ]
  node [
    id 17
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 18
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 19
    label "pokolenie"
  ]
  node [
    id 20
    label "wapniaki"
  ]
  node [
    id 21
    label "krewna"
  ]
  node [
    id 22
    label "opiekun"
  ]
  node [
    id 23
    label "wapniak"
  ]
  node [
    id 24
    label "rodzic_chrzestny"
  ]
  node [
    id 25
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 26
    label "matka"
  ]
  node [
    id 27
    label "&#380;ona"
  ]
  node [
    id 28
    label "kobieta"
  ]
  node [
    id 29
    label "partnerka"
  ]
  node [
    id 30
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 31
    label "matuszka"
  ]
  node [
    id 32
    label "parametryzacja"
  ]
  node [
    id 33
    label "pa&#324;stwo"
  ]
  node [
    id 34
    label "poj&#281;cie"
  ]
  node [
    id 35
    label "mod"
  ]
  node [
    id 36
    label "patriota"
  ]
  node [
    id 37
    label "m&#281;&#380;atka"
  ]
  node [
    id 38
    label "post&#261;pi&#263;"
  ]
  node [
    id 39
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 40
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 41
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 42
    label "zorganizowa&#263;"
  ]
  node [
    id 43
    label "appoint"
  ]
  node [
    id 44
    label "wystylizowa&#263;"
  ]
  node [
    id 45
    label "cause"
  ]
  node [
    id 46
    label "przerobi&#263;"
  ]
  node [
    id 47
    label "nabra&#263;"
  ]
  node [
    id 48
    label "make"
  ]
  node [
    id 49
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 50
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 51
    label "wydali&#263;"
  ]
  node [
    id 52
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 53
    label "advance"
  ]
  node [
    id 54
    label "act"
  ]
  node [
    id 55
    label "see"
  ]
  node [
    id 56
    label "usun&#261;&#263;"
  ]
  node [
    id 57
    label "sack"
  ]
  node [
    id 58
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 59
    label "restore"
  ]
  node [
    id 60
    label "dostosowa&#263;"
  ]
  node [
    id 61
    label "pozyska&#263;"
  ]
  node [
    id 62
    label "stworzy&#263;"
  ]
  node [
    id 63
    label "plan"
  ]
  node [
    id 64
    label "stage"
  ]
  node [
    id 65
    label "urobi&#263;"
  ]
  node [
    id 66
    label "ensnare"
  ]
  node [
    id 67
    label "wprowadzi&#263;"
  ]
  node [
    id 68
    label "zaplanowa&#263;"
  ]
  node [
    id 69
    label "przygotowa&#263;"
  ]
  node [
    id 70
    label "standard"
  ]
  node [
    id 71
    label "skupi&#263;"
  ]
  node [
    id 72
    label "podbi&#263;"
  ]
  node [
    id 73
    label "umocni&#263;"
  ]
  node [
    id 74
    label "doprowadzi&#263;"
  ]
  node [
    id 75
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 76
    label "zadowoli&#263;"
  ]
  node [
    id 77
    label "accommodate"
  ]
  node [
    id 78
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 79
    label "zabezpieczy&#263;"
  ]
  node [
    id 80
    label "wytworzy&#263;"
  ]
  node [
    id 81
    label "pomy&#347;le&#263;"
  ]
  node [
    id 82
    label "woda"
  ]
  node [
    id 83
    label "hoax"
  ]
  node [
    id 84
    label "deceive"
  ]
  node [
    id 85
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 86
    label "oszwabi&#263;"
  ]
  node [
    id 87
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 88
    label "gull"
  ]
  node [
    id 89
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 90
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 91
    label "wzi&#261;&#263;"
  ]
  node [
    id 92
    label "naby&#263;"
  ]
  node [
    id 93
    label "fraud"
  ]
  node [
    id 94
    label "kupi&#263;"
  ]
  node [
    id 95
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 96
    label "objecha&#263;"
  ]
  node [
    id 97
    label "stylize"
  ]
  node [
    id 98
    label "nada&#263;"
  ]
  node [
    id 99
    label "upodobni&#263;"
  ]
  node [
    id 100
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 101
    label "zaliczy&#263;"
  ]
  node [
    id 102
    label "overwork"
  ]
  node [
    id 103
    label "zamieni&#263;"
  ]
  node [
    id 104
    label "zmodyfikowa&#263;"
  ]
  node [
    id 105
    label "change"
  ]
  node [
    id 106
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 107
    label "przej&#347;&#263;"
  ]
  node [
    id 108
    label "zmieni&#263;"
  ]
  node [
    id 109
    label "convert"
  ]
  node [
    id 110
    label "prze&#380;y&#263;"
  ]
  node [
    id 111
    label "przetworzy&#263;"
  ]
  node [
    id 112
    label "upora&#263;_si&#281;"
  ]
  node [
    id 113
    label "sprawi&#263;"
  ]
  node [
    id 114
    label "zaj&#281;cie"
  ]
  node [
    id 115
    label "yield"
  ]
  node [
    id 116
    label "zbi&#243;r"
  ]
  node [
    id 117
    label "zaszkodzenie"
  ]
  node [
    id 118
    label "za&#322;o&#380;enie"
  ]
  node [
    id 119
    label "duty"
  ]
  node [
    id 120
    label "powierzanie"
  ]
  node [
    id 121
    label "work"
  ]
  node [
    id 122
    label "problem"
  ]
  node [
    id 123
    label "przepisanie"
  ]
  node [
    id 124
    label "nakarmienie"
  ]
  node [
    id 125
    label "przepisa&#263;"
  ]
  node [
    id 126
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 127
    label "czynno&#347;&#263;"
  ]
  node [
    id 128
    label "zobowi&#261;zanie"
  ]
  node [
    id 129
    label "activity"
  ]
  node [
    id 130
    label "bezproblemowy"
  ]
  node [
    id 131
    label "wydarzenie"
  ]
  node [
    id 132
    label "danie"
  ]
  node [
    id 133
    label "feed"
  ]
  node [
    id 134
    label "zaspokojenie"
  ]
  node [
    id 135
    label "podwini&#281;cie"
  ]
  node [
    id 136
    label "zap&#322;acenie"
  ]
  node [
    id 137
    label "przyodzianie"
  ]
  node [
    id 138
    label "budowla"
  ]
  node [
    id 139
    label "pokrycie"
  ]
  node [
    id 140
    label "rozebranie"
  ]
  node [
    id 141
    label "zak&#322;adka"
  ]
  node [
    id 142
    label "struktura"
  ]
  node [
    id 143
    label "poubieranie"
  ]
  node [
    id 144
    label "infliction"
  ]
  node [
    id 145
    label "spowodowanie"
  ]
  node [
    id 146
    label "pozak&#322;adanie"
  ]
  node [
    id 147
    label "program"
  ]
  node [
    id 148
    label "przebranie"
  ]
  node [
    id 149
    label "przywdzianie"
  ]
  node [
    id 150
    label "obleczenie_si&#281;"
  ]
  node [
    id 151
    label "utworzenie"
  ]
  node [
    id 152
    label "str&#243;j"
  ]
  node [
    id 153
    label "twierdzenie"
  ]
  node [
    id 154
    label "obleczenie"
  ]
  node [
    id 155
    label "umieszczenie"
  ]
  node [
    id 156
    label "przygotowywanie"
  ]
  node [
    id 157
    label "przymierzenie"
  ]
  node [
    id 158
    label "wyko&#324;czenie"
  ]
  node [
    id 159
    label "point"
  ]
  node [
    id 160
    label "przygotowanie"
  ]
  node [
    id 161
    label "proposition"
  ]
  node [
    id 162
    label "przewidzenie"
  ]
  node [
    id 163
    label "zrobienie"
  ]
  node [
    id 164
    label "stosunek_prawny"
  ]
  node [
    id 165
    label "oblig"
  ]
  node [
    id 166
    label "uregulowa&#263;"
  ]
  node [
    id 167
    label "oddzia&#322;anie"
  ]
  node [
    id 168
    label "occupation"
  ]
  node [
    id 169
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 170
    label "zapowied&#378;"
  ]
  node [
    id 171
    label "obowi&#261;zek"
  ]
  node [
    id 172
    label "statement"
  ]
  node [
    id 173
    label "zapewnienie"
  ]
  node [
    id 174
    label "egzemplarz"
  ]
  node [
    id 175
    label "series"
  ]
  node [
    id 176
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 177
    label "uprawianie"
  ]
  node [
    id 178
    label "praca_rolnicza"
  ]
  node [
    id 179
    label "collection"
  ]
  node [
    id 180
    label "dane"
  ]
  node [
    id 181
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 182
    label "pakiet_klimatyczny"
  ]
  node [
    id 183
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 184
    label "sum"
  ]
  node [
    id 185
    label "gathering"
  ]
  node [
    id 186
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 187
    label "album"
  ]
  node [
    id 188
    label "sprawa"
  ]
  node [
    id 189
    label "subiekcja"
  ]
  node [
    id 190
    label "problemat"
  ]
  node [
    id 191
    label "jajko_Kolumba"
  ]
  node [
    id 192
    label "obstruction"
  ]
  node [
    id 193
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 194
    label "problematyka"
  ]
  node [
    id 195
    label "trudno&#347;&#263;"
  ]
  node [
    id 196
    label "pierepa&#322;ka"
  ]
  node [
    id 197
    label "ambaras"
  ]
  node [
    id 198
    label "damage"
  ]
  node [
    id 199
    label "podniesienie"
  ]
  node [
    id 200
    label "zniesienie"
  ]
  node [
    id 201
    label "ud&#378;wigni&#281;cie"
  ]
  node [
    id 202
    label "ulepszenie"
  ]
  node [
    id 203
    label "heave"
  ]
  node [
    id 204
    label "raise"
  ]
  node [
    id 205
    label "odbudowanie"
  ]
  node [
    id 206
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 207
    label "care"
  ]
  node [
    id 208
    label "zdarzenie_si&#281;"
  ]
  node [
    id 209
    label "benedykty&#324;ski"
  ]
  node [
    id 210
    label "career"
  ]
  node [
    id 211
    label "anektowanie"
  ]
  node [
    id 212
    label "dostarczenie"
  ]
  node [
    id 213
    label "u&#380;ycie"
  ]
  node [
    id 214
    label "klasyfikacja"
  ]
  node [
    id 215
    label "wzi&#281;cie"
  ]
  node [
    id 216
    label "wzbudzenie"
  ]
  node [
    id 217
    label "tynkarski"
  ]
  node [
    id 218
    label "wype&#322;nienie"
  ]
  node [
    id 219
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 220
    label "zapanowanie"
  ]
  node [
    id 221
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 222
    label "zmiana"
  ]
  node [
    id 223
    label "czynnik_produkcji"
  ]
  node [
    id 224
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 225
    label "pozajmowanie"
  ]
  node [
    id 226
    label "ulokowanie_si&#281;"
  ]
  node [
    id 227
    label "usytuowanie_si&#281;"
  ]
  node [
    id 228
    label "obj&#281;cie"
  ]
  node [
    id 229
    label "zabranie"
  ]
  node [
    id 230
    label "oddawanie"
  ]
  node [
    id 231
    label "stanowisko"
  ]
  node [
    id 232
    label "zlecanie"
  ]
  node [
    id 233
    label "ufanie"
  ]
  node [
    id 234
    label "wyznawanie"
  ]
  node [
    id 235
    label "szko&#322;a"
  ]
  node [
    id 236
    label "przekazanie"
  ]
  node [
    id 237
    label "skopiowanie"
  ]
  node [
    id 238
    label "arrangement"
  ]
  node [
    id 239
    label "przeniesienie"
  ]
  node [
    id 240
    label "testament"
  ]
  node [
    id 241
    label "lekarstwo"
  ]
  node [
    id 242
    label "answer"
  ]
  node [
    id 243
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 244
    label "transcription"
  ]
  node [
    id 245
    label "klasa"
  ]
  node [
    id 246
    label "zalecenie"
  ]
  node [
    id 247
    label "przekaza&#263;"
  ]
  node [
    id 248
    label "supply"
  ]
  node [
    id 249
    label "zaleci&#263;"
  ]
  node [
    id 250
    label "rewrite"
  ]
  node [
    id 251
    label "zrzec_si&#281;"
  ]
  node [
    id 252
    label "skopiowa&#263;"
  ]
  node [
    id 253
    label "przenie&#347;&#263;"
  ]
  node [
    id 254
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 255
    label "domowo"
  ]
  node [
    id 256
    label "budynkowy"
  ]
  node [
    id 257
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 258
    label "domestically"
  ]
  node [
    id 259
    label "nale&#380;ny"
  ]
  node [
    id 260
    label "nale&#380;yty"
  ]
  node [
    id 261
    label "typowy"
  ]
  node [
    id 262
    label "uprawniony"
  ]
  node [
    id 263
    label "zasadniczy"
  ]
  node [
    id 264
    label "stosownie"
  ]
  node [
    id 265
    label "taki"
  ]
  node [
    id 266
    label "charakterystyczny"
  ]
  node [
    id 267
    label "prawdziwy"
  ]
  node [
    id 268
    label "ten"
  ]
  node [
    id 269
    label "dobry"
  ]
  node [
    id 270
    label "miernota"
  ]
  node [
    id 271
    label "ciura"
  ]
  node [
    id 272
    label "jako&#347;&#263;"
  ]
  node [
    id 273
    label "cz&#322;owiek"
  ]
  node [
    id 274
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 275
    label "tandetno&#347;&#263;"
  ]
  node [
    id 276
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 277
    label "chor&#261;&#380;y"
  ]
  node [
    id 278
    label "zero"
  ]
  node [
    id 279
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 280
    label "czu&#263;"
  ]
  node [
    id 281
    label "desire"
  ]
  node [
    id 282
    label "kcie&#263;"
  ]
  node [
    id 283
    label "postrzega&#263;"
  ]
  node [
    id 284
    label "przewidywa&#263;"
  ]
  node [
    id 285
    label "by&#263;"
  ]
  node [
    id 286
    label "smell"
  ]
  node [
    id 287
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 288
    label "uczuwa&#263;"
  ]
  node [
    id 289
    label "spirit"
  ]
  node [
    id 290
    label "doznawa&#263;"
  ]
  node [
    id 291
    label "anticipate"
  ]
  node [
    id 292
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 293
    label "aid"
  ]
  node [
    id 294
    label "concur"
  ]
  node [
    id 295
    label "help"
  ]
  node [
    id 296
    label "u&#322;atwi&#263;"
  ]
  node [
    id 297
    label "zaskutkowa&#263;"
  ]
  node [
    id 298
    label "sprawdzi&#263;_si&#281;"
  ]
  node [
    id 299
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 300
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 301
    label "przynie&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
]
