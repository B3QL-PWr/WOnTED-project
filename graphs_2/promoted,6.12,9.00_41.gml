graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "jakby"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 3
    label "musie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "schab"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 7
    label "wolno"
    origin "text"
  ]
  node [
    id 8
    label "je&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "dr&#243;b"
    origin "text"
  ]
  node [
    id 10
    label "ludzko&#347;&#263;"
  ]
  node [
    id 11
    label "asymilowanie"
  ]
  node [
    id 12
    label "wapniak"
  ]
  node [
    id 13
    label "asymilowa&#263;"
  ]
  node [
    id 14
    label "os&#322;abia&#263;"
  ]
  node [
    id 15
    label "posta&#263;"
  ]
  node [
    id 16
    label "hominid"
  ]
  node [
    id 17
    label "podw&#322;adny"
  ]
  node [
    id 18
    label "os&#322;abianie"
  ]
  node [
    id 19
    label "g&#322;owa"
  ]
  node [
    id 20
    label "figura"
  ]
  node [
    id 21
    label "portrecista"
  ]
  node [
    id 22
    label "dwun&#243;g"
  ]
  node [
    id 23
    label "profanum"
  ]
  node [
    id 24
    label "mikrokosmos"
  ]
  node [
    id 25
    label "nasada"
  ]
  node [
    id 26
    label "duch"
  ]
  node [
    id 27
    label "antropochoria"
  ]
  node [
    id 28
    label "osoba"
  ]
  node [
    id 29
    label "wz&#243;r"
  ]
  node [
    id 30
    label "senior"
  ]
  node [
    id 31
    label "oddzia&#322;ywanie"
  ]
  node [
    id 32
    label "Adam"
  ]
  node [
    id 33
    label "homo_sapiens"
  ]
  node [
    id 34
    label "polifag"
  ]
  node [
    id 35
    label "konsument"
  ]
  node [
    id 36
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 37
    label "cz&#322;owiekowate"
  ]
  node [
    id 38
    label "istota_&#380;ywa"
  ]
  node [
    id 39
    label "pracownik"
  ]
  node [
    id 40
    label "Chocho&#322;"
  ]
  node [
    id 41
    label "Herkules_Poirot"
  ]
  node [
    id 42
    label "Edyp"
  ]
  node [
    id 43
    label "parali&#380;owa&#263;"
  ]
  node [
    id 44
    label "Harry_Potter"
  ]
  node [
    id 45
    label "Casanova"
  ]
  node [
    id 46
    label "Gargantua"
  ]
  node [
    id 47
    label "Zgredek"
  ]
  node [
    id 48
    label "Winnetou"
  ]
  node [
    id 49
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 50
    label "Dulcynea"
  ]
  node [
    id 51
    label "kategoria_gramatyczna"
  ]
  node [
    id 52
    label "person"
  ]
  node [
    id 53
    label "Sherlock_Holmes"
  ]
  node [
    id 54
    label "Quasimodo"
  ]
  node [
    id 55
    label "Plastu&#347;"
  ]
  node [
    id 56
    label "Faust"
  ]
  node [
    id 57
    label "Wallenrod"
  ]
  node [
    id 58
    label "Dwukwiat"
  ]
  node [
    id 59
    label "koniugacja"
  ]
  node [
    id 60
    label "Don_Juan"
  ]
  node [
    id 61
    label "Don_Kiszot"
  ]
  node [
    id 62
    label "Hamlet"
  ]
  node [
    id 63
    label "Werter"
  ]
  node [
    id 64
    label "istota"
  ]
  node [
    id 65
    label "Szwejk"
  ]
  node [
    id 66
    label "doros&#322;y"
  ]
  node [
    id 67
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 68
    label "jajko"
  ]
  node [
    id 69
    label "rodzic"
  ]
  node [
    id 70
    label "wapniaki"
  ]
  node [
    id 71
    label "zwierzchnik"
  ]
  node [
    id 72
    label "feuda&#322;"
  ]
  node [
    id 73
    label "starzec"
  ]
  node [
    id 74
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 75
    label "zawodnik"
  ]
  node [
    id 76
    label "komendancja"
  ]
  node [
    id 77
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 78
    label "asymilowanie_si&#281;"
  ]
  node [
    id 79
    label "absorption"
  ]
  node [
    id 80
    label "pobieranie"
  ]
  node [
    id 81
    label "czerpanie"
  ]
  node [
    id 82
    label "acquisition"
  ]
  node [
    id 83
    label "zmienianie"
  ]
  node [
    id 84
    label "organizm"
  ]
  node [
    id 85
    label "assimilation"
  ]
  node [
    id 86
    label "upodabnianie"
  ]
  node [
    id 87
    label "g&#322;oska"
  ]
  node [
    id 88
    label "kultura"
  ]
  node [
    id 89
    label "podobny"
  ]
  node [
    id 90
    label "grupa"
  ]
  node [
    id 91
    label "fonetyka"
  ]
  node [
    id 92
    label "suppress"
  ]
  node [
    id 93
    label "robi&#263;"
  ]
  node [
    id 94
    label "os&#322;abienie"
  ]
  node [
    id 95
    label "kondycja_fizyczna"
  ]
  node [
    id 96
    label "os&#322;abi&#263;"
  ]
  node [
    id 97
    label "zdrowie"
  ]
  node [
    id 98
    label "powodowa&#263;"
  ]
  node [
    id 99
    label "zmniejsza&#263;"
  ]
  node [
    id 100
    label "bate"
  ]
  node [
    id 101
    label "de-escalation"
  ]
  node [
    id 102
    label "powodowanie"
  ]
  node [
    id 103
    label "debilitation"
  ]
  node [
    id 104
    label "zmniejszanie"
  ]
  node [
    id 105
    label "s&#322;abszy"
  ]
  node [
    id 106
    label "pogarszanie"
  ]
  node [
    id 107
    label "assimilate"
  ]
  node [
    id 108
    label "dostosowywa&#263;"
  ]
  node [
    id 109
    label "dostosowa&#263;"
  ]
  node [
    id 110
    label "przejmowa&#263;"
  ]
  node [
    id 111
    label "upodobni&#263;"
  ]
  node [
    id 112
    label "przej&#261;&#263;"
  ]
  node [
    id 113
    label "upodabnia&#263;"
  ]
  node [
    id 114
    label "pobiera&#263;"
  ]
  node [
    id 115
    label "pobra&#263;"
  ]
  node [
    id 116
    label "zapis"
  ]
  node [
    id 117
    label "figure"
  ]
  node [
    id 118
    label "typ"
  ]
  node [
    id 119
    label "spos&#243;b"
  ]
  node [
    id 120
    label "mildew"
  ]
  node [
    id 121
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 122
    label "ideal"
  ]
  node [
    id 123
    label "rule"
  ]
  node [
    id 124
    label "ruch"
  ]
  node [
    id 125
    label "dekal"
  ]
  node [
    id 126
    label "motyw"
  ]
  node [
    id 127
    label "projekt"
  ]
  node [
    id 128
    label "charakterystyka"
  ]
  node [
    id 129
    label "zaistnie&#263;"
  ]
  node [
    id 130
    label "cecha"
  ]
  node [
    id 131
    label "Osjan"
  ]
  node [
    id 132
    label "kto&#347;"
  ]
  node [
    id 133
    label "wygl&#261;d"
  ]
  node [
    id 134
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 135
    label "osobowo&#347;&#263;"
  ]
  node [
    id 136
    label "wytw&#243;r"
  ]
  node [
    id 137
    label "trim"
  ]
  node [
    id 138
    label "poby&#263;"
  ]
  node [
    id 139
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 140
    label "Aspazja"
  ]
  node [
    id 141
    label "punkt_widzenia"
  ]
  node [
    id 142
    label "kompleksja"
  ]
  node [
    id 143
    label "wytrzyma&#263;"
  ]
  node [
    id 144
    label "budowa"
  ]
  node [
    id 145
    label "formacja"
  ]
  node [
    id 146
    label "pozosta&#263;"
  ]
  node [
    id 147
    label "point"
  ]
  node [
    id 148
    label "przedstawienie"
  ]
  node [
    id 149
    label "go&#347;&#263;"
  ]
  node [
    id 150
    label "fotograf"
  ]
  node [
    id 151
    label "malarz"
  ]
  node [
    id 152
    label "artysta"
  ]
  node [
    id 153
    label "hipnotyzowanie"
  ]
  node [
    id 154
    label "&#347;lad"
  ]
  node [
    id 155
    label "docieranie"
  ]
  node [
    id 156
    label "natural_process"
  ]
  node [
    id 157
    label "reakcja_chemiczna"
  ]
  node [
    id 158
    label "wdzieranie_si&#281;"
  ]
  node [
    id 159
    label "zjawisko"
  ]
  node [
    id 160
    label "act"
  ]
  node [
    id 161
    label "rezultat"
  ]
  node [
    id 162
    label "lobbysta"
  ]
  node [
    id 163
    label "pryncypa&#322;"
  ]
  node [
    id 164
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 165
    label "kszta&#322;t"
  ]
  node [
    id 166
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 167
    label "wiedza"
  ]
  node [
    id 168
    label "kierowa&#263;"
  ]
  node [
    id 169
    label "alkohol"
  ]
  node [
    id 170
    label "zdolno&#347;&#263;"
  ]
  node [
    id 171
    label "&#380;ycie"
  ]
  node [
    id 172
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 173
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 174
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 175
    label "sztuka"
  ]
  node [
    id 176
    label "dekiel"
  ]
  node [
    id 177
    label "ro&#347;lina"
  ]
  node [
    id 178
    label "&#347;ci&#281;cie"
  ]
  node [
    id 179
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 180
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 181
    label "&#347;ci&#281;gno"
  ]
  node [
    id 182
    label "noosfera"
  ]
  node [
    id 183
    label "byd&#322;o"
  ]
  node [
    id 184
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 185
    label "makrocefalia"
  ]
  node [
    id 186
    label "obiekt"
  ]
  node [
    id 187
    label "ucho"
  ]
  node [
    id 188
    label "g&#243;ra"
  ]
  node [
    id 189
    label "m&#243;zg"
  ]
  node [
    id 190
    label "kierownictwo"
  ]
  node [
    id 191
    label "fryzura"
  ]
  node [
    id 192
    label "umys&#322;"
  ]
  node [
    id 193
    label "cia&#322;o"
  ]
  node [
    id 194
    label "cz&#322;onek"
  ]
  node [
    id 195
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 196
    label "czaszka"
  ]
  node [
    id 197
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 198
    label "allochoria"
  ]
  node [
    id 199
    label "p&#322;aszczyzna"
  ]
  node [
    id 200
    label "przedmiot"
  ]
  node [
    id 201
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 202
    label "bierka_szachowa"
  ]
  node [
    id 203
    label "obiekt_matematyczny"
  ]
  node [
    id 204
    label "gestaltyzm"
  ]
  node [
    id 205
    label "styl"
  ]
  node [
    id 206
    label "obraz"
  ]
  node [
    id 207
    label "rzecz"
  ]
  node [
    id 208
    label "d&#378;wi&#281;k"
  ]
  node [
    id 209
    label "character"
  ]
  node [
    id 210
    label "rze&#378;ba"
  ]
  node [
    id 211
    label "stylistyka"
  ]
  node [
    id 212
    label "miejsce"
  ]
  node [
    id 213
    label "antycypacja"
  ]
  node [
    id 214
    label "ornamentyka"
  ]
  node [
    id 215
    label "informacja"
  ]
  node [
    id 216
    label "facet"
  ]
  node [
    id 217
    label "popis"
  ]
  node [
    id 218
    label "wiersz"
  ]
  node [
    id 219
    label "symetria"
  ]
  node [
    id 220
    label "lingwistyka_kognitywna"
  ]
  node [
    id 221
    label "karta"
  ]
  node [
    id 222
    label "shape"
  ]
  node [
    id 223
    label "podzbi&#243;r"
  ]
  node [
    id 224
    label "perspektywa"
  ]
  node [
    id 225
    label "dziedzina"
  ]
  node [
    id 226
    label "nak&#322;adka"
  ]
  node [
    id 227
    label "li&#347;&#263;"
  ]
  node [
    id 228
    label "jama_gard&#322;owa"
  ]
  node [
    id 229
    label "rezonator"
  ]
  node [
    id 230
    label "podstawa"
  ]
  node [
    id 231
    label "base"
  ]
  node [
    id 232
    label "piek&#322;o"
  ]
  node [
    id 233
    label "human_body"
  ]
  node [
    id 234
    label "ofiarowywanie"
  ]
  node [
    id 235
    label "sfera_afektywna"
  ]
  node [
    id 236
    label "nekromancja"
  ]
  node [
    id 237
    label "Po&#347;wist"
  ]
  node [
    id 238
    label "podekscytowanie"
  ]
  node [
    id 239
    label "deformowanie"
  ]
  node [
    id 240
    label "sumienie"
  ]
  node [
    id 241
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 242
    label "deformowa&#263;"
  ]
  node [
    id 243
    label "psychika"
  ]
  node [
    id 244
    label "zjawa"
  ]
  node [
    id 245
    label "zmar&#322;y"
  ]
  node [
    id 246
    label "istota_nadprzyrodzona"
  ]
  node [
    id 247
    label "power"
  ]
  node [
    id 248
    label "entity"
  ]
  node [
    id 249
    label "ofiarowywa&#263;"
  ]
  node [
    id 250
    label "oddech"
  ]
  node [
    id 251
    label "seksualno&#347;&#263;"
  ]
  node [
    id 252
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 253
    label "byt"
  ]
  node [
    id 254
    label "si&#322;a"
  ]
  node [
    id 255
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 256
    label "ego"
  ]
  node [
    id 257
    label "ofiarowanie"
  ]
  node [
    id 258
    label "charakter"
  ]
  node [
    id 259
    label "fizjonomia"
  ]
  node [
    id 260
    label "kompleks"
  ]
  node [
    id 261
    label "zapalno&#347;&#263;"
  ]
  node [
    id 262
    label "T&#281;sknica"
  ]
  node [
    id 263
    label "ofiarowa&#263;"
  ]
  node [
    id 264
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 265
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 266
    label "passion"
  ]
  node [
    id 267
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 268
    label "odbicie"
  ]
  node [
    id 269
    label "atom"
  ]
  node [
    id 270
    label "przyroda"
  ]
  node [
    id 271
    label "Ziemia"
  ]
  node [
    id 272
    label "kosmos"
  ]
  node [
    id 273
    label "miniatura"
  ]
  node [
    id 274
    label "need"
  ]
  node [
    id 275
    label "pragn&#261;&#263;"
  ]
  node [
    id 276
    label "czu&#263;"
  ]
  node [
    id 277
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 278
    label "&#322;akn&#261;&#263;"
  ]
  node [
    id 279
    label "t&#281;skni&#263;"
  ]
  node [
    id 280
    label "desire"
  ]
  node [
    id 281
    label "chcie&#263;"
  ]
  node [
    id 282
    label "pozyska&#263;"
  ]
  node [
    id 283
    label "ustawi&#263;"
  ]
  node [
    id 284
    label "wzi&#261;&#263;"
  ]
  node [
    id 285
    label "uwierzy&#263;"
  ]
  node [
    id 286
    label "catch"
  ]
  node [
    id 287
    label "zagra&#263;"
  ]
  node [
    id 288
    label "get"
  ]
  node [
    id 289
    label "beget"
  ]
  node [
    id 290
    label "przyj&#261;&#263;"
  ]
  node [
    id 291
    label "uzna&#263;"
  ]
  node [
    id 292
    label "play"
  ]
  node [
    id 293
    label "zabrzmie&#263;"
  ]
  node [
    id 294
    label "leave"
  ]
  node [
    id 295
    label "instrument_muzyczny"
  ]
  node [
    id 296
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 297
    label "flare"
  ]
  node [
    id 298
    label "rozegra&#263;"
  ]
  node [
    id 299
    label "zrobi&#263;"
  ]
  node [
    id 300
    label "zaszczeka&#263;"
  ]
  node [
    id 301
    label "sound"
  ]
  node [
    id 302
    label "represent"
  ]
  node [
    id 303
    label "wykorzysta&#263;"
  ]
  node [
    id 304
    label "zatokowa&#263;"
  ]
  node [
    id 305
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 306
    label "uda&#263;_si&#281;"
  ]
  node [
    id 307
    label "zacz&#261;&#263;"
  ]
  node [
    id 308
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 309
    label "wykona&#263;"
  ]
  node [
    id 310
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 311
    label "typify"
  ]
  node [
    id 312
    label "rola"
  ]
  node [
    id 313
    label "poprawi&#263;"
  ]
  node [
    id 314
    label "nada&#263;"
  ]
  node [
    id 315
    label "peddle"
  ]
  node [
    id 316
    label "marshal"
  ]
  node [
    id 317
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 318
    label "wyznaczy&#263;"
  ]
  node [
    id 319
    label "stanowisko"
  ]
  node [
    id 320
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 321
    label "spowodowa&#263;"
  ]
  node [
    id 322
    label "zabezpieczy&#263;"
  ]
  node [
    id 323
    label "umie&#347;ci&#263;"
  ]
  node [
    id 324
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 325
    label "zinterpretowa&#263;"
  ]
  node [
    id 326
    label "wskaza&#263;"
  ]
  node [
    id 327
    label "set"
  ]
  node [
    id 328
    label "przyzna&#263;"
  ]
  node [
    id 329
    label "sk&#322;oni&#263;"
  ]
  node [
    id 330
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 331
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 332
    label "zdecydowa&#263;"
  ]
  node [
    id 333
    label "accommodate"
  ]
  node [
    id 334
    label "ustali&#263;"
  ]
  node [
    id 335
    label "situate"
  ]
  node [
    id 336
    label "odziedziczy&#263;"
  ]
  node [
    id 337
    label "ruszy&#263;"
  ]
  node [
    id 338
    label "take"
  ]
  node [
    id 339
    label "zaatakowa&#263;"
  ]
  node [
    id 340
    label "skorzysta&#263;"
  ]
  node [
    id 341
    label "uciec"
  ]
  node [
    id 342
    label "receive"
  ]
  node [
    id 343
    label "nakaza&#263;"
  ]
  node [
    id 344
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 345
    label "obskoczy&#263;"
  ]
  node [
    id 346
    label "bra&#263;"
  ]
  node [
    id 347
    label "u&#380;y&#263;"
  ]
  node [
    id 348
    label "wyrucha&#263;"
  ]
  node [
    id 349
    label "World_Health_Organization"
  ]
  node [
    id 350
    label "wyciupcia&#263;"
  ]
  node [
    id 351
    label "wygra&#263;"
  ]
  node [
    id 352
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 353
    label "withdraw"
  ]
  node [
    id 354
    label "wzi&#281;cie"
  ]
  node [
    id 355
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 356
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 357
    label "poczyta&#263;"
  ]
  node [
    id 358
    label "obj&#261;&#263;"
  ]
  node [
    id 359
    label "seize"
  ]
  node [
    id 360
    label "aim"
  ]
  node [
    id 361
    label "chwyci&#263;"
  ]
  node [
    id 362
    label "pokona&#263;"
  ]
  node [
    id 363
    label "arise"
  ]
  node [
    id 364
    label "otrzyma&#263;"
  ]
  node [
    id 365
    label "wej&#347;&#263;"
  ]
  node [
    id 366
    label "poruszy&#263;"
  ]
  node [
    id 367
    label "dosta&#263;"
  ]
  node [
    id 368
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 369
    label "stage"
  ]
  node [
    id 370
    label "uzyska&#263;"
  ]
  node [
    id 371
    label "wytworzy&#263;"
  ]
  node [
    id 372
    label "give_birth"
  ]
  node [
    id 373
    label "przybra&#263;"
  ]
  node [
    id 374
    label "strike"
  ]
  node [
    id 375
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 376
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 377
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 378
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 379
    label "obra&#263;"
  ]
  node [
    id 380
    label "draw"
  ]
  node [
    id 381
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 382
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 383
    label "przyj&#281;cie"
  ]
  node [
    id 384
    label "fall"
  ]
  node [
    id 385
    label "swallow"
  ]
  node [
    id 386
    label "odebra&#263;"
  ]
  node [
    id 387
    label "dostarczy&#263;"
  ]
  node [
    id 388
    label "absorb"
  ]
  node [
    id 389
    label "undertake"
  ]
  node [
    id 390
    label "oceni&#263;"
  ]
  node [
    id 391
    label "stwierdzi&#263;"
  ]
  node [
    id 392
    label "assent"
  ]
  node [
    id 393
    label "rede"
  ]
  node [
    id 394
    label "see"
  ]
  node [
    id 395
    label "trust"
  ]
  node [
    id 396
    label "wieprzowina"
  ]
  node [
    id 397
    label "tusza"
  ]
  node [
    id 398
    label "mi&#281;siwo"
  ]
  node [
    id 399
    label "mi&#281;so"
  ]
  node [
    id 400
    label "krusze&#263;"
  ]
  node [
    id 401
    label "marynata"
  ]
  node [
    id 402
    label "potrawa"
  ]
  node [
    id 403
    label "skrusze&#263;"
  ]
  node [
    id 404
    label "czerwone_mi&#281;so"
  ]
  node [
    id 405
    label "luzowanie"
  ]
  node [
    id 406
    label "t&#322;uczenie"
  ]
  node [
    id 407
    label "wyluzowanie"
  ]
  node [
    id 408
    label "ut&#322;uczenie"
  ]
  node [
    id 409
    label "tempeh"
  ]
  node [
    id 410
    label "produkt"
  ]
  node [
    id 411
    label "jedzenie"
  ]
  node [
    id 412
    label "seitan"
  ]
  node [
    id 413
    label "mi&#281;sie&#324;"
  ]
  node [
    id 414
    label "chabanina"
  ]
  node [
    id 415
    label "luzowa&#263;"
  ]
  node [
    id 416
    label "wyluzowa&#263;"
  ]
  node [
    id 417
    label "obieralnia"
  ]
  node [
    id 418
    label "panierka"
  ]
  node [
    id 419
    label "g&#322;owizna"
  ]
  node [
    id 420
    label "zaburzenie"
  ]
  node [
    id 421
    label "p&#243;&#322;tusza"
  ]
  node [
    id 422
    label "karczek"
  ]
  node [
    id 423
    label "plec&#243;wka"
  ]
  node [
    id 424
    label "fleshiness"
  ]
  node [
    id 425
    label "pol&#281;dwica"
  ]
  node [
    id 426
    label "niespiesznie"
  ]
  node [
    id 427
    label "wolny"
  ]
  node [
    id 428
    label "thinly"
  ]
  node [
    id 429
    label "wolniej"
  ]
  node [
    id 430
    label "swobodny"
  ]
  node [
    id 431
    label "wolnie"
  ]
  node [
    id 432
    label "free"
  ]
  node [
    id 433
    label "lu&#378;ny"
  ]
  node [
    id 434
    label "lu&#378;no"
  ]
  node [
    id 435
    label "swobodnie"
  ]
  node [
    id 436
    label "naturalny"
  ]
  node [
    id 437
    label "bezpruderyjny"
  ]
  node [
    id 438
    label "dowolny"
  ]
  node [
    id 439
    label "wygodny"
  ]
  node [
    id 440
    label "niezale&#380;ny"
  ]
  node [
    id 441
    label "rzedni&#281;cie"
  ]
  node [
    id 442
    label "niespieszny"
  ]
  node [
    id 443
    label "zwalnianie_si&#281;"
  ]
  node [
    id 444
    label "wakowa&#263;"
  ]
  node [
    id 445
    label "rozwadnianie"
  ]
  node [
    id 446
    label "rozwodnienie"
  ]
  node [
    id 447
    label "zrzedni&#281;cie"
  ]
  node [
    id 448
    label "rozrzedzanie"
  ]
  node [
    id 449
    label "rozrzedzenie"
  ]
  node [
    id 450
    label "strza&#322;"
  ]
  node [
    id 451
    label "zwolnienie_si&#281;"
  ]
  node [
    id 452
    label "osobny"
  ]
  node [
    id 453
    label "rozdeptanie"
  ]
  node [
    id 454
    label "daleki"
  ]
  node [
    id 455
    label "rozdeptywanie"
  ]
  node [
    id 456
    label "&#322;atwy"
  ]
  node [
    id 457
    label "nieformalny"
  ]
  node [
    id 458
    label "dodatkowy"
  ]
  node [
    id 459
    label "przyjemny"
  ]
  node [
    id 460
    label "beztroski"
  ]
  node [
    id 461
    label "nieokre&#347;lony"
  ]
  node [
    id 462
    label "nie&#347;cis&#322;y"
  ]
  node [
    id 463
    label "nieregularny"
  ]
  node [
    id 464
    label "lekko"
  ]
  node [
    id 465
    label "&#322;atwo"
  ]
  node [
    id 466
    label "odlegle"
  ]
  node [
    id 467
    label "przyjemnie"
  ]
  node [
    id 468
    label "nieformalnie"
  ]
  node [
    id 469
    label "measuredly"
  ]
  node [
    id 470
    label "k&#261;sa&#263;"
  ]
  node [
    id 471
    label "papusia&#263;"
  ]
  node [
    id 472
    label "k&#322;u&#263;"
  ]
  node [
    id 473
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 474
    label "&#380;re&#263;"
  ]
  node [
    id 475
    label "wpieprza&#263;"
  ]
  node [
    id 476
    label "write_out"
  ]
  node [
    id 477
    label "konsumowa&#263;"
  ]
  node [
    id 478
    label "przeszkadza&#263;"
  ]
  node [
    id 479
    label "bole&#263;"
  ]
  node [
    id 480
    label "incision"
  ]
  node [
    id 481
    label "rap"
  ]
  node [
    id 482
    label "&#347;lepi&#263;"
  ]
  node [
    id 483
    label "wbija&#263;"
  ]
  node [
    id 484
    label "dotyka&#263;"
  ]
  node [
    id 485
    label "pull"
  ]
  node [
    id 486
    label "doskwiera&#263;"
  ]
  node [
    id 487
    label "szkodzi&#263;"
  ]
  node [
    id 488
    label "mr&#243;z"
  ]
  node [
    id 489
    label "kaganiec"
  ]
  node [
    id 490
    label "kaleczy&#263;"
  ]
  node [
    id 491
    label "z&#380;era&#263;"
  ]
  node [
    id 492
    label "jeer"
  ]
  node [
    id 493
    label "marnowa&#263;"
  ]
  node [
    id 494
    label "dzia&#322;a&#263;_na_nerwy"
  ]
  node [
    id 495
    label "maca&#263;"
  ]
  node [
    id 496
    label "ptactwo"
  ]
  node [
    id 497
    label "tuszka"
  ]
  node [
    id 498
    label "domestic_fowl"
  ]
  node [
    id 499
    label "skubarka"
  ]
  node [
    id 500
    label "macanie"
  ]
  node [
    id 501
    label "bia&#322;e_mi&#281;so"
  ]
  node [
    id 502
    label "tokowa&#263;"
  ]
  node [
    id 503
    label "fauna"
  ]
  node [
    id 504
    label "zbi&#243;r"
  ]
  node [
    id 505
    label "kr&#243;lik"
  ]
  node [
    id 506
    label "ryba"
  ]
  node [
    id 507
    label "sondowa&#263;"
  ]
  node [
    id 508
    label "finger"
  ]
  node [
    id 509
    label "grope"
  ]
  node [
    id 510
    label "hodowa&#263;"
  ]
  node [
    id 511
    label "badanie"
  ]
  node [
    id 512
    label "dotykanie"
  ]
  node [
    id 513
    label "pomacanie"
  ]
  node [
    id 514
    label "feel"
  ]
  node [
    id 515
    label "palpation"
  ]
  node [
    id 516
    label "namacanie"
  ]
  node [
    id 517
    label "hodowanie"
  ]
  node [
    id 518
    label "maszyna_przemys&#322;owa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
]
