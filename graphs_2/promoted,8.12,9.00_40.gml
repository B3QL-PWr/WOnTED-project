graph [
  node [
    id 0
    label "cztery"
    origin "text"
  ]
  node [
    id 1
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "areszt"
    origin "text"
  ]
  node [
    id 3
    label "ranek"
  ]
  node [
    id 4
    label "doba"
  ]
  node [
    id 5
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 6
    label "noc"
  ]
  node [
    id 7
    label "podwiecz&#243;r"
  ]
  node [
    id 8
    label "po&#322;udnie"
  ]
  node [
    id 9
    label "godzina"
  ]
  node [
    id 10
    label "przedpo&#322;udnie"
  ]
  node [
    id 11
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 12
    label "long_time"
  ]
  node [
    id 13
    label "wiecz&#243;r"
  ]
  node [
    id 14
    label "t&#322;usty_czwartek"
  ]
  node [
    id 15
    label "popo&#322;udnie"
  ]
  node [
    id 16
    label "walentynki"
  ]
  node [
    id 17
    label "czynienie_si&#281;"
  ]
  node [
    id 18
    label "s&#322;o&#324;ce"
  ]
  node [
    id 19
    label "rano"
  ]
  node [
    id 20
    label "tydzie&#324;"
  ]
  node [
    id 21
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 22
    label "wzej&#347;cie"
  ]
  node [
    id 23
    label "czas"
  ]
  node [
    id 24
    label "wsta&#263;"
  ]
  node [
    id 25
    label "day"
  ]
  node [
    id 26
    label "termin"
  ]
  node [
    id 27
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 28
    label "wstanie"
  ]
  node [
    id 29
    label "przedwiecz&#243;r"
  ]
  node [
    id 30
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 31
    label "Sylwester"
  ]
  node [
    id 32
    label "poprzedzanie"
  ]
  node [
    id 33
    label "czasoprzestrze&#324;"
  ]
  node [
    id 34
    label "laba"
  ]
  node [
    id 35
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 36
    label "chronometria"
  ]
  node [
    id 37
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 38
    label "rachuba_czasu"
  ]
  node [
    id 39
    label "przep&#322;ywanie"
  ]
  node [
    id 40
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 41
    label "czasokres"
  ]
  node [
    id 42
    label "odczyt"
  ]
  node [
    id 43
    label "chwila"
  ]
  node [
    id 44
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 45
    label "dzieje"
  ]
  node [
    id 46
    label "kategoria_gramatyczna"
  ]
  node [
    id 47
    label "poprzedzenie"
  ]
  node [
    id 48
    label "trawienie"
  ]
  node [
    id 49
    label "pochodzi&#263;"
  ]
  node [
    id 50
    label "period"
  ]
  node [
    id 51
    label "okres_czasu"
  ]
  node [
    id 52
    label "poprzedza&#263;"
  ]
  node [
    id 53
    label "schy&#322;ek"
  ]
  node [
    id 54
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 55
    label "odwlekanie_si&#281;"
  ]
  node [
    id 56
    label "zegar"
  ]
  node [
    id 57
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 58
    label "czwarty_wymiar"
  ]
  node [
    id 59
    label "pochodzenie"
  ]
  node [
    id 60
    label "koniugacja"
  ]
  node [
    id 61
    label "Zeitgeist"
  ]
  node [
    id 62
    label "trawi&#263;"
  ]
  node [
    id 63
    label "pogoda"
  ]
  node [
    id 64
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 65
    label "poprzedzi&#263;"
  ]
  node [
    id 66
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 67
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 68
    label "time_period"
  ]
  node [
    id 69
    label "nazewnictwo"
  ]
  node [
    id 70
    label "term"
  ]
  node [
    id 71
    label "przypadni&#281;cie"
  ]
  node [
    id 72
    label "ekspiracja"
  ]
  node [
    id 73
    label "przypa&#347;&#263;"
  ]
  node [
    id 74
    label "chronogram"
  ]
  node [
    id 75
    label "praktyka"
  ]
  node [
    id 76
    label "nazwa"
  ]
  node [
    id 77
    label "przyj&#281;cie"
  ]
  node [
    id 78
    label "spotkanie"
  ]
  node [
    id 79
    label "night"
  ]
  node [
    id 80
    label "zach&#243;d"
  ]
  node [
    id 81
    label "vesper"
  ]
  node [
    id 82
    label "pora"
  ]
  node [
    id 83
    label "odwieczerz"
  ]
  node [
    id 84
    label "blady_&#347;wit"
  ]
  node [
    id 85
    label "podkurek"
  ]
  node [
    id 86
    label "aurora"
  ]
  node [
    id 87
    label "wsch&#243;d"
  ]
  node [
    id 88
    label "zjawisko"
  ]
  node [
    id 89
    label "&#347;rodek"
  ]
  node [
    id 90
    label "obszar"
  ]
  node [
    id 91
    label "Ziemia"
  ]
  node [
    id 92
    label "dwunasta"
  ]
  node [
    id 93
    label "strona_&#347;wiata"
  ]
  node [
    id 94
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 95
    label "dopo&#322;udnie"
  ]
  node [
    id 96
    label "p&#243;&#322;noc"
  ]
  node [
    id 97
    label "nokturn"
  ]
  node [
    id 98
    label "time"
  ]
  node [
    id 99
    label "p&#243;&#322;godzina"
  ]
  node [
    id 100
    label "jednostka_czasu"
  ]
  node [
    id 101
    label "minuta"
  ]
  node [
    id 102
    label "kwadrans"
  ]
  node [
    id 103
    label "jednostka_geologiczna"
  ]
  node [
    id 104
    label "weekend"
  ]
  node [
    id 105
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 106
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 107
    label "miesi&#261;c"
  ]
  node [
    id 108
    label "S&#322;o&#324;ce"
  ]
  node [
    id 109
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 110
    label "&#347;wiat&#322;o"
  ]
  node [
    id 111
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 112
    label "kochanie"
  ]
  node [
    id 113
    label "sunlight"
  ]
  node [
    id 114
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 115
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 116
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 117
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 118
    label "mount"
  ]
  node [
    id 119
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 120
    label "wzej&#347;&#263;"
  ]
  node [
    id 121
    label "ascend"
  ]
  node [
    id 122
    label "kuca&#263;"
  ]
  node [
    id 123
    label "wyzdrowie&#263;"
  ]
  node [
    id 124
    label "opu&#347;ci&#263;"
  ]
  node [
    id 125
    label "rise"
  ]
  node [
    id 126
    label "arise"
  ]
  node [
    id 127
    label "stan&#261;&#263;"
  ]
  node [
    id 128
    label "przesta&#263;"
  ]
  node [
    id 129
    label "wyzdrowienie"
  ]
  node [
    id 130
    label "le&#380;enie"
  ]
  node [
    id 131
    label "kl&#281;czenie"
  ]
  node [
    id 132
    label "opuszczenie"
  ]
  node [
    id 133
    label "uniesienie_si&#281;"
  ]
  node [
    id 134
    label "siedzenie"
  ]
  node [
    id 135
    label "beginning"
  ]
  node [
    id 136
    label "przestanie"
  ]
  node [
    id 137
    label "grudzie&#324;"
  ]
  node [
    id 138
    label "luty"
  ]
  node [
    id 139
    label "ograniczenie"
  ]
  node [
    id 140
    label "procedura"
  ]
  node [
    id 141
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 142
    label "zarz&#261;d"
  ]
  node [
    id 143
    label "poj&#281;cie"
  ]
  node [
    id 144
    label "miejsce_odosobnienia"
  ]
  node [
    id 145
    label "ul"
  ]
  node [
    id 146
    label "g&#322;upstwo"
  ]
  node [
    id 147
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 148
    label "prevention"
  ]
  node [
    id 149
    label "pomiarkowanie"
  ]
  node [
    id 150
    label "przeszkoda"
  ]
  node [
    id 151
    label "intelekt"
  ]
  node [
    id 152
    label "zmniejszenie"
  ]
  node [
    id 153
    label "reservation"
  ]
  node [
    id 154
    label "przekroczenie"
  ]
  node [
    id 155
    label "finlandyzacja"
  ]
  node [
    id 156
    label "otoczenie"
  ]
  node [
    id 157
    label "osielstwo"
  ]
  node [
    id 158
    label "zdyskryminowanie"
  ]
  node [
    id 159
    label "warunek"
  ]
  node [
    id 160
    label "limitation"
  ]
  node [
    id 161
    label "cecha"
  ]
  node [
    id 162
    label "przekroczy&#263;"
  ]
  node [
    id 163
    label "przekraczanie"
  ]
  node [
    id 164
    label "przekracza&#263;"
  ]
  node [
    id 165
    label "barrier"
  ]
  node [
    id 166
    label "s&#261;d"
  ]
  node [
    id 167
    label "facylitator"
  ]
  node [
    id 168
    label "przebieg"
  ]
  node [
    id 169
    label "legislacyjnie"
  ]
  node [
    id 170
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 171
    label "metodyka"
  ]
  node [
    id 172
    label "tryb"
  ]
  node [
    id 173
    label "pos&#322;uchanie"
  ]
  node [
    id 174
    label "skumanie"
  ]
  node [
    id 175
    label "orientacja"
  ]
  node [
    id 176
    label "wytw&#243;r"
  ]
  node [
    id 177
    label "teoria"
  ]
  node [
    id 178
    label "clasp"
  ]
  node [
    id 179
    label "przem&#243;wienie"
  ]
  node [
    id 180
    label "forma"
  ]
  node [
    id 181
    label "zorientowanie"
  ]
  node [
    id 182
    label "organ_sp&#243;&#322;ki"
  ]
  node [
    id 183
    label "biuro"
  ]
  node [
    id 184
    label "kierownictwo"
  ]
  node [
    id 185
    label "siedziba"
  ]
  node [
    id 186
    label "Bruksela"
  ]
  node [
    id 187
    label "organizacja"
  ]
  node [
    id 188
    label "administration"
  ]
  node [
    id 189
    label "centrala"
  ]
  node [
    id 190
    label "administracja"
  ]
  node [
    id 191
    label "czynno&#347;&#263;"
  ]
  node [
    id 192
    label "w&#322;adza"
  ]
  node [
    id 193
    label "podkarmiaczka"
  ]
  node [
    id 194
    label "gniazdo"
  ]
  node [
    id 195
    label "miejsce"
  ]
  node [
    id 196
    label "pomieszczenie"
  ]
  node [
    id 197
    label "plaster"
  ]
  node [
    id 198
    label "rejonowy"
  ]
  node [
    id 199
    label "w"
  ]
  node [
    id 200
    label "Tczew"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 166
    target 198
  ]
  edge [
    source 166
    target 199
  ]
  edge [
    source 166
    target 200
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 198
    target 200
  ]
  edge [
    source 199
    target 200
  ]
]
