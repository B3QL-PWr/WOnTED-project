graph [
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "hit"
    origin "text"
  ]
  node [
    id 2
    label "moda"
  ]
  node [
    id 3
    label "popularny"
  ]
  node [
    id 4
    label "utw&#243;r"
  ]
  node [
    id 5
    label "sensacja"
  ]
  node [
    id 6
    label "nowina"
  ]
  node [
    id 7
    label "odkrycie"
  ]
  node [
    id 8
    label "nowostka"
  ]
  node [
    id 9
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 10
    label "nius"
  ]
  node [
    id 11
    label "message"
  ]
  node [
    id 12
    label "novum"
  ]
  node [
    id 13
    label "zamieszanie"
  ]
  node [
    id 14
    label "rozg&#322;os"
  ]
  node [
    id 15
    label "disclosure"
  ]
  node [
    id 16
    label "niespodzianka"
  ]
  node [
    id 17
    label "podekscytowanie"
  ]
  node [
    id 18
    label "obrazowanie"
  ]
  node [
    id 19
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 20
    label "organ"
  ]
  node [
    id 21
    label "tre&#347;&#263;"
  ]
  node [
    id 22
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 23
    label "part"
  ]
  node [
    id 24
    label "element_anatomiczny"
  ]
  node [
    id 25
    label "tekst"
  ]
  node [
    id 26
    label "komunikat"
  ]
  node [
    id 27
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 28
    label "ukazanie"
  ]
  node [
    id 29
    label "detection"
  ]
  node [
    id 30
    label "podniesienie"
  ]
  node [
    id 31
    label "discovery"
  ]
  node [
    id 32
    label "poznanie"
  ]
  node [
    id 33
    label "zsuni&#281;cie"
  ]
  node [
    id 34
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 35
    label "znalezienie"
  ]
  node [
    id 36
    label "jawny"
  ]
  node [
    id 37
    label "objawienie"
  ]
  node [
    id 38
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 39
    label "poinformowanie"
  ]
  node [
    id 40
    label "przyst&#281;pny"
  ]
  node [
    id 41
    label "znany"
  ]
  node [
    id 42
    label "popularnie"
  ]
  node [
    id 43
    label "&#322;atwy"
  ]
  node [
    id 44
    label "miara_tendencji_centralnej"
  ]
  node [
    id 45
    label "odzie&#380;"
  ]
  node [
    id 46
    label "fashionistka"
  ]
  node [
    id 47
    label "przeb&#243;j"
  ]
  node [
    id 48
    label "dziedzina"
  ]
  node [
    id 49
    label "powodzenie"
  ]
  node [
    id 50
    label "zwyczaj"
  ]
  node [
    id 51
    label "XD"
  ]
  node [
    id 52
    label "go&#347;cia"
  ]
  node [
    id 53
    label "pan"
  ]
  node [
    id 54
    label "XXX"
  ]
  node [
    id 55
    label "telewizor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 53
    target 54
  ]
]
