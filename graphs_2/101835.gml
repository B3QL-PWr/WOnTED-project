graph [
  node [
    id 0
    label "viii"
    origin "text"
  ]
  node [
    id 1
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 2
    label "festiwal"
    origin "text"
  ]
  node [
    id 3
    label "konkurs"
    origin "text"
  ]
  node [
    id 4
    label "m&#322;odzie&#380;owy"
    origin "text"
  ]
  node [
    id 5
    label "dziennikarstwo"
    origin "text"
  ]
  node [
    id 6
    label "sk&#322;on"
    origin "text"
  ]
  node [
    id 7
    label "dniepra"
    origin "text"
  ]
  node [
    id 8
    label "rocznik"
    origin "text"
  ]
  node [
    id 9
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 10
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 11
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 12
    label "internationalization"
  ]
  node [
    id 13
    label "transgraniczny"
  ]
  node [
    id 14
    label "uwsp&#243;lnienie"
  ]
  node [
    id 15
    label "udost&#281;pnienie"
  ]
  node [
    id 16
    label "zbiorowo"
  ]
  node [
    id 17
    label "udost&#281;pnianie"
  ]
  node [
    id 18
    label "Przystanek_Woodstock"
  ]
  node [
    id 19
    label "Woodstock"
  ]
  node [
    id 20
    label "Opole"
  ]
  node [
    id 21
    label "Eurowizja"
  ]
  node [
    id 22
    label "Open'er"
  ]
  node [
    id 23
    label "Metalmania"
  ]
  node [
    id 24
    label "impreza"
  ]
  node [
    id 25
    label "Brutal"
  ]
  node [
    id 26
    label "FAMA"
  ]
  node [
    id 27
    label "Interwizja"
  ]
  node [
    id 28
    label "Nowe_Horyzonty"
  ]
  node [
    id 29
    label "impra"
  ]
  node [
    id 30
    label "rozrywka"
  ]
  node [
    id 31
    label "przyj&#281;cie"
  ]
  node [
    id 32
    label "okazja"
  ]
  node [
    id 33
    label "party"
  ]
  node [
    id 34
    label "hipster"
  ]
  node [
    id 35
    label "&#346;wierkle"
  ]
  node [
    id 36
    label "casting"
  ]
  node [
    id 37
    label "nab&#243;r"
  ]
  node [
    id 38
    label "eliminacje"
  ]
  node [
    id 39
    label "emulation"
  ]
  node [
    id 40
    label "recruitment"
  ]
  node [
    id 41
    label "wyb&#243;r"
  ]
  node [
    id 42
    label "faza"
  ]
  node [
    id 43
    label "runda"
  ]
  node [
    id 44
    label "turniej"
  ]
  node [
    id 45
    label "retirement"
  ]
  node [
    id 46
    label "przes&#322;uchanie"
  ]
  node [
    id 47
    label "w&#281;dkarstwo"
  ]
  node [
    id 48
    label "m&#322;odzie&#380;owo"
  ]
  node [
    id 49
    label "nauka_humanistyczna"
  ]
  node [
    id 50
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 51
    label "medioznawca"
  ]
  node [
    id 52
    label "absolutorium"
  ]
  node [
    id 53
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 54
    label "dzia&#322;anie"
  ]
  node [
    id 55
    label "activity"
  ]
  node [
    id 56
    label "tekstolog"
  ]
  node [
    id 57
    label "socjolog"
  ]
  node [
    id 58
    label "kulturoznawca"
  ]
  node [
    id 59
    label "&#263;wiczenie"
  ]
  node [
    id 60
    label "ruch"
  ]
  node [
    id 61
    label "slope"
  ]
  node [
    id 62
    label "rozwijanie"
  ]
  node [
    id 63
    label "doskonalenie"
  ]
  node [
    id 64
    label "training"
  ]
  node [
    id 65
    label "use"
  ]
  node [
    id 66
    label "po&#263;wiczenie"
  ]
  node [
    id 67
    label "szlifowanie"
  ]
  node [
    id 68
    label "trening"
  ]
  node [
    id 69
    label "doskonalszy"
  ]
  node [
    id 70
    label "poruszanie_si&#281;"
  ]
  node [
    id 71
    label "utw&#243;r"
  ]
  node [
    id 72
    label "zadanie"
  ]
  node [
    id 73
    label "egzercycja"
  ]
  node [
    id 74
    label "obw&#243;d"
  ]
  node [
    id 75
    label "czynno&#347;&#263;"
  ]
  node [
    id 76
    label "ch&#322;ostanie"
  ]
  node [
    id 77
    label "ulepszanie"
  ]
  node [
    id 78
    label "mechanika"
  ]
  node [
    id 79
    label "utrzymywanie"
  ]
  node [
    id 80
    label "move"
  ]
  node [
    id 81
    label "poruszenie"
  ]
  node [
    id 82
    label "movement"
  ]
  node [
    id 83
    label "myk"
  ]
  node [
    id 84
    label "utrzyma&#263;"
  ]
  node [
    id 85
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 86
    label "zjawisko"
  ]
  node [
    id 87
    label "utrzymanie"
  ]
  node [
    id 88
    label "travel"
  ]
  node [
    id 89
    label "kanciasty"
  ]
  node [
    id 90
    label "commercial_enterprise"
  ]
  node [
    id 91
    label "model"
  ]
  node [
    id 92
    label "strumie&#324;"
  ]
  node [
    id 93
    label "proces"
  ]
  node [
    id 94
    label "aktywno&#347;&#263;"
  ]
  node [
    id 95
    label "kr&#243;tki"
  ]
  node [
    id 96
    label "taktyka"
  ]
  node [
    id 97
    label "apraksja"
  ]
  node [
    id 98
    label "natural_process"
  ]
  node [
    id 99
    label "utrzymywa&#263;"
  ]
  node [
    id 100
    label "d&#322;ugi"
  ]
  node [
    id 101
    label "wydarzenie"
  ]
  node [
    id 102
    label "dyssypacja_energii"
  ]
  node [
    id 103
    label "tumult"
  ]
  node [
    id 104
    label "stopek"
  ]
  node [
    id 105
    label "zmiana"
  ]
  node [
    id 106
    label "manewr"
  ]
  node [
    id 107
    label "lokomocja"
  ]
  node [
    id 108
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 109
    label "komunikacja"
  ]
  node [
    id 110
    label "drift"
  ]
  node [
    id 111
    label "formacja"
  ]
  node [
    id 112
    label "yearbook"
  ]
  node [
    id 113
    label "czasopismo"
  ]
  node [
    id 114
    label "kronika"
  ]
  node [
    id 115
    label "Bund"
  ]
  node [
    id 116
    label "Mazowsze"
  ]
  node [
    id 117
    label "PPR"
  ]
  node [
    id 118
    label "Jakobici"
  ]
  node [
    id 119
    label "zesp&#243;&#322;"
  ]
  node [
    id 120
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 121
    label "leksem"
  ]
  node [
    id 122
    label "SLD"
  ]
  node [
    id 123
    label "zespolik"
  ]
  node [
    id 124
    label "Razem"
  ]
  node [
    id 125
    label "PiS"
  ]
  node [
    id 126
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 127
    label "partia"
  ]
  node [
    id 128
    label "Kuomintang"
  ]
  node [
    id 129
    label "ZSL"
  ]
  node [
    id 130
    label "szko&#322;a"
  ]
  node [
    id 131
    label "jednostka"
  ]
  node [
    id 132
    label "organizacja"
  ]
  node [
    id 133
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 134
    label "rugby"
  ]
  node [
    id 135
    label "AWS"
  ]
  node [
    id 136
    label "posta&#263;"
  ]
  node [
    id 137
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 138
    label "blok"
  ]
  node [
    id 139
    label "PO"
  ]
  node [
    id 140
    label "si&#322;a"
  ]
  node [
    id 141
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 142
    label "Federali&#347;ci"
  ]
  node [
    id 143
    label "PSL"
  ]
  node [
    id 144
    label "wojsko"
  ]
  node [
    id 145
    label "Wigowie"
  ]
  node [
    id 146
    label "ZChN"
  ]
  node [
    id 147
    label "egzekutywa"
  ]
  node [
    id 148
    label "The_Beatles"
  ]
  node [
    id 149
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 150
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 151
    label "unit"
  ]
  node [
    id 152
    label "Depeche_Mode"
  ]
  node [
    id 153
    label "forma"
  ]
  node [
    id 154
    label "zapis"
  ]
  node [
    id 155
    label "chronograf"
  ]
  node [
    id 156
    label "latopis"
  ]
  node [
    id 157
    label "ksi&#281;ga"
  ]
  node [
    id 158
    label "egzemplarz"
  ]
  node [
    id 159
    label "psychotest"
  ]
  node [
    id 160
    label "pismo"
  ]
  node [
    id 161
    label "communication"
  ]
  node [
    id 162
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 163
    label "wk&#322;ad"
  ]
  node [
    id 164
    label "zajawka"
  ]
  node [
    id 165
    label "ok&#322;adka"
  ]
  node [
    id 166
    label "Zwrotnica"
  ]
  node [
    id 167
    label "dzia&#322;"
  ]
  node [
    id 168
    label "prasa"
  ]
  node [
    id 169
    label "wiosna"
  ]
  node [
    id 170
    label "na"
  ]
  node [
    id 171
    label "Dniepra"
  ]
  node [
    id 172
    label "junior"
  ]
  node [
    id 173
    label "PRES"
  ]
  node [
    id 174
    label "fundacja"
  ]
  node [
    id 175
    label "nowy"
  ]
  node [
    id 176
    label "medium"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 170
  ]
  edge [
    source 168
    target 171
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 169
    target 171
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 174
    target 176
  ]
  edge [
    source 175
    target 176
  ]
]
