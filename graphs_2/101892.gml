graph [
  node [
    id 0
    label "oblicze"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "godzina"
    origin "text"
  ]
  node [
    id 4
    label "czas"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;oneczny"
    origin "text"
  ]
  node [
    id 6
    label "nowa"
    origin "text"
  ]
  node [
    id 7
    label "jork"
    origin "text"
  ]
  node [
    id 8
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 9
    label "amsterdam"
    origin "text"
  ]
  node [
    id 10
    label "wygl&#261;d"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "comeliness"
  ]
  node [
    id 13
    label "face"
  ]
  node [
    id 14
    label "charakter"
  ]
  node [
    id 15
    label "twarz"
  ]
  node [
    id 16
    label "facjata"
  ]
  node [
    id 17
    label "postarzenie"
  ]
  node [
    id 18
    label "kszta&#322;t"
  ]
  node [
    id 19
    label "postarzanie"
  ]
  node [
    id 20
    label "brzydota"
  ]
  node [
    id 21
    label "portrecista"
  ]
  node [
    id 22
    label "postarza&#263;"
  ]
  node [
    id 23
    label "nadawanie"
  ]
  node [
    id 24
    label "postarzy&#263;"
  ]
  node [
    id 25
    label "cecha"
  ]
  node [
    id 26
    label "widok"
  ]
  node [
    id 27
    label "prostota"
  ]
  node [
    id 28
    label "ubarwienie"
  ]
  node [
    id 29
    label "shape"
  ]
  node [
    id 30
    label "cera"
  ]
  node [
    id 31
    label "wielko&#347;&#263;"
  ]
  node [
    id 32
    label "rys"
  ]
  node [
    id 33
    label "przedstawiciel"
  ]
  node [
    id 34
    label "profil"
  ]
  node [
    id 35
    label "p&#322;e&#263;"
  ]
  node [
    id 36
    label "posta&#263;"
  ]
  node [
    id 37
    label "zas&#322;ona"
  ]
  node [
    id 38
    label "p&#243;&#322;profil"
  ]
  node [
    id 39
    label "policzek"
  ]
  node [
    id 40
    label "brew"
  ]
  node [
    id 41
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 42
    label "uj&#281;cie"
  ]
  node [
    id 43
    label "micha"
  ]
  node [
    id 44
    label "reputacja"
  ]
  node [
    id 45
    label "wyraz_twarzy"
  ]
  node [
    id 46
    label "powieka"
  ]
  node [
    id 47
    label "czo&#322;o"
  ]
  node [
    id 48
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 50
    label "twarzyczka"
  ]
  node [
    id 51
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 52
    label "ucho"
  ]
  node [
    id 53
    label "usta"
  ]
  node [
    id 54
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 55
    label "dzi&#243;b"
  ]
  node [
    id 56
    label "prz&#243;d"
  ]
  node [
    id 57
    label "oko"
  ]
  node [
    id 58
    label "nos"
  ]
  node [
    id 59
    label "podbr&#243;dek"
  ]
  node [
    id 60
    label "liczko"
  ]
  node [
    id 61
    label "pysk"
  ]
  node [
    id 62
    label "maskowato&#347;&#263;"
  ]
  node [
    id 63
    label "przedmiot"
  ]
  node [
    id 64
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 65
    label "zbi&#243;r"
  ]
  node [
    id 66
    label "wydarzenie"
  ]
  node [
    id 67
    label "osobowo&#347;&#263;"
  ]
  node [
    id 68
    label "psychika"
  ]
  node [
    id 69
    label "kompleksja"
  ]
  node [
    id 70
    label "fizjonomia"
  ]
  node [
    id 71
    label "zjawisko"
  ]
  node [
    id 72
    label "entity"
  ]
  node [
    id 73
    label "ludzko&#347;&#263;"
  ]
  node [
    id 74
    label "asymilowanie"
  ]
  node [
    id 75
    label "wapniak"
  ]
  node [
    id 76
    label "asymilowa&#263;"
  ]
  node [
    id 77
    label "os&#322;abia&#263;"
  ]
  node [
    id 78
    label "hominid"
  ]
  node [
    id 79
    label "podw&#322;adny"
  ]
  node [
    id 80
    label "os&#322;abianie"
  ]
  node [
    id 81
    label "g&#322;owa"
  ]
  node [
    id 82
    label "figura"
  ]
  node [
    id 83
    label "dwun&#243;g"
  ]
  node [
    id 84
    label "profanum"
  ]
  node [
    id 85
    label "mikrokosmos"
  ]
  node [
    id 86
    label "nasada"
  ]
  node [
    id 87
    label "duch"
  ]
  node [
    id 88
    label "antropochoria"
  ]
  node [
    id 89
    label "osoba"
  ]
  node [
    id 90
    label "wz&#243;r"
  ]
  node [
    id 91
    label "senior"
  ]
  node [
    id 92
    label "oddzia&#322;ywanie"
  ]
  node [
    id 93
    label "Adam"
  ]
  node [
    id 94
    label "homo_sapiens"
  ]
  node [
    id 95
    label "polifag"
  ]
  node [
    id 96
    label "strych"
  ]
  node [
    id 97
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 98
    label "mie&#263;_miejsce"
  ]
  node [
    id 99
    label "equal"
  ]
  node [
    id 100
    label "trwa&#263;"
  ]
  node [
    id 101
    label "chodzi&#263;"
  ]
  node [
    id 102
    label "si&#281;ga&#263;"
  ]
  node [
    id 103
    label "stan"
  ]
  node [
    id 104
    label "obecno&#347;&#263;"
  ]
  node [
    id 105
    label "stand"
  ]
  node [
    id 106
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 107
    label "uczestniczy&#263;"
  ]
  node [
    id 108
    label "participate"
  ]
  node [
    id 109
    label "robi&#263;"
  ]
  node [
    id 110
    label "istnie&#263;"
  ]
  node [
    id 111
    label "pozostawa&#263;"
  ]
  node [
    id 112
    label "zostawa&#263;"
  ]
  node [
    id 113
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 114
    label "adhere"
  ]
  node [
    id 115
    label "compass"
  ]
  node [
    id 116
    label "korzysta&#263;"
  ]
  node [
    id 117
    label "appreciation"
  ]
  node [
    id 118
    label "osi&#261;ga&#263;"
  ]
  node [
    id 119
    label "dociera&#263;"
  ]
  node [
    id 120
    label "get"
  ]
  node [
    id 121
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 122
    label "mierzy&#263;"
  ]
  node [
    id 123
    label "u&#380;ywa&#263;"
  ]
  node [
    id 124
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 125
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 126
    label "exsert"
  ]
  node [
    id 127
    label "being"
  ]
  node [
    id 128
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 129
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 130
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 131
    label "p&#322;ywa&#263;"
  ]
  node [
    id 132
    label "run"
  ]
  node [
    id 133
    label "bangla&#263;"
  ]
  node [
    id 134
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 135
    label "przebiega&#263;"
  ]
  node [
    id 136
    label "wk&#322;ada&#263;"
  ]
  node [
    id 137
    label "proceed"
  ]
  node [
    id 138
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 139
    label "carry"
  ]
  node [
    id 140
    label "bywa&#263;"
  ]
  node [
    id 141
    label "dziama&#263;"
  ]
  node [
    id 142
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 143
    label "stara&#263;_si&#281;"
  ]
  node [
    id 144
    label "para"
  ]
  node [
    id 145
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 146
    label "str&#243;j"
  ]
  node [
    id 147
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 148
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 149
    label "krok"
  ]
  node [
    id 150
    label "tryb"
  ]
  node [
    id 151
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 152
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 153
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 154
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 155
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 156
    label "continue"
  ]
  node [
    id 157
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 158
    label "Ohio"
  ]
  node [
    id 159
    label "wci&#281;cie"
  ]
  node [
    id 160
    label "Nowy_York"
  ]
  node [
    id 161
    label "warstwa"
  ]
  node [
    id 162
    label "samopoczucie"
  ]
  node [
    id 163
    label "Illinois"
  ]
  node [
    id 164
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 165
    label "state"
  ]
  node [
    id 166
    label "Jukatan"
  ]
  node [
    id 167
    label "Kalifornia"
  ]
  node [
    id 168
    label "Wirginia"
  ]
  node [
    id 169
    label "wektor"
  ]
  node [
    id 170
    label "Goa"
  ]
  node [
    id 171
    label "Teksas"
  ]
  node [
    id 172
    label "Waszyngton"
  ]
  node [
    id 173
    label "miejsce"
  ]
  node [
    id 174
    label "Massachusetts"
  ]
  node [
    id 175
    label "Alaska"
  ]
  node [
    id 176
    label "Arakan"
  ]
  node [
    id 177
    label "Hawaje"
  ]
  node [
    id 178
    label "Maryland"
  ]
  node [
    id 179
    label "punkt"
  ]
  node [
    id 180
    label "Michigan"
  ]
  node [
    id 181
    label "Arizona"
  ]
  node [
    id 182
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 183
    label "Georgia"
  ]
  node [
    id 184
    label "poziom"
  ]
  node [
    id 185
    label "Pensylwania"
  ]
  node [
    id 186
    label "Luizjana"
  ]
  node [
    id 187
    label "Nowy_Meksyk"
  ]
  node [
    id 188
    label "Alabama"
  ]
  node [
    id 189
    label "ilo&#347;&#263;"
  ]
  node [
    id 190
    label "Kansas"
  ]
  node [
    id 191
    label "Oregon"
  ]
  node [
    id 192
    label "Oklahoma"
  ]
  node [
    id 193
    label "Floryda"
  ]
  node [
    id 194
    label "jednostka_administracyjna"
  ]
  node [
    id 195
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 196
    label "time"
  ]
  node [
    id 197
    label "doba"
  ]
  node [
    id 198
    label "p&#243;&#322;godzina"
  ]
  node [
    id 199
    label "jednostka_czasu"
  ]
  node [
    id 200
    label "minuta"
  ]
  node [
    id 201
    label "kwadrans"
  ]
  node [
    id 202
    label "poprzedzanie"
  ]
  node [
    id 203
    label "czasoprzestrze&#324;"
  ]
  node [
    id 204
    label "laba"
  ]
  node [
    id 205
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 206
    label "chronometria"
  ]
  node [
    id 207
    label "rachuba_czasu"
  ]
  node [
    id 208
    label "przep&#322;ywanie"
  ]
  node [
    id 209
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 210
    label "czasokres"
  ]
  node [
    id 211
    label "odczyt"
  ]
  node [
    id 212
    label "chwila"
  ]
  node [
    id 213
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 214
    label "dzieje"
  ]
  node [
    id 215
    label "kategoria_gramatyczna"
  ]
  node [
    id 216
    label "poprzedzenie"
  ]
  node [
    id 217
    label "trawienie"
  ]
  node [
    id 218
    label "pochodzi&#263;"
  ]
  node [
    id 219
    label "period"
  ]
  node [
    id 220
    label "okres_czasu"
  ]
  node [
    id 221
    label "poprzedza&#263;"
  ]
  node [
    id 222
    label "schy&#322;ek"
  ]
  node [
    id 223
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 224
    label "odwlekanie_si&#281;"
  ]
  node [
    id 225
    label "zegar"
  ]
  node [
    id 226
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 227
    label "czwarty_wymiar"
  ]
  node [
    id 228
    label "pochodzenie"
  ]
  node [
    id 229
    label "koniugacja"
  ]
  node [
    id 230
    label "Zeitgeist"
  ]
  node [
    id 231
    label "trawi&#263;"
  ]
  node [
    id 232
    label "pogoda"
  ]
  node [
    id 233
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 234
    label "poprzedzi&#263;"
  ]
  node [
    id 235
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 236
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 237
    label "time_period"
  ]
  node [
    id 238
    label "zapis"
  ]
  node [
    id 239
    label "sekunda"
  ]
  node [
    id 240
    label "jednostka"
  ]
  node [
    id 241
    label "stopie&#324;"
  ]
  node [
    id 242
    label "design"
  ]
  node [
    id 243
    label "tydzie&#324;"
  ]
  node [
    id 244
    label "noc"
  ]
  node [
    id 245
    label "dzie&#324;"
  ]
  node [
    id 246
    label "long_time"
  ]
  node [
    id 247
    label "jednostka_geologiczna"
  ]
  node [
    id 248
    label "blok"
  ]
  node [
    id 249
    label "handout"
  ]
  node [
    id 250
    label "pomiar"
  ]
  node [
    id 251
    label "lecture"
  ]
  node [
    id 252
    label "reading"
  ]
  node [
    id 253
    label "podawanie"
  ]
  node [
    id 254
    label "wyk&#322;ad"
  ]
  node [
    id 255
    label "potrzyma&#263;"
  ]
  node [
    id 256
    label "warunki"
  ]
  node [
    id 257
    label "pok&#243;j"
  ]
  node [
    id 258
    label "atak"
  ]
  node [
    id 259
    label "program"
  ]
  node [
    id 260
    label "meteorology"
  ]
  node [
    id 261
    label "weather"
  ]
  node [
    id 262
    label "prognoza_meteorologiczna"
  ]
  node [
    id 263
    label "czas_wolny"
  ]
  node [
    id 264
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 265
    label "metrologia"
  ]
  node [
    id 266
    label "godzinnik"
  ]
  node [
    id 267
    label "bicie"
  ]
  node [
    id 268
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 269
    label "wahad&#322;o"
  ]
  node [
    id 270
    label "kurant"
  ]
  node [
    id 271
    label "cyferblat"
  ]
  node [
    id 272
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 273
    label "nabicie"
  ]
  node [
    id 274
    label "werk"
  ]
  node [
    id 275
    label "czasomierz"
  ]
  node [
    id 276
    label "tyka&#263;"
  ]
  node [
    id 277
    label "tykn&#261;&#263;"
  ]
  node [
    id 278
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 279
    label "urz&#261;dzenie"
  ]
  node [
    id 280
    label "kotwica"
  ]
  node [
    id 281
    label "fleksja"
  ]
  node [
    id 282
    label "liczba"
  ]
  node [
    id 283
    label "coupling"
  ]
  node [
    id 284
    label "czasownik"
  ]
  node [
    id 285
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 286
    label "orz&#281;sek"
  ]
  node [
    id 287
    label "usuwa&#263;"
  ]
  node [
    id 288
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 289
    label "lutowa&#263;"
  ]
  node [
    id 290
    label "marnowa&#263;"
  ]
  node [
    id 291
    label "przetrawia&#263;"
  ]
  node [
    id 292
    label "poch&#322;ania&#263;"
  ]
  node [
    id 293
    label "digest"
  ]
  node [
    id 294
    label "metal"
  ]
  node [
    id 295
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 296
    label "sp&#281;dza&#263;"
  ]
  node [
    id 297
    label "digestion"
  ]
  node [
    id 298
    label "unicestwianie"
  ]
  node [
    id 299
    label "sp&#281;dzanie"
  ]
  node [
    id 300
    label "contemplation"
  ]
  node [
    id 301
    label "rozk&#322;adanie"
  ]
  node [
    id 302
    label "marnowanie"
  ]
  node [
    id 303
    label "proces_fizjologiczny"
  ]
  node [
    id 304
    label "przetrawianie"
  ]
  node [
    id 305
    label "perystaltyka"
  ]
  node [
    id 306
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 307
    label "zaczynanie_si&#281;"
  ]
  node [
    id 308
    label "wynikanie"
  ]
  node [
    id 309
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 310
    label "origin"
  ]
  node [
    id 311
    label "background"
  ]
  node [
    id 312
    label "geneza"
  ]
  node [
    id 313
    label "beginning"
  ]
  node [
    id 314
    label "przeby&#263;"
  ]
  node [
    id 315
    label "min&#261;&#263;"
  ]
  node [
    id 316
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 317
    label "swimming"
  ]
  node [
    id 318
    label "zago&#347;ci&#263;"
  ]
  node [
    id 319
    label "cross"
  ]
  node [
    id 320
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 321
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 322
    label "przebywa&#263;"
  ]
  node [
    id 323
    label "pour"
  ]
  node [
    id 324
    label "sail"
  ]
  node [
    id 325
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 326
    label "go&#347;ci&#263;"
  ]
  node [
    id 327
    label "mija&#263;"
  ]
  node [
    id 328
    label "mini&#281;cie"
  ]
  node [
    id 329
    label "doznanie"
  ]
  node [
    id 330
    label "zaistnienie"
  ]
  node [
    id 331
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 332
    label "przebycie"
  ]
  node [
    id 333
    label "cruise"
  ]
  node [
    id 334
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 335
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 336
    label "zjawianie_si&#281;"
  ]
  node [
    id 337
    label "przebywanie"
  ]
  node [
    id 338
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 339
    label "mijanie"
  ]
  node [
    id 340
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 341
    label "zaznawanie"
  ]
  node [
    id 342
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 343
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 344
    label "flux"
  ]
  node [
    id 345
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 346
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 347
    label "zrobi&#263;"
  ]
  node [
    id 348
    label "opatrzy&#263;"
  ]
  node [
    id 349
    label "overwhelm"
  ]
  node [
    id 350
    label "opatrywanie"
  ]
  node [
    id 351
    label "odej&#347;cie"
  ]
  node [
    id 352
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 353
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 354
    label "zanikni&#281;cie"
  ]
  node [
    id 355
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 356
    label "ciecz"
  ]
  node [
    id 357
    label "opuszczenie"
  ]
  node [
    id 358
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 359
    label "departure"
  ]
  node [
    id 360
    label "oddalenie_si&#281;"
  ]
  node [
    id 361
    label "date"
  ]
  node [
    id 362
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 363
    label "wynika&#263;"
  ]
  node [
    id 364
    label "fall"
  ]
  node [
    id 365
    label "poby&#263;"
  ]
  node [
    id 366
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 367
    label "bolt"
  ]
  node [
    id 368
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 369
    label "spowodowa&#263;"
  ]
  node [
    id 370
    label "uda&#263;_si&#281;"
  ]
  node [
    id 371
    label "opatrzenie"
  ]
  node [
    id 372
    label "zdarzenie_si&#281;"
  ]
  node [
    id 373
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 374
    label "progress"
  ]
  node [
    id 375
    label "opatrywa&#263;"
  ]
  node [
    id 376
    label "epoka"
  ]
  node [
    id 377
    label "flow"
  ]
  node [
    id 378
    label "choroba_przyrodzona"
  ]
  node [
    id 379
    label "ciota"
  ]
  node [
    id 380
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 381
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 382
    label "kres"
  ]
  node [
    id 383
    label "przestrze&#324;"
  ]
  node [
    id 384
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 385
    label "s&#322;onecznie"
  ]
  node [
    id 386
    label "letni"
  ]
  node [
    id 387
    label "weso&#322;y"
  ]
  node [
    id 388
    label "bezdeszczowy"
  ]
  node [
    id 389
    label "ciep&#322;y"
  ]
  node [
    id 390
    label "bezchmurny"
  ]
  node [
    id 391
    label "pogodny"
  ]
  node [
    id 392
    label "fotowoltaiczny"
  ]
  node [
    id 393
    label "jasny"
  ]
  node [
    id 394
    label "o&#347;wietlenie"
  ]
  node [
    id 395
    label "szczery"
  ]
  node [
    id 396
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 397
    label "jasno"
  ]
  node [
    id 398
    label "o&#347;wietlanie"
  ]
  node [
    id 399
    label "przytomny"
  ]
  node [
    id 400
    label "zrozumia&#322;y"
  ]
  node [
    id 401
    label "niezm&#261;cony"
  ]
  node [
    id 402
    label "bia&#322;y"
  ]
  node [
    id 403
    label "klarowny"
  ]
  node [
    id 404
    label "jednoznaczny"
  ]
  node [
    id 405
    label "dobry"
  ]
  node [
    id 406
    label "mi&#322;y"
  ]
  node [
    id 407
    label "ocieplanie_si&#281;"
  ]
  node [
    id 408
    label "ocieplanie"
  ]
  node [
    id 409
    label "grzanie"
  ]
  node [
    id 410
    label "ocieplenie_si&#281;"
  ]
  node [
    id 411
    label "zagrzanie"
  ]
  node [
    id 412
    label "ocieplenie"
  ]
  node [
    id 413
    label "korzystny"
  ]
  node [
    id 414
    label "przyjemny"
  ]
  node [
    id 415
    label "ciep&#322;o"
  ]
  node [
    id 416
    label "spokojny"
  ]
  node [
    id 417
    label "&#322;adny"
  ]
  node [
    id 418
    label "udany"
  ]
  node [
    id 419
    label "pozytywny"
  ]
  node [
    id 420
    label "pogodnie"
  ]
  node [
    id 421
    label "pijany"
  ]
  node [
    id 422
    label "weso&#322;o"
  ]
  node [
    id 423
    label "beztroski"
  ]
  node [
    id 424
    label "bezopadowy"
  ]
  node [
    id 425
    label "bezdeszczowo"
  ]
  node [
    id 426
    label "bezchmurnie"
  ]
  node [
    id 427
    label "wolny"
  ]
  node [
    id 428
    label "ekologiczny"
  ]
  node [
    id 429
    label "latowy"
  ]
  node [
    id 430
    label "typowy"
  ]
  node [
    id 431
    label "sezonowy"
  ]
  node [
    id 432
    label "letnio"
  ]
  node [
    id 433
    label "oboj&#281;tny"
  ]
  node [
    id 434
    label "nijaki"
  ]
  node [
    id 435
    label "gwiazda"
  ]
  node [
    id 436
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 437
    label "Arktur"
  ]
  node [
    id 438
    label "Gwiazda_Polarna"
  ]
  node [
    id 439
    label "agregatka"
  ]
  node [
    id 440
    label "gromada"
  ]
  node [
    id 441
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 442
    label "S&#322;o&#324;ce"
  ]
  node [
    id 443
    label "Nibiru"
  ]
  node [
    id 444
    label "konstelacja"
  ]
  node [
    id 445
    label "ornament"
  ]
  node [
    id 446
    label "delta_Scuti"
  ]
  node [
    id 447
    label "&#347;wiat&#322;o"
  ]
  node [
    id 448
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 449
    label "obiekt"
  ]
  node [
    id 450
    label "s&#322;awa"
  ]
  node [
    id 451
    label "promie&#324;"
  ]
  node [
    id 452
    label "star"
  ]
  node [
    id 453
    label "gwiazdosz"
  ]
  node [
    id 454
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 455
    label "asocjacja_gwiazd"
  ]
  node [
    id 456
    label "supergrupa"
  ]
  node [
    id 457
    label "pies_miniaturowy"
  ]
  node [
    id 458
    label "pies_do_towarzystwa"
  ]
  node [
    id 459
    label "pies_pokojowy"
  ]
  node [
    id 460
    label "terier"
  ]
  node [
    id 461
    label "pies_ozdobny"
  ]
  node [
    id 462
    label "norowiec"
  ]
  node [
    id 463
    label "terrier"
  ]
  node [
    id 464
    label "nowy"
  ]
  node [
    id 465
    label "Jork"
  ]
  node [
    id 466
    label "los"
  ]
  node [
    id 467
    label "Angeles"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 464
    target 465
  ]
  edge [
    source 466
    target 467
  ]
]
