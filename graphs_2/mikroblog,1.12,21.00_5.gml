graph [
  node [
    id 0
    label "poznan"
    origin "text"
  ]
  node [
    id 1
    label "fajny"
    origin "text"
  ]
  node [
    id 2
    label "mat"
    origin "text"
  ]
  node [
    id 3
    label "lotnisko"
    origin "text"
  ]
  node [
    id 4
    label "dobry"
  ]
  node [
    id 5
    label "klawy"
  ]
  node [
    id 6
    label "byczy"
  ]
  node [
    id 7
    label "fajnie"
  ]
  node [
    id 8
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 9
    label "skuteczny"
  ]
  node [
    id 10
    label "ca&#322;y"
  ]
  node [
    id 11
    label "czw&#243;rka"
  ]
  node [
    id 12
    label "spokojny"
  ]
  node [
    id 13
    label "pos&#322;uszny"
  ]
  node [
    id 14
    label "korzystny"
  ]
  node [
    id 15
    label "drogi"
  ]
  node [
    id 16
    label "pozytywny"
  ]
  node [
    id 17
    label "moralny"
  ]
  node [
    id 18
    label "pomy&#347;lny"
  ]
  node [
    id 19
    label "powitanie"
  ]
  node [
    id 20
    label "grzeczny"
  ]
  node [
    id 21
    label "&#347;mieszny"
  ]
  node [
    id 22
    label "odpowiedni"
  ]
  node [
    id 23
    label "zwrot"
  ]
  node [
    id 24
    label "dobrze"
  ]
  node [
    id 25
    label "dobroczynny"
  ]
  node [
    id 26
    label "mi&#322;y"
  ]
  node [
    id 27
    label "na_schwa&#322;"
  ]
  node [
    id 28
    label "klawo"
  ]
  node [
    id 29
    label "byczo"
  ]
  node [
    id 30
    label "du&#380;y"
  ]
  node [
    id 31
    label "podobny"
  ]
  node [
    id 32
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 33
    label "podoficer_marynarki"
  ]
  node [
    id 34
    label "ruch"
  ]
  node [
    id 35
    label "szachy"
  ]
  node [
    id 36
    label "szach"
  ]
  node [
    id 37
    label "Persja"
  ]
  node [
    id 38
    label "pozycja"
  ]
  node [
    id 39
    label "monarcha"
  ]
  node [
    id 40
    label "Afganistan"
  ]
  node [
    id 41
    label "arrest"
  ]
  node [
    id 42
    label "move"
  ]
  node [
    id 43
    label "zmiana"
  ]
  node [
    id 44
    label "model"
  ]
  node [
    id 45
    label "aktywno&#347;&#263;"
  ]
  node [
    id 46
    label "utrzymywanie"
  ]
  node [
    id 47
    label "utrzymywa&#263;"
  ]
  node [
    id 48
    label "taktyka"
  ]
  node [
    id 49
    label "d&#322;ugi"
  ]
  node [
    id 50
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 51
    label "natural_process"
  ]
  node [
    id 52
    label "kanciasty"
  ]
  node [
    id 53
    label "utrzyma&#263;"
  ]
  node [
    id 54
    label "myk"
  ]
  node [
    id 55
    label "manewr"
  ]
  node [
    id 56
    label "utrzymanie"
  ]
  node [
    id 57
    label "wydarzenie"
  ]
  node [
    id 58
    label "tumult"
  ]
  node [
    id 59
    label "stopek"
  ]
  node [
    id 60
    label "movement"
  ]
  node [
    id 61
    label "strumie&#324;"
  ]
  node [
    id 62
    label "czynno&#347;&#263;"
  ]
  node [
    id 63
    label "komunikacja"
  ]
  node [
    id 64
    label "lokomocja"
  ]
  node [
    id 65
    label "drift"
  ]
  node [
    id 66
    label "commercial_enterprise"
  ]
  node [
    id 67
    label "zjawisko"
  ]
  node [
    id 68
    label "apraksja"
  ]
  node [
    id 69
    label "proces"
  ]
  node [
    id 70
    label "poruszenie"
  ]
  node [
    id 71
    label "mechanika"
  ]
  node [
    id 72
    label "travel"
  ]
  node [
    id 73
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 74
    label "dyssypacja_energii"
  ]
  node [
    id 75
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 76
    label "kr&#243;tki"
  ]
  node [
    id 77
    label "linia_przemiany"
  ]
  node [
    id 78
    label "przes&#322;ona"
  ]
  node [
    id 79
    label "niedoczas"
  ]
  node [
    id 80
    label "zegar_szachowy"
  ]
  node [
    id 81
    label "szachownica"
  ]
  node [
    id 82
    label "dual"
  ]
  node [
    id 83
    label "bicie_w_przelocie"
  ]
  node [
    id 84
    label "p&#243;&#322;ruch"
  ]
  node [
    id 85
    label "tempo"
  ]
  node [
    id 86
    label "sport_umys&#322;owy"
  ]
  node [
    id 87
    label "promocja"
  ]
  node [
    id 88
    label "gra_planszowa"
  ]
  node [
    id 89
    label "roszada"
  ]
  node [
    id 90
    label "p&#322;yta_postojowa"
  ]
  node [
    id 91
    label "pas_startowy"
  ]
  node [
    id 92
    label "betonka"
  ]
  node [
    id 93
    label "droga_ko&#322;owania"
  ]
  node [
    id 94
    label "baza"
  ]
  node [
    id 95
    label "terminal"
  ]
  node [
    id 96
    label "budowla"
  ]
  node [
    id 97
    label "hala"
  ]
  node [
    id 98
    label "aerodrom"
  ]
  node [
    id 99
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 100
    label "za&#322;o&#380;enie"
  ]
  node [
    id 101
    label "poj&#281;cie"
  ]
  node [
    id 102
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 103
    label "punkt_odniesienia"
  ]
  node [
    id 104
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 105
    label "informatyka"
  ]
  node [
    id 106
    label "kolumna"
  ]
  node [
    id 107
    label "pole"
  ]
  node [
    id 108
    label "zbi&#243;r"
  ]
  node [
    id 109
    label "base"
  ]
  node [
    id 110
    label "documentation"
  ]
  node [
    id 111
    label "miejsce"
  ]
  node [
    id 112
    label "boisko"
  ]
  node [
    id 113
    label "zasadzi&#263;"
  ]
  node [
    id 114
    label "baseball"
  ]
  node [
    id 115
    label "rekord"
  ]
  node [
    id 116
    label "stacjonowanie"
  ]
  node [
    id 117
    label "zasadzenie"
  ]
  node [
    id 118
    label "podstawa"
  ]
  node [
    id 119
    label "kosmetyk"
  ]
  node [
    id 120
    label "podstawowy"
  ]
  node [
    id 121
    label "system_bazy_danych"
  ]
  node [
    id 122
    label "rzecz"
  ]
  node [
    id 123
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 124
    label "stan_surowy"
  ]
  node [
    id 125
    label "postanie"
  ]
  node [
    id 126
    label "zbudowa&#263;"
  ]
  node [
    id 127
    label "obudowywa&#263;"
  ]
  node [
    id 128
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 129
    label "obudowywanie"
  ]
  node [
    id 130
    label "konstrukcja"
  ]
  node [
    id 131
    label "Sukiennice"
  ]
  node [
    id 132
    label "kolumnada"
  ]
  node [
    id 133
    label "korpus"
  ]
  node [
    id 134
    label "zbudowanie"
  ]
  node [
    id 135
    label "fundament"
  ]
  node [
    id 136
    label "obudowa&#263;"
  ]
  node [
    id 137
    label "obudowanie"
  ]
  node [
    id 138
    label "pod&#322;oga"
  ]
  node [
    id 139
    label "urz&#261;dzenie"
  ]
  node [
    id 140
    label "port"
  ]
  node [
    id 141
    label "pastwisko"
  ]
  node [
    id 142
    label "kopalnia"
  ]
  node [
    id 143
    label "halizna"
  ]
  node [
    id 144
    label "dworzec"
  ]
  node [
    id 145
    label "budynek"
  ]
  node [
    id 146
    label "oczyszczalnia"
  ]
  node [
    id 147
    label "fabryka"
  ]
  node [
    id 148
    label "pomieszczenie"
  ]
  node [
    id 149
    label "pi&#281;tro"
  ]
  node [
    id 150
    label "huta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
]
