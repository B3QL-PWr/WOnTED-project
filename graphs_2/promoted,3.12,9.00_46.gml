graph [
  node [
    id 0
    label "podczas"
    origin "text"
  ]
  node [
    id 1
    label "transmisja"
    origin "text"
  ]
  node [
    id 2
    label "dwukrotnie"
    origin "text"
  ]
  node [
    id 3
    label "nazwa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niemiecki"
    origin "text"
  ]
  node [
    id 5
    label "trener"
    origin "text"
  ]
  node [
    id 6
    label "reinharda"
    origin "text"
  ]
  node [
    id 7
    label "hessa"
    origin "text"
  ]
  node [
    id 8
    label "przekaz"
  ]
  node [
    id 9
    label "program"
  ]
  node [
    id 10
    label "zjawisko"
  ]
  node [
    id 11
    label "proces"
  ]
  node [
    id 12
    label "kognicja"
  ]
  node [
    id 13
    label "przebieg"
  ]
  node [
    id 14
    label "rozprawa"
  ]
  node [
    id 15
    label "wydarzenie"
  ]
  node [
    id 16
    label "legislacyjnie"
  ]
  node [
    id 17
    label "przes&#322;anka"
  ]
  node [
    id 18
    label "nast&#281;pstwo"
  ]
  node [
    id 19
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 20
    label "boski"
  ]
  node [
    id 21
    label "krajobraz"
  ]
  node [
    id 22
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 23
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 24
    label "przywidzenie"
  ]
  node [
    id 25
    label "presence"
  ]
  node [
    id 26
    label "charakter"
  ]
  node [
    id 27
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 28
    label "instalowa&#263;"
  ]
  node [
    id 29
    label "oprogramowanie"
  ]
  node [
    id 30
    label "odinstalowywa&#263;"
  ]
  node [
    id 31
    label "spis"
  ]
  node [
    id 32
    label "zaprezentowanie"
  ]
  node [
    id 33
    label "podprogram"
  ]
  node [
    id 34
    label "ogranicznik_referencyjny"
  ]
  node [
    id 35
    label "course_of_study"
  ]
  node [
    id 36
    label "booklet"
  ]
  node [
    id 37
    label "dzia&#322;"
  ]
  node [
    id 38
    label "odinstalowanie"
  ]
  node [
    id 39
    label "broszura"
  ]
  node [
    id 40
    label "wytw&#243;r"
  ]
  node [
    id 41
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 42
    label "kana&#322;"
  ]
  node [
    id 43
    label "teleferie"
  ]
  node [
    id 44
    label "zainstalowanie"
  ]
  node [
    id 45
    label "struktura_organizacyjna"
  ]
  node [
    id 46
    label "pirat"
  ]
  node [
    id 47
    label "zaprezentowa&#263;"
  ]
  node [
    id 48
    label "prezentowanie"
  ]
  node [
    id 49
    label "prezentowa&#263;"
  ]
  node [
    id 50
    label "interfejs"
  ]
  node [
    id 51
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 52
    label "okno"
  ]
  node [
    id 53
    label "blok"
  ]
  node [
    id 54
    label "punkt"
  ]
  node [
    id 55
    label "folder"
  ]
  node [
    id 56
    label "zainstalowa&#263;"
  ]
  node [
    id 57
    label "za&#322;o&#380;enie"
  ]
  node [
    id 58
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 59
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 60
    label "ram&#243;wka"
  ]
  node [
    id 61
    label "tryb"
  ]
  node [
    id 62
    label "emitowa&#263;"
  ]
  node [
    id 63
    label "emitowanie"
  ]
  node [
    id 64
    label "odinstalowywanie"
  ]
  node [
    id 65
    label "instrukcja"
  ]
  node [
    id 66
    label "informatyka"
  ]
  node [
    id 67
    label "deklaracja"
  ]
  node [
    id 68
    label "menu"
  ]
  node [
    id 69
    label "sekcja_krytyczna"
  ]
  node [
    id 70
    label "furkacja"
  ]
  node [
    id 71
    label "podstawa"
  ]
  node [
    id 72
    label "instalowanie"
  ]
  node [
    id 73
    label "oferta"
  ]
  node [
    id 74
    label "odinstalowa&#263;"
  ]
  node [
    id 75
    label "kwota"
  ]
  node [
    id 76
    label "explicite"
  ]
  node [
    id 77
    label "blankiet"
  ]
  node [
    id 78
    label "znaczenie"
  ]
  node [
    id 79
    label "draft"
  ]
  node [
    id 80
    label "tekst"
  ]
  node [
    id 81
    label "transakcja"
  ]
  node [
    id 82
    label "implicite"
  ]
  node [
    id 83
    label "dokument"
  ]
  node [
    id 84
    label "order"
  ]
  node [
    id 85
    label "dwakro&#263;"
  ]
  node [
    id 86
    label "kilkukrotnie"
  ]
  node [
    id 87
    label "dwukrotny"
  ]
  node [
    id 88
    label "parokrotnie"
  ]
  node [
    id 89
    label "kilkukrotny"
  ]
  node [
    id 90
    label "kilkakro&#263;"
  ]
  node [
    id 91
    label "parokrotny"
  ]
  node [
    id 92
    label "podw&#243;jny"
  ]
  node [
    id 93
    label "broadcast"
  ]
  node [
    id 94
    label "nada&#263;"
  ]
  node [
    id 95
    label "okre&#347;li&#263;"
  ]
  node [
    id 96
    label "zdecydowa&#263;"
  ]
  node [
    id 97
    label "zrobi&#263;"
  ]
  node [
    id 98
    label "spowodowa&#263;"
  ]
  node [
    id 99
    label "situate"
  ]
  node [
    id 100
    label "nominate"
  ]
  node [
    id 101
    label "da&#263;"
  ]
  node [
    id 102
    label "za&#322;atwi&#263;"
  ]
  node [
    id 103
    label "give"
  ]
  node [
    id 104
    label "zarekomendowa&#263;"
  ]
  node [
    id 105
    label "przes&#322;a&#263;"
  ]
  node [
    id 106
    label "donie&#347;&#263;"
  ]
  node [
    id 107
    label "po_niemiecku"
  ]
  node [
    id 108
    label "German"
  ]
  node [
    id 109
    label "niemiecko"
  ]
  node [
    id 110
    label "cenar"
  ]
  node [
    id 111
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 112
    label "europejski"
  ]
  node [
    id 113
    label "strudel"
  ]
  node [
    id 114
    label "niemiec"
  ]
  node [
    id 115
    label "pionier"
  ]
  node [
    id 116
    label "zachodnioeuropejski"
  ]
  node [
    id 117
    label "j&#281;zyk"
  ]
  node [
    id 118
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 119
    label "junkers"
  ]
  node [
    id 120
    label "szwabski"
  ]
  node [
    id 121
    label "szwabsko"
  ]
  node [
    id 122
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 123
    label "po_szwabsku"
  ]
  node [
    id 124
    label "platt"
  ]
  node [
    id 125
    label "europejsko"
  ]
  node [
    id 126
    label "ciasto"
  ]
  node [
    id 127
    label "&#380;o&#322;nierz"
  ]
  node [
    id 128
    label "saper"
  ]
  node [
    id 129
    label "prekursor"
  ]
  node [
    id 130
    label "osadnik"
  ]
  node [
    id 131
    label "skaut"
  ]
  node [
    id 132
    label "g&#322;osiciel"
  ]
  node [
    id 133
    label "taniec_ludowy"
  ]
  node [
    id 134
    label "melodia"
  ]
  node [
    id 135
    label "taniec"
  ]
  node [
    id 136
    label "podgrzewacz"
  ]
  node [
    id 137
    label "samolot_wojskowy"
  ]
  node [
    id 138
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 139
    label "langosz"
  ]
  node [
    id 140
    label "moreska"
  ]
  node [
    id 141
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 142
    label "zachodni"
  ]
  node [
    id 143
    label "po_europejsku"
  ]
  node [
    id 144
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 145
    label "European"
  ]
  node [
    id 146
    label "typowy"
  ]
  node [
    id 147
    label "charakterystyczny"
  ]
  node [
    id 148
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 149
    label "artykulator"
  ]
  node [
    id 150
    label "kod"
  ]
  node [
    id 151
    label "kawa&#322;ek"
  ]
  node [
    id 152
    label "przedmiot"
  ]
  node [
    id 153
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 154
    label "gramatyka"
  ]
  node [
    id 155
    label "stylik"
  ]
  node [
    id 156
    label "przet&#322;umaczenie"
  ]
  node [
    id 157
    label "formalizowanie"
  ]
  node [
    id 158
    label "ssanie"
  ]
  node [
    id 159
    label "ssa&#263;"
  ]
  node [
    id 160
    label "language"
  ]
  node [
    id 161
    label "liza&#263;"
  ]
  node [
    id 162
    label "napisa&#263;"
  ]
  node [
    id 163
    label "konsonantyzm"
  ]
  node [
    id 164
    label "wokalizm"
  ]
  node [
    id 165
    label "pisa&#263;"
  ]
  node [
    id 166
    label "fonetyka"
  ]
  node [
    id 167
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 168
    label "jeniec"
  ]
  node [
    id 169
    label "but"
  ]
  node [
    id 170
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 171
    label "po_koroniarsku"
  ]
  node [
    id 172
    label "kultura_duchowa"
  ]
  node [
    id 173
    label "t&#322;umaczenie"
  ]
  node [
    id 174
    label "m&#243;wienie"
  ]
  node [
    id 175
    label "pype&#263;"
  ]
  node [
    id 176
    label "lizanie"
  ]
  node [
    id 177
    label "pismo"
  ]
  node [
    id 178
    label "formalizowa&#263;"
  ]
  node [
    id 179
    label "rozumie&#263;"
  ]
  node [
    id 180
    label "organ"
  ]
  node [
    id 181
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 182
    label "rozumienie"
  ]
  node [
    id 183
    label "spos&#243;b"
  ]
  node [
    id 184
    label "makroglosja"
  ]
  node [
    id 185
    label "m&#243;wi&#263;"
  ]
  node [
    id 186
    label "jama_ustna"
  ]
  node [
    id 187
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 188
    label "formacja_geologiczna"
  ]
  node [
    id 189
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 190
    label "natural_language"
  ]
  node [
    id 191
    label "s&#322;ownictwo"
  ]
  node [
    id 192
    label "urz&#261;dzenie"
  ]
  node [
    id 193
    label "nauczyciel"
  ]
  node [
    id 194
    label "opiekun"
  ]
  node [
    id 195
    label "cz&#322;owiek"
  ]
  node [
    id 196
    label "konsultant"
  ]
  node [
    id 197
    label "Daniel_Dubicki"
  ]
  node [
    id 198
    label "mentor"
  ]
  node [
    id 199
    label "nadzorca"
  ]
  node [
    id 200
    label "funkcjonariusz"
  ]
  node [
    id 201
    label "ludzko&#347;&#263;"
  ]
  node [
    id 202
    label "asymilowanie"
  ]
  node [
    id 203
    label "wapniak"
  ]
  node [
    id 204
    label "asymilowa&#263;"
  ]
  node [
    id 205
    label "os&#322;abia&#263;"
  ]
  node [
    id 206
    label "posta&#263;"
  ]
  node [
    id 207
    label "hominid"
  ]
  node [
    id 208
    label "podw&#322;adny"
  ]
  node [
    id 209
    label "os&#322;abianie"
  ]
  node [
    id 210
    label "g&#322;owa"
  ]
  node [
    id 211
    label "figura"
  ]
  node [
    id 212
    label "portrecista"
  ]
  node [
    id 213
    label "dwun&#243;g"
  ]
  node [
    id 214
    label "profanum"
  ]
  node [
    id 215
    label "mikrokosmos"
  ]
  node [
    id 216
    label "nasada"
  ]
  node [
    id 217
    label "duch"
  ]
  node [
    id 218
    label "antropochoria"
  ]
  node [
    id 219
    label "osoba"
  ]
  node [
    id 220
    label "wz&#243;r"
  ]
  node [
    id 221
    label "senior"
  ]
  node [
    id 222
    label "oddzia&#322;ywanie"
  ]
  node [
    id 223
    label "Adam"
  ]
  node [
    id 224
    label "homo_sapiens"
  ]
  node [
    id 225
    label "polifag"
  ]
  node [
    id 226
    label "belfer"
  ]
  node [
    id 227
    label "kszta&#322;ciciel"
  ]
  node [
    id 228
    label "preceptor"
  ]
  node [
    id 229
    label "pedagog"
  ]
  node [
    id 230
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 231
    label "szkolnik"
  ]
  node [
    id 232
    label "profesor"
  ]
  node [
    id 233
    label "popularyzator"
  ]
  node [
    id 234
    label "autorytet"
  ]
  node [
    id 235
    label "sensat"
  ]
  node [
    id 236
    label "diagnosta"
  ]
  node [
    id 237
    label "ro&#347;lina"
  ]
  node [
    id 238
    label "sztywniak"
  ]
  node [
    id 239
    label "Towia&#324;ski"
  ]
  node [
    id 240
    label "doradca"
  ]
  node [
    id 241
    label "pracownik"
  ]
  node [
    id 242
    label "ekspert"
  ]
  node [
    id 243
    label "Reinharda"
  ]
  node [
    id 244
    label "Hessa"
  ]
  node [
    id 245
    label "Rudolf"
  ]
  node [
    id 246
    label "Hesse"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 243
    target 244
  ]
  edge [
    source 245
    target 246
  ]
]
