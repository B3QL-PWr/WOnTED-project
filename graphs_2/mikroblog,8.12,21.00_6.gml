graph [
  node [
    id 0
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 1
    label "smutny"
    origin "text"
  ]
  node [
    id 2
    label "historia"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "szybki"
  ]
  node [
    id 5
    label "jednowyrazowy"
  ]
  node [
    id 6
    label "bliski"
  ]
  node [
    id 7
    label "s&#322;aby"
  ]
  node [
    id 8
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 9
    label "kr&#243;tko"
  ]
  node [
    id 10
    label "drobny"
  ]
  node [
    id 11
    label "ruch"
  ]
  node [
    id 12
    label "brak"
  ]
  node [
    id 13
    label "z&#322;y"
  ]
  node [
    id 14
    label "pieski"
  ]
  node [
    id 15
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 16
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 17
    label "niekorzystny"
  ]
  node [
    id 18
    label "z&#322;oszczenie"
  ]
  node [
    id 19
    label "sierdzisty"
  ]
  node [
    id 20
    label "niegrzeczny"
  ]
  node [
    id 21
    label "zez&#322;oszczenie"
  ]
  node [
    id 22
    label "zdenerwowany"
  ]
  node [
    id 23
    label "negatywny"
  ]
  node [
    id 24
    label "rozgniewanie"
  ]
  node [
    id 25
    label "gniewanie"
  ]
  node [
    id 26
    label "niemoralny"
  ]
  node [
    id 27
    label "&#378;le"
  ]
  node [
    id 28
    label "niepomy&#347;lny"
  ]
  node [
    id 29
    label "syf"
  ]
  node [
    id 30
    label "sprawny"
  ]
  node [
    id 31
    label "efektywny"
  ]
  node [
    id 32
    label "zwi&#281;&#378;le"
  ]
  node [
    id 33
    label "oszcz&#281;dny"
  ]
  node [
    id 34
    label "nietrwa&#322;y"
  ]
  node [
    id 35
    label "mizerny"
  ]
  node [
    id 36
    label "marnie"
  ]
  node [
    id 37
    label "delikatny"
  ]
  node [
    id 38
    label "po&#347;ledni"
  ]
  node [
    id 39
    label "niezdrowy"
  ]
  node [
    id 40
    label "nieumiej&#281;tny"
  ]
  node [
    id 41
    label "s&#322;abo"
  ]
  node [
    id 42
    label "nieznaczny"
  ]
  node [
    id 43
    label "lura"
  ]
  node [
    id 44
    label "nieudany"
  ]
  node [
    id 45
    label "s&#322;abowity"
  ]
  node [
    id 46
    label "zawodny"
  ]
  node [
    id 47
    label "&#322;agodny"
  ]
  node [
    id 48
    label "md&#322;y"
  ]
  node [
    id 49
    label "niedoskona&#322;y"
  ]
  node [
    id 50
    label "przemijaj&#261;cy"
  ]
  node [
    id 51
    label "niemocny"
  ]
  node [
    id 52
    label "niefajny"
  ]
  node [
    id 53
    label "kiepsko"
  ]
  node [
    id 54
    label "intensywny"
  ]
  node [
    id 55
    label "prosty"
  ]
  node [
    id 56
    label "temperamentny"
  ]
  node [
    id 57
    label "bystrolotny"
  ]
  node [
    id 58
    label "dynamiczny"
  ]
  node [
    id 59
    label "szybko"
  ]
  node [
    id 60
    label "bezpo&#347;redni"
  ]
  node [
    id 61
    label "energiczny"
  ]
  node [
    id 62
    label "blisko"
  ]
  node [
    id 63
    label "cz&#322;owiek"
  ]
  node [
    id 64
    label "znajomy"
  ]
  node [
    id 65
    label "zwi&#261;zany"
  ]
  node [
    id 66
    label "przesz&#322;y"
  ]
  node [
    id 67
    label "silny"
  ]
  node [
    id 68
    label "zbli&#380;enie"
  ]
  node [
    id 69
    label "oddalony"
  ]
  node [
    id 70
    label "dok&#322;adny"
  ]
  node [
    id 71
    label "nieodleg&#322;y"
  ]
  node [
    id 72
    label "przysz&#322;y"
  ]
  node [
    id 73
    label "gotowy"
  ]
  node [
    id 74
    label "ma&#322;y"
  ]
  node [
    id 75
    label "skromny"
  ]
  node [
    id 76
    label "niesamodzielny"
  ]
  node [
    id 77
    label "niewa&#380;ny"
  ]
  node [
    id 78
    label "podhala&#324;ski"
  ]
  node [
    id 79
    label "taniec_ludowy"
  ]
  node [
    id 80
    label "szczup&#322;y"
  ]
  node [
    id 81
    label "drobno"
  ]
  node [
    id 82
    label "ma&#322;oletni"
  ]
  node [
    id 83
    label "nieistnienie"
  ]
  node [
    id 84
    label "odej&#347;cie"
  ]
  node [
    id 85
    label "defect"
  ]
  node [
    id 86
    label "gap"
  ]
  node [
    id 87
    label "odej&#347;&#263;"
  ]
  node [
    id 88
    label "wada"
  ]
  node [
    id 89
    label "odchodzi&#263;"
  ]
  node [
    id 90
    label "wyr&#243;b"
  ]
  node [
    id 91
    label "odchodzenie"
  ]
  node [
    id 92
    label "prywatywny"
  ]
  node [
    id 93
    label "jednocz&#322;onowy"
  ]
  node [
    id 94
    label "mechanika"
  ]
  node [
    id 95
    label "utrzymywanie"
  ]
  node [
    id 96
    label "move"
  ]
  node [
    id 97
    label "poruszenie"
  ]
  node [
    id 98
    label "movement"
  ]
  node [
    id 99
    label "myk"
  ]
  node [
    id 100
    label "utrzyma&#263;"
  ]
  node [
    id 101
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 102
    label "zjawisko"
  ]
  node [
    id 103
    label "utrzymanie"
  ]
  node [
    id 104
    label "travel"
  ]
  node [
    id 105
    label "kanciasty"
  ]
  node [
    id 106
    label "commercial_enterprise"
  ]
  node [
    id 107
    label "model"
  ]
  node [
    id 108
    label "strumie&#324;"
  ]
  node [
    id 109
    label "proces"
  ]
  node [
    id 110
    label "aktywno&#347;&#263;"
  ]
  node [
    id 111
    label "taktyka"
  ]
  node [
    id 112
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 113
    label "apraksja"
  ]
  node [
    id 114
    label "natural_process"
  ]
  node [
    id 115
    label "utrzymywa&#263;"
  ]
  node [
    id 116
    label "d&#322;ugi"
  ]
  node [
    id 117
    label "wydarzenie"
  ]
  node [
    id 118
    label "dyssypacja_energii"
  ]
  node [
    id 119
    label "tumult"
  ]
  node [
    id 120
    label "stopek"
  ]
  node [
    id 121
    label "czynno&#347;&#263;"
  ]
  node [
    id 122
    label "zmiana"
  ]
  node [
    id 123
    label "manewr"
  ]
  node [
    id 124
    label "lokomocja"
  ]
  node [
    id 125
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 126
    label "komunikacja"
  ]
  node [
    id 127
    label "drift"
  ]
  node [
    id 128
    label "przykry"
  ]
  node [
    id 129
    label "smutno"
  ]
  node [
    id 130
    label "negatywnie"
  ]
  node [
    id 131
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 132
    label "nieprzyjemny"
  ]
  node [
    id 133
    label "ujemnie"
  ]
  node [
    id 134
    label "niemi&#322;y"
  ]
  node [
    id 135
    label "przykro"
  ]
  node [
    id 136
    label "dokuczliwy"
  ]
  node [
    id 137
    label "nieprzyjemnie"
  ]
  node [
    id 138
    label "smutnie"
  ]
  node [
    id 139
    label "historiografia"
  ]
  node [
    id 140
    label "nauka_humanistyczna"
  ]
  node [
    id 141
    label "nautologia"
  ]
  node [
    id 142
    label "przedmiot"
  ]
  node [
    id 143
    label "epigrafika"
  ]
  node [
    id 144
    label "muzealnictwo"
  ]
  node [
    id 145
    label "report"
  ]
  node [
    id 146
    label "hista"
  ]
  node [
    id 147
    label "przebiec"
  ]
  node [
    id 148
    label "zabytkoznawstwo"
  ]
  node [
    id 149
    label "historia_gospodarcza"
  ]
  node [
    id 150
    label "motyw"
  ]
  node [
    id 151
    label "kierunek"
  ]
  node [
    id 152
    label "varsavianistyka"
  ]
  node [
    id 153
    label "filigranistyka"
  ]
  node [
    id 154
    label "neografia"
  ]
  node [
    id 155
    label "prezentyzm"
  ]
  node [
    id 156
    label "genealogia"
  ]
  node [
    id 157
    label "ikonografia"
  ]
  node [
    id 158
    label "bizantynistyka"
  ]
  node [
    id 159
    label "epoka"
  ]
  node [
    id 160
    label "historia_sztuki"
  ]
  node [
    id 161
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 162
    label "ruralistyka"
  ]
  node [
    id 163
    label "annalistyka"
  ]
  node [
    id 164
    label "charakter"
  ]
  node [
    id 165
    label "papirologia"
  ]
  node [
    id 166
    label "heraldyka"
  ]
  node [
    id 167
    label "archiwistyka"
  ]
  node [
    id 168
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 169
    label "dyplomatyka"
  ]
  node [
    id 170
    label "numizmatyka"
  ]
  node [
    id 171
    label "chronologia"
  ]
  node [
    id 172
    label "wypowied&#378;"
  ]
  node [
    id 173
    label "historyka"
  ]
  node [
    id 174
    label "prozopografia"
  ]
  node [
    id 175
    label "sfragistyka"
  ]
  node [
    id 176
    label "weksylologia"
  ]
  node [
    id 177
    label "paleografia"
  ]
  node [
    id 178
    label "mediewistyka"
  ]
  node [
    id 179
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 180
    label "przebiegni&#281;cie"
  ]
  node [
    id 181
    label "fabu&#322;a"
  ]
  node [
    id 182
    label "koleje_losu"
  ]
  node [
    id 183
    label "&#380;ycie"
  ]
  node [
    id 184
    label "czas"
  ]
  node [
    id 185
    label "zboczenie"
  ]
  node [
    id 186
    label "om&#243;wienie"
  ]
  node [
    id 187
    label "sponiewieranie"
  ]
  node [
    id 188
    label "discipline"
  ]
  node [
    id 189
    label "rzecz"
  ]
  node [
    id 190
    label "omawia&#263;"
  ]
  node [
    id 191
    label "kr&#261;&#380;enie"
  ]
  node [
    id 192
    label "tre&#347;&#263;"
  ]
  node [
    id 193
    label "robienie"
  ]
  node [
    id 194
    label "sponiewiera&#263;"
  ]
  node [
    id 195
    label "element"
  ]
  node [
    id 196
    label "entity"
  ]
  node [
    id 197
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 198
    label "tematyka"
  ]
  node [
    id 199
    label "w&#261;tek"
  ]
  node [
    id 200
    label "zbaczanie"
  ]
  node [
    id 201
    label "program_nauczania"
  ]
  node [
    id 202
    label "om&#243;wi&#263;"
  ]
  node [
    id 203
    label "omawianie"
  ]
  node [
    id 204
    label "thing"
  ]
  node [
    id 205
    label "kultura"
  ]
  node [
    id 206
    label "istota"
  ]
  node [
    id 207
    label "zbacza&#263;"
  ]
  node [
    id 208
    label "zboczy&#263;"
  ]
  node [
    id 209
    label "pos&#322;uchanie"
  ]
  node [
    id 210
    label "s&#261;d"
  ]
  node [
    id 211
    label "sparafrazowanie"
  ]
  node [
    id 212
    label "strawestowa&#263;"
  ]
  node [
    id 213
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 214
    label "trawestowa&#263;"
  ]
  node [
    id 215
    label "sparafrazowa&#263;"
  ]
  node [
    id 216
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 217
    label "sformu&#322;owanie"
  ]
  node [
    id 218
    label "parafrazowanie"
  ]
  node [
    id 219
    label "ozdobnik"
  ]
  node [
    id 220
    label "delimitacja"
  ]
  node [
    id 221
    label "parafrazowa&#263;"
  ]
  node [
    id 222
    label "stylizacja"
  ]
  node [
    id 223
    label "komunikat"
  ]
  node [
    id 224
    label "trawestowanie"
  ]
  node [
    id 225
    label "strawestowanie"
  ]
  node [
    id 226
    label "rezultat"
  ]
  node [
    id 227
    label "przebieg"
  ]
  node [
    id 228
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 229
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 230
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 231
    label "praktyka"
  ]
  node [
    id 232
    label "system"
  ]
  node [
    id 233
    label "przeorientowywanie"
  ]
  node [
    id 234
    label "studia"
  ]
  node [
    id 235
    label "linia"
  ]
  node [
    id 236
    label "bok"
  ]
  node [
    id 237
    label "skr&#281;canie"
  ]
  node [
    id 238
    label "skr&#281;ca&#263;"
  ]
  node [
    id 239
    label "przeorientowywa&#263;"
  ]
  node [
    id 240
    label "orientowanie"
  ]
  node [
    id 241
    label "skr&#281;ci&#263;"
  ]
  node [
    id 242
    label "przeorientowanie"
  ]
  node [
    id 243
    label "zorientowanie"
  ]
  node [
    id 244
    label "przeorientowa&#263;"
  ]
  node [
    id 245
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 246
    label "metoda"
  ]
  node [
    id 247
    label "ty&#322;"
  ]
  node [
    id 248
    label "zorientowa&#263;"
  ]
  node [
    id 249
    label "g&#243;ra"
  ]
  node [
    id 250
    label "orientowa&#263;"
  ]
  node [
    id 251
    label "spos&#243;b"
  ]
  node [
    id 252
    label "ideologia"
  ]
  node [
    id 253
    label "orientacja"
  ]
  node [
    id 254
    label "prz&#243;d"
  ]
  node [
    id 255
    label "bearing"
  ]
  node [
    id 256
    label "skr&#281;cenie"
  ]
  node [
    id 257
    label "aalen"
  ]
  node [
    id 258
    label "jura_wczesna"
  ]
  node [
    id 259
    label "holocen"
  ]
  node [
    id 260
    label "pliocen"
  ]
  node [
    id 261
    label "plejstocen"
  ]
  node [
    id 262
    label "paleocen"
  ]
  node [
    id 263
    label "dzieje"
  ]
  node [
    id 264
    label "bajos"
  ]
  node [
    id 265
    label "kelowej"
  ]
  node [
    id 266
    label "eocen"
  ]
  node [
    id 267
    label "jednostka_geologiczna"
  ]
  node [
    id 268
    label "okres"
  ]
  node [
    id 269
    label "schy&#322;ek"
  ]
  node [
    id 270
    label "miocen"
  ]
  node [
    id 271
    label "&#347;rodkowy_trias"
  ]
  node [
    id 272
    label "term"
  ]
  node [
    id 273
    label "Zeitgeist"
  ]
  node [
    id 274
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 275
    label "wczesny_trias"
  ]
  node [
    id 276
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 277
    label "jura_&#347;rodkowa"
  ]
  node [
    id 278
    label "oligocen"
  ]
  node [
    id 279
    label "w&#281;ze&#322;"
  ]
  node [
    id 280
    label "perypetia"
  ]
  node [
    id 281
    label "opowiadanie"
  ]
  node [
    id 282
    label "datacja"
  ]
  node [
    id 283
    label "dendrochronologia"
  ]
  node [
    id 284
    label "kolejno&#347;&#263;"
  ]
  node [
    id 285
    label "plastyka"
  ]
  node [
    id 286
    label "&#347;redniowiecze"
  ]
  node [
    id 287
    label "descendencja"
  ]
  node [
    id 288
    label "drzewo_genealogiczne"
  ]
  node [
    id 289
    label "procedencja"
  ]
  node [
    id 290
    label "pochodzenie"
  ]
  node [
    id 291
    label "medal"
  ]
  node [
    id 292
    label "kolekcjonerstwo"
  ]
  node [
    id 293
    label "numismatics"
  ]
  node [
    id 294
    label "archeologia"
  ]
  node [
    id 295
    label "archiwoznawstwo"
  ]
  node [
    id 296
    label "Byzantine_Empire"
  ]
  node [
    id 297
    label "pismo"
  ]
  node [
    id 298
    label "brachygrafia"
  ]
  node [
    id 299
    label "architektura"
  ]
  node [
    id 300
    label "nauka"
  ]
  node [
    id 301
    label "oksza"
  ]
  node [
    id 302
    label "pas"
  ]
  node [
    id 303
    label "s&#322;up"
  ]
  node [
    id 304
    label "barwa_heraldyczna"
  ]
  node [
    id 305
    label "herb"
  ]
  node [
    id 306
    label "or&#281;&#380;"
  ]
  node [
    id 307
    label "museum"
  ]
  node [
    id 308
    label "bibliologia"
  ]
  node [
    id 309
    label "historiography"
  ]
  node [
    id 310
    label "pi&#347;miennictwo"
  ]
  node [
    id 311
    label "metodologia"
  ]
  node [
    id 312
    label "fraza"
  ]
  node [
    id 313
    label "temat"
  ]
  node [
    id 314
    label "melodia"
  ]
  node [
    id 315
    label "cecha"
  ]
  node [
    id 316
    label "przyczyna"
  ]
  node [
    id 317
    label "sytuacja"
  ]
  node [
    id 318
    label "ozdoba"
  ]
  node [
    id 319
    label "umowa"
  ]
  node [
    id 320
    label "cover"
  ]
  node [
    id 321
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 322
    label "zbi&#243;r"
  ]
  node [
    id 323
    label "osobowo&#347;&#263;"
  ]
  node [
    id 324
    label "psychika"
  ]
  node [
    id 325
    label "posta&#263;"
  ]
  node [
    id 326
    label "kompleksja"
  ]
  node [
    id 327
    label "fizjonomia"
  ]
  node [
    id 328
    label "activity"
  ]
  node [
    id 329
    label "bezproblemowy"
  ]
  node [
    id 330
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 331
    label "przeby&#263;"
  ]
  node [
    id 332
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 333
    label "run"
  ]
  node [
    id 334
    label "proceed"
  ]
  node [
    id 335
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 336
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 337
    label "przemierzy&#263;"
  ]
  node [
    id 338
    label "fly"
  ]
  node [
    id 339
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 340
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 341
    label "przesun&#261;&#263;"
  ]
  node [
    id 342
    label "przemkni&#281;cie"
  ]
  node [
    id 343
    label "zabrzmienie"
  ]
  node [
    id 344
    label "przebycie"
  ]
  node [
    id 345
    label "zdarzenie_si&#281;"
  ]
  node [
    id 346
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 347
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 348
    label "mie&#263;_miejsce"
  ]
  node [
    id 349
    label "equal"
  ]
  node [
    id 350
    label "trwa&#263;"
  ]
  node [
    id 351
    label "chodzi&#263;"
  ]
  node [
    id 352
    label "si&#281;ga&#263;"
  ]
  node [
    id 353
    label "stan"
  ]
  node [
    id 354
    label "obecno&#347;&#263;"
  ]
  node [
    id 355
    label "stand"
  ]
  node [
    id 356
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 357
    label "uczestniczy&#263;"
  ]
  node [
    id 358
    label "participate"
  ]
  node [
    id 359
    label "robi&#263;"
  ]
  node [
    id 360
    label "istnie&#263;"
  ]
  node [
    id 361
    label "pozostawa&#263;"
  ]
  node [
    id 362
    label "zostawa&#263;"
  ]
  node [
    id 363
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 364
    label "adhere"
  ]
  node [
    id 365
    label "compass"
  ]
  node [
    id 366
    label "korzysta&#263;"
  ]
  node [
    id 367
    label "appreciation"
  ]
  node [
    id 368
    label "osi&#261;ga&#263;"
  ]
  node [
    id 369
    label "dociera&#263;"
  ]
  node [
    id 370
    label "get"
  ]
  node [
    id 371
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 372
    label "mierzy&#263;"
  ]
  node [
    id 373
    label "u&#380;ywa&#263;"
  ]
  node [
    id 374
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 375
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 376
    label "exsert"
  ]
  node [
    id 377
    label "being"
  ]
  node [
    id 378
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 379
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 380
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 381
    label "p&#322;ywa&#263;"
  ]
  node [
    id 382
    label "bangla&#263;"
  ]
  node [
    id 383
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 384
    label "przebiega&#263;"
  ]
  node [
    id 385
    label "wk&#322;ada&#263;"
  ]
  node [
    id 386
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 387
    label "carry"
  ]
  node [
    id 388
    label "bywa&#263;"
  ]
  node [
    id 389
    label "dziama&#263;"
  ]
  node [
    id 390
    label "stara&#263;_si&#281;"
  ]
  node [
    id 391
    label "para"
  ]
  node [
    id 392
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 393
    label "str&#243;j"
  ]
  node [
    id 394
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 395
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 396
    label "krok"
  ]
  node [
    id 397
    label "tryb"
  ]
  node [
    id 398
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 399
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 400
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 401
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 402
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 403
    label "continue"
  ]
  node [
    id 404
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 405
    label "Ohio"
  ]
  node [
    id 406
    label "wci&#281;cie"
  ]
  node [
    id 407
    label "Nowy_York"
  ]
  node [
    id 408
    label "warstwa"
  ]
  node [
    id 409
    label "samopoczucie"
  ]
  node [
    id 410
    label "Illinois"
  ]
  node [
    id 411
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 412
    label "state"
  ]
  node [
    id 413
    label "Jukatan"
  ]
  node [
    id 414
    label "Kalifornia"
  ]
  node [
    id 415
    label "Wirginia"
  ]
  node [
    id 416
    label "wektor"
  ]
  node [
    id 417
    label "Goa"
  ]
  node [
    id 418
    label "Teksas"
  ]
  node [
    id 419
    label "Waszyngton"
  ]
  node [
    id 420
    label "miejsce"
  ]
  node [
    id 421
    label "Massachusetts"
  ]
  node [
    id 422
    label "Alaska"
  ]
  node [
    id 423
    label "Arakan"
  ]
  node [
    id 424
    label "Hawaje"
  ]
  node [
    id 425
    label "Maryland"
  ]
  node [
    id 426
    label "punkt"
  ]
  node [
    id 427
    label "Michigan"
  ]
  node [
    id 428
    label "Arizona"
  ]
  node [
    id 429
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 430
    label "Georgia"
  ]
  node [
    id 431
    label "poziom"
  ]
  node [
    id 432
    label "Pensylwania"
  ]
  node [
    id 433
    label "shape"
  ]
  node [
    id 434
    label "Luizjana"
  ]
  node [
    id 435
    label "Nowy_Meksyk"
  ]
  node [
    id 436
    label "Alabama"
  ]
  node [
    id 437
    label "ilo&#347;&#263;"
  ]
  node [
    id 438
    label "Kansas"
  ]
  node [
    id 439
    label "Oregon"
  ]
  node [
    id 440
    label "Oklahoma"
  ]
  node [
    id 441
    label "Floryda"
  ]
  node [
    id 442
    label "jednostka_administracyjna"
  ]
  node [
    id 443
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
]
