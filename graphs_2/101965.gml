graph [
  node [
    id 0
    label "znale&#378;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "ostatnio"
    origin "text"
  ]
  node [
    id 3
    label "powa&#380;ny"
    origin "text"
  ]
  node [
    id 4
    label "koncert"
    origin "text"
  ]
  node [
    id 5
    label "wratislavia"
    origin "text"
  ]
  node [
    id 6
    label "cantans"
    origin "text"
  ]
  node [
    id 7
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 8
    label "gdyby"
    origin "text"
  ]
  node [
    id 9
    label "odcinek"
    origin "text"
  ]
  node [
    id 10
    label "south"
    origin "text"
  ]
  node [
    id 11
    label "park"
    origin "text"
  ]
  node [
    id 12
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 13
    label "meloman"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "wiedza"
    origin "text"
  ]
  node [
    id 16
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 17
    label "czerp"
    origin "text"
  ]
  node [
    id 18
    label "literatura"
    origin "text"
  ]
  node [
    id 19
    label "sprzed"
    origin "text"
  ]
  node [
    id 20
    label "rok"
    origin "text"
  ]
  node [
    id 21
    label "aktualnie"
  ]
  node [
    id 22
    label "poprzednio"
  ]
  node [
    id 23
    label "ostatni"
  ]
  node [
    id 24
    label "ninie"
  ]
  node [
    id 25
    label "aktualny"
  ]
  node [
    id 26
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 27
    label "poprzedni"
  ]
  node [
    id 28
    label "wcze&#347;niej"
  ]
  node [
    id 29
    label "kolejny"
  ]
  node [
    id 30
    label "cz&#322;owiek"
  ]
  node [
    id 31
    label "niedawno"
  ]
  node [
    id 32
    label "pozosta&#322;y"
  ]
  node [
    id 33
    label "sko&#324;czony"
  ]
  node [
    id 34
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 35
    label "najgorszy"
  ]
  node [
    id 36
    label "istota_&#380;ywa"
  ]
  node [
    id 37
    label "w&#261;tpliwy"
  ]
  node [
    id 38
    label "du&#380;y"
  ]
  node [
    id 39
    label "trudny"
  ]
  node [
    id 40
    label "ci&#281;&#380;ki"
  ]
  node [
    id 41
    label "prawdziwy"
  ]
  node [
    id 42
    label "spowa&#380;nienie"
  ]
  node [
    id 43
    label "ci&#281;&#380;ko"
  ]
  node [
    id 44
    label "gro&#378;ny"
  ]
  node [
    id 45
    label "powa&#380;nie"
  ]
  node [
    id 46
    label "powa&#380;nienie"
  ]
  node [
    id 47
    label "niebezpieczny"
  ]
  node [
    id 48
    label "gro&#378;nie"
  ]
  node [
    id 49
    label "nad&#261;sany"
  ]
  node [
    id 50
    label "k&#322;opotliwy"
  ]
  node [
    id 51
    label "skomplikowany"
  ]
  node [
    id 52
    label "wymagaj&#261;cy"
  ]
  node [
    id 53
    label "&#380;ywny"
  ]
  node [
    id 54
    label "szczery"
  ]
  node [
    id 55
    label "naturalny"
  ]
  node [
    id 56
    label "naprawd&#281;"
  ]
  node [
    id 57
    label "realnie"
  ]
  node [
    id 58
    label "podobny"
  ]
  node [
    id 59
    label "zgodny"
  ]
  node [
    id 60
    label "m&#261;dry"
  ]
  node [
    id 61
    label "prawdziwie"
  ]
  node [
    id 62
    label "monumentalny"
  ]
  node [
    id 63
    label "mocny"
  ]
  node [
    id 64
    label "kompletny"
  ]
  node [
    id 65
    label "masywny"
  ]
  node [
    id 66
    label "wielki"
  ]
  node [
    id 67
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 68
    label "przyswajalny"
  ]
  node [
    id 69
    label "niezgrabny"
  ]
  node [
    id 70
    label "liczny"
  ]
  node [
    id 71
    label "nieprzejrzysty"
  ]
  node [
    id 72
    label "niedelikatny"
  ]
  node [
    id 73
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 74
    label "intensywny"
  ]
  node [
    id 75
    label "wolny"
  ]
  node [
    id 76
    label "nieudany"
  ]
  node [
    id 77
    label "zbrojny"
  ]
  node [
    id 78
    label "dotkliwy"
  ]
  node [
    id 79
    label "charakterystyczny"
  ]
  node [
    id 80
    label "bojowy"
  ]
  node [
    id 81
    label "ambitny"
  ]
  node [
    id 82
    label "grubo"
  ]
  node [
    id 83
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 84
    label "doros&#322;y"
  ]
  node [
    id 85
    label "znaczny"
  ]
  node [
    id 86
    label "niema&#322;o"
  ]
  node [
    id 87
    label "wiele"
  ]
  node [
    id 88
    label "rozwini&#281;ty"
  ]
  node [
    id 89
    label "dorodny"
  ]
  node [
    id 90
    label "wa&#380;ny"
  ]
  node [
    id 91
    label "du&#380;o"
  ]
  node [
    id 92
    label "monumentalnie"
  ]
  node [
    id 93
    label "charakterystycznie"
  ]
  node [
    id 94
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 95
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 96
    label "nieudanie"
  ]
  node [
    id 97
    label "mocno"
  ]
  node [
    id 98
    label "wolno"
  ]
  node [
    id 99
    label "kompletnie"
  ]
  node [
    id 100
    label "dotkliwie"
  ]
  node [
    id 101
    label "niezgrabnie"
  ]
  node [
    id 102
    label "hard"
  ]
  node [
    id 103
    label "&#378;le"
  ]
  node [
    id 104
    label "masywnie"
  ]
  node [
    id 105
    label "heavily"
  ]
  node [
    id 106
    label "niedelikatnie"
  ]
  node [
    id 107
    label "intensywnie"
  ]
  node [
    id 108
    label "bardzo"
  ]
  node [
    id 109
    label "posmutnienie"
  ]
  node [
    id 110
    label "wydoro&#347;lenie"
  ]
  node [
    id 111
    label "uspokojenie_si&#281;"
  ]
  node [
    id 112
    label "doro&#347;lenie"
  ]
  node [
    id 113
    label "smutnienie"
  ]
  node [
    id 114
    label "uspokajanie_si&#281;"
  ]
  node [
    id 115
    label "pokaz"
  ]
  node [
    id 116
    label "performance"
  ]
  node [
    id 117
    label "wyst&#281;p"
  ]
  node [
    id 118
    label "show"
  ]
  node [
    id 119
    label "bogactwo"
  ]
  node [
    id 120
    label "mn&#243;stwo"
  ]
  node [
    id 121
    label "utw&#243;r"
  ]
  node [
    id 122
    label "zjawisko"
  ]
  node [
    id 123
    label "szale&#324;stwo"
  ]
  node [
    id 124
    label "p&#322;acz"
  ]
  node [
    id 125
    label "proces"
  ]
  node [
    id 126
    label "boski"
  ]
  node [
    id 127
    label "krajobraz"
  ]
  node [
    id 128
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 129
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 130
    label "przywidzenie"
  ]
  node [
    id 131
    label "presence"
  ]
  node [
    id 132
    label "charakter"
  ]
  node [
    id 133
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 134
    label "pokaz&#243;wka"
  ]
  node [
    id 135
    label "prezenter"
  ]
  node [
    id 136
    label "wydarzenie"
  ]
  node [
    id 137
    label "wyraz"
  ]
  node [
    id 138
    label "impreza"
  ]
  node [
    id 139
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 140
    label "tingel-tangel"
  ]
  node [
    id 141
    label "numer"
  ]
  node [
    id 142
    label "trema"
  ]
  node [
    id 143
    label "odtworzenie"
  ]
  node [
    id 144
    label "wysyp"
  ]
  node [
    id 145
    label "fullness"
  ]
  node [
    id 146
    label "podostatek"
  ]
  node [
    id 147
    label "mienie"
  ]
  node [
    id 148
    label "ilo&#347;&#263;"
  ]
  node [
    id 149
    label "fortune"
  ]
  node [
    id 150
    label "z&#322;ote_czasy"
  ]
  node [
    id 151
    label "cecha"
  ]
  node [
    id 152
    label "sytuacja"
  ]
  node [
    id 153
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 154
    label "enormousness"
  ]
  node [
    id 155
    label "g&#322;upstwo"
  ]
  node [
    id 156
    label "oszo&#322;omstwo"
  ]
  node [
    id 157
    label "ob&#322;&#281;d"
  ]
  node [
    id 158
    label "pomieszanie_zmys&#322;&#243;w"
  ]
  node [
    id 159
    label "temper"
  ]
  node [
    id 160
    label "zamieszanie"
  ]
  node [
    id 161
    label "folly"
  ]
  node [
    id 162
    label "zabawa"
  ]
  node [
    id 163
    label "poryw"
  ]
  node [
    id 164
    label "choroba_psychiczna"
  ]
  node [
    id 165
    label "gor&#261;cy_okres"
  ]
  node [
    id 166
    label "stupidity"
  ]
  node [
    id 167
    label "obrazowanie"
  ]
  node [
    id 168
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 169
    label "organ"
  ]
  node [
    id 170
    label "tre&#347;&#263;"
  ]
  node [
    id 171
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 172
    label "part"
  ]
  node [
    id 173
    label "element_anatomiczny"
  ]
  node [
    id 174
    label "tekst"
  ]
  node [
    id 175
    label "komunikat"
  ]
  node [
    id 176
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 177
    label "czynno&#347;&#263;"
  ]
  node [
    id 178
    label "&#380;al"
  ]
  node [
    id 179
    label "reakcja"
  ]
  node [
    id 180
    label "przedstawienie"
  ]
  node [
    id 181
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 182
    label "zachowanie"
  ]
  node [
    id 183
    label "wystarczy&#263;"
  ]
  node [
    id 184
    label "trwa&#263;"
  ]
  node [
    id 185
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 186
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 187
    label "przebywa&#263;"
  ]
  node [
    id 188
    label "by&#263;"
  ]
  node [
    id 189
    label "pozostawa&#263;"
  ]
  node [
    id 190
    label "kosztowa&#263;"
  ]
  node [
    id 191
    label "undertaking"
  ]
  node [
    id 192
    label "digest"
  ]
  node [
    id 193
    label "wystawa&#263;"
  ]
  node [
    id 194
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 195
    label "wystarcza&#263;"
  ]
  node [
    id 196
    label "base"
  ]
  node [
    id 197
    label "mieszka&#263;"
  ]
  node [
    id 198
    label "stand"
  ]
  node [
    id 199
    label "sprawowa&#263;"
  ]
  node [
    id 200
    label "czeka&#263;"
  ]
  node [
    id 201
    label "dobywa&#263;_si&#281;"
  ]
  node [
    id 202
    label "istnie&#263;"
  ]
  node [
    id 203
    label "zostawa&#263;"
  ]
  node [
    id 204
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 205
    label "adhere"
  ]
  node [
    id 206
    label "function"
  ]
  node [
    id 207
    label "bind"
  ]
  node [
    id 208
    label "panowa&#263;"
  ]
  node [
    id 209
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 210
    label "zjednywa&#263;"
  ]
  node [
    id 211
    label "tkwi&#263;"
  ]
  node [
    id 212
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 213
    label "pause"
  ]
  node [
    id 214
    label "przestawa&#263;"
  ]
  node [
    id 215
    label "hesitate"
  ]
  node [
    id 216
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 217
    label "mie&#263;_miejsce"
  ]
  node [
    id 218
    label "equal"
  ]
  node [
    id 219
    label "chodzi&#263;"
  ]
  node [
    id 220
    label "si&#281;ga&#263;"
  ]
  node [
    id 221
    label "stan"
  ]
  node [
    id 222
    label "obecno&#347;&#263;"
  ]
  node [
    id 223
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 224
    label "uczestniczy&#263;"
  ]
  node [
    id 225
    label "try"
  ]
  node [
    id 226
    label "savor"
  ]
  node [
    id 227
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 228
    label "cena"
  ]
  node [
    id 229
    label "doznawa&#263;"
  ]
  node [
    id 230
    label "essay"
  ]
  node [
    id 231
    label "zaspokaja&#263;"
  ]
  node [
    id 232
    label "suffice"
  ]
  node [
    id 233
    label "dostawa&#263;"
  ]
  node [
    id 234
    label "stawa&#263;"
  ]
  node [
    id 235
    label "spowodowa&#263;"
  ]
  node [
    id 236
    label "stan&#261;&#263;"
  ]
  node [
    id 237
    label "zaspokoi&#263;"
  ]
  node [
    id 238
    label "dosta&#263;"
  ]
  node [
    id 239
    label "pauzowa&#263;"
  ]
  node [
    id 240
    label "oczekiwa&#263;"
  ]
  node [
    id 241
    label "decydowa&#263;"
  ]
  node [
    id 242
    label "sp&#281;dza&#263;"
  ]
  node [
    id 243
    label "look"
  ]
  node [
    id 244
    label "hold"
  ]
  node [
    id 245
    label "anticipate"
  ]
  node [
    id 246
    label "blend"
  ]
  node [
    id 247
    label "stop"
  ]
  node [
    id 248
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 249
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 250
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 251
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 252
    label "support"
  ]
  node [
    id 253
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 254
    label "prosecute"
  ]
  node [
    id 255
    label "zajmowa&#263;"
  ]
  node [
    id 256
    label "room"
  ]
  node [
    id 257
    label "fall"
  ]
  node [
    id 258
    label "teren"
  ]
  node [
    id 259
    label "pole"
  ]
  node [
    id 260
    label "kawa&#322;ek"
  ]
  node [
    id 261
    label "line"
  ]
  node [
    id 262
    label "coupon"
  ]
  node [
    id 263
    label "fragment"
  ]
  node [
    id 264
    label "pokwitowanie"
  ]
  node [
    id 265
    label "moneta"
  ]
  node [
    id 266
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 267
    label "epizod"
  ]
  node [
    id 268
    label "uprawienie"
  ]
  node [
    id 269
    label "u&#322;o&#380;enie"
  ]
  node [
    id 270
    label "p&#322;osa"
  ]
  node [
    id 271
    label "ziemia"
  ]
  node [
    id 272
    label "t&#322;o"
  ]
  node [
    id 273
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 274
    label "gospodarstwo"
  ]
  node [
    id 275
    label "uprawi&#263;"
  ]
  node [
    id 276
    label "dw&#243;r"
  ]
  node [
    id 277
    label "okazja"
  ]
  node [
    id 278
    label "rozmiar"
  ]
  node [
    id 279
    label "irygowanie"
  ]
  node [
    id 280
    label "compass"
  ]
  node [
    id 281
    label "square"
  ]
  node [
    id 282
    label "zmienna"
  ]
  node [
    id 283
    label "irygowa&#263;"
  ]
  node [
    id 284
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 285
    label "socjologia"
  ]
  node [
    id 286
    label "boisko"
  ]
  node [
    id 287
    label "dziedzina"
  ]
  node [
    id 288
    label "baza_danych"
  ]
  node [
    id 289
    label "region"
  ]
  node [
    id 290
    label "przestrze&#324;"
  ]
  node [
    id 291
    label "zagon"
  ]
  node [
    id 292
    label "obszar"
  ]
  node [
    id 293
    label "sk&#322;ad"
  ]
  node [
    id 294
    label "powierzchnia"
  ]
  node [
    id 295
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 296
    label "plane"
  ]
  node [
    id 297
    label "radlina"
  ]
  node [
    id 298
    label "Rzym_Zachodni"
  ]
  node [
    id 299
    label "whole"
  ]
  node [
    id 300
    label "element"
  ]
  node [
    id 301
    label "Rzym_Wschodni"
  ]
  node [
    id 302
    label "urz&#261;dzenie"
  ]
  node [
    id 303
    label "acquittance"
  ]
  node [
    id 304
    label "za&#347;wiadczenie"
  ]
  node [
    id 305
    label "skwitowanie"
  ]
  node [
    id 306
    label "kwitariusz"
  ]
  node [
    id 307
    label "potwierdzenie"
  ]
  node [
    id 308
    label "kawa&#322;"
  ]
  node [
    id 309
    label "plot"
  ]
  node [
    id 310
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 311
    label "piece"
  ]
  node [
    id 312
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 313
    label "podp&#322;ywanie"
  ]
  node [
    id 314
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 315
    label "wymiar"
  ]
  node [
    id 316
    label "zakres"
  ]
  node [
    id 317
    label "kontekst"
  ]
  node [
    id 318
    label "miejsce_pracy"
  ]
  node [
    id 319
    label "nation"
  ]
  node [
    id 320
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 321
    label "przyroda"
  ]
  node [
    id 322
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 323
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 324
    label "w&#322;adza"
  ]
  node [
    id 325
    label "ta&#347;ma"
  ]
  node [
    id 326
    label "plecionka"
  ]
  node [
    id 327
    label "parciak"
  ]
  node [
    id 328
    label "p&#322;&#243;tno"
  ]
  node [
    id 329
    label "awers"
  ]
  node [
    id 330
    label "legenda"
  ]
  node [
    id 331
    label "liga"
  ]
  node [
    id 332
    label "rewers"
  ]
  node [
    id 333
    label "egzerga"
  ]
  node [
    id 334
    label "pieni&#261;dz"
  ]
  node [
    id 335
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 336
    label "otok"
  ]
  node [
    id 337
    label "balansjerka"
  ]
  node [
    id 338
    label "episode"
  ]
  node [
    id 339
    label "moment"
  ]
  node [
    id 340
    label "w&#261;tek"
  ]
  node [
    id 341
    label "szczeg&#243;&#322;"
  ]
  node [
    id 342
    label "sequence"
  ]
  node [
    id 343
    label "rola"
  ]
  node [
    id 344
    label "motyw"
  ]
  node [
    id 345
    label "teren_zielony"
  ]
  node [
    id 346
    label "teren_przemys&#322;owy"
  ]
  node [
    id 347
    label "sprz&#281;t"
  ]
  node [
    id 348
    label "miejsce"
  ]
  node [
    id 349
    label "tabor"
  ]
  node [
    id 350
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 351
    label "ballpark"
  ]
  node [
    id 352
    label "warunek_lokalowy"
  ]
  node [
    id 353
    label "plac"
  ]
  node [
    id 354
    label "location"
  ]
  node [
    id 355
    label "uwaga"
  ]
  node [
    id 356
    label "status"
  ]
  node [
    id 357
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 358
    label "chwila"
  ]
  node [
    id 359
    label "cia&#322;o"
  ]
  node [
    id 360
    label "praca"
  ]
  node [
    id 361
    label "rz&#261;d"
  ]
  node [
    id 362
    label "p&#243;&#322;noc"
  ]
  node [
    id 363
    label "Kosowo"
  ]
  node [
    id 364
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 365
    label "Zab&#322;ocie"
  ]
  node [
    id 366
    label "zach&#243;d"
  ]
  node [
    id 367
    label "po&#322;udnie"
  ]
  node [
    id 368
    label "Pow&#261;zki"
  ]
  node [
    id 369
    label "Piotrowo"
  ]
  node [
    id 370
    label "Olszanica"
  ]
  node [
    id 371
    label "zbi&#243;r"
  ]
  node [
    id 372
    label "Ruda_Pabianicka"
  ]
  node [
    id 373
    label "holarktyka"
  ]
  node [
    id 374
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 375
    label "Ludwin&#243;w"
  ]
  node [
    id 376
    label "Arktyka"
  ]
  node [
    id 377
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 378
    label "Zabu&#380;e"
  ]
  node [
    id 379
    label "antroposfera"
  ]
  node [
    id 380
    label "Neogea"
  ]
  node [
    id 381
    label "terytorium"
  ]
  node [
    id 382
    label "Syberia_Zachodnia"
  ]
  node [
    id 383
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 384
    label "pas_planetoid"
  ]
  node [
    id 385
    label "Syberia_Wschodnia"
  ]
  node [
    id 386
    label "Antarktyka"
  ]
  node [
    id 387
    label "Rakowice"
  ]
  node [
    id 388
    label "akrecja"
  ]
  node [
    id 389
    label "&#321;&#281;g"
  ]
  node [
    id 390
    label "Kresy_Zachodnie"
  ]
  node [
    id 391
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 392
    label "wsch&#243;d"
  ]
  node [
    id 393
    label "Notogea"
  ]
  node [
    id 394
    label "sprz&#281;cior"
  ]
  node [
    id 395
    label "penis"
  ]
  node [
    id 396
    label "przedmiot"
  ]
  node [
    id 397
    label "kolekcja"
  ]
  node [
    id 398
    label "furniture"
  ]
  node [
    id 399
    label "sprz&#281;cik"
  ]
  node [
    id 400
    label "equipment"
  ]
  node [
    id 401
    label "wojsko"
  ]
  node [
    id 402
    label "Cygan"
  ]
  node [
    id 403
    label "dostawa"
  ]
  node [
    id 404
    label "transport"
  ]
  node [
    id 405
    label "pojazd"
  ]
  node [
    id 406
    label "ob&#243;z"
  ]
  node [
    id 407
    label "grupa"
  ]
  node [
    id 408
    label "testify"
  ]
  node [
    id 409
    label "point"
  ]
  node [
    id 410
    label "przedstawi&#263;"
  ]
  node [
    id 411
    label "poda&#263;"
  ]
  node [
    id 412
    label "poinformowa&#263;"
  ]
  node [
    id 413
    label "udowodni&#263;"
  ]
  node [
    id 414
    label "wyrazi&#263;"
  ]
  node [
    id 415
    label "przeszkoli&#263;"
  ]
  node [
    id 416
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 417
    label "indicate"
  ]
  node [
    id 418
    label "pom&#243;c"
  ]
  node [
    id 419
    label "inform"
  ]
  node [
    id 420
    label "zakomunikowa&#263;"
  ]
  node [
    id 421
    label "uzasadni&#263;"
  ]
  node [
    id 422
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 423
    label "oznaczy&#263;"
  ]
  node [
    id 424
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 425
    label "vent"
  ]
  node [
    id 426
    label "act"
  ]
  node [
    id 427
    label "tenis"
  ]
  node [
    id 428
    label "supply"
  ]
  node [
    id 429
    label "da&#263;"
  ]
  node [
    id 430
    label "ustawi&#263;"
  ]
  node [
    id 431
    label "siatk&#243;wka"
  ]
  node [
    id 432
    label "give"
  ]
  node [
    id 433
    label "zagra&#263;"
  ]
  node [
    id 434
    label "jedzenie"
  ]
  node [
    id 435
    label "introduce"
  ]
  node [
    id 436
    label "nafaszerowa&#263;"
  ]
  node [
    id 437
    label "zaserwowa&#263;"
  ]
  node [
    id 438
    label "ukaza&#263;"
  ]
  node [
    id 439
    label "zapozna&#263;"
  ]
  node [
    id 440
    label "express"
  ]
  node [
    id 441
    label "represent"
  ]
  node [
    id 442
    label "zaproponowa&#263;"
  ]
  node [
    id 443
    label "zademonstrowa&#263;"
  ]
  node [
    id 444
    label "typify"
  ]
  node [
    id 445
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 446
    label "opisa&#263;"
  ]
  node [
    id 447
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 448
    label "sympatyk"
  ]
  node [
    id 449
    label "entuzjasta"
  ]
  node [
    id 450
    label "cognition"
  ]
  node [
    id 451
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 452
    label "intelekt"
  ]
  node [
    id 453
    label "pozwolenie"
  ]
  node [
    id 454
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 455
    label "zaawansowanie"
  ]
  node [
    id 456
    label "wykszta&#322;cenie"
  ]
  node [
    id 457
    label "zdolno&#347;&#263;"
  ]
  node [
    id 458
    label "ekstraspekcja"
  ]
  node [
    id 459
    label "feeling"
  ]
  node [
    id 460
    label "zemdle&#263;"
  ]
  node [
    id 461
    label "psychika"
  ]
  node [
    id 462
    label "Freud"
  ]
  node [
    id 463
    label "psychoanaliza"
  ]
  node [
    id 464
    label "conscience"
  ]
  node [
    id 465
    label "integer"
  ]
  node [
    id 466
    label "liczba"
  ]
  node [
    id 467
    label "zlewanie_si&#281;"
  ]
  node [
    id 468
    label "uk&#322;ad"
  ]
  node [
    id 469
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 470
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 471
    label "pe&#322;ny"
  ]
  node [
    id 472
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 473
    label "rozwini&#281;cie"
  ]
  node [
    id 474
    label "zapoznanie"
  ]
  node [
    id 475
    label "wys&#322;anie"
  ]
  node [
    id 476
    label "udoskonalenie"
  ]
  node [
    id 477
    label "pomo&#380;enie"
  ]
  node [
    id 478
    label "urszulanki"
  ]
  node [
    id 479
    label "training"
  ]
  node [
    id 480
    label "niepokalanki"
  ]
  node [
    id 481
    label "o&#347;wiecenie"
  ]
  node [
    id 482
    label "kwalifikacje"
  ]
  node [
    id 483
    label "sophistication"
  ]
  node [
    id 484
    label "skolaryzacja"
  ]
  node [
    id 485
    label "form"
  ]
  node [
    id 486
    label "umys&#322;"
  ]
  node [
    id 487
    label "noosfera"
  ]
  node [
    id 488
    label "stopie&#324;"
  ]
  node [
    id 489
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 490
    label "decyzja"
  ]
  node [
    id 491
    label "zwalnianie_si&#281;"
  ]
  node [
    id 492
    label "authorization"
  ]
  node [
    id 493
    label "koncesjonowanie"
  ]
  node [
    id 494
    label "zwolnienie_si&#281;"
  ]
  node [
    id 495
    label "pozwole&#324;stwo"
  ]
  node [
    id 496
    label "bycie_w_stanie"
  ]
  node [
    id 497
    label "odwieszenie"
  ]
  node [
    id 498
    label "odpowied&#378;"
  ]
  node [
    id 499
    label "pofolgowanie"
  ]
  node [
    id 500
    label "license"
  ]
  node [
    id 501
    label "franchise"
  ]
  node [
    id 502
    label "umo&#380;liwienie"
  ]
  node [
    id 503
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 504
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 505
    label "dokument"
  ]
  node [
    id 506
    label "uznanie"
  ]
  node [
    id 507
    label "zrobienie"
  ]
  node [
    id 508
    label "Stary_&#346;wiat"
  ]
  node [
    id 509
    label "asymilowanie_si&#281;"
  ]
  node [
    id 510
    label "Wsch&#243;d"
  ]
  node [
    id 511
    label "class"
  ]
  node [
    id 512
    label "geosfera"
  ]
  node [
    id 513
    label "obiekt_naturalny"
  ]
  node [
    id 514
    label "przejmowanie"
  ]
  node [
    id 515
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 516
    label "rzecz"
  ]
  node [
    id 517
    label "makrokosmos"
  ]
  node [
    id 518
    label "huczek"
  ]
  node [
    id 519
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 520
    label "environment"
  ]
  node [
    id 521
    label "morze"
  ]
  node [
    id 522
    label "rze&#378;ba"
  ]
  node [
    id 523
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 524
    label "przejmowa&#263;"
  ]
  node [
    id 525
    label "hydrosfera"
  ]
  node [
    id 526
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 527
    label "ciemna_materia"
  ]
  node [
    id 528
    label "ekosystem"
  ]
  node [
    id 529
    label "biota"
  ]
  node [
    id 530
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 531
    label "ekosfera"
  ]
  node [
    id 532
    label "geotermia"
  ]
  node [
    id 533
    label "planeta"
  ]
  node [
    id 534
    label "ozonosfera"
  ]
  node [
    id 535
    label "wszechstworzenie"
  ]
  node [
    id 536
    label "woda"
  ]
  node [
    id 537
    label "kuchnia"
  ]
  node [
    id 538
    label "biosfera"
  ]
  node [
    id 539
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 540
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 541
    label "populace"
  ]
  node [
    id 542
    label "magnetosfera"
  ]
  node [
    id 543
    label "Nowy_&#346;wiat"
  ]
  node [
    id 544
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 545
    label "universe"
  ]
  node [
    id 546
    label "biegun"
  ]
  node [
    id 547
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 548
    label "litosfera"
  ]
  node [
    id 549
    label "mikrokosmos"
  ]
  node [
    id 550
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 551
    label "stw&#243;r"
  ]
  node [
    id 552
    label "p&#243;&#322;kula"
  ]
  node [
    id 553
    label "przej&#281;cie"
  ]
  node [
    id 554
    label "barysfera"
  ]
  node [
    id 555
    label "czarna_dziura"
  ]
  node [
    id 556
    label "atmosfera"
  ]
  node [
    id 557
    label "przej&#261;&#263;"
  ]
  node [
    id 558
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 559
    label "Ziemia"
  ]
  node [
    id 560
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 561
    label "geoida"
  ]
  node [
    id 562
    label "zagranica"
  ]
  node [
    id 563
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 564
    label "fauna"
  ]
  node [
    id 565
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 566
    label "odm&#322;adzanie"
  ]
  node [
    id 567
    label "jednostka_systematyczna"
  ]
  node [
    id 568
    label "asymilowanie"
  ]
  node [
    id 569
    label "gromada"
  ]
  node [
    id 570
    label "asymilowa&#263;"
  ]
  node [
    id 571
    label "egzemplarz"
  ]
  node [
    id 572
    label "Entuzjastki"
  ]
  node [
    id 573
    label "kompozycja"
  ]
  node [
    id 574
    label "Terranie"
  ]
  node [
    id 575
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 576
    label "category"
  ]
  node [
    id 577
    label "pakiet_klimatyczny"
  ]
  node [
    id 578
    label "oddzia&#322;"
  ]
  node [
    id 579
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 580
    label "cz&#261;steczka"
  ]
  node [
    id 581
    label "stage_set"
  ]
  node [
    id 582
    label "type"
  ]
  node [
    id 583
    label "specgrupa"
  ]
  node [
    id 584
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 585
    label "&#346;wietliki"
  ]
  node [
    id 586
    label "odm&#322;odzenie"
  ]
  node [
    id 587
    label "Eurogrupa"
  ]
  node [
    id 588
    label "odm&#322;adza&#263;"
  ]
  node [
    id 589
    label "formacja_geologiczna"
  ]
  node [
    id 590
    label "harcerze_starsi"
  ]
  node [
    id 591
    label "rozdzielanie"
  ]
  node [
    id 592
    label "bezbrze&#380;e"
  ]
  node [
    id 593
    label "punkt"
  ]
  node [
    id 594
    label "czasoprzestrze&#324;"
  ]
  node [
    id 595
    label "niezmierzony"
  ]
  node [
    id 596
    label "przedzielenie"
  ]
  node [
    id 597
    label "nielito&#347;ciwy"
  ]
  node [
    id 598
    label "rozdziela&#263;"
  ]
  node [
    id 599
    label "oktant"
  ]
  node [
    id 600
    label "przedzieli&#263;"
  ]
  node [
    id 601
    label "przestw&#243;r"
  ]
  node [
    id 602
    label "&#347;rodowisko"
  ]
  node [
    id 603
    label "rura"
  ]
  node [
    id 604
    label "grzebiuszka"
  ]
  node [
    id 605
    label "atom"
  ]
  node [
    id 606
    label "odbicie"
  ]
  node [
    id 607
    label "kosmos"
  ]
  node [
    id 608
    label "miniatura"
  ]
  node [
    id 609
    label "smok_wawelski"
  ]
  node [
    id 610
    label "niecz&#322;owiek"
  ]
  node [
    id 611
    label "monster"
  ]
  node [
    id 612
    label "potw&#243;r"
  ]
  node [
    id 613
    label "istota_fantastyczna"
  ]
  node [
    id 614
    label "kultura"
  ]
  node [
    id 615
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 616
    label "ciep&#322;o"
  ]
  node [
    id 617
    label "energia_termiczna"
  ]
  node [
    id 618
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 619
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 620
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 621
    label "aspekt"
  ]
  node [
    id 622
    label "troposfera"
  ]
  node [
    id 623
    label "klimat"
  ]
  node [
    id 624
    label "metasfera"
  ]
  node [
    id 625
    label "atmosferyki"
  ]
  node [
    id 626
    label "homosfera"
  ]
  node [
    id 627
    label "powietrznia"
  ]
  node [
    id 628
    label "jonosfera"
  ]
  node [
    id 629
    label "termosfera"
  ]
  node [
    id 630
    label "egzosfera"
  ]
  node [
    id 631
    label "heterosfera"
  ]
  node [
    id 632
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 633
    label "tropopauza"
  ]
  node [
    id 634
    label "kwas"
  ]
  node [
    id 635
    label "powietrze"
  ]
  node [
    id 636
    label "stratosfera"
  ]
  node [
    id 637
    label "pow&#322;oka"
  ]
  node [
    id 638
    label "mezosfera"
  ]
  node [
    id 639
    label "mezopauza"
  ]
  node [
    id 640
    label "atmosphere"
  ]
  node [
    id 641
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 642
    label "sferoida"
  ]
  node [
    id 643
    label "object"
  ]
  node [
    id 644
    label "temat"
  ]
  node [
    id 645
    label "wpadni&#281;cie"
  ]
  node [
    id 646
    label "istota"
  ]
  node [
    id 647
    label "obiekt"
  ]
  node [
    id 648
    label "wpa&#347;&#263;"
  ]
  node [
    id 649
    label "wpadanie"
  ]
  node [
    id 650
    label "wpada&#263;"
  ]
  node [
    id 651
    label "treat"
  ]
  node [
    id 652
    label "czerpa&#263;"
  ]
  node [
    id 653
    label "bra&#263;"
  ]
  node [
    id 654
    label "go"
  ]
  node [
    id 655
    label "handle"
  ]
  node [
    id 656
    label "wzbudza&#263;"
  ]
  node [
    id 657
    label "ogarnia&#263;"
  ]
  node [
    id 658
    label "bang"
  ]
  node [
    id 659
    label "wzi&#261;&#263;"
  ]
  node [
    id 660
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 661
    label "stimulate"
  ]
  node [
    id 662
    label "ogarn&#261;&#263;"
  ]
  node [
    id 663
    label "wzbudzi&#263;"
  ]
  node [
    id 664
    label "thrill"
  ]
  node [
    id 665
    label "czerpanie"
  ]
  node [
    id 666
    label "acquisition"
  ]
  node [
    id 667
    label "branie"
  ]
  node [
    id 668
    label "caparison"
  ]
  node [
    id 669
    label "movement"
  ]
  node [
    id 670
    label "wzbudzanie"
  ]
  node [
    id 671
    label "ogarnianie"
  ]
  node [
    id 672
    label "wra&#380;enie"
  ]
  node [
    id 673
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 674
    label "interception"
  ]
  node [
    id 675
    label "wzbudzenie"
  ]
  node [
    id 676
    label "emotion"
  ]
  node [
    id 677
    label "zaczerpni&#281;cie"
  ]
  node [
    id 678
    label "wzi&#281;cie"
  ]
  node [
    id 679
    label "zboczenie"
  ]
  node [
    id 680
    label "om&#243;wienie"
  ]
  node [
    id 681
    label "sponiewieranie"
  ]
  node [
    id 682
    label "discipline"
  ]
  node [
    id 683
    label "omawia&#263;"
  ]
  node [
    id 684
    label "kr&#261;&#380;enie"
  ]
  node [
    id 685
    label "robienie"
  ]
  node [
    id 686
    label "sponiewiera&#263;"
  ]
  node [
    id 687
    label "entity"
  ]
  node [
    id 688
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 689
    label "tematyka"
  ]
  node [
    id 690
    label "zbaczanie"
  ]
  node [
    id 691
    label "program_nauczania"
  ]
  node [
    id 692
    label "om&#243;wi&#263;"
  ]
  node [
    id 693
    label "omawianie"
  ]
  node [
    id 694
    label "thing"
  ]
  node [
    id 695
    label "zbacza&#263;"
  ]
  node [
    id 696
    label "zboczy&#263;"
  ]
  node [
    id 697
    label "sztuka"
  ]
  node [
    id 698
    label "Boreasz"
  ]
  node [
    id 699
    label "noc"
  ]
  node [
    id 700
    label "p&#243;&#322;nocek"
  ]
  node [
    id 701
    label "strona_&#347;wiata"
  ]
  node [
    id 702
    label "godzina"
  ]
  node [
    id 703
    label "granica_pa&#324;stwa"
  ]
  node [
    id 704
    label "warstwa"
  ]
  node [
    id 705
    label "kriosfera"
  ]
  node [
    id 706
    label "lej_polarny"
  ]
  node [
    id 707
    label "sfera"
  ]
  node [
    id 708
    label "brzeg"
  ]
  node [
    id 709
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 710
    label "p&#322;oza"
  ]
  node [
    id 711
    label "zawiasy"
  ]
  node [
    id 712
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 713
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 714
    label "reda"
  ]
  node [
    id 715
    label "zbiornik_wodny"
  ]
  node [
    id 716
    label "przymorze"
  ]
  node [
    id 717
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 718
    label "bezmiar"
  ]
  node [
    id 719
    label "pe&#322;ne_morze"
  ]
  node [
    id 720
    label "latarnia_morska"
  ]
  node [
    id 721
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 722
    label "nereida"
  ]
  node [
    id 723
    label "okeanida"
  ]
  node [
    id 724
    label "marina"
  ]
  node [
    id 725
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 726
    label "Morze_Czerwone"
  ]
  node [
    id 727
    label "talasoterapia"
  ]
  node [
    id 728
    label "Morze_Bia&#322;e"
  ]
  node [
    id 729
    label "paliszcze"
  ]
  node [
    id 730
    label "Neptun"
  ]
  node [
    id 731
    label "Morze_Czarne"
  ]
  node [
    id 732
    label "laguna"
  ]
  node [
    id 733
    label "Morze_Egejskie"
  ]
  node [
    id 734
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 735
    label "Morze_Adriatyckie"
  ]
  node [
    id 736
    label "rze&#378;biarstwo"
  ]
  node [
    id 737
    label "planacja"
  ]
  node [
    id 738
    label "relief"
  ]
  node [
    id 739
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 740
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 741
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 742
    label "bozzetto"
  ]
  node [
    id 743
    label "plastyka"
  ]
  node [
    id 744
    label "j&#261;dro"
  ]
  node [
    id 745
    label "&#347;rodek"
  ]
  node [
    id 746
    label "dzie&#324;"
  ]
  node [
    id 747
    label "dwunasta"
  ]
  node [
    id 748
    label "pora"
  ]
  node [
    id 749
    label "ozon"
  ]
  node [
    id 750
    label "gleba"
  ]
  node [
    id 751
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 752
    label "sialma"
  ]
  node [
    id 753
    label "skorupa_ziemska"
  ]
  node [
    id 754
    label "warstwa_perydotytowa"
  ]
  node [
    id 755
    label "warstwa_granitowa"
  ]
  node [
    id 756
    label "kula"
  ]
  node [
    id 757
    label "kresom&#243;zgowie"
  ]
  node [
    id 758
    label "przyra"
  ]
  node [
    id 759
    label "biom"
  ]
  node [
    id 760
    label "awifauna"
  ]
  node [
    id 761
    label "ichtiofauna"
  ]
  node [
    id 762
    label "geosystem"
  ]
  node [
    id 763
    label "dotleni&#263;"
  ]
  node [
    id 764
    label "spi&#281;trza&#263;"
  ]
  node [
    id 765
    label "spi&#281;trzenie"
  ]
  node [
    id 766
    label "utylizator"
  ]
  node [
    id 767
    label "p&#322;ycizna"
  ]
  node [
    id 768
    label "nabranie"
  ]
  node [
    id 769
    label "Waruna"
  ]
  node [
    id 770
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 771
    label "przybieranie"
  ]
  node [
    id 772
    label "uci&#261;g"
  ]
  node [
    id 773
    label "bombast"
  ]
  node [
    id 774
    label "fala"
  ]
  node [
    id 775
    label "kryptodepresja"
  ]
  node [
    id 776
    label "water"
  ]
  node [
    id 777
    label "wysi&#281;k"
  ]
  node [
    id 778
    label "pustka"
  ]
  node [
    id 779
    label "ciecz"
  ]
  node [
    id 780
    label "przybrze&#380;e"
  ]
  node [
    id 781
    label "nap&#243;j"
  ]
  node [
    id 782
    label "spi&#281;trzanie"
  ]
  node [
    id 783
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 784
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 785
    label "bicie"
  ]
  node [
    id 786
    label "klarownik"
  ]
  node [
    id 787
    label "chlastanie"
  ]
  node [
    id 788
    label "woda_s&#322;odka"
  ]
  node [
    id 789
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 790
    label "nabra&#263;"
  ]
  node [
    id 791
    label "chlasta&#263;"
  ]
  node [
    id 792
    label "uj&#281;cie_wody"
  ]
  node [
    id 793
    label "zrzut"
  ]
  node [
    id 794
    label "wypowied&#378;"
  ]
  node [
    id 795
    label "wodnik"
  ]
  node [
    id 796
    label "l&#243;d"
  ]
  node [
    id 797
    label "wybrze&#380;e"
  ]
  node [
    id 798
    label "deklamacja"
  ]
  node [
    id 799
    label "tlenek"
  ]
  node [
    id 800
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 801
    label "biotop"
  ]
  node [
    id 802
    label "biocenoza"
  ]
  node [
    id 803
    label "szata_ro&#347;linna"
  ]
  node [
    id 804
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 805
    label "formacja_ro&#347;linna"
  ]
  node [
    id 806
    label "zielono&#347;&#263;"
  ]
  node [
    id 807
    label "pi&#281;tro"
  ]
  node [
    id 808
    label "plant"
  ]
  node [
    id 809
    label "ro&#347;lina"
  ]
  node [
    id 810
    label "iglak"
  ]
  node [
    id 811
    label "cyprysowate"
  ]
  node [
    id 812
    label "zaj&#281;cie"
  ]
  node [
    id 813
    label "instytucja"
  ]
  node [
    id 814
    label "tajniki"
  ]
  node [
    id 815
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 816
    label "zaplecze"
  ]
  node [
    id 817
    label "pomieszczenie"
  ]
  node [
    id 818
    label "zlewozmywak"
  ]
  node [
    id 819
    label "gotowa&#263;"
  ]
  node [
    id 820
    label "strefa"
  ]
  node [
    id 821
    label "Jowisz"
  ]
  node [
    id 822
    label "syzygia"
  ]
  node [
    id 823
    label "Saturn"
  ]
  node [
    id 824
    label "Uran"
  ]
  node [
    id 825
    label "message"
  ]
  node [
    id 826
    label "dar"
  ]
  node [
    id 827
    label "real"
  ]
  node [
    id 828
    label "Ukraina"
  ]
  node [
    id 829
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 830
    label "blok_wschodni"
  ]
  node [
    id 831
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 832
    label "Europa_Wschodnia"
  ]
  node [
    id 833
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 834
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 835
    label "dramat"
  ]
  node [
    id 836
    label "pisarstwo"
  ]
  node [
    id 837
    label "liryka"
  ]
  node [
    id 838
    label "amorfizm"
  ]
  node [
    id 839
    label "bibliografia"
  ]
  node [
    id 840
    label "epika"
  ]
  node [
    id 841
    label "translator"
  ]
  node [
    id 842
    label "pi&#347;miennictwo"
  ]
  node [
    id 843
    label "zoologia_fantastyczna"
  ]
  node [
    id 844
    label "zapis"
  ]
  node [
    id 845
    label "&#347;wiadectwo"
  ]
  node [
    id 846
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 847
    label "wytw&#243;r"
  ]
  node [
    id 848
    label "parafa"
  ]
  node [
    id 849
    label "plik"
  ]
  node [
    id 850
    label "raport&#243;wka"
  ]
  node [
    id 851
    label "record"
  ]
  node [
    id 852
    label "registratura"
  ]
  node [
    id 853
    label "dokumentacja"
  ]
  node [
    id 854
    label "fascyku&#322;"
  ]
  node [
    id 855
    label "artyku&#322;"
  ]
  node [
    id 856
    label "writing"
  ]
  node [
    id 857
    label "sygnatariusz"
  ]
  node [
    id 858
    label "rocznikarstwo"
  ]
  node [
    id 859
    label "monografistyka"
  ]
  node [
    id 860
    label "czasopi&#347;miennictwo"
  ]
  node [
    id 861
    label "pr&#243;bowanie"
  ]
  node [
    id 862
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 863
    label "realizacja"
  ]
  node [
    id 864
    label "scena"
  ]
  node [
    id 865
    label "didaskalia"
  ]
  node [
    id 866
    label "czyn"
  ]
  node [
    id 867
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 868
    label "head"
  ]
  node [
    id 869
    label "scenariusz"
  ]
  node [
    id 870
    label "jednostka"
  ]
  node [
    id 871
    label "kultura_duchowa"
  ]
  node [
    id 872
    label "fortel"
  ]
  node [
    id 873
    label "theatrical_performance"
  ]
  node [
    id 874
    label "ambala&#380;"
  ]
  node [
    id 875
    label "sprawno&#347;&#263;"
  ]
  node [
    id 876
    label "kobieta"
  ]
  node [
    id 877
    label "Faust"
  ]
  node [
    id 878
    label "scenografia"
  ]
  node [
    id 879
    label "ods&#322;ona"
  ]
  node [
    id 880
    label "turn"
  ]
  node [
    id 881
    label "Apollo"
  ]
  node [
    id 882
    label "przedstawianie"
  ]
  node [
    id 883
    label "przedstawia&#263;"
  ]
  node [
    id 884
    label "towar"
  ]
  node [
    id 885
    label "spis"
  ]
  node [
    id 886
    label "bibliologia"
  ]
  node [
    id 887
    label "rodzaj_literacki"
  ]
  node [
    id 888
    label "drama"
  ]
  node [
    id 889
    label "cios"
  ]
  node [
    id 890
    label "film"
  ]
  node [
    id 891
    label "epos"
  ]
  node [
    id 892
    label "fantastyka"
  ]
  node [
    id 893
    label "romans"
  ]
  node [
    id 894
    label "nowelistyka"
  ]
  node [
    id 895
    label "bajka"
  ]
  node [
    id 896
    label "przypowie&#347;&#263;"
  ]
  node [
    id 897
    label "utw&#243;r_epicki"
  ]
  node [
    id 898
    label "sielanka"
  ]
  node [
    id 899
    label "wra&#380;liwo&#347;&#263;"
  ]
  node [
    id 900
    label "nastrojowo&#347;&#263;"
  ]
  node [
    id 901
    label "uczuciowo&#347;&#263;"
  ]
  node [
    id 902
    label "waka"
  ]
  node [
    id 903
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 904
    label "aplikacja"
  ]
  node [
    id 905
    label "t&#322;umacz"
  ]
  node [
    id 906
    label "cia&#322;o_sta&#322;e"
  ]
  node [
    id 907
    label "organizacja"
  ]
  node [
    id 908
    label "p&#243;&#322;rocze"
  ]
  node [
    id 909
    label "martwy_sezon"
  ]
  node [
    id 910
    label "kalendarz"
  ]
  node [
    id 911
    label "cykl_astronomiczny"
  ]
  node [
    id 912
    label "lata"
  ]
  node [
    id 913
    label "pora_roku"
  ]
  node [
    id 914
    label "stulecie"
  ]
  node [
    id 915
    label "kurs"
  ]
  node [
    id 916
    label "czas"
  ]
  node [
    id 917
    label "jubileusz"
  ]
  node [
    id 918
    label "kwarta&#322;"
  ]
  node [
    id 919
    label "miesi&#261;c"
  ]
  node [
    id 920
    label "summer"
  ]
  node [
    id 921
    label "poprzedzanie"
  ]
  node [
    id 922
    label "laba"
  ]
  node [
    id 923
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 924
    label "chronometria"
  ]
  node [
    id 925
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 926
    label "rachuba_czasu"
  ]
  node [
    id 927
    label "przep&#322;ywanie"
  ]
  node [
    id 928
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 929
    label "czasokres"
  ]
  node [
    id 930
    label "odczyt"
  ]
  node [
    id 931
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 932
    label "dzieje"
  ]
  node [
    id 933
    label "kategoria_gramatyczna"
  ]
  node [
    id 934
    label "poprzedzenie"
  ]
  node [
    id 935
    label "trawienie"
  ]
  node [
    id 936
    label "pochodzi&#263;"
  ]
  node [
    id 937
    label "period"
  ]
  node [
    id 938
    label "okres_czasu"
  ]
  node [
    id 939
    label "poprzedza&#263;"
  ]
  node [
    id 940
    label "schy&#322;ek"
  ]
  node [
    id 941
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 942
    label "odwlekanie_si&#281;"
  ]
  node [
    id 943
    label "zegar"
  ]
  node [
    id 944
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 945
    label "czwarty_wymiar"
  ]
  node [
    id 946
    label "pochodzenie"
  ]
  node [
    id 947
    label "koniugacja"
  ]
  node [
    id 948
    label "Zeitgeist"
  ]
  node [
    id 949
    label "trawi&#263;"
  ]
  node [
    id 950
    label "pogoda"
  ]
  node [
    id 951
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 952
    label "poprzedzi&#263;"
  ]
  node [
    id 953
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 954
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 955
    label "time_period"
  ]
  node [
    id 956
    label "term"
  ]
  node [
    id 957
    label "rok_akademicki"
  ]
  node [
    id 958
    label "rok_szkolny"
  ]
  node [
    id 959
    label "semester"
  ]
  node [
    id 960
    label "anniwersarz"
  ]
  node [
    id 961
    label "rocznica"
  ]
  node [
    id 962
    label "tydzie&#324;"
  ]
  node [
    id 963
    label "miech"
  ]
  node [
    id 964
    label "kalendy"
  ]
  node [
    id 965
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 966
    label "long_time"
  ]
  node [
    id 967
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 968
    label "almanac"
  ]
  node [
    id 969
    label "rozk&#322;ad"
  ]
  node [
    id 970
    label "wydawnictwo"
  ]
  node [
    id 971
    label "Juliusz_Cezar"
  ]
  node [
    id 972
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 973
    label "zwy&#380;kowanie"
  ]
  node [
    id 974
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 975
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 976
    label "zaj&#281;cia"
  ]
  node [
    id 977
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 978
    label "trasa"
  ]
  node [
    id 979
    label "przeorientowywanie"
  ]
  node [
    id 980
    label "przejazd"
  ]
  node [
    id 981
    label "kierunek"
  ]
  node [
    id 982
    label "przeorientowywa&#263;"
  ]
  node [
    id 983
    label "nauka"
  ]
  node [
    id 984
    label "przeorientowanie"
  ]
  node [
    id 985
    label "klasa"
  ]
  node [
    id 986
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 987
    label "przeorientowa&#263;"
  ]
  node [
    id 988
    label "manner"
  ]
  node [
    id 989
    label "course"
  ]
  node [
    id 990
    label "passage"
  ]
  node [
    id 991
    label "zni&#380;kowanie"
  ]
  node [
    id 992
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 993
    label "seria"
  ]
  node [
    id 994
    label "stawka"
  ]
  node [
    id 995
    label "way"
  ]
  node [
    id 996
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 997
    label "spos&#243;b"
  ]
  node [
    id 998
    label "deprecjacja"
  ]
  node [
    id 999
    label "cedu&#322;a"
  ]
  node [
    id 1000
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1001
    label "drive"
  ]
  node [
    id 1002
    label "bearing"
  ]
  node [
    id 1003
    label "Lira"
  ]
  node [
    id 1004
    label "Wratislavia"
  ]
  node [
    id 1005
    label "Cantans"
  ]
  node [
    id 1006
    label "South"
  ]
  node [
    id 1007
    label "w&#322;adca"
  ]
  node [
    id 1008
    label "pier&#347;cie&#324;"
  ]
  node [
    id 1009
    label "zakocha&#263;"
  ]
  node [
    id 1010
    label "Flaubert"
  ]
  node [
    id 1011
    label "Agata"
  ]
  node [
    id 1012
    label "Zubel"
  ]
  node [
    id 1013
    label "Sun"
  ]
  node [
    id 1014
    label "Wo"
  ]
  node [
    id 1015
    label "Sonic"
  ]
  node [
    id 1016
    label "Youth"
  ]
  node [
    id 1017
    label "Walter"
  ]
  node [
    id 1018
    label "Klemmer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 1006
  ]
  edge [
    source 11
    target 1013
  ]
  edge [
    source 11
    target 1014
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 450
  ]
  edge [
    source 15
    target 451
  ]
  edge [
    source 15
    target 452
  ]
  edge [
    source 15
    target 453
  ]
  edge [
    source 15
    target 454
  ]
  edge [
    source 15
    target 455
  ]
  edge [
    source 15
    target 456
  ]
  edge [
    source 15
    target 323
  ]
  edge [
    source 15
    target 457
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 458
  ]
  edge [
    source 15
    target 459
  ]
  edge [
    source 15
    target 460
  ]
  edge [
    source 15
    target 461
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 462
  ]
  edge [
    source 15
    target 463
  ]
  edge [
    source 15
    target 464
  ]
  edge [
    source 15
    target 465
  ]
  edge [
    source 15
    target 466
  ]
  edge [
    source 15
    target 467
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 468
  ]
  edge [
    source 15
    target 469
  ]
  edge [
    source 15
    target 470
  ]
  edge [
    source 15
    target 471
  ]
  edge [
    source 15
    target 472
  ]
  edge [
    source 15
    target 473
  ]
  edge [
    source 15
    target 474
  ]
  edge [
    source 15
    target 475
  ]
  edge [
    source 15
    target 476
  ]
  edge [
    source 15
    target 477
  ]
  edge [
    source 15
    target 478
  ]
  edge [
    source 15
    target 479
  ]
  edge [
    source 15
    target 480
  ]
  edge [
    source 15
    target 481
  ]
  edge [
    source 15
    target 482
  ]
  edge [
    source 15
    target 483
  ]
  edge [
    source 15
    target 484
  ]
  edge [
    source 15
    target 485
  ]
  edge [
    source 15
    target 486
  ]
  edge [
    source 15
    target 487
  ]
  edge [
    source 15
    target 488
  ]
  edge [
    source 15
    target 489
  ]
  edge [
    source 15
    target 490
  ]
  edge [
    source 15
    target 491
  ]
  edge [
    source 15
    target 492
  ]
  edge [
    source 15
    target 493
  ]
  edge [
    source 15
    target 494
  ]
  edge [
    source 15
    target 495
  ]
  edge [
    source 15
    target 496
  ]
  edge [
    source 15
    target 497
  ]
  edge [
    source 15
    target 498
  ]
  edge [
    source 15
    target 499
  ]
  edge [
    source 15
    target 500
  ]
  edge [
    source 15
    target 501
  ]
  edge [
    source 15
    target 502
  ]
  edge [
    source 15
    target 503
  ]
  edge [
    source 15
    target 504
  ]
  edge [
    source 15
    target 505
  ]
  edge [
    source 15
    target 506
  ]
  edge [
    source 15
    target 507
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 509
  ]
  edge [
    source 16
    target 362
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 510
  ]
  edge [
    source 16
    target 511
  ]
  edge [
    source 16
    target 512
  ]
  edge [
    source 16
    target 513
  ]
  edge [
    source 16
    target 514
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 321
  ]
  edge [
    source 16
    target 515
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 516
  ]
  edge [
    source 16
    target 517
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 520
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 533
  ]
  edge [
    source 16
    target 534
  ]
  edge [
    source 16
    target 535
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 536
  ]
  edge [
    source 16
    target 537
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 539
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 542
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 544
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 546
  ]
  edge [
    source 16
    target 547
  ]
  edge [
    source 16
    target 548
  ]
  edge [
    source 16
    target 258
  ]
  edge [
    source 16
    target 549
  ]
  edge [
    source 16
    target 550
  ]
  edge [
    source 16
    target 290
  ]
  edge [
    source 16
    target 551
  ]
  edge [
    source 16
    target 552
  ]
  edge [
    source 16
    target 553
  ]
  edge [
    source 16
    target 554
  ]
  edge [
    source 16
    target 292
  ]
  edge [
    source 16
    target 555
  ]
  edge [
    source 16
    target 556
  ]
  edge [
    source 16
    target 557
  ]
  edge [
    source 16
    target 558
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 560
  ]
  edge [
    source 16
    target 561
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 563
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 565
  ]
  edge [
    source 16
    target 566
  ]
  edge [
    source 16
    target 331
  ]
  edge [
    source 16
    target 567
  ]
  edge [
    source 16
    target 568
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 348
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 316
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 315
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 465
  ]
  edge [
    source 16
    target 466
  ]
  edge [
    source 16
    target 467
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 468
  ]
  edge [
    source 16
    target 469
  ]
  edge [
    source 16
    target 470
  ]
  edge [
    source 16
    target 471
  ]
  edge [
    source 16
    target 472
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 609
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 611
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 613
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 615
  ]
  edge [
    source 16
    target 616
  ]
  edge [
    source 16
    target 617
  ]
  edge [
    source 16
    target 618
  ]
  edge [
    source 16
    target 619
  ]
  edge [
    source 16
    target 620
  ]
  edge [
    source 16
    target 621
  ]
  edge [
    source 16
    target 622
  ]
  edge [
    source 16
    target 623
  ]
  edge [
    source 16
    target 624
  ]
  edge [
    source 16
    target 625
  ]
  edge [
    source 16
    target 626
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 627
  ]
  edge [
    source 16
    target 628
  ]
  edge [
    source 16
    target 629
  ]
  edge [
    source 16
    target 630
  ]
  edge [
    source 16
    target 631
  ]
  edge [
    source 16
    target 632
  ]
  edge [
    source 16
    target 633
  ]
  edge [
    source 16
    target 634
  ]
  edge [
    source 16
    target 635
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 639
  ]
  edge [
    source 16
    target 640
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 642
  ]
  edge [
    source 16
    target 643
  ]
  edge [
    source 16
    target 644
  ]
  edge [
    source 16
    target 645
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 646
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 300
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 340
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 266
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 317
  ]
  edge [
    source 16
    target 318
  ]
  edge [
    source 16
    target 319
  ]
  edge [
    source 16
    target 320
  ]
  edge [
    source 16
    target 322
  ]
  edge [
    source 16
    target 324
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 343
  ]
  edge [
    source 18
    target 396
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 410
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 908
  ]
  edge [
    source 20
    target 909
  ]
  edge [
    source 20
    target 910
  ]
  edge [
    source 20
    target 911
  ]
  edge [
    source 20
    target 912
  ]
  edge [
    source 20
    target 913
  ]
  edge [
    source 20
    target 914
  ]
  edge [
    source 20
    target 915
  ]
  edge [
    source 20
    target 916
  ]
  edge [
    source 20
    target 917
  ]
  edge [
    source 20
    target 407
  ]
  edge [
    source 20
    target 918
  ]
  edge [
    source 20
    target 919
  ]
  edge [
    source 20
    target 920
  ]
  edge [
    source 20
    target 566
  ]
  edge [
    source 20
    target 331
  ]
  edge [
    source 20
    target 567
  ]
  edge [
    source 20
    target 568
  ]
  edge [
    source 20
    target 569
  ]
  edge [
    source 20
    target 323
  ]
  edge [
    source 20
    target 570
  ]
  edge [
    source 20
    target 571
  ]
  edge [
    source 20
    target 572
  ]
  edge [
    source 20
    target 371
  ]
  edge [
    source 20
    target 573
  ]
  edge [
    source 20
    target 574
  ]
  edge [
    source 20
    target 575
  ]
  edge [
    source 20
    target 576
  ]
  edge [
    source 20
    target 577
  ]
  edge [
    source 20
    target 578
  ]
  edge [
    source 20
    target 579
  ]
  edge [
    source 20
    target 580
  ]
  edge [
    source 20
    target 581
  ]
  edge [
    source 20
    target 582
  ]
  edge [
    source 20
    target 583
  ]
  edge [
    source 20
    target 584
  ]
  edge [
    source 20
    target 585
  ]
  edge [
    source 20
    target 586
  ]
  edge [
    source 20
    target 587
  ]
  edge [
    source 20
    target 588
  ]
  edge [
    source 20
    target 589
  ]
  edge [
    source 20
    target 590
  ]
  edge [
    source 20
    target 921
  ]
  edge [
    source 20
    target 594
  ]
  edge [
    source 20
    target 922
  ]
  edge [
    source 20
    target 923
  ]
  edge [
    source 20
    target 924
  ]
  edge [
    source 20
    target 925
  ]
  edge [
    source 20
    target 926
  ]
  edge [
    source 20
    target 927
  ]
  edge [
    source 20
    target 928
  ]
  edge [
    source 20
    target 929
  ]
  edge [
    source 20
    target 930
  ]
  edge [
    source 20
    target 358
  ]
  edge [
    source 20
    target 931
  ]
  edge [
    source 20
    target 932
  ]
  edge [
    source 20
    target 933
  ]
  edge [
    source 20
    target 934
  ]
  edge [
    source 20
    target 935
  ]
  edge [
    source 20
    target 936
  ]
  edge [
    source 20
    target 937
  ]
  edge [
    source 20
    target 938
  ]
  edge [
    source 20
    target 939
  ]
  edge [
    source 20
    target 940
  ]
  edge [
    source 20
    target 941
  ]
  edge [
    source 20
    target 942
  ]
  edge [
    source 20
    target 943
  ]
  edge [
    source 20
    target 944
  ]
  edge [
    source 20
    target 945
  ]
  edge [
    source 20
    target 946
  ]
  edge [
    source 20
    target 947
  ]
  edge [
    source 20
    target 948
  ]
  edge [
    source 20
    target 949
  ]
  edge [
    source 20
    target 950
  ]
  edge [
    source 20
    target 951
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 20
    target 292
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 20
    target 963
  ]
  edge [
    source 20
    target 619
  ]
  edge [
    source 20
    target 964
  ]
  edge [
    source 20
    target 965
  ]
  edge [
    source 20
    target 966
  ]
  edge [
    source 20
    target 967
  ]
  edge [
    source 20
    target 968
  ]
  edge [
    source 20
    target 969
  ]
  edge [
    source 20
    target 970
  ]
  edge [
    source 20
    target 971
  ]
  edge [
    source 20
    target 972
  ]
  edge [
    source 20
    target 973
  ]
  edge [
    source 20
    target 974
  ]
  edge [
    source 20
    target 975
  ]
  edge [
    source 20
    target 976
  ]
  edge [
    source 20
    target 977
  ]
  edge [
    source 20
    target 978
  ]
  edge [
    source 20
    target 979
  ]
  edge [
    source 20
    target 980
  ]
  edge [
    source 20
    target 981
  ]
  edge [
    source 20
    target 982
  ]
  edge [
    source 20
    target 983
  ]
  edge [
    source 20
    target 984
  ]
  edge [
    source 20
    target 985
  ]
  edge [
    source 20
    target 986
  ]
  edge [
    source 20
    target 987
  ]
  edge [
    source 20
    target 988
  ]
  edge [
    source 20
    target 989
  ]
  edge [
    source 20
    target 990
  ]
  edge [
    source 20
    target 991
  ]
  edge [
    source 20
    target 992
  ]
  edge [
    source 20
    target 993
  ]
  edge [
    source 20
    target 994
  ]
  edge [
    source 20
    target 995
  ]
  edge [
    source 20
    target 996
  ]
  edge [
    source 20
    target 997
  ]
  edge [
    source 20
    target 998
  ]
  edge [
    source 20
    target 999
  ]
  edge [
    source 20
    target 1000
  ]
  edge [
    source 20
    target 1001
  ]
  edge [
    source 20
    target 1002
  ]
  edge [
    source 20
    target 1003
  ]
  edge [
    source 1004
    target 1005
  ]
  edge [
    source 1007
    target 1008
  ]
  edge [
    source 1009
    target 1010
  ]
  edge [
    source 1011
    target 1012
  ]
  edge [
    source 1013
    target 1014
  ]
  edge [
    source 1015
    target 1016
  ]
  edge [
    source 1017
    target 1018
  ]
]
