graph [
  node [
    id 0
    label "wybra&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "festiwal"
    origin "text"
  ]
  node [
    id 4
    label "melbourne"
    origin "text"
  ]
  node [
    id 5
    label "australia"
    origin "text"
  ]
  node [
    id 6
    label "nie"
    origin "text"
  ]
  node [
    id 7
    label "siebie"
    origin "text"
  ]
  node [
    id 8
    label "swoje"
    origin "text"
  ]
  node [
    id 9
    label "tajski"
    origin "text"
  ]
  node [
    id 10
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 11
    label "Przystanek_Woodstock"
  ]
  node [
    id 12
    label "Woodstock"
  ]
  node [
    id 13
    label "Opole"
  ]
  node [
    id 14
    label "Eurowizja"
  ]
  node [
    id 15
    label "Open'er"
  ]
  node [
    id 16
    label "Metalmania"
  ]
  node [
    id 17
    label "impreza"
  ]
  node [
    id 18
    label "Brutal"
  ]
  node [
    id 19
    label "FAMA"
  ]
  node [
    id 20
    label "Interwizja"
  ]
  node [
    id 21
    label "Nowe_Horyzonty"
  ]
  node [
    id 22
    label "impra"
  ]
  node [
    id 23
    label "rozrywka"
  ]
  node [
    id 24
    label "przyj&#281;cie"
  ]
  node [
    id 25
    label "okazja"
  ]
  node [
    id 26
    label "party"
  ]
  node [
    id 27
    label "&#346;wierkle"
  ]
  node [
    id 28
    label "hipster"
  ]
  node [
    id 29
    label "sprzeciw"
  ]
  node [
    id 30
    label "czerwona_kartka"
  ]
  node [
    id 31
    label "protestacja"
  ]
  node [
    id 32
    label "reakcja"
  ]
  node [
    id 33
    label "etnolekt"
  ]
  node [
    id 34
    label "syjamski"
  ]
  node [
    id 35
    label "azjatycki"
  ]
  node [
    id 36
    label "po_tajsku"
  ]
  node [
    id 37
    label "dalekowschodni"
  ]
  node [
    id 38
    label "po_dalekowschodniemu"
  ]
  node [
    id 39
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 40
    label "kampong"
  ]
  node [
    id 41
    label "typowy"
  ]
  node [
    id 42
    label "ghaty"
  ]
  node [
    id 43
    label "charakterystyczny"
  ]
  node [
    id 44
    label "balut"
  ]
  node [
    id 45
    label "ka&#322;mucki"
  ]
  node [
    id 46
    label "azjatycko"
  ]
  node [
    id 47
    label "j&#281;zyk_naturalny"
  ]
  node [
    id 48
    label "kochanek"
  ]
  node [
    id 49
    label "kum"
  ]
  node [
    id 50
    label "amikus"
  ]
  node [
    id 51
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 52
    label "pobratymiec"
  ]
  node [
    id 53
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 54
    label "drogi"
  ]
  node [
    id 55
    label "sympatyk"
  ]
  node [
    id 56
    label "bratnia_dusza"
  ]
  node [
    id 57
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 58
    label "mi&#322;y"
  ]
  node [
    id 59
    label "kocha&#347;"
  ]
  node [
    id 60
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 61
    label "zwrot"
  ]
  node [
    id 62
    label "partner"
  ]
  node [
    id 63
    label "bratek"
  ]
  node [
    id 64
    label "fagas"
  ]
  node [
    id 65
    label "chrzest"
  ]
  node [
    id 66
    label "kumostwo"
  ]
  node [
    id 67
    label "cz&#322;owiek"
  ]
  node [
    id 68
    label "ojczyc"
  ]
  node [
    id 69
    label "stronnik"
  ]
  node [
    id 70
    label "pobratymca"
  ]
  node [
    id 71
    label "plemiennik"
  ]
  node [
    id 72
    label "wsp&#243;&#322;obywatel"
  ]
  node [
    id 73
    label "wsp&#243;&#322;ziomek"
  ]
  node [
    id 74
    label "brat"
  ]
  node [
    id 75
    label "zwolennik"
  ]
  node [
    id 76
    label "drogo"
  ]
  node [
    id 77
    label "bliski"
  ]
  node [
    id 78
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 79
    label "warto&#347;ciowy"
  ]
  node [
    id 80
    label "Polska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
]
