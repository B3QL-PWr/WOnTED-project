graph [
  node [
    id 0
    label "nawierzchnia"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "skoro"
    origin "text"
  ]
  node [
    id 4
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 5
    label "tak"
    origin "text"
  ]
  node [
    id 6
    label "jakby"
    origin "text"
  ]
  node [
    id 7
    label "raz"
    origin "text"
  ]
  node [
    id 8
    label "trzeba"
    origin "text"
  ]
  node [
    id 9
    label "remontowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pyta"
    origin "text"
  ]
  node [
    id 11
    label "andrzej"
    origin "text"
  ]
  node [
    id 12
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 13
    label "odnawia&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ulica"
    origin "text"
  ]
  node [
    id 15
    label "jaracz"
    origin "text"
  ]
  node [
    id 16
    label "pokrycie"
  ]
  node [
    id 17
    label "droga"
  ]
  node [
    id 18
    label "warstwa"
  ]
  node [
    id 19
    label "po&#322;o&#380;enie"
  ]
  node [
    id 20
    label "rozwini&#281;cie"
  ]
  node [
    id 21
    label "zap&#322;acenie"
  ]
  node [
    id 22
    label "ocynkowanie"
  ]
  node [
    id 23
    label "zadaszenie"
  ]
  node [
    id 24
    label "zap&#322;odnienie"
  ]
  node [
    id 25
    label "naniesienie"
  ]
  node [
    id 26
    label "tworzywo"
  ]
  node [
    id 27
    label "zaizolowanie"
  ]
  node [
    id 28
    label "zamaskowanie"
  ]
  node [
    id 29
    label "ustawienie_si&#281;"
  ]
  node [
    id 30
    label "ocynowanie"
  ]
  node [
    id 31
    label "wierzch"
  ]
  node [
    id 32
    label "cover"
  ]
  node [
    id 33
    label "poszycie"
  ]
  node [
    id 34
    label "fluke"
  ]
  node [
    id 35
    label "zaspokojenie"
  ]
  node [
    id 36
    label "ob&#322;o&#380;enie"
  ]
  node [
    id 37
    label "zafoliowanie"
  ]
  node [
    id 38
    label "wyr&#243;wnanie"
  ]
  node [
    id 39
    label "przykrycie"
  ]
  node [
    id 40
    label "p&#322;aszczyzna"
  ]
  node [
    id 41
    label "przek&#322;adaniec"
  ]
  node [
    id 42
    label "zbi&#243;r"
  ]
  node [
    id 43
    label "covering"
  ]
  node [
    id 44
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 45
    label "podwarstwa"
  ]
  node [
    id 46
    label "ekskursja"
  ]
  node [
    id 47
    label "bezsilnikowy"
  ]
  node [
    id 48
    label "budowla"
  ]
  node [
    id 49
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 50
    label "trasa"
  ]
  node [
    id 51
    label "podbieg"
  ]
  node [
    id 52
    label "turystyka"
  ]
  node [
    id 53
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 54
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 55
    label "rajza"
  ]
  node [
    id 56
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "korona_drogi"
  ]
  node [
    id 58
    label "passage"
  ]
  node [
    id 59
    label "wylot"
  ]
  node [
    id 60
    label "ekwipunek"
  ]
  node [
    id 61
    label "zbior&#243;wka"
  ]
  node [
    id 62
    label "marszrutyzacja"
  ]
  node [
    id 63
    label "wyb&#243;j"
  ]
  node [
    id 64
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 65
    label "drogowskaz"
  ]
  node [
    id 66
    label "spos&#243;b"
  ]
  node [
    id 67
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 68
    label "pobocze"
  ]
  node [
    id 69
    label "journey"
  ]
  node [
    id 70
    label "ruch"
  ]
  node [
    id 71
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 72
    label "mie&#263;_miejsce"
  ]
  node [
    id 73
    label "equal"
  ]
  node [
    id 74
    label "trwa&#263;"
  ]
  node [
    id 75
    label "chodzi&#263;"
  ]
  node [
    id 76
    label "si&#281;ga&#263;"
  ]
  node [
    id 77
    label "stan"
  ]
  node [
    id 78
    label "obecno&#347;&#263;"
  ]
  node [
    id 79
    label "stand"
  ]
  node [
    id 80
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "uczestniczy&#263;"
  ]
  node [
    id 82
    label "participate"
  ]
  node [
    id 83
    label "robi&#263;"
  ]
  node [
    id 84
    label "istnie&#263;"
  ]
  node [
    id 85
    label "pozostawa&#263;"
  ]
  node [
    id 86
    label "zostawa&#263;"
  ]
  node [
    id 87
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 88
    label "adhere"
  ]
  node [
    id 89
    label "compass"
  ]
  node [
    id 90
    label "korzysta&#263;"
  ]
  node [
    id 91
    label "appreciation"
  ]
  node [
    id 92
    label "osi&#261;ga&#263;"
  ]
  node [
    id 93
    label "dociera&#263;"
  ]
  node [
    id 94
    label "get"
  ]
  node [
    id 95
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 96
    label "mierzy&#263;"
  ]
  node [
    id 97
    label "u&#380;ywa&#263;"
  ]
  node [
    id 98
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 99
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 100
    label "exsert"
  ]
  node [
    id 101
    label "being"
  ]
  node [
    id 102
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 103
    label "cecha"
  ]
  node [
    id 104
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 105
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 106
    label "p&#322;ywa&#263;"
  ]
  node [
    id 107
    label "run"
  ]
  node [
    id 108
    label "bangla&#263;"
  ]
  node [
    id 109
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 110
    label "przebiega&#263;"
  ]
  node [
    id 111
    label "wk&#322;ada&#263;"
  ]
  node [
    id 112
    label "proceed"
  ]
  node [
    id 113
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 114
    label "carry"
  ]
  node [
    id 115
    label "bywa&#263;"
  ]
  node [
    id 116
    label "dziama&#263;"
  ]
  node [
    id 117
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 118
    label "stara&#263;_si&#281;"
  ]
  node [
    id 119
    label "para"
  ]
  node [
    id 120
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 121
    label "str&#243;j"
  ]
  node [
    id 122
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 123
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 124
    label "krok"
  ]
  node [
    id 125
    label "tryb"
  ]
  node [
    id 126
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 127
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 128
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 129
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 130
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 131
    label "continue"
  ]
  node [
    id 132
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 133
    label "Ohio"
  ]
  node [
    id 134
    label "wci&#281;cie"
  ]
  node [
    id 135
    label "Nowy_York"
  ]
  node [
    id 136
    label "samopoczucie"
  ]
  node [
    id 137
    label "Illinois"
  ]
  node [
    id 138
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 139
    label "state"
  ]
  node [
    id 140
    label "Jukatan"
  ]
  node [
    id 141
    label "Kalifornia"
  ]
  node [
    id 142
    label "Wirginia"
  ]
  node [
    id 143
    label "wektor"
  ]
  node [
    id 144
    label "Goa"
  ]
  node [
    id 145
    label "Teksas"
  ]
  node [
    id 146
    label "Waszyngton"
  ]
  node [
    id 147
    label "miejsce"
  ]
  node [
    id 148
    label "Massachusetts"
  ]
  node [
    id 149
    label "Alaska"
  ]
  node [
    id 150
    label "Arakan"
  ]
  node [
    id 151
    label "Hawaje"
  ]
  node [
    id 152
    label "Maryland"
  ]
  node [
    id 153
    label "punkt"
  ]
  node [
    id 154
    label "Michigan"
  ]
  node [
    id 155
    label "Arizona"
  ]
  node [
    id 156
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 157
    label "Georgia"
  ]
  node [
    id 158
    label "poziom"
  ]
  node [
    id 159
    label "Pensylwania"
  ]
  node [
    id 160
    label "shape"
  ]
  node [
    id 161
    label "Luizjana"
  ]
  node [
    id 162
    label "Nowy_Meksyk"
  ]
  node [
    id 163
    label "Alabama"
  ]
  node [
    id 164
    label "ilo&#347;&#263;"
  ]
  node [
    id 165
    label "Kansas"
  ]
  node [
    id 166
    label "Oregon"
  ]
  node [
    id 167
    label "Oklahoma"
  ]
  node [
    id 168
    label "Floryda"
  ]
  node [
    id 169
    label "jednostka_administracyjna"
  ]
  node [
    id 170
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 171
    label "gwiazda"
  ]
  node [
    id 172
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 173
    label "Arktur"
  ]
  node [
    id 174
    label "kszta&#322;t"
  ]
  node [
    id 175
    label "Gwiazda_Polarna"
  ]
  node [
    id 176
    label "agregatka"
  ]
  node [
    id 177
    label "gromada"
  ]
  node [
    id 178
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 179
    label "S&#322;o&#324;ce"
  ]
  node [
    id 180
    label "Nibiru"
  ]
  node [
    id 181
    label "konstelacja"
  ]
  node [
    id 182
    label "ornament"
  ]
  node [
    id 183
    label "delta_Scuti"
  ]
  node [
    id 184
    label "&#347;wiat&#322;o"
  ]
  node [
    id 185
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 186
    label "obiekt"
  ]
  node [
    id 187
    label "s&#322;awa"
  ]
  node [
    id 188
    label "promie&#324;"
  ]
  node [
    id 189
    label "star"
  ]
  node [
    id 190
    label "gwiazdosz"
  ]
  node [
    id 191
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 192
    label "asocjacja_gwiazd"
  ]
  node [
    id 193
    label "supergrupa"
  ]
  node [
    id 194
    label "lookout"
  ]
  node [
    id 195
    label "peep"
  ]
  node [
    id 196
    label "patrze&#263;"
  ]
  node [
    id 197
    label "wyziera&#263;"
  ]
  node [
    id 198
    label "look"
  ]
  node [
    id 199
    label "czeka&#263;"
  ]
  node [
    id 200
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 201
    label "punkt_widzenia"
  ]
  node [
    id 202
    label "koso"
  ]
  node [
    id 203
    label "pogl&#261;da&#263;"
  ]
  node [
    id 204
    label "dba&#263;"
  ]
  node [
    id 205
    label "szuka&#263;"
  ]
  node [
    id 206
    label "uwa&#380;a&#263;"
  ]
  node [
    id 207
    label "traktowa&#263;"
  ]
  node [
    id 208
    label "go_steady"
  ]
  node [
    id 209
    label "os&#261;dza&#263;"
  ]
  node [
    id 210
    label "pauzowa&#263;"
  ]
  node [
    id 211
    label "oczekiwa&#263;"
  ]
  node [
    id 212
    label "decydowa&#263;"
  ]
  node [
    id 213
    label "sp&#281;dza&#263;"
  ]
  node [
    id 214
    label "hold"
  ]
  node [
    id 215
    label "anticipate"
  ]
  node [
    id 216
    label "stylizacja"
  ]
  node [
    id 217
    label "wygl&#261;d"
  ]
  node [
    id 218
    label "time"
  ]
  node [
    id 219
    label "cios"
  ]
  node [
    id 220
    label "chwila"
  ]
  node [
    id 221
    label "uderzenie"
  ]
  node [
    id 222
    label "blok"
  ]
  node [
    id 223
    label "shot"
  ]
  node [
    id 224
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 225
    label "struktura_geologiczna"
  ]
  node [
    id 226
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 227
    label "pr&#243;ba"
  ]
  node [
    id 228
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 229
    label "coup"
  ]
  node [
    id 230
    label "siekacz"
  ]
  node [
    id 231
    label "instrumentalizacja"
  ]
  node [
    id 232
    label "trafienie"
  ]
  node [
    id 233
    label "walka"
  ]
  node [
    id 234
    label "zdarzenie_si&#281;"
  ]
  node [
    id 235
    label "wdarcie_si&#281;"
  ]
  node [
    id 236
    label "pogorszenie"
  ]
  node [
    id 237
    label "d&#378;wi&#281;k"
  ]
  node [
    id 238
    label "poczucie"
  ]
  node [
    id 239
    label "reakcja"
  ]
  node [
    id 240
    label "contact"
  ]
  node [
    id 241
    label "stukni&#281;cie"
  ]
  node [
    id 242
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 243
    label "bat"
  ]
  node [
    id 244
    label "spowodowanie"
  ]
  node [
    id 245
    label "rush"
  ]
  node [
    id 246
    label "odbicie"
  ]
  node [
    id 247
    label "dawka"
  ]
  node [
    id 248
    label "zadanie"
  ]
  node [
    id 249
    label "&#347;ci&#281;cie"
  ]
  node [
    id 250
    label "st&#322;uczenie"
  ]
  node [
    id 251
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 252
    label "odbicie_si&#281;"
  ]
  node [
    id 253
    label "dotkni&#281;cie"
  ]
  node [
    id 254
    label "charge"
  ]
  node [
    id 255
    label "dostanie"
  ]
  node [
    id 256
    label "skrytykowanie"
  ]
  node [
    id 257
    label "zagrywka"
  ]
  node [
    id 258
    label "manewr"
  ]
  node [
    id 259
    label "nast&#261;pienie"
  ]
  node [
    id 260
    label "uderzanie"
  ]
  node [
    id 261
    label "pogoda"
  ]
  node [
    id 262
    label "stroke"
  ]
  node [
    id 263
    label "pobicie"
  ]
  node [
    id 264
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 265
    label "flap"
  ]
  node [
    id 266
    label "dotyk"
  ]
  node [
    id 267
    label "zrobienie"
  ]
  node [
    id 268
    label "czas"
  ]
  node [
    id 269
    label "necessity"
  ]
  node [
    id 270
    label "trza"
  ]
  node [
    id 271
    label "powtarza&#263;"
  ]
  node [
    id 272
    label "ulepsza&#263;"
  ]
  node [
    id 273
    label "sum_up"
  ]
  node [
    id 274
    label "przywraca&#263;"
  ]
  node [
    id 275
    label "restore"
  ]
  node [
    id 276
    label "penis"
  ]
  node [
    id 277
    label "narz&#281;dzie_wymierzania_kary"
  ]
  node [
    id 278
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 279
    label "ptaszek"
  ]
  node [
    id 280
    label "przyrodzenie"
  ]
  node [
    id 281
    label "shaft"
  ]
  node [
    id 282
    label "fiut"
  ]
  node [
    id 283
    label "ludno&#347;&#263;"
  ]
  node [
    id 284
    label "zwierz&#281;"
  ]
  node [
    id 285
    label "cz&#322;owiek"
  ]
  node [
    id 286
    label "degenerat"
  ]
  node [
    id 287
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 288
    label "zwyrol"
  ]
  node [
    id 289
    label "czerniak"
  ]
  node [
    id 290
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 291
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 292
    label "paszcza"
  ]
  node [
    id 293
    label "popapraniec"
  ]
  node [
    id 294
    label "skuba&#263;"
  ]
  node [
    id 295
    label "skubanie"
  ]
  node [
    id 296
    label "agresja"
  ]
  node [
    id 297
    label "skubni&#281;cie"
  ]
  node [
    id 298
    label "zwierz&#281;ta"
  ]
  node [
    id 299
    label "fukni&#281;cie"
  ]
  node [
    id 300
    label "farba"
  ]
  node [
    id 301
    label "fukanie"
  ]
  node [
    id 302
    label "istota_&#380;ywa"
  ]
  node [
    id 303
    label "gad"
  ]
  node [
    id 304
    label "tresowa&#263;"
  ]
  node [
    id 305
    label "siedzie&#263;"
  ]
  node [
    id 306
    label "oswaja&#263;"
  ]
  node [
    id 307
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 308
    label "poligamia"
  ]
  node [
    id 309
    label "oz&#243;r"
  ]
  node [
    id 310
    label "skubn&#261;&#263;"
  ]
  node [
    id 311
    label "wios&#322;owa&#263;"
  ]
  node [
    id 312
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 313
    label "le&#380;enie"
  ]
  node [
    id 314
    label "niecz&#322;owiek"
  ]
  node [
    id 315
    label "wios&#322;owanie"
  ]
  node [
    id 316
    label "napasienie_si&#281;"
  ]
  node [
    id 317
    label "wiwarium"
  ]
  node [
    id 318
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 319
    label "animalista"
  ]
  node [
    id 320
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 321
    label "budowa"
  ]
  node [
    id 322
    label "hodowla"
  ]
  node [
    id 323
    label "pasienie_si&#281;"
  ]
  node [
    id 324
    label "sodomita"
  ]
  node [
    id 325
    label "monogamia"
  ]
  node [
    id 326
    label "przyssawka"
  ]
  node [
    id 327
    label "zachowanie"
  ]
  node [
    id 328
    label "budowa_cia&#322;a"
  ]
  node [
    id 329
    label "okrutnik"
  ]
  node [
    id 330
    label "grzbiet"
  ]
  node [
    id 331
    label "weterynarz"
  ]
  node [
    id 332
    label "&#322;eb"
  ]
  node [
    id 333
    label "wylinka"
  ]
  node [
    id 334
    label "bestia"
  ]
  node [
    id 335
    label "poskramia&#263;"
  ]
  node [
    id 336
    label "fauna"
  ]
  node [
    id 337
    label "treser"
  ]
  node [
    id 338
    label "siedzenie"
  ]
  node [
    id 339
    label "le&#380;e&#263;"
  ]
  node [
    id 340
    label "ludzko&#347;&#263;"
  ]
  node [
    id 341
    label "asymilowanie"
  ]
  node [
    id 342
    label "wapniak"
  ]
  node [
    id 343
    label "asymilowa&#263;"
  ]
  node [
    id 344
    label "os&#322;abia&#263;"
  ]
  node [
    id 345
    label "posta&#263;"
  ]
  node [
    id 346
    label "hominid"
  ]
  node [
    id 347
    label "podw&#322;adny"
  ]
  node [
    id 348
    label "os&#322;abianie"
  ]
  node [
    id 349
    label "g&#322;owa"
  ]
  node [
    id 350
    label "figura"
  ]
  node [
    id 351
    label "portrecista"
  ]
  node [
    id 352
    label "dwun&#243;g"
  ]
  node [
    id 353
    label "profanum"
  ]
  node [
    id 354
    label "mikrokosmos"
  ]
  node [
    id 355
    label "nasada"
  ]
  node [
    id 356
    label "duch"
  ]
  node [
    id 357
    label "antropochoria"
  ]
  node [
    id 358
    label "osoba"
  ]
  node [
    id 359
    label "wz&#243;r"
  ]
  node [
    id 360
    label "senior"
  ]
  node [
    id 361
    label "oddzia&#322;ywanie"
  ]
  node [
    id 362
    label "Adam"
  ]
  node [
    id 363
    label "homo_sapiens"
  ]
  node [
    id 364
    label "polifag"
  ]
  node [
    id 365
    label "innowierstwo"
  ]
  node [
    id 366
    label "ch&#322;opstwo"
  ]
  node [
    id 367
    label "doprowadza&#263;"
  ]
  node [
    id 368
    label "zmienia&#263;"
  ]
  node [
    id 369
    label "wstrzeliwa&#263;_si&#281;"
  ]
  node [
    id 370
    label "better"
  ]
  node [
    id 371
    label "podawa&#263;"
  ]
  node [
    id 372
    label "repeat"
  ]
  node [
    id 373
    label "podszkala&#263;_si&#281;"
  ]
  node [
    id 374
    label "impart"
  ]
  node [
    id 375
    label "pas_rozdzielczy"
  ]
  node [
    id 376
    label "&#347;rodowisko"
  ]
  node [
    id 377
    label "streetball"
  ]
  node [
    id 378
    label "miasteczko"
  ]
  node [
    id 379
    label "chodnik"
  ]
  node [
    id 380
    label "pas_ruchu"
  ]
  node [
    id 381
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 382
    label "pierzeja"
  ]
  node [
    id 383
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 384
    label "wysepka"
  ]
  node [
    id 385
    label "arteria"
  ]
  node [
    id 386
    label "Broadway"
  ]
  node [
    id 387
    label "autostrada"
  ]
  node [
    id 388
    label "jezdnia"
  ]
  node [
    id 389
    label "grupa"
  ]
  node [
    id 390
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 391
    label "Fremeni"
  ]
  node [
    id 392
    label "class"
  ]
  node [
    id 393
    label "zesp&#243;&#322;"
  ]
  node [
    id 394
    label "obiekt_naturalny"
  ]
  node [
    id 395
    label "otoczenie"
  ]
  node [
    id 396
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 397
    label "environment"
  ]
  node [
    id 398
    label "rzecz"
  ]
  node [
    id 399
    label "huczek"
  ]
  node [
    id 400
    label "ekosystem"
  ]
  node [
    id 401
    label "wszechstworzenie"
  ]
  node [
    id 402
    label "woda"
  ]
  node [
    id 403
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 404
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 405
    label "teren"
  ]
  node [
    id 406
    label "stw&#243;r"
  ]
  node [
    id 407
    label "warunki"
  ]
  node [
    id 408
    label "Ziemia"
  ]
  node [
    id 409
    label "biota"
  ]
  node [
    id 410
    label "odm&#322;adzanie"
  ]
  node [
    id 411
    label "liga"
  ]
  node [
    id 412
    label "jednostka_systematyczna"
  ]
  node [
    id 413
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 414
    label "egzemplarz"
  ]
  node [
    id 415
    label "Entuzjastki"
  ]
  node [
    id 416
    label "kompozycja"
  ]
  node [
    id 417
    label "Terranie"
  ]
  node [
    id 418
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 419
    label "category"
  ]
  node [
    id 420
    label "pakiet_klimatyczny"
  ]
  node [
    id 421
    label "oddzia&#322;"
  ]
  node [
    id 422
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 423
    label "cz&#261;steczka"
  ]
  node [
    id 424
    label "stage_set"
  ]
  node [
    id 425
    label "type"
  ]
  node [
    id 426
    label "specgrupa"
  ]
  node [
    id 427
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 428
    label "&#346;wietliki"
  ]
  node [
    id 429
    label "odm&#322;odzenie"
  ]
  node [
    id 430
    label "Eurogrupa"
  ]
  node [
    id 431
    label "odm&#322;adza&#263;"
  ]
  node [
    id 432
    label "formacja_geologiczna"
  ]
  node [
    id 433
    label "harcerze_starsi"
  ]
  node [
    id 434
    label "naczynie"
  ]
  node [
    id 435
    label "ko&#322;o_t&#281;tnicze_m&#243;zgu"
  ]
  node [
    id 436
    label "artery"
  ]
  node [
    id 437
    label "Tuszyn"
  ]
  node [
    id 438
    label "Nowy_Staw"
  ]
  node [
    id 439
    label "Bia&#322;a_Piska"
  ]
  node [
    id 440
    label "Koronowo"
  ]
  node [
    id 441
    label "Wysoka"
  ]
  node [
    id 442
    label "Ma&#322;ogoszcz"
  ]
  node [
    id 443
    label "Niemodlin"
  ]
  node [
    id 444
    label "Sulmierzyce"
  ]
  node [
    id 445
    label "Parczew"
  ]
  node [
    id 446
    label "Dyn&#243;w"
  ]
  node [
    id 447
    label "Brwin&#243;w"
  ]
  node [
    id 448
    label "Pogorzela"
  ]
  node [
    id 449
    label "Mszczon&#243;w"
  ]
  node [
    id 450
    label "Olsztynek"
  ]
  node [
    id 451
    label "Soko&#322;&#243;w_Ma&#322;opolski"
  ]
  node [
    id 452
    label "Resko"
  ]
  node [
    id 453
    label "&#379;uromin"
  ]
  node [
    id 454
    label "Dobrzany"
  ]
  node [
    id 455
    label "Wilamowice"
  ]
  node [
    id 456
    label "Kruszwica"
  ]
  node [
    id 457
    label "Jedlina-Zdr&#243;j"
  ]
  node [
    id 458
    label "Warta"
  ]
  node [
    id 459
    label "&#321;och&#243;w"
  ]
  node [
    id 460
    label "Milicz"
  ]
  node [
    id 461
    label "Niepo&#322;omice"
  ]
  node [
    id 462
    label "My&#347;lib&#243;rz"
  ]
  node [
    id 463
    label "Prabuty"
  ]
  node [
    id 464
    label "Sul&#281;cin"
  ]
  node [
    id 465
    label "Kudowa-Zdr&#243;j"
  ]
  node [
    id 466
    label "Pi&#324;cz&#243;w"
  ]
  node [
    id 467
    label "Brzeziny"
  ]
  node [
    id 468
    label "G&#322;ubczyce"
  ]
  node [
    id 469
    label "Mogilno"
  ]
  node [
    id 470
    label "Suchowola"
  ]
  node [
    id 471
    label "Ch&#281;ciny"
  ]
  node [
    id 472
    label "Pilawa"
  ]
  node [
    id 473
    label "Oborniki_&#346;l&#261;skie"
  ]
  node [
    id 474
    label "W&#322;adys&#322;aw&#243;w"
  ]
  node [
    id 475
    label "St&#281;szew"
  ]
  node [
    id 476
    label "Jasie&#324;"
  ]
  node [
    id 477
    label "Sulej&#243;w"
  ]
  node [
    id 478
    label "B&#322;a&#380;owa"
  ]
  node [
    id 479
    label "D&#261;browa_Bia&#322;ostocka"
  ]
  node [
    id 480
    label "Bychawa"
  ]
  node [
    id 481
    label "Grab&#243;w_nad_Prosn&#261;"
  ]
  node [
    id 482
    label "Dolsk"
  ]
  node [
    id 483
    label "&#346;wierzawa"
  ]
  node [
    id 484
    label "Brze&#347;&#263;_Kujawski"
  ]
  node [
    id 485
    label "Zalewo"
  ]
  node [
    id 486
    label "Olszyna"
  ]
  node [
    id 487
    label "Czerwie&#324;sk"
  ]
  node [
    id 488
    label "Biecz"
  ]
  node [
    id 489
    label "S&#281;dzisz&#243;w"
  ]
  node [
    id 490
    label "Gryf&#243;w_&#346;l&#261;ski"
  ]
  node [
    id 491
    label "Drezdenko"
  ]
  node [
    id 492
    label "Bia&#322;a"
  ]
  node [
    id 493
    label "Lipsko"
  ]
  node [
    id 494
    label "G&#243;rzno"
  ]
  node [
    id 495
    label "&#346;migiel"
  ]
  node [
    id 496
    label "&#346;wi&#261;tniki_G&#243;rne"
  ]
  node [
    id 497
    label "Suchedni&#243;w"
  ]
  node [
    id 498
    label "Lubacz&#243;w"
  ]
  node [
    id 499
    label "Tuliszk&#243;w"
  ]
  node [
    id 500
    label "Polanica-Zdr&#243;j"
  ]
  node [
    id 501
    label "Mirsk"
  ]
  node [
    id 502
    label "G&#243;ra"
  ]
  node [
    id 503
    label "Rychwa&#322;"
  ]
  node [
    id 504
    label "Jab&#322;onowo_Pomorskie"
  ]
  node [
    id 505
    label "Olesno"
  ]
  node [
    id 506
    label "Toszek"
  ]
  node [
    id 507
    label "Prusice"
  ]
  node [
    id 508
    label "Radk&#243;w"
  ]
  node [
    id 509
    label "Radzy&#324;_Che&#322;mi&#324;ski"
  ]
  node [
    id 510
    label "Radzymin"
  ]
  node [
    id 511
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 512
    label "Ryn"
  ]
  node [
    id 513
    label "Orzysz"
  ]
  node [
    id 514
    label "Radziej&#243;w"
  ]
  node [
    id 515
    label "Supra&#347;l"
  ]
  node [
    id 516
    label "Imielin"
  ]
  node [
    id 517
    label "Karczew"
  ]
  node [
    id 518
    label "Sucha_Beskidzka"
  ]
  node [
    id 519
    label "&#346;wierad&#243;w-Zdr&#243;j"
  ]
  node [
    id 520
    label "Szczucin"
  ]
  node [
    id 521
    label "Niemcza"
  ]
  node [
    id 522
    label "Kobylin"
  ]
  node [
    id 523
    label "Tokaj"
  ]
  node [
    id 524
    label "Pie&#324;sk"
  ]
  node [
    id 525
    label "Kock"
  ]
  node [
    id 526
    label "Mi&#281;dzylesie"
  ]
  node [
    id 527
    label "Bodzentyn"
  ]
  node [
    id 528
    label "Ska&#322;a"
  ]
  node [
    id 529
    label "Przedb&#243;rz"
  ]
  node [
    id 530
    label "Bielsk_Podlaski"
  ]
  node [
    id 531
    label "Krzeszowice"
  ]
  node [
    id 532
    label "Jeziorany"
  ]
  node [
    id 533
    label "Czarnk&#243;w"
  ]
  node [
    id 534
    label "Mi&#322;os&#322;aw"
  ]
  node [
    id 535
    label "Czch&#243;w"
  ]
  node [
    id 536
    label "&#321;asin"
  ]
  node [
    id 537
    label "Drohiczyn"
  ]
  node [
    id 538
    label "Kolno"
  ]
  node [
    id 539
    label "Bie&#380;u&#324;"
  ]
  node [
    id 540
    label "K&#322;ecko"
  ]
  node [
    id 541
    label "Rabka-Zdr&#243;j"
  ]
  node [
    id 542
    label "Golczewo"
  ]
  node [
    id 543
    label "Pniewy"
  ]
  node [
    id 544
    label "Jedlicze"
  ]
  node [
    id 545
    label "Glinojeck"
  ]
  node [
    id 546
    label "Wojnicz"
  ]
  node [
    id 547
    label "Podd&#281;bice"
  ]
  node [
    id 548
    label "Miastko"
  ]
  node [
    id 549
    label "Kamie&#324;_Pomorski"
  ]
  node [
    id 550
    label "Pako&#347;&#263;"
  ]
  node [
    id 551
    label "Pi&#322;awa_G&#243;rna"
  ]
  node [
    id 552
    label "I&#324;sko"
  ]
  node [
    id 553
    label "Rudnik_nad_Sanem"
  ]
  node [
    id 554
    label "Sejny"
  ]
  node [
    id 555
    label "Skaryszew"
  ]
  node [
    id 556
    label "Wojciesz&#243;w"
  ]
  node [
    id 557
    label "Nieszawa"
  ]
  node [
    id 558
    label "Gogolin"
  ]
  node [
    id 559
    label "S&#322;awa"
  ]
  node [
    id 560
    label "Bierut&#243;w"
  ]
  node [
    id 561
    label "Knyszyn"
  ]
  node [
    id 562
    label "Podkowa_Le&#347;na"
  ]
  node [
    id 563
    label "I&#322;&#380;a"
  ]
  node [
    id 564
    label "Grodk&#243;w"
  ]
  node [
    id 565
    label "Krzepice"
  ]
  node [
    id 566
    label "Janikowo"
  ]
  node [
    id 567
    label "S&#261;dowa_Wisznia"
  ]
  node [
    id 568
    label "&#321;osice"
  ]
  node [
    id 569
    label "&#379;ukowo"
  ]
  node [
    id 570
    label "Witkowo"
  ]
  node [
    id 571
    label "Czempi&#324;"
  ]
  node [
    id 572
    label "Wyszogr&#243;d"
  ]
  node [
    id 573
    label "Dzia&#322;oszyn"
  ]
  node [
    id 574
    label "Dzierzgo&#324;"
  ]
  node [
    id 575
    label "S&#281;popol"
  ]
  node [
    id 576
    label "Terespol"
  ]
  node [
    id 577
    label "Brzoz&#243;w"
  ]
  node [
    id 578
    label "Ko&#378;min_Wielkopolski"
  ]
  node [
    id 579
    label "Bystrzyca_K&#322;odzka"
  ]
  node [
    id 580
    label "Dobre_Miasto"
  ]
  node [
    id 581
    label "&#262;miel&#243;w"
  ]
  node [
    id 582
    label "Kcynia"
  ]
  node [
    id 583
    label "Obrzycko"
  ]
  node [
    id 584
    label "S&#281;p&#243;lno_Kraje&#324;skie"
  ]
  node [
    id 585
    label "Iwonicz-Zdr&#243;j"
  ]
  node [
    id 586
    label "S&#322;omniki"
  ]
  node [
    id 587
    label "Barcin"
  ]
  node [
    id 588
    label "Mak&#243;w_Mazowiecki"
  ]
  node [
    id 589
    label "Gniewkowo"
  ]
  node [
    id 590
    label "Paj&#281;czno"
  ]
  node [
    id 591
    label "Jedwabne"
  ]
  node [
    id 592
    label "Tyczyn"
  ]
  node [
    id 593
    label "Osiek"
  ]
  node [
    id 594
    label "Pu&#324;sk"
  ]
  node [
    id 595
    label "Zakroczym"
  ]
  node [
    id 596
    label "Sura&#380;"
  ]
  node [
    id 597
    label "&#321;abiszyn"
  ]
  node [
    id 598
    label "Skarszewy"
  ]
  node [
    id 599
    label "Rapperswil"
  ]
  node [
    id 600
    label "K&#261;ty_Wroc&#322;awskie"
  ]
  node [
    id 601
    label "Rzepin"
  ]
  node [
    id 602
    label "&#346;lesin"
  ]
  node [
    id 603
    label "Ko&#380;uch&#243;w"
  ]
  node [
    id 604
    label "Po&#322;aniec"
  ]
  node [
    id 605
    label "Chodecz"
  ]
  node [
    id 606
    label "W&#261;sosz"
  ]
  node [
    id 607
    label "Krasnobr&#243;d"
  ]
  node [
    id 608
    label "Kargowa"
  ]
  node [
    id 609
    label "Zakliczyn"
  ]
  node [
    id 610
    label "Bukowno"
  ]
  node [
    id 611
    label "&#379;ychlin"
  ]
  node [
    id 612
    label "G&#322;og&#243;wek"
  ]
  node [
    id 613
    label "&#321;askarzew"
  ]
  node [
    id 614
    label "Drawno"
  ]
  node [
    id 615
    label "Kazimierza_Wielka"
  ]
  node [
    id 616
    label "Kozieg&#322;owy"
  ]
  node [
    id 617
    label "Kowal"
  ]
  node [
    id 618
    label "Pilzno"
  ]
  node [
    id 619
    label "Jordan&#243;w"
  ]
  node [
    id 620
    label "S&#281;dzisz&#243;w_Ma&#322;opolski"
  ]
  node [
    id 621
    label "Ustrzyki_Dolne"
  ]
  node [
    id 622
    label "Strumie&#324;"
  ]
  node [
    id 623
    label "Radymno"
  ]
  node [
    id 624
    label "Otmuch&#243;w"
  ]
  node [
    id 625
    label "K&#243;rnik"
  ]
  node [
    id 626
    label "Wierusz&#243;w"
  ]
  node [
    id 627
    label "Na&#322;&#281;cz&#243;w"
  ]
  node [
    id 628
    label "Tychowo"
  ]
  node [
    id 629
    label "Czersk"
  ]
  node [
    id 630
    label "Mo&#324;ki"
  ]
  node [
    id 631
    label "Pelplin"
  ]
  node [
    id 632
    label "Trzci&#324;sko-Zdr&#243;j"
  ]
  node [
    id 633
    label "Poniec"
  ]
  node [
    id 634
    label "Piotrk&#243;w_Kujawski"
  ]
  node [
    id 635
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 636
    label "G&#261;bin"
  ]
  node [
    id 637
    label "Gniew"
  ]
  node [
    id 638
    label "Cieszan&#243;w"
  ]
  node [
    id 639
    label "Serock"
  ]
  node [
    id 640
    label "Drzewica"
  ]
  node [
    id 641
    label "Skwierzyna"
  ]
  node [
    id 642
    label "Bra&#324;sk"
  ]
  node [
    id 643
    label "Nowe_Brzesko"
  ]
  node [
    id 644
    label "Dobrzy&#324;_nad_Wis&#322;&#261;"
  ]
  node [
    id 645
    label "Nowe_Miasto_Lubawskie"
  ]
  node [
    id 646
    label "Szadek"
  ]
  node [
    id 647
    label "Kalety"
  ]
  node [
    id 648
    label "Borek_Wielkopolski"
  ]
  node [
    id 649
    label "Kalisz_Pomorski"
  ]
  node [
    id 650
    label "Pyzdry"
  ]
  node [
    id 651
    label "Ostr&#243;w_Lubelski"
  ]
  node [
    id 652
    label "Bia&#322;a_Rawska"
  ]
  node [
    id 653
    label "Bobowa"
  ]
  node [
    id 654
    label "Cedynia"
  ]
  node [
    id 655
    label "Sieniawa"
  ]
  node [
    id 656
    label "Su&#322;kowice"
  ]
  node [
    id 657
    label "Drobin"
  ]
  node [
    id 658
    label "Zag&#243;rz"
  ]
  node [
    id 659
    label "Brok"
  ]
  node [
    id 660
    label "Nowe"
  ]
  node [
    id 661
    label "Szczebrzeszyn"
  ]
  node [
    id 662
    label "O&#380;ar&#243;w"
  ]
  node [
    id 663
    label "Rydzyna"
  ]
  node [
    id 664
    label "&#379;arki"
  ]
  node [
    id 665
    label "Zwole&#324;"
  ]
  node [
    id 666
    label "Nowy_Dw&#243;r_Gda&#324;ski"
  ]
  node [
    id 667
    label "G&#322;og&#243;w_Ma&#322;opolski"
  ]
  node [
    id 668
    label "Drawsko_Pomorskie"
  ]
  node [
    id 669
    label "Torzym"
  ]
  node [
    id 670
    label "Ryglice"
  ]
  node [
    id 671
    label "Szepietowo"
  ]
  node [
    id 672
    label "Biskupiec"
  ]
  node [
    id 673
    label "&#379;abno"
  ]
  node [
    id 674
    label "Opat&#243;w"
  ]
  node [
    id 675
    label "Przysucha"
  ]
  node [
    id 676
    label "Ryki"
  ]
  node [
    id 677
    label "Reszel"
  ]
  node [
    id 678
    label "Kolbuszowa"
  ]
  node [
    id 679
    label "Margonin"
  ]
  node [
    id 680
    label "Kamie&#324;_Kraje&#324;ski"
  ]
  node [
    id 681
    label "Mi&#281;dzych&#243;d"
  ]
  node [
    id 682
    label "Sk&#281;pe"
  ]
  node [
    id 683
    label "Szubin"
  ]
  node [
    id 684
    label "&#379;elech&#243;w"
  ]
  node [
    id 685
    label "Proszowice"
  ]
  node [
    id 686
    label "Polan&#243;w"
  ]
  node [
    id 687
    label "Chorzele"
  ]
  node [
    id 688
    label "Kostrzyn"
  ]
  node [
    id 689
    label "Koniecpol"
  ]
  node [
    id 690
    label "Ryman&#243;w"
  ]
  node [
    id 691
    label "Dziwn&#243;w"
  ]
  node [
    id 692
    label "Lesko"
  ]
  node [
    id 693
    label "Lw&#243;wek"
  ]
  node [
    id 694
    label "Brzeszcze"
  ]
  node [
    id 695
    label "Strzy&#380;&#243;w"
  ]
  node [
    id 696
    label "Sierak&#243;w"
  ]
  node [
    id 697
    label "Bia&#322;obrzegi"
  ]
  node [
    id 698
    label "Skalbmierz"
  ]
  node [
    id 699
    label "Zawichost"
  ]
  node [
    id 700
    label "Raszk&#243;w"
  ]
  node [
    id 701
    label "Sian&#243;w"
  ]
  node [
    id 702
    label "&#379;erk&#243;w"
  ]
  node [
    id 703
    label "Pieszyce"
  ]
  node [
    id 704
    label "Zel&#243;w"
  ]
  node [
    id 705
    label "I&#322;owa"
  ]
  node [
    id 706
    label "Uniej&#243;w"
  ]
  node [
    id 707
    label "Przec&#322;aw"
  ]
  node [
    id 708
    label "Mieszkowice"
  ]
  node [
    id 709
    label "Wisztyniec"
  ]
  node [
    id 710
    label "Szumsk"
  ]
  node [
    id 711
    label "Petryk&#243;w"
  ]
  node [
    id 712
    label "Wyrzysk"
  ]
  node [
    id 713
    label "Myszyniec"
  ]
  node [
    id 714
    label "Gorz&#243;w_&#346;l&#261;ski"
  ]
  node [
    id 715
    label "Dobrzyca"
  ]
  node [
    id 716
    label "W&#322;oszczowa"
  ]
  node [
    id 717
    label "Goni&#261;dz"
  ]
  node [
    id 718
    label "L&#261;dek-Zdr&#243;j"
  ]
  node [
    id 719
    label "Dukla"
  ]
  node [
    id 720
    label "Siewierz"
  ]
  node [
    id 721
    label "Kun&#243;w"
  ]
  node [
    id 722
    label "Lubie&#324;_Kujawski"
  ]
  node [
    id 723
    label "Aleksandr&#243;w_Kujawski"
  ]
  node [
    id 724
    label "O&#380;ar&#243;w_Mazowiecki"
  ]
  node [
    id 725
    label "Piwniczna-Zdr&#243;j"
  ]
  node [
    id 726
    label "Zator"
  ]
  node [
    id 727
    label "Bolk&#243;w"
  ]
  node [
    id 728
    label "Krosno_Odrza&#324;skie"
  ]
  node [
    id 729
    label "Odolan&#243;w"
  ]
  node [
    id 730
    label "Golina"
  ]
  node [
    id 731
    label "Miech&#243;w"
  ]
  node [
    id 732
    label "Mogielnica"
  ]
  node [
    id 733
    label "Muszyna"
  ]
  node [
    id 734
    label "Dobczyce"
  ]
  node [
    id 735
    label "Radomy&#347;l_Wielki"
  ]
  node [
    id 736
    label "R&#243;&#380;an"
  ]
  node [
    id 737
    label "Zab&#322;ud&#243;w"
  ]
  node [
    id 738
    label "Wysokie_Mazowieckie"
  ]
  node [
    id 739
    label "Ulan&#243;w"
  ]
  node [
    id 740
    label "Rogo&#378;no"
  ]
  node [
    id 741
    label "Ciechanowiec"
  ]
  node [
    id 742
    label "Lubomierz"
  ]
  node [
    id 743
    label "Mierosz&#243;w"
  ]
  node [
    id 744
    label "Lubawa"
  ]
  node [
    id 745
    label "Ci&#281;&#380;kowice"
  ]
  node [
    id 746
    label "Tykocin"
  ]
  node [
    id 747
    label "Tarczyn"
  ]
  node [
    id 748
    label "Rejowiec_Fabryczny"
  ]
  node [
    id 749
    label "Alwernia"
  ]
  node [
    id 750
    label "Karlino"
  ]
  node [
    id 751
    label "Duszniki-Zdr&#243;j"
  ]
  node [
    id 752
    label "Warka"
  ]
  node [
    id 753
    label "Krynica_Morska"
  ]
  node [
    id 754
    label "Lewin_Brzeski"
  ]
  node [
    id 755
    label "Chyr&#243;w"
  ]
  node [
    id 756
    label "Przemk&#243;w"
  ]
  node [
    id 757
    label "Hel"
  ]
  node [
    id 758
    label "Chocian&#243;w"
  ]
  node [
    id 759
    label "Po&#322;czyn-Zdr&#243;j"
  ]
  node [
    id 760
    label "Stawiszyn"
  ]
  node [
    id 761
    label "Strzelce_Kraje&#324;skie"
  ]
  node [
    id 762
    label "Ciechocinek"
  ]
  node [
    id 763
    label "Puszczykowo"
  ]
  node [
    id 764
    label "Mszana_Dolna"
  ]
  node [
    id 765
    label "Rad&#322;&#243;w"
  ]
  node [
    id 766
    label "Nasielsk"
  ]
  node [
    id 767
    label "Szczyrk"
  ]
  node [
    id 768
    label "Trzemeszno"
  ]
  node [
    id 769
    label "Recz"
  ]
  node [
    id 770
    label "Wo&#322;czyn"
  ]
  node [
    id 771
    label "Pilica"
  ]
  node [
    id 772
    label "Prochowice"
  ]
  node [
    id 773
    label "Buk"
  ]
  node [
    id 774
    label "Kowary"
  ]
  node [
    id 775
    label "Tyszowce"
  ]
  node [
    id 776
    label "Szczawno-Zdr&#243;j"
  ]
  node [
    id 777
    label "Bojanowo"
  ]
  node [
    id 778
    label "Maszewo"
  ]
  node [
    id 779
    label "Ogrodzieniec"
  ]
  node [
    id 780
    label "Tuch&#243;w"
  ]
  node [
    id 781
    label "Kamie&#324;sk"
  ]
  node [
    id 782
    label "Chojna"
  ]
  node [
    id 783
    label "Gryb&#243;w"
  ]
  node [
    id 784
    label "Wasilk&#243;w"
  ]
  node [
    id 785
    label "Krzy&#380;_Wielkopolski"
  ]
  node [
    id 786
    label "Janowiec_Wielkopolski"
  ]
  node [
    id 787
    label "Zag&#243;r&#243;w"
  ]
  node [
    id 788
    label "Che&#322;mek"
  ]
  node [
    id 789
    label "Z&#322;oty_Stok"
  ]
  node [
    id 790
    label "Stronie_&#346;l&#261;skie"
  ]
  node [
    id 791
    label "Nowy_Wi&#347;nicz"
  ]
  node [
    id 792
    label "Krynica-Zdr&#243;j"
  ]
  node [
    id 793
    label "Wolbrom"
  ]
  node [
    id 794
    label "Szczuczyn"
  ]
  node [
    id 795
    label "S&#322;awk&#243;w"
  ]
  node [
    id 796
    label "Kazimierz_Dolny"
  ]
  node [
    id 797
    label "Wo&#378;niki"
  ]
  node [
    id 798
    label "obwodnica_autostradowa"
  ]
  node [
    id 799
    label "droga_publiczna"
  ]
  node [
    id 800
    label "przej&#347;cie"
  ]
  node [
    id 801
    label "kraw&#281;&#380;nik"
  ]
  node [
    id 802
    label "chody"
  ]
  node [
    id 803
    label "sztreka"
  ]
  node [
    id 804
    label "kostka_brukowa"
  ]
  node [
    id 805
    label "pieszy"
  ]
  node [
    id 806
    label "drzewo"
  ]
  node [
    id 807
    label "wyrobisko"
  ]
  node [
    id 808
    label "kornik"
  ]
  node [
    id 809
    label "dywanik"
  ]
  node [
    id 810
    label "przodek"
  ]
  node [
    id 811
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 812
    label "plac"
  ]
  node [
    id 813
    label "koszyk&#243;wka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 375
  ]
  edge [
    source 14
    target 376
  ]
  edge [
    source 14
    target 377
  ]
  edge [
    source 14
    target 378
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 380
  ]
  edge [
    source 14
    target 381
  ]
  edge [
    source 14
    target 382
  ]
  edge [
    source 14
    target 383
  ]
  edge [
    source 14
    target 384
  ]
  edge [
    source 14
    target 385
  ]
  edge [
    source 14
    target 386
  ]
  edge [
    source 14
    target 387
  ]
  edge [
    source 14
    target 388
  ]
  edge [
    source 14
    target 389
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 14
    target 390
  ]
  edge [
    source 14
    target 391
  ]
  edge [
    source 14
    target 392
  ]
  edge [
    source 14
    target 393
  ]
  edge [
    source 14
    target 394
  ]
  edge [
    source 14
    target 395
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 397
  ]
  edge [
    source 14
    target 398
  ]
  edge [
    source 14
    target 399
  ]
  edge [
    source 14
    target 400
  ]
  edge [
    source 14
    target 401
  ]
  edge [
    source 14
    target 402
  ]
  edge [
    source 14
    target 403
  ]
  edge [
    source 14
    target 404
  ]
  edge [
    source 14
    target 405
  ]
  edge [
    source 14
    target 354
  ]
  edge [
    source 14
    target 406
  ]
  edge [
    source 14
    target 407
  ]
  edge [
    source 14
    target 408
  ]
  edge [
    source 14
    target 336
  ]
  edge [
    source 14
    target 409
  ]
  edge [
    source 14
    target 410
  ]
  edge [
    source 14
    target 411
  ]
  edge [
    source 14
    target 412
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 413
  ]
  edge [
    source 14
    target 343
  ]
  edge [
    source 14
    target 414
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 42
  ]
  edge [
    source 14
    target 416
  ]
  edge [
    source 14
    target 417
  ]
  edge [
    source 14
    target 418
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 420
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 46
  ]
  edge [
    source 14
    target 47
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 14
    target 50
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 14
    target 52
  ]
  edge [
    source 14
    target 53
  ]
  edge [
    source 14
    target 54
  ]
  edge [
    source 14
    target 55
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 58
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 69
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 634
  ]
  edge [
    source 14
    target 635
  ]
  edge [
    source 14
    target 636
  ]
  edge [
    source 14
    target 637
  ]
  edge [
    source 14
    target 638
  ]
  edge [
    source 14
    target 639
  ]
  edge [
    source 14
    target 640
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 643
  ]
  edge [
    source 14
    target 644
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 651
  ]
  edge [
    source 14
    target 652
  ]
  edge [
    source 14
    target 653
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 787
  ]
  edge [
    source 14
    target 788
  ]
  edge [
    source 14
    target 789
  ]
  edge [
    source 14
    target 790
  ]
  edge [
    source 14
    target 791
  ]
  edge [
    source 14
    target 792
  ]
  edge [
    source 14
    target 793
  ]
  edge [
    source 14
    target 794
  ]
  edge [
    source 14
    target 795
  ]
  edge [
    source 14
    target 796
  ]
  edge [
    source 14
    target 797
  ]
  edge [
    source 14
    target 798
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 800
  ]
  edge [
    source 14
    target 801
  ]
  edge [
    source 14
    target 802
  ]
  edge [
    source 14
    target 803
  ]
  edge [
    source 14
    target 804
  ]
  edge [
    source 14
    target 805
  ]
  edge [
    source 14
    target 806
  ]
  edge [
    source 14
    target 807
  ]
  edge [
    source 14
    target 808
  ]
  edge [
    source 14
    target 809
  ]
  edge [
    source 14
    target 810
  ]
  edge [
    source 14
    target 811
  ]
  edge [
    source 14
    target 812
  ]
  edge [
    source 14
    target 813
  ]
]
