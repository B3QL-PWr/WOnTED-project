graph [
  node [
    id 0
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 1
    label "sprawny"
    origin "text"
  ]
  node [
    id 2
    label "telefon"
    origin "text"
  ]
  node [
    id 3
    label "wystawi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "nieco"
    origin "text"
  ]
  node [
    id 5
    label "ponad"
    origin "text"
  ]
  node [
    id 6
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 7
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 8
    label "szczyt"
  ]
  node [
    id 9
    label "zakres"
  ]
  node [
    id 10
    label "zwie&#324;czenie"
  ]
  node [
    id 11
    label "koniec"
  ]
  node [
    id 12
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 13
    label "&#346;winica"
  ]
  node [
    id 14
    label "Wielka_Racza"
  ]
  node [
    id 15
    label "Che&#322;miec"
  ]
  node [
    id 16
    label "wierzcho&#322;"
  ]
  node [
    id 17
    label "wierzcho&#322;ek"
  ]
  node [
    id 18
    label "Radunia"
  ]
  node [
    id 19
    label "Barania_G&#243;ra"
  ]
  node [
    id 20
    label "Groniczki"
  ]
  node [
    id 21
    label "wierch"
  ]
  node [
    id 22
    label "konferencja"
  ]
  node [
    id 23
    label "Czupel"
  ]
  node [
    id 24
    label "&#347;ciana"
  ]
  node [
    id 25
    label "Jaworz"
  ]
  node [
    id 26
    label "Okr&#261;glica"
  ]
  node [
    id 27
    label "Walig&#243;ra"
  ]
  node [
    id 28
    label "bok"
  ]
  node [
    id 29
    label "Wielka_Sowa"
  ]
  node [
    id 30
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 31
    label "&#321;omnica"
  ]
  node [
    id 32
    label "wzniesienie"
  ]
  node [
    id 33
    label "Beskid"
  ]
  node [
    id 34
    label "fasada"
  ]
  node [
    id 35
    label "Wo&#322;ek"
  ]
  node [
    id 36
    label "summit"
  ]
  node [
    id 37
    label "Rysianka"
  ]
  node [
    id 38
    label "Mody&#324;"
  ]
  node [
    id 39
    label "poziom"
  ]
  node [
    id 40
    label "wzmo&#380;enie"
  ]
  node [
    id 41
    label "czas"
  ]
  node [
    id 42
    label "Obidowa"
  ]
  node [
    id 43
    label "Jaworzyna"
  ]
  node [
    id 44
    label "godzina_szczytu"
  ]
  node [
    id 45
    label "Turbacz"
  ]
  node [
    id 46
    label "Rudawiec"
  ]
  node [
    id 47
    label "g&#243;ra"
  ]
  node [
    id 48
    label "Ja&#322;owiec"
  ]
  node [
    id 49
    label "Wielki_Chocz"
  ]
  node [
    id 50
    label "Orlica"
  ]
  node [
    id 51
    label "Szrenica"
  ]
  node [
    id 52
    label "&#346;nie&#380;nik"
  ]
  node [
    id 53
    label "Cubryna"
  ]
  node [
    id 54
    label "Wielki_Bukowiec"
  ]
  node [
    id 55
    label "Magura"
  ]
  node [
    id 56
    label "korona"
  ]
  node [
    id 57
    label "Czarna_G&#243;ra"
  ]
  node [
    id 58
    label "Lubogoszcz"
  ]
  node [
    id 59
    label "sfera"
  ]
  node [
    id 60
    label "granica"
  ]
  node [
    id 61
    label "zbi&#243;r"
  ]
  node [
    id 62
    label "wielko&#347;&#263;"
  ]
  node [
    id 63
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 64
    label "podzakres"
  ]
  node [
    id 65
    label "dziedzina"
  ]
  node [
    id 66
    label "desygnat"
  ]
  node [
    id 67
    label "circle"
  ]
  node [
    id 68
    label "szybki"
  ]
  node [
    id 69
    label "letki"
  ]
  node [
    id 70
    label "umiej&#281;tny"
  ]
  node [
    id 71
    label "zdrowy"
  ]
  node [
    id 72
    label "dzia&#322;alny"
  ]
  node [
    id 73
    label "dobry"
  ]
  node [
    id 74
    label "sprawnie"
  ]
  node [
    id 75
    label "dobroczynny"
  ]
  node [
    id 76
    label "czw&#243;rka"
  ]
  node [
    id 77
    label "spokojny"
  ]
  node [
    id 78
    label "skuteczny"
  ]
  node [
    id 79
    label "&#347;mieszny"
  ]
  node [
    id 80
    label "mi&#322;y"
  ]
  node [
    id 81
    label "grzeczny"
  ]
  node [
    id 82
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 83
    label "powitanie"
  ]
  node [
    id 84
    label "dobrze"
  ]
  node [
    id 85
    label "ca&#322;y"
  ]
  node [
    id 86
    label "zwrot"
  ]
  node [
    id 87
    label "pomy&#347;lny"
  ]
  node [
    id 88
    label "moralny"
  ]
  node [
    id 89
    label "drogi"
  ]
  node [
    id 90
    label "pozytywny"
  ]
  node [
    id 91
    label "odpowiedni"
  ]
  node [
    id 92
    label "korzystny"
  ]
  node [
    id 93
    label "pos&#322;uszny"
  ]
  node [
    id 94
    label "zdrowo"
  ]
  node [
    id 95
    label "wyzdrowienie"
  ]
  node [
    id 96
    label "cz&#322;owiek"
  ]
  node [
    id 97
    label "silny"
  ]
  node [
    id 98
    label "uzdrowienie"
  ]
  node [
    id 99
    label "wyleczenie_si&#281;"
  ]
  node [
    id 100
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 101
    label "normalny"
  ]
  node [
    id 102
    label "rozs&#261;dny"
  ]
  node [
    id 103
    label "zdrowienie"
  ]
  node [
    id 104
    label "solidny"
  ]
  node [
    id 105
    label "uzdrawianie"
  ]
  node [
    id 106
    label "umiej&#281;tnie"
  ]
  node [
    id 107
    label "udany"
  ]
  node [
    id 108
    label "umny"
  ]
  node [
    id 109
    label "czynny"
  ]
  node [
    id 110
    label "lekki"
  ]
  node [
    id 111
    label "delikatny"
  ]
  node [
    id 112
    label "r&#261;czy"
  ]
  node [
    id 113
    label "&#322;atwy"
  ]
  node [
    id 114
    label "beztroski"
  ]
  node [
    id 115
    label "kompetentnie"
  ]
  node [
    id 116
    label "funkcjonalnie"
  ]
  node [
    id 117
    label "szybko"
  ]
  node [
    id 118
    label "udanie"
  ]
  node [
    id 119
    label "skutecznie"
  ]
  node [
    id 120
    label "intensywny"
  ]
  node [
    id 121
    label "prosty"
  ]
  node [
    id 122
    label "kr&#243;tki"
  ]
  node [
    id 123
    label "temperamentny"
  ]
  node [
    id 124
    label "bystrolotny"
  ]
  node [
    id 125
    label "dynamiczny"
  ]
  node [
    id 126
    label "bezpo&#347;redni"
  ]
  node [
    id 127
    label "energiczny"
  ]
  node [
    id 128
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 129
    label "zadzwoni&#263;"
  ]
  node [
    id 130
    label "provider"
  ]
  node [
    id 131
    label "infrastruktura"
  ]
  node [
    id 132
    label "numer"
  ]
  node [
    id 133
    label "po&#322;&#261;czenie"
  ]
  node [
    id 134
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 135
    label "phreaker"
  ]
  node [
    id 136
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 137
    label "mikrotelefon"
  ]
  node [
    id 138
    label "billing"
  ]
  node [
    id 139
    label "dzwoni&#263;"
  ]
  node [
    id 140
    label "instalacja"
  ]
  node [
    id 141
    label "kontakt"
  ]
  node [
    id 142
    label "coalescence"
  ]
  node [
    id 143
    label "wy&#347;wietlacz"
  ]
  node [
    id 144
    label "dzwonienie"
  ]
  node [
    id 145
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 146
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 147
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 148
    label "urz&#261;dzenie"
  ]
  node [
    id 149
    label "punkt"
  ]
  node [
    id 150
    label "turn"
  ]
  node [
    id 151
    label "liczba"
  ]
  node [
    id 152
    label "&#380;art"
  ]
  node [
    id 153
    label "zi&#243;&#322;ko"
  ]
  node [
    id 154
    label "publikacja"
  ]
  node [
    id 155
    label "manewr"
  ]
  node [
    id 156
    label "impression"
  ]
  node [
    id 157
    label "wyst&#281;p"
  ]
  node [
    id 158
    label "sztos"
  ]
  node [
    id 159
    label "oznaczenie"
  ]
  node [
    id 160
    label "hotel"
  ]
  node [
    id 161
    label "pok&#243;j"
  ]
  node [
    id 162
    label "czasopismo"
  ]
  node [
    id 163
    label "akt_p&#322;ciowy"
  ]
  node [
    id 164
    label "orygina&#322;"
  ]
  node [
    id 165
    label "facet"
  ]
  node [
    id 166
    label "przedmiot"
  ]
  node [
    id 167
    label "kom&#243;rka"
  ]
  node [
    id 168
    label "furnishing"
  ]
  node [
    id 169
    label "zabezpieczenie"
  ]
  node [
    id 170
    label "zrobienie"
  ]
  node [
    id 171
    label "wyrz&#261;dzenie"
  ]
  node [
    id 172
    label "zagospodarowanie"
  ]
  node [
    id 173
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 174
    label "ig&#322;a"
  ]
  node [
    id 175
    label "narz&#281;dzie"
  ]
  node [
    id 176
    label "wirnik"
  ]
  node [
    id 177
    label "aparatura"
  ]
  node [
    id 178
    label "system_energetyczny"
  ]
  node [
    id 179
    label "impulsator"
  ]
  node [
    id 180
    label "mechanizm"
  ]
  node [
    id 181
    label "sprz&#281;t"
  ]
  node [
    id 182
    label "czynno&#347;&#263;"
  ]
  node [
    id 183
    label "blokowanie"
  ]
  node [
    id 184
    label "set"
  ]
  node [
    id 185
    label "zablokowanie"
  ]
  node [
    id 186
    label "przygotowanie"
  ]
  node [
    id 187
    label "komora"
  ]
  node [
    id 188
    label "j&#281;zyk"
  ]
  node [
    id 189
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 190
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 191
    label "communication"
  ]
  node [
    id 192
    label "styk"
  ]
  node [
    id 193
    label "wydarzenie"
  ]
  node [
    id 194
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 195
    label "association"
  ]
  node [
    id 196
    label "&#322;&#261;cznik"
  ]
  node [
    id 197
    label "katalizator"
  ]
  node [
    id 198
    label "socket"
  ]
  node [
    id 199
    label "instalacja_elektryczna"
  ]
  node [
    id 200
    label "soczewka"
  ]
  node [
    id 201
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 202
    label "formacja_geologiczna"
  ]
  node [
    id 203
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 204
    label "linkage"
  ]
  node [
    id 205
    label "regulator"
  ]
  node [
    id 206
    label "z&#322;&#261;czenie"
  ]
  node [
    id 207
    label "zwi&#261;zek"
  ]
  node [
    id 208
    label "contact"
  ]
  node [
    id 209
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 210
    label "proces"
  ]
  node [
    id 211
    label "kompozycja"
  ]
  node [
    id 212
    label "uzbrajanie"
  ]
  node [
    id 213
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 214
    label "ekran"
  ]
  node [
    id 215
    label "handset"
  ]
  node [
    id 216
    label "dodzwanianie_si&#281;"
  ]
  node [
    id 217
    label "jingle"
  ]
  node [
    id 218
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 219
    label "wydzwanianie"
  ]
  node [
    id 220
    label "dzwonek"
  ]
  node [
    id 221
    label "naciskanie"
  ]
  node [
    id 222
    label "sound"
  ]
  node [
    id 223
    label "brzmienie"
  ]
  node [
    id 224
    label "wybijanie"
  ]
  node [
    id 225
    label "dryndanie"
  ]
  node [
    id 226
    label "dodzwonienie_si&#281;"
  ]
  node [
    id 227
    label "wydzwonienie"
  ]
  node [
    id 228
    label "call"
  ]
  node [
    id 229
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 230
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 231
    label "zabi&#263;"
  ]
  node [
    id 232
    label "zadrynda&#263;"
  ]
  node [
    id 233
    label "zabrzmie&#263;"
  ]
  node [
    id 234
    label "nacisn&#261;&#263;"
  ]
  node [
    id 235
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 236
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 237
    label "bi&#263;"
  ]
  node [
    id 238
    label "brzmie&#263;"
  ]
  node [
    id 239
    label "drynda&#263;"
  ]
  node [
    id 240
    label "brz&#281;cze&#263;"
  ]
  node [
    id 241
    label "zjednoczy&#263;"
  ]
  node [
    id 242
    label "stworzy&#263;"
  ]
  node [
    id 243
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 244
    label "incorporate"
  ]
  node [
    id 245
    label "zrobi&#263;"
  ]
  node [
    id 246
    label "connect"
  ]
  node [
    id 247
    label "spowodowa&#263;"
  ]
  node [
    id 248
    label "relate"
  ]
  node [
    id 249
    label "paj&#281;czarz"
  ]
  node [
    id 250
    label "z&#322;odziej"
  ]
  node [
    id 251
    label "severance"
  ]
  node [
    id 252
    label "przerwanie"
  ]
  node [
    id 253
    label "od&#322;&#261;czenie"
  ]
  node [
    id 254
    label "oddzielenie"
  ]
  node [
    id 255
    label "prze&#322;&#261;czenie"
  ]
  node [
    id 256
    label "rozdzielenie"
  ]
  node [
    id 257
    label "dissociation"
  ]
  node [
    id 258
    label "spis"
  ]
  node [
    id 259
    label "biling"
  ]
  node [
    id 260
    label "rozdzieli&#263;"
  ]
  node [
    id 261
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 262
    label "detach"
  ]
  node [
    id 263
    label "oddzieli&#263;"
  ]
  node [
    id 264
    label "abstract"
  ]
  node [
    id 265
    label "amputate"
  ]
  node [
    id 266
    label "przerwa&#263;"
  ]
  node [
    id 267
    label "rozdzielanie"
  ]
  node [
    id 268
    label "separation"
  ]
  node [
    id 269
    label "oddzielanie"
  ]
  node [
    id 270
    label "rozsuwanie"
  ]
  node [
    id 271
    label "od&#322;&#261;czanie"
  ]
  node [
    id 272
    label "przerywanie"
  ]
  node [
    id 273
    label "prze&#322;&#261;czanie"
  ]
  node [
    id 274
    label "stworzenie"
  ]
  node [
    id 275
    label "zespolenie"
  ]
  node [
    id 276
    label "dressing"
  ]
  node [
    id 277
    label "pomy&#347;lenie"
  ]
  node [
    id 278
    label "zjednoczenie"
  ]
  node [
    id 279
    label "spowodowanie"
  ]
  node [
    id 280
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 281
    label "element"
  ]
  node [
    id 282
    label "alliance"
  ]
  node [
    id 283
    label "joining"
  ]
  node [
    id 284
    label "umo&#380;liwienie"
  ]
  node [
    id 285
    label "mention"
  ]
  node [
    id 286
    label "zwi&#261;zany"
  ]
  node [
    id 287
    label "port"
  ]
  node [
    id 288
    label "komunikacja"
  ]
  node [
    id 289
    label "rzucenie"
  ]
  node [
    id 290
    label "zgrzeina"
  ]
  node [
    id 291
    label "zestawienie"
  ]
  node [
    id 292
    label "cover"
  ]
  node [
    id 293
    label "gulf"
  ]
  node [
    id 294
    label "part"
  ]
  node [
    id 295
    label "rozdziela&#263;"
  ]
  node [
    id 296
    label "przerywa&#263;"
  ]
  node [
    id 297
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 298
    label "oddziela&#263;"
  ]
  node [
    id 299
    label "internet"
  ]
  node [
    id 300
    label "dostawca"
  ]
  node [
    id 301
    label "telefonia"
  ]
  node [
    id 302
    label "zaplecze"
  ]
  node [
    id 303
    label "radiofonia"
  ]
  node [
    id 304
    label "trasa"
  ]
  node [
    id 305
    label "wychyli&#263;"
  ]
  node [
    id 306
    label "wskaza&#263;"
  ]
  node [
    id 307
    label "zbudowa&#263;"
  ]
  node [
    id 308
    label "wynie&#347;&#263;"
  ]
  node [
    id 309
    label "przedstawi&#263;"
  ]
  node [
    id 310
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 311
    label "pies_my&#347;liwski"
  ]
  node [
    id 312
    label "wyj&#261;&#263;"
  ]
  node [
    id 313
    label "zaproponowa&#263;"
  ]
  node [
    id 314
    label "wyrazi&#263;"
  ]
  node [
    id 315
    label "wyeksponowa&#263;"
  ]
  node [
    id 316
    label "wypisa&#263;"
  ]
  node [
    id 317
    label "wysun&#261;&#263;"
  ]
  node [
    id 318
    label "indicate"
  ]
  node [
    id 319
    label "ukaza&#263;"
  ]
  node [
    id 320
    label "przedstawienie"
  ]
  node [
    id 321
    label "pokaza&#263;"
  ]
  node [
    id 322
    label "poda&#263;"
  ]
  node [
    id 323
    label "zapozna&#263;"
  ]
  node [
    id 324
    label "express"
  ]
  node [
    id 325
    label "represent"
  ]
  node [
    id 326
    label "zademonstrowa&#263;"
  ]
  node [
    id 327
    label "typify"
  ]
  node [
    id 328
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 329
    label "opisa&#263;"
  ]
  node [
    id 330
    label "nasi&#261;kn&#261;&#263;"
  ]
  node [
    id 331
    label "kwota"
  ]
  node [
    id 332
    label "odsun&#261;&#263;"
  ]
  node [
    id 333
    label "zanie&#347;&#263;"
  ]
  node [
    id 334
    label "rozpowszechni&#263;"
  ]
  node [
    id 335
    label "ujawni&#263;"
  ]
  node [
    id 336
    label "otrzyma&#263;"
  ]
  node [
    id 337
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 338
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 339
    label "podnie&#347;&#263;"
  ]
  node [
    id 340
    label "progress"
  ]
  node [
    id 341
    label "ukra&#347;&#263;"
  ]
  node [
    id 342
    label "raise"
  ]
  node [
    id 343
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 344
    label "wydzieli&#263;"
  ]
  node [
    id 345
    label "distill"
  ]
  node [
    id 346
    label "wy&#322;&#261;czy&#263;"
  ]
  node [
    id 347
    label "remove"
  ]
  node [
    id 348
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 349
    label "testify"
  ]
  node [
    id 350
    label "zakomunikowa&#263;"
  ]
  node [
    id 351
    label "oznaczy&#263;"
  ]
  node [
    id 352
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 353
    label "vent"
  ]
  node [
    id 354
    label "try"
  ]
  node [
    id 355
    label "podkre&#347;li&#263;"
  ]
  node [
    id 356
    label "advance"
  ]
  node [
    id 357
    label "przesun&#261;&#263;"
  ]
  node [
    id 358
    label "pozwoli&#263;"
  ]
  node [
    id 359
    label "digest"
  ]
  node [
    id 360
    label "uzna&#263;"
  ]
  node [
    id 361
    label "pu&#347;ci&#263;"
  ]
  node [
    id 362
    label "receive"
  ]
  node [
    id 363
    label "zach&#281;ci&#263;"
  ]
  node [
    id 364
    label "volunteer"
  ]
  node [
    id 365
    label "poinformowa&#263;"
  ]
  node [
    id 366
    label "kandydatura"
  ]
  node [
    id 367
    label "announce"
  ]
  node [
    id 368
    label "o&#322;&#243;wek"
  ]
  node [
    id 369
    label "wykluczy&#263;"
  ]
  node [
    id 370
    label "make_out"
  ]
  node [
    id 371
    label "d&#322;ugopis"
  ]
  node [
    id 372
    label "pi&#243;ro"
  ]
  node [
    id 373
    label "zu&#380;y&#263;"
  ]
  node [
    id 374
    label "wymieni&#263;"
  ]
  node [
    id 375
    label "napisa&#263;"
  ]
  node [
    id 376
    label "discharge"
  ]
  node [
    id 377
    label "budowla"
  ]
  node [
    id 378
    label "establish"
  ]
  node [
    id 379
    label "evolve"
  ]
  node [
    id 380
    label "zaplanowa&#263;"
  ]
  node [
    id 381
    label "wytworzy&#263;"
  ]
  node [
    id 382
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 383
    label "point"
  ]
  node [
    id 384
    label "picture"
  ]
  node [
    id 385
    label "aim"
  ]
  node [
    id 386
    label "wybra&#263;"
  ]
  node [
    id 387
    label "gem"
  ]
  node [
    id 388
    label "runda"
  ]
  node [
    id 389
    label "muzyka"
  ]
  node [
    id 390
    label "zestaw"
  ]
  node [
    id 391
    label "&#322;ykn&#261;&#263;"
  ]
  node [
    id 392
    label "tip_off"
  ]
  node [
    id 393
    label "wyci&#261;&#263;"
  ]
  node [
    id 394
    label "zmieni&#263;"
  ]
  node [
    id 395
    label "crush"
  ]
  node [
    id 396
    label "jednostka_monetarna"
  ]
  node [
    id 397
    label "wspania&#322;y"
  ]
  node [
    id 398
    label "metaliczny"
  ]
  node [
    id 399
    label "Polska"
  ]
  node [
    id 400
    label "szlachetny"
  ]
  node [
    id 401
    label "kochany"
  ]
  node [
    id 402
    label "doskona&#322;y"
  ]
  node [
    id 403
    label "grosz"
  ]
  node [
    id 404
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 405
    label "poz&#322;ocenie"
  ]
  node [
    id 406
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 407
    label "utytu&#322;owany"
  ]
  node [
    id 408
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 409
    label "z&#322;ocenie"
  ]
  node [
    id 410
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 411
    label "prominentny"
  ]
  node [
    id 412
    label "znany"
  ]
  node [
    id 413
    label "wybitny"
  ]
  node [
    id 414
    label "naj"
  ]
  node [
    id 415
    label "&#347;wietny"
  ]
  node [
    id 416
    label "pe&#322;ny"
  ]
  node [
    id 417
    label "doskonale"
  ]
  node [
    id 418
    label "szlachetnie"
  ]
  node [
    id 419
    label "uczciwy"
  ]
  node [
    id 420
    label "zacny"
  ]
  node [
    id 421
    label "harmonijny"
  ]
  node [
    id 422
    label "gatunkowy"
  ]
  node [
    id 423
    label "pi&#281;kny"
  ]
  node [
    id 424
    label "typowy"
  ]
  node [
    id 425
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 426
    label "metaloplastyczny"
  ]
  node [
    id 427
    label "metalicznie"
  ]
  node [
    id 428
    label "kochanek"
  ]
  node [
    id 429
    label "wybranek"
  ]
  node [
    id 430
    label "umi&#322;owany"
  ]
  node [
    id 431
    label "kochanie"
  ]
  node [
    id 432
    label "wspaniale"
  ]
  node [
    id 433
    label "&#347;wietnie"
  ]
  node [
    id 434
    label "spania&#322;y"
  ]
  node [
    id 435
    label "och&#281;do&#380;ny"
  ]
  node [
    id 436
    label "warto&#347;ciowy"
  ]
  node [
    id 437
    label "zajebisty"
  ]
  node [
    id 438
    label "bogato"
  ]
  node [
    id 439
    label "typ_mongoloidalny"
  ]
  node [
    id 440
    label "kolorowy"
  ]
  node [
    id 441
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 442
    label "ciep&#322;y"
  ]
  node [
    id 443
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 444
    label "jasny"
  ]
  node [
    id 445
    label "groszak"
  ]
  node [
    id 446
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 447
    label "szyling_austryjacki"
  ]
  node [
    id 448
    label "moneta"
  ]
  node [
    id 449
    label "Mazowsze"
  ]
  node [
    id 450
    label "Pa&#322;uki"
  ]
  node [
    id 451
    label "Pomorze_Zachodnie"
  ]
  node [
    id 452
    label "Powi&#347;le"
  ]
  node [
    id 453
    label "Wolin"
  ]
  node [
    id 454
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 455
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 456
    label "So&#322;a"
  ]
  node [
    id 457
    label "Unia_Europejska"
  ]
  node [
    id 458
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 459
    label "Opolskie"
  ]
  node [
    id 460
    label "Suwalszczyzna"
  ]
  node [
    id 461
    label "Krajna"
  ]
  node [
    id 462
    label "barwy_polskie"
  ]
  node [
    id 463
    label "Nadbu&#380;e"
  ]
  node [
    id 464
    label "Podlasie"
  ]
  node [
    id 465
    label "Izera"
  ]
  node [
    id 466
    label "Ma&#322;opolska"
  ]
  node [
    id 467
    label "Warmia"
  ]
  node [
    id 468
    label "Mazury"
  ]
  node [
    id 469
    label "NATO"
  ]
  node [
    id 470
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 471
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 472
    label "Lubelszczyzna"
  ]
  node [
    id 473
    label "Kaczawa"
  ]
  node [
    id 474
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 475
    label "Kielecczyzna"
  ]
  node [
    id 476
    label "Lubuskie"
  ]
  node [
    id 477
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 478
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 479
    label "&#321;&#243;dzkie"
  ]
  node [
    id 480
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 481
    label "Kujawy"
  ]
  node [
    id 482
    label "Podkarpacie"
  ]
  node [
    id 483
    label "Wielkopolska"
  ]
  node [
    id 484
    label "Wis&#322;a"
  ]
  node [
    id 485
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 486
    label "Bory_Tucholskie"
  ]
  node [
    id 487
    label "platerowanie"
  ]
  node [
    id 488
    label "z&#322;ocisty"
  ]
  node [
    id 489
    label "barwienie"
  ]
  node [
    id 490
    label "gilt"
  ]
  node [
    id 491
    label "plating"
  ]
  node [
    id 492
    label "zdobienie"
  ]
  node [
    id 493
    label "club"
  ]
  node [
    id 494
    label "powleczenie"
  ]
  node [
    id 495
    label "zabarwienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
]
