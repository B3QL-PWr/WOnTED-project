graph [
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wyzwanie"
    origin "text"
  ]
  node [
    id 2
    label "zadanie"
    origin "text"
  ]
  node [
    id 3
    label "nagroda"
    origin "text"
  ]
  node [
    id 4
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 5
    label "fundacja"
    origin "text"
  ]
  node [
    id 6
    label "nowa"
    origin "text"
  ]
  node [
    id 7
    label "media"
    origin "text"
  ]
  node [
    id 8
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "nowy"
    origin "text"
  ]
  node [
    id 10
    label "konkurs"
    origin "text"
  ]
  node [
    id 11
    label "dla"
    origin "text"
  ]
  node [
    id 12
    label "gimnazjum"
    origin "text"
  ]
  node [
    id 13
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 14
    label "ponadgimnazjalnych"
    origin "text"
  ]
  node [
    id 15
    label "pire"
    origin "text"
  ]
  node [
    id 16
    label "informacyjny"
    origin "text"
  ]
  node [
    id 17
    label "unia"
    origin "text"
  ]
  node [
    id 18
    label "europejski"
    origin "text"
  ]
  node [
    id 19
    label "pozna&#263;"
    origin "text"
  ]
  node [
    id 20
    label "cztery"
    origin "text"
  ]
  node [
    id 21
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "wydanie"
    origin "text"
  ]
  node [
    id 23
    label "qmama"
    origin "text"
  ]
  node [
    id 24
    label "wygranie"
    origin "text"
  ]
  node [
    id 25
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 26
    label "inny"
    origin "text"
  ]
  node [
    id 27
    label "laptop"
    origin "text"
  ]
  node [
    id 28
    label "rzutnik"
    origin "text"
  ]
  node [
    id 29
    label "multimedialny"
    origin "text"
  ]
  node [
    id 30
    label "warto"
    origin "text"
  ]
  node [
    id 31
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 32
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 33
    label "zajrze&#263;"
    origin "text"
  ]
  node [
    id 34
    label "strona"
    origin "text"
  ]
  node [
    id 35
    label "zg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 37
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 38
    label "projekt"
    origin "text"
  ]
  node [
    id 39
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 40
    label "mie&#263;_miejsce"
  ]
  node [
    id 41
    label "equal"
  ]
  node [
    id 42
    label "trwa&#263;"
  ]
  node [
    id 43
    label "chodzi&#263;"
  ]
  node [
    id 44
    label "si&#281;ga&#263;"
  ]
  node [
    id 45
    label "stan"
  ]
  node [
    id 46
    label "obecno&#347;&#263;"
  ]
  node [
    id 47
    label "stand"
  ]
  node [
    id 48
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "uczestniczy&#263;"
  ]
  node [
    id 50
    label "participate"
  ]
  node [
    id 51
    label "robi&#263;"
  ]
  node [
    id 52
    label "istnie&#263;"
  ]
  node [
    id 53
    label "pozostawa&#263;"
  ]
  node [
    id 54
    label "zostawa&#263;"
  ]
  node [
    id 55
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 56
    label "adhere"
  ]
  node [
    id 57
    label "compass"
  ]
  node [
    id 58
    label "korzysta&#263;"
  ]
  node [
    id 59
    label "appreciation"
  ]
  node [
    id 60
    label "osi&#261;ga&#263;"
  ]
  node [
    id 61
    label "dociera&#263;"
  ]
  node [
    id 62
    label "get"
  ]
  node [
    id 63
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 64
    label "mierzy&#263;"
  ]
  node [
    id 65
    label "u&#380;ywa&#263;"
  ]
  node [
    id 66
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 67
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 68
    label "exsert"
  ]
  node [
    id 69
    label "being"
  ]
  node [
    id 70
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 71
    label "cecha"
  ]
  node [
    id 72
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 73
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 74
    label "p&#322;ywa&#263;"
  ]
  node [
    id 75
    label "run"
  ]
  node [
    id 76
    label "bangla&#263;"
  ]
  node [
    id 77
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 78
    label "przebiega&#263;"
  ]
  node [
    id 79
    label "wk&#322;ada&#263;"
  ]
  node [
    id 80
    label "proceed"
  ]
  node [
    id 81
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 82
    label "carry"
  ]
  node [
    id 83
    label "bywa&#263;"
  ]
  node [
    id 84
    label "dziama&#263;"
  ]
  node [
    id 85
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 86
    label "stara&#263;_si&#281;"
  ]
  node [
    id 87
    label "para"
  ]
  node [
    id 88
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 89
    label "str&#243;j"
  ]
  node [
    id 90
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 91
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 92
    label "krok"
  ]
  node [
    id 93
    label "tryb"
  ]
  node [
    id 94
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 95
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 96
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 97
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 98
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 99
    label "continue"
  ]
  node [
    id 100
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 101
    label "Ohio"
  ]
  node [
    id 102
    label "wci&#281;cie"
  ]
  node [
    id 103
    label "Nowy_York"
  ]
  node [
    id 104
    label "warstwa"
  ]
  node [
    id 105
    label "samopoczucie"
  ]
  node [
    id 106
    label "Illinois"
  ]
  node [
    id 107
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 108
    label "state"
  ]
  node [
    id 109
    label "Jukatan"
  ]
  node [
    id 110
    label "Kalifornia"
  ]
  node [
    id 111
    label "Wirginia"
  ]
  node [
    id 112
    label "wektor"
  ]
  node [
    id 113
    label "Teksas"
  ]
  node [
    id 114
    label "Goa"
  ]
  node [
    id 115
    label "Waszyngton"
  ]
  node [
    id 116
    label "miejsce"
  ]
  node [
    id 117
    label "Massachusetts"
  ]
  node [
    id 118
    label "Alaska"
  ]
  node [
    id 119
    label "Arakan"
  ]
  node [
    id 120
    label "Hawaje"
  ]
  node [
    id 121
    label "Maryland"
  ]
  node [
    id 122
    label "punkt"
  ]
  node [
    id 123
    label "Michigan"
  ]
  node [
    id 124
    label "Arizona"
  ]
  node [
    id 125
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 126
    label "Georgia"
  ]
  node [
    id 127
    label "poziom"
  ]
  node [
    id 128
    label "Pensylwania"
  ]
  node [
    id 129
    label "shape"
  ]
  node [
    id 130
    label "Luizjana"
  ]
  node [
    id 131
    label "Nowy_Meksyk"
  ]
  node [
    id 132
    label "Alabama"
  ]
  node [
    id 133
    label "ilo&#347;&#263;"
  ]
  node [
    id 134
    label "Kansas"
  ]
  node [
    id 135
    label "Oregon"
  ]
  node [
    id 136
    label "Floryda"
  ]
  node [
    id 137
    label "Oklahoma"
  ]
  node [
    id 138
    label "jednostka_administracyjna"
  ]
  node [
    id 139
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 140
    label "rzuci&#263;"
  ]
  node [
    id 141
    label "obra&#380;enie"
  ]
  node [
    id 142
    label "wypowied&#378;"
  ]
  node [
    id 143
    label "zaproponowanie"
  ]
  node [
    id 144
    label "gauntlet"
  ]
  node [
    id 145
    label "pojedynek"
  ]
  node [
    id 146
    label "pojedynkowanie_si&#281;"
  ]
  node [
    id 147
    label "rzucenie"
  ]
  node [
    id 148
    label "pr&#243;ba"
  ]
  node [
    id 149
    label "rzuca&#263;"
  ]
  node [
    id 150
    label "rzucanie"
  ]
  node [
    id 151
    label "challenge"
  ]
  node [
    id 152
    label "pos&#322;uchanie"
  ]
  node [
    id 153
    label "s&#261;d"
  ]
  node [
    id 154
    label "sparafrazowanie"
  ]
  node [
    id 155
    label "strawestowa&#263;"
  ]
  node [
    id 156
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 157
    label "trawestowa&#263;"
  ]
  node [
    id 158
    label "sparafrazowa&#263;"
  ]
  node [
    id 159
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 160
    label "sformu&#322;owanie"
  ]
  node [
    id 161
    label "parafrazowanie"
  ]
  node [
    id 162
    label "ozdobnik"
  ]
  node [
    id 163
    label "delimitacja"
  ]
  node [
    id 164
    label "parafrazowa&#263;"
  ]
  node [
    id 165
    label "stylizacja"
  ]
  node [
    id 166
    label "komunikat"
  ]
  node [
    id 167
    label "trawestowanie"
  ]
  node [
    id 168
    label "strawestowanie"
  ]
  node [
    id 169
    label "rezultat"
  ]
  node [
    id 170
    label "do&#347;wiadczenie"
  ]
  node [
    id 171
    label "spotkanie"
  ]
  node [
    id 172
    label "pobiera&#263;"
  ]
  node [
    id 173
    label "metal_szlachetny"
  ]
  node [
    id 174
    label "pobranie"
  ]
  node [
    id 175
    label "zbi&#243;r"
  ]
  node [
    id 176
    label "usi&#322;owanie"
  ]
  node [
    id 177
    label "pobra&#263;"
  ]
  node [
    id 178
    label "pobieranie"
  ]
  node [
    id 179
    label "znak"
  ]
  node [
    id 180
    label "effort"
  ]
  node [
    id 181
    label "analiza_chemiczna"
  ]
  node [
    id 182
    label "item"
  ]
  node [
    id 183
    label "czynno&#347;&#263;"
  ]
  node [
    id 184
    label "sytuacja"
  ]
  node [
    id 185
    label "probiernictwo"
  ]
  node [
    id 186
    label "test"
  ]
  node [
    id 187
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 188
    label "zaj&#281;cie"
  ]
  node [
    id 189
    label "yield"
  ]
  node [
    id 190
    label "zaszkodzenie"
  ]
  node [
    id 191
    label "za&#322;o&#380;enie"
  ]
  node [
    id 192
    label "duty"
  ]
  node [
    id 193
    label "powierzanie"
  ]
  node [
    id 194
    label "work"
  ]
  node [
    id 195
    label "problem"
  ]
  node [
    id 196
    label "przepisanie"
  ]
  node [
    id 197
    label "nakarmienie"
  ]
  node [
    id 198
    label "przepisa&#263;"
  ]
  node [
    id 199
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 200
    label "zobowi&#261;zanie"
  ]
  node [
    id 201
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 202
    label "announcement"
  ]
  node [
    id 203
    label "zach&#281;cenie"
  ]
  node [
    id 204
    label "poinformowanie"
  ]
  node [
    id 205
    label "kandydatura"
  ]
  node [
    id 206
    label "obra&#380;anie"
  ]
  node [
    id 207
    label "zachowanie_si&#281;"
  ]
  node [
    id 208
    label "injury"
  ]
  node [
    id 209
    label "lesion"
  ]
  node [
    id 210
    label "uszkodzenie"
  ]
  node [
    id 211
    label "naruszenie"
  ]
  node [
    id 212
    label "powodowanie"
  ]
  node [
    id 213
    label "przestawanie"
  ]
  node [
    id 214
    label "poruszanie"
  ]
  node [
    id 215
    label "wrzucanie"
  ]
  node [
    id 216
    label "przerzucanie"
  ]
  node [
    id 217
    label "odchodzenie"
  ]
  node [
    id 218
    label "most"
  ]
  node [
    id 219
    label "konstruowanie"
  ]
  node [
    id 220
    label "chow"
  ]
  node [
    id 221
    label "przewracanie"
  ]
  node [
    id 222
    label "odrzucenie"
  ]
  node [
    id 223
    label "przemieszczanie"
  ]
  node [
    id 224
    label "m&#243;wienie"
  ]
  node [
    id 225
    label "opuszczanie"
  ]
  node [
    id 226
    label "odrzucanie"
  ]
  node [
    id 227
    label "wywo&#322;ywanie"
  ]
  node [
    id 228
    label "trafianie"
  ]
  node [
    id 229
    label "&#347;wiat&#322;o"
  ]
  node [
    id 230
    label "rezygnowanie"
  ]
  node [
    id 231
    label "decydowanie"
  ]
  node [
    id 232
    label "podejrzenie"
  ]
  node [
    id 233
    label "zamachiwanie_si&#281;"
  ]
  node [
    id 234
    label "ruszanie"
  ]
  node [
    id 235
    label "czar"
  ]
  node [
    id 236
    label "grzmocenie"
  ]
  node [
    id 237
    label "wyposa&#380;anie"
  ]
  node [
    id 238
    label "oddzia&#322;ywanie"
  ]
  node [
    id 239
    label "narzucanie"
  ]
  node [
    id 240
    label "cie&#324;"
  ]
  node [
    id 241
    label "porzucanie"
  ]
  node [
    id 242
    label "towar"
  ]
  node [
    id 243
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 244
    label "konwulsja"
  ]
  node [
    id 245
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 246
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 247
    label "ruszy&#263;"
  ]
  node [
    id 248
    label "powiedzie&#263;"
  ]
  node [
    id 249
    label "majdn&#261;&#263;"
  ]
  node [
    id 250
    label "poruszy&#263;"
  ]
  node [
    id 251
    label "da&#263;"
  ]
  node [
    id 252
    label "peddle"
  ]
  node [
    id 253
    label "rush"
  ]
  node [
    id 254
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 255
    label "zmieni&#263;"
  ]
  node [
    id 256
    label "bewilder"
  ]
  node [
    id 257
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 258
    label "przeznaczenie"
  ]
  node [
    id 259
    label "skonstruowa&#263;"
  ]
  node [
    id 260
    label "sygn&#261;&#263;"
  ]
  node [
    id 261
    label "spowodowa&#263;"
  ]
  node [
    id 262
    label "wywo&#322;a&#263;"
  ]
  node [
    id 263
    label "frame"
  ]
  node [
    id 264
    label "project"
  ]
  node [
    id 265
    label "odej&#347;&#263;"
  ]
  node [
    id 266
    label "zdecydowa&#263;"
  ]
  node [
    id 267
    label "opu&#347;ci&#263;"
  ]
  node [
    id 268
    label "atak"
  ]
  node [
    id 269
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 270
    label "ruszenie"
  ]
  node [
    id 271
    label "pierdolni&#281;cie"
  ]
  node [
    id 272
    label "poruszenie"
  ]
  node [
    id 273
    label "opuszczenie"
  ]
  node [
    id 274
    label "wywo&#322;anie"
  ]
  node [
    id 275
    label "odej&#347;cie"
  ]
  node [
    id 276
    label "przewr&#243;cenie"
  ]
  node [
    id 277
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 278
    label "skonstruowanie"
  ]
  node [
    id 279
    label "spowodowanie"
  ]
  node [
    id 280
    label "grzmotni&#281;cie"
  ]
  node [
    id 281
    label "zdecydowanie"
  ]
  node [
    id 282
    label "przemieszczenie"
  ]
  node [
    id 283
    label "wyposa&#380;enie"
  ]
  node [
    id 284
    label "shy"
  ]
  node [
    id 285
    label "oddzia&#322;anie"
  ]
  node [
    id 286
    label "zrezygnowanie"
  ]
  node [
    id 287
    label "porzucenie"
  ]
  node [
    id 288
    label "powiedzenie"
  ]
  node [
    id 289
    label "opuszcza&#263;"
  ]
  node [
    id 290
    label "porusza&#263;"
  ]
  node [
    id 291
    label "grzmoci&#263;"
  ]
  node [
    id 292
    label "konstruowa&#263;"
  ]
  node [
    id 293
    label "spring"
  ]
  node [
    id 294
    label "odchodzi&#263;"
  ]
  node [
    id 295
    label "unwrap"
  ]
  node [
    id 296
    label "rusza&#263;"
  ]
  node [
    id 297
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 298
    label "przestawa&#263;"
  ]
  node [
    id 299
    label "przemieszcza&#263;"
  ]
  node [
    id 300
    label "flip"
  ]
  node [
    id 301
    label "bequeath"
  ]
  node [
    id 302
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 303
    label "przewraca&#263;"
  ]
  node [
    id 304
    label "m&#243;wi&#263;"
  ]
  node [
    id 305
    label "zmienia&#263;"
  ]
  node [
    id 306
    label "syga&#263;"
  ]
  node [
    id 307
    label "tug"
  ]
  node [
    id 308
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 309
    label "engagement"
  ]
  node [
    id 310
    label "walka"
  ]
  node [
    id 311
    label "wyzwa&#263;"
  ]
  node [
    id 312
    label "odyniec"
  ]
  node [
    id 313
    label "wyzywa&#263;"
  ]
  node [
    id 314
    label "sekundant"
  ]
  node [
    id 315
    label "competitiveness"
  ]
  node [
    id 316
    label "sp&#243;r"
  ]
  node [
    id 317
    label "bout"
  ]
  node [
    id 318
    label "turniej"
  ]
  node [
    id 319
    label "wyzywanie"
  ]
  node [
    id 320
    label "kontrola"
  ]
  node [
    id 321
    label "activity"
  ]
  node [
    id 322
    label "bezproblemowy"
  ]
  node [
    id 323
    label "wydarzenie"
  ]
  node [
    id 324
    label "danie"
  ]
  node [
    id 325
    label "feed"
  ]
  node [
    id 326
    label "zaspokojenie"
  ]
  node [
    id 327
    label "podwini&#281;cie"
  ]
  node [
    id 328
    label "zap&#322;acenie"
  ]
  node [
    id 329
    label "przyodzianie"
  ]
  node [
    id 330
    label "budowla"
  ]
  node [
    id 331
    label "pokrycie"
  ]
  node [
    id 332
    label "rozebranie"
  ]
  node [
    id 333
    label "zak&#322;adka"
  ]
  node [
    id 334
    label "struktura"
  ]
  node [
    id 335
    label "poubieranie"
  ]
  node [
    id 336
    label "infliction"
  ]
  node [
    id 337
    label "pozak&#322;adanie"
  ]
  node [
    id 338
    label "program"
  ]
  node [
    id 339
    label "przebranie"
  ]
  node [
    id 340
    label "przywdzianie"
  ]
  node [
    id 341
    label "obleczenie_si&#281;"
  ]
  node [
    id 342
    label "utworzenie"
  ]
  node [
    id 343
    label "twierdzenie"
  ]
  node [
    id 344
    label "obleczenie"
  ]
  node [
    id 345
    label "umieszczenie"
  ]
  node [
    id 346
    label "przygotowywanie"
  ]
  node [
    id 347
    label "przymierzenie"
  ]
  node [
    id 348
    label "wyko&#324;czenie"
  ]
  node [
    id 349
    label "point"
  ]
  node [
    id 350
    label "przygotowanie"
  ]
  node [
    id 351
    label "proposition"
  ]
  node [
    id 352
    label "przewidzenie"
  ]
  node [
    id 353
    label "zrobienie"
  ]
  node [
    id 354
    label "stosunek_prawny"
  ]
  node [
    id 355
    label "oblig"
  ]
  node [
    id 356
    label "uregulowa&#263;"
  ]
  node [
    id 357
    label "occupation"
  ]
  node [
    id 358
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 359
    label "zapowied&#378;"
  ]
  node [
    id 360
    label "obowi&#261;zek"
  ]
  node [
    id 361
    label "statement"
  ]
  node [
    id 362
    label "zapewnienie"
  ]
  node [
    id 363
    label "egzemplarz"
  ]
  node [
    id 364
    label "series"
  ]
  node [
    id 365
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 366
    label "uprawianie"
  ]
  node [
    id 367
    label "praca_rolnicza"
  ]
  node [
    id 368
    label "collection"
  ]
  node [
    id 369
    label "dane"
  ]
  node [
    id 370
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 371
    label "pakiet_klimatyczny"
  ]
  node [
    id 372
    label "poj&#281;cie"
  ]
  node [
    id 373
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 374
    label "sum"
  ]
  node [
    id 375
    label "gathering"
  ]
  node [
    id 376
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 377
    label "album"
  ]
  node [
    id 378
    label "sprawa"
  ]
  node [
    id 379
    label "subiekcja"
  ]
  node [
    id 380
    label "problemat"
  ]
  node [
    id 381
    label "jajko_Kolumba"
  ]
  node [
    id 382
    label "obstruction"
  ]
  node [
    id 383
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 384
    label "problematyka"
  ]
  node [
    id 385
    label "trudno&#347;&#263;"
  ]
  node [
    id 386
    label "pierepa&#322;ka"
  ]
  node [
    id 387
    label "ambaras"
  ]
  node [
    id 388
    label "damage"
  ]
  node [
    id 389
    label "podniesienie"
  ]
  node [
    id 390
    label "zniesienie"
  ]
  node [
    id 391
    label "ud&#378;wigni&#281;cie"
  ]
  node [
    id 392
    label "ulepszenie"
  ]
  node [
    id 393
    label "heave"
  ]
  node [
    id 394
    label "raise"
  ]
  node [
    id 395
    label "odbudowanie"
  ]
  node [
    id 396
    label "oddawanie"
  ]
  node [
    id 397
    label "stanowisko"
  ]
  node [
    id 398
    label "zlecanie"
  ]
  node [
    id 399
    label "ufanie"
  ]
  node [
    id 400
    label "wyznawanie"
  ]
  node [
    id 401
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 402
    label "care"
  ]
  node [
    id 403
    label "zdarzenie_si&#281;"
  ]
  node [
    id 404
    label "benedykty&#324;ski"
  ]
  node [
    id 405
    label "career"
  ]
  node [
    id 406
    label "anektowanie"
  ]
  node [
    id 407
    label "dostarczenie"
  ]
  node [
    id 408
    label "u&#380;ycie"
  ]
  node [
    id 409
    label "klasyfikacja"
  ]
  node [
    id 410
    label "wzi&#281;cie"
  ]
  node [
    id 411
    label "wzbudzenie"
  ]
  node [
    id 412
    label "tynkarski"
  ]
  node [
    id 413
    label "wype&#322;nienie"
  ]
  node [
    id 414
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 415
    label "zapanowanie"
  ]
  node [
    id 416
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 417
    label "zmiana"
  ]
  node [
    id 418
    label "czynnik_produkcji"
  ]
  node [
    id 419
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 420
    label "pozajmowanie"
  ]
  node [
    id 421
    label "ulokowanie_si&#281;"
  ]
  node [
    id 422
    label "usytuowanie_si&#281;"
  ]
  node [
    id 423
    label "obj&#281;cie"
  ]
  node [
    id 424
    label "zabranie"
  ]
  node [
    id 425
    label "przekazanie"
  ]
  node [
    id 426
    label "skopiowanie"
  ]
  node [
    id 427
    label "arrangement"
  ]
  node [
    id 428
    label "przeniesienie"
  ]
  node [
    id 429
    label "testament"
  ]
  node [
    id 430
    label "lekarstwo"
  ]
  node [
    id 431
    label "answer"
  ]
  node [
    id 432
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 433
    label "transcription"
  ]
  node [
    id 434
    label "klasa"
  ]
  node [
    id 435
    label "zalecenie"
  ]
  node [
    id 436
    label "przekaza&#263;"
  ]
  node [
    id 437
    label "supply"
  ]
  node [
    id 438
    label "zaleci&#263;"
  ]
  node [
    id 439
    label "rewrite"
  ]
  node [
    id 440
    label "zrzec_si&#281;"
  ]
  node [
    id 441
    label "skopiowa&#263;"
  ]
  node [
    id 442
    label "przenie&#347;&#263;"
  ]
  node [
    id 443
    label "oskar"
  ]
  node [
    id 444
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 445
    label "return"
  ]
  node [
    id 446
    label "konsekwencja"
  ]
  node [
    id 447
    label "prize"
  ]
  node [
    id 448
    label "trophy"
  ]
  node [
    id 449
    label "oznaczenie"
  ]
  node [
    id 450
    label "potraktowanie"
  ]
  node [
    id 451
    label "nagrodzenie"
  ]
  node [
    id 452
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 453
    label "odczuwa&#263;"
  ]
  node [
    id 454
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 455
    label "skrupienie_si&#281;"
  ]
  node [
    id 456
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 457
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 458
    label "odczucie"
  ]
  node [
    id 459
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 460
    label "koszula_Dejaniry"
  ]
  node [
    id 461
    label "odczuwanie"
  ]
  node [
    id 462
    label "event"
  ]
  node [
    id 463
    label "skrupianie_si&#281;"
  ]
  node [
    id 464
    label "odczu&#263;"
  ]
  node [
    id 465
    label "darowizna"
  ]
  node [
    id 466
    label "foundation"
  ]
  node [
    id 467
    label "instytucja"
  ]
  node [
    id 468
    label "dar"
  ]
  node [
    id 469
    label "pocz&#261;tek"
  ]
  node [
    id 470
    label "osoba_prawna"
  ]
  node [
    id 471
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 472
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 473
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 474
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 475
    label "biuro"
  ]
  node [
    id 476
    label "organizacja"
  ]
  node [
    id 477
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 478
    label "Fundusze_Unijne"
  ]
  node [
    id 479
    label "zamyka&#263;"
  ]
  node [
    id 480
    label "establishment"
  ]
  node [
    id 481
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 482
    label "urz&#261;d"
  ]
  node [
    id 483
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 484
    label "afiliowa&#263;"
  ]
  node [
    id 485
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 486
    label "standard"
  ]
  node [
    id 487
    label "zamykanie"
  ]
  node [
    id 488
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 489
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 490
    label "pierworodztwo"
  ]
  node [
    id 491
    label "faza"
  ]
  node [
    id 492
    label "upgrade"
  ]
  node [
    id 493
    label "nast&#281;pstwo"
  ]
  node [
    id 494
    label "przeniesienie_praw"
  ]
  node [
    id 495
    label "zapomoga"
  ]
  node [
    id 496
    label "transakcja"
  ]
  node [
    id 497
    label "dyspozycja"
  ]
  node [
    id 498
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 499
    label "da&#324;"
  ]
  node [
    id 500
    label "faculty"
  ]
  node [
    id 501
    label "stygmat"
  ]
  node [
    id 502
    label "dobro"
  ]
  node [
    id 503
    label "rzecz"
  ]
  node [
    id 504
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 505
    label "gwiazda"
  ]
  node [
    id 506
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 507
    label "Arktur"
  ]
  node [
    id 508
    label "kszta&#322;t"
  ]
  node [
    id 509
    label "Gwiazda_Polarna"
  ]
  node [
    id 510
    label "agregatka"
  ]
  node [
    id 511
    label "gromada"
  ]
  node [
    id 512
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 513
    label "S&#322;o&#324;ce"
  ]
  node [
    id 514
    label "Nibiru"
  ]
  node [
    id 515
    label "konstelacja"
  ]
  node [
    id 516
    label "ornament"
  ]
  node [
    id 517
    label "delta_Scuti"
  ]
  node [
    id 518
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 519
    label "obiekt"
  ]
  node [
    id 520
    label "s&#322;awa"
  ]
  node [
    id 521
    label "promie&#324;"
  ]
  node [
    id 522
    label "star"
  ]
  node [
    id 523
    label "gwiazdosz"
  ]
  node [
    id 524
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 525
    label "asocjacja_gwiazd"
  ]
  node [
    id 526
    label "supergrupa"
  ]
  node [
    id 527
    label "mass-media"
  ]
  node [
    id 528
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 529
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 530
    label "przekazior"
  ]
  node [
    id 531
    label "uzbrajanie"
  ]
  node [
    id 532
    label "medium"
  ]
  node [
    id 533
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 534
    label "&#347;rodek"
  ]
  node [
    id 535
    label "jasnowidz"
  ]
  node [
    id 536
    label "hipnoza"
  ]
  node [
    id 537
    label "cz&#322;owiek"
  ]
  node [
    id 538
    label "spirytysta"
  ]
  node [
    id 539
    label "otoczenie"
  ]
  node [
    id 540
    label "publikator"
  ]
  node [
    id 541
    label "warunki"
  ]
  node [
    id 542
    label "przeka&#378;nik"
  ]
  node [
    id 543
    label "&#347;rodek_przekazu"
  ]
  node [
    id 544
    label "armament"
  ]
  node [
    id 545
    label "arming"
  ]
  node [
    id 546
    label "instalacja"
  ]
  node [
    id 547
    label "dozbrajanie"
  ]
  node [
    id 548
    label "dozbrojenie"
  ]
  node [
    id 549
    label "montowanie"
  ]
  node [
    id 550
    label "publish"
  ]
  node [
    id 551
    label "poda&#263;"
  ]
  node [
    id 552
    label "opublikowa&#263;"
  ]
  node [
    id 553
    label "obwo&#322;a&#263;"
  ]
  node [
    id 554
    label "declare"
  ]
  node [
    id 555
    label "communicate"
  ]
  node [
    id 556
    label "upubliczni&#263;"
  ]
  node [
    id 557
    label "picture"
  ]
  node [
    id 558
    label "wydawnictwo"
  ]
  node [
    id 559
    label "wprowadzi&#263;"
  ]
  node [
    id 560
    label "tenis"
  ]
  node [
    id 561
    label "ustawi&#263;"
  ]
  node [
    id 562
    label "siatk&#243;wka"
  ]
  node [
    id 563
    label "give"
  ]
  node [
    id 564
    label "zagra&#263;"
  ]
  node [
    id 565
    label "jedzenie"
  ]
  node [
    id 566
    label "poinformowa&#263;"
  ]
  node [
    id 567
    label "introduce"
  ]
  node [
    id 568
    label "nafaszerowa&#263;"
  ]
  node [
    id 569
    label "zaserwowa&#263;"
  ]
  node [
    id 570
    label "zakomunikowa&#263;"
  ]
  node [
    id 571
    label "okre&#347;li&#263;"
  ]
  node [
    id 572
    label "kolejny"
  ]
  node [
    id 573
    label "nowo"
  ]
  node [
    id 574
    label "bie&#380;&#261;cy"
  ]
  node [
    id 575
    label "drugi"
  ]
  node [
    id 576
    label "narybek"
  ]
  node [
    id 577
    label "obcy"
  ]
  node [
    id 578
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 579
    label "nowotny"
  ]
  node [
    id 580
    label "nadprzyrodzony"
  ]
  node [
    id 581
    label "nieznany"
  ]
  node [
    id 582
    label "pozaludzki"
  ]
  node [
    id 583
    label "obco"
  ]
  node [
    id 584
    label "tameczny"
  ]
  node [
    id 585
    label "osoba"
  ]
  node [
    id 586
    label "nieznajomo"
  ]
  node [
    id 587
    label "cudzy"
  ]
  node [
    id 588
    label "istota_&#380;ywa"
  ]
  node [
    id 589
    label "zaziemsko"
  ]
  node [
    id 590
    label "jednoczesny"
  ]
  node [
    id 591
    label "unowocze&#347;nianie"
  ]
  node [
    id 592
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 593
    label "tera&#378;niejszy"
  ]
  node [
    id 594
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 595
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 596
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 597
    label "nast&#281;pnie"
  ]
  node [
    id 598
    label "nastopny"
  ]
  node [
    id 599
    label "kolejno"
  ]
  node [
    id 600
    label "kt&#243;ry&#347;"
  ]
  node [
    id 601
    label "sw&#243;j"
  ]
  node [
    id 602
    label "przeciwny"
  ]
  node [
    id 603
    label "wt&#243;ry"
  ]
  node [
    id 604
    label "dzie&#324;"
  ]
  node [
    id 605
    label "odwrotnie"
  ]
  node [
    id 606
    label "podobny"
  ]
  node [
    id 607
    label "bie&#380;&#261;co"
  ]
  node [
    id 608
    label "ci&#261;g&#322;y"
  ]
  node [
    id 609
    label "aktualny"
  ]
  node [
    id 610
    label "ludzko&#347;&#263;"
  ]
  node [
    id 611
    label "asymilowanie"
  ]
  node [
    id 612
    label "wapniak"
  ]
  node [
    id 613
    label "asymilowa&#263;"
  ]
  node [
    id 614
    label "os&#322;abia&#263;"
  ]
  node [
    id 615
    label "posta&#263;"
  ]
  node [
    id 616
    label "hominid"
  ]
  node [
    id 617
    label "podw&#322;adny"
  ]
  node [
    id 618
    label "os&#322;abianie"
  ]
  node [
    id 619
    label "g&#322;owa"
  ]
  node [
    id 620
    label "figura"
  ]
  node [
    id 621
    label "portrecista"
  ]
  node [
    id 622
    label "dwun&#243;g"
  ]
  node [
    id 623
    label "profanum"
  ]
  node [
    id 624
    label "mikrokosmos"
  ]
  node [
    id 625
    label "nasada"
  ]
  node [
    id 626
    label "duch"
  ]
  node [
    id 627
    label "antropochoria"
  ]
  node [
    id 628
    label "wz&#243;r"
  ]
  node [
    id 629
    label "senior"
  ]
  node [
    id 630
    label "Adam"
  ]
  node [
    id 631
    label "homo_sapiens"
  ]
  node [
    id 632
    label "polifag"
  ]
  node [
    id 633
    label "dopiero_co"
  ]
  node [
    id 634
    label "formacja"
  ]
  node [
    id 635
    label "potomstwo"
  ]
  node [
    id 636
    label "casting"
  ]
  node [
    id 637
    label "nab&#243;r"
  ]
  node [
    id 638
    label "Eurowizja"
  ]
  node [
    id 639
    label "eliminacje"
  ]
  node [
    id 640
    label "impreza"
  ]
  node [
    id 641
    label "emulation"
  ]
  node [
    id 642
    label "Interwizja"
  ]
  node [
    id 643
    label "impra"
  ]
  node [
    id 644
    label "rozrywka"
  ]
  node [
    id 645
    label "przyj&#281;cie"
  ]
  node [
    id 646
    label "okazja"
  ]
  node [
    id 647
    label "party"
  ]
  node [
    id 648
    label "recruitment"
  ]
  node [
    id 649
    label "wyb&#243;r"
  ]
  node [
    id 650
    label "runda"
  ]
  node [
    id 651
    label "retirement"
  ]
  node [
    id 652
    label "przes&#322;uchanie"
  ]
  node [
    id 653
    label "w&#281;dkarstwo"
  ]
  node [
    id 654
    label "szko&#322;a_ponadpodstawowa"
  ]
  node [
    id 655
    label "warunek_lokalowy"
  ]
  node [
    id 656
    label "plac"
  ]
  node [
    id 657
    label "location"
  ]
  node [
    id 658
    label "uwaga"
  ]
  node [
    id 659
    label "przestrze&#324;"
  ]
  node [
    id 660
    label "status"
  ]
  node [
    id 661
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 662
    label "chwila"
  ]
  node [
    id 663
    label "cia&#322;o"
  ]
  node [
    id 664
    label "praca"
  ]
  node [
    id 665
    label "rz&#261;d"
  ]
  node [
    id 666
    label "teren_szko&#322;y"
  ]
  node [
    id 667
    label "wiedza"
  ]
  node [
    id 668
    label "Mickiewicz"
  ]
  node [
    id 669
    label "kwalifikacje"
  ]
  node [
    id 670
    label "podr&#281;cznik"
  ]
  node [
    id 671
    label "absolwent"
  ]
  node [
    id 672
    label "praktyka"
  ]
  node [
    id 673
    label "school"
  ]
  node [
    id 674
    label "system"
  ]
  node [
    id 675
    label "zda&#263;"
  ]
  node [
    id 676
    label "gabinet"
  ]
  node [
    id 677
    label "urszulanki"
  ]
  node [
    id 678
    label "sztuba"
  ]
  node [
    id 679
    label "&#322;awa_szkolna"
  ]
  node [
    id 680
    label "nauka"
  ]
  node [
    id 681
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 682
    label "muzyka"
  ]
  node [
    id 683
    label "grupa"
  ]
  node [
    id 684
    label "form"
  ]
  node [
    id 685
    label "lekcja"
  ]
  node [
    id 686
    label "metoda"
  ]
  node [
    id 687
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 688
    label "czas"
  ]
  node [
    id 689
    label "skolaryzacja"
  ]
  node [
    id 690
    label "zdanie"
  ]
  node [
    id 691
    label "stopek"
  ]
  node [
    id 692
    label "sekretariat"
  ]
  node [
    id 693
    label "ideologia"
  ]
  node [
    id 694
    label "lesson"
  ]
  node [
    id 695
    label "niepokalanki"
  ]
  node [
    id 696
    label "siedziba"
  ]
  node [
    id 697
    label "szkolenie"
  ]
  node [
    id 698
    label "kara"
  ]
  node [
    id 699
    label "tablica"
  ]
  node [
    id 700
    label "wyprawka"
  ]
  node [
    id 701
    label "pomoc_naukowa"
  ]
  node [
    id 702
    label "odm&#322;adzanie"
  ]
  node [
    id 703
    label "liga"
  ]
  node [
    id 704
    label "jednostka_systematyczna"
  ]
  node [
    id 705
    label "Entuzjastki"
  ]
  node [
    id 706
    label "kompozycja"
  ]
  node [
    id 707
    label "Terranie"
  ]
  node [
    id 708
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 709
    label "category"
  ]
  node [
    id 710
    label "oddzia&#322;"
  ]
  node [
    id 711
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 712
    label "cz&#261;steczka"
  ]
  node [
    id 713
    label "stage_set"
  ]
  node [
    id 714
    label "type"
  ]
  node [
    id 715
    label "specgrupa"
  ]
  node [
    id 716
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 717
    label "&#346;wietliki"
  ]
  node [
    id 718
    label "odm&#322;odzenie"
  ]
  node [
    id 719
    label "Eurogrupa"
  ]
  node [
    id 720
    label "odm&#322;adza&#263;"
  ]
  node [
    id 721
    label "formacja_geologiczna"
  ]
  node [
    id 722
    label "harcerze_starsi"
  ]
  node [
    id 723
    label "course"
  ]
  node [
    id 724
    label "pomaganie"
  ]
  node [
    id 725
    label "training"
  ]
  node [
    id 726
    label "zapoznawanie"
  ]
  node [
    id 727
    label "seria"
  ]
  node [
    id 728
    label "zaj&#281;cia"
  ]
  node [
    id 729
    label "pouczenie"
  ]
  node [
    id 730
    label "o&#347;wiecanie"
  ]
  node [
    id 731
    label "Lira"
  ]
  node [
    id 732
    label "kliker"
  ]
  node [
    id 733
    label "miasteczko_rowerowe"
  ]
  node [
    id 734
    label "porada"
  ]
  node [
    id 735
    label "fotowoltaika"
  ]
  node [
    id 736
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 737
    label "przem&#243;wienie"
  ]
  node [
    id 738
    label "nauki_o_poznaniu"
  ]
  node [
    id 739
    label "nomotetyczny"
  ]
  node [
    id 740
    label "systematyka"
  ]
  node [
    id 741
    label "proces"
  ]
  node [
    id 742
    label "typologia"
  ]
  node [
    id 743
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 744
    label "kultura_duchowa"
  ]
  node [
    id 745
    label "nauki_penalne"
  ]
  node [
    id 746
    label "dziedzina"
  ]
  node [
    id 747
    label "imagineskopia"
  ]
  node [
    id 748
    label "teoria_naukowa"
  ]
  node [
    id 749
    label "inwentyka"
  ]
  node [
    id 750
    label "metodologia"
  ]
  node [
    id 751
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 752
    label "nauki_o_Ziemi"
  ]
  node [
    id 753
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 754
    label "materia&#322;"
  ]
  node [
    id 755
    label "spos&#243;b"
  ]
  node [
    id 756
    label "obrz&#261;dek"
  ]
  node [
    id 757
    label "Biblia"
  ]
  node [
    id 758
    label "tekst"
  ]
  node [
    id 759
    label "lektor"
  ]
  node [
    id 760
    label "kwota"
  ]
  node [
    id 761
    label "nemezis"
  ]
  node [
    id 762
    label "punishment"
  ]
  node [
    id 763
    label "klacz"
  ]
  node [
    id 764
    label "forfeit"
  ]
  node [
    id 765
    label "roboty_przymusowe"
  ]
  node [
    id 766
    label "poprzedzanie"
  ]
  node [
    id 767
    label "czasoprzestrze&#324;"
  ]
  node [
    id 768
    label "laba"
  ]
  node [
    id 769
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 770
    label "chronometria"
  ]
  node [
    id 771
    label "rachuba_czasu"
  ]
  node [
    id 772
    label "przep&#322;ywanie"
  ]
  node [
    id 773
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 774
    label "czasokres"
  ]
  node [
    id 775
    label "odczyt"
  ]
  node [
    id 776
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 777
    label "dzieje"
  ]
  node [
    id 778
    label "kategoria_gramatyczna"
  ]
  node [
    id 779
    label "poprzedzenie"
  ]
  node [
    id 780
    label "trawienie"
  ]
  node [
    id 781
    label "pochodzi&#263;"
  ]
  node [
    id 782
    label "period"
  ]
  node [
    id 783
    label "okres_czasu"
  ]
  node [
    id 784
    label "poprzedza&#263;"
  ]
  node [
    id 785
    label "schy&#322;ek"
  ]
  node [
    id 786
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 787
    label "odwlekanie_si&#281;"
  ]
  node [
    id 788
    label "zegar"
  ]
  node [
    id 789
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 790
    label "czwarty_wymiar"
  ]
  node [
    id 791
    label "pochodzenie"
  ]
  node [
    id 792
    label "koniugacja"
  ]
  node [
    id 793
    label "Zeitgeist"
  ]
  node [
    id 794
    label "trawi&#263;"
  ]
  node [
    id 795
    label "pogoda"
  ]
  node [
    id 796
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 797
    label "poprzedzi&#263;"
  ]
  node [
    id 798
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 799
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 800
    label "time_period"
  ]
  node [
    id 801
    label "j&#261;dro"
  ]
  node [
    id 802
    label "systemik"
  ]
  node [
    id 803
    label "rozprz&#261;c"
  ]
  node [
    id 804
    label "oprogramowanie"
  ]
  node [
    id 805
    label "systemat"
  ]
  node [
    id 806
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 807
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 808
    label "model"
  ]
  node [
    id 809
    label "usenet"
  ]
  node [
    id 810
    label "porz&#261;dek"
  ]
  node [
    id 811
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 812
    label "przyn&#281;ta"
  ]
  node [
    id 813
    label "p&#322;&#243;d"
  ]
  node [
    id 814
    label "net"
  ]
  node [
    id 815
    label "eratem"
  ]
  node [
    id 816
    label "doktryna"
  ]
  node [
    id 817
    label "pulpit"
  ]
  node [
    id 818
    label "jednostka_geologiczna"
  ]
  node [
    id 819
    label "o&#347;"
  ]
  node [
    id 820
    label "podsystem"
  ]
  node [
    id 821
    label "ryba"
  ]
  node [
    id 822
    label "Leopard"
  ]
  node [
    id 823
    label "Android"
  ]
  node [
    id 824
    label "zachowanie"
  ]
  node [
    id 825
    label "cybernetyk"
  ]
  node [
    id 826
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 827
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 828
    label "method"
  ]
  node [
    id 829
    label "sk&#322;ad"
  ]
  node [
    id 830
    label "podstawa"
  ]
  node [
    id 831
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 832
    label "practice"
  ]
  node [
    id 833
    label "znawstwo"
  ]
  node [
    id 834
    label "skill"
  ]
  node [
    id 835
    label "czyn"
  ]
  node [
    id 836
    label "zwyczaj"
  ]
  node [
    id 837
    label "eksperiencja"
  ]
  node [
    id 838
    label "&#321;ubianka"
  ]
  node [
    id 839
    label "miejsce_pracy"
  ]
  node [
    id 840
    label "dzia&#322;_personalny"
  ]
  node [
    id 841
    label "Kreml"
  ]
  node [
    id 842
    label "Bia&#322;y_Dom"
  ]
  node [
    id 843
    label "budynek"
  ]
  node [
    id 844
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 845
    label "sadowisko"
  ]
  node [
    id 846
    label "wokalistyka"
  ]
  node [
    id 847
    label "przedmiot"
  ]
  node [
    id 848
    label "wykonywanie"
  ]
  node [
    id 849
    label "muza"
  ]
  node [
    id 850
    label "wykonywa&#263;"
  ]
  node [
    id 851
    label "zjawisko"
  ]
  node [
    id 852
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 853
    label "beatbox"
  ]
  node [
    id 854
    label "komponowa&#263;"
  ]
  node [
    id 855
    label "komponowanie"
  ]
  node [
    id 856
    label "wytw&#243;r"
  ]
  node [
    id 857
    label "pasa&#380;"
  ]
  node [
    id 858
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 859
    label "notacja_muzyczna"
  ]
  node [
    id 860
    label "kontrapunkt"
  ]
  node [
    id 861
    label "sztuka"
  ]
  node [
    id 862
    label "instrumentalistyka"
  ]
  node [
    id 863
    label "harmonia"
  ]
  node [
    id 864
    label "set"
  ]
  node [
    id 865
    label "wys&#322;uchanie"
  ]
  node [
    id 866
    label "kapela"
  ]
  node [
    id 867
    label "britpop"
  ]
  node [
    id 868
    label "badanie"
  ]
  node [
    id 869
    label "obserwowanie"
  ]
  node [
    id 870
    label "wy&#347;wiadczenie"
  ]
  node [
    id 871
    label "assay"
  ]
  node [
    id 872
    label "checkup"
  ]
  node [
    id 873
    label "do&#347;wiadczanie"
  ]
  node [
    id 874
    label "zbadanie"
  ]
  node [
    id 875
    label "poczucie"
  ]
  node [
    id 876
    label "proporcja"
  ]
  node [
    id 877
    label "wykszta&#322;cenie"
  ]
  node [
    id 878
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 879
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 880
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 881
    label "urszulanki_szare"
  ]
  node [
    id 882
    label "cognition"
  ]
  node [
    id 883
    label "intelekt"
  ]
  node [
    id 884
    label "pozwolenie"
  ]
  node [
    id 885
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 886
    label "zaawansowanie"
  ]
  node [
    id 887
    label "ucze&#324;"
  ]
  node [
    id 888
    label "student"
  ]
  node [
    id 889
    label "zaliczy&#263;"
  ]
  node [
    id 890
    label "powierzy&#263;"
  ]
  node [
    id 891
    label "zmusi&#263;"
  ]
  node [
    id 892
    label "translate"
  ]
  node [
    id 893
    label "przedstawi&#263;"
  ]
  node [
    id 894
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 895
    label "convey"
  ]
  node [
    id 896
    label "fraza"
  ]
  node [
    id 897
    label "wypowiedzenie"
  ]
  node [
    id 898
    label "prison_term"
  ]
  node [
    id 899
    label "okres"
  ]
  node [
    id 900
    label "przedstawienie"
  ]
  node [
    id 901
    label "wyra&#380;enie"
  ]
  node [
    id 902
    label "zaliczenie"
  ]
  node [
    id 903
    label "antylogizm"
  ]
  node [
    id 904
    label "zmuszenie"
  ]
  node [
    id 905
    label "konektyw"
  ]
  node [
    id 906
    label "attitude"
  ]
  node [
    id 907
    label "powierzenie"
  ]
  node [
    id 908
    label "adjudication"
  ]
  node [
    id 909
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 910
    label "pass"
  ]
  node [
    id 911
    label "political_orientation"
  ]
  node [
    id 912
    label "idea"
  ]
  node [
    id 913
    label "stra&#380;nik"
  ]
  node [
    id 914
    label "przedszkole"
  ]
  node [
    id 915
    label "opiekun"
  ]
  node [
    id 916
    label "ruch"
  ]
  node [
    id 917
    label "rozmiar&#243;wka"
  ]
  node [
    id 918
    label "p&#322;aszczyzna"
  ]
  node [
    id 919
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 920
    label "tarcza"
  ]
  node [
    id 921
    label "kosz"
  ]
  node [
    id 922
    label "transparent"
  ]
  node [
    id 923
    label "uk&#322;ad"
  ]
  node [
    id 924
    label "rubryka"
  ]
  node [
    id 925
    label "kontener"
  ]
  node [
    id 926
    label "spis"
  ]
  node [
    id 927
    label "plate"
  ]
  node [
    id 928
    label "konstrukcja"
  ]
  node [
    id 929
    label "szachownica_Punnetta"
  ]
  node [
    id 930
    label "chart"
  ]
  node [
    id 931
    label "izba"
  ]
  node [
    id 932
    label "biurko"
  ]
  node [
    id 933
    label "boks"
  ]
  node [
    id 934
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 935
    label "egzekutywa"
  ]
  node [
    id 936
    label "premier"
  ]
  node [
    id 937
    label "Londyn"
  ]
  node [
    id 938
    label "palestra"
  ]
  node [
    id 939
    label "pok&#243;j"
  ]
  node [
    id 940
    label "pracownia"
  ]
  node [
    id 941
    label "gabinet_cieni"
  ]
  node [
    id 942
    label "pomieszczenie"
  ]
  node [
    id 943
    label "Konsulat"
  ]
  node [
    id 944
    label "wagon"
  ]
  node [
    id 945
    label "mecz_mistrzowski"
  ]
  node [
    id 946
    label "class"
  ]
  node [
    id 947
    label "&#322;awka"
  ]
  node [
    id 948
    label "wykrzyknik"
  ]
  node [
    id 949
    label "zaleta"
  ]
  node [
    id 950
    label "programowanie_obiektowe"
  ]
  node [
    id 951
    label "rezerwa"
  ]
  node [
    id 952
    label "Ekwici"
  ]
  node [
    id 953
    label "&#347;rodowisko"
  ]
  node [
    id 954
    label "sala"
  ]
  node [
    id 955
    label "pomoc"
  ]
  node [
    id 956
    label "jako&#347;&#263;"
  ]
  node [
    id 957
    label "znak_jako&#347;ci"
  ]
  node [
    id 958
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 959
    label "promocja"
  ]
  node [
    id 960
    label "kurs"
  ]
  node [
    id 961
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 962
    label "dziennik_lekcyjny"
  ]
  node [
    id 963
    label "typ"
  ]
  node [
    id 964
    label "fakcja"
  ]
  node [
    id 965
    label "obrona"
  ]
  node [
    id 966
    label "botanika"
  ]
  node [
    id 967
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 968
    label "Wallenrod"
  ]
  node [
    id 969
    label "informacyjnie"
  ]
  node [
    id 970
    label "znaczeniowo"
  ]
  node [
    id 971
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 972
    label "Unia"
  ]
  node [
    id 973
    label "combination"
  ]
  node [
    id 974
    label "Unia_Europejska"
  ]
  node [
    id 975
    label "union"
  ]
  node [
    id 976
    label "partia"
  ]
  node [
    id 977
    label "podmiot"
  ]
  node [
    id 978
    label "jednostka_organizacyjna"
  ]
  node [
    id 979
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 980
    label "TOPR"
  ]
  node [
    id 981
    label "endecki"
  ]
  node [
    id 982
    label "od&#322;am"
  ]
  node [
    id 983
    label "przedstawicielstwo"
  ]
  node [
    id 984
    label "Cepelia"
  ]
  node [
    id 985
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 986
    label "ZBoWiD"
  ]
  node [
    id 987
    label "organization"
  ]
  node [
    id 988
    label "centrala"
  ]
  node [
    id 989
    label "GOPR"
  ]
  node [
    id 990
    label "ZOMO"
  ]
  node [
    id 991
    label "ZMP"
  ]
  node [
    id 992
    label "komitet_koordynacyjny"
  ]
  node [
    id 993
    label "przybud&#243;wka"
  ]
  node [
    id 994
    label "boj&#243;wka"
  ]
  node [
    id 995
    label "Bund"
  ]
  node [
    id 996
    label "PPR"
  ]
  node [
    id 997
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 998
    label "wybranek"
  ]
  node [
    id 999
    label "Jakobici"
  ]
  node [
    id 1000
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1001
    label "SLD"
  ]
  node [
    id 1002
    label "Razem"
  ]
  node [
    id 1003
    label "PiS"
  ]
  node [
    id 1004
    label "package"
  ]
  node [
    id 1005
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1006
    label "Kuomintang"
  ]
  node [
    id 1007
    label "ZSL"
  ]
  node [
    id 1008
    label "AWS"
  ]
  node [
    id 1009
    label "gra"
  ]
  node [
    id 1010
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1011
    label "game"
  ]
  node [
    id 1012
    label "blok"
  ]
  node [
    id 1013
    label "PO"
  ]
  node [
    id 1014
    label "si&#322;a"
  ]
  node [
    id 1015
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1016
    label "niedoczas"
  ]
  node [
    id 1017
    label "Federali&#347;ci"
  ]
  node [
    id 1018
    label "PSL"
  ]
  node [
    id 1019
    label "Wigowie"
  ]
  node [
    id 1020
    label "ZChN"
  ]
  node [
    id 1021
    label "aktyw"
  ]
  node [
    id 1022
    label "wybranka"
  ]
  node [
    id 1023
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1024
    label "unit"
  ]
  node [
    id 1025
    label "treaty"
  ]
  node [
    id 1026
    label "umowa"
  ]
  node [
    id 1027
    label "przestawi&#263;"
  ]
  node [
    id 1028
    label "alliance"
  ]
  node [
    id 1029
    label "ONZ"
  ]
  node [
    id 1030
    label "NATO"
  ]
  node [
    id 1031
    label "zawarcie"
  ]
  node [
    id 1032
    label "zawrze&#263;"
  ]
  node [
    id 1033
    label "organ"
  ]
  node [
    id 1034
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1035
    label "wi&#281;&#378;"
  ]
  node [
    id 1036
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 1037
    label "traktat_wersalski"
  ]
  node [
    id 1038
    label "eurosceptycyzm"
  ]
  node [
    id 1039
    label "euroentuzjasta"
  ]
  node [
    id 1040
    label "euroentuzjazm"
  ]
  node [
    id 1041
    label "euroko&#322;choz"
  ]
  node [
    id 1042
    label "strefa_euro"
  ]
  node [
    id 1043
    label "eurorealizm"
  ]
  node [
    id 1044
    label "p&#322;atnik_netto"
  ]
  node [
    id 1045
    label "Bruksela"
  ]
  node [
    id 1046
    label "eurorealista"
  ]
  node [
    id 1047
    label "eurosceptyczny"
  ]
  node [
    id 1048
    label "eurosceptyk"
  ]
  node [
    id 1049
    label "prawo_unijne"
  ]
  node [
    id 1050
    label "Wsp&#243;lnota_Europejska"
  ]
  node [
    id 1051
    label "po_europejsku"
  ]
  node [
    id 1052
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1053
    label "European"
  ]
  node [
    id 1054
    label "typowy"
  ]
  node [
    id 1055
    label "charakterystyczny"
  ]
  node [
    id 1056
    label "europejsko"
  ]
  node [
    id 1057
    label "charakterystycznie"
  ]
  node [
    id 1058
    label "szczeg&#243;lny"
  ]
  node [
    id 1059
    label "wyj&#261;tkowy"
  ]
  node [
    id 1060
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1061
    label "zwyczajny"
  ]
  node [
    id 1062
    label "typowo"
  ]
  node [
    id 1063
    label "cz&#281;sty"
  ]
  node [
    id 1064
    label "zwyk&#322;y"
  ]
  node [
    id 1065
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1066
    label "nale&#380;ny"
  ]
  node [
    id 1067
    label "nale&#380;yty"
  ]
  node [
    id 1068
    label "uprawniony"
  ]
  node [
    id 1069
    label "zasadniczy"
  ]
  node [
    id 1070
    label "stosownie"
  ]
  node [
    id 1071
    label "taki"
  ]
  node [
    id 1072
    label "prawdziwy"
  ]
  node [
    id 1073
    label "ten"
  ]
  node [
    id 1074
    label "dobry"
  ]
  node [
    id 1075
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1076
    label "zrozumie&#263;"
  ]
  node [
    id 1077
    label "feel"
  ]
  node [
    id 1078
    label "topographic_point"
  ]
  node [
    id 1079
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1080
    label "visualize"
  ]
  node [
    id 1081
    label "przyswoi&#263;"
  ]
  node [
    id 1082
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 1083
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 1084
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1085
    label "teach"
  ]
  node [
    id 1086
    label "experience"
  ]
  node [
    id 1087
    label "permit"
  ]
  node [
    id 1088
    label "zauwa&#380;y&#263;"
  ]
  node [
    id 1089
    label "dostrzec"
  ]
  node [
    id 1090
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 1091
    label "organizm"
  ]
  node [
    id 1092
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1093
    label "kultura"
  ]
  node [
    id 1094
    label "thrill"
  ]
  node [
    id 1095
    label "oceni&#263;"
  ]
  node [
    id 1096
    label "skuma&#263;"
  ]
  node [
    id 1097
    label "poczu&#263;"
  ]
  node [
    id 1098
    label "do"
  ]
  node [
    id 1099
    label "zacz&#261;&#263;"
  ]
  node [
    id 1100
    label "think"
  ]
  node [
    id 1101
    label "post&#261;pi&#263;"
  ]
  node [
    id 1102
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1103
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1104
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1105
    label "zorganizowa&#263;"
  ]
  node [
    id 1106
    label "appoint"
  ]
  node [
    id 1107
    label "wystylizowa&#263;"
  ]
  node [
    id 1108
    label "cause"
  ]
  node [
    id 1109
    label "przerobi&#263;"
  ]
  node [
    id 1110
    label "nabra&#263;"
  ]
  node [
    id 1111
    label "make"
  ]
  node [
    id 1112
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1113
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1114
    label "wydali&#263;"
  ]
  node [
    id 1115
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 1116
    label "advance"
  ]
  node [
    id 1117
    label "act"
  ]
  node [
    id 1118
    label "see"
  ]
  node [
    id 1119
    label "usun&#261;&#263;"
  ]
  node [
    id 1120
    label "sack"
  ]
  node [
    id 1121
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 1122
    label "restore"
  ]
  node [
    id 1123
    label "dostosowa&#263;"
  ]
  node [
    id 1124
    label "pozyska&#263;"
  ]
  node [
    id 1125
    label "stworzy&#263;"
  ]
  node [
    id 1126
    label "plan"
  ]
  node [
    id 1127
    label "stage"
  ]
  node [
    id 1128
    label "urobi&#263;"
  ]
  node [
    id 1129
    label "ensnare"
  ]
  node [
    id 1130
    label "zaplanowa&#263;"
  ]
  node [
    id 1131
    label "przygotowa&#263;"
  ]
  node [
    id 1132
    label "skupi&#263;"
  ]
  node [
    id 1133
    label "podbi&#263;"
  ]
  node [
    id 1134
    label "umocni&#263;"
  ]
  node [
    id 1135
    label "doprowadzi&#263;"
  ]
  node [
    id 1136
    label "zadowoli&#263;"
  ]
  node [
    id 1137
    label "accommodate"
  ]
  node [
    id 1138
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 1139
    label "zabezpieczy&#263;"
  ]
  node [
    id 1140
    label "wytworzy&#263;"
  ]
  node [
    id 1141
    label "pomy&#347;le&#263;"
  ]
  node [
    id 1142
    label "woda"
  ]
  node [
    id 1143
    label "hoax"
  ]
  node [
    id 1144
    label "deceive"
  ]
  node [
    id 1145
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 1146
    label "oszwabi&#263;"
  ]
  node [
    id 1147
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 1148
    label "gull"
  ]
  node [
    id 1149
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 1150
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 1151
    label "wzi&#261;&#263;"
  ]
  node [
    id 1152
    label "naby&#263;"
  ]
  node [
    id 1153
    label "fraud"
  ]
  node [
    id 1154
    label "kupi&#263;"
  ]
  node [
    id 1155
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1156
    label "objecha&#263;"
  ]
  node [
    id 1157
    label "stylize"
  ]
  node [
    id 1158
    label "nada&#263;"
  ]
  node [
    id 1159
    label "upodobni&#263;"
  ]
  node [
    id 1160
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1161
    label "overwork"
  ]
  node [
    id 1162
    label "zamieni&#263;"
  ]
  node [
    id 1163
    label "zmodyfikowa&#263;"
  ]
  node [
    id 1164
    label "change"
  ]
  node [
    id 1165
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1166
    label "przej&#347;&#263;"
  ]
  node [
    id 1167
    label "convert"
  ]
  node [
    id 1168
    label "prze&#380;y&#263;"
  ]
  node [
    id 1169
    label "przetworzy&#263;"
  ]
  node [
    id 1170
    label "upora&#263;_si&#281;"
  ]
  node [
    id 1171
    label "sprawi&#263;"
  ]
  node [
    id 1172
    label "delivery"
  ]
  node [
    id 1173
    label "rendition"
  ]
  node [
    id 1174
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1175
    label "impression"
  ]
  node [
    id 1176
    label "publikacja"
  ]
  node [
    id 1177
    label "zadenuncjowanie"
  ]
  node [
    id 1178
    label "zapach"
  ]
  node [
    id 1179
    label "reszta"
  ]
  node [
    id 1180
    label "wytworzenie"
  ]
  node [
    id 1181
    label "issue"
  ]
  node [
    id 1182
    label "czasopismo"
  ]
  node [
    id 1183
    label "podanie"
  ]
  node [
    id 1184
    label "wprowadzenie"
  ]
  node [
    id 1185
    label "odmiana"
  ]
  node [
    id 1186
    label "ujawnienie"
  ]
  node [
    id 1187
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 1188
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1189
    label "urz&#261;dzenie"
  ]
  node [
    id 1190
    label "mutant"
  ]
  node [
    id 1191
    label "rewizja"
  ]
  node [
    id 1192
    label "gramatyka"
  ]
  node [
    id 1193
    label "paradygmat"
  ]
  node [
    id 1194
    label "podgatunek"
  ]
  node [
    id 1195
    label "ferment"
  ]
  node [
    id 1196
    label "rasa"
  ]
  node [
    id 1197
    label "kom&#243;rka"
  ]
  node [
    id 1198
    label "furnishing"
  ]
  node [
    id 1199
    label "zabezpieczenie"
  ]
  node [
    id 1200
    label "wyrz&#261;dzenie"
  ]
  node [
    id 1201
    label "zagospodarowanie"
  ]
  node [
    id 1202
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 1203
    label "ig&#322;a"
  ]
  node [
    id 1204
    label "narz&#281;dzie"
  ]
  node [
    id 1205
    label "wirnik"
  ]
  node [
    id 1206
    label "aparatura"
  ]
  node [
    id 1207
    label "system_energetyczny"
  ]
  node [
    id 1208
    label "impulsator"
  ]
  node [
    id 1209
    label "mechanizm"
  ]
  node [
    id 1210
    label "sprz&#281;t"
  ]
  node [
    id 1211
    label "blokowanie"
  ]
  node [
    id 1212
    label "zablokowanie"
  ]
  node [
    id 1213
    label "komora"
  ]
  node [
    id 1214
    label "j&#281;zyk"
  ]
  node [
    id 1215
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 1216
    label "narobienie"
  ]
  node [
    id 1217
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1218
    label "creation"
  ]
  node [
    id 1219
    label "porobienie"
  ]
  node [
    id 1220
    label "coevals"
  ]
  node [
    id 1221
    label "rynek"
  ]
  node [
    id 1222
    label "nuklearyzacja"
  ]
  node [
    id 1223
    label "deduction"
  ]
  node [
    id 1224
    label "entrance"
  ]
  node [
    id 1225
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 1226
    label "wst&#281;p"
  ]
  node [
    id 1227
    label "wej&#347;cie"
  ]
  node [
    id 1228
    label "doprowadzenie"
  ]
  node [
    id 1229
    label "umo&#380;liwienie"
  ]
  node [
    id 1230
    label "wpisanie"
  ]
  node [
    id 1231
    label "podstawy"
  ]
  node [
    id 1232
    label "evocation"
  ]
  node [
    id 1233
    label "zapoznanie"
  ]
  node [
    id 1234
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1235
    label "zacz&#281;cie"
  ]
  node [
    id 1236
    label "przewietrzenie"
  ]
  node [
    id 1237
    label "druk"
  ]
  node [
    id 1238
    label "produkcja"
  ]
  node [
    id 1239
    label "notification"
  ]
  node [
    id 1240
    label "detection"
  ]
  node [
    id 1241
    label "dostrze&#380;enie"
  ]
  node [
    id 1242
    label "disclosure"
  ]
  node [
    id 1243
    label "jawny"
  ]
  node [
    id 1244
    label "objawienie"
  ]
  node [
    id 1245
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 1246
    label "denunciation"
  ]
  node [
    id 1247
    label "doniesienie"
  ]
  node [
    id 1248
    label "ustawienie"
  ]
  node [
    id 1249
    label "narrative"
  ]
  node [
    id 1250
    label "pismo"
  ]
  node [
    id 1251
    label "nafaszerowanie"
  ]
  node [
    id 1252
    label "prayer"
  ]
  node [
    id 1253
    label "pi&#322;ka"
  ]
  node [
    id 1254
    label "myth"
  ]
  node [
    id 1255
    label "service"
  ]
  node [
    id 1256
    label "zagranie"
  ]
  node [
    id 1257
    label "zaserwowanie"
  ]
  node [
    id 1258
    label "opowie&#347;&#263;"
  ]
  node [
    id 1259
    label "obiecanie"
  ]
  node [
    id 1260
    label "cios"
  ]
  node [
    id 1261
    label "udost&#281;pnienie"
  ]
  node [
    id 1262
    label "wymienienie_si&#281;"
  ]
  node [
    id 1263
    label "eating"
  ]
  node [
    id 1264
    label "coup"
  ]
  node [
    id 1265
    label "hand"
  ]
  node [
    id 1266
    label "uprawianie_seksu"
  ]
  node [
    id 1267
    label "allow"
  ]
  node [
    id 1268
    label "uderzenie"
  ]
  node [
    id 1269
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1270
    label "odst&#261;pienie"
  ]
  node [
    id 1271
    label "dodanie"
  ]
  node [
    id 1272
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1273
    label "dostanie"
  ]
  node [
    id 1274
    label "karta"
  ]
  node [
    id 1275
    label "potrawa"
  ]
  node [
    id 1276
    label "menu"
  ]
  node [
    id 1277
    label "uderzanie"
  ]
  node [
    id 1278
    label "wyst&#261;pienie"
  ]
  node [
    id 1279
    label "pobicie"
  ]
  node [
    id 1280
    label "posi&#322;ek"
  ]
  node [
    id 1281
    label "czynnik_biotyczny"
  ]
  node [
    id 1282
    label "wyewoluowanie"
  ]
  node [
    id 1283
    label "reakcja"
  ]
  node [
    id 1284
    label "individual"
  ]
  node [
    id 1285
    label "starzenie_si&#281;"
  ]
  node [
    id 1286
    label "wyewoluowa&#263;"
  ]
  node [
    id 1287
    label "okaz"
  ]
  node [
    id 1288
    label "part"
  ]
  node [
    id 1289
    label "przyswojenie"
  ]
  node [
    id 1290
    label "ewoluowanie"
  ]
  node [
    id 1291
    label "ewoluowa&#263;"
  ]
  node [
    id 1292
    label "agent"
  ]
  node [
    id 1293
    label "przyswaja&#263;"
  ]
  node [
    id 1294
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1295
    label "nicpo&#324;"
  ]
  node [
    id 1296
    label "przyswajanie"
  ]
  node [
    id 1297
    label "phone"
  ]
  node [
    id 1298
    label "wpadni&#281;cie"
  ]
  node [
    id 1299
    label "wydawa&#263;"
  ]
  node [
    id 1300
    label "wyda&#263;"
  ]
  node [
    id 1301
    label "intonacja"
  ]
  node [
    id 1302
    label "wpa&#347;&#263;"
  ]
  node [
    id 1303
    label "note"
  ]
  node [
    id 1304
    label "onomatopeja"
  ]
  node [
    id 1305
    label "modalizm"
  ]
  node [
    id 1306
    label "nadlecenie"
  ]
  node [
    id 1307
    label "sound"
  ]
  node [
    id 1308
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1309
    label "wpada&#263;"
  ]
  node [
    id 1310
    label "solmizacja"
  ]
  node [
    id 1311
    label "dobiec"
  ]
  node [
    id 1312
    label "transmiter"
  ]
  node [
    id 1313
    label "heksachord"
  ]
  node [
    id 1314
    label "akcent"
  ]
  node [
    id 1315
    label "repetycja"
  ]
  node [
    id 1316
    label "brzmienie"
  ]
  node [
    id 1317
    label "wpadanie"
  ]
  node [
    id 1318
    label "liczba_kwantowa"
  ]
  node [
    id 1319
    label "kosmetyk"
  ]
  node [
    id 1320
    label "ciasto"
  ]
  node [
    id 1321
    label "aromat"
  ]
  node [
    id 1322
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 1323
    label "puff"
  ]
  node [
    id 1324
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 1325
    label "przyprawa"
  ]
  node [
    id 1326
    label "upojno&#347;&#263;"
  ]
  node [
    id 1327
    label "owiewanie"
  ]
  node [
    id 1328
    label "smak"
  ]
  node [
    id 1329
    label "remainder"
  ]
  node [
    id 1330
    label "pozosta&#322;y"
  ]
  node [
    id 1331
    label "psychotest"
  ]
  node [
    id 1332
    label "communication"
  ]
  node [
    id 1333
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1334
    label "wk&#322;ad"
  ]
  node [
    id 1335
    label "zajawka"
  ]
  node [
    id 1336
    label "ok&#322;adka"
  ]
  node [
    id 1337
    label "Zwrotnica"
  ]
  node [
    id 1338
    label "dzia&#322;"
  ]
  node [
    id 1339
    label "prasa"
  ]
  node [
    id 1340
    label "beat"
  ]
  node [
    id 1341
    label "zwojowanie"
  ]
  node [
    id 1342
    label "wr&#243;cenie_z_tarcz&#261;"
  ]
  node [
    id 1343
    label "wygrywanie"
  ]
  node [
    id 1344
    label "znoszenie"
  ]
  node [
    id 1345
    label "robienie"
  ]
  node [
    id 1346
    label "wracanie_z_tarcz&#261;"
  ]
  node [
    id 1347
    label "osi&#261;ganie"
  ]
  node [
    id 1348
    label "dzianie_si&#281;"
  ]
  node [
    id 1349
    label "podbijanie"
  ]
  node [
    id 1350
    label "podporz&#261;dkowanie"
  ]
  node [
    id 1351
    label "control"
  ]
  node [
    id 1352
    label "powstrzymanie"
  ]
  node [
    id 1353
    label "uzyskanie"
  ]
  node [
    id 1354
    label "dochrapanie_si&#281;"
  ]
  node [
    id 1355
    label "accomplishment"
  ]
  node [
    id 1356
    label "sukces"
  ]
  node [
    id 1357
    label "dotarcie"
  ]
  node [
    id 1358
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 1359
    label "ranny"
  ]
  node [
    id 1360
    label "jajko"
  ]
  node [
    id 1361
    label "zgromadzenie"
  ]
  node [
    id 1362
    label "urodzenie"
  ]
  node [
    id 1363
    label "suspension"
  ]
  node [
    id 1364
    label "poddanie_si&#281;"
  ]
  node [
    id 1365
    label "extinction"
  ]
  node [
    id 1366
    label "coitus_interruptus"
  ]
  node [
    id 1367
    label "przetrwanie"
  ]
  node [
    id 1368
    label "&#347;cierpienie"
  ]
  node [
    id 1369
    label "abolicjonista"
  ]
  node [
    id 1370
    label "zniszczenie"
  ]
  node [
    id 1371
    label "posk&#322;adanie"
  ]
  node [
    id 1372
    label "zebranie"
  ]
  node [
    id 1373
    label "removal"
  ]
  node [
    id 1374
    label "withdrawal"
  ]
  node [
    id 1375
    label "revocation"
  ]
  node [
    id 1376
    label "usuni&#281;cie"
  ]
  node [
    id 1377
    label "porwanie"
  ]
  node [
    id 1378
    label "uniewa&#380;nienie"
  ]
  node [
    id 1379
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1380
    label "rytm"
  ]
  node [
    id 1381
    label "osobno"
  ]
  node [
    id 1382
    label "r&#243;&#380;ny"
  ]
  node [
    id 1383
    label "inszy"
  ]
  node [
    id 1384
    label "inaczej"
  ]
  node [
    id 1385
    label "odr&#281;bny"
  ]
  node [
    id 1386
    label "jaki&#347;"
  ]
  node [
    id 1387
    label "r&#243;&#380;nie"
  ]
  node [
    id 1388
    label "niestandardowo"
  ]
  node [
    id 1389
    label "individually"
  ]
  node [
    id 1390
    label "udzielnie"
  ]
  node [
    id 1391
    label "osobnie"
  ]
  node [
    id 1392
    label "odr&#281;bnie"
  ]
  node [
    id 1393
    label "osobny"
  ]
  node [
    id 1394
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 1395
    label "touchpad"
  ]
  node [
    id 1396
    label "Ultrabook"
  ]
  node [
    id 1397
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 1398
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 1399
    label "projektor"
  ]
  node [
    id 1400
    label "slide_projector"
  ]
  node [
    id 1401
    label "operatornia"
  ]
  node [
    id 1402
    label "projector"
  ]
  node [
    id 1403
    label "przyrz&#261;d"
  ]
  node [
    id 1404
    label "kondensor"
  ]
  node [
    id 1405
    label "multimedialnie"
  ]
  node [
    id 1406
    label "bonanza"
  ]
  node [
    id 1407
    label "przysparza&#263;"
  ]
  node [
    id 1408
    label "kali&#263;_si&#281;"
  ]
  node [
    id 1409
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1410
    label "enlarge"
  ]
  node [
    id 1411
    label "dodawa&#263;"
  ]
  node [
    id 1412
    label "&#378;r&#243;d&#322;o_dochodu"
  ]
  node [
    id 1413
    label "bieganina"
  ]
  node [
    id 1414
    label "jazda"
  ]
  node [
    id 1415
    label "heca"
  ]
  node [
    id 1416
    label "interes"
  ]
  node [
    id 1417
    label "&#380;y&#322;a_z&#322;ota"
  ]
  node [
    id 1418
    label "doba"
  ]
  node [
    id 1419
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 1420
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 1421
    label "teraz"
  ]
  node [
    id 1422
    label "jednocze&#347;nie"
  ]
  node [
    id 1423
    label "tydzie&#324;"
  ]
  node [
    id 1424
    label "noc"
  ]
  node [
    id 1425
    label "godzina"
  ]
  node [
    id 1426
    label "long_time"
  ]
  node [
    id 1427
    label "pozazdro&#347;ci&#263;"
  ]
  node [
    id 1428
    label "cheep"
  ]
  node [
    id 1429
    label "odwiedzi&#263;"
  ]
  node [
    id 1430
    label "spojrze&#263;"
  ]
  node [
    id 1431
    label "zinterpretowa&#263;"
  ]
  node [
    id 1432
    label "spoziera&#263;"
  ]
  node [
    id 1433
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 1434
    label "popatrze&#263;"
  ]
  node [
    id 1435
    label "pojrze&#263;"
  ]
  node [
    id 1436
    label "peek"
  ]
  node [
    id 1437
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 1438
    label "zawita&#263;"
  ]
  node [
    id 1439
    label "kartka"
  ]
  node [
    id 1440
    label "logowanie"
  ]
  node [
    id 1441
    label "plik"
  ]
  node [
    id 1442
    label "adres_internetowy"
  ]
  node [
    id 1443
    label "linia"
  ]
  node [
    id 1444
    label "serwis_internetowy"
  ]
  node [
    id 1445
    label "bok"
  ]
  node [
    id 1446
    label "skr&#281;canie"
  ]
  node [
    id 1447
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1448
    label "orientowanie"
  ]
  node [
    id 1449
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1450
    label "uj&#281;cie"
  ]
  node [
    id 1451
    label "zorientowanie"
  ]
  node [
    id 1452
    label "ty&#322;"
  ]
  node [
    id 1453
    label "fragment"
  ]
  node [
    id 1454
    label "layout"
  ]
  node [
    id 1455
    label "zorientowa&#263;"
  ]
  node [
    id 1456
    label "pagina"
  ]
  node [
    id 1457
    label "g&#243;ra"
  ]
  node [
    id 1458
    label "orientowa&#263;"
  ]
  node [
    id 1459
    label "voice"
  ]
  node [
    id 1460
    label "orientacja"
  ]
  node [
    id 1461
    label "prz&#243;d"
  ]
  node [
    id 1462
    label "internet"
  ]
  node [
    id 1463
    label "powierzchnia"
  ]
  node [
    id 1464
    label "forma"
  ]
  node [
    id 1465
    label "skr&#281;cenie"
  ]
  node [
    id 1466
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 1467
    label "byt"
  ]
  node [
    id 1468
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1469
    label "prawo"
  ]
  node [
    id 1470
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 1471
    label "nauka_prawa"
  ]
  node [
    id 1472
    label "utw&#243;r"
  ]
  node [
    id 1473
    label "charakterystyka"
  ]
  node [
    id 1474
    label "zaistnie&#263;"
  ]
  node [
    id 1475
    label "Osjan"
  ]
  node [
    id 1476
    label "kto&#347;"
  ]
  node [
    id 1477
    label "wygl&#261;d"
  ]
  node [
    id 1478
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1479
    label "trim"
  ]
  node [
    id 1480
    label "poby&#263;"
  ]
  node [
    id 1481
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1482
    label "Aspazja"
  ]
  node [
    id 1483
    label "punkt_widzenia"
  ]
  node [
    id 1484
    label "kompleksja"
  ]
  node [
    id 1485
    label "wytrzyma&#263;"
  ]
  node [
    id 1486
    label "budowa"
  ]
  node [
    id 1487
    label "pozosta&#263;"
  ]
  node [
    id 1488
    label "go&#347;&#263;"
  ]
  node [
    id 1489
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1490
    label "armia"
  ]
  node [
    id 1491
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1492
    label "poprowadzi&#263;"
  ]
  node [
    id 1493
    label "cord"
  ]
  node [
    id 1494
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1495
    label "trasa"
  ]
  node [
    id 1496
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1497
    label "tract"
  ]
  node [
    id 1498
    label "materia&#322;_zecerski"
  ]
  node [
    id 1499
    label "przeorientowywanie"
  ]
  node [
    id 1500
    label "curve"
  ]
  node [
    id 1501
    label "figura_geometryczna"
  ]
  node [
    id 1502
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 1503
    label "jard"
  ]
  node [
    id 1504
    label "szczep"
  ]
  node [
    id 1505
    label "phreaker"
  ]
  node [
    id 1506
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 1507
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1508
    label "prowadzi&#263;"
  ]
  node [
    id 1509
    label "przeorientowywa&#263;"
  ]
  node [
    id 1510
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1511
    label "access"
  ]
  node [
    id 1512
    label "przeorientowanie"
  ]
  node [
    id 1513
    label "przeorientowa&#263;"
  ]
  node [
    id 1514
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 1515
    label "billing"
  ]
  node [
    id 1516
    label "granica"
  ]
  node [
    id 1517
    label "szpaler"
  ]
  node [
    id 1518
    label "sztrych"
  ]
  node [
    id 1519
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1520
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1521
    label "drzewo_genealogiczne"
  ]
  node [
    id 1522
    label "transporter"
  ]
  node [
    id 1523
    label "line"
  ]
  node [
    id 1524
    label "przew&#243;d"
  ]
  node [
    id 1525
    label "granice"
  ]
  node [
    id 1526
    label "kontakt"
  ]
  node [
    id 1527
    label "przewo&#378;nik"
  ]
  node [
    id 1528
    label "przystanek"
  ]
  node [
    id 1529
    label "linijka"
  ]
  node [
    id 1530
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1531
    label "coalescence"
  ]
  node [
    id 1532
    label "Ural"
  ]
  node [
    id 1533
    label "bearing"
  ]
  node [
    id 1534
    label "prowadzenie"
  ]
  node [
    id 1535
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1536
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1537
    label "koniec"
  ]
  node [
    id 1538
    label "podkatalog"
  ]
  node [
    id 1539
    label "nadpisa&#263;"
  ]
  node [
    id 1540
    label "nadpisanie"
  ]
  node [
    id 1541
    label "bundle"
  ]
  node [
    id 1542
    label "folder"
  ]
  node [
    id 1543
    label "nadpisywanie"
  ]
  node [
    id 1544
    label "paczka"
  ]
  node [
    id 1545
    label "nadpisywa&#263;"
  ]
  node [
    id 1546
    label "dokument"
  ]
  node [
    id 1547
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 1548
    label "Rzym_Zachodni"
  ]
  node [
    id 1549
    label "whole"
  ]
  node [
    id 1550
    label "element"
  ]
  node [
    id 1551
    label "Rzym_Wschodni"
  ]
  node [
    id 1552
    label "rozmiar"
  ]
  node [
    id 1553
    label "obszar"
  ]
  node [
    id 1554
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 1555
    label "zwierciad&#322;o"
  ]
  node [
    id 1556
    label "capacity"
  ]
  node [
    id 1557
    label "plane"
  ]
  node [
    id 1558
    label "temat"
  ]
  node [
    id 1559
    label "poznanie"
  ]
  node [
    id 1560
    label "leksem"
  ]
  node [
    id 1561
    label "dzie&#322;o"
  ]
  node [
    id 1562
    label "blaszka"
  ]
  node [
    id 1563
    label "kantyzm"
  ]
  node [
    id 1564
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1565
    label "do&#322;ek"
  ]
  node [
    id 1566
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1567
    label "formality"
  ]
  node [
    id 1568
    label "mode"
  ]
  node [
    id 1569
    label "morfem"
  ]
  node [
    id 1570
    label "rdze&#324;"
  ]
  node [
    id 1571
    label "kielich"
  ]
  node [
    id 1572
    label "ornamentyka"
  ]
  node [
    id 1573
    label "pasmo"
  ]
  node [
    id 1574
    label "naczynie"
  ]
  node [
    id 1575
    label "p&#322;at"
  ]
  node [
    id 1576
    label "maszyna_drukarska"
  ]
  node [
    id 1577
    label "style"
  ]
  node [
    id 1578
    label "linearno&#347;&#263;"
  ]
  node [
    id 1579
    label "spirala"
  ]
  node [
    id 1580
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1581
    label "October"
  ]
  node [
    id 1582
    label "p&#281;tla"
  ]
  node [
    id 1583
    label "arystotelizm"
  ]
  node [
    id 1584
    label "szablon"
  ]
  node [
    id 1585
    label "miniatura"
  ]
  node [
    id 1586
    label "podejrzany"
  ]
  node [
    id 1587
    label "s&#261;downictwo"
  ]
  node [
    id 1588
    label "court"
  ]
  node [
    id 1589
    label "forum"
  ]
  node [
    id 1590
    label "bronienie"
  ]
  node [
    id 1591
    label "oskar&#380;yciel"
  ]
  node [
    id 1592
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1593
    label "skazany"
  ]
  node [
    id 1594
    label "post&#281;powanie"
  ]
  node [
    id 1595
    label "broni&#263;"
  ]
  node [
    id 1596
    label "my&#347;l"
  ]
  node [
    id 1597
    label "pods&#261;dny"
  ]
  node [
    id 1598
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1599
    label "&#347;wiadek"
  ]
  node [
    id 1600
    label "procesowicz"
  ]
  node [
    id 1601
    label "pochwytanie"
  ]
  node [
    id 1602
    label "wording"
  ]
  node [
    id 1603
    label "capture"
  ]
  node [
    id 1604
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1605
    label "film"
  ]
  node [
    id 1606
    label "scena"
  ]
  node [
    id 1607
    label "zapisanie"
  ]
  node [
    id 1608
    label "prezentacja"
  ]
  node [
    id 1609
    label "zamkni&#281;cie"
  ]
  node [
    id 1610
    label "zaaresztowanie"
  ]
  node [
    id 1611
    label "eastern_hemisphere"
  ]
  node [
    id 1612
    label "kierunek"
  ]
  node [
    id 1613
    label "kierowa&#263;"
  ]
  node [
    id 1614
    label "inform"
  ]
  node [
    id 1615
    label "marshal"
  ]
  node [
    id 1616
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1617
    label "wyznacza&#263;"
  ]
  node [
    id 1618
    label "pomaga&#263;"
  ]
  node [
    id 1619
    label "tu&#322;&#243;w"
  ]
  node [
    id 1620
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 1621
    label "wielok&#261;t"
  ]
  node [
    id 1622
    label "odcinek"
  ]
  node [
    id 1623
    label "strzelba"
  ]
  node [
    id 1624
    label "lufa"
  ]
  node [
    id 1625
    label "&#347;ciana"
  ]
  node [
    id 1626
    label "wyznaczenie"
  ]
  node [
    id 1627
    label "przyczynienie_si&#281;"
  ]
  node [
    id 1628
    label "zwr&#243;cenie"
  ]
  node [
    id 1629
    label "zrozumienie"
  ]
  node [
    id 1630
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1631
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1632
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 1633
    label "zorientowanie_si&#281;"
  ]
  node [
    id 1634
    label "pogubienie_si&#281;"
  ]
  node [
    id 1635
    label "orientation"
  ]
  node [
    id 1636
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 1637
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 1638
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1639
    label "gubienie_si&#281;"
  ]
  node [
    id 1640
    label "turn"
  ]
  node [
    id 1641
    label "wrench"
  ]
  node [
    id 1642
    label "nawini&#281;cie"
  ]
  node [
    id 1643
    label "os&#322;abienie"
  ]
  node [
    id 1644
    label "odbicie"
  ]
  node [
    id 1645
    label "poskr&#281;canie"
  ]
  node [
    id 1646
    label "uraz"
  ]
  node [
    id 1647
    label "odchylenie_si&#281;"
  ]
  node [
    id 1648
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1649
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1650
    label "splecenie"
  ]
  node [
    id 1651
    label "turning"
  ]
  node [
    id 1652
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 1653
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1654
    label "sple&#347;&#263;"
  ]
  node [
    id 1655
    label "os&#322;abi&#263;"
  ]
  node [
    id 1656
    label "nawin&#261;&#263;"
  ]
  node [
    id 1657
    label "scali&#263;"
  ]
  node [
    id 1658
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 1659
    label "twist"
  ]
  node [
    id 1660
    label "splay"
  ]
  node [
    id 1661
    label "uszkodzi&#263;"
  ]
  node [
    id 1662
    label "break"
  ]
  node [
    id 1663
    label "flex"
  ]
  node [
    id 1664
    label "zaty&#322;"
  ]
  node [
    id 1665
    label "pupa"
  ]
  node [
    id 1666
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1667
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1668
    label "splata&#263;"
  ]
  node [
    id 1669
    label "throw"
  ]
  node [
    id 1670
    label "screw"
  ]
  node [
    id 1671
    label "scala&#263;"
  ]
  node [
    id 1672
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 1673
    label "przelezienie"
  ]
  node [
    id 1674
    label "&#347;piew"
  ]
  node [
    id 1675
    label "Synaj"
  ]
  node [
    id 1676
    label "wysoki"
  ]
  node [
    id 1677
    label "wzniesienie"
  ]
  node [
    id 1678
    label "pi&#281;tro"
  ]
  node [
    id 1679
    label "Ropa"
  ]
  node [
    id 1680
    label "kupa"
  ]
  node [
    id 1681
    label "przele&#378;&#263;"
  ]
  node [
    id 1682
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 1683
    label "karczek"
  ]
  node [
    id 1684
    label "rami&#261;czko"
  ]
  node [
    id 1685
    label "Jaworze"
  ]
  node [
    id 1686
    label "orient"
  ]
  node [
    id 1687
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 1688
    label "aim"
  ]
  node [
    id 1689
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 1690
    label "wyznaczy&#263;"
  ]
  node [
    id 1691
    label "przyczynianie_si&#281;"
  ]
  node [
    id 1692
    label "zwracanie"
  ]
  node [
    id 1693
    label "rozeznawanie"
  ]
  node [
    id 1694
    label "oznaczanie"
  ]
  node [
    id 1695
    label "odchylanie_si&#281;"
  ]
  node [
    id 1696
    label "kszta&#322;towanie"
  ]
  node [
    id 1697
    label "uprz&#281;dzenie"
  ]
  node [
    id 1698
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1699
    label "scalanie"
  ]
  node [
    id 1700
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 1701
    label "snucie"
  ]
  node [
    id 1702
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 1703
    label "tortuosity"
  ]
  node [
    id 1704
    label "odbijanie"
  ]
  node [
    id 1705
    label "contortion"
  ]
  node [
    id 1706
    label "splatanie"
  ]
  node [
    id 1707
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 1708
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 1709
    label "uwierzytelnienie"
  ]
  node [
    id 1710
    label "liczba"
  ]
  node [
    id 1711
    label "circumference"
  ]
  node [
    id 1712
    label "cyrkumferencja"
  ]
  node [
    id 1713
    label "provider"
  ]
  node [
    id 1714
    label "hipertekst"
  ]
  node [
    id 1715
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1716
    label "mem"
  ]
  node [
    id 1717
    label "grooming"
  ]
  node [
    id 1718
    label "gra_sieciowa"
  ]
  node [
    id 1719
    label "biznes_elektroniczny"
  ]
  node [
    id 1720
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1721
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1722
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1723
    label "netbook"
  ]
  node [
    id 1724
    label "e-hazard"
  ]
  node [
    id 1725
    label "podcast"
  ]
  node [
    id 1726
    label "co&#347;"
  ]
  node [
    id 1727
    label "thing"
  ]
  node [
    id 1728
    label "faul"
  ]
  node [
    id 1729
    label "s&#281;dzia"
  ]
  node [
    id 1730
    label "bon"
  ]
  node [
    id 1731
    label "ticket"
  ]
  node [
    id 1732
    label "arkusz"
  ]
  node [
    id 1733
    label "kartonik"
  ]
  node [
    id 1734
    label "pagination"
  ]
  node [
    id 1735
    label "numer"
  ]
  node [
    id 1736
    label "report"
  ]
  node [
    id 1737
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1738
    label "write"
  ]
  node [
    id 1739
    label "announce"
  ]
  node [
    id 1740
    label "nastawi&#263;"
  ]
  node [
    id 1741
    label "draw"
  ]
  node [
    id 1742
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 1743
    label "incorporate"
  ]
  node [
    id 1744
    label "obejrze&#263;"
  ]
  node [
    id 1745
    label "impersonate"
  ]
  node [
    id 1746
    label "dokoptowa&#263;"
  ]
  node [
    id 1747
    label "prosecute"
  ]
  node [
    id 1748
    label "uruchomi&#263;"
  ]
  node [
    id 1749
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1750
    label "cover"
  ]
  node [
    id 1751
    label "wynie&#347;&#263;"
  ]
  node [
    id 1752
    label "pieni&#261;dze"
  ]
  node [
    id 1753
    label "limit"
  ]
  node [
    id 1754
    label "wynosi&#263;"
  ]
  node [
    id 1755
    label "Mazowsze"
  ]
  node [
    id 1756
    label "skupienie"
  ]
  node [
    id 1757
    label "The_Beatles"
  ]
  node [
    id 1758
    label "zabudowania"
  ]
  node [
    id 1759
    label "group"
  ]
  node [
    id 1760
    label "zespolik"
  ]
  node [
    id 1761
    label "schorzenie"
  ]
  node [
    id 1762
    label "ro&#347;lina"
  ]
  node [
    id 1763
    label "Depeche_Mode"
  ]
  node [
    id 1764
    label "batch"
  ]
  node [
    id 1765
    label "ognisko"
  ]
  node [
    id 1766
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1767
    label "powalenie"
  ]
  node [
    id 1768
    label "odezwanie_si&#281;"
  ]
  node [
    id 1769
    label "atakowanie"
  ]
  node [
    id 1770
    label "grupa_ryzyka"
  ]
  node [
    id 1771
    label "przypadek"
  ]
  node [
    id 1772
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1773
    label "nabawienie_si&#281;"
  ]
  node [
    id 1774
    label "inkubacja"
  ]
  node [
    id 1775
    label "kryzys"
  ]
  node [
    id 1776
    label "powali&#263;"
  ]
  node [
    id 1777
    label "remisja"
  ]
  node [
    id 1778
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1779
    label "zajmowa&#263;"
  ]
  node [
    id 1780
    label "zaburzenie"
  ]
  node [
    id 1781
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1782
    label "badanie_histopatologiczne"
  ]
  node [
    id 1783
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1784
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1785
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1786
    label "odzywanie_si&#281;"
  ]
  node [
    id 1787
    label "diagnoza"
  ]
  node [
    id 1788
    label "atakowa&#263;"
  ]
  node [
    id 1789
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1790
    label "nabawianie_si&#281;"
  ]
  node [
    id 1791
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1792
    label "zajmowanie"
  ]
  node [
    id 1793
    label "agglomeration"
  ]
  node [
    id 1794
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 1795
    label "przegrupowanie"
  ]
  node [
    id 1796
    label "congestion"
  ]
  node [
    id 1797
    label "kupienie"
  ]
  node [
    id 1798
    label "concentration"
  ]
  node [
    id 1799
    label "kompleks"
  ]
  node [
    id 1800
    label "Polska"
  ]
  node [
    id 1801
    label "Kurpie"
  ]
  node [
    id 1802
    label "Mogielnica"
  ]
  node [
    id 1803
    label "uatrakcyjni&#263;"
  ]
  node [
    id 1804
    label "przewietrzy&#263;"
  ]
  node [
    id 1805
    label "regenerate"
  ]
  node [
    id 1806
    label "odtworzy&#263;"
  ]
  node [
    id 1807
    label "wymieni&#263;"
  ]
  node [
    id 1808
    label "odbudowa&#263;"
  ]
  node [
    id 1809
    label "odbudowywa&#263;"
  ]
  node [
    id 1810
    label "m&#322;odzi&#263;"
  ]
  node [
    id 1811
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 1812
    label "przewietrza&#263;"
  ]
  node [
    id 1813
    label "wymienia&#263;"
  ]
  node [
    id 1814
    label "odtwarza&#263;"
  ]
  node [
    id 1815
    label "odtwarzanie"
  ]
  node [
    id 1816
    label "uatrakcyjnianie"
  ]
  node [
    id 1817
    label "zast&#281;powanie"
  ]
  node [
    id 1818
    label "odbudowywanie"
  ]
  node [
    id 1819
    label "rejuvenation"
  ]
  node [
    id 1820
    label "m&#322;odszy"
  ]
  node [
    id 1821
    label "wymienienie"
  ]
  node [
    id 1822
    label "uatrakcyjnienie"
  ]
  node [
    id 1823
    label "odtworzenie"
  ]
  node [
    id 1824
    label "zbiorowisko"
  ]
  node [
    id 1825
    label "ro&#347;liny"
  ]
  node [
    id 1826
    label "p&#281;d"
  ]
  node [
    id 1827
    label "wegetowanie"
  ]
  node [
    id 1828
    label "zadziorek"
  ]
  node [
    id 1829
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1830
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1831
    label "do&#322;owa&#263;"
  ]
  node [
    id 1832
    label "wegetacja"
  ]
  node [
    id 1833
    label "owoc"
  ]
  node [
    id 1834
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1835
    label "strzyc"
  ]
  node [
    id 1836
    label "w&#322;&#243;kno"
  ]
  node [
    id 1837
    label "g&#322;uszenie"
  ]
  node [
    id 1838
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1839
    label "fitotron"
  ]
  node [
    id 1840
    label "bulwka"
  ]
  node [
    id 1841
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1842
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1843
    label "epiderma"
  ]
  node [
    id 1844
    label "gumoza"
  ]
  node [
    id 1845
    label "strzy&#380;enie"
  ]
  node [
    id 1846
    label "wypotnik"
  ]
  node [
    id 1847
    label "flawonoid"
  ]
  node [
    id 1848
    label "wyro&#347;le"
  ]
  node [
    id 1849
    label "do&#322;owanie"
  ]
  node [
    id 1850
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1851
    label "pora&#380;a&#263;"
  ]
  node [
    id 1852
    label "fitocenoza"
  ]
  node [
    id 1853
    label "hodowla"
  ]
  node [
    id 1854
    label "fotoautotrof"
  ]
  node [
    id 1855
    label "nieuleczalnie_chory"
  ]
  node [
    id 1856
    label "wegetowa&#263;"
  ]
  node [
    id 1857
    label "pochewka"
  ]
  node [
    id 1858
    label "sok"
  ]
  node [
    id 1859
    label "system_korzeniowy"
  ]
  node [
    id 1860
    label "zawi&#261;zek"
  ]
  node [
    id 1861
    label "intencja"
  ]
  node [
    id 1862
    label "device"
  ]
  node [
    id 1863
    label "program_u&#380;ytkowy"
  ]
  node [
    id 1864
    label "pomys&#322;"
  ]
  node [
    id 1865
    label "dokumentacja"
  ]
  node [
    id 1866
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 1867
    label "agreement"
  ]
  node [
    id 1868
    label "thinking"
  ]
  node [
    id 1869
    label "zapis"
  ]
  node [
    id 1870
    label "&#347;wiadectwo"
  ]
  node [
    id 1871
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1872
    label "parafa"
  ]
  node [
    id 1873
    label "raport&#243;wka"
  ]
  node [
    id 1874
    label "record"
  ]
  node [
    id 1875
    label "fascyku&#322;"
  ]
  node [
    id 1876
    label "registratura"
  ]
  node [
    id 1877
    label "artyku&#322;"
  ]
  node [
    id 1878
    label "writing"
  ]
  node [
    id 1879
    label "sygnatariusz"
  ]
  node [
    id 1880
    label "rysunek"
  ]
  node [
    id 1881
    label "obraz"
  ]
  node [
    id 1882
    label "reprezentacja"
  ]
  node [
    id 1883
    label "dekoracja"
  ]
  node [
    id 1884
    label "perspektywa"
  ]
  node [
    id 1885
    label "ekscerpcja"
  ]
  node [
    id 1886
    label "operat"
  ]
  node [
    id 1887
    label "kosztorys"
  ]
  node [
    id 1888
    label "pocz&#261;tki"
  ]
  node [
    id 1889
    label "ukradzenie"
  ]
  node [
    id 1890
    label "ukra&#347;&#263;"
  ]
  node [
    id 1891
    label "PIRE"
  ]
  node [
    id 1892
    label "&#8211;"
  ]
  node [
    id 1893
    label "priorytet"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 1891
  ]
  edge [
    source 16
    target 1892
  ]
  edge [
    source 16
    target 1893
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 476
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 334
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 37
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 376
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 515
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1891
  ]
  edge [
    source 17
    target 1892
  ]
  edge [
    source 17
    target 1893
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 1891
  ]
  edge [
    source 18
    target 1892
  ]
  edge [
    source 18
    target 1893
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 261
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 892
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1117
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 1120
  ]
  edge [
    source 21
    target 1121
  ]
  edge [
    source 21
    target 1122
  ]
  edge [
    source 21
    target 1123
  ]
  edge [
    source 21
    target 1124
  ]
  edge [
    source 21
    target 1125
  ]
  edge [
    source 21
    target 1126
  ]
  edge [
    source 21
    target 1127
  ]
  edge [
    source 21
    target 1128
  ]
  edge [
    source 21
    target 1129
  ]
  edge [
    source 21
    target 559
  ]
  edge [
    source 21
    target 1130
  ]
  edge [
    source 21
    target 1131
  ]
  edge [
    source 21
    target 486
  ]
  edge [
    source 21
    target 1132
  ]
  edge [
    source 21
    target 1133
  ]
  edge [
    source 21
    target 1134
  ]
  edge [
    source 21
    target 1135
  ]
  edge [
    source 21
    target 243
  ]
  edge [
    source 21
    target 1136
  ]
  edge [
    source 21
    target 1137
  ]
  edge [
    source 21
    target 1138
  ]
  edge [
    source 21
    target 1139
  ]
  edge [
    source 21
    target 1140
  ]
  edge [
    source 21
    target 1141
  ]
  edge [
    source 21
    target 1142
  ]
  edge [
    source 21
    target 1143
  ]
  edge [
    source 21
    target 1144
  ]
  edge [
    source 21
    target 1145
  ]
  edge [
    source 21
    target 1146
  ]
  edge [
    source 21
    target 1147
  ]
  edge [
    source 21
    target 1148
  ]
  edge [
    source 21
    target 1149
  ]
  edge [
    source 21
    target 1150
  ]
  edge [
    source 21
    target 1151
  ]
  edge [
    source 21
    target 1152
  ]
  edge [
    source 21
    target 1153
  ]
  edge [
    source 21
    target 1154
  ]
  edge [
    source 21
    target 1155
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 21
    target 1159
  ]
  edge [
    source 21
    target 1160
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 255
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 1168
  ]
  edge [
    source 21
    target 1169
  ]
  edge [
    source 21
    target 1170
  ]
  edge [
    source 21
    target 1171
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 403
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 363
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 324
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 353
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 963
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 704
  ]
  edge [
    source 22
    target 1164
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 615
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 851
  ]
  edge [
    source 22
    target 847
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 1198
  ]
  edge [
    source 22
    target 1199
  ]
  edge [
    source 22
    target 1200
  ]
  edge [
    source 22
    target 1201
  ]
  edge [
    source 22
    target 1202
  ]
  edge [
    source 22
    target 1203
  ]
  edge [
    source 22
    target 1204
  ]
  edge [
    source 22
    target 1205
  ]
  edge [
    source 22
    target 1206
  ]
  edge [
    source 22
    target 1207
  ]
  edge [
    source 22
    target 1208
  ]
  edge [
    source 22
    target 1209
  ]
  edge [
    source 22
    target 1210
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 1211
  ]
  edge [
    source 22
    target 864
  ]
  edge [
    source 22
    target 1212
  ]
  edge [
    source 22
    target 350
  ]
  edge [
    source 22
    target 1213
  ]
  edge [
    source 22
    target 1214
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 1215
  ]
  edge [
    source 22
    target 1216
  ]
  edge [
    source 22
    target 1217
  ]
  edge [
    source 22
    target 1218
  ]
  edge [
    source 22
    target 1219
  ]
  edge [
    source 22
    target 1220
  ]
  edge [
    source 22
    target 1221
  ]
  edge [
    source 22
    target 1222
  ]
  edge [
    source 22
    target 1223
  ]
  edge [
    source 22
    target 1224
  ]
  edge [
    source 22
    target 1225
  ]
  edge [
    source 22
    target 1226
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 1227
  ]
  edge [
    source 22
    target 1228
  ]
  edge [
    source 22
    target 345
  ]
  edge [
    source 22
    target 1229
  ]
  edge [
    source 22
    target 1230
  ]
  edge [
    source 22
    target 1231
  ]
  edge [
    source 22
    target 1232
  ]
  edge [
    source 22
    target 1233
  ]
  edge [
    source 22
    target 1234
  ]
  edge [
    source 22
    target 1235
  ]
  edge [
    source 22
    target 1236
  ]
  edge [
    source 22
    target 758
  ]
  edge [
    source 22
    target 1237
  ]
  edge [
    source 22
    target 1238
  ]
  edge [
    source 22
    target 1239
  ]
  edge [
    source 22
    target 1240
  ]
  edge [
    source 22
    target 1241
  ]
  edge [
    source 22
    target 1242
  ]
  edge [
    source 22
    target 1243
  ]
  edge [
    source 22
    target 1244
  ]
  edge [
    source 22
    target 1245
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 1246
  ]
  edge [
    source 22
    target 1247
  ]
  edge [
    source 22
    target 1248
  ]
  edge [
    source 22
    target 1249
  ]
  edge [
    source 22
    target 1250
  ]
  edge [
    source 22
    target 1251
  ]
  edge [
    source 22
    target 560
  ]
  edge [
    source 22
    target 1252
  ]
  edge [
    source 22
    target 562
  ]
  edge [
    source 22
    target 1253
  ]
  edge [
    source 22
    target 563
  ]
  edge [
    source 22
    target 1254
  ]
  edge [
    source 22
    target 1255
  ]
  edge [
    source 22
    target 565
  ]
  edge [
    source 22
    target 1256
  ]
  edge [
    source 22
    target 1257
  ]
  edge [
    source 22
    target 1258
  ]
  edge [
    source 22
    target 910
  ]
  edge [
    source 22
    target 1259
  ]
  edge [
    source 22
    target 328
  ]
  edge [
    source 22
    target 1260
  ]
  edge [
    source 22
    target 1261
  ]
  edge [
    source 22
    target 1262
  ]
  edge [
    source 22
    target 1263
  ]
  edge [
    source 22
    target 1264
  ]
  edge [
    source 22
    target 1265
  ]
  edge [
    source 22
    target 1266
  ]
  edge [
    source 22
    target 1267
  ]
  edge [
    source 22
    target 407
  ]
  edge [
    source 22
    target 277
  ]
  edge [
    source 22
    target 1268
  ]
  edge [
    source 22
    target 907
  ]
  edge [
    source 22
    target 258
  ]
  edge [
    source 22
    target 1269
  ]
  edge [
    source 22
    target 425
  ]
  edge [
    source 22
    target 1270
  ]
  edge [
    source 22
    target 1271
  ]
  edge [
    source 22
    target 1272
  ]
  edge [
    source 22
    target 283
  ]
  edge [
    source 22
    target 1273
  ]
  edge [
    source 22
    target 1274
  ]
  edge [
    source 22
    target 1275
  ]
  edge [
    source 22
    target 1276
  ]
  edge [
    source 22
    target 1277
  ]
  edge [
    source 22
    target 1278
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 1279
  ]
  edge [
    source 22
    target 1280
  ]
  edge [
    source 22
    target 1281
  ]
  edge [
    source 22
    target 1282
  ]
  edge [
    source 22
    target 1283
  ]
  edge [
    source 22
    target 1284
  ]
  edge [
    source 22
    target 1081
  ]
  edge [
    source 22
    target 856
  ]
  edge [
    source 22
    target 1285
  ]
  edge [
    source 22
    target 1286
  ]
  edge [
    source 22
    target 1287
  ]
  edge [
    source 22
    target 1288
  ]
  edge [
    source 22
    target 1289
  ]
  edge [
    source 22
    target 1290
  ]
  edge [
    source 22
    target 1291
  ]
  edge [
    source 22
    target 519
  ]
  edge [
    source 22
    target 861
  ]
  edge [
    source 22
    target 1292
  ]
  edge [
    source 22
    target 1293
  ]
  edge [
    source 22
    target 1294
  ]
  edge [
    source 22
    target 1295
  ]
  edge [
    source 22
    target 1296
  ]
  edge [
    source 22
    target 1297
  ]
  edge [
    source 22
    target 1298
  ]
  edge [
    source 22
    target 1299
  ]
  edge [
    source 22
    target 1300
  ]
  edge [
    source 22
    target 1301
  ]
  edge [
    source 22
    target 1302
  ]
  edge [
    source 22
    target 1303
  ]
  edge [
    source 22
    target 1304
  ]
  edge [
    source 22
    target 1305
  ]
  edge [
    source 22
    target 1306
  ]
  edge [
    source 22
    target 1307
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 1310
  ]
  edge [
    source 22
    target 727
  ]
  edge [
    source 22
    target 1311
  ]
  edge [
    source 22
    target 1312
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 22
    target 1317
  ]
  edge [
    source 22
    target 1318
  ]
  edge [
    source 22
    target 1319
  ]
  edge [
    source 22
    target 1320
  ]
  edge [
    source 22
    target 1321
  ]
  edge [
    source 22
    target 1322
  ]
  edge [
    source 22
    target 1323
  ]
  edge [
    source 22
    target 1324
  ]
  edge [
    source 22
    target 1325
  ]
  edge [
    source 22
    target 1326
  ]
  edge [
    source 22
    target 1327
  ]
  edge [
    source 22
    target 1328
  ]
  edge [
    source 22
    target 760
  ]
  edge [
    source 22
    target 1329
  ]
  edge [
    source 22
    target 1330
  ]
  edge [
    source 22
    target 1331
  ]
  edge [
    source 22
    target 1332
  ]
  edge [
    source 22
    target 1333
  ]
  edge [
    source 22
    target 1334
  ]
  edge [
    source 22
    target 1335
  ]
  edge [
    source 22
    target 1336
  ]
  edge [
    source 22
    target 1337
  ]
  edge [
    source 22
    target 1338
  ]
  edge [
    source 22
    target 1339
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1340
  ]
  edge [
    source 24
    target 1341
  ]
  edge [
    source 24
    target 1342
  ]
  edge [
    source 24
    target 390
  ]
  edge [
    source 24
    target 403
  ]
  edge [
    source 24
    target 415
  ]
  edge [
    source 24
    target 1343
  ]
  edge [
    source 24
    target 452
  ]
  edge [
    source 24
    target 353
  ]
  edge [
    source 24
    target 1344
  ]
  edge [
    source 24
    target 1345
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 1346
  ]
  edge [
    source 24
    target 1347
  ]
  edge [
    source 24
    target 1348
  ]
  edge [
    source 24
    target 1349
  ]
  edge [
    source 24
    target 1350
  ]
  edge [
    source 24
    target 1351
  ]
  edge [
    source 24
    target 1352
  ]
  edge [
    source 24
    target 1273
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 1353
  ]
  edge [
    source 24
    target 1354
  ]
  edge [
    source 24
    target 834
  ]
  edge [
    source 24
    target 1355
  ]
  edge [
    source 24
    target 1356
  ]
  edge [
    source 24
    target 886
  ]
  edge [
    source 24
    target 1357
  ]
  edge [
    source 24
    target 1117
  ]
  edge [
    source 24
    target 1358
  ]
  edge [
    source 24
    target 1359
  ]
  edge [
    source 24
    target 1360
  ]
  edge [
    source 24
    target 1361
  ]
  edge [
    source 24
    target 1362
  ]
  edge [
    source 24
    target 1363
  ]
  edge [
    source 24
    target 1364
  ]
  edge [
    source 24
    target 1365
  ]
  edge [
    source 24
    target 1366
  ]
  edge [
    source 24
    target 1367
  ]
  edge [
    source 24
    target 1368
  ]
  edge [
    source 24
    target 1369
  ]
  edge [
    source 24
    target 1370
  ]
  edge [
    source 24
    target 1371
  ]
  edge [
    source 24
    target 1372
  ]
  edge [
    source 24
    target 428
  ]
  edge [
    source 24
    target 1373
  ]
  edge [
    source 24
    target 1374
  ]
  edge [
    source 24
    target 1375
  ]
  edge [
    source 24
    target 1376
  ]
  edge [
    source 24
    target 1377
  ]
  edge [
    source 24
    target 1378
  ]
  edge [
    source 24
    target 1379
  ]
  edge [
    source 24
    target 1380
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 572
  ]
  edge [
    source 26
    target 1381
  ]
  edge [
    source 26
    target 1382
  ]
  edge [
    source 26
    target 1383
  ]
  edge [
    source 26
    target 1384
  ]
  edge [
    source 26
    target 1385
  ]
  edge [
    source 26
    target 597
  ]
  edge [
    source 26
    target 598
  ]
  edge [
    source 26
    target 599
  ]
  edge [
    source 26
    target 600
  ]
  edge [
    source 26
    target 1386
  ]
  edge [
    source 26
    target 1387
  ]
  edge [
    source 26
    target 1388
  ]
  edge [
    source 26
    target 1389
  ]
  edge [
    source 26
    target 1390
  ]
  edge [
    source 26
    target 1391
  ]
  edge [
    source 26
    target 1392
  ]
  edge [
    source 26
    target 1393
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1394
  ]
  edge [
    source 27
    target 1395
  ]
  edge [
    source 27
    target 1396
  ]
  edge [
    source 27
    target 1397
  ]
  edge [
    source 27
    target 1398
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1399
  ]
  edge [
    source 28
    target 1400
  ]
  edge [
    source 28
    target 1401
  ]
  edge [
    source 28
    target 1402
  ]
  edge [
    source 28
    target 1403
  ]
  edge [
    source 28
    target 1404
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1405
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1406
  ]
  edge [
    source 30
    target 1407
  ]
  edge [
    source 30
    target 1408
  ]
  edge [
    source 30
    target 563
  ]
  edge [
    source 30
    target 1409
  ]
  edge [
    source 30
    target 1410
  ]
  edge [
    source 30
    target 1411
  ]
  edge [
    source 30
    target 944
  ]
  edge [
    source 30
    target 1412
  ]
  edge [
    source 30
    target 1413
  ]
  edge [
    source 30
    target 1414
  ]
  edge [
    source 30
    target 1415
  ]
  edge [
    source 30
    target 1416
  ]
  edge [
    source 30
    target 1417
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1418
  ]
  edge [
    source 32
    target 1419
  ]
  edge [
    source 32
    target 1420
  ]
  edge [
    source 32
    target 592
  ]
  edge [
    source 32
    target 1421
  ]
  edge [
    source 32
    target 688
  ]
  edge [
    source 32
    target 578
  ]
  edge [
    source 32
    target 1422
  ]
  edge [
    source 32
    target 1423
  ]
  edge [
    source 32
    target 1424
  ]
  edge [
    source 32
    target 604
  ]
  edge [
    source 32
    target 1425
  ]
  edge [
    source 32
    target 1426
  ]
  edge [
    source 32
    target 818
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1427
  ]
  edge [
    source 33
    target 1428
  ]
  edge [
    source 33
    target 1429
  ]
  edge [
    source 33
    target 1430
  ]
  edge [
    source 33
    target 1431
  ]
  edge [
    source 33
    target 1432
  ]
  edge [
    source 33
    target 1433
  ]
  edge [
    source 33
    target 1434
  ]
  edge [
    source 33
    target 1435
  ]
  edge [
    source 33
    target 1436
  ]
  edge [
    source 33
    target 1118
  ]
  edge [
    source 33
    target 1437
  ]
  edge [
    source 33
    target 1080
  ]
  edge [
    source 33
    target 1438
  ]
  edge [
    source 33
    target 1097
  ]
  edge [
    source 34
    target 1439
  ]
  edge [
    source 34
    target 72
  ]
  edge [
    source 34
    target 1440
  ]
  edge [
    source 34
    target 1441
  ]
  edge [
    source 34
    target 153
  ]
  edge [
    source 34
    target 1442
  ]
  edge [
    source 34
    target 1443
  ]
  edge [
    source 34
    target 1444
  ]
  edge [
    source 34
    target 615
  ]
  edge [
    source 34
    target 1445
  ]
  edge [
    source 34
    target 1446
  ]
  edge [
    source 34
    target 1447
  ]
  edge [
    source 34
    target 1448
  ]
  edge [
    source 34
    target 1449
  ]
  edge [
    source 34
    target 1450
  ]
  edge [
    source 34
    target 1451
  ]
  edge [
    source 34
    target 1452
  ]
  edge [
    source 34
    target 661
  ]
  edge [
    source 34
    target 1453
  ]
  edge [
    source 34
    target 1454
  ]
  edge [
    source 34
    target 519
  ]
  edge [
    source 34
    target 1455
  ]
  edge [
    source 34
    target 1456
  ]
  edge [
    source 34
    target 977
  ]
  edge [
    source 34
    target 1457
  ]
  edge [
    source 34
    target 1458
  ]
  edge [
    source 34
    target 1459
  ]
  edge [
    source 34
    target 1460
  ]
  edge [
    source 34
    target 1461
  ]
  edge [
    source 34
    target 1462
  ]
  edge [
    source 34
    target 1463
  ]
  edge [
    source 34
    target 187
  ]
  edge [
    source 34
    target 1464
  ]
  edge [
    source 34
    target 1465
  ]
  edge [
    source 34
    target 1466
  ]
  edge [
    source 34
    target 1467
  ]
  edge [
    source 34
    target 537
  ]
  edge [
    source 34
    target 1468
  ]
  edge [
    source 34
    target 476
  ]
  edge [
    source 34
    target 1469
  ]
  edge [
    source 34
    target 1470
  ]
  edge [
    source 34
    target 1471
  ]
  edge [
    source 34
    target 1472
  ]
  edge [
    source 34
    target 1473
  ]
  edge [
    source 34
    target 1474
  ]
  edge [
    source 34
    target 1475
  ]
  edge [
    source 34
    target 71
  ]
  edge [
    source 34
    target 1476
  ]
  edge [
    source 34
    target 1477
  ]
  edge [
    source 34
    target 1478
  ]
  edge [
    source 34
    target 856
  ]
  edge [
    source 34
    target 1479
  ]
  edge [
    source 34
    target 1480
  ]
  edge [
    source 34
    target 1481
  ]
  edge [
    source 34
    target 1482
  ]
  edge [
    source 34
    target 1483
  ]
  edge [
    source 34
    target 1484
  ]
  edge [
    source 34
    target 1485
  ]
  edge [
    source 34
    target 1486
  ]
  edge [
    source 34
    target 634
  ]
  edge [
    source 34
    target 1487
  ]
  edge [
    source 34
    target 349
  ]
  edge [
    source 34
    target 900
  ]
  edge [
    source 34
    target 1488
  ]
  edge [
    source 34
    target 508
  ]
  edge [
    source 34
    target 1489
  ]
  edge [
    source 34
    target 1490
  ]
  edge [
    source 34
    target 1491
  ]
  edge [
    source 34
    target 1492
  ]
  edge [
    source 34
    target 1493
  ]
  edge [
    source 34
    target 1494
  ]
  edge [
    source 34
    target 1495
  ]
  edge [
    source 34
    target 1496
  ]
  edge [
    source 34
    target 1497
  ]
  edge [
    source 34
    target 1498
  ]
  edge [
    source 34
    target 1499
  ]
  edge [
    source 34
    target 533
  ]
  edge [
    source 34
    target 1500
  ]
  edge [
    source 34
    target 1501
  ]
  edge [
    source 34
    target 175
  ]
  edge [
    source 34
    target 1502
  ]
  edge [
    source 34
    target 1503
  ]
  edge [
    source 34
    target 1504
  ]
  edge [
    source 34
    target 1505
  ]
  edge [
    source 34
    target 1506
  ]
  edge [
    source 34
    target 1507
  ]
  edge [
    source 34
    target 1508
  ]
  edge [
    source 34
    target 1509
  ]
  edge [
    source 34
    target 1510
  ]
  edge [
    source 34
    target 1511
  ]
  edge [
    source 34
    target 1512
  ]
  edge [
    source 34
    target 1513
  ]
  edge [
    source 34
    target 1514
  ]
  edge [
    source 34
    target 1515
  ]
  edge [
    source 34
    target 1516
  ]
  edge [
    source 34
    target 1517
  ]
  edge [
    source 34
    target 1518
  ]
  edge [
    source 34
    target 1519
  ]
  edge [
    source 34
    target 1520
  ]
  edge [
    source 34
    target 1521
  ]
  edge [
    source 34
    target 1522
  ]
  edge [
    source 34
    target 1523
  ]
  edge [
    source 34
    target 1524
  ]
  edge [
    source 34
    target 1525
  ]
  edge [
    source 34
    target 1526
  ]
  edge [
    source 34
    target 665
  ]
  edge [
    source 34
    target 1527
  ]
  edge [
    source 34
    target 1528
  ]
  edge [
    source 34
    target 1529
  ]
  edge [
    source 34
    target 755
  ]
  edge [
    source 34
    target 1530
  ]
  edge [
    source 34
    target 1531
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 758
  ]
  edge [
    source 34
    target 1535
  ]
  edge [
    source 34
    target 1536
  ]
  edge [
    source 34
    target 1537
  ]
  edge [
    source 34
    target 1538
  ]
  edge [
    source 34
    target 1539
  ]
  edge [
    source 34
    target 1540
  ]
  edge [
    source 34
    target 1541
  ]
  edge [
    source 34
    target 1542
  ]
  edge [
    source 34
    target 1543
  ]
  edge [
    source 34
    target 1544
  ]
  edge [
    source 34
    target 1545
  ]
  edge [
    source 34
    target 1546
  ]
  edge [
    source 34
    target 376
  ]
  edge [
    source 34
    target 1547
  ]
  edge [
    source 34
    target 1548
  ]
  edge [
    source 34
    target 1549
  ]
  edge [
    source 34
    target 133
  ]
  edge [
    source 34
    target 1550
  ]
  edge [
    source 34
    target 1551
  ]
  edge [
    source 34
    target 1189
  ]
  edge [
    source 34
    target 1552
  ]
  edge [
    source 34
    target 1553
  ]
  edge [
    source 34
    target 372
  ]
  edge [
    source 34
    target 1554
  ]
  edge [
    source 34
    target 1555
  ]
  edge [
    source 34
    target 1556
  ]
  edge [
    source 34
    target 1557
  ]
  edge [
    source 34
    target 1558
  ]
  edge [
    source 34
    target 704
  ]
  edge [
    source 34
    target 1559
  ]
  edge [
    source 34
    target 1560
  ]
  edge [
    source 34
    target 1561
  ]
  edge [
    source 34
    target 45
  ]
  edge [
    source 34
    target 1562
  ]
  edge [
    source 34
    target 1563
  ]
  edge [
    source 34
    target 1564
  ]
  edge [
    source 34
    target 1565
  ]
  edge [
    source 34
    target 1566
  ]
  edge [
    source 34
    target 505
  ]
  edge [
    source 34
    target 1567
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 34
    target 1568
  ]
  edge [
    source 34
    target 1569
  ]
  edge [
    source 34
    target 1570
  ]
  edge [
    source 34
    target 1571
  ]
  edge [
    source 34
    target 1572
  ]
  edge [
    source 34
    target 1573
  ]
  edge [
    source 34
    target 836
  ]
  edge [
    source 34
    target 619
  ]
  edge [
    source 34
    target 1574
  ]
  edge [
    source 34
    target 1575
  ]
  edge [
    source 34
    target 1576
  ]
  edge [
    source 34
    target 1577
  ]
  edge [
    source 34
    target 1578
  ]
  edge [
    source 34
    target 901
  ]
  edge [
    source 34
    target 1579
  ]
  edge [
    source 34
    target 497
  ]
  edge [
    source 34
    target 1185
  ]
  edge [
    source 34
    target 1580
  ]
  edge [
    source 34
    target 628
  ]
  edge [
    source 34
    target 1581
  ]
  edge [
    source 34
    target 1218
  ]
  edge [
    source 34
    target 1582
  ]
  edge [
    source 34
    target 1583
  ]
  edge [
    source 34
    target 1584
  ]
  edge [
    source 34
    target 1585
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 1586
  ]
  edge [
    source 34
    target 1587
  ]
  edge [
    source 34
    target 674
  ]
  edge [
    source 34
    target 475
  ]
  edge [
    source 34
    target 1588
  ]
  edge [
    source 34
    target 1589
  ]
  edge [
    source 34
    target 1590
  ]
  edge [
    source 34
    target 482
  ]
  edge [
    source 34
    target 323
  ]
  edge [
    source 34
    target 1591
  ]
  edge [
    source 34
    target 1592
  ]
  edge [
    source 34
    target 1593
  ]
  edge [
    source 34
    target 1594
  ]
  edge [
    source 34
    target 1595
  ]
  edge [
    source 34
    target 1596
  ]
  edge [
    source 34
    target 1597
  ]
  edge [
    source 34
    target 1598
  ]
  edge [
    source 34
    target 965
  ]
  edge [
    source 34
    target 142
  ]
  edge [
    source 34
    target 467
  ]
  edge [
    source 34
    target 903
  ]
  edge [
    source 34
    target 905
  ]
  edge [
    source 34
    target 1599
  ]
  edge [
    source 34
    target 1600
  ]
  edge [
    source 34
    target 1601
  ]
  edge [
    source 34
    target 1602
  ]
  edge [
    source 34
    target 411
  ]
  edge [
    source 34
    target 1374
  ]
  edge [
    source 34
    target 1603
  ]
  edge [
    source 34
    target 389
  ]
  edge [
    source 34
    target 1604
  ]
  edge [
    source 34
    target 1605
  ]
  edge [
    source 34
    target 1606
  ]
  edge [
    source 34
    target 1607
  ]
  edge [
    source 34
    target 1608
  ]
  edge [
    source 34
    target 147
  ]
  edge [
    source 34
    target 1609
  ]
  edge [
    source 34
    target 424
  ]
  edge [
    source 34
    target 204
  ]
  edge [
    source 34
    target 1610
  ]
  edge [
    source 34
    target 410
  ]
  edge [
    source 34
    target 1611
  ]
  edge [
    source 34
    target 1612
  ]
  edge [
    source 34
    target 1613
  ]
  edge [
    source 34
    target 1614
  ]
  edge [
    source 34
    target 1615
  ]
  edge [
    source 34
    target 1616
  ]
  edge [
    source 34
    target 1617
  ]
  edge [
    source 34
    target 1618
  ]
  edge [
    source 34
    target 1619
  ]
  edge [
    source 34
    target 1620
  ]
  edge [
    source 34
    target 1621
  ]
  edge [
    source 34
    target 1622
  ]
  edge [
    source 34
    target 1623
  ]
  edge [
    source 34
    target 1624
  ]
  edge [
    source 34
    target 1625
  ]
  edge [
    source 34
    target 1626
  ]
  edge [
    source 34
    target 1627
  ]
  edge [
    source 34
    target 1628
  ]
  edge [
    source 34
    target 1629
  ]
  edge [
    source 34
    target 1630
  ]
  edge [
    source 34
    target 1631
  ]
  edge [
    source 34
    target 667
  ]
  edge [
    source 34
    target 1632
  ]
  edge [
    source 34
    target 1633
  ]
  edge [
    source 34
    target 1634
  ]
  edge [
    source 34
    target 1635
  ]
  edge [
    source 34
    target 1636
  ]
  edge [
    source 34
    target 1637
  ]
  edge [
    source 34
    target 1638
  ]
  edge [
    source 34
    target 1639
  ]
  edge [
    source 34
    target 1640
  ]
  edge [
    source 34
    target 1641
  ]
  edge [
    source 34
    target 1642
  ]
  edge [
    source 34
    target 1643
  ]
  edge [
    source 34
    target 210
  ]
  edge [
    source 34
    target 1644
  ]
  edge [
    source 34
    target 1645
  ]
  edge [
    source 34
    target 1646
  ]
  edge [
    source 34
    target 1647
  ]
  edge [
    source 34
    target 1648
  ]
  edge [
    source 34
    target 1649
  ]
  edge [
    source 34
    target 1650
  ]
  edge [
    source 34
    target 1651
  ]
  edge [
    source 34
    target 1652
  ]
  edge [
    source 34
    target 1653
  ]
  edge [
    source 34
    target 1654
  ]
  edge [
    source 34
    target 1655
  ]
  edge [
    source 34
    target 1656
  ]
  edge [
    source 34
    target 1657
  ]
  edge [
    source 34
    target 1658
  ]
  edge [
    source 34
    target 1659
  ]
  edge [
    source 34
    target 1660
  ]
  edge [
    source 34
    target 789
  ]
  edge [
    source 34
    target 1661
  ]
  edge [
    source 34
    target 1662
  ]
  edge [
    source 34
    target 1663
  ]
  edge [
    source 34
    target 659
  ]
  edge [
    source 34
    target 1664
  ]
  edge [
    source 34
    target 1665
  ]
  edge [
    source 34
    target 663
  ]
  edge [
    source 34
    target 81
  ]
  edge [
    source 34
    target 614
  ]
  edge [
    source 34
    target 1666
  ]
  edge [
    source 34
    target 1667
  ]
  edge [
    source 34
    target 1668
  ]
  edge [
    source 34
    target 1669
  ]
  edge [
    source 34
    target 1670
  ]
  edge [
    source 34
    target 66
  ]
  edge [
    source 34
    target 1671
  ]
  edge [
    source 34
    target 1672
  ]
  edge [
    source 34
    target 847
  ]
  edge [
    source 34
    target 1673
  ]
  edge [
    source 34
    target 1674
  ]
  edge [
    source 34
    target 1675
  ]
  edge [
    source 34
    target 841
  ]
  edge [
    source 34
    target 1174
  ]
  edge [
    source 34
    target 1676
  ]
  edge [
    source 34
    target 1677
  ]
  edge [
    source 34
    target 683
  ]
  edge [
    source 34
    target 1678
  ]
  edge [
    source 34
    target 1679
  ]
  edge [
    source 34
    target 1680
  ]
  edge [
    source 34
    target 1681
  ]
  edge [
    source 34
    target 1682
  ]
  edge [
    source 34
    target 1683
  ]
  edge [
    source 34
    target 1684
  ]
  edge [
    source 34
    target 1685
  ]
  edge [
    source 34
    target 864
  ]
  edge [
    source 34
    target 1686
  ]
  edge [
    source 34
    target 1687
  ]
  edge [
    source 34
    target 1688
  ]
  edge [
    source 34
    target 1689
  ]
  edge [
    source 34
    target 1690
  ]
  edge [
    source 34
    target 724
  ]
  edge [
    source 34
    target 1691
  ]
  edge [
    source 34
    target 1692
  ]
  edge [
    source 34
    target 1693
  ]
  edge [
    source 34
    target 1694
  ]
  edge [
    source 34
    target 1695
  ]
  edge [
    source 34
    target 1696
  ]
  edge [
    source 34
    target 618
  ]
  edge [
    source 34
    target 1697
  ]
  edge [
    source 34
    target 1698
  ]
  edge [
    source 34
    target 1699
  ]
  edge [
    source 34
    target 1700
  ]
  edge [
    source 34
    target 1701
  ]
  edge [
    source 34
    target 1702
  ]
  edge [
    source 34
    target 1703
  ]
  edge [
    source 34
    target 1704
  ]
  edge [
    source 34
    target 1705
  ]
  edge [
    source 34
    target 1706
  ]
  edge [
    source 34
    target 620
  ]
  edge [
    source 34
    target 1707
  ]
  edge [
    source 34
    target 1708
  ]
  edge [
    source 34
    target 1709
  ]
  edge [
    source 34
    target 1710
  ]
  edge [
    source 34
    target 1711
  ]
  edge [
    source 34
    target 1712
  ]
  edge [
    source 34
    target 116
  ]
  edge [
    source 34
    target 1713
  ]
  edge [
    source 34
    target 1714
  ]
  edge [
    source 34
    target 1715
  ]
  edge [
    source 34
    target 1716
  ]
  edge [
    source 34
    target 1717
  ]
  edge [
    source 34
    target 1718
  ]
  edge [
    source 34
    target 1719
  ]
  edge [
    source 34
    target 1720
  ]
  edge [
    source 34
    target 1721
  ]
  edge [
    source 34
    target 1722
  ]
  edge [
    source 34
    target 1723
  ]
  edge [
    source 34
    target 1724
  ]
  edge [
    source 34
    target 1725
  ]
  edge [
    source 34
    target 1726
  ]
  edge [
    source 34
    target 843
  ]
  edge [
    source 34
    target 1727
  ]
  edge [
    source 34
    target 338
  ]
  edge [
    source 34
    target 503
  ]
  edge [
    source 34
    target 1728
  ]
  edge [
    source 34
    target 1334
  ]
  edge [
    source 34
    target 919
  ]
  edge [
    source 34
    target 1729
  ]
  edge [
    source 34
    target 1730
  ]
  edge [
    source 34
    target 1731
  ]
  edge [
    source 34
    target 1732
  ]
  edge [
    source 34
    target 1733
  ]
  edge [
    source 34
    target 698
  ]
  edge [
    source 34
    target 1734
  ]
  edge [
    source 34
    target 370
  ]
  edge [
    source 34
    target 1735
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1736
  ]
  edge [
    source 35
    target 1737
  ]
  edge [
    source 35
    target 566
  ]
  edge [
    source 35
    target 1738
  ]
  edge [
    source 35
    target 1739
  ]
  edge [
    source 35
    target 1740
  ]
  edge [
    source 35
    target 1741
  ]
  edge [
    source 35
    target 1742
  ]
  edge [
    source 35
    target 1743
  ]
  edge [
    source 35
    target 1744
  ]
  edge [
    source 35
    target 1745
  ]
  edge [
    source 35
    target 1746
  ]
  edge [
    source 35
    target 1747
  ]
  edge [
    source 35
    target 1748
  ]
  edge [
    source 35
    target 1749
  ]
  edge [
    source 35
    target 1099
  ]
  edge [
    source 35
    target 1614
  ]
  edge [
    source 35
    target 570
  ]
  edge [
    source 35
    target 1026
  ]
  edge [
    source 35
    target 1750
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 133
  ]
  edge [
    source 36
    target 46
  ]
  edge [
    source 36
    target 760
  ]
  edge [
    source 36
    target 985
  ]
  edge [
    source 36
    target 45
  ]
  edge [
    source 36
    target 69
  ]
  edge [
    source 36
    target 70
  ]
  edge [
    source 36
    target 71
  ]
  edge [
    source 36
    target 1751
  ]
  edge [
    source 36
    target 1752
  ]
  edge [
    source 36
    target 1753
  ]
  edge [
    source 36
    target 1754
  ]
  edge [
    source 36
    target 376
  ]
  edge [
    source 36
    target 1552
  ]
  edge [
    source 36
    target 1288
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1755
  ]
  edge [
    source 37
    target 702
  ]
  edge [
    source 37
    target 717
  ]
  edge [
    source 37
    target 175
  ]
  edge [
    source 37
    target 1549
  ]
  edge [
    source 37
    target 1756
  ]
  edge [
    source 37
    target 1757
  ]
  edge [
    source 37
    target 720
  ]
  edge [
    source 37
    target 708
  ]
  edge [
    source 37
    target 1758
  ]
  edge [
    source 37
    target 1759
  ]
  edge [
    source 37
    target 1760
  ]
  edge [
    source 37
    target 1761
  ]
  edge [
    source 37
    target 1762
  ]
  edge [
    source 37
    target 683
  ]
  edge [
    source 37
    target 1763
  ]
  edge [
    source 37
    target 1764
  ]
  edge [
    source 37
    target 718
  ]
  edge [
    source 37
    target 703
  ]
  edge [
    source 37
    target 704
  ]
  edge [
    source 37
    target 611
  ]
  edge [
    source 37
    target 511
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 613
  ]
  edge [
    source 37
    target 363
  ]
  edge [
    source 37
    target 705
  ]
  edge [
    source 37
    target 706
  ]
  edge [
    source 37
    target 707
  ]
  edge [
    source 37
    target 709
  ]
  edge [
    source 37
    target 371
  ]
  edge [
    source 37
    target 710
  ]
  edge [
    source 37
    target 711
  ]
  edge [
    source 37
    target 712
  ]
  edge [
    source 37
    target 713
  ]
  edge [
    source 37
    target 714
  ]
  edge [
    source 37
    target 715
  ]
  edge [
    source 37
    target 716
  ]
  edge [
    source 37
    target 719
  ]
  edge [
    source 37
    target 721
  ]
  edge [
    source 37
    target 722
  ]
  edge [
    source 37
    target 364
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 37
    target 370
  ]
  edge [
    source 37
    target 372
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 374
  ]
  edge [
    source 37
    target 375
  ]
  edge [
    source 37
    target 377
  ]
  edge [
    source 37
    target 1765
  ]
  edge [
    source 37
    target 1766
  ]
  edge [
    source 37
    target 1767
  ]
  edge [
    source 37
    target 1768
  ]
  edge [
    source 37
    target 1769
  ]
  edge [
    source 37
    target 1770
  ]
  edge [
    source 37
    target 1771
  ]
  edge [
    source 37
    target 1772
  ]
  edge [
    source 37
    target 1773
  ]
  edge [
    source 37
    target 1774
  ]
  edge [
    source 37
    target 1775
  ]
  edge [
    source 37
    target 1776
  ]
  edge [
    source 37
    target 1777
  ]
  edge [
    source 37
    target 1778
  ]
  edge [
    source 37
    target 1779
  ]
  edge [
    source 37
    target 1780
  ]
  edge [
    source 37
    target 1781
  ]
  edge [
    source 37
    target 1782
  ]
  edge [
    source 37
    target 1783
  ]
  edge [
    source 37
    target 1784
  ]
  edge [
    source 37
    target 1785
  ]
  edge [
    source 37
    target 1786
  ]
  edge [
    source 37
    target 1787
  ]
  edge [
    source 37
    target 1788
  ]
  edge [
    source 37
    target 1789
  ]
  edge [
    source 37
    target 1790
  ]
  edge [
    source 37
    target 1791
  ]
  edge [
    source 37
    target 1792
  ]
  edge [
    source 37
    target 1793
  ]
  edge [
    source 37
    target 658
  ]
  edge [
    source 37
    target 1794
  ]
  edge [
    source 37
    target 1795
  ]
  edge [
    source 37
    target 279
  ]
  edge [
    source 37
    target 1796
  ]
  edge [
    source 37
    target 1361
  ]
  edge [
    source 37
    target 1797
  ]
  edge [
    source 37
    target 1649
  ]
  edge [
    source 37
    target 183
  ]
  edge [
    source 37
    target 1496
  ]
  edge [
    source 37
    target 1798
  ]
  edge [
    source 37
    target 1799
  ]
  edge [
    source 37
    target 1553
  ]
  edge [
    source 37
    target 1800
  ]
  edge [
    source 37
    target 1801
  ]
  edge [
    source 37
    target 1802
  ]
  edge [
    source 37
    target 1803
  ]
  edge [
    source 37
    target 1804
  ]
  edge [
    source 37
    target 1805
  ]
  edge [
    source 37
    target 1806
  ]
  edge [
    source 37
    target 1807
  ]
  edge [
    source 37
    target 1808
  ]
  edge [
    source 37
    target 1809
  ]
  edge [
    source 37
    target 1810
  ]
  edge [
    source 37
    target 1811
  ]
  edge [
    source 37
    target 1812
  ]
  edge [
    source 37
    target 1813
  ]
  edge [
    source 37
    target 1814
  ]
  edge [
    source 37
    target 1815
  ]
  edge [
    source 37
    target 1816
  ]
  edge [
    source 37
    target 1817
  ]
  edge [
    source 37
    target 1818
  ]
  edge [
    source 37
    target 1819
  ]
  edge [
    source 37
    target 1820
  ]
  edge [
    source 37
    target 1821
  ]
  edge [
    source 37
    target 1822
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 1823
  ]
  edge [
    source 37
    target 1824
  ]
  edge [
    source 37
    target 1825
  ]
  edge [
    source 37
    target 1826
  ]
  edge [
    source 37
    target 1827
  ]
  edge [
    source 37
    target 1828
  ]
  edge [
    source 37
    target 1829
  ]
  edge [
    source 37
    target 1830
  ]
  edge [
    source 37
    target 1831
  ]
  edge [
    source 37
    target 1832
  ]
  edge [
    source 37
    target 1833
  ]
  edge [
    source 37
    target 1834
  ]
  edge [
    source 37
    target 1835
  ]
  edge [
    source 37
    target 1836
  ]
  edge [
    source 37
    target 1837
  ]
  edge [
    source 37
    target 1838
  ]
  edge [
    source 37
    target 1839
  ]
  edge [
    source 37
    target 1840
  ]
  edge [
    source 37
    target 1841
  ]
  edge [
    source 37
    target 1842
  ]
  edge [
    source 37
    target 1843
  ]
  edge [
    source 37
    target 1844
  ]
  edge [
    source 37
    target 1845
  ]
  edge [
    source 37
    target 1846
  ]
  edge [
    source 37
    target 1847
  ]
  edge [
    source 37
    target 1848
  ]
  edge [
    source 37
    target 1849
  ]
  edge [
    source 37
    target 1850
  ]
  edge [
    source 37
    target 1851
  ]
  edge [
    source 37
    target 1852
  ]
  edge [
    source 37
    target 1853
  ]
  edge [
    source 37
    target 1854
  ]
  edge [
    source 37
    target 1855
  ]
  edge [
    source 37
    target 1856
  ]
  edge [
    source 37
    target 1857
  ]
  edge [
    source 37
    target 1858
  ]
  edge [
    source 37
    target 1859
  ]
  edge [
    source 37
    target 1860
  ]
  edge [
    source 38
    target 1861
  ]
  edge [
    source 38
    target 1126
  ]
  edge [
    source 38
    target 1862
  ]
  edge [
    source 38
    target 1863
  ]
  edge [
    source 38
    target 1864
  ]
  edge [
    source 38
    target 1865
  ]
  edge [
    source 38
    target 1866
  ]
  edge [
    source 38
    target 1867
  ]
  edge [
    source 38
    target 1546
  ]
  edge [
    source 38
    target 856
  ]
  edge [
    source 38
    target 1868
  ]
  edge [
    source 38
    target 1869
  ]
  edge [
    source 38
    target 1870
  ]
  edge [
    source 38
    target 1871
  ]
  edge [
    source 38
    target 1872
  ]
  edge [
    source 38
    target 1441
  ]
  edge [
    source 38
    target 1873
  ]
  edge [
    source 38
    target 1472
  ]
  edge [
    source 38
    target 1874
  ]
  edge [
    source 38
    target 1875
  ]
  edge [
    source 38
    target 1876
  ]
  edge [
    source 38
    target 1877
  ]
  edge [
    source 38
    target 1878
  ]
  edge [
    source 38
    target 1879
  ]
  edge [
    source 38
    target 808
  ]
  edge [
    source 38
    target 122
  ]
  edge [
    source 38
    target 1880
  ]
  edge [
    source 38
    target 839
  ]
  edge [
    source 38
    target 659
  ]
  edge [
    source 38
    target 1881
  ]
  edge [
    source 38
    target 1882
  ]
  edge [
    source 38
    target 1883
  ]
  edge [
    source 38
    target 1884
  ]
  edge [
    source 38
    target 1885
  ]
  edge [
    source 38
    target 754
  ]
  edge [
    source 38
    target 1886
  ]
  edge [
    source 38
    target 1887
  ]
  edge [
    source 38
    target 912
  ]
  edge [
    source 38
    target 1888
  ]
  edge [
    source 38
    target 1889
  ]
  edge [
    source 38
    target 1890
  ]
  edge [
    source 38
    target 674
  ]
  edge [
    source 1891
    target 1892
  ]
  edge [
    source 1891
    target 1893
  ]
  edge [
    source 1892
    target 1893
  ]
]
