graph [
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "lasek"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "chodzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "liceum"
    origin "text"
  ]
  node [
    id 5
    label "formu&#322;owa&#263;"
  ]
  node [
    id 6
    label "ozdabia&#263;"
  ]
  node [
    id 7
    label "stawia&#263;"
  ]
  node [
    id 8
    label "spell"
  ]
  node [
    id 9
    label "styl"
  ]
  node [
    id 10
    label "skryba"
  ]
  node [
    id 11
    label "read"
  ]
  node [
    id 12
    label "donosi&#263;"
  ]
  node [
    id 13
    label "code"
  ]
  node [
    id 14
    label "tekst"
  ]
  node [
    id 15
    label "dysgrafia"
  ]
  node [
    id 16
    label "dysortografia"
  ]
  node [
    id 17
    label "tworzy&#263;"
  ]
  node [
    id 18
    label "prasa"
  ]
  node [
    id 19
    label "robi&#263;"
  ]
  node [
    id 20
    label "pope&#322;nia&#263;"
  ]
  node [
    id 21
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 22
    label "wytwarza&#263;"
  ]
  node [
    id 23
    label "get"
  ]
  node [
    id 24
    label "consist"
  ]
  node [
    id 25
    label "stanowi&#263;"
  ]
  node [
    id 26
    label "raise"
  ]
  node [
    id 27
    label "spill_the_beans"
  ]
  node [
    id 28
    label "przeby&#263;"
  ]
  node [
    id 29
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 30
    label "zanosi&#263;"
  ]
  node [
    id 31
    label "inform"
  ]
  node [
    id 32
    label "give"
  ]
  node [
    id 33
    label "zu&#380;y&#263;"
  ]
  node [
    id 34
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 35
    label "introduce"
  ]
  node [
    id 36
    label "render"
  ]
  node [
    id 37
    label "ci&#261;&#380;a"
  ]
  node [
    id 38
    label "informowa&#263;"
  ]
  node [
    id 39
    label "komunikowa&#263;"
  ]
  node [
    id 40
    label "convey"
  ]
  node [
    id 41
    label "pozostawia&#263;"
  ]
  node [
    id 42
    label "czyni&#263;"
  ]
  node [
    id 43
    label "wydawa&#263;"
  ]
  node [
    id 44
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 45
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 46
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 47
    label "przewidywa&#263;"
  ]
  node [
    id 48
    label "przyznawa&#263;"
  ]
  node [
    id 49
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 50
    label "go"
  ]
  node [
    id 51
    label "obstawia&#263;"
  ]
  node [
    id 52
    label "umieszcza&#263;"
  ]
  node [
    id 53
    label "ocenia&#263;"
  ]
  node [
    id 54
    label "zastawia&#263;"
  ]
  node [
    id 55
    label "stanowisko"
  ]
  node [
    id 56
    label "znak"
  ]
  node [
    id 57
    label "wskazywa&#263;"
  ]
  node [
    id 58
    label "uruchamia&#263;"
  ]
  node [
    id 59
    label "fundowa&#263;"
  ]
  node [
    id 60
    label "zmienia&#263;"
  ]
  node [
    id 61
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 62
    label "deliver"
  ]
  node [
    id 63
    label "powodowa&#263;"
  ]
  node [
    id 64
    label "wyznacza&#263;"
  ]
  node [
    id 65
    label "przedstawia&#263;"
  ]
  node [
    id 66
    label "wydobywa&#263;"
  ]
  node [
    id 67
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 68
    label "trim"
  ]
  node [
    id 69
    label "gryzipi&#243;rek"
  ]
  node [
    id 70
    label "cz&#322;owiek"
  ]
  node [
    id 71
    label "pisarz"
  ]
  node [
    id 72
    label "ekscerpcja"
  ]
  node [
    id 73
    label "j&#281;zykowo"
  ]
  node [
    id 74
    label "wypowied&#378;"
  ]
  node [
    id 75
    label "redakcja"
  ]
  node [
    id 76
    label "wytw&#243;r"
  ]
  node [
    id 77
    label "pomini&#281;cie"
  ]
  node [
    id 78
    label "dzie&#322;o"
  ]
  node [
    id 79
    label "preparacja"
  ]
  node [
    id 80
    label "odmianka"
  ]
  node [
    id 81
    label "opu&#347;ci&#263;"
  ]
  node [
    id 82
    label "koniektura"
  ]
  node [
    id 83
    label "obelga"
  ]
  node [
    id 84
    label "zesp&#243;&#322;"
  ]
  node [
    id 85
    label "t&#322;oczysko"
  ]
  node [
    id 86
    label "depesza"
  ]
  node [
    id 87
    label "maszyna"
  ]
  node [
    id 88
    label "media"
  ]
  node [
    id 89
    label "napisa&#263;"
  ]
  node [
    id 90
    label "czasopismo"
  ]
  node [
    id 91
    label "dziennikarz_prasowy"
  ]
  node [
    id 92
    label "kiosk"
  ]
  node [
    id 93
    label "maszyna_rolnicza"
  ]
  node [
    id 94
    label "gazeta"
  ]
  node [
    id 95
    label "trzonek"
  ]
  node [
    id 96
    label "reakcja"
  ]
  node [
    id 97
    label "narz&#281;dzie"
  ]
  node [
    id 98
    label "spos&#243;b"
  ]
  node [
    id 99
    label "zbi&#243;r"
  ]
  node [
    id 100
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 101
    label "zachowanie"
  ]
  node [
    id 102
    label "stylik"
  ]
  node [
    id 103
    label "dyscyplina_sportowa"
  ]
  node [
    id 104
    label "handle"
  ]
  node [
    id 105
    label "stroke"
  ]
  node [
    id 106
    label "line"
  ]
  node [
    id 107
    label "charakter"
  ]
  node [
    id 108
    label "natural_language"
  ]
  node [
    id 109
    label "kanon"
  ]
  node [
    id 110
    label "behawior"
  ]
  node [
    id 111
    label "dysleksja"
  ]
  node [
    id 112
    label "pisanie"
  ]
  node [
    id 113
    label "dysgraphia"
  ]
  node [
    id 114
    label "szko&#322;a_ponadgimnazjalna"
  ]
  node [
    id 115
    label "szko&#322;a_&#347;rednia"
  ]
  node [
    id 116
    label "szko&#322;a"
  ]
  node [
    id 117
    label "do&#347;wiadczenie"
  ]
  node [
    id 118
    label "teren_szko&#322;y"
  ]
  node [
    id 119
    label "wiedza"
  ]
  node [
    id 120
    label "Mickiewicz"
  ]
  node [
    id 121
    label "kwalifikacje"
  ]
  node [
    id 122
    label "podr&#281;cznik"
  ]
  node [
    id 123
    label "absolwent"
  ]
  node [
    id 124
    label "praktyka"
  ]
  node [
    id 125
    label "school"
  ]
  node [
    id 126
    label "system"
  ]
  node [
    id 127
    label "zda&#263;"
  ]
  node [
    id 128
    label "gabinet"
  ]
  node [
    id 129
    label "urszulanki"
  ]
  node [
    id 130
    label "sztuba"
  ]
  node [
    id 131
    label "&#322;awa_szkolna"
  ]
  node [
    id 132
    label "nauka"
  ]
  node [
    id 133
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 134
    label "przepisa&#263;"
  ]
  node [
    id 135
    label "muzyka"
  ]
  node [
    id 136
    label "grupa"
  ]
  node [
    id 137
    label "form"
  ]
  node [
    id 138
    label "klasa"
  ]
  node [
    id 139
    label "lekcja"
  ]
  node [
    id 140
    label "metoda"
  ]
  node [
    id 141
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 142
    label "przepisanie"
  ]
  node [
    id 143
    label "czas"
  ]
  node [
    id 144
    label "skolaryzacja"
  ]
  node [
    id 145
    label "zdanie"
  ]
  node [
    id 146
    label "stopek"
  ]
  node [
    id 147
    label "sekretariat"
  ]
  node [
    id 148
    label "ideologia"
  ]
  node [
    id 149
    label "lesson"
  ]
  node [
    id 150
    label "instytucja"
  ]
  node [
    id 151
    label "niepokalanki"
  ]
  node [
    id 152
    label "siedziba"
  ]
  node [
    id 153
    label "szkolenie"
  ]
  node [
    id 154
    label "kara"
  ]
  node [
    id 155
    label "tablica"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
]
