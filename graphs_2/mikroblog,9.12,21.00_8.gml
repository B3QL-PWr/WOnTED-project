graph [
  node [
    id 0
    label "&#347;liczny"
    origin "text"
  ]
  node [
    id 1
    label "&#347;licznie"
  ]
  node [
    id 2
    label "skandaliczny"
  ]
  node [
    id 3
    label "wspania&#322;y"
  ]
  node [
    id 4
    label "przyjemny"
  ]
  node [
    id 5
    label "pi&#281;knie"
  ]
  node [
    id 6
    label "dobry"
  ]
  node [
    id 7
    label "z&#322;y"
  ]
  node [
    id 8
    label "dobroczynny"
  ]
  node [
    id 9
    label "czw&#243;rka"
  ]
  node [
    id 10
    label "spokojny"
  ]
  node [
    id 11
    label "skuteczny"
  ]
  node [
    id 12
    label "&#347;mieszny"
  ]
  node [
    id 13
    label "mi&#322;y"
  ]
  node [
    id 14
    label "grzeczny"
  ]
  node [
    id 15
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 16
    label "powitanie"
  ]
  node [
    id 17
    label "dobrze"
  ]
  node [
    id 18
    label "ca&#322;y"
  ]
  node [
    id 19
    label "zwrot"
  ]
  node [
    id 20
    label "pomy&#347;lny"
  ]
  node [
    id 21
    label "moralny"
  ]
  node [
    id 22
    label "drogi"
  ]
  node [
    id 23
    label "pozytywny"
  ]
  node [
    id 24
    label "odpowiedni"
  ]
  node [
    id 25
    label "korzystny"
  ]
  node [
    id 26
    label "pos&#322;uszny"
  ]
  node [
    id 27
    label "przyjemnie"
  ]
  node [
    id 28
    label "wspaniale"
  ]
  node [
    id 29
    label "&#347;wietnie"
  ]
  node [
    id 30
    label "spania&#322;y"
  ]
  node [
    id 31
    label "och&#281;do&#380;ny"
  ]
  node [
    id 32
    label "warto&#347;ciowy"
  ]
  node [
    id 33
    label "zajebisty"
  ]
  node [
    id 34
    label "bogato"
  ]
  node [
    id 35
    label "straszny"
  ]
  node [
    id 36
    label "skandalicznie"
  ]
  node [
    id 37
    label "sensacyjny"
  ]
  node [
    id 38
    label "gorsz&#261;cy"
  ]
  node [
    id 39
    label "pieski"
  ]
  node [
    id 40
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 41
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 42
    label "niekorzystny"
  ]
  node [
    id 43
    label "z&#322;oszczenie"
  ]
  node [
    id 44
    label "sierdzisty"
  ]
  node [
    id 45
    label "niegrzeczny"
  ]
  node [
    id 46
    label "zez&#322;oszczenie"
  ]
  node [
    id 47
    label "zdenerwowany"
  ]
  node [
    id 48
    label "negatywny"
  ]
  node [
    id 49
    label "rozgniewanie"
  ]
  node [
    id 50
    label "gniewanie"
  ]
  node [
    id 51
    label "niemoralny"
  ]
  node [
    id 52
    label "&#378;le"
  ]
  node [
    id 53
    label "niepomy&#347;lny"
  ]
  node [
    id 54
    label "syf"
  ]
  node [
    id 55
    label "pi&#281;kny"
  ]
  node [
    id 56
    label "okazale"
  ]
  node [
    id 57
    label "szlachetny"
  ]
  node [
    id 58
    label "beautifully"
  ]
  node [
    id 59
    label "gor&#261;co"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
]
