graph [
  node [
    id 0
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 1
    label "opis"
    origin "text"
  ]
  node [
    id 2
    label "wymagania"
    origin "text"
  ]
  node [
    id 3
    label "funkcjonalny"
    origin "text"
  ]
  node [
    id 4
    label "system"
    origin "text"
  ]
  node [
    id 5
    label "informatyczny"
    origin "text"
  ]
  node [
    id 6
    label "pisa&#263;"
  ]
  node [
    id 7
    label "kod"
  ]
  node [
    id 8
    label "pype&#263;"
  ]
  node [
    id 9
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 10
    label "gramatyka"
  ]
  node [
    id 11
    label "language"
  ]
  node [
    id 12
    label "fonetyka"
  ]
  node [
    id 13
    label "t&#322;umaczenie"
  ]
  node [
    id 14
    label "artykulator"
  ]
  node [
    id 15
    label "rozumienie"
  ]
  node [
    id 16
    label "jama_ustna"
  ]
  node [
    id 17
    label "urz&#261;dzenie"
  ]
  node [
    id 18
    label "organ"
  ]
  node [
    id 19
    label "ssanie"
  ]
  node [
    id 20
    label "lizanie"
  ]
  node [
    id 21
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 22
    label "przedmiot"
  ]
  node [
    id 23
    label "liza&#263;"
  ]
  node [
    id 24
    label "makroglosja"
  ]
  node [
    id 25
    label "natural_language"
  ]
  node [
    id 26
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 27
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 28
    label "napisa&#263;"
  ]
  node [
    id 29
    label "m&#243;wienie"
  ]
  node [
    id 30
    label "s&#322;ownictwo"
  ]
  node [
    id 31
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 32
    label "konsonantyzm"
  ]
  node [
    id 33
    label "ssa&#263;"
  ]
  node [
    id 34
    label "wokalizm"
  ]
  node [
    id 35
    label "kultura_duchowa"
  ]
  node [
    id 36
    label "formalizowanie"
  ]
  node [
    id 37
    label "jeniec"
  ]
  node [
    id 38
    label "m&#243;wi&#263;"
  ]
  node [
    id 39
    label "kawa&#322;ek"
  ]
  node [
    id 40
    label "po_koroniarsku"
  ]
  node [
    id 41
    label "rozumie&#263;"
  ]
  node [
    id 42
    label "stylik"
  ]
  node [
    id 43
    label "przet&#322;umaczenie"
  ]
  node [
    id 44
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 45
    label "formacja_geologiczna"
  ]
  node [
    id 46
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 47
    label "spos&#243;b"
  ]
  node [
    id 48
    label "but"
  ]
  node [
    id 49
    label "pismo"
  ]
  node [
    id 50
    label "formalizowa&#263;"
  ]
  node [
    id 51
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 52
    label "piece"
  ]
  node [
    id 53
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 54
    label "kawa&#322;"
  ]
  node [
    id 55
    label "utw&#243;r"
  ]
  node [
    id 56
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 57
    label "podp&#322;ywanie"
  ]
  node [
    id 58
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 59
    label "plot"
  ]
  node [
    id 60
    label "zbi&#243;r"
  ]
  node [
    id 61
    label "model"
  ]
  node [
    id 62
    label "narz&#281;dzie"
  ]
  node [
    id 63
    label "nature"
  ]
  node [
    id 64
    label "tryb"
  ]
  node [
    id 65
    label "struktura"
  ]
  node [
    id 66
    label "ci&#261;g"
  ]
  node [
    id 67
    label "szyfrowanie"
  ]
  node [
    id 68
    label "szablon"
  ]
  node [
    id 69
    label "code"
  ]
  node [
    id 70
    label "internowa&#263;"
  ]
  node [
    id 71
    label "pojmaniec"
  ]
  node [
    id 72
    label "niewolnik"
  ]
  node [
    id 73
    label "&#380;o&#322;nierz"
  ]
  node [
    id 74
    label "internowanie"
  ]
  node [
    id 75
    label "ojczyc"
  ]
  node [
    id 76
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 77
    label "uk&#322;ad"
  ]
  node [
    id 78
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 79
    label "Komitet_Region&#243;w"
  ]
  node [
    id 80
    label "struktura_anatomiczna"
  ]
  node [
    id 81
    label "organogeneza"
  ]
  node [
    id 82
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 83
    label "tw&#243;r"
  ]
  node [
    id 84
    label "tkanka"
  ]
  node [
    id 85
    label "stomia"
  ]
  node [
    id 86
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 87
    label "budowa"
  ]
  node [
    id 88
    label "dekortykacja"
  ]
  node [
    id 89
    label "okolica"
  ]
  node [
    id 90
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 91
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 92
    label "Izba_Konsyliarska"
  ]
  node [
    id 93
    label "zesp&#243;&#322;"
  ]
  node [
    id 94
    label "jednostka_organizacyjna"
  ]
  node [
    id 95
    label "aparat_artykulacyjny"
  ]
  node [
    id 96
    label "discipline"
  ]
  node [
    id 97
    label "zboczy&#263;"
  ]
  node [
    id 98
    label "w&#261;tek"
  ]
  node [
    id 99
    label "kultura"
  ]
  node [
    id 100
    label "entity"
  ]
  node [
    id 101
    label "sponiewiera&#263;"
  ]
  node [
    id 102
    label "zboczenie"
  ]
  node [
    id 103
    label "zbaczanie"
  ]
  node [
    id 104
    label "charakter"
  ]
  node [
    id 105
    label "thing"
  ]
  node [
    id 106
    label "om&#243;wi&#263;"
  ]
  node [
    id 107
    label "tre&#347;&#263;"
  ]
  node [
    id 108
    label "element"
  ]
  node [
    id 109
    label "kr&#261;&#380;enie"
  ]
  node [
    id 110
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 111
    label "istota"
  ]
  node [
    id 112
    label "zbacza&#263;"
  ]
  node [
    id 113
    label "om&#243;wienie"
  ]
  node [
    id 114
    label "rzecz"
  ]
  node [
    id 115
    label "tematyka"
  ]
  node [
    id 116
    label "omawianie"
  ]
  node [
    id 117
    label "omawia&#263;"
  ]
  node [
    id 118
    label "robienie"
  ]
  node [
    id 119
    label "program_nauczania"
  ]
  node [
    id 120
    label "sponiewieranie"
  ]
  node [
    id 121
    label "rozbijarka"
  ]
  node [
    id 122
    label "cholewa"
  ]
  node [
    id 123
    label "raki"
  ]
  node [
    id 124
    label "wzuwanie"
  ]
  node [
    id 125
    label "podeszwa"
  ]
  node [
    id 126
    label "sznurowad&#322;o"
  ]
  node [
    id 127
    label "obuwie"
  ]
  node [
    id 128
    label "zapi&#281;tek"
  ]
  node [
    id 129
    label "wzu&#263;"
  ]
  node [
    id 130
    label "cholewka"
  ]
  node [
    id 131
    label "wytw&#243;r"
  ]
  node [
    id 132
    label "zel&#243;wka"
  ]
  node [
    id 133
    label "wzucie"
  ]
  node [
    id 134
    label "napi&#281;tek"
  ]
  node [
    id 135
    label "obcas"
  ]
  node [
    id 136
    label "przyszwa"
  ]
  node [
    id 137
    label "komora"
  ]
  node [
    id 138
    label "wyrz&#261;dzenie"
  ]
  node [
    id 139
    label "kom&#243;rka"
  ]
  node [
    id 140
    label "impulsator"
  ]
  node [
    id 141
    label "przygotowanie"
  ]
  node [
    id 142
    label "furnishing"
  ]
  node [
    id 143
    label "zabezpieczenie"
  ]
  node [
    id 144
    label "sprz&#281;t"
  ]
  node [
    id 145
    label "aparatura"
  ]
  node [
    id 146
    label "ig&#322;a"
  ]
  node [
    id 147
    label "wirnik"
  ]
  node [
    id 148
    label "zablokowanie"
  ]
  node [
    id 149
    label "blokowanie"
  ]
  node [
    id 150
    label "czynno&#347;&#263;"
  ]
  node [
    id 151
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 152
    label "system_energetyczny"
  ]
  node [
    id 153
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 154
    label "set"
  ]
  node [
    id 155
    label "zrobienie"
  ]
  node [
    id 156
    label "zagospodarowanie"
  ]
  node [
    id 157
    label "mechanizm"
  ]
  node [
    id 158
    label "opowiedzenie"
  ]
  node [
    id 159
    label "zwracanie_si&#281;"
  ]
  node [
    id 160
    label "zapeszanie"
  ]
  node [
    id 161
    label "formu&#322;owanie"
  ]
  node [
    id 162
    label "wypowiadanie"
  ]
  node [
    id 163
    label "powiadanie"
  ]
  node [
    id 164
    label "dysfonia"
  ]
  node [
    id 165
    label "ozywanie_si&#281;"
  ]
  node [
    id 166
    label "public_speaking"
  ]
  node [
    id 167
    label "wyznawanie"
  ]
  node [
    id 168
    label "dodawanie"
  ]
  node [
    id 169
    label "wydawanie"
  ]
  node [
    id 170
    label "wygadanie"
  ]
  node [
    id 171
    label "informowanie"
  ]
  node [
    id 172
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 173
    label "dowalenie"
  ]
  node [
    id 174
    label "prawienie"
  ]
  node [
    id 175
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 176
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 177
    label "przerywanie"
  ]
  node [
    id 178
    label "dogadanie_si&#281;"
  ]
  node [
    id 179
    label "wydobywanie"
  ]
  node [
    id 180
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 181
    label "wyra&#380;anie"
  ]
  node [
    id 182
    label "opowiadanie"
  ]
  node [
    id 183
    label "mawianie"
  ]
  node [
    id 184
    label "stosowanie"
  ]
  node [
    id 185
    label "zauwa&#380;enie"
  ]
  node [
    id 186
    label "speaking"
  ]
  node [
    id 187
    label "gaworzenie"
  ]
  node [
    id 188
    label "przepowiadanie"
  ]
  node [
    id 189
    label "dogadywanie_si&#281;"
  ]
  node [
    id 190
    label "termin"
  ]
  node [
    id 191
    label "terminology"
  ]
  node [
    id 192
    label "asymilowa&#263;"
  ]
  node [
    id 193
    label "zasymilowa&#263;"
  ]
  node [
    id 194
    label "transkrypcja"
  ]
  node [
    id 195
    label "asymilowanie"
  ]
  node [
    id 196
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 197
    label "g&#322;osownia"
  ]
  node [
    id 198
    label "phonetics"
  ]
  node [
    id 199
    label "zasymilowanie"
  ]
  node [
    id 200
    label "palatogram"
  ]
  node [
    id 201
    label "ortografia"
  ]
  node [
    id 202
    label "dzia&#322;"
  ]
  node [
    id 203
    label "zajawka"
  ]
  node [
    id 204
    label "paleografia"
  ]
  node [
    id 205
    label "dzie&#322;o"
  ]
  node [
    id 206
    label "dokument"
  ]
  node [
    id 207
    label "wk&#322;ad"
  ]
  node [
    id 208
    label "egzemplarz"
  ]
  node [
    id 209
    label "ok&#322;adka"
  ]
  node [
    id 210
    label "letter"
  ]
  node [
    id 211
    label "prasa"
  ]
  node [
    id 212
    label "psychotest"
  ]
  node [
    id 213
    label "communication"
  ]
  node [
    id 214
    label "Zwrotnica"
  ]
  node [
    id 215
    label "script"
  ]
  node [
    id 216
    label "czasopismo"
  ]
  node [
    id 217
    label "komunikacja"
  ]
  node [
    id 218
    label "cecha"
  ]
  node [
    id 219
    label "adres"
  ]
  node [
    id 220
    label "przekaz"
  ]
  node [
    id 221
    label "grafia"
  ]
  node [
    id 222
    label "interpunkcja"
  ]
  node [
    id 223
    label "handwriting"
  ]
  node [
    id 224
    label "paleograf"
  ]
  node [
    id 225
    label "list"
  ]
  node [
    id 226
    label "sk&#322;adnia"
  ]
  node [
    id 227
    label "morfologia"
  ]
  node [
    id 228
    label "kategoria_gramatyczna"
  ]
  node [
    id 229
    label "fleksja"
  ]
  node [
    id 230
    label "styl"
  ]
  node [
    id 231
    label "postawi&#263;"
  ]
  node [
    id 232
    label "write"
  ]
  node [
    id 233
    label "donie&#347;&#263;"
  ]
  node [
    id 234
    label "stworzy&#263;"
  ]
  node [
    id 235
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 236
    label "read"
  ]
  node [
    id 237
    label "tekst"
  ]
  node [
    id 238
    label "skryba"
  ]
  node [
    id 239
    label "dysortografia"
  ]
  node [
    id 240
    label "tworzy&#263;"
  ]
  node [
    id 241
    label "formu&#322;owa&#263;"
  ]
  node [
    id 242
    label "spell"
  ]
  node [
    id 243
    label "dysgrafia"
  ]
  node [
    id 244
    label "ozdabia&#263;"
  ]
  node [
    id 245
    label "donosi&#263;"
  ]
  node [
    id 246
    label "stawia&#263;"
  ]
  node [
    id 247
    label "rendition"
  ]
  node [
    id 248
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 249
    label "przek&#322;adanie"
  ]
  node [
    id 250
    label "zrozumia&#322;y"
  ]
  node [
    id 251
    label "remark"
  ]
  node [
    id 252
    label "uzasadnianie"
  ]
  node [
    id 253
    label "rozwianie"
  ]
  node [
    id 254
    label "explanation"
  ]
  node [
    id 255
    label "kr&#281;ty"
  ]
  node [
    id 256
    label "bronienie"
  ]
  node [
    id 257
    label "gossip"
  ]
  node [
    id 258
    label "przekonywanie"
  ]
  node [
    id 259
    label "rozwiewanie"
  ]
  node [
    id 260
    label "przedstawianie"
  ]
  node [
    id 261
    label "robi&#263;"
  ]
  node [
    id 262
    label "przekonywa&#263;"
  ]
  node [
    id 263
    label "u&#322;atwia&#263;"
  ]
  node [
    id 264
    label "sprawowa&#263;"
  ]
  node [
    id 265
    label "suplikowa&#263;"
  ]
  node [
    id 266
    label "interpretowa&#263;"
  ]
  node [
    id 267
    label "uzasadnia&#263;"
  ]
  node [
    id 268
    label "give"
  ]
  node [
    id 269
    label "przedstawia&#263;"
  ]
  node [
    id 270
    label "explain"
  ]
  node [
    id 271
    label "poja&#347;nia&#263;"
  ]
  node [
    id 272
    label "broni&#263;"
  ]
  node [
    id 273
    label "przek&#322;ada&#263;"
  ]
  node [
    id 274
    label "elaborate"
  ]
  node [
    id 275
    label "przekona&#263;"
  ]
  node [
    id 276
    label "zrobi&#263;"
  ]
  node [
    id 277
    label "put"
  ]
  node [
    id 278
    label "frame"
  ]
  node [
    id 279
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 280
    label "zinterpretowa&#263;"
  ]
  node [
    id 281
    label "match"
  ]
  node [
    id 282
    label "see"
  ]
  node [
    id 283
    label "kuma&#263;"
  ]
  node [
    id 284
    label "zna&#263;"
  ]
  node [
    id 285
    label "empatia"
  ]
  node [
    id 286
    label "dziama&#263;"
  ]
  node [
    id 287
    label "czu&#263;"
  ]
  node [
    id 288
    label "wiedzie&#263;"
  ]
  node [
    id 289
    label "odbiera&#263;"
  ]
  node [
    id 290
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 291
    label "prawi&#263;"
  ]
  node [
    id 292
    label "express"
  ]
  node [
    id 293
    label "chew_the_fat"
  ]
  node [
    id 294
    label "talk"
  ]
  node [
    id 295
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 296
    label "say"
  ]
  node [
    id 297
    label "wyra&#380;a&#263;"
  ]
  node [
    id 298
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 299
    label "tell"
  ]
  node [
    id 300
    label "informowa&#263;"
  ]
  node [
    id 301
    label "rozmawia&#263;"
  ]
  node [
    id 302
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 303
    label "powiada&#263;"
  ]
  node [
    id 304
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 305
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 306
    label "okre&#347;la&#263;"
  ]
  node [
    id 307
    label "u&#380;ywa&#263;"
  ]
  node [
    id 308
    label "gaworzy&#263;"
  ]
  node [
    id 309
    label "umie&#263;"
  ]
  node [
    id 310
    label "wydobywa&#263;"
  ]
  node [
    id 311
    label "czucie"
  ]
  node [
    id 312
    label "bycie"
  ]
  node [
    id 313
    label "interpretation"
  ]
  node [
    id 314
    label "realization"
  ]
  node [
    id 315
    label "hermeneutyka"
  ]
  node [
    id 316
    label "kumanie"
  ]
  node [
    id 317
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 318
    label "apprehension"
  ]
  node [
    id 319
    label "wnioskowanie"
  ]
  node [
    id 320
    label "obja&#347;nienie"
  ]
  node [
    id 321
    label "kontekst"
  ]
  node [
    id 322
    label "formalny"
  ]
  node [
    id 323
    label "precyzowanie"
  ]
  node [
    id 324
    label "nadawanie"
  ]
  node [
    id 325
    label "nadawa&#263;"
  ]
  node [
    id 326
    label "validate"
  ]
  node [
    id 327
    label "precyzowa&#263;"
  ]
  node [
    id 328
    label "salt_lick"
  ]
  node [
    id 329
    label "muska&#263;"
  ]
  node [
    id 330
    label "dotyka&#263;"
  ]
  node [
    id 331
    label "wada_wrodzona"
  ]
  node [
    id 332
    label "wylizywanie"
  ]
  node [
    id 333
    label "zlizywanie"
  ]
  node [
    id 334
    label "g&#322;askanie"
  ]
  node [
    id 335
    label "zlizanie"
  ]
  node [
    id 336
    label "przesuwanie"
  ]
  node [
    id 337
    label "dotykanie"
  ]
  node [
    id 338
    label "wylizanie"
  ]
  node [
    id 339
    label "&#347;lina"
  ]
  node [
    id 340
    label "rusza&#263;"
  ]
  node [
    id 341
    label "sponge"
  ]
  node [
    id 342
    label "sucking"
  ]
  node [
    id 343
    label "wci&#261;ga&#263;"
  ]
  node [
    id 344
    label "pi&#263;"
  ]
  node [
    id 345
    label "usta"
  ]
  node [
    id 346
    label "smoczek"
  ]
  node [
    id 347
    label "rozpuszcza&#263;"
  ]
  node [
    id 348
    label "mleko"
  ]
  node [
    id 349
    label "brodawka"
  ]
  node [
    id 350
    label "pip"
  ]
  node [
    id 351
    label "spot"
  ]
  node [
    id 352
    label "znami&#281;"
  ]
  node [
    id 353
    label "schorzenie"
  ]
  node [
    id 354
    label "krosta"
  ]
  node [
    id 355
    label "rozpuszczanie"
  ]
  node [
    id 356
    label "wyssanie"
  ]
  node [
    id 357
    label "wci&#261;ganie"
  ]
  node [
    id 358
    label "wessanie"
  ]
  node [
    id 359
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 360
    label "ga&#378;nik"
  ]
  node [
    id 361
    label "ruszanie"
  ]
  node [
    id 362
    label "picie"
  ]
  node [
    id 363
    label "consumption"
  ]
  node [
    id 364
    label "aspiration"
  ]
  node [
    id 365
    label "odci&#261;ganie"
  ]
  node [
    id 366
    label "wysysanie"
  ]
  node [
    id 367
    label "wypowied&#378;"
  ]
  node [
    id 368
    label "exposition"
  ]
  node [
    id 369
    label "bezproblemowy"
  ]
  node [
    id 370
    label "wydarzenie"
  ]
  node [
    id 371
    label "activity"
  ]
  node [
    id 372
    label "report"
  ]
  node [
    id 373
    label "przedstawienie"
  ]
  node [
    id 374
    label "poinformowanie"
  ]
  node [
    id 375
    label "informacja"
  ]
  node [
    id 376
    label "parafrazowanie"
  ]
  node [
    id 377
    label "komunikat"
  ]
  node [
    id 378
    label "stylizacja"
  ]
  node [
    id 379
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 380
    label "strawestowanie"
  ]
  node [
    id 381
    label "sparafrazowanie"
  ]
  node [
    id 382
    label "sformu&#322;owanie"
  ]
  node [
    id 383
    label "pos&#322;uchanie"
  ]
  node [
    id 384
    label "strawestowa&#263;"
  ]
  node [
    id 385
    label "parafrazowa&#263;"
  ]
  node [
    id 386
    label "delimitacja"
  ]
  node [
    id 387
    label "rezultat"
  ]
  node [
    id 388
    label "ozdobnik"
  ]
  node [
    id 389
    label "trawestowa&#263;"
  ]
  node [
    id 390
    label "s&#261;d"
  ]
  node [
    id 391
    label "sparafrazowa&#263;"
  ]
  node [
    id 392
    label "trawestowanie"
  ]
  node [
    id 393
    label "funkcjonalnie"
  ]
  node [
    id 394
    label "czynny"
  ]
  node [
    id 395
    label "praktyczny"
  ]
  node [
    id 396
    label "faktyczny"
  ]
  node [
    id 397
    label "wa&#380;ny"
  ]
  node [
    id 398
    label "czynnie"
  ]
  node [
    id 399
    label "istotny"
  ]
  node [
    id 400
    label "zaanga&#380;owany"
  ]
  node [
    id 401
    label "aktywnie"
  ]
  node [
    id 402
    label "realny"
  ]
  node [
    id 403
    label "zdolny"
  ]
  node [
    id 404
    label "dobry"
  ]
  node [
    id 405
    label "intensywny"
  ]
  node [
    id 406
    label "ciekawy"
  ]
  node [
    id 407
    label "uczynnienie"
  ]
  node [
    id 408
    label "zaj&#281;ty"
  ]
  node [
    id 409
    label "dzia&#322;alny"
  ]
  node [
    id 410
    label "dzia&#322;anie"
  ]
  node [
    id 411
    label "uczynnianie"
  ]
  node [
    id 412
    label "u&#380;yteczny"
  ]
  node [
    id 413
    label "praktycznie"
  ]
  node [
    id 414
    label "racjonalny"
  ]
  node [
    id 415
    label "odpowiednio"
  ]
  node [
    id 416
    label "poj&#281;cie"
  ]
  node [
    id 417
    label "systemik"
  ]
  node [
    id 418
    label "Android"
  ]
  node [
    id 419
    label "podsystem"
  ]
  node [
    id 420
    label "systemat"
  ]
  node [
    id 421
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 422
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 423
    label "p&#322;&#243;d"
  ]
  node [
    id 424
    label "konstelacja"
  ]
  node [
    id 425
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 426
    label "oprogramowanie"
  ]
  node [
    id 427
    label "j&#261;dro"
  ]
  node [
    id 428
    label "rozprz&#261;c"
  ]
  node [
    id 429
    label "usenet"
  ]
  node [
    id 430
    label "jednostka_geologiczna"
  ]
  node [
    id 431
    label "ryba"
  ]
  node [
    id 432
    label "oddzia&#322;"
  ]
  node [
    id 433
    label "net"
  ]
  node [
    id 434
    label "podstawa"
  ]
  node [
    id 435
    label "metoda"
  ]
  node [
    id 436
    label "method"
  ]
  node [
    id 437
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 438
    label "porz&#261;dek"
  ]
  node [
    id 439
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 440
    label "w&#281;dkarstwo"
  ]
  node [
    id 441
    label "doktryna"
  ]
  node [
    id 442
    label "Leopard"
  ]
  node [
    id 443
    label "zachowanie"
  ]
  node [
    id 444
    label "o&#347;"
  ]
  node [
    id 445
    label "sk&#322;ad"
  ]
  node [
    id 446
    label "pulpit"
  ]
  node [
    id 447
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 448
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 449
    label "cybernetyk"
  ]
  node [
    id 450
    label "przyn&#281;ta"
  ]
  node [
    id 451
    label "eratem"
  ]
  node [
    id 452
    label "za&#322;o&#380;enie"
  ]
  node [
    id 453
    label "strategia"
  ]
  node [
    id 454
    label "background"
  ]
  node [
    id 455
    label "punkt_odniesienia"
  ]
  node [
    id 456
    label "zasadzenie"
  ]
  node [
    id 457
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 458
    label "&#347;ciana"
  ]
  node [
    id 459
    label "podstawowy"
  ]
  node [
    id 460
    label "dzieci&#281;ctwo"
  ]
  node [
    id 461
    label "d&#243;&#322;"
  ]
  node [
    id 462
    label "documentation"
  ]
  node [
    id 463
    label "bok"
  ]
  node [
    id 464
    label "pomys&#322;"
  ]
  node [
    id 465
    label "zasadzi&#263;"
  ]
  node [
    id 466
    label "column"
  ]
  node [
    id 467
    label "pot&#281;ga"
  ]
  node [
    id 468
    label "stan"
  ]
  node [
    id 469
    label "normalizacja"
  ]
  node [
    id 470
    label "styl_architektoniczny"
  ]
  node [
    id 471
    label "relacja"
  ]
  node [
    id 472
    label "zasada"
  ]
  node [
    id 473
    label "pakiet_klimatyczny"
  ]
  node [
    id 474
    label "uprawianie"
  ]
  node [
    id 475
    label "collection"
  ]
  node [
    id 476
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 477
    label "gathering"
  ]
  node [
    id 478
    label "album"
  ]
  node [
    id 479
    label "praca_rolnicza"
  ]
  node [
    id 480
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 481
    label "sum"
  ]
  node [
    id 482
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 483
    label "series"
  ]
  node [
    id 484
    label "dane"
  ]
  node [
    id 485
    label "orientacja"
  ]
  node [
    id 486
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 487
    label "skumanie"
  ]
  node [
    id 488
    label "teoria"
  ]
  node [
    id 489
    label "forma"
  ]
  node [
    id 490
    label "zorientowanie"
  ]
  node [
    id 491
    label "clasp"
  ]
  node [
    id 492
    label "przem&#243;wienie"
  ]
  node [
    id 493
    label "konstrukcja"
  ]
  node [
    id 494
    label "mechanika"
  ]
  node [
    id 495
    label "system_komputerowy"
  ]
  node [
    id 496
    label "embryo"
  ]
  node [
    id 497
    label "moczownik"
  ]
  node [
    id 498
    label "latawiec"
  ]
  node [
    id 499
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 500
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 501
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 502
    label "zarodek"
  ]
  node [
    id 503
    label "reengineering"
  ]
  node [
    id 504
    label "program"
  ]
  node [
    id 505
    label "liczba"
  ]
  node [
    id 506
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 507
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 508
    label "integer"
  ]
  node [
    id 509
    label "zlewanie_si&#281;"
  ]
  node [
    id 510
    label "ilo&#347;&#263;"
  ]
  node [
    id 511
    label "pe&#322;ny"
  ]
  node [
    id 512
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 513
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 514
    label "grupa_dyskusyjna"
  ]
  node [
    id 515
    label "doctrine"
  ]
  node [
    id 516
    label "tar&#322;o"
  ]
  node [
    id 517
    label "rakowato&#347;&#263;"
  ]
  node [
    id 518
    label "szczelina_skrzelowa"
  ]
  node [
    id 519
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 520
    label "doniczkowiec"
  ]
  node [
    id 521
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 522
    label "cz&#322;owiek"
  ]
  node [
    id 523
    label "mi&#281;so"
  ]
  node [
    id 524
    label "fish"
  ]
  node [
    id 525
    label "patroszy&#263;"
  ]
  node [
    id 526
    label "linia_boczna"
  ]
  node [
    id 527
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 528
    label "pokrywa_skrzelowa"
  ]
  node [
    id 529
    label "kr&#281;gowiec"
  ]
  node [
    id 530
    label "ryby"
  ]
  node [
    id 531
    label "m&#281;tnooki"
  ]
  node [
    id 532
    label "ikra"
  ]
  node [
    id 533
    label "wyrostek_filtracyjny"
  ]
  node [
    id 534
    label "sport"
  ]
  node [
    id 535
    label "urozmaicenie"
  ]
  node [
    id 536
    label "pu&#322;apka"
  ]
  node [
    id 537
    label "wabik"
  ]
  node [
    id 538
    label "pon&#281;ta"
  ]
  node [
    id 539
    label "blat"
  ]
  node [
    id 540
    label "obszar"
  ]
  node [
    id 541
    label "okno"
  ]
  node [
    id 542
    label "mebel"
  ]
  node [
    id 543
    label "system_operacyjny"
  ]
  node [
    id 544
    label "interfejs"
  ]
  node [
    id 545
    label "ikona"
  ]
  node [
    id 546
    label "zdolno&#347;&#263;"
  ]
  node [
    id 547
    label "oswobodzi&#263;"
  ]
  node [
    id 548
    label "disengage"
  ]
  node [
    id 549
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 550
    label "zdezorganizowa&#263;"
  ]
  node [
    id 551
    label "os&#322;abi&#263;"
  ]
  node [
    id 552
    label "post"
  ]
  node [
    id 553
    label "etolog"
  ]
  node [
    id 554
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 555
    label "dieta"
  ]
  node [
    id 556
    label "zdyscyplinowanie"
  ]
  node [
    id 557
    label "zwierz&#281;"
  ]
  node [
    id 558
    label "bearing"
  ]
  node [
    id 559
    label "observation"
  ]
  node [
    id 560
    label "behawior"
  ]
  node [
    id 561
    label "reakcja"
  ]
  node [
    id 562
    label "tajemnica"
  ]
  node [
    id 563
    label "przechowanie"
  ]
  node [
    id 564
    label "pochowanie"
  ]
  node [
    id 565
    label "podtrzymanie"
  ]
  node [
    id 566
    label "post&#261;pienie"
  ]
  node [
    id 567
    label "oswobodzenie"
  ]
  node [
    id 568
    label "zdezorganizowanie"
  ]
  node [
    id 569
    label "relaxation"
  ]
  node [
    id 570
    label "os&#322;abienie"
  ]
  node [
    id 571
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 572
    label "naukowiec"
  ]
  node [
    id 573
    label "provider"
  ]
  node [
    id 574
    label "b&#322;&#261;d"
  ]
  node [
    id 575
    label "podcast"
  ]
  node [
    id 576
    label "mem"
  ]
  node [
    id 577
    label "cyberprzestrze&#324;"
  ]
  node [
    id 578
    label "punkt_dost&#281;pu"
  ]
  node [
    id 579
    label "sie&#263;_komputerowa"
  ]
  node [
    id 580
    label "biznes_elektroniczny"
  ]
  node [
    id 581
    label "media"
  ]
  node [
    id 582
    label "gra_sieciowa"
  ]
  node [
    id 583
    label "hipertekst"
  ]
  node [
    id 584
    label "netbook"
  ]
  node [
    id 585
    label "strona"
  ]
  node [
    id 586
    label "e-hazard"
  ]
  node [
    id 587
    label "us&#322;uga_internetowa"
  ]
  node [
    id 588
    label "grooming"
  ]
  node [
    id 589
    label "matryca"
  ]
  node [
    id 590
    label "facet"
  ]
  node [
    id 591
    label "zi&#243;&#322;ko"
  ]
  node [
    id 592
    label "mildew"
  ]
  node [
    id 593
    label "miniatura"
  ]
  node [
    id 594
    label "ideal"
  ]
  node [
    id 595
    label "adaptation"
  ]
  node [
    id 596
    label "typ"
  ]
  node [
    id 597
    label "ruch"
  ]
  node [
    id 598
    label "imitacja"
  ]
  node [
    id 599
    label "pozowa&#263;"
  ]
  node [
    id 600
    label "orygina&#322;"
  ]
  node [
    id 601
    label "wz&#243;r"
  ]
  node [
    id 602
    label "motif"
  ]
  node [
    id 603
    label "prezenter"
  ]
  node [
    id 604
    label "pozowanie"
  ]
  node [
    id 605
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 606
    label "forum"
  ]
  node [
    id 607
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 608
    label "s&#261;downictwo"
  ]
  node [
    id 609
    label "podejrzany"
  ]
  node [
    id 610
    label "&#347;wiadek"
  ]
  node [
    id 611
    label "instytucja"
  ]
  node [
    id 612
    label "biuro"
  ]
  node [
    id 613
    label "post&#281;powanie"
  ]
  node [
    id 614
    label "court"
  ]
  node [
    id 615
    label "my&#347;l"
  ]
  node [
    id 616
    label "obrona"
  ]
  node [
    id 617
    label "antylogizm"
  ]
  node [
    id 618
    label "oskar&#380;yciel"
  ]
  node [
    id 619
    label "urz&#261;d"
  ]
  node [
    id 620
    label "skazany"
  ]
  node [
    id 621
    label "konektyw"
  ]
  node [
    id 622
    label "pods&#261;dny"
  ]
  node [
    id 623
    label "procesowicz"
  ]
  node [
    id 624
    label "filia"
  ]
  node [
    id 625
    label "bank"
  ]
  node [
    id 626
    label "formacja"
  ]
  node [
    id 627
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 628
    label "klasa"
  ]
  node [
    id 629
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 630
    label "agencja"
  ]
  node [
    id 631
    label "whole"
  ]
  node [
    id 632
    label "malm"
  ]
  node [
    id 633
    label "promocja"
  ]
  node [
    id 634
    label "szpital"
  ]
  node [
    id 635
    label "siedziba"
  ]
  node [
    id 636
    label "dogger"
  ]
  node [
    id 637
    label "ajencja"
  ]
  node [
    id 638
    label "poziom"
  ]
  node [
    id 639
    label "jednostka"
  ]
  node [
    id 640
    label "lias"
  ]
  node [
    id 641
    label "wojsko"
  ]
  node [
    id 642
    label "kurs"
  ]
  node [
    id 643
    label "pi&#281;tro"
  ]
  node [
    id 644
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 645
    label "algebra_liniowa"
  ]
  node [
    id 646
    label "chromosom"
  ]
  node [
    id 647
    label "&#347;rodek"
  ]
  node [
    id 648
    label "nasieniak"
  ]
  node [
    id 649
    label "nukleon"
  ]
  node [
    id 650
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 651
    label "ziarno"
  ]
  node [
    id 652
    label "znaczenie"
  ]
  node [
    id 653
    label "j&#261;derko"
  ]
  node [
    id 654
    label "macierz_j&#261;drowa"
  ]
  node [
    id 655
    label "anorchizm"
  ]
  node [
    id 656
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 657
    label "wn&#281;trostwo"
  ]
  node [
    id 658
    label "kariokineza"
  ]
  node [
    id 659
    label "nukleosynteza"
  ]
  node [
    id 660
    label "organellum"
  ]
  node [
    id 661
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 662
    label "chemia_j&#261;drowa"
  ]
  node [
    id 663
    label "atom"
  ]
  node [
    id 664
    label "przeciwobraz"
  ]
  node [
    id 665
    label "jajo"
  ]
  node [
    id 666
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 667
    label "core"
  ]
  node [
    id 668
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 669
    label "protoplazma"
  ]
  node [
    id 670
    label "moszna"
  ]
  node [
    id 671
    label "subsystem"
  ]
  node [
    id 672
    label "suport"
  ]
  node [
    id 673
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 674
    label "granica"
  ]
  node [
    id 675
    label "o&#347;rodek"
  ]
  node [
    id 676
    label "prosta"
  ]
  node [
    id 677
    label "ko&#322;o"
  ]
  node [
    id 678
    label "eonotem"
  ]
  node [
    id 679
    label "constellation"
  ]
  node [
    id 680
    label "W&#261;&#380;"
  ]
  node [
    id 681
    label "Panna"
  ]
  node [
    id 682
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 683
    label "W&#281;&#380;ownik"
  ]
  node [
    id 684
    label "Ptak_Rajski"
  ]
  node [
    id 685
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 686
    label "fabryka"
  ]
  node [
    id 687
    label "pole"
  ]
  node [
    id 688
    label "pas"
  ]
  node [
    id 689
    label "blokada"
  ]
  node [
    id 690
    label "miejsce"
  ]
  node [
    id 691
    label "tabulacja"
  ]
  node [
    id 692
    label "hurtownia"
  ]
  node [
    id 693
    label "basic"
  ]
  node [
    id 694
    label "obr&#243;bka"
  ]
  node [
    id 695
    label "rank_and_file"
  ]
  node [
    id 696
    label "pomieszczenie"
  ]
  node [
    id 697
    label "syf"
  ]
  node [
    id 698
    label "sk&#322;adnik"
  ]
  node [
    id 699
    label "constitution"
  ]
  node [
    id 700
    label "sklep"
  ]
  node [
    id 701
    label "&#347;wiat&#322;o"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 701
  ]
]
