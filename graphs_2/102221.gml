graph [
  node [
    id 0
    label "przejazd"
    origin "text"
  ]
  node [
    id 1
    label "koszt"
    origin "text"
  ]
  node [
    id 2
    label "wojska"
    origin "text"
  ]
  node [
    id 3
    label "przypadek"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "mowa"
    origin "text"
  ]
  node [
    id 6
    label "usta"
    origin "text"
  ]
  node [
    id 7
    label "pkt"
    origin "text"
  ]
  node [
    id 8
    label "&#380;o&#322;nierz"
    origin "text"
  ]
  node [
    id 9
    label "maja"
    origin "text"
  ]
  node [
    id 10
    label "prawo"
    origin "text"
  ]
  node [
    id 11
    label "jeden"
    origin "text"
  ]
  node [
    id 12
    label "raz"
    origin "text"
  ]
  node [
    id 13
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 14
    label "obejmowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 16
    label "trasa"
    origin "text"
  ]
  node [
    id 17
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 18
    label "publiczny"
    origin "text"
  ]
  node [
    id 19
    label "transport"
    origin "text"
  ]
  node [
    id 20
    label "zbiorowy"
    origin "text"
  ]
  node [
    id 21
    label "kolejowy"
    origin "text"
  ]
  node [
    id 22
    label "klasa"
    origin "text"
  ]
  node [
    id 23
    label "poci&#261;g"
    origin "text"
  ]
  node [
    id 24
    label "osobowy"
    origin "text"
  ]
  node [
    id 25
    label "pospieszny"
    origin "text"
  ]
  node [
    id 26
    label "ekspresowy"
    origin "text"
  ]
  node [
    id 27
    label "autobusowy"
    origin "text"
  ]
  node [
    id 28
    label "komunikacja"
    origin "text"
  ]
  node [
    id 29
    label "zwyk&#322;y"
    origin "text"
  ]
  node [
    id 30
    label "przyspieszy&#263;"
    origin "text"
  ]
  node [
    id 31
    label "albo"
    origin "text"
  ]
  node [
    id 32
    label "&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 33
    label "podstawa"
    origin "text"
  ]
  node [
    id 34
    label "bilet"
    origin "text"
  ]
  node [
    id 35
    label "jednorazowy"
    origin "text"
  ]
  node [
    id 36
    label "uwzgl&#281;dnienie"
    origin "text"
  ]
  node [
    id 37
    label "posiada&#263;"
    origin "text"
  ]
  node [
    id 38
    label "przez"
    origin "text"
  ]
  node [
    id 39
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 40
    label "ulgowy"
    origin "text"
  ]
  node [
    id 41
    label "way"
  ]
  node [
    id 42
    label "miejsce"
  ]
  node [
    id 43
    label "jazda"
  ]
  node [
    id 44
    label "przestrze&#324;"
  ]
  node [
    id 45
    label "rz&#261;d"
  ]
  node [
    id 46
    label "uwaga"
  ]
  node [
    id 47
    label "cecha"
  ]
  node [
    id 48
    label "praca"
  ]
  node [
    id 49
    label "plac"
  ]
  node [
    id 50
    label "location"
  ]
  node [
    id 51
    label "warunek_lokalowy"
  ]
  node [
    id 52
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 53
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 54
    label "cia&#322;o"
  ]
  node [
    id 55
    label "status"
  ]
  node [
    id 56
    label "chwila"
  ]
  node [
    id 57
    label "sport"
  ]
  node [
    id 58
    label "wykrzyknik"
  ]
  node [
    id 59
    label "szwadron"
  ]
  node [
    id 60
    label "cavalry"
  ]
  node [
    id 61
    label "journey"
  ]
  node [
    id 62
    label "chor&#261;giew"
  ]
  node [
    id 63
    label "awantura"
  ]
  node [
    id 64
    label "formacja"
  ]
  node [
    id 65
    label "ruch"
  ]
  node [
    id 66
    label "szale&#324;stwo"
  ]
  node [
    id 67
    label "heca"
  ]
  node [
    id 68
    label "nak&#322;ad"
  ]
  node [
    id 69
    label "sumpt"
  ]
  node [
    id 70
    label "wydatek"
  ]
  node [
    id 71
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 72
    label "ilo&#347;&#263;"
  ]
  node [
    id 73
    label "liczba"
  ]
  node [
    id 74
    label "kwota"
  ]
  node [
    id 75
    label "wych&#243;d"
  ]
  node [
    id 76
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 77
    label "happening"
  ]
  node [
    id 78
    label "pacjent"
  ]
  node [
    id 79
    label "wydarzenie"
  ]
  node [
    id 80
    label "przeznaczenie"
  ]
  node [
    id 81
    label "przyk&#322;ad"
  ]
  node [
    id 82
    label "kategoria_gramatyczna"
  ]
  node [
    id 83
    label "schorzenie"
  ]
  node [
    id 84
    label "czyn"
  ]
  node [
    id 85
    label "cz&#322;owiek"
  ]
  node [
    id 86
    label "ilustracja"
  ]
  node [
    id 87
    label "fakt"
  ]
  node [
    id 88
    label "przedstawiciel"
  ]
  node [
    id 89
    label "charakter"
  ]
  node [
    id 90
    label "przebiegni&#281;cie"
  ]
  node [
    id 91
    label "przebiec"
  ]
  node [
    id 92
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 93
    label "motyw"
  ]
  node [
    id 94
    label "fabu&#322;a"
  ]
  node [
    id 95
    label "czynno&#347;&#263;"
  ]
  node [
    id 96
    label "destiny"
  ]
  node [
    id 97
    label "przymus"
  ]
  node [
    id 98
    label "wybranie"
  ]
  node [
    id 99
    label "rzuci&#263;"
  ]
  node [
    id 100
    label "si&#322;a"
  ]
  node [
    id 101
    label "p&#243;j&#347;cie"
  ]
  node [
    id 102
    label "przydzielenie"
  ]
  node [
    id 103
    label "oblat"
  ]
  node [
    id 104
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 105
    label "obowi&#261;zek"
  ]
  node [
    id 106
    label "zrobienie"
  ]
  node [
    id 107
    label "rzucenie"
  ]
  node [
    id 108
    label "ustalenie"
  ]
  node [
    id 109
    label "odezwanie_si&#281;"
  ]
  node [
    id 110
    label "zaburzenie"
  ]
  node [
    id 111
    label "ognisko"
  ]
  node [
    id 112
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 113
    label "atakowanie"
  ]
  node [
    id 114
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 115
    label "remisja"
  ]
  node [
    id 116
    label "nabawianie_si&#281;"
  ]
  node [
    id 117
    label "odzywanie_si&#281;"
  ]
  node [
    id 118
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 119
    label "powalenie"
  ]
  node [
    id 120
    label "diagnoza"
  ]
  node [
    id 121
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 122
    label "atakowa&#263;"
  ]
  node [
    id 123
    label "inkubacja"
  ]
  node [
    id 124
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 125
    label "grupa_ryzyka"
  ]
  node [
    id 126
    label "badanie_histopatologiczne"
  ]
  node [
    id 127
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 128
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 129
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 130
    label "zajmowanie"
  ]
  node [
    id 131
    label "powali&#263;"
  ]
  node [
    id 132
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 133
    label "zajmowa&#263;"
  ]
  node [
    id 134
    label "kryzys"
  ]
  node [
    id 135
    label "nabawienie_si&#281;"
  ]
  node [
    id 136
    label "od&#322;&#261;czenie"
  ]
  node [
    id 137
    label "piel&#281;gniarz"
  ]
  node [
    id 138
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 139
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 140
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 141
    label "chory"
  ]
  node [
    id 142
    label "szpitalnik"
  ]
  node [
    id 143
    label "od&#322;&#261;czanie"
  ]
  node [
    id 144
    label "klient"
  ]
  node [
    id 145
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 146
    label "przedstawienie"
  ]
  node [
    id 147
    label "zdolno&#347;&#263;"
  ]
  node [
    id 148
    label "kod"
  ]
  node [
    id 149
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 150
    label "gramatyka"
  ]
  node [
    id 151
    label "fonetyka"
  ]
  node [
    id 152
    label "t&#322;umaczenie"
  ]
  node [
    id 153
    label "rozumienie"
  ]
  node [
    id 154
    label "address"
  ]
  node [
    id 155
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 156
    label "m&#243;wienie"
  ]
  node [
    id 157
    label "s&#322;ownictwo"
  ]
  node [
    id 158
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 159
    label "konsonantyzm"
  ]
  node [
    id 160
    label "wokalizm"
  ]
  node [
    id 161
    label "m&#243;wi&#263;"
  ]
  node [
    id 162
    label "po_koroniarsku"
  ]
  node [
    id 163
    label "rozumie&#263;"
  ]
  node [
    id 164
    label "przet&#322;umaczenie"
  ]
  node [
    id 165
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 166
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 167
    label "tongue"
  ]
  node [
    id 168
    label "wypowied&#378;"
  ]
  node [
    id 169
    label "pismo"
  ]
  node [
    id 170
    label "parafrazowanie"
  ]
  node [
    id 171
    label "komunikat"
  ]
  node [
    id 172
    label "stylizacja"
  ]
  node [
    id 173
    label "sparafrazowanie"
  ]
  node [
    id 174
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 175
    label "strawestowanie"
  ]
  node [
    id 176
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 177
    label "sformu&#322;owanie"
  ]
  node [
    id 178
    label "pos&#322;uchanie"
  ]
  node [
    id 179
    label "strawestowa&#263;"
  ]
  node [
    id 180
    label "parafrazowa&#263;"
  ]
  node [
    id 181
    label "delimitacja"
  ]
  node [
    id 182
    label "rezultat"
  ]
  node [
    id 183
    label "ozdobnik"
  ]
  node [
    id 184
    label "sparafrazowa&#263;"
  ]
  node [
    id 185
    label "s&#261;d"
  ]
  node [
    id 186
    label "trawestowa&#263;"
  ]
  node [
    id 187
    label "trawestowanie"
  ]
  node [
    id 188
    label "transportation_system"
  ]
  node [
    id 189
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 190
    label "implicite"
  ]
  node [
    id 191
    label "explicite"
  ]
  node [
    id 192
    label "wydeptanie"
  ]
  node [
    id 193
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 194
    label "wydeptywanie"
  ]
  node [
    id 195
    label "ekspedytor"
  ]
  node [
    id 196
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 197
    label "zapomnie&#263;"
  ]
  node [
    id 198
    label "zapominanie"
  ]
  node [
    id 199
    label "zapomnienie"
  ]
  node [
    id 200
    label "potencja&#322;"
  ]
  node [
    id 201
    label "obliczeniowo"
  ]
  node [
    id 202
    label "ability"
  ]
  node [
    id 203
    label "zapomina&#263;"
  ]
  node [
    id 204
    label "bezproblemowy"
  ]
  node [
    id 205
    label "activity"
  ]
  node [
    id 206
    label "struktura"
  ]
  node [
    id 207
    label "ci&#261;g"
  ]
  node [
    id 208
    label "language"
  ]
  node [
    id 209
    label "szyfrowanie"
  ]
  node [
    id 210
    label "szablon"
  ]
  node [
    id 211
    label "code"
  ]
  node [
    id 212
    label "cover"
  ]
  node [
    id 213
    label "j&#281;zyk"
  ]
  node [
    id 214
    label "opowiedzenie"
  ]
  node [
    id 215
    label "zwracanie_si&#281;"
  ]
  node [
    id 216
    label "zapeszanie"
  ]
  node [
    id 217
    label "formu&#322;owanie"
  ]
  node [
    id 218
    label "wypowiadanie"
  ]
  node [
    id 219
    label "powiadanie"
  ]
  node [
    id 220
    label "dysfonia"
  ]
  node [
    id 221
    label "ozywanie_si&#281;"
  ]
  node [
    id 222
    label "public_speaking"
  ]
  node [
    id 223
    label "wyznawanie"
  ]
  node [
    id 224
    label "dodawanie"
  ]
  node [
    id 225
    label "wydawanie"
  ]
  node [
    id 226
    label "wygadanie"
  ]
  node [
    id 227
    label "informowanie"
  ]
  node [
    id 228
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 229
    label "dowalenie"
  ]
  node [
    id 230
    label "prawienie"
  ]
  node [
    id 231
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 232
    label "przerywanie"
  ]
  node [
    id 233
    label "dogadanie_si&#281;"
  ]
  node [
    id 234
    label "wydobywanie"
  ]
  node [
    id 235
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 236
    label "wyra&#380;anie"
  ]
  node [
    id 237
    label "opowiadanie"
  ]
  node [
    id 238
    label "mawianie"
  ]
  node [
    id 239
    label "stosowanie"
  ]
  node [
    id 240
    label "zauwa&#380;enie"
  ]
  node [
    id 241
    label "speaking"
  ]
  node [
    id 242
    label "gaworzenie"
  ]
  node [
    id 243
    label "przepowiadanie"
  ]
  node [
    id 244
    label "dogadywanie_si&#281;"
  ]
  node [
    id 245
    label "termin"
  ]
  node [
    id 246
    label "terminology"
  ]
  node [
    id 247
    label "asymilowa&#263;"
  ]
  node [
    id 248
    label "zasymilowa&#263;"
  ]
  node [
    id 249
    label "transkrypcja"
  ]
  node [
    id 250
    label "asymilowanie"
  ]
  node [
    id 251
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 252
    label "g&#322;osownia"
  ]
  node [
    id 253
    label "phonetics"
  ]
  node [
    id 254
    label "zasymilowanie"
  ]
  node [
    id 255
    label "palatogram"
  ]
  node [
    id 256
    label "ortografia"
  ]
  node [
    id 257
    label "dzia&#322;"
  ]
  node [
    id 258
    label "zajawka"
  ]
  node [
    id 259
    label "paleografia"
  ]
  node [
    id 260
    label "dzie&#322;o"
  ]
  node [
    id 261
    label "dokument"
  ]
  node [
    id 262
    label "wk&#322;ad"
  ]
  node [
    id 263
    label "egzemplarz"
  ]
  node [
    id 264
    label "ok&#322;adka"
  ]
  node [
    id 265
    label "letter"
  ]
  node [
    id 266
    label "prasa"
  ]
  node [
    id 267
    label "psychotest"
  ]
  node [
    id 268
    label "communication"
  ]
  node [
    id 269
    label "Zwrotnica"
  ]
  node [
    id 270
    label "script"
  ]
  node [
    id 271
    label "czasopismo"
  ]
  node [
    id 272
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 273
    label "adres"
  ]
  node [
    id 274
    label "przekaz"
  ]
  node [
    id 275
    label "grafia"
  ]
  node [
    id 276
    label "interpunkcja"
  ]
  node [
    id 277
    label "handwriting"
  ]
  node [
    id 278
    label "paleograf"
  ]
  node [
    id 279
    label "list"
  ]
  node [
    id 280
    label "sk&#322;adnia"
  ]
  node [
    id 281
    label "morfologia"
  ]
  node [
    id 282
    label "fleksja"
  ]
  node [
    id 283
    label "rendition"
  ]
  node [
    id 284
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 285
    label "przek&#322;adanie"
  ]
  node [
    id 286
    label "zrozumia&#322;y"
  ]
  node [
    id 287
    label "tekst"
  ]
  node [
    id 288
    label "remark"
  ]
  node [
    id 289
    label "uzasadnianie"
  ]
  node [
    id 290
    label "rozwianie"
  ]
  node [
    id 291
    label "explanation"
  ]
  node [
    id 292
    label "kr&#281;ty"
  ]
  node [
    id 293
    label "bronienie"
  ]
  node [
    id 294
    label "robienie"
  ]
  node [
    id 295
    label "gossip"
  ]
  node [
    id 296
    label "przekonywanie"
  ]
  node [
    id 297
    label "rozwiewanie"
  ]
  node [
    id 298
    label "przedstawianie"
  ]
  node [
    id 299
    label "robi&#263;"
  ]
  node [
    id 300
    label "przekonywa&#263;"
  ]
  node [
    id 301
    label "u&#322;atwia&#263;"
  ]
  node [
    id 302
    label "sprawowa&#263;"
  ]
  node [
    id 303
    label "suplikowa&#263;"
  ]
  node [
    id 304
    label "interpretowa&#263;"
  ]
  node [
    id 305
    label "uzasadnia&#263;"
  ]
  node [
    id 306
    label "give"
  ]
  node [
    id 307
    label "przedstawia&#263;"
  ]
  node [
    id 308
    label "explain"
  ]
  node [
    id 309
    label "poja&#347;nia&#263;"
  ]
  node [
    id 310
    label "broni&#263;"
  ]
  node [
    id 311
    label "przek&#322;ada&#263;"
  ]
  node [
    id 312
    label "elaborate"
  ]
  node [
    id 313
    label "przekona&#263;"
  ]
  node [
    id 314
    label "zrobi&#263;"
  ]
  node [
    id 315
    label "put"
  ]
  node [
    id 316
    label "frame"
  ]
  node [
    id 317
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 318
    label "zinterpretowa&#263;"
  ]
  node [
    id 319
    label "match"
  ]
  node [
    id 320
    label "see"
  ]
  node [
    id 321
    label "kuma&#263;"
  ]
  node [
    id 322
    label "zna&#263;"
  ]
  node [
    id 323
    label "empatia"
  ]
  node [
    id 324
    label "dziama&#263;"
  ]
  node [
    id 325
    label "czu&#263;"
  ]
  node [
    id 326
    label "wiedzie&#263;"
  ]
  node [
    id 327
    label "odbiera&#263;"
  ]
  node [
    id 328
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 329
    label "prawi&#263;"
  ]
  node [
    id 330
    label "express"
  ]
  node [
    id 331
    label "chew_the_fat"
  ]
  node [
    id 332
    label "talk"
  ]
  node [
    id 333
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 334
    label "say"
  ]
  node [
    id 335
    label "wyra&#380;a&#263;"
  ]
  node [
    id 336
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 337
    label "tell"
  ]
  node [
    id 338
    label "informowa&#263;"
  ]
  node [
    id 339
    label "rozmawia&#263;"
  ]
  node [
    id 340
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 341
    label "powiada&#263;"
  ]
  node [
    id 342
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 343
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 344
    label "okre&#347;la&#263;"
  ]
  node [
    id 345
    label "u&#380;ywa&#263;"
  ]
  node [
    id 346
    label "gaworzy&#263;"
  ]
  node [
    id 347
    label "formu&#322;owa&#263;"
  ]
  node [
    id 348
    label "umie&#263;"
  ]
  node [
    id 349
    label "wydobywa&#263;"
  ]
  node [
    id 350
    label "czucie"
  ]
  node [
    id 351
    label "bycie"
  ]
  node [
    id 352
    label "interpretation"
  ]
  node [
    id 353
    label "realization"
  ]
  node [
    id 354
    label "hermeneutyka"
  ]
  node [
    id 355
    label "kumanie"
  ]
  node [
    id 356
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 357
    label "wytw&#243;r"
  ]
  node [
    id 358
    label "apprehension"
  ]
  node [
    id 359
    label "wnioskowanie"
  ]
  node [
    id 360
    label "obja&#347;nienie"
  ]
  node [
    id 361
    label "kontekst"
  ]
  node [
    id 362
    label "ssa&#263;"
  ]
  node [
    id 363
    label "warga_dolna"
  ]
  node [
    id 364
    label "zaci&#281;cie"
  ]
  node [
    id 365
    label "zacinanie"
  ]
  node [
    id 366
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 367
    label "ryjek"
  ]
  node [
    id 368
    label "warga_g&#243;rna"
  ]
  node [
    id 369
    label "dzi&#243;b"
  ]
  node [
    id 370
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 371
    label "zacina&#263;"
  ]
  node [
    id 372
    label "jama_ustna"
  ]
  node [
    id 373
    label "jadaczka"
  ]
  node [
    id 374
    label "zaci&#261;&#263;"
  ]
  node [
    id 375
    label "organ"
  ]
  node [
    id 376
    label "ssanie"
  ]
  node [
    id 377
    label "twarz"
  ]
  node [
    id 378
    label "uk&#322;ad"
  ]
  node [
    id 379
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 380
    label "Komitet_Region&#243;w"
  ]
  node [
    id 381
    label "struktura_anatomiczna"
  ]
  node [
    id 382
    label "organogeneza"
  ]
  node [
    id 383
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 384
    label "tw&#243;r"
  ]
  node [
    id 385
    label "tkanka"
  ]
  node [
    id 386
    label "stomia"
  ]
  node [
    id 387
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 388
    label "budowa"
  ]
  node [
    id 389
    label "dekortykacja"
  ]
  node [
    id 390
    label "okolica"
  ]
  node [
    id 391
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 392
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 393
    label "Izba_Konsyliarska"
  ]
  node [
    id 394
    label "zesp&#243;&#322;"
  ]
  node [
    id 395
    label "jednostka_organizacyjna"
  ]
  node [
    id 396
    label "statek"
  ]
  node [
    id 397
    label "zako&#324;czenie"
  ]
  node [
    id 398
    label "bow"
  ]
  node [
    id 399
    label "ustnik"
  ]
  node [
    id 400
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 401
    label "blizna"
  ]
  node [
    id 402
    label "dziob&#243;wka"
  ]
  node [
    id 403
    label "grzebie&#324;"
  ]
  node [
    id 404
    label "samolot"
  ]
  node [
    id 405
    label "ptak"
  ]
  node [
    id 406
    label "ostry"
  ]
  node [
    id 407
    label "wyraz_twarzy"
  ]
  node [
    id 408
    label "p&#243;&#322;profil"
  ]
  node [
    id 409
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 410
    label "rys"
  ]
  node [
    id 411
    label "brew"
  ]
  node [
    id 412
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 413
    label "micha"
  ]
  node [
    id 414
    label "ucho"
  ]
  node [
    id 415
    label "profil"
  ]
  node [
    id 416
    label "podbr&#243;dek"
  ]
  node [
    id 417
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 418
    label "policzek"
  ]
  node [
    id 419
    label "posta&#263;"
  ]
  node [
    id 420
    label "cera"
  ]
  node [
    id 421
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 422
    label "czo&#322;o"
  ]
  node [
    id 423
    label "oko"
  ]
  node [
    id 424
    label "uj&#281;cie"
  ]
  node [
    id 425
    label "maskowato&#347;&#263;"
  ]
  node [
    id 426
    label "powieka"
  ]
  node [
    id 427
    label "nos"
  ]
  node [
    id 428
    label "wielko&#347;&#263;"
  ]
  node [
    id 429
    label "prz&#243;d"
  ]
  node [
    id 430
    label "zas&#322;ona"
  ]
  node [
    id 431
    label "twarzyczka"
  ]
  node [
    id 432
    label "pysk"
  ]
  node [
    id 433
    label "reputacja"
  ]
  node [
    id 434
    label "liczko"
  ]
  node [
    id 435
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 436
    label "p&#322;e&#263;"
  ]
  node [
    id 437
    label "ko&#324;"
  ]
  node [
    id 438
    label "ch&#322;osn&#261;&#263;"
  ]
  node [
    id 439
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 440
    label "odebra&#263;"
  ]
  node [
    id 441
    label "naci&#261;&#263;"
  ]
  node [
    id 442
    label "w&#281;dka"
  ]
  node [
    id 443
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 444
    label "zablokowa&#263;"
  ]
  node [
    id 445
    label "lejce"
  ]
  node [
    id 446
    label "zepsu&#263;"
  ]
  node [
    id 447
    label "nadci&#261;&#263;"
  ]
  node [
    id 448
    label "poderwa&#263;"
  ]
  node [
    id 449
    label "zrani&#263;"
  ]
  node [
    id 450
    label "ostruga&#263;"
  ]
  node [
    id 451
    label "uderzy&#263;"
  ]
  node [
    id 452
    label "bat"
  ]
  node [
    id 453
    label "przerwa&#263;"
  ]
  node [
    id 454
    label "cut"
  ]
  node [
    id 455
    label "wprawi&#263;"
  ]
  node [
    id 456
    label "write_out"
  ]
  node [
    id 457
    label "struganie"
  ]
  node [
    id 458
    label "padanie"
  ]
  node [
    id 459
    label "kaleczenie"
  ]
  node [
    id 460
    label "podrywanie"
  ]
  node [
    id 461
    label "mina"
  ]
  node [
    id 462
    label "w&#281;dkowanie"
  ]
  node [
    id 463
    label "powo&#380;enie"
  ]
  node [
    id 464
    label "nacinanie"
  ]
  node [
    id 465
    label "zaciskanie"
  ]
  node [
    id 466
    label "ch&#322;ostanie"
  ]
  node [
    id 467
    label "podrywa&#263;"
  ]
  node [
    id 468
    label "zaciska&#263;"
  ]
  node [
    id 469
    label "struga&#263;"
  ]
  node [
    id 470
    label "zaciera&#263;"
  ]
  node [
    id 471
    label "&#347;cina&#263;"
  ]
  node [
    id 472
    label "reduce"
  ]
  node [
    id 473
    label "blokowa&#263;"
  ]
  node [
    id 474
    label "wprawia&#263;"
  ]
  node [
    id 475
    label "zakrawa&#263;"
  ]
  node [
    id 476
    label "uderza&#263;"
  ]
  node [
    id 477
    label "przestawa&#263;"
  ]
  node [
    id 478
    label "nacina&#263;"
  ]
  node [
    id 479
    label "kaleczy&#263;"
  ]
  node [
    id 480
    label "hack"
  ]
  node [
    id 481
    label "pocina&#263;"
  ]
  node [
    id 482
    label "psu&#263;"
  ]
  node [
    id 483
    label "przerywa&#263;"
  ]
  node [
    id 484
    label "ch&#322;osta&#263;"
  ]
  node [
    id 485
    label "ch&#322;o&#347;ni&#281;cie"
  ]
  node [
    id 486
    label "stanowczo"
  ]
  node [
    id 487
    label "ostruganie"
  ]
  node [
    id 488
    label "zranienie"
  ]
  node [
    id 489
    label "nieust&#281;pliwie"
  ]
  node [
    id 490
    label "uwa&#380;nie"
  ]
  node [
    id 491
    label "formacja_skalna"
  ]
  node [
    id 492
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 493
    label "dash"
  ]
  node [
    id 494
    label "poderwanie"
  ]
  node [
    id 495
    label "talent"
  ]
  node [
    id 496
    label "turn"
  ]
  node [
    id 497
    label "capability"
  ]
  node [
    id 498
    label "go"
  ]
  node [
    id 499
    label "potkni&#281;cie"
  ]
  node [
    id 500
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 501
    label "zaci&#281;ty"
  ]
  node [
    id 502
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 503
    label "naci&#281;cie"
  ]
  node [
    id 504
    label "kszta&#322;t"
  ]
  node [
    id 505
    label "&#347;lina"
  ]
  node [
    id 506
    label "rusza&#263;"
  ]
  node [
    id 507
    label "sponge"
  ]
  node [
    id 508
    label "sucking"
  ]
  node [
    id 509
    label "wci&#261;ga&#263;"
  ]
  node [
    id 510
    label "pi&#263;"
  ]
  node [
    id 511
    label "smoczek"
  ]
  node [
    id 512
    label "rozpuszcza&#263;"
  ]
  node [
    id 513
    label "mleko"
  ]
  node [
    id 514
    label "narz&#261;d_g&#281;bowy"
  ]
  node [
    id 515
    label "rozpuszczanie"
  ]
  node [
    id 516
    label "wyssanie"
  ]
  node [
    id 517
    label "wci&#261;ganie"
  ]
  node [
    id 518
    label "wessanie"
  ]
  node [
    id 519
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 520
    label "ga&#378;nik"
  ]
  node [
    id 521
    label "ruszanie"
  ]
  node [
    id 522
    label "picie"
  ]
  node [
    id 523
    label "consumption"
  ]
  node [
    id 524
    label "aspiration"
  ]
  node [
    id 525
    label "odci&#261;ganie"
  ]
  node [
    id 526
    label "wysysanie"
  ]
  node [
    id 527
    label "mechanizm"
  ]
  node [
    id 528
    label "harcap"
  ]
  node [
    id 529
    label "rota"
  ]
  node [
    id 530
    label "Gurkha"
  ]
  node [
    id 531
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 532
    label "walcz&#261;cy"
  ]
  node [
    id 533
    label "elew"
  ]
  node [
    id 534
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 535
    label "demobilizowa&#263;"
  ]
  node [
    id 536
    label "zdemobilizowa&#263;"
  ]
  node [
    id 537
    label "so&#322;dat"
  ]
  node [
    id 538
    label "wojsko"
  ]
  node [
    id 539
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 540
    label "demobilizowanie"
  ]
  node [
    id 541
    label "zdemobilizowanie"
  ]
  node [
    id 542
    label "mundurowy"
  ]
  node [
    id 543
    label "&#380;o&#322;dowy"
  ]
  node [
    id 544
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 545
    label "pozycja"
  ]
  node [
    id 546
    label "rezerwa"
  ]
  node [
    id 547
    label "werbowanie_si&#281;"
  ]
  node [
    id 548
    label "Eurokorpus"
  ]
  node [
    id 549
    label "Armia_Czerwona"
  ]
  node [
    id 550
    label "ods&#322;ugiwanie"
  ]
  node [
    id 551
    label "dryl"
  ]
  node [
    id 552
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 553
    label "fala"
  ]
  node [
    id 554
    label "potencja"
  ]
  node [
    id 555
    label "soldateska"
  ]
  node [
    id 556
    label "przedmiot"
  ]
  node [
    id 557
    label "pospolite_ruszenie"
  ]
  node [
    id 558
    label "mobilizowa&#263;"
  ]
  node [
    id 559
    label "mobilizowanie"
  ]
  node [
    id 560
    label "tabor"
  ]
  node [
    id 561
    label "zrejterowanie"
  ]
  node [
    id 562
    label "dezerter"
  ]
  node [
    id 563
    label "s&#322;u&#380;ba"
  ]
  node [
    id 564
    label "korpus"
  ]
  node [
    id 565
    label "petarda"
  ]
  node [
    id 566
    label "zmobilizowanie"
  ]
  node [
    id 567
    label "szko&#322;a"
  ]
  node [
    id 568
    label "oddzia&#322;"
  ]
  node [
    id 569
    label "wojo"
  ]
  node [
    id 570
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 571
    label "oddzia&#322;_karny"
  ]
  node [
    id 572
    label "or&#281;&#380;"
  ]
  node [
    id 573
    label "obrona"
  ]
  node [
    id 574
    label "Armia_Krajowa"
  ]
  node [
    id 575
    label "wermacht"
  ]
  node [
    id 576
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 577
    label "Czerwona_Gwardia"
  ]
  node [
    id 578
    label "sztabslekarz"
  ]
  node [
    id 579
    label "zmobilizowa&#263;"
  ]
  node [
    id 580
    label "Legia_Cudzoziemska"
  ]
  node [
    id 581
    label "cofni&#281;cie"
  ]
  node [
    id 582
    label "zrejterowa&#263;"
  ]
  node [
    id 583
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 584
    label "rejterowa&#263;"
  ]
  node [
    id 585
    label "rejterowanie"
  ]
  node [
    id 586
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 587
    label "papie&#380;"
  ]
  node [
    id 588
    label "&#322;amanie"
  ]
  node [
    id 589
    label "przysi&#281;ga"
  ]
  node [
    id 590
    label "piecz&#261;tka"
  ]
  node [
    id 591
    label "whip"
  ]
  node [
    id 592
    label "s&#261;d_apelacyjny"
  ]
  node [
    id 593
    label "tortury"
  ]
  node [
    id 594
    label "instrument_strunowy"
  ]
  node [
    id 595
    label "chordofon_szarpany"
  ]
  node [
    id 596
    label "formu&#322;a"
  ]
  node [
    id 597
    label "s&#261;d_ko&#347;cielny"
  ]
  node [
    id 598
    label "&#322;ama&#263;"
  ]
  node [
    id 599
    label "Rota"
  ]
  node [
    id 600
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 601
    label "szyk"
  ]
  node [
    id 602
    label "zaw&#243;d"
  ]
  node [
    id 603
    label "grupa"
  ]
  node [
    id 604
    label "nasada"
  ]
  node [
    id 605
    label "profanum"
  ]
  node [
    id 606
    label "wz&#243;r"
  ]
  node [
    id 607
    label "senior"
  ]
  node [
    id 608
    label "os&#322;abia&#263;"
  ]
  node [
    id 609
    label "homo_sapiens"
  ]
  node [
    id 610
    label "osoba"
  ]
  node [
    id 611
    label "ludzko&#347;&#263;"
  ]
  node [
    id 612
    label "Adam"
  ]
  node [
    id 613
    label "hominid"
  ]
  node [
    id 614
    label "portrecista"
  ]
  node [
    id 615
    label "polifag"
  ]
  node [
    id 616
    label "podw&#322;adny"
  ]
  node [
    id 617
    label "dwun&#243;g"
  ]
  node [
    id 618
    label "wapniak"
  ]
  node [
    id 619
    label "duch"
  ]
  node [
    id 620
    label "os&#322;abianie"
  ]
  node [
    id 621
    label "antropochoria"
  ]
  node [
    id 622
    label "figura"
  ]
  node [
    id 623
    label "g&#322;owa"
  ]
  node [
    id 624
    label "mikrokosmos"
  ]
  node [
    id 625
    label "oddzia&#322;ywanie"
  ]
  node [
    id 626
    label "wojownik"
  ]
  node [
    id 627
    label "Nepal"
  ]
  node [
    id 628
    label "zreorganizowanie"
  ]
  node [
    id 629
    label "odstr&#281;czenie"
  ]
  node [
    id 630
    label "zniech&#281;cenie_si&#281;"
  ]
  node [
    id 631
    label "odprawienie"
  ]
  node [
    id 632
    label "zwalnianie"
  ]
  node [
    id 633
    label "zniech&#281;canie"
  ]
  node [
    id 634
    label "przebudowywanie"
  ]
  node [
    id 635
    label "zniech&#281;canie_si&#281;"
  ]
  node [
    id 636
    label "zwalnia&#263;"
  ]
  node [
    id 637
    label "zniech&#281;ca&#263;"
  ]
  node [
    id 638
    label "przebudowywa&#263;"
  ]
  node [
    id 639
    label "zreorganizowa&#263;"
  ]
  node [
    id 640
    label "odprawi&#263;"
  ]
  node [
    id 641
    label "zniech&#281;ci&#263;"
  ]
  node [
    id 642
    label "funkcjonariusz"
  ]
  node [
    id 643
    label "nosiciel"
  ]
  node [
    id 644
    label "ucze&#324;"
  ]
  node [
    id 645
    label "peruka"
  ]
  node [
    id 646
    label "warkocz"
  ]
  node [
    id 647
    label "energia"
  ]
  node [
    id 648
    label "wedyzm"
  ]
  node [
    id 649
    label "buddyzm"
  ]
  node [
    id 650
    label "egzergia"
  ]
  node [
    id 651
    label "emitowanie"
  ]
  node [
    id 652
    label "power"
  ]
  node [
    id 653
    label "zjawisko"
  ]
  node [
    id 654
    label "emitowa&#263;"
  ]
  node [
    id 655
    label "szwung"
  ]
  node [
    id 656
    label "energy"
  ]
  node [
    id 657
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 658
    label "kwant_energii"
  ]
  node [
    id 659
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 660
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 661
    label "Buddhism"
  ]
  node [
    id 662
    label "wad&#378;rajana"
  ]
  node [
    id 663
    label "asura"
  ]
  node [
    id 664
    label "tantryzm"
  ]
  node [
    id 665
    label "therawada"
  ]
  node [
    id 666
    label "mahajana"
  ]
  node [
    id 667
    label "kalpa"
  ]
  node [
    id 668
    label "li"
  ]
  node [
    id 669
    label "bardo"
  ]
  node [
    id 670
    label "ahinsa"
  ]
  node [
    id 671
    label "dana"
  ]
  node [
    id 672
    label "religia"
  ]
  node [
    id 673
    label "lampka_ma&#347;lana"
  ]
  node [
    id 674
    label "arahant"
  ]
  node [
    id 675
    label "hinajana"
  ]
  node [
    id 676
    label "bonzo"
  ]
  node [
    id 677
    label "hinduizm"
  ]
  node [
    id 678
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 679
    label "kanonistyka"
  ]
  node [
    id 680
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 681
    label "kazuistyka"
  ]
  node [
    id 682
    label "podmiot"
  ]
  node [
    id 683
    label "legislacyjnie"
  ]
  node [
    id 684
    label "zasada_d'Alemberta"
  ]
  node [
    id 685
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 686
    label "procesualistyka"
  ]
  node [
    id 687
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 688
    label "kryminalistyka"
  ]
  node [
    id 689
    label "opis"
  ]
  node [
    id 690
    label "regu&#322;a_Allena"
  ]
  node [
    id 691
    label "prawo_karne"
  ]
  node [
    id 692
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 693
    label "prawo_Mendla"
  ]
  node [
    id 694
    label "criterion"
  ]
  node [
    id 695
    label "standard"
  ]
  node [
    id 696
    label "obserwacja"
  ]
  node [
    id 697
    label "kultura_duchowa"
  ]
  node [
    id 698
    label "normatywizm"
  ]
  node [
    id 699
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 700
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 701
    label "umocowa&#263;"
  ]
  node [
    id 702
    label "cywilistyka"
  ]
  node [
    id 703
    label "nauka_prawa"
  ]
  node [
    id 704
    label "jurisprudence"
  ]
  node [
    id 705
    label "regu&#322;a_Glogera"
  ]
  node [
    id 706
    label "kryminologia"
  ]
  node [
    id 707
    label "zasada"
  ]
  node [
    id 708
    label "law"
  ]
  node [
    id 709
    label "qualification"
  ]
  node [
    id 710
    label "judykatura"
  ]
  node [
    id 711
    label "przepis"
  ]
  node [
    id 712
    label "prawo_karne_procesowe"
  ]
  node [
    id 713
    label "normalizacja"
  ]
  node [
    id 714
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 715
    label "wykonawczy"
  ]
  node [
    id 716
    label "dominion"
  ]
  node [
    id 717
    label "twierdzenie"
  ]
  node [
    id 718
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 719
    label "kierunek"
  ]
  node [
    id 720
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 721
    label "exposition"
  ]
  node [
    id 722
    label "zorganizowa&#263;"
  ]
  node [
    id 723
    label "zorganizowanie"
  ]
  node [
    id 724
    label "taniec_towarzyski"
  ]
  node [
    id 725
    label "model"
  ]
  node [
    id 726
    label "ordinariness"
  ]
  node [
    id 727
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 728
    label "organizowanie"
  ]
  node [
    id 729
    label "instytucja"
  ]
  node [
    id 730
    label "organizowa&#263;"
  ]
  node [
    id 731
    label "o&#347;"
  ]
  node [
    id 732
    label "zachowanie"
  ]
  node [
    id 733
    label "podsystem"
  ]
  node [
    id 734
    label "systemat"
  ]
  node [
    id 735
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 736
    label "system"
  ]
  node [
    id 737
    label "rozprz&#261;c"
  ]
  node [
    id 738
    label "konstrukcja"
  ]
  node [
    id 739
    label "cybernetyk"
  ]
  node [
    id 740
    label "mechanika"
  ]
  node [
    id 741
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 742
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 743
    label "konstelacja"
  ]
  node [
    id 744
    label "usenet"
  ]
  node [
    id 745
    label "sk&#322;ad"
  ]
  node [
    id 746
    label "linia"
  ]
  node [
    id 747
    label "przebieg"
  ]
  node [
    id 748
    label "orientowa&#263;"
  ]
  node [
    id 749
    label "zorientowa&#263;"
  ]
  node [
    id 750
    label "praktyka"
  ]
  node [
    id 751
    label "skr&#281;cenie"
  ]
  node [
    id 752
    label "skr&#281;ci&#263;"
  ]
  node [
    id 753
    label "przeorientowanie"
  ]
  node [
    id 754
    label "g&#243;ra"
  ]
  node [
    id 755
    label "orientowanie"
  ]
  node [
    id 756
    label "zorientowanie"
  ]
  node [
    id 757
    label "ty&#322;"
  ]
  node [
    id 758
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 759
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 760
    label "przeorientowywanie"
  ]
  node [
    id 761
    label "bok"
  ]
  node [
    id 762
    label "ideologia"
  ]
  node [
    id 763
    label "skr&#281;canie"
  ]
  node [
    id 764
    label "orientacja"
  ]
  node [
    id 765
    label "metoda"
  ]
  node [
    id 766
    label "studia"
  ]
  node [
    id 767
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 768
    label "przeorientowa&#263;"
  ]
  node [
    id 769
    label "bearing"
  ]
  node [
    id 770
    label "spos&#243;b"
  ]
  node [
    id 771
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 772
    label "skr&#281;ca&#263;"
  ]
  node [
    id 773
    label "przeorientowywa&#263;"
  ]
  node [
    id 774
    label "zdanie"
  ]
  node [
    id 775
    label "lekcja"
  ]
  node [
    id 776
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 777
    label "skolaryzacja"
  ]
  node [
    id 778
    label "wiedza"
  ]
  node [
    id 779
    label "lesson"
  ]
  node [
    id 780
    label "niepokalanki"
  ]
  node [
    id 781
    label "kwalifikacje"
  ]
  node [
    id 782
    label "Mickiewicz"
  ]
  node [
    id 783
    label "muzyka"
  ]
  node [
    id 784
    label "stopek"
  ]
  node [
    id 785
    label "school"
  ]
  node [
    id 786
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 787
    label "&#322;awa_szkolna"
  ]
  node [
    id 788
    label "przepisa&#263;"
  ]
  node [
    id 789
    label "nauka"
  ]
  node [
    id 790
    label "siedziba"
  ]
  node [
    id 791
    label "gabinet"
  ]
  node [
    id 792
    label "sekretariat"
  ]
  node [
    id 793
    label "szkolenie"
  ]
  node [
    id 794
    label "sztuba"
  ]
  node [
    id 795
    label "do&#347;wiadczenie"
  ]
  node [
    id 796
    label "podr&#281;cznik"
  ]
  node [
    id 797
    label "zda&#263;"
  ]
  node [
    id 798
    label "tablica"
  ]
  node [
    id 799
    label "przepisanie"
  ]
  node [
    id 800
    label "kara"
  ]
  node [
    id 801
    label "teren_szko&#322;y"
  ]
  node [
    id 802
    label "form"
  ]
  node [
    id 803
    label "czas"
  ]
  node [
    id 804
    label "urszulanki"
  ]
  node [
    id 805
    label "absolwent"
  ]
  node [
    id 806
    label "operator_modalny"
  ]
  node [
    id 807
    label "alternatywa"
  ]
  node [
    id 808
    label "wyb&#243;r"
  ]
  node [
    id 809
    label "egzekutywa"
  ]
  node [
    id 810
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 811
    label "prospect"
  ]
  node [
    id 812
    label "badanie"
  ]
  node [
    id 813
    label "proces_my&#347;lowy"
  ]
  node [
    id 814
    label "observation"
  ]
  node [
    id 815
    label "stwierdzenie"
  ]
  node [
    id 816
    label "proposition"
  ]
  node [
    id 817
    label "paradoks_Leontiefa"
  ]
  node [
    id 818
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 819
    label "twierdzenie_Pascala"
  ]
  node [
    id 820
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 821
    label "twierdzenie_Maya"
  ]
  node [
    id 822
    label "alternatywa_Fredholma"
  ]
  node [
    id 823
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 824
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 825
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 826
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 827
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 828
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 829
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 830
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 831
    label "komunikowanie"
  ]
  node [
    id 832
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 833
    label "teoria"
  ]
  node [
    id 834
    label "twierdzenie_Stokesa"
  ]
  node [
    id 835
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 836
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 837
    label "twierdzenie_Cevy"
  ]
  node [
    id 838
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 839
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 840
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 841
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 842
    label "zapewnianie"
  ]
  node [
    id 843
    label "teza"
  ]
  node [
    id 844
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 845
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 846
    label "oznajmianie"
  ]
  node [
    id 847
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 848
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 849
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 850
    label "twierdzenie_Pettisa"
  ]
  node [
    id 851
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 852
    label "relacja"
  ]
  node [
    id 853
    label "operacja"
  ]
  node [
    id 854
    label "zmiana"
  ]
  node [
    id 855
    label "proces"
  ]
  node [
    id 856
    label "dominance"
  ]
  node [
    id 857
    label "calibration"
  ]
  node [
    id 858
    label "standardization"
  ]
  node [
    id 859
    label "zabieg"
  ]
  node [
    id 860
    label "porz&#261;dek"
  ]
  node [
    id 861
    label "orzecznictwo"
  ]
  node [
    id 862
    label "wykonawczo"
  ]
  node [
    id 863
    label "byt"
  ]
  node [
    id 864
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 865
    label "organizacja"
  ]
  node [
    id 866
    label "osobowo&#347;&#263;"
  ]
  node [
    id 867
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 868
    label "set"
  ]
  node [
    id 869
    label "nada&#263;"
  ]
  node [
    id 870
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 871
    label "cook"
  ]
  node [
    id 872
    label "procedura"
  ]
  node [
    id 873
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 874
    label "regulation"
  ]
  node [
    id 875
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 876
    label "norma_prawna"
  ]
  node [
    id 877
    label "przedawnianie_si&#281;"
  ]
  node [
    id 878
    label "recepta"
  ]
  node [
    id 879
    label "przedawnienie_si&#281;"
  ]
  node [
    id 880
    label "porada"
  ]
  node [
    id 881
    label "kodeks"
  ]
  node [
    id 882
    label "prawid&#322;o"
  ]
  node [
    id 883
    label "base"
  ]
  node [
    id 884
    label "moralno&#347;&#263;"
  ]
  node [
    id 885
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 886
    label "umowa"
  ]
  node [
    id 887
    label "substancja"
  ]
  node [
    id 888
    label "occupation"
  ]
  node [
    id 889
    label "probabilizm"
  ]
  node [
    id 890
    label "manipulacja"
  ]
  node [
    id 891
    label "casuistry"
  ]
  node [
    id 892
    label "dermatoglifika"
  ]
  node [
    id 893
    label "technika_&#347;ledcza"
  ]
  node [
    id 894
    label "mikro&#347;lad"
  ]
  node [
    id 895
    label "kieliszek"
  ]
  node [
    id 896
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 897
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 898
    label "w&#243;dka"
  ]
  node [
    id 899
    label "ujednolicenie"
  ]
  node [
    id 900
    label "ten"
  ]
  node [
    id 901
    label "jaki&#347;"
  ]
  node [
    id 902
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 903
    label "jednakowy"
  ]
  node [
    id 904
    label "jednolicie"
  ]
  node [
    id 905
    label "shot"
  ]
  node [
    id 906
    label "naczynie"
  ]
  node [
    id 907
    label "zawarto&#347;&#263;"
  ]
  node [
    id 908
    label "szk&#322;o"
  ]
  node [
    id 909
    label "mohorycz"
  ]
  node [
    id 910
    label "gorza&#322;ka"
  ]
  node [
    id 911
    label "alkohol"
  ]
  node [
    id 912
    label "sznaps"
  ]
  node [
    id 913
    label "nap&#243;j"
  ]
  node [
    id 914
    label "okre&#347;lony"
  ]
  node [
    id 915
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 916
    label "taki&#380;"
  ]
  node [
    id 917
    label "identyczny"
  ]
  node [
    id 918
    label "zr&#243;wnanie"
  ]
  node [
    id 919
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 920
    label "zr&#243;wnywanie"
  ]
  node [
    id 921
    label "mundurowanie"
  ]
  node [
    id 922
    label "mundurowa&#263;"
  ]
  node [
    id 923
    label "jednakowo"
  ]
  node [
    id 924
    label "z&#322;o&#380;ony"
  ]
  node [
    id 925
    label "jako&#347;"
  ]
  node [
    id 926
    label "niez&#322;y"
  ]
  node [
    id 927
    label "charakterystyczny"
  ]
  node [
    id 928
    label "jako_tako"
  ]
  node [
    id 929
    label "ciekawy"
  ]
  node [
    id 930
    label "dziwny"
  ]
  node [
    id 931
    label "przyzwoity"
  ]
  node [
    id 932
    label "g&#322;&#281;bszy"
  ]
  node [
    id 933
    label "drink"
  ]
  node [
    id 934
    label "jednolity"
  ]
  node [
    id 935
    label "upodobnienie"
  ]
  node [
    id 936
    label "uderzenie"
  ]
  node [
    id 937
    label "time"
  ]
  node [
    id 938
    label "cios"
  ]
  node [
    id 939
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 940
    label "blok"
  ]
  node [
    id 941
    label "struktura_geologiczna"
  ]
  node [
    id 942
    label "pr&#243;ba"
  ]
  node [
    id 943
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 944
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 945
    label "coup"
  ]
  node [
    id 946
    label "siekacz"
  ]
  node [
    id 947
    label "pogorszenie"
  ]
  node [
    id 948
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 949
    label "spowodowanie"
  ]
  node [
    id 950
    label "d&#378;wi&#281;k"
  ]
  node [
    id 951
    label "odbicie"
  ]
  node [
    id 952
    label "odbicie_si&#281;"
  ]
  node [
    id 953
    label "dotkni&#281;cie"
  ]
  node [
    id 954
    label "zadanie"
  ]
  node [
    id 955
    label "pobicie"
  ]
  node [
    id 956
    label "skrytykowanie"
  ]
  node [
    id 957
    label "charge"
  ]
  node [
    id 958
    label "instrumentalizacja"
  ]
  node [
    id 959
    label "manewr"
  ]
  node [
    id 960
    label "st&#322;uczenie"
  ]
  node [
    id 961
    label "rush"
  ]
  node [
    id 962
    label "uderzanie"
  ]
  node [
    id 963
    label "walka"
  ]
  node [
    id 964
    label "pogoda"
  ]
  node [
    id 965
    label "stukni&#281;cie"
  ]
  node [
    id 966
    label "&#347;ci&#281;cie"
  ]
  node [
    id 967
    label "dotyk"
  ]
  node [
    id 968
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 969
    label "trafienie"
  ]
  node [
    id 970
    label "zagrywka"
  ]
  node [
    id 971
    label "zdarzenie_si&#281;"
  ]
  node [
    id 972
    label "dostanie"
  ]
  node [
    id 973
    label "poczucie"
  ]
  node [
    id 974
    label "reakcja"
  ]
  node [
    id 975
    label "stroke"
  ]
  node [
    id 976
    label "contact"
  ]
  node [
    id 977
    label "nast&#261;pienie"
  ]
  node [
    id 978
    label "flap"
  ]
  node [
    id 979
    label "wdarcie_si&#281;"
  ]
  node [
    id 980
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 981
    label "dawka"
  ]
  node [
    id 982
    label "tydzie&#324;"
  ]
  node [
    id 983
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 984
    label "rok"
  ]
  node [
    id 985
    label "miech"
  ]
  node [
    id 986
    label "kalendy"
  ]
  node [
    id 987
    label "moon"
  ]
  node [
    id 988
    label "peryselenium"
  ]
  node [
    id 989
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 990
    label "aposelenium"
  ]
  node [
    id 991
    label "Tytan"
  ]
  node [
    id 992
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 993
    label "satelita"
  ]
  node [
    id 994
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 995
    label "&#347;wiat&#322;o"
  ]
  node [
    id 996
    label "aparat_fotograficzny"
  ]
  node [
    id 997
    label "sakwa"
  ]
  node [
    id 998
    label "przyrz&#261;d"
  ]
  node [
    id 999
    label "w&#243;r"
  ]
  node [
    id 1000
    label "bag"
  ]
  node [
    id 1001
    label "torba"
  ]
  node [
    id 1002
    label "chronometria"
  ]
  node [
    id 1003
    label "odczyt"
  ]
  node [
    id 1004
    label "laba"
  ]
  node [
    id 1005
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1006
    label "time_period"
  ]
  node [
    id 1007
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1008
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1009
    label "Zeitgeist"
  ]
  node [
    id 1010
    label "pochodzenie"
  ]
  node [
    id 1011
    label "przep&#322;ywanie"
  ]
  node [
    id 1012
    label "schy&#322;ek"
  ]
  node [
    id 1013
    label "czwarty_wymiar"
  ]
  node [
    id 1014
    label "poprzedzi&#263;"
  ]
  node [
    id 1015
    label "czasokres"
  ]
  node [
    id 1016
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1017
    label "poprzedzenie"
  ]
  node [
    id 1018
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1019
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1020
    label "dzieje"
  ]
  node [
    id 1021
    label "zegar"
  ]
  node [
    id 1022
    label "koniugacja"
  ]
  node [
    id 1023
    label "trawi&#263;"
  ]
  node [
    id 1024
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1025
    label "poprzedza&#263;"
  ]
  node [
    id 1026
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1027
    label "trawienie"
  ]
  node [
    id 1028
    label "rachuba_czasu"
  ]
  node [
    id 1029
    label "poprzedzanie"
  ]
  node [
    id 1030
    label "okres_czasu"
  ]
  node [
    id 1031
    label "period"
  ]
  node [
    id 1032
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1033
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1034
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1035
    label "pochodzi&#263;"
  ]
  node [
    id 1036
    label "doba"
  ]
  node [
    id 1037
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 1038
    label "weekend"
  ]
  node [
    id 1039
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 1040
    label "pora_roku"
  ]
  node [
    id 1041
    label "kwarta&#322;"
  ]
  node [
    id 1042
    label "jubileusz"
  ]
  node [
    id 1043
    label "martwy_sezon"
  ]
  node [
    id 1044
    label "kurs"
  ]
  node [
    id 1045
    label "stulecie"
  ]
  node [
    id 1046
    label "cykl_astronomiczny"
  ]
  node [
    id 1047
    label "lata"
  ]
  node [
    id 1048
    label "p&#243;&#322;rocze"
  ]
  node [
    id 1049
    label "kalendarz"
  ]
  node [
    id 1050
    label "fold"
  ]
  node [
    id 1051
    label "obj&#261;&#263;"
  ]
  node [
    id 1052
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1053
    label "zagarnia&#263;"
  ]
  node [
    id 1054
    label "powodowa&#263;"
  ]
  node [
    id 1055
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 1056
    label "mie&#263;"
  ]
  node [
    id 1057
    label "dotyczy&#263;"
  ]
  node [
    id 1058
    label "meet"
  ]
  node [
    id 1059
    label "embrace"
  ]
  node [
    id 1060
    label "involve"
  ]
  node [
    id 1061
    label "zaskakiwa&#263;"
  ]
  node [
    id 1062
    label "obejmowanie"
  ]
  node [
    id 1063
    label "senator"
  ]
  node [
    id 1064
    label "dotyka&#263;"
  ]
  node [
    id 1065
    label "podejmowa&#263;"
  ]
  node [
    id 1066
    label "zmienia&#263;"
  ]
  node [
    id 1067
    label "podnosi&#263;"
  ]
  node [
    id 1068
    label "admit"
  ]
  node [
    id 1069
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1070
    label "drive"
  ]
  node [
    id 1071
    label "rise"
  ]
  node [
    id 1072
    label "draw"
  ]
  node [
    id 1073
    label "reagowa&#263;"
  ]
  node [
    id 1074
    label "spotyka&#263;"
  ]
  node [
    id 1075
    label "s&#261;siadowa&#263;"
  ]
  node [
    id 1076
    label "rani&#263;"
  ]
  node [
    id 1077
    label "treat"
  ]
  node [
    id 1078
    label "fall"
  ]
  node [
    id 1079
    label "fit"
  ]
  node [
    id 1080
    label "d&#322;o&#324;"
  ]
  node [
    id 1081
    label "motywowa&#263;"
  ]
  node [
    id 1082
    label "act"
  ]
  node [
    id 1083
    label "mie&#263;_miejsce"
  ]
  node [
    id 1084
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1085
    label "tycze&#263;"
  ]
  node [
    id 1086
    label "bargain"
  ]
  node [
    id 1087
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1088
    label "need"
  ]
  node [
    id 1089
    label "support"
  ]
  node [
    id 1090
    label "hide"
  ]
  node [
    id 1091
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1092
    label "aran&#380;acja"
  ]
  node [
    id 1093
    label "kawa&#322;ek"
  ]
  node [
    id 1094
    label "samorz&#261;dowiec"
  ]
  node [
    id 1095
    label "parlamentarzysta"
  ]
  node [
    id 1096
    label "klubista"
  ]
  node [
    id 1097
    label "patrycjat"
  ]
  node [
    id 1098
    label "polityk"
  ]
  node [
    id 1099
    label "senat"
  ]
  node [
    id 1100
    label "skuma&#263;"
  ]
  node [
    id 1101
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1102
    label "zacz&#261;&#263;"
  ]
  node [
    id 1103
    label "do"
  ]
  node [
    id 1104
    label "dotkn&#261;&#263;"
  ]
  node [
    id 1105
    label "zagarn&#261;&#263;"
  ]
  node [
    id 1106
    label "obj&#281;cie"
  ]
  node [
    id 1107
    label "podj&#261;&#263;"
  ]
  node [
    id 1108
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 1109
    label "assume"
  ]
  node [
    id 1110
    label "spowodowa&#263;"
  ]
  node [
    id 1111
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 1112
    label "manipulate"
  ]
  node [
    id 1113
    label "zabiera&#263;"
  ]
  node [
    id 1114
    label "ujmowa&#263;"
  ]
  node [
    id 1115
    label "przysuwa&#263;"
  ]
  node [
    id 1116
    label "porywa&#263;"
  ]
  node [
    id 1117
    label "gyp"
  ]
  node [
    id 1118
    label "scoop"
  ]
  node [
    id 1119
    label "zagarnianie"
  ]
  node [
    id 1120
    label "dzianie_si&#281;"
  ]
  node [
    id 1121
    label "powodowanie"
  ]
  node [
    id 1122
    label "t&#281;&#380;enie"
  ]
  node [
    id 1123
    label "bycie_w_posiadaniu"
  ]
  node [
    id 1124
    label "zobowi&#261;zywanie_si&#281;"
  ]
  node [
    id 1125
    label "inclusion"
  ]
  node [
    id 1126
    label "nadp&#322;yni&#281;cie"
  ]
  node [
    id 1127
    label "dotykanie"
  ]
  node [
    id 1128
    label "nadp&#322;ywanie"
  ]
  node [
    id 1129
    label "podejmowanie"
  ]
  node [
    id 1130
    label "podpadanie"
  ]
  node [
    id 1131
    label "odnoszenie_si&#281;"
  ]
  node [
    id 1132
    label "encompassment"
  ]
  node [
    id 1133
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1134
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 1135
    label "Fox"
  ]
  node [
    id 1136
    label "wpada&#263;"
  ]
  node [
    id 1137
    label "dziwi&#263;"
  ]
  node [
    id 1138
    label "surprise"
  ]
  node [
    id 1139
    label "brak"
  ]
  node [
    id 1140
    label "z&#322;y"
  ]
  node [
    id 1141
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1142
    label "jednowyrazowy"
  ]
  node [
    id 1143
    label "bliski"
  ]
  node [
    id 1144
    label "drobny"
  ]
  node [
    id 1145
    label "kr&#243;tko"
  ]
  node [
    id 1146
    label "s&#322;aby"
  ]
  node [
    id 1147
    label "szybki"
  ]
  node [
    id 1148
    label "niepomy&#347;lny"
  ]
  node [
    id 1149
    label "niegrzeczny"
  ]
  node [
    id 1150
    label "rozgniewanie"
  ]
  node [
    id 1151
    label "zdenerwowany"
  ]
  node [
    id 1152
    label "&#378;le"
  ]
  node [
    id 1153
    label "niemoralny"
  ]
  node [
    id 1154
    label "sierdzisty"
  ]
  node [
    id 1155
    label "pieski"
  ]
  node [
    id 1156
    label "zez&#322;oszczenie"
  ]
  node [
    id 1157
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 1158
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 1159
    label "z&#322;oszczenie"
  ]
  node [
    id 1160
    label "gniewanie"
  ]
  node [
    id 1161
    label "niekorzystny"
  ]
  node [
    id 1162
    label "syf"
  ]
  node [
    id 1163
    label "negatywny"
  ]
  node [
    id 1164
    label "sprawny"
  ]
  node [
    id 1165
    label "oszcz&#281;dny"
  ]
  node [
    id 1166
    label "efektywny"
  ]
  node [
    id 1167
    label "zwi&#281;&#378;le"
  ]
  node [
    id 1168
    label "lura"
  ]
  node [
    id 1169
    label "niedoskona&#322;y"
  ]
  node [
    id 1170
    label "przemijaj&#261;cy"
  ]
  node [
    id 1171
    label "zawodny"
  ]
  node [
    id 1172
    label "niefajny"
  ]
  node [
    id 1173
    label "md&#322;y"
  ]
  node [
    id 1174
    label "kiepsko"
  ]
  node [
    id 1175
    label "nietrwa&#322;y"
  ]
  node [
    id 1176
    label "nieumiej&#281;tny"
  ]
  node [
    id 1177
    label "mizerny"
  ]
  node [
    id 1178
    label "s&#322;abo"
  ]
  node [
    id 1179
    label "po&#347;ledni"
  ]
  node [
    id 1180
    label "nieznaczny"
  ]
  node [
    id 1181
    label "marnie"
  ]
  node [
    id 1182
    label "s&#322;abowity"
  ]
  node [
    id 1183
    label "nieudany"
  ]
  node [
    id 1184
    label "niemocny"
  ]
  node [
    id 1185
    label "&#322;agodny"
  ]
  node [
    id 1186
    label "niezdrowy"
  ]
  node [
    id 1187
    label "delikatny"
  ]
  node [
    id 1188
    label "przysz&#322;y"
  ]
  node [
    id 1189
    label "silny"
  ]
  node [
    id 1190
    label "ma&#322;y"
  ]
  node [
    id 1191
    label "zwi&#261;zany"
  ]
  node [
    id 1192
    label "przesz&#322;y"
  ]
  node [
    id 1193
    label "dok&#322;adny"
  ]
  node [
    id 1194
    label "nieodleg&#322;y"
  ]
  node [
    id 1195
    label "oddalony"
  ]
  node [
    id 1196
    label "znajomy"
  ]
  node [
    id 1197
    label "gotowy"
  ]
  node [
    id 1198
    label "blisko"
  ]
  node [
    id 1199
    label "zbli&#380;enie"
  ]
  node [
    id 1200
    label "bystrolotny"
  ]
  node [
    id 1201
    label "dynamiczny"
  ]
  node [
    id 1202
    label "prosty"
  ]
  node [
    id 1203
    label "bezpo&#347;redni"
  ]
  node [
    id 1204
    label "temperamentny"
  ]
  node [
    id 1205
    label "szybko"
  ]
  node [
    id 1206
    label "intensywny"
  ]
  node [
    id 1207
    label "energiczny"
  ]
  node [
    id 1208
    label "szczup&#322;y"
  ]
  node [
    id 1209
    label "skromny"
  ]
  node [
    id 1210
    label "ma&#322;oletni"
  ]
  node [
    id 1211
    label "niesamodzielny"
  ]
  node [
    id 1212
    label "taniec_ludowy"
  ]
  node [
    id 1213
    label "podhala&#324;ski"
  ]
  node [
    id 1214
    label "drobno"
  ]
  node [
    id 1215
    label "niewa&#380;ny"
  ]
  node [
    id 1216
    label "defect"
  ]
  node [
    id 1217
    label "prywatywny"
  ]
  node [
    id 1218
    label "wyr&#243;b"
  ]
  node [
    id 1219
    label "odej&#347;cie"
  ]
  node [
    id 1220
    label "odchodzi&#263;"
  ]
  node [
    id 1221
    label "odchodzenie"
  ]
  node [
    id 1222
    label "odej&#347;&#263;"
  ]
  node [
    id 1223
    label "nieistnienie"
  ]
  node [
    id 1224
    label "gap"
  ]
  node [
    id 1225
    label "wada"
  ]
  node [
    id 1226
    label "jednocz&#322;onowy"
  ]
  node [
    id 1227
    label "move"
  ]
  node [
    id 1228
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1229
    label "utrzymywanie"
  ]
  node [
    id 1230
    label "utrzymywa&#263;"
  ]
  node [
    id 1231
    label "taktyka"
  ]
  node [
    id 1232
    label "d&#322;ugi"
  ]
  node [
    id 1233
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1234
    label "natural_process"
  ]
  node [
    id 1235
    label "kanciasty"
  ]
  node [
    id 1236
    label "utrzyma&#263;"
  ]
  node [
    id 1237
    label "myk"
  ]
  node [
    id 1238
    label "utrzymanie"
  ]
  node [
    id 1239
    label "tumult"
  ]
  node [
    id 1240
    label "movement"
  ]
  node [
    id 1241
    label "strumie&#324;"
  ]
  node [
    id 1242
    label "lokomocja"
  ]
  node [
    id 1243
    label "drift"
  ]
  node [
    id 1244
    label "commercial_enterprise"
  ]
  node [
    id 1245
    label "apraksja"
  ]
  node [
    id 1246
    label "poruszenie"
  ]
  node [
    id 1247
    label "travel"
  ]
  node [
    id 1248
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1249
    label "dyssypacja_energii"
  ]
  node [
    id 1250
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1251
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 1252
    label "infrastruktura"
  ]
  node [
    id 1253
    label "w&#281;ze&#322;"
  ]
  node [
    id 1254
    label "podbieg"
  ]
  node [
    id 1255
    label "marszrutyzacja"
  ]
  node [
    id 1256
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 1257
    label "droga"
  ]
  node [
    id 1258
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1259
    label "cycle"
  ]
  node [
    id 1260
    label "zbi&#243;r"
  ]
  node [
    id 1261
    label "sequence"
  ]
  node [
    id 1262
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1263
    label "room"
  ]
  node [
    id 1264
    label "odcinek"
  ]
  node [
    id 1265
    label "ton"
  ]
  node [
    id 1266
    label "ambitus"
  ]
  node [
    id 1267
    label "rozmiar"
  ]
  node [
    id 1268
    label "skala"
  ]
  node [
    id 1269
    label "ekwipunek"
  ]
  node [
    id 1270
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 1271
    label "wyb&#243;j"
  ]
  node [
    id 1272
    label "pobocze"
  ]
  node [
    id 1273
    label "ekskursja"
  ]
  node [
    id 1274
    label "drogowskaz"
  ]
  node [
    id 1275
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 1276
    label "budowla"
  ]
  node [
    id 1277
    label "rajza"
  ]
  node [
    id 1278
    label "passage"
  ]
  node [
    id 1279
    label "zbior&#243;wka"
  ]
  node [
    id 1280
    label "turystyka"
  ]
  node [
    id 1281
    label "wylot"
  ]
  node [
    id 1282
    label "bezsilnikowy"
  ]
  node [
    id 1283
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1284
    label "nawierzchnia"
  ]
  node [
    id 1285
    label "korona_drogi"
  ]
  node [
    id 1286
    label "bieg"
  ]
  node [
    id 1287
    label "stray"
  ]
  node [
    id 1288
    label "pozostawa&#263;"
  ]
  node [
    id 1289
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1290
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1291
    label "digress"
  ]
  node [
    id 1292
    label "chodzi&#263;"
  ]
  node [
    id 1293
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 1294
    label "mieszanie_si&#281;"
  ]
  node [
    id 1295
    label "chodzenie"
  ]
  node [
    id 1296
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1297
    label "poj&#281;cie"
  ]
  node [
    id 1298
    label "zwi&#261;zanie"
  ]
  node [
    id 1299
    label "graf"
  ]
  node [
    id 1300
    label "band"
  ]
  node [
    id 1301
    label "zwi&#261;zek"
  ]
  node [
    id 1302
    label "argument"
  ]
  node [
    id 1303
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1304
    label "marriage"
  ]
  node [
    id 1305
    label "bratnia_dusza"
  ]
  node [
    id 1306
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 1307
    label "fala_stoj&#261;ca"
  ]
  node [
    id 1308
    label "mila_morska"
  ]
  node [
    id 1309
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1310
    label "uczesanie"
  ]
  node [
    id 1311
    label "punkt"
  ]
  node [
    id 1312
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 1313
    label "zgrubienie"
  ]
  node [
    id 1314
    label "pismo_klinowe"
  ]
  node [
    id 1315
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1316
    label "o&#347;rodek"
  ]
  node [
    id 1317
    label "problem"
  ]
  node [
    id 1318
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1319
    label "wi&#261;zanie"
  ]
  node [
    id 1320
    label "akcja"
  ]
  node [
    id 1321
    label "przeci&#281;cie"
  ]
  node [
    id 1322
    label "skupienie"
  ]
  node [
    id 1323
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1324
    label "tying"
  ]
  node [
    id 1325
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1326
    label "hitch"
  ]
  node [
    id 1327
    label "orbita"
  ]
  node [
    id 1328
    label "kryszta&#322;"
  ]
  node [
    id 1329
    label "ekliptyka"
  ]
  node [
    id 1330
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1331
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 1332
    label "zaplecze"
  ]
  node [
    id 1333
    label "radiofonia"
  ]
  node [
    id 1334
    label "telefonia"
  ]
  node [
    id 1335
    label "chemikalia"
  ]
  node [
    id 1336
    label "abstrakcja"
  ]
  node [
    id 1337
    label "narz&#281;dzie"
  ]
  node [
    id 1338
    label "nature"
  ]
  node [
    id 1339
    label "tryb"
  ]
  node [
    id 1340
    label "obiekt_matematyczny"
  ]
  node [
    id 1341
    label "stopie&#324;_pisma"
  ]
  node [
    id 1342
    label "problemat"
  ]
  node [
    id 1343
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1344
    label "obiekt"
  ]
  node [
    id 1345
    label "point"
  ]
  node [
    id 1346
    label "plamka"
  ]
  node [
    id 1347
    label "mark"
  ]
  node [
    id 1348
    label "ust&#281;p"
  ]
  node [
    id 1349
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1350
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1351
    label "kres"
  ]
  node [
    id 1352
    label "plan"
  ]
  node [
    id 1353
    label "podpunkt"
  ]
  node [
    id 1354
    label "jednostka"
  ]
  node [
    id 1355
    label "sprawa"
  ]
  node [
    id 1356
    label "problematyka"
  ]
  node [
    id 1357
    label "prosta"
  ]
  node [
    id 1358
    label "zapunktowa&#263;"
  ]
  node [
    id 1359
    label "temperatura_krytyczna"
  ]
  node [
    id 1360
    label "materia"
  ]
  node [
    id 1361
    label "smolisty"
  ]
  node [
    id 1362
    label "przenika&#263;"
  ]
  node [
    id 1363
    label "cz&#261;steczka"
  ]
  node [
    id 1364
    label "przenikanie"
  ]
  node [
    id 1365
    label "abstractedness"
  ]
  node [
    id 1366
    label "obraz"
  ]
  node [
    id 1367
    label "abstraction"
  ]
  node [
    id 1368
    label "sytuacja"
  ]
  node [
    id 1369
    label "spalanie"
  ]
  node [
    id 1370
    label "spalenie"
  ]
  node [
    id 1371
    label "upublicznienie"
  ]
  node [
    id 1372
    label "publicznie"
  ]
  node [
    id 1373
    label "upublicznianie"
  ]
  node [
    id 1374
    label "jawny"
  ]
  node [
    id 1375
    label "jawnie"
  ]
  node [
    id 1376
    label "udost&#281;pnianie"
  ]
  node [
    id 1377
    label "udost&#281;pnienie"
  ]
  node [
    id 1378
    label "ujawnianie"
  ]
  node [
    id 1379
    label "ujawnienie_si&#281;"
  ]
  node [
    id 1380
    label "ujawnianie_si&#281;"
  ]
  node [
    id 1381
    label "ewidentny"
  ]
  node [
    id 1382
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 1383
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 1384
    label "zdecydowany"
  ]
  node [
    id 1385
    label "ujawnienie"
  ]
  node [
    id 1386
    label "traffic"
  ]
  node [
    id 1387
    label "jednoszynowy"
  ]
  node [
    id 1388
    label "tyfon"
  ]
  node [
    id 1389
    label "gospodarka"
  ]
  node [
    id 1390
    label "prze&#322;adunek"
  ]
  node [
    id 1391
    label "cedu&#322;a"
  ]
  node [
    id 1392
    label "unos"
  ]
  node [
    id 1393
    label "towar"
  ]
  node [
    id 1394
    label "us&#322;uga"
  ]
  node [
    id 1395
    label "za&#322;adunek"
  ]
  node [
    id 1396
    label "sprz&#281;t"
  ]
  node [
    id 1397
    label "roz&#322;adunek"
  ]
  node [
    id 1398
    label "produkt_gotowy"
  ]
  node [
    id 1399
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1400
    label "service"
  ]
  node [
    id 1401
    label "asortyment"
  ]
  node [
    id 1402
    label "&#347;wiadczenie"
  ]
  node [
    id 1403
    label "kompozycja"
  ]
  node [
    id 1404
    label "pakiet_klimatyczny"
  ]
  node [
    id 1405
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1406
    label "type"
  ]
  node [
    id 1407
    label "gromada"
  ]
  node [
    id 1408
    label "specgrupa"
  ]
  node [
    id 1409
    label "stage_set"
  ]
  node [
    id 1410
    label "odm&#322;odzenie"
  ]
  node [
    id 1411
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1412
    label "harcerze_starsi"
  ]
  node [
    id 1413
    label "jednostka_systematyczna"
  ]
  node [
    id 1414
    label "category"
  ]
  node [
    id 1415
    label "liga"
  ]
  node [
    id 1416
    label "&#346;wietliki"
  ]
  node [
    id 1417
    label "formacja_geologiczna"
  ]
  node [
    id 1418
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1419
    label "Eurogrupa"
  ]
  node [
    id 1420
    label "Terranie"
  ]
  node [
    id 1421
    label "odm&#322;adzanie"
  ]
  node [
    id 1422
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1423
    label "Entuzjastki"
  ]
  node [
    id 1424
    label "rzuca&#263;"
  ]
  node [
    id 1425
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 1426
    label "szprycowa&#263;"
  ]
  node [
    id 1427
    label "rzucanie"
  ]
  node [
    id 1428
    label "&#322;&#243;dzki"
  ]
  node [
    id 1429
    label "za&#322;adownia"
  ]
  node [
    id 1430
    label "naszprycowanie"
  ]
  node [
    id 1431
    label "tkanina"
  ]
  node [
    id 1432
    label "szprycowanie"
  ]
  node [
    id 1433
    label "obr&#243;t_handlowy"
  ]
  node [
    id 1434
    label "narkobiznes"
  ]
  node [
    id 1435
    label "metka"
  ]
  node [
    id 1436
    label "tandeta"
  ]
  node [
    id 1437
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 1438
    label "naszprycowa&#263;"
  ]
  node [
    id 1439
    label "informacja"
  ]
  node [
    id 1440
    label "temat"
  ]
  node [
    id 1441
    label "wn&#281;trze"
  ]
  node [
    id 1442
    label "sprz&#281;cik"
  ]
  node [
    id 1443
    label "equipment"
  ]
  node [
    id 1444
    label "furniture"
  ]
  node [
    id 1445
    label "kolekcja"
  ]
  node [
    id 1446
    label "sprz&#281;cior"
  ]
  node [
    id 1447
    label "penis"
  ]
  node [
    id 1448
    label "relokacja"
  ]
  node [
    id 1449
    label "kolej"
  ]
  node [
    id 1450
    label "spis"
  ]
  node [
    id 1451
    label "raport"
  ]
  node [
    id 1452
    label "sygnalizator"
  ]
  node [
    id 1453
    label "granica"
  ]
  node [
    id 1454
    label "&#322;adunek"
  ]
  node [
    id 1455
    label "ci&#281;&#380;ar"
  ]
  node [
    id 1456
    label "czerwona_strefa"
  ]
  node [
    id 1457
    label "stodo&#322;a"
  ]
  node [
    id 1458
    label "sektor_prywatny"
  ]
  node [
    id 1459
    label "gospodarowanie"
  ]
  node [
    id 1460
    label "wytw&#243;rnia"
  ]
  node [
    id 1461
    label "gospodarka_wodna"
  ]
  node [
    id 1462
    label "regulacja_cen"
  ]
  node [
    id 1463
    label "fabryka"
  ]
  node [
    id 1464
    label "sch&#322;odzenie"
  ]
  node [
    id 1465
    label "pole"
  ]
  node [
    id 1466
    label "bankowo&#347;&#263;"
  ]
  node [
    id 1467
    label "szkolnictwo"
  ]
  node [
    id 1468
    label "inwentarz"
  ]
  node [
    id 1469
    label "administracja"
  ]
  node [
    id 1470
    label "gospodarowa&#263;"
  ]
  node [
    id 1471
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 1472
    label "spichlerz"
  ]
  node [
    id 1473
    label "obora"
  ]
  node [
    id 1474
    label "produkowanie"
  ]
  node [
    id 1475
    label "przemys&#322;"
  ]
  node [
    id 1476
    label "farmaceutyka"
  ]
  node [
    id 1477
    label "miejsce_pracy"
  ]
  node [
    id 1478
    label "rolnictwo"
  ]
  node [
    id 1479
    label "sektor_publiczny"
  ]
  node [
    id 1480
    label "gospodarka_le&#347;na"
  ]
  node [
    id 1481
    label "agregat_ekonomiczny"
  ]
  node [
    id 1482
    label "mieszkalnictwo"
  ]
  node [
    id 1483
    label "sch&#322;adza&#263;"
  ]
  node [
    id 1484
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 1485
    label "sch&#322;adzanie"
  ]
  node [
    id 1486
    label "obronno&#347;&#263;"
  ]
  node [
    id 1487
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 1488
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 1489
    label "rynek"
  ]
  node [
    id 1490
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 1491
    label "zbiorowo"
  ]
  node [
    id 1492
    label "wsp&#243;lny"
  ]
  node [
    id 1493
    label "uwsp&#243;lnienie"
  ]
  node [
    id 1494
    label "sp&#243;lny"
  ]
  node [
    id 1495
    label "spolny"
  ]
  node [
    id 1496
    label "wsp&#243;lnie"
  ]
  node [
    id 1497
    label "uwsp&#243;lnianie"
  ]
  node [
    id 1498
    label "licznie"
  ]
  node [
    id 1499
    label "komunikacyjny"
  ]
  node [
    id 1500
    label "dogodny"
  ]
  node [
    id 1501
    label "zaleta"
  ]
  node [
    id 1502
    label "botanika"
  ]
  node [
    id 1503
    label "jako&#347;&#263;"
  ]
  node [
    id 1504
    label "warstwa"
  ]
  node [
    id 1505
    label "atak"
  ]
  node [
    id 1506
    label "mecz_mistrzowski"
  ]
  node [
    id 1507
    label "class"
  ]
  node [
    id 1508
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1509
    label "Ekwici"
  ]
  node [
    id 1510
    label "sala"
  ]
  node [
    id 1511
    label "wagon"
  ]
  node [
    id 1512
    label "promocja"
  ]
  node [
    id 1513
    label "arrangement"
  ]
  node [
    id 1514
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1515
    label "programowanie_obiektowe"
  ]
  node [
    id 1516
    label "&#347;rodowisko"
  ]
  node [
    id 1517
    label "dziennik_lekcyjny"
  ]
  node [
    id 1518
    label "typ"
  ]
  node [
    id 1519
    label "znak_jako&#347;ci"
  ]
  node [
    id 1520
    label "&#322;awka"
  ]
  node [
    id 1521
    label "poziom"
  ]
  node [
    id 1522
    label "fakcja"
  ]
  node [
    id 1523
    label "pomoc"
  ]
  node [
    id 1524
    label "leksem"
  ]
  node [
    id 1525
    label "exclamation_mark"
  ]
  node [
    id 1526
    label "wypowiedzenie"
  ]
  node [
    id 1527
    label "znak_interpunkcyjny"
  ]
  node [
    id 1528
    label "quality"
  ]
  node [
    id 1529
    label "co&#347;"
  ]
  node [
    id 1530
    label "state"
  ]
  node [
    id 1531
    label "warto&#347;&#263;"
  ]
  node [
    id 1532
    label "podwarstwa"
  ]
  node [
    id 1533
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1534
    label "p&#322;aszczyzna"
  ]
  node [
    id 1535
    label "covering"
  ]
  node [
    id 1536
    label "przek&#322;adaniec"
  ]
  node [
    id 1537
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1538
    label "ekosystem"
  ]
  node [
    id 1539
    label "huczek"
  ]
  node [
    id 1540
    label "otoczenie"
  ]
  node [
    id 1541
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1542
    label "wszechstworzenie"
  ]
  node [
    id 1543
    label "warunki"
  ]
  node [
    id 1544
    label "fauna"
  ]
  node [
    id 1545
    label "stw&#243;r"
  ]
  node [
    id 1546
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1547
    label "teren"
  ]
  node [
    id 1548
    label "Ziemia"
  ]
  node [
    id 1549
    label "rzecz"
  ]
  node [
    id 1550
    label "woda"
  ]
  node [
    id 1551
    label "biota"
  ]
  node [
    id 1552
    label "environment"
  ]
  node [
    id 1553
    label "obiekt_naturalny"
  ]
  node [
    id 1554
    label "discipline"
  ]
  node [
    id 1555
    label "zboczy&#263;"
  ]
  node [
    id 1556
    label "w&#261;tek"
  ]
  node [
    id 1557
    label "kultura"
  ]
  node [
    id 1558
    label "entity"
  ]
  node [
    id 1559
    label "sponiewiera&#263;"
  ]
  node [
    id 1560
    label "zboczenie"
  ]
  node [
    id 1561
    label "zbaczanie"
  ]
  node [
    id 1562
    label "thing"
  ]
  node [
    id 1563
    label "om&#243;wi&#263;"
  ]
  node [
    id 1564
    label "tre&#347;&#263;"
  ]
  node [
    id 1565
    label "element"
  ]
  node [
    id 1566
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1567
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1568
    label "istota"
  ]
  node [
    id 1569
    label "zbacza&#263;"
  ]
  node [
    id 1570
    label "om&#243;wienie"
  ]
  node [
    id 1571
    label "tematyka"
  ]
  node [
    id 1572
    label "omawianie"
  ]
  node [
    id 1573
    label "omawia&#263;"
  ]
  node [
    id 1574
    label "program_nauczania"
  ]
  node [
    id 1575
    label "sponiewieranie"
  ]
  node [
    id 1576
    label "korzy&#347;&#263;"
  ]
  node [
    id 1577
    label "rewaluowanie"
  ]
  node [
    id 1578
    label "zrewaluowa&#263;"
  ]
  node [
    id 1579
    label "rewaluowa&#263;"
  ]
  node [
    id 1580
    label "wabik"
  ]
  node [
    id 1581
    label "strona"
  ]
  node [
    id 1582
    label "zrewaluowanie"
  ]
  node [
    id 1583
    label "szczebel"
  ]
  node [
    id 1584
    label "punkt_widzenia"
  ]
  node [
    id 1585
    label "budynek"
  ]
  node [
    id 1586
    label "ranga"
  ]
  node [
    id 1587
    label "wyk&#322;adnik"
  ]
  node [
    id 1588
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1589
    label "faza"
  ]
  node [
    id 1590
    label "antycypacja"
  ]
  node [
    id 1591
    label "facet"
  ]
  node [
    id 1592
    label "przypuszczenie"
  ]
  node [
    id 1593
    label "kr&#243;lestwo"
  ]
  node [
    id 1594
    label "autorament"
  ]
  node [
    id 1595
    label "sztuka"
  ]
  node [
    id 1596
    label "cynk"
  ]
  node [
    id 1597
    label "variety"
  ]
  node [
    id 1598
    label "obstawia&#263;"
  ]
  node [
    id 1599
    label "design"
  ]
  node [
    id 1600
    label "uprawianie"
  ]
  node [
    id 1601
    label "collection"
  ]
  node [
    id 1602
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1603
    label "gathering"
  ]
  node [
    id 1604
    label "album"
  ]
  node [
    id 1605
    label "praca_rolnicza"
  ]
  node [
    id 1606
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1607
    label "sum"
  ]
  node [
    id 1608
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1609
    label "series"
  ]
  node [
    id 1610
    label "dane"
  ]
  node [
    id 1611
    label "publiczno&#347;&#263;"
  ]
  node [
    id 1612
    label "zgromadzenie"
  ]
  node [
    id 1613
    label "pomieszczenie"
  ]
  node [
    id 1614
    label "audience"
  ]
  node [
    id 1615
    label "program"
  ]
  node [
    id 1616
    label "przybud&#243;wka"
  ]
  node [
    id 1617
    label "organization"
  ]
  node [
    id 1618
    label "od&#322;am"
  ]
  node [
    id 1619
    label "TOPR"
  ]
  node [
    id 1620
    label "komitet_koordynacyjny"
  ]
  node [
    id 1621
    label "przedstawicielstwo"
  ]
  node [
    id 1622
    label "ZMP"
  ]
  node [
    id 1623
    label "Cepelia"
  ]
  node [
    id 1624
    label "GOPR"
  ]
  node [
    id 1625
    label "endecki"
  ]
  node [
    id 1626
    label "ZBoWiD"
  ]
  node [
    id 1627
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1628
    label "boj&#243;wka"
  ]
  node [
    id 1629
    label "ZOMO"
  ]
  node [
    id 1630
    label "centrala"
  ]
  node [
    id 1631
    label "harmonijka"
  ]
  node [
    id 1632
    label "czo&#322;ownica"
  ]
  node [
    id 1633
    label "tramwaj"
  ]
  node [
    id 1634
    label "karton"
  ]
  node [
    id 1635
    label "palinologia"
  ]
  node [
    id 1636
    label "herboryzowa&#263;"
  ]
  node [
    id 1637
    label "geobotanika"
  ]
  node [
    id 1638
    label "herboryzowanie"
  ]
  node [
    id 1639
    label "dendrologia"
  ]
  node [
    id 1640
    label "fitopatologia"
  ]
  node [
    id 1641
    label "fitosocjologia"
  ]
  node [
    id 1642
    label "etnobotanika"
  ]
  node [
    id 1643
    label "algologia"
  ]
  node [
    id 1644
    label "hylobiologia"
  ]
  node [
    id 1645
    label "fitogeografia"
  ]
  node [
    id 1646
    label "biologia"
  ]
  node [
    id 1647
    label "botanika_farmaceutyczna"
  ]
  node [
    id 1648
    label "lichenologia"
  ]
  node [
    id 1649
    label "pteridologia"
  ]
  node [
    id 1650
    label "embriologia_ro&#347;lin"
  ]
  node [
    id 1651
    label "organologia"
  ]
  node [
    id 1652
    label "udzieli&#263;"
  ]
  node [
    id 1653
    label "brief"
  ]
  node [
    id 1654
    label "zamiana"
  ]
  node [
    id 1655
    label "szachy"
  ]
  node [
    id 1656
    label "warcaby"
  ]
  node [
    id 1657
    label "sprzeda&#380;"
  ]
  node [
    id 1658
    label "uzyska&#263;"
  ]
  node [
    id 1659
    label "nominacja"
  ]
  node [
    id 1660
    label "promotion"
  ]
  node [
    id 1661
    label "promowa&#263;"
  ]
  node [
    id 1662
    label "graduacja"
  ]
  node [
    id 1663
    label "bran&#380;a"
  ]
  node [
    id 1664
    label "impreza"
  ]
  node [
    id 1665
    label "gradation"
  ]
  node [
    id 1666
    label "wypromowa&#263;"
  ]
  node [
    id 1667
    label "decyzja"
  ]
  node [
    id 1668
    label "damka"
  ]
  node [
    id 1669
    label "&#347;wiadectwo"
  ]
  node [
    id 1670
    label "popularyzacja"
  ]
  node [
    id 1671
    label "commencement"
  ]
  node [
    id 1672
    label "okazja"
  ]
  node [
    id 1673
    label "przekazanie"
  ]
  node [
    id 1674
    label "przeniesienie"
  ]
  node [
    id 1675
    label "testament"
  ]
  node [
    id 1676
    label "answer"
  ]
  node [
    id 1677
    label "transcription"
  ]
  node [
    id 1678
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1679
    label "zalecenie"
  ]
  node [
    id 1680
    label "lekarstwo"
  ]
  node [
    id 1681
    label "skopiowanie"
  ]
  node [
    id 1682
    label "zwy&#380;kowanie"
  ]
  node [
    id 1683
    label "manner"
  ]
  node [
    id 1684
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1685
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1686
    label "deprecjacja"
  ]
  node [
    id 1687
    label "stawka"
  ]
  node [
    id 1688
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1689
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1690
    label "seria"
  ]
  node [
    id 1691
    label "Lira"
  ]
  node [
    id 1692
    label "course"
  ]
  node [
    id 1693
    label "zni&#380;kowanie"
  ]
  node [
    id 1694
    label "zaj&#281;cia"
  ]
  node [
    id 1695
    label "przenie&#347;&#263;"
  ]
  node [
    id 1696
    label "supply"
  ]
  node [
    id 1697
    label "skopiowa&#263;"
  ]
  node [
    id 1698
    label "zaleci&#263;"
  ]
  node [
    id 1699
    label "przekaza&#263;"
  ]
  node [
    id 1700
    label "rewrite"
  ]
  node [
    id 1701
    label "zrzec_si&#281;"
  ]
  node [
    id 1702
    label "kategoria"
  ]
  node [
    id 1703
    label "blat"
  ]
  node [
    id 1704
    label "siedzenie"
  ]
  node [
    id 1705
    label "krzes&#322;o"
  ]
  node [
    id 1706
    label "mebel"
  ]
  node [
    id 1707
    label "tarcza"
  ]
  node [
    id 1708
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1709
    label "kosz"
  ]
  node [
    id 1710
    label "plate"
  ]
  node [
    id 1711
    label "rozmiar&#243;wka"
  ]
  node [
    id 1712
    label "kontener"
  ]
  node [
    id 1713
    label "chart"
  ]
  node [
    id 1714
    label "rubryka"
  ]
  node [
    id 1715
    label "szachownica_Punnetta"
  ]
  node [
    id 1716
    label "transparent"
  ]
  node [
    id 1717
    label "zas&#243;b"
  ]
  node [
    id 1718
    label "resource"
  ]
  node [
    id 1719
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 1720
    label "zapasy"
  ]
  node [
    id 1721
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 1722
    label "nieufno&#347;&#263;"
  ]
  node [
    id 1723
    label "telefon_zaufania"
  ]
  node [
    id 1724
    label "property"
  ]
  node [
    id 1725
    label "zgodzi&#263;"
  ]
  node [
    id 1726
    label "darowizna"
  ]
  node [
    id 1727
    label "pomocnik"
  ]
  node [
    id 1728
    label "doch&#243;d"
  ]
  node [
    id 1729
    label "&#380;&#261;danie"
  ]
  node [
    id 1730
    label "ofensywa"
  ]
  node [
    id 1731
    label "oznaka"
  ]
  node [
    id 1732
    label "przyp&#322;yw"
  ]
  node [
    id 1733
    label "krytyka"
  ]
  node [
    id 1734
    label "kaszel"
  ]
  node [
    id 1735
    label "przemoc"
  ]
  node [
    id 1736
    label "knock"
  ]
  node [
    id 1737
    label "spasm"
  ]
  node [
    id 1738
    label "gracz"
  ]
  node [
    id 1739
    label "mecz"
  ]
  node [
    id 1740
    label "poparcie"
  ]
  node [
    id 1741
    label "ochrona"
  ]
  node [
    id 1742
    label "guard_duty"
  ]
  node [
    id 1743
    label "auspices"
  ]
  node [
    id 1744
    label "defense"
  ]
  node [
    id 1745
    label "post&#281;powanie"
  ]
  node [
    id 1746
    label "egzamin"
  ]
  node [
    id 1747
    label "gra"
  ]
  node [
    id 1748
    label "sp&#243;r"
  ]
  node [
    id 1749
    label "protection"
  ]
  node [
    id 1750
    label "defensive_structure"
  ]
  node [
    id 1751
    label "cywilizacja"
  ]
  node [
    id 1752
    label "community"
  ]
  node [
    id 1753
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 1754
    label "kastowo&#347;&#263;"
  ]
  node [
    id 1755
    label "pozaklasowy"
  ]
  node [
    id 1756
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 1757
    label "uwarstwienie"
  ]
  node [
    id 1758
    label "ludzie_pracy"
  ]
  node [
    id 1759
    label "aspo&#322;eczny"
  ]
  node [
    id 1760
    label "elita"
  ]
  node [
    id 1761
    label "jednostka_administracyjna"
  ]
  node [
    id 1762
    label "hurma"
  ]
  node [
    id 1763
    label "zoologia"
  ]
  node [
    id 1764
    label "tribe"
  ]
  node [
    id 1765
    label "cug"
  ]
  node [
    id 1766
    label "tender"
  ]
  node [
    id 1767
    label "lokomotywa"
  ]
  node [
    id 1768
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1769
    label "pojazd_kolejowy"
  ]
  node [
    id 1770
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 1771
    label "okr&#281;t"
  ]
  node [
    id 1772
    label "pojazd_trakcyjny"
  ]
  node [
    id 1773
    label "ciuchcia"
  ]
  node [
    id 1774
    label "&#347;l&#261;ski"
  ]
  node [
    id 1775
    label "stan"
  ]
  node [
    id 1776
    label "draft"
  ]
  node [
    id 1777
    label "para"
  ]
  node [
    id 1778
    label "zaprz&#281;g"
  ]
  node [
    id 1779
    label "pr&#261;d"
  ]
  node [
    id 1780
    label "tor"
  ]
  node [
    id 1781
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1782
    label "kolejno&#347;&#263;"
  ]
  node [
    id 1783
    label "blokada"
  ]
  node [
    id 1784
    label "pocz&#261;tek"
  ]
  node [
    id 1785
    label "trakcja"
  ]
  node [
    id 1786
    label "nast&#281;pstwo"
  ]
  node [
    id 1787
    label "run"
  ]
  node [
    id 1788
    label "osobowo"
  ]
  node [
    id 1789
    label "po&#347;piesznie"
  ]
  node [
    id 1790
    label "headlong"
  ]
  node [
    id 1791
    label "hurriedly"
  ]
  node [
    id 1792
    label "po&#347;pieszny"
  ]
  node [
    id 1793
    label "pospieszno"
  ]
  node [
    id 1794
    label "exspresowy"
  ]
  node [
    id 1795
    label "expresowy"
  ]
  node [
    id 1796
    label "specjalny"
  ]
  node [
    id 1797
    label "superszybko"
  ]
  node [
    id 1798
    label "zdynamizowanie"
  ]
  node [
    id 1799
    label "dynamicznie"
  ]
  node [
    id 1800
    label "aktywny"
  ]
  node [
    id 1801
    label "Tuesday"
  ]
  node [
    id 1802
    label "&#380;ywy"
  ]
  node [
    id 1803
    label "zmienny"
  ]
  node [
    id 1804
    label "dynamizowanie"
  ]
  node [
    id 1805
    label "mocny"
  ]
  node [
    id 1806
    label "energicznie"
  ]
  node [
    id 1807
    label "po_prostu"
  ]
  node [
    id 1808
    label "naiwny"
  ]
  node [
    id 1809
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 1810
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 1811
    label "prostowanie"
  ]
  node [
    id 1812
    label "prostoduszny"
  ]
  node [
    id 1813
    label "&#322;atwy"
  ]
  node [
    id 1814
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 1815
    label "prostowanie_si&#281;"
  ]
  node [
    id 1816
    label "rozprostowanie"
  ]
  node [
    id 1817
    label "naturalny"
  ]
  node [
    id 1818
    label "niepozorny"
  ]
  node [
    id 1819
    label "prosto"
  ]
  node [
    id 1820
    label "bezpo&#347;rednio"
  ]
  node [
    id 1821
    label "szczery"
  ]
  node [
    id 1822
    label "sprawnie"
  ]
  node [
    id 1823
    label "umiej&#281;tny"
  ]
  node [
    id 1824
    label "zdrowy"
  ]
  node [
    id 1825
    label "dobry"
  ]
  node [
    id 1826
    label "letki"
  ]
  node [
    id 1827
    label "dzia&#322;alny"
  ]
  node [
    id 1828
    label "specjalnie"
  ]
  node [
    id 1829
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1830
    label "niedorozw&#243;j"
  ]
  node [
    id 1831
    label "nienormalny"
  ]
  node [
    id 1832
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1833
    label "umy&#347;lnie"
  ]
  node [
    id 1834
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1835
    label "odpowiedni"
  ]
  node [
    id 1836
    label "nieetatowy"
  ]
  node [
    id 1837
    label "szczeg&#243;lny"
  ]
  node [
    id 1838
    label "intencjonalny"
  ]
  node [
    id 1839
    label "superszybki"
  ]
  node [
    id 1840
    label "expresowo"
  ]
  node [
    id 1841
    label "instantaneously"
  ]
  node [
    id 1842
    label "zniszczenie"
  ]
  node [
    id 1843
    label "ukszta&#322;towanie"
  ]
  node [
    id 1844
    label "egress"
  ]
  node [
    id 1845
    label "przej&#347;cie"
  ]
  node [
    id 1846
    label "skombinowanie"
  ]
  node [
    id 1847
    label "pozyskiwanie"
  ]
  node [
    id 1848
    label "niszczenie"
  ]
  node [
    id 1849
    label "kszta&#322;towanie"
  ]
  node [
    id 1850
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1851
    label "weryfikator"
  ]
  node [
    id 1852
    label "pracownik"
  ]
  node [
    id 1853
    label "po&#347;rednio"
  ]
  node [
    id 1854
    label "zwykle"
  ]
  node [
    id 1855
    label "zwyczajnie"
  ]
  node [
    id 1856
    label "przeci&#281;tny"
  ]
  node [
    id 1857
    label "cz&#281;sty"
  ]
  node [
    id 1858
    label "poprostu"
  ]
  node [
    id 1859
    label "zwyczajny"
  ]
  node [
    id 1860
    label "cz&#281;sto"
  ]
  node [
    id 1861
    label "taki_sobie"
  ]
  node [
    id 1862
    label "przeci&#281;tnie"
  ]
  node [
    id 1863
    label "orientacyjny"
  ]
  node [
    id 1864
    label "&#347;rednio"
  ]
  node [
    id 1865
    label "wiadomy"
  ]
  node [
    id 1866
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 1867
    label "heat"
  ]
  node [
    id 1868
    label "zmieni&#263;"
  ]
  node [
    id 1869
    label "ascend"
  ]
  node [
    id 1870
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1871
    label "prym"
  ]
  node [
    id 1872
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1873
    label "deepen"
  ]
  node [
    id 1874
    label "translate"
  ]
  node [
    id 1875
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1876
    label "uzna&#263;"
  ]
  node [
    id 1877
    label "transfer"
  ]
  node [
    id 1878
    label "picture"
  ]
  node [
    id 1879
    label "przedstawi&#263;"
  ]
  node [
    id 1880
    label "zbiorczo"
  ]
  node [
    id 1881
    label "&#322;&#261;czny"
  ]
  node [
    id 1882
    label "szeroko"
  ]
  node [
    id 1883
    label "zbiorczy"
  ]
  node [
    id 1884
    label "og&#243;lnie"
  ]
  node [
    id 1885
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1886
    label "strategia"
  ]
  node [
    id 1887
    label "background"
  ]
  node [
    id 1888
    label "punkt_odniesienia"
  ]
  node [
    id 1889
    label "zasadzenie"
  ]
  node [
    id 1890
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1891
    label "&#347;ciana"
  ]
  node [
    id 1892
    label "podstawowy"
  ]
  node [
    id 1893
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1894
    label "d&#243;&#322;"
  ]
  node [
    id 1895
    label "documentation"
  ]
  node [
    id 1896
    label "pomys&#322;"
  ]
  node [
    id 1897
    label "zasadzi&#263;"
  ]
  node [
    id 1898
    label "column"
  ]
  node [
    id 1899
    label "pot&#281;ga"
  ]
  node [
    id 1900
    label "infliction"
  ]
  node [
    id 1901
    label "przygotowanie"
  ]
  node [
    id 1902
    label "pozak&#322;adanie"
  ]
  node [
    id 1903
    label "poubieranie"
  ]
  node [
    id 1904
    label "rozebranie"
  ]
  node [
    id 1905
    label "str&#243;j"
  ]
  node [
    id 1906
    label "przewidzenie"
  ]
  node [
    id 1907
    label "zak&#322;adka"
  ]
  node [
    id 1908
    label "przygotowywanie"
  ]
  node [
    id 1909
    label "podwini&#281;cie"
  ]
  node [
    id 1910
    label "zap&#322;acenie"
  ]
  node [
    id 1911
    label "wyko&#324;czenie"
  ]
  node [
    id 1912
    label "utworzenie"
  ]
  node [
    id 1913
    label "przebranie"
  ]
  node [
    id 1914
    label "obleczenie"
  ]
  node [
    id 1915
    label "przymierzenie"
  ]
  node [
    id 1916
    label "obleczenie_si&#281;"
  ]
  node [
    id 1917
    label "przywdzianie"
  ]
  node [
    id 1918
    label "umieszczenie"
  ]
  node [
    id 1919
    label "przyodzianie"
  ]
  node [
    id 1920
    label "pokrycie"
  ]
  node [
    id 1921
    label "wyrobisko"
  ]
  node [
    id 1922
    label "trudno&#347;&#263;"
  ]
  node [
    id 1923
    label "bariera"
  ]
  node [
    id 1924
    label "przegroda"
  ]
  node [
    id 1925
    label "facebook"
  ]
  node [
    id 1926
    label "zbocze"
  ]
  node [
    id 1927
    label "obstruction"
  ]
  node [
    id 1928
    label "pow&#322;oka"
  ]
  node [
    id 1929
    label "wielo&#347;cian"
  ]
  node [
    id 1930
    label "strzelba"
  ]
  node [
    id 1931
    label "wielok&#261;t"
  ]
  node [
    id 1932
    label "tu&#322;&#243;w"
  ]
  node [
    id 1933
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 1934
    label "lufa"
  ]
  node [
    id 1935
    label "low"
  ]
  node [
    id 1936
    label "wykopanie"
  ]
  node [
    id 1937
    label "hole"
  ]
  node [
    id 1938
    label "wykopywa&#263;"
  ]
  node [
    id 1939
    label "za&#322;amanie"
  ]
  node [
    id 1940
    label "wykopa&#263;"
  ]
  node [
    id 1941
    label "&#347;piew"
  ]
  node [
    id 1942
    label "wykopywanie"
  ]
  node [
    id 1943
    label "depressive_disorder"
  ]
  node [
    id 1944
    label "niski"
  ]
  node [
    id 1945
    label "pocz&#261;tkowy"
  ]
  node [
    id 1946
    label "najwa&#380;niejszy"
  ]
  node [
    id 1947
    label "podstawowo"
  ]
  node [
    id 1948
    label "niezaawansowany"
  ]
  node [
    id 1949
    label "plant"
  ]
  node [
    id 1950
    label "przymocowa&#263;"
  ]
  node [
    id 1951
    label "establish"
  ]
  node [
    id 1952
    label "osnowa&#263;"
  ]
  node [
    id 1953
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1954
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1955
    label "wetkn&#261;&#263;"
  ]
  node [
    id 1956
    label "przetkanie"
  ]
  node [
    id 1957
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 1958
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1959
    label "anchor"
  ]
  node [
    id 1960
    label "wetkni&#281;cie"
  ]
  node [
    id 1961
    label "interposition"
  ]
  node [
    id 1962
    label "przymocowanie"
  ]
  node [
    id 1963
    label "pocz&#261;tki"
  ]
  node [
    id 1964
    label "dzieci&#324;stwo"
  ]
  node [
    id 1965
    label "ukradzenie"
  ]
  node [
    id 1966
    label "idea"
  ]
  node [
    id 1967
    label "ukra&#347;&#263;"
  ]
  node [
    id 1968
    label "doktryna"
  ]
  node [
    id 1969
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1970
    label "dziedzina"
  ]
  node [
    id 1971
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1972
    label "wrinkle"
  ]
  node [
    id 1973
    label "wzorzec_projektowy"
  ]
  node [
    id 1974
    label "iloczyn"
  ]
  node [
    id 1975
    label "violence"
  ]
  node [
    id 1976
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 1977
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 1978
    label "passe-partout"
  ]
  node [
    id 1979
    label "karta_wst&#281;pu"
  ]
  node [
    id 1980
    label "konik"
  ]
  node [
    id 1981
    label "bordiura"
  ]
  node [
    id 1982
    label "cyka&#263;"
  ]
  node [
    id 1983
    label "szara&#324;czak"
  ]
  node [
    id 1984
    label "zabawka"
  ]
  node [
    id 1985
    label "grasshopper"
  ]
  node [
    id 1986
    label "zaj&#281;cie"
  ]
  node [
    id 1987
    label "fondness"
  ]
  node [
    id 1988
    label "za&#322;atwiacz"
  ]
  node [
    id 1989
    label "unikatowy"
  ]
  node [
    id 1990
    label "jednorazowo"
  ]
  node [
    id 1991
    label "jednokrotny"
  ]
  node [
    id 1992
    label "osobny"
  ]
  node [
    id 1993
    label "specyficzny"
  ]
  node [
    id 1994
    label "pojedynczy"
  ]
  node [
    id 1995
    label "unikalnie"
  ]
  node [
    id 1996
    label "unikatowo"
  ]
  node [
    id 1997
    label "wyj&#261;tkowy"
  ]
  node [
    id 1998
    label "przemijaj&#261;co"
  ]
  node [
    id 1999
    label "domek_z_kart"
  ]
  node [
    id 2000
    label "nietrwale"
  ]
  node [
    id 2001
    label "jednoczesny"
  ]
  node [
    id 2002
    label "jednokrotnie"
  ]
  node [
    id 2003
    label "wzi&#281;cie"
  ]
  node [
    id 2004
    label "acknowledgment"
  ]
  node [
    id 2005
    label "wyruchanie"
  ]
  node [
    id 2006
    label "u&#380;ycie"
  ]
  node [
    id 2007
    label "wej&#347;cie"
  ]
  node [
    id 2008
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 2009
    label "nakazanie"
  ]
  node [
    id 2010
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 2011
    label "udanie_si&#281;"
  ]
  node [
    id 2012
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 2013
    label "powodzenie"
  ]
  node [
    id 2014
    label "pokonanie"
  ]
  node [
    id 2015
    label "niesienie"
  ]
  node [
    id 2016
    label "ruszenie"
  ]
  node [
    id 2017
    label "wygranie"
  ]
  node [
    id 2018
    label "bite"
  ]
  node [
    id 2019
    label "poczytanie"
  ]
  node [
    id 2020
    label "zniesienie"
  ]
  node [
    id 2021
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 2022
    label "dmuchni&#281;cie"
  ]
  node [
    id 2023
    label "wywiezienie"
  ]
  node [
    id 2024
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 2025
    label "pickings"
  ]
  node [
    id 2026
    label "odziedziczenie"
  ]
  node [
    id 2027
    label "branie"
  ]
  node [
    id 2028
    label "take"
  ]
  node [
    id 2029
    label "pozabieranie"
  ]
  node [
    id 2030
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 2031
    label "w&#322;o&#380;enie"
  ]
  node [
    id 2032
    label "uciekni&#281;cie"
  ]
  node [
    id 2033
    label "otrzymanie"
  ]
  node [
    id 2034
    label "capture"
  ]
  node [
    id 2035
    label "pobranie"
  ]
  node [
    id 2036
    label "wymienienie_si&#281;"
  ]
  node [
    id 2037
    label "przyj&#281;cie"
  ]
  node [
    id 2038
    label "wzi&#261;&#263;"
  ]
  node [
    id 2039
    label "kupienie"
  ]
  node [
    id 2040
    label "zacz&#281;cie"
  ]
  node [
    id 2041
    label "zawiera&#263;"
  ]
  node [
    id 2042
    label "keep_open"
  ]
  node [
    id 2043
    label "cognizance"
  ]
  node [
    id 2044
    label "zamyka&#263;"
  ]
  node [
    id 2045
    label "ustala&#263;"
  ]
  node [
    id 2046
    label "make"
  ]
  node [
    id 2047
    label "lock"
  ]
  node [
    id 2048
    label "poznawa&#263;"
  ]
  node [
    id 2049
    label "wykonawca"
  ]
  node [
    id 2050
    label "interpretator"
  ]
  node [
    id 2051
    label "title"
  ]
  node [
    id 2052
    label "authorization"
  ]
  node [
    id 2053
    label "campaign"
  ]
  node [
    id 2054
    label "causing"
  ]
  node [
    id 2055
    label "sygnatariusz"
  ]
  node [
    id 2056
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 2057
    label "dokumentacja"
  ]
  node [
    id 2058
    label "writing"
  ]
  node [
    id 2059
    label "zapis"
  ]
  node [
    id 2060
    label "artyku&#322;"
  ]
  node [
    id 2061
    label "utw&#243;r"
  ]
  node [
    id 2062
    label "record"
  ]
  node [
    id 2063
    label "raport&#243;wka"
  ]
  node [
    id 2064
    label "registratura"
  ]
  node [
    id 2065
    label "fascyku&#322;"
  ]
  node [
    id 2066
    label "parafa"
  ]
  node [
    id 2067
    label "plik"
  ]
  node [
    id 2068
    label "ta&#324;szy"
  ]
  node [
    id 2069
    label "ulgowo"
  ]
  node [
    id 2070
    label "tanienie"
  ]
  node [
    id 2071
    label "potanianie"
  ]
  node [
    id 2072
    label "zni&#380;anie"
  ]
  node [
    id 2073
    label "zni&#380;enie"
  ]
  node [
    id 2074
    label "potanienie"
  ]
  node [
    id 2075
    label "stanienie"
  ]
  node [
    id 2076
    label "minister"
  ]
  node [
    id 2077
    label "narodowy"
  ]
  node [
    id 2078
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 2079
    label "rada"
  ]
  node [
    id 2080
    label "zeszyt"
  ]
  node [
    id 2081
    label "dzie&#324;"
  ]
  node [
    id 2082
    label "5"
  ]
  node [
    id 2083
    label "listopad"
  ]
  node [
    id 2084
    label "1992"
  ]
  node [
    id 2085
    label "wyspa"
  ]
  node [
    id 2086
    label "i"
  ]
  node [
    id 2087
    label "spe&#322;nia&#263;"
  ]
  node [
    id 2088
    label "zast&#281;pczo"
  ]
  node [
    id 2089
    label "wojskowy"
  ]
  node [
    id 2090
    label "oraz"
  ]
  node [
    id 2091
    label "cz&#322;onki"
  ]
  node [
    id 2092
    label "on"
  ]
  node [
    id 2093
    label "rodzina"
  ]
  node [
    id 2094
    label "dziennik"
  ]
  node [
    id 2095
    label "u"
  ]
  node [
    id 2096
    label "J"
  ]
  node [
    id 2097
    label "Kaczy&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 2078
  ]
  edge [
    source 8
    target 2079
  ]
  edge [
    source 8
    target 2076
  ]
  edge [
    source 8
    target 2080
  ]
  edge [
    source 8
    target 2081
  ]
  edge [
    source 8
    target 2082
  ]
  edge [
    source 8
    target 2083
  ]
  edge [
    source 8
    target 2084
  ]
  edge [
    source 8
    target 984
  ]
  edge [
    source 8
    target 2085
  ]
  edge [
    source 8
    target 1355
  ]
  edge [
    source 8
    target 1837
  ]
  edge [
    source 8
    target 2086
  ]
  edge [
    source 8
    target 2087
  ]
  edge [
    source 8
    target 2088
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 2089
  ]
  edge [
    source 8
    target 2090
  ]
  edge [
    source 8
    target 2091
  ]
  edge [
    source 8
    target 2092
  ]
  edge [
    source 8
    target 2093
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 10
    target 874
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 876
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 878
  ]
  edge [
    source 10
    target 879
  ]
  edge [
    source 10
    target 880
  ]
  edge [
    source 10
    target 881
  ]
  edge [
    source 10
    target 882
  ]
  edge [
    source 10
    target 883
  ]
  edge [
    source 10
    target 884
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 885
  ]
  edge [
    source 10
    target 886
  ]
  edge [
    source 10
    target 887
  ]
  edge [
    source 10
    target 888
  ]
  edge [
    source 10
    target 889
  ]
  edge [
    source 10
    target 890
  ]
  edge [
    source 10
    target 891
  ]
  edge [
    source 10
    target 892
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 893
  ]
  edge [
    source 10
    target 894
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 11
    target 927
  ]
  edge [
    source 11
    target 928
  ]
  edge [
    source 11
    target 929
  ]
  edge [
    source 11
    target 930
  ]
  edge [
    source 11
    target 931
  ]
  edge [
    source 11
    target 932
  ]
  edge [
    source 11
    target 933
  ]
  edge [
    source 11
    target 934
  ]
  edge [
    source 11
    target 935
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 13
    target 982
  ]
  edge [
    source 13
    target 983
  ]
  edge [
    source 13
    target 984
  ]
  edge [
    source 13
    target 985
  ]
  edge [
    source 13
    target 986
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 987
  ]
  edge [
    source 13
    target 988
  ]
  edge [
    source 13
    target 989
  ]
  edge [
    source 13
    target 990
  ]
  edge [
    source 13
    target 991
  ]
  edge [
    source 13
    target 992
  ]
  edge [
    source 13
    target 993
  ]
  edge [
    source 13
    target 994
  ]
  edge [
    source 13
    target 995
  ]
  edge [
    source 13
    target 996
  ]
  edge [
    source 13
    target 997
  ]
  edge [
    source 13
    target 998
  ]
  edge [
    source 13
    target 999
  ]
  edge [
    source 13
    target 1000
  ]
  edge [
    source 13
    target 1001
  ]
  edge [
    source 13
    target 1002
  ]
  edge [
    source 13
    target 1003
  ]
  edge [
    source 13
    target 1004
  ]
  edge [
    source 13
    target 1005
  ]
  edge [
    source 13
    target 1006
  ]
  edge [
    source 13
    target 1007
  ]
  edge [
    source 13
    target 1008
  ]
  edge [
    source 13
    target 1009
  ]
  edge [
    source 13
    target 1010
  ]
  edge [
    source 13
    target 1011
  ]
  edge [
    source 13
    target 1012
  ]
  edge [
    source 13
    target 1013
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 1014
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 1015
  ]
  edge [
    source 13
    target 1016
  ]
  edge [
    source 13
    target 1017
  ]
  edge [
    source 13
    target 1018
  ]
  edge [
    source 13
    target 1019
  ]
  edge [
    source 13
    target 1020
  ]
  edge [
    source 13
    target 1021
  ]
  edge [
    source 13
    target 1022
  ]
  edge [
    source 13
    target 1023
  ]
  edge [
    source 13
    target 1024
  ]
  edge [
    source 13
    target 1025
  ]
  edge [
    source 13
    target 1026
  ]
  edge [
    source 13
    target 1027
  ]
  edge [
    source 13
    target 56
  ]
  edge [
    source 13
    target 1028
  ]
  edge [
    source 13
    target 1029
  ]
  edge [
    source 13
    target 1030
  ]
  edge [
    source 13
    target 1031
  ]
  edge [
    source 13
    target 1032
  ]
  edge [
    source 13
    target 1033
  ]
  edge [
    source 13
    target 1034
  ]
  edge [
    source 13
    target 1035
  ]
  edge [
    source 13
    target 1036
  ]
  edge [
    source 13
    target 1037
  ]
  edge [
    source 13
    target 1038
  ]
  edge [
    source 13
    target 1039
  ]
  edge [
    source 13
    target 1040
  ]
  edge [
    source 13
    target 1041
  ]
  edge [
    source 13
    target 1042
  ]
  edge [
    source 13
    target 1043
  ]
  edge [
    source 13
    target 1044
  ]
  edge [
    source 13
    target 1045
  ]
  edge [
    source 13
    target 1046
  ]
  edge [
    source 13
    target 1047
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 1048
  ]
  edge [
    source 13
    target 1049
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 299
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 328
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 325
  ]
  edge [
    source 14
    target 319
  ]
  edge [
    source 14
    target 320
  ]
  edge [
    source 14
    target 321
  ]
  edge [
    source 14
    target 322
  ]
  edge [
    source 14
    target 306
  ]
  edge [
    source 14
    target 323
  ]
  edge [
    source 14
    target 324
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 326
  ]
  edge [
    source 14
    target 327
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 350
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 37
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1139
  ]
  edge [
    source 15
    target 1140
  ]
  edge [
    source 15
    target 1141
  ]
  edge [
    source 15
    target 1142
  ]
  edge [
    source 15
    target 1143
  ]
  edge [
    source 15
    target 65
  ]
  edge [
    source 15
    target 1144
  ]
  edge [
    source 15
    target 1145
  ]
  edge [
    source 15
    target 1146
  ]
  edge [
    source 15
    target 1147
  ]
  edge [
    source 15
    target 1148
  ]
  edge [
    source 15
    target 1149
  ]
  edge [
    source 15
    target 1150
  ]
  edge [
    source 15
    target 1151
  ]
  edge [
    source 15
    target 1152
  ]
  edge [
    source 15
    target 1153
  ]
  edge [
    source 15
    target 1154
  ]
  edge [
    source 15
    target 1155
  ]
  edge [
    source 15
    target 1156
  ]
  edge [
    source 15
    target 1157
  ]
  edge [
    source 15
    target 1158
  ]
  edge [
    source 15
    target 1159
  ]
  edge [
    source 15
    target 1160
  ]
  edge [
    source 15
    target 1161
  ]
  edge [
    source 15
    target 1162
  ]
  edge [
    source 15
    target 1163
  ]
  edge [
    source 15
    target 1164
  ]
  edge [
    source 15
    target 1165
  ]
  edge [
    source 15
    target 1166
  ]
  edge [
    source 15
    target 1167
  ]
  edge [
    source 15
    target 1168
  ]
  edge [
    source 15
    target 1169
  ]
  edge [
    source 15
    target 1170
  ]
  edge [
    source 15
    target 1171
  ]
  edge [
    source 15
    target 1172
  ]
  edge [
    source 15
    target 1173
  ]
  edge [
    source 15
    target 1174
  ]
  edge [
    source 15
    target 1175
  ]
  edge [
    source 15
    target 1176
  ]
  edge [
    source 15
    target 1177
  ]
  edge [
    source 15
    target 1178
  ]
  edge [
    source 15
    target 1179
  ]
  edge [
    source 15
    target 1180
  ]
  edge [
    source 15
    target 1181
  ]
  edge [
    source 15
    target 1182
  ]
  edge [
    source 15
    target 1183
  ]
  edge [
    source 15
    target 1184
  ]
  edge [
    source 15
    target 1185
  ]
  edge [
    source 15
    target 1186
  ]
  edge [
    source 15
    target 1187
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 1188
  ]
  edge [
    source 15
    target 1189
  ]
  edge [
    source 15
    target 1190
  ]
  edge [
    source 15
    target 1191
  ]
  edge [
    source 15
    target 1192
  ]
  edge [
    source 15
    target 1193
  ]
  edge [
    source 15
    target 1194
  ]
  edge [
    source 15
    target 1195
  ]
  edge [
    source 15
    target 1196
  ]
  edge [
    source 15
    target 1197
  ]
  edge [
    source 15
    target 1198
  ]
  edge [
    source 15
    target 1199
  ]
  edge [
    source 15
    target 1200
  ]
  edge [
    source 15
    target 1201
  ]
  edge [
    source 15
    target 1202
  ]
  edge [
    source 15
    target 1203
  ]
  edge [
    source 15
    target 1204
  ]
  edge [
    source 15
    target 1205
  ]
  edge [
    source 15
    target 1206
  ]
  edge [
    source 15
    target 1207
  ]
  edge [
    source 15
    target 1208
  ]
  edge [
    source 15
    target 1209
  ]
  edge [
    source 15
    target 1210
  ]
  edge [
    source 15
    target 1211
  ]
  edge [
    source 15
    target 1212
  ]
  edge [
    source 15
    target 1213
  ]
  edge [
    source 15
    target 1214
  ]
  edge [
    source 15
    target 1215
  ]
  edge [
    source 15
    target 1216
  ]
  edge [
    source 15
    target 1217
  ]
  edge [
    source 15
    target 1218
  ]
  edge [
    source 15
    target 1219
  ]
  edge [
    source 15
    target 1220
  ]
  edge [
    source 15
    target 1221
  ]
  edge [
    source 15
    target 1222
  ]
  edge [
    source 15
    target 1223
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 15
    target 1225
  ]
  edge [
    source 15
    target 1226
  ]
  edge [
    source 15
    target 1227
  ]
  edge [
    source 15
    target 854
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 1228
  ]
  edge [
    source 15
    target 1229
  ]
  edge [
    source 15
    target 1230
  ]
  edge [
    source 15
    target 1231
  ]
  edge [
    source 15
    target 1232
  ]
  edge [
    source 15
    target 1233
  ]
  edge [
    source 15
    target 1234
  ]
  edge [
    source 15
    target 1235
  ]
  edge [
    source 15
    target 1236
  ]
  edge [
    source 15
    target 1237
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 1238
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 1239
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 1240
  ]
  edge [
    source 15
    target 1241
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 1242
  ]
  edge [
    source 15
    target 1243
  ]
  edge [
    source 15
    target 1244
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 1245
  ]
  edge [
    source 15
    target 855
  ]
  edge [
    source 15
    target 1246
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 1247
  ]
  edge [
    source 15
    target 1248
  ]
  edge [
    source 15
    target 1249
  ]
  edge [
    source 15
    target 1250
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 26
  ]
  edge [
    source 15
    target 35
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1251
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 1252
  ]
  edge [
    source 16
    target 1253
  ]
  edge [
    source 16
    target 1254
  ]
  edge [
    source 16
    target 1255
  ]
  edge [
    source 16
    target 1256
  ]
  edge [
    source 16
    target 1257
  ]
  edge [
    source 16
    target 1258
  ]
  edge [
    source 16
    target 1259
  ]
  edge [
    source 16
    target 1260
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 48
  ]
  edge [
    source 16
    target 72
  ]
  edge [
    source 16
    target 1261
  ]
  edge [
    source 16
    target 1262
  ]
  edge [
    source 16
    target 1263
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 1264
  ]
  edge [
    source 16
    target 1265
  ]
  edge [
    source 16
    target 1266
  ]
  edge [
    source 16
    target 1267
  ]
  edge [
    source 16
    target 1268
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 1269
  ]
  edge [
    source 16
    target 1270
  ]
  edge [
    source 16
    target 1271
  ]
  edge [
    source 16
    target 61
  ]
  edge [
    source 16
    target 1272
  ]
  edge [
    source 16
    target 1273
  ]
  edge [
    source 16
    target 1274
  ]
  edge [
    source 16
    target 1275
  ]
  edge [
    source 16
    target 1276
  ]
  edge [
    source 16
    target 1277
  ]
  edge [
    source 16
    target 1278
  ]
  edge [
    source 16
    target 1279
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 1280
  ]
  edge [
    source 16
    target 1281
  ]
  edge [
    source 16
    target 65
  ]
  edge [
    source 16
    target 1282
  ]
  edge [
    source 16
    target 1283
  ]
  edge [
    source 16
    target 1284
  ]
  edge [
    source 16
    target 1285
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 1286
  ]
  edge [
    source 16
    target 1287
  ]
  edge [
    source 16
    target 1288
  ]
  edge [
    source 16
    target 1289
  ]
  edge [
    source 16
    target 1290
  ]
  edge [
    source 16
    target 1291
  ]
  edge [
    source 16
    target 1292
  ]
  edge [
    source 16
    target 1293
  ]
  edge [
    source 16
    target 1294
  ]
  edge [
    source 16
    target 1295
  ]
  edge [
    source 16
    target 1296
  ]
  edge [
    source 16
    target 1297
  ]
  edge [
    source 16
    target 1298
  ]
  edge [
    source 16
    target 1299
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 1300
  ]
  edge [
    source 16
    target 1301
  ]
  edge [
    source 16
    target 1302
  ]
  edge [
    source 16
    target 1303
  ]
  edge [
    source 16
    target 1304
  ]
  edge [
    source 16
    target 1305
  ]
  edge [
    source 16
    target 1306
  ]
  edge [
    source 16
    target 1307
  ]
  edge [
    source 16
    target 1308
  ]
  edge [
    source 16
    target 1309
  ]
  edge [
    source 16
    target 1310
  ]
  edge [
    source 16
    target 1311
  ]
  edge [
    source 16
    target 1312
  ]
  edge [
    source 16
    target 1313
  ]
  edge [
    source 16
    target 1314
  ]
  edge [
    source 16
    target 1315
  ]
  edge [
    source 16
    target 1316
  ]
  edge [
    source 16
    target 1317
  ]
  edge [
    source 16
    target 1318
  ]
  edge [
    source 16
    target 1319
  ]
  edge [
    source 16
    target 1320
  ]
  edge [
    source 16
    target 1321
  ]
  edge [
    source 16
    target 1322
  ]
  edge [
    source 16
    target 1323
  ]
  edge [
    source 16
    target 1324
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 16
    target 1325
  ]
  edge [
    source 16
    target 1326
  ]
  edge [
    source 16
    target 1327
  ]
  edge [
    source 16
    target 1328
  ]
  edge [
    source 16
    target 1329
  ]
  edge [
    source 16
    target 1330
  ]
  edge [
    source 16
    target 53
  ]
  edge [
    source 16
    target 1331
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 1332
  ]
  edge [
    source 16
    target 1333
  ]
  edge [
    source 16
    target 1334
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1311
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 1335
  ]
  edge [
    source 17
    target 1336
  ]
  edge [
    source 17
    target 42
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 56
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1260
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 1337
  ]
  edge [
    source 17
    target 1338
  ]
  edge [
    source 17
    target 1339
  ]
  edge [
    source 17
    target 1340
  ]
  edge [
    source 17
    target 1341
  ]
  edge [
    source 17
    target 545
  ]
  edge [
    source 17
    target 1342
  ]
  edge [
    source 17
    target 1343
  ]
  edge [
    source 17
    target 1344
  ]
  edge [
    source 17
    target 1345
  ]
  edge [
    source 17
    target 1346
  ]
  edge [
    source 17
    target 44
  ]
  edge [
    source 17
    target 1347
  ]
  edge [
    source 17
    target 1348
  ]
  edge [
    source 17
    target 1349
  ]
  edge [
    source 17
    target 1350
  ]
  edge [
    source 17
    target 1351
  ]
  edge [
    source 17
    target 1352
  ]
  edge [
    source 17
    target 1318
  ]
  edge [
    source 17
    target 1353
  ]
  edge [
    source 17
    target 1354
  ]
  edge [
    source 17
    target 1355
  ]
  edge [
    source 17
    target 1356
  ]
  edge [
    source 17
    target 1357
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 1358
  ]
  edge [
    source 17
    target 45
  ]
  edge [
    source 17
    target 46
  ]
  edge [
    source 17
    target 47
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 17
    target 51
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 17
    target 53
  ]
  edge [
    source 17
    target 54
  ]
  edge [
    source 17
    target 55
  ]
  edge [
    source 17
    target 1359
  ]
  edge [
    source 17
    target 1360
  ]
  edge [
    source 17
    target 1361
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 1362
  ]
  edge [
    source 17
    target 1363
  ]
  edge [
    source 17
    target 1364
  ]
  edge [
    source 17
    target 1297
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 1365
  ]
  edge [
    source 17
    target 1366
  ]
  edge [
    source 17
    target 1367
  ]
  edge [
    source 17
    target 1368
  ]
  edge [
    source 17
    target 1369
  ]
  edge [
    source 17
    target 1370
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1371
  ]
  edge [
    source 18
    target 1372
  ]
  edge [
    source 18
    target 1373
  ]
  edge [
    source 18
    target 1374
  ]
  edge [
    source 18
    target 1375
  ]
  edge [
    source 18
    target 1376
  ]
  edge [
    source 18
    target 1377
  ]
  edge [
    source 18
    target 1378
  ]
  edge [
    source 18
    target 1379
  ]
  edge [
    source 18
    target 1380
  ]
  edge [
    source 18
    target 1381
  ]
  edge [
    source 18
    target 1382
  ]
  edge [
    source 18
    target 1383
  ]
  edge [
    source 18
    target 1384
  ]
  edge [
    source 18
    target 1196
  ]
  edge [
    source 18
    target 1385
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 1386
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 1387
  ]
  edge [
    source 19
    target 1388
  ]
  edge [
    source 19
    target 1389
  ]
  edge [
    source 19
    target 1390
  ]
  edge [
    source 19
    target 1391
  ]
  edge [
    source 19
    target 1392
  ]
  edge [
    source 19
    target 1393
  ]
  edge [
    source 19
    target 1394
  ]
  edge [
    source 19
    target 1395
  ]
  edge [
    source 19
    target 1396
  ]
  edge [
    source 19
    target 1397
  ]
  edge [
    source 19
    target 603
  ]
  edge [
    source 19
    target 1270
  ]
  edge [
    source 19
    target 1398
  ]
  edge [
    source 19
    target 1399
  ]
  edge [
    source 19
    target 1400
  ]
  edge [
    source 19
    target 1401
  ]
  edge [
    source 19
    target 1402
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 42
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 247
  ]
  edge [
    source 19
    target 1403
  ]
  edge [
    source 19
    target 1404
  ]
  edge [
    source 19
    target 1405
  ]
  edge [
    source 19
    target 1406
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 1407
  ]
  edge [
    source 19
    target 1408
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 1409
  ]
  edge [
    source 19
    target 250
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1410
  ]
  edge [
    source 19
    target 1411
  ]
  edge [
    source 19
    target 1412
  ]
  edge [
    source 19
    target 1413
  ]
  edge [
    source 19
    target 568
  ]
  edge [
    source 19
    target 1414
  ]
  edge [
    source 19
    target 1415
  ]
  edge [
    source 19
    target 1416
  ]
  edge [
    source 19
    target 741
  ]
  edge [
    source 19
    target 1417
  ]
  edge [
    source 19
    target 1418
  ]
  edge [
    source 19
    target 1419
  ]
  edge [
    source 19
    target 1420
  ]
  edge [
    source 19
    target 1421
  ]
  edge [
    source 19
    target 1422
  ]
  edge [
    source 19
    target 1423
  ]
  edge [
    source 19
    target 1424
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 1425
  ]
  edge [
    source 19
    target 1426
  ]
  edge [
    source 19
    target 1427
  ]
  edge [
    source 19
    target 1428
  ]
  edge [
    source 19
    target 1429
  ]
  edge [
    source 19
    target 85
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1430
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 1431
  ]
  edge [
    source 19
    target 1432
  ]
  edge [
    source 19
    target 1433
  ]
  edge [
    source 19
    target 1434
  ]
  edge [
    source 19
    target 1435
  ]
  edge [
    source 19
    target 1436
  ]
  edge [
    source 19
    target 1437
  ]
  edge [
    source 19
    target 1438
  ]
  edge [
    source 19
    target 1439
  ]
  edge [
    source 19
    target 72
  ]
  edge [
    source 19
    target 1440
  ]
  edge [
    source 19
    target 1441
  ]
  edge [
    source 19
    target 52
  ]
  edge [
    source 19
    target 556
  ]
  edge [
    source 19
    target 1442
  ]
  edge [
    source 19
    target 1443
  ]
  edge [
    source 19
    target 1444
  ]
  edge [
    source 19
    target 1445
  ]
  edge [
    source 19
    target 1446
  ]
  edge [
    source 19
    target 1447
  ]
  edge [
    source 19
    target 1448
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 1449
  ]
  edge [
    source 19
    target 1450
  ]
  edge [
    source 19
    target 1451
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1452
  ]
  edge [
    source 19
    target 1453
  ]
  edge [
    source 19
    target 1454
  ]
  edge [
    source 19
    target 1455
  ]
  edge [
    source 19
    target 1456
  ]
  edge [
    source 19
    target 1457
  ]
  edge [
    source 19
    target 1458
  ]
  edge [
    source 19
    target 1459
  ]
  edge [
    source 19
    target 1460
  ]
  edge [
    source 19
    target 1461
  ]
  edge [
    source 19
    target 1462
  ]
  edge [
    source 19
    target 1463
  ]
  edge [
    source 19
    target 1464
  ]
  edge [
    source 19
    target 1465
  ]
  edge [
    source 19
    target 1466
  ]
  edge [
    source 19
    target 1467
  ]
  edge [
    source 19
    target 1468
  ]
  edge [
    source 19
    target 1469
  ]
  edge [
    source 19
    target 1470
  ]
  edge [
    source 19
    target 1471
  ]
  edge [
    source 19
    target 1472
  ]
  edge [
    source 19
    target 1473
  ]
  edge [
    source 19
    target 1474
  ]
  edge [
    source 19
    target 1475
  ]
  edge [
    source 19
    target 1476
  ]
  edge [
    source 19
    target 1477
  ]
  edge [
    source 19
    target 1478
  ]
  edge [
    source 19
    target 1479
  ]
  edge [
    source 19
    target 1480
  ]
  edge [
    source 19
    target 1481
  ]
  edge [
    source 19
    target 1482
  ]
  edge [
    source 19
    target 1483
  ]
  edge [
    source 19
    target 1484
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 1485
  ]
  edge [
    source 19
    target 1486
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 1487
  ]
  edge [
    source 19
    target 1488
  ]
  edge [
    source 19
    target 1489
  ]
  edge [
    source 19
    target 1490
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 19
    target 38
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1491
  ]
  edge [
    source 20
    target 1492
  ]
  edge [
    source 20
    target 1493
  ]
  edge [
    source 20
    target 1494
  ]
  edge [
    source 20
    target 1495
  ]
  edge [
    source 20
    target 1496
  ]
  edge [
    source 20
    target 1497
  ]
  edge [
    source 20
    target 1498
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 1499
  ]
  edge [
    source 21
    target 1500
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1501
  ]
  edge [
    source 22
    target 546
  ]
  edge [
    source 22
    target 1502
  ]
  edge [
    source 22
    target 1503
  ]
  edge [
    source 22
    target 1406
  ]
  edge [
    source 22
    target 1504
  ]
  edge [
    source 22
    target 1344
  ]
  edge [
    source 22
    target 1407
  ]
  edge [
    source 22
    target 1505
  ]
  edge [
    source 22
    target 1506
  ]
  edge [
    source 22
    target 1507
  ]
  edge [
    source 22
    target 1508
  ]
  edge [
    source 22
    target 556
  ]
  edge [
    source 22
    target 1260
  ]
  edge [
    source 22
    target 1509
  ]
  edge [
    source 22
    target 1510
  ]
  edge [
    source 22
    target 1413
  ]
  edge [
    source 22
    target 1511
  ]
  edge [
    source 22
    target 788
  ]
  edge [
    source 22
    target 1512
  ]
  edge [
    source 22
    target 1513
  ]
  edge [
    source 22
    target 1514
  ]
  edge [
    source 22
    target 567
  ]
  edge [
    source 22
    target 1515
  ]
  edge [
    source 22
    target 1516
  ]
  edge [
    source 22
    target 58
  ]
  edge [
    source 22
    target 573
  ]
  edge [
    source 22
    target 1517
  ]
  edge [
    source 22
    target 1518
  ]
  edge [
    source 22
    target 1519
  ]
  edge [
    source 22
    target 1520
  ]
  edge [
    source 22
    target 865
  ]
  edge [
    source 22
    target 1521
  ]
  edge [
    source 22
    target 603
  ]
  edge [
    source 22
    target 798
  ]
  edge [
    source 22
    target 799
  ]
  edge [
    source 22
    target 1522
  ]
  edge [
    source 22
    target 1523
  ]
  edge [
    source 22
    target 802
  ]
  edge [
    source 22
    target 1044
  ]
  edge [
    source 22
    target 1524
  ]
  edge [
    source 22
    target 1525
  ]
  edge [
    source 22
    target 1526
  ]
  edge [
    source 22
    target 1527
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 1528
  ]
  edge [
    source 22
    target 1529
  ]
  edge [
    source 22
    target 1530
  ]
  edge [
    source 22
    target 1531
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1532
  ]
  edge [
    source 22
    target 1533
  ]
  edge [
    source 22
    target 1534
  ]
  edge [
    source 22
    target 1535
  ]
  edge [
    source 22
    target 1536
  ]
  edge [
    source 22
    target 1537
  ]
  edge [
    source 22
    target 1538
  ]
  edge [
    source 22
    target 1539
  ]
  edge [
    source 22
    target 1540
  ]
  edge [
    source 22
    target 1541
  ]
  edge [
    source 22
    target 1542
  ]
  edge [
    source 22
    target 1543
  ]
  edge [
    source 22
    target 1544
  ]
  edge [
    source 22
    target 1545
  ]
  edge [
    source 22
    target 1546
  ]
  edge [
    source 22
    target 1547
  ]
  edge [
    source 22
    target 1548
  ]
  edge [
    source 22
    target 1549
  ]
  edge [
    source 22
    target 624
  ]
  edge [
    source 22
    target 1550
  ]
  edge [
    source 22
    target 1551
  ]
  edge [
    source 22
    target 1552
  ]
  edge [
    source 22
    target 1553
  ]
  edge [
    source 22
    target 394
  ]
  edge [
    source 22
    target 1554
  ]
  edge [
    source 22
    target 1555
  ]
  edge [
    source 22
    target 1556
  ]
  edge [
    source 22
    target 1557
  ]
  edge [
    source 22
    target 1558
  ]
  edge [
    source 22
    target 1559
  ]
  edge [
    source 22
    target 1560
  ]
  edge [
    source 22
    target 1561
  ]
  edge [
    source 22
    target 89
  ]
  edge [
    source 22
    target 1562
  ]
  edge [
    source 22
    target 1563
  ]
  edge [
    source 22
    target 1564
  ]
  edge [
    source 22
    target 1565
  ]
  edge [
    source 22
    target 1566
  ]
  edge [
    source 22
    target 1567
  ]
  edge [
    source 22
    target 1568
  ]
  edge [
    source 22
    target 1569
  ]
  edge [
    source 22
    target 1570
  ]
  edge [
    source 22
    target 1571
  ]
  edge [
    source 22
    target 1572
  ]
  edge [
    source 22
    target 1573
  ]
  edge [
    source 22
    target 294
  ]
  edge [
    source 22
    target 1574
  ]
  edge [
    source 22
    target 1575
  ]
  edge [
    source 22
    target 1576
  ]
  edge [
    source 22
    target 1577
  ]
  edge [
    source 22
    target 1578
  ]
  edge [
    source 22
    target 1579
  ]
  edge [
    source 22
    target 1580
  ]
  edge [
    source 22
    target 1581
  ]
  edge [
    source 22
    target 1582
  ]
  edge [
    source 22
    target 53
  ]
  edge [
    source 22
    target 1583
  ]
  edge [
    source 22
    target 1584
  ]
  edge [
    source 22
    target 1585
  ]
  edge [
    source 22
    target 1349
  ]
  edge [
    source 22
    target 1586
  ]
  edge [
    source 22
    target 1587
  ]
  edge [
    source 22
    target 719
  ]
  edge [
    source 22
    target 1588
  ]
  edge [
    source 22
    target 1589
  ]
  edge [
    source 22
    target 1590
  ]
  edge [
    source 22
    target 85
  ]
  edge [
    source 22
    target 1591
  ]
  edge [
    source 22
    target 1592
  ]
  edge [
    source 22
    target 1593
  ]
  edge [
    source 22
    target 1594
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 1595
  ]
  edge [
    source 22
    target 1596
  ]
  edge [
    source 22
    target 1597
  ]
  edge [
    source 22
    target 1598
  ]
  edge [
    source 22
    target 1599
  ]
  edge [
    source 22
    target 1297
  ]
  edge [
    source 22
    target 1404
  ]
  edge [
    source 22
    target 1600
  ]
  edge [
    source 22
    target 1601
  ]
  edge [
    source 22
    target 1602
  ]
  edge [
    source 22
    target 1603
  ]
  edge [
    source 22
    target 1604
  ]
  edge [
    source 22
    target 741
  ]
  edge [
    source 22
    target 1605
  ]
  edge [
    source 22
    target 1606
  ]
  edge [
    source 22
    target 1607
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 1608
  ]
  edge [
    source 22
    target 1609
  ]
  edge [
    source 22
    target 1610
  ]
  edge [
    source 22
    target 247
  ]
  edge [
    source 22
    target 1403
  ]
  edge [
    source 22
    target 1405
  ]
  edge [
    source 22
    target 1363
  ]
  edge [
    source 22
    target 1408
  ]
  edge [
    source 22
    target 1409
  ]
  edge [
    source 22
    target 250
  ]
  edge [
    source 22
    target 1410
  ]
  edge [
    source 22
    target 1411
  ]
  edge [
    source 22
    target 1412
  ]
  edge [
    source 22
    target 568
  ]
  edge [
    source 22
    target 1414
  ]
  edge [
    source 22
    target 1415
  ]
  edge [
    source 22
    target 1416
  ]
  edge [
    source 22
    target 1417
  ]
  edge [
    source 22
    target 1418
  ]
  edge [
    source 22
    target 1419
  ]
  edge [
    source 22
    target 1420
  ]
  edge [
    source 22
    target 1421
  ]
  edge [
    source 22
    target 1422
  ]
  edge [
    source 22
    target 1423
  ]
  edge [
    source 22
    target 1611
  ]
  edge [
    source 22
    target 1612
  ]
  edge [
    source 22
    target 1613
  ]
  edge [
    source 22
    target 1614
  ]
  edge [
    source 22
    target 1615
  ]
  edge [
    source 22
    target 1616
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 1617
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 1618
  ]
  edge [
    source 22
    target 1619
  ]
  edge [
    source 22
    target 1620
  ]
  edge [
    source 22
    target 1621
  ]
  edge [
    source 22
    target 1622
  ]
  edge [
    source 22
    target 1623
  ]
  edge [
    source 22
    target 1624
  ]
  edge [
    source 22
    target 1625
  ]
  edge [
    source 22
    target 1626
  ]
  edge [
    source 22
    target 1627
  ]
  edge [
    source 22
    target 682
  ]
  edge [
    source 22
    target 1628
  ]
  edge [
    source 22
    target 1629
  ]
  edge [
    source 22
    target 395
  ]
  edge [
    source 22
    target 1630
  ]
  edge [
    source 22
    target 1631
  ]
  edge [
    source 22
    target 1632
  ]
  edge [
    source 22
    target 1633
  ]
  edge [
    source 22
    target 1634
  ]
  edge [
    source 22
    target 1635
  ]
  edge [
    source 22
    target 1636
  ]
  edge [
    source 22
    target 1637
  ]
  edge [
    source 22
    target 1638
  ]
  edge [
    source 22
    target 1639
  ]
  edge [
    source 22
    target 1640
  ]
  edge [
    source 22
    target 1641
  ]
  edge [
    source 22
    target 1642
  ]
  edge [
    source 22
    target 1643
  ]
  edge [
    source 22
    target 1644
  ]
  edge [
    source 22
    target 1645
  ]
  edge [
    source 22
    target 1646
  ]
  edge [
    source 22
    target 1647
  ]
  edge [
    source 22
    target 1648
  ]
  edge [
    source 22
    target 1649
  ]
  edge [
    source 22
    target 1650
  ]
  edge [
    source 22
    target 1651
  ]
  edge [
    source 22
    target 1652
  ]
  edge [
    source 22
    target 1653
  ]
  edge [
    source 22
    target 1654
  ]
  edge [
    source 22
    target 1655
  ]
  edge [
    source 22
    target 1656
  ]
  edge [
    source 22
    target 1657
  ]
  edge [
    source 22
    target 1658
  ]
  edge [
    source 22
    target 1659
  ]
  edge [
    source 22
    target 1660
  ]
  edge [
    source 22
    target 1661
  ]
  edge [
    source 22
    target 1662
  ]
  edge [
    source 22
    target 1663
  ]
  edge [
    source 22
    target 1664
  ]
  edge [
    source 22
    target 1665
  ]
  edge [
    source 22
    target 1666
  ]
  edge [
    source 22
    target 1320
  ]
  edge [
    source 22
    target 1439
  ]
  edge [
    source 22
    target 1667
  ]
  edge [
    source 22
    target 1668
  ]
  edge [
    source 22
    target 1669
  ]
  edge [
    source 22
    target 1670
  ]
  edge [
    source 22
    target 1671
  ]
  edge [
    source 22
    target 1672
  ]
  edge [
    source 22
    target 1673
  ]
  edge [
    source 22
    target 1674
  ]
  edge [
    source 22
    target 1675
  ]
  edge [
    source 22
    target 1676
  ]
  edge [
    source 22
    target 1677
  ]
  edge [
    source 22
    target 1678
  ]
  edge [
    source 22
    target 1679
  ]
  edge [
    source 22
    target 954
  ]
  edge [
    source 22
    target 1680
  ]
  edge [
    source 22
    target 1681
  ]
  edge [
    source 22
    target 1391
  ]
  edge [
    source 22
    target 1682
  ]
  edge [
    source 22
    target 1683
  ]
  edge [
    source 22
    target 753
  ]
  edge [
    source 22
    target 1684
  ]
  edge [
    source 22
    target 1685
  ]
  edge [
    source 22
    target 1686
  ]
  edge [
    source 22
    target 758
  ]
  edge [
    source 22
    target 759
  ]
  edge [
    source 22
    target 1070
  ]
  edge [
    source 22
    target 1687
  ]
  edge [
    source 22
    target 1688
  ]
  edge [
    source 22
    target 760
  ]
  edge [
    source 22
    target 1689
  ]
  edge [
    source 22
    target 789
  ]
  edge [
    source 22
    target 1690
  ]
  edge [
    source 22
    target 1691
  ]
  edge [
    source 22
    target 1692
  ]
  edge [
    source 22
    target 1278
  ]
  edge [
    source 22
    target 767
  ]
  edge [
    source 22
    target 768
  ]
  edge [
    source 22
    target 769
  ]
  edge [
    source 22
    target 770
  ]
  edge [
    source 22
    target 771
  ]
  edge [
    source 22
    target 984
  ]
  edge [
    source 22
    target 41
  ]
  edge [
    source 22
    target 1693
  ]
  edge [
    source 22
    target 773
  ]
  edge [
    source 22
    target 1694
  ]
  edge [
    source 22
    target 1695
  ]
  edge [
    source 22
    target 1696
  ]
  edge [
    source 22
    target 1697
  ]
  edge [
    source 22
    target 1698
  ]
  edge [
    source 22
    target 1699
  ]
  edge [
    source 22
    target 1700
  ]
  edge [
    source 22
    target 1701
  ]
  edge [
    source 22
    target 1702
  ]
  edge [
    source 22
    target 1703
  ]
  edge [
    source 22
    target 1704
  ]
  edge [
    source 22
    target 1705
  ]
  edge [
    source 22
    target 1706
  ]
  edge [
    source 22
    target 1707
  ]
  edge [
    source 22
    target 1708
  ]
  edge [
    source 22
    target 378
  ]
  edge [
    source 22
    target 1450
  ]
  edge [
    source 22
    target 1709
  ]
  edge [
    source 22
    target 1710
  ]
  edge [
    source 22
    target 738
  ]
  edge [
    source 22
    target 1711
  ]
  edge [
    source 22
    target 1712
  ]
  edge [
    source 22
    target 1713
  ]
  edge [
    source 22
    target 1714
  ]
  edge [
    source 22
    target 1715
  ]
  edge [
    source 22
    target 1716
  ]
  edge [
    source 22
    target 1717
  ]
  edge [
    source 22
    target 1718
  ]
  edge [
    source 22
    target 1719
  ]
  edge [
    source 22
    target 1720
  ]
  edge [
    source 22
    target 1721
  ]
  edge [
    source 22
    target 538
  ]
  edge [
    source 22
    target 1722
  ]
  edge [
    source 22
    target 1723
  ]
  edge [
    source 22
    target 1724
  ]
  edge [
    source 22
    target 1725
  ]
  edge [
    source 22
    target 1726
  ]
  edge [
    source 22
    target 1727
  ]
  edge [
    source 22
    target 1728
  ]
  edge [
    source 22
    target 947
  ]
  edge [
    source 22
    target 545
  ]
  edge [
    source 22
    target 1729
  ]
  edge [
    source 22
    target 1730
  ]
  edge [
    source 22
    target 1731
  ]
  edge [
    source 22
    target 959
  ]
  edge [
    source 22
    target 1732
  ]
  edge [
    source 22
    target 99
  ]
  edge [
    source 22
    target 1733
  ]
  edge [
    source 22
    target 1734
  ]
  edge [
    source 22
    target 1735
  ]
  edge [
    source 22
    target 963
  ]
  edge [
    source 22
    target 964
  ]
  edge [
    source 22
    target 970
  ]
  edge [
    source 22
    target 1079
  ]
  edge [
    source 22
    target 975
  ]
  edge [
    source 22
    target 107
  ]
  edge [
    source 22
    target 1736
  ]
  edge [
    source 22
    target 452
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 1737
  ]
  edge [
    source 22
    target 1738
  ]
  edge [
    source 22
    target 1739
  ]
  edge [
    source 22
    target 1740
  ]
  edge [
    source 22
    target 1741
  ]
  edge [
    source 22
    target 1742
  ]
  edge [
    source 22
    target 1743
  ]
  edge [
    source 22
    target 1744
  ]
  edge [
    source 22
    target 1745
  ]
  edge [
    source 22
    target 1746
  ]
  edge [
    source 22
    target 1747
  ]
  edge [
    source 22
    target 974
  ]
  edge [
    source 22
    target 1748
  ]
  edge [
    source 22
    target 1749
  ]
  edge [
    source 22
    target 1750
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 1465
  ]
  edge [
    source 22
    target 1751
  ]
  edge [
    source 22
    target 1752
  ]
  edge [
    source 22
    target 1753
  ]
  edge [
    source 22
    target 1754
  ]
  edge [
    source 22
    target 1755
  ]
  edge [
    source 22
    target 1756
  ]
  edge [
    source 22
    target 1757
  ]
  edge [
    source 22
    target 1758
  ]
  edge [
    source 22
    target 55
  ]
  edge [
    source 22
    target 1759
  ]
  edge [
    source 22
    target 1760
  ]
  edge [
    source 22
    target 774
  ]
  edge [
    source 22
    target 775
  ]
  edge [
    source 22
    target 776
  ]
  edge [
    source 22
    target 777
  ]
  edge [
    source 22
    target 750
  ]
  edge [
    source 22
    target 778
  ]
  edge [
    source 22
    target 779
  ]
  edge [
    source 22
    target 780
  ]
  edge [
    source 22
    target 781
  ]
  edge [
    source 22
    target 782
  ]
  edge [
    source 22
    target 783
  ]
  edge [
    source 22
    target 784
  ]
  edge [
    source 22
    target 785
  ]
  edge [
    source 22
    target 786
  ]
  edge [
    source 22
    target 787
  ]
  edge [
    source 22
    target 729
  ]
  edge [
    source 22
    target 762
  ]
  edge [
    source 22
    target 790
  ]
  edge [
    source 22
    target 791
  ]
  edge [
    source 22
    target 792
  ]
  edge [
    source 22
    target 765
  ]
  edge [
    source 22
    target 793
  ]
  edge [
    source 22
    target 794
  ]
  edge [
    source 22
    target 795
  ]
  edge [
    source 22
    target 796
  ]
  edge [
    source 22
    target 797
  ]
  edge [
    source 22
    target 800
  ]
  edge [
    source 22
    target 801
  ]
  edge [
    source 22
    target 736
  ]
  edge [
    source 22
    target 803
  ]
  edge [
    source 22
    target 804
  ]
  edge [
    source 22
    target 805
  ]
  edge [
    source 22
    target 1761
  ]
  edge [
    source 22
    target 1762
  ]
  edge [
    source 22
    target 1763
  ]
  edge [
    source 22
    target 1764
  ]
  edge [
    source 22
    target 1322
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1765
  ]
  edge [
    source 23
    target 1766
  ]
  edge [
    source 23
    target 1449
  ]
  edge [
    source 23
    target 1767
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 1768
  ]
  edge [
    source 23
    target 1511
  ]
  edge [
    source 23
    target 1769
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 396
  ]
  edge [
    source 23
    target 1770
  ]
  edge [
    source 23
    target 1771
  ]
  edge [
    source 23
    target 1631
  ]
  edge [
    source 23
    target 1632
  ]
  edge [
    source 23
    target 1633
  ]
  edge [
    source 23
    target 1634
  ]
  edge [
    source 23
    target 1772
  ]
  edge [
    source 23
    target 1773
  ]
  edge [
    source 23
    target 1774
  ]
  edge [
    source 23
    target 1775
  ]
  edge [
    source 23
    target 1776
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 1777
  ]
  edge [
    source 23
    target 1778
  ]
  edge [
    source 23
    target 1779
  ]
  edge [
    source 23
    target 746
  ]
  edge [
    source 23
    target 1391
  ]
  edge [
    source 23
    target 1780
  ]
  edge [
    source 23
    target 1781
  ]
  edge [
    source 23
    target 1782
  ]
  edge [
    source 23
    target 1783
  ]
  edge [
    source 23
    target 1784
  ]
  edge [
    source 23
    target 1785
  ]
  edge [
    source 23
    target 1786
  ]
  edge [
    source 23
    target 855
  ]
  edge [
    source 23
    target 1257
  ]
  edge [
    source 23
    target 1787
  ]
  edge [
    source 23
    target 803
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1788
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1789
  ]
  edge [
    source 25
    target 1147
  ]
  edge [
    source 25
    target 1200
  ]
  edge [
    source 25
    target 1201
  ]
  edge [
    source 25
    target 1203
  ]
  edge [
    source 25
    target 1202
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1204
  ]
  edge [
    source 25
    target 1205
  ]
  edge [
    source 25
    target 1206
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1790
  ]
  edge [
    source 25
    target 1791
  ]
  edge [
    source 25
    target 1792
  ]
  edge [
    source 25
    target 1793
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1794
  ]
  edge [
    source 26
    target 1201
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 1164
  ]
  edge [
    source 26
    target 1795
  ]
  edge [
    source 26
    target 1796
  ]
  edge [
    source 26
    target 1797
  ]
  edge [
    source 26
    target 1147
  ]
  edge [
    source 26
    target 1798
  ]
  edge [
    source 26
    target 1799
  ]
  edge [
    source 26
    target 1800
  ]
  edge [
    source 26
    target 1801
  ]
  edge [
    source 26
    target 1802
  ]
  edge [
    source 26
    target 1803
  ]
  edge [
    source 26
    target 406
  ]
  edge [
    source 26
    target 1804
  ]
  edge [
    source 26
    target 1805
  ]
  edge [
    source 26
    target 1806
  ]
  edge [
    source 26
    target 1807
  ]
  edge [
    source 26
    target 1808
  ]
  edge [
    source 26
    target 1809
  ]
  edge [
    source 26
    target 1810
  ]
  edge [
    source 26
    target 1811
  ]
  edge [
    source 26
    target 1209
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 1812
  ]
  edge [
    source 26
    target 1813
  ]
  edge [
    source 26
    target 1814
  ]
  edge [
    source 26
    target 1815
  ]
  edge [
    source 26
    target 1816
  ]
  edge [
    source 26
    target 938
  ]
  edge [
    source 26
    target 1817
  ]
  edge [
    source 26
    target 1818
  ]
  edge [
    source 26
    target 1819
  ]
  edge [
    source 26
    target 1143
  ]
  edge [
    source 26
    target 1820
  ]
  edge [
    source 26
    target 1821
  ]
  edge [
    source 26
    target 1822
  ]
  edge [
    source 26
    target 1823
  ]
  edge [
    source 26
    target 1824
  ]
  edge [
    source 26
    target 1825
  ]
  edge [
    source 26
    target 1826
  ]
  edge [
    source 26
    target 1827
  ]
  edge [
    source 26
    target 1200
  ]
  edge [
    source 26
    target 1204
  ]
  edge [
    source 26
    target 1205
  ]
  edge [
    source 26
    target 1206
  ]
  edge [
    source 26
    target 1207
  ]
  edge [
    source 26
    target 1828
  ]
  edge [
    source 26
    target 1829
  ]
  edge [
    source 26
    target 1830
  ]
  edge [
    source 26
    target 1831
  ]
  edge [
    source 26
    target 1832
  ]
  edge [
    source 26
    target 1833
  ]
  edge [
    source 26
    target 1834
  ]
  edge [
    source 26
    target 1835
  ]
  edge [
    source 26
    target 1836
  ]
  edge [
    source 26
    target 1837
  ]
  edge [
    source 26
    target 1838
  ]
  edge [
    source 26
    target 1139
  ]
  edge [
    source 26
    target 1140
  ]
  edge [
    source 26
    target 1141
  ]
  edge [
    source 26
    target 1142
  ]
  edge [
    source 26
    target 65
  ]
  edge [
    source 26
    target 1144
  ]
  edge [
    source 26
    target 1145
  ]
  edge [
    source 26
    target 1146
  ]
  edge [
    source 26
    target 1839
  ]
  edge [
    source 26
    target 1840
  ]
  edge [
    source 26
    target 1841
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 188
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 28
    target 190
  ]
  edge [
    source 28
    target 191
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 193
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 44
  ]
  edge [
    source 28
    target 45
  ]
  edge [
    source 28
    target 46
  ]
  edge [
    source 28
    target 47
  ]
  edge [
    source 28
    target 48
  ]
  edge [
    source 28
    target 49
  ]
  edge [
    source 28
    target 50
  ]
  edge [
    source 28
    target 51
  ]
  edge [
    source 28
    target 52
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 28
    target 56
  ]
  edge [
    source 28
    target 1842
  ]
  edge [
    source 28
    target 1843
  ]
  edge [
    source 28
    target 1844
  ]
  edge [
    source 28
    target 1845
  ]
  edge [
    source 28
    target 1846
  ]
  edge [
    source 28
    target 1847
  ]
  edge [
    source 28
    target 1848
  ]
  edge [
    source 28
    target 1849
  ]
  edge [
    source 28
    target 1850
  ]
  edge [
    source 28
    target 1851
  ]
  edge [
    source 28
    target 1852
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 1853
  ]
  edge [
    source 28
    target 1820
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1854
  ]
  edge [
    source 29
    target 1855
  ]
  edge [
    source 29
    target 1856
  ]
  edge [
    source 29
    target 914
  ]
  edge [
    source 29
    target 1857
  ]
  edge [
    source 29
    target 1858
  ]
  edge [
    source 29
    target 1859
  ]
  edge [
    source 29
    target 1860
  ]
  edge [
    source 29
    target 1861
  ]
  edge [
    source 29
    target 1862
  ]
  edge [
    source 29
    target 1863
  ]
  edge [
    source 29
    target 1864
  ]
  edge [
    source 29
    target 1865
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 317
  ]
  edge [
    source 30
    target 1866
  ]
  edge [
    source 30
    target 1867
  ]
  edge [
    source 30
    target 1868
  ]
  edge [
    source 30
    target 1869
  ]
  edge [
    source 30
    target 1695
  ]
  edge [
    source 30
    target 314
  ]
  edge [
    source 30
    target 1870
  ]
  edge [
    source 30
    target 1871
  ]
  edge [
    source 30
    target 1872
  ]
  edge [
    source 30
    target 1873
  ]
  edge [
    source 30
    target 315
  ]
  edge [
    source 30
    target 1874
  ]
  edge [
    source 30
    target 1875
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 1876
  ]
  edge [
    source 30
    target 1877
  ]
  edge [
    source 30
    target 1878
  ]
  edge [
    source 30
    target 213
  ]
  edge [
    source 30
    target 1879
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 1880
  ]
  edge [
    source 32
    target 1881
  ]
  edge [
    source 32
    target 1882
  ]
  edge [
    source 32
    target 1883
  ]
  edge [
    source 32
    target 1884
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1885
  ]
  edge [
    source 33
    target 1886
  ]
  edge [
    source 33
    target 1887
  ]
  edge [
    source 33
    target 556
  ]
  edge [
    source 33
    target 1888
  ]
  edge [
    source 33
    target 1889
  ]
  edge [
    source 33
    target 1890
  ]
  edge [
    source 33
    target 1891
  ]
  edge [
    source 33
    target 1892
  ]
  edge [
    source 33
    target 1893
  ]
  edge [
    source 33
    target 1894
  ]
  edge [
    source 33
    target 1895
  ]
  edge [
    source 33
    target 761
  ]
  edge [
    source 33
    target 1896
  ]
  edge [
    source 33
    target 1897
  ]
  edge [
    source 33
    target 1898
  ]
  edge [
    source 33
    target 1899
  ]
  edge [
    source 33
    target 1900
  ]
  edge [
    source 33
    target 949
  ]
  edge [
    source 33
    target 816
  ]
  edge [
    source 33
    target 1901
  ]
  edge [
    source 33
    target 1902
  ]
  edge [
    source 33
    target 1345
  ]
  edge [
    source 33
    target 1615
  ]
  edge [
    source 33
    target 1903
  ]
  edge [
    source 33
    target 1904
  ]
  edge [
    source 33
    target 1905
  ]
  edge [
    source 33
    target 1276
  ]
  edge [
    source 33
    target 1906
  ]
  edge [
    source 33
    target 1907
  ]
  edge [
    source 33
    target 95
  ]
  edge [
    source 33
    target 717
  ]
  edge [
    source 33
    target 1908
  ]
  edge [
    source 33
    target 1909
  ]
  edge [
    source 33
    target 1910
  ]
  edge [
    source 33
    target 1911
  ]
  edge [
    source 33
    target 206
  ]
  edge [
    source 33
    target 1912
  ]
  edge [
    source 33
    target 1913
  ]
  edge [
    source 33
    target 1914
  ]
  edge [
    source 33
    target 1915
  ]
  edge [
    source 33
    target 1916
  ]
  edge [
    source 33
    target 1917
  ]
  edge [
    source 33
    target 1918
  ]
  edge [
    source 33
    target 106
  ]
  edge [
    source 33
    target 1919
  ]
  edge [
    source 33
    target 1920
  ]
  edge [
    source 33
    target 1921
  ]
  edge [
    source 33
    target 1922
  ]
  edge [
    source 33
    target 1351
  ]
  edge [
    source 33
    target 1923
  ]
  edge [
    source 33
    target 1924
  ]
  edge [
    source 33
    target 1534
  ]
  edge [
    source 33
    target 415
  ]
  edge [
    source 33
    target 1925
  ]
  edge [
    source 33
    target 1926
  ]
  edge [
    source 33
    target 42
  ]
  edge [
    source 33
    target 53
  ]
  edge [
    source 33
    target 1927
  ]
  edge [
    source 33
    target 504
  ]
  edge [
    source 33
    target 1928
  ]
  edge [
    source 33
    target 1929
  ]
  edge [
    source 33
    target 1264
  ]
  edge [
    source 33
    target 1930
  ]
  edge [
    source 33
    target 1931
  ]
  edge [
    source 33
    target 1932
  ]
  edge [
    source 33
    target 1933
  ]
  edge [
    source 33
    target 1581
  ]
  edge [
    source 33
    target 1934
  ]
  edge [
    source 33
    target 719
  ]
  edge [
    source 33
    target 1554
  ]
  edge [
    source 33
    target 1555
  ]
  edge [
    source 33
    target 1556
  ]
  edge [
    source 33
    target 1557
  ]
  edge [
    source 33
    target 1558
  ]
  edge [
    source 33
    target 1559
  ]
  edge [
    source 33
    target 1560
  ]
  edge [
    source 33
    target 1561
  ]
  edge [
    source 33
    target 89
  ]
  edge [
    source 33
    target 1562
  ]
  edge [
    source 33
    target 1563
  ]
  edge [
    source 33
    target 1564
  ]
  edge [
    source 33
    target 1565
  ]
  edge [
    source 33
    target 1566
  ]
  edge [
    source 33
    target 1567
  ]
  edge [
    source 33
    target 1568
  ]
  edge [
    source 33
    target 1569
  ]
  edge [
    source 33
    target 1570
  ]
  edge [
    source 33
    target 1549
  ]
  edge [
    source 33
    target 1571
  ]
  edge [
    source 33
    target 1572
  ]
  edge [
    source 33
    target 1573
  ]
  edge [
    source 33
    target 294
  ]
  edge [
    source 33
    target 1574
  ]
  edge [
    source 33
    target 1575
  ]
  edge [
    source 33
    target 1935
  ]
  edge [
    source 33
    target 492
  ]
  edge [
    source 33
    target 950
  ]
  edge [
    source 33
    target 1936
  ]
  edge [
    source 33
    target 1937
  ]
  edge [
    source 33
    target 1938
  ]
  edge [
    source 33
    target 1939
  ]
  edge [
    source 33
    target 1940
  ]
  edge [
    source 33
    target 1941
  ]
  edge [
    source 33
    target 1942
  ]
  edge [
    source 33
    target 1943
  ]
  edge [
    source 33
    target 1944
  ]
  edge [
    source 33
    target 1945
  ]
  edge [
    source 33
    target 1946
  ]
  edge [
    source 33
    target 1947
  ]
  edge [
    source 33
    target 1948
  ]
  edge [
    source 33
    target 1949
  ]
  edge [
    source 33
    target 1950
  ]
  edge [
    source 33
    target 1951
  ]
  edge [
    source 33
    target 1952
  ]
  edge [
    source 33
    target 1953
  ]
  edge [
    source 33
    target 1954
  ]
  edge [
    source 33
    target 1955
  ]
  edge [
    source 33
    target 1956
  ]
  edge [
    source 33
    target 1957
  ]
  edge [
    source 33
    target 1410
  ]
  edge [
    source 33
    target 1958
  ]
  edge [
    source 33
    target 1959
  ]
  edge [
    source 33
    target 1960
  ]
  edge [
    source 33
    target 1961
  ]
  edge [
    source 33
    target 1962
  ]
  edge [
    source 33
    target 1963
  ]
  edge [
    source 33
    target 1964
  ]
  edge [
    source 33
    target 1010
  ]
  edge [
    source 33
    target 361
  ]
  edge [
    source 33
    target 1965
  ]
  edge [
    source 33
    target 1966
  ]
  edge [
    source 33
    target 1967
  ]
  edge [
    source 33
    target 357
  ]
  edge [
    source 33
    target 736
  ]
  edge [
    source 33
    target 853
  ]
  edge [
    source 33
    target 1968
  ]
  edge [
    source 33
    target 1352
  ]
  edge [
    source 33
    target 1747
  ]
  edge [
    source 33
    target 1969
  ]
  edge [
    source 33
    target 261
  ]
  edge [
    source 33
    target 1970
  ]
  edge [
    source 33
    target 765
  ]
  edge [
    source 33
    target 1971
  ]
  edge [
    source 33
    target 1972
  ]
  edge [
    source 33
    target 1973
  ]
  edge [
    source 33
    target 147
  ]
  edge [
    source 33
    target 1974
  ]
  edge [
    source 33
    target 1975
  ]
  edge [
    source 33
    target 538
  ]
  edge [
    source 33
    target 1976
  ]
  edge [
    source 33
    target 1977
  ]
  edge [
    source 33
    target 554
  ]
  edge [
    source 33
    target 865
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1391
  ]
  edge [
    source 34
    target 1978
  ]
  edge [
    source 34
    target 1979
  ]
  edge [
    source 34
    target 1980
  ]
  edge [
    source 34
    target 1981
  ]
  edge [
    source 34
    target 1449
  ]
  edge [
    source 34
    target 1450
  ]
  edge [
    source 34
    target 1451
  ]
  edge [
    source 34
    target 1044
  ]
  edge [
    source 34
    target 1982
  ]
  edge [
    source 34
    target 622
  ]
  edge [
    source 34
    target 1983
  ]
  edge [
    source 34
    target 1984
  ]
  edge [
    source 34
    target 1985
  ]
  edge [
    source 34
    target 1986
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 1987
  ]
  edge [
    source 34
    target 527
  ]
  edge [
    source 34
    target 1988
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1989
  ]
  edge [
    source 35
    target 1990
  ]
  edge [
    source 35
    target 1175
  ]
  edge [
    source 35
    target 1991
  ]
  edge [
    source 35
    target 1992
  ]
  edge [
    source 35
    target 1993
  ]
  edge [
    source 35
    target 1994
  ]
  edge [
    source 35
    target 1995
  ]
  edge [
    source 35
    target 1996
  ]
  edge [
    source 35
    target 1997
  ]
  edge [
    source 35
    target 1998
  ]
  edge [
    source 35
    target 1999
  ]
  edge [
    source 35
    target 1803
  ]
  edge [
    source 35
    target 2000
  ]
  edge [
    source 35
    target 2001
  ]
  edge [
    source 35
    target 1810
  ]
  edge [
    source 35
    target 2002
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 2003
  ]
  edge [
    source 36
    target 2004
  ]
  edge [
    source 36
    target 2005
  ]
  edge [
    source 36
    target 2006
  ]
  edge [
    source 36
    target 2007
  ]
  edge [
    source 36
    target 1106
  ]
  edge [
    source 36
    target 2008
  ]
  edge [
    source 36
    target 2009
  ]
  edge [
    source 36
    target 2010
  ]
  edge [
    source 36
    target 2011
  ]
  edge [
    source 36
    target 2012
  ]
  edge [
    source 36
    target 2013
  ]
  edge [
    source 36
    target 2014
  ]
  edge [
    source 36
    target 2015
  ]
  edge [
    source 36
    target 2016
  ]
  edge [
    source 36
    target 2017
  ]
  edge [
    source 36
    target 2018
  ]
  edge [
    source 36
    target 2019
  ]
  edge [
    source 36
    target 2020
  ]
  edge [
    source 36
    target 2021
  ]
  edge [
    source 36
    target 2022
  ]
  edge [
    source 36
    target 972
  ]
  edge [
    source 36
    target 2023
  ]
  edge [
    source 36
    target 2024
  ]
  edge [
    source 36
    target 2025
  ]
  edge [
    source 36
    target 2026
  ]
  edge [
    source 36
    target 2027
  ]
  edge [
    source 36
    target 2028
  ]
  edge [
    source 36
    target 2029
  ]
  edge [
    source 36
    target 2030
  ]
  edge [
    source 36
    target 2031
  ]
  edge [
    source 36
    target 2032
  ]
  edge [
    source 36
    target 2033
  ]
  edge [
    source 36
    target 2034
  ]
  edge [
    source 36
    target 2035
  ]
  edge [
    source 36
    target 2036
  ]
  edge [
    source 36
    target 106
  ]
  edge [
    source 36
    target 2037
  ]
  edge [
    source 36
    target 2038
  ]
  edge [
    source 36
    target 2039
  ]
  edge [
    source 36
    target 2040
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 147
  ]
  edge [
    source 37
    target 1089
  ]
  edge [
    source 37
    target 1056
  ]
  edge [
    source 37
    target 2041
  ]
  edge [
    source 37
    target 1091
  ]
  edge [
    source 37
    target 2042
  ]
  edge [
    source 37
    target 326
  ]
  edge [
    source 37
    target 2043
  ]
  edge [
    source 37
    target 1050
  ]
  edge [
    source 37
    target 2044
  ]
  edge [
    source 37
    target 2045
  ]
  edge [
    source 37
    target 2046
  ]
  edge [
    source 37
    target 2047
  ]
  edge [
    source 37
    target 2048
  ]
  edge [
    source 37
    target 1088
  ]
  edge [
    source 37
    target 1090
  ]
  edge [
    source 37
    target 325
  ]
  edge [
    source 37
    target 2049
  ]
  edge [
    source 37
    target 2050
  ]
  edge [
    source 37
    target 197
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 198
  ]
  edge [
    source 37
    target 199
  ]
  edge [
    source 37
    target 200
  ]
  edge [
    source 37
    target 201
  ]
  edge [
    source 37
    target 202
  ]
  edge [
    source 37
    target 203
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 708
  ]
  edge [
    source 39
    target 949
  ]
  edge [
    source 39
    target 700
  ]
  edge [
    source 39
    target 261
  ]
  edge [
    source 39
    target 2051
  ]
  edge [
    source 39
    target 2052
  ]
  edge [
    source 39
    target 95
  ]
  edge [
    source 39
    target 204
  ]
  edge [
    source 39
    target 79
  ]
  edge [
    source 39
    target 205
  ]
  edge [
    source 39
    target 2053
  ]
  edge [
    source 39
    target 2054
  ]
  edge [
    source 39
    target 806
  ]
  edge [
    source 39
    target 807
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 808
  ]
  edge [
    source 39
    target 809
  ]
  edge [
    source 39
    target 200
  ]
  edge [
    source 39
    target 201
  ]
  edge [
    source 39
    target 202
  ]
  edge [
    source 39
    target 810
  ]
  edge [
    source 39
    target 811
  ]
  edge [
    source 39
    target 2055
  ]
  edge [
    source 39
    target 2056
  ]
  edge [
    source 39
    target 2057
  ]
  edge [
    source 39
    target 2058
  ]
  edge [
    source 39
    target 1669
  ]
  edge [
    source 39
    target 2059
  ]
  edge [
    source 39
    target 2060
  ]
  edge [
    source 39
    target 2061
  ]
  edge [
    source 39
    target 2062
  ]
  edge [
    source 39
    target 357
  ]
  edge [
    source 39
    target 2063
  ]
  edge [
    source 39
    target 2064
  ]
  edge [
    source 39
    target 2065
  ]
  edge [
    source 39
    target 2066
  ]
  edge [
    source 39
    target 2067
  ]
  edge [
    source 39
    target 2078
  ]
  edge [
    source 39
    target 2079
  ]
  edge [
    source 39
    target 2076
  ]
  edge [
    source 39
    target 2080
  ]
  edge [
    source 39
    target 2081
  ]
  edge [
    source 39
    target 2082
  ]
  edge [
    source 39
    target 2083
  ]
  edge [
    source 39
    target 2084
  ]
  edge [
    source 39
    target 984
  ]
  edge [
    source 39
    target 2085
  ]
  edge [
    source 39
    target 1355
  ]
  edge [
    source 39
    target 1837
  ]
  edge [
    source 39
    target 2086
  ]
  edge [
    source 39
    target 610
  ]
  edge [
    source 39
    target 2087
  ]
  edge [
    source 39
    target 2088
  ]
  edge [
    source 39
    target 105
  ]
  edge [
    source 39
    target 563
  ]
  edge [
    source 39
    target 2089
  ]
  edge [
    source 39
    target 2090
  ]
  edge [
    source 39
    target 2091
  ]
  edge [
    source 39
    target 2092
  ]
  edge [
    source 39
    target 2093
  ]
  edge [
    source 40
    target 2068
  ]
  edge [
    source 40
    target 2069
  ]
  edge [
    source 40
    target 2070
  ]
  edge [
    source 40
    target 2071
  ]
  edge [
    source 40
    target 2072
  ]
  edge [
    source 40
    target 2073
  ]
  edge [
    source 40
    target 2074
  ]
  edge [
    source 40
    target 2075
  ]
  edge [
    source 105
    target 2078
  ]
  edge [
    source 105
    target 2079
  ]
  edge [
    source 105
    target 2076
  ]
  edge [
    source 105
    target 2080
  ]
  edge [
    source 105
    target 2081
  ]
  edge [
    source 105
    target 2082
  ]
  edge [
    source 105
    target 2083
  ]
  edge [
    source 105
    target 2084
  ]
  edge [
    source 105
    target 984
  ]
  edge [
    source 105
    target 2085
  ]
  edge [
    source 105
    target 1355
  ]
  edge [
    source 105
    target 1837
  ]
  edge [
    source 105
    target 2086
  ]
  edge [
    source 105
    target 610
  ]
  edge [
    source 105
    target 2087
  ]
  edge [
    source 105
    target 2088
  ]
  edge [
    source 105
    target 563
  ]
  edge [
    source 105
    target 2089
  ]
  edge [
    source 105
    target 2090
  ]
  edge [
    source 105
    target 2091
  ]
  edge [
    source 105
    target 2092
  ]
  edge [
    source 105
    target 2093
  ]
  edge [
    source 563
    target 2078
  ]
  edge [
    source 563
    target 2079
  ]
  edge [
    source 563
    target 2076
  ]
  edge [
    source 563
    target 2080
  ]
  edge [
    source 563
    target 2081
  ]
  edge [
    source 563
    target 2082
  ]
  edge [
    source 563
    target 2083
  ]
  edge [
    source 563
    target 2084
  ]
  edge [
    source 563
    target 984
  ]
  edge [
    source 563
    target 2085
  ]
  edge [
    source 563
    target 1355
  ]
  edge [
    source 563
    target 1837
  ]
  edge [
    source 563
    target 2086
  ]
  edge [
    source 563
    target 610
  ]
  edge [
    source 563
    target 2087
  ]
  edge [
    source 563
    target 2088
  ]
  edge [
    source 563
    target 2089
  ]
  edge [
    source 563
    target 2090
  ]
  edge [
    source 563
    target 2091
  ]
  edge [
    source 563
    target 2092
  ]
  edge [
    source 563
    target 2093
  ]
  edge [
    source 573
    target 2076
  ]
  edge [
    source 573
    target 2077
  ]
  edge [
    source 610
    target 2078
  ]
  edge [
    source 610
    target 2079
  ]
  edge [
    source 610
    target 2076
  ]
  edge [
    source 610
    target 2080
  ]
  edge [
    source 610
    target 2081
  ]
  edge [
    source 610
    target 2082
  ]
  edge [
    source 610
    target 2083
  ]
  edge [
    source 610
    target 2084
  ]
  edge [
    source 610
    target 984
  ]
  edge [
    source 610
    target 2085
  ]
  edge [
    source 610
    target 1355
  ]
  edge [
    source 610
    target 1837
  ]
  edge [
    source 610
    target 2086
  ]
  edge [
    source 610
    target 2087
  ]
  edge [
    source 610
    target 2088
  ]
  edge [
    source 610
    target 2089
  ]
  edge [
    source 610
    target 2090
  ]
  edge [
    source 610
    target 2091
  ]
  edge [
    source 610
    target 2092
  ]
  edge [
    source 610
    target 2093
  ]
  edge [
    source 984
    target 2078
  ]
  edge [
    source 984
    target 2079
  ]
  edge [
    source 984
    target 2076
  ]
  edge [
    source 984
    target 2080
  ]
  edge [
    source 984
    target 2081
  ]
  edge [
    source 984
    target 2082
  ]
  edge [
    source 984
    target 2083
  ]
  edge [
    source 984
    target 2084
  ]
  edge [
    source 984
    target 2085
  ]
  edge [
    source 984
    target 1355
  ]
  edge [
    source 984
    target 1837
  ]
  edge [
    source 984
    target 2086
  ]
  edge [
    source 984
    target 2087
  ]
  edge [
    source 984
    target 2088
  ]
  edge [
    source 984
    target 2089
  ]
  edge [
    source 984
    target 2090
  ]
  edge [
    source 984
    target 2091
  ]
  edge [
    source 984
    target 2092
  ]
  edge [
    source 984
    target 2093
  ]
  edge [
    source 1355
    target 2078
  ]
  edge [
    source 1355
    target 2079
  ]
  edge [
    source 1355
    target 2076
  ]
  edge [
    source 1355
    target 2080
  ]
  edge [
    source 1355
    target 2081
  ]
  edge [
    source 1355
    target 2082
  ]
  edge [
    source 1355
    target 2083
  ]
  edge [
    source 1355
    target 2084
  ]
  edge [
    source 1355
    target 2085
  ]
  edge [
    source 1355
    target 1837
  ]
  edge [
    source 1355
    target 2086
  ]
  edge [
    source 1355
    target 2087
  ]
  edge [
    source 1355
    target 2088
  ]
  edge [
    source 1355
    target 2089
  ]
  edge [
    source 1355
    target 2090
  ]
  edge [
    source 1355
    target 2091
  ]
  edge [
    source 1355
    target 2092
  ]
  edge [
    source 1355
    target 2093
  ]
  edge [
    source 1837
    target 2078
  ]
  edge [
    source 1837
    target 2079
  ]
  edge [
    source 1837
    target 2076
  ]
  edge [
    source 1837
    target 2080
  ]
  edge [
    source 1837
    target 2081
  ]
  edge [
    source 1837
    target 2082
  ]
  edge [
    source 1837
    target 2083
  ]
  edge [
    source 1837
    target 2084
  ]
  edge [
    source 1837
    target 2085
  ]
  edge [
    source 1837
    target 2086
  ]
  edge [
    source 1837
    target 2087
  ]
  edge [
    source 1837
    target 2088
  ]
  edge [
    source 1837
    target 2089
  ]
  edge [
    source 1837
    target 2090
  ]
  edge [
    source 1837
    target 2091
  ]
  edge [
    source 1837
    target 2092
  ]
  edge [
    source 1837
    target 2093
  ]
  edge [
    source 2076
    target 2077
  ]
  edge [
    source 2076
    target 2078
  ]
  edge [
    source 2076
    target 2079
  ]
  edge [
    source 2076
    target 2080
  ]
  edge [
    source 2076
    target 2081
  ]
  edge [
    source 2076
    target 2082
  ]
  edge [
    source 2076
    target 2083
  ]
  edge [
    source 2076
    target 2084
  ]
  edge [
    source 2076
    target 2085
  ]
  edge [
    source 2076
    target 2086
  ]
  edge [
    source 2076
    target 2087
  ]
  edge [
    source 2076
    target 2088
  ]
  edge [
    source 2076
    target 2089
  ]
  edge [
    source 2076
    target 2090
  ]
  edge [
    source 2076
    target 2091
  ]
  edge [
    source 2076
    target 2092
  ]
  edge [
    source 2076
    target 2093
  ]
  edge [
    source 2078
    target 2079
  ]
  edge [
    source 2078
    target 2080
  ]
  edge [
    source 2078
    target 2081
  ]
  edge [
    source 2078
    target 2082
  ]
  edge [
    source 2078
    target 2083
  ]
  edge [
    source 2078
    target 2084
  ]
  edge [
    source 2078
    target 2085
  ]
  edge [
    source 2078
    target 2086
  ]
  edge [
    source 2078
    target 2087
  ]
  edge [
    source 2078
    target 2088
  ]
  edge [
    source 2078
    target 2089
  ]
  edge [
    source 2078
    target 2090
  ]
  edge [
    source 2078
    target 2091
  ]
  edge [
    source 2078
    target 2092
  ]
  edge [
    source 2078
    target 2093
  ]
  edge [
    source 2079
    target 2080
  ]
  edge [
    source 2079
    target 2081
  ]
  edge [
    source 2079
    target 2082
  ]
  edge [
    source 2079
    target 2083
  ]
  edge [
    source 2079
    target 2084
  ]
  edge [
    source 2079
    target 2085
  ]
  edge [
    source 2079
    target 2086
  ]
  edge [
    source 2079
    target 2087
  ]
  edge [
    source 2079
    target 2088
  ]
  edge [
    source 2079
    target 2089
  ]
  edge [
    source 2079
    target 2090
  ]
  edge [
    source 2079
    target 2091
  ]
  edge [
    source 2079
    target 2092
  ]
  edge [
    source 2079
    target 2093
  ]
  edge [
    source 2080
    target 2081
  ]
  edge [
    source 2080
    target 2082
  ]
  edge [
    source 2080
    target 2083
  ]
  edge [
    source 2080
    target 2084
  ]
  edge [
    source 2080
    target 2085
  ]
  edge [
    source 2080
    target 2086
  ]
  edge [
    source 2080
    target 2087
  ]
  edge [
    source 2080
    target 2088
  ]
  edge [
    source 2080
    target 2089
  ]
  edge [
    source 2080
    target 2090
  ]
  edge [
    source 2080
    target 2091
  ]
  edge [
    source 2080
    target 2092
  ]
  edge [
    source 2080
    target 2093
  ]
  edge [
    source 2081
    target 2082
  ]
  edge [
    source 2081
    target 2083
  ]
  edge [
    source 2081
    target 2084
  ]
  edge [
    source 2081
    target 2085
  ]
  edge [
    source 2081
    target 2086
  ]
  edge [
    source 2081
    target 2087
  ]
  edge [
    source 2081
    target 2088
  ]
  edge [
    source 2081
    target 2089
  ]
  edge [
    source 2081
    target 2090
  ]
  edge [
    source 2081
    target 2091
  ]
  edge [
    source 2081
    target 2092
  ]
  edge [
    source 2081
    target 2093
  ]
  edge [
    source 2082
    target 2083
  ]
  edge [
    source 2082
    target 2084
  ]
  edge [
    source 2082
    target 2085
  ]
  edge [
    source 2082
    target 2086
  ]
  edge [
    source 2082
    target 2087
  ]
  edge [
    source 2082
    target 2088
  ]
  edge [
    source 2082
    target 2089
  ]
  edge [
    source 2082
    target 2090
  ]
  edge [
    source 2082
    target 2091
  ]
  edge [
    source 2082
    target 2092
  ]
  edge [
    source 2082
    target 2093
  ]
  edge [
    source 2083
    target 2084
  ]
  edge [
    source 2083
    target 2085
  ]
  edge [
    source 2083
    target 2086
  ]
  edge [
    source 2083
    target 2087
  ]
  edge [
    source 2083
    target 2088
  ]
  edge [
    source 2083
    target 2089
  ]
  edge [
    source 2083
    target 2090
  ]
  edge [
    source 2083
    target 2091
  ]
  edge [
    source 2083
    target 2092
  ]
  edge [
    source 2083
    target 2093
  ]
  edge [
    source 2084
    target 2085
  ]
  edge [
    source 2084
    target 2086
  ]
  edge [
    source 2084
    target 2087
  ]
  edge [
    source 2084
    target 2088
  ]
  edge [
    source 2084
    target 2089
  ]
  edge [
    source 2084
    target 2090
  ]
  edge [
    source 2084
    target 2091
  ]
  edge [
    source 2084
    target 2092
  ]
  edge [
    source 2084
    target 2093
  ]
  edge [
    source 2085
    target 2086
  ]
  edge [
    source 2085
    target 2087
  ]
  edge [
    source 2085
    target 2088
  ]
  edge [
    source 2085
    target 2089
  ]
  edge [
    source 2085
    target 2090
  ]
  edge [
    source 2085
    target 2091
  ]
  edge [
    source 2085
    target 2092
  ]
  edge [
    source 2085
    target 2093
  ]
  edge [
    source 2086
    target 2087
  ]
  edge [
    source 2086
    target 2088
  ]
  edge [
    source 2086
    target 2089
  ]
  edge [
    source 2086
    target 2090
  ]
  edge [
    source 2086
    target 2091
  ]
  edge [
    source 2086
    target 2092
  ]
  edge [
    source 2086
    target 2093
  ]
  edge [
    source 2087
    target 2088
  ]
  edge [
    source 2087
    target 2089
  ]
  edge [
    source 2087
    target 2090
  ]
  edge [
    source 2087
    target 2091
  ]
  edge [
    source 2087
    target 2092
  ]
  edge [
    source 2087
    target 2093
  ]
  edge [
    source 2088
    target 2089
  ]
  edge [
    source 2088
    target 2090
  ]
  edge [
    source 2088
    target 2091
  ]
  edge [
    source 2088
    target 2092
  ]
  edge [
    source 2088
    target 2093
  ]
  edge [
    source 2089
    target 2090
  ]
  edge [
    source 2089
    target 2091
  ]
  edge [
    source 2089
    target 2092
  ]
  edge [
    source 2089
    target 2093
  ]
  edge [
    source 2090
    target 2091
  ]
  edge [
    source 2090
    target 2092
  ]
  edge [
    source 2090
    target 2093
  ]
  edge [
    source 2091
    target 2092
  ]
  edge [
    source 2091
    target 2093
  ]
  edge [
    source 2092
    target 2093
  ]
  edge [
    source 2094
    target 2095
  ]
  edge [
    source 2096
    target 2097
  ]
]
