graph [
  node [
    id 0
    label "dawny"
    origin "text"
  ]
  node [
    id 1
    label "wymagaj&#261;ca"
    origin "text"
  ]
  node [
    id 2
    label "ustalenie"
    origin "text"
  ]
  node [
    id 3
    label "strefa"
    origin "text"
  ]
  node [
    id 4
    label "ochrona"
    origin "text"
  ]
  node [
    id 5
    label "ostoja"
    origin "text"
  ]
  node [
    id 6
    label "lub"
    origin "text"
  ]
  node [
    id 7
    label "stanowisko"
    origin "text"
  ]
  node [
    id 8
    label "od_dawna"
  ]
  node [
    id 9
    label "anachroniczny"
  ]
  node [
    id 10
    label "dawniej"
  ]
  node [
    id 11
    label "odleg&#322;y"
  ]
  node [
    id 12
    label "przesz&#322;y"
  ]
  node [
    id 13
    label "d&#322;ugoletni"
  ]
  node [
    id 14
    label "poprzedni"
  ]
  node [
    id 15
    label "dawno"
  ]
  node [
    id 16
    label "przestarza&#322;y"
  ]
  node [
    id 17
    label "kombatant"
  ]
  node [
    id 18
    label "niegdysiejszy"
  ]
  node [
    id 19
    label "wcze&#347;niejszy"
  ]
  node [
    id 20
    label "stary"
  ]
  node [
    id 21
    label "poprzednio"
  ]
  node [
    id 22
    label "anachronicznie"
  ]
  node [
    id 23
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 24
    label "niezgodny"
  ]
  node [
    id 25
    label "zestarzenie_si&#281;"
  ]
  node [
    id 26
    label "niedzisiejszy"
  ]
  node [
    id 27
    label "starzenie_si&#281;"
  ]
  node [
    id 28
    label "archaicznie"
  ]
  node [
    id 29
    label "przestarzale"
  ]
  node [
    id 30
    label "staro&#347;wiecki"
  ]
  node [
    id 31
    label "zgrzybienie"
  ]
  node [
    id 32
    label "ongi&#347;"
  ]
  node [
    id 33
    label "odlegle"
  ]
  node [
    id 34
    label "nieobecny"
  ]
  node [
    id 35
    label "oddalony"
  ]
  node [
    id 36
    label "obcy"
  ]
  node [
    id 37
    label "daleko"
  ]
  node [
    id 38
    label "daleki"
  ]
  node [
    id 39
    label "r&#243;&#380;ny"
  ]
  node [
    id 40
    label "s&#322;aby"
  ]
  node [
    id 41
    label "delikatny"
  ]
  node [
    id 42
    label "zwierzchnik"
  ]
  node [
    id 43
    label "charakterystyczny"
  ]
  node [
    id 44
    label "ojciec"
  ]
  node [
    id 45
    label "starczo"
  ]
  node [
    id 46
    label "starzy"
  ]
  node [
    id 47
    label "p&#243;&#378;ny"
  ]
  node [
    id 48
    label "dojrza&#322;y"
  ]
  node [
    id 49
    label "brat"
  ]
  node [
    id 50
    label "m&#261;&#380;"
  ]
  node [
    id 51
    label "gruba_ryba"
  ]
  node [
    id 52
    label "po_staro&#347;wiecku"
  ]
  node [
    id 53
    label "staro"
  ]
  node [
    id 54
    label "nienowoczesny"
  ]
  node [
    id 55
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 56
    label "dotychczasowy"
  ]
  node [
    id 57
    label "znajomy"
  ]
  node [
    id 58
    label "wcze&#347;niej"
  ]
  node [
    id 59
    label "ostatni"
  ]
  node [
    id 60
    label "miniony"
  ]
  node [
    id 61
    label "wieloletni"
  ]
  node [
    id 62
    label "d&#322;ugi"
  ]
  node [
    id 63
    label "kiedy&#347;"
  ]
  node [
    id 64
    label "d&#322;ugotrwale"
  ]
  node [
    id 65
    label "dawnie"
  ]
  node [
    id 66
    label "weteran"
  ]
  node [
    id 67
    label "wyjadacz"
  ]
  node [
    id 68
    label "umocnienie"
  ]
  node [
    id 69
    label "zdecydowanie"
  ]
  node [
    id 70
    label "spowodowanie"
  ]
  node [
    id 71
    label "appointment"
  ]
  node [
    id 72
    label "informacja"
  ]
  node [
    id 73
    label "decyzja"
  ]
  node [
    id 74
    label "zrobienie"
  ]
  node [
    id 75
    label "localization"
  ]
  node [
    id 76
    label "czynno&#347;&#263;"
  ]
  node [
    id 77
    label "punkt"
  ]
  node [
    id 78
    label "powzi&#281;cie"
  ]
  node [
    id 79
    label "obieganie"
  ]
  node [
    id 80
    label "sygna&#322;"
  ]
  node [
    id 81
    label "obiec"
  ]
  node [
    id 82
    label "doj&#347;&#263;"
  ]
  node [
    id 83
    label "wiedza"
  ]
  node [
    id 84
    label "publikacja"
  ]
  node [
    id 85
    label "powzi&#261;&#263;"
  ]
  node [
    id 86
    label "doj&#347;cie"
  ]
  node [
    id 87
    label "obiega&#263;"
  ]
  node [
    id 88
    label "obiegni&#281;cie"
  ]
  node [
    id 89
    label "dane"
  ]
  node [
    id 90
    label "zauwa&#380;alnie"
  ]
  node [
    id 91
    label "judgment"
  ]
  node [
    id 92
    label "cecha"
  ]
  node [
    id 93
    label "podj&#281;cie"
  ]
  node [
    id 94
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 95
    label "oddzia&#322;anie"
  ]
  node [
    id 96
    label "resoluteness"
  ]
  node [
    id 97
    label "pewnie"
  ]
  node [
    id 98
    label "zdecydowany"
  ]
  node [
    id 99
    label "bezproblemowy"
  ]
  node [
    id 100
    label "wydarzenie"
  ]
  node [
    id 101
    label "activity"
  ]
  node [
    id 102
    label "campaign"
  ]
  node [
    id 103
    label "causing"
  ]
  node [
    id 104
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 105
    label "narobienie"
  ]
  node [
    id 106
    label "porobienie"
  ]
  node [
    id 107
    label "creation"
  ]
  node [
    id 108
    label "management"
  ]
  node [
    id 109
    label "resolution"
  ]
  node [
    id 110
    label "dokument"
  ]
  node [
    id 111
    label "wytw&#243;r"
  ]
  node [
    id 112
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 113
    label "exploitation"
  ]
  node [
    id 114
    label "okop"
  ]
  node [
    id 115
    label "palisada"
  ]
  node [
    id 116
    label "przedbramie"
  ]
  node [
    id 117
    label "fort"
  ]
  node [
    id 118
    label "zabezpieczenie"
  ]
  node [
    id 119
    label "umacnia&#263;"
  ]
  node [
    id 120
    label "transzeja"
  ]
  node [
    id 121
    label "trwa&#322;y"
  ]
  node [
    id 122
    label "umocni&#263;"
  ]
  node [
    id 123
    label "barykada"
  ]
  node [
    id 124
    label "machiku&#322;"
  ]
  node [
    id 125
    label "szaniec"
  ]
  node [
    id 126
    label "zamek"
  ]
  node [
    id 127
    label "&#347;r&#243;dszaniec"
  ]
  node [
    id 128
    label "fosa"
  ]
  node [
    id 129
    label "baszta"
  ]
  node [
    id 130
    label "umacnianie"
  ]
  node [
    id 131
    label "utrwalenie"
  ]
  node [
    id 132
    label "kazamata"
  ]
  node [
    id 133
    label "bastion"
  ]
  node [
    id 134
    label "confirmation"
  ]
  node [
    id 135
    label "kurtyna"
  ]
  node [
    id 136
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 137
    label "obszar"
  ]
  node [
    id 138
    label "obrona_strefowa"
  ]
  node [
    id 139
    label "Rzym_Zachodni"
  ]
  node [
    id 140
    label "Rzym_Wschodni"
  ]
  node [
    id 141
    label "element"
  ]
  node [
    id 142
    label "ilo&#347;&#263;"
  ]
  node [
    id 143
    label "whole"
  ]
  node [
    id 144
    label "urz&#261;dzenie"
  ]
  node [
    id 145
    label "Kosowo"
  ]
  node [
    id 146
    label "zach&#243;d"
  ]
  node [
    id 147
    label "Zabu&#380;e"
  ]
  node [
    id 148
    label "wymiar"
  ]
  node [
    id 149
    label "antroposfera"
  ]
  node [
    id 150
    label "Arktyka"
  ]
  node [
    id 151
    label "Notogea"
  ]
  node [
    id 152
    label "przestrze&#324;"
  ]
  node [
    id 153
    label "Piotrowo"
  ]
  node [
    id 154
    label "zbi&#243;r"
  ]
  node [
    id 155
    label "akrecja"
  ]
  node [
    id 156
    label "zakres"
  ]
  node [
    id 157
    label "Ruda_Pabianicka"
  ]
  node [
    id 158
    label "Ludwin&#243;w"
  ]
  node [
    id 159
    label "po&#322;udnie"
  ]
  node [
    id 160
    label "miejsce"
  ]
  node [
    id 161
    label "wsch&#243;d"
  ]
  node [
    id 162
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 163
    label "Pow&#261;zki"
  ]
  node [
    id 164
    label "&#321;&#281;g"
  ]
  node [
    id 165
    label "p&#243;&#322;noc"
  ]
  node [
    id 166
    label "Rakowice"
  ]
  node [
    id 167
    label "Syberia_Wschodnia"
  ]
  node [
    id 168
    label "Zab&#322;ocie"
  ]
  node [
    id 169
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 170
    label "Kresy_Zachodnie"
  ]
  node [
    id 171
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 172
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 173
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 174
    label "holarktyka"
  ]
  node [
    id 175
    label "terytorium"
  ]
  node [
    id 176
    label "Antarktyka"
  ]
  node [
    id 177
    label "pas_planetoid"
  ]
  node [
    id 178
    label "Syberia_Zachodnia"
  ]
  node [
    id 179
    label "Neogea"
  ]
  node [
    id 180
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 181
    label "Olszanica"
  ]
  node [
    id 182
    label "tarcza"
  ]
  node [
    id 183
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 184
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 185
    label "borowiec"
  ]
  node [
    id 186
    label "obstawienie"
  ]
  node [
    id 187
    label "chemical_bond"
  ]
  node [
    id 188
    label "obiekt"
  ]
  node [
    id 189
    label "formacja"
  ]
  node [
    id 190
    label "obstawianie"
  ]
  node [
    id 191
    label "transportacja"
  ]
  node [
    id 192
    label "obstawia&#263;"
  ]
  node [
    id 193
    label "ubezpieczenie"
  ]
  node [
    id 194
    label "poj&#281;cie"
  ]
  node [
    id 195
    label "rzecz"
  ]
  node [
    id 196
    label "thing"
  ]
  node [
    id 197
    label "co&#347;"
  ]
  node [
    id 198
    label "budynek"
  ]
  node [
    id 199
    label "program"
  ]
  node [
    id 200
    label "strona"
  ]
  node [
    id 201
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 202
    label "The_Beatles"
  ]
  node [
    id 203
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 204
    label "AWS"
  ]
  node [
    id 205
    label "partia"
  ]
  node [
    id 206
    label "Mazowsze"
  ]
  node [
    id 207
    label "forma"
  ]
  node [
    id 208
    label "ZChN"
  ]
  node [
    id 209
    label "Bund"
  ]
  node [
    id 210
    label "PPR"
  ]
  node [
    id 211
    label "blok"
  ]
  node [
    id 212
    label "egzekutywa"
  ]
  node [
    id 213
    label "Wigowie"
  ]
  node [
    id 214
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 215
    label "Razem"
  ]
  node [
    id 216
    label "unit"
  ]
  node [
    id 217
    label "rocznik"
  ]
  node [
    id 218
    label "SLD"
  ]
  node [
    id 219
    label "ZSL"
  ]
  node [
    id 220
    label "szko&#322;a"
  ]
  node [
    id 221
    label "leksem"
  ]
  node [
    id 222
    label "posta&#263;"
  ]
  node [
    id 223
    label "Kuomintang"
  ]
  node [
    id 224
    label "si&#322;a"
  ]
  node [
    id 225
    label "PiS"
  ]
  node [
    id 226
    label "Depeche_Mode"
  ]
  node [
    id 227
    label "zjawisko"
  ]
  node [
    id 228
    label "Jakobici"
  ]
  node [
    id 229
    label "rugby"
  ]
  node [
    id 230
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 231
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 232
    label "organizacja"
  ]
  node [
    id 233
    label "PO"
  ]
  node [
    id 234
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 235
    label "jednostka"
  ]
  node [
    id 236
    label "proces"
  ]
  node [
    id 237
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 238
    label "Federali&#347;ci"
  ]
  node [
    id 239
    label "zespolik"
  ]
  node [
    id 240
    label "wojsko"
  ]
  node [
    id 241
    label "PSL"
  ]
  node [
    id 242
    label "zesp&#243;&#322;"
  ]
  node [
    id 243
    label "bro&#324;"
  ]
  node [
    id 244
    label "telefon"
  ]
  node [
    id 245
    label "bro&#324;_ochronna"
  ]
  node [
    id 246
    label "przedmiot"
  ]
  node [
    id 247
    label "obro&#324;ca"
  ]
  node [
    id 248
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 249
    label "wskaz&#243;wka"
  ]
  node [
    id 250
    label "powierzchnia"
  ]
  node [
    id 251
    label "naszywka"
  ]
  node [
    id 252
    label "or&#281;&#380;"
  ]
  node [
    id 253
    label "ucze&#324;"
  ]
  node [
    id 254
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 255
    label "target"
  ]
  node [
    id 256
    label "maszyna"
  ]
  node [
    id 257
    label "denture"
  ]
  node [
    id 258
    label "tablica"
  ]
  node [
    id 259
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 260
    label "cel"
  ]
  node [
    id 261
    label "kszta&#322;t"
  ]
  node [
    id 262
    label "odznaka"
  ]
  node [
    id 263
    label "oddzia&#322;"
  ]
  node [
    id 264
    label "suma_ubezpieczenia"
  ]
  node [
    id 265
    label "przyznanie"
  ]
  node [
    id 266
    label "franszyza"
  ]
  node [
    id 267
    label "umowa"
  ]
  node [
    id 268
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 269
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 270
    label "cover"
  ]
  node [
    id 271
    label "zapewnienie"
  ]
  node [
    id 272
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 273
    label "op&#322;ata"
  ]
  node [
    id 274
    label "screen"
  ]
  node [
    id 275
    label "uchronienie"
  ]
  node [
    id 276
    label "ubezpieczalnia"
  ]
  node [
    id 277
    label "insurance"
  ]
  node [
    id 278
    label "transport"
  ]
  node [
    id 279
    label "otacza&#263;"
  ]
  node [
    id 280
    label "obejmowa&#263;"
  ]
  node [
    id 281
    label "budowa&#263;"
  ]
  node [
    id 282
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 283
    label "powierza&#263;"
  ]
  node [
    id 284
    label "bramka"
  ]
  node [
    id 285
    label "zastawia&#263;"
  ]
  node [
    id 286
    label "przewidywa&#263;"
  ]
  node [
    id 287
    label "typ"
  ]
  node [
    id 288
    label "wysy&#322;a&#263;"
  ]
  node [
    id 289
    label "ubezpiecza&#263;"
  ]
  node [
    id 290
    label "broni&#263;"
  ]
  node [
    id 291
    label "venture"
  ]
  node [
    id 292
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 293
    label "os&#322;ania&#263;"
  ]
  node [
    id 294
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 295
    label "typowa&#263;"
  ]
  node [
    id 296
    label "frame"
  ]
  node [
    id 297
    label "zajmowa&#263;"
  ]
  node [
    id 298
    label "zapewnia&#263;"
  ]
  node [
    id 299
    label "Irish_pound"
  ]
  node [
    id 300
    label "wytypowanie"
  ]
  node [
    id 301
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 302
    label "za&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 303
    label "otoczenie"
  ]
  node [
    id 304
    label "ubezpieczanie"
  ]
  node [
    id 305
    label "otaczanie"
  ]
  node [
    id 306
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 307
    label "typowanie"
  ]
  node [
    id 308
    label "os&#322;anianie"
  ]
  node [
    id 309
    label "owado&#380;erca"
  ]
  node [
    id 310
    label "pierwiastek"
  ]
  node [
    id 311
    label "mroczkowate"
  ]
  node [
    id 312
    label "nietoperz"
  ]
  node [
    id 313
    label "BOR"
  ]
  node [
    id 314
    label "borowik"
  ]
  node [
    id 315
    label "kozio&#322;ek"
  ]
  node [
    id 316
    label "ochroniarz"
  ]
  node [
    id 317
    label "duch"
  ]
  node [
    id 318
    label "borowce"
  ]
  node [
    id 319
    label "funkcjonariusz"
  ]
  node [
    id 320
    label "rama"
  ]
  node [
    id 321
    label "podstawa"
  ]
  node [
    id 322
    label "pojazd_szynowy"
  ]
  node [
    id 323
    label "siedlisko"
  ]
  node [
    id 324
    label "podpora"
  ]
  node [
    id 325
    label "rz&#261;d"
  ]
  node [
    id 326
    label "uwaga"
  ]
  node [
    id 327
    label "praca"
  ]
  node [
    id 328
    label "plac"
  ]
  node [
    id 329
    label "location"
  ]
  node [
    id 330
    label "warunek_lokalowy"
  ]
  node [
    id 331
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 332
    label "cia&#322;o"
  ]
  node [
    id 333
    label "status"
  ]
  node [
    id 334
    label "chwila"
  ]
  node [
    id 335
    label "za&#322;o&#380;enie"
  ]
  node [
    id 336
    label "struktura"
  ]
  node [
    id 337
    label "postawa"
  ]
  node [
    id 338
    label "paczka"
  ]
  node [
    id 339
    label "dodatek"
  ]
  node [
    id 340
    label "stela&#380;"
  ]
  node [
    id 341
    label "human_body"
  ]
  node [
    id 342
    label "oprawa"
  ]
  node [
    id 343
    label "element_konstrukcyjny"
  ]
  node [
    id 344
    label "pojazd"
  ]
  node [
    id 345
    label "szablon"
  ]
  node [
    id 346
    label "obramowanie"
  ]
  node [
    id 347
    label "column"
  ]
  node [
    id 348
    label "nadzieja"
  ]
  node [
    id 349
    label "skupisko"
  ]
  node [
    id 350
    label "o&#347;rodek"
  ]
  node [
    id 351
    label "mikrosiedlisko"
  ]
  node [
    id 352
    label "sadowisko"
  ]
  node [
    id 353
    label "strategia"
  ]
  node [
    id 354
    label "background"
  ]
  node [
    id 355
    label "punkt_odniesienia"
  ]
  node [
    id 356
    label "zasadzenie"
  ]
  node [
    id 357
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 358
    label "&#347;ciana"
  ]
  node [
    id 359
    label "podstawowy"
  ]
  node [
    id 360
    label "dzieci&#281;ctwo"
  ]
  node [
    id 361
    label "d&#243;&#322;"
  ]
  node [
    id 362
    label "documentation"
  ]
  node [
    id 363
    label "bok"
  ]
  node [
    id 364
    label "pomys&#322;"
  ]
  node [
    id 365
    label "zasadzi&#263;"
  ]
  node [
    id 366
    label "pot&#281;ga"
  ]
  node [
    id 367
    label "postawi&#263;"
  ]
  node [
    id 368
    label "awansowa&#263;"
  ]
  node [
    id 369
    label "wakowa&#263;"
  ]
  node [
    id 370
    label "uprawianie"
  ]
  node [
    id 371
    label "powierzanie"
  ]
  node [
    id 372
    label "po&#322;o&#380;enie"
  ]
  node [
    id 373
    label "pogl&#261;d"
  ]
  node [
    id 374
    label "awansowanie"
  ]
  node [
    id 375
    label "stawia&#263;"
  ]
  node [
    id 376
    label "obiekt_matematyczny"
  ]
  node [
    id 377
    label "stopie&#324;_pisma"
  ]
  node [
    id 378
    label "pozycja"
  ]
  node [
    id 379
    label "problemat"
  ]
  node [
    id 380
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 381
    label "point"
  ]
  node [
    id 382
    label "plamka"
  ]
  node [
    id 383
    label "mark"
  ]
  node [
    id 384
    label "ust&#281;p"
  ]
  node [
    id 385
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 386
    label "kres"
  ]
  node [
    id 387
    label "plan"
  ]
  node [
    id 388
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 389
    label "podpunkt"
  ]
  node [
    id 390
    label "sprawa"
  ]
  node [
    id 391
    label "problematyka"
  ]
  node [
    id 392
    label "prosta"
  ]
  node [
    id 393
    label "zapunktowa&#263;"
  ]
  node [
    id 394
    label "reading"
  ]
  node [
    id 395
    label "trim"
  ]
  node [
    id 396
    label "pora&#380;ka"
  ]
  node [
    id 397
    label "zbudowanie"
  ]
  node [
    id 398
    label "ugoszczenie"
  ]
  node [
    id 399
    label "sytuacja"
  ]
  node [
    id 400
    label "pouk&#322;adanie"
  ]
  node [
    id 401
    label "ustawienie"
  ]
  node [
    id 402
    label "le&#380;e&#263;"
  ]
  node [
    id 403
    label "wygranie"
  ]
  node [
    id 404
    label "le&#380;enie"
  ]
  node [
    id 405
    label "presentation"
  ]
  node [
    id 406
    label "przenocowanie"
  ]
  node [
    id 407
    label "adres"
  ]
  node [
    id 408
    label "zepsucie"
  ]
  node [
    id 409
    label "zabicie"
  ]
  node [
    id 410
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 411
    label "umieszczenie"
  ]
  node [
    id 412
    label "pokrycie"
  ]
  node [
    id 413
    label "nak&#322;adzenie"
  ]
  node [
    id 414
    label "zaw&#243;d"
  ]
  node [
    id 415
    label "zmiana"
  ]
  node [
    id 416
    label "pracowanie"
  ]
  node [
    id 417
    label "pracowa&#263;"
  ]
  node [
    id 418
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 419
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 420
    label "czynnik_produkcji"
  ]
  node [
    id 421
    label "stosunek_pracy"
  ]
  node [
    id 422
    label "kierownictwo"
  ]
  node [
    id 423
    label "najem"
  ]
  node [
    id 424
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 425
    label "siedziba"
  ]
  node [
    id 426
    label "zak&#322;ad"
  ]
  node [
    id 427
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 428
    label "tynkarski"
  ]
  node [
    id 429
    label "tyrka"
  ]
  node [
    id 430
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 431
    label "benedykty&#324;ski"
  ]
  node [
    id 432
    label "poda&#380;_pracy"
  ]
  node [
    id 433
    label "zobowi&#261;zanie"
  ]
  node [
    id 434
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 435
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 436
    label "teoria_Arrheniusa"
  ]
  node [
    id 437
    label "belief"
  ]
  node [
    id 438
    label "zderzenie_si&#281;"
  ]
  node [
    id 439
    label "s&#261;d"
  ]
  node [
    id 440
    label "teologicznie"
  ]
  node [
    id 441
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 442
    label "rezerwa"
  ]
  node [
    id 443
    label "werbowanie_si&#281;"
  ]
  node [
    id 444
    label "Eurokorpus"
  ]
  node [
    id 445
    label "Armia_Czerwona"
  ]
  node [
    id 446
    label "ods&#322;ugiwanie"
  ]
  node [
    id 447
    label "dryl"
  ]
  node [
    id 448
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 449
    label "fala"
  ]
  node [
    id 450
    label "potencja"
  ]
  node [
    id 451
    label "soldateska"
  ]
  node [
    id 452
    label "pospolite_ruszenie"
  ]
  node [
    id 453
    label "mobilizowa&#263;"
  ]
  node [
    id 454
    label "mobilizowanie"
  ]
  node [
    id 455
    label "tabor"
  ]
  node [
    id 456
    label "zrejterowanie"
  ]
  node [
    id 457
    label "dezerter"
  ]
  node [
    id 458
    label "s&#322;u&#380;ba"
  ]
  node [
    id 459
    label "korpus"
  ]
  node [
    id 460
    label "zdemobilizowanie"
  ]
  node [
    id 461
    label "petarda"
  ]
  node [
    id 462
    label "zmobilizowanie"
  ]
  node [
    id 463
    label "wojo"
  ]
  node [
    id 464
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 465
    label "oddzia&#322;_karny"
  ]
  node [
    id 466
    label "obrona"
  ]
  node [
    id 467
    label "Armia_Krajowa"
  ]
  node [
    id 468
    label "wermacht"
  ]
  node [
    id 469
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 470
    label "Czerwona_Gwardia"
  ]
  node [
    id 471
    label "sztabslekarz"
  ]
  node [
    id 472
    label "zmobilizowa&#263;"
  ]
  node [
    id 473
    label "Legia_Cudzoziemska"
  ]
  node [
    id 474
    label "cofni&#281;cie"
  ]
  node [
    id 475
    label "zrejterowa&#263;"
  ]
  node [
    id 476
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 477
    label "zdemobilizowa&#263;"
  ]
  node [
    id 478
    label "rejterowa&#263;"
  ]
  node [
    id 479
    label "rejterowanie"
  ]
  node [
    id 480
    label "oddawanie"
  ]
  node [
    id 481
    label "wyznawanie"
  ]
  node [
    id 482
    label "ufanie"
  ]
  node [
    id 483
    label "zadanie"
  ]
  node [
    id 484
    label "zlecanie"
  ]
  node [
    id 485
    label "habilitowanie_si&#281;"
  ]
  node [
    id 486
    label "przeniesienie"
  ]
  node [
    id 487
    label "przechodzenie"
  ]
  node [
    id 488
    label "przenoszenie"
  ]
  node [
    id 489
    label "kariera"
  ]
  node [
    id 490
    label "pozyskanie"
  ]
  node [
    id 491
    label "pozyskiwanie"
  ]
  node [
    id 492
    label "obj&#281;cie"
  ]
  node [
    id 493
    label "pi&#281;cie_si&#281;"
  ]
  node [
    id 494
    label "obejmowanie"
  ]
  node [
    id 495
    label "promowanie"
  ]
  node [
    id 496
    label "przej&#347;cie"
  ]
  node [
    id 497
    label "post"
  ]
  node [
    id 498
    label "zmieni&#263;"
  ]
  node [
    id 499
    label "oceni&#263;"
  ]
  node [
    id 500
    label "wydoby&#263;"
  ]
  node [
    id 501
    label "pozostawi&#263;"
  ]
  node [
    id 502
    label "establish"
  ]
  node [
    id 503
    label "umie&#347;ci&#263;"
  ]
  node [
    id 504
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 505
    label "plant"
  ]
  node [
    id 506
    label "zafundowa&#263;"
  ]
  node [
    id 507
    label "budowla"
  ]
  node [
    id 508
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 509
    label "obra&#263;"
  ]
  node [
    id 510
    label "uczyni&#263;"
  ]
  node [
    id 511
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 512
    label "spowodowa&#263;"
  ]
  node [
    id 513
    label "wskaza&#263;"
  ]
  node [
    id 514
    label "peddle"
  ]
  node [
    id 515
    label "obstawi&#263;"
  ]
  node [
    id 516
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 517
    label "znak"
  ]
  node [
    id 518
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 519
    label "wytworzy&#263;"
  ]
  node [
    id 520
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 521
    label "set"
  ]
  node [
    id 522
    label "uruchomi&#263;"
  ]
  node [
    id 523
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 524
    label "wyda&#263;"
  ]
  node [
    id 525
    label "przyzna&#263;"
  ]
  node [
    id 526
    label "stawi&#263;"
  ]
  node [
    id 527
    label "wyznaczy&#263;"
  ]
  node [
    id 528
    label "przedstawi&#263;"
  ]
  node [
    id 529
    label "wyznacza&#263;"
  ]
  node [
    id 530
    label "introduce"
  ]
  node [
    id 531
    label "raise"
  ]
  node [
    id 532
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 533
    label "umieszcza&#263;"
  ]
  node [
    id 534
    label "ocenia&#263;"
  ]
  node [
    id 535
    label "pozostawia&#263;"
  ]
  node [
    id 536
    label "wytwarza&#263;"
  ]
  node [
    id 537
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 538
    label "go"
  ]
  node [
    id 539
    label "przedstawia&#263;"
  ]
  node [
    id 540
    label "czyni&#263;"
  ]
  node [
    id 541
    label "wydawa&#263;"
  ]
  node [
    id 542
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 543
    label "fundowa&#263;"
  ]
  node [
    id 544
    label "zmienia&#263;"
  ]
  node [
    id 545
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 546
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 547
    label "uruchamia&#263;"
  ]
  node [
    id 548
    label "powodowa&#263;"
  ]
  node [
    id 549
    label "deliver"
  ]
  node [
    id 550
    label "przyznawa&#263;"
  ]
  node [
    id 551
    label "wskazywa&#263;"
  ]
  node [
    id 552
    label "wydobywa&#263;"
  ]
  node [
    id 553
    label "wolny"
  ]
  node [
    id 554
    label "by&#263;"
  ]
  node [
    id 555
    label "obj&#261;&#263;"
  ]
  node [
    id 556
    label "pozyska&#263;"
  ]
  node [
    id 557
    label "pozyskiwa&#263;"
  ]
  node [
    id 558
    label "pi&#261;&#263;_si&#281;"
  ]
  node [
    id 559
    label "da&#263;_awans"
  ]
  node [
    id 560
    label "przechodzi&#263;"
  ]
  node [
    id 561
    label "dawa&#263;_awans"
  ]
  node [
    id 562
    label "przej&#347;&#263;"
  ]
  node [
    id 563
    label "culture"
  ]
  node [
    id 564
    label "szczepienie"
  ]
  node [
    id 565
    label "pielenie"
  ]
  node [
    id 566
    label "obrabianie"
  ]
  node [
    id 567
    label "oprysk"
  ]
  node [
    id 568
    label "koszenie"
  ]
  node [
    id 569
    label "siew"
  ]
  node [
    id 570
    label "zajmowanie_si&#281;"
  ]
  node [
    id 571
    label "sadzenie"
  ]
  node [
    id 572
    label "sianie"
  ]
  node [
    id 573
    label "rolnictwo"
  ]
  node [
    id 574
    label "use"
  ]
  node [
    id 575
    label "exercise"
  ]
  node [
    id 576
    label "hodowanie"
  ]
  node [
    id 577
    label "biotechnika"
  ]
  node [
    id 578
    label "orka"
  ]
  node [
    id 579
    label "unia"
  ]
  node [
    id 580
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 579
    target 580
  ]
]
