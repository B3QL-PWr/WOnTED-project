graph [
  node [
    id 0
    label "siebie"
    origin "text"
  ]
  node [
    id 1
    label "karta"
    origin "text"
  ]
  node [
    id 2
    label "zrobi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kurfa"
    origin "text"
  ]
  node [
    id 4
    label "kartka"
  ]
  node [
    id 5
    label "danie"
  ]
  node [
    id 6
    label "menu"
  ]
  node [
    id 7
    label "zezwolenie"
  ]
  node [
    id 8
    label "restauracja"
  ]
  node [
    id 9
    label "chart"
  ]
  node [
    id 10
    label "p&#322;ytka"
  ]
  node [
    id 11
    label "formularz"
  ]
  node [
    id 12
    label "ticket"
  ]
  node [
    id 13
    label "cennik"
  ]
  node [
    id 14
    label "oferta"
  ]
  node [
    id 15
    label "komputer"
  ]
  node [
    id 16
    label "charter"
  ]
  node [
    id 17
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 18
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 19
    label "kartonik"
  ]
  node [
    id 20
    label "urz&#261;dzenie"
  ]
  node [
    id 21
    label "circuit_board"
  ]
  node [
    id 22
    label "offer"
  ]
  node [
    id 23
    label "propozycja"
  ]
  node [
    id 24
    label "spis"
  ]
  node [
    id 25
    label "duty"
  ]
  node [
    id 26
    label "decyzja"
  ]
  node [
    id 27
    label "wiedza"
  ]
  node [
    id 28
    label "zwalnianie_si&#281;"
  ]
  node [
    id 29
    label "authorization"
  ]
  node [
    id 30
    label "zwolnienie_si&#281;"
  ]
  node [
    id 31
    label "pozwole&#324;stwo"
  ]
  node [
    id 32
    label "odwieszenie"
  ]
  node [
    id 33
    label "odpowied&#378;"
  ]
  node [
    id 34
    label "pofolgowanie"
  ]
  node [
    id 35
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 36
    label "license"
  ]
  node [
    id 37
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 38
    label "dokument"
  ]
  node [
    id 39
    label "uznanie"
  ]
  node [
    id 40
    label "zrobienie"
  ]
  node [
    id 41
    label "blaszka"
  ]
  node [
    id 42
    label "plate"
  ]
  node [
    id 43
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 44
    label "p&#322;yta"
  ]
  node [
    id 45
    label "dysk_optyczny"
  ]
  node [
    id 46
    label "przedmiot"
  ]
  node [
    id 47
    label "kom&#243;rka"
  ]
  node [
    id 48
    label "furnishing"
  ]
  node [
    id 49
    label "zabezpieczenie"
  ]
  node [
    id 50
    label "wyrz&#261;dzenie"
  ]
  node [
    id 51
    label "zagospodarowanie"
  ]
  node [
    id 52
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 53
    label "ig&#322;a"
  ]
  node [
    id 54
    label "narz&#281;dzie"
  ]
  node [
    id 55
    label "wirnik"
  ]
  node [
    id 56
    label "aparatura"
  ]
  node [
    id 57
    label "system_energetyczny"
  ]
  node [
    id 58
    label "impulsator"
  ]
  node [
    id 59
    label "mechanizm"
  ]
  node [
    id 60
    label "sprz&#281;t"
  ]
  node [
    id 61
    label "czynno&#347;&#263;"
  ]
  node [
    id 62
    label "blokowanie"
  ]
  node [
    id 63
    label "set"
  ]
  node [
    id 64
    label "zablokowanie"
  ]
  node [
    id 65
    label "przygotowanie"
  ]
  node [
    id 66
    label "komora"
  ]
  node [
    id 67
    label "j&#281;zyk"
  ]
  node [
    id 68
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 69
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 70
    label "wytw&#243;r"
  ]
  node [
    id 71
    label "zestaw"
  ]
  node [
    id 72
    label "formu&#322;a"
  ]
  node [
    id 73
    label "faul"
  ]
  node [
    id 74
    label "wk&#322;ad"
  ]
  node [
    id 75
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 76
    label "s&#281;dzia"
  ]
  node [
    id 77
    label "bon"
  ]
  node [
    id 78
    label "arkusz"
  ]
  node [
    id 79
    label "kara"
  ]
  node [
    id 80
    label "strona"
  ]
  node [
    id 81
    label "gastronomia"
  ]
  node [
    id 82
    label "zak&#322;ad"
  ]
  node [
    id 83
    label "konsument"
  ]
  node [
    id 84
    label "naprawa"
  ]
  node [
    id 85
    label "powr&#243;t"
  ]
  node [
    id 86
    label "pikolak"
  ]
  node [
    id 87
    label "go&#347;&#263;"
  ]
  node [
    id 88
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 89
    label "spos&#243;b"
  ]
  node [
    id 90
    label "program"
  ]
  node [
    id 91
    label "obiecanie"
  ]
  node [
    id 92
    label "zap&#322;acenie"
  ]
  node [
    id 93
    label "cios"
  ]
  node [
    id 94
    label "give"
  ]
  node [
    id 95
    label "udost&#281;pnienie"
  ]
  node [
    id 96
    label "rendition"
  ]
  node [
    id 97
    label "wymienienie_si&#281;"
  ]
  node [
    id 98
    label "eating"
  ]
  node [
    id 99
    label "coup"
  ]
  node [
    id 100
    label "hand"
  ]
  node [
    id 101
    label "uprawianie_seksu"
  ]
  node [
    id 102
    label "allow"
  ]
  node [
    id 103
    label "dostarczenie"
  ]
  node [
    id 104
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 105
    label "uderzenie"
  ]
  node [
    id 106
    label "zadanie"
  ]
  node [
    id 107
    label "powierzenie"
  ]
  node [
    id 108
    label "przeznaczenie"
  ]
  node [
    id 109
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 110
    label "przekazanie"
  ]
  node [
    id 111
    label "odst&#261;pienie"
  ]
  node [
    id 112
    label "dodanie"
  ]
  node [
    id 113
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 114
    label "wyposa&#380;enie"
  ]
  node [
    id 115
    label "dostanie"
  ]
  node [
    id 116
    label "potrawa"
  ]
  node [
    id 117
    label "pass"
  ]
  node [
    id 118
    label "uderzanie"
  ]
  node [
    id 119
    label "wyst&#261;pienie"
  ]
  node [
    id 120
    label "jedzenie"
  ]
  node [
    id 121
    label "wyposa&#380;anie"
  ]
  node [
    id 122
    label "pobicie"
  ]
  node [
    id 123
    label "posi&#322;ek"
  ]
  node [
    id 124
    label "pies_go&#324;czy"
  ]
  node [
    id 125
    label "polownik"
  ]
  node [
    id 126
    label "greyhound"
  ]
  node [
    id 127
    label "pies_wy&#347;cigowy"
  ]
  node [
    id 128
    label "szczwacz"
  ]
  node [
    id 129
    label "odnaj&#281;cie"
  ]
  node [
    id 130
    label "naj&#281;cie"
  ]
  node [
    id 131
    label "stacja_dysk&#243;w"
  ]
  node [
    id 132
    label "instalowa&#263;"
  ]
  node [
    id 133
    label "moc_obliczeniowa"
  ]
  node [
    id 134
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 135
    label "pad"
  ]
  node [
    id 136
    label "modem"
  ]
  node [
    id 137
    label "pami&#281;&#263;"
  ]
  node [
    id 138
    label "monitor"
  ]
  node [
    id 139
    label "zainstalowanie"
  ]
  node [
    id 140
    label "emulacja"
  ]
  node [
    id 141
    label "zainstalowa&#263;"
  ]
  node [
    id 142
    label "procesor"
  ]
  node [
    id 143
    label "maszyna_Turinga"
  ]
  node [
    id 144
    label "twardy_dysk"
  ]
  node [
    id 145
    label "klawiatura"
  ]
  node [
    id 146
    label "botnet"
  ]
  node [
    id 147
    label "mysz"
  ]
  node [
    id 148
    label "instalowanie"
  ]
  node [
    id 149
    label "radiator"
  ]
  node [
    id 150
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 2
    target 3
  ]
]
