graph [
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "przybywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "singel"
    origin "text"
  ]
  node [
    id 3
    label "dociera&#263;"
  ]
  node [
    id 4
    label "get"
  ]
  node [
    id 5
    label "zyskiwa&#263;"
  ]
  node [
    id 6
    label "poprawia&#263;_si&#281;"
  ]
  node [
    id 7
    label "nabywa&#263;"
  ]
  node [
    id 8
    label "uzyskiwa&#263;"
  ]
  node [
    id 9
    label "pozyskiwa&#263;"
  ]
  node [
    id 10
    label "use"
  ]
  node [
    id 11
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 12
    label "silnik"
  ]
  node [
    id 13
    label "dopasowywa&#263;"
  ]
  node [
    id 14
    label "g&#322;adzi&#263;"
  ]
  node [
    id 15
    label "boost"
  ]
  node [
    id 16
    label "dorabia&#263;"
  ]
  node [
    id 17
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 18
    label "trze&#263;"
  ]
  node [
    id 19
    label "znajdowa&#263;"
  ]
  node [
    id 20
    label "piosenka"
  ]
  node [
    id 21
    label "p&#322;yta"
  ]
  node [
    id 22
    label "stan_wolny"
  ]
  node [
    id 23
    label "nie&#380;onaty"
  ]
  node [
    id 24
    label "singiel"
  ]
  node [
    id 25
    label "mecz"
  ]
  node [
    id 26
    label "karta"
  ]
  node [
    id 27
    label "kartka"
  ]
  node [
    id 28
    label "danie"
  ]
  node [
    id 29
    label "menu"
  ]
  node [
    id 30
    label "zezwolenie"
  ]
  node [
    id 31
    label "restauracja"
  ]
  node [
    id 32
    label "chart"
  ]
  node [
    id 33
    label "p&#322;ytka"
  ]
  node [
    id 34
    label "formularz"
  ]
  node [
    id 35
    label "ticket"
  ]
  node [
    id 36
    label "cennik"
  ]
  node [
    id 37
    label "oferta"
  ]
  node [
    id 38
    label "komputer"
  ]
  node [
    id 39
    label "charter"
  ]
  node [
    id 40
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 41
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 42
    label "kartonik"
  ]
  node [
    id 43
    label "urz&#261;dzenie"
  ]
  node [
    id 44
    label "circuit_board"
  ]
  node [
    id 45
    label "zanuci&#263;"
  ]
  node [
    id 46
    label "nucenie"
  ]
  node [
    id 47
    label "zwrotka"
  ]
  node [
    id 48
    label "utw&#243;r"
  ]
  node [
    id 49
    label "tekst"
  ]
  node [
    id 50
    label "nuci&#263;"
  ]
  node [
    id 51
    label "zanucenie"
  ]
  node [
    id 52
    label "piosnka"
  ]
  node [
    id 53
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 54
    label "kuchnia"
  ]
  node [
    id 55
    label "przedmiot"
  ]
  node [
    id 56
    label "nagranie"
  ]
  node [
    id 57
    label "AGD"
  ]
  node [
    id 58
    label "p&#322;ytoteka"
  ]
  node [
    id 59
    label "no&#347;nik_danych"
  ]
  node [
    id 60
    label "miejsce"
  ]
  node [
    id 61
    label "plate"
  ]
  node [
    id 62
    label "sheet"
  ]
  node [
    id 63
    label "dysk"
  ]
  node [
    id 64
    label "produkcja"
  ]
  node [
    id 65
    label "phonograph_record"
  ]
  node [
    id 66
    label "obrona"
  ]
  node [
    id 67
    label "gra"
  ]
  node [
    id 68
    label "game"
  ]
  node [
    id 69
    label "serw"
  ]
  node [
    id 70
    label "dwumecz"
  ]
  node [
    id 71
    label "samotny"
  ]
  node [
    id 72
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 73
    label "pan_m&#322;ody"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
]
