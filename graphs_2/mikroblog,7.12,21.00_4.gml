graph [
  node [
    id 0
    label "bekazlewactwa"
    origin "text"
  ]
  node [
    id 1
    label "xddd"
    origin "text"
  ]
  node [
    id 2
    label "podprogowy"
    origin "text"
  ]
  node [
    id 3
    label "przekaz"
    origin "text"
  ]
  node [
    id 4
    label "podprogowo"
  ]
  node [
    id 5
    label "pod&#347;wiadomy"
  ]
  node [
    id 6
    label "pod&#347;wiadomie"
  ]
  node [
    id 7
    label "kwota"
  ]
  node [
    id 8
    label "explicite"
  ]
  node [
    id 9
    label "proces"
  ]
  node [
    id 10
    label "blankiet"
  ]
  node [
    id 11
    label "znaczenie"
  ]
  node [
    id 12
    label "draft"
  ]
  node [
    id 13
    label "tekst"
  ]
  node [
    id 14
    label "transakcja"
  ]
  node [
    id 15
    label "implicite"
  ]
  node [
    id 16
    label "dokument"
  ]
  node [
    id 17
    label "order"
  ]
  node [
    id 18
    label "odk&#322;adanie"
  ]
  node [
    id 19
    label "condition"
  ]
  node [
    id 20
    label "liczenie"
  ]
  node [
    id 21
    label "stawianie"
  ]
  node [
    id 22
    label "bycie"
  ]
  node [
    id 23
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 24
    label "assay"
  ]
  node [
    id 25
    label "wskazywanie"
  ]
  node [
    id 26
    label "wyraz"
  ]
  node [
    id 27
    label "gravity"
  ]
  node [
    id 28
    label "weight"
  ]
  node [
    id 29
    label "command"
  ]
  node [
    id 30
    label "odgrywanie_roli"
  ]
  node [
    id 31
    label "istota"
  ]
  node [
    id 32
    label "informacja"
  ]
  node [
    id 33
    label "cecha"
  ]
  node [
    id 34
    label "okre&#347;lanie"
  ]
  node [
    id 35
    label "kto&#347;"
  ]
  node [
    id 36
    label "wyra&#380;enie"
  ]
  node [
    id 37
    label "zapis"
  ]
  node [
    id 38
    label "&#347;wiadectwo"
  ]
  node [
    id 39
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 40
    label "wytw&#243;r"
  ]
  node [
    id 41
    label "parafa"
  ]
  node [
    id 42
    label "plik"
  ]
  node [
    id 43
    label "raport&#243;wka"
  ]
  node [
    id 44
    label "utw&#243;r"
  ]
  node [
    id 45
    label "record"
  ]
  node [
    id 46
    label "registratura"
  ]
  node [
    id 47
    label "dokumentacja"
  ]
  node [
    id 48
    label "fascyku&#322;"
  ]
  node [
    id 49
    label "artyku&#322;"
  ]
  node [
    id 50
    label "writing"
  ]
  node [
    id 51
    label "sygnatariusz"
  ]
  node [
    id 52
    label "ekscerpcja"
  ]
  node [
    id 53
    label "j&#281;zykowo"
  ]
  node [
    id 54
    label "wypowied&#378;"
  ]
  node [
    id 55
    label "redakcja"
  ]
  node [
    id 56
    label "pomini&#281;cie"
  ]
  node [
    id 57
    label "dzie&#322;o"
  ]
  node [
    id 58
    label "preparacja"
  ]
  node [
    id 59
    label "odmianka"
  ]
  node [
    id 60
    label "opu&#347;ci&#263;"
  ]
  node [
    id 61
    label "koniektura"
  ]
  node [
    id 62
    label "pisa&#263;"
  ]
  node [
    id 63
    label "obelga"
  ]
  node [
    id 64
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 65
    label "zam&#243;wienie"
  ]
  node [
    id 66
    label "cena_transferowa"
  ]
  node [
    id 67
    label "arbitra&#380;"
  ]
  node [
    id 68
    label "kontrakt_terminowy"
  ]
  node [
    id 69
    label "facjenda"
  ]
  node [
    id 70
    label "czynno&#347;&#263;"
  ]
  node [
    id 71
    label "lacuna"
  ]
  node [
    id 72
    label "tabela"
  ]
  node [
    id 73
    label "booklet"
  ]
  node [
    id 74
    label "druk"
  ]
  node [
    id 75
    label "wynie&#347;&#263;"
  ]
  node [
    id 76
    label "pieni&#261;dze"
  ]
  node [
    id 77
    label "ilo&#347;&#263;"
  ]
  node [
    id 78
    label "limit"
  ]
  node [
    id 79
    label "wynosi&#263;"
  ]
  node [
    id 80
    label "kognicja"
  ]
  node [
    id 81
    label "przebieg"
  ]
  node [
    id 82
    label "rozprawa"
  ]
  node [
    id 83
    label "wydarzenie"
  ]
  node [
    id 84
    label "legislacyjnie"
  ]
  node [
    id 85
    label "przes&#322;anka"
  ]
  node [
    id 86
    label "zjawisko"
  ]
  node [
    id 87
    label "nast&#281;pstwo"
  ]
  node [
    id 88
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 89
    label "komunikacja"
  ]
  node [
    id 90
    label "po&#347;rednio"
  ]
  node [
    id 91
    label "bezpo&#347;rednio"
  ]
  node [
    id 92
    label "wyb&#243;r"
  ]
  node [
    id 93
    label "wydruk"
  ]
  node [
    id 94
    label "odznaka"
  ]
  node [
    id 95
    label "kawaler"
  ]
  node [
    id 96
    label "XDDD"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
]
