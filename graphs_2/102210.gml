graph [
  node [
    id 0
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 3
    label "szcz&#281;&#347;liwy"
    origin "text"
  ]
  node [
    id 4
    label "gdybyby&#263;"
    origin "text"
  ]
  node [
    id 5
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 6
    label "da&#263;"
    origin "text"
  ]
  node [
    id 7
    label "znak"
    origin "text"
  ]
  node [
    id 8
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 9
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 10
    label "ziemia"
    origin "text"
  ]
  node [
    id 11
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "tak"
    origin "text"
  ]
  node [
    id 14
    label "niewiele"
    origin "text"
  ]
  node [
    id 15
    label "byby&#263;"
    origin "text"
  ]
  node [
    id 16
    label "pragn&#261;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "uczyni&#263;"
    origin "text"
  ]
  node [
    id 18
    label "oddawa&#263;"
  ]
  node [
    id 19
    label "zalicza&#263;"
  ]
  node [
    id 20
    label "render"
  ]
  node [
    id 21
    label "sk&#322;ada&#263;"
  ]
  node [
    id 22
    label "bequeath"
  ]
  node [
    id 23
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 24
    label "zostawia&#263;"
  ]
  node [
    id 25
    label "powierza&#263;"
  ]
  node [
    id 26
    label "opowiada&#263;"
  ]
  node [
    id 27
    label "convey"
  ]
  node [
    id 28
    label "impart"
  ]
  node [
    id 29
    label "przekazywa&#263;"
  ]
  node [
    id 30
    label "dostarcza&#263;"
  ]
  node [
    id 31
    label "sacrifice"
  ]
  node [
    id 32
    label "dawa&#263;"
  ]
  node [
    id 33
    label "odst&#281;powa&#263;"
  ]
  node [
    id 34
    label "sprzedawa&#263;"
  ]
  node [
    id 35
    label "give"
  ]
  node [
    id 36
    label "reflect"
  ]
  node [
    id 37
    label "surrender"
  ]
  node [
    id 38
    label "deliver"
  ]
  node [
    id 39
    label "odpowiada&#263;"
  ]
  node [
    id 40
    label "umieszcza&#263;"
  ]
  node [
    id 41
    label "blurt_out"
  ]
  node [
    id 42
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 43
    label "przedstawia&#263;"
  ]
  node [
    id 44
    label "yield"
  ]
  node [
    id 45
    label "robi&#263;"
  ]
  node [
    id 46
    label "opuszcza&#263;"
  ]
  node [
    id 47
    label "g&#243;rowa&#263;"
  ]
  node [
    id 48
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 49
    label "wydawa&#263;"
  ]
  node [
    id 50
    label "tworzy&#263;"
  ]
  node [
    id 51
    label "wk&#322;ada&#263;"
  ]
  node [
    id 52
    label "pomija&#263;"
  ]
  node [
    id 53
    label "doprowadza&#263;"
  ]
  node [
    id 54
    label "zachowywa&#263;"
  ]
  node [
    id 55
    label "zabiera&#263;"
  ]
  node [
    id 56
    label "zamierza&#263;"
  ]
  node [
    id 57
    label "liszy&#263;"
  ]
  node [
    id 58
    label "zrywa&#263;"
  ]
  node [
    id 59
    label "porzuca&#263;"
  ]
  node [
    id 60
    label "base_on_balls"
  ]
  node [
    id 61
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 62
    label "permit"
  ]
  node [
    id 63
    label "powodowa&#263;"
  ]
  node [
    id 64
    label "wyznacza&#263;"
  ]
  node [
    id 65
    label "rezygnowa&#263;"
  ]
  node [
    id 66
    label "krzywdzi&#263;"
  ]
  node [
    id 67
    label "wywi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 68
    label "bra&#263;"
  ]
  node [
    id 69
    label "mark"
  ]
  node [
    id 70
    label "number"
  ]
  node [
    id 71
    label "stwierdza&#263;"
  ]
  node [
    id 72
    label "odb&#281;bnia&#263;"
  ]
  node [
    id 73
    label "wlicza&#263;"
  ]
  node [
    id 74
    label "przyporz&#261;dkowywa&#263;"
  ]
  node [
    id 75
    label "wyznawa&#263;"
  ]
  node [
    id 76
    label "confide"
  ]
  node [
    id 77
    label "zleca&#263;"
  ]
  node [
    id 78
    label "ufa&#263;"
  ]
  node [
    id 79
    label "command"
  ]
  node [
    id 80
    label "grant"
  ]
  node [
    id 81
    label "zaczyna&#263;"
  ]
  node [
    id 82
    label "set_about"
  ]
  node [
    id 83
    label "wchodzi&#263;"
  ]
  node [
    id 84
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 85
    label "submit"
  ]
  node [
    id 86
    label "zbiera&#263;"
  ]
  node [
    id 87
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 88
    label "przywraca&#263;"
  ]
  node [
    id 89
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 90
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 91
    label "publicize"
  ]
  node [
    id 92
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 93
    label "uk&#322;ada&#263;"
  ]
  node [
    id 94
    label "opracowywa&#263;"
  ]
  node [
    id 95
    label "set"
  ]
  node [
    id 96
    label "train"
  ]
  node [
    id 97
    label "zmienia&#263;"
  ]
  node [
    id 98
    label "dzieli&#263;"
  ]
  node [
    id 99
    label "scala&#263;"
  ]
  node [
    id 100
    label "zestaw"
  ]
  node [
    id 101
    label "prawi&#263;"
  ]
  node [
    id 102
    label "relate"
  ]
  node [
    id 103
    label "zadowolony"
  ]
  node [
    id 104
    label "pomy&#347;lny"
  ]
  node [
    id 105
    label "udany"
  ]
  node [
    id 106
    label "szcz&#281;&#347;liwie"
  ]
  node [
    id 107
    label "pe&#322;ny"
  ]
  node [
    id 108
    label "pogodny"
  ]
  node [
    id 109
    label "dobry"
  ]
  node [
    id 110
    label "spokojny"
  ]
  node [
    id 111
    label "&#322;adny"
  ]
  node [
    id 112
    label "pozytywny"
  ]
  node [
    id 113
    label "pogodnie"
  ]
  node [
    id 114
    label "przyjemny"
  ]
  node [
    id 115
    label "udanie"
  ]
  node [
    id 116
    label "fajny"
  ]
  node [
    id 117
    label "po&#380;&#261;dany"
  ]
  node [
    id 118
    label "pomy&#347;lnie"
  ]
  node [
    id 119
    label "zadowolenie_si&#281;"
  ]
  node [
    id 120
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 121
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 122
    label "dobroczynny"
  ]
  node [
    id 123
    label "czw&#243;rka"
  ]
  node [
    id 124
    label "skuteczny"
  ]
  node [
    id 125
    label "&#347;mieszny"
  ]
  node [
    id 126
    label "mi&#322;y"
  ]
  node [
    id 127
    label "grzeczny"
  ]
  node [
    id 128
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 129
    label "powitanie"
  ]
  node [
    id 130
    label "dobrze"
  ]
  node [
    id 131
    label "ca&#322;y"
  ]
  node [
    id 132
    label "zwrot"
  ]
  node [
    id 133
    label "moralny"
  ]
  node [
    id 134
    label "drogi"
  ]
  node [
    id 135
    label "odpowiedni"
  ]
  node [
    id 136
    label "korzystny"
  ]
  node [
    id 137
    label "pos&#322;uszny"
  ]
  node [
    id 138
    label "nieograniczony"
  ]
  node [
    id 139
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 140
    label "satysfakcja"
  ]
  node [
    id 141
    label "bezwzgl&#281;dny"
  ]
  node [
    id 142
    label "otwarty"
  ]
  node [
    id 143
    label "wype&#322;nienie"
  ]
  node [
    id 144
    label "kompletny"
  ]
  node [
    id 145
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 146
    label "pe&#322;no"
  ]
  node [
    id 147
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 148
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 149
    label "zupe&#322;ny"
  ]
  node [
    id 150
    label "r&#243;wny"
  ]
  node [
    id 151
    label "gotowy"
  ]
  node [
    id 152
    label "might"
  ]
  node [
    id 153
    label "uprawi&#263;"
  ]
  node [
    id 154
    label "public_treasury"
  ]
  node [
    id 155
    label "pole"
  ]
  node [
    id 156
    label "obrobi&#263;"
  ]
  node [
    id 157
    label "nietrze&#378;wy"
  ]
  node [
    id 158
    label "czekanie"
  ]
  node [
    id 159
    label "martwy"
  ]
  node [
    id 160
    label "bliski"
  ]
  node [
    id 161
    label "gotowo"
  ]
  node [
    id 162
    label "przygotowywanie"
  ]
  node [
    id 163
    label "przygotowanie"
  ]
  node [
    id 164
    label "dyspozycyjny"
  ]
  node [
    id 165
    label "zalany"
  ]
  node [
    id 166
    label "nieuchronny"
  ]
  node [
    id 167
    label "doj&#347;cie"
  ]
  node [
    id 168
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 169
    label "mie&#263;_miejsce"
  ]
  node [
    id 170
    label "equal"
  ]
  node [
    id 171
    label "trwa&#263;"
  ]
  node [
    id 172
    label "chodzi&#263;"
  ]
  node [
    id 173
    label "si&#281;ga&#263;"
  ]
  node [
    id 174
    label "stan"
  ]
  node [
    id 175
    label "obecno&#347;&#263;"
  ]
  node [
    id 176
    label "stand"
  ]
  node [
    id 177
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 178
    label "uczestniczy&#263;"
  ]
  node [
    id 179
    label "powierzy&#263;"
  ]
  node [
    id 180
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 181
    label "obieca&#263;"
  ]
  node [
    id 182
    label "pozwoli&#263;"
  ]
  node [
    id 183
    label "odst&#261;pi&#263;"
  ]
  node [
    id 184
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 185
    label "przywali&#263;"
  ]
  node [
    id 186
    label "wyrzec_si&#281;"
  ]
  node [
    id 187
    label "sztachn&#261;&#263;"
  ]
  node [
    id 188
    label "rap"
  ]
  node [
    id 189
    label "feed"
  ]
  node [
    id 190
    label "zrobi&#263;"
  ]
  node [
    id 191
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 192
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 193
    label "testify"
  ]
  node [
    id 194
    label "udost&#281;pni&#263;"
  ]
  node [
    id 195
    label "przeznaczy&#263;"
  ]
  node [
    id 196
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 197
    label "picture"
  ]
  node [
    id 198
    label "zada&#263;"
  ]
  node [
    id 199
    label "dress"
  ]
  node [
    id 200
    label "dostarczy&#263;"
  ]
  node [
    id 201
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 202
    label "przekaza&#263;"
  ]
  node [
    id 203
    label "supply"
  ]
  node [
    id 204
    label "doda&#263;"
  ]
  node [
    id 205
    label "zap&#322;aci&#263;"
  ]
  node [
    id 206
    label "wy&#322;oi&#263;"
  ]
  node [
    id 207
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 208
    label "zabuli&#263;"
  ]
  node [
    id 209
    label "wyda&#263;"
  ]
  node [
    id 210
    label "pay"
  ]
  node [
    id 211
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 212
    label "pofolgowa&#263;"
  ]
  node [
    id 213
    label "assent"
  ]
  node [
    id 214
    label "uzna&#263;"
  ]
  node [
    id 215
    label "leave"
  ]
  node [
    id 216
    label "post&#261;pi&#263;"
  ]
  node [
    id 217
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 218
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 219
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 220
    label "zorganizowa&#263;"
  ]
  node [
    id 221
    label "appoint"
  ]
  node [
    id 222
    label "wystylizowa&#263;"
  ]
  node [
    id 223
    label "cause"
  ]
  node [
    id 224
    label "przerobi&#263;"
  ]
  node [
    id 225
    label "nabra&#263;"
  ]
  node [
    id 226
    label "make"
  ]
  node [
    id 227
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 228
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 229
    label "wydali&#263;"
  ]
  node [
    id 230
    label "sta&#263;_si&#281;"
  ]
  node [
    id 231
    label "oblat"
  ]
  node [
    id 232
    label "ustali&#263;"
  ]
  node [
    id 233
    label "wytworzy&#263;"
  ]
  node [
    id 234
    label "spowodowa&#263;"
  ]
  node [
    id 235
    label "open"
  ]
  node [
    id 236
    label "transfer"
  ]
  node [
    id 237
    label "zrzec_si&#281;"
  ]
  node [
    id 238
    label "odsun&#261;&#263;_si&#281;"
  ]
  node [
    id 239
    label "odwr&#243;t"
  ]
  node [
    id 240
    label "wycofa&#263;_si&#281;"
  ]
  node [
    id 241
    label "vow"
  ]
  node [
    id 242
    label "sign"
  ]
  node [
    id 243
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 244
    label "pieprzn&#261;&#263;"
  ]
  node [
    id 245
    label "podrzuci&#263;"
  ]
  node [
    id 246
    label "uderzy&#263;"
  ]
  node [
    id 247
    label "impact"
  ]
  node [
    id 248
    label "dokuczy&#263;"
  ]
  node [
    id 249
    label "overwhelm"
  ]
  node [
    id 250
    label "przyrzn&#261;&#263;"
  ]
  node [
    id 251
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 252
    label "crush"
  ]
  node [
    id 253
    label "przygnie&#347;&#263;"
  ]
  node [
    id 254
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 255
    label "zaszkodzi&#263;"
  ]
  node [
    id 256
    label "put"
  ]
  node [
    id 257
    label "deal"
  ]
  node [
    id 258
    label "zaj&#261;&#263;"
  ]
  node [
    id 259
    label "distribute"
  ]
  node [
    id 260
    label "nakarmi&#263;"
  ]
  node [
    id 261
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 262
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 263
    label "nada&#263;"
  ]
  node [
    id 264
    label "policzy&#263;"
  ]
  node [
    id 265
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 266
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 267
    label "complete"
  ]
  node [
    id 268
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 269
    label "perform"
  ]
  node [
    id 270
    label "wyj&#347;&#263;"
  ]
  node [
    id 271
    label "zrezygnowa&#263;"
  ]
  node [
    id 272
    label "nak&#322;oni&#263;"
  ]
  node [
    id 273
    label "appear"
  ]
  node [
    id 274
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 275
    label "zacz&#261;&#263;"
  ]
  node [
    id 276
    label "happen"
  ]
  node [
    id 277
    label "propagate"
  ]
  node [
    id 278
    label "wp&#322;aci&#263;"
  ]
  node [
    id 279
    label "wys&#322;a&#263;"
  ]
  node [
    id 280
    label "poda&#263;"
  ]
  node [
    id 281
    label "sygna&#322;"
  ]
  node [
    id 282
    label "charge"
  ]
  node [
    id 283
    label "odda&#263;"
  ]
  node [
    id 284
    label "entrust"
  ]
  node [
    id 285
    label "wyzna&#263;"
  ]
  node [
    id 286
    label "zleci&#263;"
  ]
  node [
    id 287
    label "consign"
  ]
  node [
    id 288
    label "muzyka_rozrywkowa"
  ]
  node [
    id 289
    label "karpiowate"
  ]
  node [
    id 290
    label "ryba"
  ]
  node [
    id 291
    label "czarna_muzyka"
  ]
  node [
    id 292
    label "drapie&#380;nik"
  ]
  node [
    id 293
    label "asp"
  ]
  node [
    id 294
    label "accommodate"
  ]
  node [
    id 295
    label "dow&#243;d"
  ]
  node [
    id 296
    label "oznakowanie"
  ]
  node [
    id 297
    label "fakt"
  ]
  node [
    id 298
    label "stawia&#263;"
  ]
  node [
    id 299
    label "wytw&#243;r"
  ]
  node [
    id 300
    label "point"
  ]
  node [
    id 301
    label "kodzik"
  ]
  node [
    id 302
    label "postawi&#263;"
  ]
  node [
    id 303
    label "herb"
  ]
  node [
    id 304
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 305
    label "attribute"
  ]
  node [
    id 306
    label "implikowa&#263;"
  ]
  node [
    id 307
    label "reszta"
  ]
  node [
    id 308
    label "trace"
  ]
  node [
    id 309
    label "obiekt"
  ]
  node [
    id 310
    label "&#347;wiadectwo"
  ]
  node [
    id 311
    label "przedmiot"
  ]
  node [
    id 312
    label "p&#322;&#243;d"
  ]
  node [
    id 313
    label "work"
  ]
  node [
    id 314
    label "rezultat"
  ]
  node [
    id 315
    label "&#347;rodek"
  ]
  node [
    id 316
    label "rewizja"
  ]
  node [
    id 317
    label "certificate"
  ]
  node [
    id 318
    label "argument"
  ]
  node [
    id 319
    label "act"
  ]
  node [
    id 320
    label "forsing"
  ]
  node [
    id 321
    label "rzecz"
  ]
  node [
    id 322
    label "dokument"
  ]
  node [
    id 323
    label "uzasadnienie"
  ]
  node [
    id 324
    label "bia&#322;e_plamy"
  ]
  node [
    id 325
    label "wydarzenie"
  ]
  node [
    id 326
    label "klejnot_herbowy"
  ]
  node [
    id 327
    label "barwy"
  ]
  node [
    id 328
    label "blazonowa&#263;"
  ]
  node [
    id 329
    label "symbol"
  ]
  node [
    id 330
    label "blazonowanie"
  ]
  node [
    id 331
    label "korona_rangowa"
  ]
  node [
    id 332
    label "heraldyka"
  ]
  node [
    id 333
    label "tarcza_herbowa"
  ]
  node [
    id 334
    label "trzymacz"
  ]
  node [
    id 335
    label "marking"
  ]
  node [
    id 336
    label "oznaczenie"
  ]
  node [
    id 337
    label "pozostawia&#263;"
  ]
  node [
    id 338
    label "czyni&#263;"
  ]
  node [
    id 339
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 340
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 341
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 342
    label "raise"
  ]
  node [
    id 343
    label "przewidywa&#263;"
  ]
  node [
    id 344
    label "przyznawa&#263;"
  ]
  node [
    id 345
    label "go"
  ]
  node [
    id 346
    label "obstawia&#263;"
  ]
  node [
    id 347
    label "ocenia&#263;"
  ]
  node [
    id 348
    label "zastawia&#263;"
  ]
  node [
    id 349
    label "stanowisko"
  ]
  node [
    id 350
    label "wskazywa&#263;"
  ]
  node [
    id 351
    label "introduce"
  ]
  node [
    id 352
    label "uruchamia&#263;"
  ]
  node [
    id 353
    label "wytwarza&#263;"
  ]
  node [
    id 354
    label "fundowa&#263;"
  ]
  node [
    id 355
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 356
    label "wydobywa&#263;"
  ]
  node [
    id 357
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 358
    label "zapis"
  ]
  node [
    id 359
    label "zafundowa&#263;"
  ]
  node [
    id 360
    label "budowla"
  ]
  node [
    id 361
    label "plant"
  ]
  node [
    id 362
    label "uruchomi&#263;"
  ]
  node [
    id 363
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 364
    label "pozostawi&#263;"
  ]
  node [
    id 365
    label "obra&#263;"
  ]
  node [
    id 366
    label "peddle"
  ]
  node [
    id 367
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 368
    label "obstawi&#263;"
  ]
  node [
    id 369
    label "zmieni&#263;"
  ]
  node [
    id 370
    label "post"
  ]
  node [
    id 371
    label "wyznaczy&#263;"
  ]
  node [
    id 372
    label "oceni&#263;"
  ]
  node [
    id 373
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 374
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 375
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 376
    label "umie&#347;ci&#263;"
  ]
  node [
    id 377
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 378
    label "wskaza&#263;"
  ]
  node [
    id 379
    label "przyzna&#263;"
  ]
  node [
    id 380
    label "wydoby&#263;"
  ]
  node [
    id 381
    label "przedstawi&#263;"
  ]
  node [
    id 382
    label "establish"
  ]
  node [
    id 383
    label "stawi&#263;"
  ]
  node [
    id 384
    label "oznaka"
  ]
  node [
    id 385
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 386
    label "imply"
  ]
  node [
    id 387
    label "przyzwoity"
  ]
  node [
    id 388
    label "ciekawy"
  ]
  node [
    id 389
    label "jako&#347;"
  ]
  node [
    id 390
    label "jako_tako"
  ]
  node [
    id 391
    label "niez&#322;y"
  ]
  node [
    id 392
    label "dziwny"
  ]
  node [
    id 393
    label "charakterystyczny"
  ]
  node [
    id 394
    label "intensywny"
  ]
  node [
    id 395
    label "udolny"
  ]
  node [
    id 396
    label "niczegowaty"
  ]
  node [
    id 397
    label "nieszpetny"
  ]
  node [
    id 398
    label "spory"
  ]
  node [
    id 399
    label "nie&#378;le"
  ]
  node [
    id 400
    label "kulturalny"
  ]
  node [
    id 401
    label "skromny"
  ]
  node [
    id 402
    label "stosowny"
  ]
  node [
    id 403
    label "przystojny"
  ]
  node [
    id 404
    label "nale&#380;yty"
  ]
  node [
    id 405
    label "przyzwoicie"
  ]
  node [
    id 406
    label "wystarczaj&#261;cy"
  ]
  node [
    id 407
    label "nietuzinkowy"
  ]
  node [
    id 408
    label "intryguj&#261;cy"
  ]
  node [
    id 409
    label "ch&#281;tny"
  ]
  node [
    id 410
    label "swoisty"
  ]
  node [
    id 411
    label "interesowanie"
  ]
  node [
    id 412
    label "interesuj&#261;cy"
  ]
  node [
    id 413
    label "ciekawie"
  ]
  node [
    id 414
    label "indagator"
  ]
  node [
    id 415
    label "charakterystycznie"
  ]
  node [
    id 416
    label "szczeg&#243;lny"
  ]
  node [
    id 417
    label "wyj&#261;tkowy"
  ]
  node [
    id 418
    label "typowy"
  ]
  node [
    id 419
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 420
    label "podobny"
  ]
  node [
    id 421
    label "dziwnie"
  ]
  node [
    id 422
    label "dziwy"
  ]
  node [
    id 423
    label "inny"
  ]
  node [
    id 424
    label "w_miar&#281;"
  ]
  node [
    id 425
    label "jako_taki"
  ]
  node [
    id 426
    label "ludzko&#347;&#263;"
  ]
  node [
    id 427
    label "asymilowanie"
  ]
  node [
    id 428
    label "wapniak"
  ]
  node [
    id 429
    label "asymilowa&#263;"
  ]
  node [
    id 430
    label "os&#322;abia&#263;"
  ]
  node [
    id 431
    label "posta&#263;"
  ]
  node [
    id 432
    label "hominid"
  ]
  node [
    id 433
    label "podw&#322;adny"
  ]
  node [
    id 434
    label "os&#322;abianie"
  ]
  node [
    id 435
    label "g&#322;owa"
  ]
  node [
    id 436
    label "figura"
  ]
  node [
    id 437
    label "portrecista"
  ]
  node [
    id 438
    label "dwun&#243;g"
  ]
  node [
    id 439
    label "profanum"
  ]
  node [
    id 440
    label "mikrokosmos"
  ]
  node [
    id 441
    label "nasada"
  ]
  node [
    id 442
    label "duch"
  ]
  node [
    id 443
    label "antropochoria"
  ]
  node [
    id 444
    label "osoba"
  ]
  node [
    id 445
    label "wz&#243;r"
  ]
  node [
    id 446
    label "senior"
  ]
  node [
    id 447
    label "oddzia&#322;ywanie"
  ]
  node [
    id 448
    label "Adam"
  ]
  node [
    id 449
    label "homo_sapiens"
  ]
  node [
    id 450
    label "polifag"
  ]
  node [
    id 451
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 452
    label "cz&#322;owiekowate"
  ]
  node [
    id 453
    label "konsument"
  ]
  node [
    id 454
    label "istota_&#380;ywa"
  ]
  node [
    id 455
    label "pracownik"
  ]
  node [
    id 456
    label "Chocho&#322;"
  ]
  node [
    id 457
    label "Herkules_Poirot"
  ]
  node [
    id 458
    label "Edyp"
  ]
  node [
    id 459
    label "parali&#380;owa&#263;"
  ]
  node [
    id 460
    label "Harry_Potter"
  ]
  node [
    id 461
    label "Casanova"
  ]
  node [
    id 462
    label "Zgredek"
  ]
  node [
    id 463
    label "Gargantua"
  ]
  node [
    id 464
    label "Winnetou"
  ]
  node [
    id 465
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 466
    label "Dulcynea"
  ]
  node [
    id 467
    label "kategoria_gramatyczna"
  ]
  node [
    id 468
    label "person"
  ]
  node [
    id 469
    label "Plastu&#347;"
  ]
  node [
    id 470
    label "Quasimodo"
  ]
  node [
    id 471
    label "Sherlock_Holmes"
  ]
  node [
    id 472
    label "Faust"
  ]
  node [
    id 473
    label "Wallenrod"
  ]
  node [
    id 474
    label "Dwukwiat"
  ]
  node [
    id 475
    label "Don_Juan"
  ]
  node [
    id 476
    label "koniugacja"
  ]
  node [
    id 477
    label "Don_Kiszot"
  ]
  node [
    id 478
    label "Hamlet"
  ]
  node [
    id 479
    label "Werter"
  ]
  node [
    id 480
    label "istota"
  ]
  node [
    id 481
    label "Szwejk"
  ]
  node [
    id 482
    label "doros&#322;y"
  ]
  node [
    id 483
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 484
    label "jajko"
  ]
  node [
    id 485
    label "rodzic"
  ]
  node [
    id 486
    label "wapniaki"
  ]
  node [
    id 487
    label "zwierzchnik"
  ]
  node [
    id 488
    label "feuda&#322;"
  ]
  node [
    id 489
    label "starzec"
  ]
  node [
    id 490
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 491
    label "zawodnik"
  ]
  node [
    id 492
    label "komendancja"
  ]
  node [
    id 493
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 494
    label "de-escalation"
  ]
  node [
    id 495
    label "powodowanie"
  ]
  node [
    id 496
    label "os&#322;abienie"
  ]
  node [
    id 497
    label "kondycja_fizyczna"
  ]
  node [
    id 498
    label "os&#322;abi&#263;"
  ]
  node [
    id 499
    label "debilitation"
  ]
  node [
    id 500
    label "zdrowie"
  ]
  node [
    id 501
    label "zmniejszanie"
  ]
  node [
    id 502
    label "s&#322;abszy"
  ]
  node [
    id 503
    label "pogarszanie"
  ]
  node [
    id 504
    label "suppress"
  ]
  node [
    id 505
    label "zmniejsza&#263;"
  ]
  node [
    id 506
    label "bate"
  ]
  node [
    id 507
    label "asymilowanie_si&#281;"
  ]
  node [
    id 508
    label "absorption"
  ]
  node [
    id 509
    label "pobieranie"
  ]
  node [
    id 510
    label "czerpanie"
  ]
  node [
    id 511
    label "acquisition"
  ]
  node [
    id 512
    label "zmienianie"
  ]
  node [
    id 513
    label "organizm"
  ]
  node [
    id 514
    label "assimilation"
  ]
  node [
    id 515
    label "upodabnianie"
  ]
  node [
    id 516
    label "g&#322;oska"
  ]
  node [
    id 517
    label "kultura"
  ]
  node [
    id 518
    label "grupa"
  ]
  node [
    id 519
    label "fonetyka"
  ]
  node [
    id 520
    label "assimilate"
  ]
  node [
    id 521
    label "dostosowywa&#263;"
  ]
  node [
    id 522
    label "dostosowa&#263;"
  ]
  node [
    id 523
    label "przejmowa&#263;"
  ]
  node [
    id 524
    label "upodobni&#263;"
  ]
  node [
    id 525
    label "przej&#261;&#263;"
  ]
  node [
    id 526
    label "upodabnia&#263;"
  ]
  node [
    id 527
    label "pobiera&#263;"
  ]
  node [
    id 528
    label "pobra&#263;"
  ]
  node [
    id 529
    label "charakterystyka"
  ]
  node [
    id 530
    label "zaistnie&#263;"
  ]
  node [
    id 531
    label "Osjan"
  ]
  node [
    id 532
    label "cecha"
  ]
  node [
    id 533
    label "kto&#347;"
  ]
  node [
    id 534
    label "wygl&#261;d"
  ]
  node [
    id 535
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 536
    label "osobowo&#347;&#263;"
  ]
  node [
    id 537
    label "trim"
  ]
  node [
    id 538
    label "poby&#263;"
  ]
  node [
    id 539
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 540
    label "Aspazja"
  ]
  node [
    id 541
    label "punkt_widzenia"
  ]
  node [
    id 542
    label "kompleksja"
  ]
  node [
    id 543
    label "wytrzyma&#263;"
  ]
  node [
    id 544
    label "budowa"
  ]
  node [
    id 545
    label "formacja"
  ]
  node [
    id 546
    label "pozosta&#263;"
  ]
  node [
    id 547
    label "przedstawienie"
  ]
  node [
    id 548
    label "go&#347;&#263;"
  ]
  node [
    id 549
    label "figure"
  ]
  node [
    id 550
    label "typ"
  ]
  node [
    id 551
    label "spos&#243;b"
  ]
  node [
    id 552
    label "mildew"
  ]
  node [
    id 553
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 554
    label "ideal"
  ]
  node [
    id 555
    label "rule"
  ]
  node [
    id 556
    label "ruch"
  ]
  node [
    id 557
    label "dekal"
  ]
  node [
    id 558
    label "motyw"
  ]
  node [
    id 559
    label "projekt"
  ]
  node [
    id 560
    label "pryncypa&#322;"
  ]
  node [
    id 561
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 562
    label "kszta&#322;t"
  ]
  node [
    id 563
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 564
    label "wiedza"
  ]
  node [
    id 565
    label "kierowa&#263;"
  ]
  node [
    id 566
    label "alkohol"
  ]
  node [
    id 567
    label "zdolno&#347;&#263;"
  ]
  node [
    id 568
    label "&#380;ycie"
  ]
  node [
    id 569
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 570
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 571
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 572
    label "sztuka"
  ]
  node [
    id 573
    label "dekiel"
  ]
  node [
    id 574
    label "ro&#347;lina"
  ]
  node [
    id 575
    label "&#347;ci&#281;cie"
  ]
  node [
    id 576
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 577
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 578
    label "&#347;ci&#281;gno"
  ]
  node [
    id 579
    label "noosfera"
  ]
  node [
    id 580
    label "byd&#322;o"
  ]
  node [
    id 581
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 582
    label "makrocefalia"
  ]
  node [
    id 583
    label "ucho"
  ]
  node [
    id 584
    label "g&#243;ra"
  ]
  node [
    id 585
    label "m&#243;zg"
  ]
  node [
    id 586
    label "kierownictwo"
  ]
  node [
    id 587
    label "fryzura"
  ]
  node [
    id 588
    label "umys&#322;"
  ]
  node [
    id 589
    label "cia&#322;o"
  ]
  node [
    id 590
    label "cz&#322;onek"
  ]
  node [
    id 591
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 592
    label "czaszka"
  ]
  node [
    id 593
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 594
    label "dziedzina"
  ]
  node [
    id 595
    label "hipnotyzowanie"
  ]
  node [
    id 596
    label "&#347;lad"
  ]
  node [
    id 597
    label "docieranie"
  ]
  node [
    id 598
    label "natural_process"
  ]
  node [
    id 599
    label "reakcja_chemiczna"
  ]
  node [
    id 600
    label "wdzieranie_si&#281;"
  ]
  node [
    id 601
    label "zjawisko"
  ]
  node [
    id 602
    label "lobbysta"
  ]
  node [
    id 603
    label "allochoria"
  ]
  node [
    id 604
    label "fotograf"
  ]
  node [
    id 605
    label "malarz"
  ]
  node [
    id 606
    label "artysta"
  ]
  node [
    id 607
    label "p&#322;aszczyzna"
  ]
  node [
    id 608
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 609
    label "bierka_szachowa"
  ]
  node [
    id 610
    label "obiekt_matematyczny"
  ]
  node [
    id 611
    label "gestaltyzm"
  ]
  node [
    id 612
    label "styl"
  ]
  node [
    id 613
    label "obraz"
  ]
  node [
    id 614
    label "d&#378;wi&#281;k"
  ]
  node [
    id 615
    label "character"
  ]
  node [
    id 616
    label "rze&#378;ba"
  ]
  node [
    id 617
    label "stylistyka"
  ]
  node [
    id 618
    label "miejsce"
  ]
  node [
    id 619
    label "antycypacja"
  ]
  node [
    id 620
    label "ornamentyka"
  ]
  node [
    id 621
    label "informacja"
  ]
  node [
    id 622
    label "facet"
  ]
  node [
    id 623
    label "popis"
  ]
  node [
    id 624
    label "wiersz"
  ]
  node [
    id 625
    label "symetria"
  ]
  node [
    id 626
    label "lingwistyka_kognitywna"
  ]
  node [
    id 627
    label "karta"
  ]
  node [
    id 628
    label "shape"
  ]
  node [
    id 629
    label "podzbi&#243;r"
  ]
  node [
    id 630
    label "perspektywa"
  ]
  node [
    id 631
    label "nak&#322;adka"
  ]
  node [
    id 632
    label "li&#347;&#263;"
  ]
  node [
    id 633
    label "jama_gard&#322;owa"
  ]
  node [
    id 634
    label "rezonator"
  ]
  node [
    id 635
    label "podstawa"
  ]
  node [
    id 636
    label "base"
  ]
  node [
    id 637
    label "piek&#322;o"
  ]
  node [
    id 638
    label "human_body"
  ]
  node [
    id 639
    label "ofiarowywanie"
  ]
  node [
    id 640
    label "sfera_afektywna"
  ]
  node [
    id 641
    label "nekromancja"
  ]
  node [
    id 642
    label "Po&#347;wist"
  ]
  node [
    id 643
    label "podekscytowanie"
  ]
  node [
    id 644
    label "deformowanie"
  ]
  node [
    id 645
    label "sumienie"
  ]
  node [
    id 646
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 647
    label "deformowa&#263;"
  ]
  node [
    id 648
    label "psychika"
  ]
  node [
    id 649
    label "zjawa"
  ]
  node [
    id 650
    label "zmar&#322;y"
  ]
  node [
    id 651
    label "istota_nadprzyrodzona"
  ]
  node [
    id 652
    label "power"
  ]
  node [
    id 653
    label "entity"
  ]
  node [
    id 654
    label "ofiarowywa&#263;"
  ]
  node [
    id 655
    label "oddech"
  ]
  node [
    id 656
    label "seksualno&#347;&#263;"
  ]
  node [
    id 657
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 658
    label "byt"
  ]
  node [
    id 659
    label "si&#322;a"
  ]
  node [
    id 660
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 661
    label "ego"
  ]
  node [
    id 662
    label "ofiarowanie"
  ]
  node [
    id 663
    label "charakter"
  ]
  node [
    id 664
    label "fizjonomia"
  ]
  node [
    id 665
    label "kompleks"
  ]
  node [
    id 666
    label "zapalno&#347;&#263;"
  ]
  node [
    id 667
    label "T&#281;sknica"
  ]
  node [
    id 668
    label "ofiarowa&#263;"
  ]
  node [
    id 669
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 670
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 671
    label "passion"
  ]
  node [
    id 672
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 673
    label "atom"
  ]
  node [
    id 674
    label "odbicie"
  ]
  node [
    id 675
    label "przyroda"
  ]
  node [
    id 676
    label "Ziemia"
  ]
  node [
    id 677
    label "kosmos"
  ]
  node [
    id 678
    label "miniatura"
  ]
  node [
    id 679
    label "Mazowsze"
  ]
  node [
    id 680
    label "Anglia"
  ]
  node [
    id 681
    label "Amazonia"
  ]
  node [
    id 682
    label "Bordeaux"
  ]
  node [
    id 683
    label "Naddniestrze"
  ]
  node [
    id 684
    label "plantowa&#263;"
  ]
  node [
    id 685
    label "Europa_Zachodnia"
  ]
  node [
    id 686
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 687
    label "Armagnac"
  ]
  node [
    id 688
    label "zapadnia"
  ]
  node [
    id 689
    label "Zamojszczyzna"
  ]
  node [
    id 690
    label "Amhara"
  ]
  node [
    id 691
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 692
    label "budynek"
  ]
  node [
    id 693
    label "skorupa_ziemska"
  ]
  node [
    id 694
    label "Ma&#322;opolska"
  ]
  node [
    id 695
    label "Turkiestan"
  ]
  node [
    id 696
    label "Noworosja"
  ]
  node [
    id 697
    label "Mezoameryka"
  ]
  node [
    id 698
    label "glinowanie"
  ]
  node [
    id 699
    label "Lubelszczyzna"
  ]
  node [
    id 700
    label "Ba&#322;kany"
  ]
  node [
    id 701
    label "Kurdystan"
  ]
  node [
    id 702
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 703
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 704
    label "martwica"
  ]
  node [
    id 705
    label "Baszkiria"
  ]
  node [
    id 706
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 707
    label "Szkocja"
  ]
  node [
    id 708
    label "Tonkin"
  ]
  node [
    id 709
    label "Maghreb"
  ]
  node [
    id 710
    label "teren"
  ]
  node [
    id 711
    label "litosfera"
  ]
  node [
    id 712
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 713
    label "penetrator"
  ]
  node [
    id 714
    label "Nadrenia"
  ]
  node [
    id 715
    label "glinowa&#263;"
  ]
  node [
    id 716
    label "Wielkopolska"
  ]
  node [
    id 717
    label "Zabajkale"
  ]
  node [
    id 718
    label "Apulia"
  ]
  node [
    id 719
    label "domain"
  ]
  node [
    id 720
    label "Bojkowszczyzna"
  ]
  node [
    id 721
    label "podglebie"
  ]
  node [
    id 722
    label "kompleks_sorpcyjny"
  ]
  node [
    id 723
    label "Liguria"
  ]
  node [
    id 724
    label "Pamir"
  ]
  node [
    id 725
    label "Indochiny"
  ]
  node [
    id 726
    label "Podlasie"
  ]
  node [
    id 727
    label "Polinezja"
  ]
  node [
    id 728
    label "Kurpie"
  ]
  node [
    id 729
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 730
    label "S&#261;decczyzna"
  ]
  node [
    id 731
    label "Umbria"
  ]
  node [
    id 732
    label "Karaiby"
  ]
  node [
    id 733
    label "Ukraina_Zachodnia"
  ]
  node [
    id 734
    label "Kielecczyzna"
  ]
  node [
    id 735
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 736
    label "kort"
  ]
  node [
    id 737
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 738
    label "czynnik_produkcji"
  ]
  node [
    id 739
    label "Skandynawia"
  ]
  node [
    id 740
    label "Kujawy"
  ]
  node [
    id 741
    label "Tyrol"
  ]
  node [
    id 742
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 743
    label "Huculszczyzna"
  ]
  node [
    id 744
    label "pojazd"
  ]
  node [
    id 745
    label "Turyngia"
  ]
  node [
    id 746
    label "powierzchnia"
  ]
  node [
    id 747
    label "jednostka_administracyjna"
  ]
  node [
    id 748
    label "Toskania"
  ]
  node [
    id 749
    label "Podhale"
  ]
  node [
    id 750
    label "Bory_Tucholskie"
  ]
  node [
    id 751
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 752
    label "Kalabria"
  ]
  node [
    id 753
    label "pr&#243;chnica"
  ]
  node [
    id 754
    label "Hercegowina"
  ]
  node [
    id 755
    label "Lotaryngia"
  ]
  node [
    id 756
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 757
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 758
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 759
    label "Walia"
  ]
  node [
    id 760
    label "pomieszczenie"
  ]
  node [
    id 761
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 762
    label "Opolskie"
  ]
  node [
    id 763
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 764
    label "Kampania"
  ]
  node [
    id 765
    label "Sand&#380;ak"
  ]
  node [
    id 766
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 767
    label "Syjon"
  ]
  node [
    id 768
    label "Kabylia"
  ]
  node [
    id 769
    label "ryzosfera"
  ]
  node [
    id 770
    label "Lombardia"
  ]
  node [
    id 771
    label "Warmia"
  ]
  node [
    id 772
    label "Kaszmir"
  ]
  node [
    id 773
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 774
    label "&#321;&#243;dzkie"
  ]
  node [
    id 775
    label "Kaukaz"
  ]
  node [
    id 776
    label "Europa_Wschodnia"
  ]
  node [
    id 777
    label "Biskupizna"
  ]
  node [
    id 778
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 779
    label "Afryka_Wschodnia"
  ]
  node [
    id 780
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 781
    label "Podkarpacie"
  ]
  node [
    id 782
    label "obszar"
  ]
  node [
    id 783
    label "Afryka_Zachodnia"
  ]
  node [
    id 784
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 785
    label "Bo&#347;nia"
  ]
  node [
    id 786
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 787
    label "dotleni&#263;"
  ]
  node [
    id 788
    label "Oceania"
  ]
  node [
    id 789
    label "Pomorze_Zachodnie"
  ]
  node [
    id 790
    label "Powi&#347;le"
  ]
  node [
    id 791
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 792
    label "Podbeskidzie"
  ]
  node [
    id 793
    label "&#321;emkowszczyzna"
  ]
  node [
    id 794
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 795
    label "Opolszczyzna"
  ]
  node [
    id 796
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 797
    label "Kaszuby"
  ]
  node [
    id 798
    label "Ko&#322;yma"
  ]
  node [
    id 799
    label "Szlezwik"
  ]
  node [
    id 800
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 801
    label "glej"
  ]
  node [
    id 802
    label "Mikronezja"
  ]
  node [
    id 803
    label "pa&#324;stwo"
  ]
  node [
    id 804
    label "posadzka"
  ]
  node [
    id 805
    label "Polesie"
  ]
  node [
    id 806
    label "Kerala"
  ]
  node [
    id 807
    label "Mazury"
  ]
  node [
    id 808
    label "Palestyna"
  ]
  node [
    id 809
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 810
    label "Lauda"
  ]
  node [
    id 811
    label "Azja_Wschodnia"
  ]
  node [
    id 812
    label "Galicja"
  ]
  node [
    id 813
    label "Zakarpacie"
  ]
  node [
    id 814
    label "Lubuskie"
  ]
  node [
    id 815
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 816
    label "Laponia"
  ]
  node [
    id 817
    label "Yorkshire"
  ]
  node [
    id 818
    label "Bawaria"
  ]
  node [
    id 819
    label "Zag&#243;rze"
  ]
  node [
    id 820
    label "geosystem"
  ]
  node [
    id 821
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 822
    label "Andaluzja"
  ]
  node [
    id 823
    label "&#379;ywiecczyzna"
  ]
  node [
    id 824
    label "Oksytania"
  ]
  node [
    id 825
    label "przestrze&#324;"
  ]
  node [
    id 826
    label "Kociewie"
  ]
  node [
    id 827
    label "Lasko"
  ]
  node [
    id 828
    label "warunek_lokalowy"
  ]
  node [
    id 829
    label "plac"
  ]
  node [
    id 830
    label "location"
  ]
  node [
    id 831
    label "uwaga"
  ]
  node [
    id 832
    label "status"
  ]
  node [
    id 833
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 834
    label "chwila"
  ]
  node [
    id 835
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 836
    label "praca"
  ]
  node [
    id 837
    label "rz&#261;d"
  ]
  node [
    id 838
    label "tkanina_we&#322;niana"
  ]
  node [
    id 839
    label "boisko"
  ]
  node [
    id 840
    label "siatka"
  ]
  node [
    id 841
    label "ubrani&#243;wka"
  ]
  node [
    id 842
    label "p&#243;&#322;noc"
  ]
  node [
    id 843
    label "Kosowo"
  ]
  node [
    id 844
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 845
    label "Zab&#322;ocie"
  ]
  node [
    id 846
    label "zach&#243;d"
  ]
  node [
    id 847
    label "po&#322;udnie"
  ]
  node [
    id 848
    label "Pow&#261;zki"
  ]
  node [
    id 849
    label "Piotrowo"
  ]
  node [
    id 850
    label "Olszanica"
  ]
  node [
    id 851
    label "zbi&#243;r"
  ]
  node [
    id 852
    label "Ruda_Pabianicka"
  ]
  node [
    id 853
    label "holarktyka"
  ]
  node [
    id 854
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 855
    label "Ludwin&#243;w"
  ]
  node [
    id 856
    label "Arktyka"
  ]
  node [
    id 857
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 858
    label "Zabu&#380;e"
  ]
  node [
    id 859
    label "antroposfera"
  ]
  node [
    id 860
    label "Neogea"
  ]
  node [
    id 861
    label "terytorium"
  ]
  node [
    id 862
    label "Syberia_Zachodnia"
  ]
  node [
    id 863
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 864
    label "zakres"
  ]
  node [
    id 865
    label "pas_planetoid"
  ]
  node [
    id 866
    label "Syberia_Wschodnia"
  ]
  node [
    id 867
    label "Antarktyka"
  ]
  node [
    id 868
    label "Rakowice"
  ]
  node [
    id 869
    label "akrecja"
  ]
  node [
    id 870
    label "wymiar"
  ]
  node [
    id 871
    label "&#321;&#281;g"
  ]
  node [
    id 872
    label "Kresy_Zachodnie"
  ]
  node [
    id 873
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 874
    label "wsch&#243;d"
  ]
  node [
    id 875
    label "Notogea"
  ]
  node [
    id 876
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 877
    label "mienie"
  ]
  node [
    id 878
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 879
    label "immoblizacja"
  ]
  node [
    id 880
    label "&#347;ciana"
  ]
  node [
    id 881
    label "surface"
  ]
  node [
    id 882
    label "kwadrant"
  ]
  node [
    id 883
    label "degree"
  ]
  node [
    id 884
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 885
    label "ukszta&#322;towanie"
  ]
  node [
    id 886
    label "p&#322;aszczak"
  ]
  node [
    id 887
    label "kontekst"
  ]
  node [
    id 888
    label "miejsce_pracy"
  ]
  node [
    id 889
    label "nation"
  ]
  node [
    id 890
    label "krajobraz"
  ]
  node [
    id 891
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 892
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 893
    label "w&#322;adza"
  ]
  node [
    id 894
    label "rozdzielanie"
  ]
  node [
    id 895
    label "bezbrze&#380;e"
  ]
  node [
    id 896
    label "punkt"
  ]
  node [
    id 897
    label "czasoprzestrze&#324;"
  ]
  node [
    id 898
    label "niezmierzony"
  ]
  node [
    id 899
    label "przedzielenie"
  ]
  node [
    id 900
    label "nielito&#347;ciwy"
  ]
  node [
    id 901
    label "rozdziela&#263;"
  ]
  node [
    id 902
    label "oktant"
  ]
  node [
    id 903
    label "przedzieli&#263;"
  ]
  node [
    id 904
    label "przestw&#243;r"
  ]
  node [
    id 905
    label "rozmiar"
  ]
  node [
    id 906
    label "poj&#281;cie"
  ]
  node [
    id 907
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 908
    label "zwierciad&#322;o"
  ]
  node [
    id 909
    label "capacity"
  ]
  node [
    id 910
    label "plane"
  ]
  node [
    id 911
    label "gleba"
  ]
  node [
    id 912
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 913
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 914
    label "warstwa"
  ]
  node [
    id 915
    label "sialma"
  ]
  node [
    id 916
    label "warstwa_perydotytowa"
  ]
  node [
    id 917
    label "warstwa_granitowa"
  ]
  node [
    id 918
    label "powietrze"
  ]
  node [
    id 919
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 920
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 921
    label "fauna"
  ]
  node [
    id 922
    label "balkon"
  ]
  node [
    id 923
    label "pod&#322;oga"
  ]
  node [
    id 924
    label "kondygnacja"
  ]
  node [
    id 925
    label "skrzyd&#322;o"
  ]
  node [
    id 926
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 927
    label "dach"
  ]
  node [
    id 928
    label "strop"
  ]
  node [
    id 929
    label "klatka_schodowa"
  ]
  node [
    id 930
    label "przedpro&#380;e"
  ]
  node [
    id 931
    label "Pentagon"
  ]
  node [
    id 932
    label "alkierz"
  ]
  node [
    id 933
    label "front"
  ]
  node [
    id 934
    label "amfilada"
  ]
  node [
    id 935
    label "apartment"
  ]
  node [
    id 936
    label "udost&#281;pnienie"
  ]
  node [
    id 937
    label "sklepienie"
  ]
  node [
    id 938
    label "sufit"
  ]
  node [
    id 939
    label "umieszczenie"
  ]
  node [
    id 940
    label "zakamarek"
  ]
  node [
    id 941
    label "odholowa&#263;"
  ]
  node [
    id 942
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 943
    label "tabor"
  ]
  node [
    id 944
    label "przyholowywanie"
  ]
  node [
    id 945
    label "przyholowa&#263;"
  ]
  node [
    id 946
    label "przyholowanie"
  ]
  node [
    id 947
    label "fukni&#281;cie"
  ]
  node [
    id 948
    label "l&#261;d"
  ]
  node [
    id 949
    label "zielona_karta"
  ]
  node [
    id 950
    label "fukanie"
  ]
  node [
    id 951
    label "przyholowywa&#263;"
  ]
  node [
    id 952
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 953
    label "woda"
  ]
  node [
    id 954
    label "przeszklenie"
  ]
  node [
    id 955
    label "test_zderzeniowy"
  ]
  node [
    id 956
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 957
    label "odzywka"
  ]
  node [
    id 958
    label "nadwozie"
  ]
  node [
    id 959
    label "odholowanie"
  ]
  node [
    id 960
    label "prowadzenie_si&#281;"
  ]
  node [
    id 961
    label "odholowywa&#263;"
  ]
  node [
    id 962
    label "odholowywanie"
  ]
  node [
    id 963
    label "hamulec"
  ]
  node [
    id 964
    label "podwozie"
  ]
  node [
    id 965
    label "wzbogacanie"
  ]
  node [
    id 966
    label "zabezpieczanie"
  ]
  node [
    id 967
    label "pokrywanie"
  ]
  node [
    id 968
    label "aluminize"
  ]
  node [
    id 969
    label "metalizowanie"
  ]
  node [
    id 970
    label "metalizowa&#263;"
  ]
  node [
    id 971
    label "wzbogaca&#263;"
  ]
  node [
    id 972
    label "pokrywa&#263;"
  ]
  node [
    id 973
    label "zabezpiecza&#263;"
  ]
  node [
    id 974
    label "nasyci&#263;"
  ]
  node [
    id 975
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 976
    label "level"
  ]
  node [
    id 977
    label "r&#243;wna&#263;"
  ]
  node [
    id 978
    label "uprawia&#263;"
  ]
  node [
    id 979
    label "urz&#261;dzenie"
  ]
  node [
    id 980
    label "Judea"
  ]
  node [
    id 981
    label "moszaw"
  ]
  node [
    id 982
    label "Kanaan"
  ]
  node [
    id 983
    label "Algieria"
  ]
  node [
    id 984
    label "Antigua_i_Barbuda"
  ]
  node [
    id 985
    label "Kuba"
  ]
  node [
    id 986
    label "Jamajka"
  ]
  node [
    id 987
    label "Aruba"
  ]
  node [
    id 988
    label "Haiti"
  ]
  node [
    id 989
    label "Kajmany"
  ]
  node [
    id 990
    label "Portoryko"
  ]
  node [
    id 991
    label "Anguilla"
  ]
  node [
    id 992
    label "Bahamy"
  ]
  node [
    id 993
    label "Antyle"
  ]
  node [
    id 994
    label "Polska"
  ]
  node [
    id 995
    label "Mogielnica"
  ]
  node [
    id 996
    label "Indie"
  ]
  node [
    id 997
    label "jezioro"
  ]
  node [
    id 998
    label "Niemcy"
  ]
  node [
    id 999
    label "Rumelia"
  ]
  node [
    id 1000
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1001
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1002
    label "Poprad"
  ]
  node [
    id 1003
    label "Tatry"
  ]
  node [
    id 1004
    label "Podtatrze"
  ]
  node [
    id 1005
    label "Podole"
  ]
  node [
    id 1006
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1007
    label "Hiszpania"
  ]
  node [
    id 1008
    label "Austro-W&#281;gry"
  ]
  node [
    id 1009
    label "W&#322;ochy"
  ]
  node [
    id 1010
    label "Biskupice"
  ]
  node [
    id 1011
    label "Iwanowice"
  ]
  node [
    id 1012
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1013
    label "Rogo&#378;nik"
  ]
  node [
    id 1014
    label "Ropa"
  ]
  node [
    id 1015
    label "Wietnam"
  ]
  node [
    id 1016
    label "Etiopia"
  ]
  node [
    id 1017
    label "Austria"
  ]
  node [
    id 1018
    label "Alpy"
  ]
  node [
    id 1019
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1020
    label "Francja"
  ]
  node [
    id 1021
    label "Wyspy_Marshalla"
  ]
  node [
    id 1022
    label "Nauru"
  ]
  node [
    id 1023
    label "Mariany"
  ]
  node [
    id 1024
    label "dolar"
  ]
  node [
    id 1025
    label "Karpaty"
  ]
  node [
    id 1026
    label "Samoa"
  ]
  node [
    id 1027
    label "Tonga"
  ]
  node [
    id 1028
    label "Tuwalu"
  ]
  node [
    id 1029
    label "Hawaje"
  ]
  node [
    id 1030
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1031
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1032
    label "Rosja"
  ]
  node [
    id 1033
    label "Beskid_Niski"
  ]
  node [
    id 1034
    label "Etruria"
  ]
  node [
    id 1035
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1036
    label "Bojanowo"
  ]
  node [
    id 1037
    label "Obra"
  ]
  node [
    id 1038
    label "Wilkowo_Polskie"
  ]
  node [
    id 1039
    label "Dobra"
  ]
  node [
    id 1040
    label "Buriacja"
  ]
  node [
    id 1041
    label "Rozewie"
  ]
  node [
    id 1042
    label "&#346;l&#261;sk"
  ]
  node [
    id 1043
    label "Czechy"
  ]
  node [
    id 1044
    label "Ukraina"
  ]
  node [
    id 1045
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1046
    label "Mo&#322;dawia"
  ]
  node [
    id 1047
    label "Norwegia"
  ]
  node [
    id 1048
    label "Szwecja"
  ]
  node [
    id 1049
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1050
    label "Finlandia"
  ]
  node [
    id 1051
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1052
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1053
    label "Wiktoria"
  ]
  node [
    id 1054
    label "Wielka_Brytania"
  ]
  node [
    id 1055
    label "Guernsey"
  ]
  node [
    id 1056
    label "Conrad"
  ]
  node [
    id 1057
    label "funt_szterling"
  ]
  node [
    id 1058
    label "Unia_Europejska"
  ]
  node [
    id 1059
    label "Portland"
  ]
  node [
    id 1060
    label "NATO"
  ]
  node [
    id 1061
    label "El&#380;bieta_I"
  ]
  node [
    id 1062
    label "Kornwalia"
  ]
  node [
    id 1063
    label "Amazonka"
  ]
  node [
    id 1064
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1065
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1066
    label "Libia"
  ]
  node [
    id 1067
    label "Maroko"
  ]
  node [
    id 1068
    label "Tunezja"
  ]
  node [
    id 1069
    label "Mauretania"
  ]
  node [
    id 1070
    label "Sahara_Zachodnia"
  ]
  node [
    id 1071
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1072
    label "Anglosas"
  ]
  node [
    id 1073
    label "Moza"
  ]
  node [
    id 1074
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1075
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1076
    label "Paj&#281;czno"
  ]
  node [
    id 1077
    label "Nowa_Zelandia"
  ]
  node [
    id 1078
    label "Ocean_Spokojny"
  ]
  node [
    id 1079
    label "Palau"
  ]
  node [
    id 1080
    label "Melanezja"
  ]
  node [
    id 1081
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1082
    label "Czarnog&#243;ra"
  ]
  node [
    id 1083
    label "Serbia"
  ]
  node [
    id 1084
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1085
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1086
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1087
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1088
    label "Gop&#322;o"
  ]
  node [
    id 1089
    label "Jerozolima"
  ]
  node [
    id 1090
    label "Dolna_Frankonia"
  ]
  node [
    id 1091
    label "funt_szkocki"
  ]
  node [
    id 1092
    label "Kaledonia"
  ]
  node [
    id 1093
    label "Czeczenia"
  ]
  node [
    id 1094
    label "Inguszetia"
  ]
  node [
    id 1095
    label "Abchazja"
  ]
  node [
    id 1096
    label "Sarmata"
  ]
  node [
    id 1097
    label "Dagestan"
  ]
  node [
    id 1098
    label "Eurazja"
  ]
  node [
    id 1099
    label "Warszawa"
  ]
  node [
    id 1100
    label "Mariensztat"
  ]
  node [
    id 1101
    label "Pakistan"
  ]
  node [
    id 1102
    label "Katar"
  ]
  node [
    id 1103
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1104
    label "Gwatemala"
  ]
  node [
    id 1105
    label "Afganistan"
  ]
  node [
    id 1106
    label "Ekwador"
  ]
  node [
    id 1107
    label "Tad&#380;ykistan"
  ]
  node [
    id 1108
    label "Bhutan"
  ]
  node [
    id 1109
    label "Argentyna"
  ]
  node [
    id 1110
    label "D&#380;ibuti"
  ]
  node [
    id 1111
    label "Wenezuela"
  ]
  node [
    id 1112
    label "Gabon"
  ]
  node [
    id 1113
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1114
    label "Rwanda"
  ]
  node [
    id 1115
    label "Liechtenstein"
  ]
  node [
    id 1116
    label "organizacja"
  ]
  node [
    id 1117
    label "Sri_Lanka"
  ]
  node [
    id 1118
    label "Madagaskar"
  ]
  node [
    id 1119
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1120
    label "Kongo"
  ]
  node [
    id 1121
    label "Bangladesz"
  ]
  node [
    id 1122
    label "Kanada"
  ]
  node [
    id 1123
    label "Wehrlen"
  ]
  node [
    id 1124
    label "Surinam"
  ]
  node [
    id 1125
    label "Chile"
  ]
  node [
    id 1126
    label "Uganda"
  ]
  node [
    id 1127
    label "W&#281;gry"
  ]
  node [
    id 1128
    label "Birma"
  ]
  node [
    id 1129
    label "Kazachstan"
  ]
  node [
    id 1130
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1131
    label "Armenia"
  ]
  node [
    id 1132
    label "Timor_Wschodni"
  ]
  node [
    id 1133
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1134
    label "Izrael"
  ]
  node [
    id 1135
    label "Estonia"
  ]
  node [
    id 1136
    label "Komory"
  ]
  node [
    id 1137
    label "Kamerun"
  ]
  node [
    id 1138
    label "Belize"
  ]
  node [
    id 1139
    label "Sierra_Leone"
  ]
  node [
    id 1140
    label "Luksemburg"
  ]
  node [
    id 1141
    label "USA"
  ]
  node [
    id 1142
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1143
    label "Barbados"
  ]
  node [
    id 1144
    label "San_Marino"
  ]
  node [
    id 1145
    label "Bu&#322;garia"
  ]
  node [
    id 1146
    label "Indonezja"
  ]
  node [
    id 1147
    label "Malawi"
  ]
  node [
    id 1148
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1149
    label "partia"
  ]
  node [
    id 1150
    label "Zambia"
  ]
  node [
    id 1151
    label "Angola"
  ]
  node [
    id 1152
    label "Grenada"
  ]
  node [
    id 1153
    label "Nepal"
  ]
  node [
    id 1154
    label "Panama"
  ]
  node [
    id 1155
    label "Rumunia"
  ]
  node [
    id 1156
    label "Malediwy"
  ]
  node [
    id 1157
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1158
    label "S&#322;owacja"
  ]
  node [
    id 1159
    label "para"
  ]
  node [
    id 1160
    label "Egipt"
  ]
  node [
    id 1161
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1162
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1163
    label "Kolumbia"
  ]
  node [
    id 1164
    label "Mozambik"
  ]
  node [
    id 1165
    label "Laos"
  ]
  node [
    id 1166
    label "Burundi"
  ]
  node [
    id 1167
    label "Suazi"
  ]
  node [
    id 1168
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1169
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1170
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1171
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1172
    label "Dominika"
  ]
  node [
    id 1173
    label "Syria"
  ]
  node [
    id 1174
    label "Gwinea_Bissau"
  ]
  node [
    id 1175
    label "Liberia"
  ]
  node [
    id 1176
    label "Zimbabwe"
  ]
  node [
    id 1177
    label "Dominikana"
  ]
  node [
    id 1178
    label "Senegal"
  ]
  node [
    id 1179
    label "Gruzja"
  ]
  node [
    id 1180
    label "Togo"
  ]
  node [
    id 1181
    label "Chorwacja"
  ]
  node [
    id 1182
    label "Meksyk"
  ]
  node [
    id 1183
    label "Macedonia"
  ]
  node [
    id 1184
    label "Gujana"
  ]
  node [
    id 1185
    label "Zair"
  ]
  node [
    id 1186
    label "Albania"
  ]
  node [
    id 1187
    label "Kambod&#380;a"
  ]
  node [
    id 1188
    label "Mauritius"
  ]
  node [
    id 1189
    label "Monako"
  ]
  node [
    id 1190
    label "Gwinea"
  ]
  node [
    id 1191
    label "Mali"
  ]
  node [
    id 1192
    label "Nigeria"
  ]
  node [
    id 1193
    label "Kostaryka"
  ]
  node [
    id 1194
    label "Hanower"
  ]
  node [
    id 1195
    label "Paragwaj"
  ]
  node [
    id 1196
    label "Wyspy_Salomona"
  ]
  node [
    id 1197
    label "Seszele"
  ]
  node [
    id 1198
    label "Boliwia"
  ]
  node [
    id 1199
    label "Kirgistan"
  ]
  node [
    id 1200
    label "Irlandia"
  ]
  node [
    id 1201
    label "Czad"
  ]
  node [
    id 1202
    label "Irak"
  ]
  node [
    id 1203
    label "Lesoto"
  ]
  node [
    id 1204
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1205
    label "Malta"
  ]
  node [
    id 1206
    label "Andora"
  ]
  node [
    id 1207
    label "Chiny"
  ]
  node [
    id 1208
    label "Filipiny"
  ]
  node [
    id 1209
    label "Antarktis"
  ]
  node [
    id 1210
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1211
    label "Brazylia"
  ]
  node [
    id 1212
    label "Nikaragua"
  ]
  node [
    id 1213
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1214
    label "Kenia"
  ]
  node [
    id 1215
    label "Niger"
  ]
  node [
    id 1216
    label "Portugalia"
  ]
  node [
    id 1217
    label "Fid&#380;i"
  ]
  node [
    id 1218
    label "Botswana"
  ]
  node [
    id 1219
    label "Tajlandia"
  ]
  node [
    id 1220
    label "Australia"
  ]
  node [
    id 1221
    label "Burkina_Faso"
  ]
  node [
    id 1222
    label "interior"
  ]
  node [
    id 1223
    label "Benin"
  ]
  node [
    id 1224
    label "Tanzania"
  ]
  node [
    id 1225
    label "&#321;otwa"
  ]
  node [
    id 1226
    label "Kiribati"
  ]
  node [
    id 1227
    label "Rodezja"
  ]
  node [
    id 1228
    label "Cypr"
  ]
  node [
    id 1229
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1230
    label "Peru"
  ]
  node [
    id 1231
    label "Urugwaj"
  ]
  node [
    id 1232
    label "Jordania"
  ]
  node [
    id 1233
    label "Grecja"
  ]
  node [
    id 1234
    label "Azerbejd&#380;an"
  ]
  node [
    id 1235
    label "Turcja"
  ]
  node [
    id 1236
    label "Sudan"
  ]
  node [
    id 1237
    label "Oman"
  ]
  node [
    id 1238
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1239
    label "Uzbekistan"
  ]
  node [
    id 1240
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1241
    label "Honduras"
  ]
  node [
    id 1242
    label "Mongolia"
  ]
  node [
    id 1243
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1244
    label "Tajwan"
  ]
  node [
    id 1245
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1246
    label "Liban"
  ]
  node [
    id 1247
    label "Japonia"
  ]
  node [
    id 1248
    label "Ghana"
  ]
  node [
    id 1249
    label "Bahrajn"
  ]
  node [
    id 1250
    label "Belgia"
  ]
  node [
    id 1251
    label "Kuwejt"
  ]
  node [
    id 1252
    label "Litwa"
  ]
  node [
    id 1253
    label "S&#322;owenia"
  ]
  node [
    id 1254
    label "Szwajcaria"
  ]
  node [
    id 1255
    label "Erytrea"
  ]
  node [
    id 1256
    label "Arabia_Saudyjska"
  ]
  node [
    id 1257
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1258
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1259
    label "Malezja"
  ]
  node [
    id 1260
    label "Korea"
  ]
  node [
    id 1261
    label "Jemen"
  ]
  node [
    id 1262
    label "Namibia"
  ]
  node [
    id 1263
    label "holoarktyka"
  ]
  node [
    id 1264
    label "Brunei"
  ]
  node [
    id 1265
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1266
    label "Khitai"
  ]
  node [
    id 1267
    label "Iran"
  ]
  node [
    id 1268
    label "Gambia"
  ]
  node [
    id 1269
    label "Somalia"
  ]
  node [
    id 1270
    label "Holandia"
  ]
  node [
    id 1271
    label "Turkmenistan"
  ]
  node [
    id 1272
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1273
    label "Salwador"
  ]
  node [
    id 1274
    label "system_korzeniowy"
  ]
  node [
    id 1275
    label "bakteria"
  ]
  node [
    id 1276
    label "ubytek"
  ]
  node [
    id 1277
    label "fleczer"
  ]
  node [
    id 1278
    label "choroba_bakteryjna"
  ]
  node [
    id 1279
    label "schorzenie"
  ]
  node [
    id 1280
    label "kwas_huminowy"
  ]
  node [
    id 1281
    label "substancja_szara"
  ]
  node [
    id 1282
    label "tkanka"
  ]
  node [
    id 1283
    label "kom&#243;rka_glejowa"
  ]
  node [
    id 1284
    label "neuroglia"
  ]
  node [
    id 1285
    label "kamfenol"
  ]
  node [
    id 1286
    label "&#322;yko"
  ]
  node [
    id 1287
    label "necrosis"
  ]
  node [
    id 1288
    label "odle&#380;yna"
  ]
  node [
    id 1289
    label "zanikni&#281;cie"
  ]
  node [
    id 1290
    label "zmiana_wsteczna"
  ]
  node [
    id 1291
    label "ska&#322;a_osadowa"
  ]
  node [
    id 1292
    label "korek"
  ]
  node [
    id 1293
    label "pu&#322;apka"
  ]
  node [
    id 1294
    label "take_care"
  ]
  node [
    id 1295
    label "troska&#263;_si&#281;"
  ]
  node [
    id 1296
    label "rozpatrywa&#263;"
  ]
  node [
    id 1297
    label "argue"
  ]
  node [
    id 1298
    label "os&#261;dza&#263;"
  ]
  node [
    id 1299
    label "strike"
  ]
  node [
    id 1300
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1301
    label "znajdowa&#263;"
  ]
  node [
    id 1302
    label "hold"
  ]
  node [
    id 1303
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 1304
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 1305
    label "przeprowadza&#263;"
  ]
  node [
    id 1306
    label "consider"
  ]
  node [
    id 1307
    label "volunteer"
  ]
  node [
    id 1308
    label "organizowa&#263;"
  ]
  node [
    id 1309
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1310
    label "stylizowa&#263;"
  ]
  node [
    id 1311
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1312
    label "falowa&#263;"
  ]
  node [
    id 1313
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1314
    label "wydala&#263;"
  ]
  node [
    id 1315
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1316
    label "tentegowa&#263;"
  ]
  node [
    id 1317
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1318
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1319
    label "oszukiwa&#263;"
  ]
  node [
    id 1320
    label "ukazywa&#263;"
  ]
  node [
    id 1321
    label "przerabia&#263;"
  ]
  node [
    id 1322
    label "post&#281;powa&#263;"
  ]
  node [
    id 1323
    label "participate"
  ]
  node [
    id 1324
    label "istnie&#263;"
  ]
  node [
    id 1325
    label "pozostawa&#263;"
  ]
  node [
    id 1326
    label "zostawa&#263;"
  ]
  node [
    id 1327
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1328
    label "adhere"
  ]
  node [
    id 1329
    label "compass"
  ]
  node [
    id 1330
    label "korzysta&#263;"
  ]
  node [
    id 1331
    label "appreciation"
  ]
  node [
    id 1332
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1333
    label "dociera&#263;"
  ]
  node [
    id 1334
    label "get"
  ]
  node [
    id 1335
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1336
    label "mierzy&#263;"
  ]
  node [
    id 1337
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1338
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1339
    label "exsert"
  ]
  node [
    id 1340
    label "being"
  ]
  node [
    id 1341
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1342
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1343
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1344
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1345
    label "run"
  ]
  node [
    id 1346
    label "bangla&#263;"
  ]
  node [
    id 1347
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1348
    label "przebiega&#263;"
  ]
  node [
    id 1349
    label "proceed"
  ]
  node [
    id 1350
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1351
    label "carry"
  ]
  node [
    id 1352
    label "bywa&#263;"
  ]
  node [
    id 1353
    label "dziama&#263;"
  ]
  node [
    id 1354
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1355
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1356
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1357
    label "str&#243;j"
  ]
  node [
    id 1358
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1359
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1360
    label "krok"
  ]
  node [
    id 1361
    label "tryb"
  ]
  node [
    id 1362
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1363
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1364
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1365
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1366
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1367
    label "continue"
  ]
  node [
    id 1368
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1369
    label "Ohio"
  ]
  node [
    id 1370
    label "wci&#281;cie"
  ]
  node [
    id 1371
    label "Nowy_York"
  ]
  node [
    id 1372
    label "samopoczucie"
  ]
  node [
    id 1373
    label "Illinois"
  ]
  node [
    id 1374
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1375
    label "state"
  ]
  node [
    id 1376
    label "Jukatan"
  ]
  node [
    id 1377
    label "Kalifornia"
  ]
  node [
    id 1378
    label "Wirginia"
  ]
  node [
    id 1379
    label "wektor"
  ]
  node [
    id 1380
    label "Goa"
  ]
  node [
    id 1381
    label "Teksas"
  ]
  node [
    id 1382
    label "Waszyngton"
  ]
  node [
    id 1383
    label "Massachusetts"
  ]
  node [
    id 1384
    label "Alaska"
  ]
  node [
    id 1385
    label "Arakan"
  ]
  node [
    id 1386
    label "Maryland"
  ]
  node [
    id 1387
    label "Michigan"
  ]
  node [
    id 1388
    label "Arizona"
  ]
  node [
    id 1389
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1390
    label "Georgia"
  ]
  node [
    id 1391
    label "poziom"
  ]
  node [
    id 1392
    label "Pensylwania"
  ]
  node [
    id 1393
    label "Luizjana"
  ]
  node [
    id 1394
    label "Nowy_Meksyk"
  ]
  node [
    id 1395
    label "Alabama"
  ]
  node [
    id 1396
    label "ilo&#347;&#263;"
  ]
  node [
    id 1397
    label "Kansas"
  ]
  node [
    id 1398
    label "Oregon"
  ]
  node [
    id 1399
    label "Oklahoma"
  ]
  node [
    id 1400
    label "Floryda"
  ]
  node [
    id 1401
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1402
    label "pomiernie"
  ]
  node [
    id 1403
    label "nieistotnie"
  ]
  node [
    id 1404
    label "nieznaczny"
  ]
  node [
    id 1405
    label "ma&#322;y"
  ]
  node [
    id 1406
    label "niepowa&#380;nie"
  ]
  node [
    id 1407
    label "niewa&#380;ny"
  ]
  node [
    id 1408
    label "nieznacznie"
  ]
  node [
    id 1409
    label "drobnostkowy"
  ]
  node [
    id 1410
    label "szybki"
  ]
  node [
    id 1411
    label "przeci&#281;tny"
  ]
  node [
    id 1412
    label "wstydliwy"
  ]
  node [
    id 1413
    label "s&#322;aby"
  ]
  node [
    id 1414
    label "ch&#322;opiec"
  ]
  node [
    id 1415
    label "m&#322;ody"
  ]
  node [
    id 1416
    label "ma&#322;o"
  ]
  node [
    id 1417
    label "marny"
  ]
  node [
    id 1418
    label "nieliczny"
  ]
  node [
    id 1419
    label "n&#281;dznie"
  ]
  node [
    id 1420
    label "licho"
  ]
  node [
    id 1421
    label "proporcjonalnie"
  ]
  node [
    id 1422
    label "pomierny"
  ]
  node [
    id 1423
    label "miernie"
  ]
  node [
    id 1424
    label "czu&#263;"
  ]
  node [
    id 1425
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 1426
    label "&#322;akn&#261;&#263;"
  ]
  node [
    id 1427
    label "t&#281;skni&#263;"
  ]
  node [
    id 1428
    label "desire"
  ]
  node [
    id 1429
    label "chcie&#263;"
  ]
  node [
    id 1430
    label "kcie&#263;"
  ]
  node [
    id 1431
    label "try"
  ]
  node [
    id 1432
    label "postrzega&#263;"
  ]
  node [
    id 1433
    label "smell"
  ]
  node [
    id 1434
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 1435
    label "uczuwa&#263;"
  ]
  node [
    id 1436
    label "spirit"
  ]
  node [
    id 1437
    label "doznawa&#263;"
  ]
  node [
    id 1438
    label "anticipate"
  ]
  node [
    id 1439
    label "miss"
  ]
  node [
    id 1440
    label "wymaga&#263;"
  ]
  node [
    id 1441
    label "sprawi&#263;"
  ]
  node [
    id 1442
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 1443
    label "advance"
  ]
  node [
    id 1444
    label "see"
  ]
  node [
    id 1445
    label "wyrobi&#263;"
  ]
  node [
    id 1446
    label "wzi&#261;&#263;"
  ]
  node [
    id 1447
    label "catch"
  ]
  node [
    id 1448
    label "frame"
  ]
  node [
    id 1449
    label "przygotowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 10
    target 874
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 876
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 878
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 879
  ]
  edge [
    source 10
    target 880
  ]
  edge [
    source 10
    target 881
  ]
  edge [
    source 10
    target 882
  ]
  edge [
    source 10
    target 883
  ]
  edge [
    source 10
    target 884
  ]
  edge [
    source 10
    target 885
  ]
  edge [
    source 10
    target 886
  ]
  edge [
    source 10
    target 887
  ]
  edge [
    source 10
    target 888
  ]
  edge [
    source 10
    target 889
  ]
  edge [
    source 10
    target 890
  ]
  edge [
    source 10
    target 891
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 892
  ]
  edge [
    source 10
    target 893
  ]
  edge [
    source 10
    target 894
  ]
  edge [
    source 10
    target 895
  ]
  edge [
    source 10
    target 896
  ]
  edge [
    source 10
    target 897
  ]
  edge [
    source 10
    target 898
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 10
    target 900
  ]
  edge [
    source 10
    target 901
  ]
  edge [
    source 10
    target 902
  ]
  edge [
    source 10
    target 903
  ]
  edge [
    source 10
    target 904
  ]
  edge [
    source 10
    target 905
  ]
  edge [
    source 10
    target 906
  ]
  edge [
    source 10
    target 907
  ]
  edge [
    source 10
    target 908
  ]
  edge [
    source 10
    target 909
  ]
  edge [
    source 10
    target 910
  ]
  edge [
    source 10
    target 911
  ]
  edge [
    source 10
    target 912
  ]
  edge [
    source 10
    target 913
  ]
  edge [
    source 10
    target 914
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 915
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 917
  ]
  edge [
    source 10
    target 918
  ]
  edge [
    source 10
    target 919
  ]
  edge [
    source 10
    target 920
  ]
  edge [
    source 10
    target 921
  ]
  edge [
    source 10
    target 922
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 924
  ]
  edge [
    source 10
    target 925
  ]
  edge [
    source 10
    target 926
  ]
  edge [
    source 10
    target 927
  ]
  edge [
    source 10
    target 928
  ]
  edge [
    source 10
    target 929
  ]
  edge [
    source 10
    target 930
  ]
  edge [
    source 10
    target 931
  ]
  edge [
    source 10
    target 932
  ]
  edge [
    source 10
    target 933
  ]
  edge [
    source 10
    target 934
  ]
  edge [
    source 10
    target 935
  ]
  edge [
    source 10
    target 936
  ]
  edge [
    source 10
    target 937
  ]
  edge [
    source 10
    target 938
  ]
  edge [
    source 10
    target 939
  ]
  edge [
    source 10
    target 940
  ]
  edge [
    source 10
    target 941
  ]
  edge [
    source 10
    target 942
  ]
  edge [
    source 10
    target 943
  ]
  edge [
    source 10
    target 944
  ]
  edge [
    source 10
    target 945
  ]
  edge [
    source 10
    target 946
  ]
  edge [
    source 10
    target 947
  ]
  edge [
    source 10
    target 948
  ]
  edge [
    source 10
    target 949
  ]
  edge [
    source 10
    target 950
  ]
  edge [
    source 10
    target 951
  ]
  edge [
    source 10
    target 952
  ]
  edge [
    source 10
    target 953
  ]
  edge [
    source 10
    target 954
  ]
  edge [
    source 10
    target 955
  ]
  edge [
    source 10
    target 956
  ]
  edge [
    source 10
    target 957
  ]
  edge [
    source 10
    target 958
  ]
  edge [
    source 10
    target 959
  ]
  edge [
    source 10
    target 960
  ]
  edge [
    source 10
    target 961
  ]
  edge [
    source 10
    target 962
  ]
  edge [
    source 10
    target 963
  ]
  edge [
    source 10
    target 964
  ]
  edge [
    source 10
    target 965
  ]
  edge [
    source 10
    target 966
  ]
  edge [
    source 10
    target 967
  ]
  edge [
    source 10
    target 968
  ]
  edge [
    source 10
    target 969
  ]
  edge [
    source 10
    target 970
  ]
  edge [
    source 10
    target 971
  ]
  edge [
    source 10
    target 972
  ]
  edge [
    source 10
    target 973
  ]
  edge [
    source 10
    target 974
  ]
  edge [
    source 10
    target 975
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 976
  ]
  edge [
    source 10
    target 977
  ]
  edge [
    source 10
    target 978
  ]
  edge [
    source 10
    target 979
  ]
  edge [
    source 10
    target 980
  ]
  edge [
    source 10
    target 981
  ]
  edge [
    source 10
    target 982
  ]
  edge [
    source 10
    target 983
  ]
  edge [
    source 10
    target 984
  ]
  edge [
    source 10
    target 985
  ]
  edge [
    source 10
    target 986
  ]
  edge [
    source 10
    target 987
  ]
  edge [
    source 10
    target 988
  ]
  edge [
    source 10
    target 989
  ]
  edge [
    source 10
    target 990
  ]
  edge [
    source 10
    target 991
  ]
  edge [
    source 10
    target 992
  ]
  edge [
    source 10
    target 993
  ]
  edge [
    source 10
    target 994
  ]
  edge [
    source 10
    target 995
  ]
  edge [
    source 10
    target 996
  ]
  edge [
    source 10
    target 997
  ]
  edge [
    source 10
    target 998
  ]
  edge [
    source 10
    target 999
  ]
  edge [
    source 10
    target 1000
  ]
  edge [
    source 10
    target 1001
  ]
  edge [
    source 10
    target 1002
  ]
  edge [
    source 10
    target 1003
  ]
  edge [
    source 10
    target 1004
  ]
  edge [
    source 10
    target 1005
  ]
  edge [
    source 10
    target 1006
  ]
  edge [
    source 10
    target 1007
  ]
  edge [
    source 10
    target 1008
  ]
  edge [
    source 10
    target 1009
  ]
  edge [
    source 10
    target 1010
  ]
  edge [
    source 10
    target 1011
  ]
  edge [
    source 10
    target 1012
  ]
  edge [
    source 10
    target 1013
  ]
  edge [
    source 10
    target 1014
  ]
  edge [
    source 10
    target 1015
  ]
  edge [
    source 10
    target 1016
  ]
  edge [
    source 10
    target 1017
  ]
  edge [
    source 10
    target 1018
  ]
  edge [
    source 10
    target 1019
  ]
  edge [
    source 10
    target 1020
  ]
  edge [
    source 10
    target 1021
  ]
  edge [
    source 10
    target 1022
  ]
  edge [
    source 10
    target 1023
  ]
  edge [
    source 10
    target 1024
  ]
  edge [
    source 10
    target 1025
  ]
  edge [
    source 10
    target 1026
  ]
  edge [
    source 10
    target 1027
  ]
  edge [
    source 10
    target 1028
  ]
  edge [
    source 10
    target 1029
  ]
  edge [
    source 10
    target 1030
  ]
  edge [
    source 10
    target 1031
  ]
  edge [
    source 10
    target 1032
  ]
  edge [
    source 10
    target 1033
  ]
  edge [
    source 10
    target 1034
  ]
  edge [
    source 10
    target 1035
  ]
  edge [
    source 10
    target 1036
  ]
  edge [
    source 10
    target 1037
  ]
  edge [
    source 10
    target 1038
  ]
  edge [
    source 10
    target 1039
  ]
  edge [
    source 10
    target 1040
  ]
  edge [
    source 10
    target 1041
  ]
  edge [
    source 10
    target 1042
  ]
  edge [
    source 10
    target 1043
  ]
  edge [
    source 10
    target 1044
  ]
  edge [
    source 10
    target 1045
  ]
  edge [
    source 10
    target 1046
  ]
  edge [
    source 10
    target 1047
  ]
  edge [
    source 10
    target 1048
  ]
  edge [
    source 10
    target 1049
  ]
  edge [
    source 10
    target 1050
  ]
  edge [
    source 10
    target 1051
  ]
  edge [
    source 10
    target 1052
  ]
  edge [
    source 10
    target 1053
  ]
  edge [
    source 10
    target 1054
  ]
  edge [
    source 10
    target 1055
  ]
  edge [
    source 10
    target 1056
  ]
  edge [
    source 10
    target 1057
  ]
  edge [
    source 10
    target 1058
  ]
  edge [
    source 10
    target 1059
  ]
  edge [
    source 10
    target 1060
  ]
  edge [
    source 10
    target 1061
  ]
  edge [
    source 10
    target 1062
  ]
  edge [
    source 10
    target 1063
  ]
  edge [
    source 10
    target 1064
  ]
  edge [
    source 10
    target 1065
  ]
  edge [
    source 10
    target 1066
  ]
  edge [
    source 10
    target 1067
  ]
  edge [
    source 10
    target 1068
  ]
  edge [
    source 10
    target 1069
  ]
  edge [
    source 10
    target 1070
  ]
  edge [
    source 10
    target 1071
  ]
  edge [
    source 10
    target 1072
  ]
  edge [
    source 10
    target 1073
  ]
  edge [
    source 10
    target 1074
  ]
  edge [
    source 10
    target 1075
  ]
  edge [
    source 10
    target 1076
  ]
  edge [
    source 10
    target 1077
  ]
  edge [
    source 10
    target 1078
  ]
  edge [
    source 10
    target 1079
  ]
  edge [
    source 10
    target 1080
  ]
  edge [
    source 10
    target 1081
  ]
  edge [
    source 10
    target 1082
  ]
  edge [
    source 10
    target 1083
  ]
  edge [
    source 10
    target 1084
  ]
  edge [
    source 10
    target 1085
  ]
  edge [
    source 10
    target 1086
  ]
  edge [
    source 10
    target 1087
  ]
  edge [
    source 10
    target 1088
  ]
  edge [
    source 10
    target 1089
  ]
  edge [
    source 10
    target 1090
  ]
  edge [
    source 10
    target 1091
  ]
  edge [
    source 10
    target 1092
  ]
  edge [
    source 10
    target 1093
  ]
  edge [
    source 10
    target 1094
  ]
  edge [
    source 10
    target 1095
  ]
  edge [
    source 10
    target 1096
  ]
  edge [
    source 10
    target 1097
  ]
  edge [
    source 10
    target 1098
  ]
  edge [
    source 10
    target 1099
  ]
  edge [
    source 10
    target 1100
  ]
  edge [
    source 10
    target 1101
  ]
  edge [
    source 10
    target 1102
  ]
  edge [
    source 10
    target 1103
  ]
  edge [
    source 10
    target 1104
  ]
  edge [
    source 10
    target 1105
  ]
  edge [
    source 10
    target 1106
  ]
  edge [
    source 10
    target 1107
  ]
  edge [
    source 10
    target 1108
  ]
  edge [
    source 10
    target 1109
  ]
  edge [
    source 10
    target 1110
  ]
  edge [
    source 10
    target 1111
  ]
  edge [
    source 10
    target 1112
  ]
  edge [
    source 10
    target 1113
  ]
  edge [
    source 10
    target 1114
  ]
  edge [
    source 10
    target 1115
  ]
  edge [
    source 10
    target 1116
  ]
  edge [
    source 10
    target 1117
  ]
  edge [
    source 10
    target 1118
  ]
  edge [
    source 10
    target 1119
  ]
  edge [
    source 10
    target 1120
  ]
  edge [
    source 10
    target 1121
  ]
  edge [
    source 10
    target 1122
  ]
  edge [
    source 10
    target 1123
  ]
  edge [
    source 10
    target 1124
  ]
  edge [
    source 10
    target 1125
  ]
  edge [
    source 10
    target 1126
  ]
  edge [
    source 10
    target 1127
  ]
  edge [
    source 10
    target 1128
  ]
  edge [
    source 10
    target 1129
  ]
  edge [
    source 10
    target 1130
  ]
  edge [
    source 10
    target 1131
  ]
  edge [
    source 10
    target 1132
  ]
  edge [
    source 10
    target 1133
  ]
  edge [
    source 10
    target 1134
  ]
  edge [
    source 10
    target 1135
  ]
  edge [
    source 10
    target 1136
  ]
  edge [
    source 10
    target 1137
  ]
  edge [
    source 10
    target 1138
  ]
  edge [
    source 10
    target 1139
  ]
  edge [
    source 10
    target 1140
  ]
  edge [
    source 10
    target 1141
  ]
  edge [
    source 10
    target 1142
  ]
  edge [
    source 10
    target 1143
  ]
  edge [
    source 10
    target 1144
  ]
  edge [
    source 10
    target 1145
  ]
  edge [
    source 10
    target 1146
  ]
  edge [
    source 10
    target 1147
  ]
  edge [
    source 10
    target 1148
  ]
  edge [
    source 10
    target 1149
  ]
  edge [
    source 10
    target 1150
  ]
  edge [
    source 10
    target 1151
  ]
  edge [
    source 10
    target 1152
  ]
  edge [
    source 10
    target 1153
  ]
  edge [
    source 10
    target 1154
  ]
  edge [
    source 10
    target 1155
  ]
  edge [
    source 10
    target 1156
  ]
  edge [
    source 10
    target 1157
  ]
  edge [
    source 10
    target 1158
  ]
  edge [
    source 10
    target 1159
  ]
  edge [
    source 10
    target 1160
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 1161
  ]
  edge [
    source 10
    target 1162
  ]
  edge [
    source 10
    target 1163
  ]
  edge [
    source 10
    target 1164
  ]
  edge [
    source 10
    target 1165
  ]
  edge [
    source 10
    target 1166
  ]
  edge [
    source 10
    target 1167
  ]
  edge [
    source 10
    target 1168
  ]
  edge [
    source 10
    target 1169
  ]
  edge [
    source 10
    target 1170
  ]
  edge [
    source 10
    target 1171
  ]
  edge [
    source 10
    target 1172
  ]
  edge [
    source 10
    target 1173
  ]
  edge [
    source 10
    target 1174
  ]
  edge [
    source 10
    target 1175
  ]
  edge [
    source 10
    target 1176
  ]
  edge [
    source 10
    target 1177
  ]
  edge [
    source 10
    target 1178
  ]
  edge [
    source 10
    target 1179
  ]
  edge [
    source 10
    target 1180
  ]
  edge [
    source 10
    target 1181
  ]
  edge [
    source 10
    target 1182
  ]
  edge [
    source 10
    target 1183
  ]
  edge [
    source 10
    target 1184
  ]
  edge [
    source 10
    target 1185
  ]
  edge [
    source 10
    target 1186
  ]
  edge [
    source 10
    target 1187
  ]
  edge [
    source 10
    target 1188
  ]
  edge [
    source 10
    target 1189
  ]
  edge [
    source 10
    target 1190
  ]
  edge [
    source 10
    target 1191
  ]
  edge [
    source 10
    target 1192
  ]
  edge [
    source 10
    target 1193
  ]
  edge [
    source 10
    target 1194
  ]
  edge [
    source 10
    target 1195
  ]
  edge [
    source 10
    target 1196
  ]
  edge [
    source 10
    target 1197
  ]
  edge [
    source 10
    target 1198
  ]
  edge [
    source 10
    target 1199
  ]
  edge [
    source 10
    target 1200
  ]
  edge [
    source 10
    target 1201
  ]
  edge [
    source 10
    target 1202
  ]
  edge [
    source 10
    target 1203
  ]
  edge [
    source 10
    target 1204
  ]
  edge [
    source 10
    target 1205
  ]
  edge [
    source 10
    target 1206
  ]
  edge [
    source 10
    target 1207
  ]
  edge [
    source 10
    target 1208
  ]
  edge [
    source 10
    target 1209
  ]
  edge [
    source 10
    target 1210
  ]
  edge [
    source 10
    target 1211
  ]
  edge [
    source 10
    target 1212
  ]
  edge [
    source 10
    target 1213
  ]
  edge [
    source 10
    target 1214
  ]
  edge [
    source 10
    target 1215
  ]
  edge [
    source 10
    target 1216
  ]
  edge [
    source 10
    target 1217
  ]
  edge [
    source 10
    target 1218
  ]
  edge [
    source 10
    target 1219
  ]
  edge [
    source 10
    target 1220
  ]
  edge [
    source 10
    target 1221
  ]
  edge [
    source 10
    target 1222
  ]
  edge [
    source 10
    target 1223
  ]
  edge [
    source 10
    target 1224
  ]
  edge [
    source 10
    target 1225
  ]
  edge [
    source 10
    target 1226
  ]
  edge [
    source 10
    target 1227
  ]
  edge [
    source 10
    target 1228
  ]
  edge [
    source 10
    target 1229
  ]
  edge [
    source 10
    target 1230
  ]
  edge [
    source 10
    target 1231
  ]
  edge [
    source 10
    target 1232
  ]
  edge [
    source 10
    target 1233
  ]
  edge [
    source 10
    target 1234
  ]
  edge [
    source 10
    target 1235
  ]
  edge [
    source 10
    target 1236
  ]
  edge [
    source 10
    target 1237
  ]
  edge [
    source 10
    target 1238
  ]
  edge [
    source 10
    target 1239
  ]
  edge [
    source 10
    target 1240
  ]
  edge [
    source 10
    target 1241
  ]
  edge [
    source 10
    target 1242
  ]
  edge [
    source 10
    target 1243
  ]
  edge [
    source 10
    target 1244
  ]
  edge [
    source 10
    target 1245
  ]
  edge [
    source 10
    target 1246
  ]
  edge [
    source 10
    target 1247
  ]
  edge [
    source 10
    target 1248
  ]
  edge [
    source 10
    target 1249
  ]
  edge [
    source 10
    target 1250
  ]
  edge [
    source 10
    target 1251
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 1252
  ]
  edge [
    source 10
    target 1253
  ]
  edge [
    source 10
    target 1254
  ]
  edge [
    source 10
    target 1255
  ]
  edge [
    source 10
    target 1256
  ]
  edge [
    source 10
    target 1257
  ]
  edge [
    source 10
    target 1258
  ]
  edge [
    source 10
    target 1259
  ]
  edge [
    source 10
    target 1260
  ]
  edge [
    source 10
    target 1261
  ]
  edge [
    source 10
    target 1262
  ]
  edge [
    source 10
    target 1263
  ]
  edge [
    source 10
    target 1264
  ]
  edge [
    source 10
    target 1265
  ]
  edge [
    source 10
    target 1266
  ]
  edge [
    source 10
    target 1267
  ]
  edge [
    source 10
    target 1268
  ]
  edge [
    source 10
    target 1269
  ]
  edge [
    source 10
    target 1270
  ]
  edge [
    source 10
    target 1271
  ]
  edge [
    source 10
    target 1272
  ]
  edge [
    source 10
    target 1273
  ]
  edge [
    source 10
    target 1274
  ]
  edge [
    source 10
    target 1275
  ]
  edge [
    source 10
    target 1276
  ]
  edge [
    source 10
    target 1277
  ]
  edge [
    source 10
    target 1278
  ]
  edge [
    source 10
    target 1279
  ]
  edge [
    source 10
    target 1280
  ]
  edge [
    source 10
    target 1281
  ]
  edge [
    source 10
    target 1282
  ]
  edge [
    source 10
    target 1283
  ]
  edge [
    source 10
    target 1284
  ]
  edge [
    source 10
    target 1285
  ]
  edge [
    source 10
    target 1286
  ]
  edge [
    source 10
    target 1287
  ]
  edge [
    source 10
    target 1288
  ]
  edge [
    source 10
    target 1289
  ]
  edge [
    source 10
    target 1290
  ]
  edge [
    source 10
    target 1291
  ]
  edge [
    source 10
    target 1292
  ]
  edge [
    source 10
    target 1293
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 1294
  ]
  edge [
    source 11
    target 1295
  ]
  edge [
    source 11
    target 38
  ]
  edge [
    source 11
    target 1296
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 1297
  ]
  edge [
    source 11
    target 1298
  ]
  edge [
    source 11
    target 1299
  ]
  edge [
    source 11
    target 1300
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 1301
  ]
  edge [
    source 11
    target 1302
  ]
  edge [
    source 11
    target 1303
  ]
  edge [
    source 11
    target 1304
  ]
  edge [
    source 11
    target 1305
  ]
  edge [
    source 11
    target 1306
  ]
  edge [
    source 11
    target 1307
  ]
  edge [
    source 11
    target 1308
  ]
  edge [
    source 11
    target 1309
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 1310
  ]
  edge [
    source 11
    target 1311
  ]
  edge [
    source 11
    target 1312
  ]
  edge [
    source 11
    target 1313
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 1314
  ]
  edge [
    source 11
    target 1315
  ]
  edge [
    source 11
    target 1316
  ]
  edge [
    source 11
    target 1317
  ]
  edge [
    source 11
    target 1318
  ]
  edge [
    source 11
    target 1319
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 1320
  ]
  edge [
    source 11
    target 1321
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 1322
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 1323
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 12
    target 1324
  ]
  edge [
    source 12
    target 1325
  ]
  edge [
    source 12
    target 1326
  ]
  edge [
    source 12
    target 1327
  ]
  edge [
    source 12
    target 1328
  ]
  edge [
    source 12
    target 1329
  ]
  edge [
    source 12
    target 1330
  ]
  edge [
    source 12
    target 1331
  ]
  edge [
    source 12
    target 1332
  ]
  edge [
    source 12
    target 1333
  ]
  edge [
    source 12
    target 1334
  ]
  edge [
    source 12
    target 1335
  ]
  edge [
    source 12
    target 1336
  ]
  edge [
    source 12
    target 1337
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 1338
  ]
  edge [
    source 12
    target 1339
  ]
  edge [
    source 12
    target 1340
  ]
  edge [
    source 12
    target 1341
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 1342
  ]
  edge [
    source 12
    target 1343
  ]
  edge [
    source 12
    target 1344
  ]
  edge [
    source 12
    target 1345
  ]
  edge [
    source 12
    target 1346
  ]
  edge [
    source 12
    target 1347
  ]
  edge [
    source 12
    target 1348
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 1349
  ]
  edge [
    source 12
    target 1350
  ]
  edge [
    source 12
    target 1351
  ]
  edge [
    source 12
    target 1352
  ]
  edge [
    source 12
    target 1353
  ]
  edge [
    source 12
    target 1354
  ]
  edge [
    source 12
    target 1355
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1356
  ]
  edge [
    source 12
    target 1357
  ]
  edge [
    source 12
    target 1358
  ]
  edge [
    source 12
    target 1359
  ]
  edge [
    source 12
    target 1360
  ]
  edge [
    source 12
    target 1361
  ]
  edge [
    source 12
    target 1362
  ]
  edge [
    source 12
    target 1363
  ]
  edge [
    source 12
    target 1364
  ]
  edge [
    source 12
    target 1365
  ]
  edge [
    source 12
    target 1366
  ]
  edge [
    source 12
    target 1367
  ]
  edge [
    source 12
    target 1368
  ]
  edge [
    source 12
    target 1369
  ]
  edge [
    source 12
    target 1370
  ]
  edge [
    source 12
    target 1371
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 1372
  ]
  edge [
    source 12
    target 1373
  ]
  edge [
    source 12
    target 1374
  ]
  edge [
    source 12
    target 1375
  ]
  edge [
    source 12
    target 1376
  ]
  edge [
    source 12
    target 1377
  ]
  edge [
    source 12
    target 1378
  ]
  edge [
    source 12
    target 1379
  ]
  edge [
    source 12
    target 1380
  ]
  edge [
    source 12
    target 1381
  ]
  edge [
    source 12
    target 1382
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 1383
  ]
  edge [
    source 12
    target 1384
  ]
  edge [
    source 12
    target 1385
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1386
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 1387
  ]
  edge [
    source 12
    target 1388
  ]
  edge [
    source 12
    target 1389
  ]
  edge [
    source 12
    target 1390
  ]
  edge [
    source 12
    target 1391
  ]
  edge [
    source 12
    target 1392
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 1393
  ]
  edge [
    source 12
    target 1394
  ]
  edge [
    source 12
    target 1395
  ]
  edge [
    source 12
    target 1396
  ]
  edge [
    source 12
    target 1397
  ]
  edge [
    source 12
    target 1398
  ]
  edge [
    source 12
    target 1399
  ]
  edge [
    source 12
    target 1400
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 1401
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 1402
  ]
  edge [
    source 14
    target 1403
  ]
  edge [
    source 14
    target 1404
  ]
  edge [
    source 14
    target 1405
  ]
  edge [
    source 14
    target 1406
  ]
  edge [
    source 14
    target 1407
  ]
  edge [
    source 14
    target 1408
  ]
  edge [
    source 14
    target 1409
  ]
  edge [
    source 14
    target 1410
  ]
  edge [
    source 14
    target 1411
  ]
  edge [
    source 14
    target 1412
  ]
  edge [
    source 14
    target 1413
  ]
  edge [
    source 14
    target 1414
  ]
  edge [
    source 14
    target 1415
  ]
  edge [
    source 14
    target 1416
  ]
  edge [
    source 14
    target 1417
  ]
  edge [
    source 14
    target 1418
  ]
  edge [
    source 14
    target 1419
  ]
  edge [
    source 14
    target 1420
  ]
  edge [
    source 14
    target 1421
  ]
  edge [
    source 14
    target 1422
  ]
  edge [
    source 14
    target 1423
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 1424
  ]
  edge [
    source 16
    target 1425
  ]
  edge [
    source 16
    target 1426
  ]
  edge [
    source 16
    target 1427
  ]
  edge [
    source 16
    target 1428
  ]
  edge [
    source 16
    target 1429
  ]
  edge [
    source 16
    target 1430
  ]
  edge [
    source 16
    target 1431
  ]
  edge [
    source 16
    target 1322
  ]
  edge [
    source 16
    target 1432
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 1433
  ]
  edge [
    source 16
    target 1434
  ]
  edge [
    source 16
    target 1435
  ]
  edge [
    source 16
    target 1436
  ]
  edge [
    source 16
    target 1437
  ]
  edge [
    source 16
    target 1438
  ]
  edge [
    source 16
    target 1439
  ]
  edge [
    source 16
    target 1440
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 1441
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 1442
  ]
  edge [
    source 17
    target 1443
  ]
  edge [
    source 17
    target 319
  ]
  edge [
    source 17
    target 1444
  ]
  edge [
    source 17
    target 1445
  ]
  edge [
    source 17
    target 1446
  ]
  edge [
    source 17
    target 1447
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 1448
  ]
  edge [
    source 17
    target 1449
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
]
