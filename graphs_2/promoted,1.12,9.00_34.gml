graph [
  node [
    id 0
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 1
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 2
    label "zewn&#261;trz"
    origin "text"
  ]
  node [
    id 3
    label "patrze&#263;"
  ]
  node [
    id 4
    label "look"
  ]
  node [
    id 5
    label "czeka&#263;"
  ]
  node [
    id 6
    label "lookout"
  ]
  node [
    id 7
    label "by&#263;"
  ]
  node [
    id 8
    label "wyziera&#263;"
  ]
  node [
    id 9
    label "peep"
  ]
  node [
    id 10
    label "robi&#263;"
  ]
  node [
    id 11
    label "koso"
  ]
  node [
    id 12
    label "punkt_widzenia"
  ]
  node [
    id 13
    label "uwa&#380;a&#263;"
  ]
  node [
    id 14
    label "go_steady"
  ]
  node [
    id 15
    label "szuka&#263;"
  ]
  node [
    id 16
    label "dba&#263;"
  ]
  node [
    id 17
    label "os&#261;dza&#263;"
  ]
  node [
    id 18
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 19
    label "pogl&#261;da&#263;"
  ]
  node [
    id 20
    label "traktowa&#263;"
  ]
  node [
    id 21
    label "hold"
  ]
  node [
    id 22
    label "decydowa&#263;"
  ]
  node [
    id 23
    label "anticipate"
  ]
  node [
    id 24
    label "pauzowa&#263;"
  ]
  node [
    id 25
    label "oczekiwa&#263;"
  ]
  node [
    id 26
    label "sp&#281;dza&#263;"
  ]
  node [
    id 27
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 28
    label "stan"
  ]
  node [
    id 29
    label "stand"
  ]
  node [
    id 30
    label "trwa&#263;"
  ]
  node [
    id 31
    label "equal"
  ]
  node [
    id 32
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 33
    label "chodzi&#263;"
  ]
  node [
    id 34
    label "uczestniczy&#263;"
  ]
  node [
    id 35
    label "obecno&#347;&#263;"
  ]
  node [
    id 36
    label "si&#281;ga&#263;"
  ]
  node [
    id 37
    label "mie&#263;_miejsce"
  ]
  node [
    id 38
    label "stylizacja"
  ]
  node [
    id 39
    label "wygl&#261;d"
  ]
  node [
    id 40
    label "punkt"
  ]
  node [
    id 41
    label "spos&#243;b"
  ]
  node [
    id 42
    label "chemikalia"
  ]
  node [
    id 43
    label "abstrakcja"
  ]
  node [
    id 44
    label "miejsce"
  ]
  node [
    id 45
    label "czas"
  ]
  node [
    id 46
    label "substancja"
  ]
  node [
    id 47
    label "chronometria"
  ]
  node [
    id 48
    label "odczyt"
  ]
  node [
    id 49
    label "laba"
  ]
  node [
    id 50
    label "czasoprzestrze&#324;"
  ]
  node [
    id 51
    label "time_period"
  ]
  node [
    id 52
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 53
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 54
    label "Zeitgeist"
  ]
  node [
    id 55
    label "pochodzenie"
  ]
  node [
    id 56
    label "przep&#322;ywanie"
  ]
  node [
    id 57
    label "schy&#322;ek"
  ]
  node [
    id 58
    label "czwarty_wymiar"
  ]
  node [
    id 59
    label "kategoria_gramatyczna"
  ]
  node [
    id 60
    label "poprzedzi&#263;"
  ]
  node [
    id 61
    label "pogoda"
  ]
  node [
    id 62
    label "czasokres"
  ]
  node [
    id 63
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 64
    label "poprzedzenie"
  ]
  node [
    id 65
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 66
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 67
    label "dzieje"
  ]
  node [
    id 68
    label "zegar"
  ]
  node [
    id 69
    label "koniugacja"
  ]
  node [
    id 70
    label "trawi&#263;"
  ]
  node [
    id 71
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 72
    label "poprzedza&#263;"
  ]
  node [
    id 73
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 74
    label "trawienie"
  ]
  node [
    id 75
    label "chwila"
  ]
  node [
    id 76
    label "rachuba_czasu"
  ]
  node [
    id 77
    label "poprzedzanie"
  ]
  node [
    id 78
    label "okres_czasu"
  ]
  node [
    id 79
    label "period"
  ]
  node [
    id 80
    label "odwlekanie_si&#281;"
  ]
  node [
    id 81
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 82
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 83
    label "pochodzi&#263;"
  ]
  node [
    id 84
    label "zbi&#243;r"
  ]
  node [
    id 85
    label "model"
  ]
  node [
    id 86
    label "narz&#281;dzie"
  ]
  node [
    id 87
    label "nature"
  ]
  node [
    id 88
    label "tryb"
  ]
  node [
    id 89
    label "obiekt_matematyczny"
  ]
  node [
    id 90
    label "stopie&#324;_pisma"
  ]
  node [
    id 91
    label "pozycja"
  ]
  node [
    id 92
    label "problemat"
  ]
  node [
    id 93
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 94
    label "obiekt"
  ]
  node [
    id 95
    label "point"
  ]
  node [
    id 96
    label "plamka"
  ]
  node [
    id 97
    label "przestrze&#324;"
  ]
  node [
    id 98
    label "mark"
  ]
  node [
    id 99
    label "ust&#281;p"
  ]
  node [
    id 100
    label "po&#322;o&#380;enie"
  ]
  node [
    id 101
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 102
    label "kres"
  ]
  node [
    id 103
    label "plan"
  ]
  node [
    id 104
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 105
    label "podpunkt"
  ]
  node [
    id 106
    label "jednostka"
  ]
  node [
    id 107
    label "sprawa"
  ]
  node [
    id 108
    label "problematyka"
  ]
  node [
    id 109
    label "prosta"
  ]
  node [
    id 110
    label "wojsko"
  ]
  node [
    id 111
    label "zapunktowa&#263;"
  ]
  node [
    id 112
    label "rz&#261;d"
  ]
  node [
    id 113
    label "uwaga"
  ]
  node [
    id 114
    label "cecha"
  ]
  node [
    id 115
    label "praca"
  ]
  node [
    id 116
    label "plac"
  ]
  node [
    id 117
    label "location"
  ]
  node [
    id 118
    label "warunek_lokalowy"
  ]
  node [
    id 119
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 120
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 121
    label "cia&#322;o"
  ]
  node [
    id 122
    label "status"
  ]
  node [
    id 123
    label "temperatura_krytyczna"
  ]
  node [
    id 124
    label "materia"
  ]
  node [
    id 125
    label "smolisty"
  ]
  node [
    id 126
    label "byt"
  ]
  node [
    id 127
    label "przenika&#263;"
  ]
  node [
    id 128
    label "cz&#261;steczka"
  ]
  node [
    id 129
    label "przenikanie"
  ]
  node [
    id 130
    label "poj&#281;cie"
  ]
  node [
    id 131
    label "proces_my&#347;lowy"
  ]
  node [
    id 132
    label "abstractedness"
  ]
  node [
    id 133
    label "obraz"
  ]
  node [
    id 134
    label "abstraction"
  ]
  node [
    id 135
    label "sytuacja"
  ]
  node [
    id 136
    label "spalanie"
  ]
  node [
    id 137
    label "spalenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
]
