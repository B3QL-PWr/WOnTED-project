graph [
  node [
    id 0
    label "ulubiony"
    origin "text"
  ]
  node [
    id 1
    label "tost"
    origin "text"
  ]
  node [
    id 2
    label "heheszki"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "memy"
    origin "text"
  ]
  node [
    id 5
    label "wyj&#261;tkowy"
  ]
  node [
    id 6
    label "faworytny"
  ]
  node [
    id 7
    label "wyj&#261;tkowo"
  ]
  node [
    id 8
    label "inny"
  ]
  node [
    id 9
    label "ukochany"
  ]
  node [
    id 10
    label "przypiec"
  ]
  node [
    id 11
    label "kromka"
  ]
  node [
    id 12
    label "porcja"
  ]
  node [
    id 13
    label "upiec"
  ]
  node [
    id 14
    label "pledge"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
]
