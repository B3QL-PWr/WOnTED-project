graph [
  node [
    id 0
    label "daleki"
    origin "text"
  ]
  node [
    id 1
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wczesno"
    origin "text"
  ]
  node [
    id 3
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sprawa"
    origin "text"
  ]
  node [
    id 5
    label "molestowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "latki"
    origin "text"
  ]
  node [
    id 7
    label "komenda"
    origin "text"
  ]
  node [
    id 8
    label "sto&#322;eczny"
    origin "text"
  ]
  node [
    id 9
    label "policja"
    origin "text"
  ]
  node [
    id 10
    label "dawny"
  ]
  node [
    id 11
    label "ogl&#281;dny"
  ]
  node [
    id 12
    label "d&#322;ugi"
  ]
  node [
    id 13
    label "du&#380;y"
  ]
  node [
    id 14
    label "daleko"
  ]
  node [
    id 15
    label "odleg&#322;y"
  ]
  node [
    id 16
    label "zwi&#261;zany"
  ]
  node [
    id 17
    label "r&#243;&#380;ny"
  ]
  node [
    id 18
    label "s&#322;aby"
  ]
  node [
    id 19
    label "odlegle"
  ]
  node [
    id 20
    label "oddalony"
  ]
  node [
    id 21
    label "g&#322;&#281;boki"
  ]
  node [
    id 22
    label "obcy"
  ]
  node [
    id 23
    label "nieobecny"
  ]
  node [
    id 24
    label "przysz&#322;y"
  ]
  node [
    id 25
    label "nadprzyrodzony"
  ]
  node [
    id 26
    label "nieznany"
  ]
  node [
    id 27
    label "pozaludzki"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "obco"
  ]
  node [
    id 30
    label "tameczny"
  ]
  node [
    id 31
    label "osoba"
  ]
  node [
    id 32
    label "nieznajomo"
  ]
  node [
    id 33
    label "inny"
  ]
  node [
    id 34
    label "cudzy"
  ]
  node [
    id 35
    label "istota_&#380;ywa"
  ]
  node [
    id 36
    label "zaziemsko"
  ]
  node [
    id 37
    label "doros&#322;y"
  ]
  node [
    id 38
    label "znaczny"
  ]
  node [
    id 39
    label "niema&#322;o"
  ]
  node [
    id 40
    label "wiele"
  ]
  node [
    id 41
    label "rozwini&#281;ty"
  ]
  node [
    id 42
    label "dorodny"
  ]
  node [
    id 43
    label "wa&#380;ny"
  ]
  node [
    id 44
    label "prawdziwy"
  ]
  node [
    id 45
    label "du&#380;o"
  ]
  node [
    id 46
    label "delikatny"
  ]
  node [
    id 47
    label "kolejny"
  ]
  node [
    id 48
    label "nieprzytomny"
  ]
  node [
    id 49
    label "opuszczenie"
  ]
  node [
    id 50
    label "stan"
  ]
  node [
    id 51
    label "opuszczanie"
  ]
  node [
    id 52
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 53
    label "po&#322;&#261;czenie"
  ]
  node [
    id 54
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 55
    label "nietrwa&#322;y"
  ]
  node [
    id 56
    label "mizerny"
  ]
  node [
    id 57
    label "marnie"
  ]
  node [
    id 58
    label "po&#347;ledni"
  ]
  node [
    id 59
    label "niezdrowy"
  ]
  node [
    id 60
    label "z&#322;y"
  ]
  node [
    id 61
    label "nieumiej&#281;tny"
  ]
  node [
    id 62
    label "s&#322;abo"
  ]
  node [
    id 63
    label "nieznaczny"
  ]
  node [
    id 64
    label "lura"
  ]
  node [
    id 65
    label "nieudany"
  ]
  node [
    id 66
    label "s&#322;abowity"
  ]
  node [
    id 67
    label "zawodny"
  ]
  node [
    id 68
    label "&#322;agodny"
  ]
  node [
    id 69
    label "md&#322;y"
  ]
  node [
    id 70
    label "niedoskona&#322;y"
  ]
  node [
    id 71
    label "przemijaj&#261;cy"
  ]
  node [
    id 72
    label "niemocny"
  ]
  node [
    id 73
    label "niefajny"
  ]
  node [
    id 74
    label "kiepsko"
  ]
  node [
    id 75
    label "przestarza&#322;y"
  ]
  node [
    id 76
    label "przesz&#322;y"
  ]
  node [
    id 77
    label "od_dawna"
  ]
  node [
    id 78
    label "poprzedni"
  ]
  node [
    id 79
    label "dawno"
  ]
  node [
    id 80
    label "d&#322;ugoletni"
  ]
  node [
    id 81
    label "anachroniczny"
  ]
  node [
    id 82
    label "dawniej"
  ]
  node [
    id 83
    label "niegdysiejszy"
  ]
  node [
    id 84
    label "wcze&#347;niejszy"
  ]
  node [
    id 85
    label "kombatant"
  ]
  node [
    id 86
    label "stary"
  ]
  node [
    id 87
    label "ogl&#281;dnie"
  ]
  node [
    id 88
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 89
    label "stosowny"
  ]
  node [
    id 90
    label "ch&#322;odny"
  ]
  node [
    id 91
    label "og&#243;lny"
  ]
  node [
    id 92
    label "okr&#261;g&#322;y"
  ]
  node [
    id 93
    label "byle_jaki"
  ]
  node [
    id 94
    label "niedok&#322;adny"
  ]
  node [
    id 95
    label "cnotliwy"
  ]
  node [
    id 96
    label "intensywny"
  ]
  node [
    id 97
    label "gruntowny"
  ]
  node [
    id 98
    label "mocny"
  ]
  node [
    id 99
    label "szczery"
  ]
  node [
    id 100
    label "ukryty"
  ]
  node [
    id 101
    label "silny"
  ]
  node [
    id 102
    label "wyrazisty"
  ]
  node [
    id 103
    label "dog&#322;&#281;bny"
  ]
  node [
    id 104
    label "g&#322;&#281;boko"
  ]
  node [
    id 105
    label "niezrozumia&#322;y"
  ]
  node [
    id 106
    label "niski"
  ]
  node [
    id 107
    label "m&#261;dry"
  ]
  node [
    id 108
    label "oderwany"
  ]
  node [
    id 109
    label "jaki&#347;"
  ]
  node [
    id 110
    label "r&#243;&#380;nie"
  ]
  node [
    id 111
    label "nisko"
  ]
  node [
    id 112
    label "znacznie"
  ]
  node [
    id 113
    label "het"
  ]
  node [
    id 114
    label "nieobecnie"
  ]
  node [
    id 115
    label "wysoko"
  ]
  node [
    id 116
    label "ruch"
  ]
  node [
    id 117
    label "d&#322;ugo"
  ]
  node [
    id 118
    label "Rzym_Zachodni"
  ]
  node [
    id 119
    label "whole"
  ]
  node [
    id 120
    label "ilo&#347;&#263;"
  ]
  node [
    id 121
    label "element"
  ]
  node [
    id 122
    label "Rzym_Wschodni"
  ]
  node [
    id 123
    label "urz&#261;dzenie"
  ]
  node [
    id 124
    label "r&#243;&#380;niczka"
  ]
  node [
    id 125
    label "&#347;rodowisko"
  ]
  node [
    id 126
    label "przedmiot"
  ]
  node [
    id 127
    label "materia"
  ]
  node [
    id 128
    label "szambo"
  ]
  node [
    id 129
    label "aspo&#322;eczny"
  ]
  node [
    id 130
    label "component"
  ]
  node [
    id 131
    label "szkodnik"
  ]
  node [
    id 132
    label "gangsterski"
  ]
  node [
    id 133
    label "poj&#281;cie"
  ]
  node [
    id 134
    label "underworld"
  ]
  node [
    id 135
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 136
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 137
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 138
    label "rozmiar"
  ]
  node [
    id 139
    label "part"
  ]
  node [
    id 140
    label "kom&#243;rka"
  ]
  node [
    id 141
    label "furnishing"
  ]
  node [
    id 142
    label "zabezpieczenie"
  ]
  node [
    id 143
    label "zrobienie"
  ]
  node [
    id 144
    label "wyrz&#261;dzenie"
  ]
  node [
    id 145
    label "zagospodarowanie"
  ]
  node [
    id 146
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 147
    label "ig&#322;a"
  ]
  node [
    id 148
    label "narz&#281;dzie"
  ]
  node [
    id 149
    label "wirnik"
  ]
  node [
    id 150
    label "aparatura"
  ]
  node [
    id 151
    label "system_energetyczny"
  ]
  node [
    id 152
    label "impulsator"
  ]
  node [
    id 153
    label "mechanizm"
  ]
  node [
    id 154
    label "sprz&#281;t"
  ]
  node [
    id 155
    label "czynno&#347;&#263;"
  ]
  node [
    id 156
    label "blokowanie"
  ]
  node [
    id 157
    label "set"
  ]
  node [
    id 158
    label "zablokowanie"
  ]
  node [
    id 159
    label "przygotowanie"
  ]
  node [
    id 160
    label "komora"
  ]
  node [
    id 161
    label "j&#281;zyk"
  ]
  node [
    id 162
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 163
    label "wcze&#347;nie"
  ]
  node [
    id 164
    label "wczesny"
  ]
  node [
    id 165
    label "zapoznawa&#263;"
  ]
  node [
    id 166
    label "represent"
  ]
  node [
    id 167
    label "zawiera&#263;"
  ]
  node [
    id 168
    label "poznawa&#263;"
  ]
  node [
    id 169
    label "obznajamia&#263;"
  ]
  node [
    id 170
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 171
    label "go_steady"
  ]
  node [
    id 172
    label "informowa&#263;"
  ]
  node [
    id 173
    label "kognicja"
  ]
  node [
    id 174
    label "object"
  ]
  node [
    id 175
    label "rozprawa"
  ]
  node [
    id 176
    label "temat"
  ]
  node [
    id 177
    label "wydarzenie"
  ]
  node [
    id 178
    label "szczeg&#243;&#322;"
  ]
  node [
    id 179
    label "proposition"
  ]
  node [
    id 180
    label "przes&#322;anka"
  ]
  node [
    id 181
    label "rzecz"
  ]
  node [
    id 182
    label "idea"
  ]
  node [
    id 183
    label "przebiec"
  ]
  node [
    id 184
    label "charakter"
  ]
  node [
    id 185
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 186
    label "motyw"
  ]
  node [
    id 187
    label "przebiegni&#281;cie"
  ]
  node [
    id 188
    label "fabu&#322;a"
  ]
  node [
    id 189
    label "ideologia"
  ]
  node [
    id 190
    label "byt"
  ]
  node [
    id 191
    label "intelekt"
  ]
  node [
    id 192
    label "Kant"
  ]
  node [
    id 193
    label "p&#322;&#243;d"
  ]
  node [
    id 194
    label "cel"
  ]
  node [
    id 195
    label "istota"
  ]
  node [
    id 196
    label "pomys&#322;"
  ]
  node [
    id 197
    label "ideacja"
  ]
  node [
    id 198
    label "wpadni&#281;cie"
  ]
  node [
    id 199
    label "mienie"
  ]
  node [
    id 200
    label "przyroda"
  ]
  node [
    id 201
    label "obiekt"
  ]
  node [
    id 202
    label "kultura"
  ]
  node [
    id 203
    label "wpa&#347;&#263;"
  ]
  node [
    id 204
    label "wpadanie"
  ]
  node [
    id 205
    label "wpada&#263;"
  ]
  node [
    id 206
    label "s&#261;d"
  ]
  node [
    id 207
    label "rozumowanie"
  ]
  node [
    id 208
    label "opracowanie"
  ]
  node [
    id 209
    label "proces"
  ]
  node [
    id 210
    label "obrady"
  ]
  node [
    id 211
    label "cytat"
  ]
  node [
    id 212
    label "tekst"
  ]
  node [
    id 213
    label "obja&#347;nienie"
  ]
  node [
    id 214
    label "s&#261;dzenie"
  ]
  node [
    id 215
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 216
    label "niuansowa&#263;"
  ]
  node [
    id 217
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 218
    label "sk&#322;adnik"
  ]
  node [
    id 219
    label "zniuansowa&#263;"
  ]
  node [
    id 220
    label "fakt"
  ]
  node [
    id 221
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 222
    label "przyczyna"
  ]
  node [
    id 223
    label "wnioskowanie"
  ]
  node [
    id 224
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 225
    label "wyraz_pochodny"
  ]
  node [
    id 226
    label "zboczenie"
  ]
  node [
    id 227
    label "om&#243;wienie"
  ]
  node [
    id 228
    label "cecha"
  ]
  node [
    id 229
    label "omawia&#263;"
  ]
  node [
    id 230
    label "fraza"
  ]
  node [
    id 231
    label "tre&#347;&#263;"
  ]
  node [
    id 232
    label "entity"
  ]
  node [
    id 233
    label "forum"
  ]
  node [
    id 234
    label "topik"
  ]
  node [
    id 235
    label "tematyka"
  ]
  node [
    id 236
    label "w&#261;tek"
  ]
  node [
    id 237
    label "zbaczanie"
  ]
  node [
    id 238
    label "forma"
  ]
  node [
    id 239
    label "om&#243;wi&#263;"
  ]
  node [
    id 240
    label "omawianie"
  ]
  node [
    id 241
    label "melodia"
  ]
  node [
    id 242
    label "otoczka"
  ]
  node [
    id 243
    label "zbacza&#263;"
  ]
  node [
    id 244
    label "zboczy&#263;"
  ]
  node [
    id 245
    label "zmusza&#263;"
  ]
  node [
    id 246
    label "nudzi&#263;"
  ]
  node [
    id 247
    label "prosi&#263;"
  ]
  node [
    id 248
    label "trouble_oneself"
  ]
  node [
    id 249
    label "wykorzystywa&#263;"
  ]
  node [
    id 250
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 251
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 252
    label "sandbag"
  ]
  node [
    id 253
    label "powodowa&#263;"
  ]
  node [
    id 254
    label "korzysta&#263;"
  ]
  node [
    id 255
    label "liga&#263;"
  ]
  node [
    id 256
    label "give"
  ]
  node [
    id 257
    label "distribute"
  ]
  node [
    id 258
    label "u&#380;ywa&#263;"
  ]
  node [
    id 259
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 260
    label "use"
  ]
  node [
    id 261
    label "krzywdzi&#263;"
  ]
  node [
    id 262
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 263
    label "invite"
  ]
  node [
    id 264
    label "poleca&#263;"
  ]
  node [
    id 265
    label "trwa&#263;"
  ]
  node [
    id 266
    label "zaprasza&#263;"
  ]
  node [
    id 267
    label "zach&#281;ca&#263;"
  ]
  node [
    id 268
    label "suffice"
  ]
  node [
    id 269
    label "preach"
  ]
  node [
    id 270
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 271
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 272
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 273
    label "pies"
  ]
  node [
    id 274
    label "zezwala&#263;"
  ]
  node [
    id 275
    label "ask"
  ]
  node [
    id 276
    label "harass"
  ]
  node [
    id 277
    label "naprzykrza&#263;_si&#281;"
  ]
  node [
    id 278
    label "gada&#263;"
  ]
  node [
    id 279
    label "nalega&#263;"
  ]
  node [
    id 280
    label "rant"
  ]
  node [
    id 281
    label "buttonhole"
  ]
  node [
    id 282
    label "wzbudza&#263;"
  ]
  node [
    id 283
    label "narzeka&#263;"
  ]
  node [
    id 284
    label "chat_up"
  ]
  node [
    id 285
    label "komender&#243;wka"
  ]
  node [
    id 286
    label "polecenie"
  ]
  node [
    id 287
    label "formu&#322;a"
  ]
  node [
    id 288
    label "sygna&#322;"
  ]
  node [
    id 289
    label "posterunek"
  ]
  node [
    id 290
    label "psiarnia"
  ]
  node [
    id 291
    label "direction"
  ]
  node [
    id 292
    label "awansowa&#263;"
  ]
  node [
    id 293
    label "stawia&#263;"
  ]
  node [
    id 294
    label "wakowa&#263;"
  ]
  node [
    id 295
    label "powierzanie"
  ]
  node [
    id 296
    label "postawi&#263;"
  ]
  node [
    id 297
    label "pozycja"
  ]
  node [
    id 298
    label "agencja"
  ]
  node [
    id 299
    label "awansowanie"
  ]
  node [
    id 300
    label "warta"
  ]
  node [
    id 301
    label "praca"
  ]
  node [
    id 302
    label "ukaz"
  ]
  node [
    id 303
    label "pognanie"
  ]
  node [
    id 304
    label "rekomendacja"
  ]
  node [
    id 305
    label "wypowied&#378;"
  ]
  node [
    id 306
    label "pobiegni&#281;cie"
  ]
  node [
    id 307
    label "education"
  ]
  node [
    id 308
    label "doradzenie"
  ]
  node [
    id 309
    label "statement"
  ]
  node [
    id 310
    label "recommendation"
  ]
  node [
    id 311
    label "zadanie"
  ]
  node [
    id 312
    label "zaordynowanie"
  ]
  node [
    id 313
    label "powierzenie"
  ]
  node [
    id 314
    label "przesadzenie"
  ]
  node [
    id 315
    label "consign"
  ]
  node [
    id 316
    label "przekazywa&#263;"
  ]
  node [
    id 317
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 318
    label "pulsation"
  ]
  node [
    id 319
    label "przekazywanie"
  ]
  node [
    id 320
    label "przewodzenie"
  ]
  node [
    id 321
    label "d&#378;wi&#281;k"
  ]
  node [
    id 322
    label "fala"
  ]
  node [
    id 323
    label "doj&#347;cie"
  ]
  node [
    id 324
    label "przekazanie"
  ]
  node [
    id 325
    label "przewodzi&#263;"
  ]
  node [
    id 326
    label "znak"
  ]
  node [
    id 327
    label "zapowied&#378;"
  ]
  node [
    id 328
    label "medium_transmisyjne"
  ]
  node [
    id 329
    label "demodulacja"
  ]
  node [
    id 330
    label "doj&#347;&#263;"
  ]
  node [
    id 331
    label "przekaza&#263;"
  ]
  node [
    id 332
    label "czynnik"
  ]
  node [
    id 333
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 334
    label "aliasing"
  ]
  node [
    id 335
    label "wizja"
  ]
  node [
    id 336
    label "modulacja"
  ]
  node [
    id 337
    label "point"
  ]
  node [
    id 338
    label "drift"
  ]
  node [
    id 339
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 340
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 341
    label "zapis"
  ]
  node [
    id 342
    label "formularz"
  ]
  node [
    id 343
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 344
    label "sformu&#322;owanie"
  ]
  node [
    id 345
    label "kultura_duchowa"
  ]
  node [
    id 346
    label "rule"
  ]
  node [
    id 347
    label "ceremony"
  ]
  node [
    id 348
    label "komisariat"
  ]
  node [
    id 349
    label "zesp&#243;&#322;"
  ]
  node [
    id 350
    label "stalag"
  ]
  node [
    id 351
    label "rozkaz"
  ]
  node [
    id 352
    label "miejski"
  ]
  node [
    id 353
    label "sto&#322;ecznie"
  ]
  node [
    id 354
    label "publiczny"
  ]
  node [
    id 355
    label "typowy"
  ]
  node [
    id 356
    label "miastowy"
  ]
  node [
    id 357
    label "miejsko"
  ]
  node [
    id 358
    label "organ"
  ]
  node [
    id 359
    label "grupa"
  ]
  node [
    id 360
    label "s&#322;u&#380;ba"
  ]
  node [
    id 361
    label "tkanka"
  ]
  node [
    id 362
    label "jednostka_organizacyjna"
  ]
  node [
    id 363
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 364
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 365
    label "tw&#243;r"
  ]
  node [
    id 366
    label "organogeneza"
  ]
  node [
    id 367
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 368
    label "struktura_anatomiczna"
  ]
  node [
    id 369
    label "uk&#322;ad"
  ]
  node [
    id 370
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 371
    label "dekortykacja"
  ]
  node [
    id 372
    label "Izba_Konsyliarska"
  ]
  node [
    id 373
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 374
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 375
    label "stomia"
  ]
  node [
    id 376
    label "budowa"
  ]
  node [
    id 377
    label "okolica"
  ]
  node [
    id 378
    label "Komitet_Region&#243;w"
  ]
  node [
    id 379
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 380
    label "instytucja"
  ]
  node [
    id 381
    label "wys&#322;uga"
  ]
  node [
    id 382
    label "service"
  ]
  node [
    id 383
    label "czworak"
  ]
  node [
    id 384
    label "ZOMO"
  ]
  node [
    id 385
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 386
    label "odm&#322;adzanie"
  ]
  node [
    id 387
    label "liga"
  ]
  node [
    id 388
    label "jednostka_systematyczna"
  ]
  node [
    id 389
    label "asymilowanie"
  ]
  node [
    id 390
    label "gromada"
  ]
  node [
    id 391
    label "asymilowa&#263;"
  ]
  node [
    id 392
    label "egzemplarz"
  ]
  node [
    id 393
    label "Entuzjastki"
  ]
  node [
    id 394
    label "zbi&#243;r"
  ]
  node [
    id 395
    label "kompozycja"
  ]
  node [
    id 396
    label "Terranie"
  ]
  node [
    id 397
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 398
    label "category"
  ]
  node [
    id 399
    label "pakiet_klimatyczny"
  ]
  node [
    id 400
    label "oddzia&#322;"
  ]
  node [
    id 401
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 402
    label "cz&#261;steczka"
  ]
  node [
    id 403
    label "stage_set"
  ]
  node [
    id 404
    label "type"
  ]
  node [
    id 405
    label "specgrupa"
  ]
  node [
    id 406
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 407
    label "&#346;wietliki"
  ]
  node [
    id 408
    label "odm&#322;odzenie"
  ]
  node [
    id 409
    label "Eurogrupa"
  ]
  node [
    id 410
    label "odm&#322;adza&#263;"
  ]
  node [
    id 411
    label "formacja_geologiczna"
  ]
  node [
    id 412
    label "harcerze_starsi"
  ]
  node [
    id 413
    label "urz&#261;d"
  ]
  node [
    id 414
    label "jednostka"
  ]
  node [
    id 415
    label "czasowy"
  ]
  node [
    id 416
    label "commissariat"
  ]
  node [
    id 417
    label "rewir"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
]
