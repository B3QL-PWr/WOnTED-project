graph [
  node [
    id 0
    label "promieniowanie"
    origin "text"
  ]
  node [
    id 1
    label "energia"
  ]
  node [
    id 2
    label "wysy&#322;anie"
  ]
  node [
    id 3
    label "aktynometr"
  ]
  node [
    id 4
    label "emanowanie"
  ]
  node [
    id 5
    label "irradiacja"
  ]
  node [
    id 6
    label "radiation"
  ]
  node [
    id 7
    label "radiotherapy"
  ]
  node [
    id 8
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 9
    label "radiance"
  ]
  node [
    id 10
    label "oddzia&#322;ywanie"
  ]
  node [
    id 11
    label "radiesteta"
  ]
  node [
    id 12
    label "wypromieniowywanie"
  ]
  node [
    id 13
    label "zjawisko"
  ]
  node [
    id 14
    label "emission"
  ]
  node [
    id 15
    label "narz&#261;d_krytyczny"
  ]
  node [
    id 16
    label "nakazywanie"
  ]
  node [
    id 17
    label "cover"
  ]
  node [
    id 18
    label "wytwarzanie"
  ]
  node [
    id 19
    label "transmission"
  ]
  node [
    id 20
    label "przekazywanie"
  ]
  node [
    id 21
    label "wydzielanie_si&#281;"
  ]
  node [
    id 22
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 23
    label "okazywanie"
  ]
  node [
    id 24
    label "wydzielanie"
  ]
  node [
    id 25
    label "powodowanie"
  ]
  node [
    id 26
    label "hipnotyzowanie"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "&#347;lad"
  ]
  node [
    id 29
    label "docieranie"
  ]
  node [
    id 30
    label "natural_process"
  ]
  node [
    id 31
    label "reakcja_chemiczna"
  ]
  node [
    id 32
    label "wdzieranie_si&#281;"
  ]
  node [
    id 33
    label "act"
  ]
  node [
    id 34
    label "rezultat"
  ]
  node [
    id 35
    label "lobbysta"
  ]
  node [
    id 36
    label "proces"
  ]
  node [
    id 37
    label "boski"
  ]
  node [
    id 38
    label "krajobraz"
  ]
  node [
    id 39
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 40
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 41
    label "przywidzenie"
  ]
  node [
    id 42
    label "presence"
  ]
  node [
    id 43
    label "charakter"
  ]
  node [
    id 44
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 45
    label "asocjacja"
  ]
  node [
    id 46
    label "b&#243;l"
  ]
  node [
    id 47
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 48
    label "pobudzenie"
  ]
  node [
    id 49
    label "oznaka"
  ]
  node [
    id 50
    label "irradiancja"
  ]
  node [
    id 51
    label "optyka"
  ]
  node [
    id 52
    label "zasada"
  ]
  node [
    id 53
    label "promieniowa&#263;"
  ]
  node [
    id 54
    label "konserwacja"
  ]
  node [
    id 55
    label "z&#322;udzenie"
  ]
  node [
    id 56
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 57
    label "actinometer"
  ]
  node [
    id 58
    label "bioenergoterapeuta"
  ]
  node [
    id 59
    label "psychotronik"
  ]
  node [
    id 60
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 61
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 62
    label "egzergia"
  ]
  node [
    id 63
    label "emitowa&#263;"
  ]
  node [
    id 64
    label "kwant_energii"
  ]
  node [
    id 65
    label "szwung"
  ]
  node [
    id 66
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 67
    label "power"
  ]
  node [
    id 68
    label "cecha"
  ]
  node [
    id 69
    label "emitowanie"
  ]
  node [
    id 70
    label "energy"
  ]
  node [
    id 71
    label "promie&#324;"
  ]
  node [
    id 72
    label "Roentgen"
  ]
  node [
    id 73
    label "ksi&#261;&#380;&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
]
