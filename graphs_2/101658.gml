graph [
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 2
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 3
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "konsekwencja"
    origin "text"
  ]
  node [
    id 5
    label "p&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "tychy"
    origin "text"
  ]
  node [
    id 7
    label "sk&#322;adka"
    origin "text"
  ]
  node [
    id 8
    label "druga"
    origin "text"
  ]
  node [
    id 9
    label "strona"
    origin "text"
  ]
  node [
    id 10
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 11
    label "emerytalno"
    origin "text"
  ]
  node [
    id 12
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 13
    label "bardzo"
    origin "text"
  ]
  node [
    id 14
    label "rzadko"
    origin "text"
  ]
  node [
    id 15
    label "tym"
    origin "text"
  ]
  node [
    id 16
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "rocznik"
    origin "text"
  ]
  node [
    id 18
    label "przeci&#281;tna"
    origin "text"
  ]
  node [
    id 19
    label "system"
    origin "text"
  ]
  node [
    id 20
    label "ubezpieczenie"
    origin "text"
  ]
  node [
    id 21
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 22
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 24
    label "ale"
    origin "text"
  ]
  node [
    id 25
    label "przypadek"
    origin "text"
  ]
  node [
    id 26
    label "osoba"
    origin "text"
  ]
  node [
    id 27
    label "prowadz&#261;ca"
    origin "text"
  ]
  node [
    id 28
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "gospodarczy"
    origin "text"
  ]
  node [
    id 30
    label "emerytura"
    origin "text"
  ]
  node [
    id 31
    label "renta"
    origin "text"
  ]
  node [
    id 32
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 33
    label "niezdolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 34
    label "praca"
    origin "text"
  ]
  node [
    id 35
    label "og&#243;lnie"
    origin "text"
  ]
  node [
    id 36
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 37
    label "popatrze&#263;"
    origin "text"
  ]
  node [
    id 38
    label "jak"
    origin "text"
  ]
  node [
    id 39
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 40
    label "niemal"
    origin "text"
  ]
  node [
    id 41
    label "pobiera&#263;"
    origin "text"
  ]
  node [
    id 42
    label "lub"
    origin "text"
  ]
  node [
    id 43
    label "wysoko&#347;&#263;"
    origin "text"
  ]
  node [
    id 44
    label "poni&#380;ej"
    origin "text"
  ]
  node [
    id 45
    label "te&#380;"
    origin "text"
  ]
  node [
    id 46
    label "musie&#263;"
    origin "text"
  ]
  node [
    id 47
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 48
    label "pyta&#263;"
    origin "text"
  ]
  node [
    id 49
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "punkt"
  ]
  node [
    id 51
    label "kres_&#380;ycia"
  ]
  node [
    id 52
    label "ostatnie_podrygi"
  ]
  node [
    id 53
    label "&#380;a&#322;oba"
  ]
  node [
    id 54
    label "kres"
  ]
  node [
    id 55
    label "zabicie"
  ]
  node [
    id 56
    label "pogrzebanie"
  ]
  node [
    id 57
    label "wydarzenie"
  ]
  node [
    id 58
    label "visitation"
  ]
  node [
    id 59
    label "miejsce"
  ]
  node [
    id 60
    label "agonia"
  ]
  node [
    id 61
    label "szeol"
  ]
  node [
    id 62
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 63
    label "szereg"
  ]
  node [
    id 64
    label "mogi&#322;a"
  ]
  node [
    id 65
    label "chwila"
  ]
  node [
    id 66
    label "dzia&#322;anie"
  ]
  node [
    id 67
    label "defenestracja"
  ]
  node [
    id 68
    label "charakter"
  ]
  node [
    id 69
    label "przebiegni&#281;cie"
  ]
  node [
    id 70
    label "przebiec"
  ]
  node [
    id 71
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 72
    label "motyw"
  ]
  node [
    id 73
    label "fabu&#322;a"
  ]
  node [
    id 74
    label "czynno&#347;&#263;"
  ]
  node [
    id 75
    label "Rzym_Zachodni"
  ]
  node [
    id 76
    label "Rzym_Wschodni"
  ]
  node [
    id 77
    label "element"
  ]
  node [
    id 78
    label "ilo&#347;&#263;"
  ]
  node [
    id 79
    label "whole"
  ]
  node [
    id 80
    label "urz&#261;dzenie"
  ]
  node [
    id 81
    label "przestrze&#324;"
  ]
  node [
    id 82
    label "rz&#261;d"
  ]
  node [
    id 83
    label "uwaga"
  ]
  node [
    id 84
    label "cecha"
  ]
  node [
    id 85
    label "plac"
  ]
  node [
    id 86
    label "location"
  ]
  node [
    id 87
    label "warunek_lokalowy"
  ]
  node [
    id 88
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 89
    label "cia&#322;o"
  ]
  node [
    id 90
    label "status"
  ]
  node [
    id 91
    label "time"
  ]
  node [
    id 92
    label "czas"
  ]
  node [
    id 93
    label "stan"
  ]
  node [
    id 94
    label "upadek"
  ]
  node [
    id 95
    label "zmierzch"
  ]
  node [
    id 96
    label "death"
  ]
  node [
    id 97
    label "&#347;mier&#263;"
  ]
  node [
    id 98
    label "nieuleczalnie_chory"
  ]
  node [
    id 99
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 100
    label "spocz&#281;cie"
  ]
  node [
    id 101
    label "spoczywa&#263;"
  ]
  node [
    id 102
    label "nagrobek"
  ]
  node [
    id 103
    label "pochowanie"
  ]
  node [
    id 104
    label "spoczywanie"
  ]
  node [
    id 105
    label "park_sztywnych"
  ]
  node [
    id 106
    label "pomnik"
  ]
  node [
    id 107
    label "chowanie"
  ]
  node [
    id 108
    label "prochowisko"
  ]
  node [
    id 109
    label "spocz&#261;&#263;"
  ]
  node [
    id 110
    label "za&#347;wiaty"
  ]
  node [
    id 111
    label "piek&#322;o"
  ]
  node [
    id 112
    label "judaizm"
  ]
  node [
    id 113
    label "defenestration"
  ]
  node [
    id 114
    label "zaj&#347;cie"
  ]
  node [
    id 115
    label "wyrzucenie"
  ]
  node [
    id 116
    label "kir"
  ]
  node [
    id 117
    label "brud"
  ]
  node [
    id 118
    label "paznokie&#263;"
  ]
  node [
    id 119
    label "&#380;al"
  ]
  node [
    id 120
    label "symbol"
  ]
  node [
    id 121
    label "w&#322;o&#380;enie"
  ]
  node [
    id 122
    label "zw&#322;oki"
  ]
  node [
    id 123
    label "uniemo&#380;liwienie"
  ]
  node [
    id 124
    label "burying"
  ]
  node [
    id 125
    label "porobienie"
  ]
  node [
    id 126
    label "burial"
  ]
  node [
    id 127
    label "zasypanie"
  ]
  node [
    id 128
    label "gr&#243;b"
  ]
  node [
    id 129
    label "czyn"
  ]
  node [
    id 130
    label "zamkni&#281;cie"
  ]
  node [
    id 131
    label "pozabijanie"
  ]
  node [
    id 132
    label "zniszczenie"
  ]
  node [
    id 133
    label "spowodowanie"
  ]
  node [
    id 134
    label "zdarzenie_si&#281;"
  ]
  node [
    id 135
    label "umarcie"
  ]
  node [
    id 136
    label "killing"
  ]
  node [
    id 137
    label "granie"
  ]
  node [
    id 138
    label "zaszkodzenie"
  ]
  node [
    id 139
    label "usuni&#281;cie"
  ]
  node [
    id 140
    label "skrzywdzenie"
  ]
  node [
    id 141
    label "destruction"
  ]
  node [
    id 142
    label "zabrzmienie"
  ]
  node [
    id 143
    label "compaction"
  ]
  node [
    id 144
    label "obiekt_matematyczny"
  ]
  node [
    id 145
    label "stopie&#324;_pisma"
  ]
  node [
    id 146
    label "pozycja"
  ]
  node [
    id 147
    label "problemat"
  ]
  node [
    id 148
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 149
    label "obiekt"
  ]
  node [
    id 150
    label "point"
  ]
  node [
    id 151
    label "plamka"
  ]
  node [
    id 152
    label "mark"
  ]
  node [
    id 153
    label "ust&#281;p"
  ]
  node [
    id 154
    label "po&#322;o&#380;enie"
  ]
  node [
    id 155
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 156
    label "plan"
  ]
  node [
    id 157
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 158
    label "podpunkt"
  ]
  node [
    id 159
    label "jednostka"
  ]
  node [
    id 160
    label "sprawa"
  ]
  node [
    id 161
    label "problematyka"
  ]
  node [
    id 162
    label "prosta"
  ]
  node [
    id 163
    label "wojsko"
  ]
  node [
    id 164
    label "zapunktowa&#263;"
  ]
  node [
    id 165
    label "szpaler"
  ]
  node [
    id 166
    label "zbi&#243;r"
  ]
  node [
    id 167
    label "tract"
  ]
  node [
    id 168
    label "uporz&#261;dkowanie"
  ]
  node [
    id 169
    label "wyra&#380;enie"
  ]
  node [
    id 170
    label "rozmieszczenie"
  ]
  node [
    id 171
    label "mn&#243;stwo"
  ]
  node [
    id 172
    label "unit"
  ]
  node [
    id 173
    label "column"
  ]
  node [
    id 174
    label "nakr&#281;canie"
  ]
  node [
    id 175
    label "nakr&#281;cenie"
  ]
  node [
    id 176
    label "zatrzymanie"
  ]
  node [
    id 177
    label "dzianie_si&#281;"
  ]
  node [
    id 178
    label "liczenie"
  ]
  node [
    id 179
    label "docieranie"
  ]
  node [
    id 180
    label "natural_process"
  ]
  node [
    id 181
    label "skutek"
  ]
  node [
    id 182
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 183
    label "w&#322;&#261;czanie"
  ]
  node [
    id 184
    label "liczy&#263;"
  ]
  node [
    id 185
    label "powodowanie"
  ]
  node [
    id 186
    label "w&#322;&#261;czenie"
  ]
  node [
    id 187
    label "rezultat"
  ]
  node [
    id 188
    label "rozpocz&#281;cie"
  ]
  node [
    id 189
    label "priorytet"
  ]
  node [
    id 190
    label "matematyka"
  ]
  node [
    id 191
    label "czynny"
  ]
  node [
    id 192
    label "uruchomienie"
  ]
  node [
    id 193
    label "podzia&#322;anie"
  ]
  node [
    id 194
    label "cz&#322;owiek"
  ]
  node [
    id 195
    label "bycie"
  ]
  node [
    id 196
    label "impact"
  ]
  node [
    id 197
    label "kampania"
  ]
  node [
    id 198
    label "podtrzymywanie"
  ]
  node [
    id 199
    label "tr&#243;jstronny"
  ]
  node [
    id 200
    label "funkcja"
  ]
  node [
    id 201
    label "act"
  ]
  node [
    id 202
    label "uruchamianie"
  ]
  node [
    id 203
    label "oferta"
  ]
  node [
    id 204
    label "rzut"
  ]
  node [
    id 205
    label "zadzia&#322;anie"
  ]
  node [
    id 206
    label "operacja"
  ]
  node [
    id 207
    label "wp&#322;yw"
  ]
  node [
    id 208
    label "zako&#324;czenie"
  ]
  node [
    id 209
    label "hipnotyzowanie"
  ]
  node [
    id 210
    label "operation"
  ]
  node [
    id 211
    label "supremum"
  ]
  node [
    id 212
    label "reakcja_chemiczna"
  ]
  node [
    id 213
    label "robienie"
  ]
  node [
    id 214
    label "infimum"
  ]
  node [
    id 215
    label "wdzieranie_si&#281;"
  ]
  node [
    id 216
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 217
    label "okre&#347;li&#263;"
  ]
  node [
    id 218
    label "express"
  ]
  node [
    id 219
    label "wydoby&#263;"
  ]
  node [
    id 220
    label "wyrazi&#263;"
  ]
  node [
    id 221
    label "poda&#263;"
  ]
  node [
    id 222
    label "unwrap"
  ]
  node [
    id 223
    label "rzekn&#261;&#263;"
  ]
  node [
    id 224
    label "discover"
  ]
  node [
    id 225
    label "convey"
  ]
  node [
    id 226
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 227
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 228
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 229
    label "siatk&#243;wka"
  ]
  node [
    id 230
    label "nafaszerowa&#263;"
  ]
  node [
    id 231
    label "supply"
  ]
  node [
    id 232
    label "tenis"
  ]
  node [
    id 233
    label "jedzenie"
  ]
  node [
    id 234
    label "ustawi&#263;"
  ]
  node [
    id 235
    label "poinformowa&#263;"
  ]
  node [
    id 236
    label "give"
  ]
  node [
    id 237
    label "zagra&#263;"
  ]
  node [
    id 238
    label "da&#263;"
  ]
  node [
    id 239
    label "zaserwowa&#263;"
  ]
  node [
    id 240
    label "introduce"
  ]
  node [
    id 241
    label "wydosta&#263;"
  ]
  node [
    id 242
    label "wyj&#261;&#263;"
  ]
  node [
    id 243
    label "ocali&#263;"
  ]
  node [
    id 244
    label "g&#243;rnictwo"
  ]
  node [
    id 245
    label "distill"
  ]
  node [
    id 246
    label "extract"
  ]
  node [
    id 247
    label "obtain"
  ]
  node [
    id 248
    label "uwydatni&#263;"
  ]
  node [
    id 249
    label "draw"
  ]
  node [
    id 250
    label "raise"
  ]
  node [
    id 251
    label "wyeksploatowa&#263;"
  ]
  node [
    id 252
    label "wyda&#263;"
  ]
  node [
    id 253
    label "uzyska&#263;"
  ]
  node [
    id 254
    label "doby&#263;"
  ]
  node [
    id 255
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 256
    label "testify"
  ]
  node [
    id 257
    label "oznaczy&#263;"
  ]
  node [
    id 258
    label "zakomunikowa&#263;"
  ]
  node [
    id 259
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 260
    label "vent"
  ]
  node [
    id 261
    label "situate"
  ]
  node [
    id 262
    label "zdecydowa&#263;"
  ]
  node [
    id 263
    label "zrobi&#263;"
  ]
  node [
    id 264
    label "nominate"
  ]
  node [
    id 265
    label "spowodowa&#263;"
  ]
  node [
    id 266
    label "odczuwa&#263;"
  ]
  node [
    id 267
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 268
    label "skrupienie_si&#281;"
  ]
  node [
    id 269
    label "odczu&#263;"
  ]
  node [
    id 270
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 271
    label "odczucie"
  ]
  node [
    id 272
    label "skrupianie_si&#281;"
  ]
  node [
    id 273
    label "event"
  ]
  node [
    id 274
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 275
    label "koszula_Dejaniry"
  ]
  node [
    id 276
    label "odczuwanie"
  ]
  node [
    id 277
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 278
    label "przyczyna"
  ]
  node [
    id 279
    label "typ"
  ]
  node [
    id 280
    label "wierno&#347;&#263;"
  ]
  node [
    id 281
    label "duration"
  ]
  node [
    id 282
    label "uczuwa&#263;"
  ]
  node [
    id 283
    label "notice"
  ]
  node [
    id 284
    label "smell"
  ]
  node [
    id 285
    label "doznawa&#263;"
  ]
  node [
    id 286
    label "postrzega&#263;"
  ]
  node [
    id 287
    label "widzie&#263;"
  ]
  node [
    id 288
    label "feel"
  ]
  node [
    id 289
    label "zagorze&#263;"
  ]
  node [
    id 290
    label "dozna&#263;"
  ]
  node [
    id 291
    label "postrzec"
  ]
  node [
    id 292
    label "przeczulica"
  ]
  node [
    id 293
    label "czucie"
  ]
  node [
    id 294
    label "proces"
  ]
  node [
    id 295
    label "opanowanie"
  ]
  node [
    id 296
    label "zmys&#322;"
  ]
  node [
    id 297
    label "zjawisko"
  ]
  node [
    id 298
    label "postrze&#380;enie"
  ]
  node [
    id 299
    label "poczucie"
  ]
  node [
    id 300
    label "doznanie"
  ]
  node [
    id 301
    label "odczucia"
  ]
  node [
    id 302
    label "reakcja"
  ]
  node [
    id 303
    label "os&#322;upienie"
  ]
  node [
    id 304
    label "zareagowanie"
  ]
  node [
    id 305
    label "owiewanie"
  ]
  node [
    id 306
    label "uczuwanie"
  ]
  node [
    id 307
    label "ogarnianie"
  ]
  node [
    id 308
    label "postrzeganie"
  ]
  node [
    id 309
    label "zaznawanie"
  ]
  node [
    id 310
    label "emotion"
  ]
  node [
    id 311
    label "pojmowanie"
  ]
  node [
    id 312
    label "osi&#261;ga&#263;"
  ]
  node [
    id 313
    label "pay"
  ]
  node [
    id 314
    label "buli&#263;"
  ]
  node [
    id 315
    label "wydawa&#263;"
  ]
  node [
    id 316
    label "placard"
  ]
  node [
    id 317
    label "plon"
  ]
  node [
    id 318
    label "d&#378;wi&#281;k"
  ]
  node [
    id 319
    label "reszta"
  ]
  node [
    id 320
    label "panna_na_wydaniu"
  ]
  node [
    id 321
    label "denuncjowa&#263;"
  ]
  node [
    id 322
    label "impart"
  ]
  node [
    id 323
    label "zapach"
  ]
  node [
    id 324
    label "mie&#263;_miejsce"
  ]
  node [
    id 325
    label "powierza&#263;"
  ]
  node [
    id 326
    label "dawa&#263;"
  ]
  node [
    id 327
    label "wytwarza&#263;"
  ]
  node [
    id 328
    label "tajemnica"
  ]
  node [
    id 329
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 330
    label "wiano"
  ]
  node [
    id 331
    label "podawa&#263;"
  ]
  node [
    id 332
    label "ujawnia&#263;"
  ]
  node [
    id 333
    label "robi&#263;"
  ]
  node [
    id 334
    label "produkcja"
  ]
  node [
    id 335
    label "kojarzy&#263;"
  ]
  node [
    id 336
    label "surrender"
  ]
  node [
    id 337
    label "wydawnictwo"
  ]
  node [
    id 338
    label "wprowadza&#263;"
  ]
  node [
    id 339
    label "train"
  ]
  node [
    id 340
    label "get"
  ]
  node [
    id 341
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 342
    label "dociera&#263;"
  ]
  node [
    id 343
    label "uzyskiwa&#263;"
  ]
  node [
    id 344
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 345
    label "czw&#243;rka"
  ]
  node [
    id 346
    label "sk&#322;ada&#263;_si&#281;"
  ]
  node [
    id 347
    label "zbi&#243;rka"
  ]
  node [
    id 348
    label "arkusz"
  ]
  node [
    id 349
    label "&#243;semka"
  ]
  node [
    id 350
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 351
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 352
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 353
    label "fee"
  ]
  node [
    id 354
    label "uregulowa&#263;"
  ]
  node [
    id 355
    label "kwota"
  ]
  node [
    id 356
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 357
    label "p&#322;at"
  ]
  node [
    id 358
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 359
    label "spotkanie"
  ]
  node [
    id 360
    label "koszyk&#243;wka"
  ]
  node [
    id 361
    label "kwestowanie"
  ]
  node [
    id 362
    label "recoil"
  ]
  node [
    id 363
    label "kwestarz"
  ]
  node [
    id 364
    label "apel"
  ]
  node [
    id 365
    label "collection"
  ]
  node [
    id 366
    label "chwyt"
  ]
  node [
    id 367
    label "przedtrzonowiec"
  ]
  node [
    id 368
    label "trafienie"
  ]
  node [
    id 369
    label "osada"
  ]
  node [
    id 370
    label "blotka"
  ]
  node [
    id 371
    label "p&#322;yta_winylowa"
  ]
  node [
    id 372
    label "cyfra"
  ]
  node [
    id 373
    label "pok&#243;j"
  ]
  node [
    id 374
    label "stopie&#324;"
  ]
  node [
    id 375
    label "arkusz_drukarski"
  ]
  node [
    id 376
    label "zaprz&#281;g"
  ]
  node [
    id 377
    label "toto-lotek"
  ]
  node [
    id 378
    label "&#263;wiartka"
  ]
  node [
    id 379
    label "&#322;&#243;dka"
  ]
  node [
    id 380
    label "four"
  ]
  node [
    id 381
    label "minialbum"
  ]
  node [
    id 382
    label "hotel"
  ]
  node [
    id 383
    label "w&#281;ze&#322;"
  ]
  node [
    id 384
    label "eighth"
  ]
  node [
    id 385
    label "&#322;&#243;d&#378;_wios&#322;owa"
  ]
  node [
    id 386
    label "&#243;sma_cz&#281;&#347;&#263;"
  ]
  node [
    id 387
    label "przyrz&#261;d_asekuracyjny"
  ]
  node [
    id 388
    label "bilard"
  ]
  node [
    id 389
    label "nuta"
  ]
  node [
    id 390
    label "przegub"
  ]
  node [
    id 391
    label "format_arkusza"
  ]
  node [
    id 392
    label "trzonowiec"
  ]
  node [
    id 393
    label "&#322;&#243;d&#378;_regatowa"
  ]
  node [
    id 394
    label "cepy"
  ]
  node [
    id 395
    label "figura"
  ]
  node [
    id 396
    label "wisdom_tooth"
  ]
  node [
    id 397
    label "kapica"
  ]
  node [
    id 398
    label "kszta&#322;t"
  ]
  node [
    id 399
    label "eight"
  ]
  node [
    id 400
    label "godzina"
  ]
  node [
    id 401
    label "kwadrans"
  ]
  node [
    id 402
    label "p&#243;&#322;godzina"
  ]
  node [
    id 403
    label "doba"
  ]
  node [
    id 404
    label "jednostka_czasu"
  ]
  node [
    id 405
    label "minuta"
  ]
  node [
    id 406
    label "linia"
  ]
  node [
    id 407
    label "zorientowa&#263;"
  ]
  node [
    id 408
    label "orientowa&#263;"
  ]
  node [
    id 409
    label "fragment"
  ]
  node [
    id 410
    label "skr&#281;cenie"
  ]
  node [
    id 411
    label "skr&#281;ci&#263;"
  ]
  node [
    id 412
    label "internet"
  ]
  node [
    id 413
    label "g&#243;ra"
  ]
  node [
    id 414
    label "orientowanie"
  ]
  node [
    id 415
    label "zorientowanie"
  ]
  node [
    id 416
    label "forma"
  ]
  node [
    id 417
    label "podmiot"
  ]
  node [
    id 418
    label "ty&#322;"
  ]
  node [
    id 419
    label "logowanie"
  ]
  node [
    id 420
    label "voice"
  ]
  node [
    id 421
    label "kartka"
  ]
  node [
    id 422
    label "layout"
  ]
  node [
    id 423
    label "bok"
  ]
  node [
    id 424
    label "powierzchnia"
  ]
  node [
    id 425
    label "posta&#263;"
  ]
  node [
    id 426
    label "skr&#281;canie"
  ]
  node [
    id 427
    label "orientacja"
  ]
  node [
    id 428
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 429
    label "pagina"
  ]
  node [
    id 430
    label "uj&#281;cie"
  ]
  node [
    id 431
    label "serwis_internetowy"
  ]
  node [
    id 432
    label "adres_internetowy"
  ]
  node [
    id 433
    label "prz&#243;d"
  ]
  node [
    id 434
    label "s&#261;d"
  ]
  node [
    id 435
    label "skr&#281;ca&#263;"
  ]
  node [
    id 436
    label "plik"
  ]
  node [
    id 437
    label "byt"
  ]
  node [
    id 438
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 439
    label "nauka_prawa"
  ]
  node [
    id 440
    label "prawo"
  ]
  node [
    id 441
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 442
    label "organizacja"
  ]
  node [
    id 443
    label "osobowo&#347;&#263;"
  ]
  node [
    id 444
    label "utw&#243;r"
  ]
  node [
    id 445
    label "wytrzyma&#263;"
  ]
  node [
    id 446
    label "trim"
  ]
  node [
    id 447
    label "Osjan"
  ]
  node [
    id 448
    label "formacja"
  ]
  node [
    id 449
    label "kto&#347;"
  ]
  node [
    id 450
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 451
    label "pozosta&#263;"
  ]
  node [
    id 452
    label "poby&#263;"
  ]
  node [
    id 453
    label "przedstawienie"
  ]
  node [
    id 454
    label "Aspazja"
  ]
  node [
    id 455
    label "go&#347;&#263;"
  ]
  node [
    id 456
    label "budowa"
  ]
  node [
    id 457
    label "charakterystyka"
  ]
  node [
    id 458
    label "kompleksja"
  ]
  node [
    id 459
    label "wygl&#261;d"
  ]
  node [
    id 460
    label "wytw&#243;r"
  ]
  node [
    id 461
    label "punkt_widzenia"
  ]
  node [
    id 462
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 463
    label "zaistnie&#263;"
  ]
  node [
    id 464
    label "curve"
  ]
  node [
    id 465
    label "granica"
  ]
  node [
    id 466
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 467
    label "szczep"
  ]
  node [
    id 468
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 469
    label "przeorientowanie"
  ]
  node [
    id 470
    label "grupa_organizm&#243;w"
  ]
  node [
    id 471
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 472
    label "przew&#243;d"
  ]
  node [
    id 473
    label "jard"
  ]
  node [
    id 474
    label "poprowadzi&#263;"
  ]
  node [
    id 475
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 476
    label "tekst"
  ]
  node [
    id 477
    label "line"
  ]
  node [
    id 478
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 479
    label "prowadzi&#263;"
  ]
  node [
    id 480
    label "Ural"
  ]
  node [
    id 481
    label "prowadzenie"
  ]
  node [
    id 482
    label "po&#322;&#261;czenie"
  ]
  node [
    id 483
    label "przewo&#378;nik"
  ]
  node [
    id 484
    label "przeorientowywanie"
  ]
  node [
    id 485
    label "coalescence"
  ]
  node [
    id 486
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 487
    label "billing"
  ]
  node [
    id 488
    label "transporter"
  ]
  node [
    id 489
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 490
    label "materia&#322;_zecerski"
  ]
  node [
    id 491
    label "sztrych"
  ]
  node [
    id 492
    label "drzewo_genealogiczne"
  ]
  node [
    id 493
    label "linijka"
  ]
  node [
    id 494
    label "granice"
  ]
  node [
    id 495
    label "phreaker"
  ]
  node [
    id 496
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 497
    label "figura_geometryczna"
  ]
  node [
    id 498
    label "trasa"
  ]
  node [
    id 499
    label "przeorientowa&#263;"
  ]
  node [
    id 500
    label "bearing"
  ]
  node [
    id 501
    label "spos&#243;b"
  ]
  node [
    id 502
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 503
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 504
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 505
    label "przeorientowywa&#263;"
  ]
  node [
    id 506
    label "armia"
  ]
  node [
    id 507
    label "przystanek"
  ]
  node [
    id 508
    label "access"
  ]
  node [
    id 509
    label "cord"
  ]
  node [
    id 510
    label "kontakt"
  ]
  node [
    id 511
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 512
    label "nadpisywa&#263;"
  ]
  node [
    id 513
    label "nadpisywanie"
  ]
  node [
    id 514
    label "bundle"
  ]
  node [
    id 515
    label "paczka"
  ]
  node [
    id 516
    label "podkatalog"
  ]
  node [
    id 517
    label "dokument"
  ]
  node [
    id 518
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 519
    label "folder"
  ]
  node [
    id 520
    label "nadpisa&#263;"
  ]
  node [
    id 521
    label "nadpisanie"
  ]
  node [
    id 522
    label "poj&#281;cie"
  ]
  node [
    id 523
    label "capacity"
  ]
  node [
    id 524
    label "plane"
  ]
  node [
    id 525
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 526
    label "obszar"
  ]
  node [
    id 527
    label "zwierciad&#322;o"
  ]
  node [
    id 528
    label "rozmiar"
  ]
  node [
    id 529
    label "zdolno&#347;&#263;"
  ]
  node [
    id 530
    label "zawarto&#347;&#263;"
  ]
  node [
    id 531
    label "ornamentyka"
  ]
  node [
    id 532
    label "formality"
  ]
  node [
    id 533
    label "dzie&#322;o"
  ]
  node [
    id 534
    label "temat"
  ]
  node [
    id 535
    label "rdze&#324;"
  ]
  node [
    id 536
    label "wz&#243;r"
  ]
  node [
    id 537
    label "mode"
  ]
  node [
    id 538
    label "odmiana"
  ]
  node [
    id 539
    label "poznanie"
  ]
  node [
    id 540
    label "maszyna_drukarska"
  ]
  node [
    id 541
    label "kantyzm"
  ]
  node [
    id 542
    label "kielich"
  ]
  node [
    id 543
    label "pasmo"
  ]
  node [
    id 544
    label "naczynie"
  ]
  node [
    id 545
    label "style"
  ]
  node [
    id 546
    label "jednostka_systematyczna"
  ]
  node [
    id 547
    label "szablon"
  ]
  node [
    id 548
    label "creation"
  ]
  node [
    id 549
    label "linearno&#347;&#263;"
  ]
  node [
    id 550
    label "spirala"
  ]
  node [
    id 551
    label "leksem"
  ]
  node [
    id 552
    label "arystotelizm"
  ]
  node [
    id 553
    label "miniatura"
  ]
  node [
    id 554
    label "blaszka"
  ]
  node [
    id 555
    label "zwyczaj"
  ]
  node [
    id 556
    label "October"
  ]
  node [
    id 557
    label "dyspozycja"
  ]
  node [
    id 558
    label "gwiazda"
  ]
  node [
    id 559
    label "morfem"
  ]
  node [
    id 560
    label "struktura"
  ]
  node [
    id 561
    label "g&#322;owa"
  ]
  node [
    id 562
    label "do&#322;ek"
  ]
  node [
    id 563
    label "p&#281;tla"
  ]
  node [
    id 564
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 565
    label "forum"
  ]
  node [
    id 566
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 567
    label "s&#261;downictwo"
  ]
  node [
    id 568
    label "podejrzany"
  ]
  node [
    id 569
    label "&#347;wiadek"
  ]
  node [
    id 570
    label "instytucja"
  ]
  node [
    id 571
    label "biuro"
  ]
  node [
    id 572
    label "post&#281;powanie"
  ]
  node [
    id 573
    label "court"
  ]
  node [
    id 574
    label "my&#347;l"
  ]
  node [
    id 575
    label "obrona"
  ]
  node [
    id 576
    label "broni&#263;"
  ]
  node [
    id 577
    label "antylogizm"
  ]
  node [
    id 578
    label "oskar&#380;yciel"
  ]
  node [
    id 579
    label "urz&#261;d"
  ]
  node [
    id 580
    label "skazany"
  ]
  node [
    id 581
    label "konektyw"
  ]
  node [
    id 582
    label "wypowied&#378;"
  ]
  node [
    id 583
    label "bronienie"
  ]
  node [
    id 584
    label "pods&#261;dny"
  ]
  node [
    id 585
    label "zesp&#243;&#322;"
  ]
  node [
    id 586
    label "procesowicz"
  ]
  node [
    id 587
    label "scena"
  ]
  node [
    id 588
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 589
    label "zapisanie"
  ]
  node [
    id 590
    label "prezentacja"
  ]
  node [
    id 591
    label "withdrawal"
  ]
  node [
    id 592
    label "podniesienie"
  ]
  node [
    id 593
    label "poinformowanie"
  ]
  node [
    id 594
    label "wzbudzenie"
  ]
  node [
    id 595
    label "wzi&#281;cie"
  ]
  node [
    id 596
    label "film"
  ]
  node [
    id 597
    label "zaaresztowanie"
  ]
  node [
    id 598
    label "wording"
  ]
  node [
    id 599
    label "capture"
  ]
  node [
    id 600
    label "rzucenie"
  ]
  node [
    id 601
    label "pochwytanie"
  ]
  node [
    id 602
    label "zabranie"
  ]
  node [
    id 603
    label "wyznaczenie"
  ]
  node [
    id 604
    label "zrozumienie"
  ]
  node [
    id 605
    label "zwr&#243;cenie"
  ]
  node [
    id 606
    label "kierunek"
  ]
  node [
    id 607
    label "przyczynienie_si&#281;"
  ]
  node [
    id 608
    label "odcinek"
  ]
  node [
    id 609
    label "&#347;ciana"
  ]
  node [
    id 610
    label "strzelba"
  ]
  node [
    id 611
    label "wielok&#261;t"
  ]
  node [
    id 612
    label "tu&#322;&#243;w"
  ]
  node [
    id 613
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 614
    label "lufa"
  ]
  node [
    id 615
    label "set"
  ]
  node [
    id 616
    label "aim"
  ]
  node [
    id 617
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 618
    label "eastern_hemisphere"
  ]
  node [
    id 619
    label "orient"
  ]
  node [
    id 620
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 621
    label "wyznaczy&#263;"
  ]
  node [
    id 622
    label "uszkodzi&#263;"
  ]
  node [
    id 623
    label "twist"
  ]
  node [
    id 624
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 625
    label "scali&#263;"
  ]
  node [
    id 626
    label "sple&#347;&#263;"
  ]
  node [
    id 627
    label "nawin&#261;&#263;"
  ]
  node [
    id 628
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 629
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 630
    label "flex"
  ]
  node [
    id 631
    label "splay"
  ]
  node [
    id 632
    label "os&#322;abi&#263;"
  ]
  node [
    id 633
    label "break"
  ]
  node [
    id 634
    label "wrench"
  ]
  node [
    id 635
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 636
    label "os&#322;abia&#263;"
  ]
  node [
    id 637
    label "scala&#263;"
  ]
  node [
    id 638
    label "screw"
  ]
  node [
    id 639
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 640
    label "throw"
  ]
  node [
    id 641
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 642
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 643
    label "splata&#263;"
  ]
  node [
    id 644
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 645
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 646
    label "przele&#378;&#263;"
  ]
  node [
    id 647
    label "Synaj"
  ]
  node [
    id 648
    label "Kreml"
  ]
  node [
    id 649
    label "Ropa"
  ]
  node [
    id 650
    label "przedmiot"
  ]
  node [
    id 651
    label "rami&#261;czko"
  ]
  node [
    id 652
    label "&#347;piew"
  ]
  node [
    id 653
    label "wysoki"
  ]
  node [
    id 654
    label "Jaworze"
  ]
  node [
    id 655
    label "grupa"
  ]
  node [
    id 656
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 657
    label "kupa"
  ]
  node [
    id 658
    label "karczek"
  ]
  node [
    id 659
    label "wzniesienie"
  ]
  node [
    id 660
    label "pi&#281;tro"
  ]
  node [
    id 661
    label "przelezienie"
  ]
  node [
    id 662
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 663
    label "kszta&#322;towanie"
  ]
  node [
    id 664
    label "odchylanie_si&#281;"
  ]
  node [
    id 665
    label "splatanie"
  ]
  node [
    id 666
    label "scalanie"
  ]
  node [
    id 667
    label "snucie"
  ]
  node [
    id 668
    label "odbijanie"
  ]
  node [
    id 669
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 670
    label "tortuosity"
  ]
  node [
    id 671
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 672
    label "contortion"
  ]
  node [
    id 673
    label "uprz&#281;dzenie"
  ]
  node [
    id 674
    label "os&#322;abianie"
  ]
  node [
    id 675
    label "nawini&#281;cie"
  ]
  node [
    id 676
    label "splecenie"
  ]
  node [
    id 677
    label "uszkodzenie"
  ]
  node [
    id 678
    label "poskr&#281;canie"
  ]
  node [
    id 679
    label "odbicie"
  ]
  node [
    id 680
    label "turn"
  ]
  node [
    id 681
    label "z&#322;&#261;czenie"
  ]
  node [
    id 682
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 683
    label "odchylenie_si&#281;"
  ]
  node [
    id 684
    label "turning"
  ]
  node [
    id 685
    label "uraz"
  ]
  node [
    id 686
    label "os&#322;abienie"
  ]
  node [
    id 687
    label "marshal"
  ]
  node [
    id 688
    label "inform"
  ]
  node [
    id 689
    label "kierowa&#263;"
  ]
  node [
    id 690
    label "wyznacza&#263;"
  ]
  node [
    id 691
    label "pomaga&#263;"
  ]
  node [
    id 692
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 693
    label "orientation"
  ]
  node [
    id 694
    label "przyczynianie_si&#281;"
  ]
  node [
    id 695
    label "pomaganie"
  ]
  node [
    id 696
    label "oznaczanie"
  ]
  node [
    id 697
    label "zwracanie"
  ]
  node [
    id 698
    label "rozeznawanie"
  ]
  node [
    id 699
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 700
    label "pogubienie_si&#281;"
  ]
  node [
    id 701
    label "seksualno&#347;&#263;"
  ]
  node [
    id 702
    label "wiedza"
  ]
  node [
    id 703
    label "zorientowanie_si&#281;"
  ]
  node [
    id 704
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 705
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 706
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 707
    label "gubienie_si&#281;"
  ]
  node [
    id 708
    label "zaty&#322;"
  ]
  node [
    id 709
    label "pupa"
  ]
  node [
    id 710
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 711
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 712
    label "uwierzytelnienie"
  ]
  node [
    id 713
    label "liczba"
  ]
  node [
    id 714
    label "circumference"
  ]
  node [
    id 715
    label "cyrkumferencja"
  ]
  node [
    id 716
    label "provider"
  ]
  node [
    id 717
    label "podcast"
  ]
  node [
    id 718
    label "mem"
  ]
  node [
    id 719
    label "cyberprzestrze&#324;"
  ]
  node [
    id 720
    label "punkt_dost&#281;pu"
  ]
  node [
    id 721
    label "sie&#263;_komputerowa"
  ]
  node [
    id 722
    label "biznes_elektroniczny"
  ]
  node [
    id 723
    label "media"
  ]
  node [
    id 724
    label "gra_sieciowa"
  ]
  node [
    id 725
    label "hipertekst"
  ]
  node [
    id 726
    label "netbook"
  ]
  node [
    id 727
    label "e-hazard"
  ]
  node [
    id 728
    label "us&#322;uga_internetowa"
  ]
  node [
    id 729
    label "grooming"
  ]
  node [
    id 730
    label "rzecz"
  ]
  node [
    id 731
    label "thing"
  ]
  node [
    id 732
    label "co&#347;"
  ]
  node [
    id 733
    label "budynek"
  ]
  node [
    id 734
    label "program"
  ]
  node [
    id 735
    label "ticket"
  ]
  node [
    id 736
    label "kara"
  ]
  node [
    id 737
    label "bon"
  ]
  node [
    id 738
    label "kartonik"
  ]
  node [
    id 739
    label "faul"
  ]
  node [
    id 740
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 741
    label "wk&#322;ad"
  ]
  node [
    id 742
    label "s&#281;dzia"
  ]
  node [
    id 743
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 744
    label "numer"
  ]
  node [
    id 745
    label "pagination"
  ]
  node [
    id 746
    label "performance"
  ]
  node [
    id 747
    label "pracowanie"
  ]
  node [
    id 748
    label "sk&#322;adanie"
  ]
  node [
    id 749
    label "wyraz"
  ]
  node [
    id 750
    label "koszt_rodzajowy"
  ]
  node [
    id 751
    label "opowiadanie"
  ]
  node [
    id 752
    label "command"
  ]
  node [
    id 753
    label "us&#322;uga"
  ]
  node [
    id 754
    label "znaczenie"
  ]
  node [
    id 755
    label "informowanie"
  ]
  node [
    id 756
    label "zobowi&#261;zanie"
  ]
  node [
    id 757
    label "przekonywanie"
  ]
  node [
    id 758
    label "service"
  ]
  node [
    id 759
    label "czynienie_dobra"
  ]
  node [
    id 760
    label "p&#322;acenie"
  ]
  node [
    id 761
    label "rozpowiedzenie"
  ]
  node [
    id 762
    label "report"
  ]
  node [
    id 763
    label "spalenie"
  ]
  node [
    id 764
    label "story"
  ]
  node [
    id 765
    label "podbarwianie"
  ]
  node [
    id 766
    label "rozpowiadanie"
  ]
  node [
    id 767
    label "utw&#243;r_epicki"
  ]
  node [
    id 768
    label "proza"
  ]
  node [
    id 769
    label "follow-up"
  ]
  node [
    id 770
    label "prawienie"
  ]
  node [
    id 771
    label "przedstawianie"
  ]
  node [
    id 772
    label "przyk&#322;adanie"
  ]
  node [
    id 773
    label "gromadzenie"
  ]
  node [
    id 774
    label "dawanie"
  ]
  node [
    id 775
    label "opracowywanie"
  ]
  node [
    id 776
    label "zestawianie"
  ]
  node [
    id 777
    label "gi&#281;cie"
  ]
  node [
    id 778
    label "m&#243;wienie"
  ]
  node [
    id 779
    label "wage"
  ]
  node [
    id 780
    label "wydawanie"
  ]
  node [
    id 781
    label "osi&#261;ganie"
  ]
  node [
    id 782
    label "wykupywanie"
  ]
  node [
    id 783
    label "zarz&#261;dzanie"
  ]
  node [
    id 784
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 785
    label "skakanie"
  ]
  node [
    id 786
    label "d&#261;&#380;enie"
  ]
  node [
    id 787
    label "postaranie_si&#281;"
  ]
  node [
    id 788
    label "przepracowanie"
  ]
  node [
    id 789
    label "przepracowanie_si&#281;"
  ]
  node [
    id 790
    label "podlizanie_si&#281;"
  ]
  node [
    id 791
    label "podlizywanie_si&#281;"
  ]
  node [
    id 792
    label "przepracowywanie"
  ]
  node [
    id 793
    label "awansowanie"
  ]
  node [
    id 794
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 795
    label "odpocz&#281;cie"
  ]
  node [
    id 796
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 797
    label "courtship"
  ]
  node [
    id 798
    label "dopracowanie"
  ]
  node [
    id 799
    label "zapracowanie"
  ]
  node [
    id 800
    label "wyrabianie"
  ]
  node [
    id 801
    label "maszyna"
  ]
  node [
    id 802
    label "wyrobienie"
  ]
  node [
    id 803
    label "spracowanie_si&#281;"
  ]
  node [
    id 804
    label "poruszanie_si&#281;"
  ]
  node [
    id 805
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 806
    label "podejmowanie"
  ]
  node [
    id 807
    label "funkcjonowanie"
  ]
  node [
    id 808
    label "use"
  ]
  node [
    id 809
    label "zaprz&#281;ganie"
  ]
  node [
    id 810
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 811
    label "komunikowanie"
  ]
  node [
    id 812
    label "powiadanie"
  ]
  node [
    id 813
    label "communication"
  ]
  node [
    id 814
    label "istota"
  ]
  node [
    id 815
    label "odgrywanie_roli"
  ]
  node [
    id 816
    label "assay"
  ]
  node [
    id 817
    label "wskazywanie"
  ]
  node [
    id 818
    label "gravity"
  ]
  node [
    id 819
    label "condition"
  ]
  node [
    id 820
    label "informacja"
  ]
  node [
    id 821
    label "weight"
  ]
  node [
    id 822
    label "okre&#347;lanie"
  ]
  node [
    id 823
    label "odk&#322;adanie"
  ]
  node [
    id 824
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 825
    label "stawianie"
  ]
  node [
    id 826
    label "stosunek_prawny"
  ]
  node [
    id 827
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 828
    label "zapewnienie"
  ]
  node [
    id 829
    label "oblig"
  ]
  node [
    id 830
    label "oddzia&#322;anie"
  ]
  node [
    id 831
    label "obowi&#261;zek"
  ]
  node [
    id 832
    label "zapowied&#378;"
  ]
  node [
    id 833
    label "statement"
  ]
  node [
    id 834
    label "duty"
  ]
  node [
    id 835
    label "occupation"
  ]
  node [
    id 836
    label "zachowanie"
  ]
  node [
    id 837
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 838
    label "produkt_gotowy"
  ]
  node [
    id 839
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 840
    label "asortyment"
  ]
  node [
    id 841
    label "przekonywanie_si&#281;"
  ]
  node [
    id 842
    label "sk&#322;anianie"
  ]
  node [
    id 843
    label "persuasion"
  ]
  node [
    id 844
    label "oddzia&#322;ywanie"
  ]
  node [
    id 845
    label "oznaka"
  ]
  node [
    id 846
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 847
    label "term"
  ]
  node [
    id 848
    label "rozpatrywa&#263;"
  ]
  node [
    id 849
    label "argue"
  ]
  node [
    id 850
    label "take_care"
  ]
  node [
    id 851
    label "zamierza&#263;"
  ]
  node [
    id 852
    label "deliver"
  ]
  node [
    id 853
    label "os&#261;dza&#263;"
  ]
  node [
    id 854
    label "troska&#263;_si&#281;"
  ]
  node [
    id 855
    label "hold"
  ]
  node [
    id 856
    label "strike"
  ]
  node [
    id 857
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 858
    label "s&#261;dzi&#263;"
  ]
  node [
    id 859
    label "powodowa&#263;"
  ]
  node [
    id 860
    label "znajdowa&#263;"
  ]
  node [
    id 861
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 862
    label "przeprowadza&#263;"
  ]
  node [
    id 863
    label "consider"
  ]
  node [
    id 864
    label "volunteer"
  ]
  node [
    id 865
    label "oszukiwa&#263;"
  ]
  node [
    id 866
    label "tentegowa&#263;"
  ]
  node [
    id 867
    label "urz&#261;dza&#263;"
  ]
  node [
    id 868
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 869
    label "czyni&#263;"
  ]
  node [
    id 870
    label "work"
  ]
  node [
    id 871
    label "przerabia&#263;"
  ]
  node [
    id 872
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 873
    label "post&#281;powa&#263;"
  ]
  node [
    id 874
    label "peddle"
  ]
  node [
    id 875
    label "organizowa&#263;"
  ]
  node [
    id 876
    label "falowa&#263;"
  ]
  node [
    id 877
    label "stylizowa&#263;"
  ]
  node [
    id 878
    label "wydala&#263;"
  ]
  node [
    id 879
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 880
    label "ukazywa&#263;"
  ]
  node [
    id 881
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 882
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 883
    label "w_chuj"
  ]
  node [
    id 884
    label "thinly"
  ]
  node [
    id 885
    label "lu&#378;ny"
  ]
  node [
    id 886
    label "niezwykle"
  ]
  node [
    id 887
    label "czasami"
  ]
  node [
    id 888
    label "rzadki"
  ]
  node [
    id 889
    label "rozwodnienie"
  ]
  node [
    id 890
    label "rozrzedzenie"
  ]
  node [
    id 891
    label "rozwadnianie"
  ]
  node [
    id 892
    label "lu&#378;no"
  ]
  node [
    id 893
    label "niezwyk&#322;y"
  ]
  node [
    id 894
    label "rozrzedzanie"
  ]
  node [
    id 895
    label "zrzedni&#281;cie"
  ]
  node [
    id 896
    label "rzedni&#281;cie"
  ]
  node [
    id 897
    label "nieformalny"
  ]
  node [
    id 898
    label "osobny"
  ]
  node [
    id 899
    label "dodatkowy"
  ]
  node [
    id 900
    label "nieregularny"
  ]
  node [
    id 901
    label "rozdeptywanie"
  ]
  node [
    id 902
    label "swobodny"
  ]
  node [
    id 903
    label "nieokre&#347;lony"
  ]
  node [
    id 904
    label "rozdeptanie"
  ]
  node [
    id 905
    label "beztroski"
  ]
  node [
    id 906
    label "&#322;atwy"
  ]
  node [
    id 907
    label "przyjemny"
  ]
  node [
    id 908
    label "nie&#347;cis&#322;y"
  ]
  node [
    id 909
    label "daleki"
  ]
  node [
    id 910
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 911
    label "dysfonia"
  ]
  node [
    id 912
    label "prawi&#263;"
  ]
  node [
    id 913
    label "remark"
  ]
  node [
    id 914
    label "chew_the_fat"
  ]
  node [
    id 915
    label "talk"
  ]
  node [
    id 916
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 917
    label "say"
  ]
  node [
    id 918
    label "wyra&#380;a&#263;"
  ]
  node [
    id 919
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 920
    label "j&#281;zyk"
  ]
  node [
    id 921
    label "tell"
  ]
  node [
    id 922
    label "informowa&#263;"
  ]
  node [
    id 923
    label "rozmawia&#263;"
  ]
  node [
    id 924
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 925
    label "powiada&#263;"
  ]
  node [
    id 926
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 927
    label "okre&#347;la&#263;"
  ]
  node [
    id 928
    label "u&#380;ywa&#263;"
  ]
  node [
    id 929
    label "gaworzy&#263;"
  ]
  node [
    id 930
    label "formu&#322;owa&#263;"
  ]
  node [
    id 931
    label "dziama&#263;"
  ]
  node [
    id 932
    label "umie&#263;"
  ]
  node [
    id 933
    label "wydobywa&#263;"
  ]
  node [
    id 934
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 935
    label "korzysta&#263;"
  ]
  node [
    id 936
    label "distribute"
  ]
  node [
    id 937
    label "bash"
  ]
  node [
    id 938
    label "decydowa&#263;"
  ]
  node [
    id 939
    label "signify"
  ]
  node [
    id 940
    label "komunikowa&#263;"
  ]
  node [
    id 941
    label "oznacza&#263;"
  ]
  node [
    id 942
    label "znaczy&#263;"
  ]
  node [
    id 943
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 944
    label "arouse"
  ]
  node [
    id 945
    label "represent"
  ]
  node [
    id 946
    label "give_voice"
  ]
  node [
    id 947
    label "determine"
  ]
  node [
    id 948
    label "wyjmowa&#263;"
  ]
  node [
    id 949
    label "wydostawa&#263;"
  ]
  node [
    id 950
    label "dobywa&#263;"
  ]
  node [
    id 951
    label "uwydatnia&#263;"
  ]
  node [
    id 952
    label "eksploatowa&#263;"
  ]
  node [
    id 953
    label "excavate"
  ]
  node [
    id 954
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 955
    label "ocala&#263;"
  ]
  node [
    id 956
    label "m&#243;c"
  ]
  node [
    id 957
    label "can"
  ]
  node [
    id 958
    label "wiedzie&#263;"
  ]
  node [
    id 959
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 960
    label "szczeka&#263;"
  ]
  node [
    id 961
    label "rozumie&#263;"
  ]
  node [
    id 962
    label "funkcjonowa&#263;"
  ]
  node [
    id 963
    label "mawia&#263;"
  ]
  node [
    id 964
    label "opowiada&#263;"
  ]
  node [
    id 965
    label "chatter"
  ]
  node [
    id 966
    label "niemowl&#281;"
  ]
  node [
    id 967
    label "kosmetyk"
  ]
  node [
    id 968
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 969
    label "stanowisko_archeologiczne"
  ]
  node [
    id 970
    label "pisa&#263;"
  ]
  node [
    id 971
    label "kod"
  ]
  node [
    id 972
    label "pype&#263;"
  ]
  node [
    id 973
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 974
    label "gramatyka"
  ]
  node [
    id 975
    label "language"
  ]
  node [
    id 976
    label "fonetyka"
  ]
  node [
    id 977
    label "t&#322;umaczenie"
  ]
  node [
    id 978
    label "artykulator"
  ]
  node [
    id 979
    label "rozumienie"
  ]
  node [
    id 980
    label "jama_ustna"
  ]
  node [
    id 981
    label "organ"
  ]
  node [
    id 982
    label "ssanie"
  ]
  node [
    id 983
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 984
    label "lizanie"
  ]
  node [
    id 985
    label "liza&#263;"
  ]
  node [
    id 986
    label "makroglosja"
  ]
  node [
    id 987
    label "natural_language"
  ]
  node [
    id 988
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 989
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 990
    label "napisa&#263;"
  ]
  node [
    id 991
    label "s&#322;ownictwo"
  ]
  node [
    id 992
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 993
    label "konsonantyzm"
  ]
  node [
    id 994
    label "ssa&#263;"
  ]
  node [
    id 995
    label "wokalizm"
  ]
  node [
    id 996
    label "kultura_duchowa"
  ]
  node [
    id 997
    label "formalizowanie"
  ]
  node [
    id 998
    label "jeniec"
  ]
  node [
    id 999
    label "kawa&#322;ek"
  ]
  node [
    id 1000
    label "po_koroniarsku"
  ]
  node [
    id 1001
    label "stylik"
  ]
  node [
    id 1002
    label "przet&#322;umaczenie"
  ]
  node [
    id 1003
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1004
    label "formacja_geologiczna"
  ]
  node [
    id 1005
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1006
    label "but"
  ]
  node [
    id 1007
    label "pismo"
  ]
  node [
    id 1008
    label "formalizowa&#263;"
  ]
  node [
    id 1009
    label "dysleksja"
  ]
  node [
    id 1010
    label "dysphonia"
  ]
  node [
    id 1011
    label "czasopismo"
  ]
  node [
    id 1012
    label "kronika"
  ]
  node [
    id 1013
    label "yearbook"
  ]
  node [
    id 1014
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1015
    label "The_Beatles"
  ]
  node [
    id 1016
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 1017
    label "AWS"
  ]
  node [
    id 1018
    label "partia"
  ]
  node [
    id 1019
    label "Mazowsze"
  ]
  node [
    id 1020
    label "ZChN"
  ]
  node [
    id 1021
    label "Bund"
  ]
  node [
    id 1022
    label "PPR"
  ]
  node [
    id 1023
    label "blok"
  ]
  node [
    id 1024
    label "egzekutywa"
  ]
  node [
    id 1025
    label "Wigowie"
  ]
  node [
    id 1026
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1027
    label "Razem"
  ]
  node [
    id 1028
    label "SLD"
  ]
  node [
    id 1029
    label "ZSL"
  ]
  node [
    id 1030
    label "szko&#322;a"
  ]
  node [
    id 1031
    label "Kuomintang"
  ]
  node [
    id 1032
    label "si&#322;a"
  ]
  node [
    id 1033
    label "PiS"
  ]
  node [
    id 1034
    label "Depeche_Mode"
  ]
  node [
    id 1035
    label "Jakobici"
  ]
  node [
    id 1036
    label "rugby"
  ]
  node [
    id 1037
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1038
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1039
    label "PO"
  ]
  node [
    id 1040
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1041
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1042
    label "Federali&#347;ci"
  ]
  node [
    id 1043
    label "zespolik"
  ]
  node [
    id 1044
    label "PSL"
  ]
  node [
    id 1045
    label "ksi&#281;ga"
  ]
  node [
    id 1046
    label "chronograf"
  ]
  node [
    id 1047
    label "zapis"
  ]
  node [
    id 1048
    label "latopis"
  ]
  node [
    id 1049
    label "ok&#322;adka"
  ]
  node [
    id 1050
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1051
    label "prasa"
  ]
  node [
    id 1052
    label "dzia&#322;"
  ]
  node [
    id 1053
    label "zajawka"
  ]
  node [
    id 1054
    label "psychotest"
  ]
  node [
    id 1055
    label "Zwrotnica"
  ]
  node [
    id 1056
    label "egzemplarz"
  ]
  node [
    id 1057
    label "warto&#347;&#263;"
  ]
  node [
    id 1058
    label "zmienna"
  ]
  node [
    id 1059
    label "korzy&#347;&#263;"
  ]
  node [
    id 1060
    label "rewaluowanie"
  ]
  node [
    id 1061
    label "zrewaluowa&#263;"
  ]
  node [
    id 1062
    label "worth"
  ]
  node [
    id 1063
    label "rewaluowa&#263;"
  ]
  node [
    id 1064
    label "cel"
  ]
  node [
    id 1065
    label "wabik"
  ]
  node [
    id 1066
    label "wskazywa&#263;"
  ]
  node [
    id 1067
    label "zrewaluowanie"
  ]
  node [
    id 1068
    label "model"
  ]
  node [
    id 1069
    label "systemik"
  ]
  node [
    id 1070
    label "Android"
  ]
  node [
    id 1071
    label "podsystem"
  ]
  node [
    id 1072
    label "systemat"
  ]
  node [
    id 1073
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1074
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1075
    label "p&#322;&#243;d"
  ]
  node [
    id 1076
    label "konstelacja"
  ]
  node [
    id 1077
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1078
    label "oprogramowanie"
  ]
  node [
    id 1079
    label "j&#261;dro"
  ]
  node [
    id 1080
    label "rozprz&#261;c"
  ]
  node [
    id 1081
    label "usenet"
  ]
  node [
    id 1082
    label "jednostka_geologiczna"
  ]
  node [
    id 1083
    label "ryba"
  ]
  node [
    id 1084
    label "oddzia&#322;"
  ]
  node [
    id 1085
    label "net"
  ]
  node [
    id 1086
    label "podstawa"
  ]
  node [
    id 1087
    label "metoda"
  ]
  node [
    id 1088
    label "method"
  ]
  node [
    id 1089
    label "porz&#261;dek"
  ]
  node [
    id 1090
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1091
    label "w&#281;dkarstwo"
  ]
  node [
    id 1092
    label "doktryna"
  ]
  node [
    id 1093
    label "Leopard"
  ]
  node [
    id 1094
    label "o&#347;"
  ]
  node [
    id 1095
    label "sk&#322;ad"
  ]
  node [
    id 1096
    label "pulpit"
  ]
  node [
    id 1097
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1098
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1099
    label "cybernetyk"
  ]
  node [
    id 1100
    label "przyn&#281;ta"
  ]
  node [
    id 1101
    label "eratem"
  ]
  node [
    id 1102
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1103
    label "strategia"
  ]
  node [
    id 1104
    label "background"
  ]
  node [
    id 1105
    label "punkt_odniesienia"
  ]
  node [
    id 1106
    label "zasadzenie"
  ]
  node [
    id 1107
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1108
    label "podstawowy"
  ]
  node [
    id 1109
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1110
    label "d&#243;&#322;"
  ]
  node [
    id 1111
    label "documentation"
  ]
  node [
    id 1112
    label "pomys&#322;"
  ]
  node [
    id 1113
    label "zasadzi&#263;"
  ]
  node [
    id 1114
    label "pot&#281;ga"
  ]
  node [
    id 1115
    label "narz&#281;dzie"
  ]
  node [
    id 1116
    label "nature"
  ]
  node [
    id 1117
    label "tryb"
  ]
  node [
    id 1118
    label "uk&#322;ad"
  ]
  node [
    id 1119
    label "normalizacja"
  ]
  node [
    id 1120
    label "styl_architektoniczny"
  ]
  node [
    id 1121
    label "relacja"
  ]
  node [
    id 1122
    label "zasada"
  ]
  node [
    id 1123
    label "pakiet_klimatyczny"
  ]
  node [
    id 1124
    label "uprawianie"
  ]
  node [
    id 1125
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1126
    label "gathering"
  ]
  node [
    id 1127
    label "album"
  ]
  node [
    id 1128
    label "praca_rolnicza"
  ]
  node [
    id 1129
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1130
    label "sum"
  ]
  node [
    id 1131
    label "series"
  ]
  node [
    id 1132
    label "dane"
  ]
  node [
    id 1133
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1134
    label "skumanie"
  ]
  node [
    id 1135
    label "pos&#322;uchanie"
  ]
  node [
    id 1136
    label "teoria"
  ]
  node [
    id 1137
    label "clasp"
  ]
  node [
    id 1138
    label "przem&#243;wienie"
  ]
  node [
    id 1139
    label "konstrukcja"
  ]
  node [
    id 1140
    label "mechanika"
  ]
  node [
    id 1141
    label "system_komputerowy"
  ]
  node [
    id 1142
    label "sprz&#281;t"
  ]
  node [
    id 1143
    label "embryo"
  ]
  node [
    id 1144
    label "moczownik"
  ]
  node [
    id 1145
    label "latawiec"
  ]
  node [
    id 1146
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1147
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1148
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1149
    label "zarodek"
  ]
  node [
    id 1150
    label "reengineering"
  ]
  node [
    id 1151
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1152
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1153
    label "integer"
  ]
  node [
    id 1154
    label "zlewanie_si&#281;"
  ]
  node [
    id 1155
    label "pe&#322;ny"
  ]
  node [
    id 1156
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1157
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 1158
    label "grupa_dyskusyjna"
  ]
  node [
    id 1159
    label "doctrine"
  ]
  node [
    id 1160
    label "tar&#322;o"
  ]
  node [
    id 1161
    label "rakowato&#347;&#263;"
  ]
  node [
    id 1162
    label "szczelina_skrzelowa"
  ]
  node [
    id 1163
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 1164
    label "doniczkowiec"
  ]
  node [
    id 1165
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 1166
    label "mi&#281;so"
  ]
  node [
    id 1167
    label "fish"
  ]
  node [
    id 1168
    label "patroszy&#263;"
  ]
  node [
    id 1169
    label "linia_boczna"
  ]
  node [
    id 1170
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 1171
    label "pokrywa_skrzelowa"
  ]
  node [
    id 1172
    label "kr&#281;gowiec"
  ]
  node [
    id 1173
    label "ryby"
  ]
  node [
    id 1174
    label "m&#281;tnooki"
  ]
  node [
    id 1175
    label "ikra"
  ]
  node [
    id 1176
    label "wyrostek_filtracyjny"
  ]
  node [
    id 1177
    label "sport"
  ]
  node [
    id 1178
    label "urozmaicenie"
  ]
  node [
    id 1179
    label "pu&#322;apka"
  ]
  node [
    id 1180
    label "pon&#281;ta"
  ]
  node [
    id 1181
    label "blat"
  ]
  node [
    id 1182
    label "okno"
  ]
  node [
    id 1183
    label "mebel"
  ]
  node [
    id 1184
    label "system_operacyjny"
  ]
  node [
    id 1185
    label "interfejs"
  ]
  node [
    id 1186
    label "ikona"
  ]
  node [
    id 1187
    label "oswobodzi&#263;"
  ]
  node [
    id 1188
    label "disengage"
  ]
  node [
    id 1189
    label "zdezorganizowa&#263;"
  ]
  node [
    id 1190
    label "post"
  ]
  node [
    id 1191
    label "etolog"
  ]
  node [
    id 1192
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1193
    label "dieta"
  ]
  node [
    id 1194
    label "zdyscyplinowanie"
  ]
  node [
    id 1195
    label "zwierz&#281;"
  ]
  node [
    id 1196
    label "observation"
  ]
  node [
    id 1197
    label "behawior"
  ]
  node [
    id 1198
    label "zrobienie"
  ]
  node [
    id 1199
    label "przechowanie"
  ]
  node [
    id 1200
    label "podtrzymanie"
  ]
  node [
    id 1201
    label "post&#261;pienie"
  ]
  node [
    id 1202
    label "oswobodzenie"
  ]
  node [
    id 1203
    label "zdezorganizowanie"
  ]
  node [
    id 1204
    label "relaxation"
  ]
  node [
    id 1205
    label "naukowiec"
  ]
  node [
    id 1206
    label "b&#322;&#261;d"
  ]
  node [
    id 1207
    label "matryca"
  ]
  node [
    id 1208
    label "facet"
  ]
  node [
    id 1209
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1210
    label "mildew"
  ]
  node [
    id 1211
    label "ideal"
  ]
  node [
    id 1212
    label "adaptation"
  ]
  node [
    id 1213
    label "ruch"
  ]
  node [
    id 1214
    label "imitacja"
  ]
  node [
    id 1215
    label "pozowa&#263;"
  ]
  node [
    id 1216
    label "orygina&#322;"
  ]
  node [
    id 1217
    label "motif"
  ]
  node [
    id 1218
    label "prezenter"
  ]
  node [
    id 1219
    label "pozowanie"
  ]
  node [
    id 1220
    label "filia"
  ]
  node [
    id 1221
    label "bank"
  ]
  node [
    id 1222
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 1223
    label "klasa"
  ]
  node [
    id 1224
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 1225
    label "agencja"
  ]
  node [
    id 1226
    label "malm"
  ]
  node [
    id 1227
    label "promocja"
  ]
  node [
    id 1228
    label "szpital"
  ]
  node [
    id 1229
    label "siedziba"
  ]
  node [
    id 1230
    label "dogger"
  ]
  node [
    id 1231
    label "ajencja"
  ]
  node [
    id 1232
    label "poziom"
  ]
  node [
    id 1233
    label "lias"
  ]
  node [
    id 1234
    label "kurs"
  ]
  node [
    id 1235
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 1236
    label "algebra_liniowa"
  ]
  node [
    id 1237
    label "chromosom"
  ]
  node [
    id 1238
    label "&#347;rodek"
  ]
  node [
    id 1239
    label "nasieniak"
  ]
  node [
    id 1240
    label "nukleon"
  ]
  node [
    id 1241
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 1242
    label "ziarno"
  ]
  node [
    id 1243
    label "j&#261;derko"
  ]
  node [
    id 1244
    label "macierz_j&#261;drowa"
  ]
  node [
    id 1245
    label "anorchizm"
  ]
  node [
    id 1246
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 1247
    label "wn&#281;trostwo"
  ]
  node [
    id 1248
    label "kariokineza"
  ]
  node [
    id 1249
    label "nukleosynteza"
  ]
  node [
    id 1250
    label "organellum"
  ]
  node [
    id 1251
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 1252
    label "chemia_j&#261;drowa"
  ]
  node [
    id 1253
    label "atom"
  ]
  node [
    id 1254
    label "przeciwobraz"
  ]
  node [
    id 1255
    label "jajo"
  ]
  node [
    id 1256
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 1257
    label "core"
  ]
  node [
    id 1258
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1259
    label "protoplazma"
  ]
  node [
    id 1260
    label "moszna"
  ]
  node [
    id 1261
    label "subsystem"
  ]
  node [
    id 1262
    label "suport"
  ]
  node [
    id 1263
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 1264
    label "o&#347;rodek"
  ]
  node [
    id 1265
    label "ko&#322;o"
  ]
  node [
    id 1266
    label "eonotem"
  ]
  node [
    id 1267
    label "constellation"
  ]
  node [
    id 1268
    label "W&#261;&#380;"
  ]
  node [
    id 1269
    label "Panna"
  ]
  node [
    id 1270
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 1271
    label "W&#281;&#380;ownik"
  ]
  node [
    id 1272
    label "Ptak_Rajski"
  ]
  node [
    id 1273
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1274
    label "fabryka"
  ]
  node [
    id 1275
    label "pole"
  ]
  node [
    id 1276
    label "pas"
  ]
  node [
    id 1277
    label "blokada"
  ]
  node [
    id 1278
    label "tabulacja"
  ]
  node [
    id 1279
    label "hurtownia"
  ]
  node [
    id 1280
    label "basic"
  ]
  node [
    id 1281
    label "obr&#243;bka"
  ]
  node [
    id 1282
    label "rank_and_file"
  ]
  node [
    id 1283
    label "pomieszczenie"
  ]
  node [
    id 1284
    label "syf"
  ]
  node [
    id 1285
    label "sk&#322;adnik"
  ]
  node [
    id 1286
    label "constitution"
  ]
  node [
    id 1287
    label "sklep"
  ]
  node [
    id 1288
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1289
    label "suma_ubezpieczenia"
  ]
  node [
    id 1290
    label "przyznanie"
  ]
  node [
    id 1291
    label "franszyza"
  ]
  node [
    id 1292
    label "umowa"
  ]
  node [
    id 1293
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 1294
    label "ochrona"
  ]
  node [
    id 1295
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 1296
    label "cover"
  ]
  node [
    id 1297
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 1298
    label "op&#322;ata"
  ]
  node [
    id 1299
    label "screen"
  ]
  node [
    id 1300
    label "uchronienie"
  ]
  node [
    id 1301
    label "ubezpieczalnia"
  ]
  node [
    id 1302
    label "insurance"
  ]
  node [
    id 1303
    label "proposition"
  ]
  node [
    id 1304
    label "obietnica"
  ]
  node [
    id 1305
    label "za&#347;wiadczenie"
  ]
  node [
    id 1306
    label "security"
  ]
  node [
    id 1307
    label "automatyczny"
  ]
  node [
    id 1308
    label "warunek"
  ]
  node [
    id 1309
    label "zawarcie"
  ]
  node [
    id 1310
    label "zawrze&#263;"
  ]
  node [
    id 1311
    label "contract"
  ]
  node [
    id 1312
    label "porozumienie"
  ]
  node [
    id 1313
    label "gestia_transportowa"
  ]
  node [
    id 1314
    label "klauzula"
  ]
  node [
    id 1315
    label "tarcza"
  ]
  node [
    id 1316
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1317
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 1318
    label "borowiec"
  ]
  node [
    id 1319
    label "obstawienie"
  ]
  node [
    id 1320
    label "chemical_bond"
  ]
  node [
    id 1321
    label "obstawianie"
  ]
  node [
    id 1322
    label "transportacja"
  ]
  node [
    id 1323
    label "obstawia&#263;"
  ]
  node [
    id 1324
    label "oznajmienie"
  ]
  node [
    id 1325
    label "confession"
  ]
  node [
    id 1326
    label "recognition"
  ]
  node [
    id 1327
    label "stwierdzenie"
  ]
  node [
    id 1328
    label "danie"
  ]
  node [
    id 1329
    label "preservation"
  ]
  node [
    id 1330
    label "safety"
  ]
  node [
    id 1331
    label "test_zderzeniowy"
  ]
  node [
    id 1332
    label "ubezpieczanie"
  ]
  node [
    id 1333
    label "katapultowanie"
  ]
  node [
    id 1334
    label "ubezpiecza&#263;"
  ]
  node [
    id 1335
    label "BHP"
  ]
  node [
    id 1336
    label "ubezpieczy&#263;"
  ]
  node [
    id 1337
    label "katapultowa&#263;"
  ]
  node [
    id 1338
    label "ubezpieczy&#263;_si&#281;"
  ]
  node [
    id 1339
    label "ubezpieczenie_si&#281;"
  ]
  node [
    id 1340
    label "screenshot"
  ]
  node [
    id 1341
    label "aran&#380;acja"
  ]
  node [
    id 1342
    label "publiczny"
  ]
  node [
    id 1343
    label "niepubliczny"
  ]
  node [
    id 1344
    label "spo&#322;ecznie"
  ]
  node [
    id 1345
    label "publicznie"
  ]
  node [
    id 1346
    label "upublicznienie"
  ]
  node [
    id 1347
    label "upublicznianie"
  ]
  node [
    id 1348
    label "jawny"
  ]
  node [
    id 1349
    label "podnosi&#263;"
  ]
  node [
    id 1350
    label "otrzymywa&#263;"
  ]
  node [
    id 1351
    label "rozpowszechnia&#263;"
  ]
  node [
    id 1352
    label "odsuwa&#263;"
  ]
  node [
    id 1353
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 1354
    label "zanosi&#263;"
  ]
  node [
    id 1355
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 1356
    label "kra&#347;&#263;"
  ]
  node [
    id 1357
    label "traktowa&#263;"
  ]
  node [
    id 1358
    label "nagradza&#263;"
  ]
  node [
    id 1359
    label "sign"
  ]
  node [
    id 1360
    label "forytowa&#263;"
  ]
  node [
    id 1361
    label "przyw&#322;aszcza&#263;"
  ]
  node [
    id 1362
    label "podsuwa&#263;"
  ]
  node [
    id 1363
    label "overcharge"
  ]
  node [
    id 1364
    label "podpierdala&#263;"
  ]
  node [
    id 1365
    label "mie&#263;_lepkie_r&#281;ce"
  ]
  node [
    id 1366
    label "r&#261;ba&#263;"
  ]
  node [
    id 1367
    label "zar&#261;bywa&#263;"
  ]
  node [
    id 1368
    label "przejmowa&#263;"
  ]
  node [
    id 1369
    label "saturate"
  ]
  node [
    id 1370
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 1371
    label "take"
  ]
  node [
    id 1372
    label "return"
  ]
  node [
    id 1373
    label "dostawa&#263;"
  ]
  node [
    id 1374
    label "wycenia&#263;"
  ]
  node [
    id 1375
    label "wymienia&#263;"
  ]
  node [
    id 1376
    label "mierzy&#263;"
  ]
  node [
    id 1377
    label "dyskalkulia"
  ]
  node [
    id 1378
    label "policza&#263;"
  ]
  node [
    id 1379
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1380
    label "odlicza&#263;"
  ]
  node [
    id 1381
    label "bra&#263;"
  ]
  node [
    id 1382
    label "count"
  ]
  node [
    id 1383
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1384
    label "admit"
  ]
  node [
    id 1385
    label "dodawa&#263;"
  ]
  node [
    id 1386
    label "rachowa&#263;"
  ]
  node [
    id 1387
    label "posiada&#263;"
  ]
  node [
    id 1388
    label "wynagrodzenie"
  ]
  node [
    id 1389
    label "dostarcza&#263;"
  ]
  node [
    id 1390
    label "usi&#322;owa&#263;"
  ]
  node [
    id 1391
    label "kry&#263;"
  ]
  node [
    id 1392
    label "przenosi&#263;"
  ]
  node [
    id 1393
    label "seclude"
  ]
  node [
    id 1394
    label "przestawa&#263;"
  ]
  node [
    id 1395
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 1396
    label "remove"
  ]
  node [
    id 1397
    label "odci&#261;ga&#263;"
  ]
  node [
    id 1398
    label "retard"
  ]
  node [
    id 1399
    label "oddala&#263;"
  ]
  node [
    id 1400
    label "dissolve"
  ]
  node [
    id 1401
    label "blurt_out"
  ]
  node [
    id 1402
    label "odk&#322;ada&#263;"
  ]
  node [
    id 1403
    label "przemieszcza&#263;"
  ]
  node [
    id 1404
    label "przesuwa&#263;"
  ]
  node [
    id 1405
    label "generalize"
  ]
  node [
    id 1406
    label "sprawia&#263;"
  ]
  node [
    id 1407
    label "demaskator"
  ]
  node [
    id 1408
    label "dostrzega&#263;"
  ]
  node [
    id 1409
    label "objawia&#263;"
  ]
  node [
    id 1410
    label "indicate"
  ]
  node [
    id 1411
    label "pia&#263;"
  ]
  node [
    id 1412
    label "os&#322;awia&#263;"
  ]
  node [
    id 1413
    label "escalate"
  ]
  node [
    id 1414
    label "tire"
  ]
  node [
    id 1415
    label "lift"
  ]
  node [
    id 1416
    label "chwali&#263;"
  ]
  node [
    id 1417
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1418
    label "ulepsza&#263;"
  ]
  node [
    id 1419
    label "drive"
  ]
  node [
    id 1420
    label "enhance"
  ]
  node [
    id 1421
    label "rise"
  ]
  node [
    id 1422
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1423
    label "przybli&#380;a&#263;"
  ]
  node [
    id 1424
    label "odbudowywa&#263;"
  ]
  node [
    id 1425
    label "zmienia&#263;"
  ]
  node [
    id 1426
    label "za&#322;apywa&#263;"
  ]
  node [
    id 1427
    label "zaczyna&#263;"
  ]
  node [
    id 1428
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 1429
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 1430
    label "pieni&#261;dze"
  ]
  node [
    id 1431
    label "wynie&#347;&#263;"
  ]
  node [
    id 1432
    label "limit"
  ]
  node [
    id 1433
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 1434
    label "grosz"
  ]
  node [
    id 1435
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 1436
    label "doskona&#322;y"
  ]
  node [
    id 1437
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 1438
    label "szlachetny"
  ]
  node [
    id 1439
    label "utytu&#322;owany"
  ]
  node [
    id 1440
    label "kochany"
  ]
  node [
    id 1441
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 1442
    label "Polska"
  ]
  node [
    id 1443
    label "z&#322;ocenie"
  ]
  node [
    id 1444
    label "metaliczny"
  ]
  node [
    id 1445
    label "jednostka_monetarna"
  ]
  node [
    id 1446
    label "wspania&#322;y"
  ]
  node [
    id 1447
    label "poz&#322;ocenie"
  ]
  node [
    id 1448
    label "wybitny"
  ]
  node [
    id 1449
    label "prominentny"
  ]
  node [
    id 1450
    label "znany"
  ]
  node [
    id 1451
    label "&#347;wietny"
  ]
  node [
    id 1452
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1453
    label "naj"
  ]
  node [
    id 1454
    label "doskonale"
  ]
  node [
    id 1455
    label "uczciwy"
  ]
  node [
    id 1456
    label "dobry"
  ]
  node [
    id 1457
    label "harmonijny"
  ]
  node [
    id 1458
    label "zacny"
  ]
  node [
    id 1459
    label "pi&#281;kny"
  ]
  node [
    id 1460
    label "szlachetnie"
  ]
  node [
    id 1461
    label "gatunkowy"
  ]
  node [
    id 1462
    label "metalicznie"
  ]
  node [
    id 1463
    label "typowy"
  ]
  node [
    id 1464
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 1465
    label "metaloplastyczny"
  ]
  node [
    id 1466
    label "wybranek"
  ]
  node [
    id 1467
    label "kochanek"
  ]
  node [
    id 1468
    label "drogi"
  ]
  node [
    id 1469
    label "umi&#322;owany"
  ]
  node [
    id 1470
    label "kochanie"
  ]
  node [
    id 1471
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1472
    label "zajebisty"
  ]
  node [
    id 1473
    label "&#347;wietnie"
  ]
  node [
    id 1474
    label "warto&#347;ciowy"
  ]
  node [
    id 1475
    label "spania&#322;y"
  ]
  node [
    id 1476
    label "pozytywny"
  ]
  node [
    id 1477
    label "pomy&#347;lny"
  ]
  node [
    id 1478
    label "wspaniale"
  ]
  node [
    id 1479
    label "bogato"
  ]
  node [
    id 1480
    label "kolorowy"
  ]
  node [
    id 1481
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 1482
    label "typ_mongoloidalny"
  ]
  node [
    id 1483
    label "jasny"
  ]
  node [
    id 1484
    label "ciep&#322;y"
  ]
  node [
    id 1485
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 1486
    label "groszak"
  ]
  node [
    id 1487
    label "szyling_austryjacki"
  ]
  node [
    id 1488
    label "moneta"
  ]
  node [
    id 1489
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1490
    label "Krajna"
  ]
  node [
    id 1491
    label "Kielecczyzna"
  ]
  node [
    id 1492
    label "NATO"
  ]
  node [
    id 1493
    label "Opolskie"
  ]
  node [
    id 1494
    label "Lubuskie"
  ]
  node [
    id 1495
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1496
    label "Kaczawa"
  ]
  node [
    id 1497
    label "Podlasie"
  ]
  node [
    id 1498
    label "Wolin"
  ]
  node [
    id 1499
    label "Wielkopolska"
  ]
  node [
    id 1500
    label "Lubelszczyzna"
  ]
  node [
    id 1501
    label "Izera"
  ]
  node [
    id 1502
    label "So&#322;a"
  ]
  node [
    id 1503
    label "Wis&#322;a"
  ]
  node [
    id 1504
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1505
    label "Pa&#322;uki"
  ]
  node [
    id 1506
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1507
    label "Podkarpacie"
  ]
  node [
    id 1508
    label "barwy_polskie"
  ]
  node [
    id 1509
    label "Kujawy"
  ]
  node [
    id 1510
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1511
    label "Nadbu&#380;e"
  ]
  node [
    id 1512
    label "Warmia"
  ]
  node [
    id 1513
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1514
    label "Suwalszczyzna"
  ]
  node [
    id 1515
    label "Bory_Tucholskie"
  ]
  node [
    id 1516
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1517
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1518
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1519
    label "Ma&#322;opolska"
  ]
  node [
    id 1520
    label "Unia_Europejska"
  ]
  node [
    id 1521
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1522
    label "Mazury"
  ]
  node [
    id 1523
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1524
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1525
    label "Powi&#347;le"
  ]
  node [
    id 1526
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1527
    label "gilt"
  ]
  node [
    id 1528
    label "z&#322;ocisty"
  ]
  node [
    id 1529
    label "club"
  ]
  node [
    id 1530
    label "plating"
  ]
  node [
    id 1531
    label "barwienie"
  ]
  node [
    id 1532
    label "zdobienie"
  ]
  node [
    id 1533
    label "platerowanie"
  ]
  node [
    id 1534
    label "powleczenie"
  ]
  node [
    id 1535
    label "zabarwienie"
  ]
  node [
    id 1536
    label "piwo"
  ]
  node [
    id 1537
    label "warzy&#263;"
  ]
  node [
    id 1538
    label "wyj&#347;cie"
  ]
  node [
    id 1539
    label "browarnia"
  ]
  node [
    id 1540
    label "nawarzenie"
  ]
  node [
    id 1541
    label "uwarzy&#263;"
  ]
  node [
    id 1542
    label "uwarzenie"
  ]
  node [
    id 1543
    label "bacik"
  ]
  node [
    id 1544
    label "warzenie"
  ]
  node [
    id 1545
    label "alkohol"
  ]
  node [
    id 1546
    label "birofilia"
  ]
  node [
    id 1547
    label "nap&#243;j"
  ]
  node [
    id 1548
    label "nawarzy&#263;"
  ]
  node [
    id 1549
    label "anta&#322;"
  ]
  node [
    id 1550
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 1551
    label "happening"
  ]
  node [
    id 1552
    label "pacjent"
  ]
  node [
    id 1553
    label "przeznaczenie"
  ]
  node [
    id 1554
    label "przyk&#322;ad"
  ]
  node [
    id 1555
    label "kategoria_gramatyczna"
  ]
  node [
    id 1556
    label "schorzenie"
  ]
  node [
    id 1557
    label "ilustracja"
  ]
  node [
    id 1558
    label "fakt"
  ]
  node [
    id 1559
    label "przedstawiciel"
  ]
  node [
    id 1560
    label "destiny"
  ]
  node [
    id 1561
    label "przymus"
  ]
  node [
    id 1562
    label "wybranie"
  ]
  node [
    id 1563
    label "rzuci&#263;"
  ]
  node [
    id 1564
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1565
    label "przydzielenie"
  ]
  node [
    id 1566
    label "oblat"
  ]
  node [
    id 1567
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1568
    label "ustalenie"
  ]
  node [
    id 1569
    label "odezwanie_si&#281;"
  ]
  node [
    id 1570
    label "zaburzenie"
  ]
  node [
    id 1571
    label "ognisko"
  ]
  node [
    id 1572
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1573
    label "atakowanie"
  ]
  node [
    id 1574
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1575
    label "remisja"
  ]
  node [
    id 1576
    label "nabawianie_si&#281;"
  ]
  node [
    id 1577
    label "odzywanie_si&#281;"
  ]
  node [
    id 1578
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1579
    label "powalenie"
  ]
  node [
    id 1580
    label "diagnoza"
  ]
  node [
    id 1581
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1582
    label "atakowa&#263;"
  ]
  node [
    id 1583
    label "inkubacja"
  ]
  node [
    id 1584
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1585
    label "grupa_ryzyka"
  ]
  node [
    id 1586
    label "badanie_histopatologiczne"
  ]
  node [
    id 1587
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1588
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1589
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1590
    label "zajmowanie"
  ]
  node [
    id 1591
    label "powali&#263;"
  ]
  node [
    id 1592
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1593
    label "zajmowa&#263;"
  ]
  node [
    id 1594
    label "kryzys"
  ]
  node [
    id 1595
    label "nabawienie_si&#281;"
  ]
  node [
    id 1596
    label "od&#322;&#261;czenie"
  ]
  node [
    id 1597
    label "piel&#281;gniarz"
  ]
  node [
    id 1598
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 1599
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 1600
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 1601
    label "chory"
  ]
  node [
    id 1602
    label "szpitalnik"
  ]
  node [
    id 1603
    label "od&#322;&#261;czanie"
  ]
  node [
    id 1604
    label "klient"
  ]
  node [
    id 1605
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1606
    label "Gargantua"
  ]
  node [
    id 1607
    label "Chocho&#322;"
  ]
  node [
    id 1608
    label "Hamlet"
  ]
  node [
    id 1609
    label "profanum"
  ]
  node [
    id 1610
    label "Wallenrod"
  ]
  node [
    id 1611
    label "Quasimodo"
  ]
  node [
    id 1612
    label "homo_sapiens"
  ]
  node [
    id 1613
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1614
    label "Plastu&#347;"
  ]
  node [
    id 1615
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1616
    label "portrecista"
  ]
  node [
    id 1617
    label "Casanova"
  ]
  node [
    id 1618
    label "Szwejk"
  ]
  node [
    id 1619
    label "Edyp"
  ]
  node [
    id 1620
    label "Don_Juan"
  ]
  node [
    id 1621
    label "koniugacja"
  ]
  node [
    id 1622
    label "Werter"
  ]
  node [
    id 1623
    label "duch"
  ]
  node [
    id 1624
    label "person"
  ]
  node [
    id 1625
    label "Harry_Potter"
  ]
  node [
    id 1626
    label "Sherlock_Holmes"
  ]
  node [
    id 1627
    label "antropochoria"
  ]
  node [
    id 1628
    label "Dwukwiat"
  ]
  node [
    id 1629
    label "mikrokosmos"
  ]
  node [
    id 1630
    label "Winnetou"
  ]
  node [
    id 1631
    label "Don_Kiszot"
  ]
  node [
    id 1632
    label "Herkules_Poirot"
  ]
  node [
    id 1633
    label "Faust"
  ]
  node [
    id 1634
    label "Zgredek"
  ]
  node [
    id 1635
    label "Dulcynea"
  ]
  node [
    id 1636
    label "superego"
  ]
  node [
    id 1637
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1638
    label "wn&#281;trze"
  ]
  node [
    id 1639
    label "psychika"
  ]
  node [
    id 1640
    label "hamper"
  ]
  node [
    id 1641
    label "pora&#380;a&#263;"
  ]
  node [
    id 1642
    label "mrozi&#263;"
  ]
  node [
    id 1643
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1644
    label "spasm"
  ]
  node [
    id 1645
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 1646
    label "czasownik"
  ]
  node [
    id 1647
    label "coupling"
  ]
  node [
    id 1648
    label "fleksja"
  ]
  node [
    id 1649
    label "orz&#281;sek"
  ]
  node [
    id 1650
    label "malarz"
  ]
  node [
    id 1651
    label "artysta"
  ]
  node [
    id 1652
    label "fotograf"
  ]
  node [
    id 1653
    label "&#347;lad"
  ]
  node [
    id 1654
    label "lobbysta"
  ]
  node [
    id 1655
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1656
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1657
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1658
    label "umys&#322;"
  ]
  node [
    id 1659
    label "sztuka"
  ]
  node [
    id 1660
    label "czaszka"
  ]
  node [
    id 1661
    label "fryzura"
  ]
  node [
    id 1662
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1663
    label "pryncypa&#322;"
  ]
  node [
    id 1664
    label "ro&#347;lina"
  ]
  node [
    id 1665
    label "ucho"
  ]
  node [
    id 1666
    label "byd&#322;o"
  ]
  node [
    id 1667
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1668
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1669
    label "kierownictwo"
  ]
  node [
    id 1670
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1671
    label "cz&#322;onek"
  ]
  node [
    id 1672
    label "makrocefalia"
  ]
  node [
    id 1673
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1674
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1675
    label "&#380;ycie"
  ]
  node [
    id 1676
    label "dekiel"
  ]
  node [
    id 1677
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1678
    label "m&#243;zg"
  ]
  node [
    id 1679
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1680
    label "noosfera"
  ]
  node [
    id 1681
    label "allochoria"
  ]
  node [
    id 1682
    label "gestaltyzm"
  ]
  node [
    id 1683
    label "stylistyka"
  ]
  node [
    id 1684
    label "podzbi&#243;r"
  ]
  node [
    id 1685
    label "styl"
  ]
  node [
    id 1686
    label "antycypacja"
  ]
  node [
    id 1687
    label "wiersz"
  ]
  node [
    id 1688
    label "popis"
  ]
  node [
    id 1689
    label "obraz"
  ]
  node [
    id 1690
    label "p&#322;aszczyzna"
  ]
  node [
    id 1691
    label "symetria"
  ]
  node [
    id 1692
    label "figure"
  ]
  node [
    id 1693
    label "perspektywa"
  ]
  node [
    id 1694
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1695
    label "character"
  ]
  node [
    id 1696
    label "rze&#378;ba"
  ]
  node [
    id 1697
    label "shape"
  ]
  node [
    id 1698
    label "bierka_szachowa"
  ]
  node [
    id 1699
    label "karta"
  ]
  node [
    id 1700
    label "dziedzina"
  ]
  node [
    id 1701
    label "Szekspir"
  ]
  node [
    id 1702
    label "Mickiewicz"
  ]
  node [
    id 1703
    label "cierpienie"
  ]
  node [
    id 1704
    label "deformowa&#263;"
  ]
  node [
    id 1705
    label "deformowanie"
  ]
  node [
    id 1706
    label "sfera_afektywna"
  ]
  node [
    id 1707
    label "sumienie"
  ]
  node [
    id 1708
    label "entity"
  ]
  node [
    id 1709
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1710
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1711
    label "fizjonomia"
  ]
  node [
    id 1712
    label "power"
  ]
  node [
    id 1713
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1714
    label "human_body"
  ]
  node [
    id 1715
    label "podekscytowanie"
  ]
  node [
    id 1716
    label "kompleks"
  ]
  node [
    id 1717
    label "oddech"
  ]
  node [
    id 1718
    label "ofiarowywa&#263;"
  ]
  node [
    id 1719
    label "nekromancja"
  ]
  node [
    id 1720
    label "zjawa"
  ]
  node [
    id 1721
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1722
    label "ego"
  ]
  node [
    id 1723
    label "ofiarowa&#263;"
  ]
  node [
    id 1724
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1725
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1726
    label "Po&#347;wist"
  ]
  node [
    id 1727
    label "passion"
  ]
  node [
    id 1728
    label "zmar&#322;y"
  ]
  node [
    id 1729
    label "ofiarowanie"
  ]
  node [
    id 1730
    label "ofiarowywanie"
  ]
  node [
    id 1731
    label "T&#281;sknica"
  ]
  node [
    id 1732
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1733
    label "przyroda"
  ]
  node [
    id 1734
    label "kosmos"
  ]
  node [
    id 1735
    label "Ziemia"
  ]
  node [
    id 1736
    label "activity"
  ]
  node [
    id 1737
    label "absolutorium"
  ]
  node [
    id 1738
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1739
    label "ocena"
  ]
  node [
    id 1740
    label "uko&#324;czenie"
  ]
  node [
    id 1741
    label "graduation"
  ]
  node [
    id 1742
    label "kapita&#322;"
  ]
  node [
    id 1743
    label "gospodarski"
  ]
  node [
    id 1744
    label "gospodarnie"
  ]
  node [
    id 1745
    label "porz&#261;dny"
  ]
  node [
    id 1746
    label "dziarski"
  ]
  node [
    id 1747
    label "oszcz&#281;dny"
  ]
  node [
    id 1748
    label "stronniczy"
  ]
  node [
    id 1749
    label "wiejski"
  ]
  node [
    id 1750
    label "po_gospodarsku"
  ]
  node [
    id 1751
    label "racjonalny"
  ]
  node [
    id 1752
    label "ubezpieczenie_emerytalne"
  ]
  node [
    id 1753
    label "&#347;wiadczenie_spo&#322;eczne"
  ]
  node [
    id 1754
    label "retirement"
  ]
  node [
    id 1755
    label "egzystencja"
  ]
  node [
    id 1756
    label "chronometria"
  ]
  node [
    id 1757
    label "odczyt"
  ]
  node [
    id 1758
    label "laba"
  ]
  node [
    id 1759
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1760
    label "time_period"
  ]
  node [
    id 1761
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1762
    label "Zeitgeist"
  ]
  node [
    id 1763
    label "pochodzenie"
  ]
  node [
    id 1764
    label "przep&#322;ywanie"
  ]
  node [
    id 1765
    label "schy&#322;ek"
  ]
  node [
    id 1766
    label "czwarty_wymiar"
  ]
  node [
    id 1767
    label "poprzedzi&#263;"
  ]
  node [
    id 1768
    label "pogoda"
  ]
  node [
    id 1769
    label "czasokres"
  ]
  node [
    id 1770
    label "poprzedzenie"
  ]
  node [
    id 1771
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1772
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1773
    label "dzieje"
  ]
  node [
    id 1774
    label "zegar"
  ]
  node [
    id 1775
    label "trawi&#263;"
  ]
  node [
    id 1776
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1777
    label "poprzedza&#263;"
  ]
  node [
    id 1778
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1779
    label "trawienie"
  ]
  node [
    id 1780
    label "rachuba_czasu"
  ]
  node [
    id 1781
    label "poprzedzanie"
  ]
  node [
    id 1782
    label "okres_czasu"
  ]
  node [
    id 1783
    label "period"
  ]
  node [
    id 1784
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1785
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1786
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1787
    label "pochodzi&#263;"
  ]
  node [
    id 1788
    label "utrzyma&#263;"
  ]
  node [
    id 1789
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1790
    label "warunki"
  ]
  node [
    id 1791
    label "utrzymanie"
  ]
  node [
    id 1792
    label "utrzymywanie"
  ]
  node [
    id 1793
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1794
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1795
    label "wiek_matuzalemowy"
  ]
  node [
    id 1796
    label "utrzymywa&#263;"
  ]
  node [
    id 1797
    label "doch&#243;d"
  ]
  node [
    id 1798
    label "economic_rent"
  ]
  node [
    id 1799
    label "krzywa_Engla"
  ]
  node [
    id 1800
    label "income"
  ]
  node [
    id 1801
    label "stopa_procentowa"
  ]
  node [
    id 1802
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 1803
    label "druk"
  ]
  node [
    id 1804
    label "mianowaniec"
  ]
  node [
    id 1805
    label "podtytu&#322;"
  ]
  node [
    id 1806
    label "poster"
  ]
  node [
    id 1807
    label "publikacja"
  ]
  node [
    id 1808
    label "nadtytu&#322;"
  ]
  node [
    id 1809
    label "redaktor"
  ]
  node [
    id 1810
    label "nazwa"
  ]
  node [
    id 1811
    label "szata_graficzna"
  ]
  node [
    id 1812
    label "debit"
  ]
  node [
    id 1813
    label "tytulatura"
  ]
  node [
    id 1814
    label "elevation"
  ]
  node [
    id 1815
    label "formatowa&#263;"
  ]
  node [
    id 1816
    label "tkanina"
  ]
  node [
    id 1817
    label "glif"
  ]
  node [
    id 1818
    label "printing"
  ]
  node [
    id 1819
    label "zdobnik"
  ]
  node [
    id 1820
    label "zaproszenie"
  ]
  node [
    id 1821
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1822
    label "formatowanie"
  ]
  node [
    id 1823
    label "cymelium"
  ]
  node [
    id 1824
    label "impression"
  ]
  node [
    id 1825
    label "technika"
  ]
  node [
    id 1826
    label "prohibita"
  ]
  node [
    id 1827
    label "dese&#324;"
  ]
  node [
    id 1828
    label "notification"
  ]
  node [
    id 1829
    label "patron"
  ]
  node [
    id 1830
    label "wezwanie"
  ]
  node [
    id 1831
    label "redakcja"
  ]
  node [
    id 1832
    label "edytor"
  ]
  node [
    id 1833
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1834
    label "bran&#380;owiec"
  ]
  node [
    id 1835
    label "skojarzy&#263;"
  ]
  node [
    id 1836
    label "dress"
  ]
  node [
    id 1837
    label "zadenuncjowa&#263;"
  ]
  node [
    id 1838
    label "wprowadzi&#263;"
  ]
  node [
    id 1839
    label "picture"
  ]
  node [
    id 1840
    label "ujawni&#263;"
  ]
  node [
    id 1841
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 1842
    label "wytworzy&#263;"
  ]
  node [
    id 1843
    label "powierzy&#263;"
  ]
  node [
    id 1844
    label "translate"
  ]
  node [
    id 1845
    label "stanowisko"
  ]
  node [
    id 1846
    label "mandatariusz"
  ]
  node [
    id 1847
    label "afisz"
  ]
  node [
    id 1848
    label "inability"
  ]
  node [
    id 1849
    label "brak"
  ]
  node [
    id 1850
    label "Arakan"
  ]
  node [
    id 1851
    label "Teksas"
  ]
  node [
    id 1852
    label "Georgia"
  ]
  node [
    id 1853
    label "Maryland"
  ]
  node [
    id 1854
    label "warstwa"
  ]
  node [
    id 1855
    label "Luizjana"
  ]
  node [
    id 1856
    label "Massachusetts"
  ]
  node [
    id 1857
    label "Michigan"
  ]
  node [
    id 1858
    label "by&#263;"
  ]
  node [
    id 1859
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1860
    label "samopoczucie"
  ]
  node [
    id 1861
    label "Floryda"
  ]
  node [
    id 1862
    label "Ohio"
  ]
  node [
    id 1863
    label "Alaska"
  ]
  node [
    id 1864
    label "Nowy_Meksyk"
  ]
  node [
    id 1865
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1866
    label "wci&#281;cie"
  ]
  node [
    id 1867
    label "Kansas"
  ]
  node [
    id 1868
    label "Alabama"
  ]
  node [
    id 1869
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1870
    label "Kalifornia"
  ]
  node [
    id 1871
    label "Wirginia"
  ]
  node [
    id 1872
    label "Nowy_York"
  ]
  node [
    id 1873
    label "Waszyngton"
  ]
  node [
    id 1874
    label "Pensylwania"
  ]
  node [
    id 1875
    label "wektor"
  ]
  node [
    id 1876
    label "Hawaje"
  ]
  node [
    id 1877
    label "state"
  ]
  node [
    id 1878
    label "jednostka_administracyjna"
  ]
  node [
    id 1879
    label "Illinois"
  ]
  node [
    id 1880
    label "Oklahoma"
  ]
  node [
    id 1881
    label "Jukatan"
  ]
  node [
    id 1882
    label "Arizona"
  ]
  node [
    id 1883
    label "Oregon"
  ]
  node [
    id 1884
    label "Goa"
  ]
  node [
    id 1885
    label "defect"
  ]
  node [
    id 1886
    label "prywatywny"
  ]
  node [
    id 1887
    label "wyr&#243;b"
  ]
  node [
    id 1888
    label "odej&#347;cie"
  ]
  node [
    id 1889
    label "odchodzi&#263;"
  ]
  node [
    id 1890
    label "odchodzenie"
  ]
  node [
    id 1891
    label "odej&#347;&#263;"
  ]
  node [
    id 1892
    label "nieistnienie"
  ]
  node [
    id 1893
    label "gap"
  ]
  node [
    id 1894
    label "wada"
  ]
  node [
    id 1895
    label "kr&#243;tki"
  ]
  node [
    id 1896
    label "m&#322;ot"
  ]
  node [
    id 1897
    label "marka"
  ]
  node [
    id 1898
    label "pr&#243;ba"
  ]
  node [
    id 1899
    label "attribute"
  ]
  node [
    id 1900
    label "drzewo"
  ]
  node [
    id 1901
    label "znak"
  ]
  node [
    id 1902
    label "zaw&#243;d"
  ]
  node [
    id 1903
    label "zmiana"
  ]
  node [
    id 1904
    label "pracowa&#263;"
  ]
  node [
    id 1905
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1906
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1907
    label "czynnik_produkcji"
  ]
  node [
    id 1908
    label "stosunek_pracy"
  ]
  node [
    id 1909
    label "najem"
  ]
  node [
    id 1910
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1911
    label "zak&#322;ad"
  ]
  node [
    id 1912
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1913
    label "tynkarski"
  ]
  node [
    id 1914
    label "tyrka"
  ]
  node [
    id 1915
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1916
    label "benedykty&#324;ski"
  ]
  node [
    id 1917
    label "poda&#380;_pracy"
  ]
  node [
    id 1918
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1919
    label "bezproblemowy"
  ]
  node [
    id 1920
    label "miejsce_pracy"
  ]
  node [
    id 1921
    label "wyko&#324;czenie"
  ]
  node [
    id 1922
    label "instytut"
  ]
  node [
    id 1923
    label "jednostka_organizacyjna"
  ]
  node [
    id 1924
    label "zak&#322;adka"
  ]
  node [
    id 1925
    label "firma"
  ]
  node [
    id 1926
    label "company"
  ]
  node [
    id 1927
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1928
    label "&#321;ubianka"
  ]
  node [
    id 1929
    label "Bia&#322;y_Dom"
  ]
  node [
    id 1930
    label "dzia&#322;_personalny"
  ]
  node [
    id 1931
    label "sadowisko"
  ]
  node [
    id 1932
    label "odmienianie"
  ]
  node [
    id 1933
    label "zmianka"
  ]
  node [
    id 1934
    label "amendment"
  ]
  node [
    id 1935
    label "passage"
  ]
  node [
    id 1936
    label "rewizja"
  ]
  node [
    id 1937
    label "komplet"
  ]
  node [
    id 1938
    label "tura"
  ]
  node [
    id 1939
    label "change"
  ]
  node [
    id 1940
    label "ferment"
  ]
  node [
    id 1941
    label "anatomopatolog"
  ]
  node [
    id 1942
    label "wytrwa&#322;y"
  ]
  node [
    id 1943
    label "cierpliwy"
  ]
  node [
    id 1944
    label "benedykty&#324;sko"
  ]
  node [
    id 1945
    label "mozolny"
  ]
  node [
    id 1946
    label "po_benedykty&#324;sku"
  ]
  node [
    id 1947
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 1948
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1949
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1950
    label "endeavor"
  ]
  node [
    id 1951
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 1952
    label "do"
  ]
  node [
    id 1953
    label "bangla&#263;"
  ]
  node [
    id 1954
    label "podejmowa&#263;"
  ]
  node [
    id 1955
    label "craft"
  ]
  node [
    id 1956
    label "emocja"
  ]
  node [
    id 1957
    label "zawodoznawstwo"
  ]
  node [
    id 1958
    label "office"
  ]
  node [
    id 1959
    label "kwalifikacje"
  ]
  node [
    id 1960
    label "transakcja"
  ]
  node [
    id 1961
    label "lead"
  ]
  node [
    id 1962
    label "w&#322;adza"
  ]
  node [
    id 1963
    label "posp&#243;lnie"
  ]
  node [
    id 1964
    label "generalny"
  ]
  node [
    id 1965
    label "nadrz&#281;dnie"
  ]
  node [
    id 1966
    label "og&#243;lny"
  ]
  node [
    id 1967
    label "zbiorowo"
  ]
  node [
    id 1968
    label "&#322;&#261;cznie"
  ]
  node [
    id 1969
    label "kompletny"
  ]
  node [
    id 1970
    label "nadrz&#281;dny"
  ]
  node [
    id 1971
    label "og&#243;&#322;owy"
  ]
  node [
    id 1972
    label "&#322;&#261;czny"
  ]
  node [
    id 1973
    label "ca&#322;y"
  ]
  node [
    id 1974
    label "powszechnie"
  ]
  node [
    id 1975
    label "zbiorowy"
  ]
  node [
    id 1976
    label "zwierzchni"
  ]
  node [
    id 1977
    label "zasadniczy"
  ]
  node [
    id 1978
    label "generalnie"
  ]
  node [
    id 1979
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 1980
    label "zbiorczo"
  ]
  node [
    id 1981
    label "wsp&#243;lnie"
  ]
  node [
    id 1982
    label "licznie"
  ]
  node [
    id 1983
    label "istotnie"
  ]
  node [
    id 1984
    label "posp&#243;lny"
  ]
  node [
    id 1985
    label "visualize"
  ]
  node [
    id 1986
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1987
    label "zorganizowa&#263;"
  ]
  node [
    id 1988
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1989
    label "wydali&#263;"
  ]
  node [
    id 1990
    label "make"
  ]
  node [
    id 1991
    label "wystylizowa&#263;"
  ]
  node [
    id 1992
    label "appoint"
  ]
  node [
    id 1993
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1994
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1995
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1996
    label "post&#261;pi&#263;"
  ]
  node [
    id 1997
    label "przerobi&#263;"
  ]
  node [
    id 1998
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1999
    label "cause"
  ]
  node [
    id 2000
    label "nabra&#263;"
  ]
  node [
    id 2001
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 2002
    label "zobo"
  ]
  node [
    id 2003
    label "dzo"
  ]
  node [
    id 2004
    label "yakalo"
  ]
  node [
    id 2005
    label "kr&#281;torogie"
  ]
  node [
    id 2006
    label "livestock"
  ]
  node [
    id 2007
    label "posp&#243;lstwo"
  ]
  node [
    id 2008
    label "kraal"
  ]
  node [
    id 2009
    label "czochrad&#322;o"
  ]
  node [
    id 2010
    label "prze&#380;uwacz"
  ]
  node [
    id 2011
    label "zebu"
  ]
  node [
    id 2012
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 2013
    label "bizon"
  ]
  node [
    id 2014
    label "byd&#322;o_domowe"
  ]
  node [
    id 2015
    label "patrze&#263;"
  ]
  node [
    id 2016
    label "look"
  ]
  node [
    id 2017
    label "czeka&#263;"
  ]
  node [
    id 2018
    label "lookout"
  ]
  node [
    id 2019
    label "wyziera&#263;"
  ]
  node [
    id 2020
    label "peep"
  ]
  node [
    id 2021
    label "koso"
  ]
  node [
    id 2022
    label "uwa&#380;a&#263;"
  ]
  node [
    id 2023
    label "go_steady"
  ]
  node [
    id 2024
    label "szuka&#263;"
  ]
  node [
    id 2025
    label "dba&#263;"
  ]
  node [
    id 2026
    label "pogl&#261;da&#263;"
  ]
  node [
    id 2027
    label "anticipate"
  ]
  node [
    id 2028
    label "pauzowa&#263;"
  ]
  node [
    id 2029
    label "oczekiwa&#263;"
  ]
  node [
    id 2030
    label "sp&#281;dza&#263;"
  ]
  node [
    id 2031
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 2032
    label "stand"
  ]
  node [
    id 2033
    label "trwa&#263;"
  ]
  node [
    id 2034
    label "equal"
  ]
  node [
    id 2035
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 2036
    label "chodzi&#263;"
  ]
  node [
    id 2037
    label "uczestniczy&#263;"
  ]
  node [
    id 2038
    label "obecno&#347;&#263;"
  ]
  node [
    id 2039
    label "si&#281;ga&#263;"
  ]
  node [
    id 2040
    label "stylizacja"
  ]
  node [
    id 2041
    label "kopiowa&#263;"
  ]
  node [
    id 2042
    label "pr&#243;bka"
  ]
  node [
    id 2043
    label "wch&#322;ania&#263;"
  ]
  node [
    id 2044
    label "wycina&#263;"
  ]
  node [
    id 2045
    label "open"
  ]
  node [
    id 2046
    label "arise"
  ]
  node [
    id 2047
    label "&#322;apa&#263;"
  ]
  node [
    id 2048
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 2049
    label "uprawia&#263;_seks"
  ]
  node [
    id 2050
    label "levy"
  ]
  node [
    id 2051
    label "grza&#263;"
  ]
  node [
    id 2052
    label "ucieka&#263;"
  ]
  node [
    id 2053
    label "pokonywa&#263;"
  ]
  node [
    id 2054
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 2055
    label "chwyta&#263;"
  ]
  node [
    id 2056
    label "&#263;pa&#263;"
  ]
  node [
    id 2057
    label "rusza&#263;"
  ]
  node [
    id 2058
    label "abstract"
  ]
  node [
    id 2059
    label "interpretowa&#263;"
  ]
  node [
    id 2060
    label "rucha&#263;"
  ]
  node [
    id 2061
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 2062
    label "towarzystwo"
  ]
  node [
    id 2063
    label "wk&#322;ada&#263;"
  ]
  node [
    id 2064
    label "przyjmowa&#263;"
  ]
  node [
    id 2065
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 2066
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 2067
    label "za&#380;ywa&#263;"
  ]
  node [
    id 2068
    label "zalicza&#263;"
  ]
  node [
    id 2069
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 2070
    label "wygrywa&#263;"
  ]
  node [
    id 2071
    label "przewa&#380;a&#263;"
  ]
  node [
    id 2072
    label "branie"
  ]
  node [
    id 2073
    label "poczytywa&#263;"
  ]
  node [
    id 2074
    label "wchodzi&#263;"
  ]
  node [
    id 2075
    label "porywa&#263;"
  ]
  node [
    id 2076
    label "wzi&#261;&#263;"
  ]
  node [
    id 2077
    label "poch&#322;ania&#263;"
  ]
  node [
    id 2078
    label "swallow"
  ]
  node [
    id 2079
    label "przyswaja&#263;"
  ]
  node [
    id 2080
    label "wykupywa&#263;"
  ]
  node [
    id 2081
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 2082
    label "wytraca&#263;"
  ]
  node [
    id 2083
    label "oddziela&#263;"
  ]
  node [
    id 2084
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 2085
    label "mordowa&#263;"
  ]
  node [
    id 2086
    label "fall"
  ]
  node [
    id 2087
    label "usuwa&#263;"
  ]
  node [
    id 2088
    label "hack"
  ]
  node [
    id 2089
    label "pocina&#263;"
  ]
  node [
    id 2090
    label "write_out"
  ]
  node [
    id 2091
    label "wy&#380;&#322;abia&#263;"
  ]
  node [
    id 2092
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 2093
    label "uderza&#263;"
  ]
  node [
    id 2094
    label "mock"
  ]
  node [
    id 2095
    label "pirat"
  ]
  node [
    id 2096
    label "transcribe"
  ]
  node [
    id 2097
    label "pobra&#263;"
  ]
  node [
    id 2098
    label "pobieranie"
  ]
  node [
    id 2099
    label "towar"
  ]
  node [
    id 2100
    label "pobranie"
  ]
  node [
    id 2101
    label "wielko&#347;&#263;"
  ]
  node [
    id 2102
    label "altitude"
  ]
  node [
    id 2103
    label "brzmienie"
  ]
  node [
    id 2104
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 2105
    label "degree"
  ]
  node [
    id 2106
    label "tallness"
  ]
  node [
    id 2107
    label "k&#261;t"
  ]
  node [
    id 2108
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 2109
    label "ton"
  ]
  node [
    id 2110
    label "ambitus"
  ]
  node [
    id 2111
    label "skala"
  ]
  node [
    id 2112
    label "part"
  ]
  node [
    id 2113
    label "teren"
  ]
  node [
    id 2114
    label "coupon"
  ]
  node [
    id 2115
    label "epizod"
  ]
  node [
    id 2116
    label "pokwitowanie"
  ]
  node [
    id 2117
    label "dymensja"
  ]
  node [
    id 2118
    label "odzie&#380;"
  ]
  node [
    id 2119
    label "wyra&#380;anie"
  ]
  node [
    id 2120
    label "tone"
  ]
  node [
    id 2121
    label "rejestr"
  ]
  node [
    id 2122
    label "kolorystyka"
  ]
  node [
    id 2123
    label "spirit"
  ]
  node [
    id 2124
    label "sound"
  ]
  node [
    id 2125
    label "ubocze"
  ]
  node [
    id 2126
    label "garderoba"
  ]
  node [
    id 2127
    label "rami&#281;_k&#261;ta"
  ]
  node [
    id 2128
    label "dom"
  ]
  node [
    id 2129
    label "zaleta"
  ]
  node [
    id 2130
    label "property"
  ]
  node [
    id 2131
    label "measure"
  ]
  node [
    id 2132
    label "opinia"
  ]
  node [
    id 2133
    label "potencja"
  ]
  node [
    id 2134
    label "rzadko&#347;&#263;"
  ]
  node [
    id 2135
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 2136
    label "Uzbekistan"
  ]
  node [
    id 2137
    label "catfish"
  ]
  node [
    id 2138
    label "sumowate"
  ]
  node [
    id 2139
    label "fala_elektromagnetyczna"
  ]
  node [
    id 2140
    label "d&#322;ugo&#347;&#263;"
  ]
  node [
    id 2141
    label "radiokomunikacja"
  ]
  node [
    id 2142
    label "cyclicity"
  ]
  node [
    id 2143
    label "nast&#281;pnie"
  ]
  node [
    id 2144
    label "poni&#380;szy"
  ]
  node [
    id 2145
    label "kolejny"
  ]
  node [
    id 2146
    label "ten"
  ]
  node [
    id 2147
    label "pragn&#261;&#263;"
  ]
  node [
    id 2148
    label "need"
  ]
  node [
    id 2149
    label "t&#281;skni&#263;"
  ]
  node [
    id 2150
    label "&#322;akn&#261;&#263;"
  ]
  node [
    id 2151
    label "desire"
  ]
  node [
    id 2152
    label "chcie&#263;"
  ]
  node [
    id 2153
    label "czu&#263;"
  ]
  node [
    id 2154
    label "think"
  ]
  node [
    id 2155
    label "zachowywa&#263;"
  ]
  node [
    id 2156
    label "pilnowa&#263;"
  ]
  node [
    id 2157
    label "chowa&#263;"
  ]
  node [
    id 2158
    label "zna&#263;"
  ]
  node [
    id 2159
    label "recall"
  ]
  node [
    id 2160
    label "echo"
  ]
  node [
    id 2161
    label "ukrywa&#263;"
  ]
  node [
    id 2162
    label "hodowa&#263;"
  ]
  node [
    id 2163
    label "continue"
  ]
  node [
    id 2164
    label "hide"
  ]
  node [
    id 2165
    label "meliniarz"
  ]
  node [
    id 2166
    label "umieszcza&#263;"
  ]
  node [
    id 2167
    label "przetrzymywa&#263;"
  ]
  node [
    id 2168
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 2169
    label "znosi&#263;"
  ]
  node [
    id 2170
    label "przechowywa&#263;"
  ]
  node [
    id 2171
    label "behave"
  ]
  node [
    id 2172
    label "podtrzymywa&#263;"
  ]
  node [
    id 2173
    label "control"
  ]
  node [
    id 2174
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 2175
    label "compass"
  ]
  node [
    id 2176
    label "exsert"
  ]
  node [
    id 2177
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 2178
    label "appreciation"
  ]
  node [
    id 2179
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 2180
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 2181
    label "cognizance"
  ]
  node [
    id 2182
    label "resonance"
  ]
  node [
    id 2183
    label "sprawdza&#263;"
  ]
  node [
    id 2184
    label "ankieter"
  ]
  node [
    id 2185
    label "inspect"
  ]
  node [
    id 2186
    label "przes&#322;uchiwa&#263;"
  ]
  node [
    id 2187
    label "question"
  ]
  node [
    id 2188
    label "szpiegowa&#263;"
  ]
  node [
    id 2189
    label "examine"
  ]
  node [
    id 2190
    label "przepytywa&#263;"
  ]
  node [
    id 2191
    label "wypytywa&#263;"
  ]
  node [
    id 2192
    label "s&#322;ucha&#263;"
  ]
  node [
    id 2193
    label "interrogate"
  ]
  node [
    id 2194
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 2195
    label "thank"
  ]
  node [
    id 2196
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 2197
    label "sk&#322;ada&#263;"
  ]
  node [
    id 2198
    label "etykieta"
  ]
  node [
    id 2199
    label "odmawia&#263;"
  ]
  node [
    id 2200
    label "zbiera&#263;"
  ]
  node [
    id 2201
    label "opracowywa&#263;"
  ]
  node [
    id 2202
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 2203
    label "oddawa&#263;"
  ]
  node [
    id 2204
    label "uk&#322;ada&#263;"
  ]
  node [
    id 2205
    label "publicize"
  ]
  node [
    id 2206
    label "dzieli&#263;"
  ]
  node [
    id 2207
    label "przekazywa&#263;"
  ]
  node [
    id 2208
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 2209
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 2210
    label "zestaw"
  ]
  node [
    id 2211
    label "przywraca&#263;"
  ]
  node [
    id 2212
    label "render"
  ]
  node [
    id 2213
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 2214
    label "wypowiada&#263;"
  ]
  node [
    id 2215
    label "odpowiada&#263;"
  ]
  node [
    id 2216
    label "frame"
  ]
  node [
    id 2217
    label "contest"
  ]
  node [
    id 2218
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 2219
    label "odrzuca&#263;"
  ]
  node [
    id 2220
    label "naklejka"
  ]
  node [
    id 2221
    label "tab"
  ]
  node [
    id 2222
    label "tabliczka"
  ]
  node [
    id 2223
    label "Jaros&#322;awa"
  ]
  node [
    id 2224
    label "Kalinowski"
  ]
  node [
    id 2225
    label "Lidia"
  ]
  node [
    id 2226
    label "Staro&#324;"
  ]
  node [
    id 2227
    label "Agnieszka"
  ]
  node [
    id 2228
    label "ch&#322;on&#261;&#263;"
  ]
  node [
    id 2229
    label "Domi&#324;czak"
  ]
  node [
    id 2230
    label "ministerstwo"
  ]
  node [
    id 2231
    label "i"
  ]
  node [
    id 2232
    label "polityka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 884
  ]
  edge [
    source 14
    target 885
  ]
  edge [
    source 14
    target 886
  ]
  edge [
    source 14
    target 887
  ]
  edge [
    source 14
    target 888
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 45
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 329
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 285
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 333
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 315
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 250
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 339
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 983
  ]
  edge [
    source 16
    target 984
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 985
  ]
  edge [
    source 16
    target 986
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 988
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 990
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 991
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 993
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 995
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 16
    target 997
  ]
  edge [
    source 16
    target 998
  ]
  edge [
    source 16
    target 999
  ]
  edge [
    source 16
    target 1000
  ]
  edge [
    source 16
    target 1001
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 1007
  ]
  edge [
    source 16
    target 1008
  ]
  edge [
    source 16
    target 1009
  ]
  edge [
    source 16
    target 1010
  ]
  edge [
    source 16
    target 41
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 448
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 416
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 74
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 551
  ]
  edge [
    source 17
    target 425
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 297
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 442
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 294
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 585
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 522
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 84
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 528
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 31
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 522
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 518
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 560
  ]
  edge [
    source 19
    target 501
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 434
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 650
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 609
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 423
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 84
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 365
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 19
    target 1130
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 743
  ]
  edge [
    source 19
    target 1131
  ]
  edge [
    source 19
    target 1132
  ]
  edge [
    source 19
    target 427
  ]
  edge [
    source 19
    target 1133
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 460
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 416
  ]
  edge [
    source 19
    target 415
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 19
    target 1144
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 1146
  ]
  edge [
    source 19
    target 1147
  ]
  edge [
    source 19
    target 1148
  ]
  edge [
    source 19
    target 1149
  ]
  edge [
    source 19
    target 1150
  ]
  edge [
    source 19
    target 734
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 1151
  ]
  edge [
    source 19
    target 1152
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 78
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 526
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 529
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 468
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 632
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 500
  ]
  edge [
    source 19
    target 57
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 302
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 328
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 686
  ]
  edge [
    source 19
    target 471
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 716
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 717
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 719
  ]
  edge [
    source 19
    target 720
  ]
  edge [
    source 19
    target 721
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 725
  ]
  edge [
    source 19
    target 726
  ]
  edge [
    source 19
    target 727
  ]
  edge [
    source 19
    target 728
  ]
  edge [
    source 19
    target 729
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 553
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 279
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 536
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 564
  ]
  edge [
    source 19
    target 565
  ]
  edge [
    source 19
    target 566
  ]
  edge [
    source 19
    target 567
  ]
  edge [
    source 19
    target 568
  ]
  edge [
    source 19
    target 569
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 19
    target 572
  ]
  edge [
    source 19
    target 573
  ]
  edge [
    source 19
    target 574
  ]
  edge [
    source 19
    target 575
  ]
  edge [
    source 19
    target 576
  ]
  edge [
    source 19
    target 577
  ]
  edge [
    source 19
    target 578
  ]
  edge [
    source 19
    target 579
  ]
  edge [
    source 19
    target 580
  ]
  edge [
    source 19
    target 581
  ]
  edge [
    source 19
    target 582
  ]
  edge [
    source 19
    target 583
  ]
  edge [
    source 19
    target 584
  ]
  edge [
    source 19
    target 585
  ]
  edge [
    source 19
    target 586
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 448
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 79
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 660
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 754
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1258
  ]
  edge [
    source 19
    target 1259
  ]
  edge [
    source 19
    target 62
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1261
  ]
  edge [
    source 19
    target 1262
  ]
  edge [
    source 19
    target 1263
  ]
  edge [
    source 19
    target 465
  ]
  edge [
    source 19
    target 1264
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 1265
  ]
  edge [
    source 19
    target 1266
  ]
  edge [
    source 19
    target 1267
  ]
  edge [
    source 19
    target 1268
  ]
  edge [
    source 19
    target 1269
  ]
  edge [
    source 19
    target 1270
  ]
  edge [
    source 19
    target 1271
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 1274
  ]
  edge [
    source 19
    target 1275
  ]
  edge [
    source 19
    target 476
  ]
  edge [
    source 19
    target 1276
  ]
  edge [
    source 19
    target 1277
  ]
  edge [
    source 19
    target 59
  ]
  edge [
    source 19
    target 1278
  ]
  edge [
    source 19
    target 1279
  ]
  edge [
    source 19
    target 1280
  ]
  edge [
    source 19
    target 1281
  ]
  edge [
    source 19
    target 1282
  ]
  edge [
    source 19
    target 1283
  ]
  edge [
    source 19
    target 1284
  ]
  edge [
    source 19
    target 1285
  ]
  edge [
    source 19
    target 1286
  ]
  edge [
    source 19
    target 1287
  ]
  edge [
    source 19
    target 615
  ]
  edge [
    source 19
    target 1288
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1084
  ]
  edge [
    source 20
    target 1289
  ]
  edge [
    source 20
    target 1290
  ]
  edge [
    source 20
    target 1291
  ]
  edge [
    source 20
    target 1292
  ]
  edge [
    source 20
    target 1293
  ]
  edge [
    source 20
    target 1294
  ]
  edge [
    source 20
    target 1295
  ]
  edge [
    source 20
    target 1296
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 1297
  ]
  edge [
    source 20
    target 1298
  ]
  edge [
    source 20
    target 1299
  ]
  edge [
    source 20
    target 1300
  ]
  edge [
    source 20
    target 1301
  ]
  edge [
    source 20
    target 1302
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 1303
  ]
  edge [
    source 20
    target 593
  ]
  edge [
    source 20
    target 1304
  ]
  edge [
    source 20
    target 1305
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 1198
  ]
  edge [
    source 20
    target 1306
  ]
  edge [
    source 20
    target 1307
  ]
  edge [
    source 20
    target 833
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1220
  ]
  edge [
    source 20
    target 1221
  ]
  edge [
    source 20
    target 448
  ]
  edge [
    source 20
    target 1222
  ]
  edge [
    source 20
    target 1223
  ]
  edge [
    source 20
    target 1224
  ]
  edge [
    source 20
    target 1225
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 20
    target 1226
  ]
  edge [
    source 20
    target 1227
  ]
  edge [
    source 20
    target 1228
  ]
  edge [
    source 20
    target 1082
  ]
  edge [
    source 20
    target 1229
  ]
  edge [
    source 20
    target 1230
  ]
  edge [
    source 20
    target 1231
  ]
  edge [
    source 20
    target 1232
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 1233
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 585
  ]
  edge [
    source 20
    target 1234
  ]
  edge [
    source 20
    target 660
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 1308
  ]
  edge [
    source 20
    target 1309
  ]
  edge [
    source 20
    target 1310
  ]
  edge [
    source 20
    target 1311
  ]
  edge [
    source 20
    target 1312
  ]
  edge [
    source 20
    target 1313
  ]
  edge [
    source 20
    target 1314
  ]
  edge [
    source 20
    target 1315
  ]
  edge [
    source 20
    target 1316
  ]
  edge [
    source 20
    target 1317
  ]
  edge [
    source 20
    target 1318
  ]
  edge [
    source 20
    target 1319
  ]
  edge [
    source 20
    target 1320
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 1321
  ]
  edge [
    source 20
    target 1322
  ]
  edge [
    source 20
    target 1323
  ]
  edge [
    source 20
    target 356
  ]
  edge [
    source 20
    target 355
  ]
  edge [
    source 20
    target 1324
  ]
  edge [
    source 20
    target 1325
  ]
  edge [
    source 20
    target 1326
  ]
  edge [
    source 20
    target 1327
  ]
  edge [
    source 20
    target 1328
  ]
  edge [
    source 20
    target 1329
  ]
  edge [
    source 20
    target 1330
  ]
  edge [
    source 20
    target 93
  ]
  edge [
    source 20
    target 1331
  ]
  edge [
    source 20
    target 1332
  ]
  edge [
    source 20
    target 1333
  ]
  edge [
    source 20
    target 84
  ]
  edge [
    source 20
    target 1334
  ]
  edge [
    source 20
    target 1335
  ]
  edge [
    source 20
    target 1336
  ]
  edge [
    source 20
    target 1089
  ]
  edge [
    source 20
    target 1337
  ]
  edge [
    source 20
    target 1338
  ]
  edge [
    source 20
    target 1339
  ]
  edge [
    source 20
    target 570
  ]
  edge [
    source 20
    target 1340
  ]
  edge [
    source 20
    target 1047
  ]
  edge [
    source 20
    target 1341
  ]
  edge [
    source 20
    target 999
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 49
  ]
  edge [
    source 21
    target 1342
  ]
  edge [
    source 21
    target 1343
  ]
  edge [
    source 21
    target 1344
  ]
  edge [
    source 21
    target 1345
  ]
  edge [
    source 21
    target 1346
  ]
  edge [
    source 21
    target 1347
  ]
  edge [
    source 21
    target 1348
  ]
  edge [
    source 21
    target 2230
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 2231
  ]
  edge [
    source 21
    target 2232
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1349
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 1350
  ]
  edge [
    source 22
    target 1351
  ]
  edge [
    source 22
    target 355
  ]
  edge [
    source 22
    target 1352
  ]
  edge [
    source 22
    target 250
  ]
  edge [
    source 22
    target 1353
  ]
  edge [
    source 22
    target 1354
  ]
  edge [
    source 22
    target 1355
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 22
    target 1356
  ]
  edge [
    source 22
    target 1357
  ]
  edge [
    source 22
    target 1358
  ]
  edge [
    source 22
    target 1359
  ]
  edge [
    source 22
    target 1360
  ]
  edge [
    source 22
    target 1361
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 1362
  ]
  edge [
    source 22
    target 1363
  ]
  edge [
    source 22
    target 1364
  ]
  edge [
    source 22
    target 1365
  ]
  edge [
    source 22
    target 1366
  ]
  edge [
    source 22
    target 1367
  ]
  edge [
    source 22
    target 1368
  ]
  edge [
    source 22
    target 1369
  ]
  edge [
    source 22
    target 1370
  ]
  edge [
    source 22
    target 1371
  ]
  edge [
    source 22
    target 1372
  ]
  edge [
    source 22
    target 1373
  ]
  edge [
    source 22
    target 327
  ]
  edge [
    source 22
    target 690
  ]
  edge [
    source 22
    target 1374
  ]
  edge [
    source 22
    target 1375
  ]
  edge [
    source 22
    target 1376
  ]
  edge [
    source 22
    target 1377
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 1378
  ]
  edge [
    source 22
    target 1379
  ]
  edge [
    source 22
    target 1380
  ]
  edge [
    source 22
    target 1381
  ]
  edge [
    source 22
    target 1382
  ]
  edge [
    source 22
    target 1383
  ]
  edge [
    source 22
    target 1384
  ]
  edge [
    source 22
    target 921
  ]
  edge [
    source 22
    target 1385
  ]
  edge [
    source 22
    target 1386
  ]
  edge [
    source 22
    target 312
  ]
  edge [
    source 22
    target 927
  ]
  edge [
    source 22
    target 762
  ]
  edge [
    source 22
    target 1387
  ]
  edge [
    source 22
    target 1388
  ]
  edge [
    source 22
    target 1389
  ]
  edge [
    source 22
    target 1390
  ]
  edge [
    source 22
    target 340
  ]
  edge [
    source 22
    target 1391
  ]
  edge [
    source 22
    target 1392
  ]
  edge [
    source 22
    target 1393
  ]
  edge [
    source 22
    target 1394
  ]
  edge [
    source 22
    target 1395
  ]
  edge [
    source 22
    target 1396
  ]
  edge [
    source 22
    target 1397
  ]
  edge [
    source 22
    target 1398
  ]
  edge [
    source 22
    target 1399
  ]
  edge [
    source 22
    target 1400
  ]
  edge [
    source 22
    target 1401
  ]
  edge [
    source 22
    target 1402
  ]
  edge [
    source 22
    target 1403
  ]
  edge [
    source 22
    target 1404
  ]
  edge [
    source 22
    target 1405
  ]
  edge [
    source 22
    target 1406
  ]
  edge [
    source 22
    target 922
  ]
  edge [
    source 22
    target 1407
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 22
    target 1408
  ]
  edge [
    source 22
    target 1409
  ]
  edge [
    source 22
    target 1410
  ]
  edge [
    source 22
    target 1411
  ]
  edge [
    source 22
    target 1412
  ]
  edge [
    source 22
    target 1413
  ]
  edge [
    source 22
    target 1414
  ]
  edge [
    source 22
    target 1415
  ]
  edge [
    source 22
    target 1416
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 1417
  ]
  edge [
    source 22
    target 1418
  ]
  edge [
    source 22
    target 1419
  ]
  edge [
    source 22
    target 1420
  ]
  edge [
    source 22
    target 1421
  ]
  edge [
    source 22
    target 1422
  ]
  edge [
    source 22
    target 1423
  ]
  edge [
    source 22
    target 1424
  ]
  edge [
    source 22
    target 1425
  ]
  edge [
    source 22
    target 1426
  ]
  edge [
    source 22
    target 1427
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 1428
  ]
  edge [
    source 22
    target 1429
  ]
  edge [
    source 22
    target 1430
  ]
  edge [
    source 22
    target 1431
  ]
  edge [
    source 22
    target 78
  ]
  edge [
    source 22
    target 1432
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 1433
  ]
  edge [
    source 23
    target 1434
  ]
  edge [
    source 23
    target 1435
  ]
  edge [
    source 23
    target 1436
  ]
  edge [
    source 23
    target 1437
  ]
  edge [
    source 23
    target 1438
  ]
  edge [
    source 23
    target 1439
  ]
  edge [
    source 23
    target 1440
  ]
  edge [
    source 23
    target 1441
  ]
  edge [
    source 23
    target 1442
  ]
  edge [
    source 23
    target 1443
  ]
  edge [
    source 23
    target 1444
  ]
  edge [
    source 23
    target 1445
  ]
  edge [
    source 23
    target 1446
  ]
  edge [
    source 23
    target 1447
  ]
  edge [
    source 23
    target 1448
  ]
  edge [
    source 23
    target 1449
  ]
  edge [
    source 23
    target 1450
  ]
  edge [
    source 23
    target 1451
  ]
  edge [
    source 23
    target 1452
  ]
  edge [
    source 23
    target 1453
  ]
  edge [
    source 23
    target 1454
  ]
  edge [
    source 23
    target 1155
  ]
  edge [
    source 23
    target 1455
  ]
  edge [
    source 23
    target 1456
  ]
  edge [
    source 23
    target 1457
  ]
  edge [
    source 23
    target 1458
  ]
  edge [
    source 23
    target 1459
  ]
  edge [
    source 23
    target 1460
  ]
  edge [
    source 23
    target 1461
  ]
  edge [
    source 23
    target 1462
  ]
  edge [
    source 23
    target 1463
  ]
  edge [
    source 23
    target 1464
  ]
  edge [
    source 23
    target 1465
  ]
  edge [
    source 23
    target 1466
  ]
  edge [
    source 23
    target 1467
  ]
  edge [
    source 23
    target 1468
  ]
  edge [
    source 23
    target 1469
  ]
  edge [
    source 23
    target 1470
  ]
  edge [
    source 23
    target 1471
  ]
  edge [
    source 23
    target 1472
  ]
  edge [
    source 23
    target 1473
  ]
  edge [
    source 23
    target 1474
  ]
  edge [
    source 23
    target 1475
  ]
  edge [
    source 23
    target 1476
  ]
  edge [
    source 23
    target 1477
  ]
  edge [
    source 23
    target 1478
  ]
  edge [
    source 23
    target 1479
  ]
  edge [
    source 23
    target 1480
  ]
  edge [
    source 23
    target 1481
  ]
  edge [
    source 23
    target 1482
  ]
  edge [
    source 23
    target 1483
  ]
  edge [
    source 23
    target 1484
  ]
  edge [
    source 23
    target 1485
  ]
  edge [
    source 23
    target 1486
  ]
  edge [
    source 23
    target 355
  ]
  edge [
    source 23
    target 1487
  ]
  edge [
    source 23
    target 1488
  ]
  edge [
    source 23
    target 1489
  ]
  edge [
    source 23
    target 1490
  ]
  edge [
    source 23
    target 1491
  ]
  edge [
    source 23
    target 1492
  ]
  edge [
    source 23
    target 1493
  ]
  edge [
    source 23
    target 1494
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 1495
  ]
  edge [
    source 23
    target 1496
  ]
  edge [
    source 23
    target 1497
  ]
  edge [
    source 23
    target 1498
  ]
  edge [
    source 23
    target 1499
  ]
  edge [
    source 23
    target 1500
  ]
  edge [
    source 23
    target 1501
  ]
  edge [
    source 23
    target 1502
  ]
  edge [
    source 23
    target 1503
  ]
  edge [
    source 23
    target 1504
  ]
  edge [
    source 23
    target 1505
  ]
  edge [
    source 23
    target 1506
  ]
  edge [
    source 23
    target 1507
  ]
  edge [
    source 23
    target 1508
  ]
  edge [
    source 23
    target 1509
  ]
  edge [
    source 23
    target 1510
  ]
  edge [
    source 23
    target 1511
  ]
  edge [
    source 23
    target 1512
  ]
  edge [
    source 23
    target 1513
  ]
  edge [
    source 23
    target 1514
  ]
  edge [
    source 23
    target 1515
  ]
  edge [
    source 23
    target 1516
  ]
  edge [
    source 23
    target 1517
  ]
  edge [
    source 23
    target 1518
  ]
  edge [
    source 23
    target 1519
  ]
  edge [
    source 23
    target 1520
  ]
  edge [
    source 23
    target 1521
  ]
  edge [
    source 23
    target 1522
  ]
  edge [
    source 23
    target 1523
  ]
  edge [
    source 23
    target 1524
  ]
  edge [
    source 23
    target 1525
  ]
  edge [
    source 23
    target 1526
  ]
  edge [
    source 23
    target 1527
  ]
  edge [
    source 23
    target 1528
  ]
  edge [
    source 23
    target 1529
  ]
  edge [
    source 23
    target 1530
  ]
  edge [
    source 23
    target 1531
  ]
  edge [
    source 23
    target 1532
  ]
  edge [
    source 23
    target 1533
  ]
  edge [
    source 23
    target 1534
  ]
  edge [
    source 23
    target 1535
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 46
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1536
  ]
  edge [
    source 24
    target 1537
  ]
  edge [
    source 24
    target 1538
  ]
  edge [
    source 24
    target 1539
  ]
  edge [
    source 24
    target 1540
  ]
  edge [
    source 24
    target 1541
  ]
  edge [
    source 24
    target 1542
  ]
  edge [
    source 24
    target 1543
  ]
  edge [
    source 24
    target 1544
  ]
  edge [
    source 24
    target 1545
  ]
  edge [
    source 24
    target 1546
  ]
  edge [
    source 24
    target 1547
  ]
  edge [
    source 24
    target 1548
  ]
  edge [
    source 24
    target 1549
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1550
  ]
  edge [
    source 25
    target 1551
  ]
  edge [
    source 25
    target 1552
  ]
  edge [
    source 25
    target 57
  ]
  edge [
    source 25
    target 1553
  ]
  edge [
    source 25
    target 1554
  ]
  edge [
    source 25
    target 1555
  ]
  edge [
    source 25
    target 1556
  ]
  edge [
    source 25
    target 129
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 1557
  ]
  edge [
    source 25
    target 1558
  ]
  edge [
    source 25
    target 1559
  ]
  edge [
    source 25
    target 68
  ]
  edge [
    source 25
    target 69
  ]
  edge [
    source 25
    target 70
  ]
  edge [
    source 25
    target 71
  ]
  edge [
    source 25
    target 72
  ]
  edge [
    source 25
    target 73
  ]
  edge [
    source 25
    target 74
  ]
  edge [
    source 25
    target 1560
  ]
  edge [
    source 25
    target 1561
  ]
  edge [
    source 25
    target 1562
  ]
  edge [
    source 25
    target 1563
  ]
  edge [
    source 25
    target 1032
  ]
  edge [
    source 25
    target 1564
  ]
  edge [
    source 25
    target 1565
  ]
  edge [
    source 25
    target 1566
  ]
  edge [
    source 25
    target 1567
  ]
  edge [
    source 25
    target 831
  ]
  edge [
    source 25
    target 1198
  ]
  edge [
    source 25
    target 600
  ]
  edge [
    source 25
    target 1568
  ]
  edge [
    source 25
    target 1569
  ]
  edge [
    source 25
    target 1570
  ]
  edge [
    source 25
    target 1571
  ]
  edge [
    source 25
    target 1572
  ]
  edge [
    source 25
    target 1573
  ]
  edge [
    source 25
    target 1574
  ]
  edge [
    source 25
    target 1575
  ]
  edge [
    source 25
    target 1576
  ]
  edge [
    source 25
    target 1577
  ]
  edge [
    source 25
    target 1578
  ]
  edge [
    source 25
    target 1579
  ]
  edge [
    source 25
    target 1580
  ]
  edge [
    source 25
    target 1581
  ]
  edge [
    source 25
    target 1582
  ]
  edge [
    source 25
    target 1583
  ]
  edge [
    source 25
    target 1584
  ]
  edge [
    source 25
    target 1585
  ]
  edge [
    source 25
    target 1586
  ]
  edge [
    source 25
    target 1587
  ]
  edge [
    source 25
    target 1588
  ]
  edge [
    source 25
    target 1589
  ]
  edge [
    source 25
    target 1590
  ]
  edge [
    source 25
    target 1591
  ]
  edge [
    source 25
    target 1592
  ]
  edge [
    source 25
    target 1593
  ]
  edge [
    source 25
    target 1594
  ]
  edge [
    source 25
    target 1595
  ]
  edge [
    source 25
    target 1596
  ]
  edge [
    source 25
    target 1597
  ]
  edge [
    source 25
    target 1598
  ]
  edge [
    source 25
    target 1599
  ]
  edge [
    source 25
    target 1600
  ]
  edge [
    source 25
    target 1601
  ]
  edge [
    source 25
    target 1602
  ]
  edge [
    source 25
    target 1603
  ]
  edge [
    source 25
    target 1604
  ]
  edge [
    source 25
    target 837
  ]
  edge [
    source 25
    target 453
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 1605
  ]
  edge [
    source 26
    target 1606
  ]
  edge [
    source 26
    target 1607
  ]
  edge [
    source 26
    target 1608
  ]
  edge [
    source 26
    target 1609
  ]
  edge [
    source 26
    target 1610
  ]
  edge [
    source 26
    target 1611
  ]
  edge [
    source 26
    target 1612
  ]
  edge [
    source 26
    target 1613
  ]
  edge [
    source 26
    target 1614
  ]
  edge [
    source 26
    target 1615
  ]
  edge [
    source 26
    target 1555
  ]
  edge [
    source 26
    target 425
  ]
  edge [
    source 26
    target 1616
  ]
  edge [
    source 26
    target 814
  ]
  edge [
    source 26
    target 1617
  ]
  edge [
    source 26
    target 1618
  ]
  edge [
    source 26
    target 1619
  ]
  edge [
    source 26
    target 1620
  ]
  edge [
    source 26
    target 1621
  ]
  edge [
    source 26
    target 1622
  ]
  edge [
    source 26
    target 1623
  ]
  edge [
    source 26
    target 1624
  ]
  edge [
    source 26
    target 1625
  ]
  edge [
    source 26
    target 1626
  ]
  edge [
    source 26
    target 1627
  ]
  edge [
    source 26
    target 395
  ]
  edge [
    source 26
    target 1628
  ]
  edge [
    source 26
    target 561
  ]
  edge [
    source 26
    target 1629
  ]
  edge [
    source 26
    target 1630
  ]
  edge [
    source 26
    target 844
  ]
  edge [
    source 26
    target 1631
  ]
  edge [
    source 26
    target 1632
  ]
  edge [
    source 26
    target 1633
  ]
  edge [
    source 26
    target 1634
  ]
  edge [
    source 26
    target 1635
  ]
  edge [
    source 26
    target 1636
  ]
  edge [
    source 26
    target 1637
  ]
  edge [
    source 26
    target 68
  ]
  edge [
    source 26
    target 84
  ]
  edge [
    source 26
    target 754
  ]
  edge [
    source 26
    target 1638
  ]
  edge [
    source 26
    target 1639
  ]
  edge [
    source 26
    target 445
  ]
  edge [
    source 26
    target 446
  ]
  edge [
    source 26
    target 447
  ]
  edge [
    source 26
    target 448
  ]
  edge [
    source 26
    target 150
  ]
  edge [
    source 26
    target 449
  ]
  edge [
    source 26
    target 450
  ]
  edge [
    source 26
    target 451
  ]
  edge [
    source 26
    target 194
  ]
  edge [
    source 26
    target 452
  ]
  edge [
    source 26
    target 453
  ]
  edge [
    source 26
    target 454
  ]
  edge [
    source 26
    target 455
  ]
  edge [
    source 26
    target 456
  ]
  edge [
    source 26
    target 443
  ]
  edge [
    source 26
    target 457
  ]
  edge [
    source 26
    target 458
  ]
  edge [
    source 26
    target 459
  ]
  edge [
    source 26
    target 460
  ]
  edge [
    source 26
    target 461
  ]
  edge [
    source 26
    target 462
  ]
  edge [
    source 26
    target 463
  ]
  edge [
    source 26
    target 1640
  ]
  edge [
    source 26
    target 1641
  ]
  edge [
    source 26
    target 1642
  ]
  edge [
    source 26
    target 1643
  ]
  edge [
    source 26
    target 1644
  ]
  edge [
    source 26
    target 713
  ]
  edge [
    source 26
    target 1645
  ]
  edge [
    source 26
    target 1646
  ]
  edge [
    source 26
    target 1117
  ]
  edge [
    source 26
    target 1647
  ]
  edge [
    source 26
    target 1648
  ]
  edge [
    source 26
    target 92
  ]
  edge [
    source 26
    target 1649
  ]
  edge [
    source 26
    target 1650
  ]
  edge [
    source 26
    target 1651
  ]
  edge [
    source 26
    target 1652
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 26
    target 201
  ]
  edge [
    source 26
    target 297
  ]
  edge [
    source 26
    target 1653
  ]
  edge [
    source 26
    target 187
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 179
  ]
  edge [
    source 26
    target 1654
  ]
  edge [
    source 26
    target 180
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 1655
  ]
  edge [
    source 26
    target 529
  ]
  edge [
    source 26
    target 1656
  ]
  edge [
    source 26
    target 1657
  ]
  edge [
    source 26
    target 1658
  ]
  edge [
    source 26
    target 689
  ]
  edge [
    source 26
    target 149
  ]
  edge [
    source 26
    target 1659
  ]
  edge [
    source 26
    target 1660
  ]
  edge [
    source 26
    target 413
  ]
  edge [
    source 26
    target 702
  ]
  edge [
    source 26
    target 1661
  ]
  edge [
    source 26
    target 1662
  ]
  edge [
    source 26
    target 1663
  ]
  edge [
    source 26
    target 1664
  ]
  edge [
    source 26
    target 1665
  ]
  edge [
    source 26
    target 1666
  ]
  edge [
    source 26
    target 1667
  ]
  edge [
    source 26
    target 1545
  ]
  edge [
    source 26
    target 1668
  ]
  edge [
    source 26
    target 1669
  ]
  edge [
    source 26
    target 1670
  ]
  edge [
    source 26
    target 1671
  ]
  edge [
    source 26
    target 1672
  ]
  edge [
    source 26
    target 1673
  ]
  edge [
    source 26
    target 157
  ]
  edge [
    source 26
    target 1674
  ]
  edge [
    source 26
    target 1675
  ]
  edge [
    source 26
    target 1676
  ]
  edge [
    source 26
    target 1677
  ]
  edge [
    source 26
    target 1678
  ]
  edge [
    source 26
    target 1679
  ]
  edge [
    source 26
    target 89
  ]
  edge [
    source 26
    target 398
  ]
  edge [
    source 26
    target 1680
  ]
  edge [
    source 26
    target 1681
  ]
  edge [
    source 26
    target 144
  ]
  edge [
    source 26
    target 1682
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 531
  ]
  edge [
    source 26
    target 1683
  ]
  edge [
    source 26
    target 1684
  ]
  edge [
    source 26
    target 1685
  ]
  edge [
    source 26
    target 1686
  ]
  edge [
    source 26
    target 650
  ]
  edge [
    source 26
    target 988
  ]
  edge [
    source 26
    target 1687
  ]
  edge [
    source 26
    target 59
  ]
  edge [
    source 26
    target 1208
  ]
  edge [
    source 26
    target 1688
  ]
  edge [
    source 26
    target 1689
  ]
  edge [
    source 26
    target 1690
  ]
  edge [
    source 26
    target 820
  ]
  edge [
    source 26
    target 1691
  ]
  edge [
    source 26
    target 1692
  ]
  edge [
    source 26
    target 730
  ]
  edge [
    source 26
    target 1693
  ]
  edge [
    source 26
    target 1694
  ]
  edge [
    source 26
    target 1695
  ]
  edge [
    source 26
    target 1696
  ]
  edge [
    source 26
    target 1697
  ]
  edge [
    source 26
    target 1698
  ]
  edge [
    source 26
    target 1699
  ]
  edge [
    source 26
    target 1700
  ]
  edge [
    source 26
    target 1701
  ]
  edge [
    source 26
    target 1702
  ]
  edge [
    source 26
    target 1703
  ]
  edge [
    source 26
    target 1704
  ]
  edge [
    source 26
    target 1705
  ]
  edge [
    source 26
    target 1706
  ]
  edge [
    source 26
    target 1707
  ]
  edge [
    source 26
    target 1708
  ]
  edge [
    source 26
    target 1709
  ]
  edge [
    source 26
    target 1710
  ]
  edge [
    source 26
    target 1711
  ]
  edge [
    source 26
    target 1712
  ]
  edge [
    source 26
    target 437
  ]
  edge [
    source 26
    target 1713
  ]
  edge [
    source 26
    target 1714
  ]
  edge [
    source 26
    target 1715
  ]
  edge [
    source 26
    target 1716
  ]
  edge [
    source 26
    target 111
  ]
  edge [
    source 26
    target 1717
  ]
  edge [
    source 26
    target 1718
  ]
  edge [
    source 26
    target 1719
  ]
  edge [
    source 26
    target 1032
  ]
  edge [
    source 26
    target 701
  ]
  edge [
    source 26
    target 1720
  ]
  edge [
    source 26
    target 1721
  ]
  edge [
    source 26
    target 1722
  ]
  edge [
    source 26
    target 1723
  ]
  edge [
    source 26
    target 1724
  ]
  edge [
    source 26
    target 1725
  ]
  edge [
    source 26
    target 1726
  ]
  edge [
    source 26
    target 1727
  ]
  edge [
    source 26
    target 1728
  ]
  edge [
    source 26
    target 1729
  ]
  edge [
    source 26
    target 1730
  ]
  edge [
    source 26
    target 1731
  ]
  edge [
    source 26
    target 1732
  ]
  edge [
    source 26
    target 1041
  ]
  edge [
    source 26
    target 553
  ]
  edge [
    source 26
    target 1733
  ]
  edge [
    source 26
    target 679
  ]
  edge [
    source 26
    target 1253
  ]
  edge [
    source 26
    target 1734
  ]
  edge [
    source 26
    target 1735
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 46
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 27
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1736
  ]
  edge [
    source 28
    target 1737
  ]
  edge [
    source 28
    target 1738
  ]
  edge [
    source 28
    target 66
  ]
  edge [
    source 28
    target 174
  ]
  edge [
    source 28
    target 175
  ]
  edge [
    source 28
    target 176
  ]
  edge [
    source 28
    target 177
  ]
  edge [
    source 28
    target 178
  ]
  edge [
    source 28
    target 179
  ]
  edge [
    source 28
    target 180
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 182
  ]
  edge [
    source 28
    target 183
  ]
  edge [
    source 28
    target 184
  ]
  edge [
    source 28
    target 185
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 28
    target 188
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 28
    target 190
  ]
  edge [
    source 28
    target 191
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 74
  ]
  edge [
    source 28
    target 193
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 203
  ]
  edge [
    source 28
    target 204
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 28
    target 206
  ]
  edge [
    source 28
    target 207
  ]
  edge [
    source 28
    target 208
  ]
  edge [
    source 28
    target 159
  ]
  edge [
    source 28
    target 209
  ]
  edge [
    source 28
    target 210
  ]
  edge [
    source 28
    target 211
  ]
  edge [
    source 28
    target 212
  ]
  edge [
    source 28
    target 213
  ]
  edge [
    source 28
    target 214
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 1739
  ]
  edge [
    source 28
    target 1740
  ]
  edge [
    source 28
    target 1741
  ]
  edge [
    source 28
    target 1742
  ]
  edge [
    source 28
    target 28
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 46
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 1743
  ]
  edge [
    source 29
    target 1744
  ]
  edge [
    source 29
    target 1745
  ]
  edge [
    source 29
    target 1746
  ]
  edge [
    source 29
    target 1747
  ]
  edge [
    source 29
    target 1463
  ]
  edge [
    source 29
    target 1748
  ]
  edge [
    source 29
    target 1749
  ]
  edge [
    source 29
    target 1750
  ]
  edge [
    source 29
    target 1751
  ]
  edge [
    source 29
    target 29
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 41
  ]
  edge [
    source 30
    target 42
  ]
  edge [
    source 30
    target 1752
  ]
  edge [
    source 30
    target 1753
  ]
  edge [
    source 30
    target 1754
  ]
  edge [
    source 30
    target 1755
  ]
  edge [
    source 30
    target 92
  ]
  edge [
    source 30
    target 1756
  ]
  edge [
    source 30
    target 1757
  ]
  edge [
    source 30
    target 1758
  ]
  edge [
    source 30
    target 1759
  ]
  edge [
    source 30
    target 1760
  ]
  edge [
    source 30
    target 644
  ]
  edge [
    source 30
    target 1761
  ]
  edge [
    source 30
    target 1762
  ]
  edge [
    source 30
    target 1763
  ]
  edge [
    source 30
    target 1764
  ]
  edge [
    source 30
    target 1765
  ]
  edge [
    source 30
    target 1766
  ]
  edge [
    source 30
    target 1555
  ]
  edge [
    source 30
    target 1767
  ]
  edge [
    source 30
    target 1768
  ]
  edge [
    source 30
    target 1769
  ]
  edge [
    source 30
    target 624
  ]
  edge [
    source 30
    target 1770
  ]
  edge [
    source 30
    target 1771
  ]
  edge [
    source 30
    target 1772
  ]
  edge [
    source 30
    target 1773
  ]
  edge [
    source 30
    target 1774
  ]
  edge [
    source 30
    target 1621
  ]
  edge [
    source 30
    target 1775
  ]
  edge [
    source 30
    target 1776
  ]
  edge [
    source 30
    target 1777
  ]
  edge [
    source 30
    target 1778
  ]
  edge [
    source 30
    target 1779
  ]
  edge [
    source 30
    target 65
  ]
  edge [
    source 30
    target 1780
  ]
  edge [
    source 30
    target 1781
  ]
  edge [
    source 30
    target 1782
  ]
  edge [
    source 30
    target 1783
  ]
  edge [
    source 30
    target 1784
  ]
  edge [
    source 30
    target 1785
  ]
  edge [
    source 30
    target 1786
  ]
  edge [
    source 30
    target 1787
  ]
  edge [
    source 30
    target 1788
  ]
  edge [
    source 30
    target 1789
  ]
  edge [
    source 30
    target 195
  ]
  edge [
    source 30
    target 1790
  ]
  edge [
    source 30
    target 1791
  ]
  edge [
    source 30
    target 1792
  ]
  edge [
    source 30
    target 1793
  ]
  edge [
    source 30
    target 1794
  ]
  edge [
    source 30
    target 437
  ]
  edge [
    source 30
    target 1708
  ]
  edge [
    source 30
    target 1795
  ]
  edge [
    source 30
    target 1796
  ]
  edge [
    source 30
    target 518
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 42
  ]
  edge [
    source 31
    target 43
  ]
  edge [
    source 31
    target 1753
  ]
  edge [
    source 31
    target 1797
  ]
  edge [
    source 31
    target 1798
  ]
  edge [
    source 31
    target 1799
  ]
  edge [
    source 31
    target 207
  ]
  edge [
    source 31
    target 1059
  ]
  edge [
    source 31
    target 1800
  ]
  edge [
    source 31
    target 1801
  ]
  edge [
    source 31
    target 1802
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1803
  ]
  edge [
    source 32
    target 1804
  ]
  edge [
    source 32
    target 1805
  ]
  edge [
    source 32
    target 1806
  ]
  edge [
    source 32
    target 1807
  ]
  edge [
    source 32
    target 1808
  ]
  edge [
    source 32
    target 1809
  ]
  edge [
    source 32
    target 1810
  ]
  edge [
    source 32
    target 1811
  ]
  edge [
    source 32
    target 1812
  ]
  edge [
    source 32
    target 1813
  ]
  edge [
    source 32
    target 252
  ]
  edge [
    source 32
    target 1814
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 1815
  ]
  edge [
    source 32
    target 1816
  ]
  edge [
    source 32
    target 1817
  ]
  edge [
    source 32
    target 476
  ]
  edge [
    source 32
    target 1818
  ]
  edge [
    source 32
    target 1819
  ]
  edge [
    source 32
    target 1695
  ]
  edge [
    source 32
    target 1820
  ]
  edge [
    source 32
    target 1821
  ]
  edge [
    source 32
    target 1822
  ]
  edge [
    source 32
    target 460
  ]
  edge [
    source 32
    target 1823
  ]
  edge [
    source 32
    target 1824
  ]
  edge [
    source 32
    target 1825
  ]
  edge [
    source 32
    target 1826
  ]
  edge [
    source 32
    target 1827
  ]
  edge [
    source 32
    target 1007
  ]
  edge [
    source 32
    target 334
  ]
  edge [
    source 32
    target 1828
  ]
  edge [
    source 32
    target 551
  ]
  edge [
    source 32
    target 1829
  ]
  edge [
    source 32
    target 847
  ]
  edge [
    source 32
    target 1830
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 32
    target 1831
  ]
  edge [
    source 32
    target 1832
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 1833
  ]
  edge [
    source 32
    target 1834
  ]
  edge [
    source 32
    target 1835
  ]
  edge [
    source 32
    target 1430
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 1836
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 231
  ]
  edge [
    source 32
    target 1837
  ]
  edge [
    source 32
    target 1838
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 1839
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 1840
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 32
    target 1841
  ]
  edge [
    source 32
    target 1842
  ]
  edge [
    source 32
    target 1843
  ]
  edge [
    source 32
    target 1844
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 259
  ]
  edge [
    source 32
    target 440
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 327
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 332
  ]
  edge [
    source 32
    target 333
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 32
    target 579
  ]
  edge [
    source 32
    target 1845
  ]
  edge [
    source 32
    target 1846
  ]
  edge [
    source 32
    target 1847
  ]
  edge [
    source 32
    target 166
  ]
  edge [
    source 32
    target 1132
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 84
  ]
  edge [
    source 33
    target 93
  ]
  edge [
    source 33
    target 1848
  ]
  edge [
    source 33
    target 1849
  ]
  edge [
    source 33
    target 1850
  ]
  edge [
    source 33
    target 1851
  ]
  edge [
    source 33
    target 1852
  ]
  edge [
    source 33
    target 1853
  ]
  edge [
    source 33
    target 1854
  ]
  edge [
    source 33
    target 1855
  ]
  edge [
    source 33
    target 1856
  ]
  edge [
    source 33
    target 1857
  ]
  edge [
    source 33
    target 1858
  ]
  edge [
    source 33
    target 1859
  ]
  edge [
    source 33
    target 1860
  ]
  edge [
    source 33
    target 1861
  ]
  edge [
    source 33
    target 1862
  ]
  edge [
    source 33
    target 1863
  ]
  edge [
    source 33
    target 1864
  ]
  edge [
    source 33
    target 1865
  ]
  edge [
    source 33
    target 1866
  ]
  edge [
    source 33
    target 1867
  ]
  edge [
    source 33
    target 1868
  ]
  edge [
    source 33
    target 59
  ]
  edge [
    source 33
    target 1869
  ]
  edge [
    source 33
    target 1870
  ]
  edge [
    source 33
    target 1871
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 33
    target 1872
  ]
  edge [
    source 33
    target 1873
  ]
  edge [
    source 33
    target 1874
  ]
  edge [
    source 33
    target 1875
  ]
  edge [
    source 33
    target 1876
  ]
  edge [
    source 33
    target 1877
  ]
  edge [
    source 33
    target 1232
  ]
  edge [
    source 33
    target 1878
  ]
  edge [
    source 33
    target 1879
  ]
  edge [
    source 33
    target 1880
  ]
  edge [
    source 33
    target 1881
  ]
  edge [
    source 33
    target 1882
  ]
  edge [
    source 33
    target 78
  ]
  edge [
    source 33
    target 1883
  ]
  edge [
    source 33
    target 1697
  ]
  edge [
    source 33
    target 1884
  ]
  edge [
    source 33
    target 1885
  ]
  edge [
    source 33
    target 1886
  ]
  edge [
    source 33
    target 1887
  ]
  edge [
    source 33
    target 1888
  ]
  edge [
    source 33
    target 1889
  ]
  edge [
    source 33
    target 1890
  ]
  edge [
    source 33
    target 1891
  ]
  edge [
    source 33
    target 1892
  ]
  edge [
    source 33
    target 1893
  ]
  edge [
    source 33
    target 1894
  ]
  edge [
    source 33
    target 1895
  ]
  edge [
    source 33
    target 457
  ]
  edge [
    source 33
    target 1896
  ]
  edge [
    source 33
    target 1897
  ]
  edge [
    source 33
    target 1898
  ]
  edge [
    source 33
    target 1899
  ]
  edge [
    source 33
    target 1900
  ]
  edge [
    source 33
    target 1901
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 34
    target 1902
  ]
  edge [
    source 34
    target 1903
  ]
  edge [
    source 34
    target 747
  ]
  edge [
    source 34
    target 1904
  ]
  edge [
    source 34
    target 1905
  ]
  edge [
    source 34
    target 1906
  ]
  edge [
    source 34
    target 1907
  ]
  edge [
    source 34
    target 59
  ]
  edge [
    source 34
    target 1908
  ]
  edge [
    source 34
    target 1669
  ]
  edge [
    source 34
    target 1909
  ]
  edge [
    source 34
    target 1910
  ]
  edge [
    source 34
    target 74
  ]
  edge [
    source 34
    target 1229
  ]
  edge [
    source 34
    target 1911
  ]
  edge [
    source 34
    target 1912
  ]
  edge [
    source 34
    target 1913
  ]
  edge [
    source 34
    target 1914
  ]
  edge [
    source 34
    target 1915
  ]
  edge [
    source 34
    target 1916
  ]
  edge [
    source 34
    target 1917
  ]
  edge [
    source 34
    target 460
  ]
  edge [
    source 34
    target 756
  ]
  edge [
    source 34
    target 1918
  ]
  edge [
    source 34
    target 650
  ]
  edge [
    source 34
    target 187
  ]
  edge [
    source 34
    target 1075
  ]
  edge [
    source 34
    target 870
  ]
  edge [
    source 34
    target 1919
  ]
  edge [
    source 34
    target 57
  ]
  edge [
    source 34
    target 1736
  ]
  edge [
    source 34
    target 81
  ]
  edge [
    source 34
    target 82
  ]
  edge [
    source 34
    target 83
  ]
  edge [
    source 34
    target 84
  ]
  edge [
    source 34
    target 85
  ]
  edge [
    source 34
    target 86
  ]
  edge [
    source 34
    target 87
  ]
  edge [
    source 34
    target 88
  ]
  edge [
    source 34
    target 62
  ]
  edge [
    source 34
    target 89
  ]
  edge [
    source 34
    target 90
  ]
  edge [
    source 34
    target 65
  ]
  edge [
    source 34
    target 826
  ]
  edge [
    source 34
    target 827
  ]
  edge [
    source 34
    target 828
  ]
  edge [
    source 34
    target 354
  ]
  edge [
    source 34
    target 829
  ]
  edge [
    source 34
    target 830
  ]
  edge [
    source 34
    target 831
  ]
  edge [
    source 34
    target 832
  ]
  edge [
    source 34
    target 833
  ]
  edge [
    source 34
    target 834
  ]
  edge [
    source 34
    target 835
  ]
  edge [
    source 34
    target 1920
  ]
  edge [
    source 34
    target 129
  ]
  edge [
    source 34
    target 1921
  ]
  edge [
    source 34
    target 1292
  ]
  edge [
    source 34
    target 1922
  ]
  edge [
    source 34
    target 1923
  ]
  edge [
    source 34
    target 570
  ]
  edge [
    source 34
    target 1924
  ]
  edge [
    source 34
    target 1925
  ]
  edge [
    source 34
    target 1926
  ]
  edge [
    source 34
    target 1927
  ]
  edge [
    source 34
    target 733
  ]
  edge [
    source 34
    target 1928
  ]
  edge [
    source 34
    target 1929
  ]
  edge [
    source 34
    target 1930
  ]
  edge [
    source 34
    target 648
  ]
  edge [
    source 34
    target 1931
  ]
  edge [
    source 34
    target 845
  ]
  edge [
    source 34
    target 1932
  ]
  edge [
    source 34
    target 1933
  ]
  edge [
    source 34
    target 1934
  ]
  edge [
    source 34
    target 1935
  ]
  edge [
    source 34
    target 1936
  ]
  edge [
    source 34
    target 297
  ]
  edge [
    source 34
    target 1937
  ]
  edge [
    source 34
    target 1938
  ]
  edge [
    source 34
    target 1939
  ]
  edge [
    source 34
    target 1940
  ]
  edge [
    source 34
    target 92
  ]
  edge [
    source 34
    target 1941
  ]
  edge [
    source 34
    target 1452
  ]
  edge [
    source 34
    target 1942
  ]
  edge [
    source 34
    target 1943
  ]
  edge [
    source 34
    target 1944
  ]
  edge [
    source 34
    target 1463
  ]
  edge [
    source 34
    target 1945
  ]
  edge [
    source 34
    target 1946
  ]
  edge [
    source 34
    target 1947
  ]
  edge [
    source 34
    target 1948
  ]
  edge [
    source 34
    target 1949
  ]
  edge [
    source 34
    target 872
  ]
  edge [
    source 34
    target 1117
  ]
  edge [
    source 34
    target 1950
  ]
  edge [
    source 34
    target 801
  ]
  edge [
    source 34
    target 1951
  ]
  edge [
    source 34
    target 962
  ]
  edge [
    source 34
    target 1952
  ]
  edge [
    source 34
    target 931
  ]
  edge [
    source 34
    target 1953
  ]
  edge [
    source 34
    target 324
  ]
  edge [
    source 34
    target 1954
  ]
  edge [
    source 34
    target 1955
  ]
  edge [
    source 34
    target 1956
  ]
  edge [
    source 34
    target 1957
  ]
  edge [
    source 34
    target 1958
  ]
  edge [
    source 34
    target 1959
  ]
  edge [
    source 34
    target 174
  ]
  edge [
    source 34
    target 175
  ]
  edge [
    source 34
    target 783
  ]
  edge [
    source 34
    target 784
  ]
  edge [
    source 34
    target 785
  ]
  edge [
    source 34
    target 786
  ]
  edge [
    source 34
    target 176
  ]
  edge [
    source 34
    target 787
  ]
  edge [
    source 34
    target 177
  ]
  edge [
    source 34
    target 788
  ]
  edge [
    source 34
    target 789
  ]
  edge [
    source 34
    target 790
  ]
  edge [
    source 34
    target 791
  ]
  edge [
    source 34
    target 183
  ]
  edge [
    source 34
    target 792
  ]
  edge [
    source 34
    target 186
  ]
  edge [
    source 34
    target 793
  ]
  edge [
    source 34
    target 66
  ]
  edge [
    source 34
    target 192
  ]
  edge [
    source 34
    target 794
  ]
  edge [
    source 34
    target 795
  ]
  edge [
    source 34
    target 796
  ]
  edge [
    source 34
    target 196
  ]
  edge [
    source 34
    target 198
  ]
  edge [
    source 34
    target 199
  ]
  edge [
    source 34
    target 797
  ]
  edge [
    source 34
    target 200
  ]
  edge [
    source 34
    target 798
  ]
  edge [
    source 34
    target 800
  ]
  edge [
    source 34
    target 202
  ]
  edge [
    source 34
    target 799
  ]
  edge [
    source 34
    target 802
  ]
  edge [
    source 34
    target 803
  ]
  edge [
    source 34
    target 804
  ]
  edge [
    source 34
    target 805
  ]
  edge [
    source 34
    target 806
  ]
  edge [
    source 34
    target 807
  ]
  edge [
    source 34
    target 808
  ]
  edge [
    source 34
    target 809
  ]
  edge [
    source 34
    target 1960
  ]
  edge [
    source 34
    target 1961
  ]
  edge [
    source 34
    target 1962
  ]
  edge [
    source 34
    target 571
  ]
  edge [
    source 34
    target 585
  ]
  edge [
    source 34
    target 47
  ]
  edge [
    source 34
    target 46
  ]
  edge [
    source 34
    target 2230
  ]
  edge [
    source 34
    target 2231
  ]
  edge [
    source 34
    target 2232
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1963
  ]
  edge [
    source 35
    target 1964
  ]
  edge [
    source 35
    target 1965
  ]
  edge [
    source 35
    target 1966
  ]
  edge [
    source 35
    target 1967
  ]
  edge [
    source 35
    target 1968
  ]
  edge [
    source 35
    target 1969
  ]
  edge [
    source 35
    target 1970
  ]
  edge [
    source 35
    target 1971
  ]
  edge [
    source 35
    target 1972
  ]
  edge [
    source 35
    target 1973
  ]
  edge [
    source 35
    target 1974
  ]
  edge [
    source 35
    target 1975
  ]
  edge [
    source 35
    target 1745
  ]
  edge [
    source 35
    target 1108
  ]
  edge [
    source 35
    target 1976
  ]
  edge [
    source 35
    target 1977
  ]
  edge [
    source 35
    target 1978
  ]
  edge [
    source 35
    target 1979
  ]
  edge [
    source 35
    target 1980
  ]
  edge [
    source 35
    target 1981
  ]
  edge [
    source 35
    target 1982
  ]
  edge [
    source 35
    target 1983
  ]
  edge [
    source 35
    target 1984
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1985
  ]
  edge [
    source 37
    target 1986
  ]
  edge [
    source 37
    target 263
  ]
  edge [
    source 37
    target 1987
  ]
  edge [
    source 37
    target 1988
  ]
  edge [
    source 37
    target 1989
  ]
  edge [
    source 37
    target 1990
  ]
  edge [
    source 37
    target 1991
  ]
  edge [
    source 37
    target 1992
  ]
  edge [
    source 37
    target 1993
  ]
  edge [
    source 37
    target 1994
  ]
  edge [
    source 37
    target 1995
  ]
  edge [
    source 37
    target 1996
  ]
  edge [
    source 37
    target 1997
  ]
  edge [
    source 37
    target 1998
  ]
  edge [
    source 37
    target 1999
  ]
  edge [
    source 37
    target 2000
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 2001
  ]
  edge [
    source 38
    target 2002
  ]
  edge [
    source 38
    target 1666
  ]
  edge [
    source 38
    target 2003
  ]
  edge [
    source 38
    target 2004
  ]
  edge [
    source 38
    target 166
  ]
  edge [
    source 38
    target 2005
  ]
  edge [
    source 38
    target 561
  ]
  edge [
    source 38
    target 2006
  ]
  edge [
    source 38
    target 2007
  ]
  edge [
    source 38
    target 2008
  ]
  edge [
    source 38
    target 2009
  ]
  edge [
    source 38
    target 2010
  ]
  edge [
    source 38
    target 2011
  ]
  edge [
    source 38
    target 2012
  ]
  edge [
    source 38
    target 2013
  ]
  edge [
    source 38
    target 2014
  ]
  edge [
    source 39
    target 2015
  ]
  edge [
    source 39
    target 2016
  ]
  edge [
    source 39
    target 2017
  ]
  edge [
    source 39
    target 2018
  ]
  edge [
    source 39
    target 1858
  ]
  edge [
    source 39
    target 2019
  ]
  edge [
    source 39
    target 2020
  ]
  edge [
    source 39
    target 333
  ]
  edge [
    source 39
    target 2021
  ]
  edge [
    source 39
    target 461
  ]
  edge [
    source 39
    target 2022
  ]
  edge [
    source 39
    target 2023
  ]
  edge [
    source 39
    target 2024
  ]
  edge [
    source 39
    target 2025
  ]
  edge [
    source 39
    target 853
  ]
  edge [
    source 39
    target 861
  ]
  edge [
    source 39
    target 2026
  ]
  edge [
    source 39
    target 1357
  ]
  edge [
    source 39
    target 855
  ]
  edge [
    source 39
    target 938
  ]
  edge [
    source 39
    target 2027
  ]
  edge [
    source 39
    target 2028
  ]
  edge [
    source 39
    target 2029
  ]
  edge [
    source 39
    target 2030
  ]
  edge [
    source 39
    target 2031
  ]
  edge [
    source 39
    target 93
  ]
  edge [
    source 39
    target 2032
  ]
  edge [
    source 39
    target 2033
  ]
  edge [
    source 39
    target 2034
  ]
  edge [
    source 39
    target 2035
  ]
  edge [
    source 39
    target 2036
  ]
  edge [
    source 39
    target 2037
  ]
  edge [
    source 39
    target 2038
  ]
  edge [
    source 39
    target 2039
  ]
  edge [
    source 39
    target 324
  ]
  edge [
    source 39
    target 2040
  ]
  edge [
    source 39
    target 459
  ]
  edge [
    source 41
    target 2041
  ]
  edge [
    source 41
    target 2042
  ]
  edge [
    source 41
    target 2043
  ]
  edge [
    source 41
    target 1350
  ]
  edge [
    source 41
    target 2044
  ]
  edge [
    source 41
    target 2045
  ]
  edge [
    source 41
    target 1381
  ]
  edge [
    source 41
    target 250
  ]
  edge [
    source 41
    target 2046
  ]
  edge [
    source 41
    target 2047
  ]
  edge [
    source 41
    target 2048
  ]
  edge [
    source 41
    target 2049
  ]
  edge [
    source 41
    target 2050
  ]
  edge [
    source 41
    target 1858
  ]
  edge [
    source 41
    target 2051
  ]
  edge [
    source 41
    target 2052
  ]
  edge [
    source 41
    target 2053
  ]
  edge [
    source 41
    target 2054
  ]
  edge [
    source 41
    target 2055
  ]
  edge [
    source 41
    target 2056
  ]
  edge [
    source 41
    target 2057
  ]
  edge [
    source 41
    target 2058
  ]
  edge [
    source 41
    target 935
  ]
  edge [
    source 41
    target 2059
  ]
  edge [
    source 41
    target 479
  ]
  edge [
    source 41
    target 2060
  ]
  edge [
    source 41
    target 2061
  ]
  edge [
    source 41
    target 2062
  ]
  edge [
    source 41
    target 2063
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 1582
  ]
  edge [
    source 41
    target 2064
  ]
  edge [
    source 41
    target 2065
  ]
  edge [
    source 41
    target 1373
  ]
  edge [
    source 41
    target 2066
  ]
  edge [
    source 41
    target 2067
  ]
  edge [
    source 41
    target 2068
  ]
  edge [
    source 41
    target 2069
  ]
  edge [
    source 41
    target 2070
  ]
  edge [
    source 41
    target 2071
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 2072
  ]
  edge [
    source 41
    target 1371
  ]
  edge [
    source 41
    target 928
  ]
  edge [
    source 41
    target 2073
  ]
  edge [
    source 41
    target 2074
  ]
  edge [
    source 41
    target 2075
  ]
  edge [
    source 41
    target 2076
  ]
  edge [
    source 41
    target 2077
  ]
  edge [
    source 41
    target 2078
  ]
  edge [
    source 41
    target 2079
  ]
  edge [
    source 41
    target 2080
  ]
  edge [
    source 41
    target 2081
  ]
  edge [
    source 41
    target 1372
  ]
  edge [
    source 41
    target 327
  ]
  edge [
    source 41
    target 2082
  ]
  edge [
    source 41
    target 645
  ]
  edge [
    source 41
    target 2083
  ]
  edge [
    source 41
    target 2084
  ]
  edge [
    source 41
    target 2085
  ]
  edge [
    source 41
    target 222
  ]
  edge [
    source 41
    target 2086
  ]
  edge [
    source 41
    target 2087
  ]
  edge [
    source 41
    target 2088
  ]
  edge [
    source 41
    target 2089
  ]
  edge [
    source 41
    target 2090
  ]
  edge [
    source 41
    target 2091
  ]
  edge [
    source 41
    target 2092
  ]
  edge [
    source 41
    target 2093
  ]
  edge [
    source 41
    target 879
  ]
  edge [
    source 41
    target 2094
  ]
  edge [
    source 41
    target 2095
  ]
  edge [
    source 41
    target 2096
  ]
  edge [
    source 41
    target 2097
  ]
  edge [
    source 41
    target 2098
  ]
  edge [
    source 41
    target 187
  ]
  edge [
    source 41
    target 2099
  ]
  edge [
    source 41
    target 2100
  ]
  edge [
    source 41
    target 62
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2101
  ]
  edge [
    source 43
    target 2102
  ]
  edge [
    source 43
    target 2103
  ]
  edge [
    source 43
    target 608
  ]
  edge [
    source 43
    target 2104
  ]
  edge [
    source 43
    target 528
  ]
  edge [
    source 43
    target 2105
  ]
  edge [
    source 43
    target 2106
  ]
  edge [
    source 43
    target 1130
  ]
  edge [
    source 43
    target 2107
  ]
  edge [
    source 43
    target 2108
  ]
  edge [
    source 43
    target 2109
  ]
  edge [
    source 43
    target 2110
  ]
  edge [
    source 43
    target 2111
  ]
  edge [
    source 43
    target 92
  ]
  edge [
    source 43
    target 1275
  ]
  edge [
    source 43
    target 2112
  ]
  edge [
    source 43
    target 477
  ]
  edge [
    source 43
    target 409
  ]
  edge [
    source 43
    target 999
  ]
  edge [
    source 43
    target 2113
  ]
  edge [
    source 43
    target 2114
  ]
  edge [
    source 43
    target 2115
  ]
  edge [
    source 43
    target 62
  ]
  edge [
    source 43
    target 1488
  ]
  edge [
    source 43
    target 2116
  ]
  edge [
    source 43
    target 713
  ]
  edge [
    source 43
    target 2117
  ]
  edge [
    source 43
    target 84
  ]
  edge [
    source 43
    target 78
  ]
  edge [
    source 43
    target 2118
  ]
  edge [
    source 43
    target 754
  ]
  edge [
    source 43
    target 714
  ]
  edge [
    source 43
    target 87
  ]
  edge [
    source 43
    target 2119
  ]
  edge [
    source 43
    target 2120
  ]
  edge [
    source 43
    target 780
  ]
  edge [
    source 43
    target 2121
  ]
  edge [
    source 43
    target 2122
  ]
  edge [
    source 43
    target 2123
  ]
  edge [
    source 43
    target 2124
  ]
  edge [
    source 43
    target 144
  ]
  edge [
    source 43
    target 2125
  ]
  edge [
    source 43
    target 1229
  ]
  edge [
    source 43
    target 1690
  ]
  edge [
    source 43
    target 2126
  ]
  edge [
    source 43
    target 59
  ]
  edge [
    source 43
    target 2127
  ]
  edge [
    source 43
    target 2128
  ]
  edge [
    source 43
    target 522
  ]
  edge [
    source 43
    target 529
  ]
  edge [
    source 43
    target 2129
  ]
  edge [
    source 43
    target 2130
  ]
  edge [
    source 43
    target 2131
  ]
  edge [
    source 43
    target 2132
  ]
  edge [
    source 43
    target 2133
  ]
  edge [
    source 43
    target 2134
  ]
  edge [
    source 43
    target 2135
  ]
  edge [
    source 43
    target 1083
  ]
  edge [
    source 43
    target 2136
  ]
  edge [
    source 43
    target 2137
  ]
  edge [
    source 43
    target 1445
  ]
  edge [
    source 43
    target 2138
  ]
  edge [
    source 43
    target 2139
  ]
  edge [
    source 43
    target 2140
  ]
  edge [
    source 43
    target 2141
  ]
  edge [
    source 43
    target 2142
  ]
  edge [
    source 43
    target 1910
  ]
  edge [
    source 44
    target 2143
  ]
  edge [
    source 44
    target 2144
  ]
  edge [
    source 44
    target 2145
  ]
  edge [
    source 44
    target 2146
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2147
  ]
  edge [
    source 46
    target 2148
  ]
  edge [
    source 46
    target 1947
  ]
  edge [
    source 46
    target 2149
  ]
  edge [
    source 46
    target 2150
  ]
  edge [
    source 46
    target 2151
  ]
  edge [
    source 46
    target 2152
  ]
  edge [
    source 46
    target 2153
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 333
  ]
  edge [
    source 47
    target 2154
  ]
  edge [
    source 47
    target 2155
  ]
  edge [
    source 47
    target 854
  ]
  edge [
    source 47
    target 2156
  ]
  edge [
    source 47
    target 850
  ]
  edge [
    source 47
    target 2157
  ]
  edge [
    source 47
    target 2158
  ]
  edge [
    source 47
    target 2159
  ]
  edge [
    source 47
    target 2039
  ]
  edge [
    source 47
    target 2160
  ]
  edge [
    source 47
    target 2161
  ]
  edge [
    source 47
    target 2063
  ]
  edge [
    source 47
    target 2162
  ]
  edge [
    source 47
    target 762
  ]
  edge [
    source 47
    target 2163
  ]
  edge [
    source 47
    target 2164
  ]
  edge [
    source 47
    target 2165
  ]
  edge [
    source 47
    target 2166
  ]
  edge [
    source 47
    target 2167
  ]
  edge [
    source 47
    target 2168
  ]
  edge [
    source 47
    target 2153
  ]
  edge [
    source 47
    target 339
  ]
  edge [
    source 47
    target 2169
  ]
  edge [
    source 47
    target 2170
  ]
  edge [
    source 47
    target 855
  ]
  edge [
    source 47
    target 1193
  ]
  edge [
    source 47
    target 1194
  ]
  edge [
    source 47
    target 2171
  ]
  edge [
    source 47
    target 873
  ]
  edge [
    source 47
    target 2172
  ]
  edge [
    source 47
    target 2173
  ]
  edge [
    source 47
    target 2174
  ]
  edge [
    source 47
    target 328
  ]
  edge [
    source 47
    target 1190
  ]
  edge [
    source 47
    target 2175
  ]
  edge [
    source 47
    target 2176
  ]
  edge [
    source 47
    target 340
  ]
  edge [
    source 47
    target 928
  ]
  edge [
    source 47
    target 2177
  ]
  edge [
    source 47
    target 312
  ]
  edge [
    source 47
    target 935
  ]
  edge [
    source 47
    target 2178
  ]
  edge [
    source 47
    target 342
  ]
  edge [
    source 47
    target 1376
  ]
  edge [
    source 47
    target 2179
  ]
  edge [
    source 47
    target 644
  ]
  edge [
    source 47
    target 865
  ]
  edge [
    source 47
    target 866
  ]
  edge [
    source 47
    target 867
  ]
  edge [
    source 47
    target 868
  ]
  edge [
    source 47
    target 869
  ]
  edge [
    source 47
    target 870
  ]
  edge [
    source 47
    target 871
  ]
  edge [
    source 47
    target 201
  ]
  edge [
    source 47
    target 872
  ]
  edge [
    source 47
    target 236
  ]
  edge [
    source 47
    target 874
  ]
  edge [
    source 47
    target 875
  ]
  edge [
    source 47
    target 876
  ]
  edge [
    source 47
    target 877
  ]
  edge [
    source 47
    target 878
  ]
  edge [
    source 47
    target 879
  ]
  edge [
    source 47
    target 880
  ]
  edge [
    source 47
    target 881
  ]
  edge [
    source 47
    target 882
  ]
  edge [
    source 47
    target 2180
  ]
  edge [
    source 47
    target 958
  ]
  edge [
    source 47
    target 2181
  ]
  edge [
    source 47
    target 2182
  ]
  edge [
    source 47
    target 297
  ]
  edge [
    source 48
    target 2183
  ]
  edge [
    source 48
    target 2184
  ]
  edge [
    source 48
    target 329
  ]
  edge [
    source 48
    target 2185
  ]
  edge [
    source 48
    target 2186
  ]
  edge [
    source 48
    target 2187
  ]
  edge [
    source 48
    target 333
  ]
  edge [
    source 48
    target 2188
  ]
  edge [
    source 48
    target 2189
  ]
  edge [
    source 48
    target 1833
  ]
  edge [
    source 48
    target 2190
  ]
  edge [
    source 48
    target 2191
  ]
  edge [
    source 48
    target 2192
  ]
  edge [
    source 48
    target 2193
  ]
  edge [
    source 49
    target 2194
  ]
  edge [
    source 49
    target 2195
  ]
  edge [
    source 49
    target 2196
  ]
  edge [
    source 49
    target 2197
  ]
  edge [
    source 49
    target 918
  ]
  edge [
    source 49
    target 2198
  ]
  edge [
    source 49
    target 2199
  ]
  edge [
    source 49
    target 2200
  ]
  edge [
    source 49
    target 2201
  ]
  edge [
    source 49
    target 2202
  ]
  edge [
    source 49
    target 2203
  ]
  edge [
    source 49
    target 326
  ]
  edge [
    source 49
    target 2204
  ]
  edge [
    source 49
    target 225
  ]
  edge [
    source 49
    target 1425
  ]
  edge [
    source 49
    target 2205
  ]
  edge [
    source 49
    target 2206
  ]
  edge [
    source 49
    target 2207
  ]
  edge [
    source 49
    target 2208
  ]
  edge [
    source 49
    target 2209
  ]
  edge [
    source 49
    target 2210
  ]
  edge [
    source 49
    target 2211
  ]
  edge [
    source 49
    target 2212
  ]
  edge [
    source 49
    target 637
  ]
  edge [
    source 49
    target 2213
  ]
  edge [
    source 49
    target 615
  ]
  edge [
    source 49
    target 339
  ]
  edge [
    source 49
    target 941
  ]
  edge [
    source 49
    target 942
  ]
  edge [
    source 49
    target 943
  ]
  edge [
    source 49
    target 940
  ]
  edge [
    source 49
    target 944
  ]
  edge [
    source 49
    target 945
  ]
  edge [
    source 49
    target 946
  ]
  edge [
    source 49
    target 2214
  ]
  edge [
    source 49
    target 2215
  ]
  edge [
    source 49
    target 2216
  ]
  edge [
    source 49
    target 2217
  ]
  edge [
    source 49
    target 2218
  ]
  edge [
    source 49
    target 853
  ]
  edge [
    source 49
    target 2219
  ]
  edge [
    source 49
    target 532
  ]
  edge [
    source 49
    target 2220
  ]
  edge [
    source 49
    target 555
  ]
  edge [
    source 49
    target 2221
  ]
  edge [
    source 49
    target 2222
  ]
  edge [
    source 2223
    target 2224
  ]
  edge [
    source 2225
    target 2226
  ]
  edge [
    source 2227
    target 2228
  ]
  edge [
    source 2227
    target 2229
  ]
  edge [
    source 2228
    target 2229
  ]
  edge [
    source 2230
    target 2231
  ]
  edge [
    source 2230
    target 2232
  ]
  edge [
    source 2231
    target 2232
  ]
]
