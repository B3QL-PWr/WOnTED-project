graph [
  node [
    id 0
    label "informacja"
    origin "text"
  ]
  node [
    id 1
    label "niejawny"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 3
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "forma"
    origin "text"
  ]
  node [
    id 5
    label "ustny"
    origin "text"
  ]
  node [
    id 6
    label "wizualny"
    origin "text"
  ]
  node [
    id 7
    label "lub"
    origin "text"
  ]
  node [
    id 8
    label "dokument"
    origin "text"
  ]
  node [
    id 9
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 10
    label "inny"
    origin "text"
  ]
  node [
    id 11
    label "tym"
    origin "text"
  ]
  node [
    id 12
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 13
    label "sprz&#281;t"
    origin "text"
  ]
  node [
    id 14
    label "technologia"
    origin "text"
  ]
  node [
    id 15
    label "punkt"
  ]
  node [
    id 16
    label "publikacja"
  ]
  node [
    id 17
    label "wiedza"
  ]
  node [
    id 18
    label "doj&#347;cie"
  ]
  node [
    id 19
    label "obiega&#263;"
  ]
  node [
    id 20
    label "powzi&#281;cie"
  ]
  node [
    id 21
    label "dane"
  ]
  node [
    id 22
    label "obiegni&#281;cie"
  ]
  node [
    id 23
    label "sygna&#322;"
  ]
  node [
    id 24
    label "obieganie"
  ]
  node [
    id 25
    label "powzi&#261;&#263;"
  ]
  node [
    id 26
    label "obiec"
  ]
  node [
    id 27
    label "doj&#347;&#263;"
  ]
  node [
    id 28
    label "po&#322;o&#380;enie"
  ]
  node [
    id 29
    label "sprawa"
  ]
  node [
    id 30
    label "ust&#281;p"
  ]
  node [
    id 31
    label "plan"
  ]
  node [
    id 32
    label "obiekt_matematyczny"
  ]
  node [
    id 33
    label "problemat"
  ]
  node [
    id 34
    label "plamka"
  ]
  node [
    id 35
    label "stopie&#324;_pisma"
  ]
  node [
    id 36
    label "jednostka"
  ]
  node [
    id 37
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 38
    label "miejsce"
  ]
  node [
    id 39
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 40
    label "mark"
  ]
  node [
    id 41
    label "chwila"
  ]
  node [
    id 42
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 43
    label "prosta"
  ]
  node [
    id 44
    label "problematyka"
  ]
  node [
    id 45
    label "obiekt"
  ]
  node [
    id 46
    label "zapunktowa&#263;"
  ]
  node [
    id 47
    label "podpunkt"
  ]
  node [
    id 48
    label "wojsko"
  ]
  node [
    id 49
    label "kres"
  ]
  node [
    id 50
    label "przestrze&#324;"
  ]
  node [
    id 51
    label "point"
  ]
  node [
    id 52
    label "pozycja"
  ]
  node [
    id 53
    label "cognition"
  ]
  node [
    id 54
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 55
    label "intelekt"
  ]
  node [
    id 56
    label "pozwolenie"
  ]
  node [
    id 57
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 58
    label "zaawansowanie"
  ]
  node [
    id 59
    label "wykszta&#322;cenie"
  ]
  node [
    id 60
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 61
    label "przekazywa&#263;"
  ]
  node [
    id 62
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 63
    label "pulsation"
  ]
  node [
    id 64
    label "przekazywanie"
  ]
  node [
    id 65
    label "przewodzenie"
  ]
  node [
    id 66
    label "d&#378;wi&#281;k"
  ]
  node [
    id 67
    label "po&#322;&#261;czenie"
  ]
  node [
    id 68
    label "fala"
  ]
  node [
    id 69
    label "przekazanie"
  ]
  node [
    id 70
    label "przewodzi&#263;"
  ]
  node [
    id 71
    label "znak"
  ]
  node [
    id 72
    label "zapowied&#378;"
  ]
  node [
    id 73
    label "medium_transmisyjne"
  ]
  node [
    id 74
    label "demodulacja"
  ]
  node [
    id 75
    label "przekaza&#263;"
  ]
  node [
    id 76
    label "czynnik"
  ]
  node [
    id 77
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 78
    label "aliasing"
  ]
  node [
    id 79
    label "wizja"
  ]
  node [
    id 80
    label "modulacja"
  ]
  node [
    id 81
    label "drift"
  ]
  node [
    id 82
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 83
    label "tekst"
  ]
  node [
    id 84
    label "druk"
  ]
  node [
    id 85
    label "produkcja"
  ]
  node [
    id 86
    label "notification"
  ]
  node [
    id 87
    label "edytowa&#263;"
  ]
  node [
    id 88
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 89
    label "spakowanie"
  ]
  node [
    id 90
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 91
    label "pakowa&#263;"
  ]
  node [
    id 92
    label "rekord"
  ]
  node [
    id 93
    label "korelator"
  ]
  node [
    id 94
    label "wyci&#261;ganie"
  ]
  node [
    id 95
    label "pakowanie"
  ]
  node [
    id 96
    label "sekwencjonowa&#263;"
  ]
  node [
    id 97
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 98
    label "jednostka_informacji"
  ]
  node [
    id 99
    label "zbi&#243;r"
  ]
  node [
    id 100
    label "evidence"
  ]
  node [
    id 101
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 102
    label "rozpakowywanie"
  ]
  node [
    id 103
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 104
    label "rozpakowanie"
  ]
  node [
    id 105
    label "rozpakowywa&#263;"
  ]
  node [
    id 106
    label "konwersja"
  ]
  node [
    id 107
    label "nap&#322;ywanie"
  ]
  node [
    id 108
    label "rozpakowa&#263;"
  ]
  node [
    id 109
    label "spakowa&#263;"
  ]
  node [
    id 110
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 111
    label "edytowanie"
  ]
  node [
    id 112
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 113
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 114
    label "sekwencjonowanie"
  ]
  node [
    id 115
    label "flow"
  ]
  node [
    id 116
    label "odwiedza&#263;"
  ]
  node [
    id 117
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 118
    label "rotate"
  ]
  node [
    id 119
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 120
    label "authorize"
  ]
  node [
    id 121
    label "podj&#261;&#263;"
  ]
  node [
    id 122
    label "zacz&#261;&#263;"
  ]
  node [
    id 123
    label "otrzyma&#263;"
  ]
  node [
    id 124
    label "sta&#263;_si&#281;"
  ]
  node [
    id 125
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 126
    label "supervene"
  ]
  node [
    id 127
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 128
    label "zaj&#347;&#263;"
  ]
  node [
    id 129
    label "catch"
  ]
  node [
    id 130
    label "get"
  ]
  node [
    id 131
    label "bodziec"
  ]
  node [
    id 132
    label "przesy&#322;ka"
  ]
  node [
    id 133
    label "dodatek"
  ]
  node [
    id 134
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 135
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 136
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 137
    label "heed"
  ]
  node [
    id 138
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 139
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 140
    label "spowodowa&#263;"
  ]
  node [
    id 141
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 142
    label "dozna&#263;"
  ]
  node [
    id 143
    label "dokoptowa&#263;"
  ]
  node [
    id 144
    label "postrzega&#263;"
  ]
  node [
    id 145
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 146
    label "orgazm"
  ]
  node [
    id 147
    label "dolecie&#263;"
  ]
  node [
    id 148
    label "drive"
  ]
  node [
    id 149
    label "dotrze&#263;"
  ]
  node [
    id 150
    label "uzyska&#263;"
  ]
  node [
    id 151
    label "dop&#322;ata"
  ]
  node [
    id 152
    label "become"
  ]
  node [
    id 153
    label "odwiedzi&#263;"
  ]
  node [
    id 154
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 155
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 156
    label "orb"
  ]
  node [
    id 157
    label "podj&#281;cie"
  ]
  node [
    id 158
    label "otrzymanie"
  ]
  node [
    id 159
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 160
    label "dochodzenie"
  ]
  node [
    id 161
    label "uzyskanie"
  ]
  node [
    id 162
    label "skill"
  ]
  node [
    id 163
    label "dochrapanie_si&#281;"
  ]
  node [
    id 164
    label "znajomo&#347;ci"
  ]
  node [
    id 165
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 166
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 167
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 168
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 169
    label "powi&#261;zanie"
  ]
  node [
    id 170
    label "entrance"
  ]
  node [
    id 171
    label "affiliation"
  ]
  node [
    id 172
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 173
    label "dor&#281;czenie"
  ]
  node [
    id 174
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 175
    label "spowodowanie"
  ]
  node [
    id 176
    label "dost&#281;p"
  ]
  node [
    id 177
    label "gotowy"
  ]
  node [
    id 178
    label "avenue"
  ]
  node [
    id 179
    label "postrzeganie"
  ]
  node [
    id 180
    label "doznanie"
  ]
  node [
    id 181
    label "dojrza&#322;y"
  ]
  node [
    id 182
    label "dojechanie"
  ]
  node [
    id 183
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 184
    label "ingress"
  ]
  node [
    id 185
    label "czynno&#347;&#263;"
  ]
  node [
    id 186
    label "strzelenie"
  ]
  node [
    id 187
    label "orzekni&#281;cie"
  ]
  node [
    id 188
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 189
    label "dolecenie"
  ]
  node [
    id 190
    label "rozpowszechnienie"
  ]
  node [
    id 191
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 192
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 193
    label "stanie_si&#281;"
  ]
  node [
    id 194
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 195
    label "zrobienie"
  ]
  node [
    id 196
    label "odwiedzanie"
  ]
  node [
    id 197
    label "biegni&#281;cie"
  ]
  node [
    id 198
    label "zakre&#347;lanie"
  ]
  node [
    id 199
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 200
    label "okr&#261;&#380;anie"
  ]
  node [
    id 201
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 202
    label "zakre&#347;lenie"
  ]
  node [
    id 203
    label "odwiedzenie"
  ]
  node [
    id 204
    label "okr&#261;&#380;enie"
  ]
  node [
    id 205
    label "zakryty"
  ]
  node [
    id 206
    label "niejawnie"
  ]
  node [
    id 207
    label "ukryty"
  ]
  node [
    id 208
    label "nieoficjalny"
  ]
  node [
    id 209
    label "nieprzejrzysty"
  ]
  node [
    id 210
    label "niepostrzegalny"
  ]
  node [
    id 211
    label "schronienie"
  ]
  node [
    id 212
    label "nieoficjalnie"
  ]
  node [
    id 213
    label "nieformalny"
  ]
  node [
    id 214
    label "niepewny"
  ]
  node [
    id 215
    label "m&#261;cenie"
  ]
  node [
    id 216
    label "trudny"
  ]
  node [
    id 217
    label "ciecz"
  ]
  node [
    id 218
    label "ci&#281;&#380;ki"
  ]
  node [
    id 219
    label "ciemny"
  ]
  node [
    id 220
    label "nieklarowny"
  ]
  node [
    id 221
    label "niezrozumia&#322;y"
  ]
  node [
    id 222
    label "zanieczyszczanie"
  ]
  node [
    id 223
    label "zanieczyszczenie"
  ]
  node [
    id 224
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 225
    label "z&#322;y"
  ]
  node [
    id 226
    label "by&#263;"
  ]
  node [
    id 227
    label "might"
  ]
  node [
    id 228
    label "uprawi&#263;"
  ]
  node [
    id 229
    label "public_treasury"
  ]
  node [
    id 230
    label "pole"
  ]
  node [
    id 231
    label "obrobi&#263;"
  ]
  node [
    id 232
    label "nietrze&#378;wy"
  ]
  node [
    id 233
    label "czekanie"
  ]
  node [
    id 234
    label "martwy"
  ]
  node [
    id 235
    label "bliski"
  ]
  node [
    id 236
    label "gotowo"
  ]
  node [
    id 237
    label "przygotowywanie"
  ]
  node [
    id 238
    label "przygotowanie"
  ]
  node [
    id 239
    label "dyspozycyjny"
  ]
  node [
    id 240
    label "zalany"
  ]
  node [
    id 241
    label "nieuchronny"
  ]
  node [
    id 242
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 243
    label "mie&#263;_miejsce"
  ]
  node [
    id 244
    label "equal"
  ]
  node [
    id 245
    label "trwa&#263;"
  ]
  node [
    id 246
    label "chodzi&#263;"
  ]
  node [
    id 247
    label "si&#281;ga&#263;"
  ]
  node [
    id 248
    label "stan"
  ]
  node [
    id 249
    label "obecno&#347;&#263;"
  ]
  node [
    id 250
    label "stand"
  ]
  node [
    id 251
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 252
    label "uczestniczy&#263;"
  ]
  node [
    id 253
    label "hide"
  ]
  node [
    id 254
    label "czu&#263;"
  ]
  node [
    id 255
    label "support"
  ]
  node [
    id 256
    label "need"
  ]
  node [
    id 257
    label "wykonawca"
  ]
  node [
    id 258
    label "interpretator"
  ]
  node [
    id 259
    label "cover"
  ]
  node [
    id 260
    label "przewidywa&#263;"
  ]
  node [
    id 261
    label "smell"
  ]
  node [
    id 262
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 263
    label "uczuwa&#263;"
  ]
  node [
    id 264
    label "spirit"
  ]
  node [
    id 265
    label "doznawa&#263;"
  ]
  node [
    id 266
    label "anticipate"
  ]
  node [
    id 267
    label "kszta&#322;t"
  ]
  node [
    id 268
    label "temat"
  ]
  node [
    id 269
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 270
    label "jednostka_systematyczna"
  ]
  node [
    id 271
    label "poznanie"
  ]
  node [
    id 272
    label "leksem"
  ]
  node [
    id 273
    label "dzie&#322;o"
  ]
  node [
    id 274
    label "blaszka"
  ]
  node [
    id 275
    label "poj&#281;cie"
  ]
  node [
    id 276
    label "kantyzm"
  ]
  node [
    id 277
    label "zdolno&#347;&#263;"
  ]
  node [
    id 278
    label "cecha"
  ]
  node [
    id 279
    label "do&#322;ek"
  ]
  node [
    id 280
    label "zawarto&#347;&#263;"
  ]
  node [
    id 281
    label "gwiazda"
  ]
  node [
    id 282
    label "formality"
  ]
  node [
    id 283
    label "struktura"
  ]
  node [
    id 284
    label "wygl&#261;d"
  ]
  node [
    id 285
    label "mode"
  ]
  node [
    id 286
    label "morfem"
  ]
  node [
    id 287
    label "rdze&#324;"
  ]
  node [
    id 288
    label "posta&#263;"
  ]
  node [
    id 289
    label "kielich"
  ]
  node [
    id 290
    label "ornamentyka"
  ]
  node [
    id 291
    label "pasmo"
  ]
  node [
    id 292
    label "zwyczaj"
  ]
  node [
    id 293
    label "punkt_widzenia"
  ]
  node [
    id 294
    label "g&#322;owa"
  ]
  node [
    id 295
    label "naczynie"
  ]
  node [
    id 296
    label "p&#322;at"
  ]
  node [
    id 297
    label "maszyna_drukarska"
  ]
  node [
    id 298
    label "style"
  ]
  node [
    id 299
    label "linearno&#347;&#263;"
  ]
  node [
    id 300
    label "wyra&#380;enie"
  ]
  node [
    id 301
    label "formacja"
  ]
  node [
    id 302
    label "spirala"
  ]
  node [
    id 303
    label "dyspozycja"
  ]
  node [
    id 304
    label "odmiana"
  ]
  node [
    id 305
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 306
    label "wz&#243;r"
  ]
  node [
    id 307
    label "October"
  ]
  node [
    id 308
    label "creation"
  ]
  node [
    id 309
    label "p&#281;tla"
  ]
  node [
    id 310
    label "arystotelizm"
  ]
  node [
    id 311
    label "szablon"
  ]
  node [
    id 312
    label "miniatura"
  ]
  node [
    id 313
    label "acquaintance"
  ]
  node [
    id 314
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 315
    label "spotkanie"
  ]
  node [
    id 316
    label "nauczenie_si&#281;"
  ]
  node [
    id 317
    label "poczucie"
  ]
  node [
    id 318
    label "knowing"
  ]
  node [
    id 319
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 320
    label "zapoznanie_si&#281;"
  ]
  node [
    id 321
    label "wy&#347;wiadczenie"
  ]
  node [
    id 322
    label "inclusion"
  ]
  node [
    id 323
    label "zrozumienie"
  ]
  node [
    id 324
    label "zawarcie"
  ]
  node [
    id 325
    label "designation"
  ]
  node [
    id 326
    label "umo&#380;liwienie"
  ]
  node [
    id 327
    label "sensing"
  ]
  node [
    id 328
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 329
    label "gathering"
  ]
  node [
    id 330
    label "zapoznanie"
  ]
  node [
    id 331
    label "znajomy"
  ]
  node [
    id 332
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 333
    label "obrazowanie"
  ]
  node [
    id 334
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 335
    label "dorobek"
  ]
  node [
    id 336
    label "tre&#347;&#263;"
  ]
  node [
    id 337
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 338
    label "retrospektywa"
  ]
  node [
    id 339
    label "works"
  ]
  node [
    id 340
    label "tetralogia"
  ]
  node [
    id 341
    label "komunikat"
  ]
  node [
    id 342
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 343
    label "praca"
  ]
  node [
    id 344
    label "mutant"
  ]
  node [
    id 345
    label "rewizja"
  ]
  node [
    id 346
    label "gramatyka"
  ]
  node [
    id 347
    label "typ"
  ]
  node [
    id 348
    label "paradygmat"
  ]
  node [
    id 349
    label "change"
  ]
  node [
    id 350
    label "podgatunek"
  ]
  node [
    id 351
    label "ferment"
  ]
  node [
    id 352
    label "rasa"
  ]
  node [
    id 353
    label "zjawisko"
  ]
  node [
    id 354
    label "pos&#322;uchanie"
  ]
  node [
    id 355
    label "skumanie"
  ]
  node [
    id 356
    label "orientacja"
  ]
  node [
    id 357
    label "wytw&#243;r"
  ]
  node [
    id 358
    label "zorientowanie"
  ]
  node [
    id 359
    label "teoria"
  ]
  node [
    id 360
    label "clasp"
  ]
  node [
    id 361
    label "przem&#243;wienie"
  ]
  node [
    id 362
    label "idealizm"
  ]
  node [
    id 363
    label "szko&#322;a"
  ]
  node [
    id 364
    label "koncepcja"
  ]
  node [
    id 365
    label "imperatyw_kategoryczny"
  ]
  node [
    id 366
    label "przedmiot"
  ]
  node [
    id 367
    label "kopia"
  ]
  node [
    id 368
    label "utw&#243;r"
  ]
  node [
    id 369
    label "obraz"
  ]
  node [
    id 370
    label "ilustracja"
  ]
  node [
    id 371
    label "miniature"
  ]
  node [
    id 372
    label "roztruchan"
  ]
  node [
    id 373
    label "dzia&#322;ka"
  ]
  node [
    id 374
    label "kwiat"
  ]
  node [
    id 375
    label "puch_kielichowy"
  ]
  node [
    id 376
    label "Graal"
  ]
  node [
    id 377
    label "akrobacja_lotnicza"
  ]
  node [
    id 378
    label "whirl"
  ]
  node [
    id 379
    label "krzywa"
  ]
  node [
    id 380
    label "spiralny"
  ]
  node [
    id 381
    label "nagromadzenie"
  ]
  node [
    id 382
    label "wk&#322;adka"
  ]
  node [
    id 383
    label "spirograf"
  ]
  node [
    id 384
    label "spiral"
  ]
  node [
    id 385
    label "organizacja"
  ]
  node [
    id 386
    label "postarzenie"
  ]
  node [
    id 387
    label "postarzanie"
  ]
  node [
    id 388
    label "brzydota"
  ]
  node [
    id 389
    label "portrecista"
  ]
  node [
    id 390
    label "postarza&#263;"
  ]
  node [
    id 391
    label "nadawanie"
  ]
  node [
    id 392
    label "postarzy&#263;"
  ]
  node [
    id 393
    label "widok"
  ]
  node [
    id 394
    label "prostota"
  ]
  node [
    id 395
    label "ubarwienie"
  ]
  node [
    id 396
    label "shape"
  ]
  node [
    id 397
    label "comeliness"
  ]
  node [
    id 398
    label "face"
  ]
  node [
    id 399
    label "charakter"
  ]
  node [
    id 400
    label "sid&#322;a"
  ]
  node [
    id 401
    label "ko&#322;o"
  ]
  node [
    id 402
    label "p&#281;tlica"
  ]
  node [
    id 403
    label "hank"
  ]
  node [
    id 404
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 405
    label "zawi&#261;zywanie"
  ]
  node [
    id 406
    label "z&#322;&#261;czenie"
  ]
  node [
    id 407
    label "zawi&#261;zanie"
  ]
  node [
    id 408
    label "arrest"
  ]
  node [
    id 409
    label "zawi&#261;za&#263;"
  ]
  node [
    id 410
    label "koniec"
  ]
  node [
    id 411
    label "kra&#324;c&#243;wka"
  ]
  node [
    id 412
    label "signal"
  ]
  node [
    id 413
    label "li&#347;&#263;"
  ]
  node [
    id 414
    label "tw&#243;r"
  ]
  node [
    id 415
    label "odznaczenie"
  ]
  node [
    id 416
    label "kapelusz"
  ]
  node [
    id 417
    label "kawa&#322;ek"
  ]
  node [
    id 418
    label "centrop&#322;at"
  ]
  node [
    id 419
    label "organ"
  ]
  node [
    id 420
    label "airfoil"
  ]
  node [
    id 421
    label "&#347;mig&#322;owiec"
  ]
  node [
    id 422
    label "samolot"
  ]
  node [
    id 423
    label "piece"
  ]
  node [
    id 424
    label "plaster"
  ]
  node [
    id 425
    label "Arktur"
  ]
  node [
    id 426
    label "Gwiazda_Polarna"
  ]
  node [
    id 427
    label "agregatka"
  ]
  node [
    id 428
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 429
    label "gromada"
  ]
  node [
    id 430
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 431
    label "S&#322;o&#324;ce"
  ]
  node [
    id 432
    label "Nibiru"
  ]
  node [
    id 433
    label "konstelacja"
  ]
  node [
    id 434
    label "ornament"
  ]
  node [
    id 435
    label "delta_Scuti"
  ]
  node [
    id 436
    label "&#347;wiat&#322;o"
  ]
  node [
    id 437
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 438
    label "s&#322;awa"
  ]
  node [
    id 439
    label "promie&#324;"
  ]
  node [
    id 440
    label "star"
  ]
  node [
    id 441
    label "gwiazdosz"
  ]
  node [
    id 442
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 443
    label "asocjacja_gwiazd"
  ]
  node [
    id 444
    label "supergrupa"
  ]
  node [
    id 445
    label "pryncypa&#322;"
  ]
  node [
    id 446
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 447
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 448
    label "cz&#322;owiek"
  ]
  node [
    id 449
    label "kierowa&#263;"
  ]
  node [
    id 450
    label "alkohol"
  ]
  node [
    id 451
    label "&#380;ycie"
  ]
  node [
    id 452
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 453
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 454
    label "sztuka"
  ]
  node [
    id 455
    label "dekiel"
  ]
  node [
    id 456
    label "ro&#347;lina"
  ]
  node [
    id 457
    label "&#347;ci&#281;cie"
  ]
  node [
    id 458
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 459
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 460
    label "&#347;ci&#281;gno"
  ]
  node [
    id 461
    label "noosfera"
  ]
  node [
    id 462
    label "byd&#322;o"
  ]
  node [
    id 463
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 464
    label "makrocefalia"
  ]
  node [
    id 465
    label "ucho"
  ]
  node [
    id 466
    label "g&#243;ra"
  ]
  node [
    id 467
    label "m&#243;zg"
  ]
  node [
    id 468
    label "kierownictwo"
  ]
  node [
    id 469
    label "fryzura"
  ]
  node [
    id 470
    label "umys&#322;"
  ]
  node [
    id 471
    label "cia&#322;o"
  ]
  node [
    id 472
    label "cz&#322;onek"
  ]
  node [
    id 473
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 474
    label "czaszka"
  ]
  node [
    id 475
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 476
    label "przebieg"
  ]
  node [
    id 477
    label "wydarzenie"
  ]
  node [
    id 478
    label "pas"
  ]
  node [
    id 479
    label "swath"
  ]
  node [
    id 480
    label "streak"
  ]
  node [
    id 481
    label "kana&#322;"
  ]
  node [
    id 482
    label "strip"
  ]
  node [
    id 483
    label "ulica"
  ]
  node [
    id 484
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 485
    label "sk&#322;ada&#263;"
  ]
  node [
    id 486
    label "odmawia&#263;"
  ]
  node [
    id 487
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 488
    label "wyra&#380;a&#263;"
  ]
  node [
    id 489
    label "thank"
  ]
  node [
    id 490
    label "etykieta"
  ]
  node [
    id 491
    label "areszt"
  ]
  node [
    id 492
    label "golf"
  ]
  node [
    id 493
    label "faza"
  ]
  node [
    id 494
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 495
    label "l&#261;d"
  ]
  node [
    id 496
    label "depressive_disorder"
  ]
  node [
    id 497
    label "bruzda"
  ]
  node [
    id 498
    label "obszar"
  ]
  node [
    id 499
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 500
    label "Pampa"
  ]
  node [
    id 501
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 502
    label "odlewnictwo"
  ]
  node [
    id 503
    label "za&#322;amanie"
  ]
  node [
    id 504
    label "tomizm"
  ]
  node [
    id 505
    label "akt"
  ]
  node [
    id 506
    label "kalokagatia"
  ]
  node [
    id 507
    label "potencja"
  ]
  node [
    id 508
    label "wordnet"
  ]
  node [
    id 509
    label "wypowiedzenie"
  ]
  node [
    id 510
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 511
    label "s&#322;ownictwo"
  ]
  node [
    id 512
    label "wykrzyknik"
  ]
  node [
    id 513
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 514
    label "pole_semantyczne"
  ]
  node [
    id 515
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 516
    label "pisanie_si&#281;"
  ]
  node [
    id 517
    label "nag&#322;os"
  ]
  node [
    id 518
    label "wyg&#322;os"
  ]
  node [
    id 519
    label "jednostka_leksykalna"
  ]
  node [
    id 520
    label "charakterystyka"
  ]
  node [
    id 521
    label "zaistnie&#263;"
  ]
  node [
    id 522
    label "Osjan"
  ]
  node [
    id 523
    label "kto&#347;"
  ]
  node [
    id 524
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 525
    label "osobowo&#347;&#263;"
  ]
  node [
    id 526
    label "trim"
  ]
  node [
    id 527
    label "poby&#263;"
  ]
  node [
    id 528
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 529
    label "Aspazja"
  ]
  node [
    id 530
    label "kompleksja"
  ]
  node [
    id 531
    label "wytrzyma&#263;"
  ]
  node [
    id 532
    label "budowa"
  ]
  node [
    id 533
    label "pozosta&#263;"
  ]
  node [
    id 534
    label "przedstawienie"
  ]
  node [
    id 535
    label "go&#347;&#263;"
  ]
  node [
    id 536
    label "ilo&#347;&#263;"
  ]
  node [
    id 537
    label "wn&#281;trze"
  ]
  node [
    id 538
    label "Ohio"
  ]
  node [
    id 539
    label "wci&#281;cie"
  ]
  node [
    id 540
    label "Nowy_York"
  ]
  node [
    id 541
    label "warstwa"
  ]
  node [
    id 542
    label "samopoczucie"
  ]
  node [
    id 543
    label "Illinois"
  ]
  node [
    id 544
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 545
    label "state"
  ]
  node [
    id 546
    label "Jukatan"
  ]
  node [
    id 547
    label "Kalifornia"
  ]
  node [
    id 548
    label "Wirginia"
  ]
  node [
    id 549
    label "wektor"
  ]
  node [
    id 550
    label "Teksas"
  ]
  node [
    id 551
    label "Goa"
  ]
  node [
    id 552
    label "Waszyngton"
  ]
  node [
    id 553
    label "Massachusetts"
  ]
  node [
    id 554
    label "Alaska"
  ]
  node [
    id 555
    label "Arakan"
  ]
  node [
    id 556
    label "Hawaje"
  ]
  node [
    id 557
    label "Maryland"
  ]
  node [
    id 558
    label "Michigan"
  ]
  node [
    id 559
    label "Arizona"
  ]
  node [
    id 560
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 561
    label "Georgia"
  ]
  node [
    id 562
    label "poziom"
  ]
  node [
    id 563
    label "Pensylwania"
  ]
  node [
    id 564
    label "Luizjana"
  ]
  node [
    id 565
    label "Nowy_Meksyk"
  ]
  node [
    id 566
    label "Alabama"
  ]
  node [
    id 567
    label "Kansas"
  ]
  node [
    id 568
    label "Oregon"
  ]
  node [
    id 569
    label "Floryda"
  ]
  node [
    id 570
    label "Oklahoma"
  ]
  node [
    id 571
    label "jednostka_administracyjna"
  ]
  node [
    id 572
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 573
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 574
    label "vessel"
  ]
  node [
    id 575
    label "statki"
  ]
  node [
    id 576
    label "rewaskularyzacja"
  ]
  node [
    id 577
    label "ceramika"
  ]
  node [
    id 578
    label "drewno"
  ]
  node [
    id 579
    label "przew&#243;d"
  ]
  node [
    id 580
    label "unaczyni&#263;"
  ]
  node [
    id 581
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 582
    label "receptacle"
  ]
  node [
    id 583
    label "co&#347;"
  ]
  node [
    id 584
    label "budynek"
  ]
  node [
    id 585
    label "thing"
  ]
  node [
    id 586
    label "program"
  ]
  node [
    id 587
    label "rzecz"
  ]
  node [
    id 588
    label "strona"
  ]
  node [
    id 589
    label "posiada&#263;"
  ]
  node [
    id 590
    label "potencja&#322;"
  ]
  node [
    id 591
    label "zapomnienie"
  ]
  node [
    id 592
    label "zapomina&#263;"
  ]
  node [
    id 593
    label "zapominanie"
  ]
  node [
    id 594
    label "ability"
  ]
  node [
    id 595
    label "obliczeniowo"
  ]
  node [
    id 596
    label "zapomnie&#263;"
  ]
  node [
    id 597
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 598
    label "zachowanie"
  ]
  node [
    id 599
    label "kultura_duchowa"
  ]
  node [
    id 600
    label "kultura"
  ]
  node [
    id 601
    label "ceremony"
  ]
  node [
    id 602
    label "sformu&#322;owanie"
  ]
  node [
    id 603
    label "zdarzenie_si&#281;"
  ]
  node [
    id 604
    label "poinformowanie"
  ]
  node [
    id 605
    label "wording"
  ]
  node [
    id 606
    label "kompozycja"
  ]
  node [
    id 607
    label "oznaczenie"
  ]
  node [
    id 608
    label "znak_j&#281;zykowy"
  ]
  node [
    id 609
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 610
    label "ozdobnik"
  ]
  node [
    id 611
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 612
    label "grupa_imienna"
  ]
  node [
    id 613
    label "term"
  ]
  node [
    id 614
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 615
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 616
    label "ujawnienie"
  ]
  node [
    id 617
    label "affirmation"
  ]
  node [
    id 618
    label "zapisanie"
  ]
  node [
    id 619
    label "rzucenie"
  ]
  node [
    id 620
    label "zapis"
  ]
  node [
    id 621
    label "figure"
  ]
  node [
    id 622
    label "spos&#243;b"
  ]
  node [
    id 623
    label "mildew"
  ]
  node [
    id 624
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 625
    label "ideal"
  ]
  node [
    id 626
    label "rule"
  ]
  node [
    id 627
    label "ruch"
  ]
  node [
    id 628
    label "dekal"
  ]
  node [
    id 629
    label "motyw"
  ]
  node [
    id 630
    label "projekt"
  ]
  node [
    id 631
    label "m&#322;ot"
  ]
  node [
    id 632
    label "drzewo"
  ]
  node [
    id 633
    label "pr&#243;ba"
  ]
  node [
    id 634
    label "attribute"
  ]
  node [
    id 635
    label "marka"
  ]
  node [
    id 636
    label "mechanika"
  ]
  node [
    id 637
    label "o&#347;"
  ]
  node [
    id 638
    label "usenet"
  ]
  node [
    id 639
    label "rozprz&#261;c"
  ]
  node [
    id 640
    label "cybernetyk"
  ]
  node [
    id 641
    label "podsystem"
  ]
  node [
    id 642
    label "system"
  ]
  node [
    id 643
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 644
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 645
    label "sk&#322;ad"
  ]
  node [
    id 646
    label "systemat"
  ]
  node [
    id 647
    label "konstrukcja"
  ]
  node [
    id 648
    label "model"
  ]
  node [
    id 649
    label "jig"
  ]
  node [
    id 650
    label "drabina_analgetyczna"
  ]
  node [
    id 651
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 652
    label "C"
  ]
  node [
    id 653
    label "D"
  ]
  node [
    id 654
    label "exemplar"
  ]
  node [
    id 655
    label "wyraz_pochodny"
  ]
  node [
    id 656
    label "zboczenie"
  ]
  node [
    id 657
    label "om&#243;wienie"
  ]
  node [
    id 658
    label "omawia&#263;"
  ]
  node [
    id 659
    label "fraza"
  ]
  node [
    id 660
    label "entity"
  ]
  node [
    id 661
    label "forum"
  ]
  node [
    id 662
    label "topik"
  ]
  node [
    id 663
    label "tematyka"
  ]
  node [
    id 664
    label "w&#261;tek"
  ]
  node [
    id 665
    label "zbaczanie"
  ]
  node [
    id 666
    label "om&#243;wi&#263;"
  ]
  node [
    id 667
    label "omawianie"
  ]
  node [
    id 668
    label "melodia"
  ]
  node [
    id 669
    label "otoczka"
  ]
  node [
    id 670
    label "istota"
  ]
  node [
    id 671
    label "zbacza&#263;"
  ]
  node [
    id 672
    label "zboczy&#263;"
  ]
  node [
    id 673
    label "morpheme"
  ]
  node [
    id 674
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 675
    label "figura_stylistyczna"
  ]
  node [
    id 676
    label "decoration"
  ]
  node [
    id 677
    label "dekoracja"
  ]
  node [
    id 678
    label "reaktor_j&#261;drowy"
  ]
  node [
    id 679
    label "magnes"
  ]
  node [
    id 680
    label "spowalniacz"
  ]
  node [
    id 681
    label "transformator"
  ]
  node [
    id 682
    label "mi&#281;kisz"
  ]
  node [
    id 683
    label "marrow"
  ]
  node [
    id 684
    label "pocisk"
  ]
  node [
    id 685
    label "pr&#281;t_kontrolny"
  ]
  node [
    id 686
    label "procesor"
  ]
  node [
    id 687
    label "ch&#322;odziwo"
  ]
  node [
    id 688
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 689
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 690
    label "surowiak"
  ]
  node [
    id 691
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 692
    label "core"
  ]
  node [
    id 693
    label "kondycja"
  ]
  node [
    id 694
    label "polecenie"
  ]
  node [
    id 695
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 696
    label "capability"
  ]
  node [
    id 697
    label "prawo"
  ]
  node [
    id 698
    label "Bund"
  ]
  node [
    id 699
    label "Mazowsze"
  ]
  node [
    id 700
    label "PPR"
  ]
  node [
    id 701
    label "Jakobici"
  ]
  node [
    id 702
    label "zesp&#243;&#322;"
  ]
  node [
    id 703
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 704
    label "SLD"
  ]
  node [
    id 705
    label "zespolik"
  ]
  node [
    id 706
    label "Razem"
  ]
  node [
    id 707
    label "PiS"
  ]
  node [
    id 708
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 709
    label "partia"
  ]
  node [
    id 710
    label "Kuomintang"
  ]
  node [
    id 711
    label "ZSL"
  ]
  node [
    id 712
    label "proces"
  ]
  node [
    id 713
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 714
    label "rugby"
  ]
  node [
    id 715
    label "AWS"
  ]
  node [
    id 716
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 717
    label "blok"
  ]
  node [
    id 718
    label "PO"
  ]
  node [
    id 719
    label "si&#322;a"
  ]
  node [
    id 720
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 721
    label "Federali&#347;ci"
  ]
  node [
    id 722
    label "PSL"
  ]
  node [
    id 723
    label "Wigowie"
  ]
  node [
    id 724
    label "ZChN"
  ]
  node [
    id 725
    label "egzekutywa"
  ]
  node [
    id 726
    label "rocznik"
  ]
  node [
    id 727
    label "The_Beatles"
  ]
  node [
    id 728
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 729
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 730
    label "unit"
  ]
  node [
    id 731
    label "Depeche_Mode"
  ]
  node [
    id 732
    label "ustnie"
  ]
  node [
    id 733
    label "egzamin"
  ]
  node [
    id 734
    label "oblewanie"
  ]
  node [
    id 735
    label "sesja_egzaminacyjna"
  ]
  node [
    id 736
    label "oblewa&#263;"
  ]
  node [
    id 737
    label "praca_pisemna"
  ]
  node [
    id 738
    label "sprawdzian"
  ]
  node [
    id 739
    label "magiel"
  ]
  node [
    id 740
    label "arkusz"
  ]
  node [
    id 741
    label "examination"
  ]
  node [
    id 742
    label "wizualnie"
  ]
  node [
    id 743
    label "wzrokowy"
  ]
  node [
    id 744
    label "wzrokowo"
  ]
  node [
    id 745
    label "dostrzegalnie"
  ]
  node [
    id 746
    label "&#347;wiadectwo"
  ]
  node [
    id 747
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 748
    label "parafa"
  ]
  node [
    id 749
    label "plik"
  ]
  node [
    id 750
    label "raport&#243;wka"
  ]
  node [
    id 751
    label "record"
  ]
  node [
    id 752
    label "fascyku&#322;"
  ]
  node [
    id 753
    label "dokumentacja"
  ]
  node [
    id 754
    label "registratura"
  ]
  node [
    id 755
    label "artyku&#322;"
  ]
  node [
    id 756
    label "writing"
  ]
  node [
    id 757
    label "sygnatariusz"
  ]
  node [
    id 758
    label "dow&#243;d"
  ]
  node [
    id 759
    label "o&#347;wiadczenie"
  ]
  node [
    id 760
    label "za&#347;wiadczenie"
  ]
  node [
    id 761
    label "certificate"
  ]
  node [
    id 762
    label "promocja"
  ]
  node [
    id 763
    label "wpis"
  ]
  node [
    id 764
    label "normalizacja"
  ]
  node [
    id 765
    label "part"
  ]
  node [
    id 766
    label "element_anatomiczny"
  ]
  node [
    id 767
    label "p&#322;&#243;d"
  ]
  node [
    id 768
    label "work"
  ]
  node [
    id 769
    label "rezultat"
  ]
  node [
    id 770
    label "podkatalog"
  ]
  node [
    id 771
    label "nadpisa&#263;"
  ]
  node [
    id 772
    label "nadpisanie"
  ]
  node [
    id 773
    label "bundle"
  ]
  node [
    id 774
    label "folder"
  ]
  node [
    id 775
    label "nadpisywanie"
  ]
  node [
    id 776
    label "paczka"
  ]
  node [
    id 777
    label "nadpisywa&#263;"
  ]
  node [
    id 778
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 779
    label "przedstawiciel"
  ]
  node [
    id 780
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 781
    label "biuro"
  ]
  node [
    id 782
    label "register"
  ]
  node [
    id 783
    label "ekscerpcja"
  ]
  node [
    id 784
    label "materia&#322;"
  ]
  node [
    id 785
    label "operat"
  ]
  node [
    id 786
    label "kosztorys"
  ]
  node [
    id 787
    label "torba"
  ]
  node [
    id 788
    label "wydanie"
  ]
  node [
    id 789
    label "paraph"
  ]
  node [
    id 790
    label "podpis"
  ]
  node [
    id 791
    label "prawda"
  ]
  node [
    id 792
    label "nag&#322;&#243;wek"
  ]
  node [
    id 793
    label "szkic"
  ]
  node [
    id 794
    label "line"
  ]
  node [
    id 795
    label "fragment"
  ]
  node [
    id 796
    label "wyr&#243;b"
  ]
  node [
    id 797
    label "rodzajnik"
  ]
  node [
    id 798
    label "towar"
  ]
  node [
    id 799
    label "paragraf"
  ]
  node [
    id 800
    label "jaki&#347;"
  ]
  node [
    id 801
    label "przyzwoity"
  ]
  node [
    id 802
    label "ciekawy"
  ]
  node [
    id 803
    label "jako&#347;"
  ]
  node [
    id 804
    label "jako_tako"
  ]
  node [
    id 805
    label "niez&#322;y"
  ]
  node [
    id 806
    label "dziwny"
  ]
  node [
    id 807
    label "charakterystyczny"
  ]
  node [
    id 808
    label "kolejny"
  ]
  node [
    id 809
    label "osobno"
  ]
  node [
    id 810
    label "r&#243;&#380;ny"
  ]
  node [
    id 811
    label "inszy"
  ]
  node [
    id 812
    label "inaczej"
  ]
  node [
    id 813
    label "odr&#281;bny"
  ]
  node [
    id 814
    label "nast&#281;pnie"
  ]
  node [
    id 815
    label "nastopny"
  ]
  node [
    id 816
    label "kolejno"
  ]
  node [
    id 817
    label "kt&#243;ry&#347;"
  ]
  node [
    id 818
    label "r&#243;&#380;nie"
  ]
  node [
    id 819
    label "niestandardowo"
  ]
  node [
    id 820
    label "individually"
  ]
  node [
    id 821
    label "udzielnie"
  ]
  node [
    id 822
    label "osobnie"
  ]
  node [
    id 823
    label "odr&#281;bnie"
  ]
  node [
    id 824
    label "osobny"
  ]
  node [
    id 825
    label "sprz&#281;cior"
  ]
  node [
    id 826
    label "penis"
  ]
  node [
    id 827
    label "kolekcja"
  ]
  node [
    id 828
    label "furniture"
  ]
  node [
    id 829
    label "sprz&#281;cik"
  ]
  node [
    id 830
    label "equipment"
  ]
  node [
    id 831
    label "linia"
  ]
  node [
    id 832
    label "stage_set"
  ]
  node [
    id 833
    label "collection"
  ]
  node [
    id 834
    label "album"
  ]
  node [
    id 835
    label "egzemplarz"
  ]
  node [
    id 836
    label "series"
  ]
  node [
    id 837
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 838
    label "uprawianie"
  ]
  node [
    id 839
    label "praca_rolnicza"
  ]
  node [
    id 840
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 841
    label "pakiet_klimatyczny"
  ]
  node [
    id 842
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 843
    label "sum"
  ]
  node [
    id 844
    label "sponiewieranie"
  ]
  node [
    id 845
    label "discipline"
  ]
  node [
    id 846
    label "kr&#261;&#380;enie"
  ]
  node [
    id 847
    label "robienie"
  ]
  node [
    id 848
    label "sponiewiera&#263;"
  ]
  node [
    id 849
    label "element"
  ]
  node [
    id 850
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 851
    label "program_nauczania"
  ]
  node [
    id 852
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 853
    label "ptaszek"
  ]
  node [
    id 854
    label "przyrodzenie"
  ]
  node [
    id 855
    label "shaft"
  ]
  node [
    id 856
    label "fiut"
  ]
  node [
    id 857
    label "technika"
  ]
  node [
    id 858
    label "mikrotechnologia"
  ]
  node [
    id 859
    label "technologia_nieorganiczna"
  ]
  node [
    id 860
    label "engineering"
  ]
  node [
    id 861
    label "biotechnologia"
  ]
  node [
    id 862
    label "narz&#281;dzie"
  ]
  node [
    id 863
    label "tryb"
  ]
  node [
    id 864
    label "nature"
  ]
  node [
    id 865
    label "nauka"
  ]
  node [
    id 866
    label "in&#380;ynieria_genetyczna"
  ]
  node [
    id 867
    label "bioin&#380;ynieria"
  ]
  node [
    id 868
    label "telekomunikacja"
  ]
  node [
    id 869
    label "cywilizacja"
  ]
  node [
    id 870
    label "sprawno&#347;&#263;"
  ]
  node [
    id 871
    label "fotowoltaika"
  ]
  node [
    id 872
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 873
    label "teletechnika"
  ]
  node [
    id 874
    label "mechanika_precyzyjna"
  ]
  node [
    id 875
    label "rzeczpospolita"
  ]
  node [
    id 876
    label "polski"
  ]
  node [
    id 877
    label "agencja"
  ]
  node [
    id 878
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 879
    label "wewn&#281;trzny"
  ]
  node [
    id 880
    label "s&#322;u&#380;ba"
  ]
  node [
    id 881
    label "kontrwywiad"
  ]
  node [
    id 882
    label "wojskowy"
  ]
  node [
    id 883
    label "zjednoczy&#263;"
  ]
  node [
    id 884
    label "ameryk"
  ]
  node [
    id 885
    label "departament"
  ]
  node [
    id 886
    label "obrona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 732
  ]
  edge [
    source 5
    target 733
  ]
  edge [
    source 5
    target 734
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 735
  ]
  edge [
    source 5
    target 736
  ]
  edge [
    source 5
    target 737
  ]
  edge [
    source 5
    target 738
  ]
  edge [
    source 5
    target 739
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 740
  ]
  edge [
    source 5
    target 741
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 14
    target 857
  ]
  edge [
    source 14
    target 858
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 366
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 248
    target 883
  ]
  edge [
    source 248
    target 884
  ]
  edge [
    source 875
    target 876
  ]
  edge [
    source 877
    target 878
  ]
  edge [
    source 877
    target 879
  ]
  edge [
    source 878
    target 879
  ]
  edge [
    source 880
    target 881
  ]
  edge [
    source 880
    target 882
  ]
  edge [
    source 881
    target 882
  ]
  edge [
    source 883
    target 884
  ]
  edge [
    source 885
    target 886
  ]
]
