graph [
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "&#347;rodowisko"
    origin "text"
  ]
  node [
    id 4
    label "g&#322;&#243;wny"
    origin "text"
  ]
  node [
    id 5
    label "inspektor"
    origin "text"
  ]
  node [
    id 6
    label "ochrona"
    origin "text"
  ]
  node [
    id 7
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 8
    label "poz"
    origin "text"
  ]
  node [
    id 9
    label "program_informacyjny"
  ]
  node [
    id 10
    label "journal"
  ]
  node [
    id 11
    label "diariusz"
  ]
  node [
    id 12
    label "spis"
  ]
  node [
    id 13
    label "ksi&#281;ga"
  ]
  node [
    id 14
    label "sheet"
  ]
  node [
    id 15
    label "pami&#281;tnik"
  ]
  node [
    id 16
    label "gazeta"
  ]
  node [
    id 17
    label "tytu&#322;"
  ]
  node [
    id 18
    label "redakcja"
  ]
  node [
    id 19
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 20
    label "czasopismo"
  ]
  node [
    id 21
    label "prasa"
  ]
  node [
    id 22
    label "rozdzia&#322;"
  ]
  node [
    id 23
    label "pismo"
  ]
  node [
    id 24
    label "Ewangelia"
  ]
  node [
    id 25
    label "book"
  ]
  node [
    id 26
    label "dokument"
  ]
  node [
    id 27
    label "tome"
  ]
  node [
    id 28
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 29
    label "pami&#261;tka"
  ]
  node [
    id 30
    label "notes"
  ]
  node [
    id 31
    label "zapiski"
  ]
  node [
    id 32
    label "raptularz"
  ]
  node [
    id 33
    label "album"
  ]
  node [
    id 34
    label "utw&#243;r_epicki"
  ]
  node [
    id 35
    label "zbi&#243;r"
  ]
  node [
    id 36
    label "catalog"
  ]
  node [
    id 37
    label "pozycja"
  ]
  node [
    id 38
    label "akt"
  ]
  node [
    id 39
    label "tekst"
  ]
  node [
    id 40
    label "sumariusz"
  ]
  node [
    id 41
    label "stock"
  ]
  node [
    id 42
    label "figurowa&#263;"
  ]
  node [
    id 43
    label "czynno&#347;&#263;"
  ]
  node [
    id 44
    label "wyliczanka"
  ]
  node [
    id 45
    label "oficjalny"
  ]
  node [
    id 46
    label "urz&#281;dowo"
  ]
  node [
    id 47
    label "formalny"
  ]
  node [
    id 48
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 49
    label "formalizowanie"
  ]
  node [
    id 50
    label "formalnie"
  ]
  node [
    id 51
    label "oficjalnie"
  ]
  node [
    id 52
    label "jawny"
  ]
  node [
    id 53
    label "legalny"
  ]
  node [
    id 54
    label "sformalizowanie"
  ]
  node [
    id 55
    label "pozorny"
  ]
  node [
    id 56
    label "kompletny"
  ]
  node [
    id 57
    label "prawdziwy"
  ]
  node [
    id 58
    label "prawomocny"
  ]
  node [
    id 59
    label "dostojnik"
  ]
  node [
    id 60
    label "Goebbels"
  ]
  node [
    id 61
    label "Sto&#322;ypin"
  ]
  node [
    id 62
    label "rz&#261;d"
  ]
  node [
    id 63
    label "przybli&#380;enie"
  ]
  node [
    id 64
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 65
    label "kategoria"
  ]
  node [
    id 66
    label "szpaler"
  ]
  node [
    id 67
    label "lon&#380;a"
  ]
  node [
    id 68
    label "uporz&#261;dkowanie"
  ]
  node [
    id 69
    label "egzekutywa"
  ]
  node [
    id 70
    label "jednostka_systematyczna"
  ]
  node [
    id 71
    label "instytucja"
  ]
  node [
    id 72
    label "premier"
  ]
  node [
    id 73
    label "Londyn"
  ]
  node [
    id 74
    label "gabinet_cieni"
  ]
  node [
    id 75
    label "gromada"
  ]
  node [
    id 76
    label "number"
  ]
  node [
    id 77
    label "Konsulat"
  ]
  node [
    id 78
    label "tract"
  ]
  node [
    id 79
    label "klasa"
  ]
  node [
    id 80
    label "w&#322;adza"
  ]
  node [
    id 81
    label "urz&#281;dnik"
  ]
  node [
    id 82
    label "notabl"
  ]
  node [
    id 83
    label "oficja&#322;"
  ]
  node [
    id 84
    label "class"
  ]
  node [
    id 85
    label "zesp&#243;&#322;"
  ]
  node [
    id 86
    label "obiekt_naturalny"
  ]
  node [
    id 87
    label "otoczenie"
  ]
  node [
    id 88
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 89
    label "environment"
  ]
  node [
    id 90
    label "rzecz"
  ]
  node [
    id 91
    label "huczek"
  ]
  node [
    id 92
    label "ekosystem"
  ]
  node [
    id 93
    label "wszechstworzenie"
  ]
  node [
    id 94
    label "grupa"
  ]
  node [
    id 95
    label "woda"
  ]
  node [
    id 96
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 97
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 98
    label "teren"
  ]
  node [
    id 99
    label "mikrokosmos"
  ]
  node [
    id 100
    label "stw&#243;r"
  ]
  node [
    id 101
    label "warunki"
  ]
  node [
    id 102
    label "Ziemia"
  ]
  node [
    id 103
    label "fauna"
  ]
  node [
    id 104
    label "biota"
  ]
  node [
    id 105
    label "status"
  ]
  node [
    id 106
    label "sytuacja"
  ]
  node [
    id 107
    label "okrycie"
  ]
  node [
    id 108
    label "spowodowanie"
  ]
  node [
    id 109
    label "background"
  ]
  node [
    id 110
    label "zdarzenie_si&#281;"
  ]
  node [
    id 111
    label "crack"
  ]
  node [
    id 112
    label "cortege"
  ]
  node [
    id 113
    label "okolica"
  ]
  node [
    id 114
    label "zrobienie"
  ]
  node [
    id 115
    label "odm&#322;adzanie"
  ]
  node [
    id 116
    label "liga"
  ]
  node [
    id 117
    label "asymilowanie"
  ]
  node [
    id 118
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 119
    label "asymilowa&#263;"
  ]
  node [
    id 120
    label "egzemplarz"
  ]
  node [
    id 121
    label "Entuzjastki"
  ]
  node [
    id 122
    label "kompozycja"
  ]
  node [
    id 123
    label "Terranie"
  ]
  node [
    id 124
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 125
    label "category"
  ]
  node [
    id 126
    label "pakiet_klimatyczny"
  ]
  node [
    id 127
    label "oddzia&#322;"
  ]
  node [
    id 128
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 129
    label "cz&#261;steczka"
  ]
  node [
    id 130
    label "stage_set"
  ]
  node [
    id 131
    label "type"
  ]
  node [
    id 132
    label "specgrupa"
  ]
  node [
    id 133
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 134
    label "&#346;wietliki"
  ]
  node [
    id 135
    label "odm&#322;odzenie"
  ]
  node [
    id 136
    label "Eurogrupa"
  ]
  node [
    id 137
    label "odm&#322;adza&#263;"
  ]
  node [
    id 138
    label "formacja_geologiczna"
  ]
  node [
    id 139
    label "harcerze_starsi"
  ]
  node [
    id 140
    label "Mazowsze"
  ]
  node [
    id 141
    label "whole"
  ]
  node [
    id 142
    label "skupienie"
  ]
  node [
    id 143
    label "The_Beatles"
  ]
  node [
    id 144
    label "zabudowania"
  ]
  node [
    id 145
    label "group"
  ]
  node [
    id 146
    label "zespolik"
  ]
  node [
    id 147
    label "schorzenie"
  ]
  node [
    id 148
    label "ro&#347;lina"
  ]
  node [
    id 149
    label "Depeche_Mode"
  ]
  node [
    id 150
    label "batch"
  ]
  node [
    id 151
    label "rura"
  ]
  node [
    id 152
    label "grzebiuszka"
  ]
  node [
    id 153
    label "smok_wawelski"
  ]
  node [
    id 154
    label "niecz&#322;owiek"
  ]
  node [
    id 155
    label "monster"
  ]
  node [
    id 156
    label "przyroda"
  ]
  node [
    id 157
    label "istota_&#380;ywa"
  ]
  node [
    id 158
    label "potw&#243;r"
  ]
  node [
    id 159
    label "istota_fantastyczna"
  ]
  node [
    id 160
    label "object"
  ]
  node [
    id 161
    label "przedmiot"
  ]
  node [
    id 162
    label "temat"
  ]
  node [
    id 163
    label "wpadni&#281;cie"
  ]
  node [
    id 164
    label "mienie"
  ]
  node [
    id 165
    label "istota"
  ]
  node [
    id 166
    label "obiekt"
  ]
  node [
    id 167
    label "kultura"
  ]
  node [
    id 168
    label "wpa&#347;&#263;"
  ]
  node [
    id 169
    label "wpadanie"
  ]
  node [
    id 170
    label "wpada&#263;"
  ]
  node [
    id 171
    label "performance"
  ]
  node [
    id 172
    label "sztuka"
  ]
  node [
    id 173
    label "wymiar"
  ]
  node [
    id 174
    label "zakres"
  ]
  node [
    id 175
    label "kontekst"
  ]
  node [
    id 176
    label "miejsce_pracy"
  ]
  node [
    id 177
    label "nation"
  ]
  node [
    id 178
    label "krajobraz"
  ]
  node [
    id 179
    label "obszar"
  ]
  node [
    id 180
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 181
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 182
    label "iglak"
  ]
  node [
    id 183
    label "cyprysowate"
  ]
  node [
    id 184
    label "biom"
  ]
  node [
    id 185
    label "szata_ro&#347;linna"
  ]
  node [
    id 186
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 187
    label "formacja_ro&#347;linna"
  ]
  node [
    id 188
    label "zielono&#347;&#263;"
  ]
  node [
    id 189
    label "pi&#281;tro"
  ]
  node [
    id 190
    label "plant"
  ]
  node [
    id 191
    label "geosystem"
  ]
  node [
    id 192
    label "dotleni&#263;"
  ]
  node [
    id 193
    label "spi&#281;trza&#263;"
  ]
  node [
    id 194
    label "spi&#281;trzenie"
  ]
  node [
    id 195
    label "utylizator"
  ]
  node [
    id 196
    label "p&#322;ycizna"
  ]
  node [
    id 197
    label "nabranie"
  ]
  node [
    id 198
    label "Waruna"
  ]
  node [
    id 199
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 200
    label "przybieranie"
  ]
  node [
    id 201
    label "uci&#261;g"
  ]
  node [
    id 202
    label "bombast"
  ]
  node [
    id 203
    label "fala"
  ]
  node [
    id 204
    label "kryptodepresja"
  ]
  node [
    id 205
    label "water"
  ]
  node [
    id 206
    label "wysi&#281;k"
  ]
  node [
    id 207
    label "pustka"
  ]
  node [
    id 208
    label "ciecz"
  ]
  node [
    id 209
    label "przybrze&#380;e"
  ]
  node [
    id 210
    label "nap&#243;j"
  ]
  node [
    id 211
    label "spi&#281;trzanie"
  ]
  node [
    id 212
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 213
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 214
    label "bicie"
  ]
  node [
    id 215
    label "klarownik"
  ]
  node [
    id 216
    label "chlastanie"
  ]
  node [
    id 217
    label "woda_s&#322;odka"
  ]
  node [
    id 218
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 219
    label "nabra&#263;"
  ]
  node [
    id 220
    label "chlasta&#263;"
  ]
  node [
    id 221
    label "uj&#281;cie_wody"
  ]
  node [
    id 222
    label "zrzut"
  ]
  node [
    id 223
    label "wypowied&#378;"
  ]
  node [
    id 224
    label "wodnik"
  ]
  node [
    id 225
    label "pojazd"
  ]
  node [
    id 226
    label "l&#243;d"
  ]
  node [
    id 227
    label "wybrze&#380;e"
  ]
  node [
    id 228
    label "deklamacja"
  ]
  node [
    id 229
    label "tlenek"
  ]
  node [
    id 230
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 231
    label "biotop"
  ]
  node [
    id 232
    label "biocenoza"
  ]
  node [
    id 233
    label "cz&#322;owiek"
  ]
  node [
    id 234
    label "atom"
  ]
  node [
    id 235
    label "odbicie"
  ]
  node [
    id 236
    label "kosmos"
  ]
  node [
    id 237
    label "miniatura"
  ]
  node [
    id 238
    label "awifauna"
  ]
  node [
    id 239
    label "ichtiofauna"
  ]
  node [
    id 240
    label "Stary_&#346;wiat"
  ]
  node [
    id 241
    label "p&#243;&#322;noc"
  ]
  node [
    id 242
    label "geosfera"
  ]
  node [
    id 243
    label "po&#322;udnie"
  ]
  node [
    id 244
    label "rze&#378;ba"
  ]
  node [
    id 245
    label "morze"
  ]
  node [
    id 246
    label "hydrosfera"
  ]
  node [
    id 247
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 248
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 249
    label "geotermia"
  ]
  node [
    id 250
    label "ozonosfera"
  ]
  node [
    id 251
    label "biosfera"
  ]
  node [
    id 252
    label "magnetosfera"
  ]
  node [
    id 253
    label "Nowy_&#346;wiat"
  ]
  node [
    id 254
    label "biegun"
  ]
  node [
    id 255
    label "litosfera"
  ]
  node [
    id 256
    label "p&#243;&#322;kula"
  ]
  node [
    id 257
    label "barysfera"
  ]
  node [
    id 258
    label "atmosfera"
  ]
  node [
    id 259
    label "geoida"
  ]
  node [
    id 260
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 261
    label "najwa&#380;niejszy"
  ]
  node [
    id 262
    label "g&#322;&#243;wnie"
  ]
  node [
    id 263
    label "oficer_policji"
  ]
  node [
    id 264
    label "kontroler"
  ]
  node [
    id 265
    label "pracownik"
  ]
  node [
    id 266
    label "urz&#261;dzenie"
  ]
  node [
    id 267
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 268
    label "pragmatyka"
  ]
  node [
    id 269
    label "formacja"
  ]
  node [
    id 270
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 271
    label "obstawianie"
  ]
  node [
    id 272
    label "obstawienie"
  ]
  node [
    id 273
    label "tarcza"
  ]
  node [
    id 274
    label "ubezpieczenie"
  ]
  node [
    id 275
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 276
    label "transportacja"
  ]
  node [
    id 277
    label "obstawia&#263;"
  ]
  node [
    id 278
    label "borowiec"
  ]
  node [
    id 279
    label "chemical_bond"
  ]
  node [
    id 280
    label "co&#347;"
  ]
  node [
    id 281
    label "budynek"
  ]
  node [
    id 282
    label "thing"
  ]
  node [
    id 283
    label "poj&#281;cie"
  ]
  node [
    id 284
    label "program"
  ]
  node [
    id 285
    label "strona"
  ]
  node [
    id 286
    label "Bund"
  ]
  node [
    id 287
    label "PPR"
  ]
  node [
    id 288
    label "Jakobici"
  ]
  node [
    id 289
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 290
    label "leksem"
  ]
  node [
    id 291
    label "SLD"
  ]
  node [
    id 292
    label "Razem"
  ]
  node [
    id 293
    label "PiS"
  ]
  node [
    id 294
    label "zjawisko"
  ]
  node [
    id 295
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 296
    label "partia"
  ]
  node [
    id 297
    label "Kuomintang"
  ]
  node [
    id 298
    label "ZSL"
  ]
  node [
    id 299
    label "szko&#322;a"
  ]
  node [
    id 300
    label "jednostka"
  ]
  node [
    id 301
    label "proces"
  ]
  node [
    id 302
    label "organizacja"
  ]
  node [
    id 303
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 304
    label "rugby"
  ]
  node [
    id 305
    label "AWS"
  ]
  node [
    id 306
    label "posta&#263;"
  ]
  node [
    id 307
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 308
    label "blok"
  ]
  node [
    id 309
    label "PO"
  ]
  node [
    id 310
    label "si&#322;a"
  ]
  node [
    id 311
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 312
    label "Federali&#347;ci"
  ]
  node [
    id 313
    label "PSL"
  ]
  node [
    id 314
    label "wojsko"
  ]
  node [
    id 315
    label "Wigowie"
  ]
  node [
    id 316
    label "ZChN"
  ]
  node [
    id 317
    label "rocznik"
  ]
  node [
    id 318
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 319
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 320
    label "unit"
  ]
  node [
    id 321
    label "forma"
  ]
  node [
    id 322
    label "naszywka"
  ]
  node [
    id 323
    label "kszta&#322;t"
  ]
  node [
    id 324
    label "wskaz&#243;wka"
  ]
  node [
    id 325
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 326
    label "obro&#324;ca"
  ]
  node [
    id 327
    label "bro&#324;_ochronna"
  ]
  node [
    id 328
    label "odznaka"
  ]
  node [
    id 329
    label "bro&#324;"
  ]
  node [
    id 330
    label "denture"
  ]
  node [
    id 331
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 332
    label "telefon"
  ]
  node [
    id 333
    label "or&#281;&#380;"
  ]
  node [
    id 334
    label "target"
  ]
  node [
    id 335
    label "cel"
  ]
  node [
    id 336
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 337
    label "maszyna"
  ]
  node [
    id 338
    label "ucze&#324;"
  ]
  node [
    id 339
    label "powierzchnia"
  ]
  node [
    id 340
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 341
    label "tablica"
  ]
  node [
    id 342
    label "op&#322;ata"
  ]
  node [
    id 343
    label "ubezpieczalnia"
  ]
  node [
    id 344
    label "insurance"
  ]
  node [
    id 345
    label "cover"
  ]
  node [
    id 346
    label "suma_ubezpieczenia"
  ]
  node [
    id 347
    label "screen"
  ]
  node [
    id 348
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 349
    label "franszyza"
  ]
  node [
    id 350
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 351
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 352
    label "zapewnienie"
  ]
  node [
    id 353
    label "przyznanie"
  ]
  node [
    id 354
    label "umowa"
  ]
  node [
    id 355
    label "uchronienie"
  ]
  node [
    id 356
    label "transport"
  ]
  node [
    id 357
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 358
    label "Irish_pound"
  ]
  node [
    id 359
    label "bramka"
  ]
  node [
    id 360
    label "wytypowanie"
  ]
  node [
    id 361
    label "za&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 362
    label "otaczanie"
  ]
  node [
    id 363
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 364
    label "os&#322;anianie"
  ]
  node [
    id 365
    label "typowanie"
  ]
  node [
    id 366
    label "ubezpieczanie"
  ]
  node [
    id 367
    label "ubezpiecza&#263;"
  ]
  node [
    id 368
    label "venture"
  ]
  node [
    id 369
    label "przewidywa&#263;"
  ]
  node [
    id 370
    label "zapewnia&#263;"
  ]
  node [
    id 371
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 372
    label "typowa&#263;"
  ]
  node [
    id 373
    label "zastawia&#263;"
  ]
  node [
    id 374
    label "budowa&#263;"
  ]
  node [
    id 375
    label "zajmowa&#263;"
  ]
  node [
    id 376
    label "obejmowa&#263;"
  ]
  node [
    id 377
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 378
    label "os&#322;ania&#263;"
  ]
  node [
    id 379
    label "otacza&#263;"
  ]
  node [
    id 380
    label "broni&#263;"
  ]
  node [
    id 381
    label "powierza&#263;"
  ]
  node [
    id 382
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 383
    label "frame"
  ]
  node [
    id 384
    label "wysy&#322;a&#263;"
  ]
  node [
    id 385
    label "typ"
  ]
  node [
    id 386
    label "borowce"
  ]
  node [
    id 387
    label "ochroniarz"
  ]
  node [
    id 388
    label "duch"
  ]
  node [
    id 389
    label "borowik"
  ]
  node [
    id 390
    label "nietoperz"
  ]
  node [
    id 391
    label "kozio&#322;ek"
  ]
  node [
    id 392
    label "owado&#380;erca"
  ]
  node [
    id 393
    label "funkcjonariusz"
  ]
  node [
    id 394
    label "BOR"
  ]
  node [
    id 395
    label "mroczkowate"
  ]
  node [
    id 396
    label "pierwiastek"
  ]
  node [
    id 397
    label "Barb&#243;rka"
  ]
  node [
    id 398
    label "miesi&#261;c"
  ]
  node [
    id 399
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 400
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 401
    label "Sylwester"
  ]
  node [
    id 402
    label "tydzie&#324;"
  ]
  node [
    id 403
    label "miech"
  ]
  node [
    id 404
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 405
    label "czas"
  ]
  node [
    id 406
    label "rok"
  ]
  node [
    id 407
    label "kalendy"
  ]
  node [
    id 408
    label "g&#243;rnik"
  ]
  node [
    id 409
    label "comber"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
]
