graph [
  node [
    id 0
    label "wtem"
    origin "text"
  ]
  node [
    id 1
    label "spojrze&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przysta&#324;"
    origin "text"
  ]
  node [
    id 3
    label "nedia"
    origin "text"
  ]
  node [
    id 4
    label "zawo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 5
    label "niespodziewanie"
  ]
  node [
    id 6
    label "raptownie"
  ]
  node [
    id 7
    label "raptowny"
  ]
  node [
    id 8
    label "szybko"
  ]
  node [
    id 9
    label "nieprzewidzianie"
  ]
  node [
    id 10
    label "zaskakuj&#261;co"
  ]
  node [
    id 11
    label "nieoczekiwany"
  ]
  node [
    id 12
    label "zinterpretowa&#263;"
  ]
  node [
    id 13
    label "spoziera&#263;"
  ]
  node [
    id 14
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 15
    label "popatrze&#263;"
  ]
  node [
    id 16
    label "pojrze&#263;"
  ]
  node [
    id 17
    label "peek"
  ]
  node [
    id 18
    label "see"
  ]
  node [
    id 19
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 20
    label "oceni&#263;"
  ]
  node [
    id 21
    label "zagra&#263;"
  ]
  node [
    id 22
    label "illustrate"
  ]
  node [
    id 23
    label "zanalizowa&#263;"
  ]
  node [
    id 24
    label "read"
  ]
  node [
    id 25
    label "think"
  ]
  node [
    id 26
    label "visualize"
  ]
  node [
    id 27
    label "sta&#263;_si&#281;"
  ]
  node [
    id 28
    label "zrobi&#263;"
  ]
  node [
    id 29
    label "zobaczy&#263;"
  ]
  node [
    id 30
    label "spogl&#261;da&#263;"
  ]
  node [
    id 31
    label "slip"
  ]
  node [
    id 32
    label "port"
  ]
  node [
    id 33
    label "Korynt"
  ]
  node [
    id 34
    label "Samara"
  ]
  node [
    id 35
    label "Berdia&#324;sk"
  ]
  node [
    id 36
    label "terminal"
  ]
  node [
    id 37
    label "Kajenna"
  ]
  node [
    id 38
    label "Bordeaux"
  ]
  node [
    id 39
    label "sztauer"
  ]
  node [
    id 40
    label "Koper"
  ]
  node [
    id 41
    label "basen"
  ]
  node [
    id 42
    label "za&#322;adownia"
  ]
  node [
    id 43
    label "Baku"
  ]
  node [
    id 44
    label "baza"
  ]
  node [
    id 45
    label "nabrze&#380;e"
  ]
  node [
    id 46
    label "budowla_hydrotechniczna"
  ]
  node [
    id 47
    label "cutting"
  ]
  node [
    id 48
    label "nakaza&#263;"
  ]
  node [
    id 49
    label "invite"
  ]
  node [
    id 50
    label "cry"
  ]
  node [
    id 51
    label "powiedzie&#263;"
  ]
  node [
    id 52
    label "krzykn&#261;&#263;"
  ]
  node [
    id 53
    label "przewo&#322;a&#263;"
  ]
  node [
    id 54
    label "wydoby&#263;"
  ]
  node [
    id 55
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 56
    label "shout"
  ]
  node [
    id 57
    label "discover"
  ]
  node [
    id 58
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 59
    label "poda&#263;"
  ]
  node [
    id 60
    label "okre&#347;li&#263;"
  ]
  node [
    id 61
    label "express"
  ]
  node [
    id 62
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 63
    label "wyrazi&#263;"
  ]
  node [
    id 64
    label "rzekn&#261;&#263;"
  ]
  node [
    id 65
    label "unwrap"
  ]
  node [
    id 66
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 67
    label "convey"
  ]
  node [
    id 68
    label "poleci&#263;"
  ]
  node [
    id 69
    label "order"
  ]
  node [
    id 70
    label "zapakowa&#263;"
  ]
  node [
    id 71
    label "wezwa&#263;"
  ]
  node [
    id 72
    label "zepsu&#263;"
  ]
  node [
    id 73
    label "przywo&#322;a&#263;"
  ]
  node [
    id 74
    label "wywo&#322;a&#263;"
  ]
  node [
    id 75
    label "zatoka"
  ]
  node [
    id 76
    label "odeski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 75
    target 76
  ]
]
