graph [
  node [
    id 0
    label "raz"
    origin "text"
  ]
  node [
    id 1
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 2
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 4
    label "puhaczew"
    origin "text"
  ]
  node [
    id 5
    label "wstydzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "taki"
    origin "text"
  ]
  node [
    id 10
    label "uczony"
    origin "text"
  ]
  node [
    id 11
    label "uchodzi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "zawo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pan"
    origin "text"
  ]
  node [
    id 14
    label "zab&#322;ocki"
    origin "text"
  ]
  node [
    id 15
    label "swoje"
    origin "text"
  ]
  node [
    id 16
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 17
    label "adiutant"
    origin "text"
  ]
  node [
    id 18
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 19
    label "pisywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "rozkaz"
    origin "text"
  ]
  node [
    id 21
    label "jaki"
    origin "text"
  ]
  node [
    id 22
    label "tamten"
    origin "text"
  ]
  node [
    id 23
    label "wymy&#347;la&#263;"
    origin "text"
  ]
  node [
    id 24
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 25
    label "krejd&#281;"
    origin "text"
  ]
  node [
    id 26
    label "giwer"
    origin "text"
  ]
  node [
    id 27
    label "pomaza&#263;"
    origin "text"
  ]
  node [
    id 28
    label "czort"
    origin "text"
  ]
  node [
    id 29
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 30
    label "kaza&#263;"
    origin "text"
  ]
  node [
    id 31
    label "diak"
    origin "text"
  ]
  node [
    id 32
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 33
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 34
    label "time"
  ]
  node [
    id 35
    label "cios"
  ]
  node [
    id 36
    label "chwila"
  ]
  node [
    id 37
    label "uderzenie"
  ]
  node [
    id 38
    label "blok"
  ]
  node [
    id 39
    label "shot"
  ]
  node [
    id 40
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 41
    label "struktura_geologiczna"
  ]
  node [
    id 42
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 43
    label "pr&#243;ba"
  ]
  node [
    id 44
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 45
    label "coup"
  ]
  node [
    id 46
    label "siekacz"
  ]
  node [
    id 47
    label "instrumentalizacja"
  ]
  node [
    id 48
    label "trafienie"
  ]
  node [
    id 49
    label "walka"
  ]
  node [
    id 50
    label "zdarzenie_si&#281;"
  ]
  node [
    id 51
    label "wdarcie_si&#281;"
  ]
  node [
    id 52
    label "pogorszenie"
  ]
  node [
    id 53
    label "d&#378;wi&#281;k"
  ]
  node [
    id 54
    label "poczucie"
  ]
  node [
    id 55
    label "reakcja"
  ]
  node [
    id 56
    label "contact"
  ]
  node [
    id 57
    label "stukni&#281;cie"
  ]
  node [
    id 58
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 59
    label "bat"
  ]
  node [
    id 60
    label "spowodowanie"
  ]
  node [
    id 61
    label "rush"
  ]
  node [
    id 62
    label "odbicie"
  ]
  node [
    id 63
    label "dawka"
  ]
  node [
    id 64
    label "zadanie"
  ]
  node [
    id 65
    label "&#347;ci&#281;cie"
  ]
  node [
    id 66
    label "st&#322;uczenie"
  ]
  node [
    id 67
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 68
    label "odbicie_si&#281;"
  ]
  node [
    id 69
    label "dotkni&#281;cie"
  ]
  node [
    id 70
    label "charge"
  ]
  node [
    id 71
    label "dostanie"
  ]
  node [
    id 72
    label "skrytykowanie"
  ]
  node [
    id 73
    label "zagrywka"
  ]
  node [
    id 74
    label "manewr"
  ]
  node [
    id 75
    label "nast&#261;pienie"
  ]
  node [
    id 76
    label "uderzanie"
  ]
  node [
    id 77
    label "pogoda"
  ]
  node [
    id 78
    label "stroke"
  ]
  node [
    id 79
    label "pobicie"
  ]
  node [
    id 80
    label "ruch"
  ]
  node [
    id 81
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 82
    label "flap"
  ]
  node [
    id 83
    label "dotyk"
  ]
  node [
    id 84
    label "zrobienie"
  ]
  node [
    id 85
    label "czas"
  ]
  node [
    id 86
    label "pierworodztwo"
  ]
  node [
    id 87
    label "faza"
  ]
  node [
    id 88
    label "miejsce"
  ]
  node [
    id 89
    label "upgrade"
  ]
  node [
    id 90
    label "nast&#281;pstwo"
  ]
  node [
    id 91
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 92
    label "warunek_lokalowy"
  ]
  node [
    id 93
    label "plac"
  ]
  node [
    id 94
    label "location"
  ]
  node [
    id 95
    label "uwaga"
  ]
  node [
    id 96
    label "przestrze&#324;"
  ]
  node [
    id 97
    label "status"
  ]
  node [
    id 98
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 99
    label "cia&#322;o"
  ]
  node [
    id 100
    label "cecha"
  ]
  node [
    id 101
    label "praca"
  ]
  node [
    id 102
    label "rz&#261;d"
  ]
  node [
    id 103
    label "Rzym_Zachodni"
  ]
  node [
    id 104
    label "whole"
  ]
  node [
    id 105
    label "ilo&#347;&#263;"
  ]
  node [
    id 106
    label "element"
  ]
  node [
    id 107
    label "Rzym_Wschodni"
  ]
  node [
    id 108
    label "urz&#261;dzenie"
  ]
  node [
    id 109
    label "cykl_astronomiczny"
  ]
  node [
    id 110
    label "coil"
  ]
  node [
    id 111
    label "zjawisko"
  ]
  node [
    id 112
    label "fotoelement"
  ]
  node [
    id 113
    label "komutowanie"
  ]
  node [
    id 114
    label "stan_skupienia"
  ]
  node [
    id 115
    label "nastr&#243;j"
  ]
  node [
    id 116
    label "przerywacz"
  ]
  node [
    id 117
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 118
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 119
    label "kraw&#281;d&#378;"
  ]
  node [
    id 120
    label "obsesja"
  ]
  node [
    id 121
    label "dw&#243;jnik"
  ]
  node [
    id 122
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 123
    label "okres"
  ]
  node [
    id 124
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 125
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 126
    label "przew&#243;d"
  ]
  node [
    id 127
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 128
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 129
    label "obw&#243;d"
  ]
  node [
    id 130
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 131
    label "degree"
  ]
  node [
    id 132
    label "komutowa&#263;"
  ]
  node [
    id 133
    label "pierwor&#243;dztwo"
  ]
  node [
    id 134
    label "odczuwa&#263;"
  ]
  node [
    id 135
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 136
    label "wydziedziczy&#263;"
  ]
  node [
    id 137
    label "skrupienie_si&#281;"
  ]
  node [
    id 138
    label "proces"
  ]
  node [
    id 139
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 140
    label "wydziedziczenie"
  ]
  node [
    id 141
    label "odczucie"
  ]
  node [
    id 142
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 143
    label "koszula_Dejaniry"
  ]
  node [
    id 144
    label "kolejno&#347;&#263;"
  ]
  node [
    id 145
    label "odczuwanie"
  ]
  node [
    id 146
    label "event"
  ]
  node [
    id 147
    label "rezultat"
  ]
  node [
    id 148
    label "prawo"
  ]
  node [
    id 149
    label "skrupianie_si&#281;"
  ]
  node [
    id 150
    label "odczu&#263;"
  ]
  node [
    id 151
    label "ulepszenie"
  ]
  node [
    id 152
    label "samodzielny"
  ]
  node [
    id 153
    label "swojak"
  ]
  node [
    id 154
    label "cz&#322;owiek"
  ]
  node [
    id 155
    label "odpowiedni"
  ]
  node [
    id 156
    label "bli&#378;ni"
  ]
  node [
    id 157
    label "odr&#281;bny"
  ]
  node [
    id 158
    label "sobieradzki"
  ]
  node [
    id 159
    label "niepodleg&#322;y"
  ]
  node [
    id 160
    label "czyj&#347;"
  ]
  node [
    id 161
    label "autonomicznie"
  ]
  node [
    id 162
    label "indywidualny"
  ]
  node [
    id 163
    label "samodzielnie"
  ]
  node [
    id 164
    label "w&#322;asny"
  ]
  node [
    id 165
    label "osobny"
  ]
  node [
    id 166
    label "ludzko&#347;&#263;"
  ]
  node [
    id 167
    label "asymilowanie"
  ]
  node [
    id 168
    label "wapniak"
  ]
  node [
    id 169
    label "asymilowa&#263;"
  ]
  node [
    id 170
    label "os&#322;abia&#263;"
  ]
  node [
    id 171
    label "posta&#263;"
  ]
  node [
    id 172
    label "hominid"
  ]
  node [
    id 173
    label "podw&#322;adny"
  ]
  node [
    id 174
    label "os&#322;abianie"
  ]
  node [
    id 175
    label "g&#322;owa"
  ]
  node [
    id 176
    label "figura"
  ]
  node [
    id 177
    label "portrecista"
  ]
  node [
    id 178
    label "dwun&#243;g"
  ]
  node [
    id 179
    label "profanum"
  ]
  node [
    id 180
    label "mikrokosmos"
  ]
  node [
    id 181
    label "nasada"
  ]
  node [
    id 182
    label "duch"
  ]
  node [
    id 183
    label "antropochoria"
  ]
  node [
    id 184
    label "osoba"
  ]
  node [
    id 185
    label "wz&#243;r"
  ]
  node [
    id 186
    label "senior"
  ]
  node [
    id 187
    label "oddzia&#322;ywanie"
  ]
  node [
    id 188
    label "Adam"
  ]
  node [
    id 189
    label "homo_sapiens"
  ]
  node [
    id 190
    label "polifag"
  ]
  node [
    id 191
    label "zdarzony"
  ]
  node [
    id 192
    label "odpowiednio"
  ]
  node [
    id 193
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 194
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 195
    label "nale&#380;ny"
  ]
  node [
    id 196
    label "nale&#380;yty"
  ]
  node [
    id 197
    label "stosownie"
  ]
  node [
    id 198
    label "odpowiadanie"
  ]
  node [
    id 199
    label "specjalny"
  ]
  node [
    id 200
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 201
    label "zesp&#243;&#322;"
  ]
  node [
    id 202
    label "instytucja"
  ]
  node [
    id 203
    label "wys&#322;uga"
  ]
  node [
    id 204
    label "service"
  ]
  node [
    id 205
    label "czworak"
  ]
  node [
    id 206
    label "ZOMO"
  ]
  node [
    id 207
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 208
    label "moneta"
  ]
  node [
    id 209
    label "szesnastowieczny"
  ]
  node [
    id 210
    label "dom_wielorodzinny"
  ]
  node [
    id 211
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 212
    label "Mazowsze"
  ]
  node [
    id 213
    label "odm&#322;adzanie"
  ]
  node [
    id 214
    label "&#346;wietliki"
  ]
  node [
    id 215
    label "zbi&#243;r"
  ]
  node [
    id 216
    label "skupienie"
  ]
  node [
    id 217
    label "The_Beatles"
  ]
  node [
    id 218
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 219
    label "odm&#322;adza&#263;"
  ]
  node [
    id 220
    label "zabudowania"
  ]
  node [
    id 221
    label "group"
  ]
  node [
    id 222
    label "zespolik"
  ]
  node [
    id 223
    label "schorzenie"
  ]
  node [
    id 224
    label "ro&#347;lina"
  ]
  node [
    id 225
    label "grupa"
  ]
  node [
    id 226
    label "Depeche_Mode"
  ]
  node [
    id 227
    label "batch"
  ]
  node [
    id 228
    label "odm&#322;odzenie"
  ]
  node [
    id 229
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 230
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 231
    label "najem"
  ]
  node [
    id 232
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 233
    label "zak&#322;ad"
  ]
  node [
    id 234
    label "stosunek_pracy"
  ]
  node [
    id 235
    label "benedykty&#324;ski"
  ]
  node [
    id 236
    label "poda&#380;_pracy"
  ]
  node [
    id 237
    label "pracowanie"
  ]
  node [
    id 238
    label "tyrka"
  ]
  node [
    id 239
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 240
    label "wytw&#243;r"
  ]
  node [
    id 241
    label "zaw&#243;d"
  ]
  node [
    id 242
    label "tynkarski"
  ]
  node [
    id 243
    label "pracowa&#263;"
  ]
  node [
    id 244
    label "czynno&#347;&#263;"
  ]
  node [
    id 245
    label "zmiana"
  ]
  node [
    id 246
    label "czynnik_produkcji"
  ]
  node [
    id 247
    label "zobowi&#261;zanie"
  ]
  node [
    id 248
    label "kierownictwo"
  ]
  node [
    id 249
    label "siedziba"
  ]
  node [
    id 250
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 251
    label "osoba_prawna"
  ]
  node [
    id 252
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 253
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 254
    label "poj&#281;cie"
  ]
  node [
    id 255
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 256
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 257
    label "biuro"
  ]
  node [
    id 258
    label "organizacja"
  ]
  node [
    id 259
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 260
    label "Fundusze_Unijne"
  ]
  node [
    id 261
    label "zamyka&#263;"
  ]
  node [
    id 262
    label "establishment"
  ]
  node [
    id 263
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 264
    label "urz&#261;d"
  ]
  node [
    id 265
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 266
    label "afiliowa&#263;"
  ]
  node [
    id 267
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 268
    label "standard"
  ]
  node [
    id 269
    label "zamykanie"
  ]
  node [
    id 270
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 271
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 272
    label "s&#322;uga"
  ]
  node [
    id 273
    label "s&#322;u&#380;ebnik"
  ]
  node [
    id 274
    label "ochmistrzyni"
  ]
  node [
    id 275
    label "s&#322;u&#380;ebnica"
  ]
  node [
    id 276
    label "milicja_obywatelska"
  ]
  node [
    id 277
    label "formu&#322;owa&#263;"
  ]
  node [
    id 278
    label "ozdabia&#263;"
  ]
  node [
    id 279
    label "stawia&#263;"
  ]
  node [
    id 280
    label "spell"
  ]
  node [
    id 281
    label "styl"
  ]
  node [
    id 282
    label "skryba"
  ]
  node [
    id 283
    label "read"
  ]
  node [
    id 284
    label "donosi&#263;"
  ]
  node [
    id 285
    label "code"
  ]
  node [
    id 286
    label "tekst"
  ]
  node [
    id 287
    label "dysgrafia"
  ]
  node [
    id 288
    label "dysortografia"
  ]
  node [
    id 289
    label "tworzy&#263;"
  ]
  node [
    id 290
    label "prasa"
  ]
  node [
    id 291
    label "robi&#263;"
  ]
  node [
    id 292
    label "pope&#322;nia&#263;"
  ]
  node [
    id 293
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 294
    label "wytwarza&#263;"
  ]
  node [
    id 295
    label "get"
  ]
  node [
    id 296
    label "consist"
  ]
  node [
    id 297
    label "stanowi&#263;"
  ]
  node [
    id 298
    label "raise"
  ]
  node [
    id 299
    label "spill_the_beans"
  ]
  node [
    id 300
    label "przeby&#263;"
  ]
  node [
    id 301
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 302
    label "zanosi&#263;"
  ]
  node [
    id 303
    label "inform"
  ]
  node [
    id 304
    label "give"
  ]
  node [
    id 305
    label "zu&#380;y&#263;"
  ]
  node [
    id 306
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 307
    label "introduce"
  ]
  node [
    id 308
    label "render"
  ]
  node [
    id 309
    label "ci&#261;&#380;a"
  ]
  node [
    id 310
    label "informowa&#263;"
  ]
  node [
    id 311
    label "komunikowa&#263;"
  ]
  node [
    id 312
    label "convey"
  ]
  node [
    id 313
    label "pozostawia&#263;"
  ]
  node [
    id 314
    label "czyni&#263;"
  ]
  node [
    id 315
    label "wydawa&#263;"
  ]
  node [
    id 316
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 317
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 318
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 319
    label "przewidywa&#263;"
  ]
  node [
    id 320
    label "przyznawa&#263;"
  ]
  node [
    id 321
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 322
    label "go"
  ]
  node [
    id 323
    label "obstawia&#263;"
  ]
  node [
    id 324
    label "umieszcza&#263;"
  ]
  node [
    id 325
    label "ocenia&#263;"
  ]
  node [
    id 326
    label "zastawia&#263;"
  ]
  node [
    id 327
    label "stanowisko"
  ]
  node [
    id 328
    label "znak"
  ]
  node [
    id 329
    label "wskazywa&#263;"
  ]
  node [
    id 330
    label "uruchamia&#263;"
  ]
  node [
    id 331
    label "fundowa&#263;"
  ]
  node [
    id 332
    label "zmienia&#263;"
  ]
  node [
    id 333
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 334
    label "deliver"
  ]
  node [
    id 335
    label "powodowa&#263;"
  ]
  node [
    id 336
    label "wyznacza&#263;"
  ]
  node [
    id 337
    label "przedstawia&#263;"
  ]
  node [
    id 338
    label "wydobywa&#263;"
  ]
  node [
    id 339
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 340
    label "trim"
  ]
  node [
    id 341
    label "gryzipi&#243;rek"
  ]
  node [
    id 342
    label "pisarz"
  ]
  node [
    id 343
    label "ekscerpcja"
  ]
  node [
    id 344
    label "j&#281;zykowo"
  ]
  node [
    id 345
    label "wypowied&#378;"
  ]
  node [
    id 346
    label "redakcja"
  ]
  node [
    id 347
    label "pomini&#281;cie"
  ]
  node [
    id 348
    label "dzie&#322;o"
  ]
  node [
    id 349
    label "preparacja"
  ]
  node [
    id 350
    label "odmianka"
  ]
  node [
    id 351
    label "opu&#347;ci&#263;"
  ]
  node [
    id 352
    label "koniektura"
  ]
  node [
    id 353
    label "obelga"
  ]
  node [
    id 354
    label "t&#322;oczysko"
  ]
  node [
    id 355
    label "depesza"
  ]
  node [
    id 356
    label "maszyna"
  ]
  node [
    id 357
    label "media"
  ]
  node [
    id 358
    label "napisa&#263;"
  ]
  node [
    id 359
    label "czasopismo"
  ]
  node [
    id 360
    label "dziennikarz_prasowy"
  ]
  node [
    id 361
    label "kiosk"
  ]
  node [
    id 362
    label "maszyna_rolnicza"
  ]
  node [
    id 363
    label "gazeta"
  ]
  node [
    id 364
    label "dysleksja"
  ]
  node [
    id 365
    label "pisanie"
  ]
  node [
    id 366
    label "trzonek"
  ]
  node [
    id 367
    label "narz&#281;dzie"
  ]
  node [
    id 368
    label "spos&#243;b"
  ]
  node [
    id 369
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 370
    label "zachowanie"
  ]
  node [
    id 371
    label "stylik"
  ]
  node [
    id 372
    label "dyscyplina_sportowa"
  ]
  node [
    id 373
    label "handle"
  ]
  node [
    id 374
    label "line"
  ]
  node [
    id 375
    label "charakter"
  ]
  node [
    id 376
    label "natural_language"
  ]
  node [
    id 377
    label "kanon"
  ]
  node [
    id 378
    label "behawior"
  ]
  node [
    id 379
    label "dysgraphia"
  ]
  node [
    id 380
    label "czu&#263;"
  ]
  node [
    id 381
    label "desire"
  ]
  node [
    id 382
    label "kcie&#263;"
  ]
  node [
    id 383
    label "postrzega&#263;"
  ]
  node [
    id 384
    label "by&#263;"
  ]
  node [
    id 385
    label "smell"
  ]
  node [
    id 386
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 387
    label "uczuwa&#263;"
  ]
  node [
    id 388
    label "spirit"
  ]
  node [
    id 389
    label "doznawa&#263;"
  ]
  node [
    id 390
    label "anticipate"
  ]
  node [
    id 391
    label "okre&#347;lony"
  ]
  node [
    id 392
    label "jaki&#347;"
  ]
  node [
    id 393
    label "przyzwoity"
  ]
  node [
    id 394
    label "ciekawy"
  ]
  node [
    id 395
    label "jako&#347;"
  ]
  node [
    id 396
    label "jako_tako"
  ]
  node [
    id 397
    label "niez&#322;y"
  ]
  node [
    id 398
    label "dziwny"
  ]
  node [
    id 399
    label "charakterystyczny"
  ]
  node [
    id 400
    label "wiadomy"
  ]
  node [
    id 401
    label "wykszta&#322;cony"
  ]
  node [
    id 402
    label "inteligent"
  ]
  node [
    id 403
    label "intelektualista"
  ]
  node [
    id 404
    label "Awerroes"
  ]
  node [
    id 405
    label "uczenie"
  ]
  node [
    id 406
    label "nauczny"
  ]
  node [
    id 407
    label "m&#261;dry"
  ]
  node [
    id 408
    label "zm&#261;drzenie"
  ]
  node [
    id 409
    label "m&#261;drzenie"
  ]
  node [
    id 410
    label "m&#261;drze"
  ]
  node [
    id 411
    label "skomplikowany"
  ]
  node [
    id 412
    label "m&#261;drzenie_si&#281;"
  ]
  node [
    id 413
    label "pyszny"
  ]
  node [
    id 414
    label "inteligentny"
  ]
  node [
    id 415
    label "dobry"
  ]
  node [
    id 416
    label "jajog&#322;owy"
  ]
  node [
    id 417
    label "m&#243;zg"
  ]
  node [
    id 418
    label "filozof"
  ]
  node [
    id 419
    label "przedstawiciel"
  ]
  node [
    id 420
    label "inteligencja"
  ]
  node [
    id 421
    label "rozwijanie"
  ]
  node [
    id 422
    label "wychowywanie"
  ]
  node [
    id 423
    label "pomaganie"
  ]
  node [
    id 424
    label "training"
  ]
  node [
    id 425
    label "zapoznawanie"
  ]
  node [
    id 426
    label "teaching"
  ]
  node [
    id 427
    label "education"
  ]
  node [
    id 428
    label "pouczenie"
  ]
  node [
    id 429
    label "o&#347;wiecanie"
  ]
  node [
    id 430
    label "przyuczanie"
  ]
  node [
    id 431
    label "przyuczenie"
  ]
  node [
    id 432
    label "kliker"
  ]
  node [
    id 433
    label "awerroista"
  ]
  node [
    id 434
    label "opuszcza&#263;"
  ]
  node [
    id 435
    label "escape"
  ]
  node [
    id 436
    label "pali&#263;_wrotki"
  ]
  node [
    id 437
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 438
    label "obywa&#263;_si&#281;"
  ]
  node [
    id 439
    label "blow"
  ]
  node [
    id 440
    label "proceed"
  ]
  node [
    id 441
    label "bra&#263;"
  ]
  node [
    id 442
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 443
    label "umyka&#263;"
  ]
  node [
    id 444
    label "strona_&#347;wiata"
  ]
  node [
    id 445
    label "unika&#263;"
  ]
  node [
    id 446
    label "ucieka&#263;"
  ]
  node [
    id 447
    label "spieprza&#263;"
  ]
  node [
    id 448
    label "zwiewa&#263;"
  ]
  node [
    id 449
    label "ubywa&#263;"
  ]
  node [
    id 450
    label "przechodzi&#263;"
  ]
  node [
    id 451
    label "wpada&#263;"
  ]
  node [
    id 452
    label "pochyla&#263;_si&#281;"
  ]
  node [
    id 453
    label "trwa&#263;"
  ]
  node [
    id 454
    label "izolowa&#263;_si&#281;"
  ]
  node [
    id 455
    label "pozostawa&#263;"
  ]
  node [
    id 456
    label "base_on_balls"
  ]
  node [
    id 457
    label "refuse"
  ]
  node [
    id 458
    label "zbywa&#263;"
  ]
  node [
    id 459
    label "decrease"
  ]
  node [
    id 460
    label "posiada&#263;"
  ]
  node [
    id 461
    label "traci&#263;"
  ]
  node [
    id 462
    label "obni&#380;a&#263;"
  ]
  node [
    id 463
    label "abort"
  ]
  node [
    id 464
    label "omija&#263;"
  ]
  node [
    id 465
    label "przestawa&#263;"
  ]
  node [
    id 466
    label "forowa&#263;_si&#281;"
  ]
  node [
    id 467
    label "potania&#263;"
  ]
  node [
    id 468
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 469
    label "wymyka&#263;_si&#281;"
  ]
  node [
    id 470
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 471
    label "strike"
  ]
  node [
    id 472
    label "zaziera&#263;"
  ]
  node [
    id 473
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 474
    label "spotyka&#263;"
  ]
  node [
    id 475
    label "drop"
  ]
  node [
    id 476
    label "pogo"
  ]
  node [
    id 477
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 478
    label "wpa&#347;&#263;"
  ]
  node [
    id 479
    label "rzecz"
  ]
  node [
    id 480
    label "ogrom"
  ]
  node [
    id 481
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 482
    label "zapach"
  ]
  node [
    id 483
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 484
    label "popada&#263;"
  ]
  node [
    id 485
    label "odwiedza&#263;"
  ]
  node [
    id 486
    label "przypomina&#263;"
  ]
  node [
    id 487
    label "ujmowa&#263;"
  ]
  node [
    id 488
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 489
    label "&#347;wiat&#322;o"
  ]
  node [
    id 490
    label "fall"
  ]
  node [
    id 491
    label "chowa&#263;"
  ]
  node [
    id 492
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 493
    label "demaskowa&#263;"
  ]
  node [
    id 494
    label "ulega&#263;"
  ]
  node [
    id 495
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 496
    label "emocja"
  ]
  node [
    id 497
    label "flatten"
  ]
  node [
    id 498
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 499
    label "mie&#263;_miejsce"
  ]
  node [
    id 500
    label "equal"
  ]
  node [
    id 501
    label "chodzi&#263;"
  ]
  node [
    id 502
    label "si&#281;ga&#263;"
  ]
  node [
    id 503
    label "stan"
  ]
  node [
    id 504
    label "obecno&#347;&#263;"
  ]
  node [
    id 505
    label "stand"
  ]
  node [
    id 506
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 507
    label "uczestniczy&#263;"
  ]
  node [
    id 508
    label "stara&#263;_si&#281;"
  ]
  node [
    id 509
    label "contrivance"
  ]
  node [
    id 510
    label "evade"
  ]
  node [
    id 511
    label "stroni&#263;"
  ]
  node [
    id 512
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 513
    label "dodge"
  ]
  node [
    id 514
    label "move"
  ]
  node [
    id 515
    label "zaczyna&#263;"
  ]
  node [
    id 516
    label "przebywa&#263;"
  ]
  node [
    id 517
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 518
    label "conflict"
  ]
  node [
    id 519
    label "mija&#263;"
  ]
  node [
    id 520
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 521
    label "saturate"
  ]
  node [
    id 522
    label "i&#347;&#263;"
  ]
  node [
    id 523
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 524
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 525
    label "pass"
  ]
  node [
    id 526
    label "zalicza&#263;"
  ]
  node [
    id 527
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 528
    label "test"
  ]
  node [
    id 529
    label "podlega&#263;"
  ]
  node [
    id 530
    label "przerabia&#263;"
  ]
  node [
    id 531
    label "continue"
  ]
  node [
    id 532
    label "przemieszcza&#263;"
  ]
  node [
    id 533
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 534
    label "porywa&#263;"
  ]
  node [
    id 535
    label "korzysta&#263;"
  ]
  node [
    id 536
    label "take"
  ]
  node [
    id 537
    label "wchodzi&#263;"
  ]
  node [
    id 538
    label "poczytywa&#263;"
  ]
  node [
    id 539
    label "levy"
  ]
  node [
    id 540
    label "wk&#322;ada&#263;"
  ]
  node [
    id 541
    label "pokonywa&#263;"
  ]
  node [
    id 542
    label "przyjmowa&#263;"
  ]
  node [
    id 543
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 544
    label "rucha&#263;"
  ]
  node [
    id 545
    label "prowadzi&#263;"
  ]
  node [
    id 546
    label "za&#380;ywa&#263;"
  ]
  node [
    id 547
    label "otrzymywa&#263;"
  ]
  node [
    id 548
    label "&#263;pa&#263;"
  ]
  node [
    id 549
    label "interpretowa&#263;"
  ]
  node [
    id 550
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 551
    label "dostawa&#263;"
  ]
  node [
    id 552
    label "rusza&#263;"
  ]
  node [
    id 553
    label "chwyta&#263;"
  ]
  node [
    id 554
    label "grza&#263;"
  ]
  node [
    id 555
    label "wch&#322;ania&#263;"
  ]
  node [
    id 556
    label "wygrywa&#263;"
  ]
  node [
    id 557
    label "u&#380;ywa&#263;"
  ]
  node [
    id 558
    label "arise"
  ]
  node [
    id 559
    label "uprawia&#263;_seks"
  ]
  node [
    id 560
    label "abstract"
  ]
  node [
    id 561
    label "towarzystwo"
  ]
  node [
    id 562
    label "atakowa&#263;"
  ]
  node [
    id 563
    label "branie"
  ]
  node [
    id 564
    label "open"
  ]
  node [
    id 565
    label "&#322;apa&#263;"
  ]
  node [
    id 566
    label "przewa&#380;a&#263;"
  ]
  node [
    id 567
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 568
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 569
    label "nakaza&#263;"
  ]
  node [
    id 570
    label "invite"
  ]
  node [
    id 571
    label "cry"
  ]
  node [
    id 572
    label "powiedzie&#263;"
  ]
  node [
    id 573
    label "krzykn&#261;&#263;"
  ]
  node [
    id 574
    label "przewo&#322;a&#263;"
  ]
  node [
    id 575
    label "wydoby&#263;"
  ]
  node [
    id 576
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 577
    label "shout"
  ]
  node [
    id 578
    label "discover"
  ]
  node [
    id 579
    label "okre&#347;li&#263;"
  ]
  node [
    id 580
    label "poda&#263;"
  ]
  node [
    id 581
    label "express"
  ]
  node [
    id 582
    label "wyrazi&#263;"
  ]
  node [
    id 583
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 584
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 585
    label "rzekn&#261;&#263;"
  ]
  node [
    id 586
    label "unwrap"
  ]
  node [
    id 587
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 588
    label "poleci&#263;"
  ]
  node [
    id 589
    label "order"
  ]
  node [
    id 590
    label "zapakowa&#263;"
  ]
  node [
    id 591
    label "wezwa&#263;"
  ]
  node [
    id 592
    label "zepsu&#263;"
  ]
  node [
    id 593
    label "przywo&#322;a&#263;"
  ]
  node [
    id 594
    label "wywo&#322;a&#263;"
  ]
  node [
    id 595
    label "belfer"
  ]
  node [
    id 596
    label "murza"
  ]
  node [
    id 597
    label "ojciec"
  ]
  node [
    id 598
    label "samiec"
  ]
  node [
    id 599
    label "androlog"
  ]
  node [
    id 600
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 601
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 602
    label "efendi"
  ]
  node [
    id 603
    label "opiekun"
  ]
  node [
    id 604
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 605
    label "pa&#324;stwo"
  ]
  node [
    id 606
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 607
    label "bratek"
  ]
  node [
    id 608
    label "Mieszko_I"
  ]
  node [
    id 609
    label "Midas"
  ]
  node [
    id 610
    label "m&#261;&#380;"
  ]
  node [
    id 611
    label "bogaty"
  ]
  node [
    id 612
    label "popularyzator"
  ]
  node [
    id 613
    label "pracodawca"
  ]
  node [
    id 614
    label "kszta&#322;ciciel"
  ]
  node [
    id 615
    label "preceptor"
  ]
  node [
    id 616
    label "nabab"
  ]
  node [
    id 617
    label "pupil"
  ]
  node [
    id 618
    label "andropauza"
  ]
  node [
    id 619
    label "zwrot"
  ]
  node [
    id 620
    label "przyw&#243;dca"
  ]
  node [
    id 621
    label "doros&#322;y"
  ]
  node [
    id 622
    label "pedagog"
  ]
  node [
    id 623
    label "rz&#261;dzenie"
  ]
  node [
    id 624
    label "jegomo&#347;&#263;"
  ]
  node [
    id 625
    label "szkolnik"
  ]
  node [
    id 626
    label "ch&#322;opina"
  ]
  node [
    id 627
    label "w&#322;odarz"
  ]
  node [
    id 628
    label "profesor"
  ]
  node [
    id 629
    label "gra_w_karty"
  ]
  node [
    id 630
    label "w&#322;adza"
  ]
  node [
    id 631
    label "Fidel_Castro"
  ]
  node [
    id 632
    label "Anders"
  ]
  node [
    id 633
    label "Ko&#347;ciuszko"
  ]
  node [
    id 634
    label "Tito"
  ]
  node [
    id 635
    label "Miko&#322;ajczyk"
  ]
  node [
    id 636
    label "lider"
  ]
  node [
    id 637
    label "Mao"
  ]
  node [
    id 638
    label "Sabataj_Cwi"
  ]
  node [
    id 639
    label "p&#322;atnik"
  ]
  node [
    id 640
    label "zwierzchnik"
  ]
  node [
    id 641
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 642
    label "nadzorca"
  ]
  node [
    id 643
    label "funkcjonariusz"
  ]
  node [
    id 644
    label "podmiot"
  ]
  node [
    id 645
    label "wykupienie"
  ]
  node [
    id 646
    label "bycie_w_posiadaniu"
  ]
  node [
    id 647
    label "wykupywanie"
  ]
  node [
    id 648
    label "rozszerzyciel"
  ]
  node [
    id 649
    label "wydoro&#347;lenie"
  ]
  node [
    id 650
    label "du&#380;y"
  ]
  node [
    id 651
    label "doro&#347;lenie"
  ]
  node [
    id 652
    label "&#378;ra&#322;y"
  ]
  node [
    id 653
    label "doro&#347;le"
  ]
  node [
    id 654
    label "dojrzale"
  ]
  node [
    id 655
    label "dojrza&#322;y"
  ]
  node [
    id 656
    label "doletni"
  ]
  node [
    id 657
    label "punkt"
  ]
  node [
    id 658
    label "turn"
  ]
  node [
    id 659
    label "turning"
  ]
  node [
    id 660
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 661
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 662
    label "skr&#281;t"
  ]
  node [
    id 663
    label "obr&#243;t"
  ]
  node [
    id 664
    label "fraza_czasownikowa"
  ]
  node [
    id 665
    label "jednostka_leksykalna"
  ]
  node [
    id 666
    label "wyra&#380;enie"
  ]
  node [
    id 667
    label "starosta"
  ]
  node [
    id 668
    label "zarz&#261;dca"
  ]
  node [
    id 669
    label "w&#322;adca"
  ]
  node [
    id 670
    label "nauczyciel"
  ]
  node [
    id 671
    label "stopie&#324;_naukowy"
  ]
  node [
    id 672
    label "nauczyciel_akademicki"
  ]
  node [
    id 673
    label "tytu&#322;"
  ]
  node [
    id 674
    label "profesura"
  ]
  node [
    id 675
    label "konsulent"
  ]
  node [
    id 676
    label "wirtuoz"
  ]
  node [
    id 677
    label "autor"
  ]
  node [
    id 678
    label "wyprawka"
  ]
  node [
    id 679
    label "mundurek"
  ]
  node [
    id 680
    label "szko&#322;a"
  ]
  node [
    id 681
    label "tarcza"
  ]
  node [
    id 682
    label "elew"
  ]
  node [
    id 683
    label "absolwent"
  ]
  node [
    id 684
    label "klasa"
  ]
  node [
    id 685
    label "ekspert"
  ]
  node [
    id 686
    label "ochotnik"
  ]
  node [
    id 687
    label "pomocnik"
  ]
  node [
    id 688
    label "student"
  ]
  node [
    id 689
    label "nauczyciel_muzyki"
  ]
  node [
    id 690
    label "zakonnik"
  ]
  node [
    id 691
    label "urz&#281;dnik"
  ]
  node [
    id 692
    label "bogacz"
  ]
  node [
    id 693
    label "dostojnik"
  ]
  node [
    id 694
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 695
    label "kuwada"
  ]
  node [
    id 696
    label "tworzyciel"
  ]
  node [
    id 697
    label "rodzice"
  ]
  node [
    id 698
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 699
    label "&#347;w"
  ]
  node [
    id 700
    label "pomys&#322;odawca"
  ]
  node [
    id 701
    label "rodzic"
  ]
  node [
    id 702
    label "wykonawca"
  ]
  node [
    id 703
    label "ojczym"
  ]
  node [
    id 704
    label "przodek"
  ]
  node [
    id 705
    label "papa"
  ]
  node [
    id 706
    label "stary"
  ]
  node [
    id 707
    label "kochanek"
  ]
  node [
    id 708
    label "fio&#322;ek"
  ]
  node [
    id 709
    label "facet"
  ]
  node [
    id 710
    label "brat"
  ]
  node [
    id 711
    label "zwierz&#281;"
  ]
  node [
    id 712
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 713
    label "ma&#322;&#380;onek"
  ]
  node [
    id 714
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 715
    label "m&#243;j"
  ]
  node [
    id 716
    label "ch&#322;op"
  ]
  node [
    id 717
    label "pan_m&#322;ody"
  ]
  node [
    id 718
    label "&#347;lubny"
  ]
  node [
    id 719
    label "pan_domu"
  ]
  node [
    id 720
    label "pan_i_w&#322;adca"
  ]
  node [
    id 721
    label "mo&#347;&#263;"
  ]
  node [
    id 722
    label "Frygia"
  ]
  node [
    id 723
    label "sprawowanie"
  ]
  node [
    id 724
    label "dominion"
  ]
  node [
    id 725
    label "dominowanie"
  ]
  node [
    id 726
    label "reign"
  ]
  node [
    id 727
    label "rule"
  ]
  node [
    id 728
    label "zwierz&#281;_domowe"
  ]
  node [
    id 729
    label "J&#281;drzejewicz"
  ]
  node [
    id 730
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 731
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 732
    label "John_Dewey"
  ]
  node [
    id 733
    label "specjalista"
  ]
  node [
    id 734
    label "&#380;ycie"
  ]
  node [
    id 735
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 736
    label "Turek"
  ]
  node [
    id 737
    label "effendi"
  ]
  node [
    id 738
    label "obfituj&#261;cy"
  ]
  node [
    id 739
    label "r&#243;&#380;norodny"
  ]
  node [
    id 740
    label "spania&#322;y"
  ]
  node [
    id 741
    label "obficie"
  ]
  node [
    id 742
    label "sytuowany"
  ]
  node [
    id 743
    label "och&#281;do&#380;ny"
  ]
  node [
    id 744
    label "forsiasty"
  ]
  node [
    id 745
    label "zapa&#347;ny"
  ]
  node [
    id 746
    label "bogato"
  ]
  node [
    id 747
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 748
    label "Katar"
  ]
  node [
    id 749
    label "Libia"
  ]
  node [
    id 750
    label "Gwatemala"
  ]
  node [
    id 751
    label "Ekwador"
  ]
  node [
    id 752
    label "Afganistan"
  ]
  node [
    id 753
    label "Tad&#380;ykistan"
  ]
  node [
    id 754
    label "Bhutan"
  ]
  node [
    id 755
    label "Argentyna"
  ]
  node [
    id 756
    label "D&#380;ibuti"
  ]
  node [
    id 757
    label "Wenezuela"
  ]
  node [
    id 758
    label "Gabon"
  ]
  node [
    id 759
    label "Ukraina"
  ]
  node [
    id 760
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 761
    label "Rwanda"
  ]
  node [
    id 762
    label "Liechtenstein"
  ]
  node [
    id 763
    label "Sri_Lanka"
  ]
  node [
    id 764
    label "Madagaskar"
  ]
  node [
    id 765
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 766
    label "Kongo"
  ]
  node [
    id 767
    label "Tonga"
  ]
  node [
    id 768
    label "Bangladesz"
  ]
  node [
    id 769
    label "Kanada"
  ]
  node [
    id 770
    label "Wehrlen"
  ]
  node [
    id 771
    label "Algieria"
  ]
  node [
    id 772
    label "Uganda"
  ]
  node [
    id 773
    label "Surinam"
  ]
  node [
    id 774
    label "Sahara_Zachodnia"
  ]
  node [
    id 775
    label "Chile"
  ]
  node [
    id 776
    label "W&#281;gry"
  ]
  node [
    id 777
    label "Birma"
  ]
  node [
    id 778
    label "Kazachstan"
  ]
  node [
    id 779
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 780
    label "Armenia"
  ]
  node [
    id 781
    label "Tuwalu"
  ]
  node [
    id 782
    label "Timor_Wschodni"
  ]
  node [
    id 783
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 784
    label "Izrael"
  ]
  node [
    id 785
    label "Estonia"
  ]
  node [
    id 786
    label "Komory"
  ]
  node [
    id 787
    label "Kamerun"
  ]
  node [
    id 788
    label "Haiti"
  ]
  node [
    id 789
    label "Belize"
  ]
  node [
    id 790
    label "Sierra_Leone"
  ]
  node [
    id 791
    label "Luksemburg"
  ]
  node [
    id 792
    label "USA"
  ]
  node [
    id 793
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 794
    label "Barbados"
  ]
  node [
    id 795
    label "San_Marino"
  ]
  node [
    id 796
    label "Bu&#322;garia"
  ]
  node [
    id 797
    label "Indonezja"
  ]
  node [
    id 798
    label "Wietnam"
  ]
  node [
    id 799
    label "Malawi"
  ]
  node [
    id 800
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 801
    label "Francja"
  ]
  node [
    id 802
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 803
    label "partia"
  ]
  node [
    id 804
    label "Zambia"
  ]
  node [
    id 805
    label "Angola"
  ]
  node [
    id 806
    label "Grenada"
  ]
  node [
    id 807
    label "Nepal"
  ]
  node [
    id 808
    label "Panama"
  ]
  node [
    id 809
    label "Rumunia"
  ]
  node [
    id 810
    label "Czarnog&#243;ra"
  ]
  node [
    id 811
    label "Malediwy"
  ]
  node [
    id 812
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 813
    label "S&#322;owacja"
  ]
  node [
    id 814
    label "para"
  ]
  node [
    id 815
    label "Egipt"
  ]
  node [
    id 816
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 817
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 818
    label "Mozambik"
  ]
  node [
    id 819
    label "Kolumbia"
  ]
  node [
    id 820
    label "Laos"
  ]
  node [
    id 821
    label "Burundi"
  ]
  node [
    id 822
    label "Suazi"
  ]
  node [
    id 823
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 824
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 825
    label "Czechy"
  ]
  node [
    id 826
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 827
    label "Wyspy_Marshalla"
  ]
  node [
    id 828
    label "Dominika"
  ]
  node [
    id 829
    label "Trynidad_i_Tobago"
  ]
  node [
    id 830
    label "Syria"
  ]
  node [
    id 831
    label "Palau"
  ]
  node [
    id 832
    label "Gwinea_Bissau"
  ]
  node [
    id 833
    label "Liberia"
  ]
  node [
    id 834
    label "Jamajka"
  ]
  node [
    id 835
    label "Zimbabwe"
  ]
  node [
    id 836
    label "Polska"
  ]
  node [
    id 837
    label "Dominikana"
  ]
  node [
    id 838
    label "Senegal"
  ]
  node [
    id 839
    label "Togo"
  ]
  node [
    id 840
    label "Gujana"
  ]
  node [
    id 841
    label "Gruzja"
  ]
  node [
    id 842
    label "Albania"
  ]
  node [
    id 843
    label "Zair"
  ]
  node [
    id 844
    label "Meksyk"
  ]
  node [
    id 845
    label "Macedonia"
  ]
  node [
    id 846
    label "Chorwacja"
  ]
  node [
    id 847
    label "Kambod&#380;a"
  ]
  node [
    id 848
    label "Monako"
  ]
  node [
    id 849
    label "Mauritius"
  ]
  node [
    id 850
    label "Gwinea"
  ]
  node [
    id 851
    label "Mali"
  ]
  node [
    id 852
    label "Nigeria"
  ]
  node [
    id 853
    label "Kostaryka"
  ]
  node [
    id 854
    label "Hanower"
  ]
  node [
    id 855
    label "Paragwaj"
  ]
  node [
    id 856
    label "W&#322;ochy"
  ]
  node [
    id 857
    label "Seszele"
  ]
  node [
    id 858
    label "Wyspy_Salomona"
  ]
  node [
    id 859
    label "Hiszpania"
  ]
  node [
    id 860
    label "Boliwia"
  ]
  node [
    id 861
    label "Kirgistan"
  ]
  node [
    id 862
    label "Irlandia"
  ]
  node [
    id 863
    label "Czad"
  ]
  node [
    id 864
    label "Irak"
  ]
  node [
    id 865
    label "Lesoto"
  ]
  node [
    id 866
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 867
    label "Malta"
  ]
  node [
    id 868
    label "Andora"
  ]
  node [
    id 869
    label "Chiny"
  ]
  node [
    id 870
    label "Filipiny"
  ]
  node [
    id 871
    label "Antarktis"
  ]
  node [
    id 872
    label "Niemcy"
  ]
  node [
    id 873
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 874
    label "Pakistan"
  ]
  node [
    id 875
    label "terytorium"
  ]
  node [
    id 876
    label "Nikaragua"
  ]
  node [
    id 877
    label "Brazylia"
  ]
  node [
    id 878
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 879
    label "Maroko"
  ]
  node [
    id 880
    label "Portugalia"
  ]
  node [
    id 881
    label "Niger"
  ]
  node [
    id 882
    label "Kenia"
  ]
  node [
    id 883
    label "Botswana"
  ]
  node [
    id 884
    label "Fid&#380;i"
  ]
  node [
    id 885
    label "Tunezja"
  ]
  node [
    id 886
    label "Australia"
  ]
  node [
    id 887
    label "Tajlandia"
  ]
  node [
    id 888
    label "Burkina_Faso"
  ]
  node [
    id 889
    label "interior"
  ]
  node [
    id 890
    label "Tanzania"
  ]
  node [
    id 891
    label "Benin"
  ]
  node [
    id 892
    label "Indie"
  ]
  node [
    id 893
    label "&#321;otwa"
  ]
  node [
    id 894
    label "Kiribati"
  ]
  node [
    id 895
    label "Antigua_i_Barbuda"
  ]
  node [
    id 896
    label "Rodezja"
  ]
  node [
    id 897
    label "Cypr"
  ]
  node [
    id 898
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 899
    label "Peru"
  ]
  node [
    id 900
    label "Austria"
  ]
  node [
    id 901
    label "Urugwaj"
  ]
  node [
    id 902
    label "Jordania"
  ]
  node [
    id 903
    label "Grecja"
  ]
  node [
    id 904
    label "Azerbejd&#380;an"
  ]
  node [
    id 905
    label "Turcja"
  ]
  node [
    id 906
    label "Samoa"
  ]
  node [
    id 907
    label "Sudan"
  ]
  node [
    id 908
    label "Oman"
  ]
  node [
    id 909
    label "ziemia"
  ]
  node [
    id 910
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 911
    label "Uzbekistan"
  ]
  node [
    id 912
    label "Portoryko"
  ]
  node [
    id 913
    label "Honduras"
  ]
  node [
    id 914
    label "Mongolia"
  ]
  node [
    id 915
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 916
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 917
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 918
    label "Serbia"
  ]
  node [
    id 919
    label "Tajwan"
  ]
  node [
    id 920
    label "Wielka_Brytania"
  ]
  node [
    id 921
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 922
    label "Liban"
  ]
  node [
    id 923
    label "Japonia"
  ]
  node [
    id 924
    label "Ghana"
  ]
  node [
    id 925
    label "Belgia"
  ]
  node [
    id 926
    label "Bahrajn"
  ]
  node [
    id 927
    label "Mikronezja"
  ]
  node [
    id 928
    label "Etiopia"
  ]
  node [
    id 929
    label "Kuwejt"
  ]
  node [
    id 930
    label "Bahamy"
  ]
  node [
    id 931
    label "Rosja"
  ]
  node [
    id 932
    label "Mo&#322;dawia"
  ]
  node [
    id 933
    label "Litwa"
  ]
  node [
    id 934
    label "S&#322;owenia"
  ]
  node [
    id 935
    label "Szwajcaria"
  ]
  node [
    id 936
    label "Erytrea"
  ]
  node [
    id 937
    label "Arabia_Saudyjska"
  ]
  node [
    id 938
    label "Kuba"
  ]
  node [
    id 939
    label "granica_pa&#324;stwa"
  ]
  node [
    id 940
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 941
    label "Malezja"
  ]
  node [
    id 942
    label "Korea"
  ]
  node [
    id 943
    label "Jemen"
  ]
  node [
    id 944
    label "Nowa_Zelandia"
  ]
  node [
    id 945
    label "Namibia"
  ]
  node [
    id 946
    label "Nauru"
  ]
  node [
    id 947
    label "holoarktyka"
  ]
  node [
    id 948
    label "Brunei"
  ]
  node [
    id 949
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 950
    label "Khitai"
  ]
  node [
    id 951
    label "Mauretania"
  ]
  node [
    id 952
    label "Iran"
  ]
  node [
    id 953
    label "Gambia"
  ]
  node [
    id 954
    label "Somalia"
  ]
  node [
    id 955
    label "Holandia"
  ]
  node [
    id 956
    label "Turkmenistan"
  ]
  node [
    id 957
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 958
    label "Salwador"
  ]
  node [
    id 959
    label "oficer"
  ]
  node [
    id 960
    label "podchor&#261;&#380;y"
  ]
  node [
    id 961
    label "podoficer"
  ]
  node [
    id 962
    label "mundurowy"
  ]
  node [
    id 963
    label "komender&#243;wka"
  ]
  node [
    id 964
    label "polecenie"
  ]
  node [
    id 965
    label "direction"
  ]
  node [
    id 966
    label "ukaz"
  ]
  node [
    id 967
    label "pognanie"
  ]
  node [
    id 968
    label "rekomendacja"
  ]
  node [
    id 969
    label "pobiegni&#281;cie"
  ]
  node [
    id 970
    label "doradzenie"
  ]
  node [
    id 971
    label "statement"
  ]
  node [
    id 972
    label "recommendation"
  ]
  node [
    id 973
    label "zaordynowanie"
  ]
  node [
    id 974
    label "powierzenie"
  ]
  node [
    id 975
    label "przesadzenie"
  ]
  node [
    id 976
    label "consign"
  ]
  node [
    id 977
    label "stalag"
  ]
  node [
    id 978
    label "mistreat"
  ]
  node [
    id 979
    label "obra&#380;a&#263;"
  ]
  node [
    id 980
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 981
    label "narusza&#263;"
  ]
  node [
    id 982
    label "odziedziczy&#263;"
  ]
  node [
    id 983
    label "ruszy&#263;"
  ]
  node [
    id 984
    label "zaatakowa&#263;"
  ]
  node [
    id 985
    label "skorzysta&#263;"
  ]
  node [
    id 986
    label "uciec"
  ]
  node [
    id 987
    label "receive"
  ]
  node [
    id 988
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 989
    label "obskoczy&#263;"
  ]
  node [
    id 990
    label "u&#380;y&#263;"
  ]
  node [
    id 991
    label "zrobi&#263;"
  ]
  node [
    id 992
    label "wyrucha&#263;"
  ]
  node [
    id 993
    label "World_Health_Organization"
  ]
  node [
    id 994
    label "wyciupcia&#263;"
  ]
  node [
    id 995
    label "wygra&#263;"
  ]
  node [
    id 996
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 997
    label "withdraw"
  ]
  node [
    id 998
    label "wzi&#281;cie"
  ]
  node [
    id 999
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 1000
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 1001
    label "poczyta&#263;"
  ]
  node [
    id 1002
    label "obj&#261;&#263;"
  ]
  node [
    id 1003
    label "seize"
  ]
  node [
    id 1004
    label "aim"
  ]
  node [
    id 1005
    label "chwyci&#263;"
  ]
  node [
    id 1006
    label "przyj&#261;&#263;"
  ]
  node [
    id 1007
    label "pokona&#263;"
  ]
  node [
    id 1008
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1009
    label "zacz&#261;&#263;"
  ]
  node [
    id 1010
    label "otrzyma&#263;"
  ]
  node [
    id 1011
    label "wej&#347;&#263;"
  ]
  node [
    id 1012
    label "poruszy&#263;"
  ]
  node [
    id 1013
    label "dosta&#263;"
  ]
  node [
    id 1014
    label "post&#261;pi&#263;"
  ]
  node [
    id 1015
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1016
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1017
    label "odj&#261;&#263;"
  ]
  node [
    id 1018
    label "cause"
  ]
  node [
    id 1019
    label "begin"
  ]
  node [
    id 1020
    label "do"
  ]
  node [
    id 1021
    label "przybra&#263;"
  ]
  node [
    id 1022
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1023
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 1024
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 1025
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 1026
    label "obra&#263;"
  ]
  node [
    id 1027
    label "uzna&#263;"
  ]
  node [
    id 1028
    label "draw"
  ]
  node [
    id 1029
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1030
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 1031
    label "przyj&#281;cie"
  ]
  node [
    id 1032
    label "swallow"
  ]
  node [
    id 1033
    label "odebra&#263;"
  ]
  node [
    id 1034
    label "dostarczy&#263;"
  ]
  node [
    id 1035
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1036
    label "absorb"
  ]
  node [
    id 1037
    label "undertake"
  ]
  node [
    id 1038
    label "ubra&#263;"
  ]
  node [
    id 1039
    label "oblec_si&#281;"
  ]
  node [
    id 1040
    label "przekaza&#263;"
  ]
  node [
    id 1041
    label "str&#243;j"
  ]
  node [
    id 1042
    label "insert"
  ]
  node [
    id 1043
    label "wpoi&#263;"
  ]
  node [
    id 1044
    label "pour"
  ]
  node [
    id 1045
    label "przyodzia&#263;"
  ]
  node [
    id 1046
    label "natchn&#261;&#263;"
  ]
  node [
    id 1047
    label "load"
  ]
  node [
    id 1048
    label "wzbudzi&#263;"
  ]
  node [
    id 1049
    label "deposit"
  ]
  node [
    id 1050
    label "oblec"
  ]
  node [
    id 1051
    label "motivate"
  ]
  node [
    id 1052
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 1053
    label "zabra&#263;"
  ]
  node [
    id 1054
    label "allude"
  ]
  node [
    id 1055
    label "cut"
  ]
  node [
    id 1056
    label "spowodowa&#263;"
  ]
  node [
    id 1057
    label "stimulate"
  ]
  node [
    id 1058
    label "nast&#261;pi&#263;"
  ]
  node [
    id 1059
    label "attack"
  ]
  node [
    id 1060
    label "postara&#263;_si&#281;"
  ]
  node [
    id 1061
    label "rozegra&#263;"
  ]
  node [
    id 1062
    label "anoint"
  ]
  node [
    id 1063
    label "sport"
  ]
  node [
    id 1064
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 1065
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1066
    label "skrytykowa&#263;"
  ]
  node [
    id 1067
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1068
    label "zrozumie&#263;"
  ]
  node [
    id 1069
    label "fascinate"
  ]
  node [
    id 1070
    label "notice"
  ]
  node [
    id 1071
    label "ogarn&#261;&#263;"
  ]
  node [
    id 1072
    label "deem"
  ]
  node [
    id 1073
    label "gen"
  ]
  node [
    id 1074
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1075
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1076
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1077
    label "zorganizowa&#263;"
  ]
  node [
    id 1078
    label "appoint"
  ]
  node [
    id 1079
    label "wystylizowa&#263;"
  ]
  node [
    id 1080
    label "przerobi&#263;"
  ]
  node [
    id 1081
    label "nabra&#263;"
  ]
  node [
    id 1082
    label "make"
  ]
  node [
    id 1083
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1084
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1085
    label "wydali&#263;"
  ]
  node [
    id 1086
    label "wytworzy&#263;"
  ]
  node [
    id 1087
    label "give_birth"
  ]
  node [
    id 1088
    label "return"
  ]
  node [
    id 1089
    label "utilize"
  ]
  node [
    id 1090
    label "uzyska&#263;"
  ]
  node [
    id 1091
    label "dozna&#263;"
  ]
  node [
    id 1092
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1093
    label "employment"
  ]
  node [
    id 1094
    label "wykorzysta&#263;"
  ]
  node [
    id 1095
    label "przyswoi&#263;"
  ]
  node [
    id 1096
    label "wykupi&#263;"
  ]
  node [
    id 1097
    label "sponge"
  ]
  node [
    id 1098
    label "zagwarantowa&#263;"
  ]
  node [
    id 1099
    label "znie&#347;&#263;"
  ]
  node [
    id 1100
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 1101
    label "zagra&#263;"
  ]
  node [
    id 1102
    label "score"
  ]
  node [
    id 1103
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 1104
    label "zwojowa&#263;"
  ]
  node [
    id 1105
    label "leave"
  ]
  node [
    id 1106
    label "net_income"
  ]
  node [
    id 1107
    label "instrument_muzyczny"
  ]
  node [
    id 1108
    label "poradzi&#263;_sobie"
  ]
  node [
    id 1109
    label "zapobiec"
  ]
  node [
    id 1110
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1111
    label "z&#322;oi&#263;"
  ]
  node [
    id 1112
    label "overwhelm"
  ]
  node [
    id 1113
    label "embrace"
  ]
  node [
    id 1114
    label "manipulate"
  ]
  node [
    id 1115
    label "assume"
  ]
  node [
    id 1116
    label "podj&#261;&#263;"
  ]
  node [
    id 1117
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 1118
    label "skuma&#263;"
  ]
  node [
    id 1119
    label "obejmowa&#263;"
  ]
  node [
    id 1120
    label "zagarn&#261;&#263;"
  ]
  node [
    id 1121
    label "obj&#281;cie"
  ]
  node [
    id 1122
    label "involve"
  ]
  node [
    id 1123
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 1124
    label "dotkn&#261;&#263;"
  ]
  node [
    id 1125
    label "dmuchni&#281;cie"
  ]
  node [
    id 1126
    label "niesienie"
  ]
  node [
    id 1127
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 1128
    label "nakazanie"
  ]
  node [
    id 1129
    label "ruszenie"
  ]
  node [
    id 1130
    label "pokonanie"
  ]
  node [
    id 1131
    label "wywiezienie"
  ]
  node [
    id 1132
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 1133
    label "wymienienie_si&#281;"
  ]
  node [
    id 1134
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 1135
    label "uciekni&#281;cie"
  ]
  node [
    id 1136
    label "pobranie"
  ]
  node [
    id 1137
    label "poczytanie"
  ]
  node [
    id 1138
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 1139
    label "pozabieranie"
  ]
  node [
    id 1140
    label "u&#380;ycie"
  ]
  node [
    id 1141
    label "powodzenie"
  ]
  node [
    id 1142
    label "wej&#347;cie"
  ]
  node [
    id 1143
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1144
    label "pickings"
  ]
  node [
    id 1145
    label "zniesienie"
  ]
  node [
    id 1146
    label "kupienie"
  ]
  node [
    id 1147
    label "bite"
  ]
  node [
    id 1148
    label "wyruchanie"
  ]
  node [
    id 1149
    label "odziedziczenie"
  ]
  node [
    id 1150
    label "capture"
  ]
  node [
    id 1151
    label "otrzymanie"
  ]
  node [
    id 1152
    label "wygranie"
  ]
  node [
    id 1153
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1154
    label "udanie_si&#281;"
  ]
  node [
    id 1155
    label "zacz&#281;cie"
  ]
  node [
    id 1156
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 1157
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 1158
    label "zwia&#263;"
  ]
  node [
    id 1159
    label "wypierdoli&#263;"
  ]
  node [
    id 1160
    label "fly"
  ]
  node [
    id 1161
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1162
    label "spieprzy&#263;"
  ]
  node [
    id 1163
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1164
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1165
    label "zaistnie&#263;"
  ]
  node [
    id 1166
    label "ascend"
  ]
  node [
    id 1167
    label "drapn&#261;&#263;_si&#281;"
  ]
  node [
    id 1168
    label "przekroczy&#263;"
  ]
  node [
    id 1169
    label "zag&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 1170
    label "w&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1171
    label "catch"
  ]
  node [
    id 1172
    label "intervene"
  ]
  node [
    id 1173
    label "pozna&#263;"
  ]
  node [
    id 1174
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 1175
    label "wnikn&#261;&#263;"
  ]
  node [
    id 1176
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1177
    label "przenikn&#261;&#263;"
  ]
  node [
    id 1178
    label "doj&#347;&#263;"
  ]
  node [
    id 1179
    label "spotka&#263;"
  ]
  node [
    id 1180
    label "submit"
  ]
  node [
    id 1181
    label "become"
  ]
  node [
    id 1182
    label "zapanowa&#263;"
  ]
  node [
    id 1183
    label "develop"
  ]
  node [
    id 1184
    label "nabawienie_si&#281;"
  ]
  node [
    id 1185
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 1186
    label "zwiastun"
  ]
  node [
    id 1187
    label "doczeka&#263;"
  ]
  node [
    id 1188
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1189
    label "kupi&#263;"
  ]
  node [
    id 1190
    label "wysta&#263;"
  ]
  node [
    id 1191
    label "wystarczy&#263;"
  ]
  node [
    id 1192
    label "naby&#263;"
  ]
  node [
    id 1193
    label "nabawianie_si&#281;"
  ]
  node [
    id 1194
    label "range"
  ]
  node [
    id 1195
    label "osaczy&#263;"
  ]
  node [
    id 1196
    label "okra&#347;&#263;"
  ]
  node [
    id 1197
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 1198
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 1199
    label "obiec"
  ]
  node [
    id 1200
    label "szelma"
  ]
  node [
    id 1201
    label "skurczybyk"
  ]
  node [
    id 1202
    label "cholera"
  ]
  node [
    id 1203
    label "pieron"
  ]
  node [
    id 1204
    label "diabe&#322;"
  ]
  node [
    id 1205
    label "kaduk"
  ]
  node [
    id 1206
    label "chytra_sztuka"
  ]
  node [
    id 1207
    label "diasek"
  ]
  node [
    id 1208
    label "Rokita"
  ]
  node [
    id 1209
    label "bestia"
  ]
  node [
    id 1210
    label "Boruta"
  ]
  node [
    id 1211
    label "istota_fantastyczna"
  ]
  node [
    id 1212
    label "nicpo&#324;"
  ]
  node [
    id 1213
    label "niecnota"
  ]
  node [
    id 1214
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1215
    label "niegrzeczny"
  ]
  node [
    id 1216
    label "niesforny"
  ]
  node [
    id 1217
    label "hultajstwo"
  ]
  node [
    id 1218
    label "oczajdusza"
  ]
  node [
    id 1219
    label "sztuka"
  ]
  node [
    id 1220
    label "istota_&#380;ywa"
  ]
  node [
    id 1221
    label "degenerat"
  ]
  node [
    id 1222
    label "zwyrol"
  ]
  node [
    id 1223
    label "okrutnik"
  ]
  node [
    id 1224
    label "popapraniec"
  ]
  node [
    id 1225
    label "szelmowski"
  ]
  node [
    id 1226
    label "&#380;artowni&#347;"
  ]
  node [
    id 1227
    label "zuchwalec"
  ]
  node [
    id 1228
    label "&#347;mia&#322;ek"
  ]
  node [
    id 1229
    label "spryciarz"
  ]
  node [
    id 1230
    label "piek&#322;o"
  ]
  node [
    id 1231
    label "human_body"
  ]
  node [
    id 1232
    label "ofiarowywanie"
  ]
  node [
    id 1233
    label "sfera_afektywna"
  ]
  node [
    id 1234
    label "nekromancja"
  ]
  node [
    id 1235
    label "Po&#347;wist"
  ]
  node [
    id 1236
    label "podekscytowanie"
  ]
  node [
    id 1237
    label "deformowanie"
  ]
  node [
    id 1238
    label "sumienie"
  ]
  node [
    id 1239
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1240
    label "deformowa&#263;"
  ]
  node [
    id 1241
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1242
    label "psychika"
  ]
  node [
    id 1243
    label "zjawa"
  ]
  node [
    id 1244
    label "zmar&#322;y"
  ]
  node [
    id 1245
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1246
    label "power"
  ]
  node [
    id 1247
    label "entity"
  ]
  node [
    id 1248
    label "ofiarowywa&#263;"
  ]
  node [
    id 1249
    label "oddech"
  ]
  node [
    id 1250
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1251
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1252
    label "byt"
  ]
  node [
    id 1253
    label "si&#322;a"
  ]
  node [
    id 1254
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1255
    label "ego"
  ]
  node [
    id 1256
    label "ofiarowanie"
  ]
  node [
    id 1257
    label "kompleksja"
  ]
  node [
    id 1258
    label "fizjonomia"
  ]
  node [
    id 1259
    label "kompleks"
  ]
  node [
    id 1260
    label "shape"
  ]
  node [
    id 1261
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1262
    label "T&#281;sknica"
  ]
  node [
    id 1263
    label "ofiarowa&#263;"
  ]
  node [
    id 1264
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1265
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1266
    label "passion"
  ]
  node [
    id 1267
    label "epilepsja"
  ]
  node [
    id 1268
    label "maj&#261;tek_spadkowy"
  ]
  node [
    id 1269
    label "&#322;ajdak"
  ]
  node [
    id 1270
    label "holender"
  ]
  node [
    id 1271
    label "chor&#243;bka"
  ]
  node [
    id 1272
    label "przecinkowiec_cholery"
  ]
  node [
    id 1273
    label "choroba_bakteryjna"
  ]
  node [
    id 1274
    label "wyzwisko"
  ]
  node [
    id 1275
    label "gniew"
  ]
  node [
    id 1276
    label "charakternik"
  ]
  node [
    id 1277
    label "cholewa"
  ]
  node [
    id 1278
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 1279
    label "przekle&#324;stwo"
  ]
  node [
    id 1280
    label "fury"
  ]
  node [
    id 1281
    label "choroba"
  ]
  node [
    id 1282
    label "Lucyfer"
  ]
  node [
    id 1283
    label "Mefistofeles"
  ]
  node [
    id 1284
    label "biblizm"
  ]
  node [
    id 1285
    label "z&#322;o"
  ]
  node [
    id 1286
    label "Belzebub"
  ]
  node [
    id 1287
    label "op&#281;tany"
  ]
  node [
    id 1288
    label "cognizance"
  ]
  node [
    id 1289
    label "zmusi&#263;"
  ]
  node [
    id 1290
    label "wymaga&#263;"
  ]
  node [
    id 1291
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 1292
    label "zmusza&#263;"
  ]
  node [
    id 1293
    label "nakazywa&#263;"
  ]
  node [
    id 1294
    label "wyg&#322;osi&#263;"
  ]
  node [
    id 1295
    label "say"
  ]
  node [
    id 1296
    label "command"
  ]
  node [
    id 1297
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1298
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 1299
    label "sandbag"
  ]
  node [
    id 1300
    label "pop&#281;dzi&#263;_kota"
  ]
  node [
    id 1301
    label "zada&#263;_gwa&#322;t"
  ]
  node [
    id 1302
    label "force"
  ]
  node [
    id 1303
    label "claim"
  ]
  node [
    id 1304
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 1305
    label "talk"
  ]
  node [
    id 1306
    label "wypowiada&#263;"
  ]
  node [
    id 1307
    label "poleca&#263;"
  ]
  node [
    id 1308
    label "pakowa&#263;"
  ]
  node [
    id 1309
    label "inflict"
  ]
  node [
    id 1310
    label "odznaka"
  ]
  node [
    id 1311
    label "kawaler"
  ]
  node [
    id 1312
    label "tyrystor"
  ]
  node [
    id 1313
    label "kantor"
  ]
  node [
    id 1314
    label "lada"
  ]
  node [
    id 1315
    label "kapelmistrz"
  ]
  node [
    id 1316
    label "mod&#322;y"
  ]
  node [
    id 1317
    label "kasa"
  ]
  node [
    id 1318
    label "&#347;piewak"
  ]
  node [
    id 1319
    label "waluta"
  ]
  node [
    id 1320
    label "lektor"
  ]
  node [
    id 1321
    label "po&#347;rednik"
  ]
  node [
    id 1322
    label "majufes"
  ]
  node [
    id 1323
    label "surogator"
  ]
  node [
    id 1324
    label "Machiavelli"
  ]
  node [
    id 1325
    label "Sienkiewicz"
  ]
  node [
    id 1326
    label "Andersen"
  ]
  node [
    id 1327
    label "Katon"
  ]
  node [
    id 1328
    label "Gogol"
  ]
  node [
    id 1329
    label "pisarczyk"
  ]
  node [
    id 1330
    label "Iwaszkiewicz"
  ]
  node [
    id 1331
    label "Gombrowicz"
  ]
  node [
    id 1332
    label "Proust"
  ]
  node [
    id 1333
    label "Boy-&#379;ele&#324;ski"
  ]
  node [
    id 1334
    label "Thomas_Mann"
  ]
  node [
    id 1335
    label "Balzak"
  ]
  node [
    id 1336
    label "Reymont"
  ]
  node [
    id 1337
    label "Walter_Scott"
  ]
  node [
    id 1338
    label "Stendhal"
  ]
  node [
    id 1339
    label "Juliusz_Cezar"
  ]
  node [
    id 1340
    label "Flaubert"
  ]
  node [
    id 1341
    label "Tolkien"
  ]
  node [
    id 1342
    label "Brecht"
  ]
  node [
    id 1343
    label "To&#322;stoj"
  ]
  node [
    id 1344
    label "Lem"
  ]
  node [
    id 1345
    label "Orwell"
  ]
  node [
    id 1346
    label "Kafka"
  ]
  node [
    id 1347
    label "Conrad"
  ]
  node [
    id 1348
    label "Voltaire"
  ]
  node [
    id 1349
    label "artysta"
  ]
  node [
    id 1350
    label "Bergson"
  ]
  node [
    id 1351
    label "pracownik"
  ]
  node [
    id 1352
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 1353
    label "pragmatyka"
  ]
  node [
    id 1354
    label "prostownik"
  ]
  node [
    id 1355
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 1356
    label "zaobserwowa&#263;"
  ]
  node [
    id 1357
    label "odczyta&#263;"
  ]
  node [
    id 1358
    label "przetworzy&#263;"
  ]
  node [
    id 1359
    label "przewidzie&#263;"
  ]
  node [
    id 1360
    label "bode"
  ]
  node [
    id 1361
    label "wywnioskowa&#263;"
  ]
  node [
    id 1362
    label "przepowiedzie&#263;"
  ]
  node [
    id 1363
    label "watch"
  ]
  node [
    id 1364
    label "zobaczy&#263;"
  ]
  node [
    id 1365
    label "zinterpretowa&#263;"
  ]
  node [
    id 1366
    label "sprawdzi&#263;"
  ]
  node [
    id 1367
    label "wyzyska&#263;"
  ]
  node [
    id 1368
    label "opracowa&#263;"
  ]
  node [
    id 1369
    label "convert"
  ]
  node [
    id 1370
    label "stworzy&#263;"
  ]
  node [
    id 1371
    label "feel"
  ]
  node [
    id 1372
    label "topographic_point"
  ]
  node [
    id 1373
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1374
    label "visualize"
  ]
  node [
    id 1375
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 1376
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 1377
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1378
    label "teach"
  ]
  node [
    id 1379
    label "experience"
  ]
  node [
    id 1380
    label "gaworzy&#263;"
  ]
  node [
    id 1381
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1382
    label "remark"
  ]
  node [
    id 1383
    label "rozmawia&#263;"
  ]
  node [
    id 1384
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1385
    label "umie&#263;"
  ]
  node [
    id 1386
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1387
    label "dziama&#263;"
  ]
  node [
    id 1388
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1389
    label "dysfonia"
  ]
  node [
    id 1390
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1391
    label "prawi&#263;"
  ]
  node [
    id 1392
    label "powiada&#263;"
  ]
  node [
    id 1393
    label "tell"
  ]
  node [
    id 1394
    label "chew_the_fat"
  ]
  node [
    id 1395
    label "j&#281;zyk"
  ]
  node [
    id 1396
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1397
    label "okre&#347;la&#263;"
  ]
  node [
    id 1398
    label "distribute"
  ]
  node [
    id 1399
    label "bash"
  ]
  node [
    id 1400
    label "decydowa&#263;"
  ]
  node [
    id 1401
    label "signify"
  ]
  node [
    id 1402
    label "style"
  ]
  node [
    id 1403
    label "znaczy&#263;"
  ]
  node [
    id 1404
    label "give_voice"
  ]
  node [
    id 1405
    label "oznacza&#263;"
  ]
  node [
    id 1406
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 1407
    label "represent"
  ]
  node [
    id 1408
    label "arouse"
  ]
  node [
    id 1409
    label "determine"
  ]
  node [
    id 1410
    label "work"
  ]
  node [
    id 1411
    label "reakcja_chemiczna"
  ]
  node [
    id 1412
    label "uwydatnia&#263;"
  ]
  node [
    id 1413
    label "eksploatowa&#263;"
  ]
  node [
    id 1414
    label "uzyskiwa&#263;"
  ]
  node [
    id 1415
    label "wydostawa&#263;"
  ]
  node [
    id 1416
    label "wyjmowa&#263;"
  ]
  node [
    id 1417
    label "train"
  ]
  node [
    id 1418
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 1419
    label "dobywa&#263;"
  ]
  node [
    id 1420
    label "ocala&#263;"
  ]
  node [
    id 1421
    label "excavate"
  ]
  node [
    id 1422
    label "g&#243;rnictwo"
  ]
  node [
    id 1423
    label "can"
  ]
  node [
    id 1424
    label "m&#243;c"
  ]
  node [
    id 1425
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1426
    label "rozumie&#263;"
  ]
  node [
    id 1427
    label "szczeka&#263;"
  ]
  node [
    id 1428
    label "funkcjonowa&#263;"
  ]
  node [
    id 1429
    label "mawia&#263;"
  ]
  node [
    id 1430
    label "opowiada&#263;"
  ]
  node [
    id 1431
    label "chatter"
  ]
  node [
    id 1432
    label "niemowl&#281;"
  ]
  node [
    id 1433
    label "kosmetyk"
  ]
  node [
    id 1434
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 1435
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1436
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1437
    label "artykulator"
  ]
  node [
    id 1438
    label "kod"
  ]
  node [
    id 1439
    label "kawa&#322;ek"
  ]
  node [
    id 1440
    label "przedmiot"
  ]
  node [
    id 1441
    label "gramatyka"
  ]
  node [
    id 1442
    label "przet&#322;umaczenie"
  ]
  node [
    id 1443
    label "formalizowanie"
  ]
  node [
    id 1444
    label "ssa&#263;"
  ]
  node [
    id 1445
    label "ssanie"
  ]
  node [
    id 1446
    label "language"
  ]
  node [
    id 1447
    label "liza&#263;"
  ]
  node [
    id 1448
    label "konsonantyzm"
  ]
  node [
    id 1449
    label "wokalizm"
  ]
  node [
    id 1450
    label "fonetyka"
  ]
  node [
    id 1451
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1452
    label "jeniec"
  ]
  node [
    id 1453
    label "but"
  ]
  node [
    id 1454
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1455
    label "po_koroniarsku"
  ]
  node [
    id 1456
    label "kultura_duchowa"
  ]
  node [
    id 1457
    label "t&#322;umaczenie"
  ]
  node [
    id 1458
    label "m&#243;wienie"
  ]
  node [
    id 1459
    label "pype&#263;"
  ]
  node [
    id 1460
    label "lizanie"
  ]
  node [
    id 1461
    label "pismo"
  ]
  node [
    id 1462
    label "formalizowa&#263;"
  ]
  node [
    id 1463
    label "organ"
  ]
  node [
    id 1464
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1465
    label "rozumienie"
  ]
  node [
    id 1466
    label "makroglosja"
  ]
  node [
    id 1467
    label "jama_ustna"
  ]
  node [
    id 1468
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1469
    label "formacja_geologiczna"
  ]
  node [
    id 1470
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1471
    label "s&#322;ownictwo"
  ]
  node [
    id 1472
    label "dysphonia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 245
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 26
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 963
  ]
  edge [
    source 20
    target 964
  ]
  edge [
    source 20
    target 965
  ]
  edge [
    source 20
    target 966
  ]
  edge [
    source 20
    target 967
  ]
  edge [
    source 20
    target 968
  ]
  edge [
    source 20
    target 345
  ]
  edge [
    source 20
    target 969
  ]
  edge [
    source 20
    target 427
  ]
  edge [
    source 20
    target 970
  ]
  edge [
    source 20
    target 971
  ]
  edge [
    source 20
    target 972
  ]
  edge [
    source 20
    target 64
  ]
  edge [
    source 20
    target 973
  ]
  edge [
    source 20
    target 974
  ]
  edge [
    source 20
    target 975
  ]
  edge [
    source 20
    target 976
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 977
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 978
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 980
  ]
  edge [
    source 23
    target 981
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 982
  ]
  edge [
    source 24
    target 983
  ]
  edge [
    source 24
    target 536
  ]
  edge [
    source 24
    target 984
  ]
  edge [
    source 24
    target 985
  ]
  edge [
    source 24
    target 986
  ]
  edge [
    source 24
    target 987
  ]
  edge [
    source 24
    target 569
  ]
  edge [
    source 24
    target 988
  ]
  edge [
    source 24
    target 989
  ]
  edge [
    source 24
    target 441
  ]
  edge [
    source 24
    target 990
  ]
  edge [
    source 24
    target 991
  ]
  edge [
    source 24
    target 295
  ]
  edge [
    source 24
    target 992
  ]
  edge [
    source 24
    target 993
  ]
  edge [
    source 24
    target 994
  ]
  edge [
    source 24
    target 995
  ]
  edge [
    source 24
    target 996
  ]
  edge [
    source 24
    target 997
  ]
  edge [
    source 24
    target 998
  ]
  edge [
    source 24
    target 999
  ]
  edge [
    source 24
    target 1000
  ]
  edge [
    source 24
    target 1001
  ]
  edge [
    source 24
    target 1002
  ]
  edge [
    source 24
    target 1003
  ]
  edge [
    source 24
    target 1004
  ]
  edge [
    source 24
    target 1005
  ]
  edge [
    source 24
    target 1006
  ]
  edge [
    source 24
    target 1007
  ]
  edge [
    source 24
    target 558
  ]
  edge [
    source 24
    target 1008
  ]
  edge [
    source 24
    target 1009
  ]
  edge [
    source 24
    target 1010
  ]
  edge [
    source 24
    target 1011
  ]
  edge [
    source 24
    target 1012
  ]
  edge [
    source 24
    target 1013
  ]
  edge [
    source 24
    target 1014
  ]
  edge [
    source 24
    target 1015
  ]
  edge [
    source 24
    target 1016
  ]
  edge [
    source 24
    target 1017
  ]
  edge [
    source 24
    target 1018
  ]
  edge [
    source 24
    target 307
  ]
  edge [
    source 24
    target 1019
  ]
  edge [
    source 24
    target 1020
  ]
  edge [
    source 24
    target 1021
  ]
  edge [
    source 24
    target 471
  ]
  edge [
    source 24
    target 1022
  ]
  edge [
    source 24
    target 1023
  ]
  edge [
    source 24
    target 1024
  ]
  edge [
    source 24
    target 1025
  ]
  edge [
    source 24
    target 1026
  ]
  edge [
    source 24
    target 1027
  ]
  edge [
    source 24
    target 1028
  ]
  edge [
    source 24
    target 1029
  ]
  edge [
    source 24
    target 1030
  ]
  edge [
    source 24
    target 1031
  ]
  edge [
    source 24
    target 490
  ]
  edge [
    source 24
    target 1032
  ]
  edge [
    source 24
    target 1033
  ]
  edge [
    source 24
    target 1034
  ]
  edge [
    source 24
    target 1035
  ]
  edge [
    source 24
    target 1036
  ]
  edge [
    source 24
    target 1037
  ]
  edge [
    source 24
    target 1038
  ]
  edge [
    source 24
    target 1039
  ]
  edge [
    source 24
    target 1040
  ]
  edge [
    source 24
    target 1041
  ]
  edge [
    source 24
    target 1042
  ]
  edge [
    source 24
    target 1043
  ]
  edge [
    source 24
    target 1044
  ]
  edge [
    source 24
    target 1045
  ]
  edge [
    source 24
    target 1046
  ]
  edge [
    source 24
    target 1047
  ]
  edge [
    source 24
    target 1048
  ]
  edge [
    source 24
    target 1049
  ]
  edge [
    source 24
    target 1050
  ]
  edge [
    source 24
    target 1051
  ]
  edge [
    source 24
    target 1052
  ]
  edge [
    source 24
    target 1053
  ]
  edge [
    source 24
    target 322
  ]
  edge [
    source 24
    target 1054
  ]
  edge [
    source 24
    target 1055
  ]
  edge [
    source 24
    target 1056
  ]
  edge [
    source 24
    target 1057
  ]
  edge [
    source 24
    target 1058
  ]
  edge [
    source 24
    target 1059
  ]
  edge [
    source 24
    target 300
  ]
  edge [
    source 24
    target 280
  ]
  edge [
    source 24
    target 1060
  ]
  edge [
    source 24
    target 1061
  ]
  edge [
    source 24
    target 572
  ]
  edge [
    source 24
    target 1062
  ]
  edge [
    source 24
    target 1063
  ]
  edge [
    source 24
    target 1064
  ]
  edge [
    source 24
    target 1065
  ]
  edge [
    source 24
    target 1066
  ]
  edge [
    source 24
    target 1067
  ]
  edge [
    source 24
    target 1068
  ]
  edge [
    source 24
    target 1069
  ]
  edge [
    source 24
    target 1070
  ]
  edge [
    source 24
    target 1071
  ]
  edge [
    source 24
    target 1072
  ]
  edge [
    source 24
    target 527
  ]
  edge [
    source 24
    target 1073
  ]
  edge [
    source 24
    target 1074
  ]
  edge [
    source 24
    target 1075
  ]
  edge [
    source 24
    target 1076
  ]
  edge [
    source 24
    target 1077
  ]
  edge [
    source 24
    target 1078
  ]
  edge [
    source 24
    target 1079
  ]
  edge [
    source 24
    target 1080
  ]
  edge [
    source 24
    target 1081
  ]
  edge [
    source 24
    target 1082
  ]
  edge [
    source 24
    target 1083
  ]
  edge [
    source 24
    target 1084
  ]
  edge [
    source 24
    target 1085
  ]
  edge [
    source 24
    target 1086
  ]
  edge [
    source 24
    target 1087
  ]
  edge [
    source 24
    target 1088
  ]
  edge [
    source 24
    target 588
  ]
  edge [
    source 24
    target 589
  ]
  edge [
    source 24
    target 590
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 24
    target 1090
  ]
  edge [
    source 24
    target 1091
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 1093
  ]
  edge [
    source 24
    target 1094
  ]
  edge [
    source 24
    target 1095
  ]
  edge [
    source 24
    target 1096
  ]
  edge [
    source 24
    target 1097
  ]
  edge [
    source 24
    target 1098
  ]
  edge [
    source 24
    target 1099
  ]
  edge [
    source 24
    target 1100
  ]
  edge [
    source 24
    target 1101
  ]
  edge [
    source 24
    target 1102
  ]
  edge [
    source 24
    target 1103
  ]
  edge [
    source 24
    target 1104
  ]
  edge [
    source 24
    target 1105
  ]
  edge [
    source 24
    target 1106
  ]
  edge [
    source 24
    target 1107
  ]
  edge [
    source 24
    target 1108
  ]
  edge [
    source 24
    target 1109
  ]
  edge [
    source 24
    target 1110
  ]
  edge [
    source 24
    target 1111
  ]
  edge [
    source 24
    target 1112
  ]
  edge [
    source 24
    target 1113
  ]
  edge [
    source 24
    target 1114
  ]
  edge [
    source 24
    target 1115
  ]
  edge [
    source 24
    target 1116
  ]
  edge [
    source 24
    target 1117
  ]
  edge [
    source 24
    target 1118
  ]
  edge [
    source 24
    target 1119
  ]
  edge [
    source 24
    target 1120
  ]
  edge [
    source 24
    target 1121
  ]
  edge [
    source 24
    target 1122
  ]
  edge [
    source 24
    target 1123
  ]
  edge [
    source 24
    target 1124
  ]
  edge [
    source 24
    target 1125
  ]
  edge [
    source 24
    target 1126
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 24
    target 71
  ]
  edge [
    source 24
    target 1148
  ]
  edge [
    source 24
    target 1149
  ]
  edge [
    source 24
    target 1150
  ]
  edge [
    source 24
    target 1151
  ]
  edge [
    source 24
    target 563
  ]
  edge [
    source 24
    target 1152
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 24
    target 1154
  ]
  edge [
    source 24
    target 1155
  ]
  edge [
    source 24
    target 1156
  ]
  edge [
    source 24
    target 84
  ]
  edge [
    source 24
    target 291
  ]
  edge [
    source 24
    target 533
  ]
  edge [
    source 24
    target 534
  ]
  edge [
    source 24
    target 535
  ]
  edge [
    source 24
    target 537
  ]
  edge [
    source 24
    target 538
  ]
  edge [
    source 24
    target 539
  ]
  edge [
    source 24
    target 540
  ]
  edge [
    source 24
    target 298
  ]
  edge [
    source 24
    target 541
  ]
  edge [
    source 24
    target 384
  ]
  edge [
    source 24
    target 542
  ]
  edge [
    source 24
    target 543
  ]
  edge [
    source 24
    target 544
  ]
  edge [
    source 24
    target 545
  ]
  edge [
    source 24
    target 546
  ]
  edge [
    source 24
    target 547
  ]
  edge [
    source 24
    target 548
  ]
  edge [
    source 24
    target 549
  ]
  edge [
    source 24
    target 550
  ]
  edge [
    source 24
    target 551
  ]
  edge [
    source 24
    target 552
  ]
  edge [
    source 24
    target 553
  ]
  edge [
    source 24
    target 554
  ]
  edge [
    source 24
    target 555
  ]
  edge [
    source 24
    target 556
  ]
  edge [
    source 24
    target 557
  ]
  edge [
    source 24
    target 446
  ]
  edge [
    source 24
    target 559
  ]
  edge [
    source 24
    target 560
  ]
  edge [
    source 24
    target 561
  ]
  edge [
    source 24
    target 562
  ]
  edge [
    source 24
    target 437
  ]
  edge [
    source 24
    target 526
  ]
  edge [
    source 24
    target 564
  ]
  edge [
    source 24
    target 565
  ]
  edge [
    source 24
    target 566
  ]
  edge [
    source 24
    target 567
  ]
  edge [
    source 24
    target 568
  ]
  edge [
    source 24
    target 1157
  ]
  edge [
    source 24
    target 1158
  ]
  edge [
    source 24
    target 1159
  ]
  edge [
    source 24
    target 1160
  ]
  edge [
    source 24
    target 1161
  ]
  edge [
    source 24
    target 1162
  ]
  edge [
    source 24
    target 525
  ]
  edge [
    source 24
    target 1163
  ]
  edge [
    source 24
    target 514
  ]
  edge [
    source 24
    target 1164
  ]
  edge [
    source 24
    target 1165
  ]
  edge [
    source 24
    target 1166
  ]
  edge [
    source 24
    target 1167
  ]
  edge [
    source 24
    target 1168
  ]
  edge [
    source 24
    target 1169
  ]
  edge [
    source 24
    target 1170
  ]
  edge [
    source 24
    target 1171
  ]
  edge [
    source 24
    target 1172
  ]
  edge [
    source 24
    target 1173
  ]
  edge [
    source 24
    target 1174
  ]
  edge [
    source 24
    target 1175
  ]
  edge [
    source 24
    target 1176
  ]
  edge [
    source 24
    target 1177
  ]
  edge [
    source 24
    target 1178
  ]
  edge [
    source 24
    target 1179
  ]
  edge [
    source 24
    target 1180
  ]
  edge [
    source 24
    target 1181
  ]
  edge [
    source 24
    target 1182
  ]
  edge [
    source 24
    target 1183
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 1184
  ]
  edge [
    source 24
    target 1185
  ]
  edge [
    source 24
    target 1186
  ]
  edge [
    source 24
    target 1187
  ]
  edge [
    source 24
    target 1188
  ]
  edge [
    source 24
    target 1189
  ]
  edge [
    source 24
    target 1190
  ]
  edge [
    source 24
    target 1191
  ]
  edge [
    source 24
    target 1192
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1200
  ]
  edge [
    source 28
    target 1201
  ]
  edge [
    source 28
    target 1202
  ]
  edge [
    source 28
    target 1203
  ]
  edge [
    source 28
    target 1204
  ]
  edge [
    source 28
    target 1205
  ]
  edge [
    source 28
    target 182
  ]
  edge [
    source 28
    target 1206
  ]
  edge [
    source 28
    target 1207
  ]
  edge [
    source 28
    target 1208
  ]
  edge [
    source 28
    target 1209
  ]
  edge [
    source 28
    target 1210
  ]
  edge [
    source 28
    target 1211
  ]
  edge [
    source 28
    target 1212
  ]
  edge [
    source 28
    target 1213
  ]
  edge [
    source 28
    target 154
  ]
  edge [
    source 28
    target 1214
  ]
  edge [
    source 28
    target 1215
  ]
  edge [
    source 28
    target 1216
  ]
  edge [
    source 28
    target 1217
  ]
  edge [
    source 28
    target 1218
  ]
  edge [
    source 28
    target 1219
  ]
  edge [
    source 28
    target 1220
  ]
  edge [
    source 28
    target 1221
  ]
  edge [
    source 28
    target 1222
  ]
  edge [
    source 28
    target 1223
  ]
  edge [
    source 28
    target 711
  ]
  edge [
    source 28
    target 1224
  ]
  edge [
    source 28
    target 1225
  ]
  edge [
    source 28
    target 1226
  ]
  edge [
    source 28
    target 1227
  ]
  edge [
    source 28
    target 1228
  ]
  edge [
    source 28
    target 1229
  ]
  edge [
    source 28
    target 1230
  ]
  edge [
    source 28
    target 1231
  ]
  edge [
    source 28
    target 1232
  ]
  edge [
    source 28
    target 1233
  ]
  edge [
    source 28
    target 1234
  ]
  edge [
    source 28
    target 1235
  ]
  edge [
    source 28
    target 100
  ]
  edge [
    source 28
    target 1236
  ]
  edge [
    source 28
    target 1237
  ]
  edge [
    source 28
    target 1238
  ]
  edge [
    source 28
    target 1239
  ]
  edge [
    source 28
    target 1240
  ]
  edge [
    source 28
    target 1241
  ]
  edge [
    source 28
    target 1242
  ]
  edge [
    source 28
    target 1243
  ]
  edge [
    source 28
    target 1244
  ]
  edge [
    source 28
    target 1245
  ]
  edge [
    source 28
    target 1246
  ]
  edge [
    source 28
    target 1247
  ]
  edge [
    source 28
    target 1248
  ]
  edge [
    source 28
    target 1249
  ]
  edge [
    source 28
    target 1250
  ]
  edge [
    source 28
    target 1251
  ]
  edge [
    source 28
    target 1252
  ]
  edge [
    source 28
    target 1253
  ]
  edge [
    source 28
    target 1254
  ]
  edge [
    source 28
    target 1255
  ]
  edge [
    source 28
    target 1256
  ]
  edge [
    source 28
    target 1257
  ]
  edge [
    source 28
    target 375
  ]
  edge [
    source 28
    target 1258
  ]
  edge [
    source 28
    target 1259
  ]
  edge [
    source 28
    target 1260
  ]
  edge [
    source 28
    target 1261
  ]
  edge [
    source 28
    target 180
  ]
  edge [
    source 28
    target 1262
  ]
  edge [
    source 28
    target 1263
  ]
  edge [
    source 28
    target 1264
  ]
  edge [
    source 28
    target 184
  ]
  edge [
    source 28
    target 1265
  ]
  edge [
    source 28
    target 1266
  ]
  edge [
    source 28
    target 1267
  ]
  edge [
    source 28
    target 1268
  ]
  edge [
    source 28
    target 1269
  ]
  edge [
    source 28
    target 1270
  ]
  edge [
    source 28
    target 1271
  ]
  edge [
    source 28
    target 1272
  ]
  edge [
    source 28
    target 1273
  ]
  edge [
    source 28
    target 1274
  ]
  edge [
    source 28
    target 1275
  ]
  edge [
    source 28
    target 1276
  ]
  edge [
    source 28
    target 1277
  ]
  edge [
    source 28
    target 1278
  ]
  edge [
    source 28
    target 1279
  ]
  edge [
    source 28
    target 1280
  ]
  edge [
    source 28
    target 1281
  ]
  edge [
    source 28
    target 1282
  ]
  edge [
    source 28
    target 1283
  ]
  edge [
    source 28
    target 1284
  ]
  edge [
    source 28
    target 1285
  ]
  edge [
    source 28
    target 1286
  ]
  edge [
    source 28
    target 1287
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1288
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 30
    target 569
  ]
  edge [
    source 30
    target 1289
  ]
  edge [
    source 30
    target 1290
  ]
  edge [
    source 30
    target 1291
  ]
  edge [
    source 30
    target 1292
  ]
  edge [
    source 30
    target 1293
  ]
  edge [
    source 30
    target 1294
  ]
  edge [
    source 30
    target 1295
  ]
  edge [
    source 30
    target 1296
  ]
  edge [
    source 30
    target 589
  ]
  edge [
    source 30
    target 1297
  ]
  edge [
    source 30
    target 1298
  ]
  edge [
    source 30
    target 1299
  ]
  edge [
    source 30
    target 335
  ]
  edge [
    source 30
    target 1300
  ]
  edge [
    source 30
    target 1301
  ]
  edge [
    source 30
    target 1056
  ]
  edge [
    source 30
    target 1065
  ]
  edge [
    source 30
    target 1302
  ]
  edge [
    source 30
    target 384
  ]
  edge [
    source 30
    target 1303
  ]
  edge [
    source 30
    target 1304
  ]
  edge [
    source 30
    target 536
  ]
  edge [
    source 30
    target 1305
  ]
  edge [
    source 30
    target 1306
  ]
  edge [
    source 30
    target 572
  ]
  edge [
    source 30
    target 1307
  ]
  edge [
    source 30
    target 1308
  ]
  edge [
    source 30
    target 1309
  ]
  edge [
    source 30
    target 588
  ]
  edge [
    source 30
    target 590
  ]
  edge [
    source 30
    target 1310
  ]
  edge [
    source 30
    target 1311
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 691
  ]
  edge [
    source 31
    target 1312
  ]
  edge [
    source 31
    target 1313
  ]
  edge [
    source 31
    target 342
  ]
  edge [
    source 31
    target 1314
  ]
  edge [
    source 31
    target 257
  ]
  edge [
    source 31
    target 657
  ]
  edge [
    source 31
    target 1315
  ]
  edge [
    source 31
    target 202
  ]
  edge [
    source 31
    target 1316
  ]
  edge [
    source 31
    target 1317
  ]
  edge [
    source 31
    target 1318
  ]
  edge [
    source 31
    target 1319
  ]
  edge [
    source 31
    target 1320
  ]
  edge [
    source 31
    target 1321
  ]
  edge [
    source 31
    target 1322
  ]
  edge [
    source 31
    target 1323
  ]
  edge [
    source 31
    target 154
  ]
  edge [
    source 31
    target 1324
  ]
  edge [
    source 31
    target 1325
  ]
  edge [
    source 31
    target 1326
  ]
  edge [
    source 31
    target 1327
  ]
  edge [
    source 31
    target 1328
  ]
  edge [
    source 31
    target 1329
  ]
  edge [
    source 31
    target 1330
  ]
  edge [
    source 31
    target 1331
  ]
  edge [
    source 31
    target 1332
  ]
  edge [
    source 31
    target 1333
  ]
  edge [
    source 31
    target 1334
  ]
  edge [
    source 31
    target 1335
  ]
  edge [
    source 31
    target 1336
  ]
  edge [
    source 31
    target 1337
  ]
  edge [
    source 31
    target 1338
  ]
  edge [
    source 31
    target 1339
  ]
  edge [
    source 31
    target 1340
  ]
  edge [
    source 31
    target 1341
  ]
  edge [
    source 31
    target 1342
  ]
  edge [
    source 31
    target 1343
  ]
  edge [
    source 31
    target 1344
  ]
  edge [
    source 31
    target 1345
  ]
  edge [
    source 31
    target 1346
  ]
  edge [
    source 31
    target 1347
  ]
  edge [
    source 31
    target 1348
  ]
  edge [
    source 31
    target 1349
  ]
  edge [
    source 31
    target 1350
  ]
  edge [
    source 31
    target 1351
  ]
  edge [
    source 31
    target 1352
  ]
  edge [
    source 31
    target 1353
  ]
  edge [
    source 31
    target 1354
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1173
  ]
  edge [
    source 32
    target 1355
  ]
  edge [
    source 32
    target 1356
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 1357
  ]
  edge [
    source 32
    target 1358
  ]
  edge [
    source 32
    target 1359
  ]
  edge [
    source 32
    target 1360
  ]
  edge [
    source 32
    target 1361
  ]
  edge [
    source 32
    target 1362
  ]
  edge [
    source 32
    target 1363
  ]
  edge [
    source 32
    target 1364
  ]
  edge [
    source 32
    target 1365
  ]
  edge [
    source 32
    target 1366
  ]
  edge [
    source 32
    target 580
  ]
  edge [
    source 32
    target 1367
  ]
  edge [
    source 32
    target 1368
  ]
  edge [
    source 32
    target 1369
  ]
  edge [
    source 32
    target 1370
  ]
  edge [
    source 32
    target 1067
  ]
  edge [
    source 32
    target 1068
  ]
  edge [
    source 32
    target 1371
  ]
  edge [
    source 32
    target 1372
  ]
  edge [
    source 32
    target 1373
  ]
  edge [
    source 32
    target 1374
  ]
  edge [
    source 32
    target 1095
  ]
  edge [
    source 32
    target 1375
  ]
  edge [
    source 32
    target 1376
  ]
  edge [
    source 32
    target 1377
  ]
  edge [
    source 32
    target 1378
  ]
  edge [
    source 32
    target 1379
  ]
  edge [
    source 33
    target 1380
  ]
  edge [
    source 33
    target 1381
  ]
  edge [
    source 33
    target 1382
  ]
  edge [
    source 33
    target 1383
  ]
  edge [
    source 33
    target 1384
  ]
  edge [
    source 33
    target 1385
  ]
  edge [
    source 33
    target 1386
  ]
  edge [
    source 33
    target 1387
  ]
  edge [
    source 33
    target 1388
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 1389
  ]
  edge [
    source 33
    target 581
  ]
  edge [
    source 33
    target 1390
  ]
  edge [
    source 33
    target 1305
  ]
  edge [
    source 33
    target 557
  ]
  edge [
    source 33
    target 1391
  ]
  edge [
    source 33
    target 1297
  ]
  edge [
    source 33
    target 1392
  ]
  edge [
    source 33
    target 1393
  ]
  edge [
    source 33
    target 1394
  ]
  edge [
    source 33
    target 1295
  ]
  edge [
    source 33
    target 1395
  ]
  edge [
    source 33
    target 1396
  ]
  edge [
    source 33
    target 310
  ]
  edge [
    source 33
    target 338
  ]
  edge [
    source 33
    target 1397
  ]
  edge [
    source 33
    target 535
  ]
  edge [
    source 33
    target 1398
  ]
  edge [
    source 33
    target 304
  ]
  edge [
    source 33
    target 1399
  ]
  edge [
    source 33
    target 1092
  ]
  edge [
    source 33
    target 389
  ]
  edge [
    source 33
    target 1400
  ]
  edge [
    source 33
    target 1401
  ]
  edge [
    source 33
    target 1402
  ]
  edge [
    source 33
    target 335
  ]
  edge [
    source 33
    target 311
  ]
  edge [
    source 33
    target 303
  ]
  edge [
    source 33
    target 1403
  ]
  edge [
    source 33
    target 1404
  ]
  edge [
    source 33
    target 1405
  ]
  edge [
    source 33
    target 1406
  ]
  edge [
    source 33
    target 1407
  ]
  edge [
    source 33
    target 312
  ]
  edge [
    source 33
    target 1408
  ]
  edge [
    source 33
    target 291
  ]
  edge [
    source 33
    target 1409
  ]
  edge [
    source 33
    target 1410
  ]
  edge [
    source 33
    target 1411
  ]
  edge [
    source 33
    target 1412
  ]
  edge [
    source 33
    target 1413
  ]
  edge [
    source 33
    target 1414
  ]
  edge [
    source 33
    target 1415
  ]
  edge [
    source 33
    target 1416
  ]
  edge [
    source 33
    target 1417
  ]
  edge [
    source 33
    target 1418
  ]
  edge [
    source 33
    target 315
  ]
  edge [
    source 33
    target 1419
  ]
  edge [
    source 33
    target 1420
  ]
  edge [
    source 33
    target 1421
  ]
  edge [
    source 33
    target 1422
  ]
  edge [
    source 33
    target 298
  ]
  edge [
    source 33
    target 1423
  ]
  edge [
    source 33
    target 1424
  ]
  edge [
    source 33
    target 1425
  ]
  edge [
    source 33
    target 1426
  ]
  edge [
    source 33
    target 1427
  ]
  edge [
    source 33
    target 1428
  ]
  edge [
    source 33
    target 1429
  ]
  edge [
    source 33
    target 1430
  ]
  edge [
    source 33
    target 1431
  ]
  edge [
    source 33
    target 1432
  ]
  edge [
    source 33
    target 1433
  ]
  edge [
    source 33
    target 1434
  ]
  edge [
    source 33
    target 1435
  ]
  edge [
    source 33
    target 1436
  ]
  edge [
    source 33
    target 1437
  ]
  edge [
    source 33
    target 1438
  ]
  edge [
    source 33
    target 1439
  ]
  edge [
    source 33
    target 1440
  ]
  edge [
    source 33
    target 369
  ]
  edge [
    source 33
    target 1441
  ]
  edge [
    source 33
    target 371
  ]
  edge [
    source 33
    target 1442
  ]
  edge [
    source 33
    target 1443
  ]
  edge [
    source 33
    target 1444
  ]
  edge [
    source 33
    target 1445
  ]
  edge [
    source 33
    target 1446
  ]
  edge [
    source 33
    target 1447
  ]
  edge [
    source 33
    target 358
  ]
  edge [
    source 33
    target 1448
  ]
  edge [
    source 33
    target 1449
  ]
  edge [
    source 33
    target 1450
  ]
  edge [
    source 33
    target 1451
  ]
  edge [
    source 33
    target 1452
  ]
  edge [
    source 33
    target 1453
  ]
  edge [
    source 33
    target 1454
  ]
  edge [
    source 33
    target 1455
  ]
  edge [
    source 33
    target 1456
  ]
  edge [
    source 33
    target 1457
  ]
  edge [
    source 33
    target 1458
  ]
  edge [
    source 33
    target 1459
  ]
  edge [
    source 33
    target 1460
  ]
  edge [
    source 33
    target 1461
  ]
  edge [
    source 33
    target 1462
  ]
  edge [
    source 33
    target 1463
  ]
  edge [
    source 33
    target 1464
  ]
  edge [
    source 33
    target 1465
  ]
  edge [
    source 33
    target 368
  ]
  edge [
    source 33
    target 1466
  ]
  edge [
    source 33
    target 1467
  ]
  edge [
    source 33
    target 1468
  ]
  edge [
    source 33
    target 1469
  ]
  edge [
    source 33
    target 1470
  ]
  edge [
    source 33
    target 376
  ]
  edge [
    source 33
    target 1471
  ]
  edge [
    source 33
    target 108
  ]
  edge [
    source 33
    target 1472
  ]
  edge [
    source 33
    target 364
  ]
]
