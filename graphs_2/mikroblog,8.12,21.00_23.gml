graph [
  node [
    id 0
    label "madka"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "muszy"
    origin "text"
  ]
  node [
    id 3
    label "czekac"
    origin "text"
  ]
  node [
    id 4
    label "kazdy"
    origin "text"
  ]
  node [
    id 5
    label "kolejka"
    origin "text"
  ]
  node [
    id 6
    label "utulenie"
  ]
  node [
    id 7
    label "pediatra"
  ]
  node [
    id 8
    label "dzieciak"
  ]
  node [
    id 9
    label "utulanie"
  ]
  node [
    id 10
    label "dzieciarnia"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "niepe&#322;noletni"
  ]
  node [
    id 13
    label "organizm"
  ]
  node [
    id 14
    label "utula&#263;"
  ]
  node [
    id 15
    label "cz&#322;owieczek"
  ]
  node [
    id 16
    label "fledgling"
  ]
  node [
    id 17
    label "zwierz&#281;"
  ]
  node [
    id 18
    label "utuli&#263;"
  ]
  node [
    id 19
    label "m&#322;odzik"
  ]
  node [
    id 20
    label "pedofil"
  ]
  node [
    id 21
    label "m&#322;odziak"
  ]
  node [
    id 22
    label "potomek"
  ]
  node [
    id 23
    label "entliczek-pentliczek"
  ]
  node [
    id 24
    label "potomstwo"
  ]
  node [
    id 25
    label "sraluch"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "czeladka"
  ]
  node [
    id 28
    label "dzietno&#347;&#263;"
  ]
  node [
    id 29
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 30
    label "bawienie_si&#281;"
  ]
  node [
    id 31
    label "pomiot"
  ]
  node [
    id 32
    label "grupa"
  ]
  node [
    id 33
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 34
    label "kinderbal"
  ]
  node [
    id 35
    label "krewny"
  ]
  node [
    id 36
    label "ludzko&#347;&#263;"
  ]
  node [
    id 37
    label "asymilowanie"
  ]
  node [
    id 38
    label "wapniak"
  ]
  node [
    id 39
    label "asymilowa&#263;"
  ]
  node [
    id 40
    label "os&#322;abia&#263;"
  ]
  node [
    id 41
    label "posta&#263;"
  ]
  node [
    id 42
    label "hominid"
  ]
  node [
    id 43
    label "podw&#322;adny"
  ]
  node [
    id 44
    label "os&#322;abianie"
  ]
  node [
    id 45
    label "g&#322;owa"
  ]
  node [
    id 46
    label "figura"
  ]
  node [
    id 47
    label "portrecista"
  ]
  node [
    id 48
    label "dwun&#243;g"
  ]
  node [
    id 49
    label "profanum"
  ]
  node [
    id 50
    label "mikrokosmos"
  ]
  node [
    id 51
    label "nasada"
  ]
  node [
    id 52
    label "duch"
  ]
  node [
    id 53
    label "antropochoria"
  ]
  node [
    id 54
    label "osoba"
  ]
  node [
    id 55
    label "wz&#243;r"
  ]
  node [
    id 56
    label "senior"
  ]
  node [
    id 57
    label "oddzia&#322;ywanie"
  ]
  node [
    id 58
    label "Adam"
  ]
  node [
    id 59
    label "homo_sapiens"
  ]
  node [
    id 60
    label "polifag"
  ]
  node [
    id 61
    label "ma&#322;oletny"
  ]
  node [
    id 62
    label "m&#322;ody"
  ]
  node [
    id 63
    label "p&#322;aszczyzna"
  ]
  node [
    id 64
    label "odwadnia&#263;"
  ]
  node [
    id 65
    label "przyswoi&#263;"
  ]
  node [
    id 66
    label "sk&#243;ra"
  ]
  node [
    id 67
    label "odwodni&#263;"
  ]
  node [
    id 68
    label "ewoluowanie"
  ]
  node [
    id 69
    label "staw"
  ]
  node [
    id 70
    label "ow&#322;osienie"
  ]
  node [
    id 71
    label "unerwienie"
  ]
  node [
    id 72
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 73
    label "reakcja"
  ]
  node [
    id 74
    label "wyewoluowanie"
  ]
  node [
    id 75
    label "przyswajanie"
  ]
  node [
    id 76
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 77
    label "wyewoluowa&#263;"
  ]
  node [
    id 78
    label "miejsce"
  ]
  node [
    id 79
    label "biorytm"
  ]
  node [
    id 80
    label "ewoluowa&#263;"
  ]
  node [
    id 81
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 82
    label "istota_&#380;ywa"
  ]
  node [
    id 83
    label "otworzy&#263;"
  ]
  node [
    id 84
    label "otwiera&#263;"
  ]
  node [
    id 85
    label "czynnik_biotyczny"
  ]
  node [
    id 86
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 87
    label "otworzenie"
  ]
  node [
    id 88
    label "otwieranie"
  ]
  node [
    id 89
    label "individual"
  ]
  node [
    id 90
    label "szkielet"
  ]
  node [
    id 91
    label "ty&#322;"
  ]
  node [
    id 92
    label "obiekt"
  ]
  node [
    id 93
    label "przyswaja&#263;"
  ]
  node [
    id 94
    label "przyswojenie"
  ]
  node [
    id 95
    label "odwadnianie"
  ]
  node [
    id 96
    label "odwodnienie"
  ]
  node [
    id 97
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 98
    label "starzenie_si&#281;"
  ]
  node [
    id 99
    label "prz&#243;d"
  ]
  node [
    id 100
    label "uk&#322;ad"
  ]
  node [
    id 101
    label "temperatura"
  ]
  node [
    id 102
    label "l&#281;d&#378;wie"
  ]
  node [
    id 103
    label "cia&#322;o"
  ]
  node [
    id 104
    label "cz&#322;onek"
  ]
  node [
    id 105
    label "degenerat"
  ]
  node [
    id 106
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 107
    label "zwyrol"
  ]
  node [
    id 108
    label "czerniak"
  ]
  node [
    id 109
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 110
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 111
    label "paszcza"
  ]
  node [
    id 112
    label "popapraniec"
  ]
  node [
    id 113
    label "skuba&#263;"
  ]
  node [
    id 114
    label "skubanie"
  ]
  node [
    id 115
    label "agresja"
  ]
  node [
    id 116
    label "skubni&#281;cie"
  ]
  node [
    id 117
    label "zwierz&#281;ta"
  ]
  node [
    id 118
    label "fukni&#281;cie"
  ]
  node [
    id 119
    label "farba"
  ]
  node [
    id 120
    label "fukanie"
  ]
  node [
    id 121
    label "gad"
  ]
  node [
    id 122
    label "tresowa&#263;"
  ]
  node [
    id 123
    label "siedzie&#263;"
  ]
  node [
    id 124
    label "oswaja&#263;"
  ]
  node [
    id 125
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 126
    label "poligamia"
  ]
  node [
    id 127
    label "oz&#243;r"
  ]
  node [
    id 128
    label "skubn&#261;&#263;"
  ]
  node [
    id 129
    label "wios&#322;owa&#263;"
  ]
  node [
    id 130
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 131
    label "le&#380;enie"
  ]
  node [
    id 132
    label "niecz&#322;owiek"
  ]
  node [
    id 133
    label "wios&#322;owanie"
  ]
  node [
    id 134
    label "napasienie_si&#281;"
  ]
  node [
    id 135
    label "wiwarium"
  ]
  node [
    id 136
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 137
    label "animalista"
  ]
  node [
    id 138
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 139
    label "budowa"
  ]
  node [
    id 140
    label "hodowla"
  ]
  node [
    id 141
    label "pasienie_si&#281;"
  ]
  node [
    id 142
    label "sodomita"
  ]
  node [
    id 143
    label "monogamia"
  ]
  node [
    id 144
    label "przyssawka"
  ]
  node [
    id 145
    label "zachowanie"
  ]
  node [
    id 146
    label "budowa_cia&#322;a"
  ]
  node [
    id 147
    label "okrutnik"
  ]
  node [
    id 148
    label "grzbiet"
  ]
  node [
    id 149
    label "weterynarz"
  ]
  node [
    id 150
    label "&#322;eb"
  ]
  node [
    id 151
    label "wylinka"
  ]
  node [
    id 152
    label "bestia"
  ]
  node [
    id 153
    label "poskramia&#263;"
  ]
  node [
    id 154
    label "fauna"
  ]
  node [
    id 155
    label "treser"
  ]
  node [
    id 156
    label "siedzenie"
  ]
  node [
    id 157
    label "le&#380;e&#263;"
  ]
  node [
    id 158
    label "uspokojenie"
  ]
  node [
    id 159
    label "utulenie_si&#281;"
  ]
  node [
    id 160
    label "u&#347;pienie"
  ]
  node [
    id 161
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 162
    label "uspokoi&#263;"
  ]
  node [
    id 163
    label "utulanie_si&#281;"
  ]
  node [
    id 164
    label "usypianie"
  ]
  node [
    id 165
    label "pocieszanie"
  ]
  node [
    id 166
    label "uspokajanie"
  ]
  node [
    id 167
    label "usypia&#263;"
  ]
  node [
    id 168
    label "uspokaja&#263;"
  ]
  node [
    id 169
    label "wyliczanka"
  ]
  node [
    id 170
    label "specjalista"
  ]
  node [
    id 171
    label "harcerz"
  ]
  node [
    id 172
    label "ch&#322;opta&#347;"
  ]
  node [
    id 173
    label "zawodnik"
  ]
  node [
    id 174
    label "go&#322;ow&#261;s"
  ]
  node [
    id 175
    label "m&#322;ode"
  ]
  node [
    id 176
    label "stopie&#324;_harcerski"
  ]
  node [
    id 177
    label "g&#243;wniarz"
  ]
  node [
    id 178
    label "beniaminek"
  ]
  node [
    id 179
    label "dewiant"
  ]
  node [
    id 180
    label "istotka"
  ]
  node [
    id 181
    label "bech"
  ]
  node [
    id 182
    label "dziecinny"
  ]
  node [
    id 183
    label "naiwniak"
  ]
  node [
    id 184
    label "struktura"
  ]
  node [
    id 185
    label "zabawka"
  ]
  node [
    id 186
    label "kolej"
  ]
  node [
    id 187
    label "seria"
  ]
  node [
    id 188
    label "line"
  ]
  node [
    id 189
    label "nagromadzenie"
  ]
  node [
    id 190
    label "porcja"
  ]
  node [
    id 191
    label "rz&#261;d"
  ]
  node [
    id 192
    label "przybli&#380;enie"
  ]
  node [
    id 193
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 194
    label "kategoria"
  ]
  node [
    id 195
    label "szpaler"
  ]
  node [
    id 196
    label "lon&#380;a"
  ]
  node [
    id 197
    label "uporz&#261;dkowanie"
  ]
  node [
    id 198
    label "egzekutywa"
  ]
  node [
    id 199
    label "jednostka_systematyczna"
  ]
  node [
    id 200
    label "instytucja"
  ]
  node [
    id 201
    label "premier"
  ]
  node [
    id 202
    label "Londyn"
  ]
  node [
    id 203
    label "gabinet_cieni"
  ]
  node [
    id 204
    label "gromada"
  ]
  node [
    id 205
    label "number"
  ]
  node [
    id 206
    label "Konsulat"
  ]
  node [
    id 207
    label "tract"
  ]
  node [
    id 208
    label "klasa"
  ]
  node [
    id 209
    label "w&#322;adza"
  ]
  node [
    id 210
    label "accumulation"
  ]
  node [
    id 211
    label "blockage"
  ]
  node [
    id 212
    label "przeszkoda"
  ]
  node [
    id 213
    label "zgromadzenie"
  ]
  node [
    id 214
    label "figura_my&#347;li"
  ]
  node [
    id 215
    label "hookup"
  ]
  node [
    id 216
    label "enumeracja"
  ]
  node [
    id 217
    label "set"
  ]
  node [
    id 218
    label "przebieg"
  ]
  node [
    id 219
    label "jednostka"
  ]
  node [
    id 220
    label "stage_set"
  ]
  node [
    id 221
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 222
    label "d&#378;wi&#281;k"
  ]
  node [
    id 223
    label "komplet"
  ]
  node [
    id 224
    label "sekwencja"
  ]
  node [
    id 225
    label "zestawienie"
  ]
  node [
    id 226
    label "partia"
  ]
  node [
    id 227
    label "produkcja"
  ]
  node [
    id 228
    label "zas&#243;b"
  ]
  node [
    id 229
    label "ilo&#347;&#263;"
  ]
  node [
    id 230
    label "&#380;o&#322;d"
  ]
  node [
    id 231
    label "narz&#281;dzie"
  ]
  node [
    id 232
    label "przedmiot"
  ]
  node [
    id 233
    label "bawid&#322;o"
  ]
  node [
    id 234
    label "frisbee"
  ]
  node [
    id 235
    label "smoczek"
  ]
  node [
    id 236
    label "droga"
  ]
  node [
    id 237
    label "wagon"
  ]
  node [
    id 238
    label "lokomotywa"
  ]
  node [
    id 239
    label "trakcja"
  ]
  node [
    id 240
    label "run"
  ]
  node [
    id 241
    label "blokada"
  ]
  node [
    id 242
    label "kolejno&#347;&#263;"
  ]
  node [
    id 243
    label "tor"
  ]
  node [
    id 244
    label "pojazd_kolejowy"
  ]
  node [
    id 245
    label "linia"
  ]
  node [
    id 246
    label "tender"
  ]
  node [
    id 247
    label "proces"
  ]
  node [
    id 248
    label "cug"
  ]
  node [
    id 249
    label "pocz&#261;tek"
  ]
  node [
    id 250
    label "czas"
  ]
  node [
    id 251
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 252
    label "poci&#261;g"
  ]
  node [
    id 253
    label "cedu&#322;a"
  ]
  node [
    id 254
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 255
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 256
    label "nast&#281;pstwo"
  ]
  node [
    id 257
    label "mechanika"
  ]
  node [
    id 258
    label "o&#347;"
  ]
  node [
    id 259
    label "usenet"
  ]
  node [
    id 260
    label "rozprz&#261;c"
  ]
  node [
    id 261
    label "cybernetyk"
  ]
  node [
    id 262
    label "podsystem"
  ]
  node [
    id 263
    label "system"
  ]
  node [
    id 264
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 265
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 266
    label "sk&#322;ad"
  ]
  node [
    id 267
    label "systemat"
  ]
  node [
    id 268
    label "cecha"
  ]
  node [
    id 269
    label "konstrukcja"
  ]
  node [
    id 270
    label "konstelacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 270
  ]
]
