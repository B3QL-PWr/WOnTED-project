graph [
  node [
    id 0
    label "arabski"
    origin "text"
  ]
  node [
    id 1
    label "ulotka"
    origin "text"
  ]
  node [
    id 2
    label "d&#380;ellaba"
  ]
  node [
    id 3
    label "&#380;uaw"
  ]
  node [
    id 4
    label "abaja"
  ]
  node [
    id 5
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 6
    label "harira"
  ]
  node [
    id 7
    label "typowy"
  ]
  node [
    id 8
    label "arabsko"
  ]
  node [
    id 9
    label "Arabic"
  ]
  node [
    id 10
    label "po_arabsku"
  ]
  node [
    id 11
    label "agal"
  ]
  node [
    id 12
    label "druz"
  ]
  node [
    id 13
    label "j&#281;zyk"
  ]
  node [
    id 14
    label "j&#281;zyk_semicki"
  ]
  node [
    id 15
    label "taniec_brzucha"
  ]
  node [
    id 16
    label "sznurek"
  ]
  node [
    id 17
    label "arafatka"
  ]
  node [
    id 18
    label "zupa"
  ]
  node [
    id 19
    label "we&#322;niany"
  ]
  node [
    id 20
    label "okrycie"
  ]
  node [
    id 21
    label "r&#281;kaw"
  ]
  node [
    id 22
    label "kaptur"
  ]
  node [
    id 23
    label "bersalier"
  ]
  node [
    id 24
    label "hajduk"
  ]
  node [
    id 25
    label "kosynier"
  ]
  node [
    id 26
    label "dragon"
  ]
  node [
    id 27
    label "jegier"
  ]
  node [
    id 28
    label "piechur"
  ]
  node [
    id 29
    label "tarczownik"
  ]
  node [
    id 30
    label "pikinier"
  ]
  node [
    id 31
    label "drab"
  ]
  node [
    id 32
    label "sekciarz"
  ]
  node [
    id 33
    label "druzowie"
  ]
  node [
    id 34
    label "bliskowschodni"
  ]
  node [
    id 35
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 36
    label "artykulator"
  ]
  node [
    id 37
    label "kod"
  ]
  node [
    id 38
    label "kawa&#322;ek"
  ]
  node [
    id 39
    label "przedmiot"
  ]
  node [
    id 40
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 41
    label "gramatyka"
  ]
  node [
    id 42
    label "stylik"
  ]
  node [
    id 43
    label "przet&#322;umaczenie"
  ]
  node [
    id 44
    label "formalizowanie"
  ]
  node [
    id 45
    label "ssanie"
  ]
  node [
    id 46
    label "ssa&#263;"
  ]
  node [
    id 47
    label "language"
  ]
  node [
    id 48
    label "liza&#263;"
  ]
  node [
    id 49
    label "napisa&#263;"
  ]
  node [
    id 50
    label "konsonantyzm"
  ]
  node [
    id 51
    label "wokalizm"
  ]
  node [
    id 52
    label "pisa&#263;"
  ]
  node [
    id 53
    label "fonetyka"
  ]
  node [
    id 54
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 55
    label "jeniec"
  ]
  node [
    id 56
    label "but"
  ]
  node [
    id 57
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 58
    label "po_koroniarsku"
  ]
  node [
    id 59
    label "kultura_duchowa"
  ]
  node [
    id 60
    label "t&#322;umaczenie"
  ]
  node [
    id 61
    label "m&#243;wienie"
  ]
  node [
    id 62
    label "pype&#263;"
  ]
  node [
    id 63
    label "lizanie"
  ]
  node [
    id 64
    label "pismo"
  ]
  node [
    id 65
    label "formalizowa&#263;"
  ]
  node [
    id 66
    label "rozumie&#263;"
  ]
  node [
    id 67
    label "organ"
  ]
  node [
    id 68
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 69
    label "rozumienie"
  ]
  node [
    id 70
    label "spos&#243;b"
  ]
  node [
    id 71
    label "makroglosja"
  ]
  node [
    id 72
    label "m&#243;wi&#263;"
  ]
  node [
    id 73
    label "jama_ustna"
  ]
  node [
    id 74
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 75
    label "formacja_geologiczna"
  ]
  node [
    id 76
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 77
    label "natural_language"
  ]
  node [
    id 78
    label "s&#322;ownictwo"
  ]
  node [
    id 79
    label "urz&#261;dzenie"
  ]
  node [
    id 80
    label "zwyczajny"
  ]
  node [
    id 81
    label "typowo"
  ]
  node [
    id 82
    label "cz&#281;sty"
  ]
  node [
    id 83
    label "zwyk&#322;y"
  ]
  node [
    id 84
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 85
    label "nale&#380;ny"
  ]
  node [
    id 86
    label "nale&#380;yty"
  ]
  node [
    id 87
    label "uprawniony"
  ]
  node [
    id 88
    label "zasadniczy"
  ]
  node [
    id 89
    label "stosownie"
  ]
  node [
    id 90
    label "taki"
  ]
  node [
    id 91
    label "charakterystyczny"
  ]
  node [
    id 92
    label "prawdziwy"
  ]
  node [
    id 93
    label "ten"
  ]
  node [
    id 94
    label "dobry"
  ]
  node [
    id 95
    label "druk_ulotny"
  ]
  node [
    id 96
    label "reklama"
  ]
  node [
    id 97
    label "booklet"
  ]
  node [
    id 98
    label "copywriting"
  ]
  node [
    id 99
    label "wypromowa&#263;"
  ]
  node [
    id 100
    label "brief"
  ]
  node [
    id 101
    label "samplowanie"
  ]
  node [
    id 102
    label "akcja"
  ]
  node [
    id 103
    label "promowa&#263;"
  ]
  node [
    id 104
    label "bran&#380;a"
  ]
  node [
    id 105
    label "tekst"
  ]
  node [
    id 106
    label "informacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
]
