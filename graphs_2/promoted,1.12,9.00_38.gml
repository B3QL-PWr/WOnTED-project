graph [
  node [
    id 0
    label "trzeba"
    origin "text"
  ]
  node [
    id 1
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "siebie"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "skala"
    origin "text"
  ]
  node [
    id 5
    label "europa"
    origin "text"
  ]
  node [
    id 6
    label "kredyt"
    origin "text"
  ]
  node [
    id 7
    label "mieszkaniowy"
    origin "text"
  ]
  node [
    id 8
    label "polska"
    origin "text"
  ]
  node [
    id 9
    label "nadal"
    origin "text"
  ]
  node [
    id 10
    label "cechowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "wysoki"
    origin "text"
  ]
  node [
    id 14
    label "oprocentowanie"
    origin "text"
  ]
  node [
    id 15
    label "necessity"
  ]
  node [
    id 16
    label "trza"
  ]
  node [
    id 17
    label "oddawa&#263;"
  ]
  node [
    id 18
    label "zalicza&#263;"
  ]
  node [
    id 19
    label "render"
  ]
  node [
    id 20
    label "sk&#322;ada&#263;"
  ]
  node [
    id 21
    label "bequeath"
  ]
  node [
    id 22
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 23
    label "zostawia&#263;"
  ]
  node [
    id 24
    label "powierza&#263;"
  ]
  node [
    id 25
    label "opowiada&#263;"
  ]
  node [
    id 26
    label "convey"
  ]
  node [
    id 27
    label "impart"
  ]
  node [
    id 28
    label "przekazywa&#263;"
  ]
  node [
    id 29
    label "dostarcza&#263;"
  ]
  node [
    id 30
    label "sacrifice"
  ]
  node [
    id 31
    label "dawa&#263;"
  ]
  node [
    id 32
    label "odst&#281;powa&#263;"
  ]
  node [
    id 33
    label "sprzedawa&#263;"
  ]
  node [
    id 34
    label "give"
  ]
  node [
    id 35
    label "reflect"
  ]
  node [
    id 36
    label "surrender"
  ]
  node [
    id 37
    label "deliver"
  ]
  node [
    id 38
    label "odpowiada&#263;"
  ]
  node [
    id 39
    label "umieszcza&#263;"
  ]
  node [
    id 40
    label "blurt_out"
  ]
  node [
    id 41
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 42
    label "przedstawia&#263;"
  ]
  node [
    id 43
    label "yield"
  ]
  node [
    id 44
    label "robi&#263;"
  ]
  node [
    id 45
    label "opuszcza&#263;"
  ]
  node [
    id 46
    label "g&#243;rowa&#263;"
  ]
  node [
    id 47
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 48
    label "wydawa&#263;"
  ]
  node [
    id 49
    label "tworzy&#263;"
  ]
  node [
    id 50
    label "wk&#322;ada&#263;"
  ]
  node [
    id 51
    label "pomija&#263;"
  ]
  node [
    id 52
    label "doprowadza&#263;"
  ]
  node [
    id 53
    label "zachowywa&#263;"
  ]
  node [
    id 54
    label "zabiera&#263;"
  ]
  node [
    id 55
    label "zamierza&#263;"
  ]
  node [
    id 56
    label "liszy&#263;"
  ]
  node [
    id 57
    label "zrywa&#263;"
  ]
  node [
    id 58
    label "porzuca&#263;"
  ]
  node [
    id 59
    label "base_on_balls"
  ]
  node [
    id 60
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 61
    label "permit"
  ]
  node [
    id 62
    label "powodowa&#263;"
  ]
  node [
    id 63
    label "wyznacza&#263;"
  ]
  node [
    id 64
    label "rezygnowa&#263;"
  ]
  node [
    id 65
    label "krzywdzi&#263;"
  ]
  node [
    id 66
    label "wywi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 67
    label "bra&#263;"
  ]
  node [
    id 68
    label "mark"
  ]
  node [
    id 69
    label "number"
  ]
  node [
    id 70
    label "stwierdza&#263;"
  ]
  node [
    id 71
    label "odb&#281;bnia&#263;"
  ]
  node [
    id 72
    label "wlicza&#263;"
  ]
  node [
    id 73
    label "przyporz&#261;dkowywa&#263;"
  ]
  node [
    id 74
    label "wyznawa&#263;"
  ]
  node [
    id 75
    label "confide"
  ]
  node [
    id 76
    label "zleca&#263;"
  ]
  node [
    id 77
    label "ufa&#263;"
  ]
  node [
    id 78
    label "command"
  ]
  node [
    id 79
    label "grant"
  ]
  node [
    id 80
    label "zbiera&#263;"
  ]
  node [
    id 81
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 82
    label "przywraca&#263;"
  ]
  node [
    id 83
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 84
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 85
    label "publicize"
  ]
  node [
    id 86
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 87
    label "uk&#322;ada&#263;"
  ]
  node [
    id 88
    label "opracowywa&#263;"
  ]
  node [
    id 89
    label "set"
  ]
  node [
    id 90
    label "train"
  ]
  node [
    id 91
    label "zmienia&#263;"
  ]
  node [
    id 92
    label "dzieli&#263;"
  ]
  node [
    id 93
    label "scala&#263;"
  ]
  node [
    id 94
    label "zestaw"
  ]
  node [
    id 95
    label "zaczyna&#263;"
  ]
  node [
    id 96
    label "set_about"
  ]
  node [
    id 97
    label "wchodzi&#263;"
  ]
  node [
    id 98
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 99
    label "submit"
  ]
  node [
    id 100
    label "prawi&#263;"
  ]
  node [
    id 101
    label "relate"
  ]
  node [
    id 102
    label "kognicja"
  ]
  node [
    id 103
    label "object"
  ]
  node [
    id 104
    label "rozprawa"
  ]
  node [
    id 105
    label "temat"
  ]
  node [
    id 106
    label "wydarzenie"
  ]
  node [
    id 107
    label "szczeg&#243;&#322;"
  ]
  node [
    id 108
    label "proposition"
  ]
  node [
    id 109
    label "przes&#322;anka"
  ]
  node [
    id 110
    label "rzecz"
  ]
  node [
    id 111
    label "idea"
  ]
  node [
    id 112
    label "przebiec"
  ]
  node [
    id 113
    label "charakter"
  ]
  node [
    id 114
    label "czynno&#347;&#263;"
  ]
  node [
    id 115
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 116
    label "motyw"
  ]
  node [
    id 117
    label "przebiegni&#281;cie"
  ]
  node [
    id 118
    label "fabu&#322;a"
  ]
  node [
    id 119
    label "ideologia"
  ]
  node [
    id 120
    label "byt"
  ]
  node [
    id 121
    label "intelekt"
  ]
  node [
    id 122
    label "Kant"
  ]
  node [
    id 123
    label "p&#322;&#243;d"
  ]
  node [
    id 124
    label "cel"
  ]
  node [
    id 125
    label "poj&#281;cie"
  ]
  node [
    id 126
    label "istota"
  ]
  node [
    id 127
    label "pomys&#322;"
  ]
  node [
    id 128
    label "ideacja"
  ]
  node [
    id 129
    label "przedmiot"
  ]
  node [
    id 130
    label "wpadni&#281;cie"
  ]
  node [
    id 131
    label "mienie"
  ]
  node [
    id 132
    label "przyroda"
  ]
  node [
    id 133
    label "obiekt"
  ]
  node [
    id 134
    label "kultura"
  ]
  node [
    id 135
    label "wpa&#347;&#263;"
  ]
  node [
    id 136
    label "wpadanie"
  ]
  node [
    id 137
    label "wpada&#263;"
  ]
  node [
    id 138
    label "s&#261;d"
  ]
  node [
    id 139
    label "rozumowanie"
  ]
  node [
    id 140
    label "opracowanie"
  ]
  node [
    id 141
    label "proces"
  ]
  node [
    id 142
    label "obrady"
  ]
  node [
    id 143
    label "cytat"
  ]
  node [
    id 144
    label "tekst"
  ]
  node [
    id 145
    label "obja&#347;nienie"
  ]
  node [
    id 146
    label "s&#261;dzenie"
  ]
  node [
    id 147
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 148
    label "niuansowa&#263;"
  ]
  node [
    id 149
    label "element"
  ]
  node [
    id 150
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 151
    label "sk&#322;adnik"
  ]
  node [
    id 152
    label "zniuansowa&#263;"
  ]
  node [
    id 153
    label "fakt"
  ]
  node [
    id 154
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 155
    label "przyczyna"
  ]
  node [
    id 156
    label "wnioskowanie"
  ]
  node [
    id 157
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 158
    label "wyraz_pochodny"
  ]
  node [
    id 159
    label "zboczenie"
  ]
  node [
    id 160
    label "om&#243;wienie"
  ]
  node [
    id 161
    label "cecha"
  ]
  node [
    id 162
    label "omawia&#263;"
  ]
  node [
    id 163
    label "fraza"
  ]
  node [
    id 164
    label "tre&#347;&#263;"
  ]
  node [
    id 165
    label "entity"
  ]
  node [
    id 166
    label "forum"
  ]
  node [
    id 167
    label "topik"
  ]
  node [
    id 168
    label "tematyka"
  ]
  node [
    id 169
    label "w&#261;tek"
  ]
  node [
    id 170
    label "zbaczanie"
  ]
  node [
    id 171
    label "forma"
  ]
  node [
    id 172
    label "om&#243;wi&#263;"
  ]
  node [
    id 173
    label "omawianie"
  ]
  node [
    id 174
    label "melodia"
  ]
  node [
    id 175
    label "otoczka"
  ]
  node [
    id 176
    label "zbacza&#263;"
  ]
  node [
    id 177
    label "zboczy&#263;"
  ]
  node [
    id 178
    label "masztab"
  ]
  node [
    id 179
    label "kreska"
  ]
  node [
    id 180
    label "podzia&#322;ka"
  ]
  node [
    id 181
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 182
    label "wielko&#347;&#263;"
  ]
  node [
    id 183
    label "zero"
  ]
  node [
    id 184
    label "interwa&#322;"
  ]
  node [
    id 185
    label "przymiar"
  ]
  node [
    id 186
    label "struktura"
  ]
  node [
    id 187
    label "sfera"
  ]
  node [
    id 188
    label "jednostka"
  ]
  node [
    id 189
    label "zbi&#243;r"
  ]
  node [
    id 190
    label "dominanta"
  ]
  node [
    id 191
    label "tetrachord"
  ]
  node [
    id 192
    label "scale"
  ]
  node [
    id 193
    label "przedzia&#322;"
  ]
  node [
    id 194
    label "podzakres"
  ]
  node [
    id 195
    label "proporcja"
  ]
  node [
    id 196
    label "dziedzina"
  ]
  node [
    id 197
    label "part"
  ]
  node [
    id 198
    label "rejestr"
  ]
  node [
    id 199
    label "subdominanta"
  ]
  node [
    id 200
    label "punkt"
  ]
  node [
    id 201
    label "liczba"
  ]
  node [
    id 202
    label "ilo&#347;&#263;"
  ]
  node [
    id 203
    label "ciura"
  ]
  node [
    id 204
    label "cyfra"
  ]
  node [
    id 205
    label "miernota"
  ]
  node [
    id 206
    label "g&#243;wno"
  ]
  node [
    id 207
    label "love"
  ]
  node [
    id 208
    label "brak"
  ]
  node [
    id 209
    label "odst&#281;p"
  ]
  node [
    id 210
    label "podzia&#322;"
  ]
  node [
    id 211
    label "konwersja"
  ]
  node [
    id 212
    label "znak_pisarski"
  ]
  node [
    id 213
    label "linia"
  ]
  node [
    id 214
    label "podkre&#347;la&#263;"
  ]
  node [
    id 215
    label "spos&#243;b"
  ]
  node [
    id 216
    label "rysunek"
  ]
  node [
    id 217
    label "znak_graficzny"
  ]
  node [
    id 218
    label "nie&#347;ci&#261;galno&#347;&#263;"
  ]
  node [
    id 219
    label "point"
  ]
  node [
    id 220
    label "dzia&#322;ka"
  ]
  node [
    id 221
    label "podkre&#347;lanie"
  ]
  node [
    id 222
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 223
    label "znacznik"
  ]
  node [
    id 224
    label "podkre&#347;lenie"
  ]
  node [
    id 225
    label "podkre&#347;li&#263;"
  ]
  node [
    id 226
    label "zobowi&#261;zanie"
  ]
  node [
    id 227
    label "system_dur-moll"
  ]
  node [
    id 228
    label "d&#378;wi&#281;k"
  ]
  node [
    id 229
    label "subdominant"
  ]
  node [
    id 230
    label "catalog"
  ]
  node [
    id 231
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 232
    label "przycisk"
  ]
  node [
    id 233
    label "pozycja"
  ]
  node [
    id 234
    label "stock"
  ]
  node [
    id 235
    label "regestr"
  ]
  node [
    id 236
    label "sumariusz"
  ]
  node [
    id 237
    label "procesor"
  ]
  node [
    id 238
    label "brzmienie"
  ]
  node [
    id 239
    label "figurowa&#263;"
  ]
  node [
    id 240
    label "book"
  ]
  node [
    id 241
    label "wyliczanka"
  ]
  node [
    id 242
    label "urz&#261;dzenie"
  ]
  node [
    id 243
    label "organy"
  ]
  node [
    id 244
    label "ton"
  ]
  node [
    id 245
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 246
    label "ambitus"
  ]
  node [
    id 247
    label "miejsce"
  ]
  node [
    id 248
    label "abcug"
  ]
  node [
    id 249
    label "miara_tendencji_centralnej"
  ]
  node [
    id 250
    label "wyr&#243;&#380;nia&#263;_si&#281;"
  ]
  node [
    id 251
    label "wyr&#243;&#380;nianie_si&#281;"
  ]
  node [
    id 252
    label "stopie&#324;"
  ]
  node [
    id 253
    label "dominant"
  ]
  node [
    id 254
    label "tr&#243;jd&#378;wi&#281;k"
  ]
  node [
    id 255
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 256
    label "zakres"
  ]
  node [
    id 257
    label "przegroda"
  ]
  node [
    id 258
    label "miejsce_le&#380;&#261;ce"
  ]
  node [
    id 259
    label "przerwa"
  ]
  node [
    id 260
    label "pomieszczenie"
  ]
  node [
    id 261
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 262
    label "farewell"
  ]
  node [
    id 263
    label "przyswoi&#263;"
  ]
  node [
    id 264
    label "ludzko&#347;&#263;"
  ]
  node [
    id 265
    label "one"
  ]
  node [
    id 266
    label "ewoluowanie"
  ]
  node [
    id 267
    label "supremum"
  ]
  node [
    id 268
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 269
    label "przyswajanie"
  ]
  node [
    id 270
    label "wyewoluowanie"
  ]
  node [
    id 271
    label "reakcja"
  ]
  node [
    id 272
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 273
    label "przeliczy&#263;"
  ]
  node [
    id 274
    label "wyewoluowa&#263;"
  ]
  node [
    id 275
    label "ewoluowa&#263;"
  ]
  node [
    id 276
    label "matematyka"
  ]
  node [
    id 277
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 278
    label "rzut"
  ]
  node [
    id 279
    label "liczba_naturalna"
  ]
  node [
    id 280
    label "czynnik_biotyczny"
  ]
  node [
    id 281
    label "g&#322;owa"
  ]
  node [
    id 282
    label "figura"
  ]
  node [
    id 283
    label "individual"
  ]
  node [
    id 284
    label "portrecista"
  ]
  node [
    id 285
    label "przyswaja&#263;"
  ]
  node [
    id 286
    label "przyswojenie"
  ]
  node [
    id 287
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 288
    label "profanum"
  ]
  node [
    id 289
    label "mikrokosmos"
  ]
  node [
    id 290
    label "starzenie_si&#281;"
  ]
  node [
    id 291
    label "duch"
  ]
  node [
    id 292
    label "przeliczanie"
  ]
  node [
    id 293
    label "osoba"
  ]
  node [
    id 294
    label "oddzia&#322;ywanie"
  ]
  node [
    id 295
    label "antropochoria"
  ]
  node [
    id 296
    label "funkcja"
  ]
  node [
    id 297
    label "homo_sapiens"
  ]
  node [
    id 298
    label "przelicza&#263;"
  ]
  node [
    id 299
    label "infimum"
  ]
  node [
    id 300
    label "przeliczenie"
  ]
  node [
    id 301
    label "miara"
  ]
  node [
    id 302
    label "pr&#281;t"
  ]
  node [
    id 303
    label "ta&#347;ma"
  ]
  node [
    id 304
    label "standard"
  ]
  node [
    id 305
    label "mechanika"
  ]
  node [
    id 306
    label "o&#347;"
  ]
  node [
    id 307
    label "usenet"
  ]
  node [
    id 308
    label "rozprz&#261;c"
  ]
  node [
    id 309
    label "zachowanie"
  ]
  node [
    id 310
    label "cybernetyk"
  ]
  node [
    id 311
    label "podsystem"
  ]
  node [
    id 312
    label "system"
  ]
  node [
    id 313
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 314
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 315
    label "sk&#322;ad"
  ]
  node [
    id 316
    label "systemat"
  ]
  node [
    id 317
    label "konstrukcja"
  ]
  node [
    id 318
    label "konstelacja"
  ]
  node [
    id 319
    label "wyraz_skrajny"
  ]
  node [
    id 320
    label "porz&#261;dek"
  ]
  node [
    id 321
    label "stosunek"
  ]
  node [
    id 322
    label "relationship"
  ]
  node [
    id 323
    label "iloraz"
  ]
  node [
    id 324
    label "warunek_lokalowy"
  ]
  node [
    id 325
    label "rozmiar"
  ]
  node [
    id 326
    label "rzadko&#347;&#263;"
  ]
  node [
    id 327
    label "zaleta"
  ]
  node [
    id 328
    label "measure"
  ]
  node [
    id 329
    label "znaczenie"
  ]
  node [
    id 330
    label "opinia"
  ]
  node [
    id 331
    label "dymensja"
  ]
  node [
    id 332
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 333
    label "zdolno&#347;&#263;"
  ]
  node [
    id 334
    label "potencja"
  ]
  node [
    id 335
    label "property"
  ]
  node [
    id 336
    label "egzemplarz"
  ]
  node [
    id 337
    label "series"
  ]
  node [
    id 338
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 339
    label "uprawianie"
  ]
  node [
    id 340
    label "praca_rolnicza"
  ]
  node [
    id 341
    label "collection"
  ]
  node [
    id 342
    label "dane"
  ]
  node [
    id 343
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 344
    label "pakiet_klimatyczny"
  ]
  node [
    id 345
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 346
    label "sum"
  ]
  node [
    id 347
    label "gathering"
  ]
  node [
    id 348
    label "album"
  ]
  node [
    id 349
    label "plecionka"
  ]
  node [
    id 350
    label "parciak"
  ]
  node [
    id 351
    label "p&#322;&#243;tno"
  ]
  node [
    id 352
    label "mapa"
  ]
  node [
    id 353
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 354
    label "bezdro&#380;e"
  ]
  node [
    id 355
    label "poddzia&#322;"
  ]
  node [
    id 356
    label "wymiar"
  ]
  node [
    id 357
    label "strefa"
  ]
  node [
    id 358
    label "kula"
  ]
  node [
    id 359
    label "class"
  ]
  node [
    id 360
    label "sector"
  ]
  node [
    id 361
    label "przestrze&#324;"
  ]
  node [
    id 362
    label "p&#243;&#322;kula"
  ]
  node [
    id 363
    label "huczek"
  ]
  node [
    id 364
    label "p&#243;&#322;sfera"
  ]
  node [
    id 365
    label "powierzchnia"
  ]
  node [
    id 366
    label "kolur"
  ]
  node [
    id 367
    label "grupa"
  ]
  node [
    id 368
    label "borg"
  ]
  node [
    id 369
    label "pasywa"
  ]
  node [
    id 370
    label "odsetki"
  ]
  node [
    id 371
    label "konto"
  ]
  node [
    id 372
    label "konsolidacja"
  ]
  node [
    id 373
    label "rata"
  ]
  node [
    id 374
    label "aktywa"
  ]
  node [
    id 375
    label "zapa&#347;&#263;_kredytowa"
  ]
  node [
    id 376
    label "d&#322;ug"
  ]
  node [
    id 377
    label "arrozacja"
  ]
  node [
    id 378
    label "sp&#322;ata"
  ]
  node [
    id 379
    label "linia_kredytowa"
  ]
  node [
    id 380
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 381
    label "dorobek"
  ]
  node [
    id 382
    label "subkonto"
  ]
  node [
    id 383
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 384
    label "debet"
  ]
  node [
    id 385
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 386
    label "kariera"
  ]
  node [
    id 387
    label "reprezentacja"
  ]
  node [
    id 388
    label "bank"
  ]
  node [
    id 389
    label "dost&#281;p"
  ]
  node [
    id 390
    label "rachunek"
  ]
  node [
    id 391
    label "bilans_ksi&#281;gowy"
  ]
  node [
    id 392
    label "fuzja"
  ]
  node [
    id 393
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 394
    label "annuita"
  ]
  node [
    id 395
    label "kwota"
  ]
  node [
    id 396
    label "zmiana"
  ]
  node [
    id 397
    label "doch&#243;d"
  ]
  node [
    id 398
    label "baza_odsetkowa"
  ]
  node [
    id 399
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 400
    label "skupienie"
  ]
  node [
    id 401
    label "fusion"
  ]
  node [
    id 402
    label "proces_konsolidacyjny"
  ]
  node [
    id 403
    label "zamiana"
  ]
  node [
    id 404
    label "consolidation"
  ]
  node [
    id 405
    label "zjawisko"
  ]
  node [
    id 406
    label "indebtedness"
  ]
  node [
    id 407
    label "stosunek_prawny"
  ]
  node [
    id 408
    label "oblig"
  ]
  node [
    id 409
    label "uregulowa&#263;"
  ]
  node [
    id 410
    label "oddzia&#322;anie"
  ]
  node [
    id 411
    label "occupation"
  ]
  node [
    id 412
    label "duty"
  ]
  node [
    id 413
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 414
    label "zapowied&#378;"
  ]
  node [
    id 415
    label "obowi&#261;zek"
  ]
  node [
    id 416
    label "statement"
  ]
  node [
    id 417
    label "zapewnienie"
  ]
  node [
    id 418
    label "oznacza&#263;"
  ]
  node [
    id 419
    label "by&#263;"
  ]
  node [
    id 420
    label "wyraz"
  ]
  node [
    id 421
    label "wskazywa&#263;"
  ]
  node [
    id 422
    label "signify"
  ]
  node [
    id 423
    label "represent"
  ]
  node [
    id 424
    label "ustala&#263;"
  ]
  node [
    id 425
    label "stanowi&#263;"
  ]
  node [
    id 426
    label "okre&#347;la&#263;"
  ]
  node [
    id 427
    label "wyrafinowany"
  ]
  node [
    id 428
    label "niepo&#347;ledni"
  ]
  node [
    id 429
    label "du&#380;y"
  ]
  node [
    id 430
    label "chwalebny"
  ]
  node [
    id 431
    label "z_wysoka"
  ]
  node [
    id 432
    label "wznios&#322;y"
  ]
  node [
    id 433
    label "daleki"
  ]
  node [
    id 434
    label "wysoce"
  ]
  node [
    id 435
    label "szczytnie"
  ]
  node [
    id 436
    label "znaczny"
  ]
  node [
    id 437
    label "warto&#347;ciowy"
  ]
  node [
    id 438
    label "wysoko"
  ]
  node [
    id 439
    label "uprzywilejowany"
  ]
  node [
    id 440
    label "doros&#322;y"
  ]
  node [
    id 441
    label "niema&#322;o"
  ]
  node [
    id 442
    label "wiele"
  ]
  node [
    id 443
    label "rozwini&#281;ty"
  ]
  node [
    id 444
    label "dorodny"
  ]
  node [
    id 445
    label "wa&#380;ny"
  ]
  node [
    id 446
    label "prawdziwy"
  ]
  node [
    id 447
    label "du&#380;o"
  ]
  node [
    id 448
    label "szczeg&#243;lny"
  ]
  node [
    id 449
    label "lekki"
  ]
  node [
    id 450
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 451
    label "znacznie"
  ]
  node [
    id 452
    label "zauwa&#380;alny"
  ]
  node [
    id 453
    label "niez&#322;y"
  ]
  node [
    id 454
    label "niepo&#347;lednio"
  ]
  node [
    id 455
    label "wyj&#261;tkowy"
  ]
  node [
    id 456
    label "pochwalny"
  ]
  node [
    id 457
    label "wspania&#322;y"
  ]
  node [
    id 458
    label "szlachetny"
  ]
  node [
    id 459
    label "powa&#380;ny"
  ]
  node [
    id 460
    label "chwalebnie"
  ]
  node [
    id 461
    label "podnios&#322;y"
  ]
  node [
    id 462
    label "wznio&#347;le"
  ]
  node [
    id 463
    label "oderwany"
  ]
  node [
    id 464
    label "pi&#281;kny"
  ]
  node [
    id 465
    label "rewaluowanie"
  ]
  node [
    id 466
    label "warto&#347;ciowo"
  ]
  node [
    id 467
    label "drogi"
  ]
  node [
    id 468
    label "u&#380;yteczny"
  ]
  node [
    id 469
    label "zrewaluowanie"
  ]
  node [
    id 470
    label "dobry"
  ]
  node [
    id 471
    label "obyty"
  ]
  node [
    id 472
    label "wykwintny"
  ]
  node [
    id 473
    label "wyrafinowanie"
  ]
  node [
    id 474
    label "wymy&#347;lny"
  ]
  node [
    id 475
    label "dawny"
  ]
  node [
    id 476
    label "ogl&#281;dny"
  ]
  node [
    id 477
    label "d&#322;ugi"
  ]
  node [
    id 478
    label "daleko"
  ]
  node [
    id 479
    label "odleg&#322;y"
  ]
  node [
    id 480
    label "zwi&#261;zany"
  ]
  node [
    id 481
    label "r&#243;&#380;ny"
  ]
  node [
    id 482
    label "s&#322;aby"
  ]
  node [
    id 483
    label "odlegle"
  ]
  node [
    id 484
    label "oddalony"
  ]
  node [
    id 485
    label "g&#322;&#281;boki"
  ]
  node [
    id 486
    label "obcy"
  ]
  node [
    id 487
    label "nieobecny"
  ]
  node [
    id 488
    label "przysz&#322;y"
  ]
  node [
    id 489
    label "g&#243;rno"
  ]
  node [
    id 490
    label "szczytny"
  ]
  node [
    id 491
    label "intensywnie"
  ]
  node [
    id 492
    label "wielki"
  ]
  node [
    id 493
    label "niezmiernie"
  ]
  node [
    id 494
    label "powi&#281;kszenie"
  ]
  node [
    id 495
    label "pastime"
  ]
  node [
    id 496
    label "income"
  ]
  node [
    id 497
    label "stopa_procentowa"
  ]
  node [
    id 498
    label "krzywa_Engla"
  ]
  node [
    id 499
    label "korzy&#347;&#263;"
  ]
  node [
    id 500
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 501
    label "wp&#322;yw"
  ]
  node [
    id 502
    label "wi&#281;kszy"
  ]
  node [
    id 503
    label "powi&#281;kszenie_si&#281;"
  ]
  node [
    id 504
    label "extension"
  ]
  node [
    id 505
    label "zmienienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 14
    target 397
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 398
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 368
  ]
  edge [
    source 14
    target 369
  ]
  edge [
    source 14
    target 370
  ]
  edge [
    source 14
    target 371
  ]
  edge [
    source 14
    target 372
  ]
  edge [
    source 14
    target 373
  ]
  edge [
    source 14
    target 374
  ]
  edge [
    source 14
    target 375
  ]
  edge [
    source 14
    target 376
  ]
  edge [
    source 14
    target 377
  ]
  edge [
    source 14
    target 378
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 226
  ]
]
