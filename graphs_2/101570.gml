graph [
  node [
    id 0
    label "rzym"
    origin "text"
  ]
  node [
    id 1
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 2
    label "Nowy_Rok"
  ]
  node [
    id 3
    label "miesi&#261;c"
  ]
  node [
    id 4
    label "tydzie&#324;"
  ]
  node [
    id 5
    label "miech"
  ]
  node [
    id 6
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 7
    label "czas"
  ]
  node [
    id 8
    label "rok"
  ]
  node [
    id 9
    label "kalendy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
]
