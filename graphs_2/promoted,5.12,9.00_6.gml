graph [
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "uczelnia"
    origin "text"
  ]
  node [
    id 2
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 3
    label "czas"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 5
    label "zaobserwowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "niepokoj&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "zjawisko"
    origin "text"
  ]
  node [
    id 8
    label "jaki"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "coraz"
    origin "text"
  ]
  node [
    id 11
    label "cz&#281;sty"
    origin "text"
  ]
  node [
    id 12
    label "obecno&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "tak_zwany"
    origin "text"
  ]
  node [
    id 14
    label "pseudonauka"
    origin "text"
  ]
  node [
    id 15
    label "uczelniany"
    origin "text"
  ]
  node [
    id 16
    label "mur"
    origin "text"
  ]
  node [
    id 17
    label "przedmiot"
  ]
  node [
    id 18
    label "Polish"
  ]
  node [
    id 19
    label "goniony"
  ]
  node [
    id 20
    label "oberek"
  ]
  node [
    id 21
    label "ryba_po_grecku"
  ]
  node [
    id 22
    label "sztajer"
  ]
  node [
    id 23
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 24
    label "krakowiak"
  ]
  node [
    id 25
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 26
    label "pierogi_ruskie"
  ]
  node [
    id 27
    label "lacki"
  ]
  node [
    id 28
    label "polak"
  ]
  node [
    id 29
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 30
    label "chodzony"
  ]
  node [
    id 31
    label "po_polsku"
  ]
  node [
    id 32
    label "mazur"
  ]
  node [
    id 33
    label "polsko"
  ]
  node [
    id 34
    label "skoczny"
  ]
  node [
    id 35
    label "drabant"
  ]
  node [
    id 36
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 37
    label "j&#281;zyk"
  ]
  node [
    id 38
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 39
    label "artykulator"
  ]
  node [
    id 40
    label "kod"
  ]
  node [
    id 41
    label "kawa&#322;ek"
  ]
  node [
    id 42
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 43
    label "gramatyka"
  ]
  node [
    id 44
    label "stylik"
  ]
  node [
    id 45
    label "przet&#322;umaczenie"
  ]
  node [
    id 46
    label "formalizowanie"
  ]
  node [
    id 47
    label "ssanie"
  ]
  node [
    id 48
    label "ssa&#263;"
  ]
  node [
    id 49
    label "language"
  ]
  node [
    id 50
    label "liza&#263;"
  ]
  node [
    id 51
    label "napisa&#263;"
  ]
  node [
    id 52
    label "konsonantyzm"
  ]
  node [
    id 53
    label "wokalizm"
  ]
  node [
    id 54
    label "pisa&#263;"
  ]
  node [
    id 55
    label "fonetyka"
  ]
  node [
    id 56
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 57
    label "jeniec"
  ]
  node [
    id 58
    label "but"
  ]
  node [
    id 59
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 60
    label "po_koroniarsku"
  ]
  node [
    id 61
    label "kultura_duchowa"
  ]
  node [
    id 62
    label "t&#322;umaczenie"
  ]
  node [
    id 63
    label "m&#243;wienie"
  ]
  node [
    id 64
    label "pype&#263;"
  ]
  node [
    id 65
    label "lizanie"
  ]
  node [
    id 66
    label "pismo"
  ]
  node [
    id 67
    label "formalizowa&#263;"
  ]
  node [
    id 68
    label "rozumie&#263;"
  ]
  node [
    id 69
    label "organ"
  ]
  node [
    id 70
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 71
    label "rozumienie"
  ]
  node [
    id 72
    label "spos&#243;b"
  ]
  node [
    id 73
    label "makroglosja"
  ]
  node [
    id 74
    label "m&#243;wi&#263;"
  ]
  node [
    id 75
    label "jama_ustna"
  ]
  node [
    id 76
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 77
    label "formacja_geologiczna"
  ]
  node [
    id 78
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 79
    label "natural_language"
  ]
  node [
    id 80
    label "s&#322;ownictwo"
  ]
  node [
    id 81
    label "urz&#261;dzenie"
  ]
  node [
    id 82
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 83
    label "wschodnioeuropejski"
  ]
  node [
    id 84
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 85
    label "poga&#324;ski"
  ]
  node [
    id 86
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 87
    label "topielec"
  ]
  node [
    id 88
    label "europejski"
  ]
  node [
    id 89
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 90
    label "langosz"
  ]
  node [
    id 91
    label "zboczenie"
  ]
  node [
    id 92
    label "om&#243;wienie"
  ]
  node [
    id 93
    label "sponiewieranie"
  ]
  node [
    id 94
    label "discipline"
  ]
  node [
    id 95
    label "rzecz"
  ]
  node [
    id 96
    label "omawia&#263;"
  ]
  node [
    id 97
    label "kr&#261;&#380;enie"
  ]
  node [
    id 98
    label "tre&#347;&#263;"
  ]
  node [
    id 99
    label "robienie"
  ]
  node [
    id 100
    label "sponiewiera&#263;"
  ]
  node [
    id 101
    label "element"
  ]
  node [
    id 102
    label "entity"
  ]
  node [
    id 103
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 104
    label "tematyka"
  ]
  node [
    id 105
    label "w&#261;tek"
  ]
  node [
    id 106
    label "charakter"
  ]
  node [
    id 107
    label "zbaczanie"
  ]
  node [
    id 108
    label "program_nauczania"
  ]
  node [
    id 109
    label "om&#243;wi&#263;"
  ]
  node [
    id 110
    label "omawianie"
  ]
  node [
    id 111
    label "thing"
  ]
  node [
    id 112
    label "kultura"
  ]
  node [
    id 113
    label "istota"
  ]
  node [
    id 114
    label "zbacza&#263;"
  ]
  node [
    id 115
    label "zboczy&#263;"
  ]
  node [
    id 116
    label "gwardzista"
  ]
  node [
    id 117
    label "melodia"
  ]
  node [
    id 118
    label "taniec"
  ]
  node [
    id 119
    label "taniec_ludowy"
  ]
  node [
    id 120
    label "&#347;redniowieczny"
  ]
  node [
    id 121
    label "europejsko"
  ]
  node [
    id 122
    label "specjalny"
  ]
  node [
    id 123
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 124
    label "weso&#322;y"
  ]
  node [
    id 125
    label "sprawny"
  ]
  node [
    id 126
    label "rytmiczny"
  ]
  node [
    id 127
    label "skocznie"
  ]
  node [
    id 128
    label "energiczny"
  ]
  node [
    id 129
    label "przytup"
  ]
  node [
    id 130
    label "ho&#322;ubiec"
  ]
  node [
    id 131
    label "wodzi&#263;"
  ]
  node [
    id 132
    label "lendler"
  ]
  node [
    id 133
    label "austriacki"
  ]
  node [
    id 134
    label "polka"
  ]
  node [
    id 135
    label "ludowy"
  ]
  node [
    id 136
    label "pie&#347;&#324;"
  ]
  node [
    id 137
    label "mieszkaniec"
  ]
  node [
    id 138
    label "centu&#347;"
  ]
  node [
    id 139
    label "lalka"
  ]
  node [
    id 140
    label "Ma&#322;opolanin"
  ]
  node [
    id 141
    label "krakauer"
  ]
  node [
    id 142
    label "kanclerz"
  ]
  node [
    id 143
    label "szko&#322;a"
  ]
  node [
    id 144
    label "podkanclerz"
  ]
  node [
    id 145
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 146
    label "miasteczko_studenckie"
  ]
  node [
    id 147
    label "kwestura"
  ]
  node [
    id 148
    label "wyk&#322;adanie"
  ]
  node [
    id 149
    label "rektorat"
  ]
  node [
    id 150
    label "school"
  ]
  node [
    id 151
    label "senat"
  ]
  node [
    id 152
    label "promotorstwo"
  ]
  node [
    id 153
    label "do&#347;wiadczenie"
  ]
  node [
    id 154
    label "teren_szko&#322;y"
  ]
  node [
    id 155
    label "wiedza"
  ]
  node [
    id 156
    label "Mickiewicz"
  ]
  node [
    id 157
    label "kwalifikacje"
  ]
  node [
    id 158
    label "podr&#281;cznik"
  ]
  node [
    id 159
    label "absolwent"
  ]
  node [
    id 160
    label "praktyka"
  ]
  node [
    id 161
    label "system"
  ]
  node [
    id 162
    label "zda&#263;"
  ]
  node [
    id 163
    label "gabinet"
  ]
  node [
    id 164
    label "urszulanki"
  ]
  node [
    id 165
    label "sztuba"
  ]
  node [
    id 166
    label "&#322;awa_szkolna"
  ]
  node [
    id 167
    label "nauka"
  ]
  node [
    id 168
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 169
    label "przepisa&#263;"
  ]
  node [
    id 170
    label "muzyka"
  ]
  node [
    id 171
    label "grupa"
  ]
  node [
    id 172
    label "form"
  ]
  node [
    id 173
    label "klasa"
  ]
  node [
    id 174
    label "lekcja"
  ]
  node [
    id 175
    label "metoda"
  ]
  node [
    id 176
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 177
    label "przepisanie"
  ]
  node [
    id 178
    label "skolaryzacja"
  ]
  node [
    id 179
    label "zdanie"
  ]
  node [
    id 180
    label "stopek"
  ]
  node [
    id 181
    label "sekretariat"
  ]
  node [
    id 182
    label "ideologia"
  ]
  node [
    id 183
    label "lesson"
  ]
  node [
    id 184
    label "instytucja"
  ]
  node [
    id 185
    label "niepokalanki"
  ]
  node [
    id 186
    label "siedziba"
  ]
  node [
    id 187
    label "szkolenie"
  ]
  node [
    id 188
    label "kara"
  ]
  node [
    id 189
    label "tablica"
  ]
  node [
    id 190
    label "bursary"
  ]
  node [
    id 191
    label "urz&#261;d"
  ]
  node [
    id 192
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 193
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 194
    label "parlament"
  ]
  node [
    id 195
    label "deputation"
  ]
  node [
    id 196
    label "kolegium"
  ]
  node [
    id 197
    label "izba_wy&#380;sza"
  ]
  node [
    id 198
    label "magistrat"
  ]
  node [
    id 199
    label "biuro"
  ]
  node [
    id 200
    label "puszczenie"
  ]
  node [
    id 201
    label "issue"
  ]
  node [
    id 202
    label "wyjmowanie"
  ]
  node [
    id 203
    label "pokrywanie"
  ]
  node [
    id 204
    label "wystawianie"
  ]
  node [
    id 205
    label "k&#322;adzenie"
  ]
  node [
    id 206
    label "uczenie"
  ]
  node [
    id 207
    label "czynno&#347;&#263;"
  ]
  node [
    id 208
    label "presentation"
  ]
  node [
    id 209
    label "urz&#281;dnik"
  ]
  node [
    id 210
    label "opieka"
  ]
  node [
    id 211
    label "uczy&#263;"
  ]
  node [
    id 212
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 213
    label "wyjmowa&#263;"
  ]
  node [
    id 214
    label "translate"
  ]
  node [
    id 215
    label "elaborate"
  ]
  node [
    id 216
    label "give"
  ]
  node [
    id 217
    label "Bismarck"
  ]
  node [
    id 218
    label "kuria"
  ]
  node [
    id 219
    label "premier"
  ]
  node [
    id 220
    label "Goebbels"
  ]
  node [
    id 221
    label "duchowny"
  ]
  node [
    id 222
    label "dostojnik"
  ]
  node [
    id 223
    label "daleki"
  ]
  node [
    id 224
    label "ruch"
  ]
  node [
    id 225
    label "d&#322;ugo"
  ]
  node [
    id 226
    label "mechanika"
  ]
  node [
    id 227
    label "utrzymywanie"
  ]
  node [
    id 228
    label "move"
  ]
  node [
    id 229
    label "poruszenie"
  ]
  node [
    id 230
    label "movement"
  ]
  node [
    id 231
    label "myk"
  ]
  node [
    id 232
    label "utrzyma&#263;"
  ]
  node [
    id 233
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 234
    label "utrzymanie"
  ]
  node [
    id 235
    label "travel"
  ]
  node [
    id 236
    label "kanciasty"
  ]
  node [
    id 237
    label "commercial_enterprise"
  ]
  node [
    id 238
    label "model"
  ]
  node [
    id 239
    label "strumie&#324;"
  ]
  node [
    id 240
    label "proces"
  ]
  node [
    id 241
    label "aktywno&#347;&#263;"
  ]
  node [
    id 242
    label "kr&#243;tki"
  ]
  node [
    id 243
    label "taktyka"
  ]
  node [
    id 244
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 245
    label "apraksja"
  ]
  node [
    id 246
    label "natural_process"
  ]
  node [
    id 247
    label "utrzymywa&#263;"
  ]
  node [
    id 248
    label "wydarzenie"
  ]
  node [
    id 249
    label "dyssypacja_energii"
  ]
  node [
    id 250
    label "tumult"
  ]
  node [
    id 251
    label "zmiana"
  ]
  node [
    id 252
    label "manewr"
  ]
  node [
    id 253
    label "lokomocja"
  ]
  node [
    id 254
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 255
    label "komunikacja"
  ]
  node [
    id 256
    label "drift"
  ]
  node [
    id 257
    label "dawny"
  ]
  node [
    id 258
    label "ogl&#281;dny"
  ]
  node [
    id 259
    label "du&#380;y"
  ]
  node [
    id 260
    label "daleko"
  ]
  node [
    id 261
    label "odleg&#322;y"
  ]
  node [
    id 262
    label "zwi&#261;zany"
  ]
  node [
    id 263
    label "r&#243;&#380;ny"
  ]
  node [
    id 264
    label "s&#322;aby"
  ]
  node [
    id 265
    label "odlegle"
  ]
  node [
    id 266
    label "oddalony"
  ]
  node [
    id 267
    label "g&#322;&#281;boki"
  ]
  node [
    id 268
    label "obcy"
  ]
  node [
    id 269
    label "nieobecny"
  ]
  node [
    id 270
    label "przysz&#322;y"
  ]
  node [
    id 271
    label "poprzedzanie"
  ]
  node [
    id 272
    label "czasoprzestrze&#324;"
  ]
  node [
    id 273
    label "laba"
  ]
  node [
    id 274
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 275
    label "chronometria"
  ]
  node [
    id 276
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 277
    label "rachuba_czasu"
  ]
  node [
    id 278
    label "przep&#322;ywanie"
  ]
  node [
    id 279
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 280
    label "czasokres"
  ]
  node [
    id 281
    label "odczyt"
  ]
  node [
    id 282
    label "chwila"
  ]
  node [
    id 283
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 284
    label "dzieje"
  ]
  node [
    id 285
    label "kategoria_gramatyczna"
  ]
  node [
    id 286
    label "poprzedzenie"
  ]
  node [
    id 287
    label "trawienie"
  ]
  node [
    id 288
    label "pochodzi&#263;"
  ]
  node [
    id 289
    label "period"
  ]
  node [
    id 290
    label "okres_czasu"
  ]
  node [
    id 291
    label "poprzedza&#263;"
  ]
  node [
    id 292
    label "schy&#322;ek"
  ]
  node [
    id 293
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 294
    label "odwlekanie_si&#281;"
  ]
  node [
    id 295
    label "zegar"
  ]
  node [
    id 296
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 297
    label "czwarty_wymiar"
  ]
  node [
    id 298
    label "pochodzenie"
  ]
  node [
    id 299
    label "koniugacja"
  ]
  node [
    id 300
    label "Zeitgeist"
  ]
  node [
    id 301
    label "trawi&#263;"
  ]
  node [
    id 302
    label "pogoda"
  ]
  node [
    id 303
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 304
    label "poprzedzi&#263;"
  ]
  node [
    id 305
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 306
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 307
    label "time_period"
  ]
  node [
    id 308
    label "time"
  ]
  node [
    id 309
    label "blok"
  ]
  node [
    id 310
    label "handout"
  ]
  node [
    id 311
    label "pomiar"
  ]
  node [
    id 312
    label "lecture"
  ]
  node [
    id 313
    label "reading"
  ]
  node [
    id 314
    label "podawanie"
  ]
  node [
    id 315
    label "wyk&#322;ad"
  ]
  node [
    id 316
    label "potrzyma&#263;"
  ]
  node [
    id 317
    label "warunki"
  ]
  node [
    id 318
    label "pok&#243;j"
  ]
  node [
    id 319
    label "atak"
  ]
  node [
    id 320
    label "program"
  ]
  node [
    id 321
    label "meteorology"
  ]
  node [
    id 322
    label "weather"
  ]
  node [
    id 323
    label "prognoza_meteorologiczna"
  ]
  node [
    id 324
    label "czas_wolny"
  ]
  node [
    id 325
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 326
    label "metrologia"
  ]
  node [
    id 327
    label "godzinnik"
  ]
  node [
    id 328
    label "bicie"
  ]
  node [
    id 329
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 330
    label "wahad&#322;o"
  ]
  node [
    id 331
    label "kurant"
  ]
  node [
    id 332
    label "cyferblat"
  ]
  node [
    id 333
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 334
    label "nabicie"
  ]
  node [
    id 335
    label "werk"
  ]
  node [
    id 336
    label "czasomierz"
  ]
  node [
    id 337
    label "tyka&#263;"
  ]
  node [
    id 338
    label "tykn&#261;&#263;"
  ]
  node [
    id 339
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 340
    label "kotwica"
  ]
  node [
    id 341
    label "fleksja"
  ]
  node [
    id 342
    label "liczba"
  ]
  node [
    id 343
    label "coupling"
  ]
  node [
    id 344
    label "osoba"
  ]
  node [
    id 345
    label "tryb"
  ]
  node [
    id 346
    label "czasownik"
  ]
  node [
    id 347
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 348
    label "orz&#281;sek"
  ]
  node [
    id 349
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 350
    label "zaczynanie_si&#281;"
  ]
  node [
    id 351
    label "str&#243;j"
  ]
  node [
    id 352
    label "wynikanie"
  ]
  node [
    id 353
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 354
    label "origin"
  ]
  node [
    id 355
    label "background"
  ]
  node [
    id 356
    label "geneza"
  ]
  node [
    id 357
    label "beginning"
  ]
  node [
    id 358
    label "digestion"
  ]
  node [
    id 359
    label "unicestwianie"
  ]
  node [
    id 360
    label "sp&#281;dzanie"
  ]
  node [
    id 361
    label "contemplation"
  ]
  node [
    id 362
    label "rozk&#322;adanie"
  ]
  node [
    id 363
    label "marnowanie"
  ]
  node [
    id 364
    label "proces_fizjologiczny"
  ]
  node [
    id 365
    label "przetrawianie"
  ]
  node [
    id 366
    label "perystaltyka"
  ]
  node [
    id 367
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 368
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 369
    label "przebywa&#263;"
  ]
  node [
    id 370
    label "pour"
  ]
  node [
    id 371
    label "carry"
  ]
  node [
    id 372
    label "sail"
  ]
  node [
    id 373
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 374
    label "go&#347;ci&#263;"
  ]
  node [
    id 375
    label "mija&#263;"
  ]
  node [
    id 376
    label "proceed"
  ]
  node [
    id 377
    label "odej&#347;cie"
  ]
  node [
    id 378
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 379
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 380
    label "zanikni&#281;cie"
  ]
  node [
    id 381
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 382
    label "ciecz"
  ]
  node [
    id 383
    label "opuszczenie"
  ]
  node [
    id 384
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 385
    label "departure"
  ]
  node [
    id 386
    label "oddalenie_si&#281;"
  ]
  node [
    id 387
    label "przeby&#263;"
  ]
  node [
    id 388
    label "min&#261;&#263;"
  ]
  node [
    id 389
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 390
    label "swimming"
  ]
  node [
    id 391
    label "zago&#347;ci&#263;"
  ]
  node [
    id 392
    label "cross"
  ]
  node [
    id 393
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 394
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 395
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 396
    label "zrobi&#263;"
  ]
  node [
    id 397
    label "opatrzy&#263;"
  ]
  node [
    id 398
    label "overwhelm"
  ]
  node [
    id 399
    label "opatrywa&#263;"
  ]
  node [
    id 400
    label "date"
  ]
  node [
    id 401
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 402
    label "wynika&#263;"
  ]
  node [
    id 403
    label "fall"
  ]
  node [
    id 404
    label "poby&#263;"
  ]
  node [
    id 405
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 406
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 407
    label "bolt"
  ]
  node [
    id 408
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 409
    label "spowodowa&#263;"
  ]
  node [
    id 410
    label "uda&#263;_si&#281;"
  ]
  node [
    id 411
    label "opatrzenie"
  ]
  node [
    id 412
    label "zdarzenie_si&#281;"
  ]
  node [
    id 413
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 414
    label "progress"
  ]
  node [
    id 415
    label "opatrywanie"
  ]
  node [
    id 416
    label "mini&#281;cie"
  ]
  node [
    id 417
    label "doznanie"
  ]
  node [
    id 418
    label "zaistnienie"
  ]
  node [
    id 419
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 420
    label "przebycie"
  ]
  node [
    id 421
    label "cruise"
  ]
  node [
    id 422
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 423
    label "usuwa&#263;"
  ]
  node [
    id 424
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 425
    label "lutowa&#263;"
  ]
  node [
    id 426
    label "marnowa&#263;"
  ]
  node [
    id 427
    label "przetrawia&#263;"
  ]
  node [
    id 428
    label "poch&#322;ania&#263;"
  ]
  node [
    id 429
    label "digest"
  ]
  node [
    id 430
    label "metal"
  ]
  node [
    id 431
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 432
    label "sp&#281;dza&#263;"
  ]
  node [
    id 433
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 434
    label "zjawianie_si&#281;"
  ]
  node [
    id 435
    label "przebywanie"
  ]
  node [
    id 436
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 437
    label "mijanie"
  ]
  node [
    id 438
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 439
    label "zaznawanie"
  ]
  node [
    id 440
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 441
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 442
    label "flux"
  ]
  node [
    id 443
    label "epoka"
  ]
  node [
    id 444
    label "flow"
  ]
  node [
    id 445
    label "choroba_przyrodzona"
  ]
  node [
    id 446
    label "ciota"
  ]
  node [
    id 447
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 448
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 449
    label "kres"
  ]
  node [
    id 450
    label "przestrze&#324;"
  ]
  node [
    id 451
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 452
    label "free"
  ]
  node [
    id 453
    label "watch"
  ]
  node [
    id 454
    label "zobaczy&#263;"
  ]
  node [
    id 455
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 456
    label "spoziera&#263;"
  ]
  node [
    id 457
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 458
    label "peek"
  ]
  node [
    id 459
    label "postrzec"
  ]
  node [
    id 460
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 461
    label "cognizance"
  ]
  node [
    id 462
    label "popatrze&#263;"
  ]
  node [
    id 463
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 464
    label "pojrze&#263;"
  ]
  node [
    id 465
    label "dostrzec"
  ]
  node [
    id 466
    label "spot"
  ]
  node [
    id 467
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 468
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 469
    label "go_steady"
  ]
  node [
    id 470
    label "zinterpretowa&#263;"
  ]
  node [
    id 471
    label "spotka&#263;"
  ]
  node [
    id 472
    label "obejrze&#263;"
  ]
  node [
    id 473
    label "znale&#378;&#263;"
  ]
  node [
    id 474
    label "see"
  ]
  node [
    id 475
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 476
    label "niepokoj&#261;co"
  ]
  node [
    id 477
    label "boski"
  ]
  node [
    id 478
    label "krajobraz"
  ]
  node [
    id 479
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 480
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 481
    label "przywidzenie"
  ]
  node [
    id 482
    label "presence"
  ]
  node [
    id 483
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 484
    label "jako&#347;&#263;"
  ]
  node [
    id 485
    label "wygl&#261;d"
  ]
  node [
    id 486
    label "cz&#322;owiek"
  ]
  node [
    id 487
    label "gust"
  ]
  node [
    id 488
    label "drobiazg"
  ]
  node [
    id 489
    label "kobieta"
  ]
  node [
    id 490
    label "beauty"
  ]
  node [
    id 491
    label "kalokagatia"
  ]
  node [
    id 492
    label "cecha"
  ]
  node [
    id 493
    label "prettiness"
  ]
  node [
    id 494
    label "ozdoba"
  ]
  node [
    id 495
    label "rzadko&#347;&#263;"
  ]
  node [
    id 496
    label "nadprzyrodzony"
  ]
  node [
    id 497
    label "&#347;mieszny"
  ]
  node [
    id 498
    label "wspania&#322;y"
  ]
  node [
    id 499
    label "bezpretensjonalny"
  ]
  node [
    id 500
    label "bosko"
  ]
  node [
    id 501
    label "nale&#380;ny"
  ]
  node [
    id 502
    label "wyj&#261;tkowy"
  ]
  node [
    id 503
    label "fantastyczny"
  ]
  node [
    id 504
    label "arcypi&#281;kny"
  ]
  node [
    id 505
    label "cudnie"
  ]
  node [
    id 506
    label "cudowny"
  ]
  node [
    id 507
    label "udany"
  ]
  node [
    id 508
    label "wielki"
  ]
  node [
    id 509
    label "teren"
  ]
  node [
    id 510
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 511
    label "human_body"
  ]
  node [
    id 512
    label "dzie&#322;o"
  ]
  node [
    id 513
    label "obszar"
  ]
  node [
    id 514
    label "przyroda"
  ]
  node [
    id 515
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 516
    label "obraz"
  ]
  node [
    id 517
    label "widok"
  ]
  node [
    id 518
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 519
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 520
    label "zaj&#347;cie"
  ]
  node [
    id 521
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 522
    label "kontekst"
  ]
  node [
    id 523
    label "message"
  ]
  node [
    id 524
    label "&#347;wiat"
  ]
  node [
    id 525
    label "dar"
  ]
  node [
    id 526
    label "real"
  ]
  node [
    id 527
    label "kognicja"
  ]
  node [
    id 528
    label "przebieg"
  ]
  node [
    id 529
    label "rozprawa"
  ]
  node [
    id 530
    label "legislacyjnie"
  ]
  node [
    id 531
    label "przes&#322;anka"
  ]
  node [
    id 532
    label "nast&#281;pstwo"
  ]
  node [
    id 533
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 534
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 535
    label "zbi&#243;r"
  ]
  node [
    id 536
    label "osobowo&#347;&#263;"
  ]
  node [
    id 537
    label "psychika"
  ]
  node [
    id 538
    label "posta&#263;"
  ]
  node [
    id 539
    label "kompleksja"
  ]
  node [
    id 540
    label "fizjonomia"
  ]
  node [
    id 541
    label "wytw&#243;r"
  ]
  node [
    id 542
    label "z&#322;uda"
  ]
  node [
    id 543
    label "sojourn"
  ]
  node [
    id 544
    label "z&#322;udzenie"
  ]
  node [
    id 545
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 546
    label "mie&#263;_miejsce"
  ]
  node [
    id 547
    label "equal"
  ]
  node [
    id 548
    label "trwa&#263;"
  ]
  node [
    id 549
    label "chodzi&#263;"
  ]
  node [
    id 550
    label "si&#281;ga&#263;"
  ]
  node [
    id 551
    label "stan"
  ]
  node [
    id 552
    label "stand"
  ]
  node [
    id 553
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 554
    label "uczestniczy&#263;"
  ]
  node [
    id 555
    label "participate"
  ]
  node [
    id 556
    label "robi&#263;"
  ]
  node [
    id 557
    label "istnie&#263;"
  ]
  node [
    id 558
    label "pozostawa&#263;"
  ]
  node [
    id 559
    label "zostawa&#263;"
  ]
  node [
    id 560
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 561
    label "adhere"
  ]
  node [
    id 562
    label "compass"
  ]
  node [
    id 563
    label "korzysta&#263;"
  ]
  node [
    id 564
    label "appreciation"
  ]
  node [
    id 565
    label "osi&#261;ga&#263;"
  ]
  node [
    id 566
    label "dociera&#263;"
  ]
  node [
    id 567
    label "get"
  ]
  node [
    id 568
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 569
    label "mierzy&#263;"
  ]
  node [
    id 570
    label "u&#380;ywa&#263;"
  ]
  node [
    id 571
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 572
    label "exsert"
  ]
  node [
    id 573
    label "being"
  ]
  node [
    id 574
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 575
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 576
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 577
    label "p&#322;ywa&#263;"
  ]
  node [
    id 578
    label "run"
  ]
  node [
    id 579
    label "bangla&#263;"
  ]
  node [
    id 580
    label "przebiega&#263;"
  ]
  node [
    id 581
    label "wk&#322;ada&#263;"
  ]
  node [
    id 582
    label "bywa&#263;"
  ]
  node [
    id 583
    label "dziama&#263;"
  ]
  node [
    id 584
    label "stara&#263;_si&#281;"
  ]
  node [
    id 585
    label "para"
  ]
  node [
    id 586
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 587
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 588
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 589
    label "krok"
  ]
  node [
    id 590
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 591
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 592
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 593
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 594
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 595
    label "continue"
  ]
  node [
    id 596
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 597
    label "Ohio"
  ]
  node [
    id 598
    label "wci&#281;cie"
  ]
  node [
    id 599
    label "Nowy_York"
  ]
  node [
    id 600
    label "warstwa"
  ]
  node [
    id 601
    label "samopoczucie"
  ]
  node [
    id 602
    label "Illinois"
  ]
  node [
    id 603
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 604
    label "state"
  ]
  node [
    id 605
    label "Jukatan"
  ]
  node [
    id 606
    label "Kalifornia"
  ]
  node [
    id 607
    label "Wirginia"
  ]
  node [
    id 608
    label "wektor"
  ]
  node [
    id 609
    label "Teksas"
  ]
  node [
    id 610
    label "Goa"
  ]
  node [
    id 611
    label "Waszyngton"
  ]
  node [
    id 612
    label "miejsce"
  ]
  node [
    id 613
    label "Massachusetts"
  ]
  node [
    id 614
    label "Alaska"
  ]
  node [
    id 615
    label "Arakan"
  ]
  node [
    id 616
    label "Hawaje"
  ]
  node [
    id 617
    label "Maryland"
  ]
  node [
    id 618
    label "punkt"
  ]
  node [
    id 619
    label "Michigan"
  ]
  node [
    id 620
    label "Arizona"
  ]
  node [
    id 621
    label "Georgia"
  ]
  node [
    id 622
    label "poziom"
  ]
  node [
    id 623
    label "Pensylwania"
  ]
  node [
    id 624
    label "shape"
  ]
  node [
    id 625
    label "Luizjana"
  ]
  node [
    id 626
    label "Nowy_Meksyk"
  ]
  node [
    id 627
    label "Alabama"
  ]
  node [
    id 628
    label "ilo&#347;&#263;"
  ]
  node [
    id 629
    label "Kansas"
  ]
  node [
    id 630
    label "Oregon"
  ]
  node [
    id 631
    label "Floryda"
  ]
  node [
    id 632
    label "Oklahoma"
  ]
  node [
    id 633
    label "jednostka_administracyjna"
  ]
  node [
    id 634
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 635
    label "cz&#281;sto"
  ]
  node [
    id 636
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 637
    label "charakterystyka"
  ]
  node [
    id 638
    label "m&#322;ot"
  ]
  node [
    id 639
    label "znak"
  ]
  node [
    id 640
    label "drzewo"
  ]
  node [
    id 641
    label "pr&#243;ba"
  ]
  node [
    id 642
    label "attribute"
  ]
  node [
    id 643
    label "marka"
  ]
  node [
    id 644
    label "dziedzina"
  ]
  node [
    id 645
    label "sfera"
  ]
  node [
    id 646
    label "zakres"
  ]
  node [
    id 647
    label "funkcja"
  ]
  node [
    id 648
    label "bezdro&#380;e"
  ]
  node [
    id 649
    label "poddzia&#322;"
  ]
  node [
    id 650
    label "akademicki"
  ]
  node [
    id 651
    label "naukowy"
  ]
  node [
    id 652
    label "teoretyczny"
  ]
  node [
    id 653
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 654
    label "studencki"
  ]
  node [
    id 655
    label "prawid&#322;owy"
  ]
  node [
    id 656
    label "odpowiedni"
  ]
  node [
    id 657
    label "szkolny"
  ]
  node [
    id 658
    label "tradycyjny"
  ]
  node [
    id 659
    label "intelektualny"
  ]
  node [
    id 660
    label "akademicko"
  ]
  node [
    id 661
    label "akademiczny"
  ]
  node [
    id 662
    label "po_akademicku"
  ]
  node [
    id 663
    label "gzyms"
  ]
  node [
    id 664
    label "obstruction"
  ]
  node [
    id 665
    label "budowla"
  ]
  node [
    id 666
    label "futbolista"
  ]
  node [
    id 667
    label "futr&#243;wka"
  ]
  node [
    id 668
    label "ceg&#322;a"
  ]
  node [
    id 669
    label "tynk"
  ]
  node [
    id 670
    label "fola"
  ]
  node [
    id 671
    label "trudno&#347;&#263;"
  ]
  node [
    id 672
    label "konstrukcja"
  ]
  node [
    id 673
    label "belkowanie"
  ]
  node [
    id 674
    label "&#347;ciana"
  ]
  node [
    id 675
    label "rz&#261;d"
  ]
  node [
    id 676
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 677
    label "napotka&#263;"
  ]
  node [
    id 678
    label "subiekcja"
  ]
  node [
    id 679
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 680
    label "k&#322;opotliwy"
  ]
  node [
    id 681
    label "napotkanie"
  ]
  node [
    id 682
    label "difficulty"
  ]
  node [
    id 683
    label "obstacle"
  ]
  node [
    id 684
    label "sytuacja"
  ]
  node [
    id 685
    label "przybli&#380;enie"
  ]
  node [
    id 686
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 687
    label "kategoria"
  ]
  node [
    id 688
    label "szpaler"
  ]
  node [
    id 689
    label "lon&#380;a"
  ]
  node [
    id 690
    label "uporz&#261;dkowanie"
  ]
  node [
    id 691
    label "jednostka_systematyczna"
  ]
  node [
    id 692
    label "egzekutywa"
  ]
  node [
    id 693
    label "Londyn"
  ]
  node [
    id 694
    label "gabinet_cieni"
  ]
  node [
    id 695
    label "gromada"
  ]
  node [
    id 696
    label "number"
  ]
  node [
    id 697
    label "Konsulat"
  ]
  node [
    id 698
    label "tract"
  ]
  node [
    id 699
    label "w&#322;adza"
  ]
  node [
    id 700
    label "struktura"
  ]
  node [
    id 701
    label "practice"
  ]
  node [
    id 702
    label "wykre&#347;lanie"
  ]
  node [
    id 703
    label "budowa"
  ]
  node [
    id 704
    label "element_konstrukcyjny"
  ]
  node [
    id 705
    label "profil"
  ]
  node [
    id 706
    label "zbocze"
  ]
  node [
    id 707
    label "kszta&#322;t"
  ]
  node [
    id 708
    label "przegroda"
  ]
  node [
    id 709
    label "p&#322;aszczyzna"
  ]
  node [
    id 710
    label "bariera"
  ]
  node [
    id 711
    label "facebook"
  ]
  node [
    id 712
    label "wielo&#347;cian"
  ]
  node [
    id 713
    label "pow&#322;oka"
  ]
  node [
    id 714
    label "wyrobisko"
  ]
  node [
    id 715
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 716
    label "obudowanie"
  ]
  node [
    id 717
    label "obudowywa&#263;"
  ]
  node [
    id 718
    label "zbudowa&#263;"
  ]
  node [
    id 719
    label "obudowa&#263;"
  ]
  node [
    id 720
    label "kolumnada"
  ]
  node [
    id 721
    label "korpus"
  ]
  node [
    id 722
    label "Sukiennice"
  ]
  node [
    id 723
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 724
    label "fundament"
  ]
  node [
    id 725
    label "postanie"
  ]
  node [
    id 726
    label "obudowywanie"
  ]
  node [
    id 727
    label "zbudowanie"
  ]
  node [
    id 728
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 729
    label "stan_surowy"
  ]
  node [
    id 730
    label "Messi"
  ]
  node [
    id 731
    label "pi&#322;karstwo"
  ]
  node [
    id 732
    label "pi&#322;karz"
  ]
  node [
    id 733
    label "sportowiec"
  ]
  node [
    id 734
    label "gracz"
  ]
  node [
    id 735
    label "ok&#322;adzina"
  ]
  node [
    id 736
    label "wy&#347;ci&#243;&#322;ka"
  ]
  node [
    id 737
    label "statek"
  ]
  node [
    id 738
    label "obicie"
  ]
  node [
    id 739
    label "uk&#322;adanie"
  ]
  node [
    id 740
    label "fryz"
  ]
  node [
    id 741
    label "entablature"
  ]
  node [
    id 742
    label "materia&#322;_budowlany"
  ]
  node [
    id 743
    label "wi&#261;zanie"
  ]
  node [
    id 744
    label "tile"
  ]
  node [
    id 745
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 746
    label "g&#322;&#243;wka"
  ]
  node [
    id 747
    label "woz&#243;wka"
  ]
  node [
    id 748
    label "kokaina"
  ]
  node [
    id 749
    label "narkotyk"
  ]
  node [
    id 750
    label "makija&#380;"
  ]
  node [
    id 751
    label "zaprawa"
  ]
  node [
    id 752
    label "wyst&#281;p"
  ]
  node [
    id 753
    label "karnisz"
  ]
  node [
    id 754
    label "mebel"
  ]
  node [
    id 755
    label "skrzynia"
  ]
  node [
    id 756
    label "sie&#263;_rybacka"
  ]
  node [
    id 757
    label "wapno_gaszone"
  ]
  node [
    id 758
    label "niezale&#380;ny"
  ]
  node [
    id 759
    label "zrzeszenie"
  ]
  node [
    id 760
    label "student"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 644
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 622
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 449
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 58
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 758
    target 759
  ]
  edge [
    source 758
    target 760
  ]
  edge [
    source 759
    target 760
  ]
]
