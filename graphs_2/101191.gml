graph [
  node [
    id 0
    label "nauka"
    origin "text"
  ]
  node [
    id 1
    label "czytanie"
    origin "text"
  ]
  node [
    id 2
    label "wiedza"
  ]
  node [
    id 3
    label "miasteczko_rowerowe"
  ]
  node [
    id 4
    label "porada"
  ]
  node [
    id 5
    label "fotowoltaika"
  ]
  node [
    id 6
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 7
    label "przem&#243;wienie"
  ]
  node [
    id 8
    label "nauki_o_poznaniu"
  ]
  node [
    id 9
    label "nomotetyczny"
  ]
  node [
    id 10
    label "systematyka"
  ]
  node [
    id 11
    label "proces"
  ]
  node [
    id 12
    label "typologia"
  ]
  node [
    id 13
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 14
    label "kultura_duchowa"
  ]
  node [
    id 15
    label "&#322;awa_szkolna"
  ]
  node [
    id 16
    label "nauki_penalne"
  ]
  node [
    id 17
    label "dziedzina"
  ]
  node [
    id 18
    label "imagineskopia"
  ]
  node [
    id 19
    label "teoria_naukowa"
  ]
  node [
    id 20
    label "inwentyka"
  ]
  node [
    id 21
    label "metodologia"
  ]
  node [
    id 22
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 23
    label "nauki_o_Ziemi"
  ]
  node [
    id 24
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 25
    label "sfera"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "zakres"
  ]
  node [
    id 28
    label "funkcja"
  ]
  node [
    id 29
    label "bezdro&#380;e"
  ]
  node [
    id 30
    label "poddzia&#322;"
  ]
  node [
    id 31
    label "kognicja"
  ]
  node [
    id 32
    label "przebieg"
  ]
  node [
    id 33
    label "rozprawa"
  ]
  node [
    id 34
    label "wydarzenie"
  ]
  node [
    id 35
    label "legislacyjnie"
  ]
  node [
    id 36
    label "przes&#322;anka"
  ]
  node [
    id 37
    label "zjawisko"
  ]
  node [
    id 38
    label "nast&#281;pstwo"
  ]
  node [
    id 39
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 40
    label "zrozumienie"
  ]
  node [
    id 41
    label "obronienie"
  ]
  node [
    id 42
    label "wydanie"
  ]
  node [
    id 43
    label "wyg&#322;oszenie"
  ]
  node [
    id 44
    label "wypowied&#378;"
  ]
  node [
    id 45
    label "oddzia&#322;anie"
  ]
  node [
    id 46
    label "address"
  ]
  node [
    id 47
    label "wydobycie"
  ]
  node [
    id 48
    label "wyst&#261;pienie"
  ]
  node [
    id 49
    label "talk"
  ]
  node [
    id 50
    label "odzyskanie"
  ]
  node [
    id 51
    label "sermon"
  ]
  node [
    id 52
    label "cognition"
  ]
  node [
    id 53
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 54
    label "intelekt"
  ]
  node [
    id 55
    label "pozwolenie"
  ]
  node [
    id 56
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 57
    label "zaawansowanie"
  ]
  node [
    id 58
    label "wykszta&#322;cenie"
  ]
  node [
    id 59
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 60
    label "wskaz&#243;wka"
  ]
  node [
    id 61
    label "technika"
  ]
  node [
    id 62
    label "typology"
  ]
  node [
    id 63
    label "podzia&#322;"
  ]
  node [
    id 64
    label "kwantyfikacja"
  ]
  node [
    id 65
    label "taksonomia"
  ]
  node [
    id 66
    label "biologia"
  ]
  node [
    id 67
    label "biosystematyka"
  ]
  node [
    id 68
    label "kohorta"
  ]
  node [
    id 69
    label "kladystyka"
  ]
  node [
    id 70
    label "aparat_krytyczny"
  ]
  node [
    id 71
    label "funkcjonalizm"
  ]
  node [
    id 72
    label "wyobra&#378;nia"
  ]
  node [
    id 73
    label "charakterystyczny"
  ]
  node [
    id 74
    label "recitation"
  ]
  node [
    id 75
    label "dysleksja"
  ]
  node [
    id 76
    label "wczytywanie_si&#281;"
  ]
  node [
    id 77
    label "doczytywanie"
  ]
  node [
    id 78
    label "zaczytanie_si&#281;"
  ]
  node [
    id 79
    label "poczytanie"
  ]
  node [
    id 80
    label "przepowiadanie"
  ]
  node [
    id 81
    label "obrz&#261;dek"
  ]
  node [
    id 82
    label "czytywanie"
  ]
  node [
    id 83
    label "Biblia"
  ]
  node [
    id 84
    label "pokazywanie"
  ]
  node [
    id 85
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 86
    label "wczytywanie"
  ]
  node [
    id 87
    label "reading"
  ]
  node [
    id 88
    label "bycie_w_stanie"
  ]
  node [
    id 89
    label "lektor"
  ]
  node [
    id 90
    label "poznawanie"
  ]
  node [
    id 91
    label "wyczytywanie"
  ]
  node [
    id 92
    label "oczytywanie_si&#281;"
  ]
  node [
    id 93
    label "auspicje"
  ]
  node [
    id 94
    label "przewidywanie"
  ]
  node [
    id 95
    label "powtarzanie"
  ]
  node [
    id 96
    label "divination"
  ]
  node [
    id 97
    label "profetyzm"
  ]
  node [
    id 98
    label "prorokowanie"
  ]
  node [
    id 99
    label "warto&#347;&#263;"
  ]
  node [
    id 100
    label "kszta&#322;cenie"
  ]
  node [
    id 101
    label "powodowanie"
  ]
  node [
    id 102
    label "command"
  ]
  node [
    id 103
    label "wyraz"
  ]
  node [
    id 104
    label "obnoszenie"
  ]
  node [
    id 105
    label "robienie"
  ]
  node [
    id 106
    label "znaczenie"
  ]
  node [
    id 107
    label "show"
  ]
  node [
    id 108
    label "appearance"
  ]
  node [
    id 109
    label "przedstawianie"
  ]
  node [
    id 110
    label "informowanie"
  ]
  node [
    id 111
    label "czynno&#347;&#263;"
  ]
  node [
    id 112
    label "okazywanie"
  ]
  node [
    id 113
    label "podawanie"
  ]
  node [
    id 114
    label "&#347;wiadczenie"
  ]
  node [
    id 115
    label "uczenie_si&#281;"
  ]
  node [
    id 116
    label "znajomy"
  ]
  node [
    id 117
    label "umo&#380;liwianie"
  ]
  node [
    id 118
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 119
    label "rozr&#243;&#380;nianie"
  ]
  node [
    id 120
    label "zapoznawanie"
  ]
  node [
    id 121
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 122
    label "designation"
  ]
  node [
    id 123
    label "inclusion"
  ]
  node [
    id 124
    label "czucie"
  ]
  node [
    id 125
    label "recognition"
  ]
  node [
    id 126
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 127
    label "spotykanie"
  ]
  node [
    id 128
    label "merging"
  ]
  node [
    id 129
    label "przep&#322;ywanie"
  ]
  node [
    id 130
    label "zawieranie"
  ]
  node [
    id 131
    label "apokryf"
  ]
  node [
    id 132
    label "perykopa"
  ]
  node [
    id 133
    label "Biblia_Tysi&#261;clecia"
  ]
  node [
    id 134
    label "Stary_Testament"
  ]
  node [
    id 135
    label "paginator"
  ]
  node [
    id 136
    label "posta&#263;_biblijna"
  ]
  node [
    id 137
    label "Nowy_Testament"
  ]
  node [
    id 138
    label "Biblia_Jakuba_Wujka"
  ]
  node [
    id 139
    label "S&#322;owo_Bo&#380;e"
  ]
  node [
    id 140
    label "mamotrept"
  ]
  node [
    id 141
    label "kult"
  ]
  node [
    id 142
    label "liturgika"
  ]
  node [
    id 143
    label "porz&#261;dek"
  ]
  node [
    id 144
    label "praca_rolnicza"
  ]
  node [
    id 145
    label "function"
  ]
  node [
    id 146
    label "mod&#322;y"
  ]
  node [
    id 147
    label "hodowla"
  ]
  node [
    id 148
    label "modlitwa"
  ]
  node [
    id 149
    label "ceremony"
  ]
  node [
    id 150
    label "nauczyciel"
  ]
  node [
    id 151
    label "prezenter"
  ]
  node [
    id 152
    label "ministrant"
  ]
  node [
    id 153
    label "seminarzysta"
  ]
  node [
    id 154
    label "dysfunkcja"
  ]
  node [
    id 155
    label "dyslexia"
  ]
  node [
    id 156
    label "czyta&#263;"
  ]
  node [
    id 157
    label "wprowadzanie"
  ]
  node [
    id 158
    label "ko&#324;czenie"
  ]
  node [
    id 159
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 160
    label "uznanie"
  ]
  node [
    id 161
    label "tom"
  ]
  node [
    id 162
    label "pozna&#324;ski"
  ]
  node [
    id 163
    label "hour"
  ]
  node [
    id 164
    label "Meterowa"
  ]
  node [
    id 165
    label "m&#281;ski"
  ]
  node [
    id 166
    label "albo"
  ]
  node [
    id 167
    label "Tinker"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 167
  ]
  edge [
    source 166
    target 167
  ]
]
