graph [
  node [
    id 0
    label "adam"
    origin "text"
  ]
  node [
    id 1
    label "s&#322;odowy"
    origin "text"
  ]
  node [
    id 2
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "czarnk&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "majsterkowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "popularny"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "ksi&#281;ga_przys&#322;&#243;w_"
    origin "text"
  ]
  node [
    id 8
    label "dla"
    origin "text"
  ]
  node [
    id 9
    label "zr&#243;b"
    origin "text"
  ]
  node [
    id 10
    label "s&#322;odkawy"
  ]
  node [
    id 11
    label "zbo&#380;owy"
  ]
  node [
    id 12
    label "specjalny"
  ]
  node [
    id 13
    label "m&#261;czny"
  ]
  node [
    id 14
    label "s&#322;odkawo"
  ]
  node [
    id 15
    label "Barb&#243;rka"
  ]
  node [
    id 16
    label "miesi&#261;c"
  ]
  node [
    id 17
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 18
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 19
    label "Sylwester"
  ]
  node [
    id 20
    label "tydzie&#324;"
  ]
  node [
    id 21
    label "miech"
  ]
  node [
    id 22
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 23
    label "czas"
  ]
  node [
    id 24
    label "rok"
  ]
  node [
    id 25
    label "kalendy"
  ]
  node [
    id 26
    label "g&#243;rnik"
  ]
  node [
    id 27
    label "comber"
  ]
  node [
    id 28
    label "pracowa&#263;"
  ]
  node [
    id 29
    label "extort"
  ]
  node [
    id 30
    label "endeavor"
  ]
  node [
    id 31
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "mie&#263;_miejsce"
  ]
  node [
    id 33
    label "podejmowa&#263;"
  ]
  node [
    id 34
    label "dziama&#263;"
  ]
  node [
    id 35
    label "do"
  ]
  node [
    id 36
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 37
    label "bangla&#263;"
  ]
  node [
    id 38
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 39
    label "work"
  ]
  node [
    id 40
    label "maszyna"
  ]
  node [
    id 41
    label "dzia&#322;a&#263;"
  ]
  node [
    id 42
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 43
    label "tryb"
  ]
  node [
    id 44
    label "funkcjonowa&#263;"
  ]
  node [
    id 45
    label "praca"
  ]
  node [
    id 46
    label "przyst&#281;pny"
  ]
  node [
    id 47
    label "znany"
  ]
  node [
    id 48
    label "popularnie"
  ]
  node [
    id 49
    label "&#322;atwy"
  ]
  node [
    id 50
    label "zrozumia&#322;y"
  ]
  node [
    id 51
    label "dost&#281;pny"
  ]
  node [
    id 52
    label "przyst&#281;pnie"
  ]
  node [
    id 53
    label "&#322;atwo"
  ]
  node [
    id 54
    label "letki"
  ]
  node [
    id 55
    label "prosty"
  ]
  node [
    id 56
    label "&#322;acny"
  ]
  node [
    id 57
    label "snadny"
  ]
  node [
    id 58
    label "przyjemny"
  ]
  node [
    id 59
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 60
    label "wielki"
  ]
  node [
    id 61
    label "rozpowszechnianie"
  ]
  node [
    id 62
    label "nisko"
  ]
  node [
    id 63
    label "normally"
  ]
  node [
    id 64
    label "obiegowy"
  ]
  node [
    id 65
    label "cz&#281;sto"
  ]
  node [
    id 66
    label "przedmiot"
  ]
  node [
    id 67
    label "Polish"
  ]
  node [
    id 68
    label "goniony"
  ]
  node [
    id 69
    label "oberek"
  ]
  node [
    id 70
    label "ryba_po_grecku"
  ]
  node [
    id 71
    label "sztajer"
  ]
  node [
    id 72
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 73
    label "krakowiak"
  ]
  node [
    id 74
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 75
    label "pierogi_ruskie"
  ]
  node [
    id 76
    label "lacki"
  ]
  node [
    id 77
    label "polak"
  ]
  node [
    id 78
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 79
    label "chodzony"
  ]
  node [
    id 80
    label "po_polsku"
  ]
  node [
    id 81
    label "mazur"
  ]
  node [
    id 82
    label "polsko"
  ]
  node [
    id 83
    label "skoczny"
  ]
  node [
    id 84
    label "drabant"
  ]
  node [
    id 85
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 86
    label "j&#281;zyk"
  ]
  node [
    id 87
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 88
    label "artykulator"
  ]
  node [
    id 89
    label "kod"
  ]
  node [
    id 90
    label "kawa&#322;ek"
  ]
  node [
    id 91
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 92
    label "gramatyka"
  ]
  node [
    id 93
    label "stylik"
  ]
  node [
    id 94
    label "przet&#322;umaczenie"
  ]
  node [
    id 95
    label "formalizowanie"
  ]
  node [
    id 96
    label "ssanie"
  ]
  node [
    id 97
    label "ssa&#263;"
  ]
  node [
    id 98
    label "language"
  ]
  node [
    id 99
    label "liza&#263;"
  ]
  node [
    id 100
    label "napisa&#263;"
  ]
  node [
    id 101
    label "konsonantyzm"
  ]
  node [
    id 102
    label "wokalizm"
  ]
  node [
    id 103
    label "pisa&#263;"
  ]
  node [
    id 104
    label "fonetyka"
  ]
  node [
    id 105
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 106
    label "jeniec"
  ]
  node [
    id 107
    label "but"
  ]
  node [
    id 108
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 109
    label "po_koroniarsku"
  ]
  node [
    id 110
    label "kultura_duchowa"
  ]
  node [
    id 111
    label "t&#322;umaczenie"
  ]
  node [
    id 112
    label "m&#243;wienie"
  ]
  node [
    id 113
    label "pype&#263;"
  ]
  node [
    id 114
    label "lizanie"
  ]
  node [
    id 115
    label "pismo"
  ]
  node [
    id 116
    label "formalizowa&#263;"
  ]
  node [
    id 117
    label "rozumie&#263;"
  ]
  node [
    id 118
    label "organ"
  ]
  node [
    id 119
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 120
    label "rozumienie"
  ]
  node [
    id 121
    label "spos&#243;b"
  ]
  node [
    id 122
    label "makroglosja"
  ]
  node [
    id 123
    label "m&#243;wi&#263;"
  ]
  node [
    id 124
    label "jama_ustna"
  ]
  node [
    id 125
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 126
    label "formacja_geologiczna"
  ]
  node [
    id 127
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 128
    label "natural_language"
  ]
  node [
    id 129
    label "s&#322;ownictwo"
  ]
  node [
    id 130
    label "urz&#261;dzenie"
  ]
  node [
    id 131
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 132
    label "wschodnioeuropejski"
  ]
  node [
    id 133
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 134
    label "poga&#324;ski"
  ]
  node [
    id 135
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 136
    label "topielec"
  ]
  node [
    id 137
    label "europejski"
  ]
  node [
    id 138
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 139
    label "langosz"
  ]
  node [
    id 140
    label "zboczenie"
  ]
  node [
    id 141
    label "om&#243;wienie"
  ]
  node [
    id 142
    label "sponiewieranie"
  ]
  node [
    id 143
    label "discipline"
  ]
  node [
    id 144
    label "rzecz"
  ]
  node [
    id 145
    label "omawia&#263;"
  ]
  node [
    id 146
    label "kr&#261;&#380;enie"
  ]
  node [
    id 147
    label "tre&#347;&#263;"
  ]
  node [
    id 148
    label "robienie"
  ]
  node [
    id 149
    label "sponiewiera&#263;"
  ]
  node [
    id 150
    label "element"
  ]
  node [
    id 151
    label "entity"
  ]
  node [
    id 152
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 153
    label "tematyka"
  ]
  node [
    id 154
    label "w&#261;tek"
  ]
  node [
    id 155
    label "charakter"
  ]
  node [
    id 156
    label "zbaczanie"
  ]
  node [
    id 157
    label "program_nauczania"
  ]
  node [
    id 158
    label "om&#243;wi&#263;"
  ]
  node [
    id 159
    label "omawianie"
  ]
  node [
    id 160
    label "thing"
  ]
  node [
    id 161
    label "kultura"
  ]
  node [
    id 162
    label "istota"
  ]
  node [
    id 163
    label "zbacza&#263;"
  ]
  node [
    id 164
    label "zboczy&#263;"
  ]
  node [
    id 165
    label "gwardzista"
  ]
  node [
    id 166
    label "melodia"
  ]
  node [
    id 167
    label "taniec"
  ]
  node [
    id 168
    label "taniec_ludowy"
  ]
  node [
    id 169
    label "&#347;redniowieczny"
  ]
  node [
    id 170
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 171
    label "weso&#322;y"
  ]
  node [
    id 172
    label "sprawny"
  ]
  node [
    id 173
    label "rytmiczny"
  ]
  node [
    id 174
    label "skocznie"
  ]
  node [
    id 175
    label "energiczny"
  ]
  node [
    id 176
    label "lendler"
  ]
  node [
    id 177
    label "austriacki"
  ]
  node [
    id 178
    label "polka"
  ]
  node [
    id 179
    label "europejsko"
  ]
  node [
    id 180
    label "przytup"
  ]
  node [
    id 181
    label "ho&#322;ubiec"
  ]
  node [
    id 182
    label "wodzi&#263;"
  ]
  node [
    id 183
    label "ludowy"
  ]
  node [
    id 184
    label "pie&#347;&#324;"
  ]
  node [
    id 185
    label "mieszkaniec"
  ]
  node [
    id 186
    label "centu&#347;"
  ]
  node [
    id 187
    label "lalka"
  ]
  node [
    id 188
    label "Ma&#322;opolanin"
  ]
  node [
    id 189
    label "krakauer"
  ]
  node [
    id 190
    label "Adam"
  ]
  node [
    id 191
    label "prowadz&#261;cy"
  ]
  node [
    id 192
    label "Ksi&#281;gaPrzys&#322;&#243;wPrzypowie&#347;ciSalomona"
  ]
  node [
    id 193
    label "tv"
  ]
  node [
    id 194
    label "m&#322;odzie&#380;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 191
    target 192
  ]
  edge [
    source 191
    target 193
  ]
  edge [
    source 191
    target 194
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 194
  ]
  edge [
    source 193
    target 194
  ]
]
