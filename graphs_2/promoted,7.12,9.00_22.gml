graph [
  node [
    id 0
    label "pa&#378;dziernik"
    origin "text"
  ]
  node [
    id 1
    label "polska"
    origin "text"
  ]
  node [
    id 2
    label "wypadek"
    origin "text"
  ]
  node [
    id 3
    label "drogowe"
    origin "text"
  ]
  node [
    id 4
    label "zgin&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "osoba"
    origin "text"
  ]
  node [
    id 6
    label "oktober"
  ]
  node [
    id 7
    label "miesi&#261;c"
  ]
  node [
    id 8
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 9
    label "tydzie&#324;"
  ]
  node [
    id 10
    label "miech"
  ]
  node [
    id 11
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "rok"
  ]
  node [
    id 14
    label "kalendy"
  ]
  node [
    id 15
    label "wydarzenie"
  ]
  node [
    id 16
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 17
    label "przebiec"
  ]
  node [
    id 18
    label "happening"
  ]
  node [
    id 19
    label "charakter"
  ]
  node [
    id 20
    label "event"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 23
    label "motyw"
  ]
  node [
    id 24
    label "przebiegni&#281;cie"
  ]
  node [
    id 25
    label "fabu&#322;a"
  ]
  node [
    id 26
    label "w&#261;tek"
  ]
  node [
    id 27
    label "w&#281;ze&#322;"
  ]
  node [
    id 28
    label "perypetia"
  ]
  node [
    id 29
    label "opowiadanie"
  ]
  node [
    id 30
    label "fraza"
  ]
  node [
    id 31
    label "temat"
  ]
  node [
    id 32
    label "melodia"
  ]
  node [
    id 33
    label "cecha"
  ]
  node [
    id 34
    label "przyczyna"
  ]
  node [
    id 35
    label "sytuacja"
  ]
  node [
    id 36
    label "ozdoba"
  ]
  node [
    id 37
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 38
    label "przeby&#263;"
  ]
  node [
    id 39
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 40
    label "run"
  ]
  node [
    id 41
    label "proceed"
  ]
  node [
    id 42
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 43
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 44
    label "przemierzy&#263;"
  ]
  node [
    id 45
    label "fly"
  ]
  node [
    id 46
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 47
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 48
    label "przesun&#261;&#263;"
  ]
  node [
    id 49
    label "activity"
  ]
  node [
    id 50
    label "bezproblemowy"
  ]
  node [
    id 51
    label "przedmiot"
  ]
  node [
    id 52
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 53
    label "zbi&#243;r"
  ]
  node [
    id 54
    label "cz&#322;owiek"
  ]
  node [
    id 55
    label "osobowo&#347;&#263;"
  ]
  node [
    id 56
    label "psychika"
  ]
  node [
    id 57
    label "posta&#263;"
  ]
  node [
    id 58
    label "kompleksja"
  ]
  node [
    id 59
    label "fizjonomia"
  ]
  node [
    id 60
    label "zjawisko"
  ]
  node [
    id 61
    label "entity"
  ]
  node [
    id 62
    label "przemkni&#281;cie"
  ]
  node [
    id 63
    label "zabrzmienie"
  ]
  node [
    id 64
    label "przebycie"
  ]
  node [
    id 65
    label "zdarzenie_si&#281;"
  ]
  node [
    id 66
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 67
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 68
    label "przedstawienie"
  ]
  node [
    id 69
    label "znikn&#261;&#263;"
  ]
  node [
    id 70
    label "po&#380;egna&#263;_si&#281;_z_&#380;yciem"
  ]
  node [
    id 71
    label "sko&#324;czy&#263;"
  ]
  node [
    id 72
    label "gulf"
  ]
  node [
    id 73
    label "die"
  ]
  node [
    id 74
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 75
    label "przegra&#263;"
  ]
  node [
    id 76
    label "przepa&#347;&#263;"
  ]
  node [
    id 77
    label "fail"
  ]
  node [
    id 78
    label "przesta&#263;"
  ]
  node [
    id 79
    label "r&#243;&#380;nica"
  ]
  node [
    id 80
    label "podzia&#263;_si&#281;"
  ]
  node [
    id 81
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 82
    label "vanish"
  ]
  node [
    id 83
    label "zmarnowa&#263;_si&#281;"
  ]
  node [
    id 84
    label "pa&#347;&#263;"
  ]
  node [
    id 85
    label "dziura"
  ]
  node [
    id 86
    label "collapse"
  ]
  node [
    id 87
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 88
    label "zrobi&#263;"
  ]
  node [
    id 89
    label "end"
  ]
  node [
    id 90
    label "zako&#324;czy&#263;"
  ]
  node [
    id 91
    label "communicate"
  ]
  node [
    id 92
    label "sta&#263;_si&#281;"
  ]
  node [
    id 93
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 94
    label "straci&#263;_na_sile"
  ]
  node [
    id 95
    label "worsen"
  ]
  node [
    id 96
    label "ponie&#347;&#263;"
  ]
  node [
    id 97
    label "play"
  ]
  node [
    id 98
    label "coating"
  ]
  node [
    id 99
    label "drop"
  ]
  node [
    id 100
    label "leave_office"
  ]
  node [
    id 101
    label "wyj&#347;&#263;"
  ]
  node [
    id 102
    label "dissolve"
  ]
  node [
    id 103
    label "zatoka"
  ]
  node [
    id 104
    label "Chocho&#322;"
  ]
  node [
    id 105
    label "Herkules_Poirot"
  ]
  node [
    id 106
    label "Edyp"
  ]
  node [
    id 107
    label "ludzko&#347;&#263;"
  ]
  node [
    id 108
    label "parali&#380;owa&#263;"
  ]
  node [
    id 109
    label "Harry_Potter"
  ]
  node [
    id 110
    label "Casanova"
  ]
  node [
    id 111
    label "Gargantua"
  ]
  node [
    id 112
    label "Zgredek"
  ]
  node [
    id 113
    label "Winnetou"
  ]
  node [
    id 114
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 115
    label "Dulcynea"
  ]
  node [
    id 116
    label "kategoria_gramatyczna"
  ]
  node [
    id 117
    label "g&#322;owa"
  ]
  node [
    id 118
    label "figura"
  ]
  node [
    id 119
    label "portrecista"
  ]
  node [
    id 120
    label "person"
  ]
  node [
    id 121
    label "Sherlock_Holmes"
  ]
  node [
    id 122
    label "Quasimodo"
  ]
  node [
    id 123
    label "Plastu&#347;"
  ]
  node [
    id 124
    label "Faust"
  ]
  node [
    id 125
    label "Wallenrod"
  ]
  node [
    id 126
    label "Dwukwiat"
  ]
  node [
    id 127
    label "koniugacja"
  ]
  node [
    id 128
    label "profanum"
  ]
  node [
    id 129
    label "Don_Juan"
  ]
  node [
    id 130
    label "Don_Kiszot"
  ]
  node [
    id 131
    label "mikrokosmos"
  ]
  node [
    id 132
    label "duch"
  ]
  node [
    id 133
    label "antropochoria"
  ]
  node [
    id 134
    label "oddzia&#322;ywanie"
  ]
  node [
    id 135
    label "Hamlet"
  ]
  node [
    id 136
    label "Werter"
  ]
  node [
    id 137
    label "istota"
  ]
  node [
    id 138
    label "Szwejk"
  ]
  node [
    id 139
    label "homo_sapiens"
  ]
  node [
    id 140
    label "mentalno&#347;&#263;"
  ]
  node [
    id 141
    label "superego"
  ]
  node [
    id 142
    label "znaczenie"
  ]
  node [
    id 143
    label "wn&#281;trze"
  ]
  node [
    id 144
    label "charakterystyka"
  ]
  node [
    id 145
    label "zaistnie&#263;"
  ]
  node [
    id 146
    label "Osjan"
  ]
  node [
    id 147
    label "kto&#347;"
  ]
  node [
    id 148
    label "wygl&#261;d"
  ]
  node [
    id 149
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 150
    label "wytw&#243;r"
  ]
  node [
    id 151
    label "trim"
  ]
  node [
    id 152
    label "poby&#263;"
  ]
  node [
    id 153
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 154
    label "Aspazja"
  ]
  node [
    id 155
    label "punkt_widzenia"
  ]
  node [
    id 156
    label "wytrzyma&#263;"
  ]
  node [
    id 157
    label "budowa"
  ]
  node [
    id 158
    label "formacja"
  ]
  node [
    id 159
    label "pozosta&#263;"
  ]
  node [
    id 160
    label "point"
  ]
  node [
    id 161
    label "go&#347;&#263;"
  ]
  node [
    id 162
    label "hamper"
  ]
  node [
    id 163
    label "spasm"
  ]
  node [
    id 164
    label "mrozi&#263;"
  ]
  node [
    id 165
    label "pora&#380;a&#263;"
  ]
  node [
    id 166
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 167
    label "fleksja"
  ]
  node [
    id 168
    label "liczba"
  ]
  node [
    id 169
    label "coupling"
  ]
  node [
    id 170
    label "tryb"
  ]
  node [
    id 171
    label "czasownik"
  ]
  node [
    id 172
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 173
    label "orz&#281;sek"
  ]
  node [
    id 174
    label "pryncypa&#322;"
  ]
  node [
    id 175
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 176
    label "kszta&#322;t"
  ]
  node [
    id 177
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 178
    label "wiedza"
  ]
  node [
    id 179
    label "kierowa&#263;"
  ]
  node [
    id 180
    label "alkohol"
  ]
  node [
    id 181
    label "zdolno&#347;&#263;"
  ]
  node [
    id 182
    label "&#380;ycie"
  ]
  node [
    id 183
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 184
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 185
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 186
    label "sztuka"
  ]
  node [
    id 187
    label "dekiel"
  ]
  node [
    id 188
    label "ro&#347;lina"
  ]
  node [
    id 189
    label "&#347;ci&#281;cie"
  ]
  node [
    id 190
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 191
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 192
    label "&#347;ci&#281;gno"
  ]
  node [
    id 193
    label "noosfera"
  ]
  node [
    id 194
    label "byd&#322;o"
  ]
  node [
    id 195
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 196
    label "makrocefalia"
  ]
  node [
    id 197
    label "obiekt"
  ]
  node [
    id 198
    label "ucho"
  ]
  node [
    id 199
    label "g&#243;ra"
  ]
  node [
    id 200
    label "m&#243;zg"
  ]
  node [
    id 201
    label "kierownictwo"
  ]
  node [
    id 202
    label "fryzura"
  ]
  node [
    id 203
    label "umys&#322;"
  ]
  node [
    id 204
    label "cia&#322;o"
  ]
  node [
    id 205
    label "cz&#322;onek"
  ]
  node [
    id 206
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 207
    label "czaszka"
  ]
  node [
    id 208
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 209
    label "dziedzina"
  ]
  node [
    id 210
    label "powodowanie"
  ]
  node [
    id 211
    label "hipnotyzowanie"
  ]
  node [
    id 212
    label "&#347;lad"
  ]
  node [
    id 213
    label "docieranie"
  ]
  node [
    id 214
    label "natural_process"
  ]
  node [
    id 215
    label "reakcja_chemiczna"
  ]
  node [
    id 216
    label "wdzieranie_si&#281;"
  ]
  node [
    id 217
    label "act"
  ]
  node [
    id 218
    label "rezultat"
  ]
  node [
    id 219
    label "lobbysta"
  ]
  node [
    id 220
    label "allochoria"
  ]
  node [
    id 221
    label "fotograf"
  ]
  node [
    id 222
    label "malarz"
  ]
  node [
    id 223
    label "artysta"
  ]
  node [
    id 224
    label "p&#322;aszczyzna"
  ]
  node [
    id 225
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 226
    label "bierka_szachowa"
  ]
  node [
    id 227
    label "obiekt_matematyczny"
  ]
  node [
    id 228
    label "gestaltyzm"
  ]
  node [
    id 229
    label "styl"
  ]
  node [
    id 230
    label "obraz"
  ]
  node [
    id 231
    label "rzecz"
  ]
  node [
    id 232
    label "d&#378;wi&#281;k"
  ]
  node [
    id 233
    label "character"
  ]
  node [
    id 234
    label "rze&#378;ba"
  ]
  node [
    id 235
    label "stylistyka"
  ]
  node [
    id 236
    label "figure"
  ]
  node [
    id 237
    label "miejsce"
  ]
  node [
    id 238
    label "antycypacja"
  ]
  node [
    id 239
    label "ornamentyka"
  ]
  node [
    id 240
    label "informacja"
  ]
  node [
    id 241
    label "facet"
  ]
  node [
    id 242
    label "popis"
  ]
  node [
    id 243
    label "wiersz"
  ]
  node [
    id 244
    label "symetria"
  ]
  node [
    id 245
    label "lingwistyka_kognitywna"
  ]
  node [
    id 246
    label "karta"
  ]
  node [
    id 247
    label "shape"
  ]
  node [
    id 248
    label "podzbi&#243;r"
  ]
  node [
    id 249
    label "perspektywa"
  ]
  node [
    id 250
    label "Szekspir"
  ]
  node [
    id 251
    label "Mickiewicz"
  ]
  node [
    id 252
    label "cierpienie"
  ]
  node [
    id 253
    label "piek&#322;o"
  ]
  node [
    id 254
    label "human_body"
  ]
  node [
    id 255
    label "ofiarowywanie"
  ]
  node [
    id 256
    label "sfera_afektywna"
  ]
  node [
    id 257
    label "nekromancja"
  ]
  node [
    id 258
    label "Po&#347;wist"
  ]
  node [
    id 259
    label "podekscytowanie"
  ]
  node [
    id 260
    label "deformowanie"
  ]
  node [
    id 261
    label "sumienie"
  ]
  node [
    id 262
    label "deformowa&#263;"
  ]
  node [
    id 263
    label "zjawa"
  ]
  node [
    id 264
    label "zmar&#322;y"
  ]
  node [
    id 265
    label "istota_nadprzyrodzona"
  ]
  node [
    id 266
    label "power"
  ]
  node [
    id 267
    label "ofiarowywa&#263;"
  ]
  node [
    id 268
    label "oddech"
  ]
  node [
    id 269
    label "seksualno&#347;&#263;"
  ]
  node [
    id 270
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 271
    label "byt"
  ]
  node [
    id 272
    label "si&#322;a"
  ]
  node [
    id 273
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 274
    label "ego"
  ]
  node [
    id 275
    label "ofiarowanie"
  ]
  node [
    id 276
    label "kompleks"
  ]
  node [
    id 277
    label "zapalno&#347;&#263;"
  ]
  node [
    id 278
    label "T&#281;sknica"
  ]
  node [
    id 279
    label "ofiarowa&#263;"
  ]
  node [
    id 280
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 281
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 282
    label "passion"
  ]
  node [
    id 283
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 284
    label "atom"
  ]
  node [
    id 285
    label "odbicie"
  ]
  node [
    id 286
    label "przyroda"
  ]
  node [
    id 287
    label "Ziemia"
  ]
  node [
    id 288
    label "kosmos"
  ]
  node [
    id 289
    label "miniatura"
  ]
  node [
    id 290
    label "ambasada"
  ]
  node [
    id 291
    label "Szwecja"
  ]
  node [
    id 292
    label "Erik"
  ]
  node [
    id 293
    label "Friberg"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 290
    target 291
  ]
  edge [
    source 292
    target 293
  ]
]
