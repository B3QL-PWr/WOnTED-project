graph [
  node [
    id 0
    label "sanktuarium"
    origin "text"
  ]
  node [
    id 1
    label "wambierzyckiej"
    origin "text"
  ]
  node [
    id 2
    label "kr&#243;lowa"
    origin "text"
  ]
  node [
    id 3
    label "rodzina"
    origin "text"
  ]
  node [
    id 4
    label "miejsce"
  ]
  node [
    id 5
    label "Jasna_G&#243;ra"
  ]
  node [
    id 6
    label "&#321;agiewniki"
  ]
  node [
    id 7
    label "Liche&#324;_Stary"
  ]
  node [
    id 8
    label "warunek_lokalowy"
  ]
  node [
    id 9
    label "plac"
  ]
  node [
    id 10
    label "location"
  ]
  node [
    id 11
    label "uwaga"
  ]
  node [
    id 12
    label "przestrze&#324;"
  ]
  node [
    id 13
    label "status"
  ]
  node [
    id 14
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 15
    label "chwila"
  ]
  node [
    id 16
    label "cia&#322;o"
  ]
  node [
    id 17
    label "cecha"
  ]
  node [
    id 18
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 19
    label "praca"
  ]
  node [
    id 20
    label "rz&#261;d"
  ]
  node [
    id 21
    label "Krak&#243;w"
  ]
  node [
    id 22
    label "Ba&#322;uty"
  ]
  node [
    id 23
    label "Wiktoria"
  ]
  node [
    id 24
    label "kr&#243;lowa_matka"
  ]
  node [
    id 25
    label "Izabela_I_Kastylijska"
  ]
  node [
    id 26
    label "El&#380;bieta_I"
  ]
  node [
    id 27
    label "strzelec"
  ]
  node [
    id 28
    label "kand"
  ]
  node [
    id 29
    label "Bona"
  ]
  node [
    id 30
    label "pszczo&#322;a"
  ]
  node [
    id 31
    label "mr&#243;wka"
  ]
  node [
    id 32
    label "pracownik"
  ]
  node [
    id 33
    label "kolonia"
  ]
  node [
    id 34
    label "przemytnik"
  ]
  node [
    id 35
    label "&#380;&#261;d&#322;&#243;wka"
  ]
  node [
    id 36
    label "mr&#243;wkowate"
  ]
  node [
    id 37
    label "zapaleniec"
  ]
  node [
    id 38
    label "myrmekofil"
  ]
  node [
    id 39
    label "&#380;o&#322;nierz"
  ]
  node [
    id 40
    label "figura"
  ]
  node [
    id 41
    label "cz&#322;owiek"
  ]
  node [
    id 42
    label "futbolista"
  ]
  node [
    id 43
    label "sportowiec"
  ]
  node [
    id 44
    label "Renata_Mauer"
  ]
  node [
    id 45
    label "pszczo&#322;y_&#380;&#261;dl&#261;ce"
  ]
  node [
    id 46
    label "wyroi&#263;_si&#281;"
  ]
  node [
    id 47
    label "osypanie_si&#281;"
  ]
  node [
    id 48
    label "stebnik"
  ]
  node [
    id 49
    label "wirus_ostrego_parali&#380;u_pszcz&#243;&#322;"
  ]
  node [
    id 50
    label "zimowla"
  ]
  node [
    id 51
    label "obsypywanie_si&#281;"
  ]
  node [
    id 52
    label "przegra"
  ]
  node [
    id 53
    label "rojenie_si&#281;"
  ]
  node [
    id 54
    label "wirus_chronicznego_parali&#380;u_pszcz&#243;&#322;"
  ]
  node [
    id 55
    label "mi&#243;d"
  ]
  node [
    id 56
    label "wirus_zdeformowanych_skrzyde&#322;"
  ]
  node [
    id 57
    label "wirus_izraelskiego_parali&#380;u_pszcz&#243;&#322;"
  ]
  node [
    id 58
    label "r&#243;j"
  ]
  node [
    id 59
    label "melitofag"
  ]
  node [
    id 60
    label "podkurzacz"
  ]
  node [
    id 61
    label "wyrojenie_si&#281;"
  ]
  node [
    id 62
    label "w&#322;oszczyzna"
  ]
  node [
    id 63
    label "Wielka_Brytania"
  ]
  node [
    id 64
    label "jedzenie"
  ]
  node [
    id 65
    label "powinowaci"
  ]
  node [
    id 66
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 67
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 68
    label "rodze&#324;stwo"
  ]
  node [
    id 69
    label "jednostka_systematyczna"
  ]
  node [
    id 70
    label "krewni"
  ]
  node [
    id 71
    label "Ossoli&#324;scy"
  ]
  node [
    id 72
    label "potomstwo"
  ]
  node [
    id 73
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 74
    label "theater"
  ]
  node [
    id 75
    label "zbi&#243;r"
  ]
  node [
    id 76
    label "Soplicowie"
  ]
  node [
    id 77
    label "kin"
  ]
  node [
    id 78
    label "family"
  ]
  node [
    id 79
    label "rodzice"
  ]
  node [
    id 80
    label "ordynacja"
  ]
  node [
    id 81
    label "grupa"
  ]
  node [
    id 82
    label "dom_rodzinny"
  ]
  node [
    id 83
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 84
    label "Ostrogscy"
  ]
  node [
    id 85
    label "bliscy"
  ]
  node [
    id 86
    label "przyjaciel_domu"
  ]
  node [
    id 87
    label "dom"
  ]
  node [
    id 88
    label "Firlejowie"
  ]
  node [
    id 89
    label "Kossakowie"
  ]
  node [
    id 90
    label "Czartoryscy"
  ]
  node [
    id 91
    label "Sapiehowie"
  ]
  node [
    id 92
    label "odm&#322;adzanie"
  ]
  node [
    id 93
    label "liga"
  ]
  node [
    id 94
    label "asymilowanie"
  ]
  node [
    id 95
    label "gromada"
  ]
  node [
    id 96
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 97
    label "asymilowa&#263;"
  ]
  node [
    id 98
    label "egzemplarz"
  ]
  node [
    id 99
    label "Entuzjastki"
  ]
  node [
    id 100
    label "kompozycja"
  ]
  node [
    id 101
    label "Terranie"
  ]
  node [
    id 102
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 103
    label "category"
  ]
  node [
    id 104
    label "pakiet_klimatyczny"
  ]
  node [
    id 105
    label "oddzia&#322;"
  ]
  node [
    id 106
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 107
    label "cz&#261;steczka"
  ]
  node [
    id 108
    label "stage_set"
  ]
  node [
    id 109
    label "type"
  ]
  node [
    id 110
    label "specgrupa"
  ]
  node [
    id 111
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 112
    label "&#346;wietliki"
  ]
  node [
    id 113
    label "odm&#322;odzenie"
  ]
  node [
    id 114
    label "Eurogrupa"
  ]
  node [
    id 115
    label "odm&#322;adza&#263;"
  ]
  node [
    id 116
    label "formacja_geologiczna"
  ]
  node [
    id 117
    label "harcerze_starsi"
  ]
  node [
    id 118
    label "series"
  ]
  node [
    id 119
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 120
    label "uprawianie"
  ]
  node [
    id 121
    label "praca_rolnicza"
  ]
  node [
    id 122
    label "collection"
  ]
  node [
    id 123
    label "dane"
  ]
  node [
    id 124
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 125
    label "poj&#281;cie"
  ]
  node [
    id 126
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 127
    label "sum"
  ]
  node [
    id 128
    label "gathering"
  ]
  node [
    id 129
    label "album"
  ]
  node [
    id 130
    label "grono"
  ]
  node [
    id 131
    label "kuzynostwo"
  ]
  node [
    id 132
    label "stan_cywilny"
  ]
  node [
    id 133
    label "para"
  ]
  node [
    id 134
    label "matrymonialny"
  ]
  node [
    id 135
    label "lewirat"
  ]
  node [
    id 136
    label "sakrament"
  ]
  node [
    id 137
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 138
    label "zwi&#261;zek"
  ]
  node [
    id 139
    label "partia"
  ]
  node [
    id 140
    label "czeladka"
  ]
  node [
    id 141
    label "dzietno&#347;&#263;"
  ]
  node [
    id 142
    label "bawienie_si&#281;"
  ]
  node [
    id 143
    label "pomiot"
  ]
  node [
    id 144
    label "starzy"
  ]
  node [
    id 145
    label "pokolenie"
  ]
  node [
    id 146
    label "wapniaki"
  ]
  node [
    id 147
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 148
    label "substancja_mieszkaniowa"
  ]
  node [
    id 149
    label "instytucja"
  ]
  node [
    id 150
    label "siedziba"
  ]
  node [
    id 151
    label "budynek"
  ]
  node [
    id 152
    label "stead"
  ]
  node [
    id 153
    label "garderoba"
  ]
  node [
    id 154
    label "wiecha"
  ]
  node [
    id 155
    label "fratria"
  ]
  node [
    id 156
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 157
    label "obrz&#281;d"
  ]
  node [
    id 158
    label "przybli&#380;enie"
  ]
  node [
    id 159
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 160
    label "kategoria"
  ]
  node [
    id 161
    label "szpaler"
  ]
  node [
    id 162
    label "lon&#380;a"
  ]
  node [
    id 163
    label "uporz&#261;dkowanie"
  ]
  node [
    id 164
    label "egzekutywa"
  ]
  node [
    id 165
    label "premier"
  ]
  node [
    id 166
    label "Londyn"
  ]
  node [
    id 167
    label "gabinet_cieni"
  ]
  node [
    id 168
    label "number"
  ]
  node [
    id 169
    label "Konsulat"
  ]
  node [
    id 170
    label "tract"
  ]
  node [
    id 171
    label "klasa"
  ]
  node [
    id 172
    label "w&#322;adza"
  ]
  node [
    id 173
    label "folk_music"
  ]
  node [
    id 174
    label "Wambierzyckiej"
  ]
  node [
    id 175
    label "kr&#243;lowy"
  ]
  node [
    id 176
    label "Pius"
  ]
  node [
    id 177
    label "XI"
  ]
  node [
    id 178
    label "matka"
  ]
  node [
    id 179
    label "boski"
  ]
  node [
    id 180
    label "ludwik"
  ]
  node [
    id 181
    label "von"
  ]
  node [
    id 182
    label "Panwitz"
  ]
  node [
    id 183
    label "wojna"
  ]
  node [
    id 184
    label "trzydziestoletni"
  ]
  node [
    id 185
    label "Franciszka"
  ]
  node [
    id 186
    label "Antoni"
  ]
  node [
    id 187
    label "Goetzen"
  ]
  node [
    id 188
    label "bo&#380;y"
  ]
  node [
    id 189
    label "Jan"
  ]
  node [
    id 190
    label "pawe&#322;"
  ]
  node [
    id 191
    label "ii"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 178
    target 188
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 182
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 181
    target 185
  ]
  edge [
    source 181
    target 186
  ]
  edge [
    source 181
    target 187
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 185
    target 187
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 189
    target 190
  ]
  edge [
    source 189
    target 191
  ]
  edge [
    source 190
    target 191
  ]
]
