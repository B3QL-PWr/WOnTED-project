graph [
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wi&#281;t&#243;w"
    origin "text"
  ]
  node [
    id 2
    label "kolejny"
  ]
  node [
    id 3
    label "nowo"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "bie&#380;&#261;cy"
  ]
  node [
    id 6
    label "drugi"
  ]
  node [
    id 7
    label "narybek"
  ]
  node [
    id 8
    label "obcy"
  ]
  node [
    id 9
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 10
    label "nowotny"
  ]
  node [
    id 11
    label "nadprzyrodzony"
  ]
  node [
    id 12
    label "nieznany"
  ]
  node [
    id 13
    label "pozaludzki"
  ]
  node [
    id 14
    label "obco"
  ]
  node [
    id 15
    label "tameczny"
  ]
  node [
    id 16
    label "osoba"
  ]
  node [
    id 17
    label "nieznajomo"
  ]
  node [
    id 18
    label "inny"
  ]
  node [
    id 19
    label "cudzy"
  ]
  node [
    id 20
    label "istota_&#380;ywa"
  ]
  node [
    id 21
    label "zaziemsko"
  ]
  node [
    id 22
    label "jednoczesny"
  ]
  node [
    id 23
    label "unowocze&#347;nianie"
  ]
  node [
    id 24
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 25
    label "tera&#378;niejszy"
  ]
  node [
    id 26
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 27
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 28
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 29
    label "nast&#281;pnie"
  ]
  node [
    id 30
    label "nastopny"
  ]
  node [
    id 31
    label "kolejno"
  ]
  node [
    id 32
    label "kt&#243;ry&#347;"
  ]
  node [
    id 33
    label "sw&#243;j"
  ]
  node [
    id 34
    label "przeciwny"
  ]
  node [
    id 35
    label "wt&#243;ry"
  ]
  node [
    id 36
    label "dzie&#324;"
  ]
  node [
    id 37
    label "odwrotnie"
  ]
  node [
    id 38
    label "podobny"
  ]
  node [
    id 39
    label "bie&#380;&#261;co"
  ]
  node [
    id 40
    label "ci&#261;g&#322;y"
  ]
  node [
    id 41
    label "aktualny"
  ]
  node [
    id 42
    label "ludzko&#347;&#263;"
  ]
  node [
    id 43
    label "asymilowanie"
  ]
  node [
    id 44
    label "wapniak"
  ]
  node [
    id 45
    label "asymilowa&#263;"
  ]
  node [
    id 46
    label "os&#322;abia&#263;"
  ]
  node [
    id 47
    label "posta&#263;"
  ]
  node [
    id 48
    label "hominid"
  ]
  node [
    id 49
    label "podw&#322;adny"
  ]
  node [
    id 50
    label "os&#322;abianie"
  ]
  node [
    id 51
    label "g&#322;owa"
  ]
  node [
    id 52
    label "figura"
  ]
  node [
    id 53
    label "portrecista"
  ]
  node [
    id 54
    label "dwun&#243;g"
  ]
  node [
    id 55
    label "profanum"
  ]
  node [
    id 56
    label "mikrokosmos"
  ]
  node [
    id 57
    label "nasada"
  ]
  node [
    id 58
    label "duch"
  ]
  node [
    id 59
    label "antropochoria"
  ]
  node [
    id 60
    label "wz&#243;r"
  ]
  node [
    id 61
    label "senior"
  ]
  node [
    id 62
    label "oddzia&#322;ywanie"
  ]
  node [
    id 63
    label "Adam"
  ]
  node [
    id 64
    label "homo_sapiens"
  ]
  node [
    id 65
    label "polifag"
  ]
  node [
    id 66
    label "dopiero_co"
  ]
  node [
    id 67
    label "formacja"
  ]
  node [
    id 68
    label "potomstwo"
  ]
  node [
    id 69
    label "&#346;wi&#281;t&#243;w"
  ]
  node [
    id 70
    label "Deutsch"
  ]
  node [
    id 71
    label "Wette"
  ]
  node [
    id 72
    label "Swetow"
  ]
  node [
    id 73
    label "thewtonicalis"
  ]
  node [
    id 74
    label "wojna"
  ]
  node [
    id 75
    label "30"
  ]
  node [
    id 76
    label "letni"
  ]
  node [
    id 77
    label "&#346;wi&#281;towa"
  ]
  node [
    id 78
    label "Rac&#322;awice"
  ]
  node [
    id 79
    label "&#347;l&#261;ski"
  ]
  node [
    id 80
    label "kluba"
  ]
  node [
    id 81
    label "sportowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 69
    target 80
  ]
  edge [
    source 69
    target 81
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 76
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 80
    target 81
  ]
]
