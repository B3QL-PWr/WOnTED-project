graph [
  node [
    id 0
    label "nieszczepienie"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "prywatny"
    origin "text"
  ]
  node [
    id 5
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "fi&#324;ski"
    origin "text"
  ]
  node [
    id 7
    label "minister"
    origin "text"
  ]
  node [
    id 8
    label "nauka"
    origin "text"
  ]
  node [
    id 9
    label "szkolnictwo"
    origin "text"
  ]
  node [
    id 10
    label "sanny"
    origin "text"
  ]
  node [
    id 11
    label "grahn"
    origin "text"
  ]
  node [
    id 12
    label "laasonen"
    origin "text"
  ]
  node [
    id 13
    label "utulenie"
  ]
  node [
    id 14
    label "pediatra"
  ]
  node [
    id 15
    label "dzieciak"
  ]
  node [
    id 16
    label "utulanie"
  ]
  node [
    id 17
    label "dzieciarnia"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "niepe&#322;noletni"
  ]
  node [
    id 20
    label "organizm"
  ]
  node [
    id 21
    label "utula&#263;"
  ]
  node [
    id 22
    label "cz&#322;owieczek"
  ]
  node [
    id 23
    label "fledgling"
  ]
  node [
    id 24
    label "zwierz&#281;"
  ]
  node [
    id 25
    label "utuli&#263;"
  ]
  node [
    id 26
    label "m&#322;odzik"
  ]
  node [
    id 27
    label "pedofil"
  ]
  node [
    id 28
    label "m&#322;odziak"
  ]
  node [
    id 29
    label "potomek"
  ]
  node [
    id 30
    label "entliczek-pentliczek"
  ]
  node [
    id 31
    label "potomstwo"
  ]
  node [
    id 32
    label "sraluch"
  ]
  node [
    id 33
    label "zbi&#243;r"
  ]
  node [
    id 34
    label "czeladka"
  ]
  node [
    id 35
    label "dzietno&#347;&#263;"
  ]
  node [
    id 36
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 37
    label "bawienie_si&#281;"
  ]
  node [
    id 38
    label "pomiot"
  ]
  node [
    id 39
    label "grupa"
  ]
  node [
    id 40
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 41
    label "kinderbal"
  ]
  node [
    id 42
    label "krewny"
  ]
  node [
    id 43
    label "ludzko&#347;&#263;"
  ]
  node [
    id 44
    label "asymilowanie"
  ]
  node [
    id 45
    label "wapniak"
  ]
  node [
    id 46
    label "asymilowa&#263;"
  ]
  node [
    id 47
    label "os&#322;abia&#263;"
  ]
  node [
    id 48
    label "posta&#263;"
  ]
  node [
    id 49
    label "hominid"
  ]
  node [
    id 50
    label "podw&#322;adny"
  ]
  node [
    id 51
    label "os&#322;abianie"
  ]
  node [
    id 52
    label "g&#322;owa"
  ]
  node [
    id 53
    label "figura"
  ]
  node [
    id 54
    label "portrecista"
  ]
  node [
    id 55
    label "dwun&#243;g"
  ]
  node [
    id 56
    label "profanum"
  ]
  node [
    id 57
    label "mikrokosmos"
  ]
  node [
    id 58
    label "nasada"
  ]
  node [
    id 59
    label "duch"
  ]
  node [
    id 60
    label "antropochoria"
  ]
  node [
    id 61
    label "osoba"
  ]
  node [
    id 62
    label "wz&#243;r"
  ]
  node [
    id 63
    label "senior"
  ]
  node [
    id 64
    label "oddzia&#322;ywanie"
  ]
  node [
    id 65
    label "Adam"
  ]
  node [
    id 66
    label "homo_sapiens"
  ]
  node [
    id 67
    label "polifag"
  ]
  node [
    id 68
    label "ma&#322;oletny"
  ]
  node [
    id 69
    label "m&#322;ody"
  ]
  node [
    id 70
    label "p&#322;aszczyzna"
  ]
  node [
    id 71
    label "odwadnia&#263;"
  ]
  node [
    id 72
    label "przyswoi&#263;"
  ]
  node [
    id 73
    label "sk&#243;ra"
  ]
  node [
    id 74
    label "odwodni&#263;"
  ]
  node [
    id 75
    label "ewoluowanie"
  ]
  node [
    id 76
    label "staw"
  ]
  node [
    id 77
    label "ow&#322;osienie"
  ]
  node [
    id 78
    label "unerwienie"
  ]
  node [
    id 79
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 80
    label "reakcja"
  ]
  node [
    id 81
    label "wyewoluowanie"
  ]
  node [
    id 82
    label "przyswajanie"
  ]
  node [
    id 83
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 84
    label "wyewoluowa&#263;"
  ]
  node [
    id 85
    label "miejsce"
  ]
  node [
    id 86
    label "biorytm"
  ]
  node [
    id 87
    label "ewoluowa&#263;"
  ]
  node [
    id 88
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 89
    label "istota_&#380;ywa"
  ]
  node [
    id 90
    label "otworzy&#263;"
  ]
  node [
    id 91
    label "otwiera&#263;"
  ]
  node [
    id 92
    label "czynnik_biotyczny"
  ]
  node [
    id 93
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 94
    label "otworzenie"
  ]
  node [
    id 95
    label "otwieranie"
  ]
  node [
    id 96
    label "individual"
  ]
  node [
    id 97
    label "ty&#322;"
  ]
  node [
    id 98
    label "szkielet"
  ]
  node [
    id 99
    label "obiekt"
  ]
  node [
    id 100
    label "przyswaja&#263;"
  ]
  node [
    id 101
    label "przyswojenie"
  ]
  node [
    id 102
    label "odwadnianie"
  ]
  node [
    id 103
    label "odwodnienie"
  ]
  node [
    id 104
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 105
    label "starzenie_si&#281;"
  ]
  node [
    id 106
    label "uk&#322;ad"
  ]
  node [
    id 107
    label "prz&#243;d"
  ]
  node [
    id 108
    label "temperatura"
  ]
  node [
    id 109
    label "l&#281;d&#378;wie"
  ]
  node [
    id 110
    label "cia&#322;o"
  ]
  node [
    id 111
    label "cz&#322;onek"
  ]
  node [
    id 112
    label "degenerat"
  ]
  node [
    id 113
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 114
    label "zwyrol"
  ]
  node [
    id 115
    label "czerniak"
  ]
  node [
    id 116
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 117
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 118
    label "paszcza"
  ]
  node [
    id 119
    label "popapraniec"
  ]
  node [
    id 120
    label "skuba&#263;"
  ]
  node [
    id 121
    label "skubanie"
  ]
  node [
    id 122
    label "skubni&#281;cie"
  ]
  node [
    id 123
    label "agresja"
  ]
  node [
    id 124
    label "zwierz&#281;ta"
  ]
  node [
    id 125
    label "fukni&#281;cie"
  ]
  node [
    id 126
    label "farba"
  ]
  node [
    id 127
    label "fukanie"
  ]
  node [
    id 128
    label "gad"
  ]
  node [
    id 129
    label "siedzie&#263;"
  ]
  node [
    id 130
    label "oswaja&#263;"
  ]
  node [
    id 131
    label "tresowa&#263;"
  ]
  node [
    id 132
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 133
    label "poligamia"
  ]
  node [
    id 134
    label "oz&#243;r"
  ]
  node [
    id 135
    label "skubn&#261;&#263;"
  ]
  node [
    id 136
    label "wios&#322;owa&#263;"
  ]
  node [
    id 137
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 138
    label "le&#380;enie"
  ]
  node [
    id 139
    label "niecz&#322;owiek"
  ]
  node [
    id 140
    label "wios&#322;owanie"
  ]
  node [
    id 141
    label "napasienie_si&#281;"
  ]
  node [
    id 142
    label "wiwarium"
  ]
  node [
    id 143
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 144
    label "animalista"
  ]
  node [
    id 145
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 146
    label "budowa"
  ]
  node [
    id 147
    label "hodowla"
  ]
  node [
    id 148
    label "pasienie_si&#281;"
  ]
  node [
    id 149
    label "sodomita"
  ]
  node [
    id 150
    label "monogamia"
  ]
  node [
    id 151
    label "przyssawka"
  ]
  node [
    id 152
    label "zachowanie"
  ]
  node [
    id 153
    label "budowa_cia&#322;a"
  ]
  node [
    id 154
    label "okrutnik"
  ]
  node [
    id 155
    label "grzbiet"
  ]
  node [
    id 156
    label "weterynarz"
  ]
  node [
    id 157
    label "&#322;eb"
  ]
  node [
    id 158
    label "wylinka"
  ]
  node [
    id 159
    label "bestia"
  ]
  node [
    id 160
    label "poskramia&#263;"
  ]
  node [
    id 161
    label "fauna"
  ]
  node [
    id 162
    label "treser"
  ]
  node [
    id 163
    label "siedzenie"
  ]
  node [
    id 164
    label "le&#380;e&#263;"
  ]
  node [
    id 165
    label "uspokojenie"
  ]
  node [
    id 166
    label "utulenie_si&#281;"
  ]
  node [
    id 167
    label "u&#347;pienie"
  ]
  node [
    id 168
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 169
    label "uspokoi&#263;"
  ]
  node [
    id 170
    label "utulanie_si&#281;"
  ]
  node [
    id 171
    label "usypianie"
  ]
  node [
    id 172
    label "pocieszanie"
  ]
  node [
    id 173
    label "uspokajanie"
  ]
  node [
    id 174
    label "usypia&#263;"
  ]
  node [
    id 175
    label "uspokaja&#263;"
  ]
  node [
    id 176
    label "wyliczanka"
  ]
  node [
    id 177
    label "specjalista"
  ]
  node [
    id 178
    label "harcerz"
  ]
  node [
    id 179
    label "ch&#322;opta&#347;"
  ]
  node [
    id 180
    label "zawodnik"
  ]
  node [
    id 181
    label "go&#322;ow&#261;s"
  ]
  node [
    id 182
    label "m&#322;ode"
  ]
  node [
    id 183
    label "stopie&#324;_harcerski"
  ]
  node [
    id 184
    label "g&#243;wniarz"
  ]
  node [
    id 185
    label "beniaminek"
  ]
  node [
    id 186
    label "dewiant"
  ]
  node [
    id 187
    label "istotka"
  ]
  node [
    id 188
    label "bech"
  ]
  node [
    id 189
    label "dziecinny"
  ]
  node [
    id 190
    label "naiwniak"
  ]
  node [
    id 191
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 192
    label "mie&#263;_miejsce"
  ]
  node [
    id 193
    label "equal"
  ]
  node [
    id 194
    label "trwa&#263;"
  ]
  node [
    id 195
    label "chodzi&#263;"
  ]
  node [
    id 196
    label "si&#281;ga&#263;"
  ]
  node [
    id 197
    label "stan"
  ]
  node [
    id 198
    label "obecno&#347;&#263;"
  ]
  node [
    id 199
    label "stand"
  ]
  node [
    id 200
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 201
    label "uczestniczy&#263;"
  ]
  node [
    id 202
    label "participate"
  ]
  node [
    id 203
    label "robi&#263;"
  ]
  node [
    id 204
    label "istnie&#263;"
  ]
  node [
    id 205
    label "pozostawa&#263;"
  ]
  node [
    id 206
    label "zostawa&#263;"
  ]
  node [
    id 207
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 208
    label "adhere"
  ]
  node [
    id 209
    label "compass"
  ]
  node [
    id 210
    label "korzysta&#263;"
  ]
  node [
    id 211
    label "appreciation"
  ]
  node [
    id 212
    label "osi&#261;ga&#263;"
  ]
  node [
    id 213
    label "dociera&#263;"
  ]
  node [
    id 214
    label "get"
  ]
  node [
    id 215
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 216
    label "mierzy&#263;"
  ]
  node [
    id 217
    label "u&#380;ywa&#263;"
  ]
  node [
    id 218
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 219
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 220
    label "exsert"
  ]
  node [
    id 221
    label "being"
  ]
  node [
    id 222
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 223
    label "cecha"
  ]
  node [
    id 224
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 225
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 226
    label "p&#322;ywa&#263;"
  ]
  node [
    id 227
    label "run"
  ]
  node [
    id 228
    label "bangla&#263;"
  ]
  node [
    id 229
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 230
    label "przebiega&#263;"
  ]
  node [
    id 231
    label "wk&#322;ada&#263;"
  ]
  node [
    id 232
    label "proceed"
  ]
  node [
    id 233
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 234
    label "carry"
  ]
  node [
    id 235
    label "bywa&#263;"
  ]
  node [
    id 236
    label "dziama&#263;"
  ]
  node [
    id 237
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 238
    label "stara&#263;_si&#281;"
  ]
  node [
    id 239
    label "para"
  ]
  node [
    id 240
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 241
    label "str&#243;j"
  ]
  node [
    id 242
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 243
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 244
    label "krok"
  ]
  node [
    id 245
    label "tryb"
  ]
  node [
    id 246
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 247
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 248
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 249
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 250
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 251
    label "continue"
  ]
  node [
    id 252
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 253
    label "Ohio"
  ]
  node [
    id 254
    label "wci&#281;cie"
  ]
  node [
    id 255
    label "Nowy_York"
  ]
  node [
    id 256
    label "warstwa"
  ]
  node [
    id 257
    label "samopoczucie"
  ]
  node [
    id 258
    label "Illinois"
  ]
  node [
    id 259
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 260
    label "state"
  ]
  node [
    id 261
    label "Jukatan"
  ]
  node [
    id 262
    label "Kalifornia"
  ]
  node [
    id 263
    label "Wirginia"
  ]
  node [
    id 264
    label "wektor"
  ]
  node [
    id 265
    label "Teksas"
  ]
  node [
    id 266
    label "Goa"
  ]
  node [
    id 267
    label "Waszyngton"
  ]
  node [
    id 268
    label "Massachusetts"
  ]
  node [
    id 269
    label "Alaska"
  ]
  node [
    id 270
    label "Arakan"
  ]
  node [
    id 271
    label "Hawaje"
  ]
  node [
    id 272
    label "Maryland"
  ]
  node [
    id 273
    label "punkt"
  ]
  node [
    id 274
    label "Michigan"
  ]
  node [
    id 275
    label "Arizona"
  ]
  node [
    id 276
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 277
    label "Georgia"
  ]
  node [
    id 278
    label "poziom"
  ]
  node [
    id 279
    label "Pensylwania"
  ]
  node [
    id 280
    label "shape"
  ]
  node [
    id 281
    label "Luizjana"
  ]
  node [
    id 282
    label "Nowy_Meksyk"
  ]
  node [
    id 283
    label "Alabama"
  ]
  node [
    id 284
    label "ilo&#347;&#263;"
  ]
  node [
    id 285
    label "Kansas"
  ]
  node [
    id 286
    label "Oregon"
  ]
  node [
    id 287
    label "Floryda"
  ]
  node [
    id 288
    label "Oklahoma"
  ]
  node [
    id 289
    label "jednostka_administracyjna"
  ]
  node [
    id 290
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 291
    label "kognicja"
  ]
  node [
    id 292
    label "object"
  ]
  node [
    id 293
    label "rozprawa"
  ]
  node [
    id 294
    label "temat"
  ]
  node [
    id 295
    label "wydarzenie"
  ]
  node [
    id 296
    label "szczeg&#243;&#322;"
  ]
  node [
    id 297
    label "proposition"
  ]
  node [
    id 298
    label "przes&#322;anka"
  ]
  node [
    id 299
    label "rzecz"
  ]
  node [
    id 300
    label "idea"
  ]
  node [
    id 301
    label "przebiec"
  ]
  node [
    id 302
    label "charakter"
  ]
  node [
    id 303
    label "czynno&#347;&#263;"
  ]
  node [
    id 304
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 305
    label "motyw"
  ]
  node [
    id 306
    label "przebiegni&#281;cie"
  ]
  node [
    id 307
    label "fabu&#322;a"
  ]
  node [
    id 308
    label "ideologia"
  ]
  node [
    id 309
    label "byt"
  ]
  node [
    id 310
    label "intelekt"
  ]
  node [
    id 311
    label "Kant"
  ]
  node [
    id 312
    label "p&#322;&#243;d"
  ]
  node [
    id 313
    label "cel"
  ]
  node [
    id 314
    label "poj&#281;cie"
  ]
  node [
    id 315
    label "istota"
  ]
  node [
    id 316
    label "pomys&#322;"
  ]
  node [
    id 317
    label "ideacja"
  ]
  node [
    id 318
    label "przedmiot"
  ]
  node [
    id 319
    label "wpadni&#281;cie"
  ]
  node [
    id 320
    label "mienie"
  ]
  node [
    id 321
    label "przyroda"
  ]
  node [
    id 322
    label "kultura"
  ]
  node [
    id 323
    label "wpa&#347;&#263;"
  ]
  node [
    id 324
    label "wpadanie"
  ]
  node [
    id 325
    label "wpada&#263;"
  ]
  node [
    id 326
    label "s&#261;d"
  ]
  node [
    id 327
    label "rozumowanie"
  ]
  node [
    id 328
    label "opracowanie"
  ]
  node [
    id 329
    label "proces"
  ]
  node [
    id 330
    label "obrady"
  ]
  node [
    id 331
    label "cytat"
  ]
  node [
    id 332
    label "tekst"
  ]
  node [
    id 333
    label "obja&#347;nienie"
  ]
  node [
    id 334
    label "s&#261;dzenie"
  ]
  node [
    id 335
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 336
    label "niuansowa&#263;"
  ]
  node [
    id 337
    label "element"
  ]
  node [
    id 338
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 339
    label "sk&#322;adnik"
  ]
  node [
    id 340
    label "zniuansowa&#263;"
  ]
  node [
    id 341
    label "fakt"
  ]
  node [
    id 342
    label "przyczyna"
  ]
  node [
    id 343
    label "wnioskowanie"
  ]
  node [
    id 344
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 345
    label "wyraz_pochodny"
  ]
  node [
    id 346
    label "zboczenie"
  ]
  node [
    id 347
    label "om&#243;wienie"
  ]
  node [
    id 348
    label "omawia&#263;"
  ]
  node [
    id 349
    label "fraza"
  ]
  node [
    id 350
    label "tre&#347;&#263;"
  ]
  node [
    id 351
    label "entity"
  ]
  node [
    id 352
    label "forum"
  ]
  node [
    id 353
    label "topik"
  ]
  node [
    id 354
    label "tematyka"
  ]
  node [
    id 355
    label "w&#261;tek"
  ]
  node [
    id 356
    label "zbaczanie"
  ]
  node [
    id 357
    label "forma"
  ]
  node [
    id 358
    label "om&#243;wi&#263;"
  ]
  node [
    id 359
    label "omawianie"
  ]
  node [
    id 360
    label "melodia"
  ]
  node [
    id 361
    label "otoczka"
  ]
  node [
    id 362
    label "zbacza&#263;"
  ]
  node [
    id 363
    label "zboczy&#263;"
  ]
  node [
    id 364
    label "niepubliczny"
  ]
  node [
    id 365
    label "czyj&#347;"
  ]
  node [
    id 366
    label "personalny"
  ]
  node [
    id 367
    label "prywatnie"
  ]
  node [
    id 368
    label "nieformalny"
  ]
  node [
    id 369
    label "w&#322;asny"
  ]
  node [
    id 370
    label "samodzielny"
  ]
  node [
    id 371
    label "zwi&#261;zany"
  ]
  node [
    id 372
    label "swoisty"
  ]
  node [
    id 373
    label "osobny"
  ]
  node [
    id 374
    label "nieoficjalny"
  ]
  node [
    id 375
    label "nieformalnie"
  ]
  node [
    id 376
    label "personalnie"
  ]
  node [
    id 377
    label "discover"
  ]
  node [
    id 378
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 379
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 380
    label "wydoby&#263;"
  ]
  node [
    id 381
    label "poda&#263;"
  ]
  node [
    id 382
    label "okre&#347;li&#263;"
  ]
  node [
    id 383
    label "express"
  ]
  node [
    id 384
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 385
    label "wyrazi&#263;"
  ]
  node [
    id 386
    label "rzekn&#261;&#263;"
  ]
  node [
    id 387
    label "unwrap"
  ]
  node [
    id 388
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 389
    label "convey"
  ]
  node [
    id 390
    label "tenis"
  ]
  node [
    id 391
    label "supply"
  ]
  node [
    id 392
    label "da&#263;"
  ]
  node [
    id 393
    label "ustawi&#263;"
  ]
  node [
    id 394
    label "siatk&#243;wka"
  ]
  node [
    id 395
    label "give"
  ]
  node [
    id 396
    label "zagra&#263;"
  ]
  node [
    id 397
    label "jedzenie"
  ]
  node [
    id 398
    label "poinformowa&#263;"
  ]
  node [
    id 399
    label "introduce"
  ]
  node [
    id 400
    label "nafaszerowa&#263;"
  ]
  node [
    id 401
    label "zaserwowa&#263;"
  ]
  node [
    id 402
    label "draw"
  ]
  node [
    id 403
    label "doby&#263;"
  ]
  node [
    id 404
    label "g&#243;rnictwo"
  ]
  node [
    id 405
    label "wyeksploatowa&#263;"
  ]
  node [
    id 406
    label "extract"
  ]
  node [
    id 407
    label "obtain"
  ]
  node [
    id 408
    label "wyj&#261;&#263;"
  ]
  node [
    id 409
    label "ocali&#263;"
  ]
  node [
    id 410
    label "uzyska&#263;"
  ]
  node [
    id 411
    label "wyda&#263;"
  ]
  node [
    id 412
    label "wydosta&#263;"
  ]
  node [
    id 413
    label "uwydatni&#263;"
  ]
  node [
    id 414
    label "distill"
  ]
  node [
    id 415
    label "raise"
  ]
  node [
    id 416
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 417
    label "testify"
  ]
  node [
    id 418
    label "zakomunikowa&#263;"
  ]
  node [
    id 419
    label "oznaczy&#263;"
  ]
  node [
    id 420
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 421
    label "vent"
  ]
  node [
    id 422
    label "zdecydowa&#263;"
  ]
  node [
    id 423
    label "zrobi&#263;"
  ]
  node [
    id 424
    label "spowodowa&#263;"
  ]
  node [
    id 425
    label "situate"
  ]
  node [
    id 426
    label "nominate"
  ]
  node [
    id 427
    label "j&#281;zyki_ba&#322;tyckofi&#324;skie"
  ]
  node [
    id 428
    label "j&#281;zyk_ugrofi&#324;ski"
  ]
  node [
    id 429
    label "po_fi&#324;sku"
  ]
  node [
    id 430
    label "pier&#243;g_karelski"
  ]
  node [
    id 431
    label "skandynawski"
  ]
  node [
    id 432
    label "j&#281;zyk"
  ]
  node [
    id 433
    label "p&#243;&#322;nocnoeuropejski"
  ]
  node [
    id 434
    label "europejski"
  ]
  node [
    id 435
    label "po_skandynawsku"
  ]
  node [
    id 436
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 437
    label "artykulator"
  ]
  node [
    id 438
    label "kod"
  ]
  node [
    id 439
    label "kawa&#322;ek"
  ]
  node [
    id 440
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 441
    label "gramatyka"
  ]
  node [
    id 442
    label "stylik"
  ]
  node [
    id 443
    label "przet&#322;umaczenie"
  ]
  node [
    id 444
    label "formalizowanie"
  ]
  node [
    id 445
    label "ssanie"
  ]
  node [
    id 446
    label "ssa&#263;"
  ]
  node [
    id 447
    label "language"
  ]
  node [
    id 448
    label "liza&#263;"
  ]
  node [
    id 449
    label "napisa&#263;"
  ]
  node [
    id 450
    label "konsonantyzm"
  ]
  node [
    id 451
    label "wokalizm"
  ]
  node [
    id 452
    label "pisa&#263;"
  ]
  node [
    id 453
    label "fonetyka"
  ]
  node [
    id 454
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 455
    label "jeniec"
  ]
  node [
    id 456
    label "but"
  ]
  node [
    id 457
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 458
    label "po_koroniarsku"
  ]
  node [
    id 459
    label "kultura_duchowa"
  ]
  node [
    id 460
    label "t&#322;umaczenie"
  ]
  node [
    id 461
    label "m&#243;wienie"
  ]
  node [
    id 462
    label "pype&#263;"
  ]
  node [
    id 463
    label "lizanie"
  ]
  node [
    id 464
    label "pismo"
  ]
  node [
    id 465
    label "formalizowa&#263;"
  ]
  node [
    id 466
    label "rozumie&#263;"
  ]
  node [
    id 467
    label "organ"
  ]
  node [
    id 468
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 469
    label "rozumienie"
  ]
  node [
    id 470
    label "spos&#243;b"
  ]
  node [
    id 471
    label "makroglosja"
  ]
  node [
    id 472
    label "m&#243;wi&#263;"
  ]
  node [
    id 473
    label "jama_ustna"
  ]
  node [
    id 474
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 475
    label "formacja_geologiczna"
  ]
  node [
    id 476
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 477
    label "natural_language"
  ]
  node [
    id 478
    label "s&#322;ownictwo"
  ]
  node [
    id 479
    label "urz&#261;dzenie"
  ]
  node [
    id 480
    label "dostojnik"
  ]
  node [
    id 481
    label "Goebbels"
  ]
  node [
    id 482
    label "Sto&#322;ypin"
  ]
  node [
    id 483
    label "rz&#261;d"
  ]
  node [
    id 484
    label "przybli&#380;enie"
  ]
  node [
    id 485
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 486
    label "kategoria"
  ]
  node [
    id 487
    label "szpaler"
  ]
  node [
    id 488
    label "lon&#380;a"
  ]
  node [
    id 489
    label "uporz&#261;dkowanie"
  ]
  node [
    id 490
    label "egzekutywa"
  ]
  node [
    id 491
    label "jednostka_systematyczna"
  ]
  node [
    id 492
    label "instytucja"
  ]
  node [
    id 493
    label "premier"
  ]
  node [
    id 494
    label "Londyn"
  ]
  node [
    id 495
    label "gabinet_cieni"
  ]
  node [
    id 496
    label "gromada"
  ]
  node [
    id 497
    label "number"
  ]
  node [
    id 498
    label "Konsulat"
  ]
  node [
    id 499
    label "tract"
  ]
  node [
    id 500
    label "klasa"
  ]
  node [
    id 501
    label "w&#322;adza"
  ]
  node [
    id 502
    label "urz&#281;dnik"
  ]
  node [
    id 503
    label "notabl"
  ]
  node [
    id 504
    label "oficja&#322;"
  ]
  node [
    id 505
    label "wiedza"
  ]
  node [
    id 506
    label "miasteczko_rowerowe"
  ]
  node [
    id 507
    label "porada"
  ]
  node [
    id 508
    label "fotowoltaika"
  ]
  node [
    id 509
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 510
    label "przem&#243;wienie"
  ]
  node [
    id 511
    label "nauki_o_poznaniu"
  ]
  node [
    id 512
    label "nomotetyczny"
  ]
  node [
    id 513
    label "systematyka"
  ]
  node [
    id 514
    label "typologia"
  ]
  node [
    id 515
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 516
    label "&#322;awa_szkolna"
  ]
  node [
    id 517
    label "nauki_penalne"
  ]
  node [
    id 518
    label "dziedzina"
  ]
  node [
    id 519
    label "imagineskopia"
  ]
  node [
    id 520
    label "teoria_naukowa"
  ]
  node [
    id 521
    label "inwentyka"
  ]
  node [
    id 522
    label "metodologia"
  ]
  node [
    id 523
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 524
    label "nauki_o_Ziemi"
  ]
  node [
    id 525
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 526
    label "sfera"
  ]
  node [
    id 527
    label "zakres"
  ]
  node [
    id 528
    label "funkcja"
  ]
  node [
    id 529
    label "bezdro&#380;e"
  ]
  node [
    id 530
    label "poddzia&#322;"
  ]
  node [
    id 531
    label "przebieg"
  ]
  node [
    id 532
    label "legislacyjnie"
  ]
  node [
    id 533
    label "zjawisko"
  ]
  node [
    id 534
    label "nast&#281;pstwo"
  ]
  node [
    id 535
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 536
    label "zrozumienie"
  ]
  node [
    id 537
    label "obronienie"
  ]
  node [
    id 538
    label "wydanie"
  ]
  node [
    id 539
    label "wyg&#322;oszenie"
  ]
  node [
    id 540
    label "wypowied&#378;"
  ]
  node [
    id 541
    label "oddzia&#322;anie"
  ]
  node [
    id 542
    label "address"
  ]
  node [
    id 543
    label "wydobycie"
  ]
  node [
    id 544
    label "wyst&#261;pienie"
  ]
  node [
    id 545
    label "talk"
  ]
  node [
    id 546
    label "odzyskanie"
  ]
  node [
    id 547
    label "sermon"
  ]
  node [
    id 548
    label "cognition"
  ]
  node [
    id 549
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 550
    label "pozwolenie"
  ]
  node [
    id 551
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 552
    label "zaawansowanie"
  ]
  node [
    id 553
    label "wykszta&#322;cenie"
  ]
  node [
    id 554
    label "wskaz&#243;wka"
  ]
  node [
    id 555
    label "technika"
  ]
  node [
    id 556
    label "typology"
  ]
  node [
    id 557
    label "podzia&#322;"
  ]
  node [
    id 558
    label "kwantyfikacja"
  ]
  node [
    id 559
    label "taksonomia"
  ]
  node [
    id 560
    label "biosystematyka"
  ]
  node [
    id 561
    label "biologia"
  ]
  node [
    id 562
    label "kohorta"
  ]
  node [
    id 563
    label "kladystyka"
  ]
  node [
    id 564
    label "aparat_krytyczny"
  ]
  node [
    id 565
    label "funkcjonalizm"
  ]
  node [
    id 566
    label "wyobra&#378;nia"
  ]
  node [
    id 567
    label "charakterystyczny"
  ]
  node [
    id 568
    label "Karta_Nauczyciela"
  ]
  node [
    id 569
    label "program_nauczania"
  ]
  node [
    id 570
    label "gospodarka"
  ]
  node [
    id 571
    label "inwentarz"
  ]
  node [
    id 572
    label "rynek"
  ]
  node [
    id 573
    label "mieszkalnictwo"
  ]
  node [
    id 574
    label "agregat_ekonomiczny"
  ]
  node [
    id 575
    label "miejsce_pracy"
  ]
  node [
    id 576
    label "farmaceutyka"
  ]
  node [
    id 577
    label "produkowanie"
  ]
  node [
    id 578
    label "rolnictwo"
  ]
  node [
    id 579
    label "transport"
  ]
  node [
    id 580
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 581
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 582
    label "obronno&#347;&#263;"
  ]
  node [
    id 583
    label "sektor_prywatny"
  ]
  node [
    id 584
    label "sch&#322;adza&#263;"
  ]
  node [
    id 585
    label "czerwona_strefa"
  ]
  node [
    id 586
    label "struktura"
  ]
  node [
    id 587
    label "pole"
  ]
  node [
    id 588
    label "sektor_publiczny"
  ]
  node [
    id 589
    label "bankowo&#347;&#263;"
  ]
  node [
    id 590
    label "gospodarowanie"
  ]
  node [
    id 591
    label "obora"
  ]
  node [
    id 592
    label "gospodarka_wodna"
  ]
  node [
    id 593
    label "gospodarka_le&#347;na"
  ]
  node [
    id 594
    label "gospodarowa&#263;"
  ]
  node [
    id 595
    label "fabryka"
  ]
  node [
    id 596
    label "wytw&#243;rnia"
  ]
  node [
    id 597
    label "stodo&#322;a"
  ]
  node [
    id 598
    label "przemys&#322;"
  ]
  node [
    id 599
    label "spichlerz"
  ]
  node [
    id 600
    label "sch&#322;adzanie"
  ]
  node [
    id 601
    label "administracja"
  ]
  node [
    id 602
    label "sch&#322;odzenie"
  ]
  node [
    id 603
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 604
    label "zasada"
  ]
  node [
    id 605
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 606
    label "regulacja_cen"
  ]
  node [
    id 607
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 608
    label "Grahn"
  ]
  node [
    id 609
    label "Laasonen"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 608
    target 609
  ]
]
