graph [
  node [
    id 0
    label "policjantka"
    origin "text"
  ]
  node [
    id 1
    label "legnica"
    origin "text"
  ]
  node [
    id 2
    label "dolny"
    origin "text"
  ]
  node [
    id 3
    label "&#347;l&#261;ski"
    origin "text"
  ]
  node [
    id 4
    label "zapa&#322;"
    origin "text"
  ]
  node [
    id 5
    label "&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przest&#281;pca"
    origin "text"
  ]
  node [
    id 7
    label "dostawa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "nawet"
    origin "text"
  ]
  node [
    id 9
    label "nagroda"
    origin "text"
  ]
  node [
    id 10
    label "nisko"
  ]
  node [
    id 11
    label "zminimalizowanie"
  ]
  node [
    id 12
    label "minimalnie"
  ]
  node [
    id 13
    label "graniczny"
  ]
  node [
    id 14
    label "minimalizowanie"
  ]
  node [
    id 15
    label "uni&#380;enie"
  ]
  node [
    id 16
    label "pospolicie"
  ]
  node [
    id 17
    label "blisko"
  ]
  node [
    id 18
    label "wstydliwie"
  ]
  node [
    id 19
    label "ma&#322;o"
  ]
  node [
    id 20
    label "vilely"
  ]
  node [
    id 21
    label "despicably"
  ]
  node [
    id 22
    label "niski"
  ]
  node [
    id 23
    label "po&#347;lednio"
  ]
  node [
    id 24
    label "ma&#322;y"
  ]
  node [
    id 25
    label "zmniejszenie"
  ]
  node [
    id 26
    label "umniejszenie"
  ]
  node [
    id 27
    label "minimalny"
  ]
  node [
    id 28
    label "zmniejszanie"
  ]
  node [
    id 29
    label "umniejszanie"
  ]
  node [
    id 30
    label "granicznie"
  ]
  node [
    id 31
    label "skrajny"
  ]
  node [
    id 32
    label "przyleg&#322;y"
  ]
  node [
    id 33
    label "wa&#380;ny"
  ]
  node [
    id 34
    label "ostateczny"
  ]
  node [
    id 35
    label "cug"
  ]
  node [
    id 36
    label "krepel"
  ]
  node [
    id 37
    label "francuz"
  ]
  node [
    id 38
    label "mietlorz"
  ]
  node [
    id 39
    label "etnolekt"
  ]
  node [
    id 40
    label "sza&#322;ot"
  ]
  node [
    id 41
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 42
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 43
    label "regionalny"
  ]
  node [
    id 44
    label "polski"
  ]
  node [
    id 45
    label "halba"
  ]
  node [
    id 46
    label "ch&#322;opiec"
  ]
  node [
    id 47
    label "buchta"
  ]
  node [
    id 48
    label "czarne_kluski"
  ]
  node [
    id 49
    label "szpajza"
  ]
  node [
    id 50
    label "szl&#261;ski"
  ]
  node [
    id 51
    label "&#347;lonski"
  ]
  node [
    id 52
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 53
    label "waloszek"
  ]
  node [
    id 54
    label "j&#281;zyk_naturalny"
  ]
  node [
    id 55
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 56
    label "nale&#380;ny"
  ]
  node [
    id 57
    label "nale&#380;yty"
  ]
  node [
    id 58
    label "typowy"
  ]
  node [
    id 59
    label "uprawniony"
  ]
  node [
    id 60
    label "zasadniczy"
  ]
  node [
    id 61
    label "stosownie"
  ]
  node [
    id 62
    label "taki"
  ]
  node [
    id 63
    label "charakterystyczny"
  ]
  node [
    id 64
    label "prawdziwy"
  ]
  node [
    id 65
    label "ten"
  ]
  node [
    id 66
    label "dobry"
  ]
  node [
    id 67
    label "tradycyjny"
  ]
  node [
    id 68
    label "regionalnie"
  ]
  node [
    id 69
    label "lokalny"
  ]
  node [
    id 70
    label "przedmiot"
  ]
  node [
    id 71
    label "Polish"
  ]
  node [
    id 72
    label "goniony"
  ]
  node [
    id 73
    label "oberek"
  ]
  node [
    id 74
    label "ryba_po_grecku"
  ]
  node [
    id 75
    label "sztajer"
  ]
  node [
    id 76
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 77
    label "krakowiak"
  ]
  node [
    id 78
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 79
    label "pierogi_ruskie"
  ]
  node [
    id 80
    label "lacki"
  ]
  node [
    id 81
    label "polak"
  ]
  node [
    id 82
    label "chodzony"
  ]
  node [
    id 83
    label "po_polsku"
  ]
  node [
    id 84
    label "mazur"
  ]
  node [
    id 85
    label "polsko"
  ]
  node [
    id 86
    label "skoczny"
  ]
  node [
    id 87
    label "drabant"
  ]
  node [
    id 88
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 89
    label "j&#281;zyk"
  ]
  node [
    id 90
    label "g&#243;rno&#347;l&#261;ski"
  ]
  node [
    id 91
    label "po_&#347;lonsku"
  ]
  node [
    id 92
    label "po_szl&#261;sku"
  ]
  node [
    id 93
    label "taniec_ludowy"
  ]
  node [
    id 94
    label "melodia"
  ]
  node [
    id 95
    label "taniec"
  ]
  node [
    id 96
    label "teren"
  ]
  node [
    id 97
    label "dro&#380;d&#380;owy"
  ]
  node [
    id 98
    label "nadzienie"
  ]
  node [
    id 99
    label "pampuch"
  ]
  node [
    id 100
    label "miejsce"
  ]
  node [
    id 101
    label "zatoka"
  ]
  node [
    id 102
    label "pomieszczenie"
  ]
  node [
    id 103
    label "dro&#380;d&#380;&#243;wka"
  ]
  node [
    id 104
    label "dzik"
  ]
  node [
    id 105
    label "zw&#243;j"
  ]
  node [
    id 106
    label "p&#261;czek"
  ]
  node [
    id 107
    label "bu&#322;ka_paryska"
  ]
  node [
    id 108
    label "klucz_nastawny"
  ]
  node [
    id 109
    label "warkocz"
  ]
  node [
    id 110
    label "francuski"
  ]
  node [
    id 111
    label "kufel"
  ]
  node [
    id 112
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 113
    label "skwarek"
  ]
  node [
    id 114
    label "sa&#322;atka"
  ]
  node [
    id 115
    label "musztarda"
  ]
  node [
    id 116
    label "g&#243;wniarz"
  ]
  node [
    id 117
    label "synek"
  ]
  node [
    id 118
    label "cz&#322;owiek"
  ]
  node [
    id 119
    label "boyfriend"
  ]
  node [
    id 120
    label "okrzos"
  ]
  node [
    id 121
    label "dziecko"
  ]
  node [
    id 122
    label "sympatia"
  ]
  node [
    id 123
    label "usynowienie"
  ]
  node [
    id 124
    label "pomocnik"
  ]
  node [
    id 125
    label "kawaler"
  ]
  node [
    id 126
    label "m&#322;odzieniec"
  ]
  node [
    id 127
    label "kajtek"
  ]
  node [
    id 128
    label "pederasta"
  ]
  node [
    id 129
    label "usynawianie"
  ]
  node [
    id 130
    label "deser"
  ]
  node [
    id 131
    label "para"
  ]
  node [
    id 132
    label "pr&#261;d"
  ]
  node [
    id 133
    label "poci&#261;g"
  ]
  node [
    id 134
    label "draft"
  ]
  node [
    id 135
    label "stan"
  ]
  node [
    id 136
    label "ci&#261;g"
  ]
  node [
    id 137
    label "zaprz&#281;g"
  ]
  node [
    id 138
    label "podekscytowanie"
  ]
  node [
    id 139
    label "power"
  ]
  node [
    id 140
    label "zapalno&#347;&#263;"
  ]
  node [
    id 141
    label "passion"
  ]
  node [
    id 142
    label "agitation"
  ]
  node [
    id 143
    label "podniecenie_si&#281;"
  ]
  node [
    id 144
    label "poruszenie"
  ]
  node [
    id 145
    label "nastr&#243;j"
  ]
  node [
    id 146
    label "excitation"
  ]
  node [
    id 147
    label "energia"
  ]
  node [
    id 148
    label "cecha"
  ]
  node [
    id 149
    label "kara&#263;"
  ]
  node [
    id 150
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 151
    label "straszy&#263;"
  ]
  node [
    id 152
    label "prosecute"
  ]
  node [
    id 153
    label "poszukiwa&#263;"
  ]
  node [
    id 154
    label "usi&#322;owa&#263;"
  ]
  node [
    id 155
    label "stara&#263;_si&#281;"
  ]
  node [
    id 156
    label "szuka&#263;"
  ]
  node [
    id 157
    label "ask"
  ]
  node [
    id 158
    label "look"
  ]
  node [
    id 159
    label "discipline"
  ]
  node [
    id 160
    label "robi&#263;"
  ]
  node [
    id 161
    label "try"
  ]
  node [
    id 162
    label "threaten"
  ]
  node [
    id 163
    label "zapowiada&#263;"
  ]
  node [
    id 164
    label "boast"
  ]
  node [
    id 165
    label "wzbudza&#263;"
  ]
  node [
    id 166
    label "pogwa&#322;ciciel"
  ]
  node [
    id 167
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 168
    label "zgwa&#322;ciciel"
  ]
  node [
    id 169
    label "odst&#281;pca"
  ]
  node [
    id 170
    label "naruszyciel"
  ]
  node [
    id 171
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 172
    label "sprawca"
  ]
  node [
    id 173
    label "mie&#263;_miejsce"
  ]
  node [
    id 174
    label "by&#263;"
  ]
  node [
    id 175
    label "nabywa&#263;"
  ]
  node [
    id 176
    label "uzyskiwa&#263;"
  ]
  node [
    id 177
    label "bra&#263;"
  ]
  node [
    id 178
    label "winnings"
  ]
  node [
    id 179
    label "opanowywa&#263;"
  ]
  node [
    id 180
    label "si&#281;ga&#263;"
  ]
  node [
    id 181
    label "otrzymywa&#263;"
  ]
  node [
    id 182
    label "range"
  ]
  node [
    id 183
    label "wystarcza&#263;"
  ]
  node [
    id 184
    label "kupowa&#263;"
  ]
  node [
    id 185
    label "obskakiwa&#263;"
  ]
  node [
    id 186
    label "return"
  ]
  node [
    id 187
    label "take"
  ]
  node [
    id 188
    label "wytwarza&#263;"
  ]
  node [
    id 189
    label "kupywa&#263;"
  ]
  node [
    id 190
    label "pozyskiwa&#263;"
  ]
  node [
    id 191
    label "przyjmowa&#263;"
  ]
  node [
    id 192
    label "ustawia&#263;"
  ]
  node [
    id 193
    label "get"
  ]
  node [
    id 194
    label "gra&#263;"
  ]
  node [
    id 195
    label "uznawa&#263;"
  ]
  node [
    id 196
    label "wierzy&#263;"
  ]
  node [
    id 197
    label "compass"
  ]
  node [
    id 198
    label "korzysta&#263;"
  ]
  node [
    id 199
    label "appreciation"
  ]
  node [
    id 200
    label "osi&#261;ga&#263;"
  ]
  node [
    id 201
    label "dociera&#263;"
  ]
  node [
    id 202
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 203
    label "mierzy&#263;"
  ]
  node [
    id 204
    label "u&#380;ywa&#263;"
  ]
  node [
    id 205
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 206
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 207
    label "exsert"
  ]
  node [
    id 208
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 209
    label "equal"
  ]
  node [
    id 210
    label "trwa&#263;"
  ]
  node [
    id 211
    label "chodzi&#263;"
  ]
  node [
    id 212
    label "obecno&#347;&#263;"
  ]
  node [
    id 213
    label "stand"
  ]
  node [
    id 214
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 215
    label "uczestniczy&#263;"
  ]
  node [
    id 216
    label "mark"
  ]
  node [
    id 217
    label "powodowa&#263;"
  ]
  node [
    id 218
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 219
    label "porywa&#263;"
  ]
  node [
    id 220
    label "wchodzi&#263;"
  ]
  node [
    id 221
    label "poczytywa&#263;"
  ]
  node [
    id 222
    label "levy"
  ]
  node [
    id 223
    label "wk&#322;ada&#263;"
  ]
  node [
    id 224
    label "raise"
  ]
  node [
    id 225
    label "pokonywa&#263;"
  ]
  node [
    id 226
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 227
    label "rucha&#263;"
  ]
  node [
    id 228
    label "prowadzi&#263;"
  ]
  node [
    id 229
    label "za&#380;ywa&#263;"
  ]
  node [
    id 230
    label "&#263;pa&#263;"
  ]
  node [
    id 231
    label "interpretowa&#263;"
  ]
  node [
    id 232
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 233
    label "rusza&#263;"
  ]
  node [
    id 234
    label "chwyta&#263;"
  ]
  node [
    id 235
    label "grza&#263;"
  ]
  node [
    id 236
    label "wch&#322;ania&#263;"
  ]
  node [
    id 237
    label "wygrywa&#263;"
  ]
  node [
    id 238
    label "ucieka&#263;"
  ]
  node [
    id 239
    label "arise"
  ]
  node [
    id 240
    label "uprawia&#263;_seks"
  ]
  node [
    id 241
    label "abstract"
  ]
  node [
    id 242
    label "towarzystwo"
  ]
  node [
    id 243
    label "atakowa&#263;"
  ]
  node [
    id 244
    label "branie"
  ]
  node [
    id 245
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 246
    label "zalicza&#263;"
  ]
  node [
    id 247
    label "open"
  ]
  node [
    id 248
    label "wzi&#261;&#263;"
  ]
  node [
    id 249
    label "&#322;apa&#263;"
  ]
  node [
    id 250
    label "przewa&#380;a&#263;"
  ]
  node [
    id 251
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 252
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 253
    label "okrada&#263;"
  ]
  node [
    id 254
    label "obiega&#263;"
  ]
  node [
    id 255
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 256
    label "op&#281;dza&#263;"
  ]
  node [
    id 257
    label "osacza&#263;"
  ]
  node [
    id 258
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 259
    label "environment"
  ]
  node [
    id 260
    label "radzi&#263;_sobie"
  ]
  node [
    id 261
    label "manipulate"
  ]
  node [
    id 262
    label "niewoli&#263;"
  ]
  node [
    id 263
    label "capture"
  ]
  node [
    id 264
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 265
    label "powstrzymywa&#263;"
  ]
  node [
    id 266
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 267
    label "meet"
  ]
  node [
    id 268
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 269
    label "rede"
  ]
  node [
    id 270
    label "zaspokaja&#263;"
  ]
  node [
    id 271
    label "suffice"
  ]
  node [
    id 272
    label "stawa&#263;"
  ]
  node [
    id 273
    label "oskar"
  ]
  node [
    id 274
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 275
    label "konsekwencja"
  ]
  node [
    id 276
    label "prize"
  ]
  node [
    id 277
    label "trophy"
  ]
  node [
    id 278
    label "oznaczenie"
  ]
  node [
    id 279
    label "potraktowanie"
  ]
  node [
    id 280
    label "nagrodzenie"
  ]
  node [
    id 281
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 282
    label "zrobienie"
  ]
  node [
    id 283
    label "odczuwa&#263;"
  ]
  node [
    id 284
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 285
    label "skrupienie_si&#281;"
  ]
  node [
    id 286
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 287
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 288
    label "odczucie"
  ]
  node [
    id 289
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 290
    label "koszula_Dejaniry"
  ]
  node [
    id 291
    label "odczuwanie"
  ]
  node [
    id 292
    label "event"
  ]
  node [
    id 293
    label "rezultat"
  ]
  node [
    id 294
    label "skrupianie_si&#281;"
  ]
  node [
    id 295
    label "odczu&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
]
