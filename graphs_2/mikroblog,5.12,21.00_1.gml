graph [
  node [
    id 0
    label "takaprawda"
    origin "text"
  ]
  node [
    id 1
    label "heheszki"
    origin "text"
  ]
  node [
    id 2
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "edukacja"
    origin "text"
  ]
  node [
    id 5
    label "kurde"
    origin "text"
  ]
  node [
    id 6
    label "znowu"
    origin "text"
  ]
  node [
    id 7
    label "k&#380;yrzakuw"
    origin "text"
  ]
  node [
    id 8
    label "prukwa"
    origin "text"
  ]
  node [
    id 9
    label "&#322;ikend"
    origin "text"
  ]
  node [
    id 10
    label "zada&#263;"
    origin "text"
  ]
  node [
    id 11
    label "przedmiot"
  ]
  node [
    id 12
    label "Polish"
  ]
  node [
    id 13
    label "goniony"
  ]
  node [
    id 14
    label "oberek"
  ]
  node [
    id 15
    label "ryba_po_grecku"
  ]
  node [
    id 16
    label "sztajer"
  ]
  node [
    id 17
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 18
    label "krakowiak"
  ]
  node [
    id 19
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 20
    label "pierogi_ruskie"
  ]
  node [
    id 21
    label "lacki"
  ]
  node [
    id 22
    label "polak"
  ]
  node [
    id 23
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 24
    label "chodzony"
  ]
  node [
    id 25
    label "po_polsku"
  ]
  node [
    id 26
    label "mazur"
  ]
  node [
    id 27
    label "polsko"
  ]
  node [
    id 28
    label "skoczny"
  ]
  node [
    id 29
    label "drabant"
  ]
  node [
    id 30
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 31
    label "j&#281;zyk"
  ]
  node [
    id 32
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 33
    label "artykulator"
  ]
  node [
    id 34
    label "kod"
  ]
  node [
    id 35
    label "kawa&#322;ek"
  ]
  node [
    id 36
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 37
    label "gramatyka"
  ]
  node [
    id 38
    label "stylik"
  ]
  node [
    id 39
    label "przet&#322;umaczenie"
  ]
  node [
    id 40
    label "formalizowanie"
  ]
  node [
    id 41
    label "ssanie"
  ]
  node [
    id 42
    label "ssa&#263;"
  ]
  node [
    id 43
    label "language"
  ]
  node [
    id 44
    label "liza&#263;"
  ]
  node [
    id 45
    label "napisa&#263;"
  ]
  node [
    id 46
    label "konsonantyzm"
  ]
  node [
    id 47
    label "wokalizm"
  ]
  node [
    id 48
    label "pisa&#263;"
  ]
  node [
    id 49
    label "fonetyka"
  ]
  node [
    id 50
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 51
    label "jeniec"
  ]
  node [
    id 52
    label "but"
  ]
  node [
    id 53
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 54
    label "po_koroniarsku"
  ]
  node [
    id 55
    label "kultura_duchowa"
  ]
  node [
    id 56
    label "t&#322;umaczenie"
  ]
  node [
    id 57
    label "m&#243;wienie"
  ]
  node [
    id 58
    label "pype&#263;"
  ]
  node [
    id 59
    label "lizanie"
  ]
  node [
    id 60
    label "pismo"
  ]
  node [
    id 61
    label "formalizowa&#263;"
  ]
  node [
    id 62
    label "rozumie&#263;"
  ]
  node [
    id 63
    label "organ"
  ]
  node [
    id 64
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 65
    label "rozumienie"
  ]
  node [
    id 66
    label "spos&#243;b"
  ]
  node [
    id 67
    label "makroglosja"
  ]
  node [
    id 68
    label "m&#243;wi&#263;"
  ]
  node [
    id 69
    label "jama_ustna"
  ]
  node [
    id 70
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 71
    label "formacja_geologiczna"
  ]
  node [
    id 72
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 73
    label "natural_language"
  ]
  node [
    id 74
    label "s&#322;ownictwo"
  ]
  node [
    id 75
    label "urz&#261;dzenie"
  ]
  node [
    id 76
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 77
    label "wschodnioeuropejski"
  ]
  node [
    id 78
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 79
    label "poga&#324;ski"
  ]
  node [
    id 80
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 81
    label "topielec"
  ]
  node [
    id 82
    label "europejski"
  ]
  node [
    id 83
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 84
    label "langosz"
  ]
  node [
    id 85
    label "zboczenie"
  ]
  node [
    id 86
    label "om&#243;wienie"
  ]
  node [
    id 87
    label "sponiewieranie"
  ]
  node [
    id 88
    label "discipline"
  ]
  node [
    id 89
    label "rzecz"
  ]
  node [
    id 90
    label "omawia&#263;"
  ]
  node [
    id 91
    label "kr&#261;&#380;enie"
  ]
  node [
    id 92
    label "tre&#347;&#263;"
  ]
  node [
    id 93
    label "robienie"
  ]
  node [
    id 94
    label "sponiewiera&#263;"
  ]
  node [
    id 95
    label "element"
  ]
  node [
    id 96
    label "entity"
  ]
  node [
    id 97
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 98
    label "tematyka"
  ]
  node [
    id 99
    label "w&#261;tek"
  ]
  node [
    id 100
    label "charakter"
  ]
  node [
    id 101
    label "zbaczanie"
  ]
  node [
    id 102
    label "program_nauczania"
  ]
  node [
    id 103
    label "om&#243;wi&#263;"
  ]
  node [
    id 104
    label "omawianie"
  ]
  node [
    id 105
    label "thing"
  ]
  node [
    id 106
    label "kultura"
  ]
  node [
    id 107
    label "istota"
  ]
  node [
    id 108
    label "zbacza&#263;"
  ]
  node [
    id 109
    label "zboczy&#263;"
  ]
  node [
    id 110
    label "gwardzista"
  ]
  node [
    id 111
    label "melodia"
  ]
  node [
    id 112
    label "taniec"
  ]
  node [
    id 113
    label "taniec_ludowy"
  ]
  node [
    id 114
    label "&#347;redniowieczny"
  ]
  node [
    id 115
    label "specjalny"
  ]
  node [
    id 116
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 117
    label "weso&#322;y"
  ]
  node [
    id 118
    label "sprawny"
  ]
  node [
    id 119
    label "rytmiczny"
  ]
  node [
    id 120
    label "skocznie"
  ]
  node [
    id 121
    label "energiczny"
  ]
  node [
    id 122
    label "lendler"
  ]
  node [
    id 123
    label "austriacki"
  ]
  node [
    id 124
    label "polka"
  ]
  node [
    id 125
    label "europejsko"
  ]
  node [
    id 126
    label "przytup"
  ]
  node [
    id 127
    label "ho&#322;ubiec"
  ]
  node [
    id 128
    label "wodzi&#263;"
  ]
  node [
    id 129
    label "ludowy"
  ]
  node [
    id 130
    label "pie&#347;&#324;"
  ]
  node [
    id 131
    label "mieszkaniec"
  ]
  node [
    id 132
    label "centu&#347;"
  ]
  node [
    id 133
    label "lalka"
  ]
  node [
    id 134
    label "Ma&#322;opolanin"
  ]
  node [
    id 135
    label "krakauer"
  ]
  node [
    id 136
    label "formation"
  ]
  node [
    id 137
    label "Karta_Nauczyciela"
  ]
  node [
    id 138
    label "wiedza"
  ]
  node [
    id 139
    label "heureza"
  ]
  node [
    id 140
    label "proces"
  ]
  node [
    id 141
    label "miasteczko_rowerowe"
  ]
  node [
    id 142
    label "urszulanki"
  ]
  node [
    id 143
    label "niepokalanki"
  ]
  node [
    id 144
    label "kwalifikacje"
  ]
  node [
    id 145
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 146
    label "&#322;awa_szkolna"
  ]
  node [
    id 147
    label "nauka"
  ]
  node [
    id 148
    label "skolaryzacja"
  ]
  node [
    id 149
    label "szkolnictwo"
  ]
  node [
    id 150
    label "form"
  ]
  node [
    id 151
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 152
    label "gospodarka"
  ]
  node [
    id 153
    label "porada"
  ]
  node [
    id 154
    label "fotowoltaika"
  ]
  node [
    id 155
    label "przem&#243;wienie"
  ]
  node [
    id 156
    label "nauki_o_poznaniu"
  ]
  node [
    id 157
    label "nomotetyczny"
  ]
  node [
    id 158
    label "systematyka"
  ]
  node [
    id 159
    label "typologia"
  ]
  node [
    id 160
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 161
    label "nauki_penalne"
  ]
  node [
    id 162
    label "dziedzina"
  ]
  node [
    id 163
    label "imagineskopia"
  ]
  node [
    id 164
    label "teoria_naukowa"
  ]
  node [
    id 165
    label "inwentyka"
  ]
  node [
    id 166
    label "metodologia"
  ]
  node [
    id 167
    label "nauki_o_Ziemi"
  ]
  node [
    id 168
    label "kognicja"
  ]
  node [
    id 169
    label "przebieg"
  ]
  node [
    id 170
    label "rozprawa"
  ]
  node [
    id 171
    label "wydarzenie"
  ]
  node [
    id 172
    label "legislacyjnie"
  ]
  node [
    id 173
    label "przes&#322;anka"
  ]
  node [
    id 174
    label "zjawisko"
  ]
  node [
    id 175
    label "nast&#281;pstwo"
  ]
  node [
    id 176
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 177
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 178
    label "eliminacje"
  ]
  node [
    id 179
    label "metoda"
  ]
  node [
    id 180
    label "nauczanie"
  ]
  node [
    id 181
    label "inwentarz"
  ]
  node [
    id 182
    label "rynek"
  ]
  node [
    id 183
    label "mieszkalnictwo"
  ]
  node [
    id 184
    label "agregat_ekonomiczny"
  ]
  node [
    id 185
    label "miejsce_pracy"
  ]
  node [
    id 186
    label "produkowanie"
  ]
  node [
    id 187
    label "farmaceutyka"
  ]
  node [
    id 188
    label "rolnictwo"
  ]
  node [
    id 189
    label "transport"
  ]
  node [
    id 190
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 191
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 192
    label "obronno&#347;&#263;"
  ]
  node [
    id 193
    label "sektor_prywatny"
  ]
  node [
    id 194
    label "sch&#322;adza&#263;"
  ]
  node [
    id 195
    label "czerwona_strefa"
  ]
  node [
    id 196
    label "struktura"
  ]
  node [
    id 197
    label "pole"
  ]
  node [
    id 198
    label "sektor_publiczny"
  ]
  node [
    id 199
    label "bankowo&#347;&#263;"
  ]
  node [
    id 200
    label "gospodarowanie"
  ]
  node [
    id 201
    label "obora"
  ]
  node [
    id 202
    label "gospodarka_wodna"
  ]
  node [
    id 203
    label "gospodarka_le&#347;na"
  ]
  node [
    id 204
    label "gospodarowa&#263;"
  ]
  node [
    id 205
    label "fabryka"
  ]
  node [
    id 206
    label "wytw&#243;rnia"
  ]
  node [
    id 207
    label "stodo&#322;a"
  ]
  node [
    id 208
    label "przemys&#322;"
  ]
  node [
    id 209
    label "spichlerz"
  ]
  node [
    id 210
    label "sch&#322;adzanie"
  ]
  node [
    id 211
    label "administracja"
  ]
  node [
    id 212
    label "sch&#322;odzenie"
  ]
  node [
    id 213
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 214
    label "zasada"
  ]
  node [
    id 215
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 216
    label "regulacja_cen"
  ]
  node [
    id 217
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 218
    label "proporcja"
  ]
  node [
    id 219
    label "wykszta&#322;cenie"
  ]
  node [
    id 220
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 221
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 222
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 223
    label "urszulanki_szare"
  ]
  node [
    id 224
    label "cognition"
  ]
  node [
    id 225
    label "intelekt"
  ]
  node [
    id 226
    label "pozwolenie"
  ]
  node [
    id 227
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 228
    label "zaawansowanie"
  ]
  node [
    id 229
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 230
    label "kurwa"
  ]
  node [
    id 231
    label "skurwienie_si&#281;"
  ]
  node [
    id 232
    label "zo&#322;za"
  ]
  node [
    id 233
    label "karze&#322;"
  ]
  node [
    id 234
    label "wyzwisko"
  ]
  node [
    id 235
    label "prostytutka"
  ]
  node [
    id 236
    label "przekle&#324;stwo"
  ]
  node [
    id 237
    label "szmaciarz"
  ]
  node [
    id 238
    label "kurcz&#281;"
  ]
  node [
    id 239
    label "kurwienie_si&#281;"
  ]
  node [
    id 240
    label "babsztyl"
  ]
  node [
    id 241
    label "baba"
  ]
  node [
    id 242
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 243
    label "zaszkodzi&#263;"
  ]
  node [
    id 244
    label "put"
  ]
  node [
    id 245
    label "deal"
  ]
  node [
    id 246
    label "set"
  ]
  node [
    id 247
    label "zaj&#261;&#263;"
  ]
  node [
    id 248
    label "distribute"
  ]
  node [
    id 249
    label "nakarmi&#263;"
  ]
  node [
    id 250
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 251
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 252
    label "obowi&#261;za&#263;"
  ]
  node [
    id 253
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 254
    label "perpetrate"
  ]
  node [
    id 255
    label "hurt"
  ]
  node [
    id 256
    label "spowodowa&#263;"
  ]
  node [
    id 257
    label "injury"
  ]
  node [
    id 258
    label "zrobi&#263;"
  ]
  node [
    id 259
    label "wyr&#281;czy&#263;"
  ]
  node [
    id 260
    label "da&#263;"
  ]
  node [
    id 261
    label "feed"
  ]
  node [
    id 262
    label "poda&#263;"
  ]
  node [
    id 263
    label "utrzyma&#263;"
  ]
  node [
    id 264
    label "zapoda&#263;"
  ]
  node [
    id 265
    label "wm&#243;wi&#263;"
  ]
  node [
    id 266
    label "invest"
  ]
  node [
    id 267
    label "plant"
  ]
  node [
    id 268
    label "load"
  ]
  node [
    id 269
    label "ubra&#263;"
  ]
  node [
    id 270
    label "oblec_si&#281;"
  ]
  node [
    id 271
    label "oblec"
  ]
  node [
    id 272
    label "str&#243;j"
  ]
  node [
    id 273
    label "pokry&#263;"
  ]
  node [
    id 274
    label "podwin&#261;&#263;"
  ]
  node [
    id 275
    label "przewidzie&#263;"
  ]
  node [
    id 276
    label "przyodzia&#263;"
  ]
  node [
    id 277
    label "jell"
  ]
  node [
    id 278
    label "umie&#347;ci&#263;"
  ]
  node [
    id 279
    label "insert"
  ]
  node [
    id 280
    label "utworzy&#263;"
  ]
  node [
    id 281
    label "zap&#322;aci&#263;"
  ]
  node [
    id 282
    label "create"
  ]
  node [
    id 283
    label "install"
  ]
  node [
    id 284
    label "map"
  ]
  node [
    id 285
    label "zapanowa&#263;"
  ]
  node [
    id 286
    label "rozciekawi&#263;"
  ]
  node [
    id 287
    label "skorzysta&#263;"
  ]
  node [
    id 288
    label "komornik"
  ]
  node [
    id 289
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 290
    label "klasyfikacja"
  ]
  node [
    id 291
    label "wype&#322;ni&#263;"
  ]
  node [
    id 292
    label "topographic_point"
  ]
  node [
    id 293
    label "obj&#261;&#263;"
  ]
  node [
    id 294
    label "seize"
  ]
  node [
    id 295
    label "interest"
  ]
  node [
    id 296
    label "anektowa&#263;"
  ]
  node [
    id 297
    label "employment"
  ]
  node [
    id 298
    label "prosecute"
  ]
  node [
    id 299
    label "dostarczy&#263;"
  ]
  node [
    id 300
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 301
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 302
    label "bankrupt"
  ]
  node [
    id 303
    label "sorb"
  ]
  node [
    id 304
    label "zabra&#263;"
  ]
  node [
    id 305
    label "wzi&#261;&#263;"
  ]
  node [
    id 306
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 307
    label "do"
  ]
  node [
    id 308
    label "wzbudzi&#263;"
  ]
  node [
    id 309
    label "ulepszy&#263;"
  ]
  node [
    id 310
    label "znie&#347;&#263;"
  ]
  node [
    id 311
    label "heft"
  ]
  node [
    id 312
    label "podnie&#347;&#263;"
  ]
  node [
    id 313
    label "arise"
  ]
  node [
    id 314
    label "odbudowa&#263;"
  ]
  node [
    id 315
    label "ud&#378;wign&#261;&#263;"
  ]
  node [
    id 316
    label "raise"
  ]
  node [
    id 317
    label "cover"
  ]
  node [
    id 318
    label "gem"
  ]
  node [
    id 319
    label "kompozycja"
  ]
  node [
    id 320
    label "runda"
  ]
  node [
    id 321
    label "muzyka"
  ]
  node [
    id 322
    label "zestaw"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
]
