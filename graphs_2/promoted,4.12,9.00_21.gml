graph [
  node [
    id 0
    label "kierowca"
    origin "text"
  ]
  node [
    id 1
    label "suvem"
    origin "text"
  ]
  node [
    id 2
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 3
    label "transportowiec"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "ludzko&#347;&#263;"
  ]
  node [
    id 6
    label "asymilowanie"
  ]
  node [
    id 7
    label "wapniak"
  ]
  node [
    id 8
    label "asymilowa&#263;"
  ]
  node [
    id 9
    label "os&#322;abia&#263;"
  ]
  node [
    id 10
    label "posta&#263;"
  ]
  node [
    id 11
    label "hominid"
  ]
  node [
    id 12
    label "podw&#322;adny"
  ]
  node [
    id 13
    label "os&#322;abianie"
  ]
  node [
    id 14
    label "g&#322;owa"
  ]
  node [
    id 15
    label "figura"
  ]
  node [
    id 16
    label "portrecista"
  ]
  node [
    id 17
    label "dwun&#243;g"
  ]
  node [
    id 18
    label "profanum"
  ]
  node [
    id 19
    label "mikrokosmos"
  ]
  node [
    id 20
    label "nasada"
  ]
  node [
    id 21
    label "duch"
  ]
  node [
    id 22
    label "antropochoria"
  ]
  node [
    id 23
    label "osoba"
  ]
  node [
    id 24
    label "wz&#243;r"
  ]
  node [
    id 25
    label "senior"
  ]
  node [
    id 26
    label "oddzia&#322;ywanie"
  ]
  node [
    id 27
    label "Adam"
  ]
  node [
    id 28
    label "homo_sapiens"
  ]
  node [
    id 29
    label "polifag"
  ]
  node [
    id 30
    label "pracownik"
  ]
  node [
    id 31
    label "statek_handlowy"
  ]
  node [
    id 32
    label "okr&#281;t_nawodny"
  ]
  node [
    id 33
    label "bran&#380;owiec"
  ]
  node [
    id 34
    label "korzysta&#263;"
  ]
  node [
    id 35
    label "czu&#263;"
  ]
  node [
    id 36
    label "wykonywa&#263;"
  ]
  node [
    id 37
    label "ride"
  ]
  node [
    id 38
    label "proceed"
  ]
  node [
    id 39
    label "odbywa&#263;"
  ]
  node [
    id 40
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 41
    label "carry"
  ]
  node [
    id 42
    label "go"
  ]
  node [
    id 43
    label "prowadzi&#263;"
  ]
  node [
    id 44
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 45
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 46
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 47
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 48
    label "overdrive"
  ]
  node [
    id 49
    label "kontynuowa&#263;"
  ]
  node [
    id 50
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 51
    label "napada&#263;"
  ]
  node [
    id 52
    label "drive"
  ]
  node [
    id 53
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 54
    label "continue"
  ]
  node [
    id 55
    label "wali&#263;"
  ]
  node [
    id 56
    label "jeba&#263;"
  ]
  node [
    id 57
    label "pachnie&#263;"
  ]
  node [
    id 58
    label "talerz_perkusyjny"
  ]
  node [
    id 59
    label "cover"
  ]
  node [
    id 60
    label "goban"
  ]
  node [
    id 61
    label "gra_planszowa"
  ]
  node [
    id 62
    label "sport_umys&#322;owy"
  ]
  node [
    id 63
    label "chi&#324;ski"
  ]
  node [
    id 64
    label "prosecute"
  ]
  node [
    id 65
    label "robi&#263;"
  ]
  node [
    id 66
    label "hold"
  ]
  node [
    id 67
    label "przechodzi&#263;"
  ]
  node [
    id 68
    label "uczestniczy&#263;"
  ]
  node [
    id 69
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "wytwarza&#263;"
  ]
  node [
    id 71
    label "muzyka"
  ]
  node [
    id 72
    label "work"
  ]
  node [
    id 73
    label "create"
  ]
  node [
    id 74
    label "praca"
  ]
  node [
    id 75
    label "rola"
  ]
  node [
    id 76
    label "&#380;y&#263;"
  ]
  node [
    id 77
    label "kierowa&#263;"
  ]
  node [
    id 78
    label "g&#243;rowa&#263;"
  ]
  node [
    id 79
    label "tworzy&#263;"
  ]
  node [
    id 80
    label "krzywa"
  ]
  node [
    id 81
    label "linia_melodyczna"
  ]
  node [
    id 82
    label "control"
  ]
  node [
    id 83
    label "string"
  ]
  node [
    id 84
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 85
    label "ukierunkowywa&#263;"
  ]
  node [
    id 86
    label "sterowa&#263;"
  ]
  node [
    id 87
    label "kre&#347;li&#263;"
  ]
  node [
    id 88
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 89
    label "message"
  ]
  node [
    id 90
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 91
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 92
    label "eksponowa&#263;"
  ]
  node [
    id 93
    label "navigate"
  ]
  node [
    id 94
    label "manipulate"
  ]
  node [
    id 95
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 96
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 97
    label "przesuwa&#263;"
  ]
  node [
    id 98
    label "partner"
  ]
  node [
    id 99
    label "prowadzenie"
  ]
  node [
    id 100
    label "powodowa&#263;"
  ]
  node [
    id 101
    label "traktowa&#263;"
  ]
  node [
    id 102
    label "jeer"
  ]
  node [
    id 103
    label "attack"
  ]
  node [
    id 104
    label "piratowa&#263;"
  ]
  node [
    id 105
    label "atakowa&#263;"
  ]
  node [
    id 106
    label "m&#243;wi&#263;"
  ]
  node [
    id 107
    label "krytykowa&#263;"
  ]
  node [
    id 108
    label "dopada&#263;"
  ]
  node [
    id 109
    label "u&#380;ywa&#263;"
  ]
  node [
    id 110
    label "use"
  ]
  node [
    id 111
    label "uzyskiwa&#263;"
  ]
  node [
    id 112
    label "postrzega&#263;"
  ]
  node [
    id 113
    label "przewidywa&#263;"
  ]
  node [
    id 114
    label "by&#263;"
  ]
  node [
    id 115
    label "smell"
  ]
  node [
    id 116
    label "uczuwa&#263;"
  ]
  node [
    id 117
    label "spirit"
  ]
  node [
    id 118
    label "doznawa&#263;"
  ]
  node [
    id 119
    label "anticipate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
]
