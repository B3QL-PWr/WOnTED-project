graph [
  node [
    id 0
    label "koran"
    origin "text"
  ]
  node [
    id 1
    label "wzajemny"
    origin "text"
  ]
  node [
    id 2
    label "oszukiwa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wzajemnie"
  ]
  node [
    id 4
    label "zobop&#243;lny"
  ]
  node [
    id 5
    label "wsp&#243;lny"
  ]
  node [
    id 6
    label "zajemny"
  ]
  node [
    id 7
    label "spolny"
  ]
  node [
    id 8
    label "wsp&#243;lnie"
  ]
  node [
    id 9
    label "sp&#243;lny"
  ]
  node [
    id 10
    label "jeden"
  ]
  node [
    id 11
    label "uwsp&#243;lnienie"
  ]
  node [
    id 12
    label "uwsp&#243;lnianie"
  ]
  node [
    id 13
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 14
    label "orzyna&#263;"
  ]
  node [
    id 15
    label "oszwabia&#263;"
  ]
  node [
    id 16
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 17
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 18
    label "cheat"
  ]
  node [
    id 19
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 20
    label "trenowa&#263;"
  ]
  node [
    id 21
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 22
    label "opieprza&#263;"
  ]
  node [
    id 23
    label "zje&#380;d&#380;a&#263;"
  ]
  node [
    id 24
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 25
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 26
    label "osacza&#263;"
  ]
  node [
    id 27
    label "wyprzedza&#263;"
  ]
  node [
    id 28
    label "omija&#263;"
  ]
  node [
    id 29
    label "evade"
  ]
  node [
    id 30
    label "przesuwa&#263;"
  ]
  node [
    id 31
    label "zapewnia&#263;"
  ]
  node [
    id 32
    label "wywodzi&#263;"
  ]
  node [
    id 33
    label "przeje&#380;d&#380;a&#263;"
  ]
  node [
    id 34
    label "ogrywa&#263;"
  ]
  node [
    id 35
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 36
    label "za&#322;atwia&#263;"
  ]
  node [
    id 37
    label "umieszcza&#263;"
  ]
  node [
    id 38
    label "wk&#322;ada&#263;"
  ]
  node [
    id 39
    label "Koran"
  ]
  node [
    id 40
    label "Surah"
  ]
  node [
    id 41
    label "at"
  ]
  node [
    id 42
    label "Tagh&#226;bun"
  ]
  node [
    id 43
    label "zaw&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 41
    target 42
  ]
]
