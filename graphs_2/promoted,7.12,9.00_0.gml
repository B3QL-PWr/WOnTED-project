graph [
  node [
    id 0
    label "zwolni&#263;"
    origin "text"
  ]
  node [
    id 1
    label "uzycie"
    origin "text"
  ]
  node [
    id 2
    label "hashtaga"
    origin "text"
  ]
  node [
    id 3
    label "wontbeerased"
    origin "text"
  ]
  node [
    id 4
    label "sprawi&#263;"
  ]
  node [
    id 5
    label "uprzedzi&#263;"
  ]
  node [
    id 6
    label "sta&#263;_si&#281;"
  ]
  node [
    id 7
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 8
    label "wyla&#263;"
  ]
  node [
    id 9
    label "oddali&#263;"
  ]
  node [
    id 10
    label "deliver"
  ]
  node [
    id 11
    label "spowodowa&#263;"
  ]
  node [
    id 12
    label "wypowiedzie&#263;"
  ]
  node [
    id 13
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 14
    label "usprawiedliwi&#263;"
  ]
  node [
    id 15
    label "render"
  ]
  node [
    id 16
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 17
    label "advise"
  ]
  node [
    id 18
    label "poluzowa&#263;"
  ]
  node [
    id 19
    label "rynek"
  ]
  node [
    id 20
    label "publish"
  ]
  node [
    id 21
    label "wykszta&#322;ci&#263;"
  ]
  node [
    id 22
    label "picture"
  ]
  node [
    id 23
    label "zrobi&#263;"
  ]
  node [
    id 24
    label "pozwoli&#263;"
  ]
  node [
    id 25
    label "pu&#347;ci&#263;"
  ]
  node [
    id 26
    label "leave"
  ]
  node [
    id 27
    label "release"
  ]
  node [
    id 28
    label "wyda&#263;"
  ]
  node [
    id 29
    label "zej&#347;&#263;"
  ]
  node [
    id 30
    label "issue"
  ]
  node [
    id 31
    label "przesta&#263;"
  ]
  node [
    id 32
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 33
    label "act"
  ]
  node [
    id 34
    label "wyrobi&#263;"
  ]
  node [
    id 35
    label "wzi&#261;&#263;"
  ]
  node [
    id 36
    label "catch"
  ]
  node [
    id 37
    label "frame"
  ]
  node [
    id 38
    label "przygotowa&#263;"
  ]
  node [
    id 39
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 40
    label "wydoby&#263;"
  ]
  node [
    id 41
    label "express"
  ]
  node [
    id 42
    label "rozwi&#261;za&#263;"
  ]
  node [
    id 43
    label "zwerbalizowa&#263;"
  ]
  node [
    id 44
    label "denounce"
  ]
  node [
    id 45
    label "order"
  ]
  node [
    id 46
    label "work"
  ]
  node [
    id 47
    label "chemia"
  ]
  node [
    id 48
    label "reakcja_chemiczna"
  ]
  node [
    id 49
    label "potwierdzi&#263;"
  ]
  node [
    id 50
    label "uzasadni&#263;"
  ]
  node [
    id 51
    label "obroni&#263;"
  ]
  node [
    id 52
    label "clear"
  ]
  node [
    id 53
    label "poinformowa&#263;"
  ]
  node [
    id 54
    label "og&#322;osi&#263;"
  ]
  node [
    id 55
    label "overwhelm"
  ]
  node [
    id 56
    label "announce"
  ]
  node [
    id 57
    label "begin"
  ]
  node [
    id 58
    label "nakaza&#263;"
  ]
  node [
    id 59
    label "withdraw"
  ]
  node [
    id 60
    label "suspend"
  ]
  node [
    id 61
    label "oddalenie"
  ]
  node [
    id 62
    label "bow_out"
  ]
  node [
    id 63
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 64
    label "decelerate"
  ]
  node [
    id 65
    label "odrzuci&#263;"
  ]
  node [
    id 66
    label "pokaza&#263;"
  ]
  node [
    id 67
    label "odprawi&#263;"
  ]
  node [
    id 68
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 69
    label "oddalanie"
  ]
  node [
    id 70
    label "oddala&#263;"
  ]
  node [
    id 71
    label "od&#322;o&#380;y&#263;"
  ]
  node [
    id 72
    label "remove"
  ]
  node [
    id 73
    label "opr&#243;&#380;ni&#263;"
  ]
  node [
    id 74
    label "spill"
  ]
  node [
    id 75
    label "flood"
  ]
  node [
    id 76
    label "wyrzuci&#263;"
  ]
  node [
    id 77
    label "flow_out"
  ]
  node [
    id 78
    label "pokry&#263;"
  ]
  node [
    id 79
    label "wyrazi&#263;"
  ]
  node [
    id 80
    label "wydosta&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
