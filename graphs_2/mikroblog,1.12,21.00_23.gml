graph [
  node [
    id 0
    label "kupi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ostatnio"
    origin "text"
  ]
  node [
    id 2
    label "skarpetka"
    origin "text"
  ]
  node [
    id 3
    label "zdj"
    origin "text"
  ]
  node [
    id 4
    label "heheszki"
    origin "text"
  ]
  node [
    id 5
    label "aktualnie"
  ]
  node [
    id 6
    label "poprzednio"
  ]
  node [
    id 7
    label "ostatni"
  ]
  node [
    id 8
    label "ninie"
  ]
  node [
    id 9
    label "aktualny"
  ]
  node [
    id 10
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 11
    label "poprzedni"
  ]
  node [
    id 12
    label "wcze&#347;niej"
  ]
  node [
    id 13
    label "kolejny"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "niedawno"
  ]
  node [
    id 16
    label "pozosta&#322;y"
  ]
  node [
    id 17
    label "sko&#324;czony"
  ]
  node [
    id 18
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 19
    label "najgorszy"
  ]
  node [
    id 20
    label "istota_&#380;ywa"
  ]
  node [
    id 21
    label "w&#261;tpliwy"
  ]
  node [
    id 22
    label "dodatek"
  ]
  node [
    id 23
    label "&#322;atka"
  ]
  node [
    id 24
    label "dzianina"
  ]
  node [
    id 25
    label "w&#322;&#243;czka"
  ]
  node [
    id 26
    label "materia&#322;"
  ]
  node [
    id 27
    label "przedmiot"
  ]
  node [
    id 28
    label "odzie&#380;"
  ]
  node [
    id 29
    label "element"
  ]
  node [
    id 30
    label "dochodzenie"
  ]
  node [
    id 31
    label "doj&#347;cie"
  ]
  node [
    id 32
    label "doch&#243;d"
  ]
  node [
    id 33
    label "dziennik"
  ]
  node [
    id 34
    label "rzecz"
  ]
  node [
    id 35
    label "galanteria"
  ]
  node [
    id 36
    label "doj&#347;&#263;"
  ]
  node [
    id 37
    label "aneks"
  ]
  node [
    id 38
    label "vamp"
  ]
  node [
    id 39
    label "aktualizacja"
  ]
  node [
    id 40
    label "ma&#347;&#263;"
  ]
  node [
    id 41
    label "uprzedzenie"
  ]
  node [
    id 42
    label "opinia"
  ]
  node [
    id 43
    label "program"
  ]
  node [
    id 44
    label "ocena"
  ]
  node [
    id 45
    label "H"
  ]
  node [
    id 46
    label "ampM"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 45
    target 46
  ]
]
