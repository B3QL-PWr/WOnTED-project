graph [
  node [
    id 0
    label "epidemia"
    origin "text"
  ]
  node [
    id 1
    label "kr&#243;lik"
    origin "text"
  ]
  node [
    id 2
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "bardzo"
    origin "text"
  ]
  node [
    id 5
    label "wcze&#347;nie"
    origin "text"
  ]
  node [
    id 6
    label "atakowanie"
  ]
  node [
    id 7
    label "plaga"
  ]
  node [
    id 8
    label "atakowa&#263;"
  ]
  node [
    id 9
    label "trouble"
  ]
  node [
    id 10
    label "wydarzenie"
  ]
  node [
    id 11
    label "grasowanie"
  ]
  node [
    id 12
    label "schorzenie"
  ]
  node [
    id 13
    label "napadanie"
  ]
  node [
    id 14
    label "rozgrywanie"
  ]
  node [
    id 15
    label "przewaga"
  ]
  node [
    id 16
    label "robienie"
  ]
  node [
    id 17
    label "k&#322;&#243;cenie_si&#281;"
  ]
  node [
    id 18
    label "polowanie"
  ]
  node [
    id 19
    label "walczenie"
  ]
  node [
    id 20
    label "usi&#322;owanie"
  ]
  node [
    id 21
    label "sport"
  ]
  node [
    id 22
    label "m&#243;wienie"
  ]
  node [
    id 23
    label "wyskakiwanie_z_g&#281;b&#261;"
  ]
  node [
    id 24
    label "pojawianie_si&#281;"
  ]
  node [
    id 25
    label "czynno&#347;&#263;"
  ]
  node [
    id 26
    label "krytykowanie"
  ]
  node [
    id 27
    label "torpedowanie"
  ]
  node [
    id 28
    label "szczucie"
  ]
  node [
    id 29
    label "przebywanie"
  ]
  node [
    id 30
    label "oddzia&#322;ywanie"
  ]
  node [
    id 31
    label "friction"
  ]
  node [
    id 32
    label "nast&#281;powanie"
  ]
  node [
    id 33
    label "granie"
  ]
  node [
    id 34
    label "strike"
  ]
  node [
    id 35
    label "robi&#263;"
  ]
  node [
    id 36
    label "dzia&#322;a&#263;"
  ]
  node [
    id 37
    label "ofensywny"
  ]
  node [
    id 38
    label "attack"
  ]
  node [
    id 39
    label "rozgrywa&#263;"
  ]
  node [
    id 40
    label "krytykowa&#263;"
  ]
  node [
    id 41
    label "walczy&#263;"
  ]
  node [
    id 42
    label "aim"
  ]
  node [
    id 43
    label "trouble_oneself"
  ]
  node [
    id 44
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 45
    label "napada&#263;"
  ]
  node [
    id 46
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 47
    label "m&#243;wi&#263;"
  ]
  node [
    id 48
    label "nast&#281;powa&#263;"
  ]
  node [
    id 49
    label "usi&#322;owa&#263;"
  ]
  node [
    id 50
    label "mysikr&#243;lik"
  ]
  node [
    id 51
    label "futro"
  ]
  node [
    id 52
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 53
    label "tuszka"
  ]
  node [
    id 54
    label "zaj&#261;cowate"
  ]
  node [
    id 55
    label "kicaj"
  ]
  node [
    id 56
    label "comber"
  ]
  node [
    id 57
    label "omyk"
  ]
  node [
    id 58
    label "trusia"
  ]
  node [
    id 59
    label "wirus"
  ]
  node [
    id 60
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 61
    label "program"
  ]
  node [
    id 62
    label "mi&#281;so"
  ]
  node [
    id 63
    label "turzyca"
  ]
  node [
    id 64
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 65
    label "fitofag"
  ]
  node [
    id 66
    label "herbivore"
  ]
  node [
    id 67
    label "zwierz&#281;"
  ]
  node [
    id 68
    label "ssak_&#380;yworodny"
  ]
  node [
    id 69
    label "ssaki_wy&#380;sze"
  ]
  node [
    id 70
    label "surowiec"
  ]
  node [
    id 71
    label "sier&#347;&#263;"
  ]
  node [
    id 72
    label "okrycie"
  ]
  node [
    id 73
    label "haircloth"
  ]
  node [
    id 74
    label "instalowa&#263;"
  ]
  node [
    id 75
    label "oprogramowanie"
  ]
  node [
    id 76
    label "odinstalowywa&#263;"
  ]
  node [
    id 77
    label "spis"
  ]
  node [
    id 78
    label "zaprezentowanie"
  ]
  node [
    id 79
    label "podprogram"
  ]
  node [
    id 80
    label "ogranicznik_referencyjny"
  ]
  node [
    id 81
    label "course_of_study"
  ]
  node [
    id 82
    label "booklet"
  ]
  node [
    id 83
    label "dzia&#322;"
  ]
  node [
    id 84
    label "odinstalowanie"
  ]
  node [
    id 85
    label "broszura"
  ]
  node [
    id 86
    label "wytw&#243;r"
  ]
  node [
    id 87
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 88
    label "kana&#322;"
  ]
  node [
    id 89
    label "teleferie"
  ]
  node [
    id 90
    label "zainstalowanie"
  ]
  node [
    id 91
    label "struktura_organizacyjna"
  ]
  node [
    id 92
    label "pirat"
  ]
  node [
    id 93
    label "zaprezentowa&#263;"
  ]
  node [
    id 94
    label "prezentowanie"
  ]
  node [
    id 95
    label "prezentowa&#263;"
  ]
  node [
    id 96
    label "interfejs"
  ]
  node [
    id 97
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 98
    label "okno"
  ]
  node [
    id 99
    label "blok"
  ]
  node [
    id 100
    label "punkt"
  ]
  node [
    id 101
    label "folder"
  ]
  node [
    id 102
    label "zainstalowa&#263;"
  ]
  node [
    id 103
    label "za&#322;o&#380;enie"
  ]
  node [
    id 104
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 105
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 106
    label "ram&#243;wka"
  ]
  node [
    id 107
    label "tryb"
  ]
  node [
    id 108
    label "emitowa&#263;"
  ]
  node [
    id 109
    label "emitowanie"
  ]
  node [
    id 110
    label "odinstalowywanie"
  ]
  node [
    id 111
    label "instrukcja"
  ]
  node [
    id 112
    label "informatyka"
  ]
  node [
    id 113
    label "deklaracja"
  ]
  node [
    id 114
    label "menu"
  ]
  node [
    id 115
    label "sekcja_krytyczna"
  ]
  node [
    id 116
    label "furkacja"
  ]
  node [
    id 117
    label "podstawa"
  ]
  node [
    id 118
    label "instalowanie"
  ]
  node [
    id 119
    label "oferta"
  ]
  node [
    id 120
    label "odinstalowa&#263;"
  ]
  node [
    id 121
    label "skrusze&#263;"
  ]
  node [
    id 122
    label "luzowanie"
  ]
  node [
    id 123
    label "t&#322;uczenie"
  ]
  node [
    id 124
    label "wyluzowanie"
  ]
  node [
    id 125
    label "ut&#322;uczenie"
  ]
  node [
    id 126
    label "tempeh"
  ]
  node [
    id 127
    label "produkt"
  ]
  node [
    id 128
    label "jedzenie"
  ]
  node [
    id 129
    label "krusze&#263;"
  ]
  node [
    id 130
    label "seitan"
  ]
  node [
    id 131
    label "mi&#281;sie&#324;"
  ]
  node [
    id 132
    label "cia&#322;o"
  ]
  node [
    id 133
    label "chabanina"
  ]
  node [
    id 134
    label "luzowa&#263;"
  ]
  node [
    id 135
    label "marynata"
  ]
  node [
    id 136
    label "wyluzowa&#263;"
  ]
  node [
    id 137
    label "potrawa"
  ]
  node [
    id 138
    label "obieralnia"
  ]
  node [
    id 139
    label "panierka"
  ]
  node [
    id 140
    label "kosmopolita"
  ]
  node [
    id 141
    label "ciborowate"
  ]
  node [
    id 142
    label "samica"
  ]
  node [
    id 143
    label "szuwar_turzycowy"
  ]
  node [
    id 144
    label "bylina"
  ]
  node [
    id 145
    label "trawa"
  ]
  node [
    id 146
    label "zaj&#261;c"
  ]
  node [
    id 147
    label "tur"
  ]
  node [
    id 148
    label "ogon"
  ]
  node [
    id 149
    label "zaj&#281;czaki"
  ]
  node [
    id 150
    label "czujny"
  ]
  node [
    id 151
    label "cz&#322;owiek"
  ]
  node [
    id 152
    label "strachliwy"
  ]
  node [
    id 153
    label "potulny"
  ]
  node [
    id 154
    label "cichy"
  ]
  node [
    id 155
    label "mysikr&#243;liki"
  ]
  node [
    id 156
    label "ptak_w&#281;drowny"
  ]
  node [
    id 157
    label "ptak"
  ]
  node [
    id 158
    label "cz&#261;steczka"
  ]
  node [
    id 159
    label "botnet"
  ]
  node [
    id 160
    label "trojan"
  ]
  node [
    id 161
    label "prokariont"
  ]
  node [
    id 162
    label "wirusy"
  ]
  node [
    id 163
    label "ryba"
  ]
  node [
    id 164
    label "dr&#243;b"
  ]
  node [
    id 165
    label "t&#322;usty_czwartek"
  ]
  node [
    id 166
    label "pieczyste"
  ]
  node [
    id 167
    label "ciasto"
  ]
  node [
    id 168
    label "baranina"
  ]
  node [
    id 169
    label "zabawa"
  ]
  node [
    id 170
    label "dziczyzna"
  ]
  node [
    id 171
    label "krzy&#380;owa"
  ]
  node [
    id 172
    label "barb&#243;rka"
  ]
  node [
    id 173
    label "Barb&#243;rka"
  ]
  node [
    id 174
    label "post&#261;pi&#263;"
  ]
  node [
    id 175
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 176
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 177
    label "odj&#261;&#263;"
  ]
  node [
    id 178
    label "zrobi&#263;"
  ]
  node [
    id 179
    label "cause"
  ]
  node [
    id 180
    label "introduce"
  ]
  node [
    id 181
    label "begin"
  ]
  node [
    id 182
    label "do"
  ]
  node [
    id 183
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 184
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 185
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 186
    label "zorganizowa&#263;"
  ]
  node [
    id 187
    label "appoint"
  ]
  node [
    id 188
    label "wystylizowa&#263;"
  ]
  node [
    id 189
    label "przerobi&#263;"
  ]
  node [
    id 190
    label "nabra&#263;"
  ]
  node [
    id 191
    label "make"
  ]
  node [
    id 192
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 193
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 194
    label "wydali&#263;"
  ]
  node [
    id 195
    label "withdraw"
  ]
  node [
    id 196
    label "zabra&#263;"
  ]
  node [
    id 197
    label "oddzieli&#263;"
  ]
  node [
    id 198
    label "policzy&#263;"
  ]
  node [
    id 199
    label "reduce"
  ]
  node [
    id 200
    label "oddali&#263;"
  ]
  node [
    id 201
    label "separate"
  ]
  node [
    id 202
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 203
    label "advance"
  ]
  node [
    id 204
    label "act"
  ]
  node [
    id 205
    label "see"
  ]
  node [
    id 206
    label "ut"
  ]
  node [
    id 207
    label "d&#378;wi&#281;k"
  ]
  node [
    id 208
    label "C"
  ]
  node [
    id 209
    label "his"
  ]
  node [
    id 210
    label "w_chuj"
  ]
  node [
    id 211
    label "wczesny"
  ]
  node [
    id 212
    label "wczesno"
  ]
  node [
    id 213
    label "pocz&#261;tkowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
]
