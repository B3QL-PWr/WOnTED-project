graph [
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "dobry"
    origin "text"
  ]
  node [
    id 3
    label "xxx"
    origin "text"
  ]
  node [
    id 4
    label "czym"
    origin "text"
  ]
  node [
    id 5
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 6
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 7
    label "drzewo_owocowe"
  ]
  node [
    id 8
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 9
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 10
    label "skuteczny"
  ]
  node [
    id 11
    label "ca&#322;y"
  ]
  node [
    id 12
    label "czw&#243;rka"
  ]
  node [
    id 13
    label "spokojny"
  ]
  node [
    id 14
    label "pos&#322;uszny"
  ]
  node [
    id 15
    label "korzystny"
  ]
  node [
    id 16
    label "drogi"
  ]
  node [
    id 17
    label "pozytywny"
  ]
  node [
    id 18
    label "moralny"
  ]
  node [
    id 19
    label "pomy&#347;lny"
  ]
  node [
    id 20
    label "powitanie"
  ]
  node [
    id 21
    label "grzeczny"
  ]
  node [
    id 22
    label "&#347;mieszny"
  ]
  node [
    id 23
    label "odpowiedni"
  ]
  node [
    id 24
    label "zwrot"
  ]
  node [
    id 25
    label "dobrze"
  ]
  node [
    id 26
    label "dobroczynny"
  ]
  node [
    id 27
    label "mi&#322;y"
  ]
  node [
    id 28
    label "etycznie"
  ]
  node [
    id 29
    label "moralnie"
  ]
  node [
    id 30
    label "warto&#347;ciowy"
  ]
  node [
    id 31
    label "taki"
  ]
  node [
    id 32
    label "stosownie"
  ]
  node [
    id 33
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 34
    label "prawdziwy"
  ]
  node [
    id 35
    label "typowy"
  ]
  node [
    id 36
    label "zasadniczy"
  ]
  node [
    id 37
    label "charakterystyczny"
  ]
  node [
    id 38
    label "uprawniony"
  ]
  node [
    id 39
    label "nale&#380;yty"
  ]
  node [
    id 40
    label "ten"
  ]
  node [
    id 41
    label "nale&#380;ny"
  ]
  node [
    id 42
    label "pozytywnie"
  ]
  node [
    id 43
    label "fajny"
  ]
  node [
    id 44
    label "przyjemny"
  ]
  node [
    id 45
    label "po&#380;&#261;dany"
  ]
  node [
    id 46
    label "dodatnio"
  ]
  node [
    id 47
    label "o&#347;mieszenie"
  ]
  node [
    id 48
    label "o&#347;mieszanie"
  ]
  node [
    id 49
    label "&#347;miesznie"
  ]
  node [
    id 50
    label "nieadekwatny"
  ]
  node [
    id 51
    label "bawny"
  ]
  node [
    id 52
    label "niepowa&#380;ny"
  ]
  node [
    id 53
    label "dziwny"
  ]
  node [
    id 54
    label "pos&#322;usznie"
  ]
  node [
    id 55
    label "zale&#380;ny"
  ]
  node [
    id 56
    label "uleg&#322;y"
  ]
  node [
    id 57
    label "konserwatywny"
  ]
  node [
    id 58
    label "stosowny"
  ]
  node [
    id 59
    label "grzecznie"
  ]
  node [
    id 60
    label "nijaki"
  ]
  node [
    id 61
    label "niewinny"
  ]
  node [
    id 62
    label "uspokojenie_si&#281;"
  ]
  node [
    id 63
    label "wolny"
  ]
  node [
    id 64
    label "bezproblemowy"
  ]
  node [
    id 65
    label "uspokajanie_si&#281;"
  ]
  node [
    id 66
    label "spokojnie"
  ]
  node [
    id 67
    label "uspokojenie"
  ]
  node [
    id 68
    label "nietrudny"
  ]
  node [
    id 69
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 70
    label "cicho"
  ]
  node [
    id 71
    label "uspokajanie"
  ]
  node [
    id 72
    label "korzystnie"
  ]
  node [
    id 73
    label "cz&#322;owiek"
  ]
  node [
    id 74
    label "przyjaciel"
  ]
  node [
    id 75
    label "bliski"
  ]
  node [
    id 76
    label "drogo"
  ]
  node [
    id 77
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 78
    label "kompletny"
  ]
  node [
    id 79
    label "zdr&#243;w"
  ]
  node [
    id 80
    label "ca&#322;o"
  ]
  node [
    id 81
    label "du&#380;y"
  ]
  node [
    id 82
    label "calu&#347;ko"
  ]
  node [
    id 83
    label "podobny"
  ]
  node [
    id 84
    label "&#380;ywy"
  ]
  node [
    id 85
    label "pe&#322;ny"
  ]
  node [
    id 86
    label "jedyny"
  ]
  node [
    id 87
    label "sprawny"
  ]
  node [
    id 88
    label "skutkowanie"
  ]
  node [
    id 89
    label "poskutkowanie"
  ]
  node [
    id 90
    label "skutecznie"
  ]
  node [
    id 91
    label "pomy&#347;lnie"
  ]
  node [
    id 92
    label "zbi&#243;r"
  ]
  node [
    id 93
    label "przedtrzonowiec"
  ]
  node [
    id 94
    label "trafienie"
  ]
  node [
    id 95
    label "osada"
  ]
  node [
    id 96
    label "blotka"
  ]
  node [
    id 97
    label "p&#322;yta_winylowa"
  ]
  node [
    id 98
    label "cyfra"
  ]
  node [
    id 99
    label "pok&#243;j"
  ]
  node [
    id 100
    label "obiekt"
  ]
  node [
    id 101
    label "stopie&#324;"
  ]
  node [
    id 102
    label "arkusz_drukarski"
  ]
  node [
    id 103
    label "zaprz&#281;g"
  ]
  node [
    id 104
    label "toto-lotek"
  ]
  node [
    id 105
    label "&#263;wiartka"
  ]
  node [
    id 106
    label "&#322;&#243;dka"
  ]
  node [
    id 107
    label "four"
  ]
  node [
    id 108
    label "minialbum"
  ]
  node [
    id 109
    label "hotel"
  ]
  node [
    id 110
    label "punkt"
  ]
  node [
    id 111
    label "zmiana"
  ]
  node [
    id 112
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 113
    label "turn"
  ]
  node [
    id 114
    label "wyra&#380;enie"
  ]
  node [
    id 115
    label "fraza_czasownikowa"
  ]
  node [
    id 116
    label "turning"
  ]
  node [
    id 117
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 118
    label "skr&#281;t"
  ]
  node [
    id 119
    label "jednostka_leksykalna"
  ]
  node [
    id 120
    label "obr&#243;t"
  ]
  node [
    id 121
    label "spotkanie"
  ]
  node [
    id 122
    label "pozdrowienie"
  ]
  node [
    id 123
    label "welcome"
  ]
  node [
    id 124
    label "zwyczaj"
  ]
  node [
    id 125
    label "greeting"
  ]
  node [
    id 126
    label "zdarzony"
  ]
  node [
    id 127
    label "odpowiednio"
  ]
  node [
    id 128
    label "specjalny"
  ]
  node [
    id 129
    label "odpowiadanie"
  ]
  node [
    id 130
    label "wybranek"
  ]
  node [
    id 131
    label "sk&#322;onny"
  ]
  node [
    id 132
    label "kochanek"
  ]
  node [
    id 133
    label "mi&#322;o"
  ]
  node [
    id 134
    label "dyplomata"
  ]
  node [
    id 135
    label "umi&#322;owany"
  ]
  node [
    id 136
    label "kochanie"
  ]
  node [
    id 137
    label "przyjemnie"
  ]
  node [
    id 138
    label "wiele"
  ]
  node [
    id 139
    label "lepiej"
  ]
  node [
    id 140
    label "dobroczynnie"
  ]
  node [
    id 141
    label "spo&#322;eczny"
  ]
  node [
    id 142
    label "by&#263;"
  ]
  node [
    id 143
    label "uprawi&#263;"
  ]
  node [
    id 144
    label "gotowy"
  ]
  node [
    id 145
    label "might"
  ]
  node [
    id 146
    label "pole"
  ]
  node [
    id 147
    label "public_treasury"
  ]
  node [
    id 148
    label "obrobi&#263;"
  ]
  node [
    id 149
    label "nietrze&#378;wy"
  ]
  node [
    id 150
    label "gotowo"
  ]
  node [
    id 151
    label "przygotowywanie"
  ]
  node [
    id 152
    label "dyspozycyjny"
  ]
  node [
    id 153
    label "przygotowanie"
  ]
  node [
    id 154
    label "martwy"
  ]
  node [
    id 155
    label "zalany"
  ]
  node [
    id 156
    label "doj&#347;cie"
  ]
  node [
    id 157
    label "nieuchronny"
  ]
  node [
    id 158
    label "czekanie"
  ]
  node [
    id 159
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 160
    label "stan"
  ]
  node [
    id 161
    label "stand"
  ]
  node [
    id 162
    label "trwa&#263;"
  ]
  node [
    id 163
    label "equal"
  ]
  node [
    id 164
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 165
    label "chodzi&#263;"
  ]
  node [
    id 166
    label "uczestniczy&#263;"
  ]
  node [
    id 167
    label "obecno&#347;&#263;"
  ]
  node [
    id 168
    label "si&#281;ga&#263;"
  ]
  node [
    id 169
    label "mie&#263;_miejsce"
  ]
  node [
    id 170
    label "zaskutkowa&#263;"
  ]
  node [
    id 171
    label "help"
  ]
  node [
    id 172
    label "u&#322;atwi&#263;"
  ]
  node [
    id 173
    label "zrobi&#263;"
  ]
  node [
    id 174
    label "concur"
  ]
  node [
    id 175
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 176
    label "aid"
  ]
  node [
    id 177
    label "zorganizowa&#263;"
  ]
  node [
    id 178
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 179
    label "wydali&#263;"
  ]
  node [
    id 180
    label "make"
  ]
  node [
    id 181
    label "wystylizowa&#263;"
  ]
  node [
    id 182
    label "appoint"
  ]
  node [
    id 183
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 184
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 185
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 186
    label "post&#261;pi&#263;"
  ]
  node [
    id 187
    label "przerobi&#263;"
  ]
  node [
    id 188
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 189
    label "cause"
  ]
  node [
    id 190
    label "nabra&#263;"
  ]
  node [
    id 191
    label "sprawdzi&#263;_si&#281;"
  ]
  node [
    id 192
    label "przynie&#347;&#263;"
  ]
  node [
    id 193
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 194
    label "poci&#261;gn&#261;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
]
