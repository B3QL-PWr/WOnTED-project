graph [
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "pisa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "cia"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 5
    label "zmniejszy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "wynagrodzenie"
    origin "text"
  ]
  node [
    id 8
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 9
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 10
    label "twierdzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "taki"
    origin "text"
  ]
  node [
    id 12
    label "niebezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "niewielki"
    origin "text"
  ]
  node [
    id 15
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 16
    label "dop&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 17
    label "pensja"
    origin "text"
  ]
  node [
    id 18
    label "aby"
    origin "text"
  ]
  node [
    id 19
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 20
    label "niski"
    origin "text"
  ]
  node [
    id 21
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 22
    label "mie&#263;by"
    origin "text"
  ]
  node [
    id 23
    label "podwy&#380;szy&#263;"
    origin "text"
  ]
  node [
    id 24
    label "proca"
    origin "text"
  ]
  node [
    id 25
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 26
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 27
    label "przeznaczy&#263;"
    origin "text"
  ]
  node [
    id 28
    label "ponad"
    origin "text"
  ]
  node [
    id 29
    label "milion"
    origin "text"
  ]
  node [
    id 30
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 31
    label "wiele"
    origin "text"
  ]
  node [
    id 32
    label "deklarowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "utrzyma&#263;"
    origin "text"
  ]
  node [
    id 34
    label "p&#322;aca"
    origin "text"
  ]
  node [
    id 35
    label "obecna"
    origin "text"
  ]
  node [
    id 36
    label "poziom"
    origin "text"
  ]
  node [
    id 37
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 38
    label "zobo"
  ]
  node [
    id 39
    label "yakalo"
  ]
  node [
    id 40
    label "byd&#322;o"
  ]
  node [
    id 41
    label "dzo"
  ]
  node [
    id 42
    label "kr&#281;torogie"
  ]
  node [
    id 43
    label "zbi&#243;r"
  ]
  node [
    id 44
    label "g&#322;owa"
  ]
  node [
    id 45
    label "czochrad&#322;o"
  ]
  node [
    id 46
    label "posp&#243;lstwo"
  ]
  node [
    id 47
    label "kraal"
  ]
  node [
    id 48
    label "livestock"
  ]
  node [
    id 49
    label "prze&#380;uwacz"
  ]
  node [
    id 50
    label "zebu"
  ]
  node [
    id 51
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 52
    label "bizon"
  ]
  node [
    id 53
    label "byd&#322;o_domowe"
  ]
  node [
    id 54
    label "p&#243;&#322;rocze"
  ]
  node [
    id 55
    label "martwy_sezon"
  ]
  node [
    id 56
    label "kalendarz"
  ]
  node [
    id 57
    label "cykl_astronomiczny"
  ]
  node [
    id 58
    label "lata"
  ]
  node [
    id 59
    label "pora_roku"
  ]
  node [
    id 60
    label "stulecie"
  ]
  node [
    id 61
    label "kurs"
  ]
  node [
    id 62
    label "czas"
  ]
  node [
    id 63
    label "jubileusz"
  ]
  node [
    id 64
    label "grupa"
  ]
  node [
    id 65
    label "kwarta&#322;"
  ]
  node [
    id 66
    label "miesi&#261;c"
  ]
  node [
    id 67
    label "summer"
  ]
  node [
    id 68
    label "odm&#322;adzanie"
  ]
  node [
    id 69
    label "liga"
  ]
  node [
    id 70
    label "jednostka_systematyczna"
  ]
  node [
    id 71
    label "asymilowanie"
  ]
  node [
    id 72
    label "gromada"
  ]
  node [
    id 73
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 74
    label "asymilowa&#263;"
  ]
  node [
    id 75
    label "egzemplarz"
  ]
  node [
    id 76
    label "Entuzjastki"
  ]
  node [
    id 77
    label "kompozycja"
  ]
  node [
    id 78
    label "Terranie"
  ]
  node [
    id 79
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 80
    label "category"
  ]
  node [
    id 81
    label "pakiet_klimatyczny"
  ]
  node [
    id 82
    label "oddzia&#322;"
  ]
  node [
    id 83
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 84
    label "cz&#261;steczka"
  ]
  node [
    id 85
    label "stage_set"
  ]
  node [
    id 86
    label "type"
  ]
  node [
    id 87
    label "specgrupa"
  ]
  node [
    id 88
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 89
    label "&#346;wietliki"
  ]
  node [
    id 90
    label "odm&#322;odzenie"
  ]
  node [
    id 91
    label "Eurogrupa"
  ]
  node [
    id 92
    label "odm&#322;adza&#263;"
  ]
  node [
    id 93
    label "formacja_geologiczna"
  ]
  node [
    id 94
    label "harcerze_starsi"
  ]
  node [
    id 95
    label "poprzedzanie"
  ]
  node [
    id 96
    label "czasoprzestrze&#324;"
  ]
  node [
    id 97
    label "laba"
  ]
  node [
    id 98
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 99
    label "chronometria"
  ]
  node [
    id 100
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 101
    label "rachuba_czasu"
  ]
  node [
    id 102
    label "przep&#322;ywanie"
  ]
  node [
    id 103
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 104
    label "czasokres"
  ]
  node [
    id 105
    label "odczyt"
  ]
  node [
    id 106
    label "chwila"
  ]
  node [
    id 107
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 108
    label "dzieje"
  ]
  node [
    id 109
    label "kategoria_gramatyczna"
  ]
  node [
    id 110
    label "poprzedzenie"
  ]
  node [
    id 111
    label "trawienie"
  ]
  node [
    id 112
    label "pochodzi&#263;"
  ]
  node [
    id 113
    label "period"
  ]
  node [
    id 114
    label "okres_czasu"
  ]
  node [
    id 115
    label "poprzedza&#263;"
  ]
  node [
    id 116
    label "schy&#322;ek"
  ]
  node [
    id 117
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 118
    label "odwlekanie_si&#281;"
  ]
  node [
    id 119
    label "zegar"
  ]
  node [
    id 120
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 121
    label "czwarty_wymiar"
  ]
  node [
    id 122
    label "pochodzenie"
  ]
  node [
    id 123
    label "koniugacja"
  ]
  node [
    id 124
    label "Zeitgeist"
  ]
  node [
    id 125
    label "trawi&#263;"
  ]
  node [
    id 126
    label "pogoda"
  ]
  node [
    id 127
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 128
    label "poprzedzi&#263;"
  ]
  node [
    id 129
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 130
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 131
    label "time_period"
  ]
  node [
    id 132
    label "term"
  ]
  node [
    id 133
    label "rok_akademicki"
  ]
  node [
    id 134
    label "rok_szkolny"
  ]
  node [
    id 135
    label "semester"
  ]
  node [
    id 136
    label "anniwersarz"
  ]
  node [
    id 137
    label "rocznica"
  ]
  node [
    id 138
    label "obszar"
  ]
  node [
    id 139
    label "tydzie&#324;"
  ]
  node [
    id 140
    label "miech"
  ]
  node [
    id 141
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 142
    label "kalendy"
  ]
  node [
    id 143
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 144
    label "long_time"
  ]
  node [
    id 145
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 146
    label "almanac"
  ]
  node [
    id 147
    label "rozk&#322;ad"
  ]
  node [
    id 148
    label "wydawnictwo"
  ]
  node [
    id 149
    label "Juliusz_Cezar"
  ]
  node [
    id 150
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 151
    label "zwy&#380;kowanie"
  ]
  node [
    id 152
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 153
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 154
    label "zaj&#281;cia"
  ]
  node [
    id 155
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 156
    label "trasa"
  ]
  node [
    id 157
    label "przeorientowywanie"
  ]
  node [
    id 158
    label "przejazd"
  ]
  node [
    id 159
    label "kierunek"
  ]
  node [
    id 160
    label "przeorientowywa&#263;"
  ]
  node [
    id 161
    label "nauka"
  ]
  node [
    id 162
    label "przeorientowanie"
  ]
  node [
    id 163
    label "klasa"
  ]
  node [
    id 164
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 165
    label "przeorientowa&#263;"
  ]
  node [
    id 166
    label "manner"
  ]
  node [
    id 167
    label "course"
  ]
  node [
    id 168
    label "passage"
  ]
  node [
    id 169
    label "zni&#380;kowanie"
  ]
  node [
    id 170
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 171
    label "seria"
  ]
  node [
    id 172
    label "stawka"
  ]
  node [
    id 173
    label "way"
  ]
  node [
    id 174
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 175
    label "spos&#243;b"
  ]
  node [
    id 176
    label "deprecjacja"
  ]
  node [
    id 177
    label "cedu&#322;a"
  ]
  node [
    id 178
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 179
    label "drive"
  ]
  node [
    id 180
    label "bearing"
  ]
  node [
    id 181
    label "Lira"
  ]
  node [
    id 182
    label "soften"
  ]
  node [
    id 183
    label "zmieni&#263;"
  ]
  node [
    id 184
    label "sprawi&#263;"
  ]
  node [
    id 185
    label "change"
  ]
  node [
    id 186
    label "zrobi&#263;"
  ]
  node [
    id 187
    label "zast&#261;pi&#263;"
  ]
  node [
    id 188
    label "come_up"
  ]
  node [
    id 189
    label "przej&#347;&#263;"
  ]
  node [
    id 190
    label "straci&#263;"
  ]
  node [
    id 191
    label "zyska&#263;"
  ]
  node [
    id 192
    label "danie"
  ]
  node [
    id 193
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 194
    label "return"
  ]
  node [
    id 195
    label "refund"
  ]
  node [
    id 196
    label "liczenie"
  ]
  node [
    id 197
    label "liczy&#263;"
  ]
  node [
    id 198
    label "doch&#243;d"
  ]
  node [
    id 199
    label "wynagrodzenie_brutto"
  ]
  node [
    id 200
    label "koszt_rodzajowy"
  ]
  node [
    id 201
    label "policzy&#263;"
  ]
  node [
    id 202
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 203
    label "ordynaria"
  ]
  node [
    id 204
    label "bud&#380;et_domowy"
  ]
  node [
    id 205
    label "policzenie"
  ]
  node [
    id 206
    label "pay"
  ]
  node [
    id 207
    label "zap&#322;ata"
  ]
  node [
    id 208
    label "obiecanie"
  ]
  node [
    id 209
    label "zap&#322;acenie"
  ]
  node [
    id 210
    label "cios"
  ]
  node [
    id 211
    label "give"
  ]
  node [
    id 212
    label "udost&#281;pnienie"
  ]
  node [
    id 213
    label "rendition"
  ]
  node [
    id 214
    label "wymienienie_si&#281;"
  ]
  node [
    id 215
    label "eating"
  ]
  node [
    id 216
    label "coup"
  ]
  node [
    id 217
    label "hand"
  ]
  node [
    id 218
    label "uprawianie_seksu"
  ]
  node [
    id 219
    label "allow"
  ]
  node [
    id 220
    label "dostarczenie"
  ]
  node [
    id 221
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 222
    label "uderzenie"
  ]
  node [
    id 223
    label "zadanie"
  ]
  node [
    id 224
    label "powierzenie"
  ]
  node [
    id 225
    label "przeznaczenie"
  ]
  node [
    id 226
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 227
    label "przekazanie"
  ]
  node [
    id 228
    label "odst&#261;pienie"
  ]
  node [
    id 229
    label "dodanie"
  ]
  node [
    id 230
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 231
    label "wyposa&#380;enie"
  ]
  node [
    id 232
    label "czynno&#347;&#263;"
  ]
  node [
    id 233
    label "dostanie"
  ]
  node [
    id 234
    label "karta"
  ]
  node [
    id 235
    label "potrawa"
  ]
  node [
    id 236
    label "pass"
  ]
  node [
    id 237
    label "menu"
  ]
  node [
    id 238
    label "uderzanie"
  ]
  node [
    id 239
    label "wyst&#261;pienie"
  ]
  node [
    id 240
    label "jedzenie"
  ]
  node [
    id 241
    label "wyposa&#380;anie"
  ]
  node [
    id 242
    label "pobicie"
  ]
  node [
    id 243
    label "posi&#322;ek"
  ]
  node [
    id 244
    label "urz&#261;dzenie"
  ]
  node [
    id 245
    label "zrobienie"
  ]
  node [
    id 246
    label "kwota"
  ]
  node [
    id 247
    label "konsekwencja"
  ]
  node [
    id 248
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 249
    label "income"
  ]
  node [
    id 250
    label "stopa_procentowa"
  ]
  node [
    id 251
    label "krzywa_Engla"
  ]
  node [
    id 252
    label "korzy&#347;&#263;"
  ]
  node [
    id 253
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 254
    label "wp&#322;yw"
  ]
  node [
    id 255
    label "nagroda"
  ]
  node [
    id 256
    label "compensate"
  ]
  node [
    id 257
    label "czelad&#378;"
  ]
  node [
    id 258
    label "wyrachowa&#263;"
  ]
  node [
    id 259
    label "wyceni&#263;"
  ]
  node [
    id 260
    label "wzi&#261;&#263;"
  ]
  node [
    id 261
    label "okre&#347;li&#263;"
  ]
  node [
    id 262
    label "charge"
  ]
  node [
    id 263
    label "zakwalifikowa&#263;"
  ]
  node [
    id 264
    label "frame"
  ]
  node [
    id 265
    label "wyznaczy&#263;"
  ]
  node [
    id 266
    label "badanie"
  ]
  node [
    id 267
    label "rachowanie"
  ]
  node [
    id 268
    label "dyskalkulia"
  ]
  node [
    id 269
    label "rozliczanie"
  ]
  node [
    id 270
    label "wymienianie"
  ]
  node [
    id 271
    label "oznaczanie"
  ]
  node [
    id 272
    label "wychodzenie"
  ]
  node [
    id 273
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 274
    label "naliczenie_si&#281;"
  ]
  node [
    id 275
    label "wyznaczanie"
  ]
  node [
    id 276
    label "dodawanie"
  ]
  node [
    id 277
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 278
    label "bang"
  ]
  node [
    id 279
    label "spodziewanie_si&#281;"
  ]
  node [
    id 280
    label "kwotowanie"
  ]
  node [
    id 281
    label "rozliczenie"
  ]
  node [
    id 282
    label "mierzenie"
  ]
  node [
    id 283
    label "count"
  ]
  node [
    id 284
    label "wycenianie"
  ]
  node [
    id 285
    label "branie"
  ]
  node [
    id 286
    label "sprowadzanie"
  ]
  node [
    id 287
    label "przeliczanie"
  ]
  node [
    id 288
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 289
    label "odliczanie"
  ]
  node [
    id 290
    label "przeliczenie"
  ]
  node [
    id 291
    label "evaluation"
  ]
  node [
    id 292
    label "wyrachowanie"
  ]
  node [
    id 293
    label "ustalenie"
  ]
  node [
    id 294
    label "zakwalifikowanie"
  ]
  node [
    id 295
    label "wyznaczenie"
  ]
  node [
    id 296
    label "wycenienie"
  ]
  node [
    id 297
    label "wyj&#347;cie"
  ]
  node [
    id 298
    label "zbadanie"
  ]
  node [
    id 299
    label "sprowadzenie"
  ]
  node [
    id 300
    label "przeliczenie_si&#281;"
  ]
  node [
    id 301
    label "report"
  ]
  node [
    id 302
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 303
    label "osi&#261;ga&#263;"
  ]
  node [
    id 304
    label "wymienia&#263;"
  ]
  node [
    id 305
    label "posiada&#263;"
  ]
  node [
    id 306
    label "wycenia&#263;"
  ]
  node [
    id 307
    label "bra&#263;"
  ]
  node [
    id 308
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 309
    label "mierzy&#263;"
  ]
  node [
    id 310
    label "rachowa&#263;"
  ]
  node [
    id 311
    label "tell"
  ]
  node [
    id 312
    label "odlicza&#263;"
  ]
  node [
    id 313
    label "dodawa&#263;"
  ]
  node [
    id 314
    label "wyznacza&#263;"
  ]
  node [
    id 315
    label "admit"
  ]
  node [
    id 316
    label "policza&#263;"
  ]
  node [
    id 317
    label "okre&#347;la&#263;"
  ]
  node [
    id 318
    label "belfer"
  ]
  node [
    id 319
    label "kszta&#322;ciciel"
  ]
  node [
    id 320
    label "preceptor"
  ]
  node [
    id 321
    label "pedagog"
  ]
  node [
    id 322
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 323
    label "szkolnik"
  ]
  node [
    id 324
    label "profesor"
  ]
  node [
    id 325
    label "popularyzator"
  ]
  node [
    id 326
    label "rozszerzyciel"
  ]
  node [
    id 327
    label "cz&#322;owiek"
  ]
  node [
    id 328
    label "autor"
  ]
  node [
    id 329
    label "wyprawka"
  ]
  node [
    id 330
    label "mundurek"
  ]
  node [
    id 331
    label "szko&#322;a"
  ]
  node [
    id 332
    label "tarcza"
  ]
  node [
    id 333
    label "elew"
  ]
  node [
    id 334
    label "absolwent"
  ]
  node [
    id 335
    label "stopie&#324;_naukowy"
  ]
  node [
    id 336
    label "nauczyciel_akademicki"
  ]
  node [
    id 337
    label "tytu&#322;"
  ]
  node [
    id 338
    label "profesura"
  ]
  node [
    id 339
    label "konsulent"
  ]
  node [
    id 340
    label "wirtuoz"
  ]
  node [
    id 341
    label "zwierzchnik"
  ]
  node [
    id 342
    label "ekspert"
  ]
  node [
    id 343
    label "ochotnik"
  ]
  node [
    id 344
    label "pomocnik"
  ]
  node [
    id 345
    label "student"
  ]
  node [
    id 346
    label "nauczyciel_muzyki"
  ]
  node [
    id 347
    label "zakonnik"
  ]
  node [
    id 348
    label "J&#281;drzejewicz"
  ]
  node [
    id 349
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 350
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 351
    label "John_Dewey"
  ]
  node [
    id 352
    label "przybli&#380;enie"
  ]
  node [
    id 353
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 354
    label "kategoria"
  ]
  node [
    id 355
    label "szpaler"
  ]
  node [
    id 356
    label "lon&#380;a"
  ]
  node [
    id 357
    label "uporz&#261;dkowanie"
  ]
  node [
    id 358
    label "instytucja"
  ]
  node [
    id 359
    label "egzekutywa"
  ]
  node [
    id 360
    label "premier"
  ]
  node [
    id 361
    label "Londyn"
  ]
  node [
    id 362
    label "gabinet_cieni"
  ]
  node [
    id 363
    label "number"
  ]
  node [
    id 364
    label "Konsulat"
  ]
  node [
    id 365
    label "tract"
  ]
  node [
    id 366
    label "w&#322;adza"
  ]
  node [
    id 367
    label "struktura"
  ]
  node [
    id 368
    label "spowodowanie"
  ]
  node [
    id 369
    label "structure"
  ]
  node [
    id 370
    label "sequence"
  ]
  node [
    id 371
    label "succession"
  ]
  node [
    id 372
    label "zapoznanie"
  ]
  node [
    id 373
    label "podanie"
  ]
  node [
    id 374
    label "bliski"
  ]
  node [
    id 375
    label "wyja&#347;nienie"
  ]
  node [
    id 376
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 377
    label "przemieszczenie"
  ]
  node [
    id 378
    label "approach"
  ]
  node [
    id 379
    label "pickup"
  ]
  node [
    id 380
    label "estimate"
  ]
  node [
    id 381
    label "po&#322;&#261;czenie"
  ]
  node [
    id 382
    label "ocena"
  ]
  node [
    id 383
    label "wytw&#243;r"
  ]
  node [
    id 384
    label "poj&#281;cie"
  ]
  node [
    id 385
    label "teoria"
  ]
  node [
    id 386
    label "forma"
  ]
  node [
    id 387
    label "osoba_prawna"
  ]
  node [
    id 388
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 389
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 390
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 391
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 392
    label "biuro"
  ]
  node [
    id 393
    label "organizacja"
  ]
  node [
    id 394
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 395
    label "Fundusze_Unijne"
  ]
  node [
    id 396
    label "zamyka&#263;"
  ]
  node [
    id 397
    label "establishment"
  ]
  node [
    id 398
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 399
    label "urz&#261;d"
  ]
  node [
    id 400
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 401
    label "afiliowa&#263;"
  ]
  node [
    id 402
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 403
    label "standard"
  ]
  node [
    id 404
    label "zamykanie"
  ]
  node [
    id 405
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 406
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 407
    label "organ"
  ]
  node [
    id 408
    label "obrady"
  ]
  node [
    id 409
    label "executive"
  ]
  node [
    id 410
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 411
    label "partia"
  ]
  node [
    id 412
    label "federacja"
  ]
  node [
    id 413
    label "przej&#347;cie"
  ]
  node [
    id 414
    label "espalier"
  ]
  node [
    id 415
    label "aleja"
  ]
  node [
    id 416
    label "szyk"
  ]
  node [
    id 417
    label "typ"
  ]
  node [
    id 418
    label "jednostka_administracyjna"
  ]
  node [
    id 419
    label "zoologia"
  ]
  node [
    id 420
    label "skupienie"
  ]
  node [
    id 421
    label "kr&#243;lestwo"
  ]
  node [
    id 422
    label "tribe"
  ]
  node [
    id 423
    label "hurma"
  ]
  node [
    id 424
    label "botanika"
  ]
  node [
    id 425
    label "wagon"
  ]
  node [
    id 426
    label "mecz_mistrzowski"
  ]
  node [
    id 427
    label "przedmiot"
  ]
  node [
    id 428
    label "arrangement"
  ]
  node [
    id 429
    label "class"
  ]
  node [
    id 430
    label "&#322;awka"
  ]
  node [
    id 431
    label "wykrzyknik"
  ]
  node [
    id 432
    label "zaleta"
  ]
  node [
    id 433
    label "programowanie_obiektowe"
  ]
  node [
    id 434
    label "tablica"
  ]
  node [
    id 435
    label "warstwa"
  ]
  node [
    id 436
    label "rezerwa"
  ]
  node [
    id 437
    label "Ekwici"
  ]
  node [
    id 438
    label "&#347;rodowisko"
  ]
  node [
    id 439
    label "sala"
  ]
  node [
    id 440
    label "pomoc"
  ]
  node [
    id 441
    label "form"
  ]
  node [
    id 442
    label "przepisa&#263;"
  ]
  node [
    id 443
    label "jako&#347;&#263;"
  ]
  node [
    id 444
    label "znak_jako&#347;ci"
  ]
  node [
    id 445
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 446
    label "promocja"
  ]
  node [
    id 447
    label "przepisanie"
  ]
  node [
    id 448
    label "obiekt"
  ]
  node [
    id 449
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 450
    label "dziennik_lekcyjny"
  ]
  node [
    id 451
    label "fakcja"
  ]
  node [
    id 452
    label "obrona"
  ]
  node [
    id 453
    label "atak"
  ]
  node [
    id 454
    label "lina"
  ]
  node [
    id 455
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 456
    label "Bismarck"
  ]
  node [
    id 457
    label "Sto&#322;ypin"
  ]
  node [
    id 458
    label "Miko&#322;ajczyk"
  ]
  node [
    id 459
    label "Chruszczow"
  ]
  node [
    id 460
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 461
    label "Jelcyn"
  ]
  node [
    id 462
    label "dostojnik"
  ]
  node [
    id 463
    label "prawo"
  ]
  node [
    id 464
    label "rz&#261;dzenie"
  ]
  node [
    id 465
    label "panowanie"
  ]
  node [
    id 466
    label "Kreml"
  ]
  node [
    id 467
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 468
    label "wydolno&#347;&#263;"
  ]
  node [
    id 469
    label "Wimbledon"
  ]
  node [
    id 470
    label "Westminster"
  ]
  node [
    id 471
    label "Londek"
  ]
  node [
    id 472
    label "oznajmia&#263;"
  ]
  node [
    id 473
    label "zapewnia&#263;"
  ]
  node [
    id 474
    label "attest"
  ]
  node [
    id 475
    label "komunikowa&#263;"
  ]
  node [
    id 476
    label "argue"
  ]
  node [
    id 477
    label "communicate"
  ]
  node [
    id 478
    label "powodowa&#263;"
  ]
  node [
    id 479
    label "inform"
  ]
  node [
    id 480
    label "informowa&#263;"
  ]
  node [
    id 481
    label "dostarcza&#263;"
  ]
  node [
    id 482
    label "deliver"
  ]
  node [
    id 483
    label "utrzymywa&#263;"
  ]
  node [
    id 484
    label "okre&#347;lony"
  ]
  node [
    id 485
    label "jaki&#347;"
  ]
  node [
    id 486
    label "przyzwoity"
  ]
  node [
    id 487
    label "ciekawy"
  ]
  node [
    id 488
    label "jako&#347;"
  ]
  node [
    id 489
    label "jako_tako"
  ]
  node [
    id 490
    label "niez&#322;y"
  ]
  node [
    id 491
    label "dziwny"
  ]
  node [
    id 492
    label "charakterystyczny"
  ]
  node [
    id 493
    label "wiadomy"
  ]
  node [
    id 494
    label "zawisa&#263;"
  ]
  node [
    id 495
    label "cecha"
  ]
  node [
    id 496
    label "zagrozi&#263;"
  ]
  node [
    id 497
    label "zawisanie"
  ]
  node [
    id 498
    label "czarny_punkt"
  ]
  node [
    id 499
    label "charakterystyka"
  ]
  node [
    id 500
    label "m&#322;ot"
  ]
  node [
    id 501
    label "znak"
  ]
  node [
    id 502
    label "drzewo"
  ]
  node [
    id 503
    label "pr&#243;ba"
  ]
  node [
    id 504
    label "attribute"
  ]
  node [
    id 505
    label "marka"
  ]
  node [
    id 506
    label "mie&#263;_miejsce"
  ]
  node [
    id 507
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 508
    label "nieruchomie&#263;"
  ]
  node [
    id 509
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 510
    label "ko&#324;czy&#263;"
  ]
  node [
    id 511
    label "pojawianie_si&#281;"
  ]
  node [
    id 512
    label "umieranie"
  ]
  node [
    id 513
    label "nieruchomienie"
  ]
  node [
    id 514
    label "dzianie_si&#281;"
  ]
  node [
    id 515
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 516
    label "zaszachowa&#263;"
  ]
  node [
    id 517
    label "threaten"
  ]
  node [
    id 518
    label "szachowa&#263;"
  ]
  node [
    id 519
    label "zaistnie&#263;"
  ]
  node [
    id 520
    label "uprzedzi&#263;"
  ]
  node [
    id 521
    label "menace"
  ]
  node [
    id 522
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 523
    label "equal"
  ]
  node [
    id 524
    label "trwa&#263;"
  ]
  node [
    id 525
    label "chodzi&#263;"
  ]
  node [
    id 526
    label "si&#281;ga&#263;"
  ]
  node [
    id 527
    label "stan"
  ]
  node [
    id 528
    label "obecno&#347;&#263;"
  ]
  node [
    id 529
    label "stand"
  ]
  node [
    id 530
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 531
    label "uczestniczy&#263;"
  ]
  node [
    id 532
    label "participate"
  ]
  node [
    id 533
    label "robi&#263;"
  ]
  node [
    id 534
    label "istnie&#263;"
  ]
  node [
    id 535
    label "pozostawa&#263;"
  ]
  node [
    id 536
    label "zostawa&#263;"
  ]
  node [
    id 537
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 538
    label "adhere"
  ]
  node [
    id 539
    label "compass"
  ]
  node [
    id 540
    label "korzysta&#263;"
  ]
  node [
    id 541
    label "appreciation"
  ]
  node [
    id 542
    label "dociera&#263;"
  ]
  node [
    id 543
    label "get"
  ]
  node [
    id 544
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 545
    label "u&#380;ywa&#263;"
  ]
  node [
    id 546
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 547
    label "exsert"
  ]
  node [
    id 548
    label "being"
  ]
  node [
    id 549
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 550
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 551
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 552
    label "p&#322;ywa&#263;"
  ]
  node [
    id 553
    label "run"
  ]
  node [
    id 554
    label "bangla&#263;"
  ]
  node [
    id 555
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 556
    label "przebiega&#263;"
  ]
  node [
    id 557
    label "wk&#322;ada&#263;"
  ]
  node [
    id 558
    label "proceed"
  ]
  node [
    id 559
    label "carry"
  ]
  node [
    id 560
    label "bywa&#263;"
  ]
  node [
    id 561
    label "dziama&#263;"
  ]
  node [
    id 562
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 563
    label "stara&#263;_si&#281;"
  ]
  node [
    id 564
    label "para"
  ]
  node [
    id 565
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 566
    label "str&#243;j"
  ]
  node [
    id 567
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 568
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 569
    label "krok"
  ]
  node [
    id 570
    label "tryb"
  ]
  node [
    id 571
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 572
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 573
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 574
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 575
    label "continue"
  ]
  node [
    id 576
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 577
    label "Ohio"
  ]
  node [
    id 578
    label "wci&#281;cie"
  ]
  node [
    id 579
    label "Nowy_York"
  ]
  node [
    id 580
    label "samopoczucie"
  ]
  node [
    id 581
    label "Illinois"
  ]
  node [
    id 582
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 583
    label "state"
  ]
  node [
    id 584
    label "Jukatan"
  ]
  node [
    id 585
    label "Kalifornia"
  ]
  node [
    id 586
    label "Wirginia"
  ]
  node [
    id 587
    label "wektor"
  ]
  node [
    id 588
    label "Teksas"
  ]
  node [
    id 589
    label "Goa"
  ]
  node [
    id 590
    label "Waszyngton"
  ]
  node [
    id 591
    label "miejsce"
  ]
  node [
    id 592
    label "Massachusetts"
  ]
  node [
    id 593
    label "Alaska"
  ]
  node [
    id 594
    label "Arakan"
  ]
  node [
    id 595
    label "Hawaje"
  ]
  node [
    id 596
    label "Maryland"
  ]
  node [
    id 597
    label "punkt"
  ]
  node [
    id 598
    label "Michigan"
  ]
  node [
    id 599
    label "Arizona"
  ]
  node [
    id 600
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 601
    label "Georgia"
  ]
  node [
    id 602
    label "Pensylwania"
  ]
  node [
    id 603
    label "shape"
  ]
  node [
    id 604
    label "Luizjana"
  ]
  node [
    id 605
    label "Nowy_Meksyk"
  ]
  node [
    id 606
    label "Alabama"
  ]
  node [
    id 607
    label "ilo&#347;&#263;"
  ]
  node [
    id 608
    label "Kansas"
  ]
  node [
    id 609
    label "Oregon"
  ]
  node [
    id 610
    label "Floryda"
  ]
  node [
    id 611
    label "Oklahoma"
  ]
  node [
    id 612
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 613
    label "nielicznie"
  ]
  node [
    id 614
    label "niewa&#380;ny"
  ]
  node [
    id 615
    label "ma&#322;y"
  ]
  node [
    id 616
    label "ma&#322;o"
  ]
  node [
    id 617
    label "nieznaczny"
  ]
  node [
    id 618
    label "pomiernie"
  ]
  node [
    id 619
    label "kr&#243;tko"
  ]
  node [
    id 620
    label "mikroskopijnie"
  ]
  node [
    id 621
    label "nieliczny"
  ]
  node [
    id 622
    label "mo&#380;liwie"
  ]
  node [
    id 623
    label "nieistotnie"
  ]
  node [
    id 624
    label "szybki"
  ]
  node [
    id 625
    label "przeci&#281;tny"
  ]
  node [
    id 626
    label "wstydliwy"
  ]
  node [
    id 627
    label "s&#322;aby"
  ]
  node [
    id 628
    label "ch&#322;opiec"
  ]
  node [
    id 629
    label "m&#322;ody"
  ]
  node [
    id 630
    label "marny"
  ]
  node [
    id 631
    label "n&#281;dznie"
  ]
  node [
    id 632
    label "uniewa&#380;nianie_si&#281;"
  ]
  node [
    id 633
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 634
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 635
    label "autonomy"
  ]
  node [
    id 636
    label "tkanka"
  ]
  node [
    id 637
    label "jednostka_organizacyjna"
  ]
  node [
    id 638
    label "budowa"
  ]
  node [
    id 639
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 640
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 641
    label "tw&#243;r"
  ]
  node [
    id 642
    label "organogeneza"
  ]
  node [
    id 643
    label "zesp&#243;&#322;"
  ]
  node [
    id 644
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 645
    label "struktura_anatomiczna"
  ]
  node [
    id 646
    label "uk&#322;ad"
  ]
  node [
    id 647
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 648
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 649
    label "Izba_Konsyliarska"
  ]
  node [
    id 650
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 651
    label "stomia"
  ]
  node [
    id 652
    label "dekortykacja"
  ]
  node [
    id 653
    label "okolica"
  ]
  node [
    id 654
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 655
    label "Komitet_Region&#243;w"
  ]
  node [
    id 656
    label "zap&#322;aci&#263;"
  ]
  node [
    id 657
    label "wy&#322;oi&#263;"
  ]
  node [
    id 658
    label "picture"
  ]
  node [
    id 659
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 660
    label "zabuli&#263;"
  ]
  node [
    id 661
    label "wyda&#263;"
  ]
  node [
    id 662
    label "salariat"
  ]
  node [
    id 663
    label "kategoria_urz&#281;dnicza"
  ]
  node [
    id 664
    label "salarium"
  ]
  node [
    id 665
    label "do&#347;wiadczenie"
  ]
  node [
    id 666
    label "teren_szko&#322;y"
  ]
  node [
    id 667
    label "wiedza"
  ]
  node [
    id 668
    label "Mickiewicz"
  ]
  node [
    id 669
    label "kwalifikacje"
  ]
  node [
    id 670
    label "podr&#281;cznik"
  ]
  node [
    id 671
    label "praktyka"
  ]
  node [
    id 672
    label "school"
  ]
  node [
    id 673
    label "system"
  ]
  node [
    id 674
    label "zda&#263;"
  ]
  node [
    id 675
    label "gabinet"
  ]
  node [
    id 676
    label "urszulanki"
  ]
  node [
    id 677
    label "sztuba"
  ]
  node [
    id 678
    label "&#322;awa_szkolna"
  ]
  node [
    id 679
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 680
    label "muzyka"
  ]
  node [
    id 681
    label "lekcja"
  ]
  node [
    id 682
    label "metoda"
  ]
  node [
    id 683
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 684
    label "skolaryzacja"
  ]
  node [
    id 685
    label "zdanie"
  ]
  node [
    id 686
    label "stopek"
  ]
  node [
    id 687
    label "sekretariat"
  ]
  node [
    id 688
    label "ideologia"
  ]
  node [
    id 689
    label "lesson"
  ]
  node [
    id 690
    label "niepokalanki"
  ]
  node [
    id 691
    label "siedziba"
  ]
  node [
    id 692
    label "szkolenie"
  ]
  node [
    id 693
    label "kara"
  ]
  node [
    id 694
    label "troch&#281;"
  ]
  node [
    id 695
    label "dawny"
  ]
  node [
    id 696
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 697
    label "eksprezydent"
  ]
  node [
    id 698
    label "partner"
  ]
  node [
    id 699
    label "rozw&#243;d"
  ]
  node [
    id 700
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 701
    label "wcze&#347;niejszy"
  ]
  node [
    id 702
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 703
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 704
    label "pracownik"
  ]
  node [
    id 705
    label "przedsi&#281;biorca"
  ]
  node [
    id 706
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 707
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 708
    label "kolaborator"
  ]
  node [
    id 709
    label "prowadzi&#263;"
  ]
  node [
    id 710
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 711
    label "sp&#243;lnik"
  ]
  node [
    id 712
    label "aktor"
  ]
  node [
    id 713
    label "uczestniczenie"
  ]
  node [
    id 714
    label "przestarza&#322;y"
  ]
  node [
    id 715
    label "odleg&#322;y"
  ]
  node [
    id 716
    label "przesz&#322;y"
  ]
  node [
    id 717
    label "od_dawna"
  ]
  node [
    id 718
    label "poprzedni"
  ]
  node [
    id 719
    label "dawno"
  ]
  node [
    id 720
    label "d&#322;ugoletni"
  ]
  node [
    id 721
    label "anachroniczny"
  ]
  node [
    id 722
    label "dawniej"
  ]
  node [
    id 723
    label "niegdysiejszy"
  ]
  node [
    id 724
    label "kombatant"
  ]
  node [
    id 725
    label "stary"
  ]
  node [
    id 726
    label "wcze&#347;niej"
  ]
  node [
    id 727
    label "rozstanie"
  ]
  node [
    id 728
    label "ekspartner"
  ]
  node [
    id 729
    label "rozbita_rodzina"
  ]
  node [
    id 730
    label "uniewa&#380;nienie"
  ]
  node [
    id 731
    label "separation"
  ]
  node [
    id 732
    label "prezydent"
  ]
  node [
    id 733
    label "nisko"
  ]
  node [
    id 734
    label "pomierny"
  ]
  node [
    id 735
    label "obni&#380;anie"
  ]
  node [
    id 736
    label "uni&#380;ony"
  ]
  node [
    id 737
    label "po&#347;ledni"
  ]
  node [
    id 738
    label "obni&#380;enie"
  ]
  node [
    id 739
    label "gorszy"
  ]
  node [
    id 740
    label "pospolity"
  ]
  node [
    id 741
    label "nieznacznie"
  ]
  node [
    id 742
    label "drobnostkowy"
  ]
  node [
    id 743
    label "pospolicie"
  ]
  node [
    id 744
    label "zwyczajny"
  ]
  node [
    id 745
    label "wsp&#243;lny"
  ]
  node [
    id 746
    label "jak_ps&#243;w"
  ]
  node [
    id 747
    label "niewyszukany"
  ]
  node [
    id 748
    label "pogorszenie_si&#281;"
  ]
  node [
    id 749
    label "pogarszanie_si&#281;"
  ]
  node [
    id 750
    label "pogorszenie"
  ]
  node [
    id 751
    label "uni&#380;enie"
  ]
  node [
    id 752
    label "skromny"
  ]
  node [
    id 753
    label "grzeczny"
  ]
  node [
    id 754
    label "wstydliwie"
  ]
  node [
    id 755
    label "g&#322;upi"
  ]
  node [
    id 756
    label "nie&#347;mia&#322;y"
  ]
  node [
    id 757
    label "blisko"
  ]
  node [
    id 758
    label "znajomy"
  ]
  node [
    id 759
    label "zwi&#261;zany"
  ]
  node [
    id 760
    label "silny"
  ]
  node [
    id 761
    label "zbli&#380;enie"
  ]
  node [
    id 762
    label "kr&#243;tki"
  ]
  node [
    id 763
    label "oddalony"
  ]
  node [
    id 764
    label "dok&#322;adny"
  ]
  node [
    id 765
    label "nieodleg&#322;y"
  ]
  node [
    id 766
    label "przysz&#322;y"
  ]
  node [
    id 767
    label "gotowy"
  ]
  node [
    id 768
    label "ja&#322;owy"
  ]
  node [
    id 769
    label "marnie"
  ]
  node [
    id 770
    label "nieskuteczny"
  ]
  node [
    id 771
    label "kiepski"
  ]
  node [
    id 772
    label "kiepsko"
  ]
  node [
    id 773
    label "nadaremnie"
  ]
  node [
    id 774
    label "z&#322;y"
  ]
  node [
    id 775
    label "nietrwa&#322;y"
  ]
  node [
    id 776
    label "mizerny"
  ]
  node [
    id 777
    label "delikatny"
  ]
  node [
    id 778
    label "niezdrowy"
  ]
  node [
    id 779
    label "nieumiej&#281;tny"
  ]
  node [
    id 780
    label "s&#322;abo"
  ]
  node [
    id 781
    label "lura"
  ]
  node [
    id 782
    label "nieudany"
  ]
  node [
    id 783
    label "s&#322;abowity"
  ]
  node [
    id 784
    label "zawodny"
  ]
  node [
    id 785
    label "&#322;agodny"
  ]
  node [
    id 786
    label "md&#322;y"
  ]
  node [
    id 787
    label "niedoskona&#322;y"
  ]
  node [
    id 788
    label "przemijaj&#261;cy"
  ]
  node [
    id 789
    label "niemocny"
  ]
  node [
    id 790
    label "niefajny"
  ]
  node [
    id 791
    label "po&#347;lednio"
  ]
  node [
    id 792
    label "vilely"
  ]
  node [
    id 793
    label "despicably"
  ]
  node [
    id 794
    label "n&#281;dzny"
  ]
  node [
    id 795
    label "sm&#281;tnie"
  ]
  node [
    id 796
    label "biednie"
  ]
  node [
    id 797
    label "powodowanie"
  ]
  node [
    id 798
    label "zmniejszanie"
  ]
  node [
    id 799
    label "ni&#380;szy"
  ]
  node [
    id 800
    label "brzmienie"
  ]
  node [
    id 801
    label "zabrzmienie"
  ]
  node [
    id 802
    label "kszta&#322;t"
  ]
  node [
    id 803
    label "zmniejszenie"
  ]
  node [
    id 804
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 805
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 806
    label "suspension"
  ]
  node [
    id 807
    label "pad&#243;&#322;"
  ]
  node [
    id 808
    label "snub"
  ]
  node [
    id 809
    label "mierny"
  ]
  node [
    id 810
    label "&#347;redni"
  ]
  node [
    id 811
    label "lichy"
  ]
  node [
    id 812
    label "pomiarowy"
  ]
  node [
    id 813
    label "better"
  ]
  node [
    id 814
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 815
    label "ascend"
  ]
  node [
    id 816
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 817
    label "zabawka"
  ]
  node [
    id 818
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 819
    label "urwis"
  ]
  node [
    id 820
    label "catapult"
  ]
  node [
    id 821
    label "bro&#324;"
  ]
  node [
    id 822
    label "amunicja"
  ]
  node [
    id 823
    label "karta_przetargowa"
  ]
  node [
    id 824
    label "rozbroi&#263;"
  ]
  node [
    id 825
    label "rozbrojenie"
  ]
  node [
    id 826
    label "osprz&#281;t"
  ]
  node [
    id 827
    label "uzbrojenie"
  ]
  node [
    id 828
    label "przyrz&#261;d"
  ]
  node [
    id 829
    label "rozbrajanie"
  ]
  node [
    id 830
    label "rozbraja&#263;"
  ]
  node [
    id 831
    label "or&#281;&#380;"
  ]
  node [
    id 832
    label "narz&#281;dzie"
  ]
  node [
    id 833
    label "bawid&#322;o"
  ]
  node [
    id 834
    label "frisbee"
  ]
  node [
    id 835
    label "smoczek"
  ]
  node [
    id 836
    label "dziecko"
  ]
  node [
    id 837
    label "hycel"
  ]
  node [
    id 838
    label "basa&#322;yk"
  ]
  node [
    id 839
    label "smok"
  ]
  node [
    id 840
    label "psotnik"
  ]
  node [
    id 841
    label "nicpo&#324;"
  ]
  node [
    id 842
    label "abstrakcja"
  ]
  node [
    id 843
    label "chemikalia"
  ]
  node [
    id 844
    label "substancja"
  ]
  node [
    id 845
    label "model"
  ]
  node [
    id 846
    label "nature"
  ]
  node [
    id 847
    label "po&#322;o&#380;enie"
  ]
  node [
    id 848
    label "sprawa"
  ]
  node [
    id 849
    label "ust&#281;p"
  ]
  node [
    id 850
    label "plan"
  ]
  node [
    id 851
    label "obiekt_matematyczny"
  ]
  node [
    id 852
    label "problemat"
  ]
  node [
    id 853
    label "plamka"
  ]
  node [
    id 854
    label "stopie&#324;_pisma"
  ]
  node [
    id 855
    label "jednostka"
  ]
  node [
    id 856
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 857
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 858
    label "mark"
  ]
  node [
    id 859
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 860
    label "prosta"
  ]
  node [
    id 861
    label "problematyka"
  ]
  node [
    id 862
    label "zapunktowa&#263;"
  ]
  node [
    id 863
    label "podpunkt"
  ]
  node [
    id 864
    label "wojsko"
  ]
  node [
    id 865
    label "kres"
  ]
  node [
    id 866
    label "przestrze&#324;"
  ]
  node [
    id 867
    label "point"
  ]
  node [
    id 868
    label "pozycja"
  ]
  node [
    id 869
    label "warunek_lokalowy"
  ]
  node [
    id 870
    label "plac"
  ]
  node [
    id 871
    label "location"
  ]
  node [
    id 872
    label "uwaga"
  ]
  node [
    id 873
    label "status"
  ]
  node [
    id 874
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 875
    label "cia&#322;o"
  ]
  node [
    id 876
    label "praca"
  ]
  node [
    id 877
    label "przenikanie"
  ]
  node [
    id 878
    label "byt"
  ]
  node [
    id 879
    label "materia"
  ]
  node [
    id 880
    label "temperatura_krytyczna"
  ]
  node [
    id 881
    label "przenika&#263;"
  ]
  node [
    id 882
    label "smolisty"
  ]
  node [
    id 883
    label "proces_my&#347;lowy"
  ]
  node [
    id 884
    label "abstractedness"
  ]
  node [
    id 885
    label "abstraction"
  ]
  node [
    id 886
    label "obraz"
  ]
  node [
    id 887
    label "sytuacja"
  ]
  node [
    id 888
    label "spalenie"
  ]
  node [
    id 889
    label "spalanie"
  ]
  node [
    id 890
    label "sta&#263;_si&#281;"
  ]
  node [
    id 891
    label "appoint"
  ]
  node [
    id 892
    label "oblat"
  ]
  node [
    id 893
    label "ustali&#263;"
  ]
  node [
    id 894
    label "post&#261;pi&#263;"
  ]
  node [
    id 895
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 896
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 897
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 898
    label "zorganizowa&#263;"
  ]
  node [
    id 899
    label "wystylizowa&#263;"
  ]
  node [
    id 900
    label "cause"
  ]
  node [
    id 901
    label "przerobi&#263;"
  ]
  node [
    id 902
    label "nabra&#263;"
  ]
  node [
    id 903
    label "make"
  ]
  node [
    id 904
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 905
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 906
    label "wydali&#263;"
  ]
  node [
    id 907
    label "put"
  ]
  node [
    id 908
    label "zdecydowa&#263;"
  ]
  node [
    id 909
    label "bind"
  ]
  node [
    id 910
    label "umocni&#263;"
  ]
  node [
    id 911
    label "spowodowa&#263;"
  ]
  node [
    id 912
    label "unwrap"
  ]
  node [
    id 913
    label "andrut"
  ]
  node [
    id 914
    label "nowicjusz"
  ]
  node [
    id 915
    label "&#347;wiecki"
  ]
  node [
    id 916
    label "oblaci"
  ]
  node [
    id 917
    label "miljon"
  ]
  node [
    id 918
    label "ba&#324;ka"
  ]
  node [
    id 919
    label "liczba"
  ]
  node [
    id 920
    label "pierwiastek"
  ]
  node [
    id 921
    label "rozmiar"
  ]
  node [
    id 922
    label "wyra&#380;enie"
  ]
  node [
    id 923
    label "kwadrat_magiczny"
  ]
  node [
    id 924
    label "gourd"
  ]
  node [
    id 925
    label "naczynie"
  ]
  node [
    id 926
    label "obiekt_naturalny"
  ]
  node [
    id 927
    label "pojemnik"
  ]
  node [
    id 928
    label "niedostateczny"
  ]
  node [
    id 929
    label "&#322;eb"
  ]
  node [
    id 930
    label "mak&#243;wka"
  ]
  node [
    id 931
    label "bubble"
  ]
  node [
    id 932
    label "czaszka"
  ]
  node [
    id 933
    label "dynia"
  ]
  node [
    id 934
    label "jednostka_monetarna"
  ]
  node [
    id 935
    label "wspania&#322;y"
  ]
  node [
    id 936
    label "metaliczny"
  ]
  node [
    id 937
    label "Polska"
  ]
  node [
    id 938
    label "szlachetny"
  ]
  node [
    id 939
    label "kochany"
  ]
  node [
    id 940
    label "doskona&#322;y"
  ]
  node [
    id 941
    label "grosz"
  ]
  node [
    id 942
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 943
    label "poz&#322;ocenie"
  ]
  node [
    id 944
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 945
    label "utytu&#322;owany"
  ]
  node [
    id 946
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 947
    label "z&#322;ocenie"
  ]
  node [
    id 948
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 949
    label "prominentny"
  ]
  node [
    id 950
    label "znany"
  ]
  node [
    id 951
    label "wybitny"
  ]
  node [
    id 952
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 953
    label "naj"
  ]
  node [
    id 954
    label "&#347;wietny"
  ]
  node [
    id 955
    label "pe&#322;ny"
  ]
  node [
    id 956
    label "doskonale"
  ]
  node [
    id 957
    label "szlachetnie"
  ]
  node [
    id 958
    label "uczciwy"
  ]
  node [
    id 959
    label "zacny"
  ]
  node [
    id 960
    label "harmonijny"
  ]
  node [
    id 961
    label "gatunkowy"
  ]
  node [
    id 962
    label "pi&#281;kny"
  ]
  node [
    id 963
    label "dobry"
  ]
  node [
    id 964
    label "typowy"
  ]
  node [
    id 965
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 966
    label "metaloplastyczny"
  ]
  node [
    id 967
    label "metalicznie"
  ]
  node [
    id 968
    label "kochanek"
  ]
  node [
    id 969
    label "wybranek"
  ]
  node [
    id 970
    label "umi&#322;owany"
  ]
  node [
    id 971
    label "drogi"
  ]
  node [
    id 972
    label "kochanie"
  ]
  node [
    id 973
    label "wspaniale"
  ]
  node [
    id 974
    label "pomy&#347;lny"
  ]
  node [
    id 975
    label "pozytywny"
  ]
  node [
    id 976
    label "&#347;wietnie"
  ]
  node [
    id 977
    label "spania&#322;y"
  ]
  node [
    id 978
    label "och&#281;do&#380;ny"
  ]
  node [
    id 979
    label "warto&#347;ciowy"
  ]
  node [
    id 980
    label "zajebisty"
  ]
  node [
    id 981
    label "bogato"
  ]
  node [
    id 982
    label "typ_mongoloidalny"
  ]
  node [
    id 983
    label "kolorowy"
  ]
  node [
    id 984
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 985
    label "ciep&#322;y"
  ]
  node [
    id 986
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 987
    label "jasny"
  ]
  node [
    id 988
    label "groszak"
  ]
  node [
    id 989
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 990
    label "szyling_austryjacki"
  ]
  node [
    id 991
    label "moneta"
  ]
  node [
    id 992
    label "Mazowsze"
  ]
  node [
    id 993
    label "Pa&#322;uki"
  ]
  node [
    id 994
    label "Pomorze_Zachodnie"
  ]
  node [
    id 995
    label "Powi&#347;le"
  ]
  node [
    id 996
    label "Wolin"
  ]
  node [
    id 997
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 998
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 999
    label "So&#322;a"
  ]
  node [
    id 1000
    label "Unia_Europejska"
  ]
  node [
    id 1001
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1002
    label "Opolskie"
  ]
  node [
    id 1003
    label "Suwalszczyzna"
  ]
  node [
    id 1004
    label "Krajna"
  ]
  node [
    id 1005
    label "barwy_polskie"
  ]
  node [
    id 1006
    label "Nadbu&#380;e"
  ]
  node [
    id 1007
    label "Podlasie"
  ]
  node [
    id 1008
    label "Izera"
  ]
  node [
    id 1009
    label "Ma&#322;opolska"
  ]
  node [
    id 1010
    label "Warmia"
  ]
  node [
    id 1011
    label "Mazury"
  ]
  node [
    id 1012
    label "NATO"
  ]
  node [
    id 1013
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1014
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1015
    label "Lubelszczyzna"
  ]
  node [
    id 1016
    label "Kaczawa"
  ]
  node [
    id 1017
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1018
    label "Kielecczyzna"
  ]
  node [
    id 1019
    label "Lubuskie"
  ]
  node [
    id 1020
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1021
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1022
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1023
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1024
    label "Kujawy"
  ]
  node [
    id 1025
    label "Podkarpacie"
  ]
  node [
    id 1026
    label "Wielkopolska"
  ]
  node [
    id 1027
    label "Wis&#322;a"
  ]
  node [
    id 1028
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1029
    label "Bory_Tucholskie"
  ]
  node [
    id 1030
    label "z&#322;ocisty"
  ]
  node [
    id 1031
    label "powleczenie"
  ]
  node [
    id 1032
    label "zabarwienie"
  ]
  node [
    id 1033
    label "platerowanie"
  ]
  node [
    id 1034
    label "barwienie"
  ]
  node [
    id 1035
    label "gilt"
  ]
  node [
    id 1036
    label "plating"
  ]
  node [
    id 1037
    label "zdobienie"
  ]
  node [
    id 1038
    label "club"
  ]
  node [
    id 1039
    label "wiela"
  ]
  node [
    id 1040
    label "du&#380;y"
  ]
  node [
    id 1041
    label "du&#380;o"
  ]
  node [
    id 1042
    label "doros&#322;y"
  ]
  node [
    id 1043
    label "znaczny"
  ]
  node [
    id 1044
    label "niema&#322;o"
  ]
  node [
    id 1045
    label "rozwini&#281;ty"
  ]
  node [
    id 1046
    label "dorodny"
  ]
  node [
    id 1047
    label "wa&#380;ny"
  ]
  node [
    id 1048
    label "prawdziwy"
  ]
  node [
    id 1049
    label "obiecywa&#263;"
  ]
  node [
    id 1050
    label "podawa&#263;"
  ]
  node [
    id 1051
    label "poda&#263;"
  ]
  node [
    id 1052
    label "obieca&#263;"
  ]
  node [
    id 1053
    label "sign"
  ]
  node [
    id 1054
    label "bespeak"
  ]
  node [
    id 1055
    label "zapewni&#263;"
  ]
  node [
    id 1056
    label "harbinger"
  ]
  node [
    id 1057
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1058
    label "pledge"
  ]
  node [
    id 1059
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1060
    label "vow"
  ]
  node [
    id 1061
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1062
    label "poinformowa&#263;"
  ]
  node [
    id 1063
    label "translate"
  ]
  node [
    id 1064
    label "tenis"
  ]
  node [
    id 1065
    label "supply"
  ]
  node [
    id 1066
    label "da&#263;"
  ]
  node [
    id 1067
    label "ustawi&#263;"
  ]
  node [
    id 1068
    label "siatk&#243;wka"
  ]
  node [
    id 1069
    label "zagra&#263;"
  ]
  node [
    id 1070
    label "introduce"
  ]
  node [
    id 1071
    label "nafaszerowa&#263;"
  ]
  node [
    id 1072
    label "zaserwowa&#263;"
  ]
  node [
    id 1073
    label "deal"
  ]
  node [
    id 1074
    label "dawa&#263;"
  ]
  node [
    id 1075
    label "stawia&#263;"
  ]
  node [
    id 1076
    label "rozgrywa&#263;"
  ]
  node [
    id 1077
    label "kelner"
  ]
  node [
    id 1078
    label "cover"
  ]
  node [
    id 1079
    label "tender"
  ]
  node [
    id 1080
    label "faszerowa&#263;"
  ]
  node [
    id 1081
    label "serwowa&#263;"
  ]
  node [
    id 1082
    label "obroni&#263;"
  ]
  node [
    id 1083
    label "potrzyma&#263;"
  ]
  node [
    id 1084
    label "op&#322;aci&#263;"
  ]
  node [
    id 1085
    label "manewr"
  ]
  node [
    id 1086
    label "zdo&#322;a&#263;"
  ]
  node [
    id 1087
    label "podtrzyma&#263;"
  ]
  node [
    id 1088
    label "feed"
  ]
  node [
    id 1089
    label "przetrzyma&#263;"
  ]
  node [
    id 1090
    label "foster"
  ]
  node [
    id 1091
    label "preserve"
  ]
  node [
    id 1092
    label "zachowa&#263;"
  ]
  node [
    id 1093
    label "unie&#347;&#263;"
  ]
  node [
    id 1094
    label "manipulate"
  ]
  node [
    id 1095
    label "poradzi&#263;_sobie"
  ]
  node [
    id 1096
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1097
    label "zabra&#263;"
  ]
  node [
    id 1098
    label "go"
  ]
  node [
    id 1099
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 1100
    label "float"
  ]
  node [
    id 1101
    label "pom&#243;c"
  ]
  node [
    id 1102
    label "raise"
  ]
  node [
    id 1103
    label "zdole&#263;"
  ]
  node [
    id 1104
    label "tajemnica"
  ]
  node [
    id 1105
    label "pami&#281;&#263;"
  ]
  node [
    id 1106
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 1107
    label "zdyscyplinowanie"
  ]
  node [
    id 1108
    label "post"
  ]
  node [
    id 1109
    label "przechowa&#263;"
  ]
  node [
    id 1110
    label "dieta"
  ]
  node [
    id 1111
    label "bury"
  ]
  node [
    id 1112
    label "pocieszy&#263;"
  ]
  node [
    id 1113
    label "support"
  ]
  node [
    id 1114
    label "wywalczy&#263;"
  ]
  node [
    id 1115
    label "ochroni&#263;"
  ]
  node [
    id 1116
    label "fend"
  ]
  node [
    id 1117
    label "udowodni&#263;"
  ]
  node [
    id 1118
    label "zatrzyma&#263;"
  ]
  node [
    id 1119
    label "give_birth"
  ]
  node [
    id 1120
    label "stay"
  ]
  node [
    id 1121
    label "wytrzyma&#263;"
  ]
  node [
    id 1122
    label "pokona&#263;"
  ]
  node [
    id 1123
    label "digest"
  ]
  node [
    id 1124
    label "hold"
  ]
  node [
    id 1125
    label "hodowla"
  ]
  node [
    id 1126
    label "zmusi&#263;"
  ]
  node [
    id 1127
    label "clasp"
  ]
  node [
    id 1128
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 1129
    label "utrzymywanie"
  ]
  node [
    id 1130
    label "move"
  ]
  node [
    id 1131
    label "wydarzenie"
  ]
  node [
    id 1132
    label "movement"
  ]
  node [
    id 1133
    label "posuni&#281;cie"
  ]
  node [
    id 1134
    label "myk"
  ]
  node [
    id 1135
    label "taktyka"
  ]
  node [
    id 1136
    label "ruch"
  ]
  node [
    id 1137
    label "maneuver"
  ]
  node [
    id 1138
    label "utrzymanie"
  ]
  node [
    id 1139
    label "bycie"
  ]
  node [
    id 1140
    label "entity"
  ]
  node [
    id 1141
    label "subsystencja"
  ]
  node [
    id 1142
    label "egzystencja"
  ]
  node [
    id 1143
    label "wy&#380;ywienie"
  ]
  node [
    id 1144
    label "ontologicznie"
  ]
  node [
    id 1145
    label "potencja"
  ]
  node [
    id 1146
    label "p&#322;aszczyzna"
  ]
  node [
    id 1147
    label "punkt_widzenia"
  ]
  node [
    id 1148
    label "wyk&#322;adnik"
  ]
  node [
    id 1149
    label "faza"
  ]
  node [
    id 1150
    label "szczebel"
  ]
  node [
    id 1151
    label "budynek"
  ]
  node [
    id 1152
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1153
    label "ranga"
  ]
  node [
    id 1154
    label "wymiar"
  ]
  node [
    id 1155
    label "&#347;ciana"
  ]
  node [
    id 1156
    label "surface"
  ]
  node [
    id 1157
    label "zakres"
  ]
  node [
    id 1158
    label "kwadrant"
  ]
  node [
    id 1159
    label "degree"
  ]
  node [
    id 1160
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 1161
    label "powierzchnia"
  ]
  node [
    id 1162
    label "ukszta&#322;towanie"
  ]
  node [
    id 1163
    label "p&#322;aszczak"
  ]
  node [
    id 1164
    label "przenocowanie"
  ]
  node [
    id 1165
    label "pora&#380;ka"
  ]
  node [
    id 1166
    label "nak&#322;adzenie"
  ]
  node [
    id 1167
    label "pouk&#322;adanie"
  ]
  node [
    id 1168
    label "pokrycie"
  ]
  node [
    id 1169
    label "zepsucie"
  ]
  node [
    id 1170
    label "ustawienie"
  ]
  node [
    id 1171
    label "trim"
  ]
  node [
    id 1172
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 1173
    label "ugoszczenie"
  ]
  node [
    id 1174
    label "le&#380;enie"
  ]
  node [
    id 1175
    label "adres"
  ]
  node [
    id 1176
    label "zbudowanie"
  ]
  node [
    id 1177
    label "umieszczenie"
  ]
  node [
    id 1178
    label "reading"
  ]
  node [
    id 1179
    label "zabicie"
  ]
  node [
    id 1180
    label "wygranie"
  ]
  node [
    id 1181
    label "presentation"
  ]
  node [
    id 1182
    label "le&#380;e&#263;"
  ]
  node [
    id 1183
    label "tallness"
  ]
  node [
    id 1184
    label "altitude"
  ]
  node [
    id 1185
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 1186
    label "odcinek"
  ]
  node [
    id 1187
    label "k&#261;t"
  ]
  node [
    id 1188
    label "wielko&#347;&#263;"
  ]
  node [
    id 1189
    label "sum"
  ]
  node [
    id 1190
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1191
    label "pot&#281;ga"
  ]
  node [
    id 1192
    label "wska&#378;nik"
  ]
  node [
    id 1193
    label "exponent"
  ]
  node [
    id 1194
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1195
    label "warto&#347;&#263;"
  ]
  node [
    id 1196
    label "quality"
  ]
  node [
    id 1197
    label "co&#347;"
  ]
  node [
    id 1198
    label "syf"
  ]
  node [
    id 1199
    label "stopie&#324;"
  ]
  node [
    id 1200
    label "drabina"
  ]
  node [
    id 1201
    label "gradation"
  ]
  node [
    id 1202
    label "przebieg"
  ]
  node [
    id 1203
    label "studia"
  ]
  node [
    id 1204
    label "linia"
  ]
  node [
    id 1205
    label "bok"
  ]
  node [
    id 1206
    label "skr&#281;canie"
  ]
  node [
    id 1207
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1208
    label "orientowanie"
  ]
  node [
    id 1209
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1210
    label "zorientowanie"
  ]
  node [
    id 1211
    label "ty&#322;"
  ]
  node [
    id 1212
    label "zorientowa&#263;"
  ]
  node [
    id 1213
    label "g&#243;ra"
  ]
  node [
    id 1214
    label "orientowa&#263;"
  ]
  node [
    id 1215
    label "orientacja"
  ]
  node [
    id 1216
    label "prz&#243;d"
  ]
  node [
    id 1217
    label "skr&#281;cenie"
  ]
  node [
    id 1218
    label "Rzym_Zachodni"
  ]
  node [
    id 1219
    label "whole"
  ]
  node [
    id 1220
    label "element"
  ]
  node [
    id 1221
    label "Rzym_Wschodni"
  ]
  node [
    id 1222
    label "coil"
  ]
  node [
    id 1223
    label "zjawisko"
  ]
  node [
    id 1224
    label "fotoelement"
  ]
  node [
    id 1225
    label "komutowanie"
  ]
  node [
    id 1226
    label "stan_skupienia"
  ]
  node [
    id 1227
    label "nastr&#243;j"
  ]
  node [
    id 1228
    label "przerywacz"
  ]
  node [
    id 1229
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1230
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1231
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1232
    label "obsesja"
  ]
  node [
    id 1233
    label "dw&#243;jnik"
  ]
  node [
    id 1234
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1235
    label "okres"
  ]
  node [
    id 1236
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1237
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1238
    label "przew&#243;d"
  ]
  node [
    id 1239
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1240
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1241
    label "obw&#243;d"
  ]
  node [
    id 1242
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1243
    label "komutowa&#263;"
  ]
  node [
    id 1244
    label "numer"
  ]
  node [
    id 1245
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 1246
    label "stopie&#324;_s&#322;u&#380;bowy"
  ]
  node [
    id 1247
    label "balkon"
  ]
  node [
    id 1248
    label "budowla"
  ]
  node [
    id 1249
    label "pod&#322;oga"
  ]
  node [
    id 1250
    label "kondygnacja"
  ]
  node [
    id 1251
    label "skrzyd&#322;o"
  ]
  node [
    id 1252
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 1253
    label "dach"
  ]
  node [
    id 1254
    label "strop"
  ]
  node [
    id 1255
    label "klatka_schodowa"
  ]
  node [
    id 1256
    label "przedpro&#380;e"
  ]
  node [
    id 1257
    label "Pentagon"
  ]
  node [
    id 1258
    label "alkierz"
  ]
  node [
    id 1259
    label "front"
  ]
  node [
    id 1260
    label "Leszek"
  ]
  node [
    id 1261
    label "&#346;witalski"
  ]
  node [
    id 1262
    label "Bogaczowice"
  ]
  node [
    id 1263
    label "Ryszarda"
  ]
  node [
    id 1264
    label "Legutko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 634
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 331
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 334
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 442
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 64
  ]
  edge [
    source 17
    target 441
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 447
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 358
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 434
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 435
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 695
  ]
  edge [
    source 19
    target 696
  ]
  edge [
    source 19
    target 697
  ]
  edge [
    source 19
    target 698
  ]
  edge [
    source 19
    target 699
  ]
  edge [
    source 19
    target 700
  ]
  edge [
    source 19
    target 701
  ]
  edge [
    source 19
    target 702
  ]
  edge [
    source 19
    target 703
  ]
  edge [
    source 19
    target 704
  ]
  edge [
    source 19
    target 705
  ]
  edge [
    source 19
    target 327
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 708
  ]
  edge [
    source 19
    target 709
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 712
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 714
  ]
  edge [
    source 19
    target 715
  ]
  edge [
    source 19
    target 716
  ]
  edge [
    source 19
    target 717
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 719
  ]
  edge [
    source 19
    target 720
  ]
  edge [
    source 19
    target 721
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 725
  ]
  edge [
    source 19
    target 726
  ]
  edge [
    source 19
    target 727
  ]
  edge [
    source 19
    target 728
  ]
  edge [
    source 19
    target 729
  ]
  edge [
    source 19
    target 730
  ]
  edge [
    source 19
    target 731
  ]
  edge [
    source 19
    target 732
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 617
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 734
  ]
  edge [
    source 20
    target 626
  ]
  edge [
    source 20
    target 374
  ]
  edge [
    source 20
    target 627
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 736
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 630
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 631
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 20
    target 615
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 614
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 745
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 747
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 327
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 716
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 761
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 20
    target 763
  ]
  edge [
    source 20
    target 764
  ]
  edge [
    source 20
    target 765
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 772
  ]
  edge [
    source 20
    target 773
  ]
  edge [
    source 20
    target 774
  ]
  edge [
    source 20
    target 775
  ]
  edge [
    source 20
    target 776
  ]
  edge [
    source 20
    target 777
  ]
  edge [
    source 20
    target 778
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 780
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 783
  ]
  edge [
    source 20
    target 784
  ]
  edge [
    source 20
    target 785
  ]
  edge [
    source 20
    target 786
  ]
  edge [
    source 20
    target 787
  ]
  edge [
    source 20
    target 788
  ]
  edge [
    source 20
    target 789
  ]
  edge [
    source 20
    target 790
  ]
  edge [
    source 20
    target 624
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 628
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 616
  ]
  edge [
    source 20
    target 621
  ]
  edge [
    source 20
    target 791
  ]
  edge [
    source 20
    target 792
  ]
  edge [
    source 20
    target 793
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 795
  ]
  edge [
    source 20
    target 796
  ]
  edge [
    source 20
    target 797
  ]
  edge [
    source 20
    target 798
  ]
  edge [
    source 20
    target 799
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 801
  ]
  edge [
    source 20
    target 802
  ]
  edge [
    source 20
    target 654
  ]
  edge [
    source 20
    target 803
  ]
  edge [
    source 20
    target 368
  ]
  edge [
    source 20
    target 591
  ]
  edge [
    source 20
    target 804
  ]
  edge [
    source 20
    target 805
  ]
  edge [
    source 20
    target 806
  ]
  edge [
    source 20
    target 807
  ]
  edge [
    source 20
    target 232
  ]
  edge [
    source 20
    target 808
  ]
  edge [
    source 20
    target 809
  ]
  edge [
    source 20
    target 810
  ]
  edge [
    source 20
    target 811
  ]
  edge [
    source 20
    target 618
  ]
  edge [
    source 20
    target 812
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 813
  ]
  edge [
    source 23
    target 814
  ]
  edge [
    source 23
    target 815
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 816
  ]
  edge [
    source 24
    target 817
  ]
  edge [
    source 24
    target 818
  ]
  edge [
    source 24
    target 819
  ]
  edge [
    source 24
    target 820
  ]
  edge [
    source 24
    target 821
  ]
  edge [
    source 24
    target 822
  ]
  edge [
    source 24
    target 823
  ]
  edge [
    source 24
    target 824
  ]
  edge [
    source 24
    target 825
  ]
  edge [
    source 24
    target 826
  ]
  edge [
    source 24
    target 827
  ]
  edge [
    source 24
    target 828
  ]
  edge [
    source 24
    target 829
  ]
  edge [
    source 24
    target 830
  ]
  edge [
    source 24
    target 831
  ]
  edge [
    source 24
    target 832
  ]
  edge [
    source 24
    target 427
  ]
  edge [
    source 24
    target 833
  ]
  edge [
    source 24
    target 834
  ]
  edge [
    source 24
    target 835
  ]
  edge [
    source 24
    target 836
  ]
  edge [
    source 24
    target 837
  ]
  edge [
    source 24
    target 838
  ]
  edge [
    source 24
    target 839
  ]
  edge [
    source 24
    target 840
  ]
  edge [
    source 24
    target 841
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 597
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 26
    target 591
  ]
  edge [
    source 26
    target 842
  ]
  edge [
    source 26
    target 62
  ]
  edge [
    source 26
    target 843
  ]
  edge [
    source 26
    target 844
  ]
  edge [
    source 26
    target 95
  ]
  edge [
    source 26
    target 96
  ]
  edge [
    source 26
    target 97
  ]
  edge [
    source 26
    target 98
  ]
  edge [
    source 26
    target 99
  ]
  edge [
    source 26
    target 100
  ]
  edge [
    source 26
    target 101
  ]
  edge [
    source 26
    target 102
  ]
  edge [
    source 26
    target 103
  ]
  edge [
    source 26
    target 104
  ]
  edge [
    source 26
    target 105
  ]
  edge [
    source 26
    target 106
  ]
  edge [
    source 26
    target 107
  ]
  edge [
    source 26
    target 108
  ]
  edge [
    source 26
    target 109
  ]
  edge [
    source 26
    target 110
  ]
  edge [
    source 26
    target 111
  ]
  edge [
    source 26
    target 112
  ]
  edge [
    source 26
    target 113
  ]
  edge [
    source 26
    target 114
  ]
  edge [
    source 26
    target 115
  ]
  edge [
    source 26
    target 116
  ]
  edge [
    source 26
    target 117
  ]
  edge [
    source 26
    target 118
  ]
  edge [
    source 26
    target 119
  ]
  edge [
    source 26
    target 120
  ]
  edge [
    source 26
    target 121
  ]
  edge [
    source 26
    target 122
  ]
  edge [
    source 26
    target 123
  ]
  edge [
    source 26
    target 124
  ]
  edge [
    source 26
    target 125
  ]
  edge [
    source 26
    target 126
  ]
  edge [
    source 26
    target 127
  ]
  edge [
    source 26
    target 128
  ]
  edge [
    source 26
    target 129
  ]
  edge [
    source 26
    target 130
  ]
  edge [
    source 26
    target 131
  ]
  edge [
    source 26
    target 845
  ]
  edge [
    source 26
    target 832
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 26
    target 570
  ]
  edge [
    source 26
    target 846
  ]
  edge [
    source 26
    target 847
  ]
  edge [
    source 26
    target 848
  ]
  edge [
    source 26
    target 849
  ]
  edge [
    source 26
    target 850
  ]
  edge [
    source 26
    target 851
  ]
  edge [
    source 26
    target 852
  ]
  edge [
    source 26
    target 853
  ]
  edge [
    source 26
    target 854
  ]
  edge [
    source 26
    target 855
  ]
  edge [
    source 26
    target 856
  ]
  edge [
    source 26
    target 857
  ]
  edge [
    source 26
    target 858
  ]
  edge [
    source 26
    target 859
  ]
  edge [
    source 26
    target 860
  ]
  edge [
    source 26
    target 861
  ]
  edge [
    source 26
    target 448
  ]
  edge [
    source 26
    target 862
  ]
  edge [
    source 26
    target 863
  ]
  edge [
    source 26
    target 864
  ]
  edge [
    source 26
    target 865
  ]
  edge [
    source 26
    target 866
  ]
  edge [
    source 26
    target 867
  ]
  edge [
    source 26
    target 868
  ]
  edge [
    source 26
    target 869
  ]
  edge [
    source 26
    target 870
  ]
  edge [
    source 26
    target 871
  ]
  edge [
    source 26
    target 872
  ]
  edge [
    source 26
    target 873
  ]
  edge [
    source 26
    target 874
  ]
  edge [
    source 26
    target 875
  ]
  edge [
    source 26
    target 495
  ]
  edge [
    source 26
    target 654
  ]
  edge [
    source 26
    target 876
  ]
  edge [
    source 26
    target 877
  ]
  edge [
    source 26
    target 878
  ]
  edge [
    source 26
    target 879
  ]
  edge [
    source 26
    target 84
  ]
  edge [
    source 26
    target 880
  ]
  edge [
    source 26
    target 881
  ]
  edge [
    source 26
    target 882
  ]
  edge [
    source 26
    target 883
  ]
  edge [
    source 26
    target 884
  ]
  edge [
    source 26
    target 885
  ]
  edge [
    source 26
    target 384
  ]
  edge [
    source 26
    target 886
  ]
  edge [
    source 26
    target 887
  ]
  edge [
    source 26
    target 888
  ]
  edge [
    source 26
    target 889
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 890
  ]
  edge [
    source 27
    target 891
  ]
  edge [
    source 27
    target 186
  ]
  edge [
    source 27
    target 892
  ]
  edge [
    source 27
    target 893
  ]
  edge [
    source 27
    target 894
  ]
  edge [
    source 27
    target 895
  ]
  edge [
    source 27
    target 896
  ]
  edge [
    source 27
    target 897
  ]
  edge [
    source 27
    target 898
  ]
  edge [
    source 27
    target 899
  ]
  edge [
    source 27
    target 900
  ]
  edge [
    source 27
    target 901
  ]
  edge [
    source 27
    target 902
  ]
  edge [
    source 27
    target 903
  ]
  edge [
    source 27
    target 904
  ]
  edge [
    source 27
    target 905
  ]
  edge [
    source 27
    target 906
  ]
  edge [
    source 27
    target 907
  ]
  edge [
    source 27
    target 908
  ]
  edge [
    source 27
    target 909
  ]
  edge [
    source 27
    target 910
  ]
  edge [
    source 27
    target 911
  ]
  edge [
    source 27
    target 912
  ]
  edge [
    source 27
    target 913
  ]
  edge [
    source 27
    target 836
  ]
  edge [
    source 27
    target 914
  ]
  edge [
    source 27
    target 915
  ]
  edge [
    source 27
    target 347
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 916
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 917
  ]
  edge [
    source 29
    target 918
  ]
  edge [
    source 29
    target 919
  ]
  edge [
    source 29
    target 354
  ]
  edge [
    source 29
    target 920
  ]
  edge [
    source 29
    target 921
  ]
  edge [
    source 29
    target 922
  ]
  edge [
    source 29
    target 384
  ]
  edge [
    source 29
    target 363
  ]
  edge [
    source 29
    target 495
  ]
  edge [
    source 29
    target 109
  ]
  edge [
    source 29
    target 64
  ]
  edge [
    source 29
    target 923
  ]
  edge [
    source 29
    target 123
  ]
  edge [
    source 29
    target 924
  ]
  edge [
    source 29
    target 832
  ]
  edge [
    source 29
    target 246
  ]
  edge [
    source 29
    target 925
  ]
  edge [
    source 29
    target 926
  ]
  edge [
    source 29
    target 927
  ]
  edge [
    source 29
    target 928
  ]
  edge [
    source 29
    target 929
  ]
  edge [
    source 29
    target 930
  ]
  edge [
    source 29
    target 931
  ]
  edge [
    source 29
    target 932
  ]
  edge [
    source 29
    target 933
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 934
  ]
  edge [
    source 30
    target 935
  ]
  edge [
    source 30
    target 936
  ]
  edge [
    source 30
    target 937
  ]
  edge [
    source 30
    target 938
  ]
  edge [
    source 30
    target 939
  ]
  edge [
    source 30
    target 940
  ]
  edge [
    source 30
    target 941
  ]
  edge [
    source 30
    target 942
  ]
  edge [
    source 30
    target 943
  ]
  edge [
    source 30
    target 944
  ]
  edge [
    source 30
    target 945
  ]
  edge [
    source 30
    target 946
  ]
  edge [
    source 30
    target 947
  ]
  edge [
    source 30
    target 948
  ]
  edge [
    source 30
    target 949
  ]
  edge [
    source 30
    target 950
  ]
  edge [
    source 30
    target 951
  ]
  edge [
    source 30
    target 952
  ]
  edge [
    source 30
    target 953
  ]
  edge [
    source 30
    target 954
  ]
  edge [
    source 30
    target 955
  ]
  edge [
    source 30
    target 956
  ]
  edge [
    source 30
    target 957
  ]
  edge [
    source 30
    target 958
  ]
  edge [
    source 30
    target 959
  ]
  edge [
    source 30
    target 960
  ]
  edge [
    source 30
    target 961
  ]
  edge [
    source 30
    target 962
  ]
  edge [
    source 30
    target 963
  ]
  edge [
    source 30
    target 964
  ]
  edge [
    source 30
    target 965
  ]
  edge [
    source 30
    target 966
  ]
  edge [
    source 30
    target 967
  ]
  edge [
    source 30
    target 968
  ]
  edge [
    source 30
    target 969
  ]
  edge [
    source 30
    target 970
  ]
  edge [
    source 30
    target 971
  ]
  edge [
    source 30
    target 972
  ]
  edge [
    source 30
    target 973
  ]
  edge [
    source 30
    target 974
  ]
  edge [
    source 30
    target 975
  ]
  edge [
    source 30
    target 976
  ]
  edge [
    source 30
    target 977
  ]
  edge [
    source 30
    target 978
  ]
  edge [
    source 30
    target 979
  ]
  edge [
    source 30
    target 980
  ]
  edge [
    source 30
    target 981
  ]
  edge [
    source 30
    target 982
  ]
  edge [
    source 30
    target 983
  ]
  edge [
    source 30
    target 984
  ]
  edge [
    source 30
    target 985
  ]
  edge [
    source 30
    target 986
  ]
  edge [
    source 30
    target 987
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 988
  ]
  edge [
    source 30
    target 989
  ]
  edge [
    source 30
    target 990
  ]
  edge [
    source 30
    target 991
  ]
  edge [
    source 30
    target 992
  ]
  edge [
    source 30
    target 993
  ]
  edge [
    source 30
    target 994
  ]
  edge [
    source 30
    target 995
  ]
  edge [
    source 30
    target 996
  ]
  edge [
    source 30
    target 997
  ]
  edge [
    source 30
    target 998
  ]
  edge [
    source 30
    target 999
  ]
  edge [
    source 30
    target 1000
  ]
  edge [
    source 30
    target 1001
  ]
  edge [
    source 30
    target 1002
  ]
  edge [
    source 30
    target 1003
  ]
  edge [
    source 30
    target 1004
  ]
  edge [
    source 30
    target 1005
  ]
  edge [
    source 30
    target 1006
  ]
  edge [
    source 30
    target 1007
  ]
  edge [
    source 30
    target 1008
  ]
  edge [
    source 30
    target 1009
  ]
  edge [
    source 30
    target 1010
  ]
  edge [
    source 30
    target 1011
  ]
  edge [
    source 30
    target 1012
  ]
  edge [
    source 30
    target 1013
  ]
  edge [
    source 30
    target 1014
  ]
  edge [
    source 30
    target 1015
  ]
  edge [
    source 30
    target 1016
  ]
  edge [
    source 30
    target 1017
  ]
  edge [
    source 30
    target 1018
  ]
  edge [
    source 30
    target 1019
  ]
  edge [
    source 30
    target 1020
  ]
  edge [
    source 30
    target 1021
  ]
  edge [
    source 30
    target 1022
  ]
  edge [
    source 30
    target 1023
  ]
  edge [
    source 30
    target 1024
  ]
  edge [
    source 30
    target 1025
  ]
  edge [
    source 30
    target 1026
  ]
  edge [
    source 30
    target 1027
  ]
  edge [
    source 30
    target 1028
  ]
  edge [
    source 30
    target 1029
  ]
  edge [
    source 30
    target 1030
  ]
  edge [
    source 30
    target 1031
  ]
  edge [
    source 30
    target 1032
  ]
  edge [
    source 30
    target 1033
  ]
  edge [
    source 30
    target 1034
  ]
  edge [
    source 30
    target 1035
  ]
  edge [
    source 30
    target 1036
  ]
  edge [
    source 30
    target 1037
  ]
  edge [
    source 30
    target 1038
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1039
  ]
  edge [
    source 31
    target 1040
  ]
  edge [
    source 31
    target 1041
  ]
  edge [
    source 31
    target 1042
  ]
  edge [
    source 31
    target 1043
  ]
  edge [
    source 31
    target 1044
  ]
  edge [
    source 31
    target 1045
  ]
  edge [
    source 31
    target 1046
  ]
  edge [
    source 31
    target 1047
  ]
  edge [
    source 31
    target 1048
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1049
  ]
  edge [
    source 32
    target 1050
  ]
  edge [
    source 32
    target 1051
  ]
  edge [
    source 32
    target 473
  ]
  edge [
    source 32
    target 1052
  ]
  edge [
    source 32
    target 1053
  ]
  edge [
    source 32
    target 1054
  ]
  edge [
    source 32
    target 1055
  ]
  edge [
    source 32
    target 1056
  ]
  edge [
    source 32
    target 1057
  ]
  edge [
    source 32
    target 1058
  ]
  edge [
    source 32
    target 1059
  ]
  edge [
    source 32
    target 1060
  ]
  edge [
    source 32
    target 911
  ]
  edge [
    source 32
    target 1061
  ]
  edge [
    source 32
    target 481
  ]
  edge [
    source 32
    target 480
  ]
  edge [
    source 32
    target 482
  ]
  edge [
    source 32
    target 483
  ]
  edge [
    source 32
    target 1062
  ]
  edge [
    source 32
    target 211
  ]
  edge [
    source 32
    target 1063
  ]
  edge [
    source 32
    target 1064
  ]
  edge [
    source 32
    target 1065
  ]
  edge [
    source 32
    target 1066
  ]
  edge [
    source 32
    target 1067
  ]
  edge [
    source 32
    target 1068
  ]
  edge [
    source 32
    target 1069
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 1070
  ]
  edge [
    source 32
    target 1071
  ]
  edge [
    source 32
    target 1072
  ]
  edge [
    source 32
    target 1073
  ]
  edge [
    source 32
    target 1074
  ]
  edge [
    source 32
    target 1075
  ]
  edge [
    source 32
    target 1076
  ]
  edge [
    source 32
    target 1077
  ]
  edge [
    source 32
    target 1078
  ]
  edge [
    source 32
    target 1079
  ]
  edge [
    source 32
    target 1080
  ]
  edge [
    source 32
    target 1081
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1082
  ]
  edge [
    source 33
    target 1083
  ]
  edge [
    source 33
    target 878
  ]
  edge [
    source 33
    target 1084
  ]
  edge [
    source 33
    target 1085
  ]
  edge [
    source 33
    target 1086
  ]
  edge [
    source 33
    target 1087
  ]
  edge [
    source 33
    target 1088
  ]
  edge [
    source 33
    target 186
  ]
  edge [
    source 33
    target 1089
  ]
  edge [
    source 33
    target 1090
  ]
  edge [
    source 33
    target 1091
  ]
  edge [
    source 33
    target 1055
  ]
  edge [
    source 33
    target 1092
  ]
  edge [
    source 33
    target 1093
  ]
  edge [
    source 33
    target 1094
  ]
  edge [
    source 33
    target 1095
  ]
  edge [
    source 33
    target 1096
  ]
  edge [
    source 33
    target 1097
  ]
  edge [
    source 33
    target 1098
  ]
  edge [
    source 33
    target 815
  ]
  edge [
    source 33
    target 1099
  ]
  edge [
    source 33
    target 911
  ]
  edge [
    source 33
    target 183
  ]
  edge [
    source 33
    target 1100
  ]
  edge [
    source 33
    target 1101
  ]
  edge [
    source 33
    target 1102
  ]
  edge [
    source 33
    target 850
  ]
  edge [
    source 33
    target 1103
  ]
  edge [
    source 33
    target 894
  ]
  edge [
    source 33
    target 895
  ]
  edge [
    source 33
    target 896
  ]
  edge [
    source 33
    target 897
  ]
  edge [
    source 33
    target 898
  ]
  edge [
    source 33
    target 891
  ]
  edge [
    source 33
    target 899
  ]
  edge [
    source 33
    target 900
  ]
  edge [
    source 33
    target 901
  ]
  edge [
    source 33
    target 902
  ]
  edge [
    source 33
    target 903
  ]
  edge [
    source 33
    target 904
  ]
  edge [
    source 33
    target 905
  ]
  edge [
    source 33
    target 906
  ]
  edge [
    source 33
    target 1104
  ]
  edge [
    source 33
    target 1105
  ]
  edge [
    source 33
    target 1106
  ]
  edge [
    source 33
    target 1107
  ]
  edge [
    source 33
    target 1108
  ]
  edge [
    source 33
    target 1109
  ]
  edge [
    source 33
    target 1110
  ]
  edge [
    source 33
    target 1111
  ]
  edge [
    source 33
    target 206
  ]
  edge [
    source 33
    target 659
  ]
  edge [
    source 33
    target 656
  ]
  edge [
    source 33
    target 658
  ]
  edge [
    source 33
    target 1112
  ]
  edge [
    source 33
    target 1113
  ]
  edge [
    source 33
    target 1114
  ]
  edge [
    source 33
    target 1115
  ]
  edge [
    source 33
    target 674
  ]
  edge [
    source 33
    target 1116
  ]
  edge [
    source 33
    target 1069
  ]
  edge [
    source 33
    target 1117
  ]
  edge [
    source 33
    target 1062
  ]
  edge [
    source 33
    target 211
  ]
  edge [
    source 33
    target 1063
  ]
  edge [
    source 33
    target 1118
  ]
  edge [
    source 33
    target 1119
  ]
  edge [
    source 33
    target 1120
  ]
  edge [
    source 33
    target 1121
  ]
  edge [
    source 33
    target 1122
  ]
  edge [
    source 33
    target 1123
  ]
  edge [
    source 33
    target 1124
  ]
  edge [
    source 33
    target 1125
  ]
  edge [
    source 33
    target 1126
  ]
  edge [
    source 33
    target 126
  ]
  edge [
    source 33
    target 1127
  ]
  edge [
    source 33
    target 1128
  ]
  edge [
    source 33
    target 1129
  ]
  edge [
    source 33
    target 1130
  ]
  edge [
    source 33
    target 1131
  ]
  edge [
    source 33
    target 1132
  ]
  edge [
    source 33
    target 1133
  ]
  edge [
    source 33
    target 1134
  ]
  edge [
    source 33
    target 1135
  ]
  edge [
    source 33
    target 1136
  ]
  edge [
    source 33
    target 1137
  ]
  edge [
    source 33
    target 1138
  ]
  edge [
    source 33
    target 483
  ]
  edge [
    source 33
    target 1139
  ]
  edge [
    source 33
    target 1140
  ]
  edge [
    source 33
    target 1141
  ]
  edge [
    source 33
    target 1142
  ]
  edge [
    source 33
    target 1143
  ]
  edge [
    source 33
    target 1144
  ]
  edge [
    source 33
    target 73
  ]
  edge [
    source 33
    target 1145
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 664
  ]
  edge [
    source 34
    target 662
  ]
  edge [
    source 34
    target 663
  ]
  edge [
    source 34
    target 192
  ]
  edge [
    source 34
    target 193
  ]
  edge [
    source 34
    target 194
  ]
  edge [
    source 34
    target 195
  ]
  edge [
    source 34
    target 196
  ]
  edge [
    source 34
    target 197
  ]
  edge [
    source 34
    target 198
  ]
  edge [
    source 34
    target 199
  ]
  edge [
    source 34
    target 200
  ]
  edge [
    source 34
    target 201
  ]
  edge [
    source 34
    target 202
  ]
  edge [
    source 34
    target 203
  ]
  edge [
    source 34
    target 204
  ]
  edge [
    source 34
    target 205
  ]
  edge [
    source 34
    target 206
  ]
  edge [
    source 34
    target 207
  ]
  edge [
    source 34
    target 435
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 847
  ]
  edge [
    source 36
    target 443
  ]
  edge [
    source 36
    target 1146
  ]
  edge [
    source 36
    target 1147
  ]
  edge [
    source 36
    target 159
  ]
  edge [
    source 36
    target 1148
  ]
  edge [
    source 36
    target 1149
  ]
  edge [
    source 36
    target 1150
  ]
  edge [
    source 36
    target 1151
  ]
  edge [
    source 36
    target 1152
  ]
  edge [
    source 36
    target 1153
  ]
  edge [
    source 36
    target 654
  ]
  edge [
    source 36
    target 1154
  ]
  edge [
    source 36
    target 1155
  ]
  edge [
    source 36
    target 1156
  ]
  edge [
    source 36
    target 1157
  ]
  edge [
    source 36
    target 1158
  ]
  edge [
    source 36
    target 1159
  ]
  edge [
    source 36
    target 1160
  ]
  edge [
    source 36
    target 1161
  ]
  edge [
    source 36
    target 1162
  ]
  edge [
    source 36
    target 875
  ]
  edge [
    source 36
    target 73
  ]
  edge [
    source 36
    target 1163
  ]
  edge [
    source 36
    target 1164
  ]
  edge [
    source 36
    target 1165
  ]
  edge [
    source 36
    target 1166
  ]
  edge [
    source 36
    target 1167
  ]
  edge [
    source 36
    target 1168
  ]
  edge [
    source 36
    target 1169
  ]
  edge [
    source 36
    target 1170
  ]
  edge [
    source 36
    target 368
  ]
  edge [
    source 36
    target 1171
  ]
  edge [
    source 36
    target 591
  ]
  edge [
    source 36
    target 1172
  ]
  edge [
    source 36
    target 1173
  ]
  edge [
    source 36
    target 1174
  ]
  edge [
    source 36
    target 1175
  ]
  edge [
    source 36
    target 1176
  ]
  edge [
    source 36
    target 1177
  ]
  edge [
    source 36
    target 1178
  ]
  edge [
    source 36
    target 232
  ]
  edge [
    source 36
    target 887
  ]
  edge [
    source 36
    target 1179
  ]
  edge [
    source 36
    target 1180
  ]
  edge [
    source 36
    target 1181
  ]
  edge [
    source 36
    target 1182
  ]
  edge [
    source 36
    target 1183
  ]
  edge [
    source 36
    target 1184
  ]
  edge [
    source 36
    target 921
  ]
  edge [
    source 36
    target 1185
  ]
  edge [
    source 36
    target 1186
  ]
  edge [
    source 36
    target 1187
  ]
  edge [
    source 36
    target 1188
  ]
  edge [
    source 36
    target 800
  ]
  edge [
    source 36
    target 1189
  ]
  edge [
    source 36
    target 1190
  ]
  edge [
    source 36
    target 1191
  ]
  edge [
    source 36
    target 919
  ]
  edge [
    source 36
    target 1192
  ]
  edge [
    source 36
    target 1193
  ]
  edge [
    source 36
    target 1194
  ]
  edge [
    source 36
    target 495
  ]
  edge [
    source 36
    target 1195
  ]
  edge [
    source 36
    target 1196
  ]
  edge [
    source 36
    target 1197
  ]
  edge [
    source 36
    target 583
  ]
  edge [
    source 36
    target 1198
  ]
  edge [
    source 36
    target 1199
  ]
  edge [
    source 36
    target 1200
  ]
  edge [
    source 36
    target 1201
  ]
  edge [
    source 36
    target 1202
  ]
  edge [
    source 36
    target 150
  ]
  edge [
    source 36
    target 153
  ]
  edge [
    source 36
    target 155
  ]
  edge [
    source 36
    target 671
  ]
  edge [
    source 36
    target 673
  ]
  edge [
    source 36
    target 157
  ]
  edge [
    source 36
    target 1203
  ]
  edge [
    source 36
    target 1204
  ]
  edge [
    source 36
    target 1205
  ]
  edge [
    source 36
    target 1206
  ]
  edge [
    source 36
    target 1207
  ]
  edge [
    source 36
    target 160
  ]
  edge [
    source 36
    target 1208
  ]
  edge [
    source 36
    target 1209
  ]
  edge [
    source 36
    target 162
  ]
  edge [
    source 36
    target 1210
  ]
  edge [
    source 36
    target 165
  ]
  edge [
    source 36
    target 170
  ]
  edge [
    source 36
    target 682
  ]
  edge [
    source 36
    target 1211
  ]
  edge [
    source 36
    target 1212
  ]
  edge [
    source 36
    target 1213
  ]
  edge [
    source 36
    target 1214
  ]
  edge [
    source 36
    target 175
  ]
  edge [
    source 36
    target 688
  ]
  edge [
    source 36
    target 1215
  ]
  edge [
    source 36
    target 1216
  ]
  edge [
    source 36
    target 180
  ]
  edge [
    source 36
    target 1217
  ]
  edge [
    source 36
    target 1218
  ]
  edge [
    source 36
    target 1219
  ]
  edge [
    source 36
    target 607
  ]
  edge [
    source 36
    target 1220
  ]
  edge [
    source 36
    target 1221
  ]
  edge [
    source 36
    target 244
  ]
  edge [
    source 36
    target 57
  ]
  edge [
    source 36
    target 1222
  ]
  edge [
    source 36
    target 1223
  ]
  edge [
    source 36
    target 1224
  ]
  edge [
    source 36
    target 1225
  ]
  edge [
    source 36
    target 1226
  ]
  edge [
    source 36
    target 1227
  ]
  edge [
    source 36
    target 1228
  ]
  edge [
    source 36
    target 1229
  ]
  edge [
    source 36
    target 1230
  ]
  edge [
    source 36
    target 1231
  ]
  edge [
    source 36
    target 1232
  ]
  edge [
    source 36
    target 1233
  ]
  edge [
    source 36
    target 1234
  ]
  edge [
    source 36
    target 1235
  ]
  edge [
    source 36
    target 1236
  ]
  edge [
    source 36
    target 1237
  ]
  edge [
    source 36
    target 1238
  ]
  edge [
    source 36
    target 1239
  ]
  edge [
    source 36
    target 62
  ]
  edge [
    source 36
    target 1240
  ]
  edge [
    source 36
    target 1241
  ]
  edge [
    source 36
    target 1242
  ]
  edge [
    source 36
    target 1243
  ]
  edge [
    source 36
    target 873
  ]
  edge [
    source 36
    target 1244
  ]
  edge [
    source 36
    target 1245
  ]
  edge [
    source 36
    target 1246
  ]
  edge [
    source 36
    target 1247
  ]
  edge [
    source 36
    target 1248
  ]
  edge [
    source 36
    target 1249
  ]
  edge [
    source 36
    target 1250
  ]
  edge [
    source 36
    target 1251
  ]
  edge [
    source 36
    target 1252
  ]
  edge [
    source 36
    target 1253
  ]
  edge [
    source 36
    target 1254
  ]
  edge [
    source 36
    target 1255
  ]
  edge [
    source 36
    target 1256
  ]
  edge [
    source 36
    target 1257
  ]
  edge [
    source 36
    target 1258
  ]
  edge [
    source 36
    target 1259
  ]
  edge [
    source 725
    target 1262
  ]
  edge [
    source 1260
    target 1261
  ]
  edge [
    source 1263
    target 1264
  ]
]
