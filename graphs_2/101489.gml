graph [
  node [
    id 0
    label "codex"
    origin "text"
  ]
  node [
    id 1
    label "runicus"
    origin "text"
  ]
  node [
    id 2
    label "Codex"
  ]
  node [
    id 3
    label "Runicus"
  ]
  node [
    id 4
    label "Dr&#248;mte"
  ]
  node [
    id 5
    label "mig"
  ]
  node [
    id 6
    label "en"
  ]
  node [
    id 7
    label "dr&#248;m"
  ]
  node [
    id 8
    label "i"
  ]
  node [
    id 9
    label "nat"
  ]
  node [
    id 10
    label "prawo"
  ]
  node [
    id 11
    label "Ska&#324;skie"
  ]
  node [
    id 12
    label "Sk&#229;nske"
  ]
  node [
    id 13
    label "lov"
  ]
  node [
    id 14
    label "wyspa"
  ]
  node [
    id 15
    label "noc"
  ]
  node [
    id 16
    label "mia&#322;"
  ]
  node [
    id 17
    label "senior"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
]
