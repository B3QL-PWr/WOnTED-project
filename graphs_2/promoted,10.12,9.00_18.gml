graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "facet"
    origin "text"
  ]
  node [
    id 3
    label "przy&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 4
    label "robot"
    origin "text"
  ]
  node [
    id 5
    label "kolega"
    origin "text"
  ]
  node [
    id 6
    label "wy&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 7
    label "bratek"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "ludzko&#347;&#263;"
  ]
  node [
    id 10
    label "asymilowanie"
  ]
  node [
    id 11
    label "wapniak"
  ]
  node [
    id 12
    label "asymilowa&#263;"
  ]
  node [
    id 13
    label "os&#322;abia&#263;"
  ]
  node [
    id 14
    label "posta&#263;"
  ]
  node [
    id 15
    label "hominid"
  ]
  node [
    id 16
    label "podw&#322;adny"
  ]
  node [
    id 17
    label "os&#322;abianie"
  ]
  node [
    id 18
    label "g&#322;owa"
  ]
  node [
    id 19
    label "figura"
  ]
  node [
    id 20
    label "portrecista"
  ]
  node [
    id 21
    label "dwun&#243;g"
  ]
  node [
    id 22
    label "profanum"
  ]
  node [
    id 23
    label "mikrokosmos"
  ]
  node [
    id 24
    label "nasada"
  ]
  node [
    id 25
    label "duch"
  ]
  node [
    id 26
    label "antropochoria"
  ]
  node [
    id 27
    label "osoba"
  ]
  node [
    id 28
    label "wz&#243;r"
  ]
  node [
    id 29
    label "senior"
  ]
  node [
    id 30
    label "oddzia&#322;ywanie"
  ]
  node [
    id 31
    label "Adam"
  ]
  node [
    id 32
    label "homo_sapiens"
  ]
  node [
    id 33
    label "polifag"
  ]
  node [
    id 34
    label "kochanek"
  ]
  node [
    id 35
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 36
    label "fio&#322;ek"
  ]
  node [
    id 37
    label "brat"
  ]
  node [
    id 38
    label "set"
  ]
  node [
    id 39
    label "catch"
  ]
  node [
    id 40
    label "przypalantowa&#263;"
  ]
  node [
    id 41
    label "zbli&#380;y&#263;"
  ]
  node [
    id 42
    label "uderzy&#263;"
  ]
  node [
    id 43
    label "dopieprzy&#263;"
  ]
  node [
    id 44
    label "urazi&#263;"
  ]
  node [
    id 45
    label "strike"
  ]
  node [
    id 46
    label "wystartowa&#263;"
  ]
  node [
    id 47
    label "przywali&#263;"
  ]
  node [
    id 48
    label "dupn&#261;&#263;"
  ]
  node [
    id 49
    label "skrytykowa&#263;"
  ]
  node [
    id 50
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 51
    label "nast&#261;pi&#263;"
  ]
  node [
    id 52
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 53
    label "sztachn&#261;&#263;"
  ]
  node [
    id 54
    label "rap"
  ]
  node [
    id 55
    label "zrobi&#263;"
  ]
  node [
    id 56
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 57
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 58
    label "crush"
  ]
  node [
    id 59
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 60
    label "postara&#263;_si&#281;"
  ]
  node [
    id 61
    label "fall"
  ]
  node [
    id 62
    label "hopn&#261;&#263;"
  ]
  node [
    id 63
    label "spowodowa&#263;"
  ]
  node [
    id 64
    label "zada&#263;"
  ]
  node [
    id 65
    label "uda&#263;_si&#281;"
  ]
  node [
    id 66
    label "dotkn&#261;&#263;"
  ]
  node [
    id 67
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 68
    label "anoint"
  ]
  node [
    id 69
    label "transgress"
  ]
  node [
    id 70
    label "chop"
  ]
  node [
    id 71
    label "jebn&#261;&#263;"
  ]
  node [
    id 72
    label "lumber"
  ]
  node [
    id 73
    label "approach"
  ]
  node [
    id 74
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 75
    label "set_about"
  ]
  node [
    id 76
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 77
    label "dowali&#263;"
  ]
  node [
    id 78
    label "gem"
  ]
  node [
    id 79
    label "kompozycja"
  ]
  node [
    id 80
    label "runda"
  ]
  node [
    id 81
    label "muzyka"
  ]
  node [
    id 82
    label "zestaw"
  ]
  node [
    id 83
    label "sprz&#281;t_AGD"
  ]
  node [
    id 84
    label "maszyna"
  ]
  node [
    id 85
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 86
    label "automat"
  ]
  node [
    id 87
    label "automatyczna_skrzynia_bieg&#243;w"
  ]
  node [
    id 88
    label "wrzutnik_monet"
  ]
  node [
    id 89
    label "samoch&#243;d"
  ]
  node [
    id 90
    label "telefon"
  ]
  node [
    id 91
    label "dehumanizacja"
  ]
  node [
    id 92
    label "pistolet"
  ]
  node [
    id 93
    label "bro&#324;_samoczynno-samopowtarzalna"
  ]
  node [
    id 94
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 95
    label "pralka"
  ]
  node [
    id 96
    label "urz&#261;dzenie"
  ]
  node [
    id 97
    label "lody_w&#322;oskie"
  ]
  node [
    id 98
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 99
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 100
    label "tuleja"
  ]
  node [
    id 101
    label "pracowanie"
  ]
  node [
    id 102
    label "kad&#322;ub"
  ]
  node [
    id 103
    label "n&#243;&#380;"
  ]
  node [
    id 104
    label "b&#281;benek"
  ]
  node [
    id 105
    label "wa&#322;"
  ]
  node [
    id 106
    label "maszyneria"
  ]
  node [
    id 107
    label "prototypownia"
  ]
  node [
    id 108
    label "trawers"
  ]
  node [
    id 109
    label "deflektor"
  ]
  node [
    id 110
    label "kolumna"
  ]
  node [
    id 111
    label "mechanizm"
  ]
  node [
    id 112
    label "wa&#322;ek"
  ]
  node [
    id 113
    label "pracowa&#263;"
  ]
  node [
    id 114
    label "b&#281;ben"
  ]
  node [
    id 115
    label "rz&#281;zi&#263;"
  ]
  node [
    id 116
    label "przyk&#322;adka"
  ]
  node [
    id 117
    label "t&#322;ok"
  ]
  node [
    id 118
    label "rami&#281;"
  ]
  node [
    id 119
    label "rz&#281;&#380;enie"
  ]
  node [
    id 120
    label "znajomy"
  ]
  node [
    id 121
    label "towarzysz"
  ]
  node [
    id 122
    label "kumplowanie_si&#281;"
  ]
  node [
    id 123
    label "ziom"
  ]
  node [
    id 124
    label "partner"
  ]
  node [
    id 125
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 126
    label "konfrater"
  ]
  node [
    id 127
    label "pracownik"
  ]
  node [
    id 128
    label "przedsi&#281;biorca"
  ]
  node [
    id 129
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 130
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 131
    label "kolaborator"
  ]
  node [
    id 132
    label "prowadzi&#263;"
  ]
  node [
    id 133
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 134
    label "sp&#243;lnik"
  ]
  node [
    id 135
    label "aktor"
  ]
  node [
    id 136
    label "uczestniczenie"
  ]
  node [
    id 137
    label "partyjny"
  ]
  node [
    id 138
    label "komunista"
  ]
  node [
    id 139
    label "znany"
  ]
  node [
    id 140
    label "zapoznanie"
  ]
  node [
    id 141
    label "sw&#243;j"
  ]
  node [
    id 142
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 143
    label "zapoznanie_si&#281;"
  ]
  node [
    id 144
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 145
    label "znajomek"
  ]
  node [
    id 146
    label "zapoznawanie"
  ]
  node [
    id 147
    label "znajomo"
  ]
  node [
    id 148
    label "pewien"
  ]
  node [
    id 149
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 150
    label "przyj&#281;ty"
  ]
  node [
    id 151
    label "za_pan_brat"
  ]
  node [
    id 152
    label "swojak"
  ]
  node [
    id 153
    label "sheathe"
  ]
  node [
    id 154
    label "pieni&#261;dze"
  ]
  node [
    id 155
    label "translate"
  ]
  node [
    id 156
    label "give"
  ]
  node [
    id 157
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 158
    label "wyj&#261;&#263;"
  ]
  node [
    id 159
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 160
    label "range"
  ]
  node [
    id 161
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 162
    label "post&#261;pi&#263;"
  ]
  node [
    id 163
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 164
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 165
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 166
    label "zorganizowa&#263;"
  ]
  node [
    id 167
    label "appoint"
  ]
  node [
    id 168
    label "wystylizowa&#263;"
  ]
  node [
    id 169
    label "cause"
  ]
  node [
    id 170
    label "przerobi&#263;"
  ]
  node [
    id 171
    label "nabra&#263;"
  ]
  node [
    id 172
    label "make"
  ]
  node [
    id 173
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 174
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 175
    label "wydali&#263;"
  ]
  node [
    id 176
    label "wear"
  ]
  node [
    id 177
    label "return"
  ]
  node [
    id 178
    label "plant"
  ]
  node [
    id 179
    label "pozostawi&#263;"
  ]
  node [
    id 180
    label "pokry&#263;"
  ]
  node [
    id 181
    label "znak"
  ]
  node [
    id 182
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 183
    label "przygotowa&#263;"
  ]
  node [
    id 184
    label "stagger"
  ]
  node [
    id 185
    label "zepsu&#263;"
  ]
  node [
    id 186
    label "zmieni&#263;"
  ]
  node [
    id 187
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 188
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 189
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 190
    label "umie&#347;ci&#263;"
  ]
  node [
    id 191
    label "zacz&#261;&#263;"
  ]
  node [
    id 192
    label "raise"
  ]
  node [
    id 193
    label "wygra&#263;"
  ]
  node [
    id 194
    label "wydzieli&#263;"
  ]
  node [
    id 195
    label "distill"
  ]
  node [
    id 196
    label "wy&#322;&#261;czy&#263;"
  ]
  node [
    id 197
    label "remove"
  ]
  node [
    id 198
    label "clear"
  ]
  node [
    id 199
    label "przedstawi&#263;"
  ]
  node [
    id 200
    label "explain"
  ]
  node [
    id 201
    label "poja&#347;ni&#263;"
  ]
  node [
    id 202
    label "portfel"
  ]
  node [
    id 203
    label "kwota"
  ]
  node [
    id 204
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 205
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 206
    label "forsa"
  ]
  node [
    id 207
    label "kapanie"
  ]
  node [
    id 208
    label "kapn&#261;&#263;"
  ]
  node [
    id 209
    label "kapa&#263;"
  ]
  node [
    id 210
    label "kapita&#322;"
  ]
  node [
    id 211
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 212
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 213
    label "kapni&#281;cie"
  ]
  node [
    id 214
    label "wyda&#263;"
  ]
  node [
    id 215
    label "hajs"
  ]
  node [
    id 216
    label "dydki"
  ]
  node [
    id 217
    label "sypni&#281;cie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
]
