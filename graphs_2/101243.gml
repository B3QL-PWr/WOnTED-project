graph [
  node [
    id 0
    label "pierog"
    origin "text"
  ]
  node [
    id 1
    label "ruski"
    origin "text"
  ]
  node [
    id 2
    label "nadzienie"
  ]
  node [
    id 3
    label "pierogi"
  ]
  node [
    id 4
    label "potrawa"
  ]
  node [
    id 5
    label "masa"
  ]
  node [
    id 6
    label "dodatek"
  ]
  node [
    id 7
    label "nadziewarka"
  ]
  node [
    id 8
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 9
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 10
    label "ostentacyjny"
  ]
  node [
    id 11
    label "ziemniaczany"
  ]
  node [
    id 12
    label "kacapski"
  ]
  node [
    id 13
    label "toporny"
  ]
  node [
    id 14
    label "j&#281;zyki_wschodnios&#322;owia&#324;skie"
  ]
  node [
    id 15
    label "serowy"
  ]
  node [
    id 16
    label "wulgarny"
  ]
  node [
    id 17
    label "po_rosyjsku"
  ]
  node [
    id 18
    label "wielkoruski"
  ]
  node [
    id 19
    label "Russian"
  ]
  node [
    id 20
    label "byle_jaki"
  ]
  node [
    id 21
    label "j&#281;zyk"
  ]
  node [
    id 22
    label "zbytkowny"
  ]
  node [
    id 23
    label "po_rusku"
  ]
  node [
    id 24
    label "rusek"
  ]
  node [
    id 25
    label "tandetny"
  ]
  node [
    id 26
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 27
    label "artykulator"
  ]
  node [
    id 28
    label "kod"
  ]
  node [
    id 29
    label "kawa&#322;ek"
  ]
  node [
    id 30
    label "przedmiot"
  ]
  node [
    id 31
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 32
    label "gramatyka"
  ]
  node [
    id 33
    label "stylik"
  ]
  node [
    id 34
    label "przet&#322;umaczenie"
  ]
  node [
    id 35
    label "formalizowanie"
  ]
  node [
    id 36
    label "ssa&#263;"
  ]
  node [
    id 37
    label "ssanie"
  ]
  node [
    id 38
    label "language"
  ]
  node [
    id 39
    label "liza&#263;"
  ]
  node [
    id 40
    label "napisa&#263;"
  ]
  node [
    id 41
    label "konsonantyzm"
  ]
  node [
    id 42
    label "wokalizm"
  ]
  node [
    id 43
    label "pisa&#263;"
  ]
  node [
    id 44
    label "fonetyka"
  ]
  node [
    id 45
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 46
    label "jeniec"
  ]
  node [
    id 47
    label "but"
  ]
  node [
    id 48
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 49
    label "po_koroniarsku"
  ]
  node [
    id 50
    label "kultura_duchowa"
  ]
  node [
    id 51
    label "t&#322;umaczenie"
  ]
  node [
    id 52
    label "m&#243;wienie"
  ]
  node [
    id 53
    label "pype&#263;"
  ]
  node [
    id 54
    label "lizanie"
  ]
  node [
    id 55
    label "pismo"
  ]
  node [
    id 56
    label "formalizowa&#263;"
  ]
  node [
    id 57
    label "rozumie&#263;"
  ]
  node [
    id 58
    label "organ"
  ]
  node [
    id 59
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 60
    label "rozumienie"
  ]
  node [
    id 61
    label "spos&#243;b"
  ]
  node [
    id 62
    label "makroglosja"
  ]
  node [
    id 63
    label "m&#243;wi&#263;"
  ]
  node [
    id 64
    label "jama_ustna"
  ]
  node [
    id 65
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 66
    label "formacja_geologiczna"
  ]
  node [
    id 67
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 68
    label "natural_language"
  ]
  node [
    id 69
    label "s&#322;ownictwo"
  ]
  node [
    id 70
    label "urz&#261;dzenie"
  ]
  node [
    id 71
    label "wschodnioeuropejski"
  ]
  node [
    id 72
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 73
    label "poga&#324;ski"
  ]
  node [
    id 74
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 75
    label "topielec"
  ]
  node [
    id 76
    label "europejski"
  ]
  node [
    id 77
    label "niezgrabny"
  ]
  node [
    id 78
    label "topornie"
  ]
  node [
    id 79
    label "kaleki"
  ]
  node [
    id 80
    label "ci&#281;&#380;ki"
  ]
  node [
    id 81
    label "kiczowaty"
  ]
  node [
    id 82
    label "banalny"
  ]
  node [
    id 83
    label "nieelegancki"
  ]
  node [
    id 84
    label "&#380;a&#322;osny"
  ]
  node [
    id 85
    label "tani"
  ]
  node [
    id 86
    label "kiepski"
  ]
  node [
    id 87
    label "tandetnie"
  ]
  node [
    id 88
    label "odra&#380;aj&#261;cy"
  ]
  node [
    id 89
    label "nikczemny"
  ]
  node [
    id 90
    label "ostentacyjnie"
  ]
  node [
    id 91
    label "nadmierny"
  ]
  node [
    id 92
    label "obnoszenie_si&#281;"
  ]
  node [
    id 93
    label "&#322;askawy"
  ]
  node [
    id 94
    label "niedyskretny"
  ]
  node [
    id 95
    label "wystawnie"
  ]
  node [
    id 96
    label "zb&#281;dny"
  ]
  node [
    id 97
    label "zbytkownie"
  ]
  node [
    id 98
    label "bogaty"
  ]
  node [
    id 99
    label "prostacki"
  ]
  node [
    id 100
    label "sp&#322;ycony"
  ]
  node [
    id 101
    label "niegrzeczny"
  ]
  node [
    id 102
    label "grubia&#324;ski"
  ]
  node [
    id 103
    label "nachalny"
  ]
  node [
    id 104
    label "ra&#380;&#261;cy"
  ]
  node [
    id 105
    label "wulgarnie"
  ]
  node [
    id 106
    label "ciep&#322;y"
  ]
  node [
    id 107
    label "warzywny"
  ]
  node [
    id 108
    label "przypominaj&#261;cy"
  ]
  node [
    id 109
    label "rosyjski"
  ]
  node [
    id 110
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 111
    label "po_kacapsku"
  ]
  node [
    id 112
    label "imperialny"
  ]
  node [
    id 113
    label "po_wielkorusku"
  ]
  node [
    id 114
    label "rusy"
  ]
  node [
    id 115
    label "czerwony"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 114
    target 115
  ]
]
