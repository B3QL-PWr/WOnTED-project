graph [
  node [
    id 0
    label "godzina"
    origin "text"
  ]
  node [
    id 1
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 2
    label "transport"
    origin "text"
  ]
  node [
    id 3
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 4
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wszelki"
    origin "text"
  ]
  node [
    id 6
    label "przew&#243;z"
    origin "text"
  ]
  node [
    id 7
    label "statek"
    origin "text"
  ]
  node [
    id 8
    label "morski"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "powietrzny"
    origin "text"
  ]
  node [
    id 11
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 12
    label "kolejowy"
    origin "text"
  ]
  node [
    id 13
    label "samochodowy"
    origin "text"
  ]
  node [
    id 14
    label "eksploatowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "przez"
    origin "text"
  ]
  node [
    id 16
    label "przedsi&#281;biorstwo"
    origin "text"
  ]
  node [
    id 17
    label "umawia&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 20
    label "wyj&#261;tek"
    origin "text"
  ]
  node [
    id 21
    label "przypadek"
    origin "text"
  ]
  node [
    id 22
    label "gdy"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 25
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 26
    label "miejsce"
    origin "text"
  ]
  node [
    id 27
    label "po&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 28
    label "druga"
    origin "text"
  ]
  node [
    id 29
    label "time"
  ]
  node [
    id 30
    label "doba"
  ]
  node [
    id 31
    label "p&#243;&#322;godzina"
  ]
  node [
    id 32
    label "jednostka_czasu"
  ]
  node [
    id 33
    label "czas"
  ]
  node [
    id 34
    label "minuta"
  ]
  node [
    id 35
    label "kwadrans"
  ]
  node [
    id 36
    label "poprzedzanie"
  ]
  node [
    id 37
    label "czasoprzestrze&#324;"
  ]
  node [
    id 38
    label "laba"
  ]
  node [
    id 39
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 40
    label "chronometria"
  ]
  node [
    id 41
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 42
    label "rachuba_czasu"
  ]
  node [
    id 43
    label "przep&#322;ywanie"
  ]
  node [
    id 44
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 45
    label "czasokres"
  ]
  node [
    id 46
    label "odczyt"
  ]
  node [
    id 47
    label "chwila"
  ]
  node [
    id 48
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 49
    label "dzieje"
  ]
  node [
    id 50
    label "kategoria_gramatyczna"
  ]
  node [
    id 51
    label "poprzedzenie"
  ]
  node [
    id 52
    label "trawienie"
  ]
  node [
    id 53
    label "pochodzi&#263;"
  ]
  node [
    id 54
    label "period"
  ]
  node [
    id 55
    label "okres_czasu"
  ]
  node [
    id 56
    label "poprzedza&#263;"
  ]
  node [
    id 57
    label "schy&#322;ek"
  ]
  node [
    id 58
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 59
    label "odwlekanie_si&#281;"
  ]
  node [
    id 60
    label "zegar"
  ]
  node [
    id 61
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 62
    label "czwarty_wymiar"
  ]
  node [
    id 63
    label "pochodzenie"
  ]
  node [
    id 64
    label "koniugacja"
  ]
  node [
    id 65
    label "Zeitgeist"
  ]
  node [
    id 66
    label "trawi&#263;"
  ]
  node [
    id 67
    label "pogoda"
  ]
  node [
    id 68
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 69
    label "poprzedzi&#263;"
  ]
  node [
    id 70
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 71
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 72
    label "time_period"
  ]
  node [
    id 73
    label "zapis"
  ]
  node [
    id 74
    label "sekunda"
  ]
  node [
    id 75
    label "jednostka"
  ]
  node [
    id 76
    label "stopie&#324;"
  ]
  node [
    id 77
    label "design"
  ]
  node [
    id 78
    label "tydzie&#324;"
  ]
  node [
    id 79
    label "noc"
  ]
  node [
    id 80
    label "dzie&#324;"
  ]
  node [
    id 81
    label "long_time"
  ]
  node [
    id 82
    label "jednostka_geologiczna"
  ]
  node [
    id 83
    label "follow-up"
  ]
  node [
    id 84
    label "term"
  ]
  node [
    id 85
    label "ustalenie"
  ]
  node [
    id 86
    label "appointment"
  ]
  node [
    id 87
    label "localization"
  ]
  node [
    id 88
    label "ozdobnik"
  ]
  node [
    id 89
    label "denomination"
  ]
  node [
    id 90
    label "zdecydowanie"
  ]
  node [
    id 91
    label "przewidzenie"
  ]
  node [
    id 92
    label "wyra&#380;enie"
  ]
  node [
    id 93
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 94
    label "decyzja"
  ]
  node [
    id 95
    label "pewnie"
  ]
  node [
    id 96
    label "zdecydowany"
  ]
  node [
    id 97
    label "zauwa&#380;alnie"
  ]
  node [
    id 98
    label "oddzia&#322;anie"
  ]
  node [
    id 99
    label "podj&#281;cie"
  ]
  node [
    id 100
    label "cecha"
  ]
  node [
    id 101
    label "resoluteness"
  ]
  node [
    id 102
    label "judgment"
  ]
  node [
    id 103
    label "zrobienie"
  ]
  node [
    id 104
    label "leksem"
  ]
  node [
    id 105
    label "sformu&#322;owanie"
  ]
  node [
    id 106
    label "zdarzenie_si&#281;"
  ]
  node [
    id 107
    label "poj&#281;cie"
  ]
  node [
    id 108
    label "poinformowanie"
  ]
  node [
    id 109
    label "wording"
  ]
  node [
    id 110
    label "kompozycja"
  ]
  node [
    id 111
    label "oznaczenie"
  ]
  node [
    id 112
    label "znak_j&#281;zykowy"
  ]
  node [
    id 113
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 114
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 115
    label "grupa_imienna"
  ]
  node [
    id 116
    label "jednostka_leksykalna"
  ]
  node [
    id 117
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 118
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 119
    label "ujawnienie"
  ]
  node [
    id 120
    label "affirmation"
  ]
  node [
    id 121
    label "zapisanie"
  ]
  node [
    id 122
    label "rzucenie"
  ]
  node [
    id 123
    label "umocnienie"
  ]
  node [
    id 124
    label "spowodowanie"
  ]
  node [
    id 125
    label "informacja"
  ]
  node [
    id 126
    label "czynno&#347;&#263;"
  ]
  node [
    id 127
    label "obliczenie"
  ]
  node [
    id 128
    label "spodziewanie_si&#281;"
  ]
  node [
    id 129
    label "zaplanowanie"
  ]
  node [
    id 130
    label "vision"
  ]
  node [
    id 131
    label "przedmiot"
  ]
  node [
    id 132
    label "dekor"
  ]
  node [
    id 133
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 134
    label "wypowied&#378;"
  ]
  node [
    id 135
    label "ornamentyka"
  ]
  node [
    id 136
    label "ilustracja"
  ]
  node [
    id 137
    label "d&#378;wi&#281;k"
  ]
  node [
    id 138
    label "dekoracja"
  ]
  node [
    id 139
    label "roz&#322;adunek"
  ]
  node [
    id 140
    label "sprz&#281;t"
  ]
  node [
    id 141
    label "cedu&#322;a"
  ]
  node [
    id 142
    label "jednoszynowy"
  ]
  node [
    id 143
    label "unos"
  ]
  node [
    id 144
    label "traffic"
  ]
  node [
    id 145
    label "prze&#322;adunek"
  ]
  node [
    id 146
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 147
    label "us&#322;uga"
  ]
  node [
    id 148
    label "komunikacja"
  ]
  node [
    id 149
    label "tyfon"
  ]
  node [
    id 150
    label "zawarto&#347;&#263;"
  ]
  node [
    id 151
    label "grupa"
  ]
  node [
    id 152
    label "towar"
  ]
  node [
    id 153
    label "gospodarka"
  ]
  node [
    id 154
    label "za&#322;adunek"
  ]
  node [
    id 155
    label "produkt_gotowy"
  ]
  node [
    id 156
    label "service"
  ]
  node [
    id 157
    label "asortyment"
  ]
  node [
    id 158
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 159
    label "&#347;wiadczenie"
  ]
  node [
    id 160
    label "transportation_system"
  ]
  node [
    id 161
    label "explicite"
  ]
  node [
    id 162
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 163
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 164
    label "wydeptywanie"
  ]
  node [
    id 165
    label "wydeptanie"
  ]
  node [
    id 166
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 167
    label "implicite"
  ]
  node [
    id 168
    label "ekspedytor"
  ]
  node [
    id 169
    label "odm&#322;adzanie"
  ]
  node [
    id 170
    label "liga"
  ]
  node [
    id 171
    label "jednostka_systematyczna"
  ]
  node [
    id 172
    label "asymilowanie"
  ]
  node [
    id 173
    label "gromada"
  ]
  node [
    id 174
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 175
    label "asymilowa&#263;"
  ]
  node [
    id 176
    label "egzemplarz"
  ]
  node [
    id 177
    label "Entuzjastki"
  ]
  node [
    id 178
    label "zbi&#243;r"
  ]
  node [
    id 179
    label "Terranie"
  ]
  node [
    id 180
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 181
    label "category"
  ]
  node [
    id 182
    label "pakiet_klimatyczny"
  ]
  node [
    id 183
    label "oddzia&#322;"
  ]
  node [
    id 184
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 185
    label "cz&#261;steczka"
  ]
  node [
    id 186
    label "stage_set"
  ]
  node [
    id 187
    label "type"
  ]
  node [
    id 188
    label "specgrupa"
  ]
  node [
    id 189
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 190
    label "&#346;wietliki"
  ]
  node [
    id 191
    label "odm&#322;odzenie"
  ]
  node [
    id 192
    label "Eurogrupa"
  ]
  node [
    id 193
    label "odm&#322;adza&#263;"
  ]
  node [
    id 194
    label "formacja_geologiczna"
  ]
  node [
    id 195
    label "harcerze_starsi"
  ]
  node [
    id 196
    label "metka"
  ]
  node [
    id 197
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 198
    label "cz&#322;owiek"
  ]
  node [
    id 199
    label "szprycowa&#263;"
  ]
  node [
    id 200
    label "naszprycowa&#263;"
  ]
  node [
    id 201
    label "rzuca&#263;"
  ]
  node [
    id 202
    label "tandeta"
  ]
  node [
    id 203
    label "obr&#243;t_handlowy"
  ]
  node [
    id 204
    label "wyr&#243;b"
  ]
  node [
    id 205
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 206
    label "rzuci&#263;"
  ]
  node [
    id 207
    label "naszprycowanie"
  ]
  node [
    id 208
    label "tkanina"
  ]
  node [
    id 209
    label "szprycowanie"
  ]
  node [
    id 210
    label "za&#322;adownia"
  ]
  node [
    id 211
    label "&#322;&#243;dzki"
  ]
  node [
    id 212
    label "narkobiznes"
  ]
  node [
    id 213
    label "rzucanie"
  ]
  node [
    id 214
    label "temat"
  ]
  node [
    id 215
    label "ilo&#347;&#263;"
  ]
  node [
    id 216
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 217
    label "wn&#281;trze"
  ]
  node [
    id 218
    label "sprz&#281;cior"
  ]
  node [
    id 219
    label "penis"
  ]
  node [
    id 220
    label "kolekcja"
  ]
  node [
    id 221
    label "furniture"
  ]
  node [
    id 222
    label "sprz&#281;cik"
  ]
  node [
    id 223
    label "equipment"
  ]
  node [
    id 224
    label "relokacja"
  ]
  node [
    id 225
    label "kolej"
  ]
  node [
    id 226
    label "raport"
  ]
  node [
    id 227
    label "kurs"
  ]
  node [
    id 228
    label "spis"
  ]
  node [
    id 229
    label "bilet"
  ]
  node [
    id 230
    label "sygnalizator"
  ]
  node [
    id 231
    label "ci&#281;&#380;ar"
  ]
  node [
    id 232
    label "granica"
  ]
  node [
    id 233
    label "&#322;adunek"
  ]
  node [
    id 234
    label "inwentarz"
  ]
  node [
    id 235
    label "rynek"
  ]
  node [
    id 236
    label "mieszkalnictwo"
  ]
  node [
    id 237
    label "agregat_ekonomiczny"
  ]
  node [
    id 238
    label "miejsce_pracy"
  ]
  node [
    id 239
    label "produkowanie"
  ]
  node [
    id 240
    label "farmaceutyka"
  ]
  node [
    id 241
    label "rolnictwo"
  ]
  node [
    id 242
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 243
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 244
    label "obronno&#347;&#263;"
  ]
  node [
    id 245
    label "sektor_prywatny"
  ]
  node [
    id 246
    label "sch&#322;adza&#263;"
  ]
  node [
    id 247
    label "czerwona_strefa"
  ]
  node [
    id 248
    label "struktura"
  ]
  node [
    id 249
    label "pole"
  ]
  node [
    id 250
    label "sektor_publiczny"
  ]
  node [
    id 251
    label "bankowo&#347;&#263;"
  ]
  node [
    id 252
    label "gospodarowanie"
  ]
  node [
    id 253
    label "obora"
  ]
  node [
    id 254
    label "gospodarka_wodna"
  ]
  node [
    id 255
    label "gospodarka_le&#347;na"
  ]
  node [
    id 256
    label "gospodarowa&#263;"
  ]
  node [
    id 257
    label "fabryka"
  ]
  node [
    id 258
    label "wytw&#243;rnia"
  ]
  node [
    id 259
    label "stodo&#322;a"
  ]
  node [
    id 260
    label "przemys&#322;"
  ]
  node [
    id 261
    label "spichlerz"
  ]
  node [
    id 262
    label "sch&#322;adzanie"
  ]
  node [
    id 263
    label "administracja"
  ]
  node [
    id 264
    label "sch&#322;odzenie"
  ]
  node [
    id 265
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 266
    label "zasada"
  ]
  node [
    id 267
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 268
    label "regulacja_cen"
  ]
  node [
    id 269
    label "szkolnictwo"
  ]
  node [
    id 270
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 271
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 272
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 273
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 274
    label "transgraniczny"
  ]
  node [
    id 275
    label "zbiorowo"
  ]
  node [
    id 276
    label "internationalization"
  ]
  node [
    id 277
    label "uwsp&#243;lnienie"
  ]
  node [
    id 278
    label "udost&#281;pnienie"
  ]
  node [
    id 279
    label "udost&#281;pnianie"
  ]
  node [
    id 280
    label "set"
  ]
  node [
    id 281
    label "wyraz"
  ]
  node [
    id 282
    label "wskazywa&#263;"
  ]
  node [
    id 283
    label "signify"
  ]
  node [
    id 284
    label "represent"
  ]
  node [
    id 285
    label "ustala&#263;"
  ]
  node [
    id 286
    label "stanowi&#263;"
  ]
  node [
    id 287
    label "okre&#347;la&#263;"
  ]
  node [
    id 288
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 289
    label "mie&#263;_miejsce"
  ]
  node [
    id 290
    label "equal"
  ]
  node [
    id 291
    label "trwa&#263;"
  ]
  node [
    id 292
    label "chodzi&#263;"
  ]
  node [
    id 293
    label "si&#281;ga&#263;"
  ]
  node [
    id 294
    label "stan"
  ]
  node [
    id 295
    label "obecno&#347;&#263;"
  ]
  node [
    id 296
    label "stand"
  ]
  node [
    id 297
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 298
    label "uczestniczy&#263;"
  ]
  node [
    id 299
    label "warto&#347;&#263;"
  ]
  node [
    id 300
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 301
    label "podkre&#347;la&#263;"
  ]
  node [
    id 302
    label "podawa&#263;"
  ]
  node [
    id 303
    label "pokazywa&#263;"
  ]
  node [
    id 304
    label "wybiera&#263;"
  ]
  node [
    id 305
    label "indicate"
  ]
  node [
    id 306
    label "decydowa&#263;"
  ]
  node [
    id 307
    label "style"
  ]
  node [
    id 308
    label "powodowa&#263;"
  ]
  node [
    id 309
    label "robi&#263;"
  ]
  node [
    id 310
    label "peddle"
  ]
  node [
    id 311
    label "unwrap"
  ]
  node [
    id 312
    label "zmienia&#263;"
  ]
  node [
    id 313
    label "umacnia&#263;"
  ]
  node [
    id 314
    label "arrange"
  ]
  node [
    id 315
    label "decide"
  ]
  node [
    id 316
    label "pies_my&#347;liwski"
  ]
  node [
    id 317
    label "zatrzymywa&#263;"
  ]
  node [
    id 318
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 319
    label "typify"
  ]
  node [
    id 320
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 321
    label "gem"
  ]
  node [
    id 322
    label "runda"
  ]
  node [
    id 323
    label "muzyka"
  ]
  node [
    id 324
    label "zestaw"
  ]
  node [
    id 325
    label "oznaka"
  ]
  node [
    id 326
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 327
    label "posta&#263;"
  ]
  node [
    id 328
    label "element"
  ]
  node [
    id 329
    label "ka&#380;dy"
  ]
  node [
    id 330
    label "jaki&#347;"
  ]
  node [
    id 331
    label "dobija&#263;"
  ]
  node [
    id 332
    label "zakotwiczenie"
  ]
  node [
    id 333
    label "odcumowywa&#263;"
  ]
  node [
    id 334
    label "p&#322;ywa&#263;"
  ]
  node [
    id 335
    label "odkotwicza&#263;"
  ]
  node [
    id 336
    label "zwodowanie"
  ]
  node [
    id 337
    label "odkotwiczy&#263;"
  ]
  node [
    id 338
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 339
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 340
    label "odcumowanie"
  ]
  node [
    id 341
    label "odcumowa&#263;"
  ]
  node [
    id 342
    label "zacumowanie"
  ]
  node [
    id 343
    label "kotwiczenie"
  ]
  node [
    id 344
    label "kad&#322;ub"
  ]
  node [
    id 345
    label "reling"
  ]
  node [
    id 346
    label "kabina"
  ]
  node [
    id 347
    label "kotwiczy&#263;"
  ]
  node [
    id 348
    label "szkutnictwo"
  ]
  node [
    id 349
    label "korab"
  ]
  node [
    id 350
    label "odbijacz"
  ]
  node [
    id 351
    label "dobijanie"
  ]
  node [
    id 352
    label "dobi&#263;"
  ]
  node [
    id 353
    label "proporczyk"
  ]
  node [
    id 354
    label "pok&#322;ad"
  ]
  node [
    id 355
    label "odkotwiczenie"
  ]
  node [
    id 356
    label "kabestan"
  ]
  node [
    id 357
    label "cumowanie"
  ]
  node [
    id 358
    label "zaw&#243;r_denny"
  ]
  node [
    id 359
    label "zadokowa&#263;"
  ]
  node [
    id 360
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 361
    label "flota"
  ]
  node [
    id 362
    label "rostra"
  ]
  node [
    id 363
    label "zr&#281;bnica"
  ]
  node [
    id 364
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 365
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 366
    label "bumsztak"
  ]
  node [
    id 367
    label "sterownik_automatyczny"
  ]
  node [
    id 368
    label "nadbud&#243;wka"
  ]
  node [
    id 369
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 370
    label "cumowa&#263;"
  ]
  node [
    id 371
    label "armator"
  ]
  node [
    id 372
    label "odcumowywanie"
  ]
  node [
    id 373
    label "ster"
  ]
  node [
    id 374
    label "zakotwiczy&#263;"
  ]
  node [
    id 375
    label "zacumowa&#263;"
  ]
  node [
    id 376
    label "wodowanie"
  ]
  node [
    id 377
    label "dobicie"
  ]
  node [
    id 378
    label "zadokowanie"
  ]
  node [
    id 379
    label "dokowa&#263;"
  ]
  node [
    id 380
    label "trap"
  ]
  node [
    id 381
    label "kotwica"
  ]
  node [
    id 382
    label "odkotwiczanie"
  ]
  node [
    id 383
    label "luk"
  ]
  node [
    id 384
    label "dzi&#243;b"
  ]
  node [
    id 385
    label "armada"
  ]
  node [
    id 386
    label "&#380;yroskop"
  ]
  node [
    id 387
    label "futr&#243;wka"
  ]
  node [
    id 388
    label "pojazd"
  ]
  node [
    id 389
    label "sztormtrap"
  ]
  node [
    id 390
    label "skrajnik"
  ]
  node [
    id 391
    label "dokowanie"
  ]
  node [
    id 392
    label "zwodowa&#263;"
  ]
  node [
    id 393
    label "grobla"
  ]
  node [
    id 394
    label "Z&#322;ota_Flota"
  ]
  node [
    id 395
    label "formacja"
  ]
  node [
    id 396
    label "pieni&#261;dze"
  ]
  node [
    id 397
    label "flotylla"
  ]
  node [
    id 398
    label "eskadra"
  ]
  node [
    id 399
    label "marynarka_wojenna"
  ]
  node [
    id 400
    label "odholowa&#263;"
  ]
  node [
    id 401
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 402
    label "tabor"
  ]
  node [
    id 403
    label "przyholowywanie"
  ]
  node [
    id 404
    label "przyholowa&#263;"
  ]
  node [
    id 405
    label "przyholowanie"
  ]
  node [
    id 406
    label "fukni&#281;cie"
  ]
  node [
    id 407
    label "l&#261;d"
  ]
  node [
    id 408
    label "zielona_karta"
  ]
  node [
    id 409
    label "fukanie"
  ]
  node [
    id 410
    label "przyholowywa&#263;"
  ]
  node [
    id 411
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 412
    label "woda"
  ]
  node [
    id 413
    label "przeszklenie"
  ]
  node [
    id 414
    label "test_zderzeniowy"
  ]
  node [
    id 415
    label "powietrze"
  ]
  node [
    id 416
    label "odzywka"
  ]
  node [
    id 417
    label "nadwozie"
  ]
  node [
    id 418
    label "odholowanie"
  ]
  node [
    id 419
    label "prowadzenie_si&#281;"
  ]
  node [
    id 420
    label "odholowywa&#263;"
  ]
  node [
    id 421
    label "pod&#322;oga"
  ]
  node [
    id 422
    label "odholowywanie"
  ]
  node [
    id 423
    label "hamulec"
  ]
  node [
    id 424
    label "podwozie"
  ]
  node [
    id 425
    label "but"
  ]
  node [
    id 426
    label "mur"
  ]
  node [
    id 427
    label "ok&#322;adzina"
  ]
  node [
    id 428
    label "wy&#347;ci&#243;&#322;ka"
  ]
  node [
    id 429
    label "obicie"
  ]
  node [
    id 430
    label "listwa"
  ]
  node [
    id 431
    label "por&#281;cz"
  ]
  node [
    id 432
    label "szafka"
  ]
  node [
    id 433
    label "railing"
  ]
  node [
    id 434
    label "p&#243;&#322;ka"
  ]
  node [
    id 435
    label "barierka"
  ]
  node [
    id 436
    label "flaga"
  ]
  node [
    id 437
    label "flag"
  ]
  node [
    id 438
    label "proporzec"
  ]
  node [
    id 439
    label "tyczka"
  ]
  node [
    id 440
    label "metryczna_jednostka_obj&#281;to&#347;ci"
  ]
  node [
    id 441
    label "sterownica"
  ]
  node [
    id 442
    label "kilometr_sze&#347;cienny"
  ]
  node [
    id 443
    label "wolant"
  ]
  node [
    id 444
    label "powierzchnia_sterowa"
  ]
  node [
    id 445
    label "sterolotka"
  ]
  node [
    id 446
    label "&#380;agl&#243;wka"
  ]
  node [
    id 447
    label "statek_powietrzny"
  ]
  node [
    id 448
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 449
    label "rumpel"
  ]
  node [
    id 450
    label "mechanizm"
  ]
  node [
    id 451
    label "przyw&#243;dztwo"
  ]
  node [
    id 452
    label "jacht"
  ]
  node [
    id 453
    label "schodki"
  ]
  node [
    id 454
    label "accommodation_ladder"
  ]
  node [
    id 455
    label "gyroscope"
  ]
  node [
    id 456
    label "samolot"
  ]
  node [
    id 457
    label "przyrz&#261;d_nawigacyjny"
  ]
  node [
    id 458
    label "ci&#281;gnik"
  ]
  node [
    id 459
    label "wci&#261;garka"
  ]
  node [
    id 460
    label "zr&#261;b"
  ]
  node [
    id 461
    label "otw&#243;r"
  ]
  node [
    id 462
    label "czo&#322;g"
  ]
  node [
    id 463
    label "pomieszczenie"
  ]
  node [
    id 464
    label "winda"
  ]
  node [
    id 465
    label "bombowiec"
  ]
  node [
    id 466
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 467
    label "wagonik"
  ]
  node [
    id 468
    label "narz&#281;dzie"
  ]
  node [
    id 469
    label "emocja"
  ]
  node [
    id 470
    label "wybieranie"
  ]
  node [
    id 471
    label "wybra&#263;"
  ]
  node [
    id 472
    label "wybranie"
  ]
  node [
    id 473
    label "ochrona"
  ]
  node [
    id 474
    label "dobud&#243;wka"
  ]
  node [
    id 475
    label "ptak"
  ]
  node [
    id 476
    label "grzebie&#324;"
  ]
  node [
    id 477
    label "organ"
  ]
  node [
    id 478
    label "struktura_anatomiczna"
  ]
  node [
    id 479
    label "bow"
  ]
  node [
    id 480
    label "ustnik"
  ]
  node [
    id 481
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 482
    label "zako&#324;czenie"
  ]
  node [
    id 483
    label "ostry"
  ]
  node [
    id 484
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 485
    label "blizna"
  ]
  node [
    id 486
    label "dziob&#243;wka"
  ]
  node [
    id 487
    label "drabinka_linowa"
  ]
  node [
    id 488
    label "wa&#322;"
  ]
  node [
    id 489
    label "przegroda"
  ]
  node [
    id 490
    label "trawers"
  ]
  node [
    id 491
    label "kil"
  ]
  node [
    id 492
    label "nadst&#281;pka"
  ]
  node [
    id 493
    label "pachwina"
  ]
  node [
    id 494
    label "brzuch"
  ]
  node [
    id 495
    label "konstrukcyjna_linia_wodna"
  ]
  node [
    id 496
    label "dekolt"
  ]
  node [
    id 497
    label "zad"
  ]
  node [
    id 498
    label "z&#322;ad"
  ]
  node [
    id 499
    label "z&#281;za"
  ]
  node [
    id 500
    label "korpus"
  ]
  node [
    id 501
    label "bok"
  ]
  node [
    id 502
    label "pupa"
  ]
  node [
    id 503
    label "krocze"
  ]
  node [
    id 504
    label "pier&#347;"
  ]
  node [
    id 505
    label "p&#322;atowiec"
  ]
  node [
    id 506
    label "poszycie"
  ]
  node [
    id 507
    label "gr&#243;d&#378;"
  ]
  node [
    id 508
    label "wr&#281;ga"
  ]
  node [
    id 509
    label "maszyna"
  ]
  node [
    id 510
    label "blokownia"
  ]
  node [
    id 511
    label "plecy"
  ]
  node [
    id 512
    label "stojak"
  ]
  node [
    id 513
    label "falszkil"
  ]
  node [
    id 514
    label "klatka_piersiowa"
  ]
  node [
    id 515
    label "biodro"
  ]
  node [
    id 516
    label "pacha"
  ]
  node [
    id 517
    label "podwodzie"
  ]
  node [
    id 518
    label "stewa"
  ]
  node [
    id 519
    label "p&#322;aszczyzna"
  ]
  node [
    id 520
    label "sp&#261;g"
  ]
  node [
    id 521
    label "przestrze&#324;"
  ]
  node [
    id 522
    label "pok&#322;adnik"
  ]
  node [
    id 523
    label "warstwa"
  ]
  node [
    id 524
    label "powierzchnia"
  ]
  node [
    id 525
    label "strop"
  ]
  node [
    id 526
    label "poszycie_pok&#322;adowe"
  ]
  node [
    id 527
    label "kipa"
  ]
  node [
    id 528
    label "p&#243;&#322;pok&#322;ad"
  ]
  node [
    id 529
    label "jut"
  ]
  node [
    id 530
    label "z&#322;o&#380;e"
  ]
  node [
    id 531
    label "zatrzyma&#263;_si&#281;"
  ]
  node [
    id 532
    label "zadomowi&#263;_si&#281;"
  ]
  node [
    id 533
    label "anchor"
  ]
  node [
    id 534
    label "osadzi&#263;"
  ]
  node [
    id 535
    label "przymocowa&#263;"
  ]
  node [
    id 536
    label "przymocowywa&#263;"
  ]
  node [
    id 537
    label "sta&#263;"
  ]
  node [
    id 538
    label "anchorage"
  ]
  node [
    id 539
    label "przymocowywanie"
  ]
  node [
    id 540
    label "stanie"
  ]
  node [
    id 541
    label "zadomowienie_si&#281;"
  ]
  node [
    id 542
    label "przymocowanie"
  ]
  node [
    id 543
    label "osadzenie"
  ]
  node [
    id 544
    label "wyprowadzi&#263;"
  ]
  node [
    id 545
    label "przywi&#261;za&#263;"
  ]
  node [
    id 546
    label "moor"
  ]
  node [
    id 547
    label "zrobi&#263;"
  ]
  node [
    id 548
    label "aerostat"
  ]
  node [
    id 549
    label "cuma"
  ]
  node [
    id 550
    label "wyprowadzenie"
  ]
  node [
    id 551
    label "launching"
  ]
  node [
    id 552
    label "l&#261;dowanie"
  ]
  node [
    id 553
    label "wywodzenie"
  ]
  node [
    id 554
    label "odczepia&#263;"
  ]
  node [
    id 555
    label "rzemios&#322;o"
  ]
  node [
    id 556
    label "impression"
  ]
  node [
    id 557
    label "dokuczenie"
  ]
  node [
    id 558
    label "dorobienie"
  ]
  node [
    id 559
    label "og&#322;oszenie_drukiem"
  ]
  node [
    id 560
    label "nail"
  ]
  node [
    id 561
    label "zawini&#281;cie"
  ]
  node [
    id 562
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 563
    label "doci&#347;ni&#281;cie"
  ]
  node [
    id 564
    label "dotarcie"
  ]
  node [
    id 565
    label "przygn&#281;bienie"
  ]
  node [
    id 566
    label "adjudication"
  ]
  node [
    id 567
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 568
    label "zabicie"
  ]
  node [
    id 569
    label "stacja_kosmiczna"
  ]
  node [
    id 570
    label "wprowadzanie"
  ]
  node [
    id 571
    label "modu&#322;_dokuj&#261;cy"
  ]
  node [
    id 572
    label "przywi&#261;zywanie"
  ]
  node [
    id 573
    label "mooring"
  ]
  node [
    id 574
    label "odcumowanie_si&#281;"
  ]
  node [
    id 575
    label "odczepienie"
  ]
  node [
    id 576
    label "wprowadzenie"
  ]
  node [
    id 577
    label "implantation"
  ]
  node [
    id 578
    label "obsadzenie"
  ]
  node [
    id 579
    label "przywi&#261;zywa&#263;"
  ]
  node [
    id 580
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 581
    label "zawijanie"
  ]
  node [
    id 582
    label "dociskanie"
  ]
  node [
    id 583
    label "zabijanie"
  ]
  node [
    id 584
    label "dop&#322;ywanie"
  ]
  node [
    id 585
    label "docieranie"
  ]
  node [
    id 586
    label "dokuczanie"
  ]
  node [
    id 587
    label "przygn&#281;bianie"
  ]
  node [
    id 588
    label "podmiot_gospodarczy"
  ]
  node [
    id 589
    label "przedsi&#281;biorca"
  ]
  node [
    id 590
    label "&#380;eglugowiec"
  ]
  node [
    id 591
    label "przywi&#261;zanie"
  ]
  node [
    id 592
    label "doprowadzi&#263;"
  ]
  node [
    id 593
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 594
    label "dopisa&#263;"
  ]
  node [
    id 595
    label "nabi&#263;"
  ]
  node [
    id 596
    label "get_through"
  ]
  node [
    id 597
    label "przybi&#263;"
  ]
  node [
    id 598
    label "popsu&#263;"
  ]
  node [
    id 599
    label "wybi&#263;"
  ]
  node [
    id 600
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 601
    label "pogorszy&#263;"
  ]
  node [
    id 602
    label "zabi&#263;"
  ]
  node [
    id 603
    label "wbi&#263;"
  ]
  node [
    id 604
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 605
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 606
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 607
    label "dorobi&#263;"
  ]
  node [
    id 608
    label "doj&#347;&#263;"
  ]
  node [
    id 609
    label "za&#322;ama&#263;"
  ]
  node [
    id 610
    label "dopi&#261;&#263;"
  ]
  node [
    id 611
    label "dotrze&#263;"
  ]
  node [
    id 612
    label "pogr&#261;&#380;y&#263;"
  ]
  node [
    id 613
    label "dock"
  ]
  node [
    id 614
    label "wprowadza&#263;"
  ]
  node [
    id 615
    label "drukarz"
  ]
  node [
    id 616
    label "odczepianie"
  ]
  node [
    id 617
    label "odcumowywanie_si&#281;"
  ]
  node [
    id 618
    label "odczepi&#263;"
  ]
  node [
    id 619
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 620
    label "sterowa&#263;"
  ]
  node [
    id 621
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 622
    label "ciecz"
  ]
  node [
    id 623
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 624
    label "mie&#263;"
  ]
  node [
    id 625
    label "m&#243;wi&#263;"
  ]
  node [
    id 626
    label "lata&#263;"
  ]
  node [
    id 627
    label "swimming"
  ]
  node [
    id 628
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 629
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 630
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 631
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 632
    label "pracowa&#263;"
  ]
  node [
    id 633
    label "sink"
  ]
  node [
    id 634
    label "zanika&#263;"
  ]
  node [
    id 635
    label "falowa&#263;"
  ]
  node [
    id 636
    label "dr&#261;g"
  ]
  node [
    id 637
    label "dais"
  ]
  node [
    id 638
    label "zdobienie"
  ]
  node [
    id 639
    label "dopisywa&#263;"
  ]
  node [
    id 640
    label "psu&#263;"
  ]
  node [
    id 641
    label "wyko&#324;cza&#263;"
  ]
  node [
    id 642
    label "osi&#261;ga&#263;"
  ]
  node [
    id 643
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 644
    label "wbija&#263;"
  ]
  node [
    id 645
    label "wybija&#263;"
  ]
  node [
    id 646
    label "doprowadza&#263;"
  ]
  node [
    id 647
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 648
    label "za&#322;amywa&#263;"
  ]
  node [
    id 649
    label "dorabia&#263;"
  ]
  node [
    id 650
    label "pogr&#261;&#380;a&#263;"
  ]
  node [
    id 651
    label "dociera&#263;"
  ]
  node [
    id 652
    label "nabija&#263;"
  ]
  node [
    id 653
    label "dochodzi&#263;"
  ]
  node [
    id 654
    label "dopina&#263;"
  ]
  node [
    id 655
    label "zabija&#263;"
  ]
  node [
    id 656
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 657
    label "przybija&#263;"
  ]
  node [
    id 658
    label "pogarsza&#263;"
  ]
  node [
    id 659
    label "obsadzi&#263;"
  ]
  node [
    id 660
    label "cuddle"
  ]
  node [
    id 661
    label "wprowadzi&#263;"
  ]
  node [
    id 662
    label "zielony"
  ]
  node [
    id 663
    label "nadmorski"
  ]
  node [
    id 664
    label "niebieski"
  ]
  node [
    id 665
    label "typowy"
  ]
  node [
    id 666
    label "przypominaj&#261;cy"
  ]
  node [
    id 667
    label "morsko"
  ]
  node [
    id 668
    label "wodny"
  ]
  node [
    id 669
    label "s&#322;ony"
  ]
  node [
    id 670
    label "specjalny"
  ]
  node [
    id 671
    label "podobny"
  ]
  node [
    id 672
    label "zbli&#380;ony"
  ]
  node [
    id 673
    label "s&#322;ono"
  ]
  node [
    id 674
    label "mineralny"
  ]
  node [
    id 675
    label "wysoki"
  ]
  node [
    id 676
    label "wyg&#243;rowany"
  ]
  node [
    id 677
    label "zdarcie"
  ]
  node [
    id 678
    label "dotkliwy"
  ]
  node [
    id 679
    label "zdzieranie"
  ]
  node [
    id 680
    label "&#347;wie&#380;y"
  ]
  node [
    id 681
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 682
    label "zwyczajny"
  ]
  node [
    id 683
    label "typowo"
  ]
  node [
    id 684
    label "cz&#281;sty"
  ]
  node [
    id 685
    label "zwyk&#322;y"
  ]
  node [
    id 686
    label "intencjonalny"
  ]
  node [
    id 687
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 688
    label "niedorozw&#243;j"
  ]
  node [
    id 689
    label "szczeg&#243;lny"
  ]
  node [
    id 690
    label "specjalnie"
  ]
  node [
    id 691
    label "nieetatowy"
  ]
  node [
    id 692
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 693
    label "nienormalny"
  ]
  node [
    id 694
    label "umy&#347;lnie"
  ]
  node [
    id 695
    label "odpowiedni"
  ]
  node [
    id 696
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 697
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 698
    label "dolar"
  ]
  node [
    id 699
    label "USA"
  ]
  node [
    id 700
    label "majny"
  ]
  node [
    id 701
    label "Ekwador"
  ]
  node [
    id 702
    label "Bonaire"
  ]
  node [
    id 703
    label "Sint_Eustatius"
  ]
  node [
    id 704
    label "zzielenienie"
  ]
  node [
    id 705
    label "zielono"
  ]
  node [
    id 706
    label "Portoryko"
  ]
  node [
    id 707
    label "dzia&#322;acz"
  ]
  node [
    id 708
    label "zazielenianie"
  ]
  node [
    id 709
    label "Panama"
  ]
  node [
    id 710
    label "Mikronezja"
  ]
  node [
    id 711
    label "zazielenienie"
  ]
  node [
    id 712
    label "niedojrza&#322;y"
  ]
  node [
    id 713
    label "pokryty"
  ]
  node [
    id 714
    label "socjalista"
  ]
  node [
    id 715
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 716
    label "Saba"
  ]
  node [
    id 717
    label "zwolennik"
  ]
  node [
    id 718
    label "Timor_Wschodni"
  ]
  node [
    id 719
    label "polityk"
  ]
  node [
    id 720
    label "zieloni"
  ]
  node [
    id 721
    label "Wyspy_Marshalla"
  ]
  node [
    id 722
    label "naturalny"
  ]
  node [
    id 723
    label "Palau"
  ]
  node [
    id 724
    label "Zimbabwe"
  ]
  node [
    id 725
    label "blady"
  ]
  node [
    id 726
    label "zielenienie"
  ]
  node [
    id 727
    label "&#380;ywy"
  ]
  node [
    id 728
    label "ch&#322;odny"
  ]
  node [
    id 729
    label "Salwador"
  ]
  node [
    id 730
    label "niebieszczenie"
  ]
  node [
    id 731
    label "niebiesko"
  ]
  node [
    id 732
    label "siny"
  ]
  node [
    id 733
    label "podniebnie"
  ]
  node [
    id 734
    label "powietrznie"
  ]
  node [
    id 735
    label "lekki"
  ]
  node [
    id 736
    label "podniebieski"
  ]
  node [
    id 737
    label "napowietrzny"
  ]
  node [
    id 738
    label "lekko"
  ]
  node [
    id 739
    label "&#322;atwo"
  ]
  node [
    id 740
    label "delikatny"
  ]
  node [
    id 741
    label "przewiewny"
  ]
  node [
    id 742
    label "nieg&#322;&#281;boko"
  ]
  node [
    id 743
    label "subtelny"
  ]
  node [
    id 744
    label "bezpieczny"
  ]
  node [
    id 745
    label "polotny"
  ]
  node [
    id 746
    label "przyswajalny"
  ]
  node [
    id 747
    label "beztrosko"
  ]
  node [
    id 748
    label "&#322;atwy"
  ]
  node [
    id 749
    label "piaszczysty"
  ]
  node [
    id 750
    label "suchy"
  ]
  node [
    id 751
    label "letki"
  ]
  node [
    id 752
    label "p&#322;ynny"
  ]
  node [
    id 753
    label "dietetyczny"
  ]
  node [
    id 754
    label "lekkozbrojny"
  ]
  node [
    id 755
    label "delikatnie"
  ]
  node [
    id 756
    label "s&#322;aby"
  ]
  node [
    id 757
    label "&#322;acny"
  ]
  node [
    id 758
    label "snadny"
  ]
  node [
    id 759
    label "nierozwa&#380;ny"
  ]
  node [
    id 760
    label "g&#322;adko"
  ]
  node [
    id 761
    label "zgrabny"
  ]
  node [
    id 762
    label "przyjemny"
  ]
  node [
    id 763
    label "ubogi"
  ]
  node [
    id 764
    label "zwinnie"
  ]
  node [
    id 765
    label "beztroskliwy"
  ]
  node [
    id 766
    label "zr&#281;czny"
  ]
  node [
    id 767
    label "prosty"
  ]
  node [
    id 768
    label "cienki"
  ]
  node [
    id 769
    label "nieznacznie"
  ]
  node [
    id 770
    label "punkt"
  ]
  node [
    id 771
    label "spos&#243;b"
  ]
  node [
    id 772
    label "abstrakcja"
  ]
  node [
    id 773
    label "chemikalia"
  ]
  node [
    id 774
    label "substancja"
  ]
  node [
    id 775
    label "model"
  ]
  node [
    id 776
    label "tryb"
  ]
  node [
    id 777
    label "nature"
  ]
  node [
    id 778
    label "po&#322;o&#380;enie"
  ]
  node [
    id 779
    label "sprawa"
  ]
  node [
    id 780
    label "ust&#281;p"
  ]
  node [
    id 781
    label "plan"
  ]
  node [
    id 782
    label "obiekt_matematyczny"
  ]
  node [
    id 783
    label "problemat"
  ]
  node [
    id 784
    label "plamka"
  ]
  node [
    id 785
    label "stopie&#324;_pisma"
  ]
  node [
    id 786
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 787
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 788
    label "mark"
  ]
  node [
    id 789
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 790
    label "prosta"
  ]
  node [
    id 791
    label "problematyka"
  ]
  node [
    id 792
    label "obiekt"
  ]
  node [
    id 793
    label "zapunktowa&#263;"
  ]
  node [
    id 794
    label "podpunkt"
  ]
  node [
    id 795
    label "wojsko"
  ]
  node [
    id 796
    label "kres"
  ]
  node [
    id 797
    label "point"
  ]
  node [
    id 798
    label "pozycja"
  ]
  node [
    id 799
    label "warunek_lokalowy"
  ]
  node [
    id 800
    label "plac"
  ]
  node [
    id 801
    label "location"
  ]
  node [
    id 802
    label "uwaga"
  ]
  node [
    id 803
    label "status"
  ]
  node [
    id 804
    label "cia&#322;o"
  ]
  node [
    id 805
    label "praca"
  ]
  node [
    id 806
    label "rz&#261;d"
  ]
  node [
    id 807
    label "przenikanie"
  ]
  node [
    id 808
    label "byt"
  ]
  node [
    id 809
    label "materia"
  ]
  node [
    id 810
    label "temperatura_krytyczna"
  ]
  node [
    id 811
    label "przenika&#263;"
  ]
  node [
    id 812
    label "smolisty"
  ]
  node [
    id 813
    label "proces_my&#347;lowy"
  ]
  node [
    id 814
    label "abstractedness"
  ]
  node [
    id 815
    label "abstraction"
  ]
  node [
    id 816
    label "obraz"
  ]
  node [
    id 817
    label "sytuacja"
  ]
  node [
    id 818
    label "spalenie"
  ]
  node [
    id 819
    label "spalanie"
  ]
  node [
    id 820
    label "komunikacyjny"
  ]
  node [
    id 821
    label "dogodny"
  ]
  node [
    id 822
    label "motorowy"
  ]
  node [
    id 823
    label "mechaniczny"
  ]
  node [
    id 824
    label "wydobywa&#263;"
  ]
  node [
    id 825
    label "wyzyskiwa&#263;"
  ]
  node [
    id 826
    label "use"
  ]
  node [
    id 827
    label "u&#380;ywa&#263;"
  ]
  node [
    id 828
    label "korzysta&#263;"
  ]
  node [
    id 829
    label "distribute"
  ]
  node [
    id 830
    label "give"
  ]
  node [
    id 831
    label "bash"
  ]
  node [
    id 832
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 833
    label "doznawa&#263;"
  ]
  node [
    id 834
    label "wykorzystywa&#263;"
  ]
  node [
    id 835
    label "feat"
  ]
  node [
    id 836
    label "uwydatnia&#263;"
  ]
  node [
    id 837
    label "uzyskiwa&#263;"
  ]
  node [
    id 838
    label "wydostawa&#263;"
  ]
  node [
    id 839
    label "wyjmowa&#263;"
  ]
  node [
    id 840
    label "train"
  ]
  node [
    id 841
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 842
    label "wydawa&#263;"
  ]
  node [
    id 843
    label "dobywa&#263;"
  ]
  node [
    id 844
    label "ocala&#263;"
  ]
  node [
    id 845
    label "excavate"
  ]
  node [
    id 846
    label "g&#243;rnictwo"
  ]
  node [
    id 847
    label "raise"
  ]
  node [
    id 848
    label "HP"
  ]
  node [
    id 849
    label "Google"
  ]
  node [
    id 850
    label "Apeks"
  ]
  node [
    id 851
    label "networking"
  ]
  node [
    id 852
    label "Pewex"
  ]
  node [
    id 853
    label "zasoby"
  ]
  node [
    id 854
    label "Canon"
  ]
  node [
    id 855
    label "MAC"
  ]
  node [
    id 856
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 857
    label "interes"
  ]
  node [
    id 858
    label "Hortex"
  ]
  node [
    id 859
    label "reengineering"
  ]
  node [
    id 860
    label "MAN_SE"
  ]
  node [
    id 861
    label "zasoby_ludzkie"
  ]
  node [
    id 862
    label "Orlen"
  ]
  node [
    id 863
    label "Baltona"
  ]
  node [
    id 864
    label "Orbis"
  ]
  node [
    id 865
    label "Spo&#322;em"
  ]
  node [
    id 866
    label "zasoby_kopalin"
  ]
  node [
    id 867
    label "informatyka"
  ]
  node [
    id 868
    label "ropa_naftowa"
  ]
  node [
    id 869
    label "paliwo"
  ]
  node [
    id 870
    label "firma"
  ]
  node [
    id 871
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 872
    label "driveway"
  ]
  node [
    id 873
    label "object"
  ]
  node [
    id 874
    label "zaleta"
  ]
  node [
    id 875
    label "dobro"
  ]
  node [
    id 876
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 877
    label "przer&#243;bka"
  ]
  node [
    id 878
    label "odmienienie"
  ]
  node [
    id 879
    label "strategia"
  ]
  node [
    id 880
    label "oprogramowanie"
  ]
  node [
    id 881
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 882
    label "porozumiewa&#263;_si&#281;"
  ]
  node [
    id 883
    label "ask"
  ]
  node [
    id 884
    label "kontaktowa&#263;"
  ]
  node [
    id 885
    label "agree"
  ]
  node [
    id 886
    label "reach"
  ]
  node [
    id 887
    label "m&#243;c"
  ]
  node [
    id 888
    label "przytomno&#347;&#263;"
  ]
  node [
    id 889
    label "rozumie&#263;"
  ]
  node [
    id 890
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 891
    label "funkcjonowa&#263;"
  ]
  node [
    id 892
    label "contact"
  ]
  node [
    id 893
    label "Katar"
  ]
  node [
    id 894
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 895
    label "Libia"
  ]
  node [
    id 896
    label "Gwatemala"
  ]
  node [
    id 897
    label "Afganistan"
  ]
  node [
    id 898
    label "Tad&#380;ykistan"
  ]
  node [
    id 899
    label "Bhutan"
  ]
  node [
    id 900
    label "Argentyna"
  ]
  node [
    id 901
    label "D&#380;ibuti"
  ]
  node [
    id 902
    label "Wenezuela"
  ]
  node [
    id 903
    label "Ukraina"
  ]
  node [
    id 904
    label "Gabon"
  ]
  node [
    id 905
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 906
    label "Rwanda"
  ]
  node [
    id 907
    label "Liechtenstein"
  ]
  node [
    id 908
    label "organizacja"
  ]
  node [
    id 909
    label "Sri_Lanka"
  ]
  node [
    id 910
    label "Madagaskar"
  ]
  node [
    id 911
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 912
    label "Tonga"
  ]
  node [
    id 913
    label "Kongo"
  ]
  node [
    id 914
    label "Bangladesz"
  ]
  node [
    id 915
    label "Kanada"
  ]
  node [
    id 916
    label "Wehrlen"
  ]
  node [
    id 917
    label "Algieria"
  ]
  node [
    id 918
    label "Surinam"
  ]
  node [
    id 919
    label "Chile"
  ]
  node [
    id 920
    label "Sahara_Zachodnia"
  ]
  node [
    id 921
    label "Uganda"
  ]
  node [
    id 922
    label "W&#281;gry"
  ]
  node [
    id 923
    label "Birma"
  ]
  node [
    id 924
    label "Kazachstan"
  ]
  node [
    id 925
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 926
    label "Armenia"
  ]
  node [
    id 927
    label "Tuwalu"
  ]
  node [
    id 928
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 929
    label "Izrael"
  ]
  node [
    id 930
    label "Estonia"
  ]
  node [
    id 931
    label "Komory"
  ]
  node [
    id 932
    label "Kamerun"
  ]
  node [
    id 933
    label "Haiti"
  ]
  node [
    id 934
    label "Belize"
  ]
  node [
    id 935
    label "Sierra_Leone"
  ]
  node [
    id 936
    label "Luksemburg"
  ]
  node [
    id 937
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 938
    label "Barbados"
  ]
  node [
    id 939
    label "San_Marino"
  ]
  node [
    id 940
    label "Bu&#322;garia"
  ]
  node [
    id 941
    label "Wietnam"
  ]
  node [
    id 942
    label "Indonezja"
  ]
  node [
    id 943
    label "Malawi"
  ]
  node [
    id 944
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 945
    label "Francja"
  ]
  node [
    id 946
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 947
    label "partia"
  ]
  node [
    id 948
    label "Zambia"
  ]
  node [
    id 949
    label "Angola"
  ]
  node [
    id 950
    label "Grenada"
  ]
  node [
    id 951
    label "Nepal"
  ]
  node [
    id 952
    label "Rumunia"
  ]
  node [
    id 953
    label "Czarnog&#243;ra"
  ]
  node [
    id 954
    label "Malediwy"
  ]
  node [
    id 955
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 956
    label "S&#322;owacja"
  ]
  node [
    id 957
    label "para"
  ]
  node [
    id 958
    label "Egipt"
  ]
  node [
    id 959
    label "zwrot"
  ]
  node [
    id 960
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 961
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 962
    label "Kolumbia"
  ]
  node [
    id 963
    label "Mozambik"
  ]
  node [
    id 964
    label "Laos"
  ]
  node [
    id 965
    label "Burundi"
  ]
  node [
    id 966
    label "Suazi"
  ]
  node [
    id 967
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 968
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 969
    label "Czechy"
  ]
  node [
    id 970
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 971
    label "Trynidad_i_Tobago"
  ]
  node [
    id 972
    label "Dominika"
  ]
  node [
    id 973
    label "Syria"
  ]
  node [
    id 974
    label "Gwinea_Bissau"
  ]
  node [
    id 975
    label "Liberia"
  ]
  node [
    id 976
    label "Polska"
  ]
  node [
    id 977
    label "Jamajka"
  ]
  node [
    id 978
    label "Dominikana"
  ]
  node [
    id 979
    label "Senegal"
  ]
  node [
    id 980
    label "Gruzja"
  ]
  node [
    id 981
    label "Togo"
  ]
  node [
    id 982
    label "Chorwacja"
  ]
  node [
    id 983
    label "Meksyk"
  ]
  node [
    id 984
    label "Macedonia"
  ]
  node [
    id 985
    label "Gujana"
  ]
  node [
    id 986
    label "Zair"
  ]
  node [
    id 987
    label "Albania"
  ]
  node [
    id 988
    label "Kambod&#380;a"
  ]
  node [
    id 989
    label "Mauritius"
  ]
  node [
    id 990
    label "Monako"
  ]
  node [
    id 991
    label "Gwinea"
  ]
  node [
    id 992
    label "Mali"
  ]
  node [
    id 993
    label "Nigeria"
  ]
  node [
    id 994
    label "Kostaryka"
  ]
  node [
    id 995
    label "Hanower"
  ]
  node [
    id 996
    label "Paragwaj"
  ]
  node [
    id 997
    label "W&#322;ochy"
  ]
  node [
    id 998
    label "Wyspy_Salomona"
  ]
  node [
    id 999
    label "Seszele"
  ]
  node [
    id 1000
    label "Hiszpania"
  ]
  node [
    id 1001
    label "Boliwia"
  ]
  node [
    id 1002
    label "Kirgistan"
  ]
  node [
    id 1003
    label "Irlandia"
  ]
  node [
    id 1004
    label "Czad"
  ]
  node [
    id 1005
    label "Irak"
  ]
  node [
    id 1006
    label "Lesoto"
  ]
  node [
    id 1007
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1008
    label "Malta"
  ]
  node [
    id 1009
    label "Andora"
  ]
  node [
    id 1010
    label "Chiny"
  ]
  node [
    id 1011
    label "Filipiny"
  ]
  node [
    id 1012
    label "Antarktis"
  ]
  node [
    id 1013
    label "Niemcy"
  ]
  node [
    id 1014
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1015
    label "Brazylia"
  ]
  node [
    id 1016
    label "terytorium"
  ]
  node [
    id 1017
    label "Nikaragua"
  ]
  node [
    id 1018
    label "Pakistan"
  ]
  node [
    id 1019
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1020
    label "Kenia"
  ]
  node [
    id 1021
    label "Niger"
  ]
  node [
    id 1022
    label "Tunezja"
  ]
  node [
    id 1023
    label "Portugalia"
  ]
  node [
    id 1024
    label "Fid&#380;i"
  ]
  node [
    id 1025
    label "Maroko"
  ]
  node [
    id 1026
    label "Botswana"
  ]
  node [
    id 1027
    label "Tajlandia"
  ]
  node [
    id 1028
    label "Australia"
  ]
  node [
    id 1029
    label "Burkina_Faso"
  ]
  node [
    id 1030
    label "interior"
  ]
  node [
    id 1031
    label "Benin"
  ]
  node [
    id 1032
    label "Tanzania"
  ]
  node [
    id 1033
    label "Indie"
  ]
  node [
    id 1034
    label "&#321;otwa"
  ]
  node [
    id 1035
    label "Kiribati"
  ]
  node [
    id 1036
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1037
    label "Rodezja"
  ]
  node [
    id 1038
    label "Cypr"
  ]
  node [
    id 1039
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1040
    label "Peru"
  ]
  node [
    id 1041
    label "Austria"
  ]
  node [
    id 1042
    label "Urugwaj"
  ]
  node [
    id 1043
    label "Jordania"
  ]
  node [
    id 1044
    label "Grecja"
  ]
  node [
    id 1045
    label "Azerbejd&#380;an"
  ]
  node [
    id 1046
    label "Turcja"
  ]
  node [
    id 1047
    label "Samoa"
  ]
  node [
    id 1048
    label "Sudan"
  ]
  node [
    id 1049
    label "Oman"
  ]
  node [
    id 1050
    label "ziemia"
  ]
  node [
    id 1051
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1052
    label "Uzbekistan"
  ]
  node [
    id 1053
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1054
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1055
    label "Honduras"
  ]
  node [
    id 1056
    label "Mongolia"
  ]
  node [
    id 1057
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1058
    label "Serbia"
  ]
  node [
    id 1059
    label "Tajwan"
  ]
  node [
    id 1060
    label "Wielka_Brytania"
  ]
  node [
    id 1061
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1062
    label "Liban"
  ]
  node [
    id 1063
    label "Japonia"
  ]
  node [
    id 1064
    label "Ghana"
  ]
  node [
    id 1065
    label "Bahrajn"
  ]
  node [
    id 1066
    label "Belgia"
  ]
  node [
    id 1067
    label "Etiopia"
  ]
  node [
    id 1068
    label "Kuwejt"
  ]
  node [
    id 1069
    label "Bahamy"
  ]
  node [
    id 1070
    label "Rosja"
  ]
  node [
    id 1071
    label "Mo&#322;dawia"
  ]
  node [
    id 1072
    label "Litwa"
  ]
  node [
    id 1073
    label "S&#322;owenia"
  ]
  node [
    id 1074
    label "Szwajcaria"
  ]
  node [
    id 1075
    label "Erytrea"
  ]
  node [
    id 1076
    label "Kuba"
  ]
  node [
    id 1077
    label "Arabia_Saudyjska"
  ]
  node [
    id 1078
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1079
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1080
    label "Malezja"
  ]
  node [
    id 1081
    label "Korea"
  ]
  node [
    id 1082
    label "Jemen"
  ]
  node [
    id 1083
    label "Nowa_Zelandia"
  ]
  node [
    id 1084
    label "Namibia"
  ]
  node [
    id 1085
    label "Nauru"
  ]
  node [
    id 1086
    label "holoarktyka"
  ]
  node [
    id 1087
    label "Brunei"
  ]
  node [
    id 1088
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1089
    label "Khitai"
  ]
  node [
    id 1090
    label "Mauretania"
  ]
  node [
    id 1091
    label "Iran"
  ]
  node [
    id 1092
    label "Gambia"
  ]
  node [
    id 1093
    label "Somalia"
  ]
  node [
    id 1094
    label "Holandia"
  ]
  node [
    id 1095
    label "Turkmenistan"
  ]
  node [
    id 1096
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1097
    label "pair"
  ]
  node [
    id 1098
    label "zesp&#243;&#322;"
  ]
  node [
    id 1099
    label "odparowywanie"
  ]
  node [
    id 1100
    label "gaz_cieplarniany"
  ]
  node [
    id 1101
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1102
    label "poker"
  ]
  node [
    id 1103
    label "moneta"
  ]
  node [
    id 1104
    label "parowanie"
  ]
  node [
    id 1105
    label "damp"
  ]
  node [
    id 1106
    label "nale&#380;e&#263;"
  ]
  node [
    id 1107
    label "sztuka"
  ]
  node [
    id 1108
    label "odparowanie"
  ]
  node [
    id 1109
    label "odparowa&#263;"
  ]
  node [
    id 1110
    label "dodatek"
  ]
  node [
    id 1111
    label "jednostka_monetarna"
  ]
  node [
    id 1112
    label "smoke"
  ]
  node [
    id 1113
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1114
    label "odparowywa&#263;"
  ]
  node [
    id 1115
    label "uk&#322;ad"
  ]
  node [
    id 1116
    label "gaz"
  ]
  node [
    id 1117
    label "wyparowanie"
  ]
  node [
    id 1118
    label "obszar"
  ]
  node [
    id 1119
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1120
    label "jednostka_administracyjna"
  ]
  node [
    id 1121
    label "Jukon"
  ]
  node [
    id 1122
    label "podmiot"
  ]
  node [
    id 1123
    label "jednostka_organizacyjna"
  ]
  node [
    id 1124
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1125
    label "TOPR"
  ]
  node [
    id 1126
    label "endecki"
  ]
  node [
    id 1127
    label "przedstawicielstwo"
  ]
  node [
    id 1128
    label "od&#322;am"
  ]
  node [
    id 1129
    label "Cepelia"
  ]
  node [
    id 1130
    label "ZBoWiD"
  ]
  node [
    id 1131
    label "organization"
  ]
  node [
    id 1132
    label "centrala"
  ]
  node [
    id 1133
    label "GOPR"
  ]
  node [
    id 1134
    label "ZOMO"
  ]
  node [
    id 1135
    label "ZMP"
  ]
  node [
    id 1136
    label "komitet_koordynacyjny"
  ]
  node [
    id 1137
    label "przybud&#243;wka"
  ]
  node [
    id 1138
    label "boj&#243;wka"
  ]
  node [
    id 1139
    label "turn"
  ]
  node [
    id 1140
    label "turning"
  ]
  node [
    id 1141
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1142
    label "skr&#281;t"
  ]
  node [
    id 1143
    label "obr&#243;t"
  ]
  node [
    id 1144
    label "fraza_czasownikowa"
  ]
  node [
    id 1145
    label "zmiana"
  ]
  node [
    id 1146
    label "Bund"
  ]
  node [
    id 1147
    label "PPR"
  ]
  node [
    id 1148
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1149
    label "wybranek"
  ]
  node [
    id 1150
    label "Jakobici"
  ]
  node [
    id 1151
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1152
    label "SLD"
  ]
  node [
    id 1153
    label "Razem"
  ]
  node [
    id 1154
    label "PiS"
  ]
  node [
    id 1155
    label "package"
  ]
  node [
    id 1156
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1157
    label "Kuomintang"
  ]
  node [
    id 1158
    label "ZSL"
  ]
  node [
    id 1159
    label "AWS"
  ]
  node [
    id 1160
    label "gra"
  ]
  node [
    id 1161
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1162
    label "game"
  ]
  node [
    id 1163
    label "blok"
  ]
  node [
    id 1164
    label "materia&#322;"
  ]
  node [
    id 1165
    label "PO"
  ]
  node [
    id 1166
    label "si&#322;a"
  ]
  node [
    id 1167
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1168
    label "niedoczas"
  ]
  node [
    id 1169
    label "Federali&#347;ci"
  ]
  node [
    id 1170
    label "PSL"
  ]
  node [
    id 1171
    label "Wigowie"
  ]
  node [
    id 1172
    label "ZChN"
  ]
  node [
    id 1173
    label "egzekutywa"
  ]
  node [
    id 1174
    label "aktyw"
  ]
  node [
    id 1175
    label "wybranka"
  ]
  node [
    id 1176
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1177
    label "unit"
  ]
  node [
    id 1178
    label "biom"
  ]
  node [
    id 1179
    label "szata_ro&#347;linna"
  ]
  node [
    id 1180
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1181
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1182
    label "przyroda"
  ]
  node [
    id 1183
    label "zielono&#347;&#263;"
  ]
  node [
    id 1184
    label "pi&#281;tro"
  ]
  node [
    id 1185
    label "plant"
  ]
  node [
    id 1186
    label "ro&#347;lina"
  ]
  node [
    id 1187
    label "geosystem"
  ]
  node [
    id 1188
    label "teren"
  ]
  node [
    id 1189
    label "inti"
  ]
  node [
    id 1190
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1191
    label "sol"
  ]
  node [
    id 1192
    label "Afryka_Zachodnia"
  ]
  node [
    id 1193
    label "baht"
  ]
  node [
    id 1194
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1195
    label "boliviano"
  ]
  node [
    id 1196
    label "dong"
  ]
  node [
    id 1197
    label "Annam"
  ]
  node [
    id 1198
    label "Tonkin"
  ]
  node [
    id 1199
    label "colon"
  ]
  node [
    id 1200
    label "Ameryka_Centralna"
  ]
  node [
    id 1201
    label "Piemont"
  ]
  node [
    id 1202
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1203
    label "NATO"
  ]
  node [
    id 1204
    label "Italia"
  ]
  node [
    id 1205
    label "Kalabria"
  ]
  node [
    id 1206
    label "Sardynia"
  ]
  node [
    id 1207
    label "Apulia"
  ]
  node [
    id 1208
    label "strefa_euro"
  ]
  node [
    id 1209
    label "Ok&#281;cie"
  ]
  node [
    id 1210
    label "Karyntia"
  ]
  node [
    id 1211
    label "Umbria"
  ]
  node [
    id 1212
    label "Romania"
  ]
  node [
    id 1213
    label "Sycylia"
  ]
  node [
    id 1214
    label "Warszawa"
  ]
  node [
    id 1215
    label "lir"
  ]
  node [
    id 1216
    label "Toskania"
  ]
  node [
    id 1217
    label "Lombardia"
  ]
  node [
    id 1218
    label "Liguria"
  ]
  node [
    id 1219
    label "Kampania"
  ]
  node [
    id 1220
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1221
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1222
    label "Ad&#380;aria"
  ]
  node [
    id 1223
    label "lari"
  ]
  node [
    id 1224
    label "Jukatan"
  ]
  node [
    id 1225
    label "dolar_Belize"
  ]
  node [
    id 1226
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1227
    label "Ohio"
  ]
  node [
    id 1228
    label "P&#243;&#322;noc"
  ]
  node [
    id 1229
    label "Nowy_York"
  ]
  node [
    id 1230
    label "Illinois"
  ]
  node [
    id 1231
    label "Po&#322;udnie"
  ]
  node [
    id 1232
    label "Kalifornia"
  ]
  node [
    id 1233
    label "Wirginia"
  ]
  node [
    id 1234
    label "Teksas"
  ]
  node [
    id 1235
    label "Waszyngton"
  ]
  node [
    id 1236
    label "Massachusetts"
  ]
  node [
    id 1237
    label "Alaska"
  ]
  node [
    id 1238
    label "Hawaje"
  ]
  node [
    id 1239
    label "Maryland"
  ]
  node [
    id 1240
    label "Michigan"
  ]
  node [
    id 1241
    label "Arizona"
  ]
  node [
    id 1242
    label "Georgia"
  ]
  node [
    id 1243
    label "stan_wolny"
  ]
  node [
    id 1244
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1245
    label "Pensylwania"
  ]
  node [
    id 1246
    label "Luizjana"
  ]
  node [
    id 1247
    label "Nowy_Meksyk"
  ]
  node [
    id 1248
    label "Wuj_Sam"
  ]
  node [
    id 1249
    label "Alabama"
  ]
  node [
    id 1250
    label "Kansas"
  ]
  node [
    id 1251
    label "Oregon"
  ]
  node [
    id 1252
    label "Zach&#243;d"
  ]
  node [
    id 1253
    label "Floryda"
  ]
  node [
    id 1254
    label "Oklahoma"
  ]
  node [
    id 1255
    label "Hudson"
  ]
  node [
    id 1256
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1257
    label "somoni"
  ]
  node [
    id 1258
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1259
    label "euro"
  ]
  node [
    id 1260
    label "Sand&#380;ak"
  ]
  node [
    id 1261
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1262
    label "perper"
  ]
  node [
    id 1263
    label "Bengal"
  ]
  node [
    id 1264
    label "taka"
  ]
  node [
    id 1265
    label "Karelia"
  ]
  node [
    id 1266
    label "Mari_El"
  ]
  node [
    id 1267
    label "Inguszetia"
  ]
  node [
    id 1268
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1269
    label "Udmurcja"
  ]
  node [
    id 1270
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1271
    label "Newa"
  ]
  node [
    id 1272
    label "&#321;adoga"
  ]
  node [
    id 1273
    label "Czeczenia"
  ]
  node [
    id 1274
    label "Anadyr"
  ]
  node [
    id 1275
    label "Syberia"
  ]
  node [
    id 1276
    label "Tatarstan"
  ]
  node [
    id 1277
    label "Wszechrosja"
  ]
  node [
    id 1278
    label "Azja"
  ]
  node [
    id 1279
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1280
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1281
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1282
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1283
    label "Europa_Wschodnia"
  ]
  node [
    id 1284
    label "Baszkiria"
  ]
  node [
    id 1285
    label "Kamczatka"
  ]
  node [
    id 1286
    label "Jama&#322;"
  ]
  node [
    id 1287
    label "Dagestan"
  ]
  node [
    id 1288
    label "Witim"
  ]
  node [
    id 1289
    label "Tuwa"
  ]
  node [
    id 1290
    label "car"
  ]
  node [
    id 1291
    label "Komi"
  ]
  node [
    id 1292
    label "Czuwaszja"
  ]
  node [
    id 1293
    label "Chakasja"
  ]
  node [
    id 1294
    label "Perm"
  ]
  node [
    id 1295
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1296
    label "Ajon"
  ]
  node [
    id 1297
    label "Adygeja"
  ]
  node [
    id 1298
    label "Dniepr"
  ]
  node [
    id 1299
    label "rubel_rosyjski"
  ]
  node [
    id 1300
    label "Don"
  ]
  node [
    id 1301
    label "Mordowia"
  ]
  node [
    id 1302
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1303
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1304
    label "gourde"
  ]
  node [
    id 1305
    label "Karaiby"
  ]
  node [
    id 1306
    label "escudo_angolskie"
  ]
  node [
    id 1307
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1308
    label "kwanza"
  ]
  node [
    id 1309
    label "ariary"
  ]
  node [
    id 1310
    label "Ocean_Indyjski"
  ]
  node [
    id 1311
    label "Afryka_Wschodnia"
  ]
  node [
    id 1312
    label "frank_malgaski"
  ]
  node [
    id 1313
    label "Unia_Europejska"
  ]
  node [
    id 1314
    label "Windawa"
  ]
  node [
    id 1315
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1316
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1317
    label "&#379;mud&#378;"
  ]
  node [
    id 1318
    label "lit"
  ]
  node [
    id 1319
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1320
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1321
    label "Synaj"
  ]
  node [
    id 1322
    label "paraszyt"
  ]
  node [
    id 1323
    label "funt_egipski"
  ]
  node [
    id 1324
    label "Amhara"
  ]
  node [
    id 1325
    label "birr"
  ]
  node [
    id 1326
    label "Syjon"
  ]
  node [
    id 1327
    label "negus"
  ]
  node [
    id 1328
    label "peso_kolumbijskie"
  ]
  node [
    id 1329
    label "Orinoko"
  ]
  node [
    id 1330
    label "rial_katarski"
  ]
  node [
    id 1331
    label "dram"
  ]
  node [
    id 1332
    label "Limburgia"
  ]
  node [
    id 1333
    label "gulden"
  ]
  node [
    id 1334
    label "Zelandia"
  ]
  node [
    id 1335
    label "Niderlandy"
  ]
  node [
    id 1336
    label "Brabancja"
  ]
  node [
    id 1337
    label "cedi"
  ]
  node [
    id 1338
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1339
    label "milrejs"
  ]
  node [
    id 1340
    label "cruzado"
  ]
  node [
    id 1341
    label "real"
  ]
  node [
    id 1342
    label "frank_monakijski"
  ]
  node [
    id 1343
    label "Fryburg"
  ]
  node [
    id 1344
    label "Bazylea"
  ]
  node [
    id 1345
    label "Alpy"
  ]
  node [
    id 1346
    label "frank_szwajcarski"
  ]
  node [
    id 1347
    label "Helwecja"
  ]
  node [
    id 1348
    label "Europa_Zachodnia"
  ]
  node [
    id 1349
    label "Berno"
  ]
  node [
    id 1350
    label "Ba&#322;kany"
  ]
  node [
    id 1351
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1352
    label "Naddniestrze"
  ]
  node [
    id 1353
    label "Dniestr"
  ]
  node [
    id 1354
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1355
    label "Gagauzja"
  ]
  node [
    id 1356
    label "Indie_Zachodnie"
  ]
  node [
    id 1357
    label "Sikkim"
  ]
  node [
    id 1358
    label "Asam"
  ]
  node [
    id 1359
    label "Kaszmir"
  ]
  node [
    id 1360
    label "rupia_indyjska"
  ]
  node [
    id 1361
    label "Indie_Portugalskie"
  ]
  node [
    id 1362
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1363
    label "Indie_Wschodnie"
  ]
  node [
    id 1364
    label "Kerala"
  ]
  node [
    id 1365
    label "Bollywood"
  ]
  node [
    id 1366
    label "Pend&#380;ab"
  ]
  node [
    id 1367
    label "boliwar"
  ]
  node [
    id 1368
    label "naira"
  ]
  node [
    id 1369
    label "frank_gwinejski"
  ]
  node [
    id 1370
    label "sum"
  ]
  node [
    id 1371
    label "Karaka&#322;pacja"
  ]
  node [
    id 1372
    label "dolar_liberyjski"
  ]
  node [
    id 1373
    label "Dacja"
  ]
  node [
    id 1374
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1375
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1376
    label "Dobrud&#380;a"
  ]
  node [
    id 1377
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1378
    label "dolar_namibijski"
  ]
  node [
    id 1379
    label "kuna"
  ]
  node [
    id 1380
    label "Rugia"
  ]
  node [
    id 1381
    label "Saksonia"
  ]
  node [
    id 1382
    label "Dolna_Saksonia"
  ]
  node [
    id 1383
    label "Anglosas"
  ]
  node [
    id 1384
    label "Hesja"
  ]
  node [
    id 1385
    label "Szlezwik"
  ]
  node [
    id 1386
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1387
    label "Wirtembergia"
  ]
  node [
    id 1388
    label "Po&#322;abie"
  ]
  node [
    id 1389
    label "Germania"
  ]
  node [
    id 1390
    label "Frankonia"
  ]
  node [
    id 1391
    label "Badenia"
  ]
  node [
    id 1392
    label "Holsztyn"
  ]
  node [
    id 1393
    label "Bawaria"
  ]
  node [
    id 1394
    label "marka"
  ]
  node [
    id 1395
    label "Brandenburgia"
  ]
  node [
    id 1396
    label "Szwabia"
  ]
  node [
    id 1397
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1398
    label "Nadrenia"
  ]
  node [
    id 1399
    label "Westfalia"
  ]
  node [
    id 1400
    label "Turyngia"
  ]
  node [
    id 1401
    label "Helgoland"
  ]
  node [
    id 1402
    label "Karlsbad"
  ]
  node [
    id 1403
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1404
    label "korona_w&#281;gierska"
  ]
  node [
    id 1405
    label "forint"
  ]
  node [
    id 1406
    label "Lipt&#243;w"
  ]
  node [
    id 1407
    label "tenge"
  ]
  node [
    id 1408
    label "szach"
  ]
  node [
    id 1409
    label "Baktria"
  ]
  node [
    id 1410
    label "afgani"
  ]
  node [
    id 1411
    label "kip"
  ]
  node [
    id 1412
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1413
    label "Salzburg"
  ]
  node [
    id 1414
    label "Rakuzy"
  ]
  node [
    id 1415
    label "Dyja"
  ]
  node [
    id 1416
    label "Tyrol"
  ]
  node [
    id 1417
    label "konsulent"
  ]
  node [
    id 1418
    label "szyling_austryjacki"
  ]
  node [
    id 1419
    label "peso_urugwajskie"
  ]
  node [
    id 1420
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1421
    label "korona_esto&#324;ska"
  ]
  node [
    id 1422
    label "Skandynawia"
  ]
  node [
    id 1423
    label "Inflanty"
  ]
  node [
    id 1424
    label "marka_esto&#324;ska"
  ]
  node [
    id 1425
    label "Polinezja"
  ]
  node [
    id 1426
    label "tala"
  ]
  node [
    id 1427
    label "Oceania"
  ]
  node [
    id 1428
    label "Podole"
  ]
  node [
    id 1429
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1430
    label "Wsch&#243;d"
  ]
  node [
    id 1431
    label "Zakarpacie"
  ]
  node [
    id 1432
    label "Naddnieprze"
  ]
  node [
    id 1433
    label "Ma&#322;orosja"
  ]
  node [
    id 1434
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1435
    label "Nadbu&#380;e"
  ]
  node [
    id 1436
    label "hrywna"
  ]
  node [
    id 1437
    label "Zaporo&#380;e"
  ]
  node [
    id 1438
    label "Krym"
  ]
  node [
    id 1439
    label "Przykarpacie"
  ]
  node [
    id 1440
    label "Kozaczyzna"
  ]
  node [
    id 1441
    label "karbowaniec"
  ]
  node [
    id 1442
    label "riel"
  ]
  node [
    id 1443
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1444
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1445
    label "kyat"
  ]
  node [
    id 1446
    label "Arakan"
  ]
  node [
    id 1447
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1448
    label "funt_liba&#324;ski"
  ]
  node [
    id 1449
    label "Mariany"
  ]
  node [
    id 1450
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1451
    label "Maghreb"
  ]
  node [
    id 1452
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1453
    label "dinar_algierski"
  ]
  node [
    id 1454
    label "Kabylia"
  ]
  node [
    id 1455
    label "ringgit"
  ]
  node [
    id 1456
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1457
    label "Borneo"
  ]
  node [
    id 1458
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1459
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1460
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1461
    label "lira_izraelska"
  ]
  node [
    id 1462
    label "szekel"
  ]
  node [
    id 1463
    label "Galilea"
  ]
  node [
    id 1464
    label "Judea"
  ]
  node [
    id 1465
    label "tolar"
  ]
  node [
    id 1466
    label "frank_luksemburski"
  ]
  node [
    id 1467
    label "lempira"
  ]
  node [
    id 1468
    label "Pozna&#324;"
  ]
  node [
    id 1469
    label "lira_malta&#324;ska"
  ]
  node [
    id 1470
    label "Gozo"
  ]
  node [
    id 1471
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1472
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1473
    label "Paros"
  ]
  node [
    id 1474
    label "Epir"
  ]
  node [
    id 1475
    label "panhellenizm"
  ]
  node [
    id 1476
    label "Eubea"
  ]
  node [
    id 1477
    label "Rodos"
  ]
  node [
    id 1478
    label "Achaja"
  ]
  node [
    id 1479
    label "Termopile"
  ]
  node [
    id 1480
    label "Attyka"
  ]
  node [
    id 1481
    label "Hellada"
  ]
  node [
    id 1482
    label "Etolia"
  ]
  node [
    id 1483
    label "palestra"
  ]
  node [
    id 1484
    label "Kreta"
  ]
  node [
    id 1485
    label "drachma"
  ]
  node [
    id 1486
    label "Olimp"
  ]
  node [
    id 1487
    label "Tesalia"
  ]
  node [
    id 1488
    label "Peloponez"
  ]
  node [
    id 1489
    label "Eolia"
  ]
  node [
    id 1490
    label "Beocja"
  ]
  node [
    id 1491
    label "Parnas"
  ]
  node [
    id 1492
    label "Lesbos"
  ]
  node [
    id 1493
    label "Atlantyk"
  ]
  node [
    id 1494
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1495
    label "Ulster"
  ]
  node [
    id 1496
    label "funt_irlandzki"
  ]
  node [
    id 1497
    label "tugrik"
  ]
  node [
    id 1498
    label "Azja_Wschodnia"
  ]
  node [
    id 1499
    label "Buriaci"
  ]
  node [
    id 1500
    label "ajmak"
  ]
  node [
    id 1501
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1502
    label "Lotaryngia"
  ]
  node [
    id 1503
    label "Bordeaux"
  ]
  node [
    id 1504
    label "Pikardia"
  ]
  node [
    id 1505
    label "Alzacja"
  ]
  node [
    id 1506
    label "Masyw_Centralny"
  ]
  node [
    id 1507
    label "Akwitania"
  ]
  node [
    id 1508
    label "Sekwana"
  ]
  node [
    id 1509
    label "Langwedocja"
  ]
  node [
    id 1510
    label "Armagnac"
  ]
  node [
    id 1511
    label "Martynika"
  ]
  node [
    id 1512
    label "Bretania"
  ]
  node [
    id 1513
    label "Sabaudia"
  ]
  node [
    id 1514
    label "Korsyka"
  ]
  node [
    id 1515
    label "Normandia"
  ]
  node [
    id 1516
    label "Gaskonia"
  ]
  node [
    id 1517
    label "Burgundia"
  ]
  node [
    id 1518
    label "frank_francuski"
  ]
  node [
    id 1519
    label "Wandea"
  ]
  node [
    id 1520
    label "Prowansja"
  ]
  node [
    id 1521
    label "Gwadelupa"
  ]
  node [
    id 1522
    label "lew"
  ]
  node [
    id 1523
    label "c&#243;rdoba"
  ]
  node [
    id 1524
    label "dolar_Zimbabwe"
  ]
  node [
    id 1525
    label "frank_rwandyjski"
  ]
  node [
    id 1526
    label "kwacha_zambijska"
  ]
  node [
    id 1527
    label "&#322;at"
  ]
  node [
    id 1528
    label "Kurlandia"
  ]
  node [
    id 1529
    label "Liwonia"
  ]
  node [
    id 1530
    label "rubel_&#322;otewski"
  ]
  node [
    id 1531
    label "Himalaje"
  ]
  node [
    id 1532
    label "rupia_nepalska"
  ]
  node [
    id 1533
    label "funt_suda&#324;ski"
  ]
  node [
    id 1534
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1535
    label "dolar_bahamski"
  ]
  node [
    id 1536
    label "Wielka_Bahama"
  ]
  node [
    id 1537
    label "Mazowsze"
  ]
  node [
    id 1538
    label "Pa&#322;uki"
  ]
  node [
    id 1539
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1540
    label "Powi&#347;le"
  ]
  node [
    id 1541
    label "Wolin"
  ]
  node [
    id 1542
    label "z&#322;oty"
  ]
  node [
    id 1543
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1544
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1545
    label "So&#322;a"
  ]
  node [
    id 1546
    label "Krajna"
  ]
  node [
    id 1547
    label "Opolskie"
  ]
  node [
    id 1548
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1549
    label "Suwalszczyzna"
  ]
  node [
    id 1550
    label "barwy_polskie"
  ]
  node [
    id 1551
    label "Podlasie"
  ]
  node [
    id 1552
    label "Izera"
  ]
  node [
    id 1553
    label "Ma&#322;opolska"
  ]
  node [
    id 1554
    label "Warmia"
  ]
  node [
    id 1555
    label "Mazury"
  ]
  node [
    id 1556
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1557
    label "Kaczawa"
  ]
  node [
    id 1558
    label "Lubelszczyzna"
  ]
  node [
    id 1559
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1560
    label "Kielecczyzna"
  ]
  node [
    id 1561
    label "Lubuskie"
  ]
  node [
    id 1562
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1563
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1564
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1565
    label "Kujawy"
  ]
  node [
    id 1566
    label "Podkarpacie"
  ]
  node [
    id 1567
    label "Wielkopolska"
  ]
  node [
    id 1568
    label "Wis&#322;a"
  ]
  node [
    id 1569
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1570
    label "Bory_Tucholskie"
  ]
  node [
    id 1571
    label "Antyle"
  ]
  node [
    id 1572
    label "dolar_Tuvalu"
  ]
  node [
    id 1573
    label "dinar_iracki"
  ]
  node [
    id 1574
    label "korona_s&#322;owacka"
  ]
  node [
    id 1575
    label "Turiec"
  ]
  node [
    id 1576
    label "jen"
  ]
  node [
    id 1577
    label "jinja"
  ]
  node [
    id 1578
    label "Okinawa"
  ]
  node [
    id 1579
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1580
    label "Japonica"
  ]
  node [
    id 1581
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1582
    label "szyling_kenijski"
  ]
  node [
    id 1583
    label "peso_chilijskie"
  ]
  node [
    id 1584
    label "Zanzibar"
  ]
  node [
    id 1585
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1586
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1587
    label "Cebu"
  ]
  node [
    id 1588
    label "Sahara"
  ]
  node [
    id 1589
    label "Tasmania"
  ]
  node [
    id 1590
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1591
    label "dolar_australijski"
  ]
  node [
    id 1592
    label "Quebec"
  ]
  node [
    id 1593
    label "dolar_kanadyjski"
  ]
  node [
    id 1594
    label "Nowa_Fundlandia"
  ]
  node [
    id 1595
    label "quetzal"
  ]
  node [
    id 1596
    label "Manica"
  ]
  node [
    id 1597
    label "escudo_mozambickie"
  ]
  node [
    id 1598
    label "Cabo_Delgado"
  ]
  node [
    id 1599
    label "Inhambane"
  ]
  node [
    id 1600
    label "Maputo"
  ]
  node [
    id 1601
    label "Gaza"
  ]
  node [
    id 1602
    label "Niasa"
  ]
  node [
    id 1603
    label "Nampula"
  ]
  node [
    id 1604
    label "metical"
  ]
  node [
    id 1605
    label "frank_tunezyjski"
  ]
  node [
    id 1606
    label "dinar_tunezyjski"
  ]
  node [
    id 1607
    label "lud"
  ]
  node [
    id 1608
    label "frank_kongijski"
  ]
  node [
    id 1609
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1610
    label "dinar_Bahrajnu"
  ]
  node [
    id 1611
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1612
    label "escudo_portugalskie"
  ]
  node [
    id 1613
    label "Melanezja"
  ]
  node [
    id 1614
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1615
    label "d&#380;amahirijja"
  ]
  node [
    id 1616
    label "dinar_libijski"
  ]
  node [
    id 1617
    label "balboa"
  ]
  node [
    id 1618
    label "dolar_surinamski"
  ]
  node [
    id 1619
    label "dolar_Brunei"
  ]
  node [
    id 1620
    label "Estremadura"
  ]
  node [
    id 1621
    label "Andaluzja"
  ]
  node [
    id 1622
    label "Kastylia"
  ]
  node [
    id 1623
    label "Galicja"
  ]
  node [
    id 1624
    label "Rzym_Zachodni"
  ]
  node [
    id 1625
    label "Aragonia"
  ]
  node [
    id 1626
    label "hacjender"
  ]
  node [
    id 1627
    label "Asturia"
  ]
  node [
    id 1628
    label "Baskonia"
  ]
  node [
    id 1629
    label "Majorka"
  ]
  node [
    id 1630
    label "Walencja"
  ]
  node [
    id 1631
    label "peseta"
  ]
  node [
    id 1632
    label "Katalonia"
  ]
  node [
    id 1633
    label "Luksemburgia"
  ]
  node [
    id 1634
    label "frank_belgijski"
  ]
  node [
    id 1635
    label "Walonia"
  ]
  node [
    id 1636
    label "Flandria"
  ]
  node [
    id 1637
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1638
    label "dolar_Barbadosu"
  ]
  node [
    id 1639
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1640
    label "korona_czeska"
  ]
  node [
    id 1641
    label "Lasko"
  ]
  node [
    id 1642
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1643
    label "Wojwodina"
  ]
  node [
    id 1644
    label "dinar_serbski"
  ]
  node [
    id 1645
    label "funt_syryjski"
  ]
  node [
    id 1646
    label "alawizm"
  ]
  node [
    id 1647
    label "Szantung"
  ]
  node [
    id 1648
    label "Chiny_Zachodnie"
  ]
  node [
    id 1649
    label "Kuantung"
  ]
  node [
    id 1650
    label "D&#380;ungaria"
  ]
  node [
    id 1651
    label "yuan"
  ]
  node [
    id 1652
    label "Hongkong"
  ]
  node [
    id 1653
    label "Chiny_Wschodnie"
  ]
  node [
    id 1654
    label "Guangdong"
  ]
  node [
    id 1655
    label "Junnan"
  ]
  node [
    id 1656
    label "Mand&#380;uria"
  ]
  node [
    id 1657
    label "Syczuan"
  ]
  node [
    id 1658
    label "zair"
  ]
  node [
    id 1659
    label "Katanga"
  ]
  node [
    id 1660
    label "ugija"
  ]
  node [
    id 1661
    label "dalasi"
  ]
  node [
    id 1662
    label "funt_cypryjski"
  ]
  node [
    id 1663
    label "Afrodyzje"
  ]
  node [
    id 1664
    label "frank_alba&#324;ski"
  ]
  node [
    id 1665
    label "lek"
  ]
  node [
    id 1666
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1667
    label "dolar_jamajski"
  ]
  node [
    id 1668
    label "kafar"
  ]
  node [
    id 1669
    label "Ocean_Spokojny"
  ]
  node [
    id 1670
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1671
    label "som"
  ]
  node [
    id 1672
    label "guarani"
  ]
  node [
    id 1673
    label "rial_ira&#324;ski"
  ]
  node [
    id 1674
    label "mu&#322;&#322;a"
  ]
  node [
    id 1675
    label "Persja"
  ]
  node [
    id 1676
    label "Jawa"
  ]
  node [
    id 1677
    label "Sumatra"
  ]
  node [
    id 1678
    label "rupia_indonezyjska"
  ]
  node [
    id 1679
    label "Nowa_Gwinea"
  ]
  node [
    id 1680
    label "Moluki"
  ]
  node [
    id 1681
    label "szyling_somalijski"
  ]
  node [
    id 1682
    label "szyling_ugandyjski"
  ]
  node [
    id 1683
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1684
    label "lira_turecka"
  ]
  node [
    id 1685
    label "Azja_Mniejsza"
  ]
  node [
    id 1686
    label "Ujgur"
  ]
  node [
    id 1687
    label "Pireneje"
  ]
  node [
    id 1688
    label "nakfa"
  ]
  node [
    id 1689
    label "won"
  ]
  node [
    id 1690
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1691
    label "&#346;wite&#378;"
  ]
  node [
    id 1692
    label "dinar_kuwejcki"
  ]
  node [
    id 1693
    label "Nachiczewan"
  ]
  node [
    id 1694
    label "manat_azerski"
  ]
  node [
    id 1695
    label "Karabach"
  ]
  node [
    id 1696
    label "dolar_Kiribati"
  ]
  node [
    id 1697
    label "Anglia"
  ]
  node [
    id 1698
    label "Amazonia"
  ]
  node [
    id 1699
    label "plantowa&#263;"
  ]
  node [
    id 1700
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1701
    label "zapadnia"
  ]
  node [
    id 1702
    label "Zamojszczyzna"
  ]
  node [
    id 1703
    label "budynek"
  ]
  node [
    id 1704
    label "skorupa_ziemska"
  ]
  node [
    id 1705
    label "Turkiestan"
  ]
  node [
    id 1706
    label "Noworosja"
  ]
  node [
    id 1707
    label "Mezoameryka"
  ]
  node [
    id 1708
    label "glinowanie"
  ]
  node [
    id 1709
    label "Kurdystan"
  ]
  node [
    id 1710
    label "martwica"
  ]
  node [
    id 1711
    label "Szkocja"
  ]
  node [
    id 1712
    label "litosfera"
  ]
  node [
    id 1713
    label "penetrator"
  ]
  node [
    id 1714
    label "glinowa&#263;"
  ]
  node [
    id 1715
    label "Zabajkale"
  ]
  node [
    id 1716
    label "domain"
  ]
  node [
    id 1717
    label "Bojkowszczyzna"
  ]
  node [
    id 1718
    label "podglebie"
  ]
  node [
    id 1719
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1720
    label "Pamir"
  ]
  node [
    id 1721
    label "Indochiny"
  ]
  node [
    id 1722
    label "Kurpie"
  ]
  node [
    id 1723
    label "S&#261;decczyzna"
  ]
  node [
    id 1724
    label "kort"
  ]
  node [
    id 1725
    label "czynnik_produkcji"
  ]
  node [
    id 1726
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1727
    label "Huculszczyzna"
  ]
  node [
    id 1728
    label "Podhale"
  ]
  node [
    id 1729
    label "pr&#243;chnica"
  ]
  node [
    id 1730
    label "Hercegowina"
  ]
  node [
    id 1731
    label "Walia"
  ]
  node [
    id 1732
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1733
    label "ryzosfera"
  ]
  node [
    id 1734
    label "Kaukaz"
  ]
  node [
    id 1735
    label "Biskupizna"
  ]
  node [
    id 1736
    label "Bo&#347;nia"
  ]
  node [
    id 1737
    label "dotleni&#263;"
  ]
  node [
    id 1738
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1739
    label "Podbeskidzie"
  ]
  node [
    id 1740
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1741
    label "Opolszczyzna"
  ]
  node [
    id 1742
    label "Kaszuby"
  ]
  node [
    id 1743
    label "Ko&#322;yma"
  ]
  node [
    id 1744
    label "glej"
  ]
  node [
    id 1745
    label "posadzka"
  ]
  node [
    id 1746
    label "Polesie"
  ]
  node [
    id 1747
    label "Palestyna"
  ]
  node [
    id 1748
    label "Lauda"
  ]
  node [
    id 1749
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1750
    label "Laponia"
  ]
  node [
    id 1751
    label "Yorkshire"
  ]
  node [
    id 1752
    label "Zag&#243;rze"
  ]
  node [
    id 1753
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1754
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1755
    label "Oksytania"
  ]
  node [
    id 1756
    label "Kociewie"
  ]
  node [
    id 1757
    label "wyci&#261;g"
  ]
  node [
    id 1758
    label "passage"
  ]
  node [
    id 1759
    label "ortografia"
  ]
  node [
    id 1760
    label "cytat"
  ]
  node [
    id 1761
    label "ekscerptor"
  ]
  node [
    id 1762
    label "urywek"
  ]
  node [
    id 1763
    label "wordnet"
  ]
  node [
    id 1764
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 1765
    label "wypowiedzenie"
  ]
  node [
    id 1766
    label "morfem"
  ]
  node [
    id 1767
    label "s&#322;ownictwo"
  ]
  node [
    id 1768
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 1769
    label "wykrzyknik"
  ]
  node [
    id 1770
    label "pole_semantyczne"
  ]
  node [
    id 1771
    label "pisanie_si&#281;"
  ]
  node [
    id 1772
    label "nag&#322;os"
  ]
  node [
    id 1773
    label "wyg&#322;os"
  ]
  node [
    id 1774
    label "fragment"
  ]
  node [
    id 1775
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1776
    label "warunki"
  ]
  node [
    id 1777
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1778
    label "state"
  ]
  node [
    id 1779
    label "motyw"
  ]
  node [
    id 1780
    label "realia"
  ]
  node [
    id 1781
    label "pismo"
  ]
  node [
    id 1782
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 1783
    label "fonetyzacja"
  ]
  node [
    id 1784
    label "skr&#243;t"
  ]
  node [
    id 1785
    label "skipass"
  ]
  node [
    id 1786
    label "okap"
  ]
  node [
    id 1787
    label "wyimek"
  ]
  node [
    id 1788
    label "wentylator"
  ]
  node [
    id 1789
    label "mieszanina"
  ]
  node [
    id 1790
    label "bro&#324;_lufowa"
  ]
  node [
    id 1791
    label "urz&#261;dzenie_rekreacyjne"
  ]
  node [
    id 1792
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1793
    label "dokument"
  ]
  node [
    id 1794
    label "extraction"
  ]
  node [
    id 1795
    label "d&#378;wig"
  ]
  node [
    id 1796
    label "naukowiec"
  ]
  node [
    id 1797
    label "alegacja"
  ]
  node [
    id 1798
    label "konkordancja"
  ]
  node [
    id 1799
    label "wydarzenie"
  ]
  node [
    id 1800
    label "pacjent"
  ]
  node [
    id 1801
    label "happening"
  ]
  node [
    id 1802
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 1803
    label "schorzenie"
  ]
  node [
    id 1804
    label "przyk&#322;ad"
  ]
  node [
    id 1805
    label "przeznaczenie"
  ]
  node [
    id 1806
    label "fakt"
  ]
  node [
    id 1807
    label "czyn"
  ]
  node [
    id 1808
    label "przedstawiciel"
  ]
  node [
    id 1809
    label "przebiec"
  ]
  node [
    id 1810
    label "charakter"
  ]
  node [
    id 1811
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1812
    label "przebiegni&#281;cie"
  ]
  node [
    id 1813
    label "fabu&#322;a"
  ]
  node [
    id 1814
    label "destiny"
  ]
  node [
    id 1815
    label "przymus"
  ]
  node [
    id 1816
    label "przydzielenie"
  ]
  node [
    id 1817
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1818
    label "oblat"
  ]
  node [
    id 1819
    label "obowi&#261;zek"
  ]
  node [
    id 1820
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1821
    label "ognisko"
  ]
  node [
    id 1822
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1823
    label "powalenie"
  ]
  node [
    id 1824
    label "odezwanie_si&#281;"
  ]
  node [
    id 1825
    label "atakowanie"
  ]
  node [
    id 1826
    label "grupa_ryzyka"
  ]
  node [
    id 1827
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1828
    label "nabawienie_si&#281;"
  ]
  node [
    id 1829
    label "inkubacja"
  ]
  node [
    id 1830
    label "kryzys"
  ]
  node [
    id 1831
    label "powali&#263;"
  ]
  node [
    id 1832
    label "remisja"
  ]
  node [
    id 1833
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1834
    label "zajmowa&#263;"
  ]
  node [
    id 1835
    label "zaburzenie"
  ]
  node [
    id 1836
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1837
    label "badanie_histopatologiczne"
  ]
  node [
    id 1838
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1839
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1840
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1841
    label "odzywanie_si&#281;"
  ]
  node [
    id 1842
    label "diagnoza"
  ]
  node [
    id 1843
    label "atakowa&#263;"
  ]
  node [
    id 1844
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1845
    label "nabawianie_si&#281;"
  ]
  node [
    id 1846
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1847
    label "zajmowanie"
  ]
  node [
    id 1848
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 1849
    label "klient"
  ]
  node [
    id 1850
    label "piel&#281;gniarz"
  ]
  node [
    id 1851
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 1852
    label "od&#322;&#261;czanie"
  ]
  node [
    id 1853
    label "chory"
  ]
  node [
    id 1854
    label "od&#322;&#261;czenie"
  ]
  node [
    id 1855
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 1856
    label "szpitalnik"
  ]
  node [
    id 1857
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 1858
    label "przedstawienie"
  ]
  node [
    id 1859
    label "participate"
  ]
  node [
    id 1860
    label "istnie&#263;"
  ]
  node [
    id 1861
    label "pozostawa&#263;"
  ]
  node [
    id 1862
    label "zostawa&#263;"
  ]
  node [
    id 1863
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1864
    label "adhere"
  ]
  node [
    id 1865
    label "compass"
  ]
  node [
    id 1866
    label "appreciation"
  ]
  node [
    id 1867
    label "get"
  ]
  node [
    id 1868
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1869
    label "mierzy&#263;"
  ]
  node [
    id 1870
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1871
    label "exsert"
  ]
  node [
    id 1872
    label "being"
  ]
  node [
    id 1873
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1874
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1875
    label "run"
  ]
  node [
    id 1876
    label "bangla&#263;"
  ]
  node [
    id 1877
    label "przebiega&#263;"
  ]
  node [
    id 1878
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1879
    label "proceed"
  ]
  node [
    id 1880
    label "carry"
  ]
  node [
    id 1881
    label "bywa&#263;"
  ]
  node [
    id 1882
    label "dziama&#263;"
  ]
  node [
    id 1883
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1884
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1885
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1886
    label "str&#243;j"
  ]
  node [
    id 1887
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1888
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1889
    label "krok"
  ]
  node [
    id 1890
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1891
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1892
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1893
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1894
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1895
    label "continue"
  ]
  node [
    id 1896
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1897
    label "wci&#281;cie"
  ]
  node [
    id 1898
    label "samopoczucie"
  ]
  node [
    id 1899
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1900
    label "wektor"
  ]
  node [
    id 1901
    label "Goa"
  ]
  node [
    id 1902
    label "poziom"
  ]
  node [
    id 1903
    label "shape"
  ]
  node [
    id 1904
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1905
    label "wy&#322;&#261;czny"
  ]
  node [
    id 1906
    label "w&#322;asny"
  ]
  node [
    id 1907
    label "unikatowy"
  ]
  node [
    id 1908
    label "jedyny"
  ]
  node [
    id 1909
    label "charakterystyka"
  ]
  node [
    id 1910
    label "m&#322;ot"
  ]
  node [
    id 1911
    label "znak"
  ]
  node [
    id 1912
    label "drzewo"
  ]
  node [
    id 1913
    label "pr&#243;ba"
  ]
  node [
    id 1914
    label "attribute"
  ]
  node [
    id 1915
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1916
    label "nagana"
  ]
  node [
    id 1917
    label "tekst"
  ]
  node [
    id 1918
    label "upomnienie"
  ]
  node [
    id 1919
    label "dzienniczek"
  ]
  node [
    id 1920
    label "wzgl&#261;d"
  ]
  node [
    id 1921
    label "gossip"
  ]
  node [
    id 1922
    label "whole"
  ]
  node [
    id 1923
    label "Rzym_Wschodni"
  ]
  node [
    id 1924
    label "urz&#261;dzenie"
  ]
  node [
    id 1925
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1926
    label "najem"
  ]
  node [
    id 1927
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1928
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1929
    label "zak&#322;ad"
  ]
  node [
    id 1930
    label "stosunek_pracy"
  ]
  node [
    id 1931
    label "benedykty&#324;ski"
  ]
  node [
    id 1932
    label "poda&#380;_pracy"
  ]
  node [
    id 1933
    label "pracowanie"
  ]
  node [
    id 1934
    label "tyrka"
  ]
  node [
    id 1935
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1936
    label "wytw&#243;r"
  ]
  node [
    id 1937
    label "zaw&#243;d"
  ]
  node [
    id 1938
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1939
    label "tynkarski"
  ]
  node [
    id 1940
    label "zobowi&#261;zanie"
  ]
  node [
    id 1941
    label "kierownictwo"
  ]
  node [
    id 1942
    label "siedziba"
  ]
  node [
    id 1943
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1944
    label "rozdzielanie"
  ]
  node [
    id 1945
    label "bezbrze&#380;e"
  ]
  node [
    id 1946
    label "niezmierzony"
  ]
  node [
    id 1947
    label "przedzielenie"
  ]
  node [
    id 1948
    label "nielito&#347;ciwy"
  ]
  node [
    id 1949
    label "rozdziela&#263;"
  ]
  node [
    id 1950
    label "oktant"
  ]
  node [
    id 1951
    label "przedzieli&#263;"
  ]
  node [
    id 1952
    label "przestw&#243;r"
  ]
  node [
    id 1953
    label "condition"
  ]
  node [
    id 1954
    label "awansowa&#263;"
  ]
  node [
    id 1955
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1956
    label "znaczenie"
  ]
  node [
    id 1957
    label "awans"
  ]
  node [
    id 1958
    label "podmiotowo"
  ]
  node [
    id 1959
    label "awansowanie"
  ]
  node [
    id 1960
    label "rozmiar"
  ]
  node [
    id 1961
    label "liczba"
  ]
  node [
    id 1962
    label "circumference"
  ]
  node [
    id 1963
    label "cyrkumferencja"
  ]
  node [
    id 1964
    label "strona"
  ]
  node [
    id 1965
    label "ekshumowanie"
  ]
  node [
    id 1966
    label "odwadnia&#263;"
  ]
  node [
    id 1967
    label "zabalsamowanie"
  ]
  node [
    id 1968
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1969
    label "odwodni&#263;"
  ]
  node [
    id 1970
    label "sk&#243;ra"
  ]
  node [
    id 1971
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1972
    label "staw"
  ]
  node [
    id 1973
    label "ow&#322;osienie"
  ]
  node [
    id 1974
    label "mi&#281;so"
  ]
  node [
    id 1975
    label "zabalsamowa&#263;"
  ]
  node [
    id 1976
    label "Izba_Konsyliarska"
  ]
  node [
    id 1977
    label "unerwienie"
  ]
  node [
    id 1978
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1979
    label "kremacja"
  ]
  node [
    id 1980
    label "biorytm"
  ]
  node [
    id 1981
    label "sekcja"
  ]
  node [
    id 1982
    label "istota_&#380;ywa"
  ]
  node [
    id 1983
    label "otworzy&#263;"
  ]
  node [
    id 1984
    label "otwiera&#263;"
  ]
  node [
    id 1985
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1986
    label "otworzenie"
  ]
  node [
    id 1987
    label "pochowanie"
  ]
  node [
    id 1988
    label "otwieranie"
  ]
  node [
    id 1989
    label "ty&#322;"
  ]
  node [
    id 1990
    label "szkielet"
  ]
  node [
    id 1991
    label "tanatoplastyk"
  ]
  node [
    id 1992
    label "odwadnianie"
  ]
  node [
    id 1993
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1994
    label "odwodnienie"
  ]
  node [
    id 1995
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1996
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1997
    label "nieumar&#322;y"
  ]
  node [
    id 1998
    label "pochowa&#263;"
  ]
  node [
    id 1999
    label "balsamowa&#263;"
  ]
  node [
    id 2000
    label "tanatoplastyka"
  ]
  node [
    id 2001
    label "temperatura"
  ]
  node [
    id 2002
    label "ekshumowa&#263;"
  ]
  node [
    id 2003
    label "balsamowanie"
  ]
  node [
    id 2004
    label "prz&#243;d"
  ]
  node [
    id 2005
    label "l&#281;d&#378;wie"
  ]
  node [
    id 2006
    label "cz&#322;onek"
  ]
  node [
    id 2007
    label "pogrzeb"
  ]
  node [
    id 2008
    label "&#321;ubianka"
  ]
  node [
    id 2009
    label "area"
  ]
  node [
    id 2010
    label "Majdan"
  ]
  node [
    id 2011
    label "pole_bitwy"
  ]
  node [
    id 2012
    label "stoisko"
  ]
  node [
    id 2013
    label "pierzeja"
  ]
  node [
    id 2014
    label "obiekt_handlowy"
  ]
  node [
    id 2015
    label "zgromadzenie"
  ]
  node [
    id 2016
    label "miasto"
  ]
  node [
    id 2017
    label "targowica"
  ]
  node [
    id 2018
    label "kram"
  ]
  node [
    id 2019
    label "przybli&#380;enie"
  ]
  node [
    id 2020
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 2021
    label "kategoria"
  ]
  node [
    id 2022
    label "szpaler"
  ]
  node [
    id 2023
    label "lon&#380;a"
  ]
  node [
    id 2024
    label "uporz&#261;dkowanie"
  ]
  node [
    id 2025
    label "instytucja"
  ]
  node [
    id 2026
    label "premier"
  ]
  node [
    id 2027
    label "Londyn"
  ]
  node [
    id 2028
    label "gabinet_cieni"
  ]
  node [
    id 2029
    label "number"
  ]
  node [
    id 2030
    label "Konsulat"
  ]
  node [
    id 2031
    label "tract"
  ]
  node [
    id 2032
    label "klasa"
  ]
  node [
    id 2033
    label "w&#322;adza"
  ]
  node [
    id 2034
    label "wear"
  ]
  node [
    id 2035
    label "return"
  ]
  node [
    id 2036
    label "pozostawi&#263;"
  ]
  node [
    id 2037
    label "pokry&#263;"
  ]
  node [
    id 2038
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 2039
    label "przygotowa&#263;"
  ]
  node [
    id 2040
    label "stagger"
  ]
  node [
    id 2041
    label "zepsu&#263;"
  ]
  node [
    id 2042
    label "zmieni&#263;"
  ]
  node [
    id 2043
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 2044
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 2045
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 2046
    label "umie&#347;ci&#263;"
  ]
  node [
    id 2047
    label "zacz&#261;&#263;"
  ]
  node [
    id 2048
    label "wygra&#263;"
  ]
  node [
    id 2049
    label "drop"
  ]
  node [
    id 2050
    label "skrzywdzi&#263;"
  ]
  node [
    id 2051
    label "shove"
  ]
  node [
    id 2052
    label "wyda&#263;"
  ]
  node [
    id 2053
    label "zaplanowa&#263;"
  ]
  node [
    id 2054
    label "shelve"
  ]
  node [
    id 2055
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 2056
    label "zachowa&#263;"
  ]
  node [
    id 2057
    label "impart"
  ]
  node [
    id 2058
    label "da&#263;"
  ]
  node [
    id 2059
    label "wyznaczy&#263;"
  ]
  node [
    id 2060
    label "liszy&#263;"
  ]
  node [
    id 2061
    label "zerwa&#263;"
  ]
  node [
    id 2062
    label "spowodowa&#263;"
  ]
  node [
    id 2063
    label "release"
  ]
  node [
    id 2064
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 2065
    label "przekaza&#263;"
  ]
  node [
    id 2066
    label "stworzy&#263;"
  ]
  node [
    id 2067
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 2068
    label "zabra&#263;"
  ]
  node [
    id 2069
    label "zrezygnowa&#263;"
  ]
  node [
    id 2070
    label "permit"
  ]
  node [
    id 2071
    label "put"
  ]
  node [
    id 2072
    label "uplasowa&#263;"
  ]
  node [
    id 2073
    label "wpierniczy&#263;"
  ]
  node [
    id 2074
    label "okre&#347;li&#263;"
  ]
  node [
    id 2075
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 2076
    label "umieszcza&#263;"
  ]
  node [
    id 2077
    label "favor"
  ]
  node [
    id 2078
    label "potraktowa&#263;"
  ]
  node [
    id 2079
    label "nagrodzi&#263;"
  ]
  node [
    id 2080
    label "work"
  ]
  node [
    id 2081
    label "chemia"
  ]
  node [
    id 2082
    label "reakcja_chemiczna"
  ]
  node [
    id 2083
    label "act"
  ]
  node [
    id 2084
    label "sprawi&#263;"
  ]
  node [
    id 2085
    label "change"
  ]
  node [
    id 2086
    label "zast&#261;pi&#263;"
  ]
  node [
    id 2087
    label "come_up"
  ]
  node [
    id 2088
    label "przej&#347;&#263;"
  ]
  node [
    id 2089
    label "straci&#263;"
  ]
  node [
    id 2090
    label "zyska&#263;"
  ]
  node [
    id 2091
    label "draw"
  ]
  node [
    id 2092
    label "zagwarantowa&#263;"
  ]
  node [
    id 2093
    label "znie&#347;&#263;"
  ]
  node [
    id 2094
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 2095
    label "zagra&#263;"
  ]
  node [
    id 2096
    label "score"
  ]
  node [
    id 2097
    label "zwojowa&#263;"
  ]
  node [
    id 2098
    label "leave"
  ]
  node [
    id 2099
    label "net_income"
  ]
  node [
    id 2100
    label "instrument_muzyczny"
  ]
  node [
    id 2101
    label "zaszkodzi&#263;"
  ]
  node [
    id 2102
    label "zjeba&#263;"
  ]
  node [
    id 2103
    label "drop_the_ball"
  ]
  node [
    id 2104
    label "zdemoralizowa&#263;"
  ]
  node [
    id 2105
    label "uszkodzi&#263;"
  ]
  node [
    id 2106
    label "wypierdoli&#263;_si&#281;"
  ]
  node [
    id 2107
    label "damage"
  ]
  node [
    id 2108
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 2109
    label "spapra&#263;"
  ]
  node [
    id 2110
    label "skopa&#263;"
  ]
  node [
    id 2111
    label "zap&#322;odni&#263;"
  ]
  node [
    id 2112
    label "cover"
  ]
  node [
    id 2113
    label "przykry&#263;"
  ]
  node [
    id 2114
    label "sheathing"
  ]
  node [
    id 2115
    label "brood"
  ]
  node [
    id 2116
    label "zaj&#261;&#263;"
  ]
  node [
    id 2117
    label "zap&#322;aci&#263;"
  ]
  node [
    id 2118
    label "ustawi&#263;_si&#281;"
  ]
  node [
    id 2119
    label "zamaskowa&#263;"
  ]
  node [
    id 2120
    label "zaspokoi&#263;"
  ]
  node [
    id 2121
    label "defray"
  ]
  node [
    id 2122
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 2123
    label "wykona&#263;"
  ]
  node [
    id 2124
    label "cook"
  ]
  node [
    id 2125
    label "wyszkoli&#263;"
  ]
  node [
    id 2126
    label "wytworzy&#263;"
  ]
  node [
    id 2127
    label "dress"
  ]
  node [
    id 2128
    label "ukierunkowa&#263;"
  ]
  node [
    id 2129
    label "post&#261;pi&#263;"
  ]
  node [
    id 2130
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 2131
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 2132
    label "odj&#261;&#263;"
  ]
  node [
    id 2133
    label "cause"
  ]
  node [
    id 2134
    label "introduce"
  ]
  node [
    id 2135
    label "begin"
  ]
  node [
    id 2136
    label "do"
  ]
  node [
    id 2137
    label "dow&#243;d"
  ]
  node [
    id 2138
    label "oznakowanie"
  ]
  node [
    id 2139
    label "stawia&#263;"
  ]
  node [
    id 2140
    label "kodzik"
  ]
  node [
    id 2141
    label "postawi&#263;"
  ]
  node [
    id 2142
    label "herb"
  ]
  node [
    id 2143
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 2144
    label "implikowa&#263;"
  ]
  node [
    id 2145
    label "bacteriophage"
  ]
  node [
    id 2146
    label "republika"
  ]
  node [
    id 2147
    label "ministerstwo"
  ]
  node [
    id 2148
    label "finanse"
  ]
  node [
    id 2149
    label "rzeczpospolita"
  ]
  node [
    id 2150
    label "polski"
  ]
  node [
    id 2151
    label "minister"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 33
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 38
  ]
  edge [
    source 11
    target 39
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 41
  ]
  edge [
    source 11
    target 42
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 824
  ]
  edge [
    source 14
    target 825
  ]
  edge [
    source 14
    target 826
  ]
  edge [
    source 14
    target 827
  ]
  edge [
    source 14
    target 828
  ]
  edge [
    source 14
    target 829
  ]
  edge [
    source 14
    target 830
  ]
  edge [
    source 14
    target 831
  ]
  edge [
    source 14
    target 832
  ]
  edge [
    source 14
    target 833
  ]
  edge [
    source 14
    target 834
  ]
  edge [
    source 14
    target 835
  ]
  edge [
    source 14
    target 836
  ]
  edge [
    source 14
    target 837
  ]
  edge [
    source 14
    target 838
  ]
  edge [
    source 14
    target 839
  ]
  edge [
    source 14
    target 840
  ]
  edge [
    source 14
    target 841
  ]
  edge [
    source 14
    target 842
  ]
  edge [
    source 14
    target 843
  ]
  edge [
    source 14
    target 844
  ]
  edge [
    source 14
    target 845
  ]
  edge [
    source 14
    target 846
  ]
  edge [
    source 14
    target 847
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 312
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 893
  ]
  edge [
    source 19
    target 894
  ]
  edge [
    source 19
    target 895
  ]
  edge [
    source 19
    target 896
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 701
  ]
  edge [
    source 19
    target 898
  ]
  edge [
    source 19
    target 899
  ]
  edge [
    source 19
    target 900
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 903
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 19
    target 905
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 19
    target 919
  ]
  edge [
    source 19
    target 920
  ]
  edge [
    source 19
    target 921
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 923
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 19
    target 927
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 929
  ]
  edge [
    source 19
    target 930
  ]
  edge [
    source 19
    target 931
  ]
  edge [
    source 19
    target 932
  ]
  edge [
    source 19
    target 933
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 699
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 709
  ]
  edge [
    source 19
    target 952
  ]
  edge [
    source 19
    target 953
  ]
  edge [
    source 19
    target 954
  ]
  edge [
    source 19
    target 955
  ]
  edge [
    source 19
    target 956
  ]
  edge [
    source 19
    target 957
  ]
  edge [
    source 19
    target 958
  ]
  edge [
    source 19
    target 959
  ]
  edge [
    source 19
    target 960
  ]
  edge [
    source 19
    target 961
  ]
  edge [
    source 19
    target 962
  ]
  edge [
    source 19
    target 963
  ]
  edge [
    source 19
    target 964
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 966
  ]
  edge [
    source 19
    target 967
  ]
  edge [
    source 19
    target 968
  ]
  edge [
    source 19
    target 969
  ]
  edge [
    source 19
    target 970
  ]
  edge [
    source 19
    target 721
  ]
  edge [
    source 19
    target 971
  ]
  edge [
    source 19
    target 972
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 973
  ]
  edge [
    source 19
    target 974
  ]
  edge [
    source 19
    target 975
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 976
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 729
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 292
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 248
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 1130
  ]
  edge [
    source 19
    target 1131
  ]
  edge [
    source 19
    target 1132
  ]
  edge [
    source 19
    target 1133
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 770
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 19
    target 1144
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 1146
  ]
  edge [
    source 19
    target 1147
  ]
  edge [
    source 19
    target 1148
  ]
  edge [
    source 19
    target 1149
  ]
  edge [
    source 19
    target 1150
  ]
  edge [
    source 19
    target 1151
  ]
  edge [
    source 19
    target 1152
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 698
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 408
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1258
  ]
  edge [
    source 19
    target 1259
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1261
  ]
  edge [
    source 19
    target 1262
  ]
  edge [
    source 19
    target 1263
  ]
  edge [
    source 19
    target 1264
  ]
  edge [
    source 19
    target 1265
  ]
  edge [
    source 19
    target 1266
  ]
  edge [
    source 19
    target 1267
  ]
  edge [
    source 19
    target 1268
  ]
  edge [
    source 19
    target 1269
  ]
  edge [
    source 19
    target 1270
  ]
  edge [
    source 19
    target 1271
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 1274
  ]
  edge [
    source 19
    target 1275
  ]
  edge [
    source 19
    target 1276
  ]
  edge [
    source 19
    target 1277
  ]
  edge [
    source 19
    target 1278
  ]
  edge [
    source 19
    target 1279
  ]
  edge [
    source 19
    target 1280
  ]
  edge [
    source 19
    target 1281
  ]
  edge [
    source 19
    target 1282
  ]
  edge [
    source 19
    target 1283
  ]
  edge [
    source 19
    target 1284
  ]
  edge [
    source 19
    target 1285
  ]
  edge [
    source 19
    target 1286
  ]
  edge [
    source 19
    target 1287
  ]
  edge [
    source 19
    target 1288
  ]
  edge [
    source 19
    target 1289
  ]
  edge [
    source 19
    target 1290
  ]
  edge [
    source 19
    target 1291
  ]
  edge [
    source 19
    target 1292
  ]
  edge [
    source 19
    target 1293
  ]
  edge [
    source 19
    target 1294
  ]
  edge [
    source 19
    target 1295
  ]
  edge [
    source 19
    target 1296
  ]
  edge [
    source 19
    target 1297
  ]
  edge [
    source 19
    target 1298
  ]
  edge [
    source 19
    target 1299
  ]
  edge [
    source 19
    target 1300
  ]
  edge [
    source 19
    target 1301
  ]
  edge [
    source 19
    target 1302
  ]
  edge [
    source 19
    target 1303
  ]
  edge [
    source 19
    target 1304
  ]
  edge [
    source 19
    target 1305
  ]
  edge [
    source 19
    target 1306
  ]
  edge [
    source 19
    target 1307
  ]
  edge [
    source 19
    target 1308
  ]
  edge [
    source 19
    target 1309
  ]
  edge [
    source 19
    target 1310
  ]
  edge [
    source 19
    target 1311
  ]
  edge [
    source 19
    target 1312
  ]
  edge [
    source 19
    target 1313
  ]
  edge [
    source 19
    target 1314
  ]
  edge [
    source 19
    target 1315
  ]
  edge [
    source 19
    target 1316
  ]
  edge [
    source 19
    target 1317
  ]
  edge [
    source 19
    target 1318
  ]
  edge [
    source 19
    target 1319
  ]
  edge [
    source 19
    target 1320
  ]
  edge [
    source 19
    target 1321
  ]
  edge [
    source 19
    target 1322
  ]
  edge [
    source 19
    target 1323
  ]
  edge [
    source 19
    target 1324
  ]
  edge [
    source 19
    target 1325
  ]
  edge [
    source 19
    target 1326
  ]
  edge [
    source 19
    target 1327
  ]
  edge [
    source 19
    target 1328
  ]
  edge [
    source 19
    target 1329
  ]
  edge [
    source 19
    target 1330
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 1332
  ]
  edge [
    source 19
    target 1333
  ]
  edge [
    source 19
    target 1334
  ]
  edge [
    source 19
    target 1335
  ]
  edge [
    source 19
    target 1336
  ]
  edge [
    source 19
    target 1337
  ]
  edge [
    source 19
    target 1338
  ]
  edge [
    source 19
    target 1339
  ]
  edge [
    source 19
    target 1340
  ]
  edge [
    source 19
    target 1341
  ]
  edge [
    source 19
    target 1342
  ]
  edge [
    source 19
    target 1343
  ]
  edge [
    source 19
    target 1344
  ]
  edge [
    source 19
    target 1345
  ]
  edge [
    source 19
    target 1346
  ]
  edge [
    source 19
    target 1347
  ]
  edge [
    source 19
    target 1348
  ]
  edge [
    source 19
    target 1349
  ]
  edge [
    source 19
    target 1350
  ]
  edge [
    source 19
    target 1351
  ]
  edge [
    source 19
    target 1352
  ]
  edge [
    source 19
    target 1353
  ]
  edge [
    source 19
    target 1354
  ]
  edge [
    source 19
    target 1355
  ]
  edge [
    source 19
    target 1356
  ]
  edge [
    source 19
    target 1357
  ]
  edge [
    source 19
    target 1358
  ]
  edge [
    source 19
    target 1359
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1362
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 1364
  ]
  edge [
    source 19
    target 1365
  ]
  edge [
    source 19
    target 1366
  ]
  edge [
    source 19
    target 1367
  ]
  edge [
    source 19
    target 1368
  ]
  edge [
    source 19
    target 1369
  ]
  edge [
    source 19
    target 1370
  ]
  edge [
    source 19
    target 1371
  ]
  edge [
    source 19
    target 1372
  ]
  edge [
    source 19
    target 1373
  ]
  edge [
    source 19
    target 1374
  ]
  edge [
    source 19
    target 1375
  ]
  edge [
    source 19
    target 1376
  ]
  edge [
    source 19
    target 1377
  ]
  edge [
    source 19
    target 1378
  ]
  edge [
    source 19
    target 1379
  ]
  edge [
    source 19
    target 1380
  ]
  edge [
    source 19
    target 1381
  ]
  edge [
    source 19
    target 1382
  ]
  edge [
    source 19
    target 1383
  ]
  edge [
    source 19
    target 1384
  ]
  edge [
    source 19
    target 1385
  ]
  edge [
    source 19
    target 1386
  ]
  edge [
    source 19
    target 1387
  ]
  edge [
    source 19
    target 1388
  ]
  edge [
    source 19
    target 1389
  ]
  edge [
    source 19
    target 1390
  ]
  edge [
    source 19
    target 1391
  ]
  edge [
    source 19
    target 1392
  ]
  edge [
    source 19
    target 1393
  ]
  edge [
    source 19
    target 1394
  ]
  edge [
    source 19
    target 1395
  ]
  edge [
    source 19
    target 1396
  ]
  edge [
    source 19
    target 1397
  ]
  edge [
    source 19
    target 1398
  ]
  edge [
    source 19
    target 1399
  ]
  edge [
    source 19
    target 1400
  ]
  edge [
    source 19
    target 1401
  ]
  edge [
    source 19
    target 1402
  ]
  edge [
    source 19
    target 1403
  ]
  edge [
    source 19
    target 1404
  ]
  edge [
    source 19
    target 1405
  ]
  edge [
    source 19
    target 1406
  ]
  edge [
    source 19
    target 1407
  ]
  edge [
    source 19
    target 1408
  ]
  edge [
    source 19
    target 1409
  ]
  edge [
    source 19
    target 1410
  ]
  edge [
    source 19
    target 1411
  ]
  edge [
    source 19
    target 1412
  ]
  edge [
    source 19
    target 1413
  ]
  edge [
    source 19
    target 1414
  ]
  edge [
    source 19
    target 1415
  ]
  edge [
    source 19
    target 1416
  ]
  edge [
    source 19
    target 1417
  ]
  edge [
    source 19
    target 1418
  ]
  edge [
    source 19
    target 1419
  ]
  edge [
    source 19
    target 1420
  ]
  edge [
    source 19
    target 1421
  ]
  edge [
    source 19
    target 1422
  ]
  edge [
    source 19
    target 1423
  ]
  edge [
    source 19
    target 1424
  ]
  edge [
    source 19
    target 1425
  ]
  edge [
    source 19
    target 1426
  ]
  edge [
    source 19
    target 1427
  ]
  edge [
    source 19
    target 1428
  ]
  edge [
    source 19
    target 1429
  ]
  edge [
    source 19
    target 1430
  ]
  edge [
    source 19
    target 1431
  ]
  edge [
    source 19
    target 1432
  ]
  edge [
    source 19
    target 1433
  ]
  edge [
    source 19
    target 1434
  ]
  edge [
    source 19
    target 1435
  ]
  edge [
    source 19
    target 1436
  ]
  edge [
    source 19
    target 1437
  ]
  edge [
    source 19
    target 1438
  ]
  edge [
    source 19
    target 1439
  ]
  edge [
    source 19
    target 1440
  ]
  edge [
    source 19
    target 1441
  ]
  edge [
    source 19
    target 1442
  ]
  edge [
    source 19
    target 1443
  ]
  edge [
    source 19
    target 1444
  ]
  edge [
    source 19
    target 1445
  ]
  edge [
    source 19
    target 1446
  ]
  edge [
    source 19
    target 1447
  ]
  edge [
    source 19
    target 1448
  ]
  edge [
    source 19
    target 1449
  ]
  edge [
    source 19
    target 1450
  ]
  edge [
    source 19
    target 1451
  ]
  edge [
    source 19
    target 1452
  ]
  edge [
    source 19
    target 1453
  ]
  edge [
    source 19
    target 1454
  ]
  edge [
    source 19
    target 1455
  ]
  edge [
    source 19
    target 1456
  ]
  edge [
    source 19
    target 1457
  ]
  edge [
    source 19
    target 1458
  ]
  edge [
    source 19
    target 1459
  ]
  edge [
    source 19
    target 1460
  ]
  edge [
    source 19
    target 1461
  ]
  edge [
    source 19
    target 1462
  ]
  edge [
    source 19
    target 1463
  ]
  edge [
    source 19
    target 1464
  ]
  edge [
    source 19
    target 1465
  ]
  edge [
    source 19
    target 1466
  ]
  edge [
    source 19
    target 1467
  ]
  edge [
    source 19
    target 1468
  ]
  edge [
    source 19
    target 1469
  ]
  edge [
    source 19
    target 1470
  ]
  edge [
    source 19
    target 1471
  ]
  edge [
    source 19
    target 1472
  ]
  edge [
    source 19
    target 1473
  ]
  edge [
    source 19
    target 1474
  ]
  edge [
    source 19
    target 1475
  ]
  edge [
    source 19
    target 1476
  ]
  edge [
    source 19
    target 1477
  ]
  edge [
    source 19
    target 1478
  ]
  edge [
    source 19
    target 1479
  ]
  edge [
    source 19
    target 1480
  ]
  edge [
    source 19
    target 1481
  ]
  edge [
    source 19
    target 1482
  ]
  edge [
    source 19
    target 1483
  ]
  edge [
    source 19
    target 1484
  ]
  edge [
    source 19
    target 1485
  ]
  edge [
    source 19
    target 1486
  ]
  edge [
    source 19
    target 1487
  ]
  edge [
    source 19
    target 1488
  ]
  edge [
    source 19
    target 1489
  ]
  edge [
    source 19
    target 1490
  ]
  edge [
    source 19
    target 1491
  ]
  edge [
    source 19
    target 1492
  ]
  edge [
    source 19
    target 1493
  ]
  edge [
    source 19
    target 1494
  ]
  edge [
    source 19
    target 1495
  ]
  edge [
    source 19
    target 1496
  ]
  edge [
    source 19
    target 1497
  ]
  edge [
    source 19
    target 1498
  ]
  edge [
    source 19
    target 1499
  ]
  edge [
    source 19
    target 1500
  ]
  edge [
    source 19
    target 1501
  ]
  edge [
    source 19
    target 1502
  ]
  edge [
    source 19
    target 1503
  ]
  edge [
    source 19
    target 1504
  ]
  edge [
    source 19
    target 1505
  ]
  edge [
    source 19
    target 1506
  ]
  edge [
    source 19
    target 1507
  ]
  edge [
    source 19
    target 1508
  ]
  edge [
    source 19
    target 1509
  ]
  edge [
    source 19
    target 1510
  ]
  edge [
    source 19
    target 1511
  ]
  edge [
    source 19
    target 1512
  ]
  edge [
    source 19
    target 1513
  ]
  edge [
    source 19
    target 1514
  ]
  edge [
    source 19
    target 1515
  ]
  edge [
    source 19
    target 1516
  ]
  edge [
    source 19
    target 1517
  ]
  edge [
    source 19
    target 1518
  ]
  edge [
    source 19
    target 1519
  ]
  edge [
    source 19
    target 1520
  ]
  edge [
    source 19
    target 1521
  ]
  edge [
    source 19
    target 1522
  ]
  edge [
    source 19
    target 1523
  ]
  edge [
    source 19
    target 1524
  ]
  edge [
    source 19
    target 1525
  ]
  edge [
    source 19
    target 1526
  ]
  edge [
    source 19
    target 1527
  ]
  edge [
    source 19
    target 1528
  ]
  edge [
    source 19
    target 1529
  ]
  edge [
    source 19
    target 1530
  ]
  edge [
    source 19
    target 1531
  ]
  edge [
    source 19
    target 1532
  ]
  edge [
    source 19
    target 1533
  ]
  edge [
    source 19
    target 1534
  ]
  edge [
    source 19
    target 1535
  ]
  edge [
    source 19
    target 1536
  ]
  edge [
    source 19
    target 1537
  ]
  edge [
    source 19
    target 1538
  ]
  edge [
    source 19
    target 1539
  ]
  edge [
    source 19
    target 1540
  ]
  edge [
    source 19
    target 1541
  ]
  edge [
    source 19
    target 1542
  ]
  edge [
    source 19
    target 1543
  ]
  edge [
    source 19
    target 1544
  ]
  edge [
    source 19
    target 1545
  ]
  edge [
    source 19
    target 1546
  ]
  edge [
    source 19
    target 1547
  ]
  edge [
    source 19
    target 1548
  ]
  edge [
    source 19
    target 1549
  ]
  edge [
    source 19
    target 1550
  ]
  edge [
    source 19
    target 1551
  ]
  edge [
    source 19
    target 1552
  ]
  edge [
    source 19
    target 1553
  ]
  edge [
    source 19
    target 1554
  ]
  edge [
    source 19
    target 1555
  ]
  edge [
    source 19
    target 1556
  ]
  edge [
    source 19
    target 1557
  ]
  edge [
    source 19
    target 1558
  ]
  edge [
    source 19
    target 1559
  ]
  edge [
    source 19
    target 1560
  ]
  edge [
    source 19
    target 1561
  ]
  edge [
    source 19
    target 1562
  ]
  edge [
    source 19
    target 1563
  ]
  edge [
    source 19
    target 1564
  ]
  edge [
    source 19
    target 1565
  ]
  edge [
    source 19
    target 1566
  ]
  edge [
    source 19
    target 1567
  ]
  edge [
    source 19
    target 1568
  ]
  edge [
    source 19
    target 1569
  ]
  edge [
    source 19
    target 1570
  ]
  edge [
    source 19
    target 1571
  ]
  edge [
    source 19
    target 1572
  ]
  edge [
    source 19
    target 1573
  ]
  edge [
    source 19
    target 1574
  ]
  edge [
    source 19
    target 1575
  ]
  edge [
    source 19
    target 1576
  ]
  edge [
    source 19
    target 1577
  ]
  edge [
    source 19
    target 1578
  ]
  edge [
    source 19
    target 1579
  ]
  edge [
    source 19
    target 1580
  ]
  edge [
    source 19
    target 1581
  ]
  edge [
    source 19
    target 1582
  ]
  edge [
    source 19
    target 1583
  ]
  edge [
    source 19
    target 1584
  ]
  edge [
    source 19
    target 1585
  ]
  edge [
    source 19
    target 1586
  ]
  edge [
    source 19
    target 1587
  ]
  edge [
    source 19
    target 1588
  ]
  edge [
    source 19
    target 1589
  ]
  edge [
    source 19
    target 1590
  ]
  edge [
    source 19
    target 1591
  ]
  edge [
    source 19
    target 1592
  ]
  edge [
    source 19
    target 1593
  ]
  edge [
    source 19
    target 1594
  ]
  edge [
    source 19
    target 1595
  ]
  edge [
    source 19
    target 1596
  ]
  edge [
    source 19
    target 1597
  ]
  edge [
    source 19
    target 1598
  ]
  edge [
    source 19
    target 1599
  ]
  edge [
    source 19
    target 1600
  ]
  edge [
    source 19
    target 1601
  ]
  edge [
    source 19
    target 1602
  ]
  edge [
    source 19
    target 1603
  ]
  edge [
    source 19
    target 1604
  ]
  edge [
    source 19
    target 1605
  ]
  edge [
    source 19
    target 1606
  ]
  edge [
    source 19
    target 1607
  ]
  edge [
    source 19
    target 1608
  ]
  edge [
    source 19
    target 1609
  ]
  edge [
    source 19
    target 1610
  ]
  edge [
    source 19
    target 1611
  ]
  edge [
    source 19
    target 1612
  ]
  edge [
    source 19
    target 1613
  ]
  edge [
    source 19
    target 1614
  ]
  edge [
    source 19
    target 1615
  ]
  edge [
    source 19
    target 1616
  ]
  edge [
    source 19
    target 1617
  ]
  edge [
    source 19
    target 1618
  ]
  edge [
    source 19
    target 1619
  ]
  edge [
    source 19
    target 1620
  ]
  edge [
    source 19
    target 1621
  ]
  edge [
    source 19
    target 1622
  ]
  edge [
    source 19
    target 1623
  ]
  edge [
    source 19
    target 1624
  ]
  edge [
    source 19
    target 1625
  ]
  edge [
    source 19
    target 1626
  ]
  edge [
    source 19
    target 1627
  ]
  edge [
    source 19
    target 1628
  ]
  edge [
    source 19
    target 1629
  ]
  edge [
    source 19
    target 1630
  ]
  edge [
    source 19
    target 1631
  ]
  edge [
    source 19
    target 1632
  ]
  edge [
    source 19
    target 1633
  ]
  edge [
    source 19
    target 1634
  ]
  edge [
    source 19
    target 1635
  ]
  edge [
    source 19
    target 1636
  ]
  edge [
    source 19
    target 1637
  ]
  edge [
    source 19
    target 1638
  ]
  edge [
    source 19
    target 1639
  ]
  edge [
    source 19
    target 1640
  ]
  edge [
    source 19
    target 1641
  ]
  edge [
    source 19
    target 1642
  ]
  edge [
    source 19
    target 1643
  ]
  edge [
    source 19
    target 1644
  ]
  edge [
    source 19
    target 1645
  ]
  edge [
    source 19
    target 1646
  ]
  edge [
    source 19
    target 1647
  ]
  edge [
    source 19
    target 1648
  ]
  edge [
    source 19
    target 1649
  ]
  edge [
    source 19
    target 1650
  ]
  edge [
    source 19
    target 1651
  ]
  edge [
    source 19
    target 1652
  ]
  edge [
    source 19
    target 1653
  ]
  edge [
    source 19
    target 1654
  ]
  edge [
    source 19
    target 1655
  ]
  edge [
    source 19
    target 1656
  ]
  edge [
    source 19
    target 1657
  ]
  edge [
    source 19
    target 1658
  ]
  edge [
    source 19
    target 1659
  ]
  edge [
    source 19
    target 1660
  ]
  edge [
    source 19
    target 1661
  ]
  edge [
    source 19
    target 1662
  ]
  edge [
    source 19
    target 1663
  ]
  edge [
    source 19
    target 1664
  ]
  edge [
    source 19
    target 1665
  ]
  edge [
    source 19
    target 1666
  ]
  edge [
    source 19
    target 1667
  ]
  edge [
    source 19
    target 1668
  ]
  edge [
    source 19
    target 1669
  ]
  edge [
    source 19
    target 1670
  ]
  edge [
    source 19
    target 1671
  ]
  edge [
    source 19
    target 1672
  ]
  edge [
    source 19
    target 1673
  ]
  edge [
    source 19
    target 1674
  ]
  edge [
    source 19
    target 1675
  ]
  edge [
    source 19
    target 1676
  ]
  edge [
    source 19
    target 1677
  ]
  edge [
    source 19
    target 1678
  ]
  edge [
    source 19
    target 1679
  ]
  edge [
    source 19
    target 1680
  ]
  edge [
    source 19
    target 1681
  ]
  edge [
    source 19
    target 1682
  ]
  edge [
    source 19
    target 1683
  ]
  edge [
    source 19
    target 1684
  ]
  edge [
    source 19
    target 1685
  ]
  edge [
    source 19
    target 1686
  ]
  edge [
    source 19
    target 1687
  ]
  edge [
    source 19
    target 1688
  ]
  edge [
    source 19
    target 1689
  ]
  edge [
    source 19
    target 1690
  ]
  edge [
    source 19
    target 1691
  ]
  edge [
    source 19
    target 1692
  ]
  edge [
    source 19
    target 1693
  ]
  edge [
    source 19
    target 1694
  ]
  edge [
    source 19
    target 1695
  ]
  edge [
    source 19
    target 1696
  ]
  edge [
    source 19
    target 1697
  ]
  edge [
    source 19
    target 1698
  ]
  edge [
    source 19
    target 1699
  ]
  edge [
    source 19
    target 1700
  ]
  edge [
    source 19
    target 1701
  ]
  edge [
    source 19
    target 1702
  ]
  edge [
    source 19
    target 1703
  ]
  edge [
    source 19
    target 1704
  ]
  edge [
    source 19
    target 1705
  ]
  edge [
    source 19
    target 1706
  ]
  edge [
    source 19
    target 1707
  ]
  edge [
    source 19
    target 1708
  ]
  edge [
    source 19
    target 1709
  ]
  edge [
    source 19
    target 1710
  ]
  edge [
    source 19
    target 1711
  ]
  edge [
    source 19
    target 1712
  ]
  edge [
    source 19
    target 1713
  ]
  edge [
    source 19
    target 1714
  ]
  edge [
    source 19
    target 1715
  ]
  edge [
    source 19
    target 1716
  ]
  edge [
    source 19
    target 1717
  ]
  edge [
    source 19
    target 1718
  ]
  edge [
    source 19
    target 1719
  ]
  edge [
    source 19
    target 1720
  ]
  edge [
    source 19
    target 1721
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 1722
  ]
  edge [
    source 19
    target 1723
  ]
  edge [
    source 19
    target 1724
  ]
  edge [
    source 19
    target 1725
  ]
  edge [
    source 19
    target 1726
  ]
  edge [
    source 19
    target 1727
  ]
  edge [
    source 19
    target 388
  ]
  edge [
    source 19
    target 524
  ]
  edge [
    source 19
    target 1728
  ]
  edge [
    source 19
    target 1729
  ]
  edge [
    source 19
    target 1730
  ]
  edge [
    source 19
    target 1731
  ]
  edge [
    source 19
    target 463
  ]
  edge [
    source 19
    target 1732
  ]
  edge [
    source 19
    target 1733
  ]
  edge [
    source 19
    target 1734
  ]
  edge [
    source 19
    target 1735
  ]
  edge [
    source 19
    target 1736
  ]
  edge [
    source 19
    target 519
  ]
  edge [
    source 19
    target 1737
  ]
  edge [
    source 19
    target 1738
  ]
  edge [
    source 19
    target 1739
  ]
  edge [
    source 19
    target 1740
  ]
  edge [
    source 19
    target 1741
  ]
  edge [
    source 19
    target 1742
  ]
  edge [
    source 19
    target 1743
  ]
  edge [
    source 19
    target 1744
  ]
  edge [
    source 19
    target 1745
  ]
  edge [
    source 19
    target 1746
  ]
  edge [
    source 19
    target 1747
  ]
  edge [
    source 19
    target 1748
  ]
  edge [
    source 19
    target 1749
  ]
  edge [
    source 19
    target 1750
  ]
  edge [
    source 19
    target 1751
  ]
  edge [
    source 19
    target 1752
  ]
  edge [
    source 19
    target 1753
  ]
  edge [
    source 19
    target 1754
  ]
  edge [
    source 19
    target 1755
  ]
  edge [
    source 19
    target 521
  ]
  edge [
    source 19
    target 1756
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1757
  ]
  edge [
    source 20
    target 1758
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 20
    target 1759
  ]
  edge [
    source 20
    target 1760
  ]
  edge [
    source 20
    target 1761
  ]
  edge [
    source 20
    target 817
  ]
  edge [
    source 20
    target 1762
  ]
  edge [
    source 20
    target 1763
  ]
  edge [
    source 20
    target 1764
  ]
  edge [
    source 20
    target 1765
  ]
  edge [
    source 20
    target 1766
  ]
  edge [
    source 20
    target 1767
  ]
  edge [
    source 20
    target 1768
  ]
  edge [
    source 20
    target 1769
  ]
  edge [
    source 20
    target 1770
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 1771
  ]
  edge [
    source 20
    target 1772
  ]
  edge [
    source 20
    target 1773
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 1774
  ]
  edge [
    source 20
    target 1775
  ]
  edge [
    source 20
    target 1776
  ]
  edge [
    source 20
    target 1777
  ]
  edge [
    source 20
    target 1778
  ]
  edge [
    source 20
    target 1779
  ]
  edge [
    source 20
    target 1780
  ]
  edge [
    source 20
    target 1781
  ]
  edge [
    source 20
    target 1782
  ]
  edge [
    source 20
    target 1783
  ]
  edge [
    source 20
    target 1784
  ]
  edge [
    source 20
    target 1785
  ]
  edge [
    source 20
    target 1786
  ]
  edge [
    source 20
    target 1787
  ]
  edge [
    source 20
    target 1788
  ]
  edge [
    source 20
    target 1789
  ]
  edge [
    source 20
    target 1790
  ]
  edge [
    source 20
    target 1791
  ]
  edge [
    source 20
    target 1792
  ]
  edge [
    source 20
    target 1793
  ]
  edge [
    source 20
    target 1794
  ]
  edge [
    source 20
    target 1795
  ]
  edge [
    source 20
    target 1796
  ]
  edge [
    source 20
    target 1797
  ]
  edge [
    source 20
    target 1798
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1799
  ]
  edge [
    source 21
    target 1800
  ]
  edge [
    source 21
    target 1801
  ]
  edge [
    source 21
    target 1802
  ]
  edge [
    source 21
    target 1803
  ]
  edge [
    source 21
    target 1804
  ]
  edge [
    source 21
    target 50
  ]
  edge [
    source 21
    target 1805
  ]
  edge [
    source 21
    target 1806
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 1807
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 1808
  ]
  edge [
    source 21
    target 1809
  ]
  edge [
    source 21
    target 1810
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 1811
  ]
  edge [
    source 21
    target 1779
  ]
  edge [
    source 21
    target 1812
  ]
  edge [
    source 21
    target 1813
  ]
  edge [
    source 21
    target 206
  ]
  edge [
    source 21
    target 1814
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 85
  ]
  edge [
    source 21
    target 1815
  ]
  edge [
    source 21
    target 1816
  ]
  edge [
    source 21
    target 1817
  ]
  edge [
    source 21
    target 1818
  ]
  edge [
    source 21
    target 1819
  ]
  edge [
    source 21
    target 122
  ]
  edge [
    source 21
    target 1820
  ]
  edge [
    source 21
    target 472
  ]
  edge [
    source 21
    target 103
  ]
  edge [
    source 21
    target 1821
  ]
  edge [
    source 21
    target 1822
  ]
  edge [
    source 21
    target 1823
  ]
  edge [
    source 21
    target 1824
  ]
  edge [
    source 21
    target 1825
  ]
  edge [
    source 21
    target 1826
  ]
  edge [
    source 21
    target 1827
  ]
  edge [
    source 21
    target 1828
  ]
  edge [
    source 21
    target 1829
  ]
  edge [
    source 21
    target 1830
  ]
  edge [
    source 21
    target 1831
  ]
  edge [
    source 21
    target 1832
  ]
  edge [
    source 21
    target 1833
  ]
  edge [
    source 21
    target 1834
  ]
  edge [
    source 21
    target 1835
  ]
  edge [
    source 21
    target 1836
  ]
  edge [
    source 21
    target 1837
  ]
  edge [
    source 21
    target 1838
  ]
  edge [
    source 21
    target 1839
  ]
  edge [
    source 21
    target 1840
  ]
  edge [
    source 21
    target 1841
  ]
  edge [
    source 21
    target 1842
  ]
  edge [
    source 21
    target 1843
  ]
  edge [
    source 21
    target 1844
  ]
  edge [
    source 21
    target 1845
  ]
  edge [
    source 21
    target 1846
  ]
  edge [
    source 21
    target 1847
  ]
  edge [
    source 21
    target 1848
  ]
  edge [
    source 21
    target 1849
  ]
  edge [
    source 21
    target 1850
  ]
  edge [
    source 21
    target 1851
  ]
  edge [
    source 21
    target 1852
  ]
  edge [
    source 21
    target 1853
  ]
  edge [
    source 21
    target 1854
  ]
  edge [
    source 21
    target 1855
  ]
  edge [
    source 21
    target 1856
  ]
  edge [
    source 21
    target 1857
  ]
  edge [
    source 21
    target 1858
  ]
  edge [
    source 23
    target 288
  ]
  edge [
    source 23
    target 289
  ]
  edge [
    source 23
    target 290
  ]
  edge [
    source 23
    target 291
  ]
  edge [
    source 23
    target 292
  ]
  edge [
    source 23
    target 293
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 295
  ]
  edge [
    source 23
    target 296
  ]
  edge [
    source 23
    target 297
  ]
  edge [
    source 23
    target 298
  ]
  edge [
    source 23
    target 1859
  ]
  edge [
    source 23
    target 309
  ]
  edge [
    source 23
    target 1860
  ]
  edge [
    source 23
    target 1861
  ]
  edge [
    source 23
    target 1862
  ]
  edge [
    source 23
    target 1863
  ]
  edge [
    source 23
    target 1864
  ]
  edge [
    source 23
    target 1865
  ]
  edge [
    source 23
    target 828
  ]
  edge [
    source 23
    target 1866
  ]
  edge [
    source 23
    target 642
  ]
  edge [
    source 23
    target 651
  ]
  edge [
    source 23
    target 1867
  ]
  edge [
    source 23
    target 1868
  ]
  edge [
    source 23
    target 1869
  ]
  edge [
    source 23
    target 827
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 1870
  ]
  edge [
    source 23
    target 1871
  ]
  edge [
    source 23
    target 1872
  ]
  edge [
    source 23
    target 630
  ]
  edge [
    source 23
    target 100
  ]
  edge [
    source 23
    target 1873
  ]
  edge [
    source 23
    target 1874
  ]
  edge [
    source 23
    target 334
  ]
  edge [
    source 23
    target 1875
  ]
  edge [
    source 23
    target 1876
  ]
  edge [
    source 23
    target 631
  ]
  edge [
    source 23
    target 1877
  ]
  edge [
    source 23
    target 1878
  ]
  edge [
    source 23
    target 1879
  ]
  edge [
    source 23
    target 621
  ]
  edge [
    source 23
    target 1880
  ]
  edge [
    source 23
    target 1881
  ]
  edge [
    source 23
    target 1882
  ]
  edge [
    source 23
    target 1883
  ]
  edge [
    source 23
    target 1884
  ]
  edge [
    source 23
    target 957
  ]
  edge [
    source 23
    target 1885
  ]
  edge [
    source 23
    target 1886
  ]
  edge [
    source 23
    target 1887
  ]
  edge [
    source 23
    target 1888
  ]
  edge [
    source 23
    target 1889
  ]
  edge [
    source 23
    target 776
  ]
  edge [
    source 23
    target 1890
  ]
  edge [
    source 23
    target 1891
  ]
  edge [
    source 23
    target 1892
  ]
  edge [
    source 23
    target 1893
  ]
  edge [
    source 23
    target 1894
  ]
  edge [
    source 23
    target 1895
  ]
  edge [
    source 23
    target 1896
  ]
  edge [
    source 23
    target 1227
  ]
  edge [
    source 23
    target 1897
  ]
  edge [
    source 23
    target 1229
  ]
  edge [
    source 23
    target 523
  ]
  edge [
    source 23
    target 1898
  ]
  edge [
    source 23
    target 1230
  ]
  edge [
    source 23
    target 1899
  ]
  edge [
    source 23
    target 1778
  ]
  edge [
    source 23
    target 1224
  ]
  edge [
    source 23
    target 1232
  ]
  edge [
    source 23
    target 1233
  ]
  edge [
    source 23
    target 1900
  ]
  edge [
    source 23
    target 1901
  ]
  edge [
    source 23
    target 1234
  ]
  edge [
    source 23
    target 1235
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 1236
  ]
  edge [
    source 23
    target 1237
  ]
  edge [
    source 23
    target 1446
  ]
  edge [
    source 23
    target 1238
  ]
  edge [
    source 23
    target 1239
  ]
  edge [
    source 23
    target 770
  ]
  edge [
    source 23
    target 1240
  ]
  edge [
    source 23
    target 1241
  ]
  edge [
    source 23
    target 1775
  ]
  edge [
    source 23
    target 1242
  ]
  edge [
    source 23
    target 1902
  ]
  edge [
    source 23
    target 1245
  ]
  edge [
    source 23
    target 1903
  ]
  edge [
    source 23
    target 1246
  ]
  edge [
    source 23
    target 1247
  ]
  edge [
    source 23
    target 1249
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 1250
  ]
  edge [
    source 23
    target 1251
  ]
  edge [
    source 23
    target 1254
  ]
  edge [
    source 23
    target 1253
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 1904
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1905
  ]
  edge [
    source 24
    target 1906
  ]
  edge [
    source 24
    target 1907
  ]
  edge [
    source 24
    target 1908
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 799
  ]
  edge [
    source 26
    target 800
  ]
  edge [
    source 26
    target 801
  ]
  edge [
    source 26
    target 802
  ]
  edge [
    source 26
    target 521
  ]
  edge [
    source 26
    target 803
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 47
  ]
  edge [
    source 26
    target 804
  ]
  edge [
    source 26
    target 100
  ]
  edge [
    source 26
    target 484
  ]
  edge [
    source 26
    target 805
  ]
  edge [
    source 26
    target 806
  ]
  edge [
    source 26
    target 1909
  ]
  edge [
    source 26
    target 1910
  ]
  edge [
    source 26
    target 1911
  ]
  edge [
    source 26
    target 1912
  ]
  edge [
    source 26
    target 1913
  ]
  edge [
    source 26
    target 1914
  ]
  edge [
    source 26
    target 1394
  ]
  edge [
    source 26
    target 134
  ]
  edge [
    source 26
    target 1915
  ]
  edge [
    source 26
    target 294
  ]
  edge [
    source 26
    target 1916
  ]
  edge [
    source 26
    target 1917
  ]
  edge [
    source 26
    target 1918
  ]
  edge [
    source 26
    target 1919
  ]
  edge [
    source 26
    target 1920
  ]
  edge [
    source 26
    target 1921
  ]
  edge [
    source 26
    target 1624
  ]
  edge [
    source 26
    target 1922
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 328
  ]
  edge [
    source 26
    target 1923
  ]
  edge [
    source 26
    target 1924
  ]
  edge [
    source 26
    target 1925
  ]
  edge [
    source 26
    target 1926
  ]
  edge [
    source 26
    target 1927
  ]
  edge [
    source 26
    target 1928
  ]
  edge [
    source 26
    target 1929
  ]
  edge [
    source 26
    target 1930
  ]
  edge [
    source 26
    target 1931
  ]
  edge [
    source 26
    target 1932
  ]
  edge [
    source 26
    target 1933
  ]
  edge [
    source 26
    target 1934
  ]
  edge [
    source 26
    target 1935
  ]
  edge [
    source 26
    target 1936
  ]
  edge [
    source 26
    target 1937
  ]
  edge [
    source 26
    target 1938
  ]
  edge [
    source 26
    target 1939
  ]
  edge [
    source 26
    target 632
  ]
  edge [
    source 26
    target 126
  ]
  edge [
    source 26
    target 1145
  ]
  edge [
    source 26
    target 1725
  ]
  edge [
    source 26
    target 1940
  ]
  edge [
    source 26
    target 1941
  ]
  edge [
    source 26
    target 1942
  ]
  edge [
    source 26
    target 1943
  ]
  edge [
    source 26
    target 1944
  ]
  edge [
    source 26
    target 1945
  ]
  edge [
    source 26
    target 770
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 26
    target 1946
  ]
  edge [
    source 26
    target 1947
  ]
  edge [
    source 26
    target 1948
  ]
  edge [
    source 26
    target 1949
  ]
  edge [
    source 26
    target 1950
  ]
  edge [
    source 26
    target 1951
  ]
  edge [
    source 26
    target 1952
  ]
  edge [
    source 26
    target 1953
  ]
  edge [
    source 26
    target 1954
  ]
  edge [
    source 26
    target 1955
  ]
  edge [
    source 26
    target 1956
  ]
  edge [
    source 26
    target 1957
  ]
  edge [
    source 26
    target 1958
  ]
  edge [
    source 26
    target 1959
  ]
  edge [
    source 26
    target 817
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 1960
  ]
  edge [
    source 26
    target 1961
  ]
  edge [
    source 26
    target 1962
  ]
  edge [
    source 26
    target 104
  ]
  edge [
    source 26
    target 1963
  ]
  edge [
    source 26
    target 1964
  ]
  edge [
    source 26
    target 1965
  ]
  edge [
    source 26
    target 1123
  ]
  edge [
    source 26
    target 519
  ]
  edge [
    source 26
    target 1966
  ]
  edge [
    source 26
    target 1967
  ]
  edge [
    source 26
    target 1098
  ]
  edge [
    source 26
    target 1968
  ]
  edge [
    source 26
    target 1969
  ]
  edge [
    source 26
    target 1970
  ]
  edge [
    source 26
    target 1971
  ]
  edge [
    source 26
    target 1972
  ]
  edge [
    source 26
    target 1973
  ]
  edge [
    source 26
    target 1974
  ]
  edge [
    source 26
    target 1975
  ]
  edge [
    source 26
    target 1976
  ]
  edge [
    source 26
    target 1977
  ]
  edge [
    source 26
    target 1978
  ]
  edge [
    source 26
    target 1979
  ]
  edge [
    source 26
    target 1980
  ]
  edge [
    source 26
    target 1981
  ]
  edge [
    source 26
    target 1982
  ]
  edge [
    source 26
    target 1983
  ]
  edge [
    source 26
    target 1984
  ]
  edge [
    source 26
    target 1985
  ]
  edge [
    source 26
    target 1986
  ]
  edge [
    source 26
    target 809
  ]
  edge [
    source 26
    target 1987
  ]
  edge [
    source 26
    target 1988
  ]
  edge [
    source 26
    target 1989
  ]
  edge [
    source 26
    target 1990
  ]
  edge [
    source 26
    target 1991
  ]
  edge [
    source 26
    target 1992
  ]
  edge [
    source 26
    target 1993
  ]
  edge [
    source 26
    target 1994
  ]
  edge [
    source 26
    target 1995
  ]
  edge [
    source 26
    target 1996
  ]
  edge [
    source 26
    target 1997
  ]
  edge [
    source 26
    target 1998
  ]
  edge [
    source 26
    target 1999
  ]
  edge [
    source 26
    target 2000
  ]
  edge [
    source 26
    target 2001
  ]
  edge [
    source 26
    target 2002
  ]
  edge [
    source 26
    target 2003
  ]
  edge [
    source 26
    target 1115
  ]
  edge [
    source 26
    target 2004
  ]
  edge [
    source 26
    target 2005
  ]
  edge [
    source 26
    target 2006
  ]
  edge [
    source 26
    target 2007
  ]
  edge [
    source 26
    target 2008
  ]
  edge [
    source 26
    target 2009
  ]
  edge [
    source 26
    target 2010
  ]
  edge [
    source 26
    target 2011
  ]
  edge [
    source 26
    target 2012
  ]
  edge [
    source 26
    target 1118
  ]
  edge [
    source 26
    target 2013
  ]
  edge [
    source 26
    target 2014
  ]
  edge [
    source 26
    target 2015
  ]
  edge [
    source 26
    target 2016
  ]
  edge [
    source 26
    target 2017
  ]
  edge [
    source 26
    target 2018
  ]
  edge [
    source 26
    target 2019
  ]
  edge [
    source 26
    target 2020
  ]
  edge [
    source 26
    target 2021
  ]
  edge [
    source 26
    target 2022
  ]
  edge [
    source 26
    target 2023
  ]
  edge [
    source 26
    target 2024
  ]
  edge [
    source 26
    target 2025
  ]
  edge [
    source 26
    target 171
  ]
  edge [
    source 26
    target 1173
  ]
  edge [
    source 26
    target 2026
  ]
  edge [
    source 26
    target 2027
  ]
  edge [
    source 26
    target 2028
  ]
  edge [
    source 26
    target 173
  ]
  edge [
    source 26
    target 2029
  ]
  edge [
    source 26
    target 2030
  ]
  edge [
    source 26
    target 2031
  ]
  edge [
    source 26
    target 2032
  ]
  edge [
    source 26
    target 2033
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 2034
  ]
  edge [
    source 27
    target 2035
  ]
  edge [
    source 27
    target 1185
  ]
  edge [
    source 27
    target 2036
  ]
  edge [
    source 27
    target 2037
  ]
  edge [
    source 27
    target 1911
  ]
  edge [
    source 27
    target 2038
  ]
  edge [
    source 27
    target 2039
  ]
  edge [
    source 27
    target 2040
  ]
  edge [
    source 27
    target 2041
  ]
  edge [
    source 27
    target 2042
  ]
  edge [
    source 27
    target 2043
  ]
  edge [
    source 27
    target 2044
  ]
  edge [
    source 27
    target 2045
  ]
  edge [
    source 27
    target 2046
  ]
  edge [
    source 27
    target 2047
  ]
  edge [
    source 27
    target 847
  ]
  edge [
    source 27
    target 2048
  ]
  edge [
    source 27
    target 592
  ]
  edge [
    source 27
    target 2049
  ]
  edge [
    source 27
    target 2050
  ]
  edge [
    source 27
    target 2051
  ]
  edge [
    source 27
    target 2052
  ]
  edge [
    source 27
    target 2053
  ]
  edge [
    source 27
    target 2054
  ]
  edge [
    source 27
    target 2055
  ]
  edge [
    source 27
    target 2056
  ]
  edge [
    source 27
    target 2057
  ]
  edge [
    source 27
    target 2058
  ]
  edge [
    source 27
    target 547
  ]
  edge [
    source 27
    target 2059
  ]
  edge [
    source 27
    target 2060
  ]
  edge [
    source 27
    target 2061
  ]
  edge [
    source 27
    target 2062
  ]
  edge [
    source 27
    target 2063
  ]
  edge [
    source 27
    target 2064
  ]
  edge [
    source 27
    target 2065
  ]
  edge [
    source 27
    target 2066
  ]
  edge [
    source 27
    target 2067
  ]
  edge [
    source 27
    target 2068
  ]
  edge [
    source 27
    target 2069
  ]
  edge [
    source 27
    target 2070
  ]
  edge [
    source 27
    target 280
  ]
  edge [
    source 27
    target 2071
  ]
  edge [
    source 27
    target 2072
  ]
  edge [
    source 27
    target 2073
  ]
  edge [
    source 27
    target 2074
  ]
  edge [
    source 27
    target 2075
  ]
  edge [
    source 27
    target 2076
  ]
  edge [
    source 27
    target 2077
  ]
  edge [
    source 27
    target 2078
  ]
  edge [
    source 27
    target 2079
  ]
  edge [
    source 27
    target 2080
  ]
  edge [
    source 27
    target 2081
  ]
  edge [
    source 27
    target 2082
  ]
  edge [
    source 27
    target 2083
  ]
  edge [
    source 27
    target 2084
  ]
  edge [
    source 27
    target 2085
  ]
  edge [
    source 27
    target 2086
  ]
  edge [
    source 27
    target 2087
  ]
  edge [
    source 27
    target 2088
  ]
  edge [
    source 27
    target 2089
  ]
  edge [
    source 27
    target 2090
  ]
  edge [
    source 27
    target 308
  ]
  edge [
    source 27
    target 2091
  ]
  edge [
    source 27
    target 2092
  ]
  edge [
    source 27
    target 2093
  ]
  edge [
    source 27
    target 2094
  ]
  edge [
    source 27
    target 2095
  ]
  edge [
    source 27
    target 2096
  ]
  edge [
    source 27
    target 605
  ]
  edge [
    source 27
    target 2097
  ]
  edge [
    source 27
    target 2098
  ]
  edge [
    source 27
    target 2099
  ]
  edge [
    source 27
    target 2100
  ]
  edge [
    source 27
    target 2101
  ]
  edge [
    source 27
    target 2102
  ]
  edge [
    source 27
    target 601
  ]
  edge [
    source 27
    target 2103
  ]
  edge [
    source 27
    target 2104
  ]
  edge [
    source 27
    target 2105
  ]
  edge [
    source 27
    target 2106
  ]
  edge [
    source 27
    target 2107
  ]
  edge [
    source 27
    target 2108
  ]
  edge [
    source 27
    target 2109
  ]
  edge [
    source 27
    target 2110
  ]
  edge [
    source 27
    target 2111
  ]
  edge [
    source 27
    target 2112
  ]
  edge [
    source 27
    target 2113
  ]
  edge [
    source 27
    target 2114
  ]
  edge [
    source 27
    target 2115
  ]
  edge [
    source 27
    target 2116
  ]
  edge [
    source 27
    target 2117
  ]
  edge [
    source 27
    target 2118
  ]
  edge [
    source 27
    target 2119
  ]
  edge [
    source 27
    target 2120
  ]
  edge [
    source 27
    target 2121
  ]
  edge [
    source 27
    target 2122
  ]
  edge [
    source 27
    target 2123
  ]
  edge [
    source 27
    target 2124
  ]
  edge [
    source 27
    target 2125
  ]
  edge [
    source 27
    target 840
  ]
  edge [
    source 27
    target 314
  ]
  edge [
    source 27
    target 2126
  ]
  edge [
    source 27
    target 2127
  ]
  edge [
    source 27
    target 2128
  ]
  edge [
    source 27
    target 2129
  ]
  edge [
    source 27
    target 2130
  ]
  edge [
    source 27
    target 2131
  ]
  edge [
    source 27
    target 2132
  ]
  edge [
    source 27
    target 2133
  ]
  edge [
    source 27
    target 2134
  ]
  edge [
    source 27
    target 2135
  ]
  edge [
    source 27
    target 2136
  ]
  edge [
    source 27
    target 2137
  ]
  edge [
    source 27
    target 2138
  ]
  edge [
    source 27
    target 1806
  ]
  edge [
    source 27
    target 2139
  ]
  edge [
    source 27
    target 1936
  ]
  edge [
    source 27
    target 797
  ]
  edge [
    source 27
    target 2140
  ]
  edge [
    source 27
    target 2141
  ]
  edge [
    source 27
    target 788
  ]
  edge [
    source 27
    target 2142
  ]
  edge [
    source 27
    target 2143
  ]
  edge [
    source 27
    target 1914
  ]
  edge [
    source 27
    target 2144
  ]
  edge [
    source 27
    target 2145
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 898
    target 2146
  ]
  edge [
    source 2147
    target 2148
  ]
  edge [
    source 2148
    target 2151
  ]
  edge [
    source 2149
    target 2150
  ]
]
