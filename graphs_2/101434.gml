graph [
  node [
    id 0
    label "run"
    origin "text"
  ]
  node [
    id 1
    label "singel"
    origin "text"
  ]
  node [
    id 2
    label "leona"
    origin "text"
  ]
  node [
    id 3
    label "lewis"
    origin "text"
  ]
  node [
    id 4
    label "popyt"
  ]
  node [
    id 5
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 6
    label "proces_ekonomiczny"
  ]
  node [
    id 7
    label "cena_r&#243;wnowagi_rynkowej"
  ]
  node [
    id 8
    label "krzywa_popytu"
  ]
  node [
    id 9
    label "handel"
  ]
  node [
    id 10
    label "nawis_inflacyjny"
  ]
  node [
    id 11
    label "elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 12
    label "popyt_zagregowany"
  ]
  node [
    id 13
    label "lepko&#347;&#263;_cen"
  ]
  node [
    id 14
    label "czynnik_niecenowy"
  ]
  node [
    id 15
    label "piosenka"
  ]
  node [
    id 16
    label "p&#322;yta"
  ]
  node [
    id 17
    label "stan_wolny"
  ]
  node [
    id 18
    label "nie&#380;onaty"
  ]
  node [
    id 19
    label "singiel"
  ]
  node [
    id 20
    label "mecz"
  ]
  node [
    id 21
    label "karta"
  ]
  node [
    id 22
    label "kartka"
  ]
  node [
    id 23
    label "danie"
  ]
  node [
    id 24
    label "menu"
  ]
  node [
    id 25
    label "zezwolenie"
  ]
  node [
    id 26
    label "restauracja"
  ]
  node [
    id 27
    label "chart"
  ]
  node [
    id 28
    label "p&#322;ytka"
  ]
  node [
    id 29
    label "formularz"
  ]
  node [
    id 30
    label "ticket"
  ]
  node [
    id 31
    label "cennik"
  ]
  node [
    id 32
    label "oferta"
  ]
  node [
    id 33
    label "komputer"
  ]
  node [
    id 34
    label "charter"
  ]
  node [
    id 35
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 36
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 37
    label "kartonik"
  ]
  node [
    id 38
    label "urz&#261;dzenie"
  ]
  node [
    id 39
    label "circuit_board"
  ]
  node [
    id 40
    label "zanuci&#263;"
  ]
  node [
    id 41
    label "nucenie"
  ]
  node [
    id 42
    label "zwrotka"
  ]
  node [
    id 43
    label "utw&#243;r"
  ]
  node [
    id 44
    label "tekst"
  ]
  node [
    id 45
    label "nuci&#263;"
  ]
  node [
    id 46
    label "zanucenie"
  ]
  node [
    id 47
    label "piosnka"
  ]
  node [
    id 48
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 49
    label "kuchnia"
  ]
  node [
    id 50
    label "przedmiot"
  ]
  node [
    id 51
    label "nagranie"
  ]
  node [
    id 52
    label "AGD"
  ]
  node [
    id 53
    label "p&#322;ytoteka"
  ]
  node [
    id 54
    label "no&#347;nik_danych"
  ]
  node [
    id 55
    label "miejsce"
  ]
  node [
    id 56
    label "plate"
  ]
  node [
    id 57
    label "sheet"
  ]
  node [
    id 58
    label "dysk"
  ]
  node [
    id 59
    label "produkcja"
  ]
  node [
    id 60
    label "phonograph_record"
  ]
  node [
    id 61
    label "obrona"
  ]
  node [
    id 62
    label "gra"
  ]
  node [
    id 63
    label "game"
  ]
  node [
    id 64
    label "serw"
  ]
  node [
    id 65
    label "dwumecz"
  ]
  node [
    id 66
    label "samotny"
  ]
  node [
    id 67
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 68
    label "pan_m&#322;ody"
  ]
  node [
    id 69
    label "Leona"
  ]
  node [
    id 70
    label "Lewis"
  ]
  node [
    id 71
    label "BBC"
  ]
  node [
    id 72
    label "radio"
  ]
  node [
    id 73
    label "1s"
  ]
  node [
    id 74
    label "Live"
  ]
  node [
    id 75
    label "Lounge"
  ]
  node [
    id 76
    label "Snow"
  ]
  node [
    id 77
    label "patrol"
  ]
  node [
    id 78
    label "Spirit"
  ]
  node [
    id 79
    label "The"
  ]
  node [
    id 80
    label "Deluxe"
  ]
  node [
    id 81
    label "Edition"
  ]
  node [
    id 82
    label "wielki"
  ]
  node [
    id 83
    label "brytania"
  ]
  node [
    id 84
    label "stan"
  ]
  node [
    id 85
    label "zjednoczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 71
    target 74
  ]
  edge [
    source 71
    target 75
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 72
    target 75
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 75
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 80
  ]
  edge [
    source 78
    target 81
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 81
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 84
    target 85
  ]
]
