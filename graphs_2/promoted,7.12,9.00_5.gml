graph [
  node [
    id 0
    label "m&#322;oda"
    origin "text"
  ]
  node [
    id 1
    label "wierna"
    origin "text"
  ]
  node [
    id 2
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "suknia_&#347;lubna"
  ]
  node [
    id 4
    label "&#380;ona"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "kobieta"
  ]
  node [
    id 7
    label "niezam&#281;&#380;na"
  ]
  node [
    id 8
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 9
    label "ludzko&#347;&#263;"
  ]
  node [
    id 10
    label "asymilowanie"
  ]
  node [
    id 11
    label "wapniak"
  ]
  node [
    id 12
    label "asymilowa&#263;"
  ]
  node [
    id 13
    label "os&#322;abia&#263;"
  ]
  node [
    id 14
    label "posta&#263;"
  ]
  node [
    id 15
    label "hominid"
  ]
  node [
    id 16
    label "podw&#322;adny"
  ]
  node [
    id 17
    label "os&#322;abianie"
  ]
  node [
    id 18
    label "g&#322;owa"
  ]
  node [
    id 19
    label "figura"
  ]
  node [
    id 20
    label "portrecista"
  ]
  node [
    id 21
    label "dwun&#243;g"
  ]
  node [
    id 22
    label "profanum"
  ]
  node [
    id 23
    label "mikrokosmos"
  ]
  node [
    id 24
    label "nasada"
  ]
  node [
    id 25
    label "duch"
  ]
  node [
    id 26
    label "antropochoria"
  ]
  node [
    id 27
    label "osoba"
  ]
  node [
    id 28
    label "wz&#243;r"
  ]
  node [
    id 29
    label "senior"
  ]
  node [
    id 30
    label "oddzia&#322;ywanie"
  ]
  node [
    id 31
    label "Adam"
  ]
  node [
    id 32
    label "homo_sapiens"
  ]
  node [
    id 33
    label "polifag"
  ]
  node [
    id 34
    label "doros&#322;y"
  ]
  node [
    id 35
    label "samica"
  ]
  node [
    id 36
    label "uleganie"
  ]
  node [
    id 37
    label "ulec"
  ]
  node [
    id 38
    label "m&#281;&#380;yna"
  ]
  node [
    id 39
    label "partnerka"
  ]
  node [
    id 40
    label "ulegni&#281;cie"
  ]
  node [
    id 41
    label "pa&#324;stwo"
  ]
  node [
    id 42
    label "&#322;ono"
  ]
  node [
    id 43
    label "menopauza"
  ]
  node [
    id 44
    label "przekwitanie"
  ]
  node [
    id 45
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 46
    label "babka"
  ]
  node [
    id 47
    label "ulega&#263;"
  ]
  node [
    id 48
    label "ma&#322;&#380;onek"
  ]
  node [
    id 49
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 50
    label "&#347;lubna"
  ]
  node [
    id 51
    label "kobita"
  ]
  node [
    id 52
    label "panna_m&#322;oda"
  ]
  node [
    id 53
    label "kult"
  ]
  node [
    id 54
    label "ub&#322;agalnia"
  ]
  node [
    id 55
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 56
    label "nawa"
  ]
  node [
    id 57
    label "wsp&#243;lnota"
  ]
  node [
    id 58
    label "Ska&#322;ka"
  ]
  node [
    id 59
    label "zakrystia"
  ]
  node [
    id 60
    label "prezbiterium"
  ]
  node [
    id 61
    label "kropielnica"
  ]
  node [
    id 62
    label "organizacja_religijna"
  ]
  node [
    id 63
    label "nerwica_eklezjogenna"
  ]
  node [
    id 64
    label "church"
  ]
  node [
    id 65
    label "kruchta"
  ]
  node [
    id 66
    label "dom"
  ]
  node [
    id 67
    label "zwi&#261;zanie"
  ]
  node [
    id 68
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 69
    label "podobie&#324;stwo"
  ]
  node [
    id 70
    label "Skandynawia"
  ]
  node [
    id 71
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 72
    label "partnership"
  ]
  node [
    id 73
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 74
    label "wi&#261;zanie"
  ]
  node [
    id 75
    label "Ba&#322;kany"
  ]
  node [
    id 76
    label "society"
  ]
  node [
    id 77
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 78
    label "zwi&#261;za&#263;"
  ]
  node [
    id 79
    label "Walencja"
  ]
  node [
    id 80
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 81
    label "bratnia_dusza"
  ]
  node [
    id 82
    label "zwi&#261;zek"
  ]
  node [
    id 83
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 84
    label "marriage"
  ]
  node [
    id 85
    label "przybytek"
  ]
  node [
    id 86
    label "siedlisko"
  ]
  node [
    id 87
    label "budynek"
  ]
  node [
    id 88
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 89
    label "rodzina"
  ]
  node [
    id 90
    label "substancja_mieszkaniowa"
  ]
  node [
    id 91
    label "instytucja"
  ]
  node [
    id 92
    label "siedziba"
  ]
  node [
    id 93
    label "dom_rodzinny"
  ]
  node [
    id 94
    label "grupa"
  ]
  node [
    id 95
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 96
    label "poj&#281;cie"
  ]
  node [
    id 97
    label "stead"
  ]
  node [
    id 98
    label "garderoba"
  ]
  node [
    id 99
    label "wiecha"
  ]
  node [
    id 100
    label "fratria"
  ]
  node [
    id 101
    label "uwielbienie"
  ]
  node [
    id 102
    label "religia"
  ]
  node [
    id 103
    label "translacja"
  ]
  node [
    id 104
    label "postawa"
  ]
  node [
    id 105
    label "egzegeta"
  ]
  node [
    id 106
    label "worship"
  ]
  node [
    id 107
    label "obrz&#281;d"
  ]
  node [
    id 108
    label "babiniec"
  ]
  node [
    id 109
    label "przedsionek"
  ]
  node [
    id 110
    label "pomieszczenie"
  ]
  node [
    id 111
    label "zesp&#243;&#322;"
  ]
  node [
    id 112
    label "korpus"
  ]
  node [
    id 113
    label "&#347;rodowisko"
  ]
  node [
    id 114
    label "o&#322;tarz"
  ]
  node [
    id 115
    label "stalle"
  ]
  node [
    id 116
    label "lampka_wieczysta"
  ]
  node [
    id 117
    label "tabernakulum"
  ]
  node [
    id 118
    label "duchowie&#324;stwo"
  ]
  node [
    id 119
    label "naczynie_na_wod&#281;_&#347;wi&#281;con&#261;"
  ]
  node [
    id 120
    label "paramenty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
]
