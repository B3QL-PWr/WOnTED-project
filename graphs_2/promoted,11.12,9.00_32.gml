graph [
  node [
    id 0
    label "cze&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "mirka"
    origin "text"
  ]
  node [
    id 2
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mirabelka"
    origin "text"
  ]
  node [
    id 4
    label "honorowa&#263;"
  ]
  node [
    id 5
    label "uhonorowanie"
  ]
  node [
    id 6
    label "zaimponowanie"
  ]
  node [
    id 7
    label "honorowanie"
  ]
  node [
    id 8
    label "uszanowa&#263;"
  ]
  node [
    id 9
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 10
    label "uszanowanie"
  ]
  node [
    id 11
    label "szacuneczek"
  ]
  node [
    id 12
    label "rewerencja"
  ]
  node [
    id 13
    label "uhonorowa&#263;"
  ]
  node [
    id 14
    label "dobro"
  ]
  node [
    id 15
    label "szanowa&#263;"
  ]
  node [
    id 16
    label "respect"
  ]
  node [
    id 17
    label "postawa"
  ]
  node [
    id 18
    label "imponowanie"
  ]
  node [
    id 19
    label "ekstraspekcja"
  ]
  node [
    id 20
    label "feeling"
  ]
  node [
    id 21
    label "wiedza"
  ]
  node [
    id 22
    label "zemdle&#263;"
  ]
  node [
    id 23
    label "psychika"
  ]
  node [
    id 24
    label "stan"
  ]
  node [
    id 25
    label "Freud"
  ]
  node [
    id 26
    label "psychoanaliza"
  ]
  node [
    id 27
    label "conscience"
  ]
  node [
    id 28
    label "warto&#347;&#263;"
  ]
  node [
    id 29
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 30
    label "dobro&#263;"
  ]
  node [
    id 31
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 32
    label "krzywa_Engla"
  ]
  node [
    id 33
    label "cel"
  ]
  node [
    id 34
    label "dobra"
  ]
  node [
    id 35
    label "go&#322;&#261;bek"
  ]
  node [
    id 36
    label "despond"
  ]
  node [
    id 37
    label "litera"
  ]
  node [
    id 38
    label "kalokagatia"
  ]
  node [
    id 39
    label "rzecz"
  ]
  node [
    id 40
    label "g&#322;agolica"
  ]
  node [
    id 41
    label "nastawienie"
  ]
  node [
    id 42
    label "pozycja"
  ]
  node [
    id 43
    label "attitude"
  ]
  node [
    id 44
    label "powa&#380;anie"
  ]
  node [
    id 45
    label "wypowied&#378;"
  ]
  node [
    id 46
    label "uznawanie"
  ]
  node [
    id 47
    label "p&#322;acenie"
  ]
  node [
    id 48
    label "honor"
  ]
  node [
    id 49
    label "okazywanie"
  ]
  node [
    id 50
    label "czci&#263;"
  ]
  node [
    id 51
    label "acknowledge"
  ]
  node [
    id 52
    label "wyp&#322;aca&#263;"
  ]
  node [
    id 53
    label "notice"
  ]
  node [
    id 54
    label "fit"
  ]
  node [
    id 55
    label "uznawa&#263;"
  ]
  node [
    id 56
    label "treasure"
  ]
  node [
    id 57
    label "czu&#263;"
  ]
  node [
    id 58
    label "respektowa&#263;"
  ]
  node [
    id 59
    label "wyra&#380;a&#263;"
  ]
  node [
    id 60
    label "chowa&#263;"
  ]
  node [
    id 61
    label "wyrazi&#263;"
  ]
  node [
    id 62
    label "spare_part"
  ]
  node [
    id 63
    label "nagrodzi&#263;"
  ]
  node [
    id 64
    label "uczci&#263;"
  ]
  node [
    id 65
    label "wyp&#322;aci&#263;"
  ]
  node [
    id 66
    label "wzbudzanie"
  ]
  node [
    id 67
    label "szanowanie"
  ]
  node [
    id 68
    label "wzbudzenie"
  ]
  node [
    id 69
    label "nagrodzenie"
  ]
  node [
    id 70
    label "zap&#322;acenie"
  ]
  node [
    id 71
    label "wyra&#380;enie"
  ]
  node [
    id 72
    label "greet"
  ]
  node [
    id 73
    label "welcome"
  ]
  node [
    id 74
    label "pozdrawia&#263;"
  ]
  node [
    id 75
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 76
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 77
    label "robi&#263;"
  ]
  node [
    id 78
    label "obchodzi&#263;"
  ]
  node [
    id 79
    label "bless"
  ]
  node [
    id 80
    label "&#347;liwa_domowa"
  ]
  node [
    id 81
    label "&#347;liwka"
  ]
  node [
    id 82
    label "&#347;liwa"
  ]
  node [
    id 83
    label "pestkowiec"
  ]
  node [
    id 84
    label "fiolet"
  ]
  node [
    id 85
    label "owoc"
  ]
  node [
    id 86
    label "wykop"
  ]
  node [
    id 87
    label "efekt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 86
    target 87
  ]
]
