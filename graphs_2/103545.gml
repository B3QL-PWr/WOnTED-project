graph [
  node [
    id 0
    label "kurtyna"
    origin "text"
  ]
  node [
    id 1
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 2
    label "scena"
  ]
  node [
    id 3
    label "zas&#322;ona"
  ]
  node [
    id 4
    label "bariera"
  ]
  node [
    id 5
    label "mur"
  ]
  node [
    id 6
    label "fortyfikacja"
  ]
  node [
    id 7
    label "kortyna"
  ]
  node [
    id 8
    label "teatr"
  ]
  node [
    id 9
    label "bastion"
  ]
  node [
    id 10
    label "parapet"
  ]
  node [
    id 11
    label "trudno&#347;&#263;"
  ]
  node [
    id 12
    label "ochrona"
  ]
  node [
    id 13
    label "przeszkoda"
  ]
  node [
    id 14
    label "pasmo"
  ]
  node [
    id 15
    label "obstruction"
  ]
  node [
    id 16
    label "tynk"
  ]
  node [
    id 17
    label "futr&#243;wka"
  ]
  node [
    id 18
    label "&#347;ciana"
  ]
  node [
    id 19
    label "fola"
  ]
  node [
    id 20
    label "belkowanie"
  ]
  node [
    id 21
    label "gzyms"
  ]
  node [
    id 22
    label "konstrukcja"
  ]
  node [
    id 23
    label "budowla"
  ]
  node [
    id 24
    label "ceg&#322;a"
  ]
  node [
    id 25
    label "rz&#261;d"
  ]
  node [
    id 26
    label "futbolista"
  ]
  node [
    id 27
    label "przegroda"
  ]
  node [
    id 28
    label "os&#322;ona"
  ]
  node [
    id 29
    label "obronienie"
  ]
  node [
    id 30
    label "przy&#322;bica"
  ]
  node [
    id 31
    label "dekoracja_okna"
  ]
  node [
    id 32
    label "twarz"
  ]
  node [
    id 33
    label "okop"
  ]
  node [
    id 34
    label "zamek"
  ]
  node [
    id 35
    label "palisada"
  ]
  node [
    id 36
    label "przedbramie"
  ]
  node [
    id 37
    label "baszta"
  ]
  node [
    id 38
    label "&#347;r&#243;dszaniec"
  ]
  node [
    id 39
    label "szaniec"
  ]
  node [
    id 40
    label "fosa"
  ]
  node [
    id 41
    label "kazamata"
  ]
  node [
    id 42
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 43
    label "fort"
  ]
  node [
    id 44
    label "barykada"
  ]
  node [
    id 45
    label "in&#380;ynieria"
  ]
  node [
    id 46
    label "transzeja"
  ]
  node [
    id 47
    label "machiku&#322;"
  ]
  node [
    id 48
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 49
    label "antyteatr"
  ]
  node [
    id 50
    label "przedstawienie"
  ]
  node [
    id 51
    label "literatura"
  ]
  node [
    id 52
    label "sala"
  ]
  node [
    id 53
    label "gra"
  ]
  node [
    id 54
    label "budynek"
  ]
  node [
    id 55
    label "deski"
  ]
  node [
    id 56
    label "teren"
  ]
  node [
    id 57
    label "sztuka"
  ]
  node [
    id 58
    label "przedstawia&#263;"
  ]
  node [
    id 59
    label "play"
  ]
  node [
    id 60
    label "widzownia"
  ]
  node [
    id 61
    label "modelatornia"
  ]
  node [
    id 62
    label "dekoratornia"
  ]
  node [
    id 63
    label "instytucja"
  ]
  node [
    id 64
    label "przedstawianie"
  ]
  node [
    id 65
    label "sznurownia"
  ]
  node [
    id 66
    label "nadscenie"
  ]
  node [
    id 67
    label "fragment"
  ]
  node [
    id 68
    label "film"
  ]
  node [
    id 69
    label "k&#322;&#243;tnia"
  ]
  node [
    id 70
    label "horyzont"
  ]
  node [
    id 71
    label "epizod"
  ]
  node [
    id 72
    label "sphere"
  ]
  node [
    id 73
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 74
    label "kiesze&#324;"
  ]
  node [
    id 75
    label "podest"
  ]
  node [
    id 76
    label "wydarzenie"
  ]
  node [
    id 77
    label "budka_suflera"
  ]
  node [
    id 78
    label "akt"
  ]
  node [
    id 79
    label "proscenium"
  ]
  node [
    id 80
    label "podwy&#380;szenie"
  ]
  node [
    id 81
    label "stadium"
  ]
  node [
    id 82
    label "dramaturgy"
  ]
  node [
    id 83
    label "twierdza"
  ]
  node [
    id 84
    label "narys_bastionowy"
  ]
  node [
    id 85
    label "ostoja"
  ]
  node [
    id 86
    label "dzie&#322;o_koronowe"
  ]
  node [
    id 87
    label "basteja"
  ]
  node [
    id 88
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 89
    label "d&#378;wi&#281;k"
  ]
  node [
    id 90
    label "przele&#378;&#263;"
  ]
  node [
    id 91
    label "Synaj"
  ]
  node [
    id 92
    label "Kreml"
  ]
  node [
    id 93
    label "kierunek"
  ]
  node [
    id 94
    label "Ropa"
  ]
  node [
    id 95
    label "przedmiot"
  ]
  node [
    id 96
    label "element"
  ]
  node [
    id 97
    label "rami&#261;czko"
  ]
  node [
    id 98
    label "&#347;piew"
  ]
  node [
    id 99
    label "wysoki"
  ]
  node [
    id 100
    label "Jaworze"
  ]
  node [
    id 101
    label "grupa"
  ]
  node [
    id 102
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 103
    label "kupa"
  ]
  node [
    id 104
    label "karczek"
  ]
  node [
    id 105
    label "wzniesienie"
  ]
  node [
    id 106
    label "pi&#281;tro"
  ]
  node [
    id 107
    label "przelezienie"
  ]
  node [
    id 108
    label "asymilowa&#263;"
  ]
  node [
    id 109
    label "kompozycja"
  ]
  node [
    id 110
    label "pakiet_klimatyczny"
  ]
  node [
    id 111
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 112
    label "type"
  ]
  node [
    id 113
    label "cz&#261;steczka"
  ]
  node [
    id 114
    label "gromada"
  ]
  node [
    id 115
    label "specgrupa"
  ]
  node [
    id 116
    label "egzemplarz"
  ]
  node [
    id 117
    label "stage_set"
  ]
  node [
    id 118
    label "asymilowanie"
  ]
  node [
    id 119
    label "zbi&#243;r"
  ]
  node [
    id 120
    label "odm&#322;odzenie"
  ]
  node [
    id 121
    label "odm&#322;adza&#263;"
  ]
  node [
    id 122
    label "harcerze_starsi"
  ]
  node [
    id 123
    label "jednostka_systematyczna"
  ]
  node [
    id 124
    label "oddzia&#322;"
  ]
  node [
    id 125
    label "category"
  ]
  node [
    id 126
    label "liga"
  ]
  node [
    id 127
    label "&#346;wietliki"
  ]
  node [
    id 128
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 129
    label "formacja_geologiczna"
  ]
  node [
    id 130
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 131
    label "Eurogrupa"
  ]
  node [
    id 132
    label "Terranie"
  ]
  node [
    id 133
    label "odm&#322;adzanie"
  ]
  node [
    id 134
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 135
    label "Entuzjastki"
  ]
  node [
    id 136
    label "Rzym_Zachodni"
  ]
  node [
    id 137
    label "Rzym_Wschodni"
  ]
  node [
    id 138
    label "ilo&#347;&#263;"
  ]
  node [
    id 139
    label "whole"
  ]
  node [
    id 140
    label "urz&#261;dzenie"
  ]
  node [
    id 141
    label "Sikornik"
  ]
  node [
    id 142
    label "Bukowiec"
  ]
  node [
    id 143
    label "Bielec"
  ]
  node [
    id 144
    label "raise"
  ]
  node [
    id 145
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 146
    label "wierzchowina"
  ]
  node [
    id 147
    label "wyro&#347;ni&#281;cie"
  ]
  node [
    id 148
    label "kszta&#322;t"
  ]
  node [
    id 149
    label "Izera"
  ]
  node [
    id 150
    label "nabudowanie"
  ]
  node [
    id 151
    label "miejsce"
  ]
  node [
    id 152
    label "rise"
  ]
  node [
    id 153
    label "wypi&#281;trzy&#263;_si&#281;"
  ]
  node [
    id 154
    label "wypi&#281;trza&#263;_si&#281;"
  ]
  node [
    id 155
    label "zrobienie"
  ]
  node [
    id 156
    label "podniesienie"
  ]
  node [
    id 157
    label "Skalnik"
  ]
  node [
    id 158
    label "Zwalisko"
  ]
  node [
    id 159
    label "construction"
  ]
  node [
    id 160
    label "solmizacja"
  ]
  node [
    id 161
    label "wydanie"
  ]
  node [
    id 162
    label "transmiter"
  ]
  node [
    id 163
    label "repetycja"
  ]
  node [
    id 164
    label "wpa&#347;&#263;"
  ]
  node [
    id 165
    label "akcent"
  ]
  node [
    id 166
    label "nadlecenie"
  ]
  node [
    id 167
    label "note"
  ]
  node [
    id 168
    label "heksachord"
  ]
  node [
    id 169
    label "wpadanie"
  ]
  node [
    id 170
    label "phone"
  ]
  node [
    id 171
    label "wydawa&#263;"
  ]
  node [
    id 172
    label "seria"
  ]
  node [
    id 173
    label "onomatopeja"
  ]
  node [
    id 174
    label "brzmienie"
  ]
  node [
    id 175
    label "wpada&#263;"
  ]
  node [
    id 176
    label "zjawisko"
  ]
  node [
    id 177
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 178
    label "dobiec"
  ]
  node [
    id 179
    label "intonacja"
  ]
  node [
    id 180
    label "wpadni&#281;cie"
  ]
  node [
    id 181
    label "modalizm"
  ]
  node [
    id 182
    label "wyda&#263;"
  ]
  node [
    id 183
    label "sound"
  ]
  node [
    id 184
    label "discipline"
  ]
  node [
    id 185
    label "zboczy&#263;"
  ]
  node [
    id 186
    label "w&#261;tek"
  ]
  node [
    id 187
    label "kultura"
  ]
  node [
    id 188
    label "entity"
  ]
  node [
    id 189
    label "sponiewiera&#263;"
  ]
  node [
    id 190
    label "zboczenie"
  ]
  node [
    id 191
    label "zbaczanie"
  ]
  node [
    id 192
    label "charakter"
  ]
  node [
    id 193
    label "thing"
  ]
  node [
    id 194
    label "om&#243;wi&#263;"
  ]
  node [
    id 195
    label "tre&#347;&#263;"
  ]
  node [
    id 196
    label "kr&#261;&#380;enie"
  ]
  node [
    id 197
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 198
    label "istota"
  ]
  node [
    id 199
    label "zbacza&#263;"
  ]
  node [
    id 200
    label "om&#243;wienie"
  ]
  node [
    id 201
    label "rzecz"
  ]
  node [
    id 202
    label "tematyka"
  ]
  node [
    id 203
    label "omawianie"
  ]
  node [
    id 204
    label "omawia&#263;"
  ]
  node [
    id 205
    label "robienie"
  ]
  node [
    id 206
    label "program_nauczania"
  ]
  node [
    id 207
    label "sponiewieranie"
  ]
  node [
    id 208
    label "poj&#281;cie"
  ]
  node [
    id 209
    label "materia"
  ]
  node [
    id 210
    label "&#347;rodowisko"
  ]
  node [
    id 211
    label "szkodnik"
  ]
  node [
    id 212
    label "gangsterski"
  ]
  node [
    id 213
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 214
    label "underworld"
  ]
  node [
    id 215
    label "szambo"
  ]
  node [
    id 216
    label "component"
  ]
  node [
    id 217
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 218
    label "r&#243;&#380;niczka"
  ]
  node [
    id 219
    label "aspo&#322;eczny"
  ]
  node [
    id 220
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 221
    label "eta&#380;"
  ]
  node [
    id 222
    label "floor"
  ]
  node [
    id 223
    label "kondygnacja"
  ]
  node [
    id 224
    label "p&#322;aszczyzna"
  ]
  node [
    id 225
    label "chronozona"
  ]
  node [
    id 226
    label "jednostka_geologiczna"
  ]
  node [
    id 227
    label "wydalina"
  ]
  node [
    id 228
    label "koprofilia"
  ]
  node [
    id 229
    label "stool"
  ]
  node [
    id 230
    label "balas"
  ]
  node [
    id 231
    label "knoll"
  ]
  node [
    id 232
    label "mn&#243;stwo"
  ]
  node [
    id 233
    label "odchody"
  ]
  node [
    id 234
    label "fekalia"
  ]
  node [
    id 235
    label "tragedia"
  ]
  node [
    id 236
    label "g&#243;wno"
  ]
  node [
    id 237
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 238
    label "Moj&#380;esz"
  ]
  node [
    id 239
    label "Egipt"
  ]
  node [
    id 240
    label "Beskid_Niski"
  ]
  node [
    id 241
    label "Tatry"
  ]
  node [
    id 242
    label "Ma&#322;opolska"
  ]
  node [
    id 243
    label "linia"
  ]
  node [
    id 244
    label "przebieg"
  ]
  node [
    id 245
    label "zorientowa&#263;"
  ]
  node [
    id 246
    label "orientowa&#263;"
  ]
  node [
    id 247
    label "praktyka"
  ]
  node [
    id 248
    label "skr&#281;cenie"
  ]
  node [
    id 249
    label "skr&#281;ci&#263;"
  ]
  node [
    id 250
    label "przeorientowanie"
  ]
  node [
    id 251
    label "orientowanie"
  ]
  node [
    id 252
    label "zorientowanie"
  ]
  node [
    id 253
    label "ty&#322;"
  ]
  node [
    id 254
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 255
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 256
    label "przeorientowywanie"
  ]
  node [
    id 257
    label "bok"
  ]
  node [
    id 258
    label "ideologia"
  ]
  node [
    id 259
    label "skr&#281;canie"
  ]
  node [
    id 260
    label "orientacja"
  ]
  node [
    id 261
    label "metoda"
  ]
  node [
    id 262
    label "studia"
  ]
  node [
    id 263
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 264
    label "przeorientowa&#263;"
  ]
  node [
    id 265
    label "bearing"
  ]
  node [
    id 266
    label "spos&#243;b"
  ]
  node [
    id 267
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 268
    label "prz&#243;d"
  ]
  node [
    id 269
    label "skr&#281;ca&#263;"
  ]
  node [
    id 270
    label "system"
  ]
  node [
    id 271
    label "przeorientowywa&#263;"
  ]
  node [
    id 272
    label "ascent"
  ]
  node [
    id 273
    label "pique"
  ]
  node [
    id 274
    label "przekroczy&#263;"
  ]
  node [
    id 275
    label "beat"
  ]
  node [
    id 276
    label "min&#261;&#263;"
  ]
  node [
    id 277
    label "przeby&#263;"
  ]
  node [
    id 278
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 279
    label "przebycie"
  ]
  node [
    id 280
    label "offense"
  ]
  node [
    id 281
    label "mini&#281;cie"
  ]
  node [
    id 282
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 283
    label "przekroczenie"
  ]
  node [
    id 284
    label "prze&#322;a&#380;enie"
  ]
  node [
    id 285
    label "przepuszczenie"
  ]
  node [
    id 286
    label "traversal"
  ]
  node [
    id 287
    label "muzyka"
  ]
  node [
    id 288
    label "wokal"
  ]
  node [
    id 289
    label "odg&#322;os"
  ]
  node [
    id 290
    label "d&#243;&#322;"
  ]
  node [
    id 291
    label "impostacja"
  ]
  node [
    id 292
    label "breeze"
  ]
  node [
    id 293
    label "pienie"
  ]
  node [
    id 294
    label "g&#322;os"
  ]
  node [
    id 295
    label "czynno&#347;&#263;"
  ]
  node [
    id 296
    label "znaczny"
  ]
  node [
    id 297
    label "niepo&#347;ledni"
  ]
  node [
    id 298
    label "szczytnie"
  ]
  node [
    id 299
    label "du&#380;y"
  ]
  node [
    id 300
    label "wysoko"
  ]
  node [
    id 301
    label "warto&#347;ciowy"
  ]
  node [
    id 302
    label "wysoce"
  ]
  node [
    id 303
    label "uprzywilejowany"
  ]
  node [
    id 304
    label "wznios&#322;y"
  ]
  node [
    id 305
    label "chwalebny"
  ]
  node [
    id 306
    label "z_wysoka"
  ]
  node [
    id 307
    label "daleki"
  ]
  node [
    id 308
    label "wyrafinowany"
  ]
  node [
    id 309
    label "tusza"
  ]
  node [
    id 310
    label "mi&#281;so"
  ]
  node [
    id 311
    label "pasek"
  ]
  node [
    id 312
    label "strap"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
]
