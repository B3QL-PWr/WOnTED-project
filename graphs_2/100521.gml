graph [
  node [
    id 0
    label "gwiezdny"
    origin "text"
  ]
  node [
    id 1
    label "ku&#378;nia"
    origin "text"
  ]
  node [
    id 2
    label "jasny"
  ]
  node [
    id 3
    label "pogodny"
  ]
  node [
    id 4
    label "zrozumia&#322;y"
  ]
  node [
    id 5
    label "niezm&#261;cony"
  ]
  node [
    id 6
    label "jednoznaczny"
  ]
  node [
    id 7
    label "o&#347;wietlenie"
  ]
  node [
    id 8
    label "bia&#322;y"
  ]
  node [
    id 9
    label "dobry"
  ]
  node [
    id 10
    label "klarowny"
  ]
  node [
    id 11
    label "szczery"
  ]
  node [
    id 12
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 13
    label "przytomny"
  ]
  node [
    id 14
    label "jasno"
  ]
  node [
    id 15
    label "o&#347;wietlanie"
  ]
  node [
    id 16
    label "instytucja"
  ]
  node [
    id 17
    label "fabryka"
  ]
  node [
    id 18
    label "warsztat"
  ]
  node [
    id 19
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 20
    label "farbiarnia"
  ]
  node [
    id 21
    label "gospodarka"
  ]
  node [
    id 22
    label "szlifiernia"
  ]
  node [
    id 23
    label "probiernia"
  ]
  node [
    id 24
    label "magazyn"
  ]
  node [
    id 25
    label "wytrawialnia"
  ]
  node [
    id 26
    label "szwalnia"
  ]
  node [
    id 27
    label "rurownia"
  ]
  node [
    id 28
    label "prz&#281;dzalnia"
  ]
  node [
    id 29
    label "celulozownia"
  ]
  node [
    id 30
    label "hala"
  ]
  node [
    id 31
    label "fryzernia"
  ]
  node [
    id 32
    label "dziewiarnia"
  ]
  node [
    id 33
    label "ucieralnia"
  ]
  node [
    id 34
    label "tkalnia"
  ]
  node [
    id 35
    label "wyposa&#380;enie"
  ]
  node [
    id 36
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 37
    label "spotkanie"
  ]
  node [
    id 38
    label "miejsce"
  ]
  node [
    id 39
    label "pracownia"
  ]
  node [
    id 40
    label "sprawno&#347;&#263;"
  ]
  node [
    id 41
    label "poj&#281;cie"
  ]
  node [
    id 42
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 43
    label "afiliowa&#263;"
  ]
  node [
    id 44
    label "establishment"
  ]
  node [
    id 45
    label "zamyka&#263;"
  ]
  node [
    id 46
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 47
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 48
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 49
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 50
    label "standard"
  ]
  node [
    id 51
    label "Fundusze_Unijne"
  ]
  node [
    id 52
    label "biuro"
  ]
  node [
    id 53
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 54
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 55
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 56
    label "zamykanie"
  ]
  node [
    id 57
    label "organizacja"
  ]
  node [
    id 58
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 59
    label "osoba_prawna"
  ]
  node [
    id 60
    label "urz&#261;d"
  ]
  node [
    id 61
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 62
    label "wojna"
  ]
  node [
    id 63
    label "bezkresny"
  ]
  node [
    id 64
    label "imperium"
  ]
  node [
    id 65
    label "mandaloria&#324;skich"
  ]
  node [
    id 66
    label "republika"
  ]
  node [
    id 67
    label "galaktyczny"
  ]
  node [
    id 68
    label "domowy"
  ]
  node [
    id 69
    label "Jedi"
  ]
  node [
    id 70
    label "star"
  ]
  node [
    id 71
    label "Wars"
  ]
  node [
    id 72
    label "Knights"
  ]
  node [
    id 73
    label "of"
  ]
  node [
    id 74
    label "the"
  ]
  node [
    id 75
    label "Old"
  ]
  node [
    id 76
    label "Republic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 68
  ]
  edge [
    source 62
    target 69
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 70
    target 73
  ]
  edge [
    source 70
    target 74
  ]
  edge [
    source 70
    target 75
  ]
  edge [
    source 70
    target 76
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 71
    target 74
  ]
  edge [
    source 71
    target 75
  ]
  edge [
    source 71
    target 76
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 72
    target 75
  ]
  edge [
    source 72
    target 76
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 75
  ]
  edge [
    source 73
    target 76
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 76
  ]
  edge [
    source 75
    target 76
  ]
]
