graph [
  node [
    id 0
    label "ewa"
    origin "text"
  ]
  node [
    id 1
    label "&#322;&#281;towska"
    origin "text"
  ]
  node [
    id 2
    label "przewodnicz&#261;cy"
    origin "text"
  ]
  node [
    id 3
    label "zwierzchnik"
  ]
  node [
    id 4
    label "prowadz&#261;cy"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "pryncypa&#322;"
  ]
  node [
    id 7
    label "kierowa&#263;"
  ]
  node [
    id 8
    label "kierownictwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
]
