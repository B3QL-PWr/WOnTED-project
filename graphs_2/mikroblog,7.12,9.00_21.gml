graph [
  node [
    id 0
    label "ogarn&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wiek"
    origin "text"
  ]
  node [
    id 2
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 3
    label "manipulate"
  ]
  node [
    id 4
    label "otoczy&#263;"
  ]
  node [
    id 5
    label "spotka&#263;"
  ]
  node [
    id 6
    label "spowodowa&#263;"
  ]
  node [
    id 7
    label "visit"
  ]
  node [
    id 8
    label "involve"
  ]
  node [
    id 9
    label "environment"
  ]
  node [
    id 10
    label "dotkn&#261;&#263;"
  ]
  node [
    id 11
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 12
    label "act"
  ]
  node [
    id 13
    label "insert"
  ]
  node [
    id 14
    label "visualize"
  ]
  node [
    id 15
    label "pozna&#263;"
  ]
  node [
    id 16
    label "befall"
  ]
  node [
    id 17
    label "go_steady"
  ]
  node [
    id 18
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 19
    label "znale&#378;&#263;"
  ]
  node [
    id 20
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 21
    label "zrobi&#263;"
  ]
  node [
    id 22
    label "dotkn&#261;&#263;_si&#281;"
  ]
  node [
    id 23
    label "allude"
  ]
  node [
    id 24
    label "range"
  ]
  node [
    id 25
    label "diss"
  ]
  node [
    id 26
    label "pique"
  ]
  node [
    id 27
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 28
    label "wzbudzi&#263;"
  ]
  node [
    id 29
    label "obdarowa&#263;"
  ]
  node [
    id 30
    label "sta&#263;_si&#281;"
  ]
  node [
    id 31
    label "span"
  ]
  node [
    id 32
    label "admit"
  ]
  node [
    id 33
    label "roztoczy&#263;"
  ]
  node [
    id 34
    label "zacz&#261;&#263;"
  ]
  node [
    id 35
    label "performance"
  ]
  node [
    id 36
    label "sztuka"
  ]
  node [
    id 37
    label "period"
  ]
  node [
    id 38
    label "choroba_wieku"
  ]
  node [
    id 39
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 40
    label "chron"
  ]
  node [
    id 41
    label "czas"
  ]
  node [
    id 42
    label "cecha"
  ]
  node [
    id 43
    label "rok"
  ]
  node [
    id 44
    label "long_time"
  ]
  node [
    id 45
    label "jednostka_geologiczna"
  ]
  node [
    id 46
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 47
    label "poprzedzanie"
  ]
  node [
    id 48
    label "czasoprzestrze&#324;"
  ]
  node [
    id 49
    label "laba"
  ]
  node [
    id 50
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 51
    label "chronometria"
  ]
  node [
    id 52
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 53
    label "rachuba_czasu"
  ]
  node [
    id 54
    label "przep&#322;ywanie"
  ]
  node [
    id 55
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 56
    label "czasokres"
  ]
  node [
    id 57
    label "odczyt"
  ]
  node [
    id 58
    label "chwila"
  ]
  node [
    id 59
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 60
    label "dzieje"
  ]
  node [
    id 61
    label "kategoria_gramatyczna"
  ]
  node [
    id 62
    label "poprzedzenie"
  ]
  node [
    id 63
    label "trawienie"
  ]
  node [
    id 64
    label "pochodzi&#263;"
  ]
  node [
    id 65
    label "okres_czasu"
  ]
  node [
    id 66
    label "poprzedza&#263;"
  ]
  node [
    id 67
    label "schy&#322;ek"
  ]
  node [
    id 68
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 69
    label "odwlekanie_si&#281;"
  ]
  node [
    id 70
    label "zegar"
  ]
  node [
    id 71
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 72
    label "czwarty_wymiar"
  ]
  node [
    id 73
    label "pochodzenie"
  ]
  node [
    id 74
    label "koniugacja"
  ]
  node [
    id 75
    label "Zeitgeist"
  ]
  node [
    id 76
    label "trawi&#263;"
  ]
  node [
    id 77
    label "pogoda"
  ]
  node [
    id 78
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 79
    label "poprzedzi&#263;"
  ]
  node [
    id 80
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 81
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 82
    label "time_period"
  ]
  node [
    id 83
    label "charakterystyka"
  ]
  node [
    id 84
    label "m&#322;ot"
  ]
  node [
    id 85
    label "znak"
  ]
  node [
    id 86
    label "drzewo"
  ]
  node [
    id 87
    label "pr&#243;ba"
  ]
  node [
    id 88
    label "attribute"
  ]
  node [
    id 89
    label "marka"
  ]
  node [
    id 90
    label "p&#243;&#322;rocze"
  ]
  node [
    id 91
    label "martwy_sezon"
  ]
  node [
    id 92
    label "kalendarz"
  ]
  node [
    id 93
    label "cykl_astronomiczny"
  ]
  node [
    id 94
    label "lata"
  ]
  node [
    id 95
    label "pora_roku"
  ]
  node [
    id 96
    label "stulecie"
  ]
  node [
    id 97
    label "kurs"
  ]
  node [
    id 98
    label "jubileusz"
  ]
  node [
    id 99
    label "grupa"
  ]
  node [
    id 100
    label "kwarta&#322;"
  ]
  node [
    id 101
    label "miesi&#261;c"
  ]
  node [
    id 102
    label "moment"
  ]
  node [
    id 103
    label "flow"
  ]
  node [
    id 104
    label "choroba_przyrodzona"
  ]
  node [
    id 105
    label "ciota"
  ]
  node [
    id 106
    label "proces_fizjologiczny"
  ]
  node [
    id 107
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 108
    label "notice"
  ]
  node [
    id 109
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 110
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 111
    label "styka&#263;_si&#281;"
  ]
  node [
    id 112
    label "zaczyna&#263;"
  ]
  node [
    id 113
    label "nastawia&#263;"
  ]
  node [
    id 114
    label "get_in_touch"
  ]
  node [
    id 115
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 116
    label "dokoptowywa&#263;"
  ]
  node [
    id 117
    label "uruchamia&#263;"
  ]
  node [
    id 118
    label "umieszcza&#263;"
  ]
  node [
    id 119
    label "connect"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
]
