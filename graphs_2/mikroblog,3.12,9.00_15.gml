graph [
  node [
    id 0
    label "zaplusuj"
    origin "text"
  ]
  node [
    id 1
    label "pyszny"
    origin "text"
  ]
  node [
    id 2
    label "drwal"
    origin "text"
  ]
  node [
    id 3
    label "obudzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "jutro"
    origin "text"
  ]
  node [
    id 5
    label "nasyci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pelen"
    origin "text"
  ]
  node [
    id 7
    label "sil"
    origin "text"
  ]
  node [
    id 8
    label "wynios&#322;y"
  ]
  node [
    id 9
    label "dufny"
  ]
  node [
    id 10
    label "wspania&#322;y"
  ]
  node [
    id 11
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 12
    label "napuszanie_si&#281;"
  ]
  node [
    id 13
    label "pysznie"
  ]
  node [
    id 14
    label "podufa&#322;y"
  ]
  node [
    id 15
    label "rozdymanie_si&#281;"
  ]
  node [
    id 16
    label "udany"
  ]
  node [
    id 17
    label "przesmaczny"
  ]
  node [
    id 18
    label "napuszenie_si&#281;"
  ]
  node [
    id 19
    label "przedni"
  ]
  node [
    id 20
    label "niedost&#281;pny"
  ]
  node [
    id 21
    label "pot&#281;&#380;ny"
  ]
  node [
    id 22
    label "wysoki"
  ]
  node [
    id 23
    label "wynio&#347;le"
  ]
  node [
    id 24
    label "dumny"
  ]
  node [
    id 25
    label "przebrany"
  ]
  node [
    id 26
    label "przednio"
  ]
  node [
    id 27
    label "udanie"
  ]
  node [
    id 28
    label "przyjemny"
  ]
  node [
    id 29
    label "fajny"
  ]
  node [
    id 30
    label "dobry"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 32
    label "wspaniale"
  ]
  node [
    id 33
    label "pomy&#347;lny"
  ]
  node [
    id 34
    label "pozytywny"
  ]
  node [
    id 35
    label "&#347;wietnie"
  ]
  node [
    id 36
    label "spania&#322;y"
  ]
  node [
    id 37
    label "och&#281;do&#380;ny"
  ]
  node [
    id 38
    label "warto&#347;ciowy"
  ]
  node [
    id 39
    label "zajebisty"
  ]
  node [
    id 40
    label "bogato"
  ]
  node [
    id 41
    label "dufnie"
  ]
  node [
    id 42
    label "przysadzisty"
  ]
  node [
    id 43
    label "poufa&#322;y"
  ]
  node [
    id 44
    label "zaufany"
  ]
  node [
    id 45
    label "pyszno"
  ]
  node [
    id 46
    label "smacznie"
  ]
  node [
    id 47
    label "drzewiarz"
  ]
  node [
    id 48
    label "r&#261;ba&#263;"
  ]
  node [
    id 49
    label "kantak"
  ]
  node [
    id 50
    label "robotnik"
  ]
  node [
    id 51
    label "robol"
  ]
  node [
    id 52
    label "cz&#322;owiek"
  ]
  node [
    id 53
    label "przedstawiciel"
  ]
  node [
    id 54
    label "dni&#243;wkarz"
  ]
  node [
    id 55
    label "proletariusz"
  ]
  node [
    id 56
    label "pracownik_fizyczny"
  ]
  node [
    id 57
    label "pracownik"
  ]
  node [
    id 58
    label "tracz"
  ]
  node [
    id 59
    label "bran&#380;owiec"
  ]
  node [
    id 60
    label "korowacz"
  ]
  node [
    id 61
    label "stolarz"
  ]
  node [
    id 62
    label "wycina&#263;"
  ]
  node [
    id 63
    label "robi&#263;"
  ]
  node [
    id 64
    label "odpala&#263;"
  ]
  node [
    id 65
    label "je&#347;&#263;"
  ]
  node [
    id 66
    label "plu&#263;"
  ]
  node [
    id 67
    label "fall"
  ]
  node [
    id 68
    label "m&#243;wi&#263;"
  ]
  node [
    id 69
    label "walczy&#263;"
  ]
  node [
    id 70
    label "napierdziela&#263;"
  ]
  node [
    id 71
    label "wyr&#281;bywa&#263;"
  ]
  node [
    id 72
    label "fight"
  ]
  node [
    id 73
    label "kra&#347;&#263;"
  ]
  node [
    id 74
    label "write_out"
  ]
  node [
    id 75
    label "uderza&#263;"
  ]
  node [
    id 76
    label "unwrap"
  ]
  node [
    id 77
    label "trzaskowisko"
  ]
  node [
    id 78
    label "gra&#263;"
  ]
  node [
    id 79
    label "ci&#261;&#263;"
  ]
  node [
    id 80
    label "hak"
  ]
  node [
    id 81
    label "narz&#281;dzie"
  ]
  node [
    id 82
    label "ptak_drapie&#380;ny"
  ]
  node [
    id 83
    label "pazur"
  ]
  node [
    id 84
    label "dr&#261;g"
  ]
  node [
    id 85
    label "prompt"
  ]
  node [
    id 86
    label "wyrwa&#263;"
  ]
  node [
    id 87
    label "wywo&#322;a&#263;"
  ]
  node [
    id 88
    label "arouse"
  ]
  node [
    id 89
    label "poleci&#263;"
  ]
  node [
    id 90
    label "train"
  ]
  node [
    id 91
    label "wezwa&#263;"
  ]
  node [
    id 92
    label "trip"
  ]
  node [
    id 93
    label "spowodowa&#263;"
  ]
  node [
    id 94
    label "oznajmi&#263;"
  ]
  node [
    id 95
    label "revolutionize"
  ]
  node [
    id 96
    label "przetworzy&#263;"
  ]
  node [
    id 97
    label "wydali&#263;"
  ]
  node [
    id 98
    label "pull"
  ]
  node [
    id 99
    label "poderwa&#263;"
  ]
  node [
    id 100
    label "zabra&#263;"
  ]
  node [
    id 101
    label "ruszy&#263;"
  ]
  node [
    id 102
    label "extract"
  ]
  node [
    id 103
    label "podnie&#347;&#263;"
  ]
  node [
    id 104
    label "wydosta&#263;"
  ]
  node [
    id 105
    label "fascinate"
  ]
  node [
    id 106
    label "uwolni&#263;"
  ]
  node [
    id 107
    label "blisko"
  ]
  node [
    id 108
    label "dzie&#324;_jutrzejszy"
  ]
  node [
    id 109
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 110
    label "dzie&#324;"
  ]
  node [
    id 111
    label "jutrzejszy"
  ]
  node [
    id 112
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 113
    label "bliski"
  ]
  node [
    id 114
    label "dok&#322;adnie"
  ]
  node [
    id 115
    label "silnie"
  ]
  node [
    id 116
    label "ranek"
  ]
  node [
    id 117
    label "doba"
  ]
  node [
    id 118
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 119
    label "noc"
  ]
  node [
    id 120
    label "podwiecz&#243;r"
  ]
  node [
    id 121
    label "po&#322;udnie"
  ]
  node [
    id 122
    label "godzina"
  ]
  node [
    id 123
    label "przedpo&#322;udnie"
  ]
  node [
    id 124
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 125
    label "long_time"
  ]
  node [
    id 126
    label "wiecz&#243;r"
  ]
  node [
    id 127
    label "t&#322;usty_czwartek"
  ]
  node [
    id 128
    label "popo&#322;udnie"
  ]
  node [
    id 129
    label "walentynki"
  ]
  node [
    id 130
    label "czynienie_si&#281;"
  ]
  node [
    id 131
    label "s&#322;o&#324;ce"
  ]
  node [
    id 132
    label "rano"
  ]
  node [
    id 133
    label "tydzie&#324;"
  ]
  node [
    id 134
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 135
    label "wzej&#347;cie"
  ]
  node [
    id 136
    label "czas"
  ]
  node [
    id 137
    label "wsta&#263;"
  ]
  node [
    id 138
    label "day"
  ]
  node [
    id 139
    label "termin"
  ]
  node [
    id 140
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 141
    label "wstanie"
  ]
  node [
    id 142
    label "przedwiecz&#243;r"
  ]
  node [
    id 143
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 144
    label "Sylwester"
  ]
  node [
    id 145
    label "przysz&#322;y"
  ]
  node [
    id 146
    label "odmienny"
  ]
  node [
    id 147
    label "odpowiedni"
  ]
  node [
    id 148
    label "cel"
  ]
  node [
    id 149
    label "wype&#322;ni&#263;"
  ]
  node [
    id 150
    label "soak"
  ]
  node [
    id 151
    label "dostarczy&#263;"
  ]
  node [
    id 152
    label "nakarmi&#263;"
  ]
  node [
    id 153
    label "wzmocni&#263;"
  ]
  node [
    id 154
    label "overcharge"
  ]
  node [
    id 155
    label "wprowadzi&#263;"
  ]
  node [
    id 156
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 157
    label "przenikn&#261;&#263;"
  ]
  node [
    id 158
    label "zaspokoi&#263;"
  ]
  node [
    id 159
    label "wyr&#281;czy&#263;"
  ]
  node [
    id 160
    label "da&#263;"
  ]
  node [
    id 161
    label "feed"
  ]
  node [
    id 162
    label "poda&#263;"
  ]
  node [
    id 163
    label "utrzyma&#263;"
  ]
  node [
    id 164
    label "zapoda&#263;"
  ]
  node [
    id 165
    label "wm&#243;wi&#263;"
  ]
  node [
    id 166
    label "satisfy"
  ]
  node [
    id 167
    label "p&#243;j&#347;&#263;_do_&#322;&#243;&#380;ka"
  ]
  node [
    id 168
    label "napoi&#263;_si&#281;"
  ]
  node [
    id 169
    label "zadowoli&#263;"
  ]
  node [
    id 170
    label "serve"
  ]
  node [
    id 171
    label "rynek"
  ]
  node [
    id 172
    label "doprowadzi&#263;"
  ]
  node [
    id 173
    label "testify"
  ]
  node [
    id 174
    label "insert"
  ]
  node [
    id 175
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 176
    label "wpisa&#263;"
  ]
  node [
    id 177
    label "picture"
  ]
  node [
    id 178
    label "zapozna&#263;"
  ]
  node [
    id 179
    label "zrobi&#263;"
  ]
  node [
    id 180
    label "wej&#347;&#263;"
  ]
  node [
    id 181
    label "zej&#347;&#263;"
  ]
  node [
    id 182
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 183
    label "umie&#347;ci&#263;"
  ]
  node [
    id 184
    label "zacz&#261;&#263;"
  ]
  node [
    id 185
    label "indicate"
  ]
  node [
    id 186
    label "wytworzy&#263;"
  ]
  node [
    id 187
    label "give"
  ]
  node [
    id 188
    label "mocny"
  ]
  node [
    id 189
    label "uskuteczni&#263;"
  ]
  node [
    id 190
    label "umocnienie"
  ]
  node [
    id 191
    label "wzm&#243;c"
  ]
  node [
    id 192
    label "utrwali&#263;"
  ]
  node [
    id 193
    label "fixate"
  ]
  node [
    id 194
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 195
    label "reinforce"
  ]
  node [
    id 196
    label "zmieni&#263;"
  ]
  node [
    id 197
    label "consolidate"
  ]
  node [
    id 198
    label "wyregulowa&#263;"
  ]
  node [
    id 199
    label "zabezpieczy&#263;"
  ]
  node [
    id 200
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 201
    label "follow_through"
  ]
  node [
    id 202
    label "manipulate"
  ]
  node [
    id 203
    label "perform"
  ]
  node [
    id 204
    label "play_along"
  ]
  node [
    id 205
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 206
    label "do"
  ]
  node [
    id 207
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 208
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 209
    label "erupt"
  ]
  node [
    id 210
    label "absorb"
  ]
  node [
    id 211
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 212
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 213
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 214
    label "infiltrate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 6
    target 7
  ]
]
