graph [
  node [
    id 0
    label "following"
    origin "text"
  ]
  node [
    id 1
    label "the"
    origin "text"
  ]
  node [
    id 2
    label "classic"
    origin "text"
  ]
  node [
    id 3
    label "teardown"
    origin "text"
  ]
  node [
    id 4
    label "it's"
    origin "text"
  ]
  node [
    id 5
    label "been"
    origin "text"
  ]
  node [
    id 6
    label "only"
    origin "text"
  ]
  node [
    id 7
    label "few"
    origin "text"
  ]
  node [
    id 8
    label "days"
    origin "text"
  ]
  node [
    id 9
    label "since"
    origin "text"
  ]
  node [
    id 10
    label "official"
    origin "text"
  ]
  node [
    id 11
    label "playstation"
    origin "text"
  ]
  node [
    id 12
    label "launch"
    origin "text"
  ]
  node [
    id 13
    label "anda"
    origin "text"
  ]
  node [
    id 14
    label "already"
    origin "text"
  ]
  node [
    id 15
    label "sony's"
    origin "text"
  ]
  node [
    id 16
    label "mini"
    origin "text"
  ]
  node [
    id 17
    label "console"
    origin "text"
  ]
  node [
    id 18
    label "has"
    origin "text"
  ]
  node [
    id 19
    label "hacked"
    origin "text"
  ]
  node [
    id 20
    label "with"
    origin "text"
  ]
  node [
    id 21
    label "hackers"
    origin "text"
  ]
  node [
    id 22
    label "demonstrating"
    origin "text"
  ]
  node [
    id 23
    label "game"
    origin "text"
  ]
  node [
    id 24
    label "swapping"
    origin "text"
  ]
  node [
    id 25
    label "videos"
    origin "text"
  ]
  node [
    id 26
    label "feat"
    origin "text"
  ]
  node [
    id 27
    label "minimalnie"
  ]
  node [
    id 28
    label "zminimalizowanie"
  ]
  node [
    id 29
    label "graniczny"
  ]
  node [
    id 30
    label "minimalizowanie"
  ]
  node [
    id 31
    label "skrajny"
  ]
  node [
    id 32
    label "przyleg&#322;y"
  ]
  node [
    id 33
    label "granicznie"
  ]
  node [
    id 34
    label "wa&#380;ny"
  ]
  node [
    id 35
    label "ostateczny"
  ]
  node [
    id 36
    label "zmniejszenie"
  ]
  node [
    id 37
    label "umniejszenie"
  ]
  node [
    id 38
    label "minimalny"
  ]
  node [
    id 39
    label "zmniejszanie"
  ]
  node [
    id 40
    label "umniejszanie"
  ]
  node [
    id 41
    label "&#380;elazowiec"
  ]
  node [
    id 42
    label "transuranowiec"
  ]
  node [
    id 43
    label "hassium"
  ]
  node [
    id 44
    label "metal"
  ]
  node [
    id 45
    label "pierwiastek"
  ]
  node [
    id 46
    label "uranowiec"
  ]
  node [
    id 47
    label "PS"
  ]
  node [
    id 48
    label "Classic"
  ]
  node [
    id 49
    label "Teardown"
  ]
  node [
    id 50
    label "Launch"
  ]
  node [
    id 51
    label "Anda"
  ]
  node [
    id 52
    label "Sonys"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 11
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 14
    target 50
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 14
    target 52
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 35
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 37
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 16
    target 40
  ]
  edge [
    source 16
    target 48
  ]
  edge [
    source 16
    target 50
  ]
  edge [
    source 16
    target 51
  ]
  edge [
    source 16
    target 52
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 18
    target 41
  ]
  edge [
    source 18
    target 42
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 46
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 47
  ]
  edge [
    source 21
    target 48
  ]
  edge [
    source 22
    target 47
  ]
  edge [
    source 22
    target 48
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 48
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 51
    target 52
  ]
]
