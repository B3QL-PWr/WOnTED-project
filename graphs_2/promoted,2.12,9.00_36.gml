graph [
  node [
    id 0
    label "&#347;miertelna"
    origin "text"
  ]
  node [
    id 1
    label "potr&#261;cenie"
    origin "text"
  ]
  node [
    id 2
    label "latko"
    origin "text"
  ]
  node [
    id 3
    label "droga"
    origin "text"
  ]
  node [
    id 4
    label "krajowy"
    origin "text"
  ]
  node [
    id 5
    label "warmi&#324;sko"
    origin "text"
  ]
  node [
    id 6
    label "mazurski"
    origin "text"
  ]
  node [
    id 7
    label "odliczenie"
  ]
  node [
    id 8
    label "kwota"
  ]
  node [
    id 9
    label "wytr&#261;cenie"
  ]
  node [
    id 10
    label "uderzenie"
  ]
  node [
    id 11
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 12
    label "movement"
  ]
  node [
    id 13
    label "jog"
  ]
  node [
    id 14
    label "jab"
  ]
  node [
    id 15
    label "subtraction"
  ]
  node [
    id 16
    label "odmierzenie"
  ]
  node [
    id 17
    label "odj&#281;cie"
  ]
  node [
    id 18
    label "deduction"
  ]
  node [
    id 19
    label "policzenie"
  ]
  node [
    id 20
    label "instrumentalizacja"
  ]
  node [
    id 21
    label "trafienie"
  ]
  node [
    id 22
    label "walka"
  ]
  node [
    id 23
    label "cios"
  ]
  node [
    id 24
    label "zdarzenie_si&#281;"
  ]
  node [
    id 25
    label "wdarcie_si&#281;"
  ]
  node [
    id 26
    label "pogorszenie"
  ]
  node [
    id 27
    label "d&#378;wi&#281;k"
  ]
  node [
    id 28
    label "poczucie"
  ]
  node [
    id 29
    label "coup"
  ]
  node [
    id 30
    label "reakcja"
  ]
  node [
    id 31
    label "contact"
  ]
  node [
    id 32
    label "stukni&#281;cie"
  ]
  node [
    id 33
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 34
    label "bat"
  ]
  node [
    id 35
    label "spowodowanie"
  ]
  node [
    id 36
    label "rush"
  ]
  node [
    id 37
    label "odbicie"
  ]
  node [
    id 38
    label "dawka"
  ]
  node [
    id 39
    label "zadanie"
  ]
  node [
    id 40
    label "&#347;ci&#281;cie"
  ]
  node [
    id 41
    label "st&#322;uczenie"
  ]
  node [
    id 42
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 43
    label "time"
  ]
  node [
    id 44
    label "odbicie_si&#281;"
  ]
  node [
    id 45
    label "dotkni&#281;cie"
  ]
  node [
    id 46
    label "charge"
  ]
  node [
    id 47
    label "dostanie"
  ]
  node [
    id 48
    label "skrytykowanie"
  ]
  node [
    id 49
    label "zagrywka"
  ]
  node [
    id 50
    label "manewr"
  ]
  node [
    id 51
    label "nast&#261;pienie"
  ]
  node [
    id 52
    label "uderzanie"
  ]
  node [
    id 53
    label "pogoda"
  ]
  node [
    id 54
    label "stroke"
  ]
  node [
    id 55
    label "pobicie"
  ]
  node [
    id 56
    label "ruch"
  ]
  node [
    id 57
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 58
    label "flap"
  ]
  node [
    id 59
    label "dotyk"
  ]
  node [
    id 60
    label "zrobienie"
  ]
  node [
    id 61
    label "wynie&#347;&#263;"
  ]
  node [
    id 62
    label "pieni&#261;dze"
  ]
  node [
    id 63
    label "ilo&#347;&#263;"
  ]
  node [
    id 64
    label "limit"
  ]
  node [
    id 65
    label "wynosi&#263;"
  ]
  node [
    id 66
    label "asceta"
  ]
  node [
    id 67
    label "pull"
  ]
  node [
    id 68
    label "precipitation"
  ]
  node [
    id 69
    label "przebudzenie"
  ]
  node [
    id 70
    label "wybicie"
  ]
  node [
    id 71
    label "wyizolowanie"
  ]
  node [
    id 72
    label "powyrywanie"
  ]
  node [
    id 73
    label "ekskursja"
  ]
  node [
    id 74
    label "bezsilnikowy"
  ]
  node [
    id 75
    label "budowla"
  ]
  node [
    id 76
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 77
    label "trasa"
  ]
  node [
    id 78
    label "podbieg"
  ]
  node [
    id 79
    label "turystyka"
  ]
  node [
    id 80
    label "nawierzchnia"
  ]
  node [
    id 81
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 82
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 83
    label "rajza"
  ]
  node [
    id 84
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 85
    label "korona_drogi"
  ]
  node [
    id 86
    label "passage"
  ]
  node [
    id 87
    label "wylot"
  ]
  node [
    id 88
    label "ekwipunek"
  ]
  node [
    id 89
    label "zbior&#243;wka"
  ]
  node [
    id 90
    label "marszrutyzacja"
  ]
  node [
    id 91
    label "wyb&#243;j"
  ]
  node [
    id 92
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 93
    label "drogowskaz"
  ]
  node [
    id 94
    label "spos&#243;b"
  ]
  node [
    id 95
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 96
    label "pobocze"
  ]
  node [
    id 97
    label "journey"
  ]
  node [
    id 98
    label "przebieg"
  ]
  node [
    id 99
    label "infrastruktura"
  ]
  node [
    id 100
    label "w&#281;ze&#322;"
  ]
  node [
    id 101
    label "obudowanie"
  ]
  node [
    id 102
    label "obudowywa&#263;"
  ]
  node [
    id 103
    label "zbudowa&#263;"
  ]
  node [
    id 104
    label "obudowa&#263;"
  ]
  node [
    id 105
    label "kolumnada"
  ]
  node [
    id 106
    label "korpus"
  ]
  node [
    id 107
    label "Sukiennice"
  ]
  node [
    id 108
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 109
    label "fundament"
  ]
  node [
    id 110
    label "obudowywanie"
  ]
  node [
    id 111
    label "postanie"
  ]
  node [
    id 112
    label "zbudowanie"
  ]
  node [
    id 113
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 114
    label "stan_surowy"
  ]
  node [
    id 115
    label "konstrukcja"
  ]
  node [
    id 116
    label "rzecz"
  ]
  node [
    id 117
    label "model"
  ]
  node [
    id 118
    label "narz&#281;dzie"
  ]
  node [
    id 119
    label "zbi&#243;r"
  ]
  node [
    id 120
    label "tryb"
  ]
  node [
    id 121
    label "nature"
  ]
  node [
    id 122
    label "ton"
  ]
  node [
    id 123
    label "rozmiar"
  ]
  node [
    id 124
    label "odcinek"
  ]
  node [
    id 125
    label "ambitus"
  ]
  node [
    id 126
    label "czas"
  ]
  node [
    id 127
    label "skala"
  ]
  node [
    id 128
    label "mechanika"
  ]
  node [
    id 129
    label "utrzymywanie"
  ]
  node [
    id 130
    label "move"
  ]
  node [
    id 131
    label "poruszenie"
  ]
  node [
    id 132
    label "myk"
  ]
  node [
    id 133
    label "utrzyma&#263;"
  ]
  node [
    id 134
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 135
    label "zjawisko"
  ]
  node [
    id 136
    label "utrzymanie"
  ]
  node [
    id 137
    label "travel"
  ]
  node [
    id 138
    label "kanciasty"
  ]
  node [
    id 139
    label "commercial_enterprise"
  ]
  node [
    id 140
    label "strumie&#324;"
  ]
  node [
    id 141
    label "proces"
  ]
  node [
    id 142
    label "aktywno&#347;&#263;"
  ]
  node [
    id 143
    label "kr&#243;tki"
  ]
  node [
    id 144
    label "taktyka"
  ]
  node [
    id 145
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 146
    label "apraksja"
  ]
  node [
    id 147
    label "natural_process"
  ]
  node [
    id 148
    label "utrzymywa&#263;"
  ]
  node [
    id 149
    label "d&#322;ugi"
  ]
  node [
    id 150
    label "wydarzenie"
  ]
  node [
    id 151
    label "dyssypacja_energii"
  ]
  node [
    id 152
    label "tumult"
  ]
  node [
    id 153
    label "stopek"
  ]
  node [
    id 154
    label "czynno&#347;&#263;"
  ]
  node [
    id 155
    label "zmiana"
  ]
  node [
    id 156
    label "lokomocja"
  ]
  node [
    id 157
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 158
    label "komunikacja"
  ]
  node [
    id 159
    label "drift"
  ]
  node [
    id 160
    label "pokrycie"
  ]
  node [
    id 161
    label "warstwa"
  ]
  node [
    id 162
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 163
    label "fingerpost"
  ]
  node [
    id 164
    label "tablica"
  ]
  node [
    id 165
    label "r&#281;kaw"
  ]
  node [
    id 166
    label "kontusz"
  ]
  node [
    id 167
    label "koniec"
  ]
  node [
    id 168
    label "otw&#243;r"
  ]
  node [
    id 169
    label "przydro&#380;e"
  ]
  node [
    id 170
    label "autostrada"
  ]
  node [
    id 171
    label "operacja"
  ]
  node [
    id 172
    label "bieg"
  ]
  node [
    id 173
    label "podr&#243;&#380;"
  ]
  node [
    id 174
    label "digress"
  ]
  node [
    id 175
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 176
    label "pozostawa&#263;"
  ]
  node [
    id 177
    label "s&#261;dzi&#263;"
  ]
  node [
    id 178
    label "chodzi&#263;"
  ]
  node [
    id 179
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 180
    label "stray"
  ]
  node [
    id 181
    label "mieszanie_si&#281;"
  ]
  node [
    id 182
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 183
    label "chodzenie"
  ]
  node [
    id 184
    label "beznap&#281;dowy"
  ]
  node [
    id 185
    label "dormitorium"
  ]
  node [
    id 186
    label "sk&#322;adanka"
  ]
  node [
    id 187
    label "wyprawa"
  ]
  node [
    id 188
    label "polowanie"
  ]
  node [
    id 189
    label "spis"
  ]
  node [
    id 190
    label "pomieszczenie"
  ]
  node [
    id 191
    label "fotografia"
  ]
  node [
    id 192
    label "kocher"
  ]
  node [
    id 193
    label "wyposa&#380;enie"
  ]
  node [
    id 194
    label "nie&#347;miertelnik"
  ]
  node [
    id 195
    label "moderunek"
  ]
  node [
    id 196
    label "cz&#322;owiek"
  ]
  node [
    id 197
    label "ukochanie"
  ]
  node [
    id 198
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 199
    label "feblik"
  ]
  node [
    id 200
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 201
    label "podnieci&#263;"
  ]
  node [
    id 202
    label "numer"
  ]
  node [
    id 203
    label "po&#380;ycie"
  ]
  node [
    id 204
    label "tendency"
  ]
  node [
    id 205
    label "podniecenie"
  ]
  node [
    id 206
    label "afekt"
  ]
  node [
    id 207
    label "zakochanie"
  ]
  node [
    id 208
    label "zajawka"
  ]
  node [
    id 209
    label "seks"
  ]
  node [
    id 210
    label "podniecanie"
  ]
  node [
    id 211
    label "imisja"
  ]
  node [
    id 212
    label "love"
  ]
  node [
    id 213
    label "rozmna&#380;anie"
  ]
  node [
    id 214
    label "ruch_frykcyjny"
  ]
  node [
    id 215
    label "na_pieska"
  ]
  node [
    id 216
    label "serce"
  ]
  node [
    id 217
    label "pozycja_misjonarska"
  ]
  node [
    id 218
    label "wi&#281;&#378;"
  ]
  node [
    id 219
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 220
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 221
    label "z&#322;&#261;czenie"
  ]
  node [
    id 222
    label "gra_wst&#281;pna"
  ]
  node [
    id 223
    label "erotyka"
  ]
  node [
    id 224
    label "emocja"
  ]
  node [
    id 225
    label "baraszki"
  ]
  node [
    id 226
    label "drogi"
  ]
  node [
    id 227
    label "po&#380;&#261;danie"
  ]
  node [
    id 228
    label "wzw&#243;d"
  ]
  node [
    id 229
    label "podnieca&#263;"
  ]
  node [
    id 230
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 231
    label "kochanka"
  ]
  node [
    id 232
    label "kultura_fizyczna"
  ]
  node [
    id 233
    label "turyzm"
  ]
  node [
    id 234
    label "rodzimy"
  ]
  node [
    id 235
    label "w&#322;asny"
  ]
  node [
    id 236
    label "tutejszy"
  ]
  node [
    id 237
    label "polski"
  ]
  node [
    id 238
    label "regionalny"
  ]
  node [
    id 239
    label "gwara"
  ]
  node [
    id 240
    label "po_mazursku"
  ]
  node [
    id 241
    label "przedmiot"
  ]
  node [
    id 242
    label "Polish"
  ]
  node [
    id 243
    label "goniony"
  ]
  node [
    id 244
    label "oberek"
  ]
  node [
    id 245
    label "ryba_po_grecku"
  ]
  node [
    id 246
    label "sztajer"
  ]
  node [
    id 247
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 248
    label "krakowiak"
  ]
  node [
    id 249
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 250
    label "pierogi_ruskie"
  ]
  node [
    id 251
    label "lacki"
  ]
  node [
    id 252
    label "polak"
  ]
  node [
    id 253
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 254
    label "chodzony"
  ]
  node [
    id 255
    label "po_polsku"
  ]
  node [
    id 256
    label "mazur"
  ]
  node [
    id 257
    label "polsko"
  ]
  node [
    id 258
    label "skoczny"
  ]
  node [
    id 259
    label "drabant"
  ]
  node [
    id 260
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 261
    label "j&#281;zyk"
  ]
  node [
    id 262
    label "tradycyjny"
  ]
  node [
    id 263
    label "regionalnie"
  ]
  node [
    id 264
    label "lokalny"
  ]
  node [
    id 265
    label "typowy"
  ]
  node [
    id 266
    label "dialekt"
  ]
  node [
    id 267
    label "etnolekt"
  ]
  node [
    id 268
    label "socjolekt"
  ]
  node [
    id 269
    label "nr"
  ]
  node [
    id 270
    label "16"
  ]
  node [
    id 271
    label "w"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 269
    target 270
  ]
  edge [
    source 269
    target 271
  ]
  edge [
    source 270
    target 271
  ]
]
