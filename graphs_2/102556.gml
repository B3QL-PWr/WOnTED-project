graph [
  node [
    id 0
    label "dokument"
    origin "text"
  ]
  node [
    id 1
    label "sk&#322;adany"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "kandydat"
    origin "text"
  ]
  node [
    id 4
    label "musza"
    origin "text"
  ]
  node [
    id 5
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zapis"
  ]
  node [
    id 7
    label "&#347;wiadectwo"
  ]
  node [
    id 8
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 9
    label "wytw&#243;r"
  ]
  node [
    id 10
    label "parafa"
  ]
  node [
    id 11
    label "plik"
  ]
  node [
    id 12
    label "raport&#243;wka"
  ]
  node [
    id 13
    label "utw&#243;r"
  ]
  node [
    id 14
    label "record"
  ]
  node [
    id 15
    label "registratura"
  ]
  node [
    id 16
    label "dokumentacja"
  ]
  node [
    id 17
    label "fascyku&#322;"
  ]
  node [
    id 18
    label "artyku&#322;"
  ]
  node [
    id 19
    label "writing"
  ]
  node [
    id 20
    label "sygnatariusz"
  ]
  node [
    id 21
    label "dow&#243;d"
  ]
  node [
    id 22
    label "o&#347;wiadczenie"
  ]
  node [
    id 23
    label "za&#347;wiadczenie"
  ]
  node [
    id 24
    label "certificate"
  ]
  node [
    id 25
    label "promocja"
  ]
  node [
    id 26
    label "spos&#243;b"
  ]
  node [
    id 27
    label "entrance"
  ]
  node [
    id 28
    label "czynno&#347;&#263;"
  ]
  node [
    id 29
    label "wpis"
  ]
  node [
    id 30
    label "normalizacja"
  ]
  node [
    id 31
    label "obrazowanie"
  ]
  node [
    id 32
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 33
    label "organ"
  ]
  node [
    id 34
    label "tre&#347;&#263;"
  ]
  node [
    id 35
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 36
    label "part"
  ]
  node [
    id 37
    label "element_anatomiczny"
  ]
  node [
    id 38
    label "tekst"
  ]
  node [
    id 39
    label "komunikat"
  ]
  node [
    id 40
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 41
    label "przedmiot"
  ]
  node [
    id 42
    label "p&#322;&#243;d"
  ]
  node [
    id 43
    label "work"
  ]
  node [
    id 44
    label "rezultat"
  ]
  node [
    id 45
    label "podkatalog"
  ]
  node [
    id 46
    label "nadpisa&#263;"
  ]
  node [
    id 47
    label "nadpisanie"
  ]
  node [
    id 48
    label "bundle"
  ]
  node [
    id 49
    label "folder"
  ]
  node [
    id 50
    label "nadpisywanie"
  ]
  node [
    id 51
    label "paczka"
  ]
  node [
    id 52
    label "nadpisywa&#263;"
  ]
  node [
    id 53
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 54
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 55
    label "przedstawiciel"
  ]
  node [
    id 56
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 57
    label "biuro"
  ]
  node [
    id 58
    label "register"
  ]
  node [
    id 59
    label "zbi&#243;r"
  ]
  node [
    id 60
    label "ekscerpcja"
  ]
  node [
    id 61
    label "materia&#322;"
  ]
  node [
    id 62
    label "operat"
  ]
  node [
    id 63
    label "kosztorys"
  ]
  node [
    id 64
    label "torba"
  ]
  node [
    id 65
    label "wydanie"
  ]
  node [
    id 66
    label "paraph"
  ]
  node [
    id 67
    label "podpis"
  ]
  node [
    id 68
    label "blok"
  ]
  node [
    id 69
    label "prawda"
  ]
  node [
    id 70
    label "znak_j&#281;zykowy"
  ]
  node [
    id 71
    label "nag&#322;&#243;wek"
  ]
  node [
    id 72
    label "szkic"
  ]
  node [
    id 73
    label "line"
  ]
  node [
    id 74
    label "fragment"
  ]
  node [
    id 75
    label "wyr&#243;b"
  ]
  node [
    id 76
    label "rodzajnik"
  ]
  node [
    id 77
    label "towar"
  ]
  node [
    id 78
    label "paragraf"
  ]
  node [
    id 79
    label "lista_wyborcza"
  ]
  node [
    id 80
    label "cz&#322;owiek"
  ]
  node [
    id 81
    label "aspirowanie"
  ]
  node [
    id 82
    label "bierne_prawo_wyborcze"
  ]
  node [
    id 83
    label "ludzko&#347;&#263;"
  ]
  node [
    id 84
    label "asymilowanie"
  ]
  node [
    id 85
    label "wapniak"
  ]
  node [
    id 86
    label "asymilowa&#263;"
  ]
  node [
    id 87
    label "os&#322;abia&#263;"
  ]
  node [
    id 88
    label "posta&#263;"
  ]
  node [
    id 89
    label "hominid"
  ]
  node [
    id 90
    label "podw&#322;adny"
  ]
  node [
    id 91
    label "os&#322;abianie"
  ]
  node [
    id 92
    label "g&#322;owa"
  ]
  node [
    id 93
    label "figura"
  ]
  node [
    id 94
    label "portrecista"
  ]
  node [
    id 95
    label "dwun&#243;g"
  ]
  node [
    id 96
    label "profanum"
  ]
  node [
    id 97
    label "mikrokosmos"
  ]
  node [
    id 98
    label "nasada"
  ]
  node [
    id 99
    label "duch"
  ]
  node [
    id 100
    label "antropochoria"
  ]
  node [
    id 101
    label "osoba"
  ]
  node [
    id 102
    label "wz&#243;r"
  ]
  node [
    id 103
    label "senior"
  ]
  node [
    id 104
    label "oddzia&#322;ywanie"
  ]
  node [
    id 105
    label "Adam"
  ]
  node [
    id 106
    label "homo_sapiens"
  ]
  node [
    id 107
    label "polifag"
  ]
  node [
    id 108
    label "materia"
  ]
  node [
    id 109
    label "nawil&#380;arka"
  ]
  node [
    id 110
    label "bielarnia"
  ]
  node [
    id 111
    label "dyspozycja"
  ]
  node [
    id 112
    label "dane"
  ]
  node [
    id 113
    label "tworzywo"
  ]
  node [
    id 114
    label "substancja"
  ]
  node [
    id 115
    label "archiwum"
  ]
  node [
    id 116
    label "krajka"
  ]
  node [
    id 117
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 118
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 119
    label "krajalno&#347;&#263;"
  ]
  node [
    id 120
    label "campaigning"
  ]
  node [
    id 121
    label "wymawianie"
  ]
  node [
    id 122
    label "staranie_si&#281;"
  ]
  node [
    id 123
    label "poznawa&#263;"
  ]
  node [
    id 124
    label "fold"
  ]
  node [
    id 125
    label "obejmowa&#263;"
  ]
  node [
    id 126
    label "mie&#263;"
  ]
  node [
    id 127
    label "lock"
  ]
  node [
    id 128
    label "make"
  ]
  node [
    id 129
    label "ustala&#263;"
  ]
  node [
    id 130
    label "zamyka&#263;"
  ]
  node [
    id 131
    label "suspend"
  ]
  node [
    id 132
    label "ujmowa&#263;"
  ]
  node [
    id 133
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 134
    label "instytucja"
  ]
  node [
    id 135
    label "unieruchamia&#263;"
  ]
  node [
    id 136
    label "sk&#322;ada&#263;"
  ]
  node [
    id 137
    label "ko&#324;czy&#263;"
  ]
  node [
    id 138
    label "blokowa&#263;"
  ]
  node [
    id 139
    label "umieszcza&#263;"
  ]
  node [
    id 140
    label "ukrywa&#263;"
  ]
  node [
    id 141
    label "exsert"
  ]
  node [
    id 142
    label "hide"
  ]
  node [
    id 143
    label "czu&#263;"
  ]
  node [
    id 144
    label "support"
  ]
  node [
    id 145
    label "need"
  ]
  node [
    id 146
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 147
    label "cognizance"
  ]
  node [
    id 148
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 149
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 150
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 151
    label "go_steady"
  ]
  node [
    id 152
    label "detect"
  ]
  node [
    id 153
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 154
    label "hurt"
  ]
  node [
    id 155
    label "styka&#263;_si&#281;"
  ]
  node [
    id 156
    label "zaskakiwa&#263;"
  ]
  node [
    id 157
    label "podejmowa&#263;"
  ]
  node [
    id 158
    label "cover"
  ]
  node [
    id 159
    label "rozumie&#263;"
  ]
  node [
    id 160
    label "senator"
  ]
  node [
    id 161
    label "obj&#261;&#263;"
  ]
  node [
    id 162
    label "meet"
  ]
  node [
    id 163
    label "obejmowanie"
  ]
  node [
    id 164
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 165
    label "powodowa&#263;"
  ]
  node [
    id 166
    label "involve"
  ]
  node [
    id 167
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 168
    label "dotyczy&#263;"
  ]
  node [
    id 169
    label "zagarnia&#263;"
  ]
  node [
    id 170
    label "embrace"
  ]
  node [
    id 171
    label "dotyka&#263;"
  ]
  node [
    id 172
    label "robi&#263;"
  ]
  node [
    id 173
    label "peddle"
  ]
  node [
    id 174
    label "unwrap"
  ]
  node [
    id 175
    label "decydowa&#263;"
  ]
  node [
    id 176
    label "zmienia&#263;"
  ]
  node [
    id 177
    label "umacnia&#263;"
  ]
  node [
    id 178
    label "arrange"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
]
