graph [
  node [
    id 0
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "destin"
    origin "text"
  ]
  node [
    id 2
    label "sandlin"
    origin "text"
  ]
  node [
    id 3
    label "gra&#263;"
  ]
  node [
    id 4
    label "zapoznawa&#263;"
  ]
  node [
    id 5
    label "uprzedza&#263;"
  ]
  node [
    id 6
    label "wyra&#380;a&#263;"
  ]
  node [
    id 7
    label "present"
  ]
  node [
    id 8
    label "represent"
  ]
  node [
    id 9
    label "program"
  ]
  node [
    id 10
    label "display"
  ]
  node [
    id 11
    label "attest"
  ]
  node [
    id 12
    label "przedstawia&#263;"
  ]
  node [
    id 13
    label "zawiera&#263;"
  ]
  node [
    id 14
    label "poznawa&#263;"
  ]
  node [
    id 15
    label "obznajamia&#263;"
  ]
  node [
    id 16
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 17
    label "go_steady"
  ]
  node [
    id 18
    label "informowa&#263;"
  ]
  node [
    id 19
    label "teatr"
  ]
  node [
    id 20
    label "exhibit"
  ]
  node [
    id 21
    label "podawa&#263;"
  ]
  node [
    id 22
    label "pokazywa&#263;"
  ]
  node [
    id 23
    label "demonstrowa&#263;"
  ]
  node [
    id 24
    label "przedstawienie"
  ]
  node [
    id 25
    label "opisywa&#263;"
  ]
  node [
    id 26
    label "ukazywa&#263;"
  ]
  node [
    id 27
    label "zg&#322;asza&#263;"
  ]
  node [
    id 28
    label "typify"
  ]
  node [
    id 29
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 30
    label "stanowi&#263;"
  ]
  node [
    id 31
    label "robi&#263;"
  ]
  node [
    id 32
    label "og&#322;asza&#263;"
  ]
  node [
    id 33
    label "post"
  ]
  node [
    id 34
    label "anticipate"
  ]
  node [
    id 35
    label "znaczy&#263;"
  ]
  node [
    id 36
    label "give_voice"
  ]
  node [
    id 37
    label "oznacza&#263;"
  ]
  node [
    id 38
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 39
    label "komunikowa&#263;"
  ]
  node [
    id 40
    label "convey"
  ]
  node [
    id 41
    label "arouse"
  ]
  node [
    id 42
    label "instalowa&#263;"
  ]
  node [
    id 43
    label "oprogramowanie"
  ]
  node [
    id 44
    label "odinstalowywa&#263;"
  ]
  node [
    id 45
    label "spis"
  ]
  node [
    id 46
    label "zaprezentowanie"
  ]
  node [
    id 47
    label "podprogram"
  ]
  node [
    id 48
    label "ogranicznik_referencyjny"
  ]
  node [
    id 49
    label "course_of_study"
  ]
  node [
    id 50
    label "booklet"
  ]
  node [
    id 51
    label "dzia&#322;"
  ]
  node [
    id 52
    label "odinstalowanie"
  ]
  node [
    id 53
    label "broszura"
  ]
  node [
    id 54
    label "wytw&#243;r"
  ]
  node [
    id 55
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 56
    label "kana&#322;"
  ]
  node [
    id 57
    label "teleferie"
  ]
  node [
    id 58
    label "zainstalowanie"
  ]
  node [
    id 59
    label "struktura_organizacyjna"
  ]
  node [
    id 60
    label "pirat"
  ]
  node [
    id 61
    label "zaprezentowa&#263;"
  ]
  node [
    id 62
    label "prezentowanie"
  ]
  node [
    id 63
    label "interfejs"
  ]
  node [
    id 64
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 65
    label "okno"
  ]
  node [
    id 66
    label "blok"
  ]
  node [
    id 67
    label "punkt"
  ]
  node [
    id 68
    label "folder"
  ]
  node [
    id 69
    label "zainstalowa&#263;"
  ]
  node [
    id 70
    label "za&#322;o&#380;enie"
  ]
  node [
    id 71
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 72
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 73
    label "ram&#243;wka"
  ]
  node [
    id 74
    label "tryb"
  ]
  node [
    id 75
    label "emitowa&#263;"
  ]
  node [
    id 76
    label "emitowanie"
  ]
  node [
    id 77
    label "odinstalowywanie"
  ]
  node [
    id 78
    label "instrukcja"
  ]
  node [
    id 79
    label "informatyka"
  ]
  node [
    id 80
    label "deklaracja"
  ]
  node [
    id 81
    label "menu"
  ]
  node [
    id 82
    label "sekcja_krytyczna"
  ]
  node [
    id 83
    label "furkacja"
  ]
  node [
    id 84
    label "podstawa"
  ]
  node [
    id 85
    label "instalowanie"
  ]
  node [
    id 86
    label "oferta"
  ]
  node [
    id 87
    label "odinstalowa&#263;"
  ]
  node [
    id 88
    label "&#347;wieci&#263;"
  ]
  node [
    id 89
    label "play"
  ]
  node [
    id 90
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 91
    label "muzykowa&#263;"
  ]
  node [
    id 92
    label "majaczy&#263;"
  ]
  node [
    id 93
    label "szczeka&#263;"
  ]
  node [
    id 94
    label "wykonywa&#263;"
  ]
  node [
    id 95
    label "napierdziela&#263;"
  ]
  node [
    id 96
    label "dzia&#322;a&#263;"
  ]
  node [
    id 97
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 98
    label "instrument_muzyczny"
  ]
  node [
    id 99
    label "pasowa&#263;"
  ]
  node [
    id 100
    label "sound"
  ]
  node [
    id 101
    label "dally"
  ]
  node [
    id 102
    label "i&#347;&#263;"
  ]
  node [
    id 103
    label "stara&#263;_si&#281;"
  ]
  node [
    id 104
    label "tokowa&#263;"
  ]
  node [
    id 105
    label "wida&#263;"
  ]
  node [
    id 106
    label "rozgrywa&#263;"
  ]
  node [
    id 107
    label "do"
  ]
  node [
    id 108
    label "brzmie&#263;"
  ]
  node [
    id 109
    label "wykorzystywa&#263;"
  ]
  node [
    id 110
    label "cope"
  ]
  node [
    id 111
    label "otwarcie"
  ]
  node [
    id 112
    label "rola"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 1
    target 2
  ]
]
