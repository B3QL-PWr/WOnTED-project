graph [
  node [
    id 0
    label "codziennie"
    origin "text"
  ]
  node [
    id 1
    label "rano"
    origin "text"
  ]
  node [
    id 2
    label "pospolicie"
  ]
  node [
    id 3
    label "regularnie"
  ]
  node [
    id 4
    label "daily"
  ]
  node [
    id 5
    label "codzienny"
  ]
  node [
    id 6
    label "prozaicznie"
  ]
  node [
    id 7
    label "cz&#281;sto"
  ]
  node [
    id 8
    label "stale"
  ]
  node [
    id 9
    label "regularny"
  ]
  node [
    id 10
    label "harmonijnie"
  ]
  node [
    id 11
    label "zwyczajny"
  ]
  node [
    id 12
    label "poprostu"
  ]
  node [
    id 13
    label "cz&#281;sty"
  ]
  node [
    id 14
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 15
    label "zwyczajnie"
  ]
  node [
    id 16
    label "niewymy&#347;lnie"
  ]
  node [
    id 17
    label "wsp&#243;lnie"
  ]
  node [
    id 18
    label "pospolity"
  ]
  node [
    id 19
    label "zawsze"
  ]
  node [
    id 20
    label "sta&#322;y"
  ]
  node [
    id 21
    label "zwykle"
  ]
  node [
    id 22
    label "jednakowo"
  ]
  node [
    id 23
    label "cykliczny"
  ]
  node [
    id 24
    label "prozaiczny"
  ]
  node [
    id 25
    label "powszedny"
  ]
  node [
    id 26
    label "aurora"
  ]
  node [
    id 27
    label "wsch&#243;d"
  ]
  node [
    id 28
    label "dzie&#324;"
  ]
  node [
    id 29
    label "zjawisko"
  ]
  node [
    id 30
    label "pora"
  ]
  node [
    id 31
    label "proces"
  ]
  node [
    id 32
    label "boski"
  ]
  node [
    id 33
    label "krajobraz"
  ]
  node [
    id 34
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 35
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 36
    label "przywidzenie"
  ]
  node [
    id 37
    label "presence"
  ]
  node [
    id 38
    label "charakter"
  ]
  node [
    id 39
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 40
    label "run"
  ]
  node [
    id 41
    label "okres_czasu"
  ]
  node [
    id 42
    label "czas"
  ]
  node [
    id 43
    label "brzask"
  ]
  node [
    id 44
    label "obszar"
  ]
  node [
    id 45
    label "pocz&#261;tek"
  ]
  node [
    id 46
    label "strona_&#347;wiata"
  ]
  node [
    id 47
    label "szabas"
  ]
  node [
    id 48
    label "s&#322;o&#324;ce"
  ]
  node [
    id 49
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 50
    label "ranek"
  ]
  node [
    id 51
    label "doba"
  ]
  node [
    id 52
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 53
    label "noc"
  ]
  node [
    id 54
    label "podwiecz&#243;r"
  ]
  node [
    id 55
    label "po&#322;udnie"
  ]
  node [
    id 56
    label "godzina"
  ]
  node [
    id 57
    label "przedpo&#322;udnie"
  ]
  node [
    id 58
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 59
    label "long_time"
  ]
  node [
    id 60
    label "wiecz&#243;r"
  ]
  node [
    id 61
    label "t&#322;usty_czwartek"
  ]
  node [
    id 62
    label "popo&#322;udnie"
  ]
  node [
    id 63
    label "walentynki"
  ]
  node [
    id 64
    label "czynienie_si&#281;"
  ]
  node [
    id 65
    label "tydzie&#324;"
  ]
  node [
    id 66
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 67
    label "wzej&#347;cie"
  ]
  node [
    id 68
    label "wsta&#263;"
  ]
  node [
    id 69
    label "day"
  ]
  node [
    id 70
    label "termin"
  ]
  node [
    id 71
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 72
    label "wstanie"
  ]
  node [
    id 73
    label "przedwiecz&#243;r"
  ]
  node [
    id 74
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 75
    label "Sylwester"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
]
