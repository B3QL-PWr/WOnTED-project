graph [
  node [
    id 0
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "czarny"
    origin "text"
  ]
  node [
    id 4
    label "scenariusz"
    origin "text"
  ]
  node [
    id 5
    label "dla"
    origin "text"
  ]
  node [
    id 6
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 7
    label "odejmowa&#263;"
  ]
  node [
    id 8
    label "mie&#263;_miejsce"
  ]
  node [
    id 9
    label "bankrupt"
  ]
  node [
    id 10
    label "open"
  ]
  node [
    id 11
    label "set_about"
  ]
  node [
    id 12
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 13
    label "begin"
  ]
  node [
    id 14
    label "post&#281;powa&#263;"
  ]
  node [
    id 15
    label "zabiera&#263;"
  ]
  node [
    id 16
    label "liczy&#263;"
  ]
  node [
    id 17
    label "reduce"
  ]
  node [
    id 18
    label "take"
  ]
  node [
    id 19
    label "abstract"
  ]
  node [
    id 20
    label "ujemny"
  ]
  node [
    id 21
    label "oddziela&#263;"
  ]
  node [
    id 22
    label "oddala&#263;"
  ]
  node [
    id 23
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 24
    label "robi&#263;"
  ]
  node [
    id 25
    label "go"
  ]
  node [
    id 26
    label "przybiera&#263;"
  ]
  node [
    id 27
    label "act"
  ]
  node [
    id 28
    label "i&#347;&#263;"
  ]
  node [
    id 29
    label "use"
  ]
  node [
    id 30
    label "create"
  ]
  node [
    id 31
    label "przeprowadza&#263;"
  ]
  node [
    id 32
    label "prawdzi&#263;"
  ]
  node [
    id 33
    label "prosecute"
  ]
  node [
    id 34
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 35
    label "wykorzystywa&#263;"
  ]
  node [
    id 36
    label "tworzy&#263;"
  ]
  node [
    id 37
    label "wykonywa&#263;"
  ]
  node [
    id 38
    label "powodowa&#263;"
  ]
  node [
    id 39
    label "transact"
  ]
  node [
    id 40
    label "osi&#261;ga&#263;"
  ]
  node [
    id 41
    label "string"
  ]
  node [
    id 42
    label "pomaga&#263;"
  ]
  node [
    id 43
    label "pope&#322;nia&#263;"
  ]
  node [
    id 44
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 45
    label "wytwarza&#263;"
  ]
  node [
    id 46
    label "get"
  ]
  node [
    id 47
    label "consist"
  ]
  node [
    id 48
    label "stanowi&#263;"
  ]
  node [
    id 49
    label "raise"
  ]
  node [
    id 50
    label "wymienia&#263;"
  ]
  node [
    id 51
    label "deal"
  ]
  node [
    id 52
    label "korzysta&#263;"
  ]
  node [
    id 53
    label "liga&#263;"
  ]
  node [
    id 54
    label "give"
  ]
  node [
    id 55
    label "distribute"
  ]
  node [
    id 56
    label "u&#380;ywa&#263;"
  ]
  node [
    id 57
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 58
    label "krzywdzi&#263;"
  ]
  node [
    id 59
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 60
    label "urzeczywistnia&#263;"
  ]
  node [
    id 61
    label "czarne"
  ]
  node [
    id 62
    label "kolorowy"
  ]
  node [
    id 63
    label "bierka_szachowa"
  ]
  node [
    id 64
    label "gorzki"
  ]
  node [
    id 65
    label "murzy&#324;ski"
  ]
  node [
    id 66
    label "ksi&#261;dz"
  ]
  node [
    id 67
    label "kompletny"
  ]
  node [
    id 68
    label "przewrotny"
  ]
  node [
    id 69
    label "ponury"
  ]
  node [
    id 70
    label "beznadziejny"
  ]
  node [
    id 71
    label "z&#322;y"
  ]
  node [
    id 72
    label "wedel"
  ]
  node [
    id 73
    label "czarnuch"
  ]
  node [
    id 74
    label "granatowo"
  ]
  node [
    id 75
    label "ciemny"
  ]
  node [
    id 76
    label "negatywny"
  ]
  node [
    id 77
    label "ciemnienie"
  ]
  node [
    id 78
    label "czernienie"
  ]
  node [
    id 79
    label "zaczernienie"
  ]
  node [
    id 80
    label "pesymistycznie"
  ]
  node [
    id 81
    label "abolicjonista"
  ]
  node [
    id 82
    label "brudny"
  ]
  node [
    id 83
    label "zaczernianie_si&#281;"
  ]
  node [
    id 84
    label "kafar"
  ]
  node [
    id 85
    label "czarnuchowaty"
  ]
  node [
    id 86
    label "pessimistic"
  ]
  node [
    id 87
    label "czarniawy"
  ]
  node [
    id 88
    label "ciemnosk&#243;ry"
  ]
  node [
    id 89
    label "okrutny"
  ]
  node [
    id 90
    label "czarno"
  ]
  node [
    id 91
    label "zaczernienie_si&#281;"
  ]
  node [
    id 92
    label "niepomy&#347;lny"
  ]
  node [
    id 93
    label "pieski"
  ]
  node [
    id 94
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 95
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 96
    label "niekorzystny"
  ]
  node [
    id 97
    label "z&#322;oszczenie"
  ]
  node [
    id 98
    label "sierdzisty"
  ]
  node [
    id 99
    label "niegrzeczny"
  ]
  node [
    id 100
    label "zez&#322;oszczenie"
  ]
  node [
    id 101
    label "zdenerwowany"
  ]
  node [
    id 102
    label "rozgniewanie"
  ]
  node [
    id 103
    label "gniewanie"
  ]
  node [
    id 104
    label "niemoralny"
  ]
  node [
    id 105
    label "&#378;le"
  ]
  node [
    id 106
    label "syf"
  ]
  node [
    id 107
    label "podejrzanie"
  ]
  node [
    id 108
    label "&#347;niady"
  ]
  node [
    id 109
    label "&#263;my"
  ]
  node [
    id 110
    label "ciemnow&#322;osy"
  ]
  node [
    id 111
    label "pe&#322;ny"
  ]
  node [
    id 112
    label "nierozumny"
  ]
  node [
    id 113
    label "ciemno"
  ]
  node [
    id 114
    label "g&#322;upi"
  ]
  node [
    id 115
    label "zdrowy"
  ]
  node [
    id 116
    label "zacofany"
  ]
  node [
    id 117
    label "t&#281;py"
  ]
  node [
    id 118
    label "niewykszta&#322;cony"
  ]
  node [
    id 119
    label "nieprzejrzysty"
  ]
  node [
    id 120
    label "niepewny"
  ]
  node [
    id 121
    label "zabarwienie_si&#281;"
  ]
  node [
    id 122
    label "ciekawy"
  ]
  node [
    id 123
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 124
    label "cz&#322;owiek"
  ]
  node [
    id 125
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 126
    label "weso&#322;y"
  ]
  node [
    id 127
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 128
    label "barwienie"
  ]
  node [
    id 129
    label "kolorowo"
  ]
  node [
    id 130
    label "barwnie"
  ]
  node [
    id 131
    label "kolorowanie"
  ]
  node [
    id 132
    label "barwisty"
  ]
  node [
    id 133
    label "przyjemny"
  ]
  node [
    id 134
    label "barwienie_si&#281;"
  ]
  node [
    id 135
    label "pi&#281;kny"
  ]
  node [
    id 136
    label "ubarwienie"
  ]
  node [
    id 137
    label "czerniawo"
  ]
  node [
    id 138
    label "czerniawy"
  ]
  node [
    id 139
    label "etnolekt"
  ]
  node [
    id 140
    label "niesprawiedliwy"
  ]
  node [
    id 141
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 142
    label "prymitywny"
  ]
  node [
    id 143
    label "ci&#281;&#380;ki"
  ]
  node [
    id 144
    label "po_murzy&#324;sku"
  ]
  node [
    id 145
    label "czarnosk&#243;ry"
  ]
  node [
    id 146
    label "kompletnie"
  ]
  node [
    id 147
    label "zupe&#322;ny"
  ]
  node [
    id 148
    label "w_pizdu"
  ]
  node [
    id 149
    label "beznadziejnie"
  ]
  node [
    id 150
    label "kijowy"
  ]
  node [
    id 151
    label "g&#243;wniany"
  ]
  node [
    id 152
    label "nieludzki"
  ]
  node [
    id 153
    label "straszny"
  ]
  node [
    id 154
    label "mocny"
  ]
  node [
    id 155
    label "pod&#322;y"
  ]
  node [
    id 156
    label "bezlito&#347;ny"
  ]
  node [
    id 157
    label "gro&#378;ny"
  ]
  node [
    id 158
    label "okrutnie"
  ]
  node [
    id 159
    label "zmienny"
  ]
  node [
    id 160
    label "niejednoznaczny"
  ]
  node [
    id 161
    label "przewrotnie"
  ]
  node [
    id 162
    label "gorzknienie"
  ]
  node [
    id 163
    label "zgorzknienie"
  ]
  node [
    id 164
    label "nieprzyjemny"
  ]
  node [
    id 165
    label "gorzko"
  ]
  node [
    id 166
    label "przest&#281;pstwo"
  ]
  node [
    id 167
    label "nielegalny"
  ]
  node [
    id 168
    label "brudno"
  ]
  node [
    id 169
    label "brudzenie_si&#281;"
  ]
  node [
    id 170
    label "z&#322;amany"
  ]
  node [
    id 171
    label "nieprzyzwoity"
  ]
  node [
    id 172
    label "nieporz&#261;dny"
  ]
  node [
    id 173
    label "brudzenie"
  ]
  node [
    id 174
    label "flejtuchowaty"
  ]
  node [
    id 175
    label "zabrudzenie_si&#281;"
  ]
  node [
    id 176
    label "nieczysty"
  ]
  node [
    id 177
    label "u&#380;ywany"
  ]
  node [
    id 178
    label "ponuro"
  ]
  node [
    id 179
    label "smutny"
  ]
  node [
    id 180
    label "chmurno"
  ]
  node [
    id 181
    label "pos&#281;pnie"
  ]
  node [
    id 182
    label "s&#281;pny"
  ]
  node [
    id 183
    label "niepomy&#347;lnie"
  ]
  node [
    id 184
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 185
    label "negatywnie"
  ]
  node [
    id 186
    label "ujemnie"
  ]
  node [
    id 187
    label "granatowy"
  ]
  node [
    id 188
    label "pessimistically"
  ]
  node [
    id 189
    label "pesymistyczny"
  ]
  node [
    id 190
    label "przeciwnik"
  ]
  node [
    id 191
    label "znie&#347;&#263;"
  ]
  node [
    id 192
    label "zniesienie"
  ]
  node [
    id 193
    label "zwolennik"
  ]
  node [
    id 194
    label "owocowo"
  ]
  node [
    id 195
    label "ciemnoniebiesko"
  ]
  node [
    id 196
    label "w&#281;dlina"
  ]
  node [
    id 197
    label "&#322;&#243;dzki"
  ]
  node [
    id 198
    label "czarna_bierka"
  ]
  node [
    id 199
    label "strona"
  ]
  node [
    id 200
    label "zabarwienie"
  ]
  node [
    id 201
    label "shoe_polish"
  ]
  node [
    id 202
    label "odcinanie_si&#281;"
  ]
  node [
    id 203
    label "zasnuwanie_si&#281;"
  ]
  node [
    id 204
    label "stawanie_si&#281;"
  ]
  node [
    id 205
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 206
    label "baba"
  ]
  node [
    id 207
    label "Jamajka"
  ]
  node [
    id 208
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 209
    label "sze&#347;ciopak"
  ]
  node [
    id 210
    label "maszyna_robocza"
  ]
  node [
    id 211
    label "m&#322;ot_kafarowy"
  ]
  node [
    id 212
    label "bijak"
  ]
  node [
    id 213
    label "kulturysta"
  ]
  node [
    id 214
    label "mu&#322;y"
  ]
  node [
    id 215
    label "narciarstwo"
  ]
  node [
    id 216
    label "technika"
  ]
  node [
    id 217
    label "ksi&#281;&#380;a"
  ]
  node [
    id 218
    label "rozgrzeszanie"
  ]
  node [
    id 219
    label "duszpasterstwo"
  ]
  node [
    id 220
    label "eklezjasta"
  ]
  node [
    id 221
    label "duchowny"
  ]
  node [
    id 222
    label "rozgrzesza&#263;"
  ]
  node [
    id 223
    label "seminarzysta"
  ]
  node [
    id 224
    label "klecha"
  ]
  node [
    id 225
    label "pasterz"
  ]
  node [
    id 226
    label "kol&#281;da"
  ]
  node [
    id 227
    label "kap&#322;an"
  ]
  node [
    id 228
    label "dramat"
  ]
  node [
    id 229
    label "plan"
  ]
  node [
    id 230
    label "prognoza"
  ]
  node [
    id 231
    label "scenario"
  ]
  node [
    id 232
    label "tekst"
  ]
  node [
    id 233
    label "rola"
  ]
  node [
    id 234
    label "model"
  ]
  node [
    id 235
    label "intencja"
  ]
  node [
    id 236
    label "punkt"
  ]
  node [
    id 237
    label "rysunek"
  ]
  node [
    id 238
    label "miejsce_pracy"
  ]
  node [
    id 239
    label "przestrze&#324;"
  ]
  node [
    id 240
    label "wytw&#243;r"
  ]
  node [
    id 241
    label "device"
  ]
  node [
    id 242
    label "pomys&#322;"
  ]
  node [
    id 243
    label "obraz"
  ]
  node [
    id 244
    label "reprezentacja"
  ]
  node [
    id 245
    label "agreement"
  ]
  node [
    id 246
    label "dekoracja"
  ]
  node [
    id 247
    label "perspektywa"
  ]
  node [
    id 248
    label "diagnoza"
  ]
  node [
    id 249
    label "przewidywanie"
  ]
  node [
    id 250
    label "ekscerpcja"
  ]
  node [
    id 251
    label "j&#281;zykowo"
  ]
  node [
    id 252
    label "wypowied&#378;"
  ]
  node [
    id 253
    label "redakcja"
  ]
  node [
    id 254
    label "pomini&#281;cie"
  ]
  node [
    id 255
    label "dzie&#322;o"
  ]
  node [
    id 256
    label "preparacja"
  ]
  node [
    id 257
    label "odmianka"
  ]
  node [
    id 258
    label "opu&#347;ci&#263;"
  ]
  node [
    id 259
    label "koniektura"
  ]
  node [
    id 260
    label "pisa&#263;"
  ]
  node [
    id 261
    label "obelga"
  ]
  node [
    id 262
    label "uprawienie"
  ]
  node [
    id 263
    label "kszta&#322;t"
  ]
  node [
    id 264
    label "dialog"
  ]
  node [
    id 265
    label "p&#322;osa"
  ]
  node [
    id 266
    label "wykonywanie"
  ]
  node [
    id 267
    label "plik"
  ]
  node [
    id 268
    label "ziemia"
  ]
  node [
    id 269
    label "czyn"
  ]
  node [
    id 270
    label "ustawienie"
  ]
  node [
    id 271
    label "pole"
  ]
  node [
    id 272
    label "gospodarstwo"
  ]
  node [
    id 273
    label "uprawi&#263;"
  ]
  node [
    id 274
    label "function"
  ]
  node [
    id 275
    label "posta&#263;"
  ]
  node [
    id 276
    label "zreinterpretowa&#263;"
  ]
  node [
    id 277
    label "zastosowanie"
  ]
  node [
    id 278
    label "reinterpretowa&#263;"
  ]
  node [
    id 279
    label "wrench"
  ]
  node [
    id 280
    label "irygowanie"
  ]
  node [
    id 281
    label "ustawi&#263;"
  ]
  node [
    id 282
    label "irygowa&#263;"
  ]
  node [
    id 283
    label "zreinterpretowanie"
  ]
  node [
    id 284
    label "cel"
  ]
  node [
    id 285
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 286
    label "gra&#263;"
  ]
  node [
    id 287
    label "aktorstwo"
  ]
  node [
    id 288
    label "kostium"
  ]
  node [
    id 289
    label "zagon"
  ]
  node [
    id 290
    label "znaczenie"
  ]
  node [
    id 291
    label "zagra&#263;"
  ]
  node [
    id 292
    label "reinterpretowanie"
  ]
  node [
    id 293
    label "sk&#322;ad"
  ]
  node [
    id 294
    label "zagranie"
  ]
  node [
    id 295
    label "radlina"
  ]
  node [
    id 296
    label "granie"
  ]
  node [
    id 297
    label "rodzaj_literacki"
  ]
  node [
    id 298
    label "drama"
  ]
  node [
    id 299
    label "cios"
  ]
  node [
    id 300
    label "film"
  ]
  node [
    id 301
    label "didaskalia"
  ]
  node [
    id 302
    label "utw&#243;r"
  ]
  node [
    id 303
    label "literatura"
  ]
  node [
    id 304
    label "Faust"
  ]
  node [
    id 305
    label "przybli&#380;enie"
  ]
  node [
    id 306
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 307
    label "kategoria"
  ]
  node [
    id 308
    label "szpaler"
  ]
  node [
    id 309
    label "lon&#380;a"
  ]
  node [
    id 310
    label "uporz&#261;dkowanie"
  ]
  node [
    id 311
    label "egzekutywa"
  ]
  node [
    id 312
    label "jednostka_systematyczna"
  ]
  node [
    id 313
    label "instytucja"
  ]
  node [
    id 314
    label "premier"
  ]
  node [
    id 315
    label "Londyn"
  ]
  node [
    id 316
    label "gabinet_cieni"
  ]
  node [
    id 317
    label "gromada"
  ]
  node [
    id 318
    label "number"
  ]
  node [
    id 319
    label "Konsulat"
  ]
  node [
    id 320
    label "tract"
  ]
  node [
    id 321
    label "klasa"
  ]
  node [
    id 322
    label "w&#322;adza"
  ]
  node [
    id 323
    label "struktura"
  ]
  node [
    id 324
    label "ustalenie"
  ]
  node [
    id 325
    label "spowodowanie"
  ]
  node [
    id 326
    label "structure"
  ]
  node [
    id 327
    label "czynno&#347;&#263;"
  ]
  node [
    id 328
    label "sequence"
  ]
  node [
    id 329
    label "succession"
  ]
  node [
    id 330
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 331
    label "zapoznanie"
  ]
  node [
    id 332
    label "podanie"
  ]
  node [
    id 333
    label "bliski"
  ]
  node [
    id 334
    label "wyja&#347;nienie"
  ]
  node [
    id 335
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 336
    label "przemieszczenie"
  ]
  node [
    id 337
    label "approach"
  ]
  node [
    id 338
    label "pickup"
  ]
  node [
    id 339
    label "estimate"
  ]
  node [
    id 340
    label "po&#322;&#261;czenie"
  ]
  node [
    id 341
    label "ocena"
  ]
  node [
    id 342
    label "zbi&#243;r"
  ]
  node [
    id 343
    label "type"
  ]
  node [
    id 344
    label "poj&#281;cie"
  ]
  node [
    id 345
    label "teoria"
  ]
  node [
    id 346
    label "forma"
  ]
  node [
    id 347
    label "organ"
  ]
  node [
    id 348
    label "obrady"
  ]
  node [
    id 349
    label "executive"
  ]
  node [
    id 350
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 351
    label "partia"
  ]
  node [
    id 352
    label "federacja"
  ]
  node [
    id 353
    label "osoba_prawna"
  ]
  node [
    id 354
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 355
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 356
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 357
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 358
    label "biuro"
  ]
  node [
    id 359
    label "organizacja"
  ]
  node [
    id 360
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 361
    label "Fundusze_Unijne"
  ]
  node [
    id 362
    label "zamyka&#263;"
  ]
  node [
    id 363
    label "establishment"
  ]
  node [
    id 364
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 365
    label "urz&#261;d"
  ]
  node [
    id 366
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 367
    label "afiliowa&#263;"
  ]
  node [
    id 368
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 369
    label "standard"
  ]
  node [
    id 370
    label "zamykanie"
  ]
  node [
    id 371
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 372
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 373
    label "przej&#347;cie"
  ]
  node [
    id 374
    label "espalier"
  ]
  node [
    id 375
    label "aleja"
  ]
  node [
    id 376
    label "szyk"
  ]
  node [
    id 377
    label "wagon"
  ]
  node [
    id 378
    label "mecz_mistrzowski"
  ]
  node [
    id 379
    label "przedmiot"
  ]
  node [
    id 380
    label "arrangement"
  ]
  node [
    id 381
    label "class"
  ]
  node [
    id 382
    label "&#322;awka"
  ]
  node [
    id 383
    label "wykrzyknik"
  ]
  node [
    id 384
    label "zaleta"
  ]
  node [
    id 385
    label "programowanie_obiektowe"
  ]
  node [
    id 386
    label "tablica"
  ]
  node [
    id 387
    label "warstwa"
  ]
  node [
    id 388
    label "rezerwa"
  ]
  node [
    id 389
    label "Ekwici"
  ]
  node [
    id 390
    label "&#347;rodowisko"
  ]
  node [
    id 391
    label "szko&#322;a"
  ]
  node [
    id 392
    label "sala"
  ]
  node [
    id 393
    label "pomoc"
  ]
  node [
    id 394
    label "form"
  ]
  node [
    id 395
    label "grupa"
  ]
  node [
    id 396
    label "przepisa&#263;"
  ]
  node [
    id 397
    label "jako&#347;&#263;"
  ]
  node [
    id 398
    label "znak_jako&#347;ci"
  ]
  node [
    id 399
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 400
    label "poziom"
  ]
  node [
    id 401
    label "promocja"
  ]
  node [
    id 402
    label "przepisanie"
  ]
  node [
    id 403
    label "kurs"
  ]
  node [
    id 404
    label "obiekt"
  ]
  node [
    id 405
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 406
    label "dziennik_lekcyjny"
  ]
  node [
    id 407
    label "typ"
  ]
  node [
    id 408
    label "fakcja"
  ]
  node [
    id 409
    label "obrona"
  ]
  node [
    id 410
    label "atak"
  ]
  node [
    id 411
    label "botanika"
  ]
  node [
    id 412
    label "jednostka_administracyjna"
  ]
  node [
    id 413
    label "zoologia"
  ]
  node [
    id 414
    label "skupienie"
  ]
  node [
    id 415
    label "kr&#243;lestwo"
  ]
  node [
    id 416
    label "stage_set"
  ]
  node [
    id 417
    label "tribe"
  ]
  node [
    id 418
    label "hurma"
  ]
  node [
    id 419
    label "lina"
  ]
  node [
    id 420
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 421
    label "Bismarck"
  ]
  node [
    id 422
    label "zwierzchnik"
  ]
  node [
    id 423
    label "Sto&#322;ypin"
  ]
  node [
    id 424
    label "Miko&#322;ajczyk"
  ]
  node [
    id 425
    label "Chruszczow"
  ]
  node [
    id 426
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 427
    label "Jelcyn"
  ]
  node [
    id 428
    label "dostojnik"
  ]
  node [
    id 429
    label "prawo"
  ]
  node [
    id 430
    label "rz&#261;dzenie"
  ]
  node [
    id 431
    label "panowanie"
  ]
  node [
    id 432
    label "Kreml"
  ]
  node [
    id 433
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 434
    label "wydolno&#347;&#263;"
  ]
  node [
    id 435
    label "Wimbledon"
  ]
  node [
    id 436
    label "Westminster"
  ]
  node [
    id 437
    label "Londek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
]
