graph [
  node [
    id 0
    label "rocznik"
    origin "text"
  ]
  node [
    id 1
    label "formacja"
  ]
  node [
    id 2
    label "czasopismo"
  ]
  node [
    id 3
    label "kronika"
  ]
  node [
    id 4
    label "yearbook"
  ]
  node [
    id 5
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 6
    label "The_Beatles"
  ]
  node [
    id 7
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 8
    label "AWS"
  ]
  node [
    id 9
    label "partia"
  ]
  node [
    id 10
    label "Mazowsze"
  ]
  node [
    id 11
    label "forma"
  ]
  node [
    id 12
    label "ZChN"
  ]
  node [
    id 13
    label "Bund"
  ]
  node [
    id 14
    label "PPR"
  ]
  node [
    id 15
    label "blok"
  ]
  node [
    id 16
    label "egzekutywa"
  ]
  node [
    id 17
    label "Wigowie"
  ]
  node [
    id 18
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 19
    label "Razem"
  ]
  node [
    id 20
    label "unit"
  ]
  node [
    id 21
    label "SLD"
  ]
  node [
    id 22
    label "ZSL"
  ]
  node [
    id 23
    label "czynno&#347;&#263;"
  ]
  node [
    id 24
    label "szko&#322;a"
  ]
  node [
    id 25
    label "leksem"
  ]
  node [
    id 26
    label "posta&#263;"
  ]
  node [
    id 27
    label "Kuomintang"
  ]
  node [
    id 28
    label "si&#322;a"
  ]
  node [
    id 29
    label "PiS"
  ]
  node [
    id 30
    label "Depeche_Mode"
  ]
  node [
    id 31
    label "zjawisko"
  ]
  node [
    id 32
    label "Jakobici"
  ]
  node [
    id 33
    label "rugby"
  ]
  node [
    id 34
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 35
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 36
    label "organizacja"
  ]
  node [
    id 37
    label "PO"
  ]
  node [
    id 38
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 39
    label "jednostka"
  ]
  node [
    id 40
    label "proces"
  ]
  node [
    id 41
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 42
    label "Federali&#347;ci"
  ]
  node [
    id 43
    label "zespolik"
  ]
  node [
    id 44
    label "wojsko"
  ]
  node [
    id 45
    label "PSL"
  ]
  node [
    id 46
    label "zesp&#243;&#322;"
  ]
  node [
    id 47
    label "ksi&#281;ga"
  ]
  node [
    id 48
    label "chronograf"
  ]
  node [
    id 49
    label "zapis"
  ]
  node [
    id 50
    label "latopis"
  ]
  node [
    id 51
    label "ok&#322;adka"
  ]
  node [
    id 52
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 53
    label "prasa"
  ]
  node [
    id 54
    label "dzia&#322;"
  ]
  node [
    id 55
    label "zajawka"
  ]
  node [
    id 56
    label "psychotest"
  ]
  node [
    id 57
    label "wk&#322;ad"
  ]
  node [
    id 58
    label "communication"
  ]
  node [
    id 59
    label "Zwrotnica"
  ]
  node [
    id 60
    label "egzemplarz"
  ]
  node [
    id 61
    label "pismo"
  ]
  node [
    id 62
    label "rok"
  ]
  node [
    id 63
    label "33"
  ]
  node [
    id 64
    label "mig"
  ]
  node [
    id 65
    label "31"
  ]
  node [
    id 66
    label "by&#322;y"
  ]
  node [
    id 67
    label "1"
  ]
  node [
    id 68
    label "lancer"
  ]
  node [
    id 69
    label "senior"
  ]
  node [
    id 70
    label "71"
  ]
  node [
    id 71
    label "Blackbird"
  ]
  node [
    id 72
    label "52"
  ]
  node [
    id 73
    label "Stratofortress"
  ]
  node [
    id 74
    label "aa"
  ]
  node [
    id 75
    label "9"
  ]
  node [
    id 76
    label "Amos"
  ]
  node [
    id 77
    label "AIM"
  ]
  node [
    id 78
    label "54"
  ]
  node [
    id 79
    label "Phoenix"
  ]
  node [
    id 80
    label "40"
  ]
  node [
    id 81
    label "25"
  ]
  node [
    id 82
    label "XB"
  ]
  node [
    id 83
    label "70"
  ]
  node [
    id 84
    label "Valkyrie"
  ]
  node [
    id 85
    label "GosMBK"
  ]
  node [
    id 86
    label "Wympie&#322;"
  ]
  node [
    id 87
    label "&#1043;&#1086;&#1089;&#1052;&#1050;&#1041;"
  ]
  node [
    id 88
    label "&#171;"
  ]
  node [
    id 89
    label "&#1042;&#1099;&#1084;&#1087;&#1077;&#1083;"
  ]
  node [
    id 90
    label "&#187;"
  ]
  node [
    id 91
    label "e"
  ]
  node [
    id 92
    label "155MP"
  ]
  node [
    id 93
    label "ko&#322;o"
  ]
  node [
    id 94
    label "23"
  ]
  node [
    id 95
    label "13"
  ]
  node [
    id 96
    label "tu"
  ]
  node [
    id 97
    label "104"
  ]
  node [
    id 98
    label "ZBI"
  ]
  node [
    id 99
    label "16"
  ]
  node [
    id 100
    label "zas&#322;ona"
  ]
  node [
    id 101
    label "21"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 80
  ]
  edge [
    source 63
    target 93
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 81
  ]
  edge [
    source 64
    target 101
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 68
  ]
  edge [
    source 66
    target 72
  ]
  edge [
    source 66
    target 73
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 76
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 79
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 89
  ]
  edge [
    source 87
    target 90
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 95
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 100
  ]
  edge [
    source 99
    target 100
  ]
]
