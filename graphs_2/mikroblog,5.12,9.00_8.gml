graph [
  node [
    id 0
    label "powiat"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
  ]
  node [
    id 2
    label "gmina"
  ]
  node [
    id 3
    label "jednostka_administracyjna"
  ]
  node [
    id 4
    label "urz&#261;d"
  ]
  node [
    id 5
    label "Karlsbad"
  ]
  node [
    id 6
    label "Dobro&#324;"
  ]
  node [
    id 7
    label "rada_gminy"
  ]
  node [
    id 8
    label "Wielka_Wie&#347;"
  ]
  node [
    id 9
    label "radny"
  ]
  node [
    id 10
    label "organizacja_religijna"
  ]
  node [
    id 11
    label "Biskupice"
  ]
  node [
    id 12
    label "mikroregion"
  ]
  node [
    id 13
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 14
    label "pa&#324;stwo"
  ]
  node [
    id 15
    label "makroregion"
  ]
  node [
    id 16
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
]
