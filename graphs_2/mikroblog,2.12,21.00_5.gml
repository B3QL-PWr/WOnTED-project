graph [
  node [
    id 0
    label "wysoce"
    origin "text"
  ]
  node [
    id 1
    label "plusowany"
    origin "text"
  ]
  node [
    id 2
    label "obrazek"
    origin "text"
  ]
  node [
    id 3
    label "wygrywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wysoki"
  ]
  node [
    id 5
    label "intensywnie"
  ]
  node [
    id 6
    label "wielki"
  ]
  node [
    id 7
    label "intensywny"
  ]
  node [
    id 8
    label "g&#281;sto"
  ]
  node [
    id 9
    label "dynamicznie"
  ]
  node [
    id 10
    label "znaczny"
  ]
  node [
    id 11
    label "wyj&#261;tkowy"
  ]
  node [
    id 12
    label "nieprzeci&#281;tny"
  ]
  node [
    id 13
    label "wa&#380;ny"
  ]
  node [
    id 14
    label "prawdziwy"
  ]
  node [
    id 15
    label "wybitny"
  ]
  node [
    id 16
    label "dupny"
  ]
  node [
    id 17
    label "wyrafinowany"
  ]
  node [
    id 18
    label "niepo&#347;ledni"
  ]
  node [
    id 19
    label "du&#380;y"
  ]
  node [
    id 20
    label "chwalebny"
  ]
  node [
    id 21
    label "z_wysoka"
  ]
  node [
    id 22
    label "wznios&#322;y"
  ]
  node [
    id 23
    label "daleki"
  ]
  node [
    id 24
    label "szczytnie"
  ]
  node [
    id 25
    label "warto&#347;ciowy"
  ]
  node [
    id 26
    label "wysoko"
  ]
  node [
    id 27
    label "uprzywilejowany"
  ]
  node [
    id 28
    label "druk_ulotny"
  ]
  node [
    id 29
    label "rysunek"
  ]
  node [
    id 30
    label "opowiadanie"
  ]
  node [
    id 31
    label "picture"
  ]
  node [
    id 32
    label "kreska"
  ]
  node [
    id 33
    label "kszta&#322;t"
  ]
  node [
    id 34
    label "teka"
  ]
  node [
    id 35
    label "photograph"
  ]
  node [
    id 36
    label "ilustracja"
  ]
  node [
    id 37
    label "grafika"
  ]
  node [
    id 38
    label "plastyka"
  ]
  node [
    id 39
    label "shape"
  ]
  node [
    id 40
    label "follow-up"
  ]
  node [
    id 41
    label "rozpowiadanie"
  ]
  node [
    id 42
    label "wypowied&#378;"
  ]
  node [
    id 43
    label "report"
  ]
  node [
    id 44
    label "spalenie"
  ]
  node [
    id 45
    label "podbarwianie"
  ]
  node [
    id 46
    label "przedstawianie"
  ]
  node [
    id 47
    label "story"
  ]
  node [
    id 48
    label "rozpowiedzenie"
  ]
  node [
    id 49
    label "proza"
  ]
  node [
    id 50
    label "prawienie"
  ]
  node [
    id 51
    label "utw&#243;r_epicki"
  ]
  node [
    id 52
    label "fabu&#322;a"
  ]
  node [
    id 53
    label "strike"
  ]
  node [
    id 54
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 55
    label "robi&#263;"
  ]
  node [
    id 56
    label "muzykowa&#263;"
  ]
  node [
    id 57
    label "mie&#263;_miejsce"
  ]
  node [
    id 58
    label "play"
  ]
  node [
    id 59
    label "znosi&#263;"
  ]
  node [
    id 60
    label "zagwarantowywa&#263;"
  ]
  node [
    id 61
    label "osi&#261;ga&#263;"
  ]
  node [
    id 62
    label "gra&#263;"
  ]
  node [
    id 63
    label "net_income"
  ]
  node [
    id 64
    label "instrument_muzyczny"
  ]
  node [
    id 65
    label "organizowa&#263;"
  ]
  node [
    id 66
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 67
    label "czyni&#263;"
  ]
  node [
    id 68
    label "give"
  ]
  node [
    id 69
    label "stylizowa&#263;"
  ]
  node [
    id 70
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 71
    label "falowa&#263;"
  ]
  node [
    id 72
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 73
    label "peddle"
  ]
  node [
    id 74
    label "praca"
  ]
  node [
    id 75
    label "wydala&#263;"
  ]
  node [
    id 76
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 77
    label "tentegowa&#263;"
  ]
  node [
    id 78
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 79
    label "urz&#261;dza&#263;"
  ]
  node [
    id 80
    label "oszukiwa&#263;"
  ]
  node [
    id 81
    label "work"
  ]
  node [
    id 82
    label "ukazywa&#263;"
  ]
  node [
    id 83
    label "przerabia&#263;"
  ]
  node [
    id 84
    label "act"
  ]
  node [
    id 85
    label "post&#281;powa&#263;"
  ]
  node [
    id 86
    label "zapewnia&#263;"
  ]
  node [
    id 87
    label "guarantee"
  ]
  node [
    id 88
    label "&#347;wieci&#263;"
  ]
  node [
    id 89
    label "majaczy&#263;"
  ]
  node [
    id 90
    label "szczeka&#263;"
  ]
  node [
    id 91
    label "wykonywa&#263;"
  ]
  node [
    id 92
    label "napierdziela&#263;"
  ]
  node [
    id 93
    label "dzia&#322;a&#263;"
  ]
  node [
    id 94
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 95
    label "pasowa&#263;"
  ]
  node [
    id 96
    label "sound"
  ]
  node [
    id 97
    label "dally"
  ]
  node [
    id 98
    label "i&#347;&#263;"
  ]
  node [
    id 99
    label "stara&#263;_si&#281;"
  ]
  node [
    id 100
    label "tokowa&#263;"
  ]
  node [
    id 101
    label "wida&#263;"
  ]
  node [
    id 102
    label "prezentowa&#263;"
  ]
  node [
    id 103
    label "rozgrywa&#263;"
  ]
  node [
    id 104
    label "do"
  ]
  node [
    id 105
    label "brzmie&#263;"
  ]
  node [
    id 106
    label "wykorzystywa&#263;"
  ]
  node [
    id 107
    label "cope"
  ]
  node [
    id 108
    label "otwarcie"
  ]
  node [
    id 109
    label "typify"
  ]
  node [
    id 110
    label "przedstawia&#263;"
  ]
  node [
    id 111
    label "rola"
  ]
  node [
    id 112
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 113
    label "uzyskiwa&#263;"
  ]
  node [
    id 114
    label "dociera&#263;"
  ]
  node [
    id 115
    label "mark"
  ]
  node [
    id 116
    label "get"
  ]
  node [
    id 117
    label "gromadzi&#263;"
  ]
  node [
    id 118
    label "usuwa&#263;"
  ]
  node [
    id 119
    label "porywa&#263;"
  ]
  node [
    id 120
    label "sk&#322;ada&#263;"
  ]
  node [
    id 121
    label "ranny"
  ]
  node [
    id 122
    label "zbiera&#263;"
  ]
  node [
    id 123
    label "behave"
  ]
  node [
    id 124
    label "carry"
  ]
  node [
    id 125
    label "represent"
  ]
  node [
    id 126
    label "podrze&#263;"
  ]
  node [
    id 127
    label "przenosi&#263;"
  ]
  node [
    id 128
    label "str&#243;j"
  ]
  node [
    id 129
    label "wytrzymywa&#263;"
  ]
  node [
    id 130
    label "seclude"
  ]
  node [
    id 131
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 132
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 133
    label "set"
  ]
  node [
    id 134
    label "zu&#380;y&#263;"
  ]
  node [
    id 135
    label "niszczy&#263;"
  ]
  node [
    id 136
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 137
    label "tolerowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
]
