graph [
  node [
    id 0
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;ama&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 6
    label "przyzwoity"
  ]
  node [
    id 7
    label "ciekawy"
  ]
  node [
    id 8
    label "jako&#347;"
  ]
  node [
    id 9
    label "jako_tako"
  ]
  node [
    id 10
    label "niez&#322;y"
  ]
  node [
    id 11
    label "dziwny"
  ]
  node [
    id 12
    label "charakterystyczny"
  ]
  node [
    id 13
    label "intensywny"
  ]
  node [
    id 14
    label "udolny"
  ]
  node [
    id 15
    label "skuteczny"
  ]
  node [
    id 16
    label "&#347;mieszny"
  ]
  node [
    id 17
    label "niczegowaty"
  ]
  node [
    id 18
    label "dobrze"
  ]
  node [
    id 19
    label "nieszpetny"
  ]
  node [
    id 20
    label "spory"
  ]
  node [
    id 21
    label "pozytywny"
  ]
  node [
    id 22
    label "korzystny"
  ]
  node [
    id 23
    label "nie&#378;le"
  ]
  node [
    id 24
    label "skromny"
  ]
  node [
    id 25
    label "kulturalny"
  ]
  node [
    id 26
    label "grzeczny"
  ]
  node [
    id 27
    label "stosowny"
  ]
  node [
    id 28
    label "przystojny"
  ]
  node [
    id 29
    label "nale&#380;yty"
  ]
  node [
    id 30
    label "moralny"
  ]
  node [
    id 31
    label "przyzwoicie"
  ]
  node [
    id 32
    label "wystarczaj&#261;cy"
  ]
  node [
    id 33
    label "nietuzinkowy"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "intryguj&#261;cy"
  ]
  node [
    id 36
    label "ch&#281;tny"
  ]
  node [
    id 37
    label "swoisty"
  ]
  node [
    id 38
    label "interesowanie"
  ]
  node [
    id 39
    label "interesuj&#261;cy"
  ]
  node [
    id 40
    label "ciekawie"
  ]
  node [
    id 41
    label "indagator"
  ]
  node [
    id 42
    label "charakterystycznie"
  ]
  node [
    id 43
    label "szczeg&#243;lny"
  ]
  node [
    id 44
    label "wyj&#261;tkowy"
  ]
  node [
    id 45
    label "typowy"
  ]
  node [
    id 46
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 47
    label "podobny"
  ]
  node [
    id 48
    label "dziwnie"
  ]
  node [
    id 49
    label "dziwy"
  ]
  node [
    id 50
    label "inny"
  ]
  node [
    id 51
    label "w_miar&#281;"
  ]
  node [
    id 52
    label "jako_taki"
  ]
  node [
    id 53
    label "poprzedzanie"
  ]
  node [
    id 54
    label "czasoprzestrze&#324;"
  ]
  node [
    id 55
    label "laba"
  ]
  node [
    id 56
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 57
    label "chronometria"
  ]
  node [
    id 58
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 59
    label "rachuba_czasu"
  ]
  node [
    id 60
    label "przep&#322;ywanie"
  ]
  node [
    id 61
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 62
    label "czasokres"
  ]
  node [
    id 63
    label "odczyt"
  ]
  node [
    id 64
    label "chwila"
  ]
  node [
    id 65
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 66
    label "dzieje"
  ]
  node [
    id 67
    label "kategoria_gramatyczna"
  ]
  node [
    id 68
    label "poprzedzenie"
  ]
  node [
    id 69
    label "trawienie"
  ]
  node [
    id 70
    label "pochodzi&#263;"
  ]
  node [
    id 71
    label "period"
  ]
  node [
    id 72
    label "okres_czasu"
  ]
  node [
    id 73
    label "poprzedza&#263;"
  ]
  node [
    id 74
    label "schy&#322;ek"
  ]
  node [
    id 75
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 76
    label "odwlekanie_si&#281;"
  ]
  node [
    id 77
    label "zegar"
  ]
  node [
    id 78
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 79
    label "czwarty_wymiar"
  ]
  node [
    id 80
    label "pochodzenie"
  ]
  node [
    id 81
    label "koniugacja"
  ]
  node [
    id 82
    label "Zeitgeist"
  ]
  node [
    id 83
    label "trawi&#263;"
  ]
  node [
    id 84
    label "pogoda"
  ]
  node [
    id 85
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 86
    label "poprzedzi&#263;"
  ]
  node [
    id 87
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 88
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 89
    label "time_period"
  ]
  node [
    id 90
    label "time"
  ]
  node [
    id 91
    label "blok"
  ]
  node [
    id 92
    label "handout"
  ]
  node [
    id 93
    label "pomiar"
  ]
  node [
    id 94
    label "lecture"
  ]
  node [
    id 95
    label "reading"
  ]
  node [
    id 96
    label "podawanie"
  ]
  node [
    id 97
    label "wyk&#322;ad"
  ]
  node [
    id 98
    label "potrzyma&#263;"
  ]
  node [
    id 99
    label "warunki"
  ]
  node [
    id 100
    label "pok&#243;j"
  ]
  node [
    id 101
    label "atak"
  ]
  node [
    id 102
    label "program"
  ]
  node [
    id 103
    label "zjawisko"
  ]
  node [
    id 104
    label "meteorology"
  ]
  node [
    id 105
    label "weather"
  ]
  node [
    id 106
    label "prognoza_meteorologiczna"
  ]
  node [
    id 107
    label "czas_wolny"
  ]
  node [
    id 108
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 109
    label "metrologia"
  ]
  node [
    id 110
    label "godzinnik"
  ]
  node [
    id 111
    label "bicie"
  ]
  node [
    id 112
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 113
    label "wahad&#322;o"
  ]
  node [
    id 114
    label "kurant"
  ]
  node [
    id 115
    label "cyferblat"
  ]
  node [
    id 116
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 117
    label "nabicie"
  ]
  node [
    id 118
    label "werk"
  ]
  node [
    id 119
    label "czasomierz"
  ]
  node [
    id 120
    label "tyka&#263;"
  ]
  node [
    id 121
    label "tykn&#261;&#263;"
  ]
  node [
    id 122
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 123
    label "urz&#261;dzenie"
  ]
  node [
    id 124
    label "kotwica"
  ]
  node [
    id 125
    label "fleksja"
  ]
  node [
    id 126
    label "liczba"
  ]
  node [
    id 127
    label "coupling"
  ]
  node [
    id 128
    label "osoba"
  ]
  node [
    id 129
    label "tryb"
  ]
  node [
    id 130
    label "czasownik"
  ]
  node [
    id 131
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 132
    label "orz&#281;sek"
  ]
  node [
    id 133
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 134
    label "zaczynanie_si&#281;"
  ]
  node [
    id 135
    label "str&#243;j"
  ]
  node [
    id 136
    label "wynikanie"
  ]
  node [
    id 137
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 138
    label "origin"
  ]
  node [
    id 139
    label "background"
  ]
  node [
    id 140
    label "geneza"
  ]
  node [
    id 141
    label "beginning"
  ]
  node [
    id 142
    label "digestion"
  ]
  node [
    id 143
    label "unicestwianie"
  ]
  node [
    id 144
    label "sp&#281;dzanie"
  ]
  node [
    id 145
    label "contemplation"
  ]
  node [
    id 146
    label "rozk&#322;adanie"
  ]
  node [
    id 147
    label "marnowanie"
  ]
  node [
    id 148
    label "proces_fizjologiczny"
  ]
  node [
    id 149
    label "przetrawianie"
  ]
  node [
    id 150
    label "perystaltyka"
  ]
  node [
    id 151
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 152
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 153
    label "przebywa&#263;"
  ]
  node [
    id 154
    label "pour"
  ]
  node [
    id 155
    label "carry"
  ]
  node [
    id 156
    label "sail"
  ]
  node [
    id 157
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 158
    label "go&#347;ci&#263;"
  ]
  node [
    id 159
    label "mija&#263;"
  ]
  node [
    id 160
    label "proceed"
  ]
  node [
    id 161
    label "odej&#347;cie"
  ]
  node [
    id 162
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 163
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 164
    label "zanikni&#281;cie"
  ]
  node [
    id 165
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 166
    label "ciecz"
  ]
  node [
    id 167
    label "opuszczenie"
  ]
  node [
    id 168
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 169
    label "departure"
  ]
  node [
    id 170
    label "oddalenie_si&#281;"
  ]
  node [
    id 171
    label "przeby&#263;"
  ]
  node [
    id 172
    label "min&#261;&#263;"
  ]
  node [
    id 173
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 174
    label "swimming"
  ]
  node [
    id 175
    label "zago&#347;ci&#263;"
  ]
  node [
    id 176
    label "cross"
  ]
  node [
    id 177
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 178
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 179
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 180
    label "zrobi&#263;"
  ]
  node [
    id 181
    label "opatrzy&#263;"
  ]
  node [
    id 182
    label "overwhelm"
  ]
  node [
    id 183
    label "opatrywa&#263;"
  ]
  node [
    id 184
    label "date"
  ]
  node [
    id 185
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 186
    label "wynika&#263;"
  ]
  node [
    id 187
    label "fall"
  ]
  node [
    id 188
    label "poby&#263;"
  ]
  node [
    id 189
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 190
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 191
    label "bolt"
  ]
  node [
    id 192
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 193
    label "spowodowa&#263;"
  ]
  node [
    id 194
    label "uda&#263;_si&#281;"
  ]
  node [
    id 195
    label "opatrzenie"
  ]
  node [
    id 196
    label "zdarzenie_si&#281;"
  ]
  node [
    id 197
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 198
    label "progress"
  ]
  node [
    id 199
    label "opatrywanie"
  ]
  node [
    id 200
    label "mini&#281;cie"
  ]
  node [
    id 201
    label "doznanie"
  ]
  node [
    id 202
    label "zaistnienie"
  ]
  node [
    id 203
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 204
    label "przebycie"
  ]
  node [
    id 205
    label "cruise"
  ]
  node [
    id 206
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 207
    label "usuwa&#263;"
  ]
  node [
    id 208
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 209
    label "lutowa&#263;"
  ]
  node [
    id 210
    label "marnowa&#263;"
  ]
  node [
    id 211
    label "przetrawia&#263;"
  ]
  node [
    id 212
    label "poch&#322;ania&#263;"
  ]
  node [
    id 213
    label "digest"
  ]
  node [
    id 214
    label "metal"
  ]
  node [
    id 215
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 216
    label "sp&#281;dza&#263;"
  ]
  node [
    id 217
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 218
    label "zjawianie_si&#281;"
  ]
  node [
    id 219
    label "przebywanie"
  ]
  node [
    id 220
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 221
    label "mijanie"
  ]
  node [
    id 222
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 223
    label "zaznawanie"
  ]
  node [
    id 224
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 225
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 226
    label "flux"
  ]
  node [
    id 227
    label "epoka"
  ]
  node [
    id 228
    label "charakter"
  ]
  node [
    id 229
    label "flow"
  ]
  node [
    id 230
    label "choroba_przyrodzona"
  ]
  node [
    id 231
    label "ciota"
  ]
  node [
    id 232
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 233
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 234
    label "kres"
  ]
  node [
    id 235
    label "przestrze&#324;"
  ]
  node [
    id 236
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 237
    label "adjustment"
  ]
  node [
    id 238
    label "panowanie"
  ]
  node [
    id 239
    label "animation"
  ]
  node [
    id 240
    label "kwadrat"
  ]
  node [
    id 241
    label "stanie"
  ]
  node [
    id 242
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 243
    label "pomieszkanie"
  ]
  node [
    id 244
    label "lokal"
  ]
  node [
    id 245
    label "dom"
  ]
  node [
    id 246
    label "zajmowanie"
  ]
  node [
    id 247
    label "sprawowanie"
  ]
  node [
    id 248
    label "bycie"
  ]
  node [
    id 249
    label "kierowanie"
  ]
  node [
    id 250
    label "w&#322;adca"
  ]
  node [
    id 251
    label "dominowanie"
  ]
  node [
    id 252
    label "przewaga"
  ]
  node [
    id 253
    label "przewa&#380;anie"
  ]
  node [
    id 254
    label "znaczenie"
  ]
  node [
    id 255
    label "laterality"
  ]
  node [
    id 256
    label "control"
  ]
  node [
    id 257
    label "temper"
  ]
  node [
    id 258
    label "kontrolowanie"
  ]
  node [
    id 259
    label "dominance"
  ]
  node [
    id 260
    label "rule"
  ]
  node [
    id 261
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 262
    label "prym"
  ]
  node [
    id 263
    label "w&#322;adza"
  ]
  node [
    id 264
    label "ocieranie_si&#281;"
  ]
  node [
    id 265
    label "otoczenie_si&#281;"
  ]
  node [
    id 266
    label "posiedzenie"
  ]
  node [
    id 267
    label "otarcie_si&#281;"
  ]
  node [
    id 268
    label "atakowanie"
  ]
  node [
    id 269
    label "otaczanie_si&#281;"
  ]
  node [
    id 270
    label "wyj&#347;cie"
  ]
  node [
    id 271
    label "zmierzanie"
  ]
  node [
    id 272
    label "residency"
  ]
  node [
    id 273
    label "sojourn"
  ]
  node [
    id 274
    label "wychodzenie"
  ]
  node [
    id 275
    label "tkwienie"
  ]
  node [
    id 276
    label "powodowanie"
  ]
  node [
    id 277
    label "lokowanie_si&#281;"
  ]
  node [
    id 278
    label "schorzenie"
  ]
  node [
    id 279
    label "zajmowanie_si&#281;"
  ]
  node [
    id 280
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 281
    label "stosowanie"
  ]
  node [
    id 282
    label "anektowanie"
  ]
  node [
    id 283
    label "zabieranie"
  ]
  node [
    id 284
    label "robienie"
  ]
  node [
    id 285
    label "sytuowanie_si&#281;"
  ]
  node [
    id 286
    label "wype&#322;nianie"
  ]
  node [
    id 287
    label "obejmowanie"
  ]
  node [
    id 288
    label "klasyfikacja"
  ]
  node [
    id 289
    label "czynno&#347;&#263;"
  ]
  node [
    id 290
    label "dzianie_si&#281;"
  ]
  node [
    id 291
    label "branie"
  ]
  node [
    id 292
    label "rz&#261;dzenie"
  ]
  node [
    id 293
    label "occupation"
  ]
  node [
    id 294
    label "zadawanie"
  ]
  node [
    id 295
    label "zaj&#281;ty"
  ]
  node [
    id 296
    label "miejsce"
  ]
  node [
    id 297
    label "gastronomia"
  ]
  node [
    id 298
    label "zak&#322;ad"
  ]
  node [
    id 299
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 300
    label "rodzina"
  ]
  node [
    id 301
    label "substancja_mieszkaniowa"
  ]
  node [
    id 302
    label "instytucja"
  ]
  node [
    id 303
    label "siedziba"
  ]
  node [
    id 304
    label "dom_rodzinny"
  ]
  node [
    id 305
    label "budynek"
  ]
  node [
    id 306
    label "grupa"
  ]
  node [
    id 307
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 308
    label "poj&#281;cie"
  ]
  node [
    id 309
    label "stead"
  ]
  node [
    id 310
    label "garderoba"
  ]
  node [
    id 311
    label "wiecha"
  ]
  node [
    id 312
    label "fratria"
  ]
  node [
    id 313
    label "trwanie"
  ]
  node [
    id 314
    label "ustanie"
  ]
  node [
    id 315
    label "wystanie"
  ]
  node [
    id 316
    label "postanie"
  ]
  node [
    id 317
    label "wystawanie"
  ]
  node [
    id 318
    label "kosztowanie"
  ]
  node [
    id 319
    label "przestanie"
  ]
  node [
    id 320
    label "pot&#281;ga"
  ]
  node [
    id 321
    label "wielok&#261;t_foremny"
  ]
  node [
    id 322
    label "stopie&#324;_pisma"
  ]
  node [
    id 323
    label "prostok&#261;t"
  ]
  node [
    id 324
    label "square"
  ]
  node [
    id 325
    label "romb"
  ]
  node [
    id 326
    label "justunek"
  ]
  node [
    id 327
    label "dzielnica"
  ]
  node [
    id 328
    label "poletko"
  ]
  node [
    id 329
    label "ekologia"
  ]
  node [
    id 330
    label "tango"
  ]
  node [
    id 331
    label "figura_taneczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
]
