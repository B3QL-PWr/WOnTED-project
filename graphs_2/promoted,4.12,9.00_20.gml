graph [
  node [
    id 0
    label "zaginiona"
    origin "text"
  ]
  node [
    id 1
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 2
    label "letni"
    origin "text"
  ]
  node [
    id 3
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 4
    label "po&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nad"
    origin "text"
  ]
  node [
    id 6
    label "ren"
    origin "text"
  ]
  node [
    id 7
    label "miasto"
    origin "text"
  ]
  node [
    id 8
    label "unkel"
    origin "text"
  ]
  node [
    id 9
    label "niemiecki"
    origin "text"
  ]
  node [
    id 10
    label "nadrenia"
    origin "text"
  ]
  node [
    id 11
    label "palatynat"
    origin "text"
  ]
  node [
    id 12
    label "pad&#322;o"
    origin "text"
  ]
  node [
    id 13
    label "ofiara"
    origin "text"
  ]
  node [
    id 14
    label "zab&#243;jstwo"
    origin "text"
  ]
  node [
    id 15
    label "tydzie&#324;"
  ]
  node [
    id 16
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 17
    label "dzie&#324;_powszedni"
  ]
  node [
    id 18
    label "doba"
  ]
  node [
    id 19
    label "weekend"
  ]
  node [
    id 20
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 21
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "miesi&#261;c"
  ]
  node [
    id 24
    label "latowy"
  ]
  node [
    id 25
    label "typowy"
  ]
  node [
    id 26
    label "weso&#322;y"
  ]
  node [
    id 27
    label "s&#322;oneczny"
  ]
  node [
    id 28
    label "sezonowy"
  ]
  node [
    id 29
    label "ciep&#322;y"
  ]
  node [
    id 30
    label "letnio"
  ]
  node [
    id 31
    label "oboj&#281;tny"
  ]
  node [
    id 32
    label "nijaki"
  ]
  node [
    id 33
    label "nijak"
  ]
  node [
    id 34
    label "niezabawny"
  ]
  node [
    id 35
    label "&#380;aden"
  ]
  node [
    id 36
    label "zwyczajny"
  ]
  node [
    id 37
    label "poszarzenie"
  ]
  node [
    id 38
    label "neutralny"
  ]
  node [
    id 39
    label "szarzenie"
  ]
  node [
    id 40
    label "bezbarwnie"
  ]
  node [
    id 41
    label "nieciekawy"
  ]
  node [
    id 42
    label "czasowy"
  ]
  node [
    id 43
    label "sezonowo"
  ]
  node [
    id 44
    label "zoboj&#281;tnienie"
  ]
  node [
    id 45
    label "nieszkodliwy"
  ]
  node [
    id 46
    label "&#347;ni&#281;ty"
  ]
  node [
    id 47
    label "oboj&#281;tnie"
  ]
  node [
    id 48
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 49
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 50
    label "niewa&#380;ny"
  ]
  node [
    id 51
    label "neutralizowanie"
  ]
  node [
    id 52
    label "bierny"
  ]
  node [
    id 53
    label "zneutralizowanie"
  ]
  node [
    id 54
    label "pijany"
  ]
  node [
    id 55
    label "weso&#322;o"
  ]
  node [
    id 56
    label "pozytywny"
  ]
  node [
    id 57
    label "beztroski"
  ]
  node [
    id 58
    label "dobry"
  ]
  node [
    id 59
    label "mi&#322;y"
  ]
  node [
    id 60
    label "ocieplanie_si&#281;"
  ]
  node [
    id 61
    label "ocieplanie"
  ]
  node [
    id 62
    label "grzanie"
  ]
  node [
    id 63
    label "ocieplenie_si&#281;"
  ]
  node [
    id 64
    label "zagrzanie"
  ]
  node [
    id 65
    label "ocieplenie"
  ]
  node [
    id 66
    label "korzystny"
  ]
  node [
    id 67
    label "przyjemny"
  ]
  node [
    id 68
    label "ciep&#322;o"
  ]
  node [
    id 69
    label "s&#322;onecznie"
  ]
  node [
    id 70
    label "bezdeszczowy"
  ]
  node [
    id 71
    label "bezchmurny"
  ]
  node [
    id 72
    label "pogodny"
  ]
  node [
    id 73
    label "fotowoltaiczny"
  ]
  node [
    id 74
    label "jasny"
  ]
  node [
    id 75
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 76
    label "typowo"
  ]
  node [
    id 77
    label "cz&#281;sty"
  ]
  node [
    id 78
    label "zwyk&#322;y"
  ]
  node [
    id 79
    label "dziewka"
  ]
  node [
    id 80
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 81
    label "sikorka"
  ]
  node [
    id 82
    label "kora"
  ]
  node [
    id 83
    label "cz&#322;owiek"
  ]
  node [
    id 84
    label "dziewcz&#281;"
  ]
  node [
    id 85
    label "m&#322;&#243;dka"
  ]
  node [
    id 86
    label "dziecina"
  ]
  node [
    id 87
    label "sympatia"
  ]
  node [
    id 88
    label "dziunia"
  ]
  node [
    id 89
    label "dziewczynina"
  ]
  node [
    id 90
    label "partnerka"
  ]
  node [
    id 91
    label "siksa"
  ]
  node [
    id 92
    label "dziewoja"
  ]
  node [
    id 93
    label "aktorka"
  ]
  node [
    id 94
    label "kobieta"
  ]
  node [
    id 95
    label "partner"
  ]
  node [
    id 96
    label "kobita"
  ]
  node [
    id 97
    label "emocja"
  ]
  node [
    id 98
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 99
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 100
    label "love"
  ]
  node [
    id 101
    label "ludzko&#347;&#263;"
  ]
  node [
    id 102
    label "asymilowanie"
  ]
  node [
    id 103
    label "wapniak"
  ]
  node [
    id 104
    label "asymilowa&#263;"
  ]
  node [
    id 105
    label "os&#322;abia&#263;"
  ]
  node [
    id 106
    label "posta&#263;"
  ]
  node [
    id 107
    label "hominid"
  ]
  node [
    id 108
    label "podw&#322;adny"
  ]
  node [
    id 109
    label "os&#322;abianie"
  ]
  node [
    id 110
    label "g&#322;owa"
  ]
  node [
    id 111
    label "figura"
  ]
  node [
    id 112
    label "portrecista"
  ]
  node [
    id 113
    label "dwun&#243;g"
  ]
  node [
    id 114
    label "profanum"
  ]
  node [
    id 115
    label "mikrokosmos"
  ]
  node [
    id 116
    label "nasada"
  ]
  node [
    id 117
    label "duch"
  ]
  node [
    id 118
    label "antropochoria"
  ]
  node [
    id 119
    label "osoba"
  ]
  node [
    id 120
    label "wz&#243;r"
  ]
  node [
    id 121
    label "senior"
  ]
  node [
    id 122
    label "oddzia&#322;ywanie"
  ]
  node [
    id 123
    label "Adam"
  ]
  node [
    id 124
    label "homo_sapiens"
  ]
  node [
    id 125
    label "polifag"
  ]
  node [
    id 126
    label "s&#322;u&#380;ba"
  ]
  node [
    id 127
    label "ochmistrzyni"
  ]
  node [
    id 128
    label "s&#322;u&#380;ebnica"
  ]
  node [
    id 129
    label "laska"
  ]
  node [
    id 130
    label "sikora"
  ]
  node [
    id 131
    label "panna"
  ]
  node [
    id 132
    label "prostytutka"
  ]
  node [
    id 133
    label "ma&#322;olata"
  ]
  node [
    id 134
    label "crust"
  ]
  node [
    id 135
    label "ciasto"
  ]
  node [
    id 136
    label "szabla"
  ]
  node [
    id 137
    label "drzewko"
  ]
  node [
    id 138
    label "drzewo"
  ]
  node [
    id 139
    label "po&#347;ciel&#243;wka"
  ]
  node [
    id 140
    label "harfa"
  ]
  node [
    id 141
    label "bawe&#322;na"
  ]
  node [
    id 142
    label "tkanka_sta&#322;a"
  ]
  node [
    id 143
    label "piskl&#281;"
  ]
  node [
    id 144
    label "samica"
  ]
  node [
    id 145
    label "ptak"
  ]
  node [
    id 146
    label "upierzenie"
  ]
  node [
    id 147
    label "m&#322;odzie&#380;"
  ]
  node [
    id 148
    label "mo&#322;odyca"
  ]
  node [
    id 149
    label "dziecko"
  ]
  node [
    id 150
    label "zwrot"
  ]
  node [
    id 151
    label "wear"
  ]
  node [
    id 152
    label "return"
  ]
  node [
    id 153
    label "plant"
  ]
  node [
    id 154
    label "pozostawi&#263;"
  ]
  node [
    id 155
    label "pokry&#263;"
  ]
  node [
    id 156
    label "znak"
  ]
  node [
    id 157
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 158
    label "przygotowa&#263;"
  ]
  node [
    id 159
    label "stagger"
  ]
  node [
    id 160
    label "zepsu&#263;"
  ]
  node [
    id 161
    label "zmieni&#263;"
  ]
  node [
    id 162
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 163
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 164
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 165
    label "umie&#347;ci&#263;"
  ]
  node [
    id 166
    label "zacz&#261;&#263;"
  ]
  node [
    id 167
    label "raise"
  ]
  node [
    id 168
    label "wygra&#263;"
  ]
  node [
    id 169
    label "doprowadzi&#263;"
  ]
  node [
    id 170
    label "drop"
  ]
  node [
    id 171
    label "skrzywdzi&#263;"
  ]
  node [
    id 172
    label "shove"
  ]
  node [
    id 173
    label "wyda&#263;"
  ]
  node [
    id 174
    label "zaplanowa&#263;"
  ]
  node [
    id 175
    label "shelve"
  ]
  node [
    id 176
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 177
    label "zachowa&#263;"
  ]
  node [
    id 178
    label "impart"
  ]
  node [
    id 179
    label "da&#263;"
  ]
  node [
    id 180
    label "zrobi&#263;"
  ]
  node [
    id 181
    label "wyznaczy&#263;"
  ]
  node [
    id 182
    label "liszy&#263;"
  ]
  node [
    id 183
    label "zerwa&#263;"
  ]
  node [
    id 184
    label "spowodowa&#263;"
  ]
  node [
    id 185
    label "release"
  ]
  node [
    id 186
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 187
    label "przekaza&#263;"
  ]
  node [
    id 188
    label "stworzy&#263;"
  ]
  node [
    id 189
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 190
    label "zabra&#263;"
  ]
  node [
    id 191
    label "zrezygnowa&#263;"
  ]
  node [
    id 192
    label "permit"
  ]
  node [
    id 193
    label "set"
  ]
  node [
    id 194
    label "put"
  ]
  node [
    id 195
    label "uplasowa&#263;"
  ]
  node [
    id 196
    label "wpierniczy&#263;"
  ]
  node [
    id 197
    label "okre&#347;li&#263;"
  ]
  node [
    id 198
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 199
    label "umieszcza&#263;"
  ]
  node [
    id 200
    label "favor"
  ]
  node [
    id 201
    label "potraktowa&#263;"
  ]
  node [
    id 202
    label "nagrodzi&#263;"
  ]
  node [
    id 203
    label "work"
  ]
  node [
    id 204
    label "chemia"
  ]
  node [
    id 205
    label "reakcja_chemiczna"
  ]
  node [
    id 206
    label "act"
  ]
  node [
    id 207
    label "sprawi&#263;"
  ]
  node [
    id 208
    label "change"
  ]
  node [
    id 209
    label "zast&#261;pi&#263;"
  ]
  node [
    id 210
    label "come_up"
  ]
  node [
    id 211
    label "przej&#347;&#263;"
  ]
  node [
    id 212
    label "straci&#263;"
  ]
  node [
    id 213
    label "zyska&#263;"
  ]
  node [
    id 214
    label "powodowa&#263;"
  ]
  node [
    id 215
    label "draw"
  ]
  node [
    id 216
    label "zagwarantowa&#263;"
  ]
  node [
    id 217
    label "znie&#347;&#263;"
  ]
  node [
    id 218
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 219
    label "zagra&#263;"
  ]
  node [
    id 220
    label "score"
  ]
  node [
    id 221
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 222
    label "zwojowa&#263;"
  ]
  node [
    id 223
    label "leave"
  ]
  node [
    id 224
    label "net_income"
  ]
  node [
    id 225
    label "instrument_muzyczny"
  ]
  node [
    id 226
    label "zaszkodzi&#263;"
  ]
  node [
    id 227
    label "zjeba&#263;"
  ]
  node [
    id 228
    label "pogorszy&#263;"
  ]
  node [
    id 229
    label "drop_the_ball"
  ]
  node [
    id 230
    label "zdemoralizowa&#263;"
  ]
  node [
    id 231
    label "uszkodzi&#263;"
  ]
  node [
    id 232
    label "wypierdoli&#263;_si&#281;"
  ]
  node [
    id 233
    label "damage"
  ]
  node [
    id 234
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 235
    label "spapra&#263;"
  ]
  node [
    id 236
    label "skopa&#263;"
  ]
  node [
    id 237
    label "zap&#322;odni&#263;"
  ]
  node [
    id 238
    label "cover"
  ]
  node [
    id 239
    label "przykry&#263;"
  ]
  node [
    id 240
    label "sheathing"
  ]
  node [
    id 241
    label "brood"
  ]
  node [
    id 242
    label "zaj&#261;&#263;"
  ]
  node [
    id 243
    label "zap&#322;aci&#263;"
  ]
  node [
    id 244
    label "ustawi&#263;_si&#281;"
  ]
  node [
    id 245
    label "zamaskowa&#263;"
  ]
  node [
    id 246
    label "zaspokoi&#263;"
  ]
  node [
    id 247
    label "defray"
  ]
  node [
    id 248
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 249
    label "wykona&#263;"
  ]
  node [
    id 250
    label "cook"
  ]
  node [
    id 251
    label "wyszkoli&#263;"
  ]
  node [
    id 252
    label "train"
  ]
  node [
    id 253
    label "arrange"
  ]
  node [
    id 254
    label "wytworzy&#263;"
  ]
  node [
    id 255
    label "dress"
  ]
  node [
    id 256
    label "ukierunkowa&#263;"
  ]
  node [
    id 257
    label "post&#261;pi&#263;"
  ]
  node [
    id 258
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 259
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 260
    label "odj&#261;&#263;"
  ]
  node [
    id 261
    label "cause"
  ]
  node [
    id 262
    label "introduce"
  ]
  node [
    id 263
    label "begin"
  ]
  node [
    id 264
    label "do"
  ]
  node [
    id 265
    label "dow&#243;d"
  ]
  node [
    id 266
    label "oznakowanie"
  ]
  node [
    id 267
    label "fakt"
  ]
  node [
    id 268
    label "stawia&#263;"
  ]
  node [
    id 269
    label "wytw&#243;r"
  ]
  node [
    id 270
    label "point"
  ]
  node [
    id 271
    label "kodzik"
  ]
  node [
    id 272
    label "postawi&#263;"
  ]
  node [
    id 273
    label "mark"
  ]
  node [
    id 274
    label "herb"
  ]
  node [
    id 275
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 276
    label "attribute"
  ]
  node [
    id 277
    label "implikowa&#263;"
  ]
  node [
    id 278
    label "bacteriophage"
  ]
  node [
    id 279
    label "metal_szlachetny"
  ]
  node [
    id 280
    label "caribou"
  ]
  node [
    id 281
    label "katalizator"
  ]
  node [
    id 282
    label "manganowiec"
  ]
  node [
    id 283
    label "jeleniowate"
  ]
  node [
    id 284
    label "prze&#380;uwacz"
  ]
  node [
    id 285
    label "metal"
  ]
  node [
    id 286
    label "pierwiastek"
  ]
  node [
    id 287
    label "catalyst"
  ]
  node [
    id 288
    label "substancja"
  ]
  node [
    id 289
    label "czynnik"
  ]
  node [
    id 290
    label "fotokataliza"
  ]
  node [
    id 291
    label "ssak_parzystokopytny"
  ]
  node [
    id 292
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 293
    label "przed&#380;o&#322;&#261;dek"
  ]
  node [
    id 294
    label "prze&#380;uwacze"
  ]
  node [
    id 295
    label "Brunszwik"
  ]
  node [
    id 296
    label "Twer"
  ]
  node [
    id 297
    label "Marki"
  ]
  node [
    id 298
    label "Tarnopol"
  ]
  node [
    id 299
    label "Czerkiesk"
  ]
  node [
    id 300
    label "Johannesburg"
  ]
  node [
    id 301
    label "Nowogr&#243;d"
  ]
  node [
    id 302
    label "Heidelberg"
  ]
  node [
    id 303
    label "Korsze"
  ]
  node [
    id 304
    label "Chocim"
  ]
  node [
    id 305
    label "Lenzen"
  ]
  node [
    id 306
    label "Bie&#322;gorod"
  ]
  node [
    id 307
    label "Hebron"
  ]
  node [
    id 308
    label "Korynt"
  ]
  node [
    id 309
    label "Pemba"
  ]
  node [
    id 310
    label "Norfolk"
  ]
  node [
    id 311
    label "Tarragona"
  ]
  node [
    id 312
    label "Loreto"
  ]
  node [
    id 313
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 314
    label "Paczk&#243;w"
  ]
  node [
    id 315
    label "Krasnodar"
  ]
  node [
    id 316
    label "Hadziacz"
  ]
  node [
    id 317
    label "Cymlansk"
  ]
  node [
    id 318
    label "Efez"
  ]
  node [
    id 319
    label "Kandahar"
  ]
  node [
    id 320
    label "&#346;wiebodzice"
  ]
  node [
    id 321
    label "Antwerpia"
  ]
  node [
    id 322
    label "Baltimore"
  ]
  node [
    id 323
    label "Eger"
  ]
  node [
    id 324
    label "Cumana"
  ]
  node [
    id 325
    label "Kanton"
  ]
  node [
    id 326
    label "Sarat&#243;w"
  ]
  node [
    id 327
    label "Siena"
  ]
  node [
    id 328
    label "Dubno"
  ]
  node [
    id 329
    label "Tyl&#380;a"
  ]
  node [
    id 330
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 331
    label "Pi&#324;sk"
  ]
  node [
    id 332
    label "Toledo"
  ]
  node [
    id 333
    label "Piza"
  ]
  node [
    id 334
    label "Triest"
  ]
  node [
    id 335
    label "Struga"
  ]
  node [
    id 336
    label "Gettysburg"
  ]
  node [
    id 337
    label "Sierdobsk"
  ]
  node [
    id 338
    label "Xai-Xai"
  ]
  node [
    id 339
    label "Bristol"
  ]
  node [
    id 340
    label "Katania"
  ]
  node [
    id 341
    label "Parma"
  ]
  node [
    id 342
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 343
    label "Dniepropetrowsk"
  ]
  node [
    id 344
    label "Tours"
  ]
  node [
    id 345
    label "Mohylew"
  ]
  node [
    id 346
    label "Suzdal"
  ]
  node [
    id 347
    label "Samara"
  ]
  node [
    id 348
    label "Akerman"
  ]
  node [
    id 349
    label "Szk&#322;&#243;w"
  ]
  node [
    id 350
    label "Chimoio"
  ]
  node [
    id 351
    label "Perm"
  ]
  node [
    id 352
    label "Murma&#324;sk"
  ]
  node [
    id 353
    label "Z&#322;oczew"
  ]
  node [
    id 354
    label "Reda"
  ]
  node [
    id 355
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 356
    label "Kowel"
  ]
  node [
    id 357
    label "Aleksandria"
  ]
  node [
    id 358
    label "Hamburg"
  ]
  node [
    id 359
    label "Rudki"
  ]
  node [
    id 360
    label "O&#322;omuniec"
  ]
  node [
    id 361
    label "Luksor"
  ]
  node [
    id 362
    label "Kowno"
  ]
  node [
    id 363
    label "Cremona"
  ]
  node [
    id 364
    label "Suczawa"
  ]
  node [
    id 365
    label "M&#252;nster"
  ]
  node [
    id 366
    label "Peszawar"
  ]
  node [
    id 367
    label "Los_Angeles"
  ]
  node [
    id 368
    label "Szawle"
  ]
  node [
    id 369
    label "Winnica"
  ]
  node [
    id 370
    label "I&#322;awka"
  ]
  node [
    id 371
    label "Poniatowa"
  ]
  node [
    id 372
    label "Ko&#322;omyja"
  ]
  node [
    id 373
    label "Asy&#380;"
  ]
  node [
    id 374
    label "Tolkmicko"
  ]
  node [
    id 375
    label "Orlean"
  ]
  node [
    id 376
    label "Koper"
  ]
  node [
    id 377
    label "Le&#324;sk"
  ]
  node [
    id 378
    label "Rostock"
  ]
  node [
    id 379
    label "Mantua"
  ]
  node [
    id 380
    label "Barcelona"
  ]
  node [
    id 381
    label "Mo&#347;ciska"
  ]
  node [
    id 382
    label "Koluszki"
  ]
  node [
    id 383
    label "Stalingrad"
  ]
  node [
    id 384
    label "Fergana"
  ]
  node [
    id 385
    label "A&#322;czewsk"
  ]
  node [
    id 386
    label "Kaszyn"
  ]
  node [
    id 387
    label "D&#252;sseldorf"
  ]
  node [
    id 388
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 389
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 390
    label "Mozyrz"
  ]
  node [
    id 391
    label "Syrakuzy"
  ]
  node [
    id 392
    label "Peszt"
  ]
  node [
    id 393
    label "Lichinga"
  ]
  node [
    id 394
    label "Choroszcz"
  ]
  node [
    id 395
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 396
    label "Po&#322;ock"
  ]
  node [
    id 397
    label "Cherso&#324;"
  ]
  node [
    id 398
    label "Fryburg"
  ]
  node [
    id 399
    label "Izmir"
  ]
  node [
    id 400
    label "Jawor&#243;w"
  ]
  node [
    id 401
    label "Wenecja"
  ]
  node [
    id 402
    label "Mrocza"
  ]
  node [
    id 403
    label "Kordoba"
  ]
  node [
    id 404
    label "Solikamsk"
  ]
  node [
    id 405
    label "Be&#322;z"
  ]
  node [
    id 406
    label "Wo&#322;gograd"
  ]
  node [
    id 407
    label "&#379;ar&#243;w"
  ]
  node [
    id 408
    label "Brugia"
  ]
  node [
    id 409
    label "Radk&#243;w"
  ]
  node [
    id 410
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 411
    label "Harbin"
  ]
  node [
    id 412
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 413
    label "Zaporo&#380;e"
  ]
  node [
    id 414
    label "Smorgonie"
  ]
  node [
    id 415
    label "Nowa_D&#281;ba"
  ]
  node [
    id 416
    label "Aktobe"
  ]
  node [
    id 417
    label "Ussuryjsk"
  ]
  node [
    id 418
    label "Mo&#380;ajsk"
  ]
  node [
    id 419
    label "Tanger"
  ]
  node [
    id 420
    label "Nowogard"
  ]
  node [
    id 421
    label "Utrecht"
  ]
  node [
    id 422
    label "Czerniejewo"
  ]
  node [
    id 423
    label "Bazylea"
  ]
  node [
    id 424
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 425
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 426
    label "Tu&#322;a"
  ]
  node [
    id 427
    label "Al-Kufa"
  ]
  node [
    id 428
    label "Jutrosin"
  ]
  node [
    id 429
    label "Czelabi&#324;sk"
  ]
  node [
    id 430
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 431
    label "Split"
  ]
  node [
    id 432
    label "Czerniowce"
  ]
  node [
    id 433
    label "Majsur"
  ]
  node [
    id 434
    label "Poczdam"
  ]
  node [
    id 435
    label "Troick"
  ]
  node [
    id 436
    label "Kostroma"
  ]
  node [
    id 437
    label "Minusi&#324;sk"
  ]
  node [
    id 438
    label "Barwice"
  ]
  node [
    id 439
    label "U&#322;an_Ude"
  ]
  node [
    id 440
    label "Czeskie_Budziejowice"
  ]
  node [
    id 441
    label "Getynga"
  ]
  node [
    id 442
    label "Kercz"
  ]
  node [
    id 443
    label "B&#322;aszki"
  ]
  node [
    id 444
    label "Lipawa"
  ]
  node [
    id 445
    label "Bujnaksk"
  ]
  node [
    id 446
    label "Wittenberga"
  ]
  node [
    id 447
    label "Gorycja"
  ]
  node [
    id 448
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 449
    label "Swatowe"
  ]
  node [
    id 450
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 451
    label "Magadan"
  ]
  node [
    id 452
    label "Rzg&#243;w"
  ]
  node [
    id 453
    label "Bijsk"
  ]
  node [
    id 454
    label "Norylsk"
  ]
  node [
    id 455
    label "Mesyna"
  ]
  node [
    id 456
    label "Berezyna"
  ]
  node [
    id 457
    label "Stawropol"
  ]
  node [
    id 458
    label "Kircholm"
  ]
  node [
    id 459
    label "Hawana"
  ]
  node [
    id 460
    label "Pardubice"
  ]
  node [
    id 461
    label "Drezno"
  ]
  node [
    id 462
    label "Zaklik&#243;w"
  ]
  node [
    id 463
    label "Kozielsk"
  ]
  node [
    id 464
    label "Paw&#322;owo"
  ]
  node [
    id 465
    label "Kani&#243;w"
  ]
  node [
    id 466
    label "Adana"
  ]
  node [
    id 467
    label "Rybi&#324;sk"
  ]
  node [
    id 468
    label "Kleczew"
  ]
  node [
    id 469
    label "Dayton"
  ]
  node [
    id 470
    label "Nowy_Orlean"
  ]
  node [
    id 471
    label "Perejas&#322;aw"
  ]
  node [
    id 472
    label "Jenisejsk"
  ]
  node [
    id 473
    label "Bolonia"
  ]
  node [
    id 474
    label "Marsylia"
  ]
  node [
    id 475
    label "Bir&#380;e"
  ]
  node [
    id 476
    label "Workuta"
  ]
  node [
    id 477
    label "Sewilla"
  ]
  node [
    id 478
    label "Megara"
  ]
  node [
    id 479
    label "Gotha"
  ]
  node [
    id 480
    label "Kiejdany"
  ]
  node [
    id 481
    label "Zaleszczyki"
  ]
  node [
    id 482
    label "Ja&#322;ta"
  ]
  node [
    id 483
    label "Burgas"
  ]
  node [
    id 484
    label "Essen"
  ]
  node [
    id 485
    label "Czadca"
  ]
  node [
    id 486
    label "Manchester"
  ]
  node [
    id 487
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 488
    label "Schmalkalden"
  ]
  node [
    id 489
    label "Oleszyce"
  ]
  node [
    id 490
    label "Kie&#380;mark"
  ]
  node [
    id 491
    label "Kleck"
  ]
  node [
    id 492
    label "Suez"
  ]
  node [
    id 493
    label "Brack"
  ]
  node [
    id 494
    label "Symferopol"
  ]
  node [
    id 495
    label "Michalovce"
  ]
  node [
    id 496
    label "Tambow"
  ]
  node [
    id 497
    label "Turkmenbaszy"
  ]
  node [
    id 498
    label "Bogumin"
  ]
  node [
    id 499
    label "Sambor"
  ]
  node [
    id 500
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 501
    label "Milan&#243;wek"
  ]
  node [
    id 502
    label "Nachiczewan"
  ]
  node [
    id 503
    label "Cluny"
  ]
  node [
    id 504
    label "Stalinogorsk"
  ]
  node [
    id 505
    label "Lipsk"
  ]
  node [
    id 506
    label "Karlsbad"
  ]
  node [
    id 507
    label "Pietrozawodsk"
  ]
  node [
    id 508
    label "Bar"
  ]
  node [
    id 509
    label "Korfant&#243;w"
  ]
  node [
    id 510
    label "Nieftiegorsk"
  ]
  node [
    id 511
    label "Hanower"
  ]
  node [
    id 512
    label "Windawa"
  ]
  node [
    id 513
    label "&#346;niatyn"
  ]
  node [
    id 514
    label "Dalton"
  ]
  node [
    id 515
    label "tramwaj"
  ]
  node [
    id 516
    label "Kaszgar"
  ]
  node [
    id 517
    label "Berdia&#324;sk"
  ]
  node [
    id 518
    label "Koprzywnica"
  ]
  node [
    id 519
    label "Brno"
  ]
  node [
    id 520
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 521
    label "Wia&#378;ma"
  ]
  node [
    id 522
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 523
    label "Starobielsk"
  ]
  node [
    id 524
    label "Ostr&#243;g"
  ]
  node [
    id 525
    label "Oran"
  ]
  node [
    id 526
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 527
    label "Wyszehrad"
  ]
  node [
    id 528
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 529
    label "Trembowla"
  ]
  node [
    id 530
    label "Tobolsk"
  ]
  node [
    id 531
    label "Liberec"
  ]
  node [
    id 532
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 533
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 534
    label "G&#322;uszyca"
  ]
  node [
    id 535
    label "Akwileja"
  ]
  node [
    id 536
    label "Kar&#322;owice"
  ]
  node [
    id 537
    label "Borys&#243;w"
  ]
  node [
    id 538
    label "Stryj"
  ]
  node [
    id 539
    label "Czeski_Cieszyn"
  ]
  node [
    id 540
    label "Opawa"
  ]
  node [
    id 541
    label "Darmstadt"
  ]
  node [
    id 542
    label "Rydu&#322;towy"
  ]
  node [
    id 543
    label "Jerycho"
  ]
  node [
    id 544
    label "&#321;ohojsk"
  ]
  node [
    id 545
    label "Fatima"
  ]
  node [
    id 546
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 547
    label "Sara&#324;sk"
  ]
  node [
    id 548
    label "Lyon"
  ]
  node [
    id 549
    label "Wormacja"
  ]
  node [
    id 550
    label "Perwomajsk"
  ]
  node [
    id 551
    label "Lubeka"
  ]
  node [
    id 552
    label "Sura&#380;"
  ]
  node [
    id 553
    label "Karaganda"
  ]
  node [
    id 554
    label "Nazaret"
  ]
  node [
    id 555
    label "Poniewie&#380;"
  ]
  node [
    id 556
    label "Siewieromorsk"
  ]
  node [
    id 557
    label "Greifswald"
  ]
  node [
    id 558
    label "Nitra"
  ]
  node [
    id 559
    label "Trewir"
  ]
  node [
    id 560
    label "Karwina"
  ]
  node [
    id 561
    label "Houston"
  ]
  node [
    id 562
    label "Demmin"
  ]
  node [
    id 563
    label "Peczora"
  ]
  node [
    id 564
    label "Szamocin"
  ]
  node [
    id 565
    label "Kolkata"
  ]
  node [
    id 566
    label "Brasz&#243;w"
  ]
  node [
    id 567
    label "&#321;uck"
  ]
  node [
    id 568
    label "S&#322;onim"
  ]
  node [
    id 569
    label "Mekka"
  ]
  node [
    id 570
    label "Rzeczyca"
  ]
  node [
    id 571
    label "Konstancja"
  ]
  node [
    id 572
    label "Orenburg"
  ]
  node [
    id 573
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 574
    label "Pittsburgh"
  ]
  node [
    id 575
    label "Barabi&#324;sk"
  ]
  node [
    id 576
    label "Mory&#324;"
  ]
  node [
    id 577
    label "Hallstatt"
  ]
  node [
    id 578
    label "Mannheim"
  ]
  node [
    id 579
    label "Tarent"
  ]
  node [
    id 580
    label "Dortmund"
  ]
  node [
    id 581
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 582
    label "Dodona"
  ]
  node [
    id 583
    label "Trojan"
  ]
  node [
    id 584
    label "Nankin"
  ]
  node [
    id 585
    label "Weimar"
  ]
  node [
    id 586
    label "Brac&#322;aw"
  ]
  node [
    id 587
    label "Izbica_Kujawska"
  ]
  node [
    id 588
    label "&#321;uga&#324;sk"
  ]
  node [
    id 589
    label "Sewastopol"
  ]
  node [
    id 590
    label "Sankt_Florian"
  ]
  node [
    id 591
    label "Pilzno"
  ]
  node [
    id 592
    label "Poczaj&#243;w"
  ]
  node [
    id 593
    label "Sulech&#243;w"
  ]
  node [
    id 594
    label "Pas&#322;&#281;k"
  ]
  node [
    id 595
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 596
    label "ulica"
  ]
  node [
    id 597
    label "Norak"
  ]
  node [
    id 598
    label "Filadelfia"
  ]
  node [
    id 599
    label "Maribor"
  ]
  node [
    id 600
    label "Detroit"
  ]
  node [
    id 601
    label "Bobolice"
  ]
  node [
    id 602
    label "K&#322;odawa"
  ]
  node [
    id 603
    label "Radziech&#243;w"
  ]
  node [
    id 604
    label "Eleusis"
  ]
  node [
    id 605
    label "W&#322;odzimierz"
  ]
  node [
    id 606
    label "Tartu"
  ]
  node [
    id 607
    label "Drohobycz"
  ]
  node [
    id 608
    label "Saloniki"
  ]
  node [
    id 609
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 610
    label "Buchara"
  ]
  node [
    id 611
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 612
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 613
    label "P&#322;owdiw"
  ]
  node [
    id 614
    label "Koszyce"
  ]
  node [
    id 615
    label "Brema"
  ]
  node [
    id 616
    label "Wagram"
  ]
  node [
    id 617
    label "Czarnobyl"
  ]
  node [
    id 618
    label "Brze&#347;&#263;"
  ]
  node [
    id 619
    label "S&#232;vres"
  ]
  node [
    id 620
    label "Dubrownik"
  ]
  node [
    id 621
    label "Grenada"
  ]
  node [
    id 622
    label "Jekaterynburg"
  ]
  node [
    id 623
    label "zabudowa"
  ]
  node [
    id 624
    label "Inhambane"
  ]
  node [
    id 625
    label "Konstantyn&#243;wka"
  ]
  node [
    id 626
    label "Krajowa"
  ]
  node [
    id 627
    label "Norymberga"
  ]
  node [
    id 628
    label "Tarnogr&#243;d"
  ]
  node [
    id 629
    label "Beresteczko"
  ]
  node [
    id 630
    label "Chabarowsk"
  ]
  node [
    id 631
    label "Boden"
  ]
  node [
    id 632
    label "Bamberg"
  ]
  node [
    id 633
    label "Lhasa"
  ]
  node [
    id 634
    label "Podhajce"
  ]
  node [
    id 635
    label "Oszmiana"
  ]
  node [
    id 636
    label "Narbona"
  ]
  node [
    id 637
    label "Carrara"
  ]
  node [
    id 638
    label "Gandawa"
  ]
  node [
    id 639
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 640
    label "Malin"
  ]
  node [
    id 641
    label "Soleczniki"
  ]
  node [
    id 642
    label "burmistrz"
  ]
  node [
    id 643
    label "Lancaster"
  ]
  node [
    id 644
    label "S&#322;uck"
  ]
  node [
    id 645
    label "Kronsztad"
  ]
  node [
    id 646
    label "Mosty"
  ]
  node [
    id 647
    label "Budionnowsk"
  ]
  node [
    id 648
    label "Oksford"
  ]
  node [
    id 649
    label "Awinion"
  ]
  node [
    id 650
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 651
    label "Edynburg"
  ]
  node [
    id 652
    label "Kaspijsk"
  ]
  node [
    id 653
    label "Zagorsk"
  ]
  node [
    id 654
    label "Konotop"
  ]
  node [
    id 655
    label "Nantes"
  ]
  node [
    id 656
    label "Sydney"
  ]
  node [
    id 657
    label "Orsza"
  ]
  node [
    id 658
    label "Krzanowice"
  ]
  node [
    id 659
    label "Tiume&#324;"
  ]
  node [
    id 660
    label "Wyborg"
  ]
  node [
    id 661
    label "Nerczy&#324;sk"
  ]
  node [
    id 662
    label "Rost&#243;w"
  ]
  node [
    id 663
    label "Halicz"
  ]
  node [
    id 664
    label "Sumy"
  ]
  node [
    id 665
    label "Locarno"
  ]
  node [
    id 666
    label "Luboml"
  ]
  node [
    id 667
    label "Mariupol"
  ]
  node [
    id 668
    label "Bras&#322;aw"
  ]
  node [
    id 669
    label "Orneta"
  ]
  node [
    id 670
    label "Witnica"
  ]
  node [
    id 671
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 672
    label "Gr&#243;dek"
  ]
  node [
    id 673
    label "Go&#347;cino"
  ]
  node [
    id 674
    label "Cannes"
  ]
  node [
    id 675
    label "Lw&#243;w"
  ]
  node [
    id 676
    label "Ulm"
  ]
  node [
    id 677
    label "Aczy&#324;sk"
  ]
  node [
    id 678
    label "Stuttgart"
  ]
  node [
    id 679
    label "weduta"
  ]
  node [
    id 680
    label "Borowsk"
  ]
  node [
    id 681
    label "Niko&#322;ajewsk"
  ]
  node [
    id 682
    label "Worone&#380;"
  ]
  node [
    id 683
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 684
    label "Delhi"
  ]
  node [
    id 685
    label "Adrianopol"
  ]
  node [
    id 686
    label "Byczyna"
  ]
  node [
    id 687
    label "Obuch&#243;w"
  ]
  node [
    id 688
    label "Tyraspol"
  ]
  node [
    id 689
    label "Modena"
  ]
  node [
    id 690
    label "Rajgr&#243;d"
  ]
  node [
    id 691
    label "Wo&#322;kowysk"
  ]
  node [
    id 692
    label "&#379;ylina"
  ]
  node [
    id 693
    label "Zurych"
  ]
  node [
    id 694
    label "Vukovar"
  ]
  node [
    id 695
    label "Narwa"
  ]
  node [
    id 696
    label "Neapol"
  ]
  node [
    id 697
    label "Frydek-Mistek"
  ]
  node [
    id 698
    label "W&#322;adywostok"
  ]
  node [
    id 699
    label "Calais"
  ]
  node [
    id 700
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 701
    label "Trydent"
  ]
  node [
    id 702
    label "Magnitogorsk"
  ]
  node [
    id 703
    label "Padwa"
  ]
  node [
    id 704
    label "Isfahan"
  ]
  node [
    id 705
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 706
    label "grupa"
  ]
  node [
    id 707
    label "Marburg"
  ]
  node [
    id 708
    label "Homel"
  ]
  node [
    id 709
    label "Boston"
  ]
  node [
    id 710
    label "W&#252;rzburg"
  ]
  node [
    id 711
    label "Antiochia"
  ]
  node [
    id 712
    label "Wotki&#324;sk"
  ]
  node [
    id 713
    label "A&#322;apajewsk"
  ]
  node [
    id 714
    label "Nieder_Selters"
  ]
  node [
    id 715
    label "Lejda"
  ]
  node [
    id 716
    label "Nicea"
  ]
  node [
    id 717
    label "Dmitrow"
  ]
  node [
    id 718
    label "Taganrog"
  ]
  node [
    id 719
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 720
    label "Nowomoskowsk"
  ]
  node [
    id 721
    label "Koby&#322;ka"
  ]
  node [
    id 722
    label "Iwano-Frankowsk"
  ]
  node [
    id 723
    label "Kis&#322;owodzk"
  ]
  node [
    id 724
    label "Tomsk"
  ]
  node [
    id 725
    label "Ferrara"
  ]
  node [
    id 726
    label "Turka"
  ]
  node [
    id 727
    label "Edam"
  ]
  node [
    id 728
    label "Suworow"
  ]
  node [
    id 729
    label "Aralsk"
  ]
  node [
    id 730
    label "Kobry&#324;"
  ]
  node [
    id 731
    label "Rotterdam"
  ]
  node [
    id 732
    label "L&#252;neburg"
  ]
  node [
    id 733
    label "Bordeaux"
  ]
  node [
    id 734
    label "Akwizgran"
  ]
  node [
    id 735
    label "Liverpool"
  ]
  node [
    id 736
    label "Asuan"
  ]
  node [
    id 737
    label "Bonn"
  ]
  node [
    id 738
    label "Szumsk"
  ]
  node [
    id 739
    label "Teby"
  ]
  node [
    id 740
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 741
    label "Ku&#378;nieck"
  ]
  node [
    id 742
    label "Tyberiada"
  ]
  node [
    id 743
    label "Turkiestan"
  ]
  node [
    id 744
    label "Nanning"
  ]
  node [
    id 745
    label "G&#322;uch&#243;w"
  ]
  node [
    id 746
    label "Bajonna"
  ]
  node [
    id 747
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 748
    label "Orze&#322;"
  ]
  node [
    id 749
    label "Opalenica"
  ]
  node [
    id 750
    label "Buczacz"
  ]
  node [
    id 751
    label "Armenia"
  ]
  node [
    id 752
    label "Nowoku&#378;nieck"
  ]
  node [
    id 753
    label "Wuppertal"
  ]
  node [
    id 754
    label "Wuhan"
  ]
  node [
    id 755
    label "Betlejem"
  ]
  node [
    id 756
    label "Wi&#322;komierz"
  ]
  node [
    id 757
    label "Podiebrady"
  ]
  node [
    id 758
    label "Rawenna"
  ]
  node [
    id 759
    label "Haarlem"
  ]
  node [
    id 760
    label "Woskriesiensk"
  ]
  node [
    id 761
    label "Pyskowice"
  ]
  node [
    id 762
    label "Kilonia"
  ]
  node [
    id 763
    label "Ruciane-Nida"
  ]
  node [
    id 764
    label "Kursk"
  ]
  node [
    id 765
    label "Stralsund"
  ]
  node [
    id 766
    label "Wolgast"
  ]
  node [
    id 767
    label "Sydon"
  ]
  node [
    id 768
    label "Natal"
  ]
  node [
    id 769
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 770
    label "Stara_Zagora"
  ]
  node [
    id 771
    label "Baranowicze"
  ]
  node [
    id 772
    label "Regensburg"
  ]
  node [
    id 773
    label "Kapsztad"
  ]
  node [
    id 774
    label "Kemerowo"
  ]
  node [
    id 775
    label "Mi&#347;nia"
  ]
  node [
    id 776
    label "Stary_Sambor"
  ]
  node [
    id 777
    label "Soligorsk"
  ]
  node [
    id 778
    label "Ostaszk&#243;w"
  ]
  node [
    id 779
    label "T&#322;uszcz"
  ]
  node [
    id 780
    label "Uljanowsk"
  ]
  node [
    id 781
    label "Tuluza"
  ]
  node [
    id 782
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 783
    label "Chicago"
  ]
  node [
    id 784
    label "Kamieniec_Podolski"
  ]
  node [
    id 785
    label "Dijon"
  ]
  node [
    id 786
    label "Siedliszcze"
  ]
  node [
    id 787
    label "Haga"
  ]
  node [
    id 788
    label "Bobrujsk"
  ]
  node [
    id 789
    label "Windsor"
  ]
  node [
    id 790
    label "Kokand"
  ]
  node [
    id 791
    label "Chmielnicki"
  ]
  node [
    id 792
    label "Winchester"
  ]
  node [
    id 793
    label "Bria&#324;sk"
  ]
  node [
    id 794
    label "Uppsala"
  ]
  node [
    id 795
    label "Paw&#322;odar"
  ]
  node [
    id 796
    label "Omsk"
  ]
  node [
    id 797
    label "Canterbury"
  ]
  node [
    id 798
    label "Tyr"
  ]
  node [
    id 799
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 800
    label "Kolonia"
  ]
  node [
    id 801
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 802
    label "Nowa_Ruda"
  ]
  node [
    id 803
    label "Czerkasy"
  ]
  node [
    id 804
    label "Budziszyn"
  ]
  node [
    id 805
    label "Rohatyn"
  ]
  node [
    id 806
    label "Nowogr&#243;dek"
  ]
  node [
    id 807
    label "Buda"
  ]
  node [
    id 808
    label "Zbara&#380;"
  ]
  node [
    id 809
    label "Korzec"
  ]
  node [
    id 810
    label "Medyna"
  ]
  node [
    id 811
    label "Piatigorsk"
  ]
  node [
    id 812
    label "Monako"
  ]
  node [
    id 813
    label "Chark&#243;w"
  ]
  node [
    id 814
    label "Zadar"
  ]
  node [
    id 815
    label "Brandenburg"
  ]
  node [
    id 816
    label "&#379;ytawa"
  ]
  node [
    id 817
    label "Konstantynopol"
  ]
  node [
    id 818
    label "Wismar"
  ]
  node [
    id 819
    label "Wielsk"
  ]
  node [
    id 820
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 821
    label "Genewa"
  ]
  node [
    id 822
    label "Lozanna"
  ]
  node [
    id 823
    label "Merseburg"
  ]
  node [
    id 824
    label "Azow"
  ]
  node [
    id 825
    label "K&#322;ajpeda"
  ]
  node [
    id 826
    label "Angarsk"
  ]
  node [
    id 827
    label "Ostrawa"
  ]
  node [
    id 828
    label "Jastarnia"
  ]
  node [
    id 829
    label "Moguncja"
  ]
  node [
    id 830
    label "Siewsk"
  ]
  node [
    id 831
    label "Pasawa"
  ]
  node [
    id 832
    label "Penza"
  ]
  node [
    id 833
    label "Borys&#322;aw"
  ]
  node [
    id 834
    label "Osaka"
  ]
  node [
    id 835
    label "Eupatoria"
  ]
  node [
    id 836
    label "Kalmar"
  ]
  node [
    id 837
    label "Troki"
  ]
  node [
    id 838
    label "Mosina"
  ]
  node [
    id 839
    label "Zas&#322;aw"
  ]
  node [
    id 840
    label "Orany"
  ]
  node [
    id 841
    label "Dobrodzie&#324;"
  ]
  node [
    id 842
    label "Kars"
  ]
  node [
    id 843
    label "Poprad"
  ]
  node [
    id 844
    label "Sajgon"
  ]
  node [
    id 845
    label "Tulon"
  ]
  node [
    id 846
    label "Kro&#347;niewice"
  ]
  node [
    id 847
    label "Krzywi&#324;"
  ]
  node [
    id 848
    label "Batumi"
  ]
  node [
    id 849
    label "Werona"
  ]
  node [
    id 850
    label "&#379;migr&#243;d"
  ]
  node [
    id 851
    label "Ka&#322;uga"
  ]
  node [
    id 852
    label "Rakoniewice"
  ]
  node [
    id 853
    label "Trabzon"
  ]
  node [
    id 854
    label "Debreczyn"
  ]
  node [
    id 855
    label "Jena"
  ]
  node [
    id 856
    label "Walencja"
  ]
  node [
    id 857
    label "Gwardiejsk"
  ]
  node [
    id 858
    label "Wersal"
  ]
  node [
    id 859
    label "Ba&#322;tijsk"
  ]
  node [
    id 860
    label "Bych&#243;w"
  ]
  node [
    id 861
    label "Strzelno"
  ]
  node [
    id 862
    label "Trenczyn"
  ]
  node [
    id 863
    label "Warna"
  ]
  node [
    id 864
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 865
    label "Huma&#324;"
  ]
  node [
    id 866
    label "Wilejka"
  ]
  node [
    id 867
    label "Ochryda"
  ]
  node [
    id 868
    label "Berdycz&#243;w"
  ]
  node [
    id 869
    label "Krasnogorsk"
  ]
  node [
    id 870
    label "Bogus&#322;aw"
  ]
  node [
    id 871
    label "Trzyniec"
  ]
  node [
    id 872
    label "urz&#261;d"
  ]
  node [
    id 873
    label "Mariampol"
  ]
  node [
    id 874
    label "Ko&#322;omna"
  ]
  node [
    id 875
    label "Chanty-Mansyjsk"
  ]
  node [
    id 876
    label "Piast&#243;w"
  ]
  node [
    id 877
    label "Jastrowie"
  ]
  node [
    id 878
    label "Nampula"
  ]
  node [
    id 879
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 880
    label "Bor"
  ]
  node [
    id 881
    label "Lengyel"
  ]
  node [
    id 882
    label "Lubecz"
  ]
  node [
    id 883
    label "Wierchoja&#324;sk"
  ]
  node [
    id 884
    label "Barczewo"
  ]
  node [
    id 885
    label "Madras"
  ]
  node [
    id 886
    label "stanowisko"
  ]
  node [
    id 887
    label "position"
  ]
  node [
    id 888
    label "instytucja"
  ]
  node [
    id 889
    label "siedziba"
  ]
  node [
    id 890
    label "organ"
  ]
  node [
    id 891
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 892
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 893
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 894
    label "mianowaniec"
  ]
  node [
    id 895
    label "dzia&#322;"
  ]
  node [
    id 896
    label "okienko"
  ]
  node [
    id 897
    label "w&#322;adza"
  ]
  node [
    id 898
    label "odm&#322;adzanie"
  ]
  node [
    id 899
    label "liga"
  ]
  node [
    id 900
    label "jednostka_systematyczna"
  ]
  node [
    id 901
    label "gromada"
  ]
  node [
    id 902
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 903
    label "egzemplarz"
  ]
  node [
    id 904
    label "Entuzjastki"
  ]
  node [
    id 905
    label "zbi&#243;r"
  ]
  node [
    id 906
    label "kompozycja"
  ]
  node [
    id 907
    label "Terranie"
  ]
  node [
    id 908
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 909
    label "category"
  ]
  node [
    id 910
    label "pakiet_klimatyczny"
  ]
  node [
    id 911
    label "oddzia&#322;"
  ]
  node [
    id 912
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 913
    label "cz&#261;steczka"
  ]
  node [
    id 914
    label "stage_set"
  ]
  node [
    id 915
    label "type"
  ]
  node [
    id 916
    label "specgrupa"
  ]
  node [
    id 917
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 918
    label "&#346;wietliki"
  ]
  node [
    id 919
    label "odm&#322;odzenie"
  ]
  node [
    id 920
    label "Eurogrupa"
  ]
  node [
    id 921
    label "odm&#322;adza&#263;"
  ]
  node [
    id 922
    label "formacja_geologiczna"
  ]
  node [
    id 923
    label "harcerze_starsi"
  ]
  node [
    id 924
    label "Aurignac"
  ]
  node [
    id 925
    label "Sabaudia"
  ]
  node [
    id 926
    label "Cecora"
  ]
  node [
    id 927
    label "Saint-Acheul"
  ]
  node [
    id 928
    label "Boulogne"
  ]
  node [
    id 929
    label "Opat&#243;wek"
  ]
  node [
    id 930
    label "osiedle"
  ]
  node [
    id 931
    label "Levallois-Perret"
  ]
  node [
    id 932
    label "kompleks"
  ]
  node [
    id 933
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 934
    label "droga"
  ]
  node [
    id 935
    label "korona_drogi"
  ]
  node [
    id 936
    label "pas_rozdzielczy"
  ]
  node [
    id 937
    label "&#347;rodowisko"
  ]
  node [
    id 938
    label "streetball"
  ]
  node [
    id 939
    label "miasteczko"
  ]
  node [
    id 940
    label "chodnik"
  ]
  node [
    id 941
    label "pas_ruchu"
  ]
  node [
    id 942
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 943
    label "pierzeja"
  ]
  node [
    id 944
    label "wysepka"
  ]
  node [
    id 945
    label "arteria"
  ]
  node [
    id 946
    label "Broadway"
  ]
  node [
    id 947
    label "autostrada"
  ]
  node [
    id 948
    label "jezdnia"
  ]
  node [
    id 949
    label "harcerstwo"
  ]
  node [
    id 950
    label "Mozambik"
  ]
  node [
    id 951
    label "Budionowsk"
  ]
  node [
    id 952
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 953
    label "Niemcy"
  ]
  node [
    id 954
    label "edam"
  ]
  node [
    id 955
    label "Kalinin"
  ]
  node [
    id 956
    label "Monaster"
  ]
  node [
    id 957
    label "archidiecezja"
  ]
  node [
    id 958
    label "Rosja"
  ]
  node [
    id 959
    label "Stanis&#322;aw&#243;w"
  ]
  node [
    id 960
    label "Budapeszt"
  ]
  node [
    id 961
    label "Dunajec"
  ]
  node [
    id 962
    label "Tatry"
  ]
  node [
    id 963
    label "S&#261;decczyzna"
  ]
  node [
    id 964
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 965
    label "dram"
  ]
  node [
    id 966
    label "woda_kolo&#324;ska"
  ]
  node [
    id 967
    label "Azerbejd&#380;an"
  ]
  node [
    id 968
    label "Szwajcaria"
  ]
  node [
    id 969
    label "wirus"
  ]
  node [
    id 970
    label "gor&#261;czka_krwotoczna"
  ]
  node [
    id 971
    label "filowirusy"
  ]
  node [
    id 972
    label "mury_Jerycha"
  ]
  node [
    id 973
    label "Hiszpania"
  ]
  node [
    id 974
    label "&#321;otwa"
  ]
  node [
    id 975
    label "Litwa"
  ]
  node [
    id 976
    label "&#321;yczak&#243;w"
  ]
  node [
    id 977
    label "Skierniewice"
  ]
  node [
    id 978
    label "Stambu&#322;"
  ]
  node [
    id 979
    label "Bizancjum"
  ]
  node [
    id 980
    label "Brenna"
  ]
  node [
    id 981
    label "frank_monakijski"
  ]
  node [
    id 982
    label "euro"
  ]
  node [
    id 983
    label "Ukraina"
  ]
  node [
    id 984
    label "Dzikie_Pola"
  ]
  node [
    id 985
    label "Sicz"
  ]
  node [
    id 986
    label "Francja"
  ]
  node [
    id 987
    label "Frysztat"
  ]
  node [
    id 988
    label "The_Beatles"
  ]
  node [
    id 989
    label "Prusy"
  ]
  node [
    id 990
    label "Swierd&#322;owsk"
  ]
  node [
    id 991
    label "Psie_Pole"
  ]
  node [
    id 992
    label "obraz"
  ]
  node [
    id 993
    label "dzie&#322;o"
  ]
  node [
    id 994
    label "wagon"
  ]
  node [
    id 995
    label "bimba"
  ]
  node [
    id 996
    label "pojazd_szynowy"
  ]
  node [
    id 997
    label "odbierak"
  ]
  node [
    id 998
    label "&#347;rodek_transportu_publicznego"
  ]
  node [
    id 999
    label "samorz&#261;dowiec"
  ]
  node [
    id 1000
    label "ceklarz"
  ]
  node [
    id 1001
    label "burmistrzyna"
  ]
  node [
    id 1002
    label "po_niemiecku"
  ]
  node [
    id 1003
    label "German"
  ]
  node [
    id 1004
    label "niemiecko"
  ]
  node [
    id 1005
    label "cenar"
  ]
  node [
    id 1006
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 1007
    label "europejski"
  ]
  node [
    id 1008
    label "strudel"
  ]
  node [
    id 1009
    label "niemiec"
  ]
  node [
    id 1010
    label "pionier"
  ]
  node [
    id 1011
    label "zachodnioeuropejski"
  ]
  node [
    id 1012
    label "j&#281;zyk"
  ]
  node [
    id 1013
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 1014
    label "junkers"
  ]
  node [
    id 1015
    label "szwabski"
  ]
  node [
    id 1016
    label "szwabsko"
  ]
  node [
    id 1017
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 1018
    label "po_szwabsku"
  ]
  node [
    id 1019
    label "platt"
  ]
  node [
    id 1020
    label "europejsko"
  ]
  node [
    id 1021
    label "&#380;o&#322;nierz"
  ]
  node [
    id 1022
    label "saper"
  ]
  node [
    id 1023
    label "prekursor"
  ]
  node [
    id 1024
    label "osadnik"
  ]
  node [
    id 1025
    label "skaut"
  ]
  node [
    id 1026
    label "g&#322;osiciel"
  ]
  node [
    id 1027
    label "taniec_ludowy"
  ]
  node [
    id 1028
    label "melodia"
  ]
  node [
    id 1029
    label "taniec"
  ]
  node [
    id 1030
    label "podgrzewacz"
  ]
  node [
    id 1031
    label "samolot_wojskowy"
  ]
  node [
    id 1032
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 1033
    label "langosz"
  ]
  node [
    id 1034
    label "moreska"
  ]
  node [
    id 1035
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 1036
    label "zachodni"
  ]
  node [
    id 1037
    label "po_europejsku"
  ]
  node [
    id 1038
    label "European"
  ]
  node [
    id 1039
    label "charakterystyczny"
  ]
  node [
    id 1040
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1041
    label "artykulator"
  ]
  node [
    id 1042
    label "kod"
  ]
  node [
    id 1043
    label "kawa&#322;ek"
  ]
  node [
    id 1044
    label "przedmiot"
  ]
  node [
    id 1045
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1046
    label "gramatyka"
  ]
  node [
    id 1047
    label "stylik"
  ]
  node [
    id 1048
    label "przet&#322;umaczenie"
  ]
  node [
    id 1049
    label "formalizowanie"
  ]
  node [
    id 1050
    label "ssa&#263;"
  ]
  node [
    id 1051
    label "ssanie"
  ]
  node [
    id 1052
    label "language"
  ]
  node [
    id 1053
    label "liza&#263;"
  ]
  node [
    id 1054
    label "napisa&#263;"
  ]
  node [
    id 1055
    label "konsonantyzm"
  ]
  node [
    id 1056
    label "wokalizm"
  ]
  node [
    id 1057
    label "pisa&#263;"
  ]
  node [
    id 1058
    label "fonetyka"
  ]
  node [
    id 1059
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1060
    label "jeniec"
  ]
  node [
    id 1061
    label "but"
  ]
  node [
    id 1062
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1063
    label "po_koroniarsku"
  ]
  node [
    id 1064
    label "kultura_duchowa"
  ]
  node [
    id 1065
    label "t&#322;umaczenie"
  ]
  node [
    id 1066
    label "m&#243;wienie"
  ]
  node [
    id 1067
    label "pype&#263;"
  ]
  node [
    id 1068
    label "lizanie"
  ]
  node [
    id 1069
    label "pismo"
  ]
  node [
    id 1070
    label "formalizowa&#263;"
  ]
  node [
    id 1071
    label "rozumie&#263;"
  ]
  node [
    id 1072
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1073
    label "rozumienie"
  ]
  node [
    id 1074
    label "spos&#243;b"
  ]
  node [
    id 1075
    label "makroglosja"
  ]
  node [
    id 1076
    label "m&#243;wi&#263;"
  ]
  node [
    id 1077
    label "jama_ustna"
  ]
  node [
    id 1078
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1079
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1080
    label "natural_language"
  ]
  node [
    id 1081
    label "s&#322;ownictwo"
  ]
  node [
    id 1082
    label "urz&#261;dzenie"
  ]
  node [
    id 1083
    label "prowincja"
  ]
  node [
    id 1084
    label "Ulster"
  ]
  node [
    id 1085
    label "Limburgia"
  ]
  node [
    id 1086
    label "nation"
  ]
  node [
    id 1087
    label "ubocze"
  ]
  node [
    id 1088
    label "sufragania"
  ]
  node [
    id 1089
    label "Katanga"
  ]
  node [
    id 1090
    label "Zelandia"
  ]
  node [
    id 1091
    label "Papua"
  ]
  node [
    id 1092
    label "Prusy_Wschodnie"
  ]
  node [
    id 1093
    label "Gaza"
  ]
  node [
    id 1094
    label "Niasa"
  ]
  node [
    id 1095
    label "Junnan"
  ]
  node [
    id 1096
    label "Syczuan"
  ]
  node [
    id 1097
    label "Luksemburgia"
  ]
  node [
    id 1098
    label "Manica"
  ]
  node [
    id 1099
    label "Szantung"
  ]
  node [
    id 1100
    label "Wojwodina"
  ]
  node [
    id 1101
    label "Cabo_Delgado"
  ]
  node [
    id 1102
    label "terytorium"
  ]
  node [
    id 1103
    label "Quebec"
  ]
  node [
    id 1104
    label "Maputo"
  ]
  node [
    id 1105
    label "egzarcha"
  ]
  node [
    id 1106
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1107
    label "Luzytania"
  ]
  node [
    id 1108
    label "municypium"
  ]
  node [
    id 1109
    label "Asturia"
  ]
  node [
    id 1110
    label "Ko&#347;ci&#243;&#322;_katolicki"
  ]
  node [
    id 1111
    label "przedmie&#347;cie"
  ]
  node [
    id 1112
    label "Guangdong"
  ]
  node [
    id 1113
    label "jednostka_administracyjna"
  ]
  node [
    id 1114
    label "Panonia"
  ]
  node [
    id 1115
    label "pastwa"
  ]
  node [
    id 1116
    label "gamo&#324;"
  ]
  node [
    id 1117
    label "ko&#347;cielny"
  ]
  node [
    id 1118
    label "zwierz&#281;"
  ]
  node [
    id 1119
    label "dar"
  ]
  node [
    id 1120
    label "nastawienie"
  ]
  node [
    id 1121
    label "crack"
  ]
  node [
    id 1122
    label "po&#347;miewisko"
  ]
  node [
    id 1123
    label "zbi&#243;rka"
  ]
  node [
    id 1124
    label "istota_&#380;ywa"
  ]
  node [
    id 1125
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 1126
    label "rzecz"
  ]
  node [
    id 1127
    label "dupa_wo&#322;owa"
  ]
  node [
    id 1128
    label "object"
  ]
  node [
    id 1129
    label "temat"
  ]
  node [
    id 1130
    label "wpadni&#281;cie"
  ]
  node [
    id 1131
    label "mienie"
  ]
  node [
    id 1132
    label "przyroda"
  ]
  node [
    id 1133
    label "istota"
  ]
  node [
    id 1134
    label "obiekt"
  ]
  node [
    id 1135
    label "kultura"
  ]
  node [
    id 1136
    label "wpa&#347;&#263;"
  ]
  node [
    id 1137
    label "wpadanie"
  ]
  node [
    id 1138
    label "wpada&#263;"
  ]
  node [
    id 1139
    label "degenerat"
  ]
  node [
    id 1140
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 1141
    label "zwyrol"
  ]
  node [
    id 1142
    label "czerniak"
  ]
  node [
    id 1143
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1144
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 1145
    label "paszcza"
  ]
  node [
    id 1146
    label "popapraniec"
  ]
  node [
    id 1147
    label "skuba&#263;"
  ]
  node [
    id 1148
    label "skubanie"
  ]
  node [
    id 1149
    label "skubni&#281;cie"
  ]
  node [
    id 1150
    label "agresja"
  ]
  node [
    id 1151
    label "zwierz&#281;ta"
  ]
  node [
    id 1152
    label "fukni&#281;cie"
  ]
  node [
    id 1153
    label "farba"
  ]
  node [
    id 1154
    label "fukanie"
  ]
  node [
    id 1155
    label "gad"
  ]
  node [
    id 1156
    label "siedzie&#263;"
  ]
  node [
    id 1157
    label "oswaja&#263;"
  ]
  node [
    id 1158
    label "tresowa&#263;"
  ]
  node [
    id 1159
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 1160
    label "poligamia"
  ]
  node [
    id 1161
    label "oz&#243;r"
  ]
  node [
    id 1162
    label "skubn&#261;&#263;"
  ]
  node [
    id 1163
    label "wios&#322;owa&#263;"
  ]
  node [
    id 1164
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 1165
    label "le&#380;enie"
  ]
  node [
    id 1166
    label "niecz&#322;owiek"
  ]
  node [
    id 1167
    label "wios&#322;owanie"
  ]
  node [
    id 1168
    label "napasienie_si&#281;"
  ]
  node [
    id 1169
    label "wiwarium"
  ]
  node [
    id 1170
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 1171
    label "animalista"
  ]
  node [
    id 1172
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 1173
    label "budowa"
  ]
  node [
    id 1174
    label "hodowla"
  ]
  node [
    id 1175
    label "pasienie_si&#281;"
  ]
  node [
    id 1176
    label "sodomita"
  ]
  node [
    id 1177
    label "monogamia"
  ]
  node [
    id 1178
    label "przyssawka"
  ]
  node [
    id 1179
    label "zachowanie"
  ]
  node [
    id 1180
    label "budowa_cia&#322;a"
  ]
  node [
    id 1181
    label "okrutnik"
  ]
  node [
    id 1182
    label "grzbiet"
  ]
  node [
    id 1183
    label "weterynarz"
  ]
  node [
    id 1184
    label "&#322;eb"
  ]
  node [
    id 1185
    label "wylinka"
  ]
  node [
    id 1186
    label "bestia"
  ]
  node [
    id 1187
    label "poskramia&#263;"
  ]
  node [
    id 1188
    label "fauna"
  ]
  node [
    id 1189
    label "treser"
  ]
  node [
    id 1190
    label "siedzenie"
  ]
  node [
    id 1191
    label "le&#380;e&#263;"
  ]
  node [
    id 1192
    label "ustawienie"
  ]
  node [
    id 1193
    label "z&#322;amanie"
  ]
  node [
    id 1194
    label "gotowanie_si&#281;"
  ]
  node [
    id 1195
    label "oddzia&#322;anie"
  ]
  node [
    id 1196
    label "ponastawianie"
  ]
  node [
    id 1197
    label "bearing"
  ]
  node [
    id 1198
    label "powaga"
  ]
  node [
    id 1199
    label "z&#322;o&#380;enie"
  ]
  node [
    id 1200
    label "podej&#347;cie"
  ]
  node [
    id 1201
    label "umieszczenie"
  ]
  node [
    id 1202
    label "cecha"
  ]
  node [
    id 1203
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1204
    label "ukierunkowanie"
  ]
  node [
    id 1205
    label "dyspozycja"
  ]
  node [
    id 1206
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1207
    label "da&#324;"
  ]
  node [
    id 1208
    label "faculty"
  ]
  node [
    id 1209
    label "stygmat"
  ]
  node [
    id 1210
    label "dobro"
  ]
  node [
    id 1211
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 1212
    label "kwestarz"
  ]
  node [
    id 1213
    label "kwestowanie"
  ]
  node [
    id 1214
    label "apel"
  ]
  node [
    id 1215
    label "recoil"
  ]
  node [
    id 1216
    label "collection"
  ]
  node [
    id 1217
    label "spotkanie"
  ]
  node [
    id 1218
    label "koszyk&#243;wka"
  ]
  node [
    id 1219
    label "chwyt"
  ]
  node [
    id 1220
    label "czynno&#347;&#263;"
  ]
  node [
    id 1221
    label "zboczenie"
  ]
  node [
    id 1222
    label "om&#243;wienie"
  ]
  node [
    id 1223
    label "sponiewieranie"
  ]
  node [
    id 1224
    label "discipline"
  ]
  node [
    id 1225
    label "omawia&#263;"
  ]
  node [
    id 1226
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1227
    label "tre&#347;&#263;"
  ]
  node [
    id 1228
    label "robienie"
  ]
  node [
    id 1229
    label "sponiewiera&#263;"
  ]
  node [
    id 1230
    label "element"
  ]
  node [
    id 1231
    label "entity"
  ]
  node [
    id 1232
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1233
    label "tematyka"
  ]
  node [
    id 1234
    label "w&#261;tek"
  ]
  node [
    id 1235
    label "charakter"
  ]
  node [
    id 1236
    label "zbaczanie"
  ]
  node [
    id 1237
    label "program_nauczania"
  ]
  node [
    id 1238
    label "om&#243;wi&#263;"
  ]
  node [
    id 1239
    label "omawianie"
  ]
  node [
    id 1240
    label "thing"
  ]
  node [
    id 1241
    label "zbacza&#263;"
  ]
  node [
    id 1242
    label "zboczy&#263;"
  ]
  node [
    id 1243
    label "bidaka"
  ]
  node [
    id 1244
    label "bidak"
  ]
  node [
    id 1245
    label "Hiob"
  ]
  node [
    id 1246
    label "cz&#322;eczyna"
  ]
  node [
    id 1247
    label "niebo&#380;&#281;"
  ]
  node [
    id 1248
    label "jest"
  ]
  node [
    id 1249
    label "szydzenie"
  ]
  node [
    id 1250
    label "&#322;up"
  ]
  node [
    id 1251
    label "zdobycz"
  ]
  node [
    id 1252
    label "jedzenie"
  ]
  node [
    id 1253
    label "ba&#322;wan"
  ]
  node [
    id 1254
    label "t&#281;pak"
  ]
  node [
    id 1255
    label "oferma"
  ]
  node [
    id 1256
    label "ko&#347;cielnie"
  ]
  node [
    id 1257
    label "parafianin"
  ]
  node [
    id 1258
    label "sidesman"
  ]
  node [
    id 1259
    label "pomoc"
  ]
  node [
    id 1260
    label "taca"
  ]
  node [
    id 1261
    label "s&#322;u&#380;ba_ko&#347;cielna"
  ]
  node [
    id 1262
    label "gap"
  ]
  node [
    id 1263
    label "kokaina"
  ]
  node [
    id 1264
    label "program"
  ]
  node [
    id 1265
    label "przest&#281;pstwo"
  ]
  node [
    id 1266
    label "zabicie"
  ]
  node [
    id 1267
    label "brudny"
  ]
  node [
    id 1268
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 1269
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 1270
    label "crime"
  ]
  node [
    id 1271
    label "sprawstwo"
  ]
  node [
    id 1272
    label "&#347;mier&#263;"
  ]
  node [
    id 1273
    label "destruction"
  ]
  node [
    id 1274
    label "zabrzmienie"
  ]
  node [
    id 1275
    label "skrzywdzenie"
  ]
  node [
    id 1276
    label "pozabijanie"
  ]
  node [
    id 1277
    label "zniszczenie"
  ]
  node [
    id 1278
    label "zaszkodzenie"
  ]
  node [
    id 1279
    label "usuni&#281;cie"
  ]
  node [
    id 1280
    label "spowodowanie"
  ]
  node [
    id 1281
    label "killing"
  ]
  node [
    id 1282
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1283
    label "czyn"
  ]
  node [
    id 1284
    label "umarcie"
  ]
  node [
    id 1285
    label "granie"
  ]
  node [
    id 1286
    label "zamkni&#281;cie"
  ]
  node [
    id 1287
    label "compaction"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 789
  ]
  edge [
    source 7
    target 790
  ]
  edge [
    source 7
    target 791
  ]
  edge [
    source 7
    target 792
  ]
  edge [
    source 7
    target 793
  ]
  edge [
    source 7
    target 794
  ]
  edge [
    source 7
    target 795
  ]
  edge [
    source 7
    target 796
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 798
  ]
  edge [
    source 7
    target 799
  ]
  edge [
    source 7
    target 800
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 802
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 804
  ]
  edge [
    source 7
    target 805
  ]
  edge [
    source 7
    target 806
  ]
  edge [
    source 7
    target 807
  ]
  edge [
    source 7
    target 808
  ]
  edge [
    source 7
    target 809
  ]
  edge [
    source 7
    target 810
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 7
    target 817
  ]
  edge [
    source 7
    target 818
  ]
  edge [
    source 7
    target 819
  ]
  edge [
    source 7
    target 820
  ]
  edge [
    source 7
    target 821
  ]
  edge [
    source 7
    target 822
  ]
  edge [
    source 7
    target 823
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 7
    target 839
  ]
  edge [
    source 7
    target 840
  ]
  edge [
    source 7
    target 841
  ]
  edge [
    source 7
    target 842
  ]
  edge [
    source 7
    target 843
  ]
  edge [
    source 7
    target 844
  ]
  edge [
    source 7
    target 845
  ]
  edge [
    source 7
    target 846
  ]
  edge [
    source 7
    target 847
  ]
  edge [
    source 7
    target 848
  ]
  edge [
    source 7
    target 849
  ]
  edge [
    source 7
    target 850
  ]
  edge [
    source 7
    target 851
  ]
  edge [
    source 7
    target 852
  ]
  edge [
    source 7
    target 853
  ]
  edge [
    source 7
    target 854
  ]
  edge [
    source 7
    target 855
  ]
  edge [
    source 7
    target 856
  ]
  edge [
    source 7
    target 857
  ]
  edge [
    source 7
    target 858
  ]
  edge [
    source 7
    target 859
  ]
  edge [
    source 7
    target 860
  ]
  edge [
    source 7
    target 861
  ]
  edge [
    source 7
    target 862
  ]
  edge [
    source 7
    target 863
  ]
  edge [
    source 7
    target 864
  ]
  edge [
    source 7
    target 865
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 867
  ]
  edge [
    source 7
    target 868
  ]
  edge [
    source 7
    target 869
  ]
  edge [
    source 7
    target 870
  ]
  edge [
    source 7
    target 871
  ]
  edge [
    source 7
    target 872
  ]
  edge [
    source 7
    target 873
  ]
  edge [
    source 7
    target 874
  ]
  edge [
    source 7
    target 875
  ]
  edge [
    source 7
    target 876
  ]
  edge [
    source 7
    target 877
  ]
  edge [
    source 7
    target 878
  ]
  edge [
    source 7
    target 879
  ]
  edge [
    source 7
    target 880
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 882
  ]
  edge [
    source 7
    target 883
  ]
  edge [
    source 7
    target 884
  ]
  edge [
    source 7
    target 885
  ]
  edge [
    source 7
    target 886
  ]
  edge [
    source 7
    target 887
  ]
  edge [
    source 7
    target 888
  ]
  edge [
    source 7
    target 889
  ]
  edge [
    source 7
    target 890
  ]
  edge [
    source 7
    target 891
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 893
  ]
  edge [
    source 7
    target 894
  ]
  edge [
    source 7
    target 895
  ]
  edge [
    source 7
    target 896
  ]
  edge [
    source 7
    target 897
  ]
  edge [
    source 7
    target 898
  ]
  edge [
    source 7
    target 899
  ]
  edge [
    source 7
    target 900
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 901
  ]
  edge [
    source 7
    target 902
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 903
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 7
    target 907
  ]
  edge [
    source 7
    target 908
  ]
  edge [
    source 7
    target 909
  ]
  edge [
    source 7
    target 910
  ]
  edge [
    source 7
    target 911
  ]
  edge [
    source 7
    target 912
  ]
  edge [
    source 7
    target 913
  ]
  edge [
    source 7
    target 914
  ]
  edge [
    source 7
    target 915
  ]
  edge [
    source 7
    target 916
  ]
  edge [
    source 7
    target 917
  ]
  edge [
    source 7
    target 918
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 921
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 923
  ]
  edge [
    source 7
    target 924
  ]
  edge [
    source 7
    target 925
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 7
    target 928
  ]
  edge [
    source 7
    target 929
  ]
  edge [
    source 7
    target 930
  ]
  edge [
    source 7
    target 931
  ]
  edge [
    source 7
    target 932
  ]
  edge [
    source 7
    target 933
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 7
    target 937
  ]
  edge [
    source 7
    target 938
  ]
  edge [
    source 7
    target 939
  ]
  edge [
    source 7
    target 940
  ]
  edge [
    source 7
    target 941
  ]
  edge [
    source 7
    target 942
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 944
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 974
  ]
  edge [
    source 7
    target 975
  ]
  edge [
    source 7
    target 976
  ]
  edge [
    source 7
    target 977
  ]
  edge [
    source 7
    target 978
  ]
  edge [
    source 7
    target 979
  ]
  edge [
    source 7
    target 980
  ]
  edge [
    source 7
    target 981
  ]
  edge [
    source 7
    target 982
  ]
  edge [
    source 7
    target 983
  ]
  edge [
    source 7
    target 984
  ]
  edge [
    source 7
    target 985
  ]
  edge [
    source 7
    target 986
  ]
  edge [
    source 7
    target 987
  ]
  edge [
    source 7
    target 988
  ]
  edge [
    source 7
    target 989
  ]
  edge [
    source 7
    target 990
  ]
  edge [
    source 7
    target 991
  ]
  edge [
    source 7
    target 992
  ]
  edge [
    source 7
    target 993
  ]
  edge [
    source 7
    target 994
  ]
  edge [
    source 7
    target 995
  ]
  edge [
    source 7
    target 996
  ]
  edge [
    source 7
    target 997
  ]
  edge [
    source 7
    target 998
  ]
  edge [
    source 7
    target 999
  ]
  edge [
    source 7
    target 1000
  ]
  edge [
    source 7
    target 1001
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1002
  ]
  edge [
    source 9
    target 1003
  ]
  edge [
    source 9
    target 1004
  ]
  edge [
    source 9
    target 1005
  ]
  edge [
    source 9
    target 1006
  ]
  edge [
    source 9
    target 1007
  ]
  edge [
    source 9
    target 1008
  ]
  edge [
    source 9
    target 1009
  ]
  edge [
    source 9
    target 1010
  ]
  edge [
    source 9
    target 1011
  ]
  edge [
    source 9
    target 1012
  ]
  edge [
    source 9
    target 1013
  ]
  edge [
    source 9
    target 1014
  ]
  edge [
    source 9
    target 1015
  ]
  edge [
    source 9
    target 1016
  ]
  edge [
    source 9
    target 1017
  ]
  edge [
    source 9
    target 1018
  ]
  edge [
    source 9
    target 1019
  ]
  edge [
    source 9
    target 1020
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 1021
  ]
  edge [
    source 9
    target 1022
  ]
  edge [
    source 9
    target 1023
  ]
  edge [
    source 9
    target 1024
  ]
  edge [
    source 9
    target 1025
  ]
  edge [
    source 9
    target 1026
  ]
  edge [
    source 9
    target 1027
  ]
  edge [
    source 9
    target 1028
  ]
  edge [
    source 9
    target 1029
  ]
  edge [
    source 9
    target 1030
  ]
  edge [
    source 9
    target 1031
  ]
  edge [
    source 9
    target 1032
  ]
  edge [
    source 9
    target 1033
  ]
  edge [
    source 9
    target 1034
  ]
  edge [
    source 9
    target 1035
  ]
  edge [
    source 9
    target 1036
  ]
  edge [
    source 9
    target 1037
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 1038
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 1039
  ]
  edge [
    source 9
    target 1040
  ]
  edge [
    source 9
    target 1041
  ]
  edge [
    source 9
    target 1042
  ]
  edge [
    source 9
    target 1043
  ]
  edge [
    source 9
    target 1044
  ]
  edge [
    source 9
    target 1045
  ]
  edge [
    source 9
    target 1046
  ]
  edge [
    source 9
    target 1047
  ]
  edge [
    source 9
    target 1048
  ]
  edge [
    source 9
    target 1049
  ]
  edge [
    source 9
    target 1050
  ]
  edge [
    source 9
    target 1051
  ]
  edge [
    source 9
    target 1052
  ]
  edge [
    source 9
    target 1053
  ]
  edge [
    source 9
    target 1054
  ]
  edge [
    source 9
    target 1055
  ]
  edge [
    source 9
    target 1056
  ]
  edge [
    source 9
    target 1057
  ]
  edge [
    source 9
    target 1058
  ]
  edge [
    source 9
    target 1059
  ]
  edge [
    source 9
    target 1060
  ]
  edge [
    source 9
    target 1061
  ]
  edge [
    source 9
    target 1062
  ]
  edge [
    source 9
    target 1063
  ]
  edge [
    source 9
    target 1064
  ]
  edge [
    source 9
    target 1065
  ]
  edge [
    source 9
    target 1066
  ]
  edge [
    source 9
    target 1067
  ]
  edge [
    source 9
    target 1068
  ]
  edge [
    source 9
    target 1069
  ]
  edge [
    source 9
    target 1070
  ]
  edge [
    source 9
    target 1071
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 1072
  ]
  edge [
    source 9
    target 1073
  ]
  edge [
    source 9
    target 1074
  ]
  edge [
    source 9
    target 1075
  ]
  edge [
    source 9
    target 1076
  ]
  edge [
    source 9
    target 1077
  ]
  edge [
    source 9
    target 1078
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 1079
  ]
  edge [
    source 9
    target 1080
  ]
  edge [
    source 9
    target 1081
  ]
  edge [
    source 9
    target 1082
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 1083
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 1084
  ]
  edge [
    source 11
    target 1085
  ]
  edge [
    source 11
    target 1086
  ]
  edge [
    source 11
    target 1087
  ]
  edge [
    source 11
    target 1088
  ]
  edge [
    source 11
    target 1089
  ]
  edge [
    source 11
    target 1090
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 1091
  ]
  edge [
    source 11
    target 1092
  ]
  edge [
    source 11
    target 1093
  ]
  edge [
    source 11
    target 1094
  ]
  edge [
    source 11
    target 1095
  ]
  edge [
    source 11
    target 1096
  ]
  edge [
    source 11
    target 1097
  ]
  edge [
    source 11
    target 1098
  ]
  edge [
    source 11
    target 1099
  ]
  edge [
    source 11
    target 937
  ]
  edge [
    source 11
    target 1100
  ]
  edge [
    source 11
    target 1101
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 1102
  ]
  edge [
    source 11
    target 1103
  ]
  edge [
    source 11
    target 1104
  ]
  edge [
    source 11
    target 1105
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 1106
  ]
  edge [
    source 11
    target 1107
  ]
  edge [
    source 11
    target 1108
  ]
  edge [
    source 11
    target 1109
  ]
  edge [
    source 11
    target 1110
  ]
  edge [
    source 11
    target 1111
  ]
  edge [
    source 11
    target 1112
  ]
  edge [
    source 11
    target 1113
  ]
  edge [
    source 11
    target 1114
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1115
  ]
  edge [
    source 13
    target 1044
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 1116
  ]
  edge [
    source 13
    target 1117
  ]
  edge [
    source 13
    target 1118
  ]
  edge [
    source 13
    target 1119
  ]
  edge [
    source 13
    target 1120
  ]
  edge [
    source 13
    target 1121
  ]
  edge [
    source 13
    target 1122
  ]
  edge [
    source 13
    target 1123
  ]
  edge [
    source 13
    target 1124
  ]
  edge [
    source 13
    target 1125
  ]
  edge [
    source 13
    target 1126
  ]
  edge [
    source 13
    target 1127
  ]
  edge [
    source 13
    target 1128
  ]
  edge [
    source 13
    target 1129
  ]
  edge [
    source 13
    target 1130
  ]
  edge [
    source 13
    target 1131
  ]
  edge [
    source 13
    target 1132
  ]
  edge [
    source 13
    target 1133
  ]
  edge [
    source 13
    target 1134
  ]
  edge [
    source 13
    target 1135
  ]
  edge [
    source 13
    target 1136
  ]
  edge [
    source 13
    target 1137
  ]
  edge [
    source 13
    target 1138
  ]
  edge [
    source 13
    target 1139
  ]
  edge [
    source 13
    target 1140
  ]
  edge [
    source 13
    target 1141
  ]
  edge [
    source 13
    target 1142
  ]
  edge [
    source 13
    target 1143
  ]
  edge [
    source 13
    target 1144
  ]
  edge [
    source 13
    target 1145
  ]
  edge [
    source 13
    target 1146
  ]
  edge [
    source 13
    target 1147
  ]
  edge [
    source 13
    target 1148
  ]
  edge [
    source 13
    target 1149
  ]
  edge [
    source 13
    target 1150
  ]
  edge [
    source 13
    target 1151
  ]
  edge [
    source 13
    target 1152
  ]
  edge [
    source 13
    target 1153
  ]
  edge [
    source 13
    target 1154
  ]
  edge [
    source 13
    target 1155
  ]
  edge [
    source 13
    target 1156
  ]
  edge [
    source 13
    target 1157
  ]
  edge [
    source 13
    target 1158
  ]
  edge [
    source 13
    target 1159
  ]
  edge [
    source 13
    target 1160
  ]
  edge [
    source 13
    target 1161
  ]
  edge [
    source 13
    target 1162
  ]
  edge [
    source 13
    target 1163
  ]
  edge [
    source 13
    target 1164
  ]
  edge [
    source 13
    target 1165
  ]
  edge [
    source 13
    target 1166
  ]
  edge [
    source 13
    target 1167
  ]
  edge [
    source 13
    target 1168
  ]
  edge [
    source 13
    target 1169
  ]
  edge [
    source 13
    target 1170
  ]
  edge [
    source 13
    target 1171
  ]
  edge [
    source 13
    target 1172
  ]
  edge [
    source 13
    target 1173
  ]
  edge [
    source 13
    target 1174
  ]
  edge [
    source 13
    target 1175
  ]
  edge [
    source 13
    target 1176
  ]
  edge [
    source 13
    target 1177
  ]
  edge [
    source 13
    target 1178
  ]
  edge [
    source 13
    target 1179
  ]
  edge [
    source 13
    target 1180
  ]
  edge [
    source 13
    target 1181
  ]
  edge [
    source 13
    target 1182
  ]
  edge [
    source 13
    target 1183
  ]
  edge [
    source 13
    target 1184
  ]
  edge [
    source 13
    target 1185
  ]
  edge [
    source 13
    target 1186
  ]
  edge [
    source 13
    target 1187
  ]
  edge [
    source 13
    target 1188
  ]
  edge [
    source 13
    target 1189
  ]
  edge [
    source 13
    target 1190
  ]
  edge [
    source 13
    target 1191
  ]
  edge [
    source 13
    target 1192
  ]
  edge [
    source 13
    target 1193
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 1194
  ]
  edge [
    source 13
    target 1195
  ]
  edge [
    source 13
    target 1196
  ]
  edge [
    source 13
    target 1197
  ]
  edge [
    source 13
    target 1198
  ]
  edge [
    source 13
    target 1199
  ]
  edge [
    source 13
    target 1200
  ]
  edge [
    source 13
    target 1201
  ]
  edge [
    source 13
    target 1202
  ]
  edge [
    source 13
    target 1203
  ]
  edge [
    source 13
    target 1204
  ]
  edge [
    source 13
    target 1205
  ]
  edge [
    source 13
    target 1206
  ]
  edge [
    source 13
    target 1207
  ]
  edge [
    source 13
    target 1208
  ]
  edge [
    source 13
    target 1209
  ]
  edge [
    source 13
    target 1210
  ]
  edge [
    source 13
    target 1211
  ]
  edge [
    source 13
    target 1212
  ]
  edge [
    source 13
    target 1213
  ]
  edge [
    source 13
    target 1214
  ]
  edge [
    source 13
    target 1215
  ]
  edge [
    source 13
    target 1216
  ]
  edge [
    source 13
    target 1217
  ]
  edge [
    source 13
    target 1218
  ]
  edge [
    source 13
    target 1219
  ]
  edge [
    source 13
    target 1220
  ]
  edge [
    source 13
    target 1221
  ]
  edge [
    source 13
    target 1222
  ]
  edge [
    source 13
    target 1223
  ]
  edge [
    source 13
    target 1224
  ]
  edge [
    source 13
    target 1225
  ]
  edge [
    source 13
    target 1226
  ]
  edge [
    source 13
    target 1227
  ]
  edge [
    source 13
    target 1228
  ]
  edge [
    source 13
    target 1229
  ]
  edge [
    source 13
    target 1230
  ]
  edge [
    source 13
    target 1231
  ]
  edge [
    source 13
    target 1232
  ]
  edge [
    source 13
    target 1233
  ]
  edge [
    source 13
    target 1234
  ]
  edge [
    source 13
    target 1235
  ]
  edge [
    source 13
    target 1236
  ]
  edge [
    source 13
    target 1237
  ]
  edge [
    source 13
    target 1238
  ]
  edge [
    source 13
    target 1239
  ]
  edge [
    source 13
    target 1240
  ]
  edge [
    source 13
    target 1241
  ]
  edge [
    source 13
    target 1242
  ]
  edge [
    source 13
    target 1243
  ]
  edge [
    source 13
    target 1244
  ]
  edge [
    source 13
    target 1245
  ]
  edge [
    source 13
    target 1246
  ]
  edge [
    source 13
    target 1247
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 1248
  ]
  edge [
    source 13
    target 1249
  ]
  edge [
    source 13
    target 1250
  ]
  edge [
    source 13
    target 1251
  ]
  edge [
    source 13
    target 1252
  ]
  edge [
    source 13
    target 1253
  ]
  edge [
    source 13
    target 1254
  ]
  edge [
    source 13
    target 1255
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 1256
  ]
  edge [
    source 13
    target 1257
  ]
  edge [
    source 13
    target 1258
  ]
  edge [
    source 13
    target 1259
  ]
  edge [
    source 13
    target 1260
  ]
  edge [
    source 13
    target 1261
  ]
  edge [
    source 13
    target 1262
  ]
  edge [
    source 13
    target 1263
  ]
  edge [
    source 13
    target 1264
  ]
  edge [
    source 14
    target 1265
  ]
  edge [
    source 14
    target 1266
  ]
  edge [
    source 14
    target 1267
  ]
  edge [
    source 14
    target 1268
  ]
  edge [
    source 14
    target 1269
  ]
  edge [
    source 14
    target 1270
  ]
  edge [
    source 14
    target 1271
  ]
  edge [
    source 14
    target 1272
  ]
  edge [
    source 14
    target 1273
  ]
  edge [
    source 14
    target 1274
  ]
  edge [
    source 14
    target 1275
  ]
  edge [
    source 14
    target 1276
  ]
  edge [
    source 14
    target 1277
  ]
  edge [
    source 14
    target 1278
  ]
  edge [
    source 14
    target 1279
  ]
  edge [
    source 14
    target 1280
  ]
  edge [
    source 14
    target 1281
  ]
  edge [
    source 14
    target 1282
  ]
  edge [
    source 14
    target 1283
  ]
  edge [
    source 14
    target 1284
  ]
  edge [
    source 14
    target 1285
  ]
  edge [
    source 14
    target 1286
  ]
  edge [
    source 14
    target 1287
  ]
]
