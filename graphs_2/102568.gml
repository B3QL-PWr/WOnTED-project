graph [
  node [
    id 0
    label "cena"
    origin "text"
  ]
  node [
    id 1
    label "wywo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pierwsza"
    origin "text"
  ]
  node [
    id 3
    label "termin"
    origin "text"
  ]
  node [
    id 4
    label "licytacja"
    origin "text"
  ]
  node [
    id 5
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "warto&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "szacunkowy"
    origin "text"
  ]
  node [
    id 8
    label "ruchomo&#347;ci"
    origin "text"
  ]
  node [
    id 9
    label "druga"
    origin "text"
  ]
  node [
    id 10
    label "kupowanie"
  ]
  node [
    id 11
    label "wyceni&#263;"
  ]
  node [
    id 12
    label "kosztowa&#263;"
  ]
  node [
    id 13
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 14
    label "dyskryminacja_cenowa"
  ]
  node [
    id 15
    label "wycenienie"
  ]
  node [
    id 16
    label "worth"
  ]
  node [
    id 17
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 18
    label "inflacja"
  ]
  node [
    id 19
    label "kosztowanie"
  ]
  node [
    id 20
    label "rozmiar"
  ]
  node [
    id 21
    label "zrewaluowa&#263;"
  ]
  node [
    id 22
    label "zmienna"
  ]
  node [
    id 23
    label "wskazywanie"
  ]
  node [
    id 24
    label "rewaluowanie"
  ]
  node [
    id 25
    label "cel"
  ]
  node [
    id 26
    label "wskazywa&#263;"
  ]
  node [
    id 27
    label "korzy&#347;&#263;"
  ]
  node [
    id 28
    label "poj&#281;cie"
  ]
  node [
    id 29
    label "cecha"
  ]
  node [
    id 30
    label "zrewaluowanie"
  ]
  node [
    id 31
    label "rewaluowa&#263;"
  ]
  node [
    id 32
    label "wabik"
  ]
  node [
    id 33
    label "strona"
  ]
  node [
    id 34
    label "ustalenie"
  ]
  node [
    id 35
    label "appraisal"
  ]
  node [
    id 36
    label "policzenie"
  ]
  node [
    id 37
    label "proces_ekonomiczny"
  ]
  node [
    id 38
    label "faza"
  ]
  node [
    id 39
    label "wzrost"
  ]
  node [
    id 40
    label "ewolucja_kosmosu"
  ]
  node [
    id 41
    label "kosmologia"
  ]
  node [
    id 42
    label "uznawanie"
  ]
  node [
    id 43
    label "wkupienie_si&#281;"
  ]
  node [
    id 44
    label "kupienie"
  ]
  node [
    id 45
    label "purchase"
  ]
  node [
    id 46
    label "buying"
  ]
  node [
    id 47
    label "wkupywanie_si&#281;"
  ]
  node [
    id 48
    label "wierzenie"
  ]
  node [
    id 49
    label "wykupywanie"
  ]
  node [
    id 50
    label "handlowanie"
  ]
  node [
    id 51
    label "pozyskiwanie"
  ]
  node [
    id 52
    label "ustawianie"
  ]
  node [
    id 53
    label "importowanie"
  ]
  node [
    id 54
    label "granie"
  ]
  node [
    id 55
    label "badanie"
  ]
  node [
    id 56
    label "bycie"
  ]
  node [
    id 57
    label "jedzenie"
  ]
  node [
    id 58
    label "kiperstwo"
  ]
  node [
    id 59
    label "tasting"
  ]
  node [
    id 60
    label "zaznawanie"
  ]
  node [
    id 61
    label "estimate"
  ]
  node [
    id 62
    label "policzy&#263;"
  ]
  node [
    id 63
    label "ustali&#263;"
  ]
  node [
    id 64
    label "by&#263;"
  ]
  node [
    id 65
    label "try"
  ]
  node [
    id 66
    label "savor"
  ]
  node [
    id 67
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 68
    label "doznawa&#263;"
  ]
  node [
    id 69
    label "essay"
  ]
  node [
    id 70
    label "poleci&#263;"
  ]
  node [
    id 71
    label "train"
  ]
  node [
    id 72
    label "wezwa&#263;"
  ]
  node [
    id 73
    label "trip"
  ]
  node [
    id 74
    label "spowodowa&#263;"
  ]
  node [
    id 75
    label "oznajmi&#263;"
  ]
  node [
    id 76
    label "revolutionize"
  ]
  node [
    id 77
    label "przetworzy&#263;"
  ]
  node [
    id 78
    label "wydali&#263;"
  ]
  node [
    id 79
    label "arouse"
  ]
  node [
    id 80
    label "nakaza&#263;"
  ]
  node [
    id 81
    label "invite"
  ]
  node [
    id 82
    label "zach&#281;ci&#263;"
  ]
  node [
    id 83
    label "poinformowa&#263;"
  ]
  node [
    id 84
    label "przewo&#322;a&#263;"
  ]
  node [
    id 85
    label "adduce"
  ]
  node [
    id 86
    label "ask"
  ]
  node [
    id 87
    label "poprosi&#263;"
  ]
  node [
    id 88
    label "powierzy&#263;"
  ]
  node [
    id 89
    label "wyda&#263;"
  ]
  node [
    id 90
    label "doradzi&#263;"
  ]
  node [
    id 91
    label "commend"
  ]
  node [
    id 92
    label "charge"
  ]
  node [
    id 93
    label "zada&#263;"
  ]
  node [
    id 94
    label "zaordynowa&#263;"
  ]
  node [
    id 95
    label "zrobi&#263;"
  ]
  node [
    id 96
    label "usun&#261;&#263;"
  ]
  node [
    id 97
    label "sack"
  ]
  node [
    id 98
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 99
    label "restore"
  ]
  node [
    id 100
    label "wyzyska&#263;"
  ]
  node [
    id 101
    label "opracowa&#263;"
  ]
  node [
    id 102
    label "convert"
  ]
  node [
    id 103
    label "stworzy&#263;"
  ]
  node [
    id 104
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 105
    label "act"
  ]
  node [
    id 106
    label "advise"
  ]
  node [
    id 107
    label "godzina"
  ]
  node [
    id 108
    label "time"
  ]
  node [
    id 109
    label "doba"
  ]
  node [
    id 110
    label "p&#243;&#322;godzina"
  ]
  node [
    id 111
    label "jednostka_czasu"
  ]
  node [
    id 112
    label "czas"
  ]
  node [
    id 113
    label "minuta"
  ]
  node [
    id 114
    label "kwadrans"
  ]
  node [
    id 115
    label "nazewnictwo"
  ]
  node [
    id 116
    label "term"
  ]
  node [
    id 117
    label "przypadni&#281;cie"
  ]
  node [
    id 118
    label "ekspiracja"
  ]
  node [
    id 119
    label "przypa&#347;&#263;"
  ]
  node [
    id 120
    label "chronogram"
  ]
  node [
    id 121
    label "praktyka"
  ]
  node [
    id 122
    label "nazwa"
  ]
  node [
    id 123
    label "wezwanie"
  ]
  node [
    id 124
    label "patron"
  ]
  node [
    id 125
    label "leksem"
  ]
  node [
    id 126
    label "poprzedzanie"
  ]
  node [
    id 127
    label "czasoprzestrze&#324;"
  ]
  node [
    id 128
    label "laba"
  ]
  node [
    id 129
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 130
    label "chronometria"
  ]
  node [
    id 131
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 132
    label "rachuba_czasu"
  ]
  node [
    id 133
    label "przep&#322;ywanie"
  ]
  node [
    id 134
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 135
    label "czasokres"
  ]
  node [
    id 136
    label "odczyt"
  ]
  node [
    id 137
    label "chwila"
  ]
  node [
    id 138
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 139
    label "dzieje"
  ]
  node [
    id 140
    label "kategoria_gramatyczna"
  ]
  node [
    id 141
    label "poprzedzenie"
  ]
  node [
    id 142
    label "trawienie"
  ]
  node [
    id 143
    label "pochodzi&#263;"
  ]
  node [
    id 144
    label "period"
  ]
  node [
    id 145
    label "okres_czasu"
  ]
  node [
    id 146
    label "poprzedza&#263;"
  ]
  node [
    id 147
    label "schy&#322;ek"
  ]
  node [
    id 148
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 149
    label "odwlekanie_si&#281;"
  ]
  node [
    id 150
    label "zegar"
  ]
  node [
    id 151
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 152
    label "czwarty_wymiar"
  ]
  node [
    id 153
    label "pochodzenie"
  ]
  node [
    id 154
    label "koniugacja"
  ]
  node [
    id 155
    label "Zeitgeist"
  ]
  node [
    id 156
    label "trawi&#263;"
  ]
  node [
    id 157
    label "pogoda"
  ]
  node [
    id 158
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 159
    label "poprzedzi&#263;"
  ]
  node [
    id 160
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 161
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 162
    label "time_period"
  ]
  node [
    id 163
    label "practice"
  ]
  node [
    id 164
    label "wiedza"
  ]
  node [
    id 165
    label "znawstwo"
  ]
  node [
    id 166
    label "skill"
  ]
  node [
    id 167
    label "czyn"
  ]
  node [
    id 168
    label "nauka"
  ]
  node [
    id 169
    label "zwyczaj"
  ]
  node [
    id 170
    label "eksperiencja"
  ]
  node [
    id 171
    label "praca"
  ]
  node [
    id 172
    label "s&#322;ownictwo"
  ]
  node [
    id 173
    label "terminology"
  ]
  node [
    id 174
    label "wydech"
  ]
  node [
    id 175
    label "ekspirowanie"
  ]
  node [
    id 176
    label "spodoba&#263;_si&#281;"
  ]
  node [
    id 177
    label "trafi&#263;_si&#281;"
  ]
  node [
    id 178
    label "fall"
  ]
  node [
    id 179
    label "pa&#347;&#263;"
  ]
  node [
    id 180
    label "dotrze&#263;"
  ]
  node [
    id 181
    label "wypa&#347;&#263;"
  ]
  node [
    id 182
    label "przywrze&#263;"
  ]
  node [
    id 183
    label "rzuci&#263;_si&#281;"
  ]
  node [
    id 184
    label "zapis"
  ]
  node [
    id 185
    label "barok"
  ]
  node [
    id 186
    label "przytulenie_si&#281;"
  ]
  node [
    id 187
    label "spadni&#281;cie"
  ]
  node [
    id 188
    label "zdarzenie_si&#281;"
  ]
  node [
    id 189
    label "okrojenie_si&#281;"
  ]
  node [
    id 190
    label "prolapse"
  ]
  node [
    id 191
    label "przetarg"
  ]
  node [
    id 192
    label "rozdanie"
  ]
  node [
    id 193
    label "pas"
  ]
  node [
    id 194
    label "sprzeda&#380;"
  ]
  node [
    id 195
    label "bryd&#380;"
  ]
  node [
    id 196
    label "tysi&#261;c"
  ]
  node [
    id 197
    label "skat"
  ]
  node [
    id 198
    label "gra_w_karty"
  ]
  node [
    id 199
    label "przeniesienie_praw"
  ]
  node [
    id 200
    label "przeda&#380;"
  ]
  node [
    id 201
    label "transakcja"
  ]
  node [
    id 202
    label "sprzedaj&#261;cy"
  ]
  node [
    id 203
    label "rabat"
  ]
  node [
    id 204
    label "cykl_astronomiczny"
  ]
  node [
    id 205
    label "coil"
  ]
  node [
    id 206
    label "zjawisko"
  ]
  node [
    id 207
    label "fotoelement"
  ]
  node [
    id 208
    label "komutowanie"
  ]
  node [
    id 209
    label "stan_skupienia"
  ]
  node [
    id 210
    label "nastr&#243;j"
  ]
  node [
    id 211
    label "przerywacz"
  ]
  node [
    id 212
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 213
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 214
    label "kraw&#281;d&#378;"
  ]
  node [
    id 215
    label "obsesja"
  ]
  node [
    id 216
    label "dw&#243;jnik"
  ]
  node [
    id 217
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 218
    label "okres"
  ]
  node [
    id 219
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 220
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 221
    label "przew&#243;d"
  ]
  node [
    id 222
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 223
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 224
    label "obw&#243;d"
  ]
  node [
    id 225
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 226
    label "degree"
  ]
  node [
    id 227
    label "komutowa&#263;"
  ]
  node [
    id 228
    label "danie"
  ]
  node [
    id 229
    label "porozdawanie"
  ]
  node [
    id 230
    label "distribute"
  ]
  node [
    id 231
    label "runda"
  ]
  node [
    id 232
    label "kwota"
  ]
  node [
    id 233
    label "liczba"
  ]
  node [
    id 234
    label "molarity"
  ]
  node [
    id 235
    label "tauzen"
  ]
  node [
    id 236
    label "patyk"
  ]
  node [
    id 237
    label "musik"
  ]
  node [
    id 238
    label "&#347;piew"
  ]
  node [
    id 239
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 240
    label "inwit"
  ]
  node [
    id 241
    label "odzywanie_si&#281;"
  ]
  node [
    id 242
    label "rekontra"
  ]
  node [
    id 243
    label "odezwanie_si&#281;"
  ]
  node [
    id 244
    label "longer"
  ]
  node [
    id 245
    label "odwrotka"
  ]
  node [
    id 246
    label "sport"
  ]
  node [
    id 247
    label "rober"
  ]
  node [
    id 248
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 249
    label "korona"
  ]
  node [
    id 250
    label "kontrakt"
  ]
  node [
    id 251
    label "sale"
  ]
  node [
    id 252
    label "konkurs"
  ]
  node [
    id 253
    label "przybitka"
  ]
  node [
    id 254
    label "sp&#243;r"
  ]
  node [
    id 255
    label "auction"
  ]
  node [
    id 256
    label "dodatek"
  ]
  node [
    id 257
    label "linia"
  ]
  node [
    id 258
    label "kawa&#322;ek"
  ]
  node [
    id 259
    label "figura_heraldyczna"
  ]
  node [
    id 260
    label "wci&#281;cie"
  ]
  node [
    id 261
    label "obszar"
  ]
  node [
    id 262
    label "bielizna"
  ]
  node [
    id 263
    label "miejsce"
  ]
  node [
    id 264
    label "sk&#322;ad"
  ]
  node [
    id 265
    label "obiekt"
  ]
  node [
    id 266
    label "zagranie"
  ]
  node [
    id 267
    label "heraldyka"
  ]
  node [
    id 268
    label "odznaka"
  ]
  node [
    id 269
    label "tarcza_herbowa"
  ]
  node [
    id 270
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 271
    label "nap&#281;d"
  ]
  node [
    id 272
    label "podnosi&#263;"
  ]
  node [
    id 273
    label "liczy&#263;"
  ]
  node [
    id 274
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 275
    label "zanosi&#263;"
  ]
  node [
    id 276
    label "rozpowszechnia&#263;"
  ]
  node [
    id 277
    label "ujawnia&#263;"
  ]
  node [
    id 278
    label "otrzymywa&#263;"
  ]
  node [
    id 279
    label "kra&#347;&#263;"
  ]
  node [
    id 280
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 281
    label "raise"
  ]
  node [
    id 282
    label "odsuwa&#263;"
  ]
  node [
    id 283
    label "nagradza&#263;"
  ]
  node [
    id 284
    label "forytowa&#263;"
  ]
  node [
    id 285
    label "traktowa&#263;"
  ]
  node [
    id 286
    label "sign"
  ]
  node [
    id 287
    label "robi&#263;"
  ]
  node [
    id 288
    label "podpierdala&#263;"
  ]
  node [
    id 289
    label "mie&#263;_lepkie_r&#281;ce"
  ]
  node [
    id 290
    label "r&#261;ba&#263;"
  ]
  node [
    id 291
    label "podsuwa&#263;"
  ]
  node [
    id 292
    label "overcharge"
  ]
  node [
    id 293
    label "zar&#261;bywa&#263;"
  ]
  node [
    id 294
    label "przyw&#322;aszcza&#263;"
  ]
  node [
    id 295
    label "return"
  ]
  node [
    id 296
    label "dostawa&#263;"
  ]
  node [
    id 297
    label "take"
  ]
  node [
    id 298
    label "wytwarza&#263;"
  ]
  node [
    id 299
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 300
    label "przejmowa&#263;"
  ]
  node [
    id 301
    label "saturate"
  ]
  node [
    id 302
    label "report"
  ]
  node [
    id 303
    label "dyskalkulia"
  ]
  node [
    id 304
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 305
    label "wynagrodzenie"
  ]
  node [
    id 306
    label "osi&#261;ga&#263;"
  ]
  node [
    id 307
    label "wymienia&#263;"
  ]
  node [
    id 308
    label "posiada&#263;"
  ]
  node [
    id 309
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 310
    label "wycenia&#263;"
  ]
  node [
    id 311
    label "bra&#263;"
  ]
  node [
    id 312
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 313
    label "mierzy&#263;"
  ]
  node [
    id 314
    label "rachowa&#263;"
  ]
  node [
    id 315
    label "count"
  ]
  node [
    id 316
    label "tell"
  ]
  node [
    id 317
    label "odlicza&#263;"
  ]
  node [
    id 318
    label "dodawa&#263;"
  ]
  node [
    id 319
    label "wyznacza&#263;"
  ]
  node [
    id 320
    label "admit"
  ]
  node [
    id 321
    label "policza&#263;"
  ]
  node [
    id 322
    label "okre&#347;la&#263;"
  ]
  node [
    id 323
    label "dostarcza&#263;"
  ]
  node [
    id 324
    label "kry&#263;"
  ]
  node [
    id 325
    label "get"
  ]
  node [
    id 326
    label "przenosi&#263;"
  ]
  node [
    id 327
    label "usi&#322;owa&#263;"
  ]
  node [
    id 328
    label "remove"
  ]
  node [
    id 329
    label "seclude"
  ]
  node [
    id 330
    label "przestawa&#263;"
  ]
  node [
    id 331
    label "przemieszcza&#263;"
  ]
  node [
    id 332
    label "przesuwa&#263;"
  ]
  node [
    id 333
    label "oddala&#263;"
  ]
  node [
    id 334
    label "dissolve"
  ]
  node [
    id 335
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 336
    label "retard"
  ]
  node [
    id 337
    label "blurt_out"
  ]
  node [
    id 338
    label "odci&#261;ga&#263;"
  ]
  node [
    id 339
    label "odk&#322;ada&#263;"
  ]
  node [
    id 340
    label "demaskator"
  ]
  node [
    id 341
    label "dostrzega&#263;"
  ]
  node [
    id 342
    label "objawia&#263;"
  ]
  node [
    id 343
    label "unwrap"
  ]
  node [
    id 344
    label "informowa&#263;"
  ]
  node [
    id 345
    label "indicate"
  ]
  node [
    id 346
    label "generalize"
  ]
  node [
    id 347
    label "sprawia&#263;"
  ]
  node [
    id 348
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 349
    label "zaczyna&#263;"
  ]
  node [
    id 350
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 351
    label "escalate"
  ]
  node [
    id 352
    label "pia&#263;"
  ]
  node [
    id 353
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 354
    label "przybli&#380;a&#263;"
  ]
  node [
    id 355
    label "ulepsza&#263;"
  ]
  node [
    id 356
    label "tire"
  ]
  node [
    id 357
    label "pomaga&#263;"
  ]
  node [
    id 358
    label "express"
  ]
  node [
    id 359
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 360
    label "chwali&#263;"
  ]
  node [
    id 361
    label "rise"
  ]
  node [
    id 362
    label "os&#322;awia&#263;"
  ]
  node [
    id 363
    label "odbudowywa&#263;"
  ]
  node [
    id 364
    label "drive"
  ]
  node [
    id 365
    label "zmienia&#263;"
  ]
  node [
    id 366
    label "enhance"
  ]
  node [
    id 367
    label "za&#322;apywa&#263;"
  ]
  node [
    id 368
    label "lift"
  ]
  node [
    id 369
    label "wynie&#347;&#263;"
  ]
  node [
    id 370
    label "pieni&#261;dze"
  ]
  node [
    id 371
    label "ilo&#347;&#263;"
  ]
  node [
    id 372
    label "limit"
  ]
  node [
    id 373
    label "pos&#322;uchanie"
  ]
  node [
    id 374
    label "skumanie"
  ]
  node [
    id 375
    label "orientacja"
  ]
  node [
    id 376
    label "wytw&#243;r"
  ]
  node [
    id 377
    label "zorientowanie"
  ]
  node [
    id 378
    label "teoria"
  ]
  node [
    id 379
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 380
    label "clasp"
  ]
  node [
    id 381
    label "forma"
  ]
  node [
    id 382
    label "przem&#243;wienie"
  ]
  node [
    id 383
    label "kartka"
  ]
  node [
    id 384
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 385
    label "logowanie"
  ]
  node [
    id 386
    label "plik"
  ]
  node [
    id 387
    label "s&#261;d"
  ]
  node [
    id 388
    label "adres_internetowy"
  ]
  node [
    id 389
    label "serwis_internetowy"
  ]
  node [
    id 390
    label "posta&#263;"
  ]
  node [
    id 391
    label "bok"
  ]
  node [
    id 392
    label "skr&#281;canie"
  ]
  node [
    id 393
    label "skr&#281;ca&#263;"
  ]
  node [
    id 394
    label "orientowanie"
  ]
  node [
    id 395
    label "skr&#281;ci&#263;"
  ]
  node [
    id 396
    label "uj&#281;cie"
  ]
  node [
    id 397
    label "ty&#322;"
  ]
  node [
    id 398
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 399
    label "fragment"
  ]
  node [
    id 400
    label "layout"
  ]
  node [
    id 401
    label "zorientowa&#263;"
  ]
  node [
    id 402
    label "pagina"
  ]
  node [
    id 403
    label "podmiot"
  ]
  node [
    id 404
    label "g&#243;ra"
  ]
  node [
    id 405
    label "orientowa&#263;"
  ]
  node [
    id 406
    label "voice"
  ]
  node [
    id 407
    label "prz&#243;d"
  ]
  node [
    id 408
    label "internet"
  ]
  node [
    id 409
    label "powierzchnia"
  ]
  node [
    id 410
    label "skr&#281;cenie"
  ]
  node [
    id 411
    label "charakterystyka"
  ]
  node [
    id 412
    label "m&#322;ot"
  ]
  node [
    id 413
    label "znak"
  ]
  node [
    id 414
    label "drzewo"
  ]
  node [
    id 415
    label "pr&#243;ba"
  ]
  node [
    id 416
    label "attribute"
  ]
  node [
    id 417
    label "marka"
  ]
  node [
    id 418
    label "punkt"
  ]
  node [
    id 419
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 420
    label "rezultat"
  ]
  node [
    id 421
    label "thing"
  ]
  node [
    id 422
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 423
    label "rzecz"
  ]
  node [
    id 424
    label "warunek_lokalowy"
  ]
  node [
    id 425
    label "circumference"
  ]
  node [
    id 426
    label "odzie&#380;"
  ]
  node [
    id 427
    label "znaczenie"
  ]
  node [
    id 428
    label "dymensja"
  ]
  node [
    id 429
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 430
    label "variable"
  ]
  node [
    id 431
    label "wielko&#347;&#263;"
  ]
  node [
    id 432
    label "podniesienie"
  ]
  node [
    id 433
    label "zaleta"
  ]
  node [
    id 434
    label "warto&#347;ciowy"
  ]
  node [
    id 435
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 436
    label "dobro"
  ]
  node [
    id 437
    label "czynnik"
  ]
  node [
    id 438
    label "przedmiot"
  ]
  node [
    id 439
    label "magnes"
  ]
  node [
    id 440
    label "appreciate"
  ]
  node [
    id 441
    label "podnie&#347;&#263;"
  ]
  node [
    id 442
    label "przewarto&#347;ciowywanie"
  ]
  node [
    id 443
    label "podnoszenie"
  ]
  node [
    id 444
    label "command"
  ]
  node [
    id 445
    label "wywodzenie"
  ]
  node [
    id 446
    label "wyraz"
  ]
  node [
    id 447
    label "pokierowanie"
  ]
  node [
    id 448
    label "wywiedzenie"
  ]
  node [
    id 449
    label "wybieranie"
  ]
  node [
    id 450
    label "podkre&#347;lanie"
  ]
  node [
    id 451
    label "pokazywanie"
  ]
  node [
    id 452
    label "show"
  ]
  node [
    id 453
    label "assignment"
  ]
  node [
    id 454
    label "t&#322;umaczenie"
  ]
  node [
    id 455
    label "indication"
  ]
  node [
    id 456
    label "podawanie"
  ]
  node [
    id 457
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 458
    label "set"
  ]
  node [
    id 459
    label "podkre&#347;la&#263;"
  ]
  node [
    id 460
    label "podawa&#263;"
  ]
  node [
    id 461
    label "pokazywa&#263;"
  ]
  node [
    id 462
    label "wybiera&#263;"
  ]
  node [
    id 463
    label "signify"
  ]
  node [
    id 464
    label "represent"
  ]
  node [
    id 465
    label "szacunkowo"
  ]
  node [
    id 466
    label "przybli&#380;ony"
  ]
  node [
    id 467
    label "niedok&#322;adny"
  ]
  node [
    id 468
    label "niedok&#322;adnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
]
