graph [
  node [
    id 0
    label "podatek"
    origin "text"
  ]
  node [
    id 1
    label "progresywny"
    origin "text"
  ]
  node [
    id 2
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 3
    label "bilans_handlowy"
  ]
  node [
    id 4
    label "trybut"
  ]
  node [
    id 5
    label "op&#322;ata"
  ]
  node [
    id 6
    label "danina"
  ]
  node [
    id 7
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 8
    label "kwota"
  ]
  node [
    id 9
    label "&#347;wiadczenie"
  ]
  node [
    id 10
    label "nowoczesny"
  ]
  node [
    id 11
    label "progresywnie"
  ]
  node [
    id 12
    label "ambitny"
  ]
  node [
    id 13
    label "stopniowy"
  ]
  node [
    id 14
    label "skomplikowany"
  ]
  node [
    id 15
    label "innowacyjny"
  ]
  node [
    id 16
    label "rozwojowy"
  ]
  node [
    id 17
    label "ci&#281;&#380;ki"
  ]
  node [
    id 18
    label "zdeterminowany"
  ]
  node [
    id 19
    label "ambitnie"
  ]
  node [
    id 20
    label "wymagaj&#261;cy"
  ]
  node [
    id 21
    label "wysokich_lot&#243;w"
  ]
  node [
    id 22
    label "trudny"
  ]
  node [
    id 23
    label "samodzielny"
  ]
  node [
    id 24
    label "&#347;mia&#322;y"
  ]
  node [
    id 25
    label "skomplikowanie"
  ]
  node [
    id 26
    label "nowo&#380;ytny"
  ]
  node [
    id 27
    label "nowy"
  ]
  node [
    id 28
    label "nowocze&#347;nie"
  ]
  node [
    id 29
    label "otwarty"
  ]
  node [
    id 30
    label "innowacyjnie"
  ]
  node [
    id 31
    label "nowatorski"
  ]
  node [
    id 32
    label "niekonwencjonalny"
  ]
  node [
    id 33
    label "zmienny"
  ]
  node [
    id 34
    label "linearny"
  ]
  node [
    id 35
    label "stopniowo"
  ]
  node [
    id 36
    label "systematycznie"
  ]
  node [
    id 37
    label "odpowiednio"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
]
