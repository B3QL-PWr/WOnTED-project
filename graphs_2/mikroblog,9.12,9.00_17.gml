graph [
  node [
    id 0
    label "fa&#322;szywy"
    origin "text"
  ]
  node [
    id 1
    label "prorok"
    origin "text"
  ]
  node [
    id 2
    label "nieprawdziwie"
  ]
  node [
    id 3
    label "niezgodny"
  ]
  node [
    id 4
    label "fa&#322;szywie"
  ]
  node [
    id 5
    label "przebieg&#322;y"
  ]
  node [
    id 6
    label "nieharmonijny"
  ]
  node [
    id 7
    label "nieszczery"
  ]
  node [
    id 8
    label "r&#243;&#380;ny"
  ]
  node [
    id 9
    label "niespokojny"
  ]
  node [
    id 10
    label "odmienny"
  ]
  node [
    id 11
    label "k&#322;&#243;tny"
  ]
  node [
    id 12
    label "niezgodnie"
  ]
  node [
    id 13
    label "napi&#281;ty"
  ]
  node [
    id 14
    label "oszuka&#324;czy"
  ]
  node [
    id 15
    label "nieuczciwy"
  ]
  node [
    id 16
    label "nieszczerze"
  ]
  node [
    id 17
    label "k&#322;amany"
  ]
  node [
    id 18
    label "niebezpieczny"
  ]
  node [
    id 19
    label "nieprzewidywalny"
  ]
  node [
    id 20
    label "zmy&#347;lny"
  ]
  node [
    id 21
    label "przebiegle"
  ]
  node [
    id 22
    label "sprytny"
  ]
  node [
    id 23
    label "zwodny"
  ]
  node [
    id 24
    label "podst&#281;pnie"
  ]
  node [
    id 25
    label "nieharmonijnie"
  ]
  node [
    id 26
    label "nieprawdziwy"
  ]
  node [
    id 27
    label "fictitiously"
  ]
  node [
    id 28
    label "jasnowidz"
  ]
  node [
    id 29
    label "mistyk"
  ]
  node [
    id 30
    label "Moj&#380;esz"
  ]
  node [
    id 31
    label "Mahomet"
  ]
  node [
    id 32
    label "&#347;w"
  ]
  node [
    id 33
    label "Salomon"
  ]
  node [
    id 34
    label "parapsycholog"
  ]
  node [
    id 35
    label "wr&#243;&#380;biarz"
  ]
  node [
    id 36
    label "cz&#322;owiek"
  ]
  node [
    id 37
    label "zwolennik"
  ]
  node [
    id 38
    label "wyznawca"
  ]
  node [
    id 39
    label "metapsychik"
  ]
  node [
    id 40
    label "Towia&#324;ski"
  ]
  node [
    id 41
    label "Synaj"
  ]
  node [
    id 42
    label "dekalog"
  ]
  node [
    id 43
    label "mirad&#380;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
]
