graph [
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "syn"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "rodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "usynowienie"
  ]
  node [
    id 5
    label "dziecko"
  ]
  node [
    id 6
    label "usynawianie"
  ]
  node [
    id 7
    label "utulenie"
  ]
  node [
    id 8
    label "pediatra"
  ]
  node [
    id 9
    label "dzieciak"
  ]
  node [
    id 10
    label "utulanie"
  ]
  node [
    id 11
    label "dzieciarnia"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "niepe&#322;noletni"
  ]
  node [
    id 14
    label "organizm"
  ]
  node [
    id 15
    label "utula&#263;"
  ]
  node [
    id 16
    label "cz&#322;owieczek"
  ]
  node [
    id 17
    label "fledgling"
  ]
  node [
    id 18
    label "zwierz&#281;"
  ]
  node [
    id 19
    label "utuli&#263;"
  ]
  node [
    id 20
    label "m&#322;odzik"
  ]
  node [
    id 21
    label "pedofil"
  ]
  node [
    id 22
    label "m&#322;odziak"
  ]
  node [
    id 23
    label "potomek"
  ]
  node [
    id 24
    label "entliczek-pentliczek"
  ]
  node [
    id 25
    label "potomstwo"
  ]
  node [
    id 26
    label "sraluch"
  ]
  node [
    id 27
    label "przysposabianie"
  ]
  node [
    id 28
    label "przysposobienie"
  ]
  node [
    id 29
    label "adoption"
  ]
  node [
    id 30
    label "create"
  ]
  node [
    id 31
    label "plon"
  ]
  node [
    id 32
    label "give"
  ]
  node [
    id 33
    label "wytwarza&#263;"
  ]
  node [
    id 34
    label "robi&#263;"
  ]
  node [
    id 35
    label "return"
  ]
  node [
    id 36
    label "wydawa&#263;"
  ]
  node [
    id 37
    label "metr"
  ]
  node [
    id 38
    label "wyda&#263;"
  ]
  node [
    id 39
    label "rezultat"
  ]
  node [
    id 40
    label "produkcja"
  ]
  node [
    id 41
    label "naturalia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
]
