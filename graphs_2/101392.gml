graph [
  node [
    id 0
    label "potok"
    origin "text"
  ]
  node [
    id 1
    label "funkcja"
    origin "text"
  ]
  node [
    id 2
    label "pow&#322;ok"
    origin "text"
  ]
  node [
    id 3
    label "flood"
  ]
  node [
    id 4
    label "ruch"
  ]
  node [
    id 5
    label "fala"
  ]
  node [
    id 6
    label "mn&#243;stwo"
  ]
  node [
    id 7
    label "ciek_wodny"
  ]
  node [
    id 8
    label "enormousness"
  ]
  node [
    id 9
    label "ilo&#347;&#263;"
  ]
  node [
    id 10
    label "stream"
  ]
  node [
    id 11
    label "rozbijanie_si&#281;"
  ]
  node [
    id 12
    label "efekt_Dopplera"
  ]
  node [
    id 13
    label "przemoc"
  ]
  node [
    id 14
    label "grzywa_fali"
  ]
  node [
    id 15
    label "strumie&#324;"
  ]
  node [
    id 16
    label "obcinka"
  ]
  node [
    id 17
    label "zafalowanie"
  ]
  node [
    id 18
    label "zjawisko"
  ]
  node [
    id 19
    label "znak_diakrytyczny"
  ]
  node [
    id 20
    label "clutter"
  ]
  node [
    id 21
    label "fit"
  ]
  node [
    id 22
    label "reakcja"
  ]
  node [
    id 23
    label "rozbicie_si&#281;"
  ]
  node [
    id 24
    label "okres"
  ]
  node [
    id 25
    label "zafalowa&#263;"
  ]
  node [
    id 26
    label "woda"
  ]
  node [
    id 27
    label "t&#322;um"
  ]
  node [
    id 28
    label "kot"
  ]
  node [
    id 29
    label "wojsko"
  ]
  node [
    id 30
    label "pasemko"
  ]
  node [
    id 31
    label "karb"
  ]
  node [
    id 32
    label "kszta&#322;t"
  ]
  node [
    id 33
    label "czo&#322;o_fali"
  ]
  node [
    id 34
    label "move"
  ]
  node [
    id 35
    label "zmiana"
  ]
  node [
    id 36
    label "model"
  ]
  node [
    id 37
    label "aktywno&#347;&#263;"
  ]
  node [
    id 38
    label "utrzymywanie"
  ]
  node [
    id 39
    label "utrzymywa&#263;"
  ]
  node [
    id 40
    label "taktyka"
  ]
  node [
    id 41
    label "d&#322;ugi"
  ]
  node [
    id 42
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 43
    label "natural_process"
  ]
  node [
    id 44
    label "kanciasty"
  ]
  node [
    id 45
    label "utrzyma&#263;"
  ]
  node [
    id 46
    label "myk"
  ]
  node [
    id 47
    label "manewr"
  ]
  node [
    id 48
    label "utrzymanie"
  ]
  node [
    id 49
    label "wydarzenie"
  ]
  node [
    id 50
    label "tumult"
  ]
  node [
    id 51
    label "stopek"
  ]
  node [
    id 52
    label "movement"
  ]
  node [
    id 53
    label "czynno&#347;&#263;"
  ]
  node [
    id 54
    label "komunikacja"
  ]
  node [
    id 55
    label "lokomocja"
  ]
  node [
    id 56
    label "drift"
  ]
  node [
    id 57
    label "commercial_enterprise"
  ]
  node [
    id 58
    label "apraksja"
  ]
  node [
    id 59
    label "proces"
  ]
  node [
    id 60
    label "poruszenie"
  ]
  node [
    id 61
    label "mechanika"
  ]
  node [
    id 62
    label "travel"
  ]
  node [
    id 63
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 64
    label "dyssypacja_energii"
  ]
  node [
    id 65
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 66
    label "kr&#243;tki"
  ]
  node [
    id 67
    label "wakowa&#263;"
  ]
  node [
    id 68
    label "praca"
  ]
  node [
    id 69
    label "zastosowanie"
  ]
  node [
    id 70
    label "dziedzina"
  ]
  node [
    id 71
    label "czyn"
  ]
  node [
    id 72
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 73
    label "znaczenie"
  ]
  node [
    id 74
    label "matematyka"
  ]
  node [
    id 75
    label "awansowanie"
  ]
  node [
    id 76
    label "awansowa&#263;"
  ]
  node [
    id 77
    label "przeciwdziedzina"
  ]
  node [
    id 78
    label "powierzanie"
  ]
  node [
    id 79
    label "function"
  ]
  node [
    id 80
    label "rzut"
  ]
  node [
    id 81
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 82
    label "stawia&#263;"
  ]
  node [
    id 83
    label "addytywno&#347;&#263;"
  ]
  node [
    id 84
    label "postawi&#263;"
  ]
  node [
    id 85
    label "jednostka"
  ]
  node [
    id 86
    label "supremum"
  ]
  node [
    id 87
    label "cel"
  ]
  node [
    id 88
    label "funkcjonowanie"
  ]
  node [
    id 89
    label "infimum"
  ]
  node [
    id 90
    label "zaw&#243;d"
  ]
  node [
    id 91
    label "pracowanie"
  ]
  node [
    id 92
    label "pracowa&#263;"
  ]
  node [
    id 93
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 94
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 95
    label "czynnik_produkcji"
  ]
  node [
    id 96
    label "miejsce"
  ]
  node [
    id 97
    label "stosunek_pracy"
  ]
  node [
    id 98
    label "kierownictwo"
  ]
  node [
    id 99
    label "najem"
  ]
  node [
    id 100
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 101
    label "siedziba"
  ]
  node [
    id 102
    label "zak&#322;ad"
  ]
  node [
    id 103
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 104
    label "tynkarski"
  ]
  node [
    id 105
    label "tyrka"
  ]
  node [
    id 106
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 107
    label "benedykty&#324;ski"
  ]
  node [
    id 108
    label "poda&#380;_pracy"
  ]
  node [
    id 109
    label "wytw&#243;r"
  ]
  node [
    id 110
    label "zobowi&#261;zanie"
  ]
  node [
    id 111
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 112
    label "rzecz"
  ]
  node [
    id 113
    label "punkt"
  ]
  node [
    id 114
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 115
    label "thing"
  ]
  node [
    id 116
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 117
    label "rezultat"
  ]
  node [
    id 118
    label "istota"
  ]
  node [
    id 119
    label "odgrywanie_roli"
  ]
  node [
    id 120
    label "bycie"
  ]
  node [
    id 121
    label "assay"
  ]
  node [
    id 122
    label "wskazywanie"
  ]
  node [
    id 123
    label "wyraz"
  ]
  node [
    id 124
    label "cecha"
  ]
  node [
    id 125
    label "command"
  ]
  node [
    id 126
    label "gravity"
  ]
  node [
    id 127
    label "condition"
  ]
  node [
    id 128
    label "informacja"
  ]
  node [
    id 129
    label "weight"
  ]
  node [
    id 130
    label "okre&#347;lanie"
  ]
  node [
    id 131
    label "odk&#322;adanie"
  ]
  node [
    id 132
    label "liczenie"
  ]
  node [
    id 133
    label "wyra&#380;enie"
  ]
  node [
    id 134
    label "kto&#347;"
  ]
  node [
    id 135
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 136
    label "stawianie"
  ]
  node [
    id 137
    label "oddawanie"
  ]
  node [
    id 138
    label "wyznawanie"
  ]
  node [
    id 139
    label "ufanie"
  ]
  node [
    id 140
    label "stanowisko"
  ]
  node [
    id 141
    label "zadanie"
  ]
  node [
    id 142
    label "zlecanie"
  ]
  node [
    id 143
    label "habilitowanie_si&#281;"
  ]
  node [
    id 144
    label "przeniesienie"
  ]
  node [
    id 145
    label "przechodzenie"
  ]
  node [
    id 146
    label "przenoszenie"
  ]
  node [
    id 147
    label "kariera"
  ]
  node [
    id 148
    label "pozyskanie"
  ]
  node [
    id 149
    label "pozyskiwanie"
  ]
  node [
    id 150
    label "obj&#281;cie"
  ]
  node [
    id 151
    label "pi&#281;cie_si&#281;"
  ]
  node [
    id 152
    label "obejmowanie"
  ]
  node [
    id 153
    label "status"
  ]
  node [
    id 154
    label "promowanie"
  ]
  node [
    id 155
    label "przej&#347;cie"
  ]
  node [
    id 156
    label "obj&#261;&#263;"
  ]
  node [
    id 157
    label "pozyska&#263;"
  ]
  node [
    id 158
    label "pozyskiwa&#263;"
  ]
  node [
    id 159
    label "obejmowa&#263;"
  ]
  node [
    id 160
    label "pi&#261;&#263;_si&#281;"
  ]
  node [
    id 161
    label "da&#263;_awans"
  ]
  node [
    id 162
    label "przechodzi&#263;"
  ]
  node [
    id 163
    label "dawa&#263;_awans"
  ]
  node [
    id 164
    label "raise"
  ]
  node [
    id 165
    label "przej&#347;&#263;"
  ]
  node [
    id 166
    label "wolny"
  ]
  node [
    id 167
    label "by&#263;"
  ]
  node [
    id 168
    label "wyznacza&#263;"
  ]
  node [
    id 169
    label "introduce"
  ]
  node [
    id 170
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 171
    label "umieszcza&#263;"
  ]
  node [
    id 172
    label "ocenia&#263;"
  ]
  node [
    id 173
    label "obstawia&#263;"
  ]
  node [
    id 174
    label "pozostawia&#263;"
  ]
  node [
    id 175
    label "wytwarza&#263;"
  ]
  node [
    id 176
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 177
    label "go"
  ]
  node [
    id 178
    label "przedstawia&#263;"
  ]
  node [
    id 179
    label "czyni&#263;"
  ]
  node [
    id 180
    label "wydawa&#263;"
  ]
  node [
    id 181
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 182
    label "fundowa&#263;"
  ]
  node [
    id 183
    label "zmienia&#263;"
  ]
  node [
    id 184
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 185
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 186
    label "uruchamia&#263;"
  ]
  node [
    id 187
    label "powodowa&#263;"
  ]
  node [
    id 188
    label "przewidywa&#263;"
  ]
  node [
    id 189
    label "zastawia&#263;"
  ]
  node [
    id 190
    label "deliver"
  ]
  node [
    id 191
    label "znak"
  ]
  node [
    id 192
    label "przyznawa&#263;"
  ]
  node [
    id 193
    label "wskazywa&#263;"
  ]
  node [
    id 194
    label "wydobywa&#263;"
  ]
  node [
    id 195
    label "post"
  ]
  node [
    id 196
    label "zmieni&#263;"
  ]
  node [
    id 197
    label "oceni&#263;"
  ]
  node [
    id 198
    label "wydoby&#263;"
  ]
  node [
    id 199
    label "pozostawi&#263;"
  ]
  node [
    id 200
    label "establish"
  ]
  node [
    id 201
    label "umie&#347;ci&#263;"
  ]
  node [
    id 202
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 203
    label "plant"
  ]
  node [
    id 204
    label "zafundowa&#263;"
  ]
  node [
    id 205
    label "budowla"
  ]
  node [
    id 206
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 207
    label "obra&#263;"
  ]
  node [
    id 208
    label "uczyni&#263;"
  ]
  node [
    id 209
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 210
    label "spowodowa&#263;"
  ]
  node [
    id 211
    label "wskaza&#263;"
  ]
  node [
    id 212
    label "peddle"
  ]
  node [
    id 213
    label "obstawi&#263;"
  ]
  node [
    id 214
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 215
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 216
    label "wytworzy&#263;"
  ]
  node [
    id 217
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 218
    label "set"
  ]
  node [
    id 219
    label "uruchomi&#263;"
  ]
  node [
    id 220
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 221
    label "wyda&#263;"
  ]
  node [
    id 222
    label "przyzna&#263;"
  ]
  node [
    id 223
    label "stawi&#263;"
  ]
  node [
    id 224
    label "wyznaczy&#263;"
  ]
  node [
    id 225
    label "przedstawi&#263;"
  ]
  node [
    id 226
    label "rozpoznawalno&#347;&#263;"
  ]
  node [
    id 227
    label "rachunki"
  ]
  node [
    id 228
    label "topologia_algebraiczna"
  ]
  node [
    id 229
    label "forsing"
  ]
  node [
    id 230
    label "przedmiot"
  ]
  node [
    id 231
    label "matematyka_stosowana"
  ]
  node [
    id 232
    label "modelowanie_matematyczne"
  ]
  node [
    id 233
    label "matma"
  ]
  node [
    id 234
    label "teoria_katastrof"
  ]
  node [
    id 235
    label "rachunek_operatorowy"
  ]
  node [
    id 236
    label "fizyka_matematyczna"
  ]
  node [
    id 237
    label "logika"
  ]
  node [
    id 238
    label "matematyka_czysta"
  ]
  node [
    id 239
    label "teoria_graf&#243;w"
  ]
  node [
    id 240
    label "logicyzm"
  ]
  node [
    id 241
    label "kryptologia"
  ]
  node [
    id 242
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 243
    label "kierunek"
  ]
  node [
    id 244
    label "ograniczenie"
  ]
  node [
    id 245
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 246
    label "rysunek"
  ]
  node [
    id 247
    label "k&#322;ad"
  ]
  node [
    id 248
    label "injection"
  ]
  node [
    id 249
    label "scene"
  ]
  node [
    id 250
    label "odwzorowanie"
  ]
  node [
    id 251
    label "pomys&#322;"
  ]
  node [
    id 252
    label "nawr&#243;t_choroby"
  ]
  node [
    id 253
    label "throw"
  ]
  node [
    id 254
    label "float"
  ]
  node [
    id 255
    label "projection"
  ]
  node [
    id 256
    label "armia"
  ]
  node [
    id 257
    label "potomstwo"
  ]
  node [
    id 258
    label "mold"
  ]
  node [
    id 259
    label "blow"
  ]
  node [
    id 260
    label "poj&#281;cie"
  ]
  node [
    id 261
    label "wyewoluowanie"
  ]
  node [
    id 262
    label "przyswojenie"
  ]
  node [
    id 263
    label "one"
  ]
  node [
    id 264
    label "przelicza&#263;"
  ]
  node [
    id 265
    label "starzenie_si&#281;"
  ]
  node [
    id 266
    label "obiekt"
  ]
  node [
    id 267
    label "profanum"
  ]
  node [
    id 268
    label "skala"
  ]
  node [
    id 269
    label "przyswajanie"
  ]
  node [
    id 270
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 271
    label "przeliczanie"
  ]
  node [
    id 272
    label "homo_sapiens"
  ]
  node [
    id 273
    label "przeliczy&#263;"
  ]
  node [
    id 274
    label "osoba"
  ]
  node [
    id 275
    label "ludzko&#347;&#263;"
  ]
  node [
    id 276
    label "ewoluowanie"
  ]
  node [
    id 277
    label "ewoluowa&#263;"
  ]
  node [
    id 278
    label "czynnik_biotyczny"
  ]
  node [
    id 279
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 280
    label "portrecista"
  ]
  node [
    id 281
    label "przyswaja&#263;"
  ]
  node [
    id 282
    label "przeliczenie"
  ]
  node [
    id 283
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 284
    label "duch"
  ]
  node [
    id 285
    label "wyewoluowa&#263;"
  ]
  node [
    id 286
    label "antropochoria"
  ]
  node [
    id 287
    label "figura"
  ]
  node [
    id 288
    label "mikrokosmos"
  ]
  node [
    id 289
    label "g&#322;owa"
  ]
  node [
    id 290
    label "przyswoi&#263;"
  ]
  node [
    id 291
    label "individual"
  ]
  node [
    id 292
    label "oddzia&#322;ywanie"
  ]
  node [
    id 293
    label "liczba_naturalna"
  ]
  node [
    id 294
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 295
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 296
    label "stosowanie"
  ]
  node [
    id 297
    label "use"
  ]
  node [
    id 298
    label "zrobienie"
  ]
  node [
    id 299
    label "act"
  ]
  node [
    id 300
    label "nakr&#281;canie"
  ]
  node [
    id 301
    label "nakr&#281;cenie"
  ]
  node [
    id 302
    label "w&#322;&#261;czanie"
  ]
  node [
    id 303
    label "impact"
  ]
  node [
    id 304
    label "podtrzymywanie"
  ]
  node [
    id 305
    label "tr&#243;jstronny"
  ]
  node [
    id 306
    label "zatrzymanie"
  ]
  node [
    id 307
    label "uruchamianie"
  ]
  node [
    id 308
    label "w&#322;&#261;czenie"
  ]
  node [
    id 309
    label "dzianie_si&#281;"
  ]
  node [
    id 310
    label "uruchomienie"
  ]
  node [
    id 311
    label "zbi&#243;r"
  ]
  node [
    id 312
    label "zakres"
  ]
  node [
    id 313
    label "bezdro&#380;e"
  ]
  node [
    id 314
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 315
    label "sfera"
  ]
  node [
    id 316
    label "poddzia&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
]
