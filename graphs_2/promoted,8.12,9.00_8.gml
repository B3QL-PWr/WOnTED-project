graph [
  node [
    id 0
    label "nietypowy"
    origin "text"
  ]
  node [
    id 1
    label "przypadek"
    origin "text"
  ]
  node [
    id 2
    label "jemm'y"
    origin "text"
  ]
  node [
    id 3
    label "beale"
    origin "text"
  ]
  node [
    id 4
    label "inny"
  ]
  node [
    id 5
    label "nietypowo"
  ]
  node [
    id 6
    label "kolejny"
  ]
  node [
    id 7
    label "osobno"
  ]
  node [
    id 8
    label "r&#243;&#380;ny"
  ]
  node [
    id 9
    label "inszy"
  ]
  node [
    id 10
    label "inaczej"
  ]
  node [
    id 11
    label "atypically"
  ]
  node [
    id 12
    label "niezwykle"
  ]
  node [
    id 13
    label "odmiennie"
  ]
  node [
    id 14
    label "wydarzenie"
  ]
  node [
    id 15
    label "pacjent"
  ]
  node [
    id 16
    label "happening"
  ]
  node [
    id 17
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 18
    label "schorzenie"
  ]
  node [
    id 19
    label "przyk&#322;ad"
  ]
  node [
    id 20
    label "kategoria_gramatyczna"
  ]
  node [
    id 21
    label "przeznaczenie"
  ]
  node [
    id 22
    label "fakt"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "czyn"
  ]
  node [
    id 25
    label "ilustracja"
  ]
  node [
    id 26
    label "przedstawiciel"
  ]
  node [
    id 27
    label "przebiec"
  ]
  node [
    id 28
    label "charakter"
  ]
  node [
    id 29
    label "czynno&#347;&#263;"
  ]
  node [
    id 30
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 31
    label "motyw"
  ]
  node [
    id 32
    label "przebiegni&#281;cie"
  ]
  node [
    id 33
    label "fabu&#322;a"
  ]
  node [
    id 34
    label "rzuci&#263;"
  ]
  node [
    id 35
    label "destiny"
  ]
  node [
    id 36
    label "si&#322;a"
  ]
  node [
    id 37
    label "ustalenie"
  ]
  node [
    id 38
    label "przymus"
  ]
  node [
    id 39
    label "przydzielenie"
  ]
  node [
    id 40
    label "p&#243;j&#347;cie"
  ]
  node [
    id 41
    label "oblat"
  ]
  node [
    id 42
    label "obowi&#261;zek"
  ]
  node [
    id 43
    label "rzucenie"
  ]
  node [
    id 44
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 45
    label "wybranie"
  ]
  node [
    id 46
    label "zrobienie"
  ]
  node [
    id 47
    label "ognisko"
  ]
  node [
    id 48
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 49
    label "powalenie"
  ]
  node [
    id 50
    label "odezwanie_si&#281;"
  ]
  node [
    id 51
    label "atakowanie"
  ]
  node [
    id 52
    label "grupa_ryzyka"
  ]
  node [
    id 53
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 54
    label "nabawienie_si&#281;"
  ]
  node [
    id 55
    label "inkubacja"
  ]
  node [
    id 56
    label "kryzys"
  ]
  node [
    id 57
    label "powali&#263;"
  ]
  node [
    id 58
    label "remisja"
  ]
  node [
    id 59
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 60
    label "zajmowa&#263;"
  ]
  node [
    id 61
    label "zaburzenie"
  ]
  node [
    id 62
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 63
    label "badanie_histopatologiczne"
  ]
  node [
    id 64
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 65
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 66
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 67
    label "odzywanie_si&#281;"
  ]
  node [
    id 68
    label "diagnoza"
  ]
  node [
    id 69
    label "atakowa&#263;"
  ]
  node [
    id 70
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 71
    label "nabawianie_si&#281;"
  ]
  node [
    id 72
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 73
    label "zajmowanie"
  ]
  node [
    id 74
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 75
    label "klient"
  ]
  node [
    id 76
    label "piel&#281;gniarz"
  ]
  node [
    id 77
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 78
    label "od&#322;&#261;czanie"
  ]
  node [
    id 79
    label "chory"
  ]
  node [
    id 80
    label "od&#322;&#261;czenie"
  ]
  node [
    id 81
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 82
    label "szpitalnik"
  ]
  node [
    id 83
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 84
    label "przedstawienie"
  ]
  node [
    id 85
    label "Jemmy"
  ]
  node [
    id 86
    label "Beale"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 85
    target 86
  ]
]
