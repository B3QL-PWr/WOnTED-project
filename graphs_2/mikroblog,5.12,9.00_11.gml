graph [
  node [
    id 0
    label "&#322;ukasz"
    origin "text"
  ]
  node [
    id 1
    label "fabia&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "bro&#324;"
    origin "text"
  ]
  node [
    id 3
    label "karny"
    origin "text"
  ]
  node [
    id 4
    label "westa"
    origin "text"
  ]
  node [
    id 5
    label "ham"
    origin "text"
  ]
  node [
    id 6
    label "cardiff"
    origin "text"
  ]
  node [
    id 7
    label "amunicja"
  ]
  node [
    id 8
    label "karta_przetargowa"
  ]
  node [
    id 9
    label "rozbroi&#263;"
  ]
  node [
    id 10
    label "rozbrojenie"
  ]
  node [
    id 11
    label "osprz&#281;t"
  ]
  node [
    id 12
    label "uzbrojenie"
  ]
  node [
    id 13
    label "przyrz&#261;d"
  ]
  node [
    id 14
    label "rozbrajanie"
  ]
  node [
    id 15
    label "rozbraja&#263;"
  ]
  node [
    id 16
    label "or&#281;&#380;"
  ]
  node [
    id 17
    label "utensylia"
  ]
  node [
    id 18
    label "narz&#281;dzie"
  ]
  node [
    id 19
    label "o&#380;aglowanie"
  ]
  node [
    id 20
    label "&#380;aglowiec"
  ]
  node [
    id 21
    label "appointment"
  ]
  node [
    id 22
    label "olinowanie"
  ]
  node [
    id 23
    label "wyposa&#380;enie"
  ]
  node [
    id 24
    label "omasztowanie"
  ]
  node [
    id 25
    label "twornik"
  ]
  node [
    id 26
    label "instalacja"
  ]
  node [
    id 27
    label "munition"
  ]
  node [
    id 28
    label "zainstalowanie"
  ]
  node [
    id 29
    label "tackle"
  ]
  node [
    id 30
    label "pocisk"
  ]
  node [
    id 31
    label "wojsko"
  ]
  node [
    id 32
    label "si&#322;a"
  ]
  node [
    id 33
    label "element_anatomiczny"
  ]
  node [
    id 34
    label "poro&#380;e"
  ]
  node [
    id 35
    label "heraldyka"
  ]
  node [
    id 36
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 37
    label "roz&#322;adowywanie"
  ]
  node [
    id 38
    label "powodowanie"
  ]
  node [
    id 39
    label "zabieranie"
  ]
  node [
    id 40
    label "usuwanie"
  ]
  node [
    id 41
    label "demontowanie"
  ]
  node [
    id 42
    label "zabra&#263;"
  ]
  node [
    id 43
    label "roz&#322;adowa&#263;"
  ]
  node [
    id 44
    label "spowodowa&#263;"
  ]
  node [
    id 45
    label "rozebra&#263;"
  ]
  node [
    id 46
    label "discharge"
  ]
  node [
    id 47
    label "likwidacja"
  ]
  node [
    id 48
    label "roz&#322;adowanie"
  ]
  node [
    id 49
    label "disarming"
  ]
  node [
    id 50
    label "spowodowanie"
  ]
  node [
    id 51
    label "zdemontowanie"
  ]
  node [
    id 52
    label "zabranie"
  ]
  node [
    id 53
    label "czynno&#347;&#263;"
  ]
  node [
    id 54
    label "zabiera&#263;"
  ]
  node [
    id 55
    label "robi&#263;"
  ]
  node [
    id 56
    label "usuwa&#263;"
  ]
  node [
    id 57
    label "sk&#322;ada&#263;"
  ]
  node [
    id 58
    label "roz&#322;adowywa&#263;"
  ]
  node [
    id 59
    label "powodowa&#263;"
  ]
  node [
    id 60
    label "karnie"
  ]
  node [
    id 61
    label "zdyscyplinowany"
  ]
  node [
    id 62
    label "strza&#322;"
  ]
  node [
    id 63
    label "pos&#322;uszny"
  ]
  node [
    id 64
    label "wzorowy"
  ]
  node [
    id 65
    label "trafny"
  ]
  node [
    id 66
    label "shot"
  ]
  node [
    id 67
    label "przykro&#347;&#263;"
  ]
  node [
    id 68
    label "huk"
  ]
  node [
    id 69
    label "bum-bum"
  ]
  node [
    id 70
    label "pi&#322;ka"
  ]
  node [
    id 71
    label "uderzenie"
  ]
  node [
    id 72
    label "eksplozja"
  ]
  node [
    id 73
    label "wyrzut"
  ]
  node [
    id 74
    label "usi&#322;owanie"
  ]
  node [
    id 75
    label "przypadek"
  ]
  node [
    id 76
    label "shooting"
  ]
  node [
    id 77
    label "odgadywanie"
  ]
  node [
    id 78
    label "zale&#380;ny"
  ]
  node [
    id 79
    label "uleg&#322;y"
  ]
  node [
    id 80
    label "mi&#322;y"
  ]
  node [
    id 81
    label "pos&#322;usznie"
  ]
  node [
    id 82
    label "skupiony"
  ]
  node [
    id 83
    label "dyscyplinowany"
  ]
  node [
    id 84
    label "doskona&#322;y"
  ]
  node [
    id 85
    label "przyk&#322;adny"
  ]
  node [
    id 86
    label "&#322;adny"
  ]
  node [
    id 87
    label "dobry"
  ]
  node [
    id 88
    label "wzorowo"
  ]
  node [
    id 89
    label "kamizelka"
  ]
  node [
    id 90
    label "g&#243;ra"
  ]
  node [
    id 91
    label "&#321;ukasz"
  ]
  node [
    id 92
    label "Cardiff"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 92
  ]
]
