graph [
  node [
    id 0
    label "plan"
    origin "text"
  ]
  node [
    id 1
    label "informatyzacja"
    origin "text"
  ]
  node [
    id 2
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 3
    label "reprezentacja"
  ]
  node [
    id 4
    label "przestrze&#324;"
  ]
  node [
    id 5
    label "intencja"
  ]
  node [
    id 6
    label "punkt"
  ]
  node [
    id 7
    label "perspektywa"
  ]
  node [
    id 8
    label "model"
  ]
  node [
    id 9
    label "miejsce_pracy"
  ]
  node [
    id 10
    label "device"
  ]
  node [
    id 11
    label "obraz"
  ]
  node [
    id 12
    label "rysunek"
  ]
  node [
    id 13
    label "agreement"
  ]
  node [
    id 14
    label "dekoracja"
  ]
  node [
    id 15
    label "wytw&#243;r"
  ]
  node [
    id 16
    label "pomys&#322;"
  ]
  node [
    id 17
    label "dru&#380;yna"
  ]
  node [
    id 18
    label "zesp&#243;&#322;"
  ]
  node [
    id 19
    label "deputation"
  ]
  node [
    id 20
    label "emblemat"
  ]
  node [
    id 21
    label "grafika"
  ]
  node [
    id 22
    label "ilustracja"
  ]
  node [
    id 23
    label "plastyka"
  ]
  node [
    id 24
    label "kreska"
  ]
  node [
    id 25
    label "shape"
  ]
  node [
    id 26
    label "teka"
  ]
  node [
    id 27
    label "picture"
  ]
  node [
    id 28
    label "kszta&#322;t"
  ]
  node [
    id 29
    label "photograph"
  ]
  node [
    id 30
    label "cz&#322;owiek"
  ]
  node [
    id 31
    label "spos&#243;b"
  ]
  node [
    id 32
    label "matryca"
  ]
  node [
    id 33
    label "facet"
  ]
  node [
    id 34
    label "zi&#243;&#322;ko"
  ]
  node [
    id 35
    label "mildew"
  ]
  node [
    id 36
    label "miniatura"
  ]
  node [
    id 37
    label "ideal"
  ]
  node [
    id 38
    label "adaptation"
  ]
  node [
    id 39
    label "typ"
  ]
  node [
    id 40
    label "ruch"
  ]
  node [
    id 41
    label "imitacja"
  ]
  node [
    id 42
    label "pozowa&#263;"
  ]
  node [
    id 43
    label "orygina&#322;"
  ]
  node [
    id 44
    label "wz&#243;r"
  ]
  node [
    id 45
    label "motif"
  ]
  node [
    id 46
    label "prezenter"
  ]
  node [
    id 47
    label "pozowanie"
  ]
  node [
    id 48
    label "oktant"
  ]
  node [
    id 49
    label "przedzielenie"
  ]
  node [
    id 50
    label "przedzieli&#263;"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "przestw&#243;r"
  ]
  node [
    id 53
    label "rozdziela&#263;"
  ]
  node [
    id 54
    label "nielito&#347;ciwy"
  ]
  node [
    id 55
    label "czasoprzestrze&#324;"
  ]
  node [
    id 56
    label "miejsce"
  ]
  node [
    id 57
    label "niezmierzony"
  ]
  node [
    id 58
    label "bezbrze&#380;e"
  ]
  node [
    id 59
    label "rozdzielanie"
  ]
  node [
    id 60
    label "projekcja"
  ]
  node [
    id 61
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 62
    label "wypunktowa&#263;"
  ]
  node [
    id 63
    label "opinion"
  ]
  node [
    id 64
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 65
    label "inning"
  ]
  node [
    id 66
    label "zaj&#347;cie"
  ]
  node [
    id 67
    label "representation"
  ]
  node [
    id 68
    label "filmoteka"
  ]
  node [
    id 69
    label "widok"
  ]
  node [
    id 70
    label "podobrazie"
  ]
  node [
    id 71
    label "przeplot"
  ]
  node [
    id 72
    label "napisy"
  ]
  node [
    id 73
    label "pogl&#261;d"
  ]
  node [
    id 74
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 75
    label "oprawia&#263;"
  ]
  node [
    id 76
    label "ziarno"
  ]
  node [
    id 77
    label "human_body"
  ]
  node [
    id 78
    label "ostro&#347;&#263;"
  ]
  node [
    id 79
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 80
    label "pulment"
  ]
  node [
    id 81
    label "sztafa&#380;"
  ]
  node [
    id 82
    label "punktowa&#263;"
  ]
  node [
    id 83
    label "scena"
  ]
  node [
    id 84
    label "przedstawienie"
  ]
  node [
    id 85
    label "persona"
  ]
  node [
    id 86
    label "zjawisko"
  ]
  node [
    id 87
    label "malarz"
  ]
  node [
    id 88
    label "oprawianie"
  ]
  node [
    id 89
    label "uj&#281;cie"
  ]
  node [
    id 90
    label "parkiet"
  ]
  node [
    id 91
    label "t&#322;o"
  ]
  node [
    id 92
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 93
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 94
    label "effigy"
  ]
  node [
    id 95
    label "anamorfoza"
  ]
  node [
    id 96
    label "czo&#322;&#243;wka"
  ]
  node [
    id 97
    label "plama_barwna"
  ]
  node [
    id 98
    label "postprodukcja"
  ]
  node [
    id 99
    label "rola"
  ]
  node [
    id 100
    label "ty&#322;&#243;wka"
  ]
  node [
    id 101
    label "thinking"
  ]
  node [
    id 102
    label "ukradzenie"
  ]
  node [
    id 103
    label "pocz&#261;tki"
  ]
  node [
    id 104
    label "ukra&#347;&#263;"
  ]
  node [
    id 105
    label "idea"
  ]
  node [
    id 106
    label "system"
  ]
  node [
    id 107
    label "przedmiot"
  ]
  node [
    id 108
    label "rezultat"
  ]
  node [
    id 109
    label "p&#322;&#243;d"
  ]
  node [
    id 110
    label "work"
  ]
  node [
    id 111
    label "patrze&#263;"
  ]
  node [
    id 112
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 113
    label "anticipation"
  ]
  node [
    id 114
    label "dystans"
  ]
  node [
    id 115
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 116
    label "posta&#263;"
  ]
  node [
    id 117
    label "patrzenie"
  ]
  node [
    id 118
    label "decentracja"
  ]
  node [
    id 119
    label "scene"
  ]
  node [
    id 120
    label "metoda"
  ]
  node [
    id 121
    label "prognoza"
  ]
  node [
    id 122
    label "figura_geometryczna"
  ]
  node [
    id 123
    label "pojmowanie"
  ]
  node [
    id 124
    label "widzie&#263;"
  ]
  node [
    id 125
    label "krajobraz"
  ]
  node [
    id 126
    label "tryb"
  ]
  node [
    id 127
    label "expectation"
  ]
  node [
    id 128
    label "sznurownia"
  ]
  node [
    id 129
    label "upi&#281;kszanie"
  ]
  node [
    id 130
    label "adornment"
  ]
  node [
    id 131
    label "ozdoba"
  ]
  node [
    id 132
    label "pi&#281;kniejszy"
  ]
  node [
    id 133
    label "ferm"
  ]
  node [
    id 134
    label "scenografia"
  ]
  node [
    id 135
    label "plan_zdj&#281;ciowy"
  ]
  node [
    id 136
    label "wystr&#243;j"
  ]
  node [
    id 137
    label "obiekt_matematyczny"
  ]
  node [
    id 138
    label "stopie&#324;_pisma"
  ]
  node [
    id 139
    label "pozycja"
  ]
  node [
    id 140
    label "problemat"
  ]
  node [
    id 141
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 142
    label "obiekt"
  ]
  node [
    id 143
    label "point"
  ]
  node [
    id 144
    label "plamka"
  ]
  node [
    id 145
    label "mark"
  ]
  node [
    id 146
    label "ust&#281;p"
  ]
  node [
    id 147
    label "po&#322;o&#380;enie"
  ]
  node [
    id 148
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 149
    label "kres"
  ]
  node [
    id 150
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 151
    label "chwila"
  ]
  node [
    id 152
    label "podpunkt"
  ]
  node [
    id 153
    label "jednostka"
  ]
  node [
    id 154
    label "sprawa"
  ]
  node [
    id 155
    label "problematyka"
  ]
  node [
    id 156
    label "prosta"
  ]
  node [
    id 157
    label "wojsko"
  ]
  node [
    id 158
    label "zapunktowa&#263;"
  ]
  node [
    id 159
    label "elektronizacja"
  ]
  node [
    id 160
    label "computerization"
  ]
  node [
    id 161
    label "modernizacja"
  ]
  node [
    id 162
    label "Japonia"
  ]
  node [
    id 163
    label "Zair"
  ]
  node [
    id 164
    label "Belize"
  ]
  node [
    id 165
    label "San_Marino"
  ]
  node [
    id 166
    label "Tanzania"
  ]
  node [
    id 167
    label "Antigua_i_Barbuda"
  ]
  node [
    id 168
    label "granica_pa&#324;stwa"
  ]
  node [
    id 169
    label "Senegal"
  ]
  node [
    id 170
    label "Seszele"
  ]
  node [
    id 171
    label "Mauretania"
  ]
  node [
    id 172
    label "Indie"
  ]
  node [
    id 173
    label "Filipiny"
  ]
  node [
    id 174
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 175
    label "Zimbabwe"
  ]
  node [
    id 176
    label "Malezja"
  ]
  node [
    id 177
    label "Rumunia"
  ]
  node [
    id 178
    label "Surinam"
  ]
  node [
    id 179
    label "Ukraina"
  ]
  node [
    id 180
    label "Syria"
  ]
  node [
    id 181
    label "Wyspy_Marshalla"
  ]
  node [
    id 182
    label "Burkina_Faso"
  ]
  node [
    id 183
    label "Grecja"
  ]
  node [
    id 184
    label "Polska"
  ]
  node [
    id 185
    label "Wenezuela"
  ]
  node [
    id 186
    label "Suazi"
  ]
  node [
    id 187
    label "Nepal"
  ]
  node [
    id 188
    label "S&#322;owacja"
  ]
  node [
    id 189
    label "Algieria"
  ]
  node [
    id 190
    label "Chiny"
  ]
  node [
    id 191
    label "Grenada"
  ]
  node [
    id 192
    label "Barbados"
  ]
  node [
    id 193
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 194
    label "Pakistan"
  ]
  node [
    id 195
    label "Niemcy"
  ]
  node [
    id 196
    label "Bahrajn"
  ]
  node [
    id 197
    label "Komory"
  ]
  node [
    id 198
    label "Australia"
  ]
  node [
    id 199
    label "Rodezja"
  ]
  node [
    id 200
    label "Malawi"
  ]
  node [
    id 201
    label "Gwinea"
  ]
  node [
    id 202
    label "Wehrlen"
  ]
  node [
    id 203
    label "Meksyk"
  ]
  node [
    id 204
    label "Liechtenstein"
  ]
  node [
    id 205
    label "Czarnog&#243;ra"
  ]
  node [
    id 206
    label "Wielka_Brytania"
  ]
  node [
    id 207
    label "Kuwejt"
  ]
  node [
    id 208
    label "Monako"
  ]
  node [
    id 209
    label "Angola"
  ]
  node [
    id 210
    label "Jemen"
  ]
  node [
    id 211
    label "Etiopia"
  ]
  node [
    id 212
    label "Madagaskar"
  ]
  node [
    id 213
    label "terytorium"
  ]
  node [
    id 214
    label "Kolumbia"
  ]
  node [
    id 215
    label "Portoryko"
  ]
  node [
    id 216
    label "Mauritius"
  ]
  node [
    id 217
    label "Kostaryka"
  ]
  node [
    id 218
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 219
    label "Tajlandia"
  ]
  node [
    id 220
    label "Argentyna"
  ]
  node [
    id 221
    label "Zambia"
  ]
  node [
    id 222
    label "Sri_Lanka"
  ]
  node [
    id 223
    label "Gwatemala"
  ]
  node [
    id 224
    label "Kirgistan"
  ]
  node [
    id 225
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 226
    label "Hiszpania"
  ]
  node [
    id 227
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 228
    label "Salwador"
  ]
  node [
    id 229
    label "Korea"
  ]
  node [
    id 230
    label "Macedonia"
  ]
  node [
    id 231
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 232
    label "Brunei"
  ]
  node [
    id 233
    label "Mozambik"
  ]
  node [
    id 234
    label "Turcja"
  ]
  node [
    id 235
    label "Kambod&#380;a"
  ]
  node [
    id 236
    label "Benin"
  ]
  node [
    id 237
    label "Bhutan"
  ]
  node [
    id 238
    label "Tunezja"
  ]
  node [
    id 239
    label "Austria"
  ]
  node [
    id 240
    label "Izrael"
  ]
  node [
    id 241
    label "Sierra_Leone"
  ]
  node [
    id 242
    label "Jamajka"
  ]
  node [
    id 243
    label "Rosja"
  ]
  node [
    id 244
    label "Rwanda"
  ]
  node [
    id 245
    label "holoarktyka"
  ]
  node [
    id 246
    label "Nigeria"
  ]
  node [
    id 247
    label "USA"
  ]
  node [
    id 248
    label "Oman"
  ]
  node [
    id 249
    label "Luksemburg"
  ]
  node [
    id 250
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 251
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 252
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 253
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 254
    label "Dominikana"
  ]
  node [
    id 255
    label "Irlandia"
  ]
  node [
    id 256
    label "Liban"
  ]
  node [
    id 257
    label "Hanower"
  ]
  node [
    id 258
    label "Estonia"
  ]
  node [
    id 259
    label "Iran"
  ]
  node [
    id 260
    label "Nowa_Zelandia"
  ]
  node [
    id 261
    label "Gabon"
  ]
  node [
    id 262
    label "Samoa"
  ]
  node [
    id 263
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 264
    label "S&#322;owenia"
  ]
  node [
    id 265
    label "Kiribati"
  ]
  node [
    id 266
    label "Egipt"
  ]
  node [
    id 267
    label "Togo"
  ]
  node [
    id 268
    label "Mongolia"
  ]
  node [
    id 269
    label "Sudan"
  ]
  node [
    id 270
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 271
    label "Bahamy"
  ]
  node [
    id 272
    label "Bangladesz"
  ]
  node [
    id 273
    label "partia"
  ]
  node [
    id 274
    label "Serbia"
  ]
  node [
    id 275
    label "Czechy"
  ]
  node [
    id 276
    label "Holandia"
  ]
  node [
    id 277
    label "Birma"
  ]
  node [
    id 278
    label "Albania"
  ]
  node [
    id 279
    label "Mikronezja"
  ]
  node [
    id 280
    label "Gambia"
  ]
  node [
    id 281
    label "Kazachstan"
  ]
  node [
    id 282
    label "interior"
  ]
  node [
    id 283
    label "Uzbekistan"
  ]
  node [
    id 284
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 285
    label "Malta"
  ]
  node [
    id 286
    label "Lesoto"
  ]
  node [
    id 287
    label "para"
  ]
  node [
    id 288
    label "Antarktis"
  ]
  node [
    id 289
    label "Andora"
  ]
  node [
    id 290
    label "Nauru"
  ]
  node [
    id 291
    label "Kuba"
  ]
  node [
    id 292
    label "Wietnam"
  ]
  node [
    id 293
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 294
    label "ziemia"
  ]
  node [
    id 295
    label "Kamerun"
  ]
  node [
    id 296
    label "Chorwacja"
  ]
  node [
    id 297
    label "Urugwaj"
  ]
  node [
    id 298
    label "Niger"
  ]
  node [
    id 299
    label "Turkmenistan"
  ]
  node [
    id 300
    label "Szwajcaria"
  ]
  node [
    id 301
    label "zwrot"
  ]
  node [
    id 302
    label "organizacja"
  ]
  node [
    id 303
    label "grupa"
  ]
  node [
    id 304
    label "Palau"
  ]
  node [
    id 305
    label "Litwa"
  ]
  node [
    id 306
    label "Gruzja"
  ]
  node [
    id 307
    label "Tajwan"
  ]
  node [
    id 308
    label "Kongo"
  ]
  node [
    id 309
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 310
    label "Honduras"
  ]
  node [
    id 311
    label "Boliwia"
  ]
  node [
    id 312
    label "Uganda"
  ]
  node [
    id 313
    label "Namibia"
  ]
  node [
    id 314
    label "Azerbejd&#380;an"
  ]
  node [
    id 315
    label "Erytrea"
  ]
  node [
    id 316
    label "Gujana"
  ]
  node [
    id 317
    label "Panama"
  ]
  node [
    id 318
    label "Somalia"
  ]
  node [
    id 319
    label "Burundi"
  ]
  node [
    id 320
    label "Tuwalu"
  ]
  node [
    id 321
    label "Libia"
  ]
  node [
    id 322
    label "Katar"
  ]
  node [
    id 323
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 324
    label "Sahara_Zachodnia"
  ]
  node [
    id 325
    label "Trynidad_i_Tobago"
  ]
  node [
    id 326
    label "Gwinea_Bissau"
  ]
  node [
    id 327
    label "Bu&#322;garia"
  ]
  node [
    id 328
    label "Fid&#380;i"
  ]
  node [
    id 329
    label "Nikaragua"
  ]
  node [
    id 330
    label "Tonga"
  ]
  node [
    id 331
    label "Timor_Wschodni"
  ]
  node [
    id 332
    label "Laos"
  ]
  node [
    id 333
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 334
    label "Ghana"
  ]
  node [
    id 335
    label "Brazylia"
  ]
  node [
    id 336
    label "Belgia"
  ]
  node [
    id 337
    label "Irak"
  ]
  node [
    id 338
    label "Peru"
  ]
  node [
    id 339
    label "Arabia_Saudyjska"
  ]
  node [
    id 340
    label "Indonezja"
  ]
  node [
    id 341
    label "Malediwy"
  ]
  node [
    id 342
    label "Afganistan"
  ]
  node [
    id 343
    label "Jordania"
  ]
  node [
    id 344
    label "Kenia"
  ]
  node [
    id 345
    label "Czad"
  ]
  node [
    id 346
    label "Liberia"
  ]
  node [
    id 347
    label "W&#281;gry"
  ]
  node [
    id 348
    label "Chile"
  ]
  node [
    id 349
    label "Mali"
  ]
  node [
    id 350
    label "Armenia"
  ]
  node [
    id 351
    label "Kanada"
  ]
  node [
    id 352
    label "Cypr"
  ]
  node [
    id 353
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 354
    label "Ekwador"
  ]
  node [
    id 355
    label "Mo&#322;dawia"
  ]
  node [
    id 356
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 357
    label "W&#322;ochy"
  ]
  node [
    id 358
    label "Wyspy_Salomona"
  ]
  node [
    id 359
    label "&#321;otwa"
  ]
  node [
    id 360
    label "D&#380;ibuti"
  ]
  node [
    id 361
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 362
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 363
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 364
    label "Portugalia"
  ]
  node [
    id 365
    label "Botswana"
  ]
  node [
    id 366
    label "Maroko"
  ]
  node [
    id 367
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 368
    label "Francja"
  ]
  node [
    id 369
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 370
    label "Dominika"
  ]
  node [
    id 371
    label "Paragwaj"
  ]
  node [
    id 372
    label "Tad&#380;ykistan"
  ]
  node [
    id 373
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 374
    label "Haiti"
  ]
  node [
    id 375
    label "Khitai"
  ]
  node [
    id 376
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 377
    label "poker"
  ]
  node [
    id 378
    label "nale&#380;e&#263;"
  ]
  node [
    id 379
    label "odparowanie"
  ]
  node [
    id 380
    label "sztuka"
  ]
  node [
    id 381
    label "smoke"
  ]
  node [
    id 382
    label "odparowa&#263;"
  ]
  node [
    id 383
    label "parowanie"
  ]
  node [
    id 384
    label "chodzi&#263;"
  ]
  node [
    id 385
    label "pair"
  ]
  node [
    id 386
    label "uk&#322;ad"
  ]
  node [
    id 387
    label "odparowywa&#263;"
  ]
  node [
    id 388
    label "dodatek"
  ]
  node [
    id 389
    label "odparowywanie"
  ]
  node [
    id 390
    label "jednostka_monetarna"
  ]
  node [
    id 391
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 392
    label "moneta"
  ]
  node [
    id 393
    label "damp"
  ]
  node [
    id 394
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 395
    label "wyparowanie"
  ]
  node [
    id 396
    label "gaz_cieplarniany"
  ]
  node [
    id 397
    label "gaz"
  ]
  node [
    id 398
    label "jednostka_administracyjna"
  ]
  node [
    id 399
    label "Wile&#324;szczyzna"
  ]
  node [
    id 400
    label "obszar"
  ]
  node [
    id 401
    label "Jukon"
  ]
  node [
    id 402
    label "przybud&#243;wka"
  ]
  node [
    id 403
    label "struktura"
  ]
  node [
    id 404
    label "organization"
  ]
  node [
    id 405
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 406
    label "od&#322;am"
  ]
  node [
    id 407
    label "TOPR"
  ]
  node [
    id 408
    label "komitet_koordynacyjny"
  ]
  node [
    id 409
    label "przedstawicielstwo"
  ]
  node [
    id 410
    label "ZMP"
  ]
  node [
    id 411
    label "Cepelia"
  ]
  node [
    id 412
    label "GOPR"
  ]
  node [
    id 413
    label "endecki"
  ]
  node [
    id 414
    label "ZBoWiD"
  ]
  node [
    id 415
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 416
    label "podmiot"
  ]
  node [
    id 417
    label "boj&#243;wka"
  ]
  node [
    id 418
    label "ZOMO"
  ]
  node [
    id 419
    label "jednostka_organizacyjna"
  ]
  node [
    id 420
    label "centrala"
  ]
  node [
    id 421
    label "zmiana"
  ]
  node [
    id 422
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 423
    label "turn"
  ]
  node [
    id 424
    label "wyra&#380;enie"
  ]
  node [
    id 425
    label "fraza_czasownikowa"
  ]
  node [
    id 426
    label "turning"
  ]
  node [
    id 427
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 428
    label "skr&#281;t"
  ]
  node [
    id 429
    label "jednostka_leksykalna"
  ]
  node [
    id 430
    label "obr&#243;t"
  ]
  node [
    id 431
    label "asymilowa&#263;"
  ]
  node [
    id 432
    label "kompozycja"
  ]
  node [
    id 433
    label "pakiet_klimatyczny"
  ]
  node [
    id 434
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 435
    label "type"
  ]
  node [
    id 436
    label "cz&#261;steczka"
  ]
  node [
    id 437
    label "gromada"
  ]
  node [
    id 438
    label "specgrupa"
  ]
  node [
    id 439
    label "egzemplarz"
  ]
  node [
    id 440
    label "stage_set"
  ]
  node [
    id 441
    label "asymilowanie"
  ]
  node [
    id 442
    label "odm&#322;odzenie"
  ]
  node [
    id 443
    label "odm&#322;adza&#263;"
  ]
  node [
    id 444
    label "harcerze_starsi"
  ]
  node [
    id 445
    label "jednostka_systematyczna"
  ]
  node [
    id 446
    label "oddzia&#322;"
  ]
  node [
    id 447
    label "category"
  ]
  node [
    id 448
    label "liga"
  ]
  node [
    id 449
    label "&#346;wietliki"
  ]
  node [
    id 450
    label "formacja_geologiczna"
  ]
  node [
    id 451
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 452
    label "Eurogrupa"
  ]
  node [
    id 453
    label "Terranie"
  ]
  node [
    id 454
    label "odm&#322;adzanie"
  ]
  node [
    id 455
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 456
    label "Entuzjastki"
  ]
  node [
    id 457
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 458
    label "AWS"
  ]
  node [
    id 459
    label "ZChN"
  ]
  node [
    id 460
    label "Bund"
  ]
  node [
    id 461
    label "PPR"
  ]
  node [
    id 462
    label "blok"
  ]
  node [
    id 463
    label "egzekutywa"
  ]
  node [
    id 464
    label "Wigowie"
  ]
  node [
    id 465
    label "aktyw"
  ]
  node [
    id 466
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 467
    label "Razem"
  ]
  node [
    id 468
    label "unit"
  ]
  node [
    id 469
    label "wybranka"
  ]
  node [
    id 470
    label "SLD"
  ]
  node [
    id 471
    label "ZSL"
  ]
  node [
    id 472
    label "Kuomintang"
  ]
  node [
    id 473
    label "si&#322;a"
  ]
  node [
    id 474
    label "PiS"
  ]
  node [
    id 475
    label "gra"
  ]
  node [
    id 476
    label "Jakobici"
  ]
  node [
    id 477
    label "materia&#322;"
  ]
  node [
    id 478
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 479
    label "package"
  ]
  node [
    id 480
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 481
    label "PO"
  ]
  node [
    id 482
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 483
    label "game"
  ]
  node [
    id 484
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 485
    label "wybranek"
  ]
  node [
    id 486
    label "niedoczas"
  ]
  node [
    id 487
    label "Federali&#347;ci"
  ]
  node [
    id 488
    label "PSL"
  ]
  node [
    id 489
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 490
    label "plant"
  ]
  node [
    id 491
    label "przyroda"
  ]
  node [
    id 492
    label "ro&#347;lina"
  ]
  node [
    id 493
    label "formacja_ro&#347;linna"
  ]
  node [
    id 494
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 495
    label "biom"
  ]
  node [
    id 496
    label "geosystem"
  ]
  node [
    id 497
    label "szata_ro&#347;linna"
  ]
  node [
    id 498
    label "zielono&#347;&#263;"
  ]
  node [
    id 499
    label "pi&#281;tro"
  ]
  node [
    id 500
    label "teren"
  ]
  node [
    id 501
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 502
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 503
    label "Kaszmir"
  ]
  node [
    id 504
    label "Pend&#380;ab"
  ]
  node [
    id 505
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 506
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 507
    label "funt_liba&#324;ski"
  ]
  node [
    id 508
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 509
    label "Pozna&#324;"
  ]
  node [
    id 510
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 511
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 512
    label "Gozo"
  ]
  node [
    id 513
    label "lira_malta&#324;ska"
  ]
  node [
    id 514
    label "strefa_euro"
  ]
  node [
    id 515
    label "Afryka_Zachodnia"
  ]
  node [
    id 516
    label "Afryka_Wschodnia"
  ]
  node [
    id 517
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 518
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 519
    label "dolar_namibijski"
  ]
  node [
    id 520
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 521
    label "NATO"
  ]
  node [
    id 522
    label "escudo_portugalskie"
  ]
  node [
    id 523
    label "milrejs"
  ]
  node [
    id 524
    label "Wielka_Bahama"
  ]
  node [
    id 525
    label "dolar_bahamski"
  ]
  node [
    id 526
    label "Karaiby"
  ]
  node [
    id 527
    label "dolar_liberyjski"
  ]
  node [
    id 528
    label "riel"
  ]
  node [
    id 529
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 530
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 531
    label "&#321;adoga"
  ]
  node [
    id 532
    label "Dniepr"
  ]
  node [
    id 533
    label "Kamczatka"
  ]
  node [
    id 534
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 535
    label "Witim"
  ]
  node [
    id 536
    label "Tuwa"
  ]
  node [
    id 537
    label "Czeczenia"
  ]
  node [
    id 538
    label "Ajon"
  ]
  node [
    id 539
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 540
    label "car"
  ]
  node [
    id 541
    label "Karelia"
  ]
  node [
    id 542
    label "Don"
  ]
  node [
    id 543
    label "Mordowia"
  ]
  node [
    id 544
    label "Czuwaszja"
  ]
  node [
    id 545
    label "Udmurcja"
  ]
  node [
    id 546
    label "Jama&#322;"
  ]
  node [
    id 547
    label "Azja"
  ]
  node [
    id 548
    label "Newa"
  ]
  node [
    id 549
    label "Adygeja"
  ]
  node [
    id 550
    label "Inguszetia"
  ]
  node [
    id 551
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 552
    label "Mari_El"
  ]
  node [
    id 553
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 554
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 555
    label "Wszechrosja"
  ]
  node [
    id 556
    label "rubel_rosyjski"
  ]
  node [
    id 557
    label "s&#322;owianofilstwo"
  ]
  node [
    id 558
    label "Dagestan"
  ]
  node [
    id 559
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 560
    label "Komi"
  ]
  node [
    id 561
    label "Tatarstan"
  ]
  node [
    id 562
    label "Baszkiria"
  ]
  node [
    id 563
    label "Perm"
  ]
  node [
    id 564
    label "Chakasja"
  ]
  node [
    id 565
    label "Syberia"
  ]
  node [
    id 566
    label "Europa_Wschodnia"
  ]
  node [
    id 567
    label "Anadyr"
  ]
  node [
    id 568
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 569
    label "Unia_Europejska"
  ]
  node [
    id 570
    label "Dobrud&#380;a"
  ]
  node [
    id 571
    label "lew"
  ]
  node [
    id 572
    label "Judea"
  ]
  node [
    id 573
    label "lira_izraelska"
  ]
  node [
    id 574
    label "Galilea"
  ]
  node [
    id 575
    label "szekel"
  ]
  node [
    id 576
    label "Luksemburgia"
  ]
  node [
    id 577
    label "Flandria"
  ]
  node [
    id 578
    label "Brabancja"
  ]
  node [
    id 579
    label "Limburgia"
  ]
  node [
    id 580
    label "Walonia"
  ]
  node [
    id 581
    label "frank_belgijski"
  ]
  node [
    id 582
    label "Niderlandy"
  ]
  node [
    id 583
    label "dinar_iracki"
  ]
  node [
    id 584
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 585
    label "Maghreb"
  ]
  node [
    id 586
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 587
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 588
    label "szyling_ugandyjski"
  ]
  node [
    id 589
    label "kafar"
  ]
  node [
    id 590
    label "dolar_jamajski"
  ]
  node [
    id 591
    label "Borneo"
  ]
  node [
    id 592
    label "ringgit"
  ]
  node [
    id 593
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 594
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 595
    label "dolar_surinamski"
  ]
  node [
    id 596
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 597
    label "funt_suda&#324;ski"
  ]
  node [
    id 598
    label "dolar_guja&#324;ski"
  ]
  node [
    id 599
    label "Manica"
  ]
  node [
    id 600
    label "Inhambane"
  ]
  node [
    id 601
    label "Maputo"
  ]
  node [
    id 602
    label "Nampula"
  ]
  node [
    id 603
    label "metical"
  ]
  node [
    id 604
    label "escudo_mozambickie"
  ]
  node [
    id 605
    label "Gaza"
  ]
  node [
    id 606
    label "Niasa"
  ]
  node [
    id 607
    label "Cabo_Delgado"
  ]
  node [
    id 608
    label "Sahara"
  ]
  node [
    id 609
    label "sol"
  ]
  node [
    id 610
    label "inti"
  ]
  node [
    id 611
    label "kip"
  ]
  node [
    id 612
    label "Pireneje"
  ]
  node [
    id 613
    label "euro"
  ]
  node [
    id 614
    label "kwacha_zambijska"
  ]
  node [
    id 615
    label "tugrik"
  ]
  node [
    id 616
    label "Azja_Wschodnia"
  ]
  node [
    id 617
    label "ajmak"
  ]
  node [
    id 618
    label "Buriaci"
  ]
  node [
    id 619
    label "Ameryka_Centralna"
  ]
  node [
    id 620
    label "balboa"
  ]
  node [
    id 621
    label "dolar"
  ]
  node [
    id 622
    label "Zelandia"
  ]
  node [
    id 623
    label "gulden"
  ]
  node [
    id 624
    label "manat_turkme&#324;ski"
  ]
  node [
    id 625
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 626
    label "dolar_Tuvalu"
  ]
  node [
    id 627
    label "Polinezja"
  ]
  node [
    id 628
    label "Katanga"
  ]
  node [
    id 629
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 630
    label "zair"
  ]
  node [
    id 631
    label "frank_szwajcarski"
  ]
  node [
    id 632
    label "Europa_Zachodnia"
  ]
  node [
    id 633
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 634
    label "dolar_Belize"
  ]
  node [
    id 635
    label "Jukatan"
  ]
  node [
    id 636
    label "colon"
  ]
  node [
    id 637
    label "Dyja"
  ]
  node [
    id 638
    label "Izera"
  ]
  node [
    id 639
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 640
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 641
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 642
    label "Lasko"
  ]
  node [
    id 643
    label "korona_czeska"
  ]
  node [
    id 644
    label "ugija"
  ]
  node [
    id 645
    label "szyling_kenijski"
  ]
  node [
    id 646
    label "Karabach"
  ]
  node [
    id 647
    label "manat_azerski"
  ]
  node [
    id 648
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 649
    label "Nachiczewan"
  ]
  node [
    id 650
    label "Bengal"
  ]
  node [
    id 651
    label "taka"
  ]
  node [
    id 652
    label "dolar_Kiribati"
  ]
  node [
    id 653
    label "Ocean_Spokojny"
  ]
  node [
    id 654
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 655
    label "Cebu"
  ]
  node [
    id 656
    label "peso_filipi&#324;skie"
  ]
  node [
    id 657
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 658
    label "Ulster"
  ]
  node [
    id 659
    label "Atlantyk"
  ]
  node [
    id 660
    label "funt_irlandzki"
  ]
  node [
    id 661
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 662
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 663
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 664
    label "cedi"
  ]
  node [
    id 665
    label "ariary"
  ]
  node [
    id 666
    label "Ocean_Indyjski"
  ]
  node [
    id 667
    label "frank_malgaski"
  ]
  node [
    id 668
    label "Walencja"
  ]
  node [
    id 669
    label "Galicja"
  ]
  node [
    id 670
    label "Aragonia"
  ]
  node [
    id 671
    label "Estremadura"
  ]
  node [
    id 672
    label "Rzym_Zachodni"
  ]
  node [
    id 673
    label "Baskonia"
  ]
  node [
    id 674
    label "Katalonia"
  ]
  node [
    id 675
    label "Majorka"
  ]
  node [
    id 676
    label "Asturia"
  ]
  node [
    id 677
    label "Andaluzja"
  ]
  node [
    id 678
    label "Kastylia"
  ]
  node [
    id 679
    label "peseta"
  ]
  node [
    id 680
    label "hacjender"
  ]
  node [
    id 681
    label "peso_chilijskie"
  ]
  node [
    id 682
    label "rupia_indyjska"
  ]
  node [
    id 683
    label "Kerala"
  ]
  node [
    id 684
    label "Indie_Zachodnie"
  ]
  node [
    id 685
    label "Indie_Wschodnie"
  ]
  node [
    id 686
    label "Asam"
  ]
  node [
    id 687
    label "Indie_Portugalskie"
  ]
  node [
    id 688
    label "Bollywood"
  ]
  node [
    id 689
    label "Sikkim"
  ]
  node [
    id 690
    label "jen"
  ]
  node [
    id 691
    label "Okinawa"
  ]
  node [
    id 692
    label "jinja"
  ]
  node [
    id 693
    label "Japonica"
  ]
  node [
    id 694
    label "Karlsbad"
  ]
  node [
    id 695
    label "Turyngia"
  ]
  node [
    id 696
    label "Brandenburgia"
  ]
  node [
    id 697
    label "marka"
  ]
  node [
    id 698
    label "Saksonia"
  ]
  node [
    id 699
    label "Szlezwik"
  ]
  node [
    id 700
    label "Niemcy_Wschodnie"
  ]
  node [
    id 701
    label "Frankonia"
  ]
  node [
    id 702
    label "Rugia"
  ]
  node [
    id 703
    label "Helgoland"
  ]
  node [
    id 704
    label "Bawaria"
  ]
  node [
    id 705
    label "Holsztyn"
  ]
  node [
    id 706
    label "Badenia"
  ]
  node [
    id 707
    label "Wirtembergia"
  ]
  node [
    id 708
    label "Nadrenia"
  ]
  node [
    id 709
    label "Anglosas"
  ]
  node [
    id 710
    label "Hesja"
  ]
  node [
    id 711
    label "Dolna_Saksonia"
  ]
  node [
    id 712
    label "Niemcy_Zachodnie"
  ]
  node [
    id 713
    label "Germania"
  ]
  node [
    id 714
    label "Po&#322;abie"
  ]
  node [
    id 715
    label "Szwabia"
  ]
  node [
    id 716
    label "Westfalia"
  ]
  node [
    id 717
    label "Romania"
  ]
  node [
    id 718
    label "Ok&#281;cie"
  ]
  node [
    id 719
    label "Apulia"
  ]
  node [
    id 720
    label "Liguria"
  ]
  node [
    id 721
    label "Kalabria"
  ]
  node [
    id 722
    label "Piemont"
  ]
  node [
    id 723
    label "Umbria"
  ]
  node [
    id 724
    label "lir"
  ]
  node [
    id 725
    label "Lombardia"
  ]
  node [
    id 726
    label "Warszawa"
  ]
  node [
    id 727
    label "Karyntia"
  ]
  node [
    id 728
    label "Sardynia"
  ]
  node [
    id 729
    label "Toskania"
  ]
  node [
    id 730
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 731
    label "Italia"
  ]
  node [
    id 732
    label "Kampania"
  ]
  node [
    id 733
    label "Sycylia"
  ]
  node [
    id 734
    label "Dacja"
  ]
  node [
    id 735
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 736
    label "lej_rumu&#324;ski"
  ]
  node [
    id 737
    label "Ba&#322;kany"
  ]
  node [
    id 738
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 739
    label "Siedmiogr&#243;d"
  ]
  node [
    id 740
    label "alawizm"
  ]
  node [
    id 741
    label "funt_syryjski"
  ]
  node [
    id 742
    label "frank_rwandyjski"
  ]
  node [
    id 743
    label "dinar_Bahrajnu"
  ]
  node [
    id 744
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 745
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 746
    label "frank_luksemburski"
  ]
  node [
    id 747
    label "peso_kuba&#324;skie"
  ]
  node [
    id 748
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 749
    label "frank_monakijski"
  ]
  node [
    id 750
    label "dinar_algierski"
  ]
  node [
    id 751
    label "Kabylia"
  ]
  node [
    id 752
    label "Oceania"
  ]
  node [
    id 753
    label "Sand&#380;ak"
  ]
  node [
    id 754
    label "Wojwodina"
  ]
  node [
    id 755
    label "dinar_serbski"
  ]
  node [
    id 756
    label "boliwar"
  ]
  node [
    id 757
    label "Orinoko"
  ]
  node [
    id 758
    label "tenge"
  ]
  node [
    id 759
    label "lek"
  ]
  node [
    id 760
    label "frank_alba&#324;ski"
  ]
  node [
    id 761
    label "dolar_Barbadosu"
  ]
  node [
    id 762
    label "Antyle"
  ]
  node [
    id 763
    label "Arakan"
  ]
  node [
    id 764
    label "kyat"
  ]
  node [
    id 765
    label "c&#243;rdoba"
  ]
  node [
    id 766
    label "Lesbos"
  ]
  node [
    id 767
    label "Tesalia"
  ]
  node [
    id 768
    label "Eolia"
  ]
  node [
    id 769
    label "panhellenizm"
  ]
  node [
    id 770
    label "Achaja"
  ]
  node [
    id 771
    label "Kreta"
  ]
  node [
    id 772
    label "Peloponez"
  ]
  node [
    id 773
    label "Olimp"
  ]
  node [
    id 774
    label "drachma"
  ]
  node [
    id 775
    label "Termopile"
  ]
  node [
    id 776
    label "Rodos"
  ]
  node [
    id 777
    label "palestra"
  ]
  node [
    id 778
    label "Eubea"
  ]
  node [
    id 779
    label "Paros"
  ]
  node [
    id 780
    label "Hellada"
  ]
  node [
    id 781
    label "Beocja"
  ]
  node [
    id 782
    label "Parnas"
  ]
  node [
    id 783
    label "Etolia"
  ]
  node [
    id 784
    label "Attyka"
  ]
  node [
    id 785
    label "Epir"
  ]
  node [
    id 786
    label "Mariany"
  ]
  node [
    id 787
    label "Tyrol"
  ]
  node [
    id 788
    label "Salzburg"
  ]
  node [
    id 789
    label "konsulent"
  ]
  node [
    id 790
    label "Rakuzy"
  ]
  node [
    id 791
    label "szyling_austryjacki"
  ]
  node [
    id 792
    label "Amhara"
  ]
  node [
    id 793
    label "negus"
  ]
  node [
    id 794
    label "birr"
  ]
  node [
    id 795
    label "Syjon"
  ]
  node [
    id 796
    label "rupia_indonezyjska"
  ]
  node [
    id 797
    label "Jawa"
  ]
  node [
    id 798
    label "Moluki"
  ]
  node [
    id 799
    label "Nowa_Gwinea"
  ]
  node [
    id 800
    label "Sumatra"
  ]
  node [
    id 801
    label "boliviano"
  ]
  node [
    id 802
    label "frank_francuski"
  ]
  node [
    id 803
    label "Lotaryngia"
  ]
  node [
    id 804
    label "Alzacja"
  ]
  node [
    id 805
    label "Gwadelupa"
  ]
  node [
    id 806
    label "Bordeaux"
  ]
  node [
    id 807
    label "Pikardia"
  ]
  node [
    id 808
    label "Sabaudia"
  ]
  node [
    id 809
    label "Korsyka"
  ]
  node [
    id 810
    label "Bretania"
  ]
  node [
    id 811
    label "Masyw_Centralny"
  ]
  node [
    id 812
    label "Armagnac"
  ]
  node [
    id 813
    label "Akwitania"
  ]
  node [
    id 814
    label "Wandea"
  ]
  node [
    id 815
    label "Martynika"
  ]
  node [
    id 816
    label "Prowansja"
  ]
  node [
    id 817
    label "Sekwana"
  ]
  node [
    id 818
    label "Normandia"
  ]
  node [
    id 819
    label "Burgundia"
  ]
  node [
    id 820
    label "Gaskonia"
  ]
  node [
    id 821
    label "Langwedocja"
  ]
  node [
    id 822
    label "somoni"
  ]
  node [
    id 823
    label "Melanezja"
  ]
  node [
    id 824
    label "dolar_Fid&#380;i"
  ]
  node [
    id 825
    label "Afrodyzje"
  ]
  node [
    id 826
    label "funt_cypryjski"
  ]
  node [
    id 827
    label "peso_dominika&#324;skie"
  ]
  node [
    id 828
    label "Fryburg"
  ]
  node [
    id 829
    label "Bazylea"
  ]
  node [
    id 830
    label "Helwecja"
  ]
  node [
    id 831
    label "Alpy"
  ]
  node [
    id 832
    label "Berno"
  ]
  node [
    id 833
    label "sum"
  ]
  node [
    id 834
    label "Karaka&#322;pacja"
  ]
  node [
    id 835
    label "Liwonia"
  ]
  node [
    id 836
    label "Kurlandia"
  ]
  node [
    id 837
    label "rubel_&#322;otewski"
  ]
  node [
    id 838
    label "&#322;at"
  ]
  node [
    id 839
    label "Windawa"
  ]
  node [
    id 840
    label "Inflanty"
  ]
  node [
    id 841
    label "&#379;mud&#378;"
  ]
  node [
    id 842
    label "lit"
  ]
  node [
    id 843
    label "dinar_tunezyjski"
  ]
  node [
    id 844
    label "frank_tunezyjski"
  ]
  node [
    id 845
    label "lempira"
  ]
  node [
    id 846
    label "Lipt&#243;w"
  ]
  node [
    id 847
    label "korona_w&#281;gierska"
  ]
  node [
    id 848
    label "forint"
  ]
  node [
    id 849
    label "dong"
  ]
  node [
    id 850
    label "Annam"
  ]
  node [
    id 851
    label "Tonkin"
  ]
  node [
    id 852
    label "lud"
  ]
  node [
    id 853
    label "frank_kongijski"
  ]
  node [
    id 854
    label "szyling_somalijski"
  ]
  node [
    id 855
    label "real"
  ]
  node [
    id 856
    label "cruzado"
  ]
  node [
    id 857
    label "Ma&#322;orosja"
  ]
  node [
    id 858
    label "Podole"
  ]
  node [
    id 859
    label "Nadbu&#380;e"
  ]
  node [
    id 860
    label "Przykarpacie"
  ]
  node [
    id 861
    label "Naddnieprze"
  ]
  node [
    id 862
    label "Zaporo&#380;e"
  ]
  node [
    id 863
    label "Kozaczyzna"
  ]
  node [
    id 864
    label "Wsch&#243;d"
  ]
  node [
    id 865
    label "karbowaniec"
  ]
  node [
    id 866
    label "hrywna"
  ]
  node [
    id 867
    label "Ukraina_Zachodnia"
  ]
  node [
    id 868
    label "Dniestr"
  ]
  node [
    id 869
    label "Krym"
  ]
  node [
    id 870
    label "Zakarpacie"
  ]
  node [
    id 871
    label "Wo&#322;y&#324;"
  ]
  node [
    id 872
    label "Tasmania"
  ]
  node [
    id 873
    label "dolar_australijski"
  ]
  node [
    id 874
    label "Nowy_&#346;wiat"
  ]
  node [
    id 875
    label "gourde"
  ]
  node [
    id 876
    label "kwanza"
  ]
  node [
    id 877
    label "escudo_angolskie"
  ]
  node [
    id 878
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 879
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 880
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 881
    label "lari"
  ]
  node [
    id 882
    label "Ad&#380;aria"
  ]
  node [
    id 883
    label "naira"
  ]
  node [
    id 884
    label "Hudson"
  ]
  node [
    id 885
    label "Teksas"
  ]
  node [
    id 886
    label "Georgia"
  ]
  node [
    id 887
    label "Maryland"
  ]
  node [
    id 888
    label "Michigan"
  ]
  node [
    id 889
    label "Massachusetts"
  ]
  node [
    id 890
    label "Luizjana"
  ]
  node [
    id 891
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 892
    label "stan_wolny"
  ]
  node [
    id 893
    label "Floryda"
  ]
  node [
    id 894
    label "Po&#322;udnie"
  ]
  node [
    id 895
    label "Ohio"
  ]
  node [
    id 896
    label "Alaska"
  ]
  node [
    id 897
    label "Nowy_Meksyk"
  ]
  node [
    id 898
    label "Wuj_Sam"
  ]
  node [
    id 899
    label "Kansas"
  ]
  node [
    id 900
    label "Alabama"
  ]
  node [
    id 901
    label "Kalifornia"
  ]
  node [
    id 902
    label "Wirginia"
  ]
  node [
    id 903
    label "Nowy_York"
  ]
  node [
    id 904
    label "Waszyngton"
  ]
  node [
    id 905
    label "Pensylwania"
  ]
  node [
    id 906
    label "zielona_karta"
  ]
  node [
    id 907
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 908
    label "P&#243;&#322;noc"
  ]
  node [
    id 909
    label "Hawaje"
  ]
  node [
    id 910
    label "Zach&#243;d"
  ]
  node [
    id 911
    label "Illinois"
  ]
  node [
    id 912
    label "Oklahoma"
  ]
  node [
    id 913
    label "Oregon"
  ]
  node [
    id 914
    label "Arizona"
  ]
  node [
    id 915
    label "som"
  ]
  node [
    id 916
    label "peso_urugwajskie"
  ]
  node [
    id 917
    label "denar_macedo&#324;ski"
  ]
  node [
    id 918
    label "dolar_Brunei"
  ]
  node [
    id 919
    label "Persja"
  ]
  node [
    id 920
    label "rial_ira&#324;ski"
  ]
  node [
    id 921
    label "mu&#322;&#322;a"
  ]
  node [
    id 922
    label "dinar_libijski"
  ]
  node [
    id 923
    label "d&#380;amahirijja"
  ]
  node [
    id 924
    label "nakfa"
  ]
  node [
    id 925
    label "rial_katarski"
  ]
  node [
    id 926
    label "quetzal"
  ]
  node [
    id 927
    label "won"
  ]
  node [
    id 928
    label "rial_jeme&#324;ski"
  ]
  node [
    id 929
    label "peso_argenty&#324;skie"
  ]
  node [
    id 930
    label "guarani"
  ]
  node [
    id 931
    label "perper"
  ]
  node [
    id 932
    label "dinar_kuwejcki"
  ]
  node [
    id 933
    label "dalasi"
  ]
  node [
    id 934
    label "dolar_Zimbabwe"
  ]
  node [
    id 935
    label "Szantung"
  ]
  node [
    id 936
    label "Mand&#380;uria"
  ]
  node [
    id 937
    label "Hongkong"
  ]
  node [
    id 938
    label "Guangdong"
  ]
  node [
    id 939
    label "yuan"
  ]
  node [
    id 940
    label "D&#380;ungaria"
  ]
  node [
    id 941
    label "Chiny_Zachodnie"
  ]
  node [
    id 942
    label "Junnan"
  ]
  node [
    id 943
    label "Kuantung"
  ]
  node [
    id 944
    label "Chiny_Wschodnie"
  ]
  node [
    id 945
    label "Syczuan"
  ]
  node [
    id 946
    label "Krajna"
  ]
  node [
    id 947
    label "Kielecczyzna"
  ]
  node [
    id 948
    label "Opolskie"
  ]
  node [
    id 949
    label "Lubuskie"
  ]
  node [
    id 950
    label "Mazowsze"
  ]
  node [
    id 951
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 952
    label "Kaczawa"
  ]
  node [
    id 953
    label "Podlasie"
  ]
  node [
    id 954
    label "Wolin"
  ]
  node [
    id 955
    label "Wielkopolska"
  ]
  node [
    id 956
    label "Lubelszczyzna"
  ]
  node [
    id 957
    label "So&#322;a"
  ]
  node [
    id 958
    label "Wis&#322;a"
  ]
  node [
    id 959
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 960
    label "Pa&#322;uki"
  ]
  node [
    id 961
    label "Pomorze_Zachodnie"
  ]
  node [
    id 962
    label "Podkarpacie"
  ]
  node [
    id 963
    label "barwy_polskie"
  ]
  node [
    id 964
    label "Kujawy"
  ]
  node [
    id 965
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 966
    label "Warmia"
  ]
  node [
    id 967
    label "&#321;&#243;dzkie"
  ]
  node [
    id 968
    label "Suwalszczyzna"
  ]
  node [
    id 969
    label "Bory_Tucholskie"
  ]
  node [
    id 970
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 971
    label "z&#322;oty"
  ]
  node [
    id 972
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 973
    label "Ma&#322;opolska"
  ]
  node [
    id 974
    label "Mazury"
  ]
  node [
    id 975
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 976
    label "Powi&#347;le"
  ]
  node [
    id 977
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 978
    label "Azja_Mniejsza"
  ]
  node [
    id 979
    label "lira_turecka"
  ]
  node [
    id 980
    label "Ujgur"
  ]
  node [
    id 981
    label "kuna"
  ]
  node [
    id 982
    label "dram"
  ]
  node [
    id 983
    label "tala"
  ]
  node [
    id 984
    label "korona_s&#322;owacka"
  ]
  node [
    id 985
    label "Turiec"
  ]
  node [
    id 986
    label "rupia_nepalska"
  ]
  node [
    id 987
    label "Himalaje"
  ]
  node [
    id 988
    label "frank_gwinejski"
  ]
  node [
    id 989
    label "marka_esto&#324;ska"
  ]
  node [
    id 990
    label "Skandynawia"
  ]
  node [
    id 991
    label "korona_esto&#324;ska"
  ]
  node [
    id 992
    label "Nowa_Fundlandia"
  ]
  node [
    id 993
    label "Quebec"
  ]
  node [
    id 994
    label "dolar_kanadyjski"
  ]
  node [
    id 995
    label "Zanzibar"
  ]
  node [
    id 996
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 997
    label "&#346;wite&#378;"
  ]
  node [
    id 998
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 999
    label "peso_kolumbijskie"
  ]
  node [
    id 1000
    label "funt_egipski"
  ]
  node [
    id 1001
    label "Synaj"
  ]
  node [
    id 1002
    label "paraszyt"
  ]
  node [
    id 1003
    label "afgani"
  ]
  node [
    id 1004
    label "Baktria"
  ]
  node [
    id 1005
    label "szach"
  ]
  node [
    id 1006
    label "baht"
  ]
  node [
    id 1007
    label "tolar"
  ]
  node [
    id 1008
    label "Naddniestrze"
  ]
  node [
    id 1009
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1010
    label "Gagauzja"
  ]
  node [
    id 1011
    label "posadzka"
  ]
  node [
    id 1012
    label "podglebie"
  ]
  node [
    id 1013
    label "Ko&#322;yma"
  ]
  node [
    id 1014
    label "Indochiny"
  ]
  node [
    id 1015
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1016
    label "Bo&#347;nia"
  ]
  node [
    id 1017
    label "Kaukaz"
  ]
  node [
    id 1018
    label "Opolszczyzna"
  ]
  node [
    id 1019
    label "czynnik_produkcji"
  ]
  node [
    id 1020
    label "kort"
  ]
  node [
    id 1021
    label "Polesie"
  ]
  node [
    id 1022
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1023
    label "Yorkshire"
  ]
  node [
    id 1024
    label "zapadnia"
  ]
  node [
    id 1025
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1026
    label "Noworosja"
  ]
  node [
    id 1027
    label "glinowa&#263;"
  ]
  node [
    id 1028
    label "litosfera"
  ]
  node [
    id 1029
    label "Kurpie"
  ]
  node [
    id 1030
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1031
    label "Kociewie"
  ]
  node [
    id 1032
    label "Anglia"
  ]
  node [
    id 1033
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1034
    label "Laponia"
  ]
  node [
    id 1035
    label "Amazonia"
  ]
  node [
    id 1036
    label "Hercegowina"
  ]
  node [
    id 1037
    label "Pamir"
  ]
  node [
    id 1038
    label "powierzchnia"
  ]
  node [
    id 1039
    label "p&#322;aszczyzna"
  ]
  node [
    id 1040
    label "Podhale"
  ]
  node [
    id 1041
    label "pomieszczenie"
  ]
  node [
    id 1042
    label "plantowa&#263;"
  ]
  node [
    id 1043
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1044
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1045
    label "dotleni&#263;"
  ]
  node [
    id 1046
    label "Zabajkale"
  ]
  node [
    id 1047
    label "skorupa_ziemska"
  ]
  node [
    id 1048
    label "glinowanie"
  ]
  node [
    id 1049
    label "Kaszuby"
  ]
  node [
    id 1050
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1051
    label "Oksytania"
  ]
  node [
    id 1052
    label "Mezoameryka"
  ]
  node [
    id 1053
    label "Turkiestan"
  ]
  node [
    id 1054
    label "Kurdystan"
  ]
  node [
    id 1055
    label "glej"
  ]
  node [
    id 1056
    label "Biskupizna"
  ]
  node [
    id 1057
    label "Podbeskidzie"
  ]
  node [
    id 1058
    label "Zag&#243;rze"
  ]
  node [
    id 1059
    label "Szkocja"
  ]
  node [
    id 1060
    label "domain"
  ]
  node [
    id 1061
    label "Huculszczyzna"
  ]
  node [
    id 1062
    label "pojazd"
  ]
  node [
    id 1063
    label "budynek"
  ]
  node [
    id 1064
    label "S&#261;decczyzna"
  ]
  node [
    id 1065
    label "Palestyna"
  ]
  node [
    id 1066
    label "Lauda"
  ]
  node [
    id 1067
    label "penetrator"
  ]
  node [
    id 1068
    label "Bojkowszczyzna"
  ]
  node [
    id 1069
    label "ryzosfera"
  ]
  node [
    id 1070
    label "Zamojszczyzna"
  ]
  node [
    id 1071
    label "Walia"
  ]
  node [
    id 1072
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1073
    label "martwica"
  ]
  node [
    id 1074
    label "pr&#243;chnica"
  ]
  node [
    id 1075
    label "ustawa"
  ]
  node [
    id 1076
    label "zeszyt"
  ]
  node [
    id 1077
    label "dzie&#324;"
  ]
  node [
    id 1078
    label "17"
  ]
  node [
    id 1079
    label "luty"
  ]
  node [
    id 1080
    label "2005"
  ]
  node [
    id 1081
    label "rok"
  ]
  node [
    id 1082
    label "ojciec"
  ]
  node [
    id 1083
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1084
    label "realizowa&#263;"
  ]
  node [
    id 1085
    label "zada&#263;"
  ]
  node [
    id 1086
    label "publiczny"
  ]
  node [
    id 1087
    label "rada"
  ]
  node [
    id 1088
    label "minister"
  ]
  node [
    id 1089
    label "ministerstwo"
  ]
  node [
    id 1090
    label "sprawi&#263;"
  ]
  node [
    id 1091
    label "wewn&#281;trzny"
  ]
  node [
    id 1092
    label "i"
  ]
  node [
    id 1093
    label "administracja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 1075
  ]
  edge [
    source 1
    target 1076
  ]
  edge [
    source 1
    target 1077
  ]
  edge [
    source 1
    target 1078
  ]
  edge [
    source 1
    target 1079
  ]
  edge [
    source 1
    target 1080
  ]
  edge [
    source 1
    target 1081
  ]
  edge [
    source 1
    target 1082
  ]
  edge [
    source 1
    target 1083
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 1084
  ]
  edge [
    source 1
    target 1085
  ]
  edge [
    source 1
    target 1086
  ]
  edge [
    source 1
    target 1087
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 569
  ]
  edge [
    source 2
    target 570
  ]
  edge [
    source 2
    target 571
  ]
  edge [
    source 2
    target 572
  ]
  edge [
    source 2
    target 573
  ]
  edge [
    source 2
    target 574
  ]
  edge [
    source 2
    target 575
  ]
  edge [
    source 2
    target 576
  ]
  edge [
    source 2
    target 577
  ]
  edge [
    source 2
    target 578
  ]
  edge [
    source 2
    target 579
  ]
  edge [
    source 2
    target 580
  ]
  edge [
    source 2
    target 581
  ]
  edge [
    source 2
    target 582
  ]
  edge [
    source 2
    target 583
  ]
  edge [
    source 2
    target 584
  ]
  edge [
    source 2
    target 585
  ]
  edge [
    source 2
    target 586
  ]
  edge [
    source 2
    target 587
  ]
  edge [
    source 2
    target 588
  ]
  edge [
    source 2
    target 589
  ]
  edge [
    source 2
    target 590
  ]
  edge [
    source 2
    target 591
  ]
  edge [
    source 2
    target 592
  ]
  edge [
    source 2
    target 593
  ]
  edge [
    source 2
    target 594
  ]
  edge [
    source 2
    target 595
  ]
  edge [
    source 2
    target 596
  ]
  edge [
    source 2
    target 597
  ]
  edge [
    source 2
    target 598
  ]
  edge [
    source 2
    target 599
  ]
  edge [
    source 2
    target 600
  ]
  edge [
    source 2
    target 601
  ]
  edge [
    source 2
    target 602
  ]
  edge [
    source 2
    target 603
  ]
  edge [
    source 2
    target 604
  ]
  edge [
    source 2
    target 605
  ]
  edge [
    source 2
    target 606
  ]
  edge [
    source 2
    target 607
  ]
  edge [
    source 2
    target 608
  ]
  edge [
    source 2
    target 609
  ]
  edge [
    source 2
    target 610
  ]
  edge [
    source 2
    target 611
  ]
  edge [
    source 2
    target 612
  ]
  edge [
    source 2
    target 613
  ]
  edge [
    source 2
    target 614
  ]
  edge [
    source 2
    target 615
  ]
  edge [
    source 2
    target 616
  ]
  edge [
    source 2
    target 617
  ]
  edge [
    source 2
    target 618
  ]
  edge [
    source 2
    target 619
  ]
  edge [
    source 2
    target 620
  ]
  edge [
    source 2
    target 621
  ]
  edge [
    source 2
    target 622
  ]
  edge [
    source 2
    target 623
  ]
  edge [
    source 2
    target 624
  ]
  edge [
    source 2
    target 625
  ]
  edge [
    source 2
    target 626
  ]
  edge [
    source 2
    target 627
  ]
  edge [
    source 2
    target 628
  ]
  edge [
    source 2
    target 629
  ]
  edge [
    source 2
    target 630
  ]
  edge [
    source 2
    target 631
  ]
  edge [
    source 2
    target 632
  ]
  edge [
    source 2
    target 633
  ]
  edge [
    source 2
    target 634
  ]
  edge [
    source 2
    target 635
  ]
  edge [
    source 2
    target 636
  ]
  edge [
    source 2
    target 637
  ]
  edge [
    source 2
    target 638
  ]
  edge [
    source 2
    target 639
  ]
  edge [
    source 2
    target 640
  ]
  edge [
    source 2
    target 641
  ]
  edge [
    source 2
    target 642
  ]
  edge [
    source 2
    target 643
  ]
  edge [
    source 2
    target 644
  ]
  edge [
    source 2
    target 645
  ]
  edge [
    source 2
    target 646
  ]
  edge [
    source 2
    target 647
  ]
  edge [
    source 2
    target 648
  ]
  edge [
    source 2
    target 649
  ]
  edge [
    source 2
    target 650
  ]
  edge [
    source 2
    target 651
  ]
  edge [
    source 2
    target 652
  ]
  edge [
    source 2
    target 653
  ]
  edge [
    source 2
    target 654
  ]
  edge [
    source 2
    target 655
  ]
  edge [
    source 2
    target 656
  ]
  edge [
    source 2
    target 657
  ]
  edge [
    source 2
    target 658
  ]
  edge [
    source 2
    target 659
  ]
  edge [
    source 2
    target 660
  ]
  edge [
    source 2
    target 661
  ]
  edge [
    source 2
    target 662
  ]
  edge [
    source 2
    target 663
  ]
  edge [
    source 2
    target 664
  ]
  edge [
    source 2
    target 665
  ]
  edge [
    source 2
    target 666
  ]
  edge [
    source 2
    target 667
  ]
  edge [
    source 2
    target 668
  ]
  edge [
    source 2
    target 669
  ]
  edge [
    source 2
    target 670
  ]
  edge [
    source 2
    target 671
  ]
  edge [
    source 2
    target 672
  ]
  edge [
    source 2
    target 673
  ]
  edge [
    source 2
    target 674
  ]
  edge [
    source 2
    target 675
  ]
  edge [
    source 2
    target 676
  ]
  edge [
    source 2
    target 677
  ]
  edge [
    source 2
    target 678
  ]
  edge [
    source 2
    target 679
  ]
  edge [
    source 2
    target 680
  ]
  edge [
    source 2
    target 681
  ]
  edge [
    source 2
    target 682
  ]
  edge [
    source 2
    target 683
  ]
  edge [
    source 2
    target 684
  ]
  edge [
    source 2
    target 685
  ]
  edge [
    source 2
    target 686
  ]
  edge [
    source 2
    target 687
  ]
  edge [
    source 2
    target 688
  ]
  edge [
    source 2
    target 689
  ]
  edge [
    source 2
    target 690
  ]
  edge [
    source 2
    target 691
  ]
  edge [
    source 2
    target 692
  ]
  edge [
    source 2
    target 693
  ]
  edge [
    source 2
    target 694
  ]
  edge [
    source 2
    target 695
  ]
  edge [
    source 2
    target 696
  ]
  edge [
    source 2
    target 697
  ]
  edge [
    source 2
    target 698
  ]
  edge [
    source 2
    target 699
  ]
  edge [
    source 2
    target 700
  ]
  edge [
    source 2
    target 701
  ]
  edge [
    source 2
    target 702
  ]
  edge [
    source 2
    target 703
  ]
  edge [
    source 2
    target 704
  ]
  edge [
    source 2
    target 705
  ]
  edge [
    source 2
    target 706
  ]
  edge [
    source 2
    target 707
  ]
  edge [
    source 2
    target 708
  ]
  edge [
    source 2
    target 709
  ]
  edge [
    source 2
    target 710
  ]
  edge [
    source 2
    target 711
  ]
  edge [
    source 2
    target 712
  ]
  edge [
    source 2
    target 713
  ]
  edge [
    source 2
    target 714
  ]
  edge [
    source 2
    target 715
  ]
  edge [
    source 2
    target 716
  ]
  edge [
    source 2
    target 717
  ]
  edge [
    source 2
    target 718
  ]
  edge [
    source 2
    target 719
  ]
  edge [
    source 2
    target 720
  ]
  edge [
    source 2
    target 721
  ]
  edge [
    source 2
    target 722
  ]
  edge [
    source 2
    target 723
  ]
  edge [
    source 2
    target 724
  ]
  edge [
    source 2
    target 725
  ]
  edge [
    source 2
    target 726
  ]
  edge [
    source 2
    target 727
  ]
  edge [
    source 2
    target 728
  ]
  edge [
    source 2
    target 729
  ]
  edge [
    source 2
    target 730
  ]
  edge [
    source 2
    target 731
  ]
  edge [
    source 2
    target 732
  ]
  edge [
    source 2
    target 733
  ]
  edge [
    source 2
    target 734
  ]
  edge [
    source 2
    target 735
  ]
  edge [
    source 2
    target 736
  ]
  edge [
    source 2
    target 737
  ]
  edge [
    source 2
    target 738
  ]
  edge [
    source 2
    target 739
  ]
  edge [
    source 2
    target 740
  ]
  edge [
    source 2
    target 741
  ]
  edge [
    source 2
    target 742
  ]
  edge [
    source 2
    target 743
  ]
  edge [
    source 2
    target 744
  ]
  edge [
    source 2
    target 745
  ]
  edge [
    source 2
    target 746
  ]
  edge [
    source 2
    target 747
  ]
  edge [
    source 2
    target 748
  ]
  edge [
    source 2
    target 749
  ]
  edge [
    source 2
    target 750
  ]
  edge [
    source 2
    target 751
  ]
  edge [
    source 2
    target 752
  ]
  edge [
    source 2
    target 753
  ]
  edge [
    source 2
    target 754
  ]
  edge [
    source 2
    target 755
  ]
  edge [
    source 2
    target 756
  ]
  edge [
    source 2
    target 757
  ]
  edge [
    source 2
    target 758
  ]
  edge [
    source 2
    target 759
  ]
  edge [
    source 2
    target 760
  ]
  edge [
    source 2
    target 761
  ]
  edge [
    source 2
    target 762
  ]
  edge [
    source 2
    target 763
  ]
  edge [
    source 2
    target 764
  ]
  edge [
    source 2
    target 765
  ]
  edge [
    source 2
    target 766
  ]
  edge [
    source 2
    target 767
  ]
  edge [
    source 2
    target 768
  ]
  edge [
    source 2
    target 769
  ]
  edge [
    source 2
    target 770
  ]
  edge [
    source 2
    target 771
  ]
  edge [
    source 2
    target 772
  ]
  edge [
    source 2
    target 773
  ]
  edge [
    source 2
    target 774
  ]
  edge [
    source 2
    target 775
  ]
  edge [
    source 2
    target 776
  ]
  edge [
    source 2
    target 777
  ]
  edge [
    source 2
    target 778
  ]
  edge [
    source 2
    target 779
  ]
  edge [
    source 2
    target 780
  ]
  edge [
    source 2
    target 781
  ]
  edge [
    source 2
    target 782
  ]
  edge [
    source 2
    target 783
  ]
  edge [
    source 2
    target 784
  ]
  edge [
    source 2
    target 785
  ]
  edge [
    source 2
    target 786
  ]
  edge [
    source 2
    target 787
  ]
  edge [
    source 2
    target 788
  ]
  edge [
    source 2
    target 789
  ]
  edge [
    source 2
    target 790
  ]
  edge [
    source 2
    target 791
  ]
  edge [
    source 2
    target 792
  ]
  edge [
    source 2
    target 793
  ]
  edge [
    source 2
    target 794
  ]
  edge [
    source 2
    target 795
  ]
  edge [
    source 2
    target 796
  ]
  edge [
    source 2
    target 797
  ]
  edge [
    source 2
    target 798
  ]
  edge [
    source 2
    target 799
  ]
  edge [
    source 2
    target 800
  ]
  edge [
    source 2
    target 801
  ]
  edge [
    source 2
    target 802
  ]
  edge [
    source 2
    target 803
  ]
  edge [
    source 2
    target 804
  ]
  edge [
    source 2
    target 805
  ]
  edge [
    source 2
    target 806
  ]
  edge [
    source 2
    target 807
  ]
  edge [
    source 2
    target 808
  ]
  edge [
    source 2
    target 809
  ]
  edge [
    source 2
    target 810
  ]
  edge [
    source 2
    target 811
  ]
  edge [
    source 2
    target 812
  ]
  edge [
    source 2
    target 813
  ]
  edge [
    source 2
    target 814
  ]
  edge [
    source 2
    target 815
  ]
  edge [
    source 2
    target 816
  ]
  edge [
    source 2
    target 817
  ]
  edge [
    source 2
    target 818
  ]
  edge [
    source 2
    target 819
  ]
  edge [
    source 2
    target 820
  ]
  edge [
    source 2
    target 821
  ]
  edge [
    source 2
    target 822
  ]
  edge [
    source 2
    target 823
  ]
  edge [
    source 2
    target 824
  ]
  edge [
    source 2
    target 825
  ]
  edge [
    source 2
    target 826
  ]
  edge [
    source 2
    target 827
  ]
  edge [
    source 2
    target 828
  ]
  edge [
    source 2
    target 829
  ]
  edge [
    source 2
    target 830
  ]
  edge [
    source 2
    target 831
  ]
  edge [
    source 2
    target 832
  ]
  edge [
    source 2
    target 833
  ]
  edge [
    source 2
    target 834
  ]
  edge [
    source 2
    target 835
  ]
  edge [
    source 2
    target 836
  ]
  edge [
    source 2
    target 837
  ]
  edge [
    source 2
    target 838
  ]
  edge [
    source 2
    target 839
  ]
  edge [
    source 2
    target 840
  ]
  edge [
    source 2
    target 841
  ]
  edge [
    source 2
    target 842
  ]
  edge [
    source 2
    target 843
  ]
  edge [
    source 2
    target 844
  ]
  edge [
    source 2
    target 845
  ]
  edge [
    source 2
    target 846
  ]
  edge [
    source 2
    target 847
  ]
  edge [
    source 2
    target 848
  ]
  edge [
    source 2
    target 849
  ]
  edge [
    source 2
    target 850
  ]
  edge [
    source 2
    target 851
  ]
  edge [
    source 2
    target 852
  ]
  edge [
    source 2
    target 853
  ]
  edge [
    source 2
    target 854
  ]
  edge [
    source 2
    target 855
  ]
  edge [
    source 2
    target 856
  ]
  edge [
    source 2
    target 857
  ]
  edge [
    source 2
    target 858
  ]
  edge [
    source 2
    target 859
  ]
  edge [
    source 2
    target 860
  ]
  edge [
    source 2
    target 861
  ]
  edge [
    source 2
    target 862
  ]
  edge [
    source 2
    target 863
  ]
  edge [
    source 2
    target 864
  ]
  edge [
    source 2
    target 865
  ]
  edge [
    source 2
    target 866
  ]
  edge [
    source 2
    target 867
  ]
  edge [
    source 2
    target 868
  ]
  edge [
    source 2
    target 869
  ]
  edge [
    source 2
    target 870
  ]
  edge [
    source 2
    target 871
  ]
  edge [
    source 2
    target 872
  ]
  edge [
    source 2
    target 873
  ]
  edge [
    source 2
    target 874
  ]
  edge [
    source 2
    target 875
  ]
  edge [
    source 2
    target 876
  ]
  edge [
    source 2
    target 877
  ]
  edge [
    source 2
    target 878
  ]
  edge [
    source 2
    target 879
  ]
  edge [
    source 2
    target 880
  ]
  edge [
    source 2
    target 881
  ]
  edge [
    source 2
    target 882
  ]
  edge [
    source 2
    target 883
  ]
  edge [
    source 2
    target 884
  ]
  edge [
    source 2
    target 885
  ]
  edge [
    source 2
    target 886
  ]
  edge [
    source 2
    target 887
  ]
  edge [
    source 2
    target 888
  ]
  edge [
    source 2
    target 889
  ]
  edge [
    source 2
    target 890
  ]
  edge [
    source 2
    target 891
  ]
  edge [
    source 2
    target 892
  ]
  edge [
    source 2
    target 893
  ]
  edge [
    source 2
    target 894
  ]
  edge [
    source 2
    target 895
  ]
  edge [
    source 2
    target 896
  ]
  edge [
    source 2
    target 897
  ]
  edge [
    source 2
    target 898
  ]
  edge [
    source 2
    target 899
  ]
  edge [
    source 2
    target 900
  ]
  edge [
    source 2
    target 901
  ]
  edge [
    source 2
    target 902
  ]
  edge [
    source 2
    target 903
  ]
  edge [
    source 2
    target 904
  ]
  edge [
    source 2
    target 905
  ]
  edge [
    source 2
    target 906
  ]
  edge [
    source 2
    target 907
  ]
  edge [
    source 2
    target 908
  ]
  edge [
    source 2
    target 909
  ]
  edge [
    source 2
    target 910
  ]
  edge [
    source 2
    target 911
  ]
  edge [
    source 2
    target 912
  ]
  edge [
    source 2
    target 913
  ]
  edge [
    source 2
    target 914
  ]
  edge [
    source 2
    target 915
  ]
  edge [
    source 2
    target 916
  ]
  edge [
    source 2
    target 917
  ]
  edge [
    source 2
    target 918
  ]
  edge [
    source 2
    target 919
  ]
  edge [
    source 2
    target 920
  ]
  edge [
    source 2
    target 921
  ]
  edge [
    source 2
    target 922
  ]
  edge [
    source 2
    target 923
  ]
  edge [
    source 2
    target 924
  ]
  edge [
    source 2
    target 925
  ]
  edge [
    source 2
    target 926
  ]
  edge [
    source 2
    target 927
  ]
  edge [
    source 2
    target 928
  ]
  edge [
    source 2
    target 929
  ]
  edge [
    source 2
    target 930
  ]
  edge [
    source 2
    target 931
  ]
  edge [
    source 2
    target 932
  ]
  edge [
    source 2
    target 933
  ]
  edge [
    source 2
    target 934
  ]
  edge [
    source 2
    target 935
  ]
  edge [
    source 2
    target 936
  ]
  edge [
    source 2
    target 937
  ]
  edge [
    source 2
    target 938
  ]
  edge [
    source 2
    target 939
  ]
  edge [
    source 2
    target 940
  ]
  edge [
    source 2
    target 941
  ]
  edge [
    source 2
    target 942
  ]
  edge [
    source 2
    target 943
  ]
  edge [
    source 2
    target 944
  ]
  edge [
    source 2
    target 945
  ]
  edge [
    source 2
    target 946
  ]
  edge [
    source 2
    target 947
  ]
  edge [
    source 2
    target 948
  ]
  edge [
    source 2
    target 949
  ]
  edge [
    source 2
    target 950
  ]
  edge [
    source 2
    target 951
  ]
  edge [
    source 2
    target 952
  ]
  edge [
    source 2
    target 953
  ]
  edge [
    source 2
    target 954
  ]
  edge [
    source 2
    target 955
  ]
  edge [
    source 2
    target 956
  ]
  edge [
    source 2
    target 957
  ]
  edge [
    source 2
    target 958
  ]
  edge [
    source 2
    target 959
  ]
  edge [
    source 2
    target 960
  ]
  edge [
    source 2
    target 961
  ]
  edge [
    source 2
    target 962
  ]
  edge [
    source 2
    target 963
  ]
  edge [
    source 2
    target 964
  ]
  edge [
    source 2
    target 965
  ]
  edge [
    source 2
    target 966
  ]
  edge [
    source 2
    target 967
  ]
  edge [
    source 2
    target 968
  ]
  edge [
    source 2
    target 969
  ]
  edge [
    source 2
    target 970
  ]
  edge [
    source 2
    target 971
  ]
  edge [
    source 2
    target 972
  ]
  edge [
    source 2
    target 973
  ]
  edge [
    source 2
    target 974
  ]
  edge [
    source 2
    target 975
  ]
  edge [
    source 2
    target 976
  ]
  edge [
    source 2
    target 977
  ]
  edge [
    source 2
    target 978
  ]
  edge [
    source 2
    target 979
  ]
  edge [
    source 2
    target 980
  ]
  edge [
    source 2
    target 981
  ]
  edge [
    source 2
    target 982
  ]
  edge [
    source 2
    target 983
  ]
  edge [
    source 2
    target 984
  ]
  edge [
    source 2
    target 985
  ]
  edge [
    source 2
    target 986
  ]
  edge [
    source 2
    target 987
  ]
  edge [
    source 2
    target 988
  ]
  edge [
    source 2
    target 989
  ]
  edge [
    source 2
    target 990
  ]
  edge [
    source 2
    target 991
  ]
  edge [
    source 2
    target 992
  ]
  edge [
    source 2
    target 993
  ]
  edge [
    source 2
    target 994
  ]
  edge [
    source 2
    target 995
  ]
  edge [
    source 2
    target 996
  ]
  edge [
    source 2
    target 997
  ]
  edge [
    source 2
    target 998
  ]
  edge [
    source 2
    target 999
  ]
  edge [
    source 2
    target 1000
  ]
  edge [
    source 2
    target 1001
  ]
  edge [
    source 2
    target 1002
  ]
  edge [
    source 2
    target 1003
  ]
  edge [
    source 2
    target 1004
  ]
  edge [
    source 2
    target 1005
  ]
  edge [
    source 2
    target 1006
  ]
  edge [
    source 2
    target 1007
  ]
  edge [
    source 2
    target 1008
  ]
  edge [
    source 2
    target 1009
  ]
  edge [
    source 2
    target 1010
  ]
  edge [
    source 2
    target 1011
  ]
  edge [
    source 2
    target 1012
  ]
  edge [
    source 2
    target 1013
  ]
  edge [
    source 2
    target 1014
  ]
  edge [
    source 2
    target 1015
  ]
  edge [
    source 2
    target 1016
  ]
  edge [
    source 2
    target 1017
  ]
  edge [
    source 2
    target 1018
  ]
  edge [
    source 2
    target 1019
  ]
  edge [
    source 2
    target 1020
  ]
  edge [
    source 2
    target 1021
  ]
  edge [
    source 2
    target 1022
  ]
  edge [
    source 2
    target 1023
  ]
  edge [
    source 2
    target 1024
  ]
  edge [
    source 2
    target 1025
  ]
  edge [
    source 2
    target 1026
  ]
  edge [
    source 2
    target 1027
  ]
  edge [
    source 2
    target 1028
  ]
  edge [
    source 2
    target 1029
  ]
  edge [
    source 2
    target 1030
  ]
  edge [
    source 2
    target 1031
  ]
  edge [
    source 2
    target 1032
  ]
  edge [
    source 2
    target 1033
  ]
  edge [
    source 2
    target 1034
  ]
  edge [
    source 2
    target 1035
  ]
  edge [
    source 2
    target 1036
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 1037
  ]
  edge [
    source 2
    target 1038
  ]
  edge [
    source 2
    target 1039
  ]
  edge [
    source 2
    target 1040
  ]
  edge [
    source 2
    target 1041
  ]
  edge [
    source 2
    target 1042
  ]
  edge [
    source 2
    target 1043
  ]
  edge [
    source 2
    target 1044
  ]
  edge [
    source 2
    target 1045
  ]
  edge [
    source 2
    target 1046
  ]
  edge [
    source 2
    target 1047
  ]
  edge [
    source 2
    target 1048
  ]
  edge [
    source 2
    target 1049
  ]
  edge [
    source 2
    target 1050
  ]
  edge [
    source 2
    target 1051
  ]
  edge [
    source 2
    target 1052
  ]
  edge [
    source 2
    target 1053
  ]
  edge [
    source 2
    target 1054
  ]
  edge [
    source 2
    target 1055
  ]
  edge [
    source 2
    target 1056
  ]
  edge [
    source 2
    target 1057
  ]
  edge [
    source 2
    target 1058
  ]
  edge [
    source 2
    target 1059
  ]
  edge [
    source 2
    target 1060
  ]
  edge [
    source 2
    target 1061
  ]
  edge [
    source 2
    target 1062
  ]
  edge [
    source 2
    target 1063
  ]
  edge [
    source 2
    target 1064
  ]
  edge [
    source 2
    target 1065
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 1066
  ]
  edge [
    source 2
    target 1067
  ]
  edge [
    source 2
    target 1068
  ]
  edge [
    source 2
    target 1069
  ]
  edge [
    source 2
    target 1070
  ]
  edge [
    source 2
    target 1071
  ]
  edge [
    source 2
    target 1072
  ]
  edge [
    source 2
    target 1073
  ]
  edge [
    source 2
    target 1074
  ]
  edge [
    source 416
    target 1075
  ]
  edge [
    source 416
    target 1076
  ]
  edge [
    source 416
    target 1077
  ]
  edge [
    source 416
    target 1078
  ]
  edge [
    source 416
    target 1079
  ]
  edge [
    source 416
    target 1080
  ]
  edge [
    source 416
    target 1081
  ]
  edge [
    source 416
    target 1082
  ]
  edge [
    source 416
    target 1083
  ]
  edge [
    source 416
    target 1084
  ]
  edge [
    source 416
    target 1085
  ]
  edge [
    source 416
    target 1086
  ]
  edge [
    source 1075
    target 1076
  ]
  edge [
    source 1075
    target 1077
  ]
  edge [
    source 1075
    target 1078
  ]
  edge [
    source 1075
    target 1079
  ]
  edge [
    source 1075
    target 1080
  ]
  edge [
    source 1075
    target 1081
  ]
  edge [
    source 1075
    target 1082
  ]
  edge [
    source 1075
    target 1083
  ]
  edge [
    source 1075
    target 1084
  ]
  edge [
    source 1075
    target 1085
  ]
  edge [
    source 1075
    target 1086
  ]
  edge [
    source 1076
    target 1077
  ]
  edge [
    source 1076
    target 1078
  ]
  edge [
    source 1076
    target 1079
  ]
  edge [
    source 1076
    target 1080
  ]
  edge [
    source 1076
    target 1081
  ]
  edge [
    source 1076
    target 1082
  ]
  edge [
    source 1076
    target 1083
  ]
  edge [
    source 1076
    target 1084
  ]
  edge [
    source 1076
    target 1085
  ]
  edge [
    source 1076
    target 1086
  ]
  edge [
    source 1077
    target 1078
  ]
  edge [
    source 1077
    target 1079
  ]
  edge [
    source 1077
    target 1080
  ]
  edge [
    source 1077
    target 1081
  ]
  edge [
    source 1077
    target 1082
  ]
  edge [
    source 1077
    target 1083
  ]
  edge [
    source 1077
    target 1084
  ]
  edge [
    source 1077
    target 1085
  ]
  edge [
    source 1077
    target 1086
  ]
  edge [
    source 1078
    target 1079
  ]
  edge [
    source 1078
    target 1080
  ]
  edge [
    source 1078
    target 1081
  ]
  edge [
    source 1078
    target 1082
  ]
  edge [
    source 1078
    target 1083
  ]
  edge [
    source 1078
    target 1084
  ]
  edge [
    source 1078
    target 1085
  ]
  edge [
    source 1078
    target 1086
  ]
  edge [
    source 1079
    target 1080
  ]
  edge [
    source 1079
    target 1081
  ]
  edge [
    source 1079
    target 1082
  ]
  edge [
    source 1079
    target 1083
  ]
  edge [
    source 1079
    target 1084
  ]
  edge [
    source 1079
    target 1085
  ]
  edge [
    source 1079
    target 1086
  ]
  edge [
    source 1080
    target 1081
  ]
  edge [
    source 1080
    target 1082
  ]
  edge [
    source 1080
    target 1083
  ]
  edge [
    source 1080
    target 1084
  ]
  edge [
    source 1080
    target 1085
  ]
  edge [
    source 1080
    target 1086
  ]
  edge [
    source 1081
    target 1082
  ]
  edge [
    source 1081
    target 1083
  ]
  edge [
    source 1081
    target 1084
  ]
  edge [
    source 1081
    target 1085
  ]
  edge [
    source 1081
    target 1086
  ]
  edge [
    source 1082
    target 1083
  ]
  edge [
    source 1082
    target 1084
  ]
  edge [
    source 1082
    target 1085
  ]
  edge [
    source 1082
    target 1086
  ]
  edge [
    source 1083
    target 1084
  ]
  edge [
    source 1083
    target 1085
  ]
  edge [
    source 1083
    target 1086
  ]
  edge [
    source 1084
    target 1085
  ]
  edge [
    source 1084
    target 1086
  ]
  edge [
    source 1085
    target 1086
  ]
  edge [
    source 1087
    target 1088
  ]
  edge [
    source 1089
    target 1090
  ]
  edge [
    source 1089
    target 1091
  ]
  edge [
    source 1089
    target 1092
  ]
  edge [
    source 1089
    target 1093
  ]
  edge [
    source 1090
    target 1091
  ]
  edge [
    source 1090
    target 1092
  ]
  edge [
    source 1090
    target 1093
  ]
  edge [
    source 1091
    target 1092
  ]
  edge [
    source 1091
    target 1093
  ]
  edge [
    source 1092
    target 1093
  ]
]
