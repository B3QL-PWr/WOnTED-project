graph [
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 2
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "dziewczynka"
    origin "text"
  ]
  node [
    id 4
    label "dziewczyna"
  ]
  node [
    id 5
    label "prostytutka"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "dziecko"
  ]
  node [
    id 8
    label "potomkini"
  ]
  node [
    id 9
    label "siksa"
  ]
  node [
    id 10
    label "dziewcz&#281;"
  ]
  node [
    id 11
    label "partnerka"
  ]
  node [
    id 12
    label "m&#322;&#243;dka"
  ]
  node [
    id 13
    label "dziunia"
  ]
  node [
    id 14
    label "dziecina"
  ]
  node [
    id 15
    label "dziewka"
  ]
  node [
    id 16
    label "kora"
  ]
  node [
    id 17
    label "dziewczynina"
  ]
  node [
    id 18
    label "sympatia"
  ]
  node [
    id 19
    label "dziewoja"
  ]
  node [
    id 20
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 21
    label "sikorka"
  ]
  node [
    id 22
    label "dzieciarnia"
  ]
  node [
    id 23
    label "m&#322;odzik"
  ]
  node [
    id 24
    label "utuli&#263;"
  ]
  node [
    id 25
    label "zwierz&#281;"
  ]
  node [
    id 26
    label "organizm"
  ]
  node [
    id 27
    label "m&#322;odziak"
  ]
  node [
    id 28
    label "pedofil"
  ]
  node [
    id 29
    label "dzieciak"
  ]
  node [
    id 30
    label "potomstwo"
  ]
  node [
    id 31
    label "potomek"
  ]
  node [
    id 32
    label "sraluch"
  ]
  node [
    id 33
    label "utulenie"
  ]
  node [
    id 34
    label "utulanie"
  ]
  node [
    id 35
    label "fledgling"
  ]
  node [
    id 36
    label "utula&#263;"
  ]
  node [
    id 37
    label "entliczek-pentliczek"
  ]
  node [
    id 38
    label "niepe&#322;noletni"
  ]
  node [
    id 39
    label "cz&#322;owieczek"
  ]
  node [
    id 40
    label "pediatra"
  ]
  node [
    id 41
    label "krewna"
  ]
  node [
    id 42
    label "asymilowa&#263;"
  ]
  node [
    id 43
    label "nasada"
  ]
  node [
    id 44
    label "profanum"
  ]
  node [
    id 45
    label "wz&#243;r"
  ]
  node [
    id 46
    label "senior"
  ]
  node [
    id 47
    label "asymilowanie"
  ]
  node [
    id 48
    label "os&#322;abia&#263;"
  ]
  node [
    id 49
    label "homo_sapiens"
  ]
  node [
    id 50
    label "osoba"
  ]
  node [
    id 51
    label "ludzko&#347;&#263;"
  ]
  node [
    id 52
    label "Adam"
  ]
  node [
    id 53
    label "hominid"
  ]
  node [
    id 54
    label "posta&#263;"
  ]
  node [
    id 55
    label "portrecista"
  ]
  node [
    id 56
    label "polifag"
  ]
  node [
    id 57
    label "podw&#322;adny"
  ]
  node [
    id 58
    label "dwun&#243;g"
  ]
  node [
    id 59
    label "wapniak"
  ]
  node [
    id 60
    label "duch"
  ]
  node [
    id 61
    label "os&#322;abianie"
  ]
  node [
    id 62
    label "antropochoria"
  ]
  node [
    id 63
    label "figura"
  ]
  node [
    id 64
    label "g&#322;owa"
  ]
  node [
    id 65
    label "mikrokosmos"
  ]
  node [
    id 66
    label "oddzia&#322;ywanie"
  ]
  node [
    id 67
    label "ma&#322;pa"
  ]
  node [
    id 68
    label "dziewczyna_lekkich_obyczaj&#243;w"
  ]
  node [
    id 69
    label "diva"
  ]
  node [
    id 70
    label "kurwa"
  ]
  node [
    id 71
    label "jawnogrzesznica"
  ]
  node [
    id 72
    label "rozpustnica"
  ]
  node [
    id 73
    label "pigalak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
]
